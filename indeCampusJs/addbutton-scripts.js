/*---LEFT BAR ACCORDION----*/
var counterforadditem = ""; 
var nTR = 0;
$(function() {
   if( jQuery('.tableDateSlier').length > 0){ 
			$('.tableDateSlier').bxSlider({
					  nextSelector: '.iconTableNext',
					  prevSelector: '.iconTablePrev',
					  infiniteLoop: false,
					  hideControlOnEnd:true 
			});
	}
	if( jQuery(".itemNumber a").length > 0){ 
		 $(".itemNumber a").click(function(){
			 $('.packageTable').removeClass('packageColorChange');
			 $('.itemNumber').removeClass('active');
			$(this).parent('.itemNumber').addClass('active');
			$(this).parents('.packageTable').addClass('packageColorChange');
		 })
	 }
	 if( jQuery(".fa-building-o").length > 0){ 
	 
	 $('[data-number]').click(function(){
		 	$('[data-get-number]').focus();
			var getNumber = $(this).attr('data-number');
			var targetnumber = $('[data-get-number]');
			targetnumber.val(targetnumber.val()+''+getNumber);
		});
		$('[data-get-number]').blur(function(){
		  	$('.calculaterBox').slideUp()
		})
		
	  $('.fa-building-o').click(function(){
		  $('[data-get-number]').focus();
		var calculaterBox =  $(this).next('.calculaterBox')
		if($('.calculaterBox').is(":visible")){
			if(calculaterBox.is(":visible")){
				calculaterBox.slideUp()
			}else{
				$('.calculaterBox').slideUp()
				calculaterBox.slideDown()
			}
			
		}else{
			calculaterBox.slideDown()	
		}
	   });
	   }
	   
	   $('.cliendAddress').hide();
	    var getNumber = $('[data-address]').val();
	    $('[data-id="data'+getNumber+'"]').show();
		
	   $('[data-address]').change(function(){
			   var getNumber = $(this).val();
			   var targetnumber = $('[data-id]');
			   targetnumber.hide();
			   $('[data-id="data'+getNumber+'"]').show();
		  
		  })
	   
	 if( jQuery('#selectyourlocation').length > 0){ 
	  var getselectyourlocation = $('#selectyourlocation').val();
	  if(getselectyourlocation!="notSelect"){
		  $('.messagelocation').hide()
	 	  $(".showyourlocation").show();
	  }
	 }
    if( jQuery('#selectyourlocation').length > 0){ 
   	$("#selectyourlocation").change(function(){
	  var yourlocation = $(this).val()
	  if(yourlocation=='notSelect'){
		  $('.messagelocation').show()
		  $(".showyourlocation").hide();
	  }else{
		  $('.messagelocation').hide()
	 		$(".showyourlocation").show();
	  }
	 })
	 }
	if( jQuery('#SpaceManagementedir').length > 0){ 
	 $('#SpaceManagementedir').dataTable( {
	       "bPaginate": false,
		  "bRetrieve": true,
		  "bInfo": false,		  
		  "sZeroRecords": "No records to display"
	    } );
	}
	if( jQuery('#SpaceManagementedir2').length > 0){ 
	 $('#SpaceManagementedir2').dataTable( {
	       "bPaginate": false,
		   "bInfo": false,	   
	    } );
	}
	if( jQuery('#SpaceManagementedir3').length > 0){ 
	 $('#SpaceManagementedir3').dataTable( {
	       "bPaginate": false,
		   "bInfo": false,	   
	    } );
	}
	 if( jQuery('.venuerList [type="radio"]').length > 0){ 
		$('.venuerList [type="radio"]').click(function(){
				if($("#odcradio").is(":checked")){
					$('.odcBox').show()
				}else{
					$('.odcBox').hide()
				}
			  
			 })
	 }	
   
   if( jQuery("#nav-accordion").length > 0){ 
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
	}
	
	var availableTags = [
      "Ahmedabad",
      "Mehsana",
      "Surat",
      "Baroda",
      "Ghandhinagar",
      "Patan",
      "Kalol",
      "Mumbai",
      "Porbandar"
    ];
	
   if(jQuery("#searchtag").length > 0){ 
    $("#searchtag").autocomplete({
      source: availableTags
    });
   }
   
   
   if( jQuery('.eventDataListing li a').length > 0){   
	   $(".eventDataListing li a").click(function(){
			var accordionContainer =  $(this).next('ul');
			var thisaccordionContainer = $(this).parents('.eventDataListing').find('ul');
			if(thisaccordionContainer.is(":visible")){
				if(accordionContainer.is(":visible")){
					accordionContainer.slideUp().parents("li");
				}else{
					thisaccordionContainer.slideUp().parents("li");
					accordionContainer.slideDown().parents("li"); 
				}
			
			}else{
				accordionContainer.slideDown().parents("li"); 
			}		
		})
	}
		
	if( jQuery('#my_multi_select2').length > 0){
        multiSelectFunction();
	}
	

	if( jQuery('.carousel').length > 0){
		$('.carousel').carousel({
			interval: 0
		})
	}
	
	if( jQuery('#default').length > 0){
		$('#default').stepy({
			backLabel: 'Previous',
			block: true,
			nextLabel: 'Next',
			titleClick: true,
			validate:true,
			titleTarget: '.stepy-tab'
		});
	}
	
});


var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });

// custom scrollbar
    $("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});

    $("html").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});
	
	$(".addpagescroll").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});
	
	$(".pricing-table ul").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '5', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});

// widget tools

    jQuery('.iconDownArrow').click(function () {
        var el = jQuery(this).parents(".pricing-table").children(".list-unstyled");
        if (jQuery(this).hasClass("iconDownArrowDown")) {
            jQuery(this).removeClass("iconDownArrowDown").addClass("iconDownArrowup");
            el.slideDown(200);
        } else {
            jQuery(this).removeClass("iconDownArrowup").addClass("iconDownArrowDown");
            el.slideUp(200);
        }
    });
	
	jQuery('.tableDataCustomer .panel-heading .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".tableDataCustomer").children(".customerPanelBody");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });


    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips
	if( jQuery('.tooltips').length > 0){ 
    	$('.tooltips').tooltip();
	}
//    popovers

    $('.popovers').popover();



// custom bar chart

if ($(".custom-bar-chart")) {
    $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
    })
}

 if( jQuery('#addItemstep').length > 0){ 
 	itemSelections = $("#itemSelections");
	itemSelectionsinput = $("#itemSelections input[type='checkbox']");
	$("#addItemstep").click(function(){
		
		
		itemSelectionsinput.each(function() {
			
			if($(this).is(":checked")){
				var itelval = $(this).val();
				var itelTotal = $(this).parents('li').find('select').val();
				$('<li><div class="task-title"> <span class="task-title-sp">'+itelval+'</span><div class="pull-right hidden-phone">'+itelTotal+'</div></div></li>').appendTo('.selectItemSequence');
			}
			
		});
		
	})
}
}();


$(document).ready(function() {
			
	//$("#listitemadd").hide(); //for lable hide 
		   
	
	$(document).on('click','.deletebtn',function(){
		$(this).closest('tr').remove();
	});
	
	$(document).on('click','.plusbtn',function(){
		
        var tmp_a = $(".selecttt").val();
		var tmp_b = $(".gss").val();
		var tmp_c = $(".itemm").val();
		var tmp_d = $(".qtyy").val();		
		var tmp_e = $(".amtt").val();		
		var rowCount = $('#gama tr').length;
		rowCount++;
		var temp_html='<tr class="gradeX">'+
                  '<td data-value = '+rowCount+'>1</td>'+
                  '<td>'+tmp_a+'</td>'+
                  '<td>'+tmp_b+'</td>'+
                  '<td>'+tmp_c+'</td>'+
                  '<td>'+tmp_d+'</td>'+
				  '<td>'+tmp_e+'</td>'+
                  '<td><a href="#" class="adisp fd editbtn" title="Edit"><i class="fa fa-edit saveedit btn btn-success buttontable"></i></a> <a  href="#" class="adisp deletebtn" title="Delete"><i class="fa fa-trash-o btn btn-success buttontable"></i></a></td>'+
              '</tr>';
		
                
				
               
				
		if($(this).find('.pico').hasClass("fa-check")){
			
			$(".changico").removeClass("fa-check");
			$(".fx").remove();
			$(".changico").addClass("fa-plus-square");
			
			
			$("#gama tr:nth-child("+nTR+")").find('td:nth-child(1)').text($(".srr").val());
			$("#gama tr:nth-child("+nTR+")").find('td:nth-child(2)').text(tmp_a);
			$("#gama tr:nth-child("+nTR+")").find('td:nth-child(3)').text(tmp_b);
			$("#gama tr:nth-child("+nTR+")").find('td:nth-child(4)').text(tmp_c);
			$("#gama tr:nth-child("+nTR+")").find('td:nth-child(5)').text(tmp_d);
			$("#gama tr:nth-child("+nTR+")").find('td:nth-child(6)').text(tmp_e);
			
			
		}else{
			$("#gama").append(temp_html);
		}
		
		 		$(".selecttt").val('');
                $(".gss").val('');
				$(".itemm").val('');
				$(".qtyy").val('');
                $(".amtt").val('');
				$(".srr").val('');
		
	});					   
    
	$(document).on('click','.editbtn',function(){
		
		$(".selecttt").val($(this).closest('tr').find('td:nth-child(2)').text());
        $(".gss").val($(this).closest('tr').find('td:nth-child(3)').text());
		$(".itemm").val($(this).closest('tr').find('td:nth-child(4)').text());
		$(".qtyy").val($(this).closest('tr').find('td:nth-child(5)').text());
        $(".amtt").val($(this).closest('tr').find('td:nth-child(6)').text());
		$(".srr").val($(this).closest('tr').find('td:nth-child(1)').text());
		$(".changico").addClass("fa-check");
		$(".changico").removeClass("fa-plus-square");
		nTR = $(this).closest('tr').find('td:nth-child(1)').attr('data-value');
		 $(".minusbtn").append('<a class="btn btn-success buttontable adisp fx" title="Cancel" style="padding: 5px 13px !important;" href="#"><i class="fa fa-times"></i></a>');
	});
	
	$(document).on('click','.fx',function(){
											   
	            $(".selecttt").val('');
                $(".gss").val('');
				$(".itemm").val('');
				$(".qtyy").val('');
                $(".amtt").val('');
				$(".srr").val('');
	           $(this).remove();
			   $(".changico").removeClass("fa-check");
			$(".changico").addClass("fa-plus-square");
	
	
	});
	
						   
	$(document).on('click','.poadd',function(){
		var qty=$("#qtynew").val();
		var name=$("#search").val();
		var taxable=$("#checkbox-01").val();
		
		counterforadditem = 1;
		if(counterforadditem == 1){
			$("#listitemadd").css('visibility', 'visible');
		}
		
		var itemtax = $("#checkbox-01").is(':checked')?"Yes":"No";
		var amt=$("#amtnew").val();
		//var uomid1=$("#uomid").val();
                var uomname = $("#uomnew").val(); 
				
                if(name != '' && qty != '' ){     //alert message if name and qty is blank              
                }else{
                    alert("Please Enter Detail");
                    return false;
                }
				
               
		var temp_html='<div class="row"><div class="row check add-price-row"><div class="col-lg-3 recipebox1" style="width:20%;">'+
                                '<div class="bio-row">'+
                                    '<input type="text" class="form-control " disabled value="'+name+'"> '+
                                    
                                '</div>'+                          
                            '</div>'+

                            '<div class="col-lg-2 recipebox5">'+
                                '<div class="bio-row">'+
                                    '<input class="form-control"  placeholder="Qty" size="16" value="'+qty+'"  type="text" >'+
									
                                '</div>'+
                            '</div>'+

                            '<div class="col-lg-2 recipebox5">'+
                                '<div class="bio-row">'+
                                
                                '<input class="form-control"  placeholder="Uom" size="16" disabled="" value="'+uomname+'">'+ 
                               '</div>'+
                            '</div>'+
							

							'<div class="col-lg-2 recipebox5">'+
                                '<div class="bio-row">'+
                                 '<input class="form-control"  placeholder="Amt" size="16" value="'+amt+'" type="text">'+
                               
                               '</div>'+
                            '</div>'+
							
							'<div class="col-lg-2 recipebox5">'+
                                '<div class="bio-row">'+
                                 ' <p style=" padding-top:5px;"><span>'+itemtax+'</span></p>'+
                               
                               '</div>'+
                            '</div>'+
							
                            '<div class="col-lg-2 recipebox4" style="margin-top:5px;">'+
                                '<div class="bio-row">'+
                                    '<a  class="delete sprite iconDelete remove-price">Remove</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+                    
                '</div>';
                $("#selectadditembox").append(temp_html);
                $("#search").val('');
                $("#itemid").val('');
		$("#qty").val('');
		$("#uomid").val('');
                $("#uom").val('');
	});					  
	
	
	// Price - Price Validity Row Add-Delete
	
	$(document).on('click','.add-price',function(){
		$(this).next().show();
		var AddRow = $(this).parents("div.add-price-row").clone().find("input").val("").end();
		$(this).parents("div.add-price-row").after(AddRow);
		$('.default-date-picker').datepicker()
		//$.getScript( "../js/bootstrap-datepicker.js", function() {});
		$(this).hide();
	});
	
	 $(document).on('click','.remove-price',function(){
                var hiddenval = $("#deleteitemid").val();
                hiddenval += this.id + ",";
                $("#deleteitemid").val(hiddenval);
                
		var TotalRow = $("div.add-price-row").length;
		var LastRow = TotalRow - 2;
		if(TotalRow == 2){
			$("div.add-price-row").find(".add-price").show();
			$("div.add-price-row").find(".remove-price").hide();
		}
		if($(this).prev(".add-price").css("display") == "block"){
			$("div.add-price-row").eq(TotalRow-2).find(".add-price").show();
		}
                
		$(this).hide();
		$(this).parents("div.add-price-row").remove();
	});
	// End
	
	$('#demo').html( '<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>' );
    $('#example').dataTable( {
        "aaData": [
            /* Reduced data set */
            [ "Trident", "Internet Explorer 4.0", "Win 95+", 4, "X" ],
            [ "Trident", "Internet Explorer 5.0", "Win 95+", 5, "C" ],
            [ "Trident", "Internet Explorer 5.5", "Win 95+", 5.5, "A" ],
            [ "Trident", "Internet Explorer 6.0", "Win 98+", 6, "A" ],
            [ "Trident", "Internet Explorer 7.0", "Win XP SP2+", 7, "A" ],
            [ "Gecko", "Firefox 1.5", "Win 98+ / OSX.2+", 1.8, "A" ],
            [ "Gecko", "Firefox 2", "Win 98+ / OSX.2+", 1.8, "A" ],
            [ "Gecko", "Firefox 3", "Win 2k+ / OSX.3+", 1.9, "A" ],
            [ "Webkit", "Safari 1.2", "OSX.3", 125.5, "A" ],
            [ "Webkit", "Safari 1.3", "OSX.3", 312.8, "A" ],
            [ "Webkit", "Safari 2.0", "OSX.4+", 419.3, "A" ],
            [ "Webkit", "Safari 3.0", "OSX.4+", 522.1, "A" ]
        ],
        "aoColumns": [
            { "sTitle": "Engine" },
            { "sTitle": "Browser" },
            { "sTitle": "Platform" },
            { "sTitle": "Version", "sClass": "center" },
            { "sTitle": "Grade", "sClass": "center" }
        ]
    } );   
		
} );


function multiSelectFunction(){
    $('#my_multi_select2').multiSelect({
        selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search...'>",
        selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search...'>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });
}

