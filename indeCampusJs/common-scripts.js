/*---LEFT BAR ACCORDION----*/

$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
    $('#nav-accordion-side').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
    $('#nav-accordion-sub').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});

// right slidebar
//$(function(){
// $.slidebars();
//});



$(function () {
    if (jQuery('.tableDateSlier').length > 0) {
        $('.tableDateSlier').bxSlider({
            nextSelector: '.iconTableNext',
            prevSelector: '.iconTablePrev',
            infiniteLoop: false,
            hideControlOnEnd: true
        });
    }
    if (jQuery(".itemNumber a").length > 0) {
        $(".itemNumber a").click(function () {
            $('.packageTable').removeClass('packageColorChange');
            $('.itemNumber').removeClass('active');
            $(this).parent('.itemNumber').addClass('active');
            $(this).parents('.packageTable').addClass('packageColorChange');
        })
    }
    if (jQuery(".fa-building-o").length > 0) {

        $('[data-number]').click(function () {
            $('[data-get-number]').focus();
            var getNumber = $(this).attr('data-number');
            var targetnumber = $('[data-get-number]');
            targetnumber.val(targetnumber.val() + '' + getNumber);
        });
        $('[data-get-number]').blur(function () {
            $('.calculaterBox').slideUp()
        })

        $('.fa-building-o').click(function () {
            $('[data-get-number]').focus();
            var calculaterBox = $(this).next('.calculaterBox')
            if ($('.calculaterBox').is(":visible")) {
                if (calculaterBox.is(":visible")) {
                    calculaterBox.slideUp()
                } else {
                    $('.calculaterBox').slideUp()
                    calculaterBox.slideDown()
                }

            } else {
                calculaterBox.slideDown()
            }
        });
    }

    $('.cliendAddress').hide();
    var getNumber = $('[data-address]').val();
    $('[data-id="data' + getNumber + '"]').show();

    $('[data-address]').change(function () {
        var getNumber = $(this).val();
        var targetnumber = $('[data-id]');
        targetnumber.hide();
        $('[data-id="data' + getNumber + '"]').show();

    })

    if (jQuery('#selectyourlocation').length > 0) {
        var getselectyourlocation = $('#selectyourlocation').val();
        if (getselectyourlocation != "notSelect") {
            $('.messagelocation').hide()
            $(".showyourlocation").show();
        }
    }
    if (jQuery('#selectyourlocation').length > 0) {
        $("#selectyourlocation").change(function () {
            var yourlocation = $(this).val()
            if (yourlocation == 'notSelect') {
                $('.messagelocation').show()
                $(".showyourlocation").hide();
            } else {
                $('.messagelocation').hide()
                $(".showyourlocation").show();
            }
        })
    }

    if (jQuery('.venuerList [type="radio"]').length > 0) {
        $('.venuerList [type="radio"]').click(function () {
            if ($("#odcradio").is(":checked")) {
                $('.odcBox').show()
            } else {
                $('.odcBox').hide()
            }

        })
    }

    if (jQuery("#nav-accordion").length > 0) {
        $('#nav-accordion').dcAccordion({
            eventType: 'click',
            autoClose: true,
            saveState: true,
            disableLink: true,
            speed: 'slow',
            showCount: false,
            autoExpand: true,
//        cookie: 'dcjq-accordion-1',
            classExpand: 'dcjq-current-parent'
        });
    }

    /*var availableTags = [
     "Dominos",
     "U.S Pizza",
     "Hero MotoCorp",
     "Yamaha Motor Company",
     "Suzuki Motor Corporation",
     "Mercedes-Benz",
     "Adidas",
     "Reebok",
     "Reliance Communications",
     "Tata DoCoMo",
     "Karnavati Club Limited",
     "The Grand Bhagwati Hotel"
     ];
     
     if(jQuery("#searchtag").length > 0){ 
     $("#searchtag").autocomplete({
     source: availableTags
     });
     }*/


    if (jQuery('.eventDataListing li a').length > 0) {
        $(".eventDataListing li a").click(function () {
            var accordionContainer = $(this).next('ul');
            var thisaccordionContainer = $(this).parents('.eventDataListing').find('ul');
            if (thisaccordionContainer.is(":visible")) {
                if (accordionContainer.is(":visible")) {
                    accordionContainer.slideUp().parents("li");
                } else {
                    thisaccordionContainer.slideUp().parents("li");
                    accordionContainer.slideDown().parents("li");
                }

            } else {
                accordionContainer.slideDown().parents("li");
            }
        })
    }

    if (jQuery('#my_multi_select1').length > 0) {
        $('#my_multi_select1').multiSelect({
            selectableHeader: "<h3>List Option</h3>",
            selectionHeader: "<h3>Selected Item</h3>"
        });
    }

    if (jQuery('.carousel').length > 0) {
        $('.carousel').carousel({
            interval: 0
        })
    }

    if (jQuery('#default').length > 0) {
        $('#default').stepy({
            backLabel: 'Previous',
            block: true,
            nextLabel: 'Next',
            titleClick: true,
            titleTarget: '.stepy-tab'
        });
    }


});


var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if (diff > 0)
            $("#sidebar").scrollTo("-=" + Math.abs(diff), 500);
        else
            $("#sidebar").scrollTo("+=" + Math.abs(diff), 500);
    });

//    sidebar toggle

    $(function () {



        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });

// custom scrollbar
    $("#sidebar").niceScroll({styler: "fb", cursorcolor: "#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', horizrailenabled: false});

    $("body").niceScroll({styler: "fb", cursorcolor: "#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', zindex: '1000', horizrailenabled: false});

    $(".addpagescroll").niceScroll({styler: "fb", cursorcolor: "#6dbb4a", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', zindex: '1000', horizrailenabled: false});

    $(".addscroll2").niceScroll({styler: "fb", cursorcolor: "#6dbb4a", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', zindex: '1000', horizrailenabled: false});

    $(".pricing-table ul").niceScroll({styler: "fb", cursorcolor: "#e8403f", cursorwidth: '5', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', zindex: '1000', horizrailenabled: false});

// widget tools

    jQuery('.iconDownArrow').click(function () {
        var el = jQuery(this).parents(".pricing-table").children(".list-unstyled");
        if (jQuery(this).hasClass("iconDownArrowDown")) {
            jQuery(this).removeClass("iconDownArrowDown").addClass("iconDownArrowup");
            el.slideDown(200);
        } else {
            jQuery(this).removeClass("iconDownArrowup").addClass("iconDownArrowDown");
            el.slideUp(200);
        }
    });

    jQuery('.tableDataCustomer .panel-heading .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".tableDataCustomer").children(".customerPanelBody");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });


    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips
    if (jQuery('.tooltips').length > 0) {
        $('.tooltips').tooltip();
    }
//    popovers

    $('.popovers').popover();



// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

    if (jQuery('#addItemstep').length > 0) {
        itemSelections = $("#itemSelections");
        itemSelectionsinput = $("#itemSelections input[type='checkbox']");
        $("#addItemstep").click(function () {


            itemSelectionsinput.each(function () {

                if ($(this).is(":checked")) {
                    var itelval = $(this).val();
                    var itelTotal = $(this).parents('li').find('select').val();
                    $('<li><div class="task-title"> <span class="task-title-sp">' + itelval + '</span><div class="pull-right hidden-phone">' + itelTotal + '</div></div></li>').appendTo('.selectItemSequence');
                }

            });

        })
    }
}();

//modal effect added by Mehul @26/6/2015
$(document).ready(function () {
    $('.modal').on('show.bs.modal', function () {
        $('.modal').removeClass('closeanimate');
//        $('.modal-backdrop').fadeIn();
        $(this).fadeIn(10);
    });
    
  
 
    
//    $('.event-date-picker').on('hide.bs.modal', function (event) {
//        event.stopPropagation();
//        event.preventDefault();
//      console.log('closeEvent');
//    });
    
    
    
$('.event-date-picker').on('change', function(){ $('.datepicker').hide(); });
    
     $(document).on('click','.modal .close',function () {
        $(this).parents('.modal').addClass('closeanimate');
        $(this).parents('.modal').delay(100).modal('hide');
        $('.modal-backdrop').hide();
    });
});


