//date picker start

if (top.location != location) {
    top.location.href = document.location.href;
}
$(function () {
    window.prettyPrint && prettyPrint();
    $('.default-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true
    });
    $('.default-dateTime-picker').datetimepicker({
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        autoclose: true,
        minuteStep: 15,
    });
    $('.normal-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });
    $('.back-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        endDate: 'today',
        autoclose: true
    });
    
    $('.dp').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true
    });

    $('.headerDp').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $('.default-date-picker').prop("readonly",true);
    $('.default-dateTime-picker').prop("readonly",true);
    $('.default-EndDateTime-picker').prop("readonly",true);
    $('.dueDatedp-dateTime').prop("readonly",true);
    $('.back-date-picker').prop("readonly",true);
    $('.default-date-picker').click(function(){$(this).trigger('focus')});
    $('.timepicker-default').prop("readonly",true);
    $('.timepicker-default').click(function(){$(this).trigger('focus')});
    $('.clockface-open').click(function(){$(this).trigger('focus')});
    $('.dpYears').datepicker();
    $('.dpMonths').datepicker();

    
    var startDate = new Date(2012, 1, 20);
    var endDate = new Date(2012, 1, 25);


    $('.dp4').datepicker()
        .on('changeDate', function (ev) {
            
            if (ev.date.valueOf() > endDate.valueOf()) {
                $('.alert').show().find('strong').text('The start date can not be greater then the end date');
            } else {
                $('.alert').hide();
                startDate = new Date(ev.date);
                $('#startDate').text($('.dp4').data('date'));
            }
            $('.dp4').datepicker('hide');
        });
    $('.dp5').datepicker()
        .on('changeDate', function (ev) {
            if (ev.date.valueOf() < startDate.valueOf()) {
                $('.alert').show().find('strong').text('The end date can not be less then the start date');
            } else {
                $('.alert').hide();
                endDate = new Date(ev.date);
                $('.endDate').text($('.dp5').data('date'));
            }
            $('.dp5').datepicker('hide');
        });

    // disabling dates
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('.dpd1').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true,
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        if(ev.date.valueOf() > SYSMAXDATE.valueOf()){
            alert(MAXDATEREACHED +SYSMAXDATE.format("dd-MM-yyyy"));
            $('.dpd1').val('');
        }else{
            if (ev.date.format("dd-MM-yyyy") < new Date(getTodaysDate()).format("dd-MM-yyyy")) {
                checkin.hide();
                alert("Date Can not be less Then Todays Date...!!");
                setTimeout(function () {
                    $('.dpd1').val('');
                }, 200);
            } else {
                checkin.hide();
                $('.dpd2')[0].focus();
            }

        }
    }).data('datepicker');
    var checkout = $('.dpd2').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true,
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        //log(ev);
        if (new Date(ddmmyyToMysql($('.dpd2').val())).valueOf() < new Date(ddmmyyToMysql($('.dpd1').val())).valueOf()) {
            checkout.hide();
            alert("End Date Cannot Be Less then Start Date");
            setTimeout(function () {
                $('.dpd2').val('');
                $('.dpd2').focus();
            }, 200);
        } else {
            checkout.hide();
        }

    }).data('datepicker');
    $('.dpd1').prop("readonly",true);
    $('.dpd2').prop("readonly",true);
    
    
    // NORMAL COMBO DATE PICKER WITH VALIDATION DATEPICKER 
    var checkin = $('.dp10').datepicker({
        format: 'dd-mm-yyyy',
        //startDate: 'today',
        autoclose: true,
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        if(ev.date.valueOf() > SYSMAXDATE.valueOf()){
            alert(MAXDATEREACHED +SYSMAXDATE.format("dd-MM-yyyy"));
            $('.dp10').val('');
        }else{

            checkin.hide();
            $('.dp11')[0].focus();
            

        }
    }).data('datepicker');
    var checkout = $('.dp11').datepicker({
        format: 'dd-mm-yyyy',
        //startDate: 'today',
        orientation: 'right',
        autoclose: true,
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        //log(ev);
        if (new Date(ddmmyyToMysql($('.dp11').val())).valueOf() < new Date(ddmmyyToMysql($('.dp10').val())).valueOf()) {
            checkout.hide();
            alert("End Date Cannot Be Less then Start Date");
            setTimeout(function () {
                $('.dp11').val('');
                $('.dp11').focus();
            }, 200);
        } else {
            checkout.hide();
        }

    }).data('datepicker');
    $('.dp10').prop("readonly",true);
    $('.dp11').prop("readonly",true);
});

//date picker end


//datetime picker start

/*
 $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

 $(".form_datetime-component").datetimepicker({
 format: "dd MM yyyy - hh:ii"
 });

 $(".form_datetime-adv").datetimepicker({
 format: "dd MM yyyy - hh:ii",
 autoclose: true,
 todayBtn: true,
 startDate: "2013-02-14 10:00",
 minuteStep: 15,
 });

 $(".form_datetime-meridian").datetimepicker({
 format: "dd MM yyyy - HH:ii P",
 showMeridian: true,
 autoclose: true,
 todayBtn: true
 });

 //datetime picker end

 //timepicker start
 $('.timepicker-default').timepicker();


 $('.timepicker-24').timepicker({
 autoclose: true,
 minuteStep: 15,
 showSeconds: true,
 showMeridian: false
 });

 //timepicker end

 */
function initDefaultDateTimePicker(){
    $('.default-dateTime-picker').datetimepicker({
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        autoclose: true,
        minuteStep: 15,
    });
    $('.default-dateTime-picker').prop("readonly",true);
}

function intdefaultdatepicker() {
    $('.new-default-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true
    });
    $('.new-default-date-picker').prop("readonly",true);
}

function initNoramlDatePicker(){
    $('.normal-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });
}

function intdefaulttimepicker() {
    $('.timepicker-default').timepicker({
        defaultTime : 'current'
    });
}

function intClosuretimepicker() {
    $('.timepicker-closure').timepicker({
        defaultTime : 'current',
    });
}

function closuredefaultDatePicker(startDate, endDate){
    //log("start datae"+startDate+"---"+"End Date:"+endDate);
    $('.closure-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(startDate).format("dd-MM-yyyy"),
        endDate: new Date(endDate).format("dd-MM-yyyy"),
        autoclose: true
    })
    $('.closure-date-picker').prop("readonly",true);
}

function initeventBindDatePicker(startDate, endDate){
    //log("start date"+startDate+"---"+"End Date:"+endDate);
    //log("deepak")
    //var startDate = new Date(startDate).format("dd-MM-yyyy");
    //var endDate = new Date(endDate).format("dd-MM-yyyy");
    $('.event-date-picker').datepicker({
        format: 'dd-mm-yyyy',
        startDate: startDate,
        endDate: endDate,
        autoclose: true
    });
   
    $('.closure-date-picker').prop("readonly",true);
    $('.event-date-picker').prop("readonly",true);
}

function initDynamicDateBindingDatePicker(startDate, endDate){
    //log(startDate+"'"+endDate);
    // NORMAL COMBO DATE PICKER WITH VALIDATION DATEPICKER 
    //$('.dpdb1').datepicker();
    //$('.dpdb2').datepicker();
    var checkin = $('.dpdb1').datepicker({
        format: 'dd-mm-yyyy',
        startDate: mysqltoDesiredFormat(startDate,"dd-MM-yyyy"),
        endDate: mysqltoDesiredFormat(endDate,"dd-MM-yyyy"),
        autoclose: true,
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        if(ev.date.valueOf() > SYSMAXDATE.valueOf()){
            alert(MAXDATEREACHED +SYSMAXDATE.format("dd-MM-yyyy"));
            $('.dpdb1').val('');
        }else{

            checkin.hide();
            $('.dpdb2')[0].focus();
            

        }
    }).data('datepicker');
    var checkout = $('.dpdb2').datepicker({
        format: 'dd-mm-yyyy',
        startDate: mysqltoDesiredFormat(startDate,"dd-MM-yyyy"),
        endDate: mysqltoDesiredFormat(endDate,"dd-MM-yyyy"),
        autoclose: true,
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        //log(ev);
        if (new Date(ddmmyyToMysql($('.dpdb2').val())).valueOf() < new Date(ddmmyyToMysql($('.dpdb1').val())).valueOf()) {
            checkout.hide();
            alert("End Date Cannot Be Less then Start Date");
            setTimeout(function () {
                $('.dpdb2').val('');
                $('.dpdb2').focus();
            }, 200);
        } else {
            checkout.hide();
        }

    }).data('datepicker');
    $('.dpdb1').prop("readonly",true);
    $('.dpdb2').prop("readonly",true);
}
function intdefaulttooltips() {
    //alert("ia mher");
    // $('.tooltips').tooltip();
      $('.tooltips').tooltip({ container: 'body' });
}

function initDefaultNestable(){
    $('#nestableList').nestable();
}

function initCommonSlider(index){
    
    $("#slider-range_"+index).slider({
        range: true,
        slide: function (event, ui) {
            $(".slider-start-time_"+index).text(getAMPMvalueFromGivenDateTime(ui.values[0]));
            $(".slider-end-time_"+index).text(getAMPMvalueFromGivenDateTime(ui.values[1]));
        },
        change: function() { 
            range = $("#slider-range_"+index).slider("option", "values");
            //$(".slider-start-time_"+index).text(AMPMFromTimeStampSlider(range[0]));
            //$(".slider-end-time_"+index).text(AMPMFromTimeStampSlider(range[1]));
            $(".slider-start-time_"+index).text(getAMPMvalueFromGivenDateTime(range[0]));
            $(".slider-end-time_"+index).text(getAMPMvalueFromGivenDateTime(range[1]));
            $("#stimestamp_"+index).val(range[0]);
            $("#etimestamp_"+index).val(range[1]);
        }
    });
    
    range = $("#slider-range_"+index).slider("option", "values");
    if(range[0] == 0 && range[1] == 0){
        $(".slider-start-time_"+index).text("00-00-0000 00:00");
        $(".slider-end-time_"+index).text("00-00-0000 00:00");
    }else{
        $("#time-range").show();
        $("#alertMessage").hide();
        $(".slider-start-time_"+index).text(getAMPMvalueFromGivenDateTime(range[0]));
        $(".slider-end-time_"+index).text(getAMPMvalueFromGivenDateTime(range[1]));
    }
    
    // range min
    $("#slider-range-min_"+index).slider({
        range: "min",
        animate: true,
        min: 0,
        max: 10000,
        step: 50,
        slide: function (event, ui) {
            $(".slider-range-min-amount_"+index).text( ui.value);
        },
        change: function() { 
            var tempPax = $("#slider-range-min_"+index).slider("option", "value");
            //log(tempPax);
            $(".slider-range-min-amount_"+index).text(tempPax);
        }
    });

    $(".slider-range-min-amount_"+index).text( $("#slider-range-min_"+index).slider("value"));
    
    $('#slider-range_'+index).slider({ disabled: true });
    $("#slider-range-min_"+index).slider({ disabled: true });
}


function AMPMFromTimeStampSlider(values){
    var hours1 = Math.floor(values / 60);
    var minutes1 = values - (hours1 * 60);

    if (hours1.length == 1) hours1 = '0' + hours1;
    if (minutes1.length == 1) minutes1 = '0' + minutes1;
    if (minutes1 == 0) minutes1 = '00';
    if (hours1 >= 12) {
        if (hours1 == 12) {
            hours1 = hours1;
            minutes1 = minutes1 + " PM ";
        } else {
            hours1 = hours1 - 12;
            minutes1 = minutes1 + " PM ";
        }
    } else {
        hours1 = hours1;
        minutes1 = minutes1 + " AM ";
    }
    if (hours1 == 0) {
        hours1 = 12;
        minutes1 = minutes1;
    }

    return hours1 + ':' + minutes1;
}
function AMPMToTimeStampSlider(values){
    //log(values);
    var hrs = Number(values.match(/^(\d+)/)[1].trim());
    var mnts = Number(values.match(/:(\d+)/)[1].trim());
    var format = values.match(/\s(.*)$/)[1].trim();
    
    if (format == "PM" && hrs <= 12) hrs = hrs + 12;
    if (format == "AM" && hrs == 12) hrs = hrs - 12;
    
    
    var temphrs = Math.floor(hrs * 60);
    var tempminutes = Math.floor((mnts / 60)*60);
    
    return (temphrs+tempminutes);
}

function getAMPMvalueFromGivenDateTime(dateTime) {
    
    var currentTime = new Date(parseInt(dateTime)*1000);
    //log(currentTime);
    var date = ("0" + currentTime.getDate()).slice(-2);
    var month = ("0" + (currentTime.getMonth() + 1)).slice(-2);
    var year = currentTime.getFullYear();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();

    if (minutes < 10)
        minutes = "0" + minutes;

    var suffix = "AM";
    if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
    }
    if (hours == 0) {
        hours = 12;
    }
    var current_time = hours + ":" + minutes + " " + suffix;
    return date+"-"+month+"-"+year+" "+current_time;
}

function initsliderBar(){
    $.slidebars();
    
}

function initPophover(){
    $('[data-toggle="popover_tiles"]').popover({html: true, placement: "bottom"});   
}

function initBootStrapDateTimePicker(){
    $('.bootstrap-daterangepicker-basic').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    
    $('.bootstrap-daterangepicker-basic').prop("readonly",true);
}

function initBootStrapDatePicker(){
     
    $('.bootstrap-daterangepicker-basic').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true
    });
    $('.bootstrap-daterangepicker-basic').prop("readonly",true);
}


