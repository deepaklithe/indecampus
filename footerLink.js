/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 05-09-2017.
 * File : headerLink.js.
 * File Type : .js.
 * Project : POTG
 *
 * */

 var currPage = $(location).attr('href').split('.html')[0].split(httpHead  + "//" + FINALPATH)[1];

 var dynamicJsLinks = '<script src="mobileMenu.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/js/global-vendors.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/plugins/datatables/media/js/jquery.dataTables.min.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/plugins/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>'+
                        '<script src="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/scripts/tables-datatables.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/scripts/tables-datatables-editor-1.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/js/pleasure.js?v='+VERSION+'"></script>'+
                        '<script src="assets/admin1/js/layout.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/scripts/sliders.js?v='+VERSION+'"></script>'+
                        '<script src="indeCampusJs/advanced-form-components.js?v='+VERSION+'"></script>'+
                        '<script src="controller/lib.js?v='+VERSION+'"></script>'+
                        '<script src="indeCampusJs/bootstrap-typeahead.js?v='+VERSION+'"></script>'+
                        '<script src="controller/globalvar.js?v='+VERSION+'"></script>'+
                        '<script src="controller/commonController.js?v='+VERSION+'"></script>'+
                        '<script type="text/javascript" src="indeCampusJs/loader.js?v='+VERSION+'"></script>'+
                        '<script src="indeCampusJs/jspdf.min.js?v='+VERSION+'"></script>'+ 
                        '<script src="indeCampusJs/jspdf.plugin.autotable.js?v='+VERSION+'"></script>'+
                        '<script src="indeCampusJs/md5.min.js?v='+VERSION+'"></script>'+
                        '<script src="assets/intl/js/intlTelInput.js?v='+VERSION +'"></script>'+
                        '<script type="text/javascript" src="assets/globals/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js?v='+VERSION +'"></script>'+
                        '<script src="assets/globals/plugins/bootstrap-tour/js/bootstrap-tour.min.js?v='+VERSION+'"></script>'+
                        '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                        '<script src="assets/globals/plugins/pnikolov-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js?v='+VERSION+'"></script>'; // TOUR JS
 
    switch (currPage) {
        case "/inquiryCalender": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/globals/scripts/chart.bundle.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                // '<script src="controller/activityAppActListingController.js?v='+VERSION+'"></script>'+
                                // '<script src="controller/activityAppCommonController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/jquery-knob/excanvas.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/jquery-knob/dist/jquery.knob.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/charts-knob.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/index.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/fullcalendar/dist/fullcalendar.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>'+
                                '<script src="controller/inquiryCalenderController.js?v='+VERSION+'"></script>';
            break;

        case "/actSummaryReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/globals/scripts/index.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/actSummaryReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/pivotActSummary.js?v='+VERSION+'" type="text/javascript"></script>';
            break;

        case "/actTargetDist": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/clientActTargetDistController.js?v='+VERSION+'"></script>';
            break;

        case "/activity": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-editable.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/dynamic_table_init.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityApplib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppCommonController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppActListingController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jasny-bootstrap.min.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/activityAnalyticsChart": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/admin1/js/touchpoint.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAnalyticsController.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBXU_n338VgJvuwQI0BathW7QQqf5uIrG8" async="" defer="defer"></script>';
            break;

        case "/activityMainReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityMainReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/activityProjectPivotReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityProjectPivotReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/pivot.js?v='+VERSION+'" type="text/javascript"></script>';
            break;

        case "/activityProjectReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityProjectReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/activityTarget": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityTargetController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/activityTargetReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityTargetReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/pivot.js?v='+VERSION+'" type="text/javascript"></script>';
            break;

        case "/activitydetail": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-editable.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityApplib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppCommonController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jquery.progresstimer.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppActivityDetailsController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>';
            break;

        case "/adduser": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/addUserController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/analyticsChart": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/analyticsChartController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/amcharts.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/serial.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/moment.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/daterangepicker.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBXU_n338VgJvuwQI0BathW7QQqf5uIrG8" async="" defer="defer"></script>';
            break;

        case "/clientConfig": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/clientConfigController.js?v='+VERSION+'"></script>';
            break;

        case "/clientiteamgroup": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/itemgroupController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery.fancytree.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery.fancytree.edit.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/sample.js?v='+VERSION+'"></script>';
            break;

        case "/clientiteammas": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/itemmassController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery-ui.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery.treetable.js?v='+VERSION+'"></script>';
            break;

        case "/createRoom": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/createRoomController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery-ui.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery.treetable.js?v='+VERSION+'"></script>';
            break;

        case "/clientmaster": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '';
            break;

        case "/clientproposal": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/clientProposalController.js?v='+VERSION+'"></script>';
            break;

        case "/clientstatuscat": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/globals/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/switchery/dist/switchery.min.js?v='+VERSION+'"></script>'+
                                '<script src="controller/statusManagementController.js?v='+VERSION+'"></script>';
            break;

        case "/clienttarget": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/clientTargetController.js?v='+VERSION+'"></script>';
            break;

        case "/clientteam": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/teamController.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/jquery-ui-1.10.2.custom.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/jquery.layout-latest.min.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/primitives.min.js?v='+VERSION+'"></script>';
            break;


        case "/student": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/studentController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jasny-bootstrap.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/amcharts.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/serial.js?v='+VERSION+'"></script>';
            break;

        case "/customerDetail": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/customerDetailController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/customergroup": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/customerGroupController.js?v='+VERSION+'"></script>';
            break;

        case "/customFields": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/customFieldController.js?v='+VERSION+'"></script>'+
                              '<script src="indeCampusJs/jquery.tagsinput.js"></script>';
            break;

        case "/dashboardMasterCompo": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/dashboardMasterCompoController.js?v='+VERSION+'"></script>';
            break;

        case "/devloperbranch": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/developerBranchController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/propertyMaster": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/propertyMasterController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/devloperclient": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/admin1/js/jquery.validate.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/forms-wizard.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/jquery.bootstrap.wizard.min.js?v='+VERSION+'"></script>'+
                                '<script src="controller/clientManagementController.js?v='+VERSION+'"></script>'+  
                                '<script src="controller/Client_developerBranchController.js?v='+VERSION+'"></script>'+  
                                '<script src="controller/Client_userlevelController.js?v='+VERSION+'"></script>'+  
                                '<script src="controller/Client_addUserController.js?v='+VERSION+'"></script>'+  
                                '<script src="controller/Client_userManageController.js?v='+VERSION+'"></script>'+  
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/devloperclientList": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/clientListController.js?v='+VERSION+'"></script>';
            break;

        case "/devloperlookup": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/lookupManagementController.js?v='+VERSION+'"></script>';
            break;
        
        case "/devlopertype": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/developerTypeController.js?v='+VERSION+'"></script>';
            break;
        
        case "/documentManagement": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/documentManagementController.js?v='+VERSION+'"></script> ';
            break;
        
        case "/draggableInfo": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/configuredDashboardController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/amcharts.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/serial.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/jsapi"></script>'+
                                '<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBXU_n338VgJvuwQI0BathW7QQqf5uIrG8" async="" defer="defer"></script>'+
                                '<script src="assets/globals/plugins/fullcalendar/dist/fullcalendar.min.js?v='+VERSION+'"></script>';
            break;
        
        case "/dynamicProposalTemplate": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/dynamicProposalTemplateController.js?v='+VERSION+'"></script>';
            break;

        case "/dynamicReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/dynamicReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/treeJs/jquery-ui.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/dynamicReportList": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/dynamicReportListController.js?v='+VERSION+'"></script>';
            break;

        case "/dynamicTemplateCreate": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/dynamicTemplateCreateController.js?v='+VERSION+'"></script>';
            break;

        case "/eTrack": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/eTrackController.js?v='+VERSION+'"></script>';
            break; 

        case "/tracking": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/trackingController.js?v='+VERSION+'"></script>';
            break; 

        case "/edituser": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/editUserController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/emailConfig": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/emailController.js?v='+VERSION+'"></script>';
            break;

        case "/fileUploadClient": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/fileUploadClientController.js?v='+VERSION+'"></script>';
            break;

        case "/greeting": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/greetingController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jasny-bootstrap.min.js?v='+VERSION+'"></script>';
            break;

        case "/home": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/configuredDashboardController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/amcharts.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/serial.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/jsapi"></script>'+
                                '<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBXU_n338VgJvuwQI0BathW7QQqf5uIrG8" async="" defer="defer"></script>'+
                                // '<script type="text/javascript" src="assets/admin1/js/loader.js"></script>'+
                                '<script src="assets/globals/plugins/fullcalendar/dist/fullcalendar.min.js?v='+VERSION+'"></script>';
            break;

        case "/index": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks = '<script src="assets/globals/js/global-vendors.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/user-pages.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/js/pleasure.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/layout.js?v='+VERSION+'"></script>'+
                                '<script src="controller/lib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/loginController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/globalvar.js?v='+VERSION+'"></script>'+
                                '<script src="controller/commonController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/md5.min.js?v='+VERSION+'"></script>';
            break;

        case "/": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks = '<script src="assets/globals/js/global-vendors.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/user-pages.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/js/pleasure.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/layout.js?v='+VERSION+'"></script>'+
                                '<script src="controller/lib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/loginController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/globalvar.js?v='+VERSION+'"></script>'+
                                '<script src="controller/commonController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/md5.min.js?v='+VERSION+'"></script>';
            break;
       
        case "/inqReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/inqReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;
       
        case "/inqWonReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/inqWonReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/inquiry": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/inquiryListController.js?v='+VERSION+'"></script>';
            break;

        case "/inquiryAllocationReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/inquiryAllocationReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/inquiryCreate": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/inquiryCreateController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>';
            break;

        case "/loginHistory": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/loginHistoryController.js?v='+VERSION+'"></script>';
            break;

        case "/masterdevloper": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userManageModuleController.js?v='+VERSION+'"></script>';
            break;
            
        case "/paymentReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/paymentReportController.js?v='+VERSION+'"></script>';
            break;

        case "/profile": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userProfileController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION+'"></script>';
            break;

        case "/proposalReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/proposalReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;
            
        case "/proposaledit": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/proposalEditController.js?v='+VERSION+'"></script>';
            break;

        case "/quickInquiry": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/quickInquiryController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            break;

        case "/report": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '';
            break;

        case "/inquiryReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/inquiryListReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            break;

        case "/inquirydetail": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/globals/plugins/d3/d3.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/c3js-chart/c3.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/charts-c3.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jquery.fancybox.min.js?v='+VERSION+'"></script>'+
                                '<script src="controller/inquiryDetailController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/proposalController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>';
            break;

        case "/salesTargetReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/salesTargetReportController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/pivot.js?v='+VERSION+'" type="text/javascript"></script>';
            break;

        case "/salesbom": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/salesBomController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/switchery/dist/switchery.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jasny-bootstrap.min.js?v='+VERSION+'" type="text/javascript"></script>';
            break;

        case "/scheduleActivity": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityAppScheduleActivityController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityApplib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppActListingController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppCommonController.js?v='+VERSION+'"></script>';
            break;

        case "/smartActivity": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/globals/plugins/select2/select2.full.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/jquery.validate.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/jquery.bootstrap.wizard.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/forms-wizard.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityApplib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppCommonController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/smartActivityController.js?v='+VERSION+'"></script>'+  
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>';
            break;

        case "/settings": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/settingsController.js?v='+VERSION+'"></script>';
            break;

        case "/smartSchedulerActivity": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/activityApplib.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppActListingController.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppCommonController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/jquery.progresstimer.js?v='+VERSION+'"></script>'+
                                '<script src="controller/activityAppSmartSchedulerController.js?v='+VERSION+'"></script>';
            break;

        case "/smsConfig": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/smsController.js?v='+VERSION+'"></script>';
            break;

        case "/studentEnrollment": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=   '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/studentEnrollController.js?v='+VERSION+'"></script>';
            break;

        case "/statusCategory": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/statusCategoryController.js?v='+VERSION+'"></script>';
            break;

        case "/target": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/targetController.js?v='+VERSION+'"></script>';
            break;

        case "/targetDist": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/clientTargetDistController.js?v='+VERSION+'"></script>';
            break;

        case "/uomManagement": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/uomManagementController.js?v='+VERSION+'"></script>';
            break;

        case "/usergroup": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userGroupController.js?v='+VERSION+'"></script>';
            break;

        case "/userlevel": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userlevelController.js?v='+VERSION+'"></script>';
            break;

        case "/usermanagment": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '';
            break;

        case "/usermanagpermission": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userManagePermissionController.js?v='+VERSION+'"></script>';
            break;

        case "/usermanagrole": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userManageRoleController.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/jquery.multi-select.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/jquery.quicksearch.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="indeCampusJs/addbutton-scripts.js?v='+VERSION+'"></script>';
            break;

        case "/usermanaguser": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userManageController.js?v='+VERSION+'"></script>';
            break;

        case "/usermanagmodule": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/userManageModuleController.js?v='+VERSION+'"></script>';
            break;

        case "/versionInfo": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/versionInfoController.js?v='+VERSION+'"></script>';
            break;

        case "/why": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/whyController.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/moment.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/daterangepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/pivot.js?v='+VERSION+'" type="text/javascript"></script>';
            break;

        case "/workflow": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/workflowController.js?v='+VERSION+'"></script>';
            break;

        case "/createEnrollment": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/createEnrollmentController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            
            break;

        case "/studentAllotment": // IT WILL APPEND ALL THE JS DYNAMICALLY 
            dynamicJsLinks += '<script src="controller/studentAllotmentController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
                               
            
            break;

        case "/createEnrollmentInvoice": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/createEnrollmentController.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
                                // '<script type="text/javascript" src="assets/intl/js/intlTelInput.js?v='+VERSION+'"></script>';
            
            break;

        case "/renewEnrollment": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/renewEnrollmentController.js?v='+VERSION+'"></script>';
            
            break;

        case "/itemMasterList": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            
            break;
            
        case "/itemMasterListDetail": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>';
            
            break;
        
        case "/chargePosting": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/chargePostingController.js?v='+VERSION+'"></script>';
            
            break;

        case "/chargePostListing": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/chargePostListingController.js?v='+VERSION+'"></script>';
            
            break;
            
            

        case "/eventManagementDetail": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>'+
                                '<script src="controller/eventMgmtDetailController.js?v='+VERSION+'"></script>';            
            
            break;

        case "/eventManagementList": // IT WILL APPEND ALL THE JS DYNAMICALLY 
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/eventMgmtListController.js?v='+VERSION+'"></script>';            
            
            break;

        case "/collectpayment": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/collectPaymentController.js?v='+VERSION+'"></script>';                               
            
            break;
         case "/studentEnrollmentDetail": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="assets/globals/plugins/pnikolov-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/minicolors/jquery.minicolors.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/moment.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/daterangepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/plugins/clockface/js/clockface.js?v='+VERSION+'"></script>'+
                                '<script src="assets/globals/scripts/forms-pickers.js?v='+VERSION+'"></script>'+
                                '<script src="controller/studentEnrollDetailController.js?v='+VERSION+'"></script>';            
            
            break;

        case "/roommaster": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '';
            
            break;

        case "/centralisedinvoice": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="controller/centralizedInvoiceController.js?v='+VERSION+'"></script>'; 
            break;
            
        case "/NonProcessCharge": // IT WILL APPEND ALL THE JS DYNAMICALLY 
            dynamicJsLinks +=  '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/nonProcessChargesController.js?v='+VERSION+'"></script>'; 
//                          '<script src="assets/globals/scripts/forms-pickers.js?v='+VERSION+'"></script>';
           
            break;
    
        case "/generalInvoice": // IT WILL APPEND ALL THE JS DYNAMICALLY 
            dynamicJsLinks +=  '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/generalInvoiceController.js?v='+VERSION+'"></script>'; 
//                          '<script src="assets/globals/scripts/forms-pickers.js?v='+VERSION+'"></script>';
           
            break;

        case "/viewCharges": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=  '<script src="controller/viewChargesController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/eventManagementView": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=  '<script src="controller/eventMgmtViewController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/RequestComplainCreate": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=  '<script src="controller/createRequestComplaintController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/RequestComplain": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=  '<script src="indeCampusJs/bootstrap-editable.min.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/dynamic_table_init.js?v='+VERSION+'"></script>'+
                                '<script src="controller/requestComplaintListController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/departmentCategoryMaster": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=  '<script src="controller/departmentCategoryMasterController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/RequestComplainView": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=  '<script src="controller/requestComplaintViewController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/suspensionTermination": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks += '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                             '<script src="controller/suspensionTerminationController.js?v='+VERSION+'"></script>'; 
           
            break; 

        case "/suspensionTerminationCreate": // IT WILL APPEND ALL THE JS DYNAMICALLY
            dynamicJsLinks +=   '<script src="assets/admin1/js/moment.min.js?v='+VERSION+'"></script>'+
                                '<script src="assets/admin1/js/daterangepicker.js?v='+VERSION+'"></script>'+
                                '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
                                '<script src="controller/createSuspensionController.js?v='+VERSION+'"></script>'; 
           
            break;

        case "/enrollmentReport": // IT WILL APPEND ALL THE JS DYNAMICALLY
                   dynamicJsLinks +=   '<script src="controller/enrollmentReportController.js?v='+VERSION+'"></script>'; 
            break;

	    case "/revenueReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                  dynamicJsLinks +=   '<script src="controller/revenueReportController.js?v='+VERSION+'"></script>'; 
            break;

	    case "/requestReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                  dynamicJsLinks +=   '<script src="controller/requestReportController.js?v='+VERSION+'"></script>'; 
            break;

	    case "/complaintReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                 dynamicJsLinks +=   '<script src="controller/complaintReportController.js?v='+VERSION+'"></script>'; 
            break;

	    case "/suspensionReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                dynamicJsLinks +=   '<script src="controller/suspensionReportController.js?v='+VERSION+'"></script>'; 
            break;

	    case "/terminationReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                dynamicJsLinks +=   '<script src="controller/terminationReportController.js?v='+VERSION+'"></script>'; 
            break;

	    case "/rentalSpaceInquiryReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                dynamicJsLinks +=   '<script src="indeCampusJs/bootstrap-datepicker.js?v='+VERSION+'"></script>'+
			            '<script src="controller/rentalSpaceInquiryReportController.js?v='+VERSION+'"></script>'; 
            break;
            
	    case "/eventManagementReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                dynamicJsLinks +=   '<script src="controller/eventManagementReportController.js?v='+VERSION+'"></script>'; 
            break;

        case "/pageContent": 
                dynamicJsLinks +=   '<script src="controller/pageContentController.js?v='+VERSION+'"></script>'; 
            break;

        case "/nonAvailingService":
                dynamicJsLinks +=   '<script src="controller/nonAvailingListController.js?v='+VERSION+'"></script>';
            break; 

        case "/addNonAvailService":
                dynamicJsLinks +=  '<script src="controller/addNonAvailingController.js?v='+VERSION+'"></script>'; 
            break;

        case "/createreceipt" : // ADDED BY KAUSHA SHAH ON 16-08-2018 
            dynamicJsLinks += '<script src="controller/createReceiptController.js?v='+VERSION+'"></script>' ;
            break;
            
        case "/createinvoice" : // ADDED BY KAUSHA SHAH ON 16-08-2018 
            dynamicJsLinks += '<script src="controller/createInvoiceController.js?v='+VERSION+'"></script>' ;
            break;

        case "/nonAvailingView" :
            dynamicJsLinks += '<script src="controller/nonAvailingViewController.js?v='+VERSION+'"></script>' ;
            break;

        case "/timezoneMaster" :
            dynamicJsLinks += '<script src="controller/timezoneMasterController.js?v='+VERSION+'"></script>' ;
            break;
            
        case "/chargePostingMaster" : // ADDED BY KAUSHA SHAH ON 27-08-2018
            dynamicJsLinks += '<script src="controller/centralizedInvoiceController.js?v='+VERSION+'"></script>' ;
            break;

        case "/roomOccupancyReport" : 
            dynamicJsLinks += '<script src="controller/roomOccupancyReportController.js?v='+VERSION+'"></script>' ;
            break;

        case "/requestComplaintSummary" : 
            dynamicJsLinks += '<script src="controller/requestComplaintSummaryController.js?v='+VERSION+'"></script>' +
                                '<script src="indeCampusJs/pivot.js?v='+VERSION+'" type="text/javascript"></script>';
            break;
    }

document.write( dynamicJsLinks ); // ASSIGN ALL THE LINKS TO ONE PAGE
 