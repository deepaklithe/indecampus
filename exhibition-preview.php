<?php  
    
    require_once "potgProject/include/globals.php"; // GLOBAL VAR FILE
    
    $data=base64_decode($_REQUEST["data"]);
    $dataAry=  explode('&', $data);
    //echo "<pre>";print_r($attach); exit;
    
    $inqId = trim(base64_decode($dataAry[1]));
    $proId = trim(base64_decode($dataAry[2]));
    $exhibitionId = '14';//trim(base64_decode($dataAry[3]));
    $custID = trim(base64_decode($dataAry[4]));
    $clientId = trim(base64_decode($dataAry[5]));
    $reciverName = trim(base64_decode($dataAry[6]));
    $proposalDate = trim(base64_decode($dataAry[7]));
    $requestType = trim($dataAry[8]);
    //print_r($attach); exit;
    
    // GET PROPOSAL CHAT DETAIL
    $postData=array();
    $postData['postData']['cusId']=$custID;
    $postData['postData']['inqId']=$inqId;
    $postData['postData']['proId']=$proId;
    $postData['postData']['requestCase']="proposalChatListing";
    $postData['postData']['clientId']=$clientId;
    $postData['postData']['curlFlag']=1;
    
    
    //GET STALL DETAIL
    if($exhibitionId!='' && $exhibitionId > 0) {
        $sqlGetHallListing = "SELECT HM.PK_HALL_ID, HM.HALL_NAME, HM.NO_ROW, HM.NO_COLUMN FROM ".HALLMASTER." HM WHERE FK_EXHIBITION_ID = '".$exhibitionId."' AND HM.STATUS = 0 AND HM.DELETE_FLAG = 0 ";
        $resultGetHallListing = fetch_rec_query($sqlGetHallListing);
        if(count($resultGetHallListing) > 0) {
            $finalJson = array();
            foreach($resultGetHallListing as $keyHall => $valueHall){
                $finalJson[$keyHall]['hallId'] = $valueHall['PK_HALL_ID'];
                $finalJson[$keyHall]['hallName'] = $valueHall['HALL_NAME'];
                $stallNoRow = $valueHall['NO_ROW'];
                $stallNoColumn = $valueHall['NO_COLUMN'];
                
                $sqlGetStallListing = "SELECT SM.PK_STALL_ID,SM.STALL_NAME,SM.STALL_X,SM.STALL_Y FROM ".STALLMASTER." SM WHERE SM.FK_CLIENT_ID = '6' AND SM.FK_EXHIBITION_ID = '".$exhibitionId."' AND SM.FK_HALL_ID = '".$valueHall['PK_HALL_ID']."' AND SM.DELETE_FLAG = 0 ";
                $resultGetStallListing = fetch_rec_query($sqlGetStallListing);
                if(count($resultGetStallListing) > 0) {
                    $tempArr1 = array();
                    for($i=0; $i<count($resultGetStallListing); $i++) {
                        $tempArr = array();
                        $tempArr['stallId'] = $resultGetStallListing[$i]['PK_STALL_ID'];
                        $tempArr['stallName'] = $resultGetStallListing[$i]['STALL_NAME'];
                        $tempArr['stallX'] = $resultGetStallListing[$i]['STALL_X'];
                        $tempArr['stallY'] = $resultGetStallListing[$i]['STALL_Y'];
                        $tempArr['stallNoRow'] = $stallNoRow;
                        $tempArr['stallNoColumn'] = $stallNoColumn;
                        
                        $tempArr1[] = $tempArr;
                    }
                    $tempArr1 = json_encode($tempArr1);
                    //echo '<pre>';
                    //print_r($tempArr1);
                    // echo '<script type="text/javascript">',
                    //'var stallData ='. $tempArr .';</script>';
                }
            }
        }
        //print_r($finalJson);
    }
    
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => PROPOSALCHATPATH,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => http_build_query($postData),
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: 9d702c87-3566-04da-9b08-0b5416044a35"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } 
    $resAry=json_decode($response);
    $array =  (array) $resAry;

    // GET PROPOSAL DETAIL
    $postDataDetail=array();
    $postDataDetail['postData']['inqId']=$inqId;
    $postDataDetail['postData']['proId']=$proId;
    $postDataDetail['postData']['requestCase']="getProposalDetailForDocx";
    $postDataDetail['postData']['clientId']=$clientId;
    //print_r($postDataDetail); exit;
    
    $curlDoc = curl_init();
    curl_setopt_array($curlDoc, array(
      CURLOPT_URL => PROPOSALCHATPATH,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => http_build_query($postDataDetail),
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: 9d702c87-3566-04da-9b08-0b5416044a35"
      ),
    ));

    $responseDocx = curl_exec($curlDoc);
    //echo "<pre>";
    //print_r($responseDocx); exit;
    $errDocx = curl_error($curlDoc);
    curl_close($curlDoc);

    if ($errDocx) {
      echo "cURL Error #:" . $errDocx;
    } 
    $resDocxAry=json_decode($responseDocx);
    $arrayDoc =  (array) $resDocxAry;
    //print_r($arrayDoc); exit;
    $finalSectionDetail = "";
    if(!empty($arrayDoc['status'])){
        foreach($arrayDoc['data'] as $valueOfSection){
            $finalSectionDetail .= trim(strip_tags($valueOfSection->sectionDetail,"<table><p><tr><th><br><ul><li><td>"));
        } 
        //$finalSectionDetail = preg_replace('/\s\s+/', ' ', $finalSectionDetail);
        $finalSectionDetail = preg_replace('/>\s+</', '><', $finalSectionDetail);
        $finalSectionDetail = preg_replace('/\s\s+/', ' ', $finalSectionDetail);
        $finalSectionDetail = str_replace('&nbsp;', '', $finalSectionDetail);
        $finalSectionDetail = addslashes($finalSectionDetail);
        //$finalSectionDetail = htmlentities($finalSectionDetail,ENT_QUOTES,"UTF-8");
        //print_r($finalSectionDetail); exit;
    }
    
    
    
    
    if($_POST['reply']){
        
        $addData=array();
       
        $addData['postData']['inqId']=$inqId;
        $addData['postData']['proId']=$proId;
        $addData['postData']['follow_remark']=$_POST['message'];
        $addData['postData']['requestType']=isset($requestType) ? $requestType : "0";
        $addData['postData']['requestId']=$custID;
        $addData['postData']['requestCase']="proposalChatAdd";
        
                $curl = curl_init();
                curl_setopt_array($curl, array(
                  CURLOPT_URL => PROPOSALCHATPATH,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_POST => true,
                  CURLOPT_POSTFIELDS => http_build_query($addData),
                  CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: 9d702c87-3566-04da-9b08-0b5416044a35"
                  ),
                ));

                curl_exec($curl);
                curl_error($curl);
                curl_close($curl);
        header("location:proposal-preview.php?data=".$_REQUEST["data"]);
    }
    
    //print_r($tempArr);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Proposal - PDF</title>

    <style>
        body {
            margin: 0;
            padding: 0;
            max-height: 768px;
            overflow: hidden;
            font-family: 'RobotoDraft', sans-serif;
            text-rendering: optimizeLegibility;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-feature-settings: "liga", "kern";
            -moz-font-feature-settings: "liga", "kern";
            font-feature-settings: "liga", "kern";
            -webkit-font-kerning: normal;
            -moz-font-kerning: normal;
            font-kerning: normal;
        }
        
        .header-bar {
            background: #008ab1;
            top: 0;
            height: 60px;
            left: 0;
            display: flex;
        }
        
        .logo {
            position: relative;
            top: 0px;
            left: 0px;
            background: #fff;
        }
        
        .logo img {
            max-width: 45px;
        }
        
        ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        
        .custname {
            width: 95%;
            float: right;
            color: #fff !important;
        }
        
        .custname li {
            float: left;
        }
        
        .custname li.name {
            width: 90%;
            text-align: center;
            float: left;
        }
        
        .custname li.pr {
            width: 10%;
            text-align: center;
            float: left;
        }
        
        .prNo,
        .prDate {
            vertical-align: middle;
            width: 100%;
            float: left;
            text-align: right;
            font-size: 18px;
        }
        
        .prNo {
            margin-top: 10px;
        }
        
        .wrapper {
            margin-top: 10px;
            padding: 10px 20px;
            max-height: 700px;
            overflow: hidden;
        }
        
        .col-6,
        .col-4 {
            box-sizing: border-box;
            box-shadow: 0 0 10px #ddd;
            max-height: 600px;
            overflow: auto;
            padding: 10px;
            float: left;
        }
        
        .col-6 {
            width: 58%;
            margin-right: 15px;
            overflow: hidden;
        }
        
        .col-4 {
            width: 40%;
        }
        
        .pdfobject-container {
            height: 600px;
            width: 100%;
        }
        /*        timeline*/
        
        .pdfobject {
            height: 600px !important;
        }
        
        .timeline1 {
            padding: 0;
            margin: 0;
        }
        
        .tl-item {
            display: block;
        }
        
        .tl-item:before,
        .tl-item:after {
            display: table;
            content: " ";
        }
        
        .tl-item:after {
            clear: both;
        }
        
        .visible-left {
            display: none;
        }
        
        .tl-wrap {
            display: block;
            padding: 15px 0 15px 20px;
            margin-left: 6em;
            border-color: #dee5e7;
            border-style: solid;
            border-width: 0 0 0 4px;
        }
        
        .tl-wrap:before,
        .tl-wrap:after {
            display: table;
            content: " ";
        }
        
        .tl-wrap:after {
            clear: both;
        }
        
        .tl-wrap:before {
            position: relative;
            top: 15px;
            float: left;
            width: 10px;
            height: 10px;
            margin-left: -30px;
            background: #edf1f2;
            border-color: inherit;
            border-style: solid;
            border-width: 3px;
            border-radius: 50%;
            content: "";
            box-shadow: 0 0 0 4px #f0f3f4;
        }
        
        .tl-wrap:hover:before {
            background: transparent;
            border-color: #fff;
        }
        
        .tl-date {
            position: relative;
            top: 10px;
            display: block;
            float: left;
            width: 5.5em;
            margin-left: -9em;
            text-align: right;
            font-size: 14px;
        }
        
        .tl-content {
            position: relative;
            display: inline-block;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        
        .tl-content.block {
            display: block;
            width: 100%;
        }
        
        .tl-content.panel-1 {
            margin-bottom: 0;
            padding: 10px 15px;
            background: #f0f0f0;
        }
        
        .tl-header {
            display: block;
            width: 12em;
            margin-left: 2px;
            text-align: center;
        }
        
        .timeline-center .tl-item {
            margin-left: 50%;
        }
        
        .timeline-center .tl-item .tl-wrap {
            margin-left: -2px;
        }
        
        .timeline-center .tl-header {
            width: auto;
            margin: 0;
        }
        
        .timeline-center .tl-left {
            margin-right: 50%;
            margin-left: 0;
        }
        
        .timeline-center .tl-left .hidden-left {
            display: none !important;
        }
        
        .timeline-center .tl-left .visible-left {
            display: inherit;
        }
        
        .timeline-center .tl-left .tl-wrap {
            float: right;
            padding-right: 20px;
            padding-left: 0;
            margin-right: -2px;
            border-right-width: 4px;
            border-left-width: 0;
        }
        
        .timeline-center .tl-left .tl-wrap:before {
            float: right;
            margin-right: -27px;
            margin-left: 0;
        }
        
        .timeline-center .tl-left .tl-date {
            float: right;
            margin-right: -8.5em;
            margin-left: 0;
            text-align: left;
        }
        
        .arrow {
            z-index: 10;
            border-width: 9px;
        }
        
        .arrow,
        .arrow:after {
            position: absolute;
            display: block;
            width: 0;
            height: 0;
            border-color: transparent;
            border-style: solid;
        }
        
        .arrow:after {
            border-width: 8px;
            content: "";
        }
        
        .arrow.left {
            top: 50%;
            left: -9px;
            margin-top: -9px;
            border-right-color: rgba(0, 0, 0, 0.1);
            border-left-width: 0;
        }
        
        .arrow.left:after {
            bottom: -8px;
            left: 1px;
            border-right-color: #f0f0f0;
            border-left-width: 0;
        }
        
        .arrow.pull-left {
            left: 19px;
        }
        
        .arrow.pull-right {
            right: 19px;
            left: auto;
        }
        
        .arrow.pull-up {
            top: 19px;
        }
        
        .arrow.pull-down {
            top: auto;
            bottom: 19px;
        }
        
        .text-muted {
            color: #000000;
        }
        
        .text-grey {
            color: #ffffff;
        }
        
        .pull-right {
            float: right;
        }
        
        .tl-wrap.you:before {
            box-shadow: 0 0 0 4px #fdb928;
            background-color: #fdb928;
        }
        
        .tl-wrap.emp:before {
            box-shadow: 0 0 0 4px #008ab1;
            background-color: #008ab1;
        }
        
        #chat .tl-date {
            width: 5.5em;
            margin-left: -8.0em;
        }
        
        .emp .tl-content.panel-1 {
            background-color: #008AB1;
            color: #fff;
        }
        
        .emp .arrow.left {
            border-right-color: #008AB1;
        }
        
        .emp .arrow.left:after {
            border-right-color: #008AB1;
        }
        
        .you .tl-content.panel-1 {
            background-color: #fdb928;
            color: #466B75;
        }
        
        .you .arrow.left {
            border-right-color: #fdb928;
        }
        
        .you .arrow.left:after {
            border-right-color: #fdb928;
        }
        
        .commentBox {
            width: 100%;
            height: 100%;
            margin-bottom: 15px;
        }
        
        .commentBox lable {
            width: 100%;
            float: left;
        }
        
        .commentBox input[type="text"] {
            float: left;
            width: 65%;
            margin-right: 10px;
            height: 30px;
            border: none;
            border-bottom: 1px solid #ddd;
        }
        
        .commentBox input[type="text"]:focus {
            border-bottom: 2px solid #66afe9;
            outline: none
        }
        
       .replybutton {
            background: #8ac249;
            color: #fff;
            padding: 5px 10px;
            font-size: 18px;
            text-decoration: none;
            border-radius: 2px;
            text-align: center;
            margin-top: 10px;
            border:none;
        }
        /*        scrollbar*/
        
        ::-webkit-scrollbar {
            width: 6px;
        }
        
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.3);
        }
        
        ::-webkit-scrollbar-thumb {
            border-radius: 3px;
            -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.5);
        }

    </style>
</head>

<body>
    <nav class="header-bar">
        <a href="#" class="logo">
            <img src="assets/admin1/img/logo-web.png" alt="">
        </a>
        <ul class="custname">
            <li class="name">
                <h2><?=ucfirst($reciverName)?></h2></li>
            <li class="pr">
                <span class="prNo">#<?=$proId?></span>
                <span class="prDate"><?=date("d-m-Y",$proposalDate)?></span>
            </li>
        </ul>
    </nav>
    <div class="wrapper">
        <div class="col-6">
            <div id="example1"><canvas id="myCanvas" style="margin:0 auto;"></canvas></div>
        </div>
        <div class="col-4">
            <ul class="timeline1">
                <!--
                                <li class="tl-header">
                                    <div class="btn btn-info">Now</div>
                                </li>
-->
                <div class="commentBox">
                    
                    <?php if($requestType == 0 && !empty($finalSectionDetail)){ // ONLY VISIABLE TO USER ?>
                        <div style="width: 80%; float: left"><lable>Comment</lable></div>
                        <div style="width: 20%; float: right;">
                            <a href="javascript:void(0)" onclick="downloadDocxJS();"><i><img src="img/rep_word_docx_files_001.png" height="36px" width="36px" style="float:right;cursor: pointer" title="Proposal Docx"></i></a>
                        </div>
                    <?php }else{ ?>
                        <div style="width: 100%; float: left"><lable>Comment</lable></div>
                    <?php  } ?>
                    <form method="post" id="chatForm" onsubmit="return validation()" name="chatForm">
                        <input type="text" class="form-control" name="message" id="message" autocomplete="off" placeholder="Enter Message">
                        <input type="submit" name="reply" class="replybutton" value="Reply">
                        <input type="button" name="refresh" value="Refresh" onclick="reFresh()" class="replybutton">
                    </form>
                    
                </div>
                <?php if($array['status']=="No record found") { 
                    echo "No Comment Found";
                } else { 
                    foreach($array['data'] as $array1) {  
                    ?>
                    <?php if($array1->userType==1) { ?>
                    <li class="tl-item">
                        <div class="tl-wrap emp">
                            <span class="tl-date">
                                <?=date("d-m-Y",strtotime($array1->date))?>
                                <?=date("h:i:s A",$array1->createdOn)?>
                            </span>
                            <div class="tl-content panel-1 padder b-a">
                                <span class="arrow left pull-up"></span>
                                <div><?=$array1->message?></div>
                                <div class="text-muted pull-right">-
                                    <?php 
                                        if($array1->id == $custID){
                                            echo "You";
                                        }else{
                                            echo $array1->name;
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php } else { ?>
                   <li class="tl-item">
                    <div class="tl-wrap you">
                        <span class="tl-date">
                            <?=date("d-m-Y",strtotime($array1->date))?>
                            <?=date("h:i:s A",$array1->createdOn)?>
                        </span>
                        <div class="tl-content panel-1 padder b-a">
                            <span class="arrow left pull-up"></span>
                            <div><?=$array1->message?></div>
                            <div class="text-grey pull-right">-
                                <?php 
                                    if($array1->id == $custID){
                                        echo "You";
                                    }else{
                                        echo $array1->name;
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                   </li>
                   
                    <?php }  } } ?>
                <!--
                                <li class="tl-header">
                                    <div class="btn btn-sm btn-primary btn-rounded">more</div>
                                </li>
-->
            </ul>
        </div>
    </div>
    <!--    script files -->
    <script src="assets/globals/js/global-vendors.js"></script>
    <script src="assets/globals/js/pleasure.js"></script>
    <script>
        $(document).ready(function() {
            Pleasure.init();
        });

    </script>
    <script>
        Array.min = function( array ){
            return Math.min.apply( Math, array );
        };

        Array.max = function( array ){
            return Math.max.apply( Math, array );
        };
        
        function renderStalls(){

        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');

        // var img = document.getElementById("theImg");
        var cnvs = document.getElementById("myCanvas");
        var blockSize = 75;
        var stallData = <?php echo ($tempArr1); ?>;
console.log(stallData);
        var canvasObj = document.getElementsByTagName('canvas')[0];
        canvasObj.width  = stallData[0].stallNoRow * blockSize;
        canvasObj.height = stallData[0].stallNoColumn * blockSize;
        //canvasObj.width  = 10 * blockSize;
        //canvasObj.height = 10 * blockSize;

        ctx.beginPath();
        ctx.fillStyle = "white";
        ctx.lineWidth = 2;
        ctx.strokeStyle = 'grey';
        for (var row = 0; row < 10; row++) {
            for (var column = 0; column < stallData[0].stallNoColumn; column++) {
                var x = column * blockSize;
                var y = row * blockSize;
                ctx.rect(x, y, blockSize, blockSize);
                ctx.fill();
                ctx.stroke();
            }
        }
        ctx.closePath();

        var ctx = cnvs.getContext("2d");

        for( var j=0; j<stallData.length; j++ ){

            var Title = stallData[j].stallName;
            var pointX = stallData[j].stallX.split(',');
            var pointY = stallData[j].stallY.split(',');

            var pointsXMin = (Array.min(pointX) - 108);
            var pointsXMax = (Array.max(pointX) - 108);
            var pointsYMin = (Array.min(pointY) - 250);
            var pointsYMax = (Array.max(pointY) - 250);

            // var avgPointX = ( pointsXMin + pointsXMax ) / 2;
            // var avgPointY = ( pointsYMin + pointsYMax ) / 2;

            var avgPointX = ( pointsXMin + 10 ) ;
            var avgPointY = ( pointsYMin + 10 );

            ctx.beginPath();

            for( var i=0; i<pointX.length; i++ ){
                var xPoint = (pointX[i] - 108);
                var yPoint = (pointY[i] - 250);
                ctx.lineTo(Math.round(xPoint/blockSize)*blockSize,Math.round(yPoint/blockSize)*blockSize);
                // console.log((pointX[i] - 108) +' '+(pointY[i] - 100));
            }
            var xPoint = (pointX[0] - 108);
            var yPoint = (pointY[0] - 250);
            ctx.lineTo(Math.round(xPoint/blockSize)*blockSize,Math.round(yPoint/blockSize)*blockSize);

            ctx.lineWidth = 2;
            ctx.fillStyle = "grey";
            ctx.fill();
            ctx.closePath();
            ctx.strokeStyle = 'black';
            ctx.stroke();
            ctx.fillStyle = "#00ff00";
            ctx.font="15px Arial";
            ctx.fillText(Title,avgPointX,avgPointY);

        }
    }
    renderStalls();
    </script>
    
    

</body>

</html>