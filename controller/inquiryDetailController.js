/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 1-06-2016.
 * File : inquiryDetailController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var custId = "-2";
var itemJson = [];
var bomJson = [];
var itemGroupJson = [];
var inqMasterData = [];
var inqTransData = [];
var getData = [];
var editContext = '';
var finalEditMoreInfoData = [];
var finalStatusArr = [];
var cancelRemarkData = [];
var newPotAmt = 0;
var newCartData = [];
var cartTotalAmt = 0;
var potFlag = false;
var extraTransData = [];
var outStandingTotal = "";
var GBLRATETYPEID = "-1";
var GBL_NOTE_LIST = [];
var GBL_CHECKLIST_ARR = [];
var GBL_MILESTONE_ARR = [];
var GBL_INQ_LOG_ARR = [];

var GBL_ACT_LOADED = "0";
var GBL_ATTACH_LOADED = "0";
var GBL_ALLOC_LOADED = "0";
var GBL_CANCEL_FLAG = "0";

var GBL_CANCEL_INQ_STATID = "";
var GBL_CURR_STAT_ID = "";

var GBL_PAY_TYPE = "";
var GBL_PAY_CUSTID = "";
var GBL_PAY_OUTAMT = "";
var GBL_PAY_INQID = "";
var GBL_PAY_MILEID = "";
var GBL_PAY_FLATID = "";
var GBL_PAY_MILEINQID = "";

var finalStatWon = false;

var GBL_MILESTONE_AMT = 0;
var GBL_MS_DUE_PER = 0;

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";        
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

function getinquiryDetailListing(){
    
    inqTransData = [];
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
    }

    commonAjax(COMMONURL, postData, inquiryDetailCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryDetailCallback(flag,data){

    if (data.status == "Success" && flag) {
    
         console.log(data);

        itemJson = [];
        getData = data.data;
        GBL_NOTE_LIST = getData[0].note;

        if( getData[0].snapShotLink != "" ){
            $('#visitCard').show();
        }

        GBL_CURR_STAT_ID = getData[0].statusId;
        var tmp_date1 = new Date(data.data[0].inqDate);

        var month1 = months[ tmp_date1.getMonth() ];
        var date1 = tmp_date1.getDate();
        var year1 = tmp_date1.getFullYear();
        
        localStorage.POTGcustId = data.data[0].custId;
        
        $('#custName').html( '<span class="curPoint" style="color:#008ab1;" custid="'+ getData[0].custId +'" onclick="navigateToCustomerDetailPage(this)">'+ data.data[0].custDetail.fName + ' ' + data.data[0].custDetail.lName +'</span>' );
        $('#inqDate').html( month1 + ' ' + date1 + ', ' + year1 );
        $('#assignedTo').html( data.data[0].inqAssignToName );
        $('#inqSource').html( data.data[0].inqSourceName );
        $('#inqType').html( data.data[0].inqTypeName );
        $('#contactPerson').html( data.data[0].custDetail.cpName );
        $('#contactPersonNumber').html( data.data[0].custDetail.cpMobile );
        $('#inqId').html( '#'+data.data[0].clientLevelInqId );
        $('#contNum').html( data.data[0].custDetail.custMobile );
        $('#email').html( data.data[0].custDetail.custEmail );
        $('#add1').html( data.data[0].custDetail.custAddress );
        $('#custGstinNum').html( data.data[0].custDetail.gstTinNo );
        
        $('#contactPersonDesig').html( data.data[0].custDetail.contPerDesig );
        $('#contactPersonDOB').html( (data.data[0].custDetail.contPerBirthDate != "" ? mysqltoDesiredFormat( data.data[0].custDetail.contPerBirthDate , 'dd-MM-yyyy' ) : "") );
        $('#contactPersonDOA').html( (data.data[0].custDetail.contPerAnniDate ? mysqltoDesiredFormat( data.data[0].custDetail.contPerAnniDate , 'dd-MM-yyyy' ) : "") );

        ( (data.data[0].custDetail.contPerDesig != "") ? $('#cpSpanDesig').show() : "" );
        ( (data.data[0].custDetail.contPerBirthDate != "") ? $('#cpSpanDOB').show() : "" );
        ( (data.data[0].custDetail.contPerAnniDate != "") ? $('#cpSpanDOA').show() : "" );

        GBLRATETYPEID = data.data[0].custDetail.rateTypeId;
        // var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        // var index = findIndexByKeyValue( roleAuth.data.closureDays , 'ID' , data.data[0].closerBy )

        // if( index != "-1" ){
        $('#closerBy').html( mysqltoDesiredFormat(data.data[0].closerBy,'dd-MM-yyyy') );
        // }
        $('#potAmt').html( numberFormat( parseFloat( data.data[0].potAmt ) ) );
        $('#proPerc').html( data.data[0].probablePercompleted.toFixed(2) + '%' );
        $('#progressbardis').css( 'width' , data.data[0].probablePercompleted.toFixed(2) + '%' );
        $('#progressbardis .sr-onlyInq').html( data.data[0].probablePercompleted.toFixed(2)+'%' );


        var renderScoreCalc = "<table>";
        if( data.data[0].potgScore != "" && data.data[0].potgScore != "No record found" ){
            data.data[0].potgScore.data.forEach( function( record , index ){
                renderScoreCalc += '<tr><td>'+record.lookupTypeName +'</td><td>'+ record.lookupTypeWaitage + ' |  </td><td>'+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].displayValue + ' - ' : "" ) +' '+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].lookWaitage +"" : " " ) +'</td></tr>'; 
            });
        }
        renderScoreCalc += "</table>";

        $('#potgScoreCont').attr( 'data-content' , renderScoreCalc );

        //GAUGE CHART ADDED FOR POT AMOUNT SHOWS START HERE
        var chart = c3.generate({
            bindto: '.c3-gauge',
            data: {
                columns: [
                    ['POTG Score (Out of 100)', parseFloat(data.data[0].potgScore.score)]
                    // ['POTG Score (Out of 100)'+ renderScoreCalc +'', parseFloat(data.data[0].potgScore.score)]
                    // ['Probable Percentage', parseFloat(data.data[0].probablePercompleted)]
                ],
                type: 'gauge',
                // onclick: function (d, i) { console.log("onclick", d, i); },
                // onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                // onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            color: {
                pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                threshold: {
                    values: [30, 60, 90, 100]
                }
            },
            size: {
                height:150,
                width:230,
            }

        });
        //GAUGE CHART ADDED FOR POT AMOUNT SHOWS ENDS HERE

        var newInqName = '';

        if( data.data[0].projectName.length > 12 ){
          newInqName  = data.data[0].projectName.substring(0, 12) + '...';
        }else{
          newInqName  = data.data[0].projectName;
        }
        newInqName  = data.data[0].projectName;
        $('#projectName').html(newInqName);

        $('#inqRemarkOutSide').html( data.data[0].inqRemark );
        $('#inqTypeOutSide').html( data.data[0].inqTypeName );
        $('#inqSourceOutSide').html( data.data[0].inqSourceName );

        
        $('#statName').html( data.data[0].statusName );
        $('#statNameList').html( data.data[0].statusName );

        $('#statName').html( data.data[0].statusName );
        $('#statusCat').val( data.data[0].statusId );
        $('#statusCat').selectpicker('refresh');

        var imgPath = (data.data[0].inqAssignToPic) == "" ? httpHead + "//"+FINALPATH+"/assets/globals/img/avtar1.png" : data.data[0].inqAssignToPic ;
        $('#userProfInq').attr('src',imgPath);
        localStorage.POTGinqStatus = data.data[0].statusId;
        
        var inqRow = '';
        
        if( data.data[0].inqTransDetail != 'No record found' && data.data[0].inqTransDetail != undefined ){
            
            for( var i=0; i<data.data[0].inqTransDetail.length; i++ ){
                
                tempData = {
                    cancleFlag: "0",
                    eventStatus: "17",
                    inqtransId: data.data[0].inqTransDetail[i].inqtransId,
                    bomId: data.data[0].inqTransDetail[i].bomId,
                    itemBasePrice: data.data[0].inqTransDetail[i].itemBasePrice,
                    itemId: data.data[0].inqTransDetail[i].itemId,
                    itemPrice: data.data[0].inqTransDetail[i].itemPrice,
                    itemQty: data.data[0].inqTransDetail[i].itemQty,
                    groupName: data.data[0].inqTransDetail[i].groupName,
                    groupId : data.data[0].inqTransDetail[i].groupId,
                    itemName: data.data[0].inqTransDetail[i].itemName,
                    itemUOM: data.data[0].inqTransDetail[i].itemUOM,
                    itemRemark: data.data[0].inqTransDetail[i].itemRemark,
                    typeOfItem: data.data[0].inqTransDetail[i].typeOfItem,
                    sequenceId: data.data[0].inqTransDetail[i].sequenceId,
                    itemCat : data.data[0].inqTransDetail[i].itemCat, // 1 = item 2 - header text 3 - BOM
                    itemStatus : data.data[0].inqTransDetail[i].itemStatus, // STATUS - PENDING / CANCEL / CONFIRM
                    detailing: data.data[0].inqTransDetail[i].detailing,
                }                                 
                inqTransData.push(tempData);
                                                        
            }
        }
        
        addRowTable( inqTransData , 'editBlock' );

        // $('#inquiryDetailTable').html(inqRow);
        
        $('.editBlock').hide();

        // TablesDataTables.init();
        // TablesDataTablesEditor.init();
        
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var catName = "INQUIRY";
        var statusInd = findIndexByKeyValue(data.data.statusListing,"statusCatName",catName);
        var statusInnerInq = findIndexByKeyValue(data.data.statusListing[statusInd].statusListing,"thirdPartyFlag","1");
        var statusCancelFlagInd = findIndexByKeyValue(data.data.statusListing[statusInd].statusListing,"remarkOnStatusFlag","1");
        var statusFinalPriority = data.data.statusListing[statusInd]['statusListing'][statusInnerInq]['statusPriority'];
        var statusFinalId = data.data.statusListing[statusInd]['statusListing'][statusInnerInq]['ID'];
        finalStatusArr =[];
        ///if(getData[0].isConfirmed == "NO"){
            for(var g=0;g<data.data.statusListing[statusInd].statusListing.length;g++){
                var finalStatusList = data.data.statusListing[statusInd].statusListing[g];
                if(finalStatusList['statusPriority'] <= statusFinalPriority && finalStatusList['ID'] != statusFinalId){
                    var tempStatus = {
                        ID : finalStatusList['ID'],
                        VALUE : finalStatusList['VALUE'],
                        statusPriority : finalStatusList['statusPriority'],
                        thirdPartyFlag : finalStatusList['thirdPartyFlag'],
                    };
                    finalStatusArr.push(tempStatus);
                }else if(finalStatusList['statusPriority'] >= statusFinalPriority && finalStatusList['ID'] == statusFinalId){
                    var tempStatus = {
                        ID : finalStatusList['ID'],
                        VALUE : finalStatusList['VALUE'],
                        statusPriority : finalStatusList['statusPriority'],
                        thirdPartyFlag : finalStatusList['thirdPartyFlag'],
                    };
                    finalStatusArr.push(tempStatus);
                }else if(finalStatusList['statusPriority'] >= statusFinalPriority){
                    var tempStatus = {
                        ID : finalStatusList['ID'],
                        VALUE : finalStatusList['VALUE'],
                        statusPriority : finalStatusList['statusPriority'],
                        thirdPartyFlag : finalStatusList['thirdPartyFlag'],
                    };
                    finalStatusArr.push(tempStatus);
                }
            }

            GBL_CANCEL_INQ_STATID = data.data.statusListing[statusInd].statusListing[statusCancelFlagInd].ID;
        //}
//        else{
//            for(var g=0;g<data.data.statusListing[statusInd].statusListing.length;g++){
//                var finalStatusList = data.data.statusListing[statusInd].statusListing[g];
//                if(finalStatusList['statusPriority'] >= statusFinalPriority && finalStatusList['ID'] == statusFinalId){
//                    var tempStatus = {
//                        ID : finalStatusList['ID'],
//                        VALUE : finalStatusList['VALUE'],
//                        statusPriority : finalStatusList['statusPriority'],
//                        thirdPartyFlag : finalStatusList['thirdPartyFlag'],
//                    };
//                    finalStatusArr.push(tempStatus);
//                }else if(finalStatusList['statusPriority'] > statusFinalPriority){
//                    var tempStatus = {
//                        ID : finalStatusList['ID'],
//                        VALUE : finalStatusList['VALUE'],
//                        statusPriority : finalStatusList['statusPriority'],
//                        thirdPartyFlag : finalStatusList['thirdPartyFlag'],
//                    };
//                    finalStatusArr.push(tempStatus);
//                }
//            }
//        }

        //Sort status array according to status priority
        finalStatusArr.sort(function(a, b) {
            return parseFloat(a.statusPriority) - parseFloat(b.statusPriority);
        });

        setOption("0", "statusCat", finalStatusArr, "--Select Inquiry Status--");
        
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var currClientType = data.data.clientTypeId;

        // console.log(currClientType);
        $('#propTabBtn').show();
        if( currClientType == "32" ){
            // openCheckListModal();

            if (performance.navigation.type == 1) {
                //console.info( "This page is reloaded" );
            } else {
                openCheckListModal();
            }
            
            $('#propTabBtn').remove();
        }

        if( getData[0].statusId == statusFinalId && data.data.productionFlag == "1" ){
            $('#productionPrint').show();
        }

        if( (currClientType == "34") && getData[0].statusId == statusFinalId ){
            $('#navExhibBtn').show();
            $('#extraItemBtn').show();
            $('#paymentBtn').show();
            $('#paymentCollectInfo').show();
            localStorage.POTGSelectedHall = getData[0].hallId;

        }else if( currClientType == "32" && getData[0].statusId == statusFinalId ){
            $('#navProjectBtn').show();
            $('#paymentBtn').remove();
            $('#mileStoneList').show();
            $('#paymentCollectInfo').remove();


            if( getData[0].flatBookFlag == "1" ){
                if( getData[0].custDetail.custLoginFlag == "1" ){
                    //DISABLE
                    $('#custEnableDisableChk').prop('checked',false);
                }else{
                    //ENABLE
                    $('#custEnableDisableChk').prop('checked',true);
                }
                $('#custEnableDisableBtn').show();  
            }
        }

        if( currClientType == "32" ){
            $('#projCheckList').show();
            $('#GalleryBtn').show();
        }

        GBLnewStatId = getData[0].statusId;
        GBLoldStatId = getData[0].statusId;

        if( localStorage.POTGredirectProp == "1" ){
            localStorage.removeItem('POTGredirectProp'); 
            $('#propBtn').click();
        }

        renderNoteData();

        if( localStorage.loadProposalTab == "true" ){
            $('#propBtn').click();
            localStorage.loadProposalTab = "false";
        }

        getInqLog();    // CALL AN INQUIRY LOG API

    } else {
        if (flag){

            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_DATA );
            setTimeout(function(){
                window.close();
            },2500);
        }
        else{   
            toastr.error(SERVERERROR);
        }
    }
}


function openCheckListModal(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getCheckList',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(50,200),
        inqId:getData[0].inqId,
        projectId:getData[0].exhibId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, openCheckListModalDataCallback,"Please Wait... Getting Dashboard Detail");

    function openCheckListModalDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            GBL_CHECKLIST_ARR = data.data;

            renderCheckListData();
            modalShow('addCheckListModal');

        } else {

            GBL_CHECKLIST_ARR = [];
            renderCheckListData();
            // modalShow('addCheckListModal');

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CHECK_LIST );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function custEnableDisable(){

    var custLoginFlag = "0";
    if( getData[0].custDetail.custLoginFlag == "0" ){
        custLoginFlag = "1";
    }else{
        var confirmDisable = confirm('Are you Sure ? You wants to disable the customer Login');
        if( !confirmDisable ){
            $('#custEnableDisableChk').prop('checked',false);
            return false;
        }
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'enableDisableCustLogin',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,15),
        inqId:getData[0].inqId,
        custId:getData[0].custId,
        custLoginFlag:custLoginFlag,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, custEnableDisableCallback,"Please Wait... Getting Dashboard Detail");

    function custEnableDisableCallback(flag,data){

        if (data.status == "Success" && flag) {

            getData[0].custDetail.custLoginFlag = custLoginFlag;
            if( custLoginFlag == "1" ){
                //DISABLE
                $('#custEnableDisableChk').prop('checked',false);
            }else{
                //ENABLE
                $('#custEnableDisableChk').prop('checked',true);
            }

            getInqLog();    // CALL AN INQUIRY LOG API

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_CUST_LOGIN );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function assignChkListChangeEvent(){

    $('input[type=checkbox][name=CheckList]').change(function() {

        var chckListId = $(this).attr('checklistid');

        var chkFlag = "2";
        if( $('#checkList_'+chckListId).prop('checked') ){
            chkFlag = "1";
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'checkUncheckChkList',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,15),
            projectId:getData[0].exhibId,
            inqId:getData[0].inqId,
            checkListId:chckListId,
            chkFlag:chkFlag,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, chkUnchkDataCallback,"Please Wait... Getting Dashboard Detail");

        function chkUnchkDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                var index = findIndexByKeyValue( GBL_CHECKLIST_ARR , "checkListId" , chckListId );
                if( index != "-1" ){
                    GBL_CHECKLIST_ARR[index].chkFlag = chkFlag;
                    GBL_CHECKLIST_ARR[index].checkListDate = mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' );
                    if( chkFlag == "2" ){
                        GBL_CHECKLIST_ARR[index].checkListDate = "";
                    }
                    renderCheckListData();
                }
                
                getInqLog();    // CALL AN INQUIRY LOG API

            } else {

                renderCheckListData();
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_FAILED_CHECK_UNCHECK );
                else
                    toastr.error(SERVERERROR);
            }
        }
    });
}

function renderCheckListData(){

    var newRow = '';

    var thead = '<table id="checkListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>Title</th>'+
                        '<th>Date</th>'+
                        '<th>Description</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    if( GBL_CHECKLIST_ARR != 'No record found' && GBL_CHECKLIST_ARR != "" ){

        GBL_CHECKLIST_ARR.forEach( function( record , index ){

            var strikThrough = "";
            var checkedFlag = "";
            if( record.chkFlag == "1" ){
                strikThrough = "strikThrough";
                checkedFlag = "checked";
            }

            newRow += '<tr>'+
                        '<td>'+
                            '<div class="form-group">'+
                                '<div class="checkboxer">'+
                                    '<input type="checkbox" '+ checkedFlag +' id="checkList_'+ record.checkListId +'" name="CheckList" checkListId="'+ record.checkListId +'" >'+
                                    '<label class="'+ strikThrough +'" for="checkList_'+ record.checkListId +'">'+ record.checkListName +'</label>'+
                                '</div>'+
                            '</div>'+
                        '</td>'+                               
                        '<td>'+ record.checkListDate +'</td>'+
                        '<td>'+ record.checkListDesc +'</td>'+
                    '</tr>';
        });

    }
    $('#CheckListTable').html(thead + newRow + tfoot);
    $('#checkListing').dataTable({"stateSave": true});
    assignChkListChangeEvent();
}


function openMileStoneModal(){

    var projectId = getData[0].exhibId;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getPaymentReportRealEstate',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(50,200),
        projectId:projectId,
        inqId:getData[0].inqId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, openMileStoneModalDataCallback,"Please Wait... Getting Dashboard Detail");

    function openMileStoneModalDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            GBL_MILESTONE_ARR = data.data;
            renderMileStoneData();

        } else {

            GBL_MILESTONE_ARR = [];
            renderMileStoneData();

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_PAYMENT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderMileStoneData(){

    var newRow = '';

    var thead = '<table id="mileStoneListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Unit Name</th>'+
                        '<th>Title</th>'+
                        '<th>Description</th>'+
                        '<th>Amount</th>'+
                        '<th>Due Date</th>'+
                        '<th>Payment Date</th>'+
                        '<th>Status</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tfoot>'+
                    '<tr>'+
                        '<th colspan="3"></th>'+
                        // '<th></th>'+
                        // '<th></th>'+
                        '<th></th>'+
                        '<th></th>'+
                        '<th></th>'+
                        '<th></th>'+
                        '<th></th>'+
                        '<th></th>'+
                    '</tr>'+
                '</tfoot>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    GBL_MILESTONE_AMT = 0;
    GBL_MS_DUE_PER = 0;
    if( GBL_MILESTONE_ARR != 'No record found' && GBL_MILESTONE_ARR != "" ){

        var count = 0;

        if( GBL_MILESTONE_ARR[0].flatTrans != "No record found" ){

            GBL_MILESTONE_ARR[0].flatTrans.forEach( function( record , index ){

                if( record.flatDetail == "No record found" ){
                    return false;
                }

                count = count + 1;
                newRow += '<tr>'+
                            '<td>'+ count +'</a></td>'+
                            '<td>'+ record.flatName +'</td>'+                               
                            '<td></td>'+                               
                            '<td></td>'+                
                            '<td></td>'+                               
                            '<td></td>'+           
                            '<td></td>'+           
                            '<td></td>'+           
                            '<td></td>'+           
                        '</tr>';

                record.flatDetail.forEach( function( innrRecord , innrIndex ){

                    var disabledBtn = "";
                    if( innrRecord.mileStoneoutStanding == "0" ){
                        disabledBtn = "disabled";
                    }

                    // innrRecord.mileStatus = "due";
                    GBL_MILESTONE_AMT = parseFloat( GBL_MILESTONE_AMT ) + parseFloat( innrRecord.mileStonePerc );
                    newRow += '<tr>'+
                                '<td><span style="display:none;">'+ count +'</span></td>'+
                                '<td></td>'+                               
                                '<td>'+ innrRecord.mileStoneTitle +'</td>'+                               
                                '<td>'+ innrRecord.mileStoneDesc +'</td>'+                
                                '<td class="amtDisp">'+ innrRecord.mileStonePerc +'</td>'+                               
                                '<td>'+ innrRecord.mileStoneDueDate +'</td>'+           
                                '<td>'+ innrRecord.mileStonePaymentDate +'</td>'+     
                                '<td><button class="btn btn-sm btn-'+ ( (innrRecord.status).toLowerCase() == "due" ? "danger" : "success" ) +'">'+ innrRecord.status +'</button></td>'+             
                                '<td><a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" mileStoneId="'+ innrRecord.mileStoneId +'" id="payBtn_'+ innrRecord.mileStoneInqId +'" outStandAmt="'+ innrRecord.mileStoneoutStanding +'" flatId="'+ record.flatId +'" mileStoneInqId="'+ innrRecord.mileStoneInqId +'" onclick="collectPayModal(this)" data-toggle="tooltip" data-placement="top" data-original-title="Collect Payment" '+ disabledBtn +'><i class="fa fa-rupee"></i></a></td>'+             
                                // '<td>'+ payDateBtn +'</td>'+           
                            '</tr>';

                        if( (innrRecord.status).toLowerCase() == "due" ){
                            GBL_MS_DUE_PER = parseFloat( GBL_MS_DUE_PER ) + parseFloat( innrRecord.mileStonePerc );
                        }
                    });
            });
        }

    }
    $('#mileStoneTable').html(thead + newRow + tfoot);
    $('#mileStoneListing').dataTable({

        "aoColumns": [
            null,
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
        ],
        "bPaginate": false,
        "bInfo": false,
        "stateSave": true,
        "footerCallback": function( tfoot, data, start, end, display ) {
            var api = this.api();

            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            $( api.column( 0 ).footer() ).html( "Total Due MileStone" );
            $( api.column( 6 ).footer() ).html( '<button class="btn btn-sm btn-danger">'+ GBL_MS_DUE_PER +' <i class="fa fa-rupee"></i></button>' );
        }
    });
    
    intdefaultdatepicker();

    $('.default-date-pickerToday').datepicker({
        format: 'dd-mm-yyyy',
        endDate: 'today',
        autoclose: true
    });
    $('.default-date-pickerToday').prop("readonly",true);
}

function collectPayModal(context){

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    
    GBL_PAY_MILEID = $(context).attr('mileStoneId');

    GBL_PAY_CUSTID = getData[0].custId;
    GBL_PAY_OUTAMT = $(context).attr('outstandamt');
    GBL_PAY_INQID = getData[0].inqId;
    GBL_PAY_FLATID = $(context).attr('flatid');

    setOption("0", "payTypeMS", roleAuth.data.paymentMode, "---Select Payment Mode---");
    $('#payTypeMS').selectpicker('refresh');

    setOption("0", "payModeMS", roleAuth.data.paymentType, "---Select  Payment Type---");
    $('#payModeMS').selectpicker('refresh');

    $('#creditCardPaymentCollectMS').hide();
    $('#ddPaymentCollectMS').hide();
    $('#cashPaymentCollectMS').hide();
    
    GBL_PAY_OUTAMT = $(context).attr('outStandAmt');
    GBL_PAY_MILEINQID = $(context).attr('mileStoneInqId');

    $('#paymentModal').modal({
      backdrop: 'static'
    });   
}


function changeFiledMS(paymentType){
    //console.log(outStandingTotal);
    if(paymentType == "36"){ // CASH
       $("#cashPaymentCollectMS").show();
       $("#ddPaymentCollectMS").hide();
       $("#creditCardPaymentCollectMS").hide();
       $("#payAmountMS").val(GBL_PAY_OUTAMT);
       $("#payAmountMS").attr("disabled",true);
    }else if(paymentType == "37"){ // DD 
       $("#ddPaymentCollectMS").show();
       $("#cashPaymentCollectMS").hide();
       $("#creditCardPaymentCollectMS").hide();
       $("#ddpayAmountMS").val(GBL_PAY_OUTAMT);
       $("#ddpayAmountMS").attr("disabled",true);
    }else if(paymentType == "38"){ // CREDIT CARD
        $("#creditCardPaymentCollectMS").show();
        $("#ddPaymentCollectMS").hide();
        $("#cashPaymentCollectMS").hide();
        $("#ccpayAmountMS").val(GBL_PAY_OUTAMT);
        $("#ccpayAmountMS").attr("disabled",true);
    }else if(paymentType == "39"){ // OTHER
        $("#cashPaymentCollectMS").show();
        $("#ddPaymentCollectMS").hide();
        $("#creditCardPaymentCollectMS").hide();
        $("#payAmountMS").val(GBL_PAY_OUTAMT);
        $("#payAmountMS").attr("disabled",true);
    }
}

function sendPayMS(){

    //var payMode = $('#payMode').val();
    var payType = $('#payTypeMS').val();
    
    // CASH & OTHER PAYMENT TYPE FIELDS
    var amount = "";
    var chequeChargeNo = "";
    var bankName = "";
    var dateOrLast4Digit = "";
    var paymentRemark = "";
    var flag = 0;
    
    if( payType == "-1" ){
        toastr.warning('Please Select Payment Mode');
        $('#payMode').focus();
        return false;

    }
    
    if(payType == "36" ){ // CASH
        
        amount = $("#payAmountMS").val();
        paymentRemark = $("#payRemarkMS").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#payAmountMS').focus();
            flag = 1;
        }else{
            flag = 0;            
        }
        
        if(flag){
            return false;
        }
    }
    
    if(payType == "37" ){ // DD
       
        amount = $("#ddpayAmountMS").val();
        chequeChargeNo = $("#ddcheckNoMS").val();
        bankName = $("#BankNameddMS").val();
        dateOrLast4Digit = $("#chequeDateddMS").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#ddpayAmountMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(chequeChargeNo.length <= 0){
            toastr.warning('Please Enter Valid DD / Cheque Number');            
            $('#ddcheckNoMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(bankName.length <= 0){
            toastr.warning('Please Enter Valid Bank Name');            
            $('#BankNameddMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(dateOrLast4Digit.length <= 0){
            toastr.warning('Please Enter Valid date');            
            $('#chequeDateddMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(flag){
            return false;
        }
    }
    
    if(payType == "38" ){ // CC
        
        amount = $("#ccpayAmountMS").val();
        chequeChargeNo = $("#ccChargeSlipNoMS").val();
        bankName = $("#BankNameMS").val();
        dateOrLast4Digit = $("#CardL4digitMS").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount.');           
            $('#ccpayAmountMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(chequeChargeNo.length <= 0){
            toastr.warning('Please Enter Valid Charge Slip Number.');            
            $('#ccChargeSlipNoMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(bankName.length <= 0){
            toastr.warning('Please Enter Valid Bank Name.');             
            $('#BankNameMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(dateOrLast4Digit.length <= 0){
            toastr.warning('Please Enter Valid last 4 digit of credit card.');            
            $('#CardL4digitMS').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(flag){
            return false;
        }
        
    }
    
    if(payType == "39" ){ // OTHER
        amount = $("#payAmountMS").val();
        paymentRemark = $("#payRemarkMS").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#payAmountMS').focus();
            flag = 1;
        }else{
            flag = 0;
        }
        
        if(flag){
            return false;
        }
    }
    
    //console.log("valiation success"); return false;
    // var totCartAmt = 0;
    // getData[0].inqTransDetail.forEach(function( record , index ){
    //     totCartAmt = totCartAmt + parseInt( record.itemPrice );
    // });
    // var remainToPay = totCartAmt - alreadyPaid;
    var remainToPay = GBL_PAY_OUTAMT;

    if( parseFloat(amount) > parseFloat(remainToPay) ){
        toastr.error('You cannot Pay More than '+remainToPay+'rs.');
        $('#payAmountMS').focus();
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    
    var postData = {
        requestCase: 'collectPayment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : GBL_PAY_INQID,
        custId : GBL_PAY_CUSTID,
        flatId : GBL_PAY_FLATID,
        mileStoneId : GBL_PAY_MILEID,
        mileStoneInqId : GBL_PAY_MILEINQID,
        paymentMode : "-1",
        paymentType : payType,
        paymentRemark : paymentRemark,
        paymentAmount : amount,
        paymentfiled1 : chequeChargeNo,
        paymentfiled2 : bankName,
        paymentfiled3 : dateOrLast4Digit,
    }
    // console.log(postData);return false;
    commonAjax(COMMONURL, postData, sendPayCallback,"Please Wait... Getting Inquiry Data");

        function sendPayCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
            // location.reload();

            GBL_PAY_OUTAMT = data.outStandingTotal;
            // getReportData();
            // $('#payBtn_'+GBL_PAY_MILEINQID).attr('disabled',true);

            openMileStoneModal();
            modalHide('paymentModal');

            getInqLog();    // CALL AN INQUIRY LOG API

            resetPayModal();
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_COLLECT_PAYMENT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function addPayment(context){

    var mileStoneId = $(context).attr('mileStoneId');
    var mileStoneDate = $('#mileStoneDate_'+mileStoneId).val();
    var paymenDate = $('#payDate_'+mileStoneId).val();
    var flatId = $(context).attr('flatId');

    if( mileStoneDate == "" ){
        toastr.warning('Please Select MileStone Date');
        $('#mileStoneDate_'+mileStoneId).datepicker('show');
        return false;

    }

    var projectId = getData[0].exhibId;
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'addMileStonePayment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(50,200),
        projectId:projectId,
        inqId:getData[0].inqId,
        mileStoneId:mileStoneId,
        mileStoneDate:mileStoneDate,
        flatId:flatId,
        paymenDate:paymenDate,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, addPaymentDataCallback,"Please Wait... Getting Dashboard Detail");

    function addPaymentDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            var index = findIndexByKeyValue( GBL_MILESTONE_ARR , "mileStoneId" , mileStoneId );
            if( index != "-1" ){
                GBL_MILESTONE_ARR[index] = data.data[0];
                renderMileStoneData();
            }

            getInqLog();    // CALL AN INQUIRY LOG API

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_COLLECT_PAYMENT );
            else
                toastr.error(SERVERERROR);
        }
    }

}


function addItemRow(){

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    
    var selectedItemName = $('#itemName').attr('itemid');
    var selectedGroupName = $('#itemGroupText').attr('groupid');
    var itemQty =  $('#itemQty').val();
    var itemStatus =  'Pending';
    var itemRemark =  $('#itemRemark').val();
    var itemHeaderText =  $('#itemHeaderText').val();
    var itemType =  $('#itemType').val();
    var itemPrice =  $('#itemPrice').val();
    if(itemType == 1){
        if((selectedItemName == '' || selectedItemName == undefined) && (selectedGroupName == "" || selectedGroupName == undefined )){
            toastr.warning('Please select item');
            $('#itemName').focus();
            return false;

        }else if( (selectedItemName != '' && selectedItemName != undefined )  && itemPrice == ""){
            toastr.warning('please Enter Price');
            $('#itemPrice').focus();
            return false;

        }else if( itemQty == "" || parseFloat( itemQty ) < 0.01 ){
            toastr.warning('please Enter Valid Quantity');
            $('#itemQty').focus();
            return false;
        }else{
            if(selectedItemName != "" && selectedItemName != undefined){
                var sequenceId = (inqTransData.length+1);
                var index = findIndexByKeyValue(itemJson,"itemId",selectedItemName);
                var searchParam = [];
                var  tempData = {
                    cancleFlag : "1",
                    itemId : itemJson[index].itemId,
                    itemName : itemJson[index].itemName,
                    groupName : itemJson[index].itemGroupName,
                    groupId : itemJson[index].groupId,
                    itemQty : itemQty,
                    itemStatus : itemStatus,
                    eventStatus : "",
                    itemPrice : (Number( itemPrice * itemQty)).toFixed(2),
                    itemBasePrice : itemPrice,
                    itemType : "ITEM",
                    itemRemark : (itemRemark != "")? itemRemark : "",
                    itemUOM : itemJson[index].itemUomName,
                    itemCat : itemType, // 1 = item 2 - header text
                    sequenceId : sequenceId,
                    typeOfItem : "ITEM",
                    detailing : detailJSON,
                }
                var dupInd = findIndexByKeyValue( inqTransData,'itemId',itemJson[index].itemId );
                if( dupInd == "-1" ){
                    inqTransData.push(tempData);
                }else{
                    searchParam.push(tempData);
                    if( roleAuth.data.clientTypeId == "35" && (tempData.detailing).length != "0" ){
                        // var loopFlag = true;
                        var addFlag = true;
                        // inqTransData.forEach( function( record , index ){
                        //     if( record.itemId == tempData.itemId && loopFlag == true ){
                        //         record.detailing.forEach( function( innrRecord , innrIndex ){
                        //             if( loopFlag == true ){
                        //                 if( jsonEqual(record.detailing, tempData.detailing) ){
                        //                     loopFlag = false;
                        //                     addFlag = false;
                        //                 }
                        //             }
                        //         });
                        //     }
                        // });


                        searchParam.forEach(function (recordSearch , indexSearch ){ 

                            // REMOVE KEY FIELD FROM JSON
                            var temp1 = [];
                            if( recordSearch.detailing != "No record found" ){
                                temp1 = [];
                                recordSearch.detailing.forEach( function( rcrdRcrd , rcrdIndx ){
                                    rcrdRcrd.children.forEach( function( innrRrcrdRcrd , innrRcrdIndx){
                                        delete innrRrcrdRcrd.key;
                                        delete innrRrcrdRcrd.inputType;
                                        delete innrRrcrdRcrd.price;
                                        temp1.push( { title : innrRrcrdRcrd.title } );
                                    });
                                    temp1.push( { title : rcrdRcrd.title } );
                                    delete rcrdRcrd.key;
                                    delete rcrdRcrd.inputType;
                                    delete rcrdRcrd.price;
                                });
                            } 
                            var temp2 = [];
                            inqTransData.forEach( function( recordProduct , indexProduct ){
                                var tempRecProduct = [];
                                if( recordProduct.detailing != "No record found" ){
                                    recordProduct.detailing.forEach( function( rcrdRcrd , rcrdIndx ){
                                        rcrdRcrd.children.forEach( function( innrRrcrdRcrd , innrRcrdIndx){
                                            delete innrRrcrdRcrd.key;
                                            delete innrRrcrdRcrd.inputType;
                                            delete innrRrcrdRcrd.price;
                                            temp2.push( { title : innrRrcrdRcrd.title } );
                                        })
                                        delete rcrdRcrd.key;
                                        delete rcrdRcrd.inputType;
                                        delete rcrdRcrd.price;
                                        temp2.push( { title : rcrdRcrd.title } );
                                    });
                                }
                                if( jsonEqual(temp1, temp2) ){
                                // if( jsonEqual(recordProduct.detailing, recordSearch.detailing) ){
                                    addFlag = false;
                                    return false; 
                                }
                            });
                        });

                        if( addFlag == true ){
                            inqTransData.push(tempData);
                        }else{
                            toastr.warning('This Item already Added in the Cart');
                        }
                        addRowTable( inqTransData );
                    }else{
                        toastr.warning('This Item already Added in the Cart');
                    }

                }
            }else if(selectedGroupName != "" && selectedGroupName != undefined){
                var sequenceId = (inqTransData.length+1);
                var index = findIndexByKeyValue(itemGroupJson,"id",selectedGroupName);
                var  tempData = {
                    cancleFlag : "1",
                    itemId : itemGroupJson[index].id,
                    itemName : "",
                    groupName : itemGroupJson[index].name,
                    groupId : itemGroupJson[index].groupId,
                    itemQty : itemQty,
                    itemStatus : itemStatus,
                    eventStatus : "",
                    itemPrice : "0.00",
                    itemBasePrice : "0.00",
                    itemType : "GROUP",
                    itemRemark : (itemRemark != "")? itemRemark : "",
                    itemUOM : (itemGroupJson[index].itemUomName != undefined) ? itemGroupJson[index].itemUomName : "",
                    itemCat : itemType, // 1 = item 2 - header text
                    sequenceId : sequenceId, 
                    typeOfItem : "GROUP" 
                }
                var dupInd = findIndexByKeyValue( inqTransData,'itemId',itemGroupJson[index].itemId );
                if( dupInd == "-1" ){
                    inqTransData.push(tempData);
                }else{
                    toastr.warning('This Group already Added in the Cart');
                }
            }

            $('#itemName').val('');
            $('#itemName').attr('itemid','');
            $('#itemQty').val('1');
            $('#itemRemark').val('');
            $('#itemGroupText').val('');
            $('#itemPrice').val('');
            $('#itemGroupText').attr('groupid','');
        }
    }else if(itemType == 2){
        if( itemHeaderText == ""){
            toastr.warning('Please enter value');
            $('#itemHeaderText').focus();
            return false;
         }else{
            var sequenceId = (inqTransData.length+1);
            var  tempData = {
                cancleFlag : "1",
                itemId : (itemType+itemHeaderText),
                itemName : itemHeaderText,
                groupName : "",
                groupId : "",
                itemQty : "",
                itemStatus : "",
                eventStatus : "",
                itemPrice : 0,
                itemBasePrice : 0,
                itemType : "ITEM",
                itemRemark : "",
                itemCat : itemType, // 1 = item 2 - header text
                sequenceId : sequenceId,
                typeOfItem : "SECTION"
            }
            inqTransData.push(tempData) 
         }
         $('#itemHeaderText').val('');
    }
    $('#AddItem').modal('hide');
    addRowTable( inqTransData );
    
    potFlag = true;
    $('#updateInqBtn').attr("onclick","changePOT()");
}

function addRowTable(itemJson , classEdit){
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    var totAmt = 0;
    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting" style="border:0px;">'+
                    '<thead>'+
                        '<tr>'+
                            '<th style="display:none"></th>'+
                            '<th>Group</th>'+
                            '<th class="inqitem">Item</th>'+
                            '<th class="">Status</th>'+
                            '<th class="">Quantity</th>'+
                            '<th class="">UOM</th>'+
                            '<th class="">Price</th>'+
                            '<th class="">Value</th>'+
                            '<th class="">Remark <i class="fa fa-info"></i></th>'+
                            '<th class="'+ classEdit +' '+ ((classEdit == "editBlock")?"hide":"") +'">Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="inquiryDetailTable">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    newPotAmt = 0;
    for ( var i=0; i<itemJson.length; i++ ){

        var popoverContent = '<table><tr>'+itemJson[i].itemRemark+'</tr></table>'

        if( itemJson[i].cancleFlag != "-1" ){  
            newPotAmt = parseInt( newPotAmt ) + parseInt( itemJson[i].itemPrice );
            if(itemJson[i].itemCat == 3 ){
                newRow += '<tr>'+
                        '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                        '<td colspan="7">'+ itemJson[i].itemName +'</td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+                    
                        '<td width="10%" class="'+ classEdit +' '+ ((classEdit == "editBlock")?"hide":"") +'">'+
                            '<button item-id="'+ itemJson[i].itemId +'" bomId="'+ itemJson[i].bomId +'" type="button" class="btn btn-primary btn-ripple btn-xs btn-danger" onclick="removeItem(this , '+i+')"><i class="fa fa-trash-o"></i></button>'+
                        '</td>'+
                    '</tr>';
            }else if(itemJson[i]['typeOfItem'] == "SECTION"){
                newRow += '<tr>'+
                        '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                        '<td colspan="7">'+ itemJson[i].itemName +'</td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+
                        '<td style="display:none"></td>'+                    
                        '<td width="10%" class="'+ classEdit +' '+ ((classEdit == "editBlock")?"hide":"show") +'">'+
    //                        '<button item-id="'+ itemJson[i].itemId +'" qty="'+ itemJson[i].itemQty +'" type="button" class="btn btn-primary btn-ripple btn-xs " onclick="editItem(this)"><i class="fa fa-pencil"></i></button>'+
                            '<button item-id="'+ itemJson[i].itemId +'" type="button" class="btn btn-primary btn-ripple btn-xs btn-danger" onclick="removeItem(this , '+i+')"><i class="fa fa-trash-o"></i></button>'+
                        '</td>'+
                    '</tr>';
            }else{
                totAmt = parseFloat( totAmt ) + parseFloat( itemJson[i].itemPrice );
                newRow += '<tr>'+
                        '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                        '<td>'+ itemJson[i].groupName +'</td>'+
                        '<td><a data-placement="top" item-id="'+ itemJson[i].itemId +'" data-toggle="tooltip" id="varConfigBtn" href="javascript:void(0)" onclick="openVCDialog(this,'+i+')" data-original-title="Item Detailing" title="" class="tooltips btn btn-blue-grey btn-xs btn-ripple e2b btn-right-mrg '+ ( (currClientType == "35") ? 'show' : 'hide' ) +'" style=""><i class="fa fa-sitemap"></i></a>&nbsp;&nbsp;&nbsp;'+ (itemJson[i].itemName != 'null' ? itemJson[i].itemName : '')+'</td>'+
                        '<td class="status">'+ itemJson[i].itemStatus +'</td>'+
                        '<td class="qty">'+ itemJson[i].itemQty +'</td>'+
                        '<td>'+ itemJson[i].itemUOM +'</td>'+
                        '<td class="basePrice amtDisp">'+ numberFormat( parseFloat( itemJson[i].itemBasePrice ) ) +'</td>'+
                        '<td class="price amtDisp">'+ numberFormat( parseFloat( itemJson[i].itemPrice ) ) +'</td>'+
                        '<td>'+
                            '<a class="color-assign1" style="font-size:14px;" id="itemRemark" data-html="true" href="javascript:void(0);" data-toggle="popover" data-content="'+ popoverContent +'" data-placement="left" data-original-title="Item Remark">'+ itemJson[i].itemRemark +'</a>'+
                        '</td>'+
                        '<td width="10%" class="'+ classEdit +' '+ ((classEdit == "editBlock")?"hide":"") +'">'+
                            '<button item-id="'+ itemJson[i].itemId +'" qty="'+ itemJson[i].itemQty +'" type="button" class="btn-right-mrg btn btn-primary btn-ripple btn-xs " onclick="editItem(this , '+i+')"><i class="fa fa-pencil"></i></button>'+
                            '<button item-id="'+ itemJson[i].itemId +'" type="button" class="btn btn-primary btn-ripple btn-xs btn-danger" onclick="removeItem(this , '+i+')"><i class="fa fa-trash-o"></i></button>'+
                        '</td>'+
                    '</tr>';
            }
        }
    }
    
    newRow += '<tr class="group">'+
                    '<td style="display:none"></td>'+
                    '<td colspan="5"> Sub Total </td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td></td>'+
                    '<td class="amtDisp">'+ numberFormat( totAmt ) +'</td>'+
                    '<td></td>'+
                    '<td class="'+ classEdit +' '+ ((classEdit == "editBlock")?"hide":"") +'"></td>'+
                    // '<td style="display:none"></td>'+
                '</tr>';

    $('#dtTable').html(thead + newRow + tfoot);
    $('.color-assign1').popover({
      trigger: 'Click'
    })

    // console.log(newPotAmt);
    $("#tableListing").DataTable({
        bSort : false,
        "stateSave": true
    });

    // console.log(currClientType);
    if( currClientType == 35 ){
        $('.e2b').show();
    }else{
        $('.e2b').hide();
    }
}


function AutoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
            
            var endDate = mysqltoDesiredFormat( ITEMS[itemIndex].priceValidityEndDate , "dd-MM-yyyy" );
            // if( new Date(ITEMS[itemIndex].priceValidityEndDate +" 11:59 PM") < new Date() ){
            //     toastr.warning(ITEMS[itemIndex].itemName+' Price validity is Over on <b>"'+ endDate +'"</b>, So you need to set it from Client Master page');
            //     $('#searchitem').val('');
            //     return false;
            // }
            
            $('#itemName').attr('itemId',item.value);
            $('#itemGroupText').val(ITEMS[itemIndex].itemGroupName);
            $('#itemPrice').val(ITEMS[itemIndex].itemPrice);

            $('#extraItemName').attr('itemId',item.value);
            var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
            $('#extraItemGroupText').val(ITEMS[itemIndex].itemGroupName);
            $('#extraItemGroupText').attr('groupid',ITEMS[itemIndex].itemGroupId);
            $('#extraItemPrice').val(ITEMS[itemIndex].itemPrice);
            $('#extraItemQty').val("1");

            bomJson = ITEMS[itemIndex].bomInnerItem;
            
            callback(ITEMS[itemIndex].bomInnerItem);

            if( ITEMS[itemIndex].detailing != "No record found" && ITEMS[itemIndex].detailing != "" && ITEMS[itemIndex].detailing != null ){
                openDetailModal(ITEMS[itemIndex].detailing);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {

                var type = "1";
                var itemType = $('#itemType').val();
                if( itemType == 3 ){
                    type = "3";
                }

                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getItemListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(4, 14),
                    keyWord: query,
                    type: type,
                    custRateType:GBLRATETYPEID
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                        if( data != "No record found" ){
                            var tempJson = '';
                            tempJson = {
                                id: data.itemId,
                                name: data.itemName,
                                data: data.itemName,
                                clientId: data.clientId,
                                itemId: data.itemId,
                                itemName: data.itemName,
                                itemGroupName: data.itemGroupName,
                                itemGroupId: data.itemGroupId,
                                itemPrice: data.itemPrice,
                                itemSubType: data.itemSubType,
                                itemType: data.itemType,
                                orgId: data.orgId,
                                validityEndDate: data.validityEndDate,
                                itemUomName: data.itemUomName,
                                itemUomId: data.itemUomId,
                                bomInnerItem: data.bomInnerItem,
                                priceValidityEndDate: data.priceValidityEndDate,
                                detailing: data.detailing,
                            }
                            itemJson.push(tempJson);
                            ITEMS.push(tempJson);
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveItemDetail(){

}

function editInq(type){

    if( checkAuth(4, 15, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var currStatusId = getData[0].statusId;

    if( type == "0" && GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Stat');
        return false;
    }
    
    var proStatusData = finalStatusArr;
    var index = findIndexByKeyValue(proStatusData,"ID",currStatusId);
    var tempFlag = "";
    
    if( type == 0 ){    
        
        var index = findIndexByKeyValue( proStatusData , "thirdPartyFlag" , "1" );
        if( index != "-1" ){
            if( currStatusId == proStatusData[index].ID || parseInt( getData[0].statusPriority ) > parseInt( proStatusData[index].statusPriority ) ){
                toastr.error('Inquiry cant be update when it is in won state');
                return false;
            }
        }
    }
    
    var priorityOption = '';
    for( var j=0; j<proStatusData.length; j++ ){

        var selcted = "";
        if( index == "-1" ){
            toastr.warning('No Status Were Found');
            return false;
        }
        
        if((proStatusData[j].statusPriority ==  proStatusData[index].statusPriority) && (proStatusData[j].ID != proStatusData[index].ID)){
            var tempData = {
                ID: proStatusData[j].ID,
                VALUE: proStatusData[j].VALUE,
                statusPriority: proStatusData[j].statusPriority,
                thirdPartyFlag: proStatusData[j].thirdPartyFlag,
                disFlag : 'remove',
            };

            proStatusData[j] = tempData;
        }else if((proStatusData[j].statusPriority > proStatusData[index].statusPriority)){

            // if(tempFlag == "" || tempFlag == proStatusData[j].statusPriority){
                var tempData = {
                    ID: proStatusData[j].ID,
                    VALUE: proStatusData[j].VALUE,
                    statusPriority: proStatusData[j].statusPriority,
                    thirdPartyFlag: proStatusData[j].thirdPartyFlag,
                    disFlag : 'off',
                };
                // tempFlag = proStatusData[j].statusPriority;
                proStatusData[j] = tempData;
            // }
        }

        var thirdPartyFlag = proStatusData[j].thirdPartyFlag
        // if(proStatusData[j].disFlag != "remove"){
        //     if( proStatusData[j].disFlag == "off"){
                priorityOption += '<option value="'+ proStatusData[j].ID +'" currStatus="'+ currStatusId +'" thirdPartyFlag="'+ thirdPartyFlag +'" flag = '+proStatusData[j].disFlag+' priority = '+proStatusData[j].statusPriority+'>'+ proStatusData[j].VALUE +'</option>';
        //     }else{
        //         //priorityOption += '<option '+ disabled +' value="'+ proStatusData[j].ID +'" '+ selcted +'  currStatus="'+ currStatusId +'" thirdPartyFlag="'+ thirdPartyFlag +'" priority = '+proStatusData[j].statusPriority+'>'+ proStatusData[j].VALUE +'</option>';
        //         priorityOption += '';
        //     }
        // }
    }

    if( priorityOption.length == 0 ){
        toastr.warning('Oops, Inquiry Reached At Final Status, You are not allowed to Edit');
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = tmp_json.data[0].PK_USER_ID;
    var assignedToId = getData[0].inqAssignToId;
    var authToUserId = getData[0].authToThisUser;

    if ( userId == assignedToId || userId == authToUserId ){

        if( type == 0 ){
            $('.editBlock').show();
            $('.editBlock').removeClass('hide');
            $('.normalBlock').hide();
        }
        
        finalEditMoreInfoData = [];
        var tempEditMoreInfoData = {
            projectName : getData[0].projectName,
            inqRemark : getData[0].inqRemark,
            field1 : getData[0].field1,
            field2 : getData[0].field2,
            field3 : getData[0].field3,
            field4 : getData[0].field4,
            field5 : getData[0].field5,
            field6 : getData[0].field6,
            field7 : getData[0].field7,
            field8 : getData[0].field8,
            field9 : getData[0].field9,
            field10 : getData[0].field10,
        }
        finalEditMoreInfoData.push(tempEditMoreInfoData);
    }else{
        toastr.warning('You are not Authorized to Edit the Inquiry');
    }
}

function updateInquiry(){
    
    if( checkAuth(4, 15, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;

    }

    var modalOpen = $('#updatePotAmt').hasClass('in');
    if( modalOpen ){

        if( $('#newPotInput').val() == "" ){
            toastr.warning('Please Enter POT Amount');
            $('#newPotInput').focus();
            return false;
        }
        getData[0].potAmt = $('#newPotInput').val();
    }

    var transEmpty = false;

    var cancelFlag1 = findIndexByKeyValue( inqTransData , "cancleFlag" , "1" );
    var cancelFlag0 = findIndexByKeyValue( inqTransData , "cancleFlag" , "0" );
    // var typeItem = findIndexByKeyValue( inqTransData , "typeOfItem" , "ITEM" );
    
    if( cancelFlag1 == "-1" && cancelFlag0 == "-1" ){
        transEmpty = true;
    // }else if( typeItem == "-1" ){
    //     transEmpty = true;
    }

    if( inqTransData.length == "0" || transEmpty ){
        toastr.warning("Please add Items before updating an Inquiry");
        return false;

    }

    // console.log(finalTaxation);return false;

    inqMasterData = {
        inqDate : getData[0].inqDate,
        inqId : getData[0].inqId,
        inq_trans : inqTransData,
        inqStatus : localStorage.POTGinqStatus,
        newStatusId : GBLnewStatId,
        currentStatusId : GBLoldStatId,
        closerBy : getData[0].closerBy,
        potAmt :  getData[0].potAmt,
        sourceOf :  getData[0].inqSource,
        inqType :  getData[0].inqType,
        custId : getData[0].custDetail.customerId,
        cancelRemarkId : (cancelRemarkData.length > 0) ? cancelRemarkData[0]['cancelRemarkId'] : "0",
        cancelRemarkText : (cancelRemarkData.length > 0) ? cancelRemarkData[0]['cancelText'] : "",
    }

    var cartModalOpen = $('#cartDataModal').hasClass('in');
    if( cartModalOpen ){

        var dealWonRemarkId = $('#dealWonRemarkId').val();
        var dealWonRemarkText = $('#dealWonRemark').val();

        var tempOrderCart = [];
        newCartData.forEach( function( record , index ){
            if( record.deleteFlag == "0" ){
                tempOrderCart.push( record );
            }
        });

        if( tempOrderCart.length == "0" ){
            toastr.warning('Please Select atleast one item for confirm inquiry');
            return false;
        }else if( dealWonRemarkId == "-1" ){
            toastr.warning('Please Select Deal Won Reason');
            $('#dealWonRemarkId').focus();
            return false;

        }else if( dealWonRemarkText == "" ){
            toastr.warning('Please Enter Deal Won Remark');
            $('#dealWonRemark').focus();
            return false;
        }

        inqMasterData.taxDiscDetail = GBLheaderLevelData;
        inqMasterData.dealWonRemarkId = dealWonRemarkId;
        inqMasterData.dealWonRemarkText = dealWonRemarkText;
    }

    // START INSERT MORE INFO INTO INQMASTER JSON
    inqMasterData['projectName'] = (typeof finalEditMoreInfoData[0]['projectName'] != "undefined" || finalEditMoreInfoData[0]['projectName'] != null) ? finalEditMoreInfoData[0]['projectName'] : ""; // finalEditMoreInfoData[0]['inqRemark'];
    inqMasterData['inqRemark'] = (typeof finalEditMoreInfoData[0]['inqRemark'] != "undefined" || finalEditMoreInfoData[0]['inqRemark'] != null) ? finalEditMoreInfoData[0]['inqRemark'] : ""; // finalEditMoreInfoData[0]['inqRemark'];
    inqMasterData['field1'] = (typeof finalEditMoreInfoData[0]['field1'] != "undefined" || finalEditMoreInfoData[0]['field1'] != null) ? finalEditMoreInfoData[0]['field1'] : "";
    inqMasterData['field2'] = (typeof finalEditMoreInfoData[0]['field2'] != "undefined" || finalEditMoreInfoData[0]['field2'] != null) ? finalEditMoreInfoData[0]['field2'] : "";
    inqMasterData['field3'] = (typeof finalEditMoreInfoData[0]['field3'] != "undefined" || finalEditMoreInfoData[0]['field3'] != null) ? finalEditMoreInfoData[0]['field3'] : "";
    inqMasterData['field4'] = (typeof finalEditMoreInfoData[0]['field4'] != "undefined" || finalEditMoreInfoData[0]['field4'] != null) ? finalEditMoreInfoData[0]['field4'] : "";
    inqMasterData['field5'] = (typeof finalEditMoreInfoData[0]['field5'] != "undefined" || finalEditMoreInfoData[0]['field5'] != null) ? finalEditMoreInfoData[0]['field5'] : "";
    inqMasterData['field6'] = (typeof finalEditMoreInfoData[0]['field6'] != "undefined" || finalEditMoreInfoData[0]['field6'] != null) ? finalEditMoreInfoData[0]['field6'] : "";
    inqMasterData['field7'] = (typeof finalEditMoreInfoData[0]['field7'] != "undefined" || finalEditMoreInfoData[0]['field7'] != null) ? finalEditMoreInfoData[0]['field7'] : "";
    inqMasterData['field8'] = (typeof finalEditMoreInfoData[0]['field8'] != "undefined" || finalEditMoreInfoData[0]['field8'] != null) ? finalEditMoreInfoData[0]['field8'] : "";
    inqMasterData['field9'] = (typeof finalEditMoreInfoData[0]['field9'] != "undefined" || finalEditMoreInfoData[0]['field9'] != null) ? finalEditMoreInfoData[0]['field9'] : "";
    inqMasterData['field10'] = (typeof finalEditMoreInfoData[0]['field10'] !== "undefined" || finalEditMoreInfoData[0]['field10'] != null) ? finalEditMoreInfoData[0]['field10'] : "";
    inqMasterData['field11'] = (typeof finalEditMoreInfoData[0]['field11'] != "undefined" || finalEditMoreInfoData[0]['field11'] != null) ? finalEditMoreInfoData[0]['field11'] : "";
    inqMasterData['field12'] = (typeof finalEditMoreInfoData[0]['field12'] != "undefined" || finalEditMoreInfoData[0]['field12'] != null) ? finalEditMoreInfoData[0]['field12'] : "";
    inqMasterData['field13'] = (typeof finalEditMoreInfoData[0]['field13'] != "undefined" || finalEditMoreInfoData[0]['field13'] != null) ? finalEditMoreInfoData[0]['field13'] : "";
    inqMasterData['field14'] = (typeof finalEditMoreInfoData[0]['field14'] != "undefined" || finalEditMoreInfoData[0]['field14'] != null) ? finalEditMoreInfoData[0]['field14'] : "";
    inqMasterData['field15'] = (typeof finalEditMoreInfoData[0]['field15'] != "undefined" || finalEditMoreInfoData[0]['field15'] != null) ? finalEditMoreInfoData[0]['field15'] : "";
    inqMasterData['field16'] = (typeof finalEditMoreInfoData[0]['field16'] != "undefined" || finalEditMoreInfoData[0]['field16'] != null) ? finalEditMoreInfoData[0]['field16'] : "";
    inqMasterData['field17'] = (typeof finalEditMoreInfoData[0]['field17'] != "undefined" || finalEditMoreInfoData[0]['field17'] != null) ? finalEditMoreInfoData[0]['field17'] : "";
    inqMasterData['field18'] = (typeof finalEditMoreInfoData[0]['field18'] != "undefined" || finalEditMoreInfoData[0]['field18'] != null) ? finalEditMoreInfoData[0]['field18'] : "";
    inqMasterData['field19'] = (typeof finalEditMoreInfoData[0]['field19'] != "undefined" || finalEditMoreInfoData[0]['field19'] != null) ? finalEditMoreInfoData[0]['field19'] : "";
    inqMasterData['field20'] = (typeof finalEditMoreInfoData[0]['field20'] !== "undefined" || finalEditMoreInfoData[0]['field20'] != null) ? finalEditMoreInfoData[0]['field20'] : "";

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "updateInquiry",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 15),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inq_master : inqMasterData,
        orderCart : tempOrderCart,
    };
   
    // console.log(postData); return false;
    commonAjax(INQURL, postData, updateInquiryTroughPopupCallBack,CREATINGINQDATA);
    $('.normalBlock').show();
    $('.editBlock').hide();
}

function updateInquiryTroughPopupCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        toastr.success("Inquiry Updated Successfully")
        console.log(data);
        
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var currClientType = data.data.clientTypeId;
        console.log(currClientType);

        $('#updatePotAmt').modal( 'hide' );
        $('#cartDataModal').modal( 'hide' );

        // if(  )

        if( ( currClientType == 34 || currClientType == 32 ) && finalStatWon ){
            if( currClientType == 32 ){
                navigateToRealEstateBookInq( getData[0].exhibId , getData[0].inqId , getData[0].clientLevelInqId );
            }else{
                localStorage.exhibitionId = getData[0].exhibId;
                localStorage.POTGinqIdForStallBook = getData[0].inqId;
                navigateToCreateStallsWithExhbId();
            }
        }else if( GBL_CANCEL_FLAG == "1" ){
            // window.location.href = "inquirydetail.html";
            // getinquiryDetailListing();
            navigateToInquiry();
            $('#navExhibBtn').hide();
            $('#navProjectBtn').hide();
            $('#visitCard').hide();
            $('#extraItemBtn').hide();
            $('#paymentBtn').hide();
        }else{
            getinquiryDetailListing();
        }

        getInqLog();    // CALL AN INQUIRY LOG API
        
    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_UPDATE_INQ );
        else
            toastr.error(SERVERERROR);
    }
}

function removeItem(context,index){

    potFlag = true;
    $('#updateInqBtn').attr("onclick","changePOT()");

    var deleteRow = confirm('Are You sure ? You want to delete this Item from Cart');

    if( deleteRow ){
        
        var itemId = $(context).attr('item-id');
        var bomId = $(context).attr('bomId');

        if( bomId != "" && bomId != "undefined" && bomId != undefined ){

            // for( var i=0; i<inqTransData.length; i++ ){
            //     var index = findIndexByKeyValueAndCond(inqTransData,"bomId","cancleFlag",bomId,'0');
            //     if( index != "-1" ){
            //         if( inqTransData[index].inqtransId == "" ){
            //             inqTransData.splice(index,1);
            //         }else{
            //             inqTransData[index].cancleFlag = '-1';
            //         }
            //     }
            // }
            for( var i=inqTransData.length-1; i>=0; i-- ){
                var index = findIndexByKeyValue(inqTransData,"bomId",bomId);
                // var index = findIndexByKeyValue(inqTransData,"bomId",bomId);
                if( index != "-1" ){
                    if( inqTransData[index].inqtransId == "" ){
                        inqTransData.splice(index,1);
                    }else{
                        inqTransData[index].cancleFlag = '-1';
                    }
                }
            }
            //addRowTable( inqTransData , "editBlock" );

        }else{
            var index = index;    
            // var index = findIndexByKeyValue(inqTransData,"itemId",itemId);    
            $(context).parents('tr').remove();
            
            if( inqTransData[index].inqtransId == "" ){
                inqTransData.splice(index,1);
            }else{
                inqTransData[index].cancleFlag = '-1';
            }
            newPotAmt = parseInt( newPotAmt ) - parseInt( inqTransData[index].itemPrice );

        }
        // addRowTable( inqTransData , "" );
        addRowTable(inqTransData , "editBlock");
        editInq('0');

    }
}
var editInd = '';
function editItem(context,indexPos){

   var itemId = $(context).attr('item-id');
   var qty = $(context).attr('qty');
   
   editContext = context;

   $('#rowdataEditBtn').attr('item-id',itemId);

   // var index = findIndexByKeyValue( inqTransData , 'itemId' , itemId );
   var index = indexPos;
   editInd = indexPos;
   var remarkData = inqTransData[index].itemRemark;
   var price = inqTransData[index].itemBasePrice;

    $('#editQty').val(qty); 
    $('#editRemark').val(remarkData); 
    $('#editPice').val(price); 
    
    if( inqTransData[index].typeOfItem != "GROUP" ){    
       $('#editPice').attr('disabled',false);
    }else{
        $('#editPice').attr('disabled',true);
    }

    $('#rowdataEdit').modal('show');
    addFocusId( 'editPice' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

}

function UpdateRowData(context){

    var itemId = $(context).attr('item-id');
    
    var qty = $('#editQty').val();
    var Remark = $('#editRemark').val();
    var price = $('#editPice').val();
    
    if( price == "" ){
        toastr.warning("Please Enter price");
        $('#editPice').focus();
        return false;

    }else if( qty == ""  || parseFloat( qty ) < 0.1 ){
        toastr.warning("Please Enter Valid Quantity");
        $('#editQty').focus();
        return false;

    // }else if( Remark == "" ){
    //     toastr.warning("Please Enter Remark");
    //     $('#editRemark').focus();
    //     return false;

    }else{

        potFlag = true;
        $('#updateInqBtn').attr("onclick","changePOT()") ;
        
        // var index = findIndexByKeyValue(inqTransData,'itemId',itemId);
        var index = editInd;

        var multipl = ( parseFloat(qty) * parseFloat(price) ).toFixed(2);

        inqTransData[index].itemBasePrice = price;
        inqTransData[index].itemQty = qty;
        inqTransData[index].itemPrice = ( multipl );
        inqTransData[index].itemRemark = Remark;

        $(editContext).parents('td').parents('tr').find('.basePrice').html( price );
        $(editContext).parents('td').parents('tr').find('.price').html( multipl );
        $(editContext).parents('td').parents('tr').find('.qty').html(qty);
        $('#editRemark').val("");
        $('#rowdataEdit').modal('hide');
        addRowTable(inqTransData , "editBlock");
        editInq('0');
    }

}


//Followup code

function getFollowupListing(){

    if( GBL_ACT_LOADED == "1" ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = tmp_json.data[0].PK_USER_ID;
    var assignedToId = getData[0].inqAssignToId;

    // if ( userId == assignedToId ){
        // NOT IN USE COMMENT BY DEEPAK PATIL ON 06-07-2016
//        var postData = {
//            requestCase: "followupInqDetail",
//            clientId: tmp_json.data[0].FK_CLIENT_ID,
//            userId: tmp_json.data[0].PK_USER_ID,
//            orgId: checkAuth(26, 102),
//            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//            inqId : getData[0].inqId
//        };
        var postData = {
            requestCase: "activityInqDetail",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(26, 102),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            inqId : getData[0].inqId
        };
        // console.log(postData); return false;
        commonAjax(INQURL, postData, getFollowupListingCallBack,CREATINGINQDATA);
    // }else{
    //     toastr.warning('You are not Authorized for this Activity');
    // }
}

function getFollowupListingCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        GBL_ACT_LOADED = "1";
        if( data.data.followupDetail == 'No activity found on this inquiry' ){
            toastr.warning('No activity found on this inquiry');
            $('#followup').modal('show');
            return false;
        }

        console.log(data);
        var followupData = '<li class="tl-header">'+
                                '<div class="btn btn-info">Now</div>'+
                            '</li>';
        
        for( var i=0; i<data.data.followupDetail.length; i++ ){


            var followupRemark = data.data.followupDetail[i].ACT_TITLE;
            var createTime = data.data.followupDetail[i].CREATEDTIME;
            var assignTo = data.data.followupDetail[i].ASSIGNEDTO;
            var actId = data.data.followupDetail[i].PK_ACT_ID;
            var actStatus = data.data.followupDetail[i].ACT_STATUS;
            var actStatusRepIndex = findIndexByKeyValue(STATUS_DYNAMIC_TYPE,"key","activity report");
            var actStatusIndex = findIndexByKeyValue(STATUS_DYNAMIC_TYPE[actStatusRepIndex]['lookup'],"ID",actStatus);
            var actStatusName = STATUS_DYNAMIC_TYPE[actStatusRepIndex]['lookup'][actStatusIndex]['VALUE'];
            var tmp_date1 = new Date(data.data.followupDetail[i].DUE_DATE);
            var month1 = months[ tmp_date1.getMonth() ];
            var date1 = tmp_date1.getDate();
            var year1 = tmp_date1.getFullYear();

            var date = month1 +' '+ date1 +', '+ year1 ;
            var dueOn = date;

            if( data.data.followupDetail[i].DUE_TIME != 0 ){
                dueOn = dueOn + ' ' + data.data.followupDetail[i].DUE_TIME;
            }
            var openCloseClass = (data.data.followupDetail[i].ENDTTIME == "") ? "open" : "closeAct";
            followupData += '<li class="tl-item">'+
                                '<div class="tl-wrap '+openCloseClass+' ">'+
                                    '<a href="javascript:void(0)" data-activityid="'+actId+'" onclick="navigateToActivityAppDetailsPage(this)"><div class="tl-content panel-1 padder b-a">'+
                                        '<span class="arrow left pull-up"></span>'+
                                        '<div>'+ followupRemark +'</div>'+
                                        '<div class="customInqActListing">Assign To : '+assignTo+'</div>'+
                                        '<div class="customInqActListing"><b>Activity Status: '+actStatusName+'</b></div>'+
                                        '<div class="customInqActListing">Created By : '+createTime+'</div>'+
                                        '<div class="customInqActListing">Due On : '+dueOn+'</div>'+
                                    '</div></a>'+
                                '</div>'+
                            '</li>';
        }
        
        followupData += '<li class="tl-header">'+
                            '<div class="btn btn-sm btn-primary btn-rounded">more</div>'+
                        '</li>';
        
        $('#flwpTimeline').html(followupData);

        $('#followup').modal('show');
        
    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_ACT );
        else
            toastr.error(SERVERERROR);
    }
}


function createFollowup(){

    var followupDate = $('#followupDate').val();
    var followupRemark = $('#followupRemark').val().trim();

    if( followupDate == "" ){
        toastr.warning('Please Select Followup Date');
        $('#followupDate').datepicker('show');
        return false;

    }else if( followupRemark.length == 0 ){
        toastr.warning('Please Enter Followup Remark');
        $('#followupRemark').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase: "followupAddDetail",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(26, 101),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            followupDate : ddmmyyToMysql( followupDate , 'yyyy-MM-dd'),
            followupRemark : followupRemark,
            inqId : getData[0].inqId
        };
       
        // console.log(postData); return false;
        commonAjax(INQURL, postData, createFollowupCallBack,CREATINGINQDATA);
    }
    
}


function createFollowupCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        toastr.success("Followup Inserted Successfully")
        console.log(data);
        
        $('#addFollowup').modal('hide');
         getFollowupListing();

         getInqLog();    // CALL AN INQUIRY LOG API

    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_ACT );
        else
            toastr.error(SERVERERROR);
    }
}

function changeInqStatus(){

    var roleAuthData = JSON.parse(localStorage.indeCampusRoleAuth);

    var statusCat = $('#statusCat').val();
    var statusPriCat = "";
    var statustext = $('#statusCat option:selected').text();
    
    GBLnewStatId = statusCat;
    // $('#statName').html(statustext);
    var statusArr = JSON.parse(localStorage.indeCampusRoleAuth);
    var index = findIndexByKeyValue(statusArr.data.statusListing,"statusCatName","INQUIRY");
    if(index != "-1"){
        var innerIndex = findIndexByKeyValue(statusArr.data.statusListing[index]['statusListing'],"ID",statusCat);
        
        if(innerIndex != "-1"){

            statusPriCat = statusArr.data.statusListing[index]['statusListing'][innerIndex].statusPriority;
            if(statusArr.data.statusListing[index]['statusListing'][innerIndex]['remarkOnStatusFlag'] == 1){
                // OPEN POPUP FOR CANCEL REMARK
                showCancelRemark(statusCat,statustext);
            }else{

                statChange();
            }
        }else{
            statChange();
        }   
    }else{
        statChange();
    }

    function statChange(){

        var statCatList = statusArr.data.statusListing[index]['statusListing'];
        var statFlag = true;
        for( var n=0; n<statCatList.length; n++ ){
            if( parseInt( statusArr.data.statusListing[index]['statusListing'][n].statusPriority ) > parseInt( statusPriCat ) ){
                statFlag = false;
            }
        }

        $('#potPopSubmit').attr('onclick','updateInquiry()');
        if( statFlag ){
            
            // if( roleAuthData.data.inquiryBookingFlag == "1" && getData[0].inqBookFlag == "0"){
            //     toastr.warning("Please approve proposal before confirming inquiry");
            //     return false;
            // }
            var confirmStat = confirm('Are You sure ? You want to Proceed Inquiry to '+statustext+' Status.');

            if( confirmStat ){
                // $('#statNameList').html(statustext);
                localStorage.POTGinqStatus = statusCat;
                finalStatWon = true;
                potFlag = true;
                editInq(1);
                changePOT();
                $('#potPopSubmit').attr('onclick','openCartData()');
                $('#newPotInput').focus();
            }
        }else{
            // $('#statNameList').html(statustext);
            localStorage.POTGinqStatus = statusCat;
            editInq(1);
            updateInquiry();
        }
    }
    $('#changeStatus').modal('hide');
}

function cancelEdit(){
    $('.editBlock').hide();
    $('.normalBlock').show();
}

function openInqStatusPop(){

    if( checkAuth(4, 15, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;
    }

    var currStatusId = getData[0].statusId;

//    $('#statusCat').val( currStatusId );
//    
//    $('#statusCat option').attr('disabled','true')
//
//    $('#statusCat option:selected+option').removeAttr('disabled');
//
//    $('#statusCat').selectpicker('refresh');
//    $('#changeStatus').modal('show');
    var prioritySelection = '';
    var priorityHead = '<select class="priority">';
    var priorityOption = '';
    var priorityFoot = '</select>';
    var disabled = 'disabled';
    
    var proStatusData = finalStatusArr;
    var index = findIndexByKeyValue(proStatusData,"ID",currStatusId);
    var tempFlag = "";
    var confirmFlag = 0;
    for( var j=0; j<proStatusData.length; j++ ){

        var selcted = "";
        if( index == "-1" ){
            toastr.warning('No Status Were Found');
            return false;
        }
        if(proStatusData[index].ID == proStatusData[j].ID){
            selcted = "selected";
        }

        if((proStatusData[j].statusPriority ==  proStatusData[index].statusPriority) && (proStatusData[j].ID != proStatusData[index].ID)){
            var tempData = {
                ID: proStatusData[j].ID,
                VALUE: proStatusData[j].VALUE,
                statusPriority: proStatusData[j].statusPriority,
                thirdPartyFlag: proStatusData[j].thirdPartyFlag,
                disFlag : 'remove',
            };

            proStatusData[j] = tempData;
        }else if(( parseInt( proStatusData[j].statusPriority ) > parseInt( proStatusData[index].statusPriority ) )){

            // if(tempFlag == "" || tempFlag == proStatusData[j].statusPriority){
                var tempData = {
                    ID: proStatusData[j].ID,
                    VALUE: proStatusData[j].VALUE,
                    statusPriority: proStatusData[j].statusPriority,
                    thirdPartyFlag: proStatusData[j].thirdPartyFlag,
                    disFlag : 'off',
                };
                // tempFlag = proStatusData[j].statusPriority;
                proStatusData[j] = tempData;
            // }
        }

        var thirdPartyFlag = proStatusData[j].thirdPartyFlag
        // if(proStatusData[j].disFlag != "remove"){
        //     if( proStatusData[j].disFlag == "off"){
                priorityOption += '<option value="'+ proStatusData[j].ID +'" '+ selcted +' currStatus="'+ currStatusId +'" thirdPartyFlag="'+ thirdPartyFlag +'" flag = "'+proStatusData[j].disFlag+'" priority = '+proStatusData[j].statusPriority+'>'+ proStatusData[j].VALUE +'</option>';
        //     }else{
        //         //priorityOption += '<option '+ disabled +' value="'+ proStatusData[j].ID +'" '+ selcted +'  currStatus="'+ currStatusId +'" thirdPartyFlag="'+ thirdPartyFlag +'" priority = '+proStatusData[j].statusPriority+'>'+ proStatusData[j].VALUE +'</option>';
        //         priorityOption += '';
        //     }
        // }
    }
    //console.log(priorityOption);
    prioritySelection = priorityHead + priorityOption + priorityFoot;
    $('#statusCat').html(prioritySelection);
    if( priorityOption.length == 0 ){
        toastr.warning('No More Status Available');
    }else{
        $('#statusCat').selectpicker('refresh');
        $('#changeStatus').modal('show');
    }
}

function addFollowupOpen(){
    $("#followupDate").val('');
    $("#followupRemark").val('');
    $('.bootstrap-daterangepicker-basic').datetimepicker('remove');
    initBootStrapDatePicker();
    $('#addFollowup').modal('show');
}

function openItemUpdate(){
    $("#itemType").selectpicker(); 
    $('#AddItem').modal('show'); 
    addFocusId( 'itemName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function editProName(){

    $('#proNameEdit').val( $('#projectName').html() );
    $('#editProjectName').modal('show');
}

function editMoreInfo(flag){
    if(flag == false){
        var disableClass = "disabled";
        var readOnlyClass = "readonly";
    }
    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        var divStruct = '<div class="row">';
        for(var j=0;j<localJson.data.customFieldDetail.length;j++){
            if(localJson.data.customFieldDetail[j]['tabType'] == "INQMASTER"){
                divStruct += '<div class="col-lg-6">';
                if(localJson.data.customFieldDetail[j].fieldType == "text"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="'+ getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'" />';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "number"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="number" min="0" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+ getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'"/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "email"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="email" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+ getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'"/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<textarea maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'">'+ getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</textarea>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
                    
                    divStruct += '<div class="form-group radioer">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)] ){
                            selectedClass = "checked";
                            if( flag == false ){
                                divStruct += '<span>'+ explodeValue[g] +'</span>'
                            }   
                        }
                        if( flag != false ){
                            divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+' '+disableClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
                        }
                    }
               
                }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectpicker" '+disableClass+'>';
                    divStruct += '<option value="-1">---'+localJson.data.customFieldDetail[j]['fieldName']+'---</option>';
                    for(var g=0;g<explodeValue.length;g++){
                        var classSeleted = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            classSeleted = "selected";
                        }
                        divStruct += '<option value="'+explodeValue[g]+'" '+classSeleted+'>'+explodeValue[g]+'</option>';
                    }
                    divStruct += "</select>";
                    $("#field"+(j+1)).selectpicker();
               
                }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" class="normal-date-picker form-control" readonly id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]+'" '+readOnlyClass+' />';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input class="normal-date-picker" type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+' '+disableClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += '<div class="form-group checkboxer">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    var explodeGetData = getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)].split(",");
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span></br>";
                    }else{
                        requiredClass = "";
                    }
                    
                    divStruct += requiredClass;
                    for(var g=0;g<explodeValue.length;g++){
                        for(var h=0;h<explodeGetData.length;h++){
                            var selectedClass = "";
                            if(explodeValue[g] == explodeGetData[h] ){
                                selectedClass = "checked";
                                break;
                            }
                        }
                        divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ '" value="'+explodeValue[g]+'" '+selectedClass+' '+ disableClass +' /> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" '+readOnlyClass+'>'+explodeValue[g]+'</label>';
                    }
                }
                divStruct += '</div>'+
                '</div>';
                if(((j+1)%2) == 0){
                    divStruct += '</div><div class="row">';

                }   
                $("#customFieldDiv").show();
            }
            
        }
        $("#customFieldDiv").html(divStruct);
        $('.normal-date-picker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    }
    $("#inqRemark").val(getData[0]['inqRemark']);
    $('#proNameEdit').val( getData[0]['projectName']);
    $('#potAmtVw').val( getData[0]['potAmt']);
    $('#closurByVw').val( mysqltoDesiredFormat( getData[0]['closerBy'],'dd-MM-yyyy'));

    $('#inquirytype').val( getData[0]['inqType']).selectpicker('refresh');
    $('#inquirysource').val( getData[0]['inqSource']).selectpicker('refresh');

    
    if(flag == false){
        $("#inqRemark").prop("disabled",true);
        $("#proNameEdit").prop("disabled",true);
        $("#potAmtVw").prop("disabled",true);
        $("#closurByVw").prop("disabled",true);
        $("#inquirytype").prop("disabled",true).selectpicker('refresh');
        $("#inquirysource").prop("disabled",true).selectpicker('refresh');
    }else{
        $("#inqRemark").prop("disabled",false);
        $("#proNameEdit").prop("disabled",false);
        $("#potAmtVw").prop("disabled",false);
        $("#closurByVw").prop("disabled",false);
        $("#inquirytype").prop("disabled",false).selectpicker('refresh');
        $("#inquirysource").prop("disabled",false).selectpicker('refresh');
    }
    
    $('#editMoreInfo').modal('show');
    addFocusId( 'proNameEdit' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function viewMoreInfo(){
    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        var divStruct = '<div class="row">';
        for(var j=0;j<localJson.data.customFieldDetail.length;j++){
            if(localJson.data.customFieldDetail[j]['tabType'] == "INQMASTER"){
                
                divStruct += '<div class="col-lg-6">';
                if(localJson.data.customFieldDetail[j].fieldType == "text"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="" />';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "number"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="number" min="0" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value=""/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "email"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="email" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value=""/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<textarea maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'"></textarea>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }

                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectpicker">';
                    divStruct += '<option value="-1">---'+localJson.data.customFieldDetail[j]['fieldName']+'---</option>';
                    for(var g=0;g<explodeValue.length;g++){
                        var classSeleted = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            classSeleted = "selected";
                        }
                        divStruct += '<option value="'+explodeValue[g]+'" classSeleted>'+explodeValue[g]+'</option>';
                    }
                    divStruct += "</select>";
                    $("#field"+(j+1)).selectpicker();
               
                }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="" class="form-control" value="'+getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]+'"  readOnly/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="radio" class="normal-date-picker" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
                    divStruct += '<div class="form-group checkboxer" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span></br>";
                    }else{
                        requiredClass = "";
                    }
                    
                    divStruct += requiredClass;
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
                }
                divStruct += '</div>'+
                '</div>';
                if(((j+1)%2) == 0){
                    divStruct += '</div><div class="row">';

                }   
            }
            
        }
        $("#customFieldDiv").html(divStruct);
        $("#customFieldDiv").show();
    }
    $("#inqRemark").val(getData[0]['inqRemark']);
    finalEditMoreInfoData = [];
    $('#editMoreInfo').modal('show');
}

function openAssigned(){

    if( checkAuth(4, 110, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;
    }
    
    if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Stat');
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = tmp_json.data[0].PK_USER_ID;
    var assignedToId = getData[0].inqAssignToId;

    // if ( userId == assignedToId ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase: "getInqAssignUser",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4, 110),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
       
        // console.log(postData); return false;
        commonAjax(INQURL, postData, openAssignedCallBack,CREATINGINQDATA);
        
    // }else{
    //     toastr.warning('You are not Authorized to Assign Inquiry');
    // }
    
}


function openAssignedCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        var statusArr = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(statusArr.data.statusListing,"statusCatName","INQUIRY");
        var currStatThrParty = "";

        var statusCatId = getData[0].statusId;
        var currInqStatPriorityId = getData[0].statusPriority;
        if(index != "-1"){
            var inqStatPrIdInd = findIndexByKeyValue(statusArr.data.statusListing[index].statusListing,"ID",statusCatId);
            if( inqStatPrIdInd != "-1" ){
                inqStatPrId = statusArr.data.statusListing[index].statusListing[inqStatPrIdInd].statusPriority;
            }

            var statPriInd = findIndexByKeyValue(statusArr.data.statusListing[index].statusListing,"thirdPartyFlag","1");
            if( statPriInd != "-1" ){
                var currStatThrParty = statusArr.data.statusListing[index].statusListing[statPriInd].statusPriority;
                var currStatThrPartyId = statusArr.data.statusListing[index].statusListing[statPriInd].ID;
                
                if( currStatThrParty != "-1" ){
                    if( parseInt( currStatThrParty ) == parseInt( currInqStatPriorityId ) ){
                        toastr.warning('Oops..., Not allowed to Change Assignment in this Stage of Inquiry');
                        return false;

                    }else if( parseInt( currStatThrParty ) < parseInt( currInqStatPriorityId ) ){
                        toastr.warning('Oops..., Not allowed to Change Assignment in this Stage of Inquiry');
                        return false;
                    
                    }
                }
            }
        }

        var newRow = '';
        for( var i=0; i<data.data.length; i++ ){
            newRow += '<option value="'+ data.data[i].userId +'">'+ data.data[i].userFullName +'</option>'
        }
        $('#assignedUserList').html( newRow );
        $('#assignedUserList').selectpicker('refresh');

        $('#changeAssigned').modal('show');

    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_INQ_ASSIGNMENT );
        else
            toastr.error(SERVERERROR);
    }
}

function saveProName(){

    var projectNm = $('#proNameEdit').val();

    $('#projectName').html(projectNm);

    $('#editProjectName').modal('hide');
}

function saveMoreInfo(){
    var inqRemark = $('#inqRemark').val();
    var inqName = $('#proNameEdit').val();
    var potAmtVw = $('#potAmtVw').val();
    var closurByVw = $('#closurByVw').val();
    var inqType = $('#inquirytype').val();
    var sourceOf = $('#inquirysource').val();

     if( inqName.length == 0 ){
        toastr.warning('Please Enter Inquiry Remarks');
        $('#proNameEdit').focus();
        return false;

    }else if( inqRemark.length == 0 ){
        toastr.warning('Please Enter Remarks');
        $('#inqRemark').focus();
        return false;

    }else if( potAmtVw == "" ){
        toastr.warning('Please Enter Potential Amount');
        $('#potAmtVw').focus();
        return false;

    }else if( closurByVw == "" ){
        toastr.warning('Please Select ClosureBy Date');
        $('#closurByVw').datepicker('show');
        return false;
    
    }else if( inqType == "-1" ){
        toastr.warning('Please Select Inquiry Type');
        addFocusId('inquirytype')
        return false;
    
    }else if( sourceOf == "-1" ){
        toastr.warning('Please Select Inquiry Source');
        addFocusId('inquirysource')
        return false;
    
    }

    var tempEditMoreInfoData = [];
    
    // START GETTING CUSTOM FILED AND IT DATA IF SET FOR INQUIRY
    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        for(var f=0;f<localJson.data.customFieldDetail.length;f++){
            if(localJson.data.customFieldDetail[f]['tabType'] == "INQMASTER"){
                var customFieldValue = "";
                if(localJson.data.customFieldDetail[f]['fieldType'] == "radio"){
                    //var customFieldValueTemp = 
                    customFieldValue = $('input[name=field'+(localJson.data.customFieldDetail[f].priorityLevel)+']:checked').val();
                }else if(localJson.data.customFieldDetail[f]['fieldType'] == "checkbox"){
                    var tempChck = '';
                    $('input[name="field'+(localJson.data.customFieldDetail[f].priorityLevel)+'"]:checked').each(function() {
                       // console.log(this.value);
                       if( tempChck.length == 0 ){
                            tempChck += $(this).val();
                       }else{
                            tempChck += ',' + $(this).val();
                       } 
                    });
                    if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && tempChck == "" ){
                        toastr.warning("Please Select "+ localJson.data.customFieldDetail[f].fieldName);
                        return false;
                    }else{
                        customFieldValue = tempChck; 
                    }
                } else{
                    if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && ( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "" || $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "-1" ) ){
                            
                        var msg = "Please Enter "+ localJson.data.customFieldDetail[f].fieldName;
                        if( localJson.data.customFieldDetail[f].fieldType == "dropdown" || localJson.data.customFieldDetail[f].fieldType == "date"){
                            msg = "Please Select "+ localJson.data.customFieldDetail[f].fieldName;
                        }

                        toastr.warning( msg );
                        $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                        return false;
                    }else{
                        if( localJson.data.customFieldDetail[f]['fieldType'] == "email" && $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() != "" && !validateEmail( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() ) ){
                            toastr.warning( "Please Enter Valid Email Id in "+ localJson.data.customFieldDetail[f].fieldName );
                            $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                            return false;
                        }
                        customFieldValue = $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val();
                        // customFieldValue = $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)+' input[type=text]').val();
                    }
                }
                tempEditMoreInfoData['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = customFieldValue;
                getData[0]['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = customFieldValue;
            }
        }
    }

    closurByVw = ddmmyyToMysql(closurByVw);

    tempEditMoreInfoData['inqRemark'] = inqRemark;
    tempEditMoreInfoData['projectName'] = inqName;
    tempEditMoreInfoData['potAmt'] = potAmtVw;
    tempEditMoreInfoData['closerBy'] = closurByVw;
    getData[0]['inqRemark'] = inqRemark;
    getData[0]['projectName'] = inqName;
    getData[0]['potAmt'] = potAmtVw;
    getData[0]['closerBy'] = closurByVw;
    getData[0]['inqType'] = inqType;
    getData[0]['sourceOf'] = sourceOf;
    finalEditMoreInfoData[0] = (tempEditMoreInfoData);
    var newInqName = ""
    
    if( inqName.length > 12 ){
      newInqName  = inqName.substring(0, 12) + '...';
    }else{
      newInqName  = inqName;
    }
    $('#projectName').html(newInqName);

    var inqTypeName = $('#inquirytype option:selected').text();
    var sourceOfName = $('#inquirysource option:selected').text();

    $('#inqRemarkOutSide').html( inqRemark );
    $('#inqTypeOutSide').html( inqTypeName );
    $('#inqSourceOutSide').html( sourceOfName );

    $('#editMoreInfo').modal('hide');
}

function checkAssignedIt(){

    if( getData[0].inqAssignToId == $('#assignedUserList').val() ){
        toastr.warning('Not allowed to assign same user');
        return false;
    }

    var assignee = $('#assignedUserList option:selected').text();
    
    $('#assLable').html('Do You wants to assign all the Activity to '+ assignee +'..!');
    $('#changeAssigned').modal('hide');
    $('#assActModal').modal('show');
}
function AssignedIt(){

    if( checkAuth(4, 15, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    if( getData[0].inqAssignToId == $('#assignedUserList').val() ){
        toastr.warning('Not allowed to assign same user');
        return false;
    }

    var transferFlag = ($('#assAct').prop('checked')) ? "1" : "0";

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "reAssignInquiry",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 110),
        inqAllocationUserId: $('#assignedUserList').val(),
        inqId: localStorage.POTGclickInqId,
        flagToTransferAct: transferFlag,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
   
    // console.log(postData); return false;
    commonAjax(INQURL, postData,AssignedItCallBack,CREATINGINQDATA);
}


function AssignedItCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        // location.reload();
        navigateToInquiry();
        $('#assignedTo').html($('#assignedUserList option:selected').text());
        $('#changeAssigned').modal('hide');
        $('#assActModal').modal('hide');

        GBL_ALLOC_LOADED = 0;

        getInqLog();    // CALL AN INQUIRY LOG API

    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_INQ_ASSIGNMENT );
        else
            toastr.error(SERVERERROR);
    }
}

function autoCompleteForGroup(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#itemGroupText').attr('groupId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getGroupListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(13, 50),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMSGROUP= [];
                if (data.status != "No Item Group Found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.PK_ITEM_GROUP_ID,
                            name: data.GROUP_NAME,
                            data: data.GROUP_NAME,
                            clientId: data.FK_CLIENT_ID
                        }
                        itemGroupJson.push(tempJson);
                        ITEMSGROUP.push(tempJson);
                    });

                    return ITEMSGROUP;
                }
                console.log(data);
            }
        }
    });
}

function saveItemGroupDetail(){
    
}

function navigateToProposalInq(){
    
    if (checkAuth(15, 58) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var userId = tmp_json.data[0].PK_USER_ID;
        var assignedToId = getData[0].inqAssignToId;
        var authToUserId = getData[0].authToThisUser;

        if ( userId == assignedToId || userId == authToUserId ){

            var inqInfo = {
                projectName : getData[0].projectName,
                inqId : getData[0].clientLevelInqId,
                inqDate : getData[0].inqDate,
                custName : getData[0].custDetail.fName + ' '+ getData[0].custDetail.lName,
                custMobile : getData[0].custDetail.custMobile,
                custEmail : getData[0].custDetail.custEmail,
                custAddress : getData[0].custDetail.custAddress,
                contactPerson : getData[0].custDetail.cpName,
                contactMobile : getData[0].custDetail.cpMobile,
                proCatId : getData[0].statusId,
            }

            localStorage.POTGinqInfoForProposal = JSON.stringify( inqInfo );

            window.location.href = "proposal.html";
        }else{
            toastr.warning('You are not Authorized for this Activity');
        }
    }
    
}

var attachmetData = [];

function openAttachment(){

    if( GBL_ATTACH_LOADED == "1" ){
        return false;
    }

    var currStatusId = getData[0].statusId;
    
    var proStatusData = finalStatusArr;
    var index = findIndexByKeyValue(proStatusData,"ID",currStatusId);
    var tempFlag = "";
      
    var index = findIndexByKeyValue( proStatusData , "thirdPartyFlag" , "1" );
    $('#submitInqAttachBtn').removeAttr('disabled');
    if( index != "-1" ){
        if( parseInt( currStatusId ) == parseInt( proStatusData[index].ID ) || parseInt( getData[0].statusPriority ) > parseInt( proStatusData[index].statusPriority ) ){
            $('#submitInqAttachBtn').attr('disabled',true);
            return false;
        }
    }
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "attachmentListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        inqId: localStorage.POTGclickInqId,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, sendAttachmentCallBack,CREATINGINQDATA);

    function sendAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            attachmetData = data.data;
            GBL_ATTACH_LOADED = "1";
            renderAttachment();

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ATTACH );
            else
                toastr.error(SERVERERROR);
        }
    }
    $('#attachmentAdd').modal('show');
 
    addFocusId( 'attachmentTitle' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    // assignAttachementEvent();
}

function renderAttachment(){
    var attachLi = '<div class="row">';
    
    for( var i=0; i<attachmetData.length; i++ ){
        var iconNm = '';
        if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "pdf" ){
            iconNm = 'file-pdf-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "doc" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "docx"  ){
            iconNm = 'file-word-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "txt" ){
            iconNm = 'file-text-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "csv" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "xls" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "xlsx" ){
            iconNm = 'file-excel-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "jpg" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "jpeg" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "png" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "gif" ){
            iconNm = 'file-image-o';
        }
        var title = attachmetData[i].attachTitle;
        if( attachmetData[i].attachTitle == "" ){
            title = attachmetData[i].fileName;
        }
        
        attachLi += '<div class="col-md-3 col-sm-6 col-xs-6">'+
                        '<div class="list-content">'+
                            '<a style="text-align:center" href="javascript:void(0)" data-link="'+ attachmetData[i].fileLink +'" onclick="openLinkInNewTab(this);">'+
                                '<i class="fa fa-'+ iconNm +' iconStyle"></i>'+
                                '<a href="javascript:void(0)"  >'+
                                    '<i style="margin-top: 5px;" class="fa fa-trash-o pull-right" attchId="'+ attachmetData[i].fileId +'" onclick="deleteAttachment(this)"></i>'+
                                    '<span>'+ title +'</span>'+
                                '</a>'+
                            '</a>'+                            
                        '</div>'+
                    '</div>';
    }
    attachLi += '</div>';

    $('#AttachmentListing').html( attachLi );
}

function submitInqAttach(){
    // grab your file object from a file input
    // $('#attachmentName').change(function () {
        if( checkAuth(4,15) == -1 ){
            toastr.error(NOAUTH);
            return false;
        }
        
        if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
            toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Stat');
            return false;
        }

        var attachTitle = $('#attachmentTitle').val();
        var attachFile = $('#attachmentName').val()
        if( attachTitle == "" ){
            toastr.warning('Please Enter Title');
            $('#attachmentTitle').focus();
            return false;

        }else if( attachFile == "" ){
            toastr.warning('Please Select File');
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "attachmentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7, 23),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            inqId : localStorage.POTGclickInqId,
            attachTitle :attachTitle,
            emailAdress : ""
        };
        $("#postData").val( JSON.stringify(temp_json));

        $("#fileupload").off().on('submit',(function(e) {
            
            addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",       // Type of request to be send, called as method
                data:  new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData:false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function(data)// A function to be called if request succeeds
                {
                    data = JSON.parse(data);
                    console.log(data);

                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                            attachTitle: data.data.attachTitle,
                        }

                        attachmetData.push( tempData );
                        GBL_ATTACH_LOADED = "0";
                        renderAttachment();
                        
                        getInqLog();    // CALL AN INQUIRY LOG API

                    }else{
                        toastr.warning(data.status);

                    } 
                },
                error: function (jqXHR, errdata, errorThrown) {
                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    toastr.warning( JSON.parse( jqXHR.responseText ).status );
                    log("error");
                }
            });
        }));
        $("#fileupload").submit();
        $('#attachmentName').val('');
        $('#attachmentTitle').val('');
    // });
}

function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attchId');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'attachmentListingDelete',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 16),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId: localStorage.POTGclickInqId,
        attachId : attachmentId
    };
    commonAjax(INQURL1, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var index = findIndexByKeyValue( attachmetData , "fileId" , attachmentId );
            
            attachmetData.splice( index , 1 );

            renderAttachment();

            getInqLog();    // CALL AN INQUIRY LOG API

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

function openLinkInNewTab(linkData){
    var link = $(linkData).attr("data-link");
    if(link==""){
        toastr.warning("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}



function openModalActivity(){
    
    $('#txtActTitle').val('Call');      
    if( checkAuth(4, 15, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;
    }
    
    if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Stat');
        return false;
    }

    if( actModeHideShow() == "-1" ){  //  HIDE SHOW ACTIVITY MODE ACCORDING TO ROLE AUTH RIGHTS
        toastr.warning( NOACTMODERIGHTS );
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = tmp_json.data[0].PK_USER_ID;
    var assignedToId = getData[0].inqAssignToId;

    // if ( userId != assignedToId ){
    //     toastr.warning('You are not Authorized for this Activity');
    //     return false;
    // }

    var currStatusId = getData[0].statusId;
    
    var proStatusData = finalStatusArr;
    var index = findIndexByKeyValue(proStatusData,"ID",currStatusId);
    var tempFlag = "";
      
    var index = findIndexByKeyValue( proStatusData , "thirdPartyFlag" , "1" );
    // if( index != "-1" ){
    //     if( parseInt( currStatusId ) == parseInt( proStatusData[index].ID ) || parseInt( getData[0].statusPriority ) > parseInt( proStatusData[index].statusPriority ) ){
    //         toastr.error('Inquiry cant be update when it is in won state');
    //         return false;
    //     }
    // }
    
//    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//    // Followup Listing Todays, Delayed, Upcomming
//    var postData = {
//        requestCase: 'getInqListingByUser',
//        clientId: tmp_json.data[0].FK_CLIENT_ID,
//        userId: tmp_json.data[0].PK_USER_ID,
//        orgId: checkAuth(4,14),
//        custId: tmp_json.data[0].PK_USER_ID,
//        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
//    }
//
//    commonAjax(COMMONURL, postData, inquiryDataCallBack,INQUIRYDETAILGET);
//
//    function inquiryDataCallBack(flag,data) {
//
//        if(data.status == "Success" && flag){
            
//            var inqLogOptions = '<option value=""> --Please Select Inquiry -- </option>';
//            for( var i=0; i<data.data.length; i++ ){
//
//                inqLogOptions += '<option custId="'+ data.data[i].custId +'" value="'+ data.data[i].inqId +'">'+ data.data[i].inqId +'</option>';
//            }
//
//            $('#inqListAct').html( inqLogOptions );
//            $('#inqListAct').selectpicker();
            
            var customerUserGrp = {
                requestCase: "getUserCustomerGroup",
                userId: localStorage.indeCampusActivityAppUserId,
                clientId: localStorage.indeCampusActivityAppClientId,
                orgId: checkAuth(2,6),
            };
            
            commonAjax(ACTURL, customerUserGrp, callBackGetUserCustomerGroupData, "Please wait. Getting customer and user group.");
            
            function callBackGetUserCustomerGroupData(flag,data){
                if(data.status == "Success" && flag){
                    
                    var custGrpArr = data.data.customerGroupList;
                    var userGrpArr = data.data.userGroupList;
                    var assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';

                    if (custGrpArr)
                    {
                        for (var i = 0; i < custGrpArr.length; i++)
                        {
                            custGrpArr[i].label = custGrpArr[i].GROUP_NAME;
                            custGrpArr[i].text = custGrpArr[i].GROUP_NAME;
                            custGrpArr[i].value = custGrpArr[i].PK_GROUP_ID;
                            custGrpArr[i].grpid = custGrpArr[i].PK_GROUP_ID;
                        }


                    }

                    if (userGrpArr)
                    {

                        for (var i = 0; i < userGrpArr.length; i++)
                        {
                            userGrpArr[i].label = userGrpArr[i].GROUP_NAME;
                            userGrpArr[i].text = userGrpArr[i].GROUP_NAME;
                            userGrpArr[i].value = userGrpArr[i].PK_GROUP_ID;
                            userGrpArr[i].grpid = userGrpArr[i].PK_GROUP_ID;

                            var grpid = userGrpArr[i].value;
                            var grpName = userGrpArr[i].label;

                            assignTypeGroupOptionHtml += '<option value="' + grpid + '">' + grpName + '</option>';
                        }


                    }
                    
                    localStorage.POTGactivityAppUserGrpJson = JSON.stringify(userGrpArr);
                    localStorage.POTGactivityAppCustGrpJson = JSON.stringify(custGrpArr);
                    localStorage.POTGactivityAppAssignTypeGroupOptionHtml = assignTypeGroupOptionHtml;
                    
                    $('#ddActAssignedTo').selectpicker();
                    
                    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                    //SET GROUP LOAD
                    var userDetailJson ={
                        requestCase : "userListingActivityModule",
                        clientId: tmp_json.data[0].FK_CLIENT_ID,
                        orgId: checkAuth(2,6),
                        userId: tmp_json.data[0].PK_USER_ID,
                        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
                    };
                    
                    commonAjax(COMMONURL, userDetailJson, callBackGetUserUserListData, "Please wait. Getting User Detail.");
                    
                    function callBackGetUserUserListData(flag, data){
                        if(data.status == "Success" && flag){
                            var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
                            var userListArr = data.data;

                            if (userListArr){
                                for (var i = 0; i < userListArr.length; i++){
                                    userListArr[i].label = userListArr[i].userName;
                                    userListArr[i].text = userListArr[i].userName;
                                    userListArr[i].value = userListArr[i].userId;
                                    userListArr[i].custId = userListArr[i].userId;

                                    var userId = userListArr[i].value;
                                    var userName = userListArr[i].label;
                                        assignTypeUserOptionHtml += '<option value="' + userId + '">' + userName + '</option>';
                                }
                            }
                            localStorage.POTGactivityAppUserListJson = JSON.stringify(userListArr);
                            localStorage.POTGactivityAppAssignTypeUserOptionHtml = assignTypeUserOptionHtml;
                            $("#ddActAssignedTo").html( assignTypeUserOptionHtml ).selectpicker('refresh');

                            $("#ddActAssignedTo").val( getData[0].inqAssignToId ).selectpicker('refresh');

                        }else{
                            if (flag)
                                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
                            else
                                toastr.error(SERVERERROR);
                        }
                    }
                    
                }else{
                    if (flag)
                        displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );
                    else
                        toastr.error(SERVERERROR);
                }
            }
//        }else{
//            if (flag)
//                toastr.error(data.status);
//            else
//                toastr.error(SERVERERROR);
//        }
    //}
    $('#radio-05').prop('checked',true);
    $('#creatactivity').modal('show');
 
    addFocusId( 'txtActTitle' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}


$("input[type=radio][name='rdoIsUserGroup']").off().on('change', (function (e) {
    if (this.value == '0') {
        //user //single
        $("#ddActAssignedTo").html(localStorage.POTGactivityAppAssignTypeUserOptionHtml).selectpicker('refresh');

    } else if (this.value == '1') {
        //group
        $("#ddActAssignedTo").html(localStorage.POTGactivityAppAssignTypeGroupOptionHtml).selectpicker('refresh');
    }

}));

function createActivity(){
    
    var actTitle = $("#txtActTitle").val();
    var actType = $('#actTypeListSch').val();
    var actDesc = $("#txtActDesc").val();
    var actMode = $('input[type=radio][name="rdoActMode"]').filter(":checked").val();
    var actDueDate = $("#txtActDueDate").val();
    var isUserGroup = $('input[type=radio][name="rdoIsUserGroup"]').filter(":checked").val();
    var isCustomerGroup = "";
    var assigneTo = $("#ddActAssignedTo").val();
    var customerType = "";
    var inqIdAct = getData[0].inqId;
    var custIdAct = getData[0].custId;
    var actDueTime = "";
    var remindFlag = $('#reminderActChk').is(':checked');
    var reminderDate = $('#reminderDate').val();
    var reminderTime = "";

    var actNormEndDate = $('#txtActNormEndDate').val();
    var actNormEndTime = "";

    if (!actTitle)
    {
        toastr.warning("Please Enter Title");
        $("#txtActTitle").focus();
        return false;
    }
    else if (!actDesc)
    {
        toastr.warning("Please Enter Description");
        $("#txtActDesc").focus();
        return false;

    }
    else if (!actMode)
    {
        toastr.warning("Please Select Mode");
        $('input[type=radio][name="rdoActModeForEditAct"]').focus();
        return false;

    }
    else if (!actDueDate)
    {
        toastr.warning("Please Select Due Date/Time");
        $("#txtActDueDate").focus();
        return false;

    // }
    // else if (!actDueTime)
    // {
    //     toastr.warning("Please Select Due Time");
    //     $("#dueTime").focus();
    //     return false;
    
    // }else if( !validateDateTimeFormat(actDueTime) ){
    //     toastr.warning('Please Select Proper Due Time eg:HH:MM (AM/PM) like 10:00 AM');
    //     return false;

    }else if (!actNormEndDate)
    {
        toastr.warning("Please Select End Date/Time");
        $('#txtActNormEndDate').focus();
        return false;

    // }
    // else if (!actNormEndTime)
    // {
    //     toastr.warning("Please Select End Time");
    //     $('#endNormTime').focus();
    //     return false;

    // }else if( !validateDateTimeFormat(actNormEndTime) ){
    //     toastr.warning('Please Select Proper End Time eg:HH:MM (AM/PM) like 10:00 AM');
    //     return false;
    }
    else if (!isUserGroup)
    {
        toastr.warning("Please Select Assign Type");
        $('input[type=radio][name="rdoIsUserGroup"]').focus();
        return false;

    }
    else if (!assigneTo || assigneTo == "-1")
    {   
        toastr.warning("Please Assign the Activity");
        $("#ddActAssignedTo").focus();
        return false;
    }else{
        //console.log("Create activity validation passed");

        actDueDate = (($('#txtActDueDate').val() ).split('|')[0]).trim();
        actDueTime = (($('#txtActDueDate').val() ).split('|')[1]).trim();

        actNormEndDate = (($('#txtActNormEndDate').val() ).split('|')[0]).trim();
        actNormEndTime = (($('#txtActNormEndDate').val() ).split('|')[1]).trim();

        if ( remindFlag && !reminderTime ){
            reminderDate = (($('#reminderDate').val() ).split('|')[0]).trim();
            reminderTime = (($('#reminderDate').val() ).split('|')[1]).trim();
        }
        
        var dueStart = new Date( ddmmyyToMysql( actDueDate ) +' '+ actDueTime );
        var currTime = new Date( );
        var remindStart = new Date( ddmmyyToMysql( reminderDate ) +' '+ reminderTime );

        var actEnd = new Date( ddmmyyToMysql( actNormEndDate ) +' '+ actNormEndTime );

        if( +dueStart < +currTime ){
            toastr.warning("Due Date/Time should not be less than Current Date/Time");
            $('#dueDateSmart').datetimepicker('show');
            return false;

        }else if( +actEnd < +dueStart ){
            
            toastr.warning("End Date/Time should not be less than Due Date/Time");
            $('#txtActNormEndDate').datetimepicker('show');
            return false;

        }

        if (remindFlag){ 
            
            if( +dueStart < +currTime ){
                toastr.warning("Due Date/Time should not be less than Current Date/Time");
                $('#dueDateSmart').datetimepicker('show');
                return false;

            }else if( +dueStart < +remindStart ){
                toastr.warning("Reminder Date/Time should not be greater than Activity Due Date/Time");
                $('#dueDateSmart').datetimepicker('show');
                return false;

            }else if( +remindStart < +currTime ){ //dateActEndDate dateActStartDate
                toastr.warning("Reminder Date/Time should not be less Current Date/Time");
                $('#reminderDate').datetimepicker('show');
                return false;

            } 
            // else if( !validateDateTimeFormat(reminderTime) ){
            //     toastr.warning('Please Select Proper End Time eg:HH:MM (AM/PM) like 10:00 AM');
            //     return false;
            // }
        }

        var remindTimeSend = ddmmyyToMysql( reminderDate ) +'|'+reminderTime;
        if( !remindFlag ){
            remindTimeSend = "";
        }

        /* generate postData to submit activity START */
        var requestCase = "createActivity";
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase: requestCase,
            actTitle: actTitle,
            actDesc: actDesc,
            actMode: actMode,
            orgId: checkAuth(2,5),
            actDueDate: ddmmyyToMysql(actDueDate,'yyyy-MM-dd'),
            isUserGroup: isUserGroup,
            isCustomerGroup: isCustomerGroup,
            assigneTo: assigneTo,
            customerId: custIdAct,
            refId: "",
            actId: "",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            actType : actType, // activty type from role auth
            custType : "SINGLE", // type of customer single or group
            inqId : inqIdAct,
            custId : custIdAct,
            contactId : getData[0].custDetail.contPerId,
            actDueTime : actDueTime,
            reminderTime : remindTimeSend,
            actEndDate : ddmmyyToMysql( actNormEndDate ),
            actEndTime : actNormEndTime,
            actRefType : "INQ",
        };
        /* generate postData to submit activity END */
        $("#postDataActivty").val(JSON.stringify(postData));
        // return false;
        $("#formCreateUpdateAct").submit();
    }
}

$("#formCreateUpdateAct").off().on('submit',(function (e) {
    e.preventDefault(); 
    addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
    $.ajax({
        url: ACTURL, // Url to which the request is send
        type: "POST", // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
        contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
        cache: false, // To unable request pages to be cached
        processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
        headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
        beforeSend: function () {
            //alert(bfsendMsg);
            //$("#msgDiv").html(bfsendMsg);
            addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            $("#ajaxloader").removeClass("hideajaxLoader");

        },
        success: function (data)// A function to be called if request succeeds
        {
            data = JSON.parse(data);
            console.log(data);
            addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
            if (data.status == SCS) {

                var msg = 'Your Activity is successfully created.';

                // if( data.data != "" ){

                //     msg = 'Your Activity is been Updated , but attached '+data.data;
                // }

                toastr.success( msg );
                $("#creatactivity").modal('hide');
                GBL_ACT_LOADED = "0";
                getFollowupListing();
                resetActPop();
                
                getInqLog();    // CALL AN INQUIRY LOG API

            } else {
                toastr.warning(data.status);

            }
            $("#ajaxloader").addClass("hideajaxLoader");
        },
        error: function (jqXHR, errdata, errorThrown) {
            addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
            log("error");
            $("#ajaxloader").addClass("hideajaxLoader");
        }
    });
}));


function fieldsChange(selectedValue){
    var itemSelectionDiv = "";
    var className = "display: none";
    if(selectedValue == 1){
        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        for(var i=0;i<roleAuth.data.clientConfig.length;i++){
            if(roleAuth.data.clientConfig[i].FLAG == "1" && roleAuth.data.clientConfig[i].TYPE_OF_CONFIG == "INQGROUP"){
                className = "display: block";
            }
        } 
       
       itemSelectionDiv += '<div class="col-lg-2">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Type</label>'+
                                            '<select id="itemType" onchange="fieldsChange(this.value)" data-width="100%">'+
                                                '<option value="1" class="fa fa-cube">&nbsp;&nbsp;I</option>'+
                                                '<option value="2" class="fa fa-file-text-o">&nbsp;&nbsp;T</option>'+
                                                '<option value="3" class="fa fa-sitemap">&nbsp;&nbsp;B</option>'+
                                            '</select>'+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-2">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-cube"></i> '+
                                            '<label class="control-label">Name</label>'+
                                            '<input type="text" id="itemName" class="form-control" autocomplete="off" placeholder="Search Item (eg. Pencil)"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-2">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-rupee"></i> '+
                                            '<label class="control-label"> Price </label>'+
                                            '<input type="text" id="itemPrice" class="form-control onlyNumberWithDec" autocomplete="off" placeholder="Price (eg. 500)" />'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-2" id="itemGroup" style="'+className+'">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-cubes"></i> '+
                                            '<label class="control-label">Group </label>'+
                                            '<input type="text" id="itemGroupText" class="form-control" autocomplete="off"  placeholder="Search Group (eg. Groups)"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-1">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-hand-peace-o"></i> '+
                                            '<label class="control-label">Qty </label>'+
                                            '<input type="text" id="itemQty" value="1" class="onlyNumber form-control"  placeholder="Quantity (eg. 1)"/>'+
                                        '</div>'+
                                    '</div>'+  
                                    '<div class="col-lg-3">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-file-text-o"></i> '+
                                            '<label class="control-label">Remark </label>'+
                                            '<textarea id="itemRemark" name="itemRemark" class="form-control" placeholder="Enter Remark"></textarea>'+
                                        '</div>'+
                                    '</div>';                                 
                                    
    }else if(selectedValue == 2){
        itemSelectionDiv += '<div class="col-lg-2">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Type</label>'+
                                            '<select id="itemType" onchange="fieldsChange(this.value)" data-width="100%">'+
                                                '<option value="1" class="fa fa-cube">&nbsp;&nbsp;I</option>'+
                                                '<option value="2" class="fa fa-file-text-o">&nbsp;&nbsp;T</option>'+
                                                '<option value="3" class="fa fa-sitemap">&nbsp;&nbsp;B</option>'+
                                            '</select>'+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-10">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label"><i class="fa fa-file-text-o"></i> Section Name</label>'+
                                            '<input type="text" id="itemHeaderText" placeholder="Section (eg. xyz)" class="form-control" autocomplete="off"/>'+
                                        '</div>'+
                                    '</div>';
                                    
    }else{
        itemSelectionDiv += '<div class="col-lg-2">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Type</label>'+
                                            '<select id="itemType" onchange="fieldsChange(this.value)" data-width="100%">'+
                                                '<option value="1" class="fa fa-cube">&nbsp;&nbsp;I</option>'+
                                                '<option value="2" class="fa fa-file-text-o">&nbsp;&nbsp;T</option>'+
                                                '<option value="3" class="fa fa-sitemap">&nbsp;&nbsp;B</option>'+
                                            '</select>'+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-10">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label"><i class="fa fa-sitemap"></i> BOM Search</label>'+
                                            '<input type="text" id="itemName" class="form-control" placeholder="Search BOM (eg. Computer)" autocomplete="off"/>'+
                                        '</div>'+
                                    '</div>';

    }
    
    $("#itemSelectionDiv").html(itemSelectionDiv);
    if( selectedValue == 3 ){
        AutoCompleteForItem("itemName", openBOMDetail);                    
    }else{
        AutoCompleteForItem("itemName", saveItemDetail);
    }
    
    if(className == "display: block"){
        autoCompleteForGroup("itemGroupText", saveItemGroupDetail);
        AutoCompleteForItem("itemName", saveItemDetail);
    }
    $("#itemType").val(selectedValue); 
    $("#itemType").selectpicker(); 
}

function showCancelRemark(statusCat,statustext){
    var statusArr = JSON.parse(localStorage.indeCampusRoleAuth);
    //var remarksArr = statusArr.data.cancelRemark;
    $("#cancelRemark").attr("statusId",statusCat);
    $("#cancelRemark").attr("statusName",statustext);
    setOption("0", "cancelRemarkId", statusArr.data.cancelRemark, "-- Select Cancel Remark --");
    $("#cancelRemarkId").selectpicker("refresh");
    $("#remarkOnstatus").modal("show");
    addFocusId( 'cancelRemarkId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function saveCancelRemarkAndStatus(){

    GBL_CANCEL_FLAG = "1";
    var statusId = $("#cancelRemark").attr("statusId");
    var statusName = $("#cancelRemark").attr("statusName");
    var cancelRemarkId  = $("#cancelRemarkId").val();
    var cancelText = $('#cancelRemarkId option:selected').text();
    // $('#statNameList').html(statusName);
    localStorage.POTGinqStatus = statusId;
    var tempData = {
        cancelRemarkId : cancelRemarkId,
        cancelText : cancelText,
        statusId : statusId,
        statusName : statusName
    };
    cancelRemarkData.push(tempData);
    $("#remarkOnstatus").modal("hide");
    editInq(1);
    updateInquiry();
}


function openBOMDetail(){
    console.log(bomJson);
    var startHtml = "<table id='BomListTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>Sr No.</th>"+
                            "<th>Item Name</th>"+
                            "<th>Item Price</th>"+
                            "<th>Group Name</th>"+
                            "<th>UOM Name</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";
    var inqRow = '';
    var bomListData = '';

    for( var i=0; i<bomJson.length; i++ ){
        bomListData += '<tr>'+
                            '<td>'+ (i+1) +'</td>'+
                            '<td>'+ bomJson[i].itemName +'</td>'+
                            '<td>'+ bomJson[i].itemPrice +'</td>'+
                            '<td>'+ bomJson[i].groupName +'</td>'+
                            '<td>'+ bomJson[i].uomName +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" onclick="DeleteBomItem(this)" item-Id="'+ bomJson[i].itemId +'" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';   
    }
    $('#bomListData').html( startHtml + bomListData + endHtml);
    $('#BomListTable').DataTable({ "stateSave": true });
    $('#addBOM').modal('show');
}

function DeleteBomItem(context){

    var itemId = $(context).attr('item-Id');
    var index = findIndexByKeyValue( bomJson , 'itemId' , itemId );

    if( index != '-1' ){
        bomJson.splice(index,1);
    }

    openBOMDetail();
}

function saveBomDetails(){

    // CHECK VALIDATION
    for( var i=0; i<inqTransData.length; i++ ){
        if(inqTransData[i].typeOfItem == "ITEM"){
            for( var j=0; j<bomJson.length; j++ ){
                var newIndex = findIndexByKeyValue(inqTransData,"itemId",bomJson[j].itemId);
                if(newIndex != "-1"){
                    toastr.error('Some/All sItem of BOM is already available in the cart');
                    $('#itemName').val('');
                    $('#addBOM').modal('hide');
                    return false;
                }
            }
        }
    }

    var sequenceId = (inqTransData.length+1);
    // var  tempData = {
    //     cancleFlag : "1",
    //     itemId : 0,
    //     itemName : $('#itemName').val(),
    //     groupName : "",
    //     itemUOM : "",
    //     itemQty : "",
    //     eventStatus : "",
    //     itemPrice : "",
    //     itemBasePrice : "",
    //     itemType : "SECTION",
    //     typeOfItem : "SECTION",
    //     itemRemark : "",
    //     itemCat : 2, // 1 = item 2 - header text
    //     sequenceId : sequenceId 
    // }
    // inqTransData.push(tempData);
    var  tempData = {
        cancleFlag : "1",
        itemId : $('#itemName').attr('itemid'),
        bomId : $('#itemName').attr('itemid'),
        itemName : $('#itemName').val(),
        groupName : "",
        groupId : "",
        itemQty : "",
        itemStatus : "",
        eventStatus : "",
        itemPrice : 0,
        itemBasePrice : 0,
        itemType : "BOM",
        typeOfItem : "BOM",
        itemRemark : "",
        itemCat : 3, // 1 = item 2 - header text 3 - BOM
        sequenceId : sequenceId 
    }
    inqTransData.push(tempData);

    for( var i=0; i<bomJson.length; i++ ){
        sequenceId = (inqTransData.length+1);
        var  tempData = {
            cancleFlag : "1",
            itemId : bomJson[i].itemId,
            bomId : $('#itemName').attr('itemid'),
            itemName : bomJson[i].itemName,
            groupName : bomJson[i].groupName,
            groupId : bomJson[i].groupId,
            itemUOM : bomJson[i].uomName,
            itemQty : 1,
            itemStatus : 'Pending',
            eventStatus : "",
            itemPrice : (Number( bomJson[i].itemPrice * 1)).toFixed(2),
            itemBasePrice : bomJson[i].itemPrice,
            itemType : "ITEM",
            typeOfItem : "ITEM",
            itemRemark : '',
            itemCat : 1, // 1 = item 2 - header text
            sequenceId : sequenceId 
        }
        inqTransData.push( tempData );
    } 
    addRowTable( inqTransData );
    $('#itemName').val('');
    $('#addBOM').modal('hide');
    $('#AddItem').modal('hide');
}

function resetActPop(){
    $('#txtActTitle').val('');
    $('#txtActDesc').val('');
    $('#txtActDueDate').val('');
    $('#actAttachment').val('');
    $('#ddActAssignedTo').val('-1').selectpicker('refresh');
    $('#radio-05').attr("checked", "checked");
    $('#radio-13').attr("checked", "checked");
}

function openAllocationPop(){

    if( GBL_ALLOC_LOADED == "1" ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInquiryAssignmentReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        selectedUserId : "-1",
        inqId : getData[0].inqId,
        inqFlag : "1",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    } 
    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryListCallback(flag,data){

    if (data.status == "Success" && flag) {
        
        GBL_ALLOC_LOADED = "1";
        var reportData = data.data;
        var index = findIndexByKeyValue( reportData , 'inqId' , getData[0].inqId );
        if( index == "-1" ){
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_ASSIGNMENT );
            return false;
        }

        var inqData = reportData[index].allocationHistroy;
        // GBL_INQ_LOG_ARR = inqData;  // TEMPORARY ADDED
        if( inqData != "No record found" ){
            var listHeader = '<ul class="timeline2" id="allocationTimeline"><li class="tl-header"><div class="btn btn-info tl-content padder b-a"><div><i class="fa fa-anchor" aria-hidden="true"></i>  '+reportData[index].inqCreatedBy+' </div><div class="customInqActListing">Created On : '+reportData[index].inqCreatedOn+'</div></li>';
            var listMid = '';
            var listFooter = '<li class="tl-header"><div class="btn btn-sm btn-primary btn-rounded">Complete</div></li></ul>';
            
            for( var i=0; i<inqData.length; i++ ){
               var currentH = "";
               var lrClass = "";
               var activeClass = "transfer";
               var arrorwPost = "left";
               var activeOwner = '<i class="fa fa-share" aria-hidden="true"></i>';
                if(inqData.length == (i+1)){
                    currentH = "open";
                    activeClass = "active";
                    activeOwner = '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
                }
                
                if((i+1)%2 == 0){
                    lrClass = 'timeline-inverted';
                    arrorwPost = 'right';
                }
                listMid += '<li class="tl-item '+lrClass+'"><div class="tl-wrap '+currentH+' '+activeClass+'"><div class="tl-content panel-1 padder b-a"><span class="arrow '+arrorwPost+' pull-up"></span><div>'+activeOwner+'&nbsp;'+inqData[i]['inqAssignTo']+' </div>';
                listMid += '<div class="customInqActListing">Assign On : '+inqData[i]['inqAssignToOn']+'</div>';
                listMid += '<div class="customInqActListing">Assigned By : '+inqData[i]['inqAssignBY']+'</div>';
                //listMid += '<div class="customInqActListing">Assigned On : '+inqData[i]['inqAssignByOn']+'</div>';
                listMid += '</div></li>';
            }

            $('#inqAllocationData').html(listHeader + listMid + listFooter);
            $('#assignHistoryModal').modal('show');
        }
        else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_ASSIGNMENT );
        }

    } else {        
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_ASSIGNMENT );
        }else{
            toastr.error(SERVERERROR);
        }            
    }
}


function openAssignHist(context){

    var inqId = $(context).attr('inqId'); 
    
    var index = findIndexByKeyValue( reportData , "inqId" , inqId );
    if( index == "-1" ){
        toastr.warning('No record found');
        return false;
    }

    var inqData = reportData[index].allocationHistroy;
    
    if( inqData != "No record found" ){
        var listHeader = '<ul class="timeline2" id="allocationTimeline"><li class="tl-header"><div class="btn btn-info tl-content padder b-a"><div><i class="fa fa-anchor" aria-hidden="true"></i>  '+reportData[index].inqCreatedBy+' </div><div class="customInqActListing">Created On : '+reportData[index].inqCreatedOn+'</div></li>';
        var listMid = '';
        var listFooter = '<li class="tl-header"><div class="btn btn-sm btn-primary btn-rounded">Complete</div></li></ul>';
        
        for( var i=0; i<inqData.length; i++ ){
           var currentH = "";
           var lrClass = "";
           var activeClass = "transfer";
           var arrorwPost = "left";
           var activeOwner = '<i class="fa fa-share" aria-hidden="true"></i>';
            if(inqData.length == (i+1)){
                currentH = "open";
                activeClass = "active";
                activeOwner = '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
            }
            
            if((i+1)%2 == 0){
                lrClass = 'timeline-inverted';
                arrorwPost = 'right';
            }
            listMid += '<li class="tl-item '+lrClass+'"><div class="tl-wrap '+currentH+' '+activeClass+'"><div class="tl-content panel-1 padder b-a"><span class="arrow '+arrorwPost+' pull-up"></span><div>'+activeOwner+'&nbsp;'+inqData[i]['inqAssignTo']+' </div>';
            listMid += '<div class="customInqActListing">Assign On : '+inqData[i]['inqAssignToOn']+'</div>';
            listMid += '<div class="customInqActListing">Assigned By : '+inqData[i]['inqAssignBY']+'</div>';
            //listMid += '<div class="customInqActListing">Assigned On : '+inqData[i]['inqAssignByOn']+'</div>';
            listMid += '</div></li>';
        }

        $('#inqAllocationData').html(listHeader + listMid + listFooter);
        $('#assignHistoryModal').modal('show');
    }
    else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_INQ_ASSIGNMENT );
    }
    
}

function showVisitingCard(){
    window.open(getData[0].snapShotLink);
}

function changePOT(){

    if( potFlag ){
        $('#existingPot').html( getData[0].potAmt );
        $('#newPotInput').val( newPotAmt );
        $('#updatePotAmt').modal('show');
        addFocusId( 'newPotInput' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

function openCartData(){

    
    var newPotInputVal = $('#newPotInput').val();
    if( newPotInputVal == ""  ){
        toastr.warning('Please Enter POT Amount');
        $('#newPotInput').focus();
        return false;
    }
    getData[0].potAmt = newPotInputVal;

    renderCartTransData();
}


function renderCartTransData(){

    var itemList = "";
    newCartData = inqTransData;
    
    inqTransData.forEach( function( record , index ){
        
        if( record.typeOfItem == "ITEM" ){

            itemList += '<tr>' +
                            '<td>' + (index+1) + '</td>' +
                            '<td> <div class="checkboxer"><input type="checkbox" index="'+ index +'" class="cartChk" itemId="'+ record.itemId +'" id="'+ record.itemId +'_'+index+'"><label for="'+ record.itemId +'_'+index+'">'+ record.itemName +'</label></div> </td>' +
                            '<td><input type="text" placeholder="Quantity (eg. 1)" value=' + record.itemQty + ' onchange="calCartItem('+ index +','+ record.itemId +',this)" itemIdKey="'+ record.itemId +'_'+index+'" id="qty_'+ record.itemId +'_'+index+'" class="form-control onlyNumber" disabled></td>' +
                            '<td><input type="text" placeholder="Amount (eg. 10)" value=' + record.itemBasePrice + ' onchange="calCartItem('+ index +','+ record.itemId +',this)" itemIdKey="'+ record.itemId +'_'+index+'" id="price_'+ record.itemId +'_'+index+'" class="form-control onlyNumber" disabled></td>' + 
                            '<td><input type="text" placeholder="Amount (eg. 10)" value=' + record.itemPrice + ' inpType="total" onchange="calCartItem('+ index +','+ record.itemId +',this)" itemIdKey="'+ record.itemId +'_'+index+'" id="totalPrice_'+ record.itemId +'_'+index+'" class="form-control onlyNumber" disabled></td>' +
                        '<tr>';
        }else if( record.typeOfItem == "GROUP" ){
            itemList += '<tr>' +
                            '<td>' + (index+1) + '</td>' +
                            '<td>' + record.groupName + '</td>' +
                            '<td></td>' +
                            '<td></td>' + 
                            '<td></td>' +
                        '<tr>';
        }else if( record.typeOfItem == "SECTION" || record.typeOfItem == "BOM" ){
            itemList += '<tr>' +
                            '<td>' + (index+1) + '</td>' +
                            '<td>' + record.itemName + '</td>' +
                            '<td></td>' +
                            '<td></td>' + 
                            '<td></td>' +
                        '<tr>';
        }
    });

    $('#itemListBodyCart').html(itemList);
    $('#cartTotal').html( 0 );

    var transEmpty = false;

    var cancelFlag1 = findIndexByKeyValue( inqTransData , "cancleFlag" , "1" );
    var cancelFlag0 = findIndexByKeyValue( inqTransData , "cancleFlag" , "0" );
    var typeItem = findIndexByKeyValue( inqTransData , "typeOfItem" , "ITEM" );
    
    if( cancelFlag1 == "-1" && cancelFlag0 == "-1" ){
        transEmpty = true;
    }else if( typeItem == "-1" ){
        transEmpty = true;
    }

    if( inqTransData.length == "0" || transEmpty ){
        toastr.warning("Please add Items before moving Inquiry to final status");
        $('#updatePotAmt').modal('hide');
        return false;
    }else{
        
        var roleAuthData = JSON.parse(localStorage.indeCampusRoleAuth);
        var currClientType = roleAuthData.data.clientTypeId;
        if( currClientType == 34 ){
            if( getData[0].pendingStallListing != "No record found" ){
                $('#cartDataModal').modal('show'); 
                addFocusId( 'dealWonRemarkId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

                GBLsetDiscountData();   // TAXATION MODAL
            }else{
                toastr.error("Sorry..! No Pendings stalls are available of this Inquiry's Exhibition");
            }
        }else{
            $('#cartDataModal').modal('show');
            addFocusId( 'dealWonRemarkId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
            GBLsetDiscountData();
        }
    }

    $('.cartChk').change(function(){

        cartTotalAmt = 0;
        var check = $(this).is(':checked');
        var newItemId = $(this).attr('id');
        var actItemId = $(this).attr('itemId');
        var index = $(this).attr('index');

        if( check ){

            $('#qty_'+newItemId).attr('disabled',false);
            $('#price_'+newItemId).attr('disabled',false);

            newCartData[index].deleteFlag = "0";

        }else{

            $('#qty_'+newItemId).attr('disabled',true);
            $('#price_'+newItemId).attr('disabled',true);
            $('#totalPrice_'+newItemId).attr('disabled',true);

            if( newCartData.length != 0 ){
                if( index != "-1" ){
                    // newCartData.splice( index , 1 );
                    newCartData[index].deleteFlag = "1";
                }
            }
        }

        newCartData.forEach(function( record , index ){
            if( record.deleteFlag != "1" ){
                cartTotalAmt = parseFloat( cartTotalAmt ) + parseFloat( record.itemPrice );
            }
        });

        $('#cartTotal').html( numberFormat( cartTotalAmt ) );
    });

    $('.cartChk').click();

    $('.onlyNumber').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });
}

function calCartItem(index,itemId,context){

    var inpType = $(context).attr('inpType');
    // var index = findIndexByKeyValue( newCartData , "itemId" , itemId );

    if( inpType == "total" ){
        var totPrice = $('#totalPrice_'+itemId).val();
        newCartData[index].itemPrice = parseFloat( totPrice );
    }else{

        var itemIdKey = $(context).attr('itemIdKey');
        var qty = $('#qty_'+itemIdKey).val();
        var price = $('#price_'+itemIdKey).val();

        if( qty == 0 || qty == "" ){
            toastr.warning('Please Enter Valid Qty');
            $('#qty_'+itemIdKey).focus();
            return false;
        
        }else if( price == 0 || price == "" ){
            toastr.warning('Please Enter Valid Price');
            $('#price_'+itemIdKey).focus();
            return false;
        
        }else if( index == "-1" ){
            toastr.warning('Item is not found in cart');
            $('#qty_'+itemIdKey).focus();
            return false;
        }

        var totPrice = parseFloat( qty ) * parseFloat( price );

        $('#totalPrice_'+itemIdKey).val( parseFloat( totPrice ) );

        // cartTotalAmt = parseFloat( cartTotalAmt ) - parseFloat( newCartData[index].itemPrice );
        
        newCartData[index].itemQty = parseFloat( qty );
        newCartData[index].itemBasePrice = parseFloat( price );
        newCartData[index].itemPrice = parseFloat( totPrice );

        // cartTotalAmt = parseFloat( cartTotalAmt ) + parseFloat( newCartData[index].itemPrice );
        var cartTotalAmt = 0;
        newCartData.forEach(function( record , index ){
            if( record.deleteFlag != "1" ){
                cartTotalAmt = parseFloat( cartTotalAmt ) + parseFloat( record.itemPrice );
            }
        });
        $('#cartTotal').html( numberFormat( cartTotalAmt.toFixed(2) ) );
    }
}

function setRemindDate(actDate){
    $('#reminderDate').val( actDate );
    $(".remindDate").datetimepicker('remove');

    var actTrimDate = (( actDate ).split('|')[0]).trim();
    var actTime = (( actDate ).split('|')[1]).trim();

    $('.remindDate').datetimepicker({
        // startDate: 'today',
        // endDate: actDate,
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        endDate: new Date( ddmmyyToMysql( actTrimDate ) +" "+ actTime ),
        autoclose: true,
        minuteStep: 15,
    });
    $('.remindDate').prop("readonly",true);

    if( $('input[type=radio][name="rdoActMode"]').filter(":checked").val() == "4" ){    // ADD 60 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#txtActDueDate').val() , 60 )
    }else{    // ADD 15 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#txtActDueDate').val() , 15 )
    }
    
    callActListFun( 'txtActDueDate' );
}

function getCustomField(type){

    if( type == "itemHeadTab" ){
        $('.nav-tabs a[href="#itemLisTab"]').tab('show');
    }else{
        $('.nav-tabs a[href="#noteHeadTab"]').tab('show');
    }
}

function pageInitialEvents(){

    $(".panel-body .nav-tabs").on("click", "li", function(){
        
        var tabId = $(this).attr('hrefId');

        $('.tab-content .active').removeClass('active');
        $('#'+tabId).addClass('active');

    });
    $('#dueTime').clockface();
    $('#endNormTime').clockface();
    $('#reminderTime').clockface();
    
    $('#dueTime').on('pick.clockface', function() 
    {
        var d = new Date('01-01-1970 '+$('#dueTime').val());
        var remindTime = d.setMinutes(d.getMinutes() - 20);
        remindTime = formatAMPMRoundOff( new Date(remindTime) );
        $('#reminderTime').val( remindTime );
    });


    $('.dueDatedp').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true,
    }) 
    .on('changeDate', checkActList);

    $('#dueTime').on('pick.clockface', checkActList);
    
    var currTime = formatAMPMRoundOff( new Date() );

    var d = new Date();
    var remindTime = d.setMinutes(d.getMinutes() - 20);
    remindTime = formatAMPMRoundOff( new Date(remindTime) );

    $('#dueTime').val( currTime );
    $('#endNormTime').val( currTime );
    $('#reminderTime').val( remindTime );
    
    initCommonFunction();
    getinquiryDetailListing();


    // $("#tableListing").DataTable({ "stateSave": true });
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    for(var i=0;i<roleAuth.data.clientConfig.length;i++){
        if(roleAuth.data.clientConfig[i].FLAG == "1" && roleAuth.data.clientConfig[i].TYPE_OF_CONFIG == "INQGROUP"){
            $("#itemGroup").show();
            autoCompleteForGroup("itemGroupText", saveItemGroupDetail);
        }
    }
    
    $('input[name=rdoActMode]').change(function(){

        if($(this).val() == "1"){
            $('#txtActTitle').val('Call');

        }else if($(this).val() == "2"){
            $('#txtActTitle').val('Send Email');

        }else if($(this).val() == "3"){
            $('#txtActTitle').val('Send SMS');

        }else if($(this).val() == "4"){
            $('#txtActTitle').val('Meet');

        }else {
            $('#txtActTitle').val('');
        }
    });

    $(".bmd-fab-speed-dialer").on('click', function () {
        $(this).toggleClass("press")
    });

    $('.fab-dial-handle').click(function(){
        $('.fabdail-buttons').toggleClass('active');
    })
    $('.fab-dial-handle').click();

    $('#reminderActChk').change(function() {
        var scheduleChk = $('#scheduleAct').is(':checked');
        if($(this).is(":checked")) {
            $('.reminderActChkCls').show();
            if( scheduleChk ){
                $('#reminderDate').val('');
                $('.reminderDateDiv').hide();
            }
        }else{
            $('.reminderActChkCls').hide();
        } 
    });

    AutoCompleteForItem("itemName", saveItemDetail);
    AutoCompleteForItem("extraItemName", saveItemDetail);
    
    initBootStrapDatePicker();

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var actType = roleAuth.data.actType;

    setOption("0", "actTypeListSch", actType, "");
    $('#actTypeListSch').selectpicker( 'refresh' );

    if( roleAuth.data.costCalcFlag == "1" ){
        $('#costCalcButton').show();
    }


    //CHECK DECIMAL ALLOWED OR NOT
    // roleAuth.data.decimalQtyFlag = 1;
    if( roleAuth.data.decimalQtyFlag == "1" ){
        $('#itemQtyHead').html(''); 
        $('#itemQtyHead').html('<input type="number" min=".01" step=".01" id="itemQty" name="itemQty" value="1" class="form-control onlyNumber" autocomplete="off" placeholder="Quantity (eg. 1)">');
        $('#editQtyHead').html(''); 
        $('#editQtyHead').html('<input type="number" min=".01" step=".01" id="editQty" value="" class="form-control onlyNumber" placeholder="Quantity (eg. 1)">');
    }else{
        // it allow only number and plus symbol just place in input fields
        $('.onlyNumber').keypress(function (e) {
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                       return false;
            }
        });
    }
    
    var statusArr = JSON.parse(localStorage.indeCampusRoleAuth);
    setOption("0", "dealWonRemarkId", statusArr.data.dealWonReason, "-- Select Deal Won Remark --");
    $('#dealWonRemarkId').selectpicker('refresh');
    
    $('#proAttc').change(function() {
        if($(this).is(":checked")) {
            $('#attcPro').show(500);
            $('.itemPro').hide(500);
            $('#sendTmplt').hide(500);
        }else{
            $('#attcPro').hide(500);
            $('.itemPro').show(500);
            $('#sendTmplt').show(500);
        }
    });
    
    $('#itemListTab').click();

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    setOption("0", "inquirytype", data.data.inqType, "---Select Inquiry Type---");
    setOption("0", "inquirysource", data.data.inqSource, "---Select Inquiry Source---");

    $('#inquirytype').selectpicker();
    $('#inquirysource').selectpicker();

    // INITIALIZE TOUR
    if( localStorage.POTGinqTourFlag == "1" ){
        initSelfTour();
        localStorage.POTGinqTourFlag = "0";
    }
}

function navigateToExhib(){
    
    localStorage.exhibitionId = getData[0].exhibId;
    localStorage.POTGinqIdForStallBook = getData[0].inqId;
    navigateToCreateStallsWithExhbId();
}

function navigateToProject(){
    
    navigateToRealEstateBookInq( getData[0].exhibId , getData[0].inqId , getData[0].clientLevelInqId );
}

function openPaymentModal(){

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    
    setOption("0", "payType", roleAuth.data.paymentMode, "---Select Payment Mode---");
    $('#payType').selectpicker('refresh');

    setOption("0", "payMode", roleAuth.data.paymentType, "---Select  Payment Type---");
    $('#payMode').selectpicker('refresh');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getPaymentListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        custId : getData[0].custId,
    }

    commonAjax(COMMONURL, postData, payListCallback,"Please Wait... Getting Inquiry Data");

    function payListCallback(flag,data){

        if (data.status == "Success" && flag) {
        
            console.log(data);
            var paymentData = data.data;
            
            renderPayData(paymentData);
            outStandingTotal = data.outStandingTotal;
            
            // $('#paymentModal').modal('show');

        } else {
            if (flag){   
                displayAPIErrorMsg( data.status , GBL_ERR_NO_PAYMENT );
                outStandingTotal = data.outStandingTotal;
                renderPayData([]);
                // $('#paymentTable').dataTable({ "stateSave": true });
            }
            else{
                toastr.error(SERVERERROR);
                outStandingTotal = data.outStandingTotal;
                renderPayData([]);
                // $('#paymentTable').dataTable( {"stateSave": true} );  
            }
        }
    }
}

var alreadyPaid = 0;
function renderPayData(paymentData){

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = roleAuth.data.clientTypeId;

    var projFlatHideShow = "hide";
    if( currClientType == "32" ){
        projFlatHideShow = "show";
    }
    var tHead = '<table class="table table-hover" id="paymentTable">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            // '<th>Customer Name</th>'+
                            '<th class="'+ projFlatHideShow +'">Project-Unit</th>'+
                            '<th>Payment Date</th>'+
                            '<th>Payment Type</th>'+
                            '<th>Payment Remark</th>'+
                            '<th>Amount</th>'+
                            '<th>Cheque / DD / Charge Slip No </th>'+
                            '<th>Bank Name </th>'+
                            '<th>4 Digit of CC / DD or Cheque Date </th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';
                       
    var tEnd = '</tbody></table>';
    var payList = '';
    alreadyPaid = 0;
    paymentData.forEach(function( record , index ){
        alreadyPaid = alreadyPaid + parseInt( record.amount );
        payList += '<tr>'+
                        '<td>'+ (index + 1) +'</td>'+
                        // '<td>'+ record.custName +'</td>'+
                        '<td class="'+ projFlatHideShow +'">'+ record.projName +'-'+ record.flatName +'</td>'+
                        '<td>'+ mysqltoDesiredFormat( record.paymentDate , 'dd-MM-yyyy' )+'</td>'+
                        '<td>'+ record.paymentType +'</td>'+
                        '<td>'+ ((record.paymentRemark) ?  record.paymentRemark : "--") +'</td>'+  
                        '<td class="amtDisp">'+ numberFormat( parseFloat( record.amount ) ) +'</td>'+
                        '<td>'+ ((record.paymentfieldNo1) ? record.paymentfieldNo1 : "--")+'</td>'+
                        '<td>'+ ((record.paymentfieldNo2) ? record.paymentfieldNo2 : "--") +'</td>'+
                        '<td>'+ ((record.paymentfieldNo3) ? record.paymentfieldNo3 : "--") +'</td>'+
                        
                    '</tr>';
    });
    
    $('#paymentTabDiv').html( tHead + payList + tEnd );
    $('#paymentTable').dataTable({ "stateSave": true });
    resetPayModal();
}

function sendPay(){

    if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Stat');
        return false;
    }

    //var payMode = $('#payMode').val();
    var payType = $('#payType').val();
    //console.log(payType);
    // CASH & OTHER PAYMENT TYPE FIELDS
    var amount = "";
    var chequeChargeNo = "";
    var bankName = "";
    var dateOrLast4Digit = "";
    var paymentRemark = "";
    var flag = 0;
    
    if( payType == "-1" ){
        toastr.warning('Please Select Payment Mode');
        $('#payMode').focus();
        return false;

    }
    
    if(payType == "36" ){ // CASH
        
        amount = $("#payAmount").val();
        paymentRemark = $("#payRemark").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#payAmount').focus();
            flag = 1;
        }else{
            flag = 0;            
        }
        
        if(flag){
            return false;
        }
    }
    
    if(payType == "37" ){ // DD
       
        amount = $("#ddpayAmount").val();
        chequeChargeNo = $("#ddcheckNo").val();
        bankName = $("#BankNamedd").val();
        dateOrLast4Digit = $("#chequeDatedd").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#ddpayAmount').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(chequeChargeNo.length <= 0){
            toastr.warning('Please Enter Valid DD / Cheque Number');            
            $('#ddcheckNo').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(bankName.length <= 0){
            toastr.warning('Please Enter Valid Bank Name');            
            $('#BankNamedd').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(dateOrLast4Digit.length <= 0){
            toastr.warning('Please Enter Valid date');            
            $('#chequeDatedd').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(flag){
            return false;
        }
    }
    
    if(payType == "38" ){ // CC
        
        amount = $("#ccpayAmount").val();
        chequeChargeNo = $("#ccChargeSlipNo").val();
        bankName = $("#BankName").val();
        dateOrLast4Digit = $("#CardL4digit").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount.');            
            $('#ccpayAmount').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(chequeChargeNo.length <= 0){
            toastr.warning('Please Enter Valid Charge Slip Number.');            
            $('#ccChargeSlipNo').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(bankName.length <= 0){
            toastr.warning('Please Enter Valid Bank Name.');            
            $('#BankName').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(dateOrLast4Digit.length <= 0){
            toastr.warning('Please Enter Valid last 4 digit of credit card.');            
            $('#CardL4digit').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(flag){
            return false;
        }
        
    }
    
    if(payType == "39" ){ // OTHER
        amount = $("#payAmount").val();
        paymentRemark = $("#payRemark").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#payAmount').focus();
            flag = 1;
        }else{
            flag = 0;
        }
        
        if(flag){
            return false;
        }
    }
    
    //console.log("valiation success"); return false;
    // var totCartAmt = 0;
    // getData[0].inqTransDetail.forEach(function( record , index ){
    //     totCartAmt = totCartAmt + parseInt( record.itemPrice );
    // });
    // var remainToPay = totCartAmt - alreadyPaid;
    var remainToPay = outStandingTotal;

    if( parseFloat(amount) > parseFloat(remainToPay) ){
        toastr.error('You cannot Pay More than '+remainToPay+'rs.');
        $('#payAmount').focus();
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    
    var postData = {
        requestCase: 'collectPayment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        custId : getData[0].custId,
        paymentMode : "-1",
        paymentType : payType,
        paymentRemark : paymentRemark,
        paymentAmount : amount,
        paymentfiled1 : chequeChargeNo,
        paymentfiled2 : bankName,
        paymentfiled3 : dateOrLast4Digit,
    }
    commonAjax(COMMONURL, postData, sendPayCallback,"Please Wait... Getting Inquiry Data");

    function sendPayCallback(flag,data){

        if (data.status == "Success" && flag) {
        
            console.log(data);
            var paymentData = data.data;
            outStandingTotal = data.outStandingTotal;
            renderPayData(paymentData);
            resetPayModal();

            getInqLog();    // CALL AN INQUIRY LOG API

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_COLLECT_PAYMENT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function resetPayModal(){

    $('#payMode').val('-1');
    $('#payMode').selectpicker('refresh');
    $('#payType').val('-1');
    $('#payType').selectpicker('refresh');
    $('#payRemark').val('');
    $('#payAmount').val('');
    $('#ddcheckNo').val('');
    $('#BankNamedd').val('');
    $('#chequeDatedd').val('');
    $('#ccChargeSlipNo').val('');
    $('#BankName').val('');
    $('#CardL4digit').val('');
}

var totExtraItems = [];
function openExtraItemModal(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getExtraItemDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
    }

    commonAjax(COMMONURL, postData, openExtraItemCallback,"Please Wait... Getting Inquiry Data");

    function openExtraItemCallback(flag,data){

        if (data.status == "Success" && flag) {
        
            console.log(data);
            var extraData = data.data;
            totExtraItems = extraData;

            renderExtraData(extraData);
            $('#AddExtraItemModal').modal('show');

        } else {
            totExtraItems = [];
            renderExtraData( totExtraItems );
            if (flag){   
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEMS );
                // $('#extraItemTable').DataTable({ "stateSave": true });
            }
            else{
                toastr.error(SERVERERROR);
                // $('#extraItemTable').DataTable( {"stateSave": true} );
            }
        }
    }
    resetExtraItem();
}


function addextraItemRow(){
    
    if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Stat');
        return false;
    }

    extraTransData = [];
    var selectedItemName = $('#extraItemName').attr('itemid');
    var selectedGroupName = $('#extraItemGroupText').attr('groupid');
    var itemQty =  $('#extraItemQty').val();
    var itemRemark =  $('#extraItemRemark').val();
    var itemPrice =  $('#extraItemPrice').val();
    
    if((selectedItemName == '' || selectedItemName == undefined) && (selectedGroupName == "" || selectedGroupName == undefined )){
        toastr.warning('Please select item from autocomplete');
        $('#extraItemName').focus();
        return false;

    }else if( (selectedItemName != '' && selectedItemName != undefined )  && itemPrice == ""){
        toastr.warning('please Enter Item Price');
        $('#extraItemPrice').focus();
        return false;

    }else if( itemQty == "" || parseFloat( itemQty ) < 0.01){
        toastr.warning('please Enter Item Quantity');
        $('#extraItemQty').focus();
        return false;
    }else{

        var sequenceId = (inqTransData.length+1);
        var index = findIndexByKeyValue(itemJson,"itemId",selectedItemName);
        var  tempData = {
            cancleFlag : "1",
            itemId : itemJson[index].itemId,
            itemName : itemJson[index].itemName,
            groupName : itemJson[index].itemGroupName,
            groupId : itemJson[index].itemGroupId,
            itemQty : itemQty,
            eventStatus : "",
            itemPrice : (Number( parseFloat( itemPrice ) * parseFloat( itemQty ) )).toFixed(2),
            itemBasePrice : itemPrice,
            itemType : "ITEM",
            itemRemark : (itemRemark != "")? itemRemark : "",
            itemUOM : itemJson[index].itemUomName,
            itemCat : "1", // 1 = item 2 - header text
            sequenceId : sequenceId,
            typeOfItem : "ITEM"
        }
        var dupInd = findIndexByKeyValue( extraTransData,'itemId',itemJson[index].itemId );
        var dupTotInd = findIndexByKeyValue( totExtraItems,'itemId',itemJson[index].itemId );
        if( dupInd == "-1" && dupTotInd == "-1" ){
            extraTransData.push(tempData);
        }else{
            toastr.warning('This Item already Added in the Cart');
            return false;
        }

        sendExtraItem();
        console.log(extraTransData);
        resetExtraItem();
    }
    
}

function sendExtraItem(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'addExtraItemIntoBooking',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        custId : getData[0].custId,
        itemTrans : extraTransData
    }
    commonAjax(COMMONURL, postData, sendPayCallback,"Please Wait... Getting Inquiry Data");

    function sendPayCallback(flag,data){

        if (data.status == "Success" && flag) {
        
            console.log(data);
            var extraData = data.data;
            renderExtraData(extraData);
            
            getInqLog();    // CALL AN INQUIRY LOG API
           
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ADD_ITEM );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderExtraData(extraData){

    var tHead = '<table class="table table-hover" id="extraItemTable">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Group</th>'+
                        '<th>Item</th>'+
                        '<th>Qty</th>'+
                        '<th>UOM</th>'+
                        '<th>Price</th>'+
                        '<th>Value</th>'+
                        '<th>Remark</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>';
           
    var tEnd = '</tbody></table>';
    var extraList = '';
    if( extraData.length != "0" ){
        extraData.forEach(function( record , index ){
            extraList += '<tr>'+
                            '<td>'+ (index + 1) +'</td>'+
                            '<td>'+ record.groupName +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td>'+ record.itemQty +'</td>'+
                            '<td>'+ record.itemUOM +'</td>'+
                            '<td class="amtDisp">'+ numberFormat( parseFloat( record.itemBasePrice ) ) +'</td>'+
                            '<td class="amtDisp">'+ numberFormat( parseFloat( record.itemPrice ) ) +'</td>'+
                            '<td>'+ record.itemRemark +'</td>'+
                            '<td><a href="javascript:void(0)"><button item-id="'+ record.itemId +'" trans-id="'+ record.inqtransId +'" type="button" class="btn btn-primary btn-ripple btn-xs btn-danger" onclick="removeExtraItem(this)"><i class="fa fa-trash-o"></i></button></td>'+
                        '</tr>';
        });
    }
    
    $('#extraItemTableDiv').html( tHead + extraList + tEnd );
    $('#extraItemTable').dataTable({ "stateSave": true });
    resetExtraItem();
}

function removeExtraItem(context){

    var itemTransId = $(context).attr('trans-id');
    var itemId = $(context).attr('item-id');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'deleteExtraItem',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,15),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        transId : itemTransId,
        itemId : itemId,
    }

    commonAjax(COMMONURL, postData, removeExtraItemCallback,"Please Wait... Getting Inquiry Data");

    function removeExtraItemCallback(flag,data){

        if (data.status == "Success" && flag) {
        
            console.log(data);
            var extraData = data.data;
            
            var index = findIndexByKeyValue( extraTransData , "itemId" , itemId );
            extraTransData.splice(index,1);
            var index2 = findIndexByKeyValue( totExtraItems , "itemId" , itemId );
            totExtraItems.splice(index2,1);
            renderExtraData(extraData);
            getInqLog();    // CALL AN INQUIRY LOG API

        } else {
            if (flag){   
                displayAPIErrorMsg( data.status , GBL_ERR_DELETE_ITEM );
                extraTransData = [];
                // $('#extraItemTbody').html('<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty">No data available in table</td></tr>');
                // $('#extraItemTable').DataTable({ "stateSave": true });
            }
            else{
                toastr.error(SERVERERROR);
            }
        }
    }
}

function resetExtraItem(){
    $('#extraItemName').val('');
    $('#extraItemName').attr('itemid','');
    $('#extraItemQty').val('');
    $('#extraItemRemark').val('');
    $('#extraItemGroupText').val('');
    $('#extraItemPrice').val('');
    $('#extraItemGroupText').attr('groupid','');
}

function changeFiled(paymentType){
    console.log(outStandingTotal);
    if(paymentType == "36"){ // CASH
       $("#cashPaymentCollect").show();
       $("#ddPaymentCollect").hide();
       $("#creditCardPaymentCollect").hide();
       $("#payAmount").val(outStandingTotal);
       $("#payAmount").focus();
    }else if(paymentType == "37"){ // DD 
       $("#ddPaymentCollect").show();
       $("#cashPaymentCollect").hide();
       $("#creditCardPaymentCollect").hide();
       $("#ddpayAmount").val(outStandingTotal);
       $("#ddpayAmount").focus();
    }else if(paymentType == "38"){ // CREDIT CARD
        $("#creditCardPaymentCollect").show();
        $("#ddPaymentCollect").hide();
        $("#cashPaymentCollect").hide();
        $("#ccpayAmount").val(outStandingTotal);
        $("#ccpayAmount").focus();
    }else if(paymentType == "39"){ // OTHER
        $("#cashPaymentCollect").show();
        $("#ddPaymentCollect").hide();
        $("#creditCardPaymentCollect").hide();
        $("#payAmount").val(outStandingTotal);
        $("#payAmount").focus();
    }
}

var itemDetailData = [];
function openDetailModal( itemDetail ){
    itemDetailData = itemDetail;

    var itemDetailInput = "";

    itemDetail.forEach( function( record , index ){

        if( record.inputType == "LIST" ){
            itemDetailInput += '<div class="col-lg-12">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+ record.title +'</label>'+
                                        '<select data-width="100%" id="select_'+ record.key +'">';
                                        record.children.forEach( function( innrRecord , innrIndex ){
                                            itemDetailInput += '<option key="'+ innrRecord.key +'" inpTitle="'+ innrRecord.title +'" value="'+ innrRecord.price +'">'+ innrRecord.title +' -- '+ innrRecord.price +'</option>';
                                        });
                                        itemDetailInput += '</select>'+
                                    '</div>'+
                                '</div>';

        }else if( record.inputType == "TEXT" ){
            itemDetailInput += '<div class="col-lg-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+ record.title +'</label>'+
                                        '<input type="text" id="inputTxt_'+ record.children[0].key +'" key="'+ record.children[0].key +'" inpTitle="'+ record.children[0].title +'" value="'+ record.title +'" class="form-control" autocomplete="off" placeholder="'+ record.children[0].title +'"/>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label"> Rate </label>'+
                                        '<input type="text" id="inputRate_'+ record.key +'" key="'+ record.key +'" inpTitle="'+ record.title +'" value="'+ record.children[0].price +'" class="onlyNumber form-control" autocomplete="off" placeholder="'+ record.children[0].price +'"/>'+
                                    '</div>'+
                                '</div>';
        }
    });
    $('#itemDetailData').html( itemDetailInput );
    $('#addItemDetailModal select').selectpicker();

    $('#addItemDetailModal').modal('show');
}

var detailJSON = [];
function saveItemDetailing(){

    var itemPrice = parseFloat($('#itemPrice').val());
    var inpPrice = 0;
    detailJSON = [];
    itemDetailData.forEach( function( record , index ){
        tempJson = [];
        if( record.inputType == "LIST" ){
            inpPrice = $("#select_"+ record.key).val();

            var childJson = [{
                inputType: record.inputType,
                key: $("#select_"+ record.key + ' option:selected').attr('key'),
                price: inpPrice,
                title: $("#select_"+ record.key + ' option:selected').attr('inptitle'),
            }]

            tempJson = {
                children : childJson,
                inputType : record.inputType,
                key : record.key,
                title : record.title,
            }

            detailJSON.push( tempJson );

        }else if( record.inputType == "TEXT" ){
            inpPrice = $("#inputRate_"+ record.key).val();
            if( inpPrice == "" || inpPrice == undefined ){
                toastr.warning('Please Enter Valid rate to this Item Option');
                $("#inputRate_"+ record.key).focus();
                setTimeout(function(){
                    $('#addItemDetailModal').modal('show');
                    return false;
                },200);
            }

            var childJson = [{
                inputType: record.inputType,
                key: $("#inputRate_"+ record.key).attr('key'),
                price: inpPrice,
                title: $("#inputTxt_"+ record.children[0].key).val(),
            }]

            tempJson = {
                children : childJson,
                inputType : record.inputType,
                key : record.key,
                title : record.title,
            }

            detailJSON.push( tempJson );
        }
        itemPrice = parseFloat( itemPrice ) + parseFloat( inpPrice ); 
    });
    $('#itemPrice').val(itemPrice);
    $('#addItemDetailModal').modal('hide');
    console.log(detailJSON);
}

var GBLITEMID = "";
function openVCDialog(context,index){

    var itemId = $(context).attr('item-id');
    // GBLITEMID = itemId;
    // var index = findIndexByKeyValue( inqTransData , "itemId" , itemId );
    var itemDetailData = [];
    if( index != "-1" ){
        itemDetailData = inqTransData[index].detailing;

        if( itemDetailData != "No record found" && itemDetailData != "" && itemDetailData != null ){
            var startHtml = "<table id='VCItemTable' class='display datatables-alphabet-sorting'>"+
                        "<thead>"+
                            "<tr>"+
                                // "<th>Sr No.</th>"+
                                // "<th>Input Type</th>"+
                                "<th>Property</th>"+
                                "<th>Option</th>"+
                                "<th>Price</th>"+
                            "</tr>"+
                        "</thead>"+
                        "<tbody>";
            var endHtml = "</tbody>";
            var inqRow = '';
            var bomListData = '';
            
            itemDetailData.forEach( function( record , index ){
                bomListData += '<tr>'+
                                // '<td>'+ (index+1) +'</td>'+
                                // '<td>'+ record.inputType +'</td>'+
                                '<td>'+ record.title +'</td>'+
                                '<td>'+ record.children[0].title +'</td>'+
                                '<td>'+ record.children[0].price +'</td>'+
                            '</tr>';  
            });
            $('#showItemDetailData').html( startHtml + bomListData + endHtml);
            $('#VCItemTable').DataTable({ "stateSave": true });

            $('#showItemDetailModal').modal('show');
        }else{
            toastr.warning('Detailing not available');
        }
    }
}

function copyInquiry(){
    
    localStorage.POTGCopyInqId = getData[0].inqId;
    navigateToCreateInquiry();
}

var optionArr = [];
function costCalcModal(){

    optionArr = [];
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getItemCostCalculation',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        itemId : GBLITEMID,
    }
    // console.log(postData);return false;
    commonAjax(COMMONURL, postData,costCalcModalCallback,"Please Wait... Getting Inquiry Data");

    function costCalcModalCallback(flag,data){

        if (data.status == "Success" && flag) {
        
            console.log(data);
            if( data.data != "No record found" ){
                renderCostTable(data.data , "1");
            }
            
        } else {
            if (flag){
                renderCostTable(data.data , "0");
                displayAPIErrorMsg( data.status , GBL_ERR_NO_COST_CALCULATION );
            }  
            else{
                toastr.error(SERVERERROR);
            }
        }
    }
}

function renderCostTable(data , type){


    var itemId = GBLITEMID;
    var index = findIndexByKeyValue( inqTransData , "itemId" , itemId );
    itemDetailData = [];
    if( index != "-1" ){
        itemDetailData = inqTransData[index].detailing;
        var startHtml = "<table id='calcItemTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th style='width:30%;'>Property - Option</th>"+
                            "<th>Qty1</th>"+
                            "<th>Qty2</th>"+
                            "<th>Qty3</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
        var endHtml = "</tbody></table>";
        var inqRow = '';
        var bomListData = '';
        bomListData += '<tr class="totCostBg">'+
                            '<td> 1 </td>'+
                            '<td> Qty </td>'+
                            '<td><input type="text" id="qtyAmt1" name="qty1" class="form-control centAlign onlyNumberWithDec" value="0" onchange="QtyChange(1,this);"></td>'+
                            '<td><input type="text" id="qtyAmt2" name="qty2" class="form-control centAlign onlyNumberWithDec" value="0" onchange="QtyChange(2,this);"></td>'+
                            '<td><input type="text" id="qtyAmt3" name="qty3" class="form-control centAlign onlyNumberWithDec" value="0" onchange="QtyChange(3,this);"></td>'+
                        '</tr>'; 

        itemDetailData.forEach( function( record , index ){
            bomListData += '<tr>'+
                            '<td>'+ (index+2) +'</td>'+
                            '<td style="width:30%;">'+ record.title +' - '+ record.children[0].title +'</td>'+
                            '<td><input type="text" id="optAmtQty1_'+ index +'" name="qty1" class="form-control centAlign onlyNumberWithDec amtFieldQty1" value="0.00" onchange="QtyChange(1,this);" disabled></td>'+
                            '<td><input type="text" id="optAmtQty2_'+ index +'" name="qty2" class="form-control centAlign onlyNumberWithDec amtFieldQty2" value="0.00" onchange="QtyChange(2,this);" disabled></td>'+
                            '<td><input type="text" id="optAmtQty3_'+ index +'" name="qty3" class="form-control centAlign onlyNumberWithDec amtFieldQty3" value="0.00" onchange="QtyChange(3,this);" disabled></td>'+
                        '</tr>';
            var temp = {
                id : "optAmtQty1_"+ index,
                title : record.title +' - '+ record.children[0].title,
                qtyNum : "qtyAmt1",
                qtyAmt : "0",
                priceAmt : "0",
                totalCost : "0",
                costPerPiece : "0",
                pricePerPiece : "0",
            }  
            optionArr.push( temp );
            
            var temp = {
                id : "optAmtQty2_"+ index,
                title : record.title +' - '+ record.children[0].title,
                qtyNum : "qtyAmt2",
                qtyAmt : "0",
                priceAmt : "0",
                totalCost : "0",
                costPerPiece : "0",
                pricePerPiece : "0",
            }  
            optionArr.push( temp );

            var temp = {
                id : "optAmtQty3_"+ index,
                title : record.title +' - '+ record.children[0].title,
                qtyNum : "qtyAmt3",
                qtyAmt : "0",
                priceAmt : "0",
                totalCost : "0",
                costPerPiece : "0",
                pricePerPiece : "0",
            }  
            optionArr.push( temp );
        });

        bomListData += '<tr class="totCostBg">'+
                            '<td>'+ (itemDetailData.length + 2) +'</td>'+
                            '<td> Total Cost </td>'+
                            '<td><input type="text" id="totCostqty1" name="Total Cost Qty 1" class="form-control centAlign onlyNumberWithDec" value="0.00" disabled></td>'+
                            '<td><input type="text" id="totCostqty2" name="Total Cost Qty 2" class="form-control centAlign onlyNumberWithDec" value="0.00" disabled></td>'+
                            '<td><input type="text" id="totCostqty3" name="Total Cost Qty 3" class="form-control centAlign onlyNumberWithDec" value="0.00" disabled></td>'+
                        '</tr>'; 

        bomListData +=  '<tr class="totCostBg">'+
                            '<td>'+ (itemDetailData.length + 3) +'</td>'+
                            '<td> Cost per Piece </td>'+
                            '<td><input type="text" id="costPerPiece1" name="Cost Per Piece Qty 1" class="form-control centAlign onlyNumberWithDec" value="0.00" disabled></td>'+
                            '<td><input type="text" id="costPerPiece2" name="Cost Per Piece Qty 2" class="form-control centAlign onlyNumberWithDec" value="0.00" disabled></td>'+
                            '<td><input type="text" id="costPerPiece3" name="Cost Per Piece Qty 3" class="form-control centAlign onlyNumberWithDec" value="0.00" disabled></td>'+
                        '</tr>';  

        bomListData +=  '<tr class="totCostBg">'+
                            '<td>'+ (itemDetailData.length + 4) +'</td>'+
                            '<td> Price per Piece </td>'+
                            '<td><input type="text" id="pricePerPiece1" name="Price Per Piece Qty 1" class="form-control centAlign onlyNumberWithDec amtFieldQty1" value="0.00" qtyKey="qtyAmt1" onchange="changePerPiecePrice(this);" disabled></td>'+
                            '<td><input type="text" id="pricePerPiece2" name="Price Per Piece Qty 1" class="form-control centAlign onlyNumberWithDec amtFieldQty2" value="0.00" qtyKey="qtyAmt2" onchange="changePerPiecePrice(this);" disabled></td>'+
                            '<td><input type="text" id="pricePerPiece3" name="Price Per Piece Qty 1" class="form-control centAlign onlyNumberWithDec amtFieldQty3" value="0.00" qtyKey="qtyAmt3" onchange="changePerPiecePrice(this);" disabled></td>'+
                        '</tr>'; 

        $('#showCalclData').html( startHtml + bomListData + endHtml);
        $('#calcItemTable').DataTable({ "stateSave": true });

        $('#costCalModal').modal('show');

         // it allow only number and plus symbol just place in input fields
        $('.onlyNumber').keypress(function (e) {
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                       return false;
            }
        });

        // it allow only number and plus symbol just place in input fields
        $('.onlyNumberWithDec').keypress(function (e) {
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 && e.which != 46 ) {
                       return false;
            }
        });
        
        if( type == "1" ){
            data.itemCost.forEach( function( record , index ){
                record.qtyCalculation.forEach( function( innrRecord , innrndex ){
                    if( record.qtyName == "Qty1" ){
                        var index = findIndexByKeyValueAndCond( optionArr , "title" , "qtyNum" , innrRecord.property , "qtyAmt1" );
                        if( index != "-1" ){
                            optionArr[index].qtyAmt = record.qty;
                            optionArr[index].priceAmt = innrRecord.cost;
                            optionArr[index].totalCost = record.totalCost;
                            optionArr[index].costPerPiece = record.costPerPiece;
                            optionArr[index].pricePerPiece = record.pricePerPiece;
                        }
                        $('#qtyAmt1').val( record.qty );
                        $('#pricePerPiece1').val( record.pricePerPiece );
                        $('.amtFieldQty1').removeAttr('disabled');

                    }else if( record.qtyName == "Qty2" ){
                        var index = findIndexByKeyValueAndCond( optionArr , "title" , "qtyNum" , innrRecord.property , "qtyAmt2" );
                        if( index != "-1" ){
                            optionArr[index].qtyAmt = record.qty;
                            optionArr[index].priceAmt = innrRecord.cost;
                            optionArr[index].totalCost = record.totalCost;
                            optionArr[index].costPerPiece = record.costPerPiece;
                            optionArr[index].pricePerPiece = record.pricePerPiece;
                        }
                        $('#qtyAmt2').val( record.qty );
                        $('#pricePerPiece2').val( record.pricePerPiece );
                        $('.amtFieldQty2').removeAttr('disabled');

                    }else if( record.qtyName == "Qty3" ){
                        var index = findIndexByKeyValueAndCond( optionArr , "title" , "qtyNum" , innrRecord.property , "qtyAmt3" );
                        if( index != "-1" ){
                            optionArr[index].qtyAmt = record.qty;
                            optionArr[index].priceAmt = innrRecord.cost;
                            optionArr[index].totalCost = record.totalCost;
                            optionArr[index].costPerPiece = record.costPerPiece;
                            optionArr[index].pricePerPiece = record.pricePerPiece;
                        }
                        $('#qtyAmt3').val( record.qty );
                        $('#pricePerPiece3').val( record.pricePerPiece );
                        $('.amtFieldQty3').removeAttr('disabled');
                    }
                });
            });
            renderCostCalCulation();
        }
        
    }
}

function QtyChange(type,context){
    
    var id = $(context).attr('id');
    var inpVal = $('#'+id).val();
    if( inpVal == "" || inpVal == "0" ){
        toastr.warning('Please Enter Valid Qty');
        $('#'+id).focus();
        
        if( id == "qtyAmt1" ){
            $('.amtFieldQty1').attr('disabled',true);
            $('#totCostqty1').val('0.00');
            $('#costPerPiece1').val('0.00');
            $('#pricePerPiece1').val('0.00');
        }

        if( id == "qtyAmt2" ){
            $('.amtFieldQty2').attr('disabled',true);
            $('#totCostqty2').val('0.00');
            $('#costPerPiece2').val('0.00');
            $('#pricePerPiece2').val('0.00');
        }

        if( id == "qtyAmt3" ){
            $('.amtFieldQty3').attr('disabled',true);
            $('#totCostqty3').val('0.00');
            $('#costPerPiece3').val('0.00');
            $('#pricePerPiece3').val('0.00');
        }

        optionArr.forEach( function( record , index ){
                
            if( record.qtyNum == id ){
                optionArr[index].qtyAmt = inpVal;
                optionArr[index].priceAmt = "0";
                optionArr[index].totalCost = "0";
                optionArr[index].costPerPiece = "0";
                optionArr[index].pricePerPiece = "0";
            }
        });
    }else{

        if( id == "qtyAmt1" || id == "qtyAmt2" || id == "qtyAmt3" ){
            
            if( inpVal != "0" && id == "qtyAmt1" ){
                $('.amtFieldQty1').removeAttr('disabled');
            }

            if( inpVal != "0" && id == "qtyAmt2" ){
                $('.amtFieldQty2').removeAttr('disabled');
            }

            if( inpVal != "0" && id == "qtyAmt3" ){
                $('.amtFieldQty3').removeAttr('disabled');
            }

            optionArr.forEach( function( record , index ){
                
                if( record.qtyNum == id ){
                    optionArr[index].qtyAmt = inpVal;
                }
            });
        }else{
            var index = findIndexByKeyValue( optionArr , "id" , id );
            if( index != "-1" ){
                optionArr[index].priceAmt = inpVal;
            }
        }
    }
    renderCostCalCulation();
}

function renderCostCalCulation(){

    optionArr.forEach( function( record , index ){
        
//        $('#'+record.id).val( numberFormat( record.priceAmt , 2 ) );
        $('#'+record.id).val(  record.priceAmt  );

        var totalCostQty1 = 0;
        var totalCostQty2 = 0;
        var totalCostQty3 = 0;
        optionArr.forEach( function( record , index ){
            if( record.qtyNum == "qtyAmt1" ){
                totalCostQty1 = parseFloat(totalCostQty1) + parseFloat(optionArr[index].priceAmt);
            
            }else if( record.qtyNum == "qtyAmt2" ){
                totalCostQty2 = parseFloat(totalCostQty2) + parseFloat(optionArr[index].priceAmt);
            
            }else if( record.qtyNum == "qtyAmt3" ){
                totalCostQty3 = parseFloat(totalCostQty3) + parseFloat(optionArr[index].priceAmt);
            }
        });

        var itemQty1 = $('#qtyAmt1').val();
        var itemQty2 = $('#qtyAmt2').val();
        var itemQty3 = $('#qtyAmt3').val();

        var costPerPiece1 = parseFloat( totalCostQty1 / itemQty1 ).toFixed(2);
        var costPerPiece2 = parseFloat( totalCostQty2 / itemQty2 ).toFixed(2);
        var costPerPiece3 = parseFloat( totalCostQty3 / itemQty3 ).toFixed(2);

        var pricePerPiece1 = parseFloat( totalCostQty1 / itemQty1 ).toFixed(2);
        var pricePerPiece2 = parseFloat( totalCostQty2 / itemQty2 ).toFixed(2);
        var pricePerPiece3 = parseFloat( totalCostQty3 / itemQty3 ).toFixed(2);

        if( itemQty1 != "0" ){
            $('#totCostqty1').val( numberFormat( totalCostQty1 , 2 ) );
            $('#costPerPiece1').val( numberFormat( costPerPiece1 , 2 ) );
        }

        if( itemQty2 != "0" ){
            $('#totCostqty2').val( numberFormat( totalCostQty2 , 2 ) );
            $('#costPerPiece2').val( numberFormat( costPerPiece2 , 2 ) );
        }

        if( itemQty3 != "0" ){
            $('#totCostqty3').val( numberFormat( totalCostQty3 , 2 ) );
            $('#costPerPiece3').val( numberFormat( costPerPiece3 , 2 ) );
        }

        optionArr.forEach( function( record , index ){
            if( record.qtyNum == "qtyAmt1" && record.qtyAmt != "0" ){
                optionArr[index].qtyAmt = ( parseFloat( itemQty1 ) ).toFixed(2);
                optionArr[index].totalCost = ( parseFloat( totalCostQty1 ) ).toFixed(2);
                optionArr[index].costPerPiece = ( parseFloat( costPerPiece1 ) ).toFixed(2);
                // optionArr[index].pricePerPiece = pricePerPiece1;
            
            }else if( record.qtyNum == "qtyAmt2" && record.qtyAmt != "0" ){
                optionArr[index].qtyAmt = ( parseFloat( itemQty2 ) ).toFixed(2);
                optionArr[index].totalCost = ( parseFloat( totalCostQty2 ) ).toFixed(2);
                optionArr[index].costPerPiece = ( parseFloat( costPerPiece2 ) ).toFixed(2);
                // optionArr[index].pricePerPiece = pricePerPiece2;
            
            }else if( record.qtyNum == "qtyAmt3" && record.qtyAmt != "0" ){
                optionArr[index].qtyAmt = ( parseFloat( itemQty3 ) ).toFixed(2);
                optionArr[index].totalCost = ( parseFloat( totalCostQty3 ) ).toFixed(2);
                optionArr[index].costPerPiece = ( parseFloat( costPerPiece3 ) ).toFixed(2);
                // optionArr[index].pricePerPiece = pricePerPiece3;
            }
        });
    });   
    console.log(optionArr);
}

function changePerPiecePrice(context){
    var qtyKey = $(context).attr('qtyKey');
    optionArr.forEach( function( record , index ){
        if( record.qtyNum == qtyKey ){
            optionArr[index].pricePerPiece = $(context).val();
        }
    });
}

function saveCalculation(){

    var sendFlag = true;
    var itemCostCalJson = [];

    var itemQty1Json = [];
    var itemQty2Json = [];
    var itemQty3Json = [];

    var qtyVal1 = 0;
    var qtyVal2 = 0;
    var qtyVal3 = 0;

    var totalCostQty1 = 0;
    var totalCostQty2 = 0;
    var totalCostQty3 = 0;

    var costPerPieceQty1 = 0;
    var costPerPieceQty2 = 0;
    var costPerPieceQty3 = 0;

    var pricePerPieceQty1 = 0;
    var pricePerPieceQty2 = 0;
    var pricePerPieceQty3 = 0;

    optionArr.forEach( function( record , index ){
        if( record.qtyNum == "qtyAmt1" ){

            var tempJson = {
                property : record.title,
                cost : record.priceAmt,
            }
            itemQty1Json.push( tempJson );

            qtyVal1 = record.qtyAmt,
            totalCostQty1 = record.totalCost;
            costPerPieceQty1 = record.costPerPiece;
            pricePerPieceQty1 = record.pricePerPiece;

        }else if( record.qtyNum == "qtyAmt2" ){

            var tempJson = {
                property : record.title,
                cost : record.priceAmt,
            }
            itemQty2Json.push( tempJson );

            qtyVal2 = record.qtyAmt,
            totalCostQty2 = record.totalCost;
            costPerPieceQty2 = record.costPerPiece;
            pricePerPieceQty2 = record.pricePerPiece;
        
        }else if( record.qtyNum == "qtyAmt3" ){

            var tempJson = {
                property : record.title,
                cost : record.priceAmt,
            }
            itemQty3Json.push( tempJson );

            qtyVal3 = record.qtyAmt,
            totalCostQty3 = record.totalCost;
            costPerPieceQty3 = record.costPerPiece;
            pricePerPieceQty3 = record.pricePerPiece;
        }
    });

    itemCostCalJson = [
        {
            qtyName : "Qty1",
            qty : qtyVal1,
            qtyCalculation : itemQty1Json,
            totalCost : totalCostQty1,
            costPerPiece : costPerPieceQty1,
            pricePerPiece : pricePerPieceQty1,
        },
        {
            qtyName : "Qty2",
            qty : qtyVal2,
            qtyCalculation : itemQty2Json,
            totalCost : totalCostQty2,
            costPerPiece : costPerPieceQty2,
            pricePerPiece : pricePerPieceQty2,
        },
        {
            qtyName : "Qty3",
            qty : qtyVal3,
            qtyCalculation : itemQty3Json,
            totalCost : totalCostQty3,
            costPerPiece : costPerPieceQty3,
            pricePerPiece : pricePerPieceQty3,
        },
    ];

    for(i = itemCostCalJson.length - 1; i >= 0; i -= 1) {
        if (itemCostCalJson[i].qty == "0" ) {
            itemCostCalJson.splice(i, 1);
        }
    }

    if( itemCostCalJson.length == "0" ){
        toastr.error('Please Calculate Cost of Different Item Qty');
        sendFlag = false;
        return false;
    }

    itemCostCalJson.forEach( function( record , index ){
        if( record.pricePerPiece == "0"){
            toastr.error('Please Enter Proper per piece price for this '+record.qtyName);
            $('.amtField'+record.qtyName);
            sendFlag = false;
            return false;

        }else if( parseFloat( record.pricePerPiece ) < parseFloat( record.costPerPiece ) ){
            toastr.error('Item Price Can not be smaller then cost Price '+record.qtyName);
            $('.amtField'+record.qtyName);
            sendFlag = false;
            return false;
        }
    });

    // console.log(itemCostCalJson);return false;

    if( sendFlag ){
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addUpdateItemCostCalculation',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,13),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            inqId : localStorage.POTGclickInqId,
            itemId : GBLITEMID,
            itemCost : itemCostCalJson,
        }
        // console.log(postData);return false;
        commonAjax(COMMONURL, postData,saveCalculationCallback,"Please Wait... Getting Inquiry Data");

        function saveCalculationCallback(flag,data){

            if (data.status == "Success" && flag) {
            
                console.log(data);
                toastr.success('Your Cost Calculation Successfully Saved.');
                $('#costCalModal').modal('hide');
                $('#showItemDetailModal').modal('hide');
                
                getInqLog();    // CALL AN INQUIRY LOG API

            } else {
                if (flag)  
                    displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_UPDATE_CALCULATION );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}


function addNote() {
    
    if( checkAuth(4,15) == -1 ){
        toastr.error(NOAUTH);
        return false;
    }   

    if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Status');
        return false;
    }

    var noteText = $('#noteArea').val();

    if( noteText == "" ){
        toastr.warning("Please add note");
        $('#noteArea').focus();
        return false;
    }
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'addEditNoteInInquiry',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        note : noteText,
        noteId : GBL_EDIT_NOTE_ID
    }

    if( GBL_EDIT_NOTE_ID != "-1" && GBL_EDIT_NOTE_ID != undefined ){
        commonAjax(COMMONURL, postData, editNoteCallBack,"Please Wait... Getting Inquiry Data");
    }else{
        commonAjax(COMMONURL, postData, addNoteCallBack,"Please Wait... Getting Inquiry Data");
    }

    function addNoteCallBack(flag,data){

        if (data.status == "Success" && flag) {
                    
            toastr.success('Note Added Successfully');
            GBL_NOTE_LIST = data.data ;
            $('#noteArea').val('');  
            renderNoteData();  

            getInqLog();    // CALL AN INQUIRY LOG API

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_UPDATE_NOTE );
            else
                toastr.error(SERVERERROR);
        }
    }

    function editNoteCallBack(flag,data){

        if (data.status == "Success" && flag) {
                    
            toastr.success('Note Updated Successfully');
            GBL_NOTE_LIST = data.data ;     
            GBL_EDIT_NOTE_ID = "-1";
            $('#noteArea').val('');  
            renderNoteData();

            getInqLog();    // CALL AN INQUIRY LOG API

        } else {
            if (flag)  
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_UPDATE_NOTE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderNoteData() {
   
   var theader = '<table class="display table datatables-alphabet-sorting" id="noteTble">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Note</th>'+
                            '<th>Created By</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';

    var newRow = '';
    var tfoot = '</tbody></table>';
    if(GBL_NOTE_LIST != "No record found" && GBL_NOTE_LIST != null){

        GBL_NOTE_LIST.forEach(function( record , index ){

            newRow += '<tr>'+
                            '<td>'+ (index+1)  +'</td>'+
                            '<td>'+ record.note  +'</td>'+
                            '<td>'+ record.createdByName +'</td>'+
                            '<td>'+ 
                             '<a href="javascript:void(0)" onclick="editNote(this)" noteId="'+ index +'" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>&nbsp;'+
                             // '<a href="javascript:void(0)" onclick="DeleteNote(this)" noteId="'+ index +'" class="btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                             '</td>'+
                      '</tr>';
        });
    }              

    $('#noteTable').html( theader + newRow + tfoot );
    $('#noteTble').dataTable({ "stateSave": true });
}

var GBL_EDIT_NOTE_ID = "-1";
function editNote(context){

    if( checkAuth(4, 15, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;

    }

    GBL_EDIT_NOTE_ID = $(context).attr('noteId');
    $('#noteArea').val( GBL_NOTE_LIST[GBL_EDIT_NOTE_ID].note );
}

function tabChange(){

}



function openGalleryTab(context){

    GBL_PRJCT_ID = getData[0].exhibId;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'projectAttachmentListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(50,200),
        projectId:GBL_PRJCT_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, openGalleryTabCallback,"Please Wait... Getting Dashboard Detail");

    function openGalleryTabCallback(flag,data){

        var options = "<option value='-1'> -- All Project Status -- </option>";
        if (data.status == "Success" && flag) {

            GBL_GALLARY_ARR = data.data;
            renderGallaryData();

        } else {

            GBL_GALLARY_ARR = [];
            renderGallaryData();
            $('#galleryTagList').html( options ).selectpicker('refresh');

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_GALLERY );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function changeGallery(){

    renderGallaryData();
}

function renderGallaryData(){

    var imageList = "";

    GBL_GALLARY_ARR.forEach( function( record , index ){

        if( record.gallary != "No record found" ){

            record.gallary.forEach(function( innrRecrd , innrIndx ){

                var clsHide = "hide";
                if( innrIndx == "0" ){
                    clsHide = "";
                }
                imageList += '<div class="col-sm-3 col-xs-6 col-md-3 '+ clsHide +'">'+
                                '<a class="thumbnail fancybox tagName_'+ index +'" title="'+ innrRecrd.attachDesc +'" rel="ligthbox" href="'+ innrRecrd.fileLink +'">'+
                                    '<div class="blockImage">'+
                                        '<img class="img-responsive" alt="" src="'+ innrRecrd.fileLink +'" />'+
                                    '</div>'+
                                    '<div class="text-center">'+
                                        '<small class="text-muted"> '+ record.tagName + '<i href="#" class="viewImg fa fa-eye text-white"></i></small>'+                                        
                                    '</div>'+ 
                                '</a>'+
                            '</div>';
            })
        }
    });

    $('#galleryListing').html( imageList );

    GBL_GALLARY_ARR.forEach( function( record , index ){
        $(".tagName_"+index).fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });
}

function openPrintProduction(){
  
    var document_print = window.open("production.html", 'title', 'height=800,width=1600'); 
    localStorage.inqDetGetData = JSON.stringify( getData[0] );
    document_print.onload = function() { 
       document_print.print(); 
       document_print.close();
    }
}

function getInqLog(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getLogReportByInqId',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        inqId:getData[0].inqId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getInqLogCallback,"Please Wait... Getting Dashboard Detail");

    function getInqLogCallback(flag,data){

        if (data.status == "Success" && flag) {

            GBL_INQ_LOG_ARR = data.data;
            $('#inqLogDiv').show();
            renderInquiryLog(); // RENDER INQ LOG

        } else {

            GBL_INQ_LOG_ARR = [];
            $('#inqLogDiv').hide();

            if (flag){
                //displayAPIErrorMsg( data.status , GBL_ERR_NO_GALLERY );
            }else
                toastr.error(SERVERERROR);
        }
    }
}

function renderInquiryLog(){

    if( GBL_INQ_LOG_ARR != "No record found" ){

        var logList = "";
        GBL_INQ_LOG_ARR.forEach( function( record , index ){

            logList += '<li class="tl-item">'+
                             '<div class="tl-wrap active">'+
                                '<div class="tl-content panel-1 padder b-a" style="margin-top: 23px;">'+
                                   '<span class="arrow left pull-up"></span>'+
                                   '<div><i class="fa fa fa-comments"></i> '+ record.logData +'</div>'+
                                   '<div class="customInqActListing"><i class="fa fa fa-user"></i> By '+ record.createdByName +'</div>'+
                                   '<div class="customInqActListing"><i class="fa fa-clock-o"></i> '+ record.time +'</div>'+
                                '</div>'+
                             '</div>'+
                          '</li>';
        });
        $('#inqLogData').html( logList );
        $('#inqLogDiv').show();
    }
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 08-MAY-2018 BY VISHAKHA TAK
function initSelfTour(){

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: ".customer-Card",
            title: "Customer Detail",
            content: "This is the customer's details area!!",
            placement: 'bottom'
        },
        {
            path: "",
            element: "#custName",
            title: "Customer Name",
            content: "This is the name of customer on which the inquiry has been created!!",
            placement: 'right',
        },
        {
            path: "", // REDIRECTS TO THIS PATH
            element: "#copyInqBtn",
            title: "Copy Inquiry",
            content: "If you wish to copy this inquiry, click on this button",
            placement: 'right',
        },
        {
            path: "", 
            element: "#editMoreInfoBtn",
            title: "View Info",
            content: "Click on this !!</br> And it will display other info regarding this inquiry and custom fields",
            placement: 'right',
        },
        {
            path: "", 
            element: ".assignedBox",
            title: "Inquiry Assigned To",
            content: "Displays user to whom this inquiry is assigned ! <br/> You can change it !",
            placement: 'right',
        },
        {
            path: "", 
            element: ".changeStatusDiv",
            title: "Inquiry Status",
            content: "Displays status of current inquiry ! <br/> You can change it !",
            placement: 'right',
        },
        {
            path: "", 
            element: ".closerByDiv",
            title: "Closure By",
            content: "Displays closure date of current inquiry !",
            placement: 'right',
        },
        {
            path: "", 
            element: ".potAmtDiv",
            title: "Potential Amount",
            content: "Displays potential amount of current inquiry !",
            placement: 'right',
            onNext: function (tour){
                $('#itemListTabBtn').click();
            }
        },
        {
            path: "", 
            element: "#itemLisTab",
            title: "Cart Items",
            content: "Displays items available in cart for current inquiry !",
            placement: 'top',
            onNext: function (tour){
                $('#noteBtn').click();
            },
            onPrev:function (tour){
                $('#itemListTabBtn').click();
            }
        },
        {
            path: "", 
            element: "#noteTable",
            title: "Notes",
            content: "Displays notes added for current inquiry  !",
            placement: 'top',
            onNext: function (tour){
                $('#actListBtn').click();
            },
            onPrev:function (tour){
                $('#itemListTabBtn').click();
            }
        },
        {
            path: "", 
            element: "#actListTab",
            title: "Activity",
            content: "Displays activities created on current inquiry !<br/> New activity can also be created !!",
            placement: 'top',
            onNext: function (tour){
                $('#propBtn').click();
            },
            onPrev:function (tour){
                $('#noteBtn').click();
            }
        },
        {
            path: "", 
            element: "#proposalListTab",
            title: "Proposal",
            content: "Displays proposals created on current inquiry !<br/> New proposal can also be created !!",
            placement: 'top',
            onNext: function (tour){
                $('#attachBtn').click();
            },
            onPrev:function (tour){
                $('#actListBtn').click();
            }
        },
        {
            path: "", 
            element: "#attachmentsListTab",
            title: "Attachments",
            content: "Displays attachments on current inquiry !<br/> New attachment can also be added !!",
            placement: 'top',
            onNext: function (tour){
                $('#allocatBtn').click();
            },
            onPrev:function (tour){
                $('#propBtn').click();
            }
        },
        {
            path: "", 
            element: "#allocationListTab",
            title: "Allocation History",
            content: "Displays allocation history of current inquiry !",
            placement: 'top',
            onPrev:function (tour){
                $('#attachBtn').click();
            }
        },
        {
            path: "", 
            element: ".c3-gauge",
            title: "POTG Score",
            content: "Displaying POTG score of inquiry in quage chart format!",
            placement: 'left',
            onPrev:function (tour){
                $('#allocatBtn').click();
            }
        },
        {
            path: "", 
            element: "#custContPersonDiv",
            title: "Contact Person",
            content: "Displaying contact persons of customer!",
            placement: 'left',
            onNext: function (tour) {
                if( GBL_INQ_LOG_ARR.length == 0 || GBL_INQ_LOG_ARR == "" ){
                    tour.end();
                }
            }
        },
        {
            path: "", 
            element: "#inqLogDiv",
            title: "Inquiry Log",
            content: "Displaying logs of current inquiry and its detail!",
            placement: 'left',
        }
    ];
   
    // Instance the tour
    instanceTour();
    // tour._options.backdrop = false

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM ZEROth ELEMENT TO PREVENT FROM DISPLAYING LAST STEP

    // Start the tour
    tour.start(true);
}

setInterval(function(){  // END TOUR WHEN REACHED TO LAST STEP
    if( tour != "" ){
        if( tour.getCurrentStep() == (tour._options.steps.length-1) ){
            tour.end();
        }
    }
},5000);