/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 10-08-2018.
 * File : centralizedInvoiceController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
GBL_CENTRALIZED_DATA = [];

function pageInitialEvents() {

    $('.menuToggle').click(function (e) {
        // $('.Sidenav, body').toggleClass('open');            
        $('.Sidenav').toggleClass('open');
        e.preventDefault();
    });
    $('.Sidenav .closeBtn').click(function (e) {
        $('.Sidenav').removeClass('open');
        e.preventDefault();
    });

    $('#filterEnrollmentDate,#filterEnrollmentDate1').val(mysqltoDesiredFormat(Date(), 'dd-MM-yyyy'));
    getCentralizedData();
}

function getCentralizedData() {

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var startDate = $('#filterEnrollmentDate').val();
    var endDate = $('#filterEnrollmentDate1').val();

    if( startDate != "" && endDate != "" ){
        //START END DATE VALIDATION
        var dateStartDate = new Date(ddmmyyToMysql(startDate));
        var dateEndDate = new Date(ddmmyyToMysql(endDate));

        if (+dateEndDate < +dateStartDate) { //dateActEndDate dateActStartDate
            toastr.warning("End Date cannot be less than Start Date");
            addFocusId('filterEnrollmentDate1');
            return false;

        } 
    }

    var postData = {
        requestCase: 'getCentraliseInvoices',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        startDate: ddmmyyToMysql(startDate),
        endDate: ddmmyyToMysql(endDate)
    };
    console.log(postData);
    commonAjax(FOLLOWUPURL, postData, getCentralizedDataCallback, "Please wait...Adding your item.");

    function getCentralizedDataCallback(flag, data) {
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_CENTRALIZED_DATA = data.data;
            renderCentralizedInvoices(data);
            $('.Sidenav').removeClass('open');

        } else {

            GBL_CENTRALIZED_DATA = [];
            renderCentralizedInvoices(); // RENDER EVENTS

            if (flag)
                displayAPIErrorMsg(data.status, GBL_ERR_NO_FILTER);
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderCentralizedInvoices() {

    // FNB DATA START
    var eposFnbHead = '<table class="display datatables-basic" id="eposfbnDataTable">' +
            '<thead>' +
            '<tr>' +
            '<th>Voucher type</th>' +
            '<th>Voucher No.</th>' +
            '<th>Student Name</th>' +
            '<th>Date</th>' +
            '<th>Amount</th>' +
            '<th>Ref .No</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';
    var eposFnbEnd = '</tbody></table>';

    var eposFnbBody = '';
    var eposFNBData = GBL_CENTRALIZED_DATA.FNB;

    if (eposFNBData != "No record found" && eposFNBData.length != 0 && eposFNBData != undefined) {

        // FOR FNB SALES
        var eposSalesInvoice = eposFNBData.eposSalesInvoice;

        if (typeof (eposSalesInvoice) != "undefined") {
            $.each(eposSalesInvoice, function (key, value) {
                eposFnbBody += '<tr>' +
                        '<td> <a data-toggle="modal" data-billid="' + value.BILL_NO + '" data-mainType="FNB" data-subType="sales" onclick="viewInvoice(this,0);">' + value.VOUCHER_TYPE + '</a></td>' +
                        '<td>' + value.BILL_NO + '</td>' +
                        '<td>' + value.STUDENT_NAME + '</td>' +
                        '<td>' + mysqltoDesiredFormat(value.BILL_DATE, 'dd-MM-yyyy') + '</td>' +
                        '<td> <i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.BASIC_AMOUNT) + '</td>' +
                        '<td> </td>' +
                        '</td>' +
                        '</tr>';
            });
        }

        // FOR FNB RECEIPT
        var eposReceiptInvoice = eposFNBData.eposReceiptInvoice;
        if (typeof (eposReceiptInvoice) != "undefined") {
            $.each(eposReceiptInvoice, function (key, value) {
                eposFnbBody += '<tr>' +
                        '<td> <a data-toggle="modal" data-billid="' + value.BILL_NO + '" data-mainType="FNB" data-subType="receipt" onclick="viewInvoice(this,0);">' + value.VOUCHER_TYPE + '</a></td>' +
                        '<td>' + value.BILL_NO + '</td>' +
                        '<td>' + value.STUDENT_NAME + '</td>' +
                        '<td>' + mysqltoDesiredFormat(value.BILL_DATE, 'dd-MM-yyyy') + '</td>' +
                        '<td> <i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.PAYMENT_AMOUNT) + '</td>' +
                        '<td> </td>' +
                        '</td>' +
                        '</tr>';
            });
        }

        // FOR FNB CREDIT NOTE
        var eposCrNoteInvoice = eposFNBData.eposCrNoteInvoice;
        if (typeof (eposCrNoteInvoice) != "undefined") {
            $.each(eposCrNoteInvoice, function (key, value) {
                eposFnbBody += '<tr>' +
                        '<td> <a data-toggle="modal" data-billid="' + value.BILL_NO + '" data-mainType="FNB" data-subType="creditNote" onclick="viewInvoice(this,0);">' + value.VOUCHER_TYPE + '</a></td>' +
                        '<td>' + value.BILL_NO + '</td>' +
                        '<td>' + value.STUDENT_NAME + '</td>' +
                        '<td>' + mysqltoDesiredFormat(value.BILL_DATE, 'dd-MM-yyyy') + '</td>' +
                        '<td> <i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.BASIC_AMOUNT) + '</td>' +
                        '<td> </td>' +
                        '</td>' +
                        '</tr>';
            });
        }

    }

    $('#eposFnbDiv').html(eposFnbHead + eposFnbBody + eposFnbEnd);
    $('#eposfbnDataTable').dataTable();

    // FNB DATA END

    // LAUNDARY DATA START
    var eposLaundryHead = '<table class="display datatables-basic" id="eposlaundrDataTbl">' +
            '<thead>' +
            '<tr>' +
            '<th>Voucher type</th>' +
            '<th>Voucher No.</th>' +
            '<th>Student Name</th>' +
            '<th>Date</th>' +
            '<th>Amount</th>' +
            '<th>Ref .No</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';
    var eposLaundryEnd = '</tbody></table>';
    var eposLaundryBody = '';

    var eposLaundryata = GBL_CENTRALIZED_DATA.Laundry;

    if (eposLaundryata != "No record found" && eposLaundryata.length != 0 && eposLaundryata != undefined ) {

        // FOR FNB SALES
        var eposSalesInvoice = eposLaundryata.eposSalesInvoice;

        if (typeof (eposSalesInvoice) != "undefined") {
            $.each(eposSalesInvoice, function (key, value) {
                eposLaundryBody += '<tr>' +
                        '<td> <a data-toggle="modal" data-billid="' + value.BILL_NO + '" data-mainType="FNB" data-subType="sales" onclick="viewInvoice(this,0);">' + value.VOUCHER_TYPE + '</a></td>' +
                        '<td>' + value.BILL_NO + '</td>' +
                        '<td>' + value.STUDENT_NAME + '</td>' +
                        '<td>' + mysqltoDesiredFormat(value.BILL_DATE, 'dd-MM-yyyy') + '</td>' +
                        '<td class="amtDisp"> <i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.BASIC_AMOUNT) + '</td>' +
                        '<td> </td>' +
                        '</td>' +
                        '</tr>';
            });
        }

        // FOR FNB RECEIPT
        var eposReceiptInvoice = eposLaundryata.eposReceiptInvoice;
        console.log(eposReceiptInvoice);
        if (typeof (eposReceiptInvoice) != "undefined") {
            $.each(eposReceiptInvoice, function (key, value) {
                eposLaundryBody += '<tr>' +
                        '<td> <a data-toggle="modal" data-billid="' + value.BILL_NO + '" data-mainType="FNB" data-subType="receipt" onclick="viewInvoice(this,0);">' + value.VOUCHER_TYPE + '</a></td>' +
                        '<td>' + value.BILL_NO + '</td>' +
                        '<td>' + value.STUDENT_NAME + '</td>' +
                        '<td>' + mysqltoDesiredFormat(value.BILL_DATE, 'dd-MM-yyyy') + '</td>' +
                        '<td class="amtDisp"> <i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.PAYMENT_AMOUNT) + '</td>' +
                        '<td> </td>' +
                        '</td>' +
                        '</tr>';
            });
        }

        // FOR FNB CREDIT NOTE
        var eposCrNoteInvoice = eposLaundryata.eposCrNoteInvoice;
        if (typeof (eposCrNoteInvoice) != "undefined") {
            $.each(eposCrNoteInvoice, function (key, value) {
                eposLaundryBody += '<tr>' +
                        '<td> <a data-toggle="modal" data-billid="' + value.BILL_NO + '" data-mainType="FNB" data-subType="creditNote" onclick="viewInvoice(this,0);">' + value.VOUCHER_TYPE + '</a></td>' +
                        '<td>' + value.BILL_NO + '</td>' +
                        '<td>' + value.STUDENT_NAME + '</td>' +
                        '<td>' + mysqltoDesiredFormat(value.BILL_DATE, 'dd-MM-yyyy') + '</td>' +
                        '<td class="amtDisp"> <i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.BASIC_AMOUNT) + '</td>' +
                        '<td> </td>' +
                        '</td>' +
                        '</tr>';
            });
        }

    }

    $('#eposLaundryDiv').html(eposLaundryHead + eposLaundryBody + eposLaundryEnd);
    $('#eposlaundrDataTbl').dataTable();

    // LAUNDRY DATA END

    // INVOICE DATA START
    var invHead = '<table class="display datatables-basic" id="generalinvoiceDataTbl">' +
            '<thead>' +
            '<tr>' +
            '<th>Voucher type</th>' +
            '<th>Voucher No.</th>' +
            '<th>Student Name</th>' +
            '<th>Date</th>' +
            '<th>Amount</th>' +
            '<th>Ref .No</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';
    var invEnd = '</tbody></table>';
    var invBody = '';

    var invData = GBL_CENTRALIZED_DATA.invoice;
    if (invData != "No record found" && invData.length != 0 && invData != undefined) {

        $.each(invData, function (key, value) {

            invBody += '<tr>' +
                    '<td><a data-toggle="modal" data-target="#Eposreceipt" data-billid="' + value.C_INVOICE_ID + '" data-mainType="invoice" data-subType="" onclick="viewInvoice(this,0);">Invoice</a></td>' +
                    '<td>' + value.C_INVOICE_ID + '</td>' +
                    '<td>' + value.STUDENTNAME + '</td>' +
                    '<td>' + mysqltoDesiredFormat(value.INVOICE_DATE, 'dd-MM-yyyy') + '</td>' +
                    '<td class="amtDisp"><i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(value.TOTAL_CHARGES) + '</td>' +
                    '<td></td>' +
                    '</tr>';
        });
    }

    $('#generalInvDiv').html(invHead + invBody + invEnd);
    $('#generalinvoiceDataTbl').dataTable();
    // INVOICE DATA END

    // RECEIPT DATA START    
    var receiptHead = '<table class="display datatables-basic" id="receiptTbl">' +
            '<thead>' +
            '<tr>' +
            '<th>Voucher type</th>' +
            '<th>Voucher No.</th>' +
            '<th>Student Name</th>' +
            '<th>Date</th>' +
            '<th>Amount</th>' +
            '<th>Ref .No</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';
    var receiptEnd = '</tbody></table>';

    var receiptData = GBL_CENTRALIZED_DATA.receipt;
    var receiptBody = '';

    if (receiptData != "No record found" && receiptData.length != 0 && receiptData != undefined ) {

        receiptData.forEach(function (record, index) {

            var refNo = record.INVOICE_ID;
            if( record.INVOICE_ID == "-1" ){
                refNo = record.MEMBERSHIP_NUMBER;
            }
            receiptBody += '<tr>' +
                    '<td> <a data-toggle="modal" data-billid="' + record.PK_RECEIPT_ID + '" data-mainType="receipt" data-subType="" onclick="viewInvoice(this,0);"> Receipt </a></td>' +
                    '<td>' + record.PK_RECEIPT_ID + '</td>' +
                    '<td>' + record.STUDENT_NAME + '</td>' +
                    '<td>' + mysqltoDesiredFormat(record.RECEIPT_DATE, 'dd-MM-yyyy') + '</td>' +
                    '<td class="amtDisp"><i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(record.PAYMENT_AMOUNT) + '</td>' +
                    '<td>'+ refNo +'</td>' +
                    '</td>' +
                    '</tr>';

        });

    }

    $('#receiptDataHtml').html(receiptHead + receiptBody + receiptEnd);
    $('#receiptTbl').dataTable();
    // RECEIPT DATA END

    // GENERAL INVOICE DATA START
    var genInvHead = '<table class="display datatables-basic" id="generalinvoice1DataTbl">' +
            '<thead>' +
            '<tr>' +
            '<th>Voucher type</th>' +
            '<th>Voucher No.</th>' +
            '<th>Student Name</th>' +
            '<th>Date</th>' +
            '<th>Amount</th>' +
            '<th>Ref .No</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>';
    var genInvEnd = '</tbody></table>';

    var genInvData = GBL_CENTRALIZED_DATA.generalInvoice;
    var genInvBody = '';

    if (genInvData != "No record found" && genInvData.length != 0 && genInvData != undefined ) {

        genInvData.forEach(function (record, index) {

            var totalItemPrice = 0;
            var viewInvoiceItemData = record.invoiceItems;
            // LOOP FOR ITEM TOTAL PRICE
            //if(viewInvoiceItemData.constructor.toString().indexOf("Array") != -1){
            if ( viewInvoiceItemData != "" && viewInvoiceItemData != "No record found" ) {

                viewInvoiceItemData.forEach(function (record1, index1) {
                    totalItemPrice = parseFloat(totalItemPrice) + parseFloat(record1.ITEM_BASE_PRICE) + parseFloat(record1.TOTAL_TAX);
                });//ADD BY DEVIKA.parseFloat(record1.TOTAL_TAX)

            }

            genInvBody += '<tr>' +
                    '<td> <a data-toggle="modal" data-billid="' + record.C_INVOICE_ID + '" data-mainType="generalInvoice" data-subType="" onclick="viewInvoice(this,0);"> General Invoice </a></td>' +
                    '<td>' + record.C_INVOICE_ID + '</td>' +
                    '<td>' + record.STUDENTNAME + '</td>' +
                    '<td>' + mysqltoDesiredFormat(record.INVOICE_DATE, 'dd-MM-yyyy') + '</td>' +
                    '<td class="amtDisp"><i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(fixedTo2(totalItemPrice)) + '</td>' +
                    '<td> </td>' +
                    '</td>' +
                    '</tr>';

        });

    }

    $('#customeInvoiceHtml').html(genInvHead + genInvBody + genInvEnd);
    $('#generalinvoice1DataTbl').dataTable();
    // GENERAL INVOICE DATA END


}

function viewReceipt(context, type) {

    var dataId = $(context).attr('data-id');
    var viewdata = GBL_CENTRALIZED_DATA.customReceipt[dataId];

    var sname = viewdata.STUDENT_NAME;
    var ino = viewdata.PK_RECEIPT_ID;
    var idate = mysqltoDesiredFormat(viewdata.RECEIPT_DATE, 'dd-MM-yyyy');
    var naration = viewdata.NARRATION;
    var amount = Math.abs(viewdata.PAYMENT_AMOUNT).toLocaleString('en');

    $('#sname').html(sname);
    $('#ino').html(ino);
    $('#idate').html(idate);
    $('#naration').html(naration);
    $('#amount').html(amount);

    if (type == 0) {
        modalShow('receiptDetails');
    } else {
        printReceipt();
    }

}

function printReceipt() {

    alert('test');
    var data = $('#customReceipt').html();
    var myWindow = window.open('', '', 'height=500');

    myWindow.document.write(data);
    myWindow.document.close();
    setTimeout(function () {
        myWindow.print();
        myWindow.close();
    }, 250);

}

function viewInvoice(context, type) {

    var mainType = $(context).attr('data-mainType');
    var subType = $(context).attr('data-subType');
    var billId = $(context).attr('data-billid');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'getCentraliseTransactionInvoices',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        type: mainType,
        subType: subType,
        billId: billId
    };
    console.log(postData);
    //modalShow('eposCreditModal');

    commonAjax(FOLLOWUPURL, postData, viewInvoiceCallback, "Please wait...Adding your item.");

    function viewInvoiceCallback(flag, data) {

        console.log(data.data[0]);
        if (data.status == "Success" && flag) {

            if (mainType == 'FNB' || mainType == 'Laundry') {

                var billData = data.data[0];

                if (subType == 'receipt') {

                    var receiptData = data.data[0];

                    var sname = billData.STUDENT_NAME;
                    var ino = billData.PK_RECEIPT_ID;
                    var idate = mysqltoDesiredFormat(billData.BILL_DATE, 'dd-MM-yyyy');
                    var naration = billData.NARRATION;
                    var amount = Math.abs(billData.PAYMENT_AMOUNT).toLocaleString('en');

                    $('#sname').html(sname);
                    $('#ino').html(ino);
                    $('#idate').html(idate);
                    $('#naration').html(naration);
                    $('#amount').html(amount);

                    modalShow('receiptDetails');
                } else {

                    var billDate = mysqltoDesiredFormat(billData['BILL_DATE'], 'dd-MM-yyyy');
                    var billid = billData['BILL_NO'];

                    $('#billHeader').html('Bill #' + billId);
                    $('#billid').html(billid + ' / ' + billDate);

                    var tbodyEposData = '';
                    var itemData = billData['items'];

                    if (itemData.length != 0) {

                        var totalAmnt = 0;
                        itemData.forEach(function (record, index) {

                            var itemRate = record.ITEM_RATE;
                            var itemQty = record.ITEM_QTY;
                            // var itemAmount = parseFloat(itemRate * itemQty);
                            var itemAmount = record.ITEM_AMOUNT;
                            totalAmnt = parseFloat(totalAmnt) + parseFloat(itemAmount);
                            tbodyEposData += '<tr>' +
                                    '<td class="srno">' + (index + 1) + '</td>' +
                                    '<td class="item">' + record.ITEM_NAME + '</td>' +
                                    '<td class="qty">' + itemQty + '</td>' +
                                    '<td class="amt">' + itemRate + '</td>' +
                                    '<td class="amt">' + itemAmount + '</td>' +
                                    '</tr>';
                        });

                        tbodyEposData += '<tr>' +
                                '<td colspan="5" style="border:1px dashed;"></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td class="total" colspan="4">TOTAL</td>' +
                                '<td class="totalamt" id="totalAmt" colspan="3">' + totalAmnt + '</td>' +
                                '</tr>';

                        var taxData = billData['tax'];
                        console.log(taxData);
                        if (taxData != 0) {

                            taxData.forEach(function (record, index) {

                                totalAmnt = parseFloat(totalAmnt) + parseFloat(record.TAX_AMOUNT);

                                tbodyEposData += '<tr>' +
                                        '<td class="total"colspan="4">' + record.TAX_NAME + '  ' + record.TAX_PERCENTAGE + '%</td>' +
                                        '<td class="totalamt">' + record.TAX_AMOUNT + '</td>' +
                                        '</tr>';

                            });

                        }


                        tbodyEposData += '<tr>' +
                                '<td colspan="5" style="border:1px dashed;"></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td class="total" colspan="4">Net Payable</td>' +
                                '<td class="totalamt" id="netPayable">' + parseFloat(totalAmnt).toFixed(2) + '</td>' +
                                '</tr>';
                    }

                    $('#tbodyEposData').html(tbodyEposData);
                    modalShow('eposCreditModal');
                }

            } else if (mainType == 'invoice' || mainType == 'generalInvoice') {

                var invoiceData = data.data[0];
                var billNo = invoiceData['C_INVOICE_ID'];
                var billDate = mysqltoDesiredFormat(invoiceData['INVOICE_DATE'], 'dd-MM-yyyy');
                var periodFrom = invoiceData['PERIOD_FROM'];
                var periodTo = invoiceData['PERIOD_TO'];
                if (mainType == 'generalInvoice') {
                    var studName = invoiceData['FIRST_NAME'] + ' ' + invoiceData['LAST_NAME'];
                } else {
                    var studName = invoiceData['STUDENTNAME'];
                }
                var studAdd = invoiceData['ADDRESS'];
                var cityName = invoiceData['CITY_NAME'];
                var stateName = invoiceData['STATE_NAME'];
                var countryName = invoiceData['COUNTRY_NAME'];
                var memberShipNo = invoiceData['MEMBERSHIP_NUMBER'];
                var pinCode = invoiceData['PINCODE'];

                $('#invoiceNoPrint').html('#' + billNo);
                $('#invoiceDatePrint').html(billDate);
                $('#invPeriodPrint').html('Period From: ' + (periodFrom != "" ? mysqltoDesiredFormat(periodFrom, 'dd-MM-yyyy') : "") + ' To ' + (periodTo != "" ? mysqltoDesiredFormat(periodTo, 'dd-MM-yyyy') : ""));
                $('#studInvNamePrint').html(studName);
                $('#address1Print').html(studAdd);
                $('#address2Print').html(cityName + ' ' + stateName + ' ' + countryName + ' - ' + pinCode);
                $('#studMemIdPrint').html('Student Membership No : ' + memberShipNo);

                // GET ITEM DATA
                var viewInvoiceItemData = invoiceData.invoiceItems;

                var totalCgstAmount = 0;
                var totalSgstAmount = 0;
                var totalIgstAmount = 0; //ADD BY DEVIKA.27.8.2018
                var subTotal = 0;
                var preBalance = 0.00;
                var collection = 0.00;
                var balDue = 0.00;

                var newRow = '';

                // if(viewInvoiceItemData.constructor.toString().indexOf("Array") != -1){
                if ( viewInvoiceItemData != "" && viewInvoiceItemData != "No record found" ) {

                    viewInvoiceItemData.forEach(function (record, index) {

                        var cgstPer = (record.TAX_CGST != "" ? fixedTo2(record.TAX_CGST) : 0);
                        var sgstPer = (record.TAX_SGST != "" ? fixedTo2(record.TAX_SGST) : 0);
                        var igstPer = (record.TAX_IGST != "" ? fixedTo2(record.TAX_IGST) : 0);//ADD BY DEVIKA.27.8.2018
                        var itemTotal = parseFloat(record.ITEM_BASE_PRICE) * parseFloat(record.QTY_OF_SESSION);
                        var cgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(cgstPer)) / 100);
                        var sgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(sgstPer)) / 100);
                        var igstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(igstPer)) / 100);
                        totalCgstAmount = fixedTo2(parseFloat(totalCgstAmount) + parseFloat(cgstAmt));
                        totalSgstAmount = fixedTo2(parseFloat(totalSgstAmount) + parseFloat(sgstAmt));
                        totalIgstAmount = fixedTo2(parseFloat(totalIgstAmount) + parseFloat(igstAmt));//ADD BY DEVIKA.27.8.2018
                        var itemQty = parseFloat(record.QTY_OF_SESSION);
                        var itemSub = parseFloat(itemTotal) + parseFloat(cgstAmt) + parseFloat(sgstAmt) + parseFloat(igstAmt);
                        subTotal = fixedTo2(parseFloat(subTotal) + parseFloat(itemTotal));
                        var hsnCode = (record.HSN_CODE != "" ? record.HSN_CODE : "");

                        newRow += '<tr>' +
                                '<td style="padding: 5px 0 5px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">' + (index + 1) + '</td>' +
                                '<td style="padding: 5px 0px 5px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">' +
                                    '<div>' +
                                    '<div><span style="word-wrap: break-word;" id="item_name">' + record.ITEM_NAME + '</span><br><span class="item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px; " > ' + hsnCode + '</span></div>' +
                                    '</div>' +
                                '</td>' +
                                '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">' +
                                    '<div id="item_tax_amount"><span>₹</span> ' + parseFloat(record.ITEM_BASE_PRICE) + '</div>' +
                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                                '</td>' +
                                '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">' +
                                    '<div id="item_tax_amount"><span></span> ' + itemQty + '</div>' +
                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                                '</td>' +
                                '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">' +
                                    '<div id="item_tax_amount"><span>₹</span> ' + cgstAmt + '</div>' +
                                    '<div class="item-desc" style="color: #727272; font-size: 8pt;">' + cgstPer + '% </div>' +
                                '</td>' +
                                '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">' +
                                    '<div id="item_tax_amount"><span>₹</span> ' + sgstAmt + '</div>' +
                                    '<div class="item-desc" style="color: #727272; font-size: 8pt;">' + sgstPer + '% </div>' +
                                '</td>' +
                                '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                    '<div id="item_tax_amount"><span>₹</span> '+ igstAmt +'</div>'+
                                    '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ igstPer +'% </div>'+
                                '</td>'+
                                '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">' +
                                    '<span id="item_amount">' +
                                    '<span>₹</span> ' + formatPriceInIndianFormat(fixedTo2(itemTotal)) + '</span>' +
                                '</td>' +
                                '</tr>';

                    });

                    var totalPayment = 0.00;
                    var finalTotal = fixedTo2(parseFloat(subTotal) + parseFloat(totalCgstAmount) + parseFloat(totalSgstAmount) + parseFloat(totalIgstAmount));//ADD BY DEVIKA.27.8.2018 igst
                    balDue = (parseFloat(preBalance) + parseFloat(totalPayment)) - parseFloat(finalTotal);
                    var balanceDueType = (parseFloat(balDue) > 0) ? 'CR' : 'DR';
                    collection = parseFloat(totalPayment);

                    newRow += '<tr>' +
                                '<td colspan="8" style="text-align:right;">' +
                                '<table class="totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">' +
                                '<tbody>' +
                                '<tr>' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>' +
                                '<td colspan="2" id="subtotal" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> ' + formatPriceInIndianFormat(subTotal) + '</td>' +
                                '</tr>' +
                                '<tr style="height:10px;">' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>' +
                                '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> ' + formatPriceInIndianFormat(totalCgstAmount) + '</td>' +
                                '</tr>' +
                                '<tr style="height:10px;">' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>' +
                                '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> ' + formatPriceInIndianFormat(totalSgstAmount) + '</td>' +
                                '</tr>' +
                                '<tr style="height:10px;">' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>' +
                                '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> ' + formatPriceInIndianFormat(totalIgstAmount) + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>' +
                                '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>' +
                                '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> ' + formatPriceInIndianFormat(finalTotal) + '</b></td>' +
                                '</tr>' +
                                //  '<tr>'+
                                //    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Pre Balance</b></td>'+
                                //    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                                //    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ preBalance +'</b></td>'+
                                // '</tr>'+
                                '<tr>' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>' +
                                '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>CR</b></td>' +
                                '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> ' + formatPriceInIndianFormat(collection) + '</b></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right background-color: #f5f4f3; height: 40px;font-size: 9pt;" ><b>Balance Due</b></td>' +
                                '<td id="total" style="width:50px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><b>' + balanceDueType + '</b></td>' +
                                '<td id="total" style="width:120px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><span>₹</span><b> ' + formatPriceInIndianFormat(fixedTo2(Math.abs(balDue))) + '</b></td>' +
                                '</tr>' +
                                '<tr><td colspan="5"  style="font-size:16px;text-align: left;"><b><u>Terms and Conditions</u></b></td></tr>' +
                                '<tr><td colspan="5" style="padding-top:10px;padding-bottom: 4px;text-align: left;"><span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span></td></tr>' +
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>' +
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>' +
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus</span></b>".</span></td></tr>' +
                                '</tbody>' +
                                '</table>' +
                                '</td>' +
                            '</tr>';

                    $('#printItemBody').html(newRow);
                }

                modalShow('Eposreceipt');

            } else if (mainType == 'receipt') {

                var receiptData = data.data[0];

                var sname = receiptData.STUDENT_NAME;
                var ino = receiptData.PK_RECEIPT_ID;
                var idate = mysqltoDesiredFormat(receiptData.RECEIPT_DATE, 'dd-MM-yyyy');
                var naration = receiptData.NARRATION;
                var amount = Math.abs(receiptData.PAYMENT_AMOUNT).toLocaleString('en');

                $('#sname').html(sname);
                $('#ino').html(ino);
                $('#idate').html(idate);
                $('#naration').html(naration);
                $('#amount').html(amount);

                modalShow('receiptDetails');
            }
        }
    }
}

//function viewInvoice(context,type) {
//    
//    var dataId = $(context).attr('data-id');
//    var viewInvoiceData = GBL_CENTRALIZED_DATA.generalInvoice[dataId];
//    
//    var receNo = viewInvoiceData.C_INVOICE_ID;
//    var recDate = mysqltoDesiredFormat(viewInvoiceData.INVOICE_DATE,'dd-MM-yyyy') ;
//    var periodFrom = viewInvoiceData.PERIOD_FROM ;
//    var periodTo = viewInvoiceData.PERIOD_TO ;
//    var recStudName = viewInvoiceData.FIRST_NAME+' '+viewInvoiceData.LAST_NAME ;
//    var recStudAdd = viewInvoiceData.ADDRESS_LINE1+', '+viewInvoiceData.ADDRESS_LINE2+', '+viewInvoiceData.ADDRESS_LINE3 ;
//    var cityName = viewInvoiceData.CITY_NAME ;
//    var stateName = viewInvoiceData.STATE_NAME ;
//    var countryName = viewInvoiceData.COUNTRY_NAME ;
//    var pinCode = viewInvoiceData.PINCODE ;
//    var memberShipNo = viewInvoiceData.MEMBERSHIP_NUMBER ;
//    
//    
//    $('#invoiceNoPrint').html( '# ' + receNo );
//    $('#invoiceDatePrint').html(recDate);
//    $('#invPeriodPrint').html(  'Period From: '+ ( periodFrom != "" ? mysqltoDesiredFormat(periodFrom,'dd-MM-yyyy') : "" ) +' To ' + ( periodTo != "" ? mysqltoDesiredFormat(periodTo,'dd-MM-yyyy') : "") );
//    $('#studInvNamePrint').html(recStudName);
//    $('#address1Print').html(recStudAdd);
//    $('#address2Print').html( cityName + ' ' + stateName + ' ' + countryName + ' - ' + pinCode );
//    $('#studMemIdPrint').html( 'Student Membership No : ' + memberShipNo  );
//    
//    // GET ITEM DATA
//    var viewInvoiceItemData = viewInvoiceData.invoiceItems ;
//    
//    var tBody = '';
//        var totalCgstAmount = 0;
//        var totalSgstAmount = 0;
//        var subTotal = 0;
//        var preBalance = 0.00;
//        var collection = 0.00;
//        var balDue = 0.00;
//        
//    var newRow = '';
//    
//    if(viewInvoiceItemData.constructor.toString().indexOf("Array") != -1){
//        
//        viewInvoiceItemData.forEach(function( record , index ) {
//            
//            var cgstPer = ( record.TAX_CGST != "" ? fixedTo2(record.TAX_CGST) : 0 );
//            var sgstPer = ( record.TAX_SGST != "" ? fixedTo2(record.TAX_SGST) : 0 );
//            var itemTotal = parseFloat(record.ITEM_TOTAL_PRICE) * parseFloat(record.QTY_OF_SESSION);
//            var cgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(cgstPer)) / 100);
//            var sgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(sgstPer)) / 100);
//            totalCgstAmount = fixedTo2(parseFloat(totalCgstAmount) + parseFloat(cgstAmt));
//            totalSgstAmount = fixedTo2(parseFloat(totalSgstAmount) + parseFloat(sgstAmt));
//            var itemQty = parseFloat(record.QTY_OF_SESSION);
//            subTotal = fixedTo2(parseFloat(subTotal) + parseFloat(itemTotal));
//                
//            newRow += '<tr>'+
//                            '<td style="padding: 5px 0 5px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+ (index+1) +'</td>'+
//                            '<td style="padding: 5px 0px 5px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+
//                                '<div>'+
//                                   '<div><span style="word-wrap: break-word;" id="item_name">'+ record.ITEM_NAME +'</span><br><span class="item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px; " >SAC: '+record.hsnCode+'</span></div>'+
//                                '</div>'+
//                            '</td>'+
//                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
//                                '<div id="item_tax_amount"><span>₹</span> '+ parseFloat(record.ITEM_TOTAL_PRICE) +'</div>'+
//                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
//                            '</td>'+
//                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
//                                '<div id="item_tax_amount"><span></span> '+ itemQty +'</div>'+
//                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
//                            '</td>'+
//                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
//                                '<div id="item_tax_amount"><span>₹</span> '+ cgstAmt +'</div>'+
//                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
//                            '</td>'+
//                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
//                                '<div id="item_tax_amount"><span>₹</span> '+ sgstAmt +'</div>'+
//                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ sgstPer +'% </div>'+
//                            '</td>'+
//                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">'+
//                                '<span id="item_amount">'+
//                                '<span>₹</span> '+ fixedTo2(itemTotal) +'</span>'+
//                            '</td>'+
//                        '</tr>';
//        });  
//        
//        var totalPayment = 0.00;
//        var finalTotal = fixedTo2(parseFloat(subTotal) + parseFloat(totalCgstAmount) + parseFloat(totalSgstAmount));
//        balDue = (parseFloat(preBalance) + parseFloat(totalPayment)) - parseFloat(finalTotal);
//        var balanceDueType = (parseFloat(balDue) > 0) ? 'CR' : 'DR';
//        collection = parseFloat(totalPayment);
//
//        newRow += '<tr>'+
//                  '<td colspan="7" style="text-align:right;">'+
//                     '<table class="totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">'+
//                        '<tbody>'+
//                           '<tr>'+
//                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>'+
//                              '<td colspan="2" id="subtotal" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ subTotal +'</td>'+
//                           '</tr>'+
//                           '<tr style="height:10px;">'+
//                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>'+
//                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ totalCgstAmount +'</td>'+
//                           '</tr>'+
//                           '<tr style="height:10px;">'+
//                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>'+
//                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ totalSgstAmount +'</td>'+
//                           '</tr>'+
//                           '<tr>'+
//                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>'+
//                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
//                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ finalTotal +'</b></td>'+
//                           '</tr>'+
//                           //  '<tr>'+
//                           //    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Pre Balance</b></td>'+
//                           //    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
//                           //    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ preBalance +'</b></td>'+
//                           // '</tr>'+
//                             '<tr>'+
//                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>'+
//                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>CR</b></td>'+
//                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ collection +'</b></td>'+
//                           '</tr>'+
//                            '<tr>'+
//                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right background-color: #f5f4f3; height: 40px;font-size: 9pt;" ><b>Balance Due</b></td>'+
//                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><b>'+ balanceDueType +'</b></td>'+
//                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><span>₹</span><b> '+ fixedTo2( Math.abs(balDue) ) +'</b></td>'+
//                           '</tr>'+
//                            '<tr><td colspan="5"  style="font-size:16px;text-align: left;"><b><u>Terms and Conditions</u></b></td></tr>'+
//                            '<tr><td colspan="5" style="padding-top:10px;padding-bottom: 4px;text-align: left;"><span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 258848858 </span></b></td></tr>'+
//                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U92190GJ2005PLC046625 </span></b></td></tr>'+
//                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span></td></tr>'+
//                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
//                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
//                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus</span></b>".</span></td></tr>'+
//                         '</tbody>'+
//                     '</table>'+
//                  '</td>'+
//                '</tr>';
//
//        $('#printItemBody').html( newRow );
//    }
//    
//   // $('#generalInvItmData').html( newRow ) ;
//    
//    if( type == 0 ){
//        modalShow('Eposreceipt');
//    }else{
//        printReceipt();
//    }
//}

function printInvoice(modalId) {

    var data = $('#' + modalId).html();
    var myWindow = window.open('', '', 'height=800,width=1200');

    myWindow.document.write(data);
    myWindow.document.close();
    setTimeout(function () {
        myWindow.print();
        myWindow.close();
    }, 250);

}

// ADDED BY KAUSHA SHAH ON 20-08-2018 
function saveCentralisedDataToTally() {

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var tmp_auth_json = JSON.parse(localStorage.indeCampusRoleAuth);

    var tallyFlagId = tmp_auth_json.data.tallyFlagId;
    var tallyIntegrationFlag = tmp_auth_json.data.tallyIntegrationFlag;

    if (tallyIntegrationFlag != 1) {
        toastr.error(GBL_ERR_TALLY_INTEGRATION_NOT_ALLOW);
        return false;
    }

    var postData = {
        requestCase: 'saveCentralisedDataToTally',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        tallyFlagId: tallyFlagId,
        tallyIntegrationFlag: tallyIntegrationFlag

    };
    console.log(postData);

    commonAjax(FOLLOWUPURL, postData, saveCentralisedDataToTallyCallback, "Please wait...Adding your item.");

    function saveCentralisedDataToTallyCallback(flag, data) {

        console.log(data);

        if (data.status == "Success" && flag) {

            toastr.success(data.data);
            //getCentralizedData();

        } else {

            if (flag)
                toastr.error(data.data);
            else
                toastr.error(SERVERERROR);
        }

    }
}
