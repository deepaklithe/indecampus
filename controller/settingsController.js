/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 09-11-2016.
 * File : settingsController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_TOUR_STEPS = []; // GLOBAL VARIABLE FOR STEPS OF SELF TOUR

// Dashboard Configuration function added 
function configDashBoardApi(){

  var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
  var allDashCompoArr = loadauth.data.dashbordGridList;
  var userDashCompoArr = loadauth.data.userActiveGridList;

  var dashCompoActiveList = "";
  var dashCompoInActiveList = "";

  if( userDashCompoArr != "No record found" ){

      userDashCompoArr.forEach( function( record , index ){

          // ACTIVE LIST
          // var indexFound = findIndexByKeyValue( userDashCompoArr , "ID" , record.ID );

              var checkedOnOff = "checked";
              dashCompoActiveList += '<div class="SelectList portletslist col-lg-4 col-md-4 col-sm-4 col-xs-12">'+
                                  '<div class="portlet-handle">'+
                                      '<span class="ListTitle set_list" style=""><i class="fa fa-check-circle-o"></i> '+ record.VALUE +'</span>'+
                                      '<input type="checkbox" id="checkboxDashComp'+ record.ID +'" dashCompId="'+ record.ID +'" value="portletslist1" class="slider-toggle chkBoxCompo" '+checkedOnOff+' />'+
                                      '<label class="slider-viewport" for="checkboxDashComp'+ record.ID +'">'+
                                          '<div class="slider">'+
                                              '<div class="slider-button">&nbsp;</div>'+
                                              '<div class="slider-content left"><span>On</span></div>'+
                                              '<div class="slider-content right"><span>Off</span></div>'+
                                          '</div>'+
                                      '</label>'+
                                  '</div>'+
                              '</div>';
      });
      
  }

  if( allDashCompoArr != "No record found" ){

    allDashCompoArr.forEach( function( record , index ){

          // INACTIVE LIST
        var indexFound = findIndexByKeyValue( userDashCompoArr , "ID" , record.ID );
        if( indexFound == "-1" ){

          dashCompoInActiveList += '<div class="SelectList portletslist col-lg-4 col-md-4 col-sm-4 col-xs-12">'+
                              '<div class="portlet-handle">'+
                                  '<span class="ListTitle set_list"><i class="fa fa-check-circle-o"></i> '+ record.VALUE +'</span>'+
                                  '<input type="checkbox" id="checkboxDashComp'+ record.ID +'" dashCompId="'+ record.ID +'" value="portletslist1" class="slider-toggle chkBoxCompo"/>'+
                                  '<label class="slider-viewport" for="checkboxDashComp'+ record.ID +'">'+
                                      '<div class="slider">'+
                                          '<div class="slider-button">&nbsp;</div>'+
                                          '<div class="slider-content left"><span>On</span></div>'+
                                          '<div class="slider-content right"><span>Off</span></div>'+
                                      '</div>'+
                                  '</label>'+
                              '</div>'+
                          '</div>';
        }
    });
  }
  
  if( localStorage.POTGselfTourFlag == "1" ){ // LOCALSTORAGE VARIABLE FOR TOUR STARTED FROM DASHBOARD
    initSelfTour(); // INITIALIZE TOUR 
    localStorage.POTGselfTourFlag = "0";
  }
  $('#dashboardCompoActiveList').html( dashCompoActiveList );
  $('#dashboardCompoInActiveList').html( dashCompoInActiveList );
}

function saveDashboardCompo(){

  var dashBoardCompArr = "";

  $('#dashboardActInAct .SelectList .chkBoxCompo').each(function(ind, obj) {
      var currChkId = $(this).attr('id');

      if( $('#'+currChkId).prop('checked') == true ){

          if( dashBoardCompArr ){
              dashBoardCompArr += ","+$('#'+currChkId).attr('dashcompid');
          }else{
              dashBoardCompArr = $('#'+currChkId).attr('dashcompid');
          }
      }
  });
  // console.log(dashBoardCompArr);return false;

  var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
  var postData = {
      requestCase: 'checkUncheckDashbordUserWise',
      clientId: tmp_json.data[0].FK_CLIENT_ID,
      userId: tmp_json.data[0].PK_USER_ID,
      orgId: checkAuth(1,3),
      gridId: dashBoardCompArr,
      dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
  }
  commonAjax(FOLLOWUPURL, postData, saveDashboardCompoCallback,"Please Wait... Setting Dashboard Component");

  function saveDashboardCompoCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('Dashboard Component Updated Successfully..!');

        var tempLoadAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        tempLoadAuth.data.userActiveGridList = data.data.userActiveGridList;

        localStorage.indeCampusRoleAuth = JSON.stringify( tempLoadAuth );

        configDashBoardApi();

    } else {
      if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_DASHBOARD_COMPO );
      else
          toastr.error(SERVERERROR);
    }
  }
  
}

// INITIALIZE TOUR  - ADDED ON 08-MAY-2018 BY VISHAKHA TAK
function initSelfTour() {

    // ADD STEPS TO OCCUR FOR TOUR
    GBL_TOUR_STEPS = [
        {   
            path: "",
            element: "#dashboardCompoActiveList",
            title: "Change dashboard components",
            content: "Here are activated components which you can deactivate !!",
            placement: 'top',
        },
        {   
            path: "",
            element: "#dashboardCompoInActiveList",
            title: "Change dashboard components",
            content: "Here are deactivated components which you can activate !!",
            placement: 'top'
        },
        {   
            path: "",
            element: ".set_but",
            title: "Save dashboard components",
            content: "Click on this and save your components!!",
            placement: 'top'
        },
        {   
            path: "home.html",
            element: ".set_but",
            title: "View dashboard components",
            content: "Dashboard Component Grids!!",
            placement: 'top'
        }
    ];

    // Instance the tour
    instanceTour();

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM ZEROth ELEMENT TO PREVENT FROM DISPLAYING LAST STEP

    // Start the tour
    tour.start(true);
}
