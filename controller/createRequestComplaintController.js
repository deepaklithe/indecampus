    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 03-08-2018.
 * File : createRequestComplaintController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_CATEGORY = [];
var GBL_SUB_CATEGORY = [];
var reqComplaintData = []; // FOR EDIT

// PAGE LOAD EVENTS
function pageInitialEvents() {
	autoCompleteForStudentId('studentName',saveStudentNameData);
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var departmentData = roleAuth.data.departmentList;
    setOption('0','requestComplaintDept',departmentData,'Select Department');
    $('#requestComplaintDept').selectpicker('refresh');
    if( localStorage.indeCampusUpdateReqId != "" && localStorage.indeCampusUpdateReqId != undefined ){
        getRequestComplaint();
    }
}

// CREATE REQUEST OR COMPLAINT
function createRequestComplaint(){
 
	var studentName = $('#studentName').val();
	var studId = $('#studentName').attr('studId');
	var requestComplaintType = $('#requestComplaintType').val();
	var requestComplaintDept = $('#requestComplaintDept').val();
    var requestComplaintCategory = $('#requestComplaintCategory').val();
    var requestComplaintSubCat = $('#requestComplaintSubCat').val();
    var requestComplaintDesc = $('#requestComplaintDesc').val();
	   
    if( studentName == "" || studId == ""  ){
        toastr.warning("Please search student");
        addFocusId('studentName');
        return false;

    }else if( requestComplaintDept == "-1" || requestComplaintDept == "" || requestComplaintDept == null  ){
        toastr.warning("Please select department");
        addFocusId('requestComplaintDept');
        return false;

    }else if( requestComplaintCategory == "-1" || requestComplaintCategory == "" || requestComplaintCategory == null  ){
        toastr.warning("Please select category");
        addFocusId('requestComplaintCategory');
        return false;

    }else if( requestComplaintSubCat == "-1" || requestComplaintSubCat == "" || requestComplaintSubCat == null  ){
        toastr.warning("Please select sub category");
        addFocusId('requestComplaintSubCat');
        return false;

    }else if( requestComplaintDesc == "" ){
        toastr.warning("Please enter request / complaint description");
        addFocusId('requestComplaintDesc');
        return false;

    }
    studentName = studentName.split('-')[0].trim();

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "addComplaint",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22 , 89),
        studentId : studId,
        type : requestComplaintType,
        departmentId : requestComplaintDept,
        departmentCatId : requestComplaintCategory,
        departmentSubCatId : requestComplaintSubCat,
        description : requestComplaintDesc,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, createRequestComplaintCallback,"Please wait...Adding rooms to your item.");

    function createRequestComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            var type = "request";
            if( requestComplaintType == "2" ){
                type = "complaint";
            }
            toastr.success("Your "+type+" added successfully");
            resetRequestComplaint();
            // navigateToReqComplaintList();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_REQ_COMP );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RESET REQUEST / COMPLAINT FIELDS
function resetRequestComplaint(){

    $('#studentName').val('');
    $('#studentName').attr( 'studId' , '' ); 
	$('#requestComplaintType').val( $("#requestComplaintType option:first").val() );
	$('#requestComplaintType').selectpicker('refresh');
	$('#requestComplaintDept').val('-1').selectpicker('refresh');
	$('#requestComplaintCategory').val('-1').selectpicker('refresh');
	$('#requestComplaintSubCat').val('-1').selectpicker('refresh');
	$('#requestComplaintDesc').val('');
    $('#requestComplaintDept').closest('div').find('button').css('pointer-events','');
    $('#addReqBtn').html('<i class="fa fa-floppy-o"></i> Save');
    $('#addReqBtn').attr('onclick','createRequestComplaint()');
    localStorage.removeItem('indeCampusUpdateReqId');
	
}

// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID 
function autoCompleteForStudentId(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( index != -1 ){
                setTimeout(function() {
                    $('#'+id).val( ITEMS[index].studentName + ' - ' + ITEMS[index].membershipNumber );
                    $('#'+id).attr( 'studId' , ITEMS[index].id ); 
                },1);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId:  checkAuth(11,45),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            var tempJson = {
                                id: data.studId,
                                name: data.studName + ' - ' + data.membershipNumber,
                                data: data.studName,
                                studentName : data.studName,
                                membershipNumber : data.membershipNumber,
                            }
                            ITEMS.push(tempJson);

                           
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}

// GET CATEGORY ON SELECTED DEPARTMENT
function getDepartmentCategory(context){

    var department = $(context).val();

    if( department != "" && department != "-1" ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : "departmentCategoryList",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            departmentId : department,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(FOLLOWUPURL, postData, getDepartmentCatCallback,"Please wait...Adding rooms to your item.");

        function getDepartmentCatCallback( flag , data ){
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_CATEGORY = data.data;
                renderDeptCategoryDropdown(); // RENDER CATEGORY DROPDOWN

            }else{

                GBL_CATEGORY = [];
                renderDeptCategoryDropdown(); // RENDER CATEGORY DROPDOWN
                GBL_SUB_CATEGORY = [];
                renderDeptSubCatDropdown(); // RENDER CATEGORY DROPDOWN

                if (flag){
                  displayAPIErrorMsg( data.status , GBL_ERR_NO_DEPT_CAT );
                }
                else{
                    toastr.error(SERVERERROR);
                }
            }
        }

    }else{
        GBL_CATEGORY = [];
        renderDeptCategoryDropdown(); // RENDER CATEGORY DROPDOWN
        GBL_SUB_CATEGORY = [];
        renderDeptSubCatDropdown(); // RENDER CATEGORY DROPDOWN
    }

}

// RENDER CATEGORY DROPDOWN
function renderDeptCategoryDropdown(){

    var options = '<option value="-1">Select Category</option>';
    if( GBL_CATEGORY != "" && GBL_CATEGORY != "No record found" ){

        GBL_CATEGORY.forEach(function( record , index ){

            options += '<option value="'+ record.departmentCategotyId +'">'+ record.departmentCatName +'</option>';

        });

    }
    $('#requestComplaintCategory').html( options );
    $('#requestComplaintCategory').selectpicker('refresh');
    if( reqComplaintData != "" && reqComplaintData.lenth != 0 && reqComplaintData != "No record found" ){
        $('#requestComplaintCategory').val( reqComplaintData[0].departmentCatId ).selectpicker('refresh');
        $('#requestComplaintCategory').trigger('change');
    }

}
// GET CATEGORY ON SELECTED DEPARTMENT
function getDepartmentSubCategory(context){

    var requestComplaintCategory = $(context).val();
    var requestComplaintDept = $('#requestComplaintDept').val();

    if( requestComplaintCategory != "" && requestComplaintCategory != "-1" ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : "departmentCategoryList",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            departmentId : requestComplaintDept,
            categotyId : requestComplaintCategory,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(FOLLOWUPURL, postData, getDepartmentCatCallback,"Please wait...Adding rooms to your item.");

        function getDepartmentCatCallback( flag , data ){
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_SUB_CATEGORY = data.data;
                renderDeptSubCatDropdown(); // RENDER SUB CATEGORY DROPDOWN

            }else{

                GBL_SUB_CATEGORY = [];
                renderDeptSubCatDropdown(); // RENDER SUB CATEGORY DROPDOWN

                if (flag){
                  displayAPIErrorMsg( data.status , GBL_ERR_NO_DEPT_SUB_CAT );
                }
                else{
                    toastr.error(SERVERERROR);
                }
            }
        }

    }else{
        GBL_SUB_CATEGORY = [];
        renderDeptSubCatDropdown(); // RENDER CATEGORY DROPDOWN
    }

}

// RENDER SUB CATEGORY DROPDOWN
function renderDeptSubCatDropdown(){

    var options = '<option value="-1">Select Sub Category</option>';
    if( GBL_SUB_CATEGORY != "" && GBL_SUB_CATEGORY != "No record found" ){

        GBL_SUB_CATEGORY.forEach(function( record , index ){

            options += '<option value="'+ record.departmentCategotyId +'">'+ record.departmentCatName +'</option>';

        });

    }
    $('#requestComplaintSubCat').html( options );
    $('#requestComplaintSubCat').selectpicker('refresh');
    if( reqComplaintData != "" && reqComplaintData.length != 0 && reqComplaintData != "No record found" ){
        $('#requestComplaintSubCat').val( reqComplaintData[0].departmentSubCatId ).selectpicker('refresh');
        $('#requestComplaintSubCat').trigger('change');
    }

}

// GET REQUEST / COMPLAINT LISTING DATA
function getRequestComplaint(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "getComplaintsData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22,90),
        complaintId : localStorage.indeCampusUpdateReqId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestComplaintCallback,"Please wait...Adding rooms to your item.");

    function getRequestComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            reqComplaintData = data.data;
            renderDataForEdit(reqComplaintData );

        }else{


            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_REQ_COM );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER DATA TO UPDATE
function renderDataForEdit(reqComplaintData){

    if( reqComplaintData != "" && reqComplaintData != "No record found" ){

        var reqComplaintDetails = reqComplaintData[0];
        $('#studentName').val( reqComplaintDetails.studName );
        $('#studentName').attr( 'studId' , reqComplaintDetails.studId ); 
        $('#requestComplaintType').val( reqComplaintDetails.type );
        $('#requestComplaintType').selectpicker('refresh');
        $('#requestComplaintDept').val( reqComplaintDetails.departmentId ).selectpicker('refresh');
        $('#requestComplaintDept').trigger('change');
        $('#requestComplaintCategory').val( reqComplaintDetails.departmentCatId ).selectpicker('refresh');
        $('#requestComplaintCategory').trigger('change');
        $('#requestComplaintSubCat').val( reqComplaintDetails.departmentSubCatId ).selectpicker('refresh');
        var reqCompDesc = ( reqComplaintDetails.transData != "" && reqComplaintDetails.transData != "No record found" ? reqComplaintDetails.transData[0].description : "" );
        $('#requestComplaintDesc').val( reqCompDesc);

        // IF ASSIGNED TO ANY USER , THEN DO NOT ALLOW TO EDIT DEPT
        if( reqComplaintDetails.assignUserId != "0" && reqComplaintDetails.assignUserId != "" && reqComplaintDetails.assignUserId != null ){
            $('#requestComplaintDept').closest('div').find('button').css('pointer-events','none');
        }

        $('#addReqBtn').html('<i class="fa fa-floppy-o"></i> Update');
        $('#addReqBtn').attr('onclick','updateReqComplaint()');
    }

}

// CREATE REQUEST OR COMPLAINT
function updateReqComplaint(){
 
    var studentName = $('#studentName').val();
    var studId = $('#studentName').attr('studId');
    var requestComplaintType = $('#requestComplaintType').val();
    var requestComplaintDept = $('#requestComplaintDept').val();
    var requestComplaintCategory = $('#requestComplaintCategory').val();
    var requestComplaintSubCat = $('#requestComplaintSubCat').val();
    var requestComplaintDesc = $('#requestComplaintDesc').val();
       
    if( studentName == "" || studId == ""  ){
        toastr.warning("Please search student");
        addFocusId('studentName');
        return false;

    }else if( requestComplaintDept == "-1" || requestComplaintDept == "" || requestComplaintDept == null  ){
        toastr.warning("Please select department");
        addFocusId('requestComplaintDept');
        return false;

    }else if( requestComplaintCategory == "-1" || requestComplaintCategory == "" || requestComplaintCategory == null  ){
        toastr.warning("Please select category");
        addFocusId('requestComplaintCategory');
        return false;

    }else if( requestComplaintSubCat == "-1" || requestComplaintSubCat == "" || requestComplaintSubCat == null  ){
        toastr.warning("Please select sub category");
        addFocusId('requestComplaintSubCat');
        return false;

    }else if( requestComplaintDesc == "" ){
        toastr.warning("Please enter request / complaint description");
        addFocusId('requestComplaintDesc');
        return false;

    }
    var transId = "";
    if( reqComplaintData[0].transData != "" && reqComplaintData[0].transData != "No record found" ){
        transId = reqComplaintData[0].transData[0].transId;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "addComplaint",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22 , 91),
        studentId : studId,
        type : requestComplaintType,
        departmentId : requestComplaintDept,
        departmentCatId : requestComplaintCategory,
        departmentSubCatId : requestComplaintSubCat,
        description : requestComplaintDesc,
        complaintId : localStorage.indeCampusUpdateReqId,
        complaintTransId : transId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, updateReqComplaintCallback,"Please wait...Adding rooms to your item.");

    function updateReqComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            localStorage.removeItem('indeCampusUpdateReqId');
            navigateToReqComplaintList();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_REQ_COMP );
            else
                toastr.error(SERVERERROR);
        }
    }

}