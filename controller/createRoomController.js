/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 10-07-2018.
 * File : createRoomController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_ROOM_COUNT = 1; // ROOM ROW COUNT VAR
var GBL_ROOM_ITEM_ID = ""; // SELECTED ITEM ID
var GBL_ROOM_NUM = []; // ROOM ARRAY
var GBL_UPD_INDEX = '';

// PAGE LOAD EVENTS
function pageInitialEvents() {
 	if( localStorage.indeCampusRoomDetail != undefined && localStorage.indeCampusRoomDetail != "" ){
 		// renderAvailableRooms(); // RENDER ROOMS
 		var itemData = JSON.parse(localStorage.indeCampusRoomDetail);
		GBL_ROOM_ITEM_ID = itemData.itemId;
		$('#itemName').html( itemData.itemName ); // SET ITEM NAME
		GBL_ROOM_NUM = [];
		if( itemData.roomsArr != "No record found" ){
			itemData.roomsArr.forEach(function( record , index ){
				var temp = {
					roomId : record.roomId,
					roomName : record.roomName,
					roomCapacity : record.roomCapacity
				}
				GBL_ROOM_NUM.push(temp);
			});
			$('#saveRoomBtn').html( '<i class="fa fa-floppy-o"></i> Update' );
		}
		renderRoomList();
 	}else{
 		toastr.warning("No room found to add room numbers");
 		setTimeout(function(){
			navigateToItem('room');
 		},300);
 		
 	}
}

// RENDER ROOMS IF AVAILABLE
function renderAvailableRooms(){

	var itemData = JSON.parse(localStorage.indeCampusRoomDetail);
	GBL_ROOM_ITEM_ID = itemData.itemId;
	$('#itemName').html( itemData.itemName ); // SET ITEM NAME
	if( itemData.roomsArr != "No record found" ){
		var count = 0;
		GBL_ROOM_COUNT = 1;
		for(var i = 0; i < itemData.roomsArr.length ; i++){
			addRoomRow(); // ADD ROOMS ROW
			count = count + 1;
			$('#roomName_'+count).val( itemData.roomsArr[i].roomName ); // SET ROOM NAME
			$('#roomName_'+count).attr( 'roomId' , itemData.roomsArr[i].roomId );
			$('#deleteItem_'+count).attr( 'roomId' , itemData.roomsArr[i].roomId );
		}
		// REMOVE EXTRA CREATED ROWS
		var rooms = $('.roomName');
		var newCnt = 0;
		for( var j = 0; j < rooms.length ; j++ ){
			newCnt = newCnt + 1;
			if( newCnt > itemData.roomsArr.length ){
				$(rooms[j]).parent().parent().parent().remove();
				GBL_ROOM_COUNT = GBL_ROOM_COUNT - 1;
			}
		}
	}	

}

// ADD A NEW ROOM 
function addRoomRow(){

	GBL_ROOM_COUNT = GBL_ROOM_COUNT + 1; // INCREMENT IN COUNT VAR
	createRoom();

}

// DISPLAY ADDED ROOM ROW
function createRoom(){

	var innerRow = "";
	var currIndex = GBL_ROOM_COUNT;
	innerRow += '<div class="row margin-bottom-10 itemFieldsOther currRoomIndex_'+currIndex+'" id="roomRowDiv_'+GBL_ROOM_COUNT+'">'+
		            '<div class="col-md-6 col-sm-6">'+
		                '<div class="form-group">'+
		                    '<label class="control-label"><i class="fa fa-bed"></i> Room Number <span class="mandatory">*</span></label>'+
		                    '<input type="text" value="" name="Item Name" id="roomName_'+GBL_ROOM_COUNT+'" class="form-control roomName room_'+currIndex+'" placeholder="Room No.(eg. A 101)">'+
		                '</div>'+
		            '</div>'+
			        '<div class="col-md-3 col-sm-3">'+
		                '<div class="form-group" style="margin-top: 32px; margin-left: 10px;">'+
		                    '<a class="btn btn-sm btn-danger" currIndex="'+currIndex+'" data-toggle="tooltip" data-original-title="Remove Room No." id="deleteItem_'+GBL_ROOM_COUNT+'" onclick="deleteRoom(this);"><i class="fa fa-trash"></i></a>'+
		                '</div>'+
		            '</div>'+
		        '</div>';

    $('#createRoomRow').append( innerRow );

}

// DELETE SELECTED ROOM FROM THE ITEM 
function deleteRoom(context){

	var currIndex = $(context).attr('currIndex');
	var roomId = $(context).attr('roomId');

	if( roomId == undefined || roomId == "undefined" || roomId == "" ){ // DELETE ROOM
		$('#roomRowDiv_'+currIndex).remove();
		GBL_ROOM_COUNT = GBL_ROOM_COUNT - 1;

	}else{	 // DELETE ALREADY EXISTING ROOM DURING UPDATE

		var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

	    var postData = {
	        requestCase : 'deleteRoom', 
	        clientId : tmp_json.data[0].FK_CLIENT_ID,
	        userId : tmp_json.data[0].PK_USER_ID,
	        orgId : checkAuth(13, 56),
	        roomId : roomId,
	        itemId : GBL_ROOM_ITEM_ID,
	        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
	    };
	    // console.log(postData); return false;
	    commonAjax(FOLLOWUPURL, postData, deleteRoomCallback,"Please wait...Adding rooms to your item.");

	    function deleteRoomCallback( flag , data ){
	        if (data.status == "Success" && flag) {

	            console.log(data.data);
	            var roomsData = data.data;
	            var itemData = JSON.parse(localStorage.indeCampusRoomDetail); 
	            itemData.roomsArr = roomsData;
	            localStorage.indeCampusRoomDetail = JSON.stringify(itemData); // UPDATE LOCALSTORAGE ROOM JSON DATA
	            $('#roomRowDiv_'+currIndex).remove(); // REMOVE ROOM ROW
				renderAvailableRooms();	// RENDER ROOMS

	        }else{
	            if (flag)
	              displayAPIErrorMsg( data.status , GBL_ERR_DELETE_ROOM );
	            else
	                toastr.error(SERVERERROR);
	        }
	    }
	}
}

// CREATE / UPDATE ROOMS IN ITEM
function saveRoom(){

	var GBL_ROOM_JSON = [];
	var rooms = $('.roomName');

	for( var j = 0; j < rooms.length ; j++ ){
		var roomName = $(rooms[j]).val();
		var roomId = $(rooms[j]).attr('roomId');

		if( roomName == "" ){
			toastr.warning("Please enter room number");
			$(rooms[j]).focus();
			return false;
		}else{

			if( roomId == undefined || roomId == "undefined" || roomId == "" ){

				var tempJson = {
					roomName : roomName
				}
				GBL_ROOM_JSON.push(tempJson);
			}else{

				var tempJson = {
					roomId : roomId,
					roomName : roomName
				}
				GBL_ROOM_JSON.push(tempJson);
			}
		}
	}
	console.log(GBL_ROOM_JSON);

	if( GBL_ROOM_JSON.length == 0 ){
		toastr.warning("Please add atleast one room to save this item.");
		$('#roomName_1').focus();
		return false;

	}
	else{

		var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

	    var postData = {
	        requestCase : 'addRoom', 
	        clientId : tmp_json.data[0].FK_CLIENT_ID,
	        userId : tmp_json.data[0].PK_USER_ID,
	        orgId : checkAuth(13, 53),
	        itemId : GBL_ROOM_ITEM_ID,
	        roomsArr : GBL_ROOM_JSON,
	        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
	    };
	    // console.log(postData); return false;
	    commonAjax(FOLLOWUPURL, postData, addRoomCallback,"Please wait...Adding rooms to your item.");

	    function addRoomCallback( flag , data ){
	        if (data.status == "Success" && flag) {

	        	toastr.success("Room numbers added successfully");
	            console.log(data.data);
	            setTimeout(function(){
	            	navigateToItem('room'); // REDIRECT TO ITEM PAGE AFTER CREATE / UPDATE ROOM
	            },200);
	            

	        }else{
	            if (flag)
	              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_ROOM );
	            else
	                toastr.error(SERVERERROR);
	        }
	    }

	}
}

// CHANGED ADDING / UPDATING ROOM NUMBERS FORMAT FROM ROW TO SIMPLE DATATABLE

// RESET ROOMS ROW
function resetRooms(){

	$('#roomName_1').val('');
	$('#roomCapacity').val('');
	$('.itemFieldsOther').remove();
	GBL_ROOM_COUNT = 1;
	GBL_ROOM_NUM = [];
	renderRoomList();
}

// ADD ROOM NUMBERS TO PARTICULAR ROOM
function addRoom(){

	var roomName = $('#roomName_1').val();
	var roomCapacity = $('#roomCapacity').val();

	if( roomName == "" ){
		toastr.warning("Please enter room number");
		$('#roomName_1').focus();
		return false;

	}else if( roomCapacity == "" ){
		toastr.warning("Please enter room capacity");
		$('#roomCapacity').focus();
		return false;

	}else if( roomCapacity != "" && parseInt(roomCapacity) < 1 ){
		toastr.warning("Please enter valid room capacity");
		$('#roomCapacity').focus();
		return false;

	}else{

		var temp = {
			roomName : roomName,
			roomCapacity : roomCapacity // ROOM CAPACTIY FIELD - ADDED ON 14-09-2018 BY VISHAKHA TAK
		}
		GBL_ROOM_NUM.push(temp);
		renderRoomList();
		$('#roomName_1').val('');
		$('#roomCapacity').val('');

	}

}

// RENDER AVAILABLE LISTS OF ROOM NUMBER AND ITS CAPACITY
function renderRoomList() {
	
	var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Room Number</th>'+
                        '<th>Room Capacity</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    if( GBL_ROOM_NUM != "" && GBL_ROOM_NUM.length > 0 ){

    	GBL_ROOM_NUM.forEach(function( record , index ){

    		var roomId = "";
    		if( record.roomId != "" && record.roomId != undefined && record.roomId != "undefined" ){
    			roomId = record.roomId;
    		}

    		newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.roomName +'</td>'+ 
                            '<td>'+ record.roomCapacity +'</td>'+ 
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple updBtn_'+ index +'" arrIndex="'+ index +'" roomId="'+ roomId +'" onclick="updateRoomName(this)"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger delBtn_'+ index +'" arrIndex="'+ index +'" onclick="deleteRoomName(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';

    	});

    }
    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable();
}

// EDIT ROOM NUMBER , ROOM CAPACITY AND SET ITS VALUE TO UPDATE
function updateRoomName(context){
	
	var arrIndex = $(context).attr('arrIndex');
	if( arrIndex != "" && arrIndex != undefined && arrIndex != "undefined" ){
		GBL_UPD_INDEX = arrIndex;
		$('#roomName_1').val( GBL_ROOM_NUM[arrIndex].roomName );
		$('#roomCapacity').val( GBL_ROOM_NUM[arrIndex].roomCapacity );
		$('#saveItemBtn').attr('data-original-title','Update Room No.');
		$('#saveItemBtn').attr('onclick','updateRoom()');
		GBL_ROOM_NUM.forEach(function( record , index ){
			$('.updBtn_'+index).removeAttr('disabled');
			$('.delBtn_'+index).removeAttr('disabled');
		});
		$('.updBtn_'+arrIndex).attr('disabled',true);
		$('.delBtn_'+arrIndex).attr('disabled',true);
	}
}

// UPDATE ROOM NUMBER AND ROOM CAPACITY
function updateRoom(){

	var roomName = $('#roomName_1').val();
	var roomCapacity = $('#roomCapacity').val();

	if( roomName == "" ){
		toastr.warning("Please enter room number");
		$('#roomName_1').focus();
		return false;
	}else if( roomCapacity == "" ){
		toastr.warning("Please enter room capacity");
		$('#roomCapacity').focus();
		return false;

	}else if( roomCapacity != "" && parseInt(roomCapacity) < 1 ){
		toastr.warning("Please enter valid room capacity");
		$('#roomCapacity').focus();
		return false;

	}else{

		if( GBL_UPD_INDEX != "" ){
			GBL_ROOM_NUM[GBL_UPD_INDEX].roomName = roomName; 
			GBL_ROOM_NUM[GBL_UPD_INDEX].roomCapacity = roomCapacity; 
			renderRoomList();
			$('#roomName_1').val('');
			$('#roomCapacity').val('');
			$('.updBtn_'+GBL_UPD_INDEX).removeAttr('disabled');
			$('.delBtn_'+GBL_UPD_INDEX).removeAttr('disabled');
			$('#saveItemBtn').attr('data-original-title','Add Room No.');
		    $('#saveItemBtn').attr('onclick','addRoom()');
			GBL_UPD_INDEX = "";
		}
		
	}

}

// DELETE ROOM NUMBER
function deleteRoomName(context){

	var arrIndex = $(context).attr('arrIndex');
	if( arrIndex != "" && arrIndex != undefined && arrIndex != "undefined" ){

		var cnfDelete = confirm("Are you sure you want to delete this room number ?");
		if( !cnfDelete ){
			return false;
		}

		if( GBL_ROOM_NUM[arrIndex].roomId != "" && GBL_ROOM_NUM[arrIndex].roomId != undefined && GBL_ROOM_NUM[arrIndex].roomId != "undefined"  ){

			var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

		    var postData = {
		        requestCase : 'deleteRoom', 
		        clientId : tmp_json.data[0].FK_CLIENT_ID,
		        userId : tmp_json.data[0].PK_USER_ID,
		        orgId : checkAuth(13, 56),
		        roomId : GBL_ROOM_NUM[arrIndex].roomId,
		        itemId : GBL_ROOM_ITEM_ID,
		        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
		    };
		    // console.log(postData); return false;
		    commonAjax(FOLLOWUPURL, postData, deleteRoomCallback,"Please wait...Adding rooms to your item.");

		    function deleteRoomCallback( flag , data ){
		        if (data.status == "Success" && flag) {

		            console.log(data.data);
		            GBL_ROOM_NUM.splice(arrIndex,1);
		            var itemData = JSON.parse(localStorage.indeCampusRoomDetail); 
		            itemData.roomsArr = data.data;
		            localStorage.indeCampusRoomDetail = JSON.stringify(itemData); // UPDATE LOCALSTORAGE ROOM JSON DATA
		            renderRoomList();
		            

		        }else{
		            if (flag)
		              displayAPIErrorMsg( data.status , GBL_ERR_DELETE_ROOM );
		            else
		                toastr.error(SERVERERROR);
		        }
		    }
	    }else{

	    	GBL_ROOM_NUM.splice(arrIndex,1);
            renderRoomList();

	    }
    }

}

// ADD / UPDATE SELECTED ROOM WITH ALL ITS ROOM NUMBERS DETAILS
function saveAllRoom(){

	if( GBL_ROOM_NUM.length == 0 ){
		toastr.warning("Please add atleast one room number to save this room.");
		$('#roomName_1').focus();
		return false;

	}
	else{

		var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

	    var postData = {
	        requestCase : 'addRoom', 
	        clientId : tmp_json.data[0].FK_CLIENT_ID,
	        userId : tmp_json.data[0].PK_USER_ID,
	        orgId : checkAuth(13, 53),
	        itemId : GBL_ROOM_ITEM_ID,
	        roomsArr : GBL_ROOM_NUM,
	        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
	    };
	    // console.log(postData); return false;
	    commonAjax(FOLLOWUPURL, postData, addRoomCallback,"Please wait...Adding rooms to your item.");

	    function addRoomCallback( flag , data ){
	        if (data.status == "Success" && flag) {

	        	if( localStorage.indeCampusRoomDetail != undefined && localStorage.indeCampusRoomDetail != "" ){
	        		toastr.success("Room numbers updated successfully");
	        	}else{
	        		toastr.success("Room numbers added successfully");
	        	}
	            console.log(data.data);
	            setTimeout(function(){
	            	navigateToItem('room'); // REDIRECT TO ITEM PAGE AFTER CREATE / UPDATE ROOM
	            },250);
	            

	        }else{
	            if (flag)
	              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_ROOM );
	            else
	                toastr.error(SERVERERROR);
	        }
	    }

	}

}

// RESET ROOM NUMBER FIELDS
function clearRoomNum(){

	$('#roomName_1').val('');
	$('#roomCapacity').val('');
	GBL_ROOM_NUM.forEach(function( record , index ){
		$('.updBtn_'+index).removeAttr('disabled');
		$('.delBtn_'+index).removeAttr('disabled');
	});
	$('#saveItemBtn').attr('data-original-title','Add Room No.');
    $('#saveItemBtn').attr('onclick','addRoom()');
}