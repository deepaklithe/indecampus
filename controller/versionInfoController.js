/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : clientProposalController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var versionContent = [];
function getVersionData(){

    $('#versionData').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getVersionInfo",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(21,82),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getVersionDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getVersionDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        versionContent = data.data[0].versionDetail;
    
        setTimeout(function(){
            tinyMCE.get('versionData').setContent( versionContent );
        },1000);

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_VERSION_INFO );
        else
            toastr.error(SERVERERROR);
    }
}
 
function saveVerInfo(){

    var versionDetail = tinyMCE.get('versionData').getContent();

    if( versionDetail == ""){
        toastr.warning('Please Enter Version Detail');
        $('#versionData').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'createUpdateVersionInfo',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(21,81),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            versionDetail : versionDetail,
        }  
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, saveVerInfoCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function saveVerInfoCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        toastr.success(' Version Detail Updated Successfully..!');
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_VERSION_INFO );
        else
            toastr.error(SERVERERROR);
    }
}


function releaseVersion(){

    var versionDetail = tinyMCE.get('versionData').getContent();

    if( versionDetail == ""){
        toastr.warning('Please Enter Version Detail for Release the Version');
        $('#versionData').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "releaseNewVersion",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(21,82),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
        commonAjax(FOLLOWUPURL, postData, releaseVersionCallback,"Please Wait... Getting Dashboard Detail");
        function releaseVersionCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data);
                toastr.success(' Version Released Successfully..!');    
            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_RELEASE_VERSION_INFO );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}