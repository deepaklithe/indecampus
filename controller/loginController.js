/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 02-07-2018.
 * File : loginController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

//alert("deepak");

var USERDETAIL = [];
var username=getCookie("c_name");
var password = getCookie("id");
if ((username!=null && username!="" && username != undefined) && (password!=null && password!="" && password != undefined)){
    $("#username").val(username);
    $("#pwd").val(password);
    $('#checkValue').attr('checked',true);
}else{
    $("#username").val('');
    $("#pwd").val('');
}

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

function doLogin() {
    //login request
    var username = $("#username").val().trim();
    var password = $("#pwd").val().trim();
    
    if($('#checkValue').is(':checked')){
        setCookie('c_name',username,1);
        setCookie('id',password,1);
    }else{
        delete_cookie('c_name');
        delete_cookie('id');
    }
    
    if (username.length <= 0) {
        toastr.warning(EMPTYUSERNAME);
        $("#username").focus();
    } else if (password.length <= 0) {
        toastr.warning(EMPTYPWD);
        $("#pwd").focus();
    } else {

        var userType = 1;
        //var mastUser = username.substring(1, 10);
        var mastUser = username.substring(username.search("litheuser"));//CHANGED BY VAIBHAV TO GET USERNAME FOR MASTER USER
        
        if( mastUser == "litheuser" ){
            userType = -1;
        }

        var postData = {
            requestCase: "userlogin",
            userName: username,
            userType : userType,
            password: md5(password),
            deviceId:generateDeviceToken()
        };
        //log(postData); return false;
        commonAjax(COMMONURL, postData,function(a,b){

            if(b.status == "Success" ){
                console.log(JSON.stringify(b));
                localStorage.indeCampusUserDetail = JSON.stringify(b); 
                // localStorage.indeCampusDashVersion = b.data[0].DEFAULT_DASH;
                localStorage.indeCampusUserType = b.data[0].USER_TYPE;
                
                if( b.data[0].PK_USER_ID == -1 ){
                    localStorage.indeCampusDeviceId = b.data[0].deviceId;
                    localStorage.indeCampusDeviceName = b.data[0].deviceName;
                    localStorage.indeCampusDeviceactivateDate = b.data[0].deviceactivateDate;
                }
                // addRemoveLoader(1);
                // loadRoleAndAuth();
                setTimeout(function()
                    { commonCallback(a,b,loadRoleAndAuth); log("ajax load auth");},
                100);
                // if( localStorage.indeCampusAlertKey == '' || localStorage.indeCampusAlertKey == undefined ){
                //     // alertRegister();
                // }else{
                    // setTimeout(function()
                    //     { commonCallback(a,b,loadRoleAndAuth); log("ajax load auth");},
                    // 100);
                // }
                
                // var moduleId = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26];               

            }else{
                toastr.warning(b.status);
            }

        }, "Please wait... Autheticating Login Data");
    }

}

function forgetPassword(){
    var username = $("#email").val();
    if(validateEmail(username)){
        var data = {
            requestCase  : 'userforgetPassword',
            userName     : username,
        };
        commonAjax(FORGETPASSURL,data,function(flag,data){
            if(data.status == "Success" && flag){
                toastr.success("Successfully send. Please check mail to find login detail.");
                $("#pane-forgot-password").hide();
                $("#pane-login").show();
                $('.login-screen').removeClass('forgot-password create-account device-register');
            }else{
                if(flag){
                    displayAPIErrorMsg( data.status , GBL_ERR_MAIL_SEND );
                }else{
                    toastr.warning(SERVERERROR);
                }
            }
        },FORGTPASSWORD);
    }else{
        toastr.warning(EMAILVALID);
        $("#email").val('');
        $("#email").focus();
        return false;
    }
    
}

function submitOTP(){

    var otp = $("#OTP").val().trim();
    if(otp.length <= 0){
        toastr.error("Please enter OTP send on your email or Mobile");
        $("#OTP").focus();
        return false;
    }else if(!isNaN(otp) && otp.length < 6 && otp.length > 6){
        toastr.error("Please enter valid OTP of 6 digit.");
        $("#OTP").focus();
        return false;
    }else{
        var postdata= {
            requestCase : 'optCheck',
            deviceName : USERDETAIL['deviceName'],
            userId : USERDETAIL['PK_USER_ID'],
            clientId : USERDETAIL['FK_CLIENT_ID'],
            deviceRegistrationType : USERDETAIL['deviceRegistrationType'],
            deviceId : USERDETAIL['deviceId'],
            opt : otp,
        };
        log(postdata);

        commonAjax(DEVICERGURL, postdata, optCheckCallBack,CHECKDEVICE);
    }
    
}

function optCheckCallBack(flag,data){
    
    if(data.status == "Success" && flag){
        localStorage.indeCampusDeviceName = data.data['deviceName'];
        localStorage.indeCampusDeviceactivateDate = data.data['activeDate'];
        localStorage.indeCampusDeviceId = data.data['deviceId'];
        localStorage.indeCampusDeviceType = data.data.deviceType;
        localStorage.indeCampusAppName = 'indecampus';

        var alertKey = btoa(randomString(10)+"|--|"+btoa(localStorage.appName)+"|--|"+btoa(localStorage.deviceType)+"|--|"+btoa(localStorage.deviceactivateDate)+"|--|"+btoa(localStorage.deviceId)+"|--|"+btoa(localStorage.deviceName)+"|--|"+randomString(10));
        localStorage.indeCampusAlertKey = alertKey;

        console.log(data);

        // Login Directly After OTP Send Start

        var postData = {
            requestCase: "userlogin",
            userName: USERDETAIL['USERNAME'],
            userType : 1,
            password: USERDETAIL['PASSWORD'],
            deviceId:generateDeviceToken()
        };
        //log(postData); return false;
        commonAjax(COMMONURL, postData,function(a,b){

            if(b.status == "Success" ){

                localStorage.indeCampusUserDetail = JSON.stringify(b); 
                // localStorage.indeCampusDashVersion = b.data[0].DEFAULT_DASH;
                localStorage.indeCampusUserType = b.data[0].USER_TYPE;
                
                if( b.data[0].PK_USER_ID == -1 ){
                    localStorage.indeCampusDeviceId = b.data[0].deviceId;
                    localStorage.indeCampusDeviceName = b.data[0].deviceName;
                    localStorage.indeCampusDeviceactivateDate = b.data[0].deviceactivateDate;
                }

                if( localStorage.alertKey == '' || localStorage.alertKey == undefined ){
                    alertRegister();
                }else{
                    setTimeout(function()
                        { commonCallback(a,b,loadRoleAndAuth); log("ajax load auth");},
                    100);
                }
                
                // var moduleId = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26];               

            }else{
                displayAPIErrorMsg( data.status , GBL_ERR_LOGIN );
                //location.reload();
            }

        }, "Please wait... Autheticating Login Data");

        // Login Directly After OTP Send Ends
        
    }else{
        if(flag){
            displayAPIErrorMsg( data.status , GBL_ERR_OTP );
            //location.reload();
        }else{
            toastr.warning(SERVERERROR);
            location.reload();
        }
    }
}
 

function getCookie(c_name)
{
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1)
    {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1)
    {
        c_value = null;
    }
    else
    {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1)
        {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

function setCookie(c_name, value, exdays)
{
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function delete_cookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

var resendTimer = "";
function startTimer(duration, display) {

    var timer = duration, minutes, seconds;
    resendTimer = setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
            $('#resendOtpBtn').attr('disabled',false);
            $('#resendOtpTxt').html('');
        }
    }, 1000);
}
 

//REMOVE LOCAL STORAGE LOAD
function removeLsAndLoad(){

    removeLocalStorageProjWise('indecampus');
    location.reload();
}

// function setDeviceName(){

//     // IF DEVICE NAME IS UNDEFINED THAN SHOW BUTTON
//     if( localStorage.indeCampusDeviceName == undefined || localStorage.indeCampusDeviceName == "undefined" ){
//         $('#resetSysBtn').show();
//     }

//     $("#devName").html(localStorage.indeCampusDeviceName);
//     $("#devType").html(localStorage.indeCampusDeviceType);
//     $("#stDate").html(mysqltoDesiredFormat(localStorage.indeCampusDeviceactivateDate,'dd-MM-yyyy'));
// }



function alertRegister(){
   
    var alertKey = btoa(randomString(10)+"|--|"+btoa(localStorage.POTGappName)+"|--|"+btoa(localStorage.POTGdeviceType)+"|--|"+btoa(localStorage.POTGdeviceactivateDate)+"|--|"+btoa(localStorage.POTGdeviceId)+"|--|"+btoa(localStorage.POTGdeviceName)+"|--|"+randomString(10));     
    var tmp_json = JSON.parse(localStorage.POTGuserDetail);
    var postData = {
        requestCase: "registerAlertId",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        aPOTGdeviceId: alertKey,
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, alertRegisterCallBack,"Please wait... getting alert register id");
   
   function alertRegisterCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var alertId = data.data.alertId;
            localStorage.POTGalertKey = alertId;
            setTimeout(function()
                { commonCallback(flag,data,loadRoleAndAuth); log("ajax load auth");},
            100);
            
        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_REG_ALERT );
            else
                toastr.error(SERVERERROR);
        }
    }
}