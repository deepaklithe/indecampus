/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : developerTypeController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var branchTransData = [];
var FinalTarget = '';
var MainTarget = '';
var TotalAmt = '';
var TotalPerc = '';
var remainAmt = '';
var remainPerc = '';
var empData = [];
var branchUserData = JSON.parse(localStorage.POTGuserTargetDetail);
var targetId = branchUserData.targetId;
var editTargetIndex = '-1';
var editTargetDistFlag = '';

function setBranchData(){

    addFocusId( 'empList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    
    var branchUserData = JSON.parse(localStorage.POTGuserTargetDetail);

    if( branchUserData != '' && branchUserData != undefined ){
        $('#BrName').html( branchUserData.branchName );
        $('#month').html( branchUserData.monthName );
        $('#year').html( branchUserData.year );
        $('#category').html( branchUserData.itemGroupName );
        
        FinalTarget = parseFloat( branchUserData.targetAmt );
        MainTarget = parseFloat( branchUserData.targetAmt );

        TotalAmt = branchUserData.targetAmt;
        TotalPerc = 100;

        remainAmt = branchUserData.targetAmt;
        remainPerc = 100;

        if( branchUserData.transDetail != 'No record found' && branchUserData.transDetail != undefined ){
            var tempTransData = branchUserData.transDetail;

            for( var i=0; i<tempTransData.length; i++ ){

                var userContribute = '';

                userContribute = ((parseFloat(tempTransData[i].amt))*100)/FinalTarget;

                remainAmt = parseFloat(remainAmt) - parseFloat(tempTransData[i].amt);
                remainPerc = parseFloat(remainPerc) - parseFloat(userContribute.toFixed(2));
                
                var tempJSON = {
                    userName : tempTransData[i].userName,
                    userId : tempTransData[i].userId,
                    contribution : userContribute,
                    amt : tempTransData[i].amt,
                    date : branchUserData.date,
                }
                branchTransData.push( tempJSON );
            }
            $('#equalDivide').remove();

            editTargetDistFlag = '1';
        }

        $('#TotAmt').html( TotalAmt );
        $('#TotPer').html('('+ TotalPerc +'%)');

        $('#remAmt').html( remainAmt );
        $('#remPer').html('('+ Math.round(remainPerc) +'%)');
    }

    renderEmpListing();
    getEmpListingData(); //Getting Render List of target data
    if( localStorage.POTGtargetTour == 1 ){
        initSelfTour();
        localStorage.POTGtargetTour = 0;
    }
}

function addSingle(){
    
    $('#equalDivide').remove();

    if( editTargetIndex > '-1' ){
        branchTransData.splice(editTargetIndex,1);
        editTargetIndex = '-1';
    }

    var userId = $('#empList').val();
    var index = findIndexByKeyValue( branchTransData , "userId" , userId );

    if( userId == "-1" ){
        toastr.warning('Please Select Employee');
        $('#empList').focus();
        return false;

    }else if( index == "-1" ){


        var typeOfAmt = $('#typeOfAmt').val();
        var targetValue = $('#targetValue').val();

        var userId = $('#empList').val();
        var userName = $('#empList option:selected').text();

        var amtTarget;
        var amtPerc;
        
        if( targetValue == 0 || targetValue == "" ){
            toastr.warning("Please Enter Value");
            $('#targetValue').focus();
            return false;
        }
        
        if( typeOfAmt == 'perc' ){

            if( parseInt(targetValue) > parseInt(remainPerc) ){
                toastr.warning("You can't Enter Target Value more than "+remainPerc +"%");
                return false;
            }
            amtTarget = FinalTarget - ((MainTarget * targetValue ) / 100) ;
            remainAmt = amtTarget;
            var percentageofGiveAmt = targetValue;
            remainPerc = remainPerc - percentageofGiveAmt;
            amtPerc = percentageofGiveAmt
            amtTarget = (MainTarget * targetValue ) / 100;
        }else{
            if( parseInt(targetValue) > parseInt(FinalTarget) ){
                toastr.warning("You can't Enter Target Value more than "+FinalTarget);
                return false;
            }
            amtTarget = FinalTarget - targetValue;
            remainAmt = amtTarget;
            var percentageofGiveAmt = ((targetValue * 100 ) / MainTarget );
            remainPerc = remainPerc - percentageofGiveAmt;
            amtPerc = percentageofGiveAmt
            amtTarget = targetValue;
        }

        $('#remAmt').html( remainAmt );
        $('#remPer').html('('+ Math.round(remainPerc) +'%)');

        tempJson = {
            userName : userName,
            userId : userId,
            contribution : amtPerc,
            amt : amtTarget,
            date : branchUserData.date,
        }
        branchTransData.push( tempJson );

        renderEmpListing();
        FinalTarget = remainAmt;

        resetData();
    
    }else{
        toastr.warning('You have already added Target for this Employee');
        return false;
    } 
}

function addEqual(){
    
    branchTransData = [];

    $('#SingleDivide').remove(); 
    $('#equalDivide').remove(); 

    var totalContribute = empData.length;

    var amtTarget = FinalTarget / totalContribute;
    var amtPerc = ((amtTarget * 100 ) / FinalTarget );

    var tempJson = [];
    for( var i=0; i<empData.length; i++ ){
        tempJson = {
            userName : empData[i].FULLNAME,
            userId : empData[i].PK_USER_ID,
            contribution : amtPerc,
            amt : amtTarget,
            date : branchUserData.date,
        }
        branchTransData.push( tempJson );
    }

    remainAmt = 0;
    remainPerc = 0;
    FinalTarget = remainAmt;

    $('#remAmt').html( remainAmt );
    $('#remPer').html('('+ Math.round(remainPerc) +'%)');

    renderEmpListing();   
}

function getEmpListingData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getLevel10UserList',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(19,74),
        type: "2",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getEmpListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getEmpListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // console.log(empData);return false;
            var brListOpt = '<option value="-1"> Select Employee </option>';
            for( var i=0;i<data.data.length; i++ ){

                // if( tmp_json.data[0].PK_USER_ID != data.data[i].PK_USER_ID ){
                    brListOpt += '<option value="'+ data.data[i].PK_USER_ID +'">'+ data.data[i].FULLNAME +'</option>'
                    empData.push( data.data[i] );
                // }
            }
            $('#empList').html( brListOpt );
            $('#empList').selectpicker();

        }else{
            // toastr.warning('No record found');
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USERLIST );
        }

    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USERLIST );
        else
            toastr.error(SERVERERROR);
    }
}

function renderEmpListing(){

    var startHtml = "<table id='targetListTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th>Name Of Employee</th>"+
                            "<th>Contribution(%)</th>"+
                            "<th>Date</th>"+
                            "<th>Qty</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";
    var inqRow = '';
    for( var i=0; i<branchTransData.length; i++ ){

        var contribution = parseFloat(branchTransData[i].contribution);
        var Amount = parseFloat(branchTransData[i].amt);
        var date = ( (branchTransData[i].date != "0000-00-00") ? mysqltoDesiredFormat(branchTransData[i].date,'dd-MM-yyyy') : "-"); 

        inqRow += '<tr>'+
                    '<td>'+ (i+1) +'</td>'+
                    '<td>'+ branchTransData[i].userName +'</a></td>'+
                    '<td><input type="text" class="form-control" placeholder="amount" value="'+ contribution.toFixed(2) +'" disabled></td>'+
                    '<td>'+ date +'</a></td>'+
                    '<td><input type="text" class="form-control" placeholder="amount" value="'+ Amount.toFixed(2) +'" disabled></td>'+
                    '<td><a href="javascript:void(0)" class="btn btn-right-mrg btn-primary btn-sm" userId="'+ branchTransData[i].userId +'" onclick="editTarget(this)"><i class="fa fa-pencil"></i></a>'+
                    '<a href="javascript:void(0)" class="btn btn-danger btn-sm" userId="'+ branchTransData[i].userId +'" onclick="deleteTarget(this)"><i class="fa fa-trash"></i></a></td>'+
                '</tr>';
                                                
    }
    var finalHtml = startHtml + inqRow + endHtml;
    $('#TargetTable').html(finalHtml);
    $('#targetListTable').DataTable({ "stateSave": true });
    
}

function createTargetOnUser(){

    if( checkAuth(6, 23, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

    if( remainPerc < 0 ){
        toastr.warning('Remaining Target Should not be in Negative');
        return false; 
    }       
    if( branchTransData.length == 0 ){
        toastr.warning('Please Add Target than Save');
        return false;
    }

    var reqCase = '';
    var checkAuthData = '';
    if( editTargetDistFlag == "1" ){
        reqCase = 'updateUserWiseTarget';
        checkAuthData = checkAuth(6,23);
    }else{
        reqCase = 'createUserWiseTarget';
        checkAuthData = checkAuth(6,21);
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: reqCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuthData,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        userWiseData: branchTransData,
        targetId: targetId,
        type: "2",
    }
    commonAjax(FOLLOWUPURL, postData, sendTargetDataCallback,"Please Wait... Getting Dashboard Detail");

    function sendTargetDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            if(data.data != 'No record found'){

                console.log(data);
                window.location.href = "activityTarget.html";

            }else{
                // toastr.warning('No record found');
                displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_TARGET_FAIL );
            }

        } else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_TARGET_FAIL );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function editTarget(context){

    var userId = $(context).attr('userId');
    var index = findIndexByKeyValue( branchTransData , 'userId' , userId );

    if( index != '-1' ){

        var amt = branchTransData[index].amt; 
        var contribution = branchTransData[index].contribution; 

        FinalTarget = parseFloat(FinalTarget) + parseFloat(amt);
        remainPerc = parseFloat(remainPerc) + parseFloat(contribution);

        $('#empList').val( branchTransData[index].userId );
        $('#empList').selectpicker( 'refresh' );
        $('#targetValue').val( branchTransData[index].amt );
        $('#typeOfAmt').val( 'amt' );
        $('#typeOfAmt').selectpicker( 'refresh' );

        editTargetIndex = index;
        addFocusId( 'empList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

function deleteTarget(context){

    var userId = $(context).attr('userId');
    var index = findIndexByKeyValue( branchTransData , 'userId' , userId );

    if( index != '-1' ){

        var amt = branchTransData[index].amt; 
        var contribution = branchTransData[index].contribution; 

        FinalTarget = parseFloat(FinalTarget) + parseFloat(amt);
        remainPerc = parseFloat(remainPerc) + parseFloat(contribution);

        var remainAmt = FinalTarget;
        
        $('#remAmt').html( remainAmt );
        $('#remPer').html('('+ Math.round(remainPerc) +'%)');

        branchTransData.splice(index,1);
        renderEmpListing();

        if( branchTransData.length == 0 ){
            FinalTarget = MainTarget;
            remainAmt = MainTarget;
            remainPerc = 100;
            $('#remAmt').html( remainAmt );
            $('#remPer').html('('+ Math.round(remainPerc) +'%)');
        }
    }
}

function resetData(){

    $('#empList').val('-1');
    $('#targetValue').val('');
    $('#empList').selectpicker('refresh');
    editTargetIndex = '-1';
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 09-MAY-2018 BY VISHAKHA TAK
function initSelfTour() {

    if( localStorage.GBL_TOUR_STEPS != undefined ){

        GBL_TOUR_STEPS = JSON.parse(localStorage.GBL_TOUR_STEPS);
        // Instance the tour
        instanceTour();
        tour._options.name = "Target Dist Tour";
        // tour._options.debug = true;

        // Initialize the tour
        tour.init();

        tour.setCurrentStep(8); // START FROM THIS STEP TO PREVENT FROM DISPLAYING LAST STORED STEP

        // Start the tour
        tour.start(true);
        localStorage.removeItem('GBL_TOUR_STEPS');
    }
    
}