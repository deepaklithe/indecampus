    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 31-07-2018.
 * File : eventMgmtDetailController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
// PAGE LOAD EVENTS
function pageInitialEvents(){

	if( localStorage.indeCampusUpdateItemId != undefined && localStorage.indeCampusUpdateItemId != "" ){
		if( localStorage.indeCampusUpdateItemJson != undefined ){
			renderItemDetails(); // RENDER ITEM DETAILS FOR UPDATE
			$('#addItemBtn').html( '<i class="fa fa-file-text-o "></i> Update' );
			$('#addItemBtn').attr('onclick','updateEventItem()');
		}
	}

}

// ADD NEW EVENT
function addEventItem() {

	var eventName = $('#eventName').val();
	var itemEventStDate = $('#itemEventStDate').val();
	var itemEventEndDate = $('#itemEventEndDate').val();
	var eventMaxCapacity = $('#eventMaxCapacity').val();
	var eventVenue = $('#eventVenue').val();
	var eventRate = $('#eventRate').val();
    var eventDescription = $('#eventDescription').val();
    var eventCgst = $('#eventCgst').val();
    var eventSgst = $('#eventSgst').val();
    var eventIgst = $('#eventIgst').val();
    var eventHsnCode = $('#eventHsnCode').val();
	var maxNosOfBook = $('#maxNosOfBook').val();

	if( eventName == "" ){
		toastr.warning("Please enter event name");
        addFocusId('eventName');
        return false;

	}else if( itemEventStDate == "" ){
		toastr.warning("Please select event start date and time");
        addFocusId('itemEventStDate');
        return false;

	}else if( itemEventEndDate == "" ){
		toastr.warning("Please select event end date and time");
        addFocusId('itemEventEndDate');
        return false;

	}else if( eventMaxCapacity == "" ){
		toastr.warning("Please enter event max capacity");
        addFocusId('eventMaxCapacity');
        return false;

	}else if( eventVenue == "" ){
		toastr.warning("Please enter event venue");
        addFocusId('eventVenue');
        return false;

	}else if( eventRate == "" ){
		toastr.warning("Please enter event rate");
        addFocusId('eventRate');
        return false;

	// }else if( eventCgst == "" ){
 //        toastr.warning("Please enter event cgst percentage");
 //        addFocusId('eventCgst');
 //        return false;

    }else if( eventCgst != "" && parseFloat(eventCgst) != 0 && parseFloat(eventCgst) > 100  ){
        toastr.warning("Cgst percentage cannot be more than 100");
        addFocusId('eventCgst');
        return false;

    // }else if( eventSgst == "" ){
    //     toastr.warning("Please enter event sgst percentage");
    //     addFocusId('eventSgst');
    //     return false;

    }else if( eventSgst != "" && parseFloat(eventSgst) != 0 && parseFloat(eventSgst) > 100  ){
        toastr.warning("Sgst percentage cannot be more than 100");
        addFocusId('eventSgst');
        return false;

    }else if( ( eventCgst != "" && parseFloat(eventCgst) != "0" || eventSgst != "" && parseFloat(eventSgst) != "0"  ) && (eventIgst != "" && parseFloat(eventIgst) != "0") ){
        toastr.warning("You cannot enter Igst alongwith Cgst or Sgst");
        return false;

    }else if( eventIgst != "" && parseFloat(eventIgst) != 0 && parseFloat(eventIgst) > 100  ){
        toastr.warning("Igst percentage cannot be more than 100");
        addFocusId('eventIgst');
        return false;

    }else if( maxNosOfBook == "" ){
        toastr.warning("Please enter maximum no of tickets for booking this event");
        addFocusId('maxNosOfBook');
        return false;

    }else if( eventHsnCode == "" ){
        toastr.warning("Please enter event hsn");
        addFocusId('eventHsnCode');
        return false;

    }else if( eventDescription == "" ){
        toastr.warning("Please enter event description");
        addFocusId('eventDescription');
        return false;

    }
    if( eventCgst == "" ){
        eventCgst = "0";
    }
    if( eventSgst == "" ){
        eventSgst = "0";
    }
    if( eventIgst == "" ){
        eventIgst = "0";
    }

	var eventStartDate = (($('#itemEventStDate').val() ).split('|')[0]).trim();
    var eventEndDate = (($('#itemEventEndDate').val() ).split('|')[0]).trim();
    var eventStartTime = (($('#itemEventStDate').val() ).split('|')[1]).trim();
    var eventEndTime = (($('#itemEventEndDate').val() ).split('|')[1]).trim();

    var eventStartDateVal = ddmmyyToMysql(eventStartDate,'yyyy-MM-dd');
    var eventEndDateVal = ddmmyyToMysql(eventEndDate,'yyyy-MM-dd');

    var dateEventStartDate = new Date(eventStartDateVal +" "+ eventStartTime);
    var dateEventEndDate = new Date(eventEndDateVal +" "+ eventEndTime);
    if( +dateEventEndDate < +dateEventStartDate ){
        toastr.warning("End Date/Time cannot be less than the Start Date / Time");
        addFocusId('itemEventEndDate');
        return false;
    }
    var eveStDateTime = eventStartDate + ' ' + eventStartTime;
    var eveEnDateTime = eventEndDate + ' ' + eventEndTime;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'addEvent', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(21, 85),
        itemName : eventName,
        price : eventRate,
        itemType : "EVENTS",
        description : eventDescription,
        qty : eventMaxCapacity,
        eventStartDateTime : eveStDateTime,
        eventEndDateTime : eveEnDateTime,
        eventVenue : eventVenue,
        cgst : eventCgst,
        sgst : eventSgst,
        igst : eventIgst,
        hsnCode : eventHsnCode,
        maxNosOfBook : maxNosOfBook,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            navigateToEventManagementList();
        }else{
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_EVENT );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER ITEM DETAILS FOR UPDATE
function renderItemDetails(){

	var updateData = JSON.parse(localStorage.indeCampusUpdateItemJson);

    console.log(updateData);
	$('#eventName').val( updateData.itemName );
	var eventStartDateTime = updateData.eventStartDateTime;
	var eventEndDateTime = updateData.eventEndDateTime;
	var newStDate = "" , newEnDate = "";
    if( eventStartDateTime != "" ){
        newStDate = mysqltoDesiredFormat(eventStartDateTime.split(' ')[0].trim(),'dd-MM-yyyy') + ' | ' + ((eventStartDateTime.split(' ')[1].trim()).split(":")[0] + ':' + (eventStartDateTime.split(' ')[1].trim()).split(":")[1]) + ' ' + eventStartDateTime.split(' ')[2].trim();
    }
    if( eventEndDateTime != "" ){
        newEnDate = mysqltoDesiredFormat(eventEndDateTime.split(' ')[0].trim(),'dd-MM-yyyy') + ' | ' + ((eventEndDateTime.split(' ')[1].trim()).split(":")[0] + ':' + (eventEndDateTime.split(' ')[1].trim()).split(":")[1]) + ' ' + eventEndDateTime.split(' ')[2].trim();
    }
	$('#itemEventStDate').datetimepicker('update' , newStDate);
	$('#itemEventEndDate').datetimepicker('update' , newEnDate);
	$('#eventMaxCapacity').val( updateData.qty );
	$('#eventVenue').val( updateData.eventVenue );
	$('#eventRate').val( fixedTo2(updateData.itemPrice) );
	$('#eventDescription').val( updateData.description );
    $('#eventCgst').val( ( updateData.cgst != "" ? fixedTo2(updateData.cgst) : 0.00 ) );
    $('#eventSgst').val( ( updateData.sgst != "" ? fixedTo2(updateData.sgst) : 0.00 ) );
    $('#eventIgst').val( ( updateData.igst != "" && updateData.igst != undefined ? fixedTo2(updateData.igst) : 0.00 ) );
    $('#eventHsnCode').val( updateData.hsnCode );
    $('#maxNosOfBook').val( updateData.maxNosOfBook );
        
	localStorage.removeItem('indeCampusUpdateItemJson');
	
}


// UPDATE EVENT
function updateEventItem() {

	var eventName = $('#eventName').val();
	var itemEventStDate = $('#itemEventStDate').val();
	var itemEventEndDate = $('#itemEventEndDate').val();
	var eventMaxCapacity = $('#eventMaxCapacity').val();
	var eventVenue = $('#eventVenue').val();
	var eventRate = $('#eventRate').val();
	var eventDescription = $('#eventDescription').val();
    var eventCgst = $('#eventCgst').val();
    var eventSgst = $('#eventSgst').val();
    var eventIgst = $('#eventIgst').val();
    var eventHsnCode = $('#eventHsnCode').val();
    var maxNosOfBook = $('#maxNosOfBook').val();

	if( eventName == "" ){
		toastr.warning("Please enter event name");
        addFocusId('eventName');
        return false;

	}else if( itemEventStDate == "" ){
		toastr.warning("Please select event start date and time");
        addFocusId('itemEventStDate');
        return false;

	}else if( itemEventEndDate == "" ){
		toastr.warning("Please select event end date and time");
        addFocusId('itemEventEndDate');
        return false;

	}else if( eventMaxCapacity == "" ){
		toastr.warning("Please enter event max capacity");
        addFocusId('eventMaxCapacity');
        return false;

	}else if( eventVenue == "" ){
		toastr.warning("Please enter event venue");
        addFocusId('eventVenue');
        return false;

	}else if( eventRate == "" ){
		toastr.warning("Please enter event rate");
        addFocusId('eventRate');
        return false;

	// }else if( eventCgst == "" ){
 //        toastr.warning("Please enter event cgst percentage");
 //        addFocusId('eventCgst');
 //        return false;

    }else if( eventCgst != "" && parseFloat(eventCgst) != 0 && parseFloat(eventCgst) > 100  ){
        toastr.warning("Cgst percentage cannot be more than 100");
        addFocusId('eventCgst');
        return false;

    // }else if( eventSgst == "" ){
    //     toastr.warning("Please enter event sgst percentage");
    //     addFocusId('eventSgst');
    //     return false;

    }else if( eventSgst != "" && parseFloat(eventSgst) != 0 && parseFloat(eventSgst) > 100  ){
        toastr.warning("Sgst percentage cannot be more than 100");
        addFocusId('eventSgst');
        return false;

    }else if( ( eventCgst != "" && parseFloat(eventCgst) != "0" || eventSgst != "" && parseFloat(eventSgst) != "0"  ) && (eventIgst != "" && parseFloat(eventIgst) != "0") ){
        toastr.warning("You cannot enter Igst alongwith Cgst or Sgst");
        return false;

    }else if( eventIgst != "" && parseFloat(eventIgst) != 0 && parseFloat(eventIgst) > 100  ){
        toastr.warning("Igst percentage cannot be more than 100");
        addFocusId('eventIgst');
        return false;

    }else if( maxNosOfBook == "" ){
        toastr.warning("Please enter maximum no of tickets for booking this event");
        addFocusId('maxNosOfBook');
        return false;

    }else if( eventHsnCode == "" ){
        toastr.warning("Please enter event hsn");
        addFocusId('eventHsnCode');
        return false;

    }else if( eventDescription == "" ){
        toastr.warning("Please enter event description");
        addFocusId('eventDescription');
        return false;

    }
    if( eventCgst == "" ){
        eventCgst = "0";
    }
    if( eventSgst == "" ){
        eventSgst = "0";
    }
    if( eventIgst == "" ){
        eventIgst = "0";
    }
	var eventStartDate = (($('#itemEventStDate').val() ).split('|')[0]).trim();
    var eventEndDate = (($('#itemEventEndDate').val() ).split('|')[0]).trim();
    var eventStartTime = (($('#itemEventStDate').val() ).split('|')[1]).trim();
    var eventEndTime = (($('#itemEventEndDate').val() ).split('|')[1]).trim();

    var eventStartDateVal = ddmmyyToMysql(eventStartDate,'yyyy-MM-dd');
    var eventEndDateVal = ddmmyyToMysql(eventEndDate,'yyyy-MM-dd');

    var dateEventStartDate = new Date(eventStartDateVal +" "+ eventStartTime);
    var dateEventEndDate = new Date(eventEndDateVal +" "+ eventEndTime);
    if( +dateEventEndDate < +dateEventStartDate ){
        toastr.warning("End Date/Time cannot be less than the Start Date / Time");
        addFocusId('itemEventEndDate');
        return false;
    }
    var eveStDateTime = eventStartDate + ' ' + eventStartTime;
    var eveEnDateTime = eventEndDate + ' ' + eventEndTime;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'editEvent', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(21, 87),
        itemId : localStorage.indeCampusUpdateItemId,
        itemName : eventName,
        price : eventRate,
        itemType : "EVENTS",
        description : eventDescription,
        qty : eventMaxCapacity,
        eventStartDateTime : eveStDateTime,
        eventEndDateTime : eveEnDateTime,
        eventVenue : eventVenue,
        cgst : eventCgst,
        sgst : eventSgst,
        igst : eventIgst,
        hsnCode : eventHsnCode,
        maxNosOfBook : maxNosOfBook,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, updateEventItemCallback,"Please wait...Adding your item.");

    function updateEventItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            localStorage.removeItem('indeCampusUpdateItemId');
            navigateToEventManagementList();
        }else{
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_ITEM );
            else
                toastr.error(SERVERERROR);
        }
    }

}