/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 09-02-2016.
 * File : activityAnlyticsController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_TYPE_WISE = [];
var GBL_MODE_WISE = [];
var GBL_CONVERT_WISE = [];
var GBL_TARGET_ACTUAL_WISE = [];
function getAnalyticData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getAnalyticActivityChartReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(FOLLOWUPURL, postData, getAnalyticDataCallback,"Please Wait... Getting Dashboard Detail");

    function getAnalyticDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log( data.data );

            GBL_TYPE_WISE = data.data.actTypeChart;
            GBL_MODE_WISE = data.data.actModeChart;
            GBL_CONVERT_WISE = data.data.actConversion;
            GBL_TARGET_ACTUAL_WISE = data.data.actComparison;
            $('#1stLi a').click();
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CHART_DATA );
            else
                toastr.error(SERVERERROR);
        }
        
    }
}

function typeWiseChart(){

    if( GBL_TYPE_WISE != "No record found" ){

      var chartData = [];
      GBL_TYPE_WISE.forEach( function( record , index ){
          var tempData = {
              name : record.statusName,
              y : parseFloat( record.actCount ),
          }
          chartData.push( tempData );
      });
      Highcharts.chart('typeWiseChrt', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: ''
          },
          colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: true,
                      format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                      style: {
                          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  }
              }
          },
          series: [{
              name: 'Count',
              colorByPoint: true,
              data: chartData
          }]
      });
    }
}

function modeWiseChart(){

    if( GBL_MODE_WISE != "No record found" ){

      var chartData = [];
      GBL_MODE_WISE.forEach( function( record , index ){
          var tempData = {
              name : record.statusName,
              y : parseFloat( record.actCount ),
          }
          chartData.push( tempData );
      });
      Highcharts.chart('modeWiseChrt', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
          title: {
              text: ''
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: true,
                      format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                      style: {
                          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  }
              }
          },
          series: [{
              name: 'Count',
              colorByPoint: true,
              data: chartData
          }]
      });
    }
}

function statConvertChart(){

    if( GBL_CONVERT_WISE != "No record found" ){

      var chartData = [];
      GBL_CONVERT_WISE.forEach( function( record , index ){
          var tempData = {
              name : record.statusName,
              y : parseFloat( record.statusCount ),
          }
          chartData.push( tempData );
      });
      Highcharts.chart('convertChrt', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE',
   '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
          title: {
              text: ''
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: true,
                      format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                      style: {
                          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  }
              }
          },
          series: [{
              name: 'Count',
              colorByPoint: true,
              data: chartData
          }]
      });
    }
}

function targetVsActChart(){

    if( GBL_TARGET_ACTUAL_WISE != "No record found" ){

      var chartCategory = [];
      var chartTargetData = [];
      var chartActualData = [];

      GBL_TARGET_ACTUAL_WISE.forEach( function( record , index ){
          
          chartTargetData.push( parseFloat( record.typeData[0].targetAct ) );
          chartActualData.push( parseFloat( record.typeData[0].actCount ) );

          chartCategory.push( record.actType );
      });

      var chartData = [];
      chartData = [
        {
          name : "Target",
          data : chartTargetData
        },
        {
          name : "Actual",
          data : chartActualData
        }
      ]

      Highcharts.chart('targetVsActChrt', {
          chart: {
              type: 'column',
              options3d: {
                  enabled: true,
                  alpha: 15,
                  beta: 15,
                  viewDistance: 25,
                  depth: 40
              }
          },
          colors: ['#4572A7', '#AA4643'],
          title: {
              text: ''
          },

          xAxis: {
              categories: chartCategory,
              labels: {
                  skew3d: true,
                  style: {
                      fontSize: '16px'
                  }
              }
          },

          yAxis: {
              allowDecimals: false,
              min: 0,
              title: {
                  text: 'Number of Activity',
                  skew3d: true
              }
          },

          tooltip: {
              headerFormat: '<b>{point.key}</b><br>',
              pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
          },

          plotOptions: {
              column: {
                  // stacking: 'normal',
                  depth: 40
              }
          },

          series: chartData
      });
    }
}