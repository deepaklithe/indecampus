/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportData = [];
var GBL_EXCELJSON = [];
var itemGroupJson = [];

function groupList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(13,50),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, groupListDataCallback,"Please Wait... Getting Dashboard Detail");
}

function groupListDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var groupData = data.data;

        var option = "";
        groupData.forEach(function( record , index ){
            option += '<option value="'+ record.PK_ITEM_GROUP_ID +'">'+ record.GROUP_NAME +'</option>';
        });
        $('#groupName').html( option );
        $('#groupName').selectpicker( 'refresh' );

    } else {

        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}
var group = false;
function getReportData(){

    reportData = [];
    GBL_EXCELJSON = [];

    var stDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var status = $('#typeStatusListing').val();
    var userId = $('#user').attr('userId');
    var groupid = $('#groupName').val();
    var targetType = $('#targetType').val();

    if( stDate != "-1" && stDate != "" ){
        stDate = ddmmyyToMysql(stDate);
    }else{
        stDate = "-1";
    }
    if( endDate != "-1" && endDate != "" ){
        endDate = ddmmyyToMysql(endDate);
    }else{
        endDate = "-1";
    }
    
    if($('#user').val().length <= 0){
        userId = "";
    }

    var dateActStartDate = new Date(stDate);
    var dateActEndDate = new Date(endDate);

    if( +dateActEndDate < +dateActStartDate ) //dateActEndDate dateActStartDate
    {
        toastr.warning("End Date cannot be less than the StartDate");
        $('#endDate').datepicker('show');
        return false;
    }

    group = false;
    var groupIds = "";
    if( groupid ){
        for( var j=0; j<groupid.length; j++ ){
            if( groupIds ){
                groupIds += ','+groupid[j];
            }else{
                groupIds = groupid[j];
            }
        }
        group = true;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getTragetPivotReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        startDate: stDate,
        endDate: endDate,
        selectedUserId: userId,
        groupId: groupIds,
        salesTargetType: targetType, // 1 = amt 2 = qty
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getReportDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        GBL_EXCELJSON = [];
        reportData = data.data;
        console.log(reportData);

        GBL_EXCELJSON = "Success";
        if( reportData ){
            if( group ){
                GeneratePivotTableWithGroup(reportData);
            }else{
                GeneratePivotTable(reportData);
            }
        }


    } else {

        reportData = [];
        GeneratePivotTable(reportData);
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
        else
            toastr.error(SERVERERROR);
    }
}

function GeneratePivotTable(mps) {

    console.log(mps);
    var Sum = $.pivotUtilities.aggregators["Sum"];
    $("#inqReportList").pivot(mps, {
        aggregator: Sum(["Amount"]),
        rows: ["User Name"],
        cols: ["Year", "Month", "Type"]
    });
    $('.sidemenu').addClass('hiden', 300);
}

function GeneratePivotTableWithGroup(mps) {
    console.log(mps);
    var Sum = $.pivotUtilities.aggregators["Sum"];
    $("#inqReportList").pivot(mps, {
        aggregator: Sum(["Amount"]),
        rows: [ "Group Name" , "User Name"],
        cols: ["Year", "Month", "Type"]
    });
    $('.sidemenu').addClass('hiden', 300);
}

function AutoCompleteForUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#user').attr('userid',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveUserDetail(){

}

function autoCompleteForGroup(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#groupName').attr('groupId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getGroupListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(13, 50),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMSGROUP= [];
                if (data.status != "No Item Group Found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.PK_ITEM_GROUP_ID,
                            name: data.GROUP_NAME,
                            data: data.GROUP_NAME,
                            clientId: data.FK_CLIENT_ID
                        }
                        itemGroupJson.push(tempJson);
                        ITEMSGROUP.push(tempJson);
                    });

                    return ITEMSGROUP;
                }
                console.log(data);
            }
        }
    });
}

function saveGroupDetail(){

}


function downLoadExcel(){
    if( GBL_EXCELJSON.length < 2 ){
        toastr.warning('Please Search Report Before Downloading...');
    }else{
        customCsvFormatSave($('#inqReportList').html(), "Sales_Target_Report.xls");
    }
}

function resetReportFilter(){
    $('#startDate').val('');
    $('#endDate').val('');
    $('#actTypeList').val('-1');
    $('#actTypeList').selectpicker('refresh');
    $('#customer').val('');
    $('#user').val('');
    $('#customer').attr('custId','-1');
    $('#user').attr('userId','-1');
}

function menu() {
    if ($('.sidemenu').hasClass('hiden', 300)) {
        $('.sidemenu').removeClass('hiden', 300);
        setTimeout(function(){
            $('#startDate').focus();
            $('#startDate').datepicker('show');
        },500);
    } else {
        $('.sidemenu').addClass('hiden', 300);
    }
}

function pageInitialEvents(){
    
    AutoCompleteForUser("user", saveUserDetail);
    // autoCompleteForGroup("groupName", saveGroupDetail);

    groupList();
    
    initCommonFunction();

    reportData = [];
    GeneratePivotTable(reportData);
    
    $(".bmd-fab-speed-dialer").on('click', function(){$(this).toggleClass("press")});

    $('.sidemenu').removeClass('hiden', 300);

    var date = new Date();
    var day = "01";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if( month < 10 ){
        month = "0" + month;
    }

    var monStDate = day + '-' + month + '-' + year;
    var today = new Date().format('dd-MM-yyyy')
    
    $('#startDate').val( today );
    $('#endDate').val( today );

    $('.fab-dial-handle').click(function(){
        $('.fabdail-buttons').toggleClass('active');
    });
    
    getReportData();
}