/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 01-06-2016.
 * File : inquiryListController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportData = [];
function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";        
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}


function getEmpListingData(){

    $('#empList').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getLevel10UserList',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(19,74),
        activeInactiveFlag : "1",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getEmpListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getEmpListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // console.log(empData);return false;
            var brListOpt = '<option value="-1">All User</option>';
            for( var i=0;i<data.data.length; i++ ){
                
                if( tmp_json.data[0].PK_USER_ID == data.data[i].PK_USER_ID ){
                    brListOpt += '<option value="'+ data.data[i].PK_USER_ID +'" selected>'+ data.data[i].FULLNAME +'</option>';
                    getInqListingByUser(data.data[i].PK_USER_ID);
                }else{
                    brListOpt += '<option value="'+ data.data[i].PK_USER_ID +'">'+ data.data[i].FULLNAME +'</option>';
                }
            }
            $('#empList').html( brListOpt );
            $('#empList').selectpicker();

            renderInquiryAss('');

        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        }

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }
}

function getInqListingByUser(id){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInquiryAssignmentReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        selectedUserId : id
    } 
    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryListCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data.data);
        // return false;
        reportData = data.data;
        renderInquiryAss(data.data);
    } else {        
        if (flag){
            renderInquiryAss('');
        }else{
            toastr.error(SERVERERROR);
        }            
    }
}

function renderInquiryAss(reportData){

    
    addFocusId( 'empList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    var startHtml = "<table class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th>Inquiry No.</th>"+
                            "<th>Created To</th>"+
                            "<th>Current Owner</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";

    var inqRow = '';
    var inqData = reportData;

    if( inqData != 'No record found' ){
        
        for( var i=0; i<inqData.length; i++ ){
            
            var status = inqData[i].statusName;

            if( status == "" ){
                status = "open";
            } 

            inqRow += '<tr>'+
                        '<td>'+ (i+1) +'</td>'+
                        '<td><a href="javascript:void(0)" inqId="'+ inqData[i].inqId +'" onclick="openAssignHist(this)">'+ inqData[i].clientLevelInqId +'</a></td>'+
                        '<td>'+ inqData[i].inqCreatedBy + ' <br/> '+ inqData[i].inqCreatedOn + '</td>'+
                        '<td>'+ inqData[i].inqAssignTo + ' <br/> '+ inqData[i].inqAssignToOn +'</td>'+
                    '</tr>';
                                                    
        }
    }
    
    var finalHtml = startHtml + inqRow + endHtml;
    $('#inquiryTable').html(finalHtml);
    
    TablesDataTables.init();
    TablesDataTablesEditor.init();
}

function openAssignHist(context){

    var inqId = $(context).attr('inqId'); 
    
    var index = findIndexByKeyValue( reportData , "inqId" , inqId );
    if( index == "-1" ){
        displayAPIErrorMsg( "" , GBL_ERR_NO_ASSIGNMENT );
        return false;
    }

    var inqData = reportData[index].allocationHistroy;
    
    if( inqData != "No record found" ){
        var listHeader = '<ul class="timeline2" id="allocationTimeline"><li class="tl-header"><div class="btn btn-info tl-content padder b-a"><div><i class="fa fa-anchor" aria-hidden="true"></i>  '+reportData[index].inqCreatedBy+' </div><div class="customInqActListing">Created On : '+reportData[index].inqCreatedOn+'</div></li>';
        var listMid = '';
        var listFooter = '<li class="tl-header"><div class="btn btn-sm btn-primary btn-rounded">Complete</div></li></ul>';
        
        for( var i=0; i<inqData.length; i++ ){
           var currentH = "";
           var lrClass = "";
           var activeClass = "transfer";
           var arrorwPost = "left";
           var activeOwner = '<i class="fa fa-share" aria-hidden="true"></i>';
            if(inqData.length == (i+1)){
                currentH = "open";
                activeClass = "active";
                activeOwner = '<i class="fa fa-handshake-o" aria-hidden="true"></i>';
            }
            
            if((i+1)%2 == 0){
                lrClass = 'timeline-inverted';
                arrorwPost = 'right';
            }
            listMid += '<li class="tl-item '+lrClass+'"><div class="tl-wrap '+currentH+' '+activeClass+'"><div class="tl-content panel-1 padder b-a"><span class="arrow '+arrorwPost+' pull-up"></span><div>'+activeOwner+'&nbsp;'+inqData[i]['inqAssignTo']+' </div>';
            listMid += '<div class="customInqActListing">Assign On : '+inqData[i]['inqAssignToOn']+'</div>';
            listMid += '<div class="customInqActListing">Assigned By : '+inqData[i]['inqAssignBY']+'</div>';
            //listMid += '<div class="customInqActListing">Assigned On : '+inqData[i]['inqAssignByOn']+'</div>';
            listMid += '</div></li>';
        }

        $('#inqAllocationData').html(listHeader + listMid + listFooter);
        $('#assignHistoryModal').modal('show');
    }
    else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_ASSIGNMENT );
    }
    
}