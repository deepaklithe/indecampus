/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 16-08-2016.
 * File : nonAvailingListController.
 * File Type : .js.
 * Project : indecampus
 *
 * */
var GBL_NON_AVAIL_DATA = []; // GLOBAL DATA JSON 

// INITIAL PAGE EVENTS
function pageInitialEvents(){

    localStorage.removeItem('indeCampusEditAvail');
	getNonAvailingServices(); //GET NON AVAILING SERVICES LIST DATA
}
  
//GET NON AVAILING SERVICES LIST DATA
function getNonAvailingServices(){  
    
    var tmp_json = JSON.parse( localStorage.indeCampusUserDetail );
    var postdata= {
        requestCase : 'listStudentNonAvailingService',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(27, 108),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }; 

    commonAjax(FOLLOWUPURL, postdata, getNonAvailingServicesCallBack,"Please wait.....Changing Student password");
    // console.log(postdata);return false; 
    function getNonAvailingServicesCallBack(flag,data){
        
        if(data.status == "Success" && flag){
            
            console.log(data);
            GBL_NON_AVAIL_DATA = data.data;
            renderNonAvailData(); // RENDER NON AVAILING SERVICES LIST DATA

        }else{

            GBL_NON_AVAIL_DATA = [];
            renderNonAvailData(); // RENDER NON AVAILING SERVICES LIST DATA

            if(flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NON_AVAIL_DATA );
                //location.reload();
            }else{
                toastr.warning(SERVERERROR); 
            }
        }
    }
} 

// RENDER NON AVAILING SERVICES LIST DATA
function renderNonAvailData(){

	var tHead = '<table class="table table-striped display" id="nonAvailTbl">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Student Name</th>'+
                            '<th>Raised Start Date</th>'+
                            '<th>Request ID</th>'+
                            '<th>Subject</th>'+
                            // '<th>Status</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';

    var tEnd = '</tbody></table>';

    var newRow = '';

    if( GBL_NON_AVAIL_DATA != "" && GBL_NON_AVAIL_DATA != "No record found" ){

    	GBL_NON_AVAIL_DATA.forEach(function( record , index ){

            
            var disableOld = '';
            var today = new Date();
            var requestDate = record.serviceStartDate;
            if( requestDate != "" && record.serviceStartDate != "0000-00-00" ){
                requestDate = new Date(requestDate);
                if( +requestDate < +today ){
                    disableOld = 'disabled';
                }
            }


	    	newRow +=  '<tr>'+
	                        '<td>'+ (index+1) +'</td>'+
	                        '<td>'+ record.studName +'</td>'+
                            '<td>'+ ( record.serviceStartDate != "" && record.serviceStartDate != "0000-00-00" ? mysqltoDesiredFormat(record.serviceStartDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ record.cServiceId +'</td>'+
	                        '<td>'+ record.description +'</td>'+
	                        // '<td>Pending</td>'+
	                        '<td>'+
	                            '<a href="javascript:void(0);" class="btn btn-sm btn-primary " data-toggle="tooltip" data-original-title="View" data-placement="top" serviceId="'+record.serviceId+'" onclick="viewNonAvailService(this);"><i class="fa fa-eye"></i></a>&nbsp;'+
	                            // '<a href="javascript:void(0);" class="btn btn-sm btn-success" data-toggle="tooltip" data-original-title="Edit" serviceId="'+record.serviceId+'" onclick="editNonAvail(this);"><i class="fa fa-pencil"></i></a>&nbsp;'+
	                            // '<a href="javascript:void(0);" class="btn btn-sm btn-danger" data-toggle="tooltip" data-original-title="Cancel" data-placement="top" serviceId="'+record.serviceId+'" onclick="deleteNonAvailService(this);" '+disableOld+'><i class="fa fa-trash"></i></a>'+
	                        '</td>'+
	                    '</tr>';       
                                                
    	});

    }   
    $('#nonAvailDiv').html( tHead + newRow + tEnd );
    $('#nonAvailTbl').dataTable();

}

// EDIT NON AVAIL SERVICE
function editNonAvail(context){

    var serviceId = $(context).attr('serviceId');
    localStorage.indeCampusEditAvail = serviceId;
    window.location.href = "addNonAvailService.html"; 

}

// EDIT NON AVAIL SERVICE
function viewNonAvailService(context){

    var serviceId = $(context).attr('serviceId');
    localStorage.indeCampusEditAvail = serviceId;
    window.location.href = "nonAvailingView.html"; 

}

// DELETE SELECTED NON AVAIL SERVICE
function deleteNonAvailService(context){

    var serviceId = $(context).attr('serviceId');
    var cnfmDelete = confirm("Are you sure you want to delete this raised service ?");
    if( !cnfmDelete ){
        return false;
    }

    var tmp_json = JSON.parse( localStorage.indeCampusUserDetail );
    var postdata= {
        requestCase : 'deleteStudentService',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(27, 110),
        serviceId : serviceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }; 

    commonAjax(FOLLOWUPURL, postdata, deleteNonAvailServiceCallBack,"Please wait.....Changing Student password");
    // console.log(postdata);return false; 
    function deleteNonAvailServiceCallBack(flag,data){
        
        if(data.status == "Success" && flag){
            
            console.log(data);
            toastr.success("Non availing service request canceled successfully !");
            var index = findIndexByKeyValue(GBL_NON_AVAIL_DATA,'serviceId',serviceId);
            if( index != -1 ){
                GBL_NON_AVAIL_DATA.splice(index,1); // REMOVE SELECTED SERVICE DATA
                renderNonAvailData(); // RENDER NON AVAILING SERVICES LIST DATA
            }

        }else{

            if(flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NON_AVAIL_DEL );
                //location.reload();
            }else{
                toastr.warning(SERVERERROR); 
            }
        }
    }

}

//REDIRECTED TO NON AVAILING SERVICES ADD
function navigateToAddAvailServices(){ 

    window.location.href = "addNonAvailService.html"; 
} 
