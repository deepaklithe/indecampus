/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 31-05-2016.
 * File : itemmassController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_ITEMS = []; // ITEM JSON VARIABLE

// PAGE INITIAL EVENTS
function pageInitialEvents(){

    $('#itemCategoryType').selectpicker('refresh');

    if( localStorage.indeCampusAddItemType != undefined && localStorage.indeCampusAddItemType != "" ){
        if( localStorage.indeCampusAddItemType == "room" ){
            // $('#tab2').html('Room Master');
            // $('#tab2').attr('href','roommaster.html');
            // $('#tab3').html('Room Management');
            $('#itemType').html( '' );
            $('#itemType').html( '<option value="RENTAL">Rental</option>' ); // SET DATA INTO SELECTPICKER
            $('#itemType').attr('disabled',true);
            $('#pageTitle').html( 'Room Item <small>Management</small>' );
            $('#itemName').attr('placeholder','Room Name (eg. Double Occupancy for Handicapped)');
            var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
            var categoryType = roleAuth.data.categoryType;
            setOption( 0 , 'itemCategoryType' , categoryType , '' );
            $('#itemCategoryType').selectpicker('refresh');
            $('#itemMaxCapacity').removeAttr('readonly');
        }else{
            $('#roomCatDiv').hide();
            $('#pageTitle').html( 'Facility Item <small>Management</small>' );
        }
        $('#itemType').selectpicker('refresh');
        getItemList();
    }
    $('#itemPriceValidityStDate').val( mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
    localStorage.removeItem('indeCampusRoomDetail');

}

// GET ITEMS LISTING DATA
function getItemList(){

    var itemDataType = localStorage.indeCampusAddItemType;
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : ( itemDataType == "room" ? "RENTAL" : "" ),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ITEMS = data.data;
            renderItemList(); // RENDER ITEMS

        }else{

            GBL_ITEMS = [];
            renderItemList(); // RENDER ITEMS
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEMS );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER ITEM LISTING DATA
function renderItemList(){

    var thead = '<table class="display datatables-alphabet-sorting" id="itemListTable">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Type</th>'+
                        '<th>Name</th>'+
                        // '<th class="rentalField">Category</th>'+
                        '<th>Price</th>'+
                        '<th>Cgst</th>'+
                        '<th>Sgst</th>'+
                        '<th>Igst</th>'+
                        '<th>HSN Code</th>'+
                        '<th>Tally Name</th>'+
                        '<th>Action</th>'+ 
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var innerRow = '';
    if( GBL_ITEMS.length != 0 && GBL_ITEMS != "No record found" ){

        GBL_ITEMS.forEach(function( record , index ){
            var hideClass = '';
            if( localStorage.indeCampusAddItemType != "room" ){
                hideClass = 'hide';
            }
            var itemType = record.itemTypeDisplayName;
            if( localStorage.indeCampusAddItemType == "room" ){
                itemType = "Rental";
            }

            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ itemType +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            // '<td class="rentalField">'+ record.itemCategory +'</td>'+
                            '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.itemPrice) +'</td>'+
                            '<td>'+ ( record.cgst != "" ? fixedTo2(record.cgst) : "0.00") +' %</td>'+
                            '<td>'+ ( record.sgst != "" ? fixedTo2(record.sgst) : "0.00") +' %</td>'+
                            '<td>'+ ( record.igst != "" ? fixedTo2(record.igst) : "0.00") +' %</td>'+
                            '<td>'+ (record.hsnCode) +' </td>'+
                            '<td>'+ (record.tallyName) +' </td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" onclick="assignRoomToItem(this)" itemType="'+ record.itemType +'" itemId="'+ record.itemId +'" data-toggle="tooltip" data-original-title="Assign Room" data-placement="top" class="btn-right-mrg btn btn-xs btn-primary btn-ripple btn-amber assnBtn_'+index+' '+hideClass+'"><i class="fa fa-home"></i></a>'+
                                '<a href="javascript:void(0)" onclick="editItem(this)" itemType="'+ record.itemType +'" itemId="'+ record.itemId +'" data-toggle="tooltip" data-original-title="Edit" data-placement="top" class="btn-right-mrg btn btn-xs btn-primary btn-ripple btn-primary updBtn_'+index+'"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" onclick="deleteItem(this)" itemType="'+ record.itemType +'" itemId="'+ record.itemId +'" data-toggle="tooltip" data-original-title="Delete" data-placement="top" class="btn btn-xs btn-danger btn-ripple delBtn_'+index+'"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    if( localStorage.indeCampusAddItemType != "room" ){
        $('#rentalField').hide();
    }
    $('#itemListTable').DataTable();

}

// CREATE AN ITEM
function createItem(){

    var itemName = $('#itemName').val();
    var itemPrice = $('#itemPrice').val();
    var itemPriceValidityStDate = $('#itemPriceValidityStDate').val();
    var itemValidityEnDate = $('#itemValidityEnDate').val();
    var itemType = $('#itemType').val();
    var itemCgst = $('#itemCgst').val();
    var itemSgst = $('#itemSgst').val();
    var itemHsnCode = $('#itemHsnCode').val();
    var itemTallyName = $('#itemTallyName').val();
    var itemMaxCapacity = $('#itemMaxCapacity').val();
    var itemDescription = $('#itemDescription').val();
    var itemCategoryType = $('#itemCategoryType').val();
    var itemEventStDate = $('#itemEventStDate').val();
    var itemEventEndDate = $('#itemEventEndDate').val();
    var itemIgst = $('#itemIgst').val();
    var eposItemId = $('#eposItemId').val();
    var eposSpaceId = $('#eposSpaceId').val();

    // CHECK FIELD VALIDATIONS
    if( itemName == "" ){
        toastr.warning("Please enter item name");
        addFocusId('itemName');
        return false;

    }else if( itemPrice == "" ){
        toastr.warning("Please enter item price");
        addFocusId('itemPrice');
        return false;

    }else if( itemPriceValidityStDate == "" ){
        itemPriceValidityStDate = mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy');

    }else if( itemValidityEnDate == "" ){
        toastr.warning("Please select item validity end date");
        addFocusId('itemValidityEnDate');
        return false;

    }else if( itemMaxCapacity == "" ){
        toastr.warning("Please enter item max capacity");
        addFocusId('itemMaxCapacity');
        return false;

    // }else if( itemIgst != "" && parseFloat(itemIgst) != "0" &&  itemCgst != "" && parseFloat(itemCgst) != 0){
    //     toastr.warning("You cannot enter Cgst alongwith Igst");
    //     // addFocusId('itemCgst');
    //     return false;

    }else if( itemCgst != "" && parseFloat(itemCgst) != 0 && parseFloat(itemCgst) > 100  ){
        toastr.warning("Cgst percentage cannot be more than 100");
        addFocusId('itemCgst');
        return false;

    // }else if( itemIgst != "" && parseFloat(itemIgst) != "0" &&  itemSgst != "" && parseFloat(itemSgst) != 0){
    //     toastr.warning("You cannot enter Sgst alongwith Igst");
    //     // addFocusId('itemCgst');
    //     return false;

    }else if( itemSgst != "" && parseFloat(itemSgst) != 0 && parseFloat(itemSgst) > 100  ){
        toastr.warning("Sgst percentage cannot be more than 100");
        addFocusId('itemSgst');
        return false;

    }else if( ( itemCgst != "" && parseFloat(itemCgst) != "0" || itemSgst != "" && parseFloat(itemSgst) != "0"  ) && (itemIgst != "" && parseFloat(itemIgst) != "0") ){
        toastr.warning("You cannot enter Igst alongwith Cgst or Sgst");
        return false;

    }else if( itemIgst != "" && parseFloat(itemIgst) != 0 && parseFloat(itemIgst) > 100  ){
        toastr.warning("Igst percentage cannot be more than 100");
        addFocusId('itemIgst');
        return false;

    }else if( itemHsnCode == "" ){
        toastr.warning("Please enter item Hsn code");
        addFocusId('itemHsnCode');
        return false;

    }else if( itemTallyName == "" ){
        toastr.warning("Please enter item tally name");
        addFocusId('itemTallyName');
        return false;

    }else if( itemType == null || itemType == "" ){
        toastr.warning("Please select item type");
        addFocusId('itemType');
        return false;

    }else if( itemType == "EVENTS" && itemEventStDate == "" ){
        toastr.warning("Please select event start date & time");
        addFocusId('itemEventStDate');
        return false;

    }else if( itemType == "EVENTS" && itemEventEndDate == "" ){
        toastr.warning("Please select event end date & time");
        addFocusId('itemEventEndDate');
        return false;
    }
    // ADDED START DATE-TIME & END DATE-TIME FOR EVENT ITEM TYPE - ON 25-JULY-2018
    var eventStartDate = "";
    var eventEndDate = "";
    var eventStartTime = "";
    var eventEndTime = "";
    if( itemType == "EVENTS" ){
        eventStartDate = (($('#itemEventStDate').val() ).split('|')[0]).trim();
        eventEndDate = (($('#itemEventEndDate').val() ).split('|')[0]).trim();
        eventStartTime = (($('#itemEventStDate').val() ).split('|')[1]).trim();
        eventEndTime = (($('#itemEventEndDate').val() ).split('|')[1]).trim();

        var eventStartDateVal = ddmmyyToMysql(eventStartDate,'yyyy-MM-dd');
        var eventEndDateVal = ddmmyyToMysql(eventEndDate,'yyyy-MM-dd');

        var dateEventStartDate = new Date(eventStartDateVal +" "+ eventStartTime);
        var dateEventEndDate = new Date(eventEndDateVal +" "+ eventEndTime);
        if( +dateEventEndDate < +dateEventStartDate ){
            toastr.warning("End Date/Time cannot be less than the Start Date / Time");
            addFocusId('itemEventEndDate');
            return false;
        }
    }
    var eveStDateTime = ( itemType == "EVENTS" ? (eventStartDate + ' ' + eventStartTime) : "" );
    var eveEnDateTime = ( itemType == "EVENTS" ? (eventEndDate + ' ' + eventEndTime) : "" );

    if( localStorage.indeCampusAddItemType == "room" ){

        if( itemCategoryType == "-1" || itemCategoryType == null || itemCategoryType == "" ){
            toastr.warning("Please select item type");
            addFocusId('itemCategoryType');
            return false;
        }

    }
    if( itemSgst == "" ){
        itemSgst = "0";
    }
    if( itemCgst == "" ){
        itemCgst = "0";
    }
    if( itemIgst == "" ){
        itemIgst = "0";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'addItem', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15, 61),
        itemName : itemName,
        price : itemPrice,
        priceStartDate : itemPriceValidityStDate,
        itemValidityEndDate : itemValidityEnDate,
        itemType : itemType,
        cgst : itemCgst,
        sgst : itemSgst,
        igst : itemIgst,
        hsnCode : itemHsnCode,
        tallyName : itemTallyName,
        description : itemDescription,
        qty : itemMaxCapacity,
        eventStartDateTime : eveStDateTime,
        eventEndDateTime : eveEnDateTime,
        eposItemId : eposItemId,
        eposSpaceId : eposSpaceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    if( localStorage.indeCampusAddItemType == "room" ){
        postData.itemSubType = itemCategoryType;
    }
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            // GBL_ITEMS = data.data;
            // renderItemList();
            getItemList();

        }else{
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_ITEM );
            else
                toastr.error(SERVERERROR);
        }
        resetItemFields();
    }
}

// RESET FIELD VALUES
function resetItemFields(){

    $('#itemName').val('');
    $('#itemPrice').val('');
    $('#itemPriceValidityStDate').val('');
    $('#itemValidityEnDate').val('');
    $('#itemCgst').val('');
    $('#itemSgst').val('');
    $('#itemHsnCode').val('');
    $('#itemTallyName').val('');
    $('#itemIgst').val('');
    $("#itemType").val($("#itemType option:first").val());
    $('#itemType').selectpicker('refresh');
    $('#itemType').trigger('change');
    $('#saveItemBtn').html( '<i class="fa fa-floppy-o"></i> Save' );
    $('#saveItemBtn').attr('onclick','createItem();');
    $('#unlimitedValidity').prop('checked',false);
    $('#itemDescription').val('');
    $('#itemMaxCapacity').val('1');
    $('#itemMaxCapacity').attr('readonly',true);
    $('#itemCategoryType').val('').selectpicker('refresh');
    $('.eventTypeDateTime').hide();
    $('#itemEventStDate').val('');
    $('#itemEventEndDate').val('');
    $('#eposItemId').val('');
    $('#eposSpaceId').val('');
    if( GBL_ITEMS != "" ){
        GBL_ITEMS.forEach(function( record , index ){
            $('.assnBtn_'+index).removeAttr('disabled');
            $('.updBtn_'+index).removeAttr('disabled');
            $('.delBtn_'+index).removeAttr('disabled');
        });
    }
    
    localStorage.removeItem('indeCampusUpdateItemId');
}

// EDIT ITEM AND SET ITS VALUE TO UPDATE
function editItem(context){

    var itemId = $(context).attr('itemId');
    var itemIndex = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );
    if( itemIndex != -1 ){
        $('#itemName').val( GBL_ITEMS[itemIndex].itemName );
        $('#itemPrice').val( GBL_ITEMS[itemIndex].itemPrice );
        $('#itemPriceValidityStDate').val( mysqltoDesiredFormat( GBL_ITEMS[itemIndex].priceStartDate , 'dd-MM-yyyy' ) );
        if( GBL_ITEMS[itemIndex].itemValidityEndDate == "9999-12-31" ){
            $('#unlimitedValidity').prop('checked',true);
            $('#unlimitedValidity').trigger('change');
        }else{
            $('#itemValidityEnDate').val( mysqltoDesiredFormat( GBL_ITEMS[itemIndex].itemValidityEndDate , 'dd-MM-yyyy' ) );
        }
        
        $("#itemType").val( GBL_ITEMS[itemIndex].itemType );
        $('#itemType').selectpicker('refresh');
        $('#itemType').trigger('change');
        $('#itemCgst').val( ( GBL_ITEMS[itemIndex].cgst != "" ? parseFloat(GBL_ITEMS[itemIndex].cgst) : 0.00 ) );
        $('#itemSgst').val( ( GBL_ITEMS[itemIndex].sgst != "" ? parseFloat(GBL_ITEMS[itemIndex].sgst) : 0.00 ) );
        $('#itemIgst').val( ( GBL_ITEMS[itemIndex].igst != "" ? parseFloat(GBL_ITEMS[itemIndex].igst) : 0.00 ) );
        $('#itemHsnCode').val( GBL_ITEMS[itemIndex].hsnCode );
        $('#itemTallyName').val( GBL_ITEMS[itemIndex].tallyName );
        $('#itemDescription').val( GBL_ITEMS[itemIndex].description );
        $('#itemMaxCapacity').val( GBL_ITEMS[itemIndex].qty );
        $('#itemCategoryType').val( GBL_ITEMS[itemIndex].itemSubType ).selectpicker('refresh');

        if( GBL_ITEMS[itemIndex].itemType == "EVENTS" ){
            $('.eventTypeDateTime').show();
            var eventStartDateTime = GBL_ITEMS[itemIndex].eventStartDateTime;
            var eventEndDateTime = GBL_ITEMS[itemIndex].eventEndDateTime;
            var newStDate = "" , newEnDate = "";
            if( eventStartDateTime != "" && eventStartDateTime != undefined ){
                newStDate = eventStartDateTime.split(' ')[0].trim() + ' | ' + eventStartDateTime.split(' ')[1].trim() + ' ' + eventStartDateTime.split(' ')[2].trim();
            }
            if( eventEndDateTime != "" && eventEndDateTime != undefined ){
                newEnDate = eventEndDateTime.split(' ')[0].trim() + ' | ' + eventEndDateTime.split(' ')[1].trim() + ' ' + eventEndDateTime.split(' ')[2].trim();
            }
            $('#itemEventStDate').val( newStDate );
            $('#itemEventEndDate').val( newEnDate );
            $('#eposItemId').val('');
            $('#eposSpaceId').val('');
        }else{
            $('#eposItemId').val( GBL_ITEMS[itemIndex].eposItemId );
            $('#eposSpaceId').val( GBL_ITEMS[itemIndex].eposSpaceId );
            $('.eventTypeDateTime').hide();
            $('#itemEventStDate').val('');
            $('#itemEventEndDate').val('');
        }

        localStorage.indeCampusUpdateItemId = itemId;
        $('#saveItemBtn').html( '<i class="fa fa-floppy-o"></i> Update' );
        $('#saveItemBtn').attr('onclick','updateItem();');
        if( GBL_ITEMS != "" ){
            GBL_ITEMS.forEach(function( record , index ){
                $('.assnBtn_'+index).removeAttr('disabled');
                $('.updBtn_'+index).removeAttr('disabled');
                $('.delBtn_'+index).removeAttr('disabled');
            });
        }
        $('.assnBtn_'+itemIndex).attr('disabled',true);
        $('.updBtn_'+itemIndex).attr('disabled',true);
        $('.delBtn_'+itemIndex).attr('disabled',true);
    }

}

// UPDATE EXISTING ITEM
function updateItem(){

    var itemName = $('#itemName').val();
    var itemPrice = $('#itemPrice').val();
    var itemPriceValidityStDate = $('#itemPriceValidityStDate').val();
    var itemValidityEnDate = $('#itemValidityEnDate').val();
    var itemType = $('#itemType').val();
    var itemCgst = $('#itemCgst').val();
    var itemSgst = $('#itemSgst').val();
    var itemHsnCode = $('#itemHsnCode').val();
    var itemTallyName = $('#itemTallyName').val();
    var itemMaxCapacity = $('#itemMaxCapacity').val();
    var itemDescription = $('#itemDescription').val();
    var itemIgst = $('#itemIgst').val();
    var itemCategoryType = $('#itemCategoryType').val();
    var itemEventStDate = $('#itemEventStDate').val();
    var itemEventEndDate = $('#itemEventEndDate').val();
    var eposItemId = $('#eposItemId').val();
    var eposSpaceId = $('#eposSpaceId').val();

    // CHECK FIELD VALIDATIONS
    if( itemName == "" ){
        toastr.warning("Please enter item name");
        addFocusId('itemName');
        return false;

    }else if( itemPrice == "" ){
        toastr.warning("Please enter item price");
        addFocusId('itemPrice');
        return false;

    }else if( itemPriceValidityStDate == "" ){
        itemPriceValidityStDate = mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy');

    }else if( itemValidityEnDate == "" ){
        toastr.warning("Please select item validity end date");
        addFocusId('itemValidityEnDate');
        return false;

    }else if( itemMaxCapacity == "" ){
        toastr.warning("Please enter item max capacity");
        addFocusId('itemMaxCapacity');
        return false;

    // }else if( itemIgst != "" && parseFloat(itemIgst) != "0" &&  itemCgst != "" && parseFloat(itemCgst) != 0){
    //     toastr.warning("You cannot enter Cgst alongwith Igst");
    //     // addFocusId('itemCgst');
    //     return false;

    }else if( itemCgst != "" && parseFloat(itemCgst) != 0 && parseFloat(itemCgst) > 100  ){
        toastr.warning("Cgst percentage cannot be more than 100");
        addFocusId('itemCgst');
        return false;

    // }else if( itemIgst != "" && parseFloat(itemIgst) != "0" &&  itemSgst != "" && parseFloat(itemSgst) != 0){
    //     toastr.warning("You cannot enter Sgst alongwith Igst");
    //     // addFocusId('itemCgst');
    //     return false;

    }else if( itemSgst != "" && parseFloat(itemSgst) != 0 && parseFloat(itemSgst) > 100  ){
        toastr.warning("Sgst percentage cannot be more than 100");
        addFocusId('itemSgst');
        return false;

    }else if( ( itemCgst != "" && parseFloat(itemCgst) != "0" || itemSgst != "" && parseFloat(itemSgst) != "0"  ) && (itemIgst != "" && parseFloat(itemIgst) != "0") ){
        toastr.warning("You cannot enter Igst alongwith Cgst or Sgst");
        return false;

    }else if( itemIgst != "" && parseFloat(itemIgst) != 0 && parseFloat(itemIgst) > 100  ){
        toastr.warning("Igst percentage cannot be more than 100");
        addFocusId('itemIgst');
        return false;

    }else if( itemHsnCode == "" ){
        toastr.warning("Please enter item Hsn code");
        addFocusId('itemHsnCode');
        return false;

    }else if( itemTallyName == "" ){
        toastr.warning("Please enter item tally name");
        addFocusId('itemTallyName');
        return false;

    }else if( itemType == null || itemType == "" ){
        toastr.warning("Please select item type");
        addFocusId('itemType');
        return false;

    }else if( itemType == "EVENTS" && itemEventStDate == "" ){
        toastr.warning("Please select event start date & time");
        addFocusId('itemEventStDate');
        return false;

    }else if( itemType == "EVENTS" && itemEventEndDate == "" ){
        toastr.warning("Please select event end date & time");
        addFocusId('itemEventEndDate');
        return false;
    }
    // ADDED START DATE-TIME & END DATE-TIME FOR EVENT ITEM TYPE - ON 25-JULY-2018
    var eventStartDate = "";
    var eventEndDate = "";
    var eventStartTime = "";
    var eventEndTime = "";
    if( itemType == "EVENTS" ){
        eventStartDate = (($('#itemEventStDate').val() ).split('|')[0]).trim();
        eventEndDate = (($('#itemEventEndDate').val() ).split('|')[0]).trim();
        eventStartTime = (($('#itemEventStDate').val() ).split('|')[1]).trim();
        eventEndTime = (($('#itemEventEndDate').val() ).split('|')[1]).trim();

        var eventStartDateVal = ddmmyyToMysql(eventStartDate,'yyyy-MM-dd');
        var eventEndDateVal = ddmmyyToMysql(eventEndDate,'yyyy-MM-dd');

        var dateEventStartDate = new Date(eventStartDateVal +" "+ eventStartTime);
        var dateEventEndDate = new Date(eventEndDateVal +" "+ eventEndTime);
        if( +dateEventEndDate < +dateEventStartDate ){
            toastr.warning("End Date/Time cannot be less than the Start Date / Time");
            addFocusId('itemEventEndDate');
            return false;
        }
    }
    var eveStDateTime = ( itemType == "EVENTS" ? (eventStartDate + ' ' + eventStartTime) : "" );
    var eveEnDateTime = ( itemType == "EVENTS" ? (eventEndDate + ' ' + eventEndTime) : "" );

    if( localStorage.indeCampusAddItemType == "room" ){

        if( itemCategoryType == "-1" || itemCategoryType == null || itemCategoryType == "" ){
            toastr.warning("Please select item type");
            addFocusId('itemCategoryType');
            return false;
        }

    }
    
    if( itemSgst == "" ){
        itemSgst = "0";
    }
    if( itemCgst == "" ){
        itemCgst = "0";
    }
    if( itemIgst == "" ){
        itemIgst = "0";
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'editItem', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15, 63),
        itemId : localStorage.indeCampusUpdateItemId,
        itemName : itemName,
        price : itemPrice,
        priceStartDate : itemPriceValidityStDate,
        itemValidityEndDate : itemValidityEnDate,
        itemType : itemType,
        cgst : itemCgst,
        sgst : itemSgst,
        igst : itemIgst,
        hsnCode : itemHsnCode,
        tallyName : itemTallyName,
        description : itemDescription,
        qty : itemMaxCapacity,
        eventStartDateTime : eveStDateTime,
        eventEndDateTime : eveEnDateTime,
        eposItemId : eposItemId,
        eposSpaceId : eposSpaceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    if( localStorage.indeCampusAddItemType == "room" ){
        postData.itemSubType = itemCategoryType;
    }
    commonAjax(FOLLOWUPURL, postData, editItemCallback,"Please wait...Updating your item.");

    function editItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            // var itemIndex = findIndexByKeyValue( GBL_ITEMS , 'itemId' , localStorage.indeCampusUpdateItemId );
            // if( itemIndex != -1 ){
            //     GBL_ITEMS = data.data;
            // }
            // renderItemList();
            getItemList();

        }else{
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_ITEM );
            else
                toastr.error(SERVERERROR);
        }
        resetItemFields();
    }

}

// DELETE SELECTED ITEM
function deleteItem(context){

    var itemId = $(context).attr('itemId');
    var itemType = $(context).attr('itemType');
    itemType = ( itemType != "RENTAL" ? "" : "RENTAL" );

    var confrmDelete = confirm('Are you sure you want to delete this item ?');
    if( !confrmDelete ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'deleteItem', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,64),
        itemId : itemId,
        itemType : itemType,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, deleteItemCallback,"Please wait...Updating your item.");

    function deleteItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ITEMS = data.data;
            var itemIndex = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );
            if( itemIndex != -1 ){
                GBL_ITEMS.splice( itemIndex , 1 );
            }
            renderItemList();

        }else{
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_ITEM );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// ASSIGN ROOMS TO ROOM TYPE ITEM
function assignRoomToItem(context){

    var itemId = $(context).attr('itemId');
    var index = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );
    if( index != -1 ){
        localStorage.indeCampusRoomDetail = JSON.stringify( GBL_ITEMS[index] );
        window.location.href = "createRoom.html";
    }

}

// SET ITEM VALIDITY DATE TO UNLIMITED
function checkUnlimitedValidity(context){

    var checked = $(context).prop('checked');
    if( checked == true ){
        $('#itemValidityEnDate').datepicker( 'update' , '31-12-9999' );
    }else{
        $('#itemValidityEnDate').datepicker( 'update' , mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
        $('#itemValidityEnDate').val('');
    }

}

// CHECK SELECTED ITEM TYPE AND DISPLAY FIELDS ACCORDINGLY
function checkItemType(context){

    var itemType = $(context).val();
    console.log(itemType);
    if( itemType == "EVENTS"  ){
        $('#itemMaxCapacity').removeAttr('readonly');
        $('.eposItemIdDiv').hide();
        $('.eventTypeDateTime').show();
    }else if ( itemType == "SUBSC" ){
        $('#itemMaxCapacity').attr('readonly',true);
        $('.eposItemIdDiv').show();
        $('.eventTypeDateTime').hide();
    }else{
        $('#itemMaxCapacity').attr('readonly',true);
        $('.eposItemIdDiv').hide();
        $('.eventTypeDateTime').hide();
    }

}