/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 06-11-2016.
 * File : fileUploadClientController.js.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_USERLIST = [];
var GBL_ACT_TYPE = "";

function sampleDownload(){

    var uploadType = $("input[name=uploadType]:checked").val();

    if( uploadType == "item" ){
        downloadItemCSV();
        
    }else if( uploadType == "itemDepartment" ){
        downloadItemDepartmentCSV();
        
    }else if( uploadType == "property" ){
        downloadPropertySample();
        
    }else if( uploadType == "user" ){
        downloadUserSample();
        
    }else{
        toastr.warning('Please Select atleast 1 option');
    }
} 

function downloadItemCSV(){

    var GBL_ITEMJSON = [];
    var tempItemJson = {
        fullName : "Item Name", 
        tallyName : "Tally Name", 
        hsnCode : "hsn Code", 
        itemType : "Item Type", 
        itemPrice : "Item Price", 
        description : "Description", 
        qty : "Qty", 
        cgst : "Cgst", 
        sgst : "Sgst", 
        igst : "Igst", 
        itemValidity : "Item Validity End Date", 
    }
    GBL_ITEMJSON.push( tempItemJson );

    csvSave(GBL_ITEMJSON, "ITEM_SAMPLE.csv");   
}

function downloadItemDepartmentCSV(){

    var GBL_ITEMJSON = [];
    var tempItemJson = {
        depCatName : "Department category Name", 
        depIdParent : "Fk Parent Department Id", 
        depTypeId : "Fk Department Type Id", 
        clientId : "Fk Client Id", 
        orgId : "Fk Org Id",  
    }
    GBL_ITEMJSON.push( tempItemJson );

    csvSave(GBL_ITEMJSON, "ITEM_DEPARTMENT_SAMPLE.csv");   
}
// Download Price CSV Sample file starts here
 

function downloadPropertySample(){

    var GBL_ITEMJSON = [];
    var tempItemJson = {
        propName : "Property Name", 
        propValidEnd : "Property Validity End Date",  
        cgst : "Cgst", 
        sgst : "Sgst", 
        igst : "Igst", 
        price : "Price", 
        type : "Type", 
        hsn : "Hsn Code", 
        tallyName : "Tally Name", 
    }
    GBL_ITEMJSON.push( tempItemJson );

    csvSave(GBL_ITEMJSON, "ITEM_SAMPLE.csv");   
}


function downloadUserSample(){

    var GBL_USER_SAMPLE_DATA = [];
    var branchListing = [];
    var userLevel = [];
    var userRole = [];
    var designationList = [];
    var userList = [];

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getUserLevelForUserManagement",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(COMMONURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
    function getUserDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
            
            branchListing = data.data.branchListing;
            userLevel = data.data.userLevel;
            userRole = data.data.userRole; 

            var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
            designationList = roleAuth.data.designation;  

            getUserList();

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_LEVEL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

    //GET USER LISTING
    function getUserList(){

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getUserListing',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(8,30),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };

        commonAjax(COMMONURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
        function getUserDataCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data);
                
                userList = data.data; 
                generateSampleUserCsv();

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_LEVEL );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

    function generateSampleUserCsv(){

        GBL_USER_SAMPLE_DATA = [];

        //BRANCH DATA
        if( branchListing != "" && branchListing != "No record found" ){

            var tempBranch = {
                branchName : 'Branch Name',
                branchId : 'Branch Id',
            }
            GBL_USER_SAMPLE_DATA.push( tempBranch );

            branchListing.forEach( function( record , index ){
                var tempBranch = {
                    branchName : record.orgName,
                    branchId : record.orgId,
                }
                GBL_USER_SAMPLE_DATA.push( tempBranch );
            });
            var tempBranch = {
                branchName : '',
                branchId : '',
            }
            GBL_USER_SAMPLE_DATA.push( tempBranch );
        }

        //USER LEVEL DATA
        if( userLevel != "" && userLevel != "No record found" ){

            var tempLevel = {
                levelName : 'Level Name',
                defaultName : 'Default Name',
                levelId : 'Level Id',
            }
            GBL_USER_SAMPLE_DATA.push( tempLevel );

            userLevel.forEach( function( record , index ){
                var tempLevel = {
                    levelName : record.levelName,
                    defaultName : record.levelDefaultName,
                    levelId : record.levelId,
                }
                GBL_USER_SAMPLE_DATA.push( tempLevel );
            });
            var tempLevel = {
                levelName : '',
                defaultName : '',
                levelId : '',
            }
            GBL_USER_SAMPLE_DATA.push( tempLevel );
        }

        //USER ROLE DATA
        if( userRole != "" && userRole != "No record found" ){

            var tempRole = {
                roleName : 'Role Name', 
                roleId : 'Role Id',
            }
            GBL_USER_SAMPLE_DATA.push( tempRole );

            userRole.forEach( function( record , index ){
                var tempRole = {
                    rolelName : record.roleName, 
                    roleId : record.roleId,
                }
                GBL_USER_SAMPLE_DATA.push( tempRole );
            });
            var tempRole = {
                rolelName : '', 
                roleId : '',
            }
            GBL_USER_SAMPLE_DATA.push( tempRole );
        }

        //DESIGNATION DATA
        if( designationList != "" && designationList != "No record found" ){

            var tempDesig = {
                desigName : 'Designation Name', 
                desigId : 'Designation Id',
            }
            GBL_USER_SAMPLE_DATA.push( tempDesig );

            designationList.forEach( function( record , index ){
                var tempDesig = {
                    desigName : record.VALUE, 
                    desigId : record.ID,
                }
                GBL_USER_SAMPLE_DATA.push( tempDesig );
            });
            var tempDesig = {
                desigName : '', 
                desigId : '',
            }
            GBL_USER_SAMPLE_DATA.push( tempDesig );
        }

        //USER DATA
        if( userList != "" && userList != "No record found" ){

            var tempUser = {
                userId : 'User Id',
                userName : 'User Name', 
                userLevel : 'User Level', 
                userDesignation : 'Designation', 
            }
            GBL_USER_SAMPLE_DATA.push( tempUser );

            userList.forEach( function( record , index ){
                var tempUser = {
                    userId : record.userId,
                    userName : record.userFullName, 
                    userLevel : record.userLevelName, 
                    userDesignation : record.userDesignationName, 
                }
                GBL_USER_SAMPLE_DATA.push( tempUser );
            });
            var tempUser = {
                userId : '',
                userName : '', 
                userLevel : '', 
                userDesignation : '', 
            }
            GBL_USER_SAMPLE_DATA.push( tempUser );
        }

        csvSave(GBL_USER_SAMPLE_DATA, "USER_MASTER_DATA.csv");     

        var GBL_USERJSON = [];
        var tempUserJson = {
            fullName : "Full Name",
            emailAddress : "Email Address",
            mobileNo : "Mobile No",
            userName : "Username",
            password : "Password",
            birthDate : "Birth Date",
            anniDate : "Anniversary Date",
            gender : "Gender",
            address : "Address",
            userLevel : "User Level",
            designation : "Designation",
            role : "Role",
            supervisorName : "Supervisor Name",
            branch : "Branch",
            googleCalEmail : "Google Calender Email", 
        }
        GBL_USERJSON.push( tempUserJson );

        csvSave(GBL_USERJSON, "USER_SAMPLE.csv");     
    }
}


// UPLOAD CODE STARTED FROM HERE

//START FUNCTION FOR ADD Activity
function submitData(){
    
    var uploadMode = $("input[name=uploadMode]:checked").val();

    if( uploadMode == "csv" ){
        var uploadType = $("input[name=uploadType]:checked").val();
        var uploadFileName = $("#attachmentName").val();
        if(!uploadFileName){
            toastr.warning('Please select file for upload '+uploadType);
            $('#attachmentName').click();
            return false;
        }
        $("#fileupload").submit();
    
    }else if( uploadMode == "json" ){
        jsonWiseSubmit();
    }else if( uploadMode == "xml" ){
        xmlWiseSubmit();
    }
}

function submitActData(){

    if( $('#actTypeListUpload').val() == "-1" ){
        toastr.warning('Please Select activity type');
        return false;
    }else{
        GBL_ACT_TYPE = $('#actTypeListUpload').val();
        $("#fileupload").submit();
    }
}

function assignAttachementEvent(){
    
    $("#fileupload").off().on('submit', (function (e) {
        
        var uploadType = $("input[name=uploadType]:checked").val();
        var uploadTypeVal = "";

        if( uploadType == "item" ){
            uploadTypeVal = "item_master";
            
        }else if( uploadType == "itemDepartment" ){
            uploadTypeVal = "item_department";
            
        }else if( uploadType == "property" ){
            uploadTypeVal = "property_master";
            
        }else if( uploadType == "user" ){
            uploadTypeVal = "user_master";
            
        }else{
            toastr.error('Please Select Proper Upload option');
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 10),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            uploadType : uploadTypeVal,
        }; 

        $("#postDataAttach").val(JSON.stringify(temp_json));
        // console.log(temp_json);return false;

        var bfsendMsg = "Sending data to server..";
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1);
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                
                $('#activityTypeModal').modal('hide');
                if(data.status == "Success"){
                    
                    console.log(data.data);

                    refreshFile();
                    $('#errorHideDiv').hide();
                    $('#errorDiv').html("");
                    toastr.warning( data.data );
                }else{
//                    toastr.warning(data.status);
//                    refreshFile();
                    toastr.warning(data.status);
                    refreshFile();
                    GBL_ERROR = data.data;
                    renderError(GBL_ERROR);
                    
                }
                addRemoveLoader(0);
                $('#uploadSalesBomModal').modal('hide');
                // $("body").addClass("loaded");
                // $("#loader-wrapper").hide();
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                // $("body").addClass("loaded");
                addRemoveLoader(0);
                toastr.error(errdata);
                if( jqXHR.responseText != "" ){
                    GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                    renderError(GBL_ERROR);
                }
                refreshFile();
            }
        });
    }));
}

function modeChangeEvent(){
    
    $("input[name=uploadMode]").change(function(){
        var uploadMode = $("input[name=uploadMode]:checked").val();
        console.log(uploadMode);

        if( uploadMode == "csv" ){
            $('.csvModule').show();
            $('.jsonModule').hide();
            $('.xmlModule').hide();
        
        }else if( uploadMode == "json" ){
            $('.jsonModule').show();
            $('.csvModule').hide();
            $('.xmlModule').hide();
        
        }else if( uploadMode == "xml" ){
            $('.xmlModule').show();
            $('.jsonModule').hide();
            $('.csvModule').hide();
        }
    });
}


function jsonWiseSubmit(){

    var uploadJson = $('#uploadJson').val();
    if( uploadJson == "" ){
        toastr.warning('Please Enter Valid Json for Upload');
        $('#uploadJson').focus();
        return false;
    }
    
    toastr.info('Json Data Upload');
}

function xmlWiseSubmit(){
    toastr.info('XML Data Upload');
}

function refreshFile(){

    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

