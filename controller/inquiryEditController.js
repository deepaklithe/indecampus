/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : inquiryEditController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var custId = "-2";
var itemJson = [];
var inqMasterData = [];
var inqTransData = [];
var getData = [];

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

function initRoleAuthData(){
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    setOption("0", "inquirytype", data.data.inqtype, "---Select Inquiry Type---");
    setOption("0", "inquirysource", data.data.sourceof, "---Select Inquiry Source---");

    if ($(data.data.inqDefaultSetting).length > 0) {
        $("#inquirytype").val(data.data.inqDefaultSetting.inqTypeId);
        $("#inquirysource").val(data.data.inqDefaultSetting.inqSrcId);
    }
}

function getitemListingForEdit(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "getItemListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        orgId: checkAuth(4, 14),
        inqId : localStorage.POTGclickInqId
    };
    
    commonAjax(URL1, postData, editItemListingCallBack,CREATINGINQDATA);
}

function editItemListingCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        var option = "<option value='-1'>--- Please Select Item ---</option>"
        for( var i=0; i<data.data.length; i++ ){
            
            var tempJson = '';
            tempJson = {
                clientId: data.data[i].clientId,
                itemId: data.data[i].itemId,
                itemName: data.data[i].itemName,
                itemPrice: data.data[i].itemPrice,
                itemSubType: data.data[i].itemSubType,
                itemType: data.data[i].itemType,
                orgId: data.data[i].orgId,
                validityEndDate: data.data[i].validityEndDate,
            }
            
            itemJson.push(tempJson);
            
            option += "<option value='"+ data.data[i].itemId +"'>"+ data.data[i].itemName +"</option>";
            
        }
        
        
        $('#itemName').html(option);
        $('#itemName').selectpicker();

        getinquiryDetailListing();
        
    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEMS );
        else
            toastr.error(SERVERERROR);
    }
}


function getinquiryDetailListing(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId
    }

    commonAjax(COMMONURL, postData, inquiryDetailCallback,"Please Wait... Getting Inquiry Data");
}


function inquiryDetailCallback(flag,data){

    if (data.status == "Success" && flag) {
    
        console.log(data);
        
        getData = data.data;

        var inqRow = '';
        
        if( data.data[0].inqTransDetail != 'No record found' ){
            
            for( var i=0; i<data.data[0].inqTransDetail.length; i++ ){
                
                inqRow += '<tr>'+
                            '<td>'+ data.data[0].inqTransDetail[i].itemName +'</td>'+
                            '<td>'+ data.data[0].inqTransDetail[i].groupName +'</td>'+
                            '<td>'+ data.data[0].inqTransDetail[i].itemQty +'</td>'+
                            '<td>'+ data.data[0].inqTransDetail[i].itemBasePrice +'</td>'+
                            '<td>'+ ( data.data[0].inqTransDetail[i].itemBasePrice * data.data[0].inqTransDetail[i].itemQty ) +'</td>'+
                            '<td>'+
                                '<button item-id="'+ data.data[0].inqTransDetail[i].itemId +'" type="button" class="btn btn-primary btn-ripple btn-sm btn-danger" onclick="removeItem(this)"><i class="fa fa-trash-o"></i></button>'+
                            '</td>'+
                        '</tr>';


                var tempData = [];
                tempData = {
                    cancleFlag: "0",
                    eventStatus: "17",
                    inqtransId: data.data[0].inqTransDetail[i].inqtransId,
                    itemBasePrice: data.data[0].inqTransDetail[i].itemBasePrice,
                    itemId: data.data[0].inqTransDetail[i].itemId,
                    itemPrice: data.data[0].inqTransDetail[i].itemPrice,
                    itemQty: data.data[0].inqTransDetail[i].itemQty,
                }
                                                        
                inqTransData.push(tempData);
            }
        }
        
        $('#inquiryDetailTable').html(inqRow);
        
        TablesDataTables.init();
        TablesDataTablesEditor.init();

        
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_DATA );
        else
            toastr.error(SERVERERROR);
    }
}

function addItemRow(){
    

    var selectedItemName = $('#itemName').val();
    var itemQty =  $('#itemQty').val();
    
    if( selectedItemName == '-1' ){
        toastr.warning('Please Enter Item Name');
        $('#itemName').focus();
        return false;

    }else if( itemQty == ""){
        toastr.warning('Please Enter Item Quantity');
        $('#itemQty').focus();
        return false;

    }else{
        
        var index = findIndexByKeyValue(itemJson,"itemId",selectedItemName);  
        var newRow = '<tr>'+
                '<td>'+ itemJson[index].itemName +'</td>'+
                '<td>'+ itemJson[index].itemName +'</td>'+
                '<td>'+ itemQty +'</td>'+
                '<td>'+ itemJson[index].itemPrice +'</td>'+
                '<td>'+ ( itemJson[index].itemPrice * itemQty) +'</td>'+
                '<td>'+
                    '<button item-id="'+ itemJson[index].itemId +'" type="button" class="btn btn-primary btn-ripple btn-sm btn-danger" onclick="removeItem(this)"><i class="fa fa-trash-o"></i></button>'+
                '</td>'+
            '</tr>';
        

            var  tempData = {
                    cancleFlag : "1",
                    itemId : itemJson[index].itemId,
                    inqtransId : "",
                    itemQty : itemQty,
                    eventStatus : "17",
                    itemPrice : ( itemJson[index].itemPrice * itemQty ),
                    itemBasePrice : itemJson[index].itemPrice,
                }
        
            inqTransData.push(tempData);

        $('.dataTables_empty').hide();
        $('#inquiryDetailTable').append(newRow);

        $('#itemName').val('-1');
        $('#itemQty').val('');
    }
}


function updateInquiry(){
    
    var inqMasterData = {
            inqDate : getData[0].inqDate,
            projectName : getData[0].projectName,
            inqId : getData[0].inqId,
            inq_trans : inqTransData
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "updateInquiry",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 15),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inq_master : inqMasterData,
    };
   
    // console.log(postData); return false;
    commonAjax(INQURL, postData, createInquiryTroughPopupCallBack,CREATINGINQDATA);
    
}


function createInquiryTroughPopupCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        toastr.success("Inquiry Updated Successfully")
        console.log(data);
        window.location.href = "inquirydetail.html";
        
    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_UPDATE_INQ );
        else
            toastr.error(SERVERERROR);
    }
}

function removeItem(context){

    var itemId = $(context).attr('item-id');

    var index = findIndexByKeyValue(inqTransData,"itemId",itemId);    

    $(context).parents('tr').remove();
    
    if( inqTransData[index].inqtransId == "" ){

        inqTransData.splice(index,1);
    }else{

        inqTransData[index].cancleFlag = '-1';
    }
}