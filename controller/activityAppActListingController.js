/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : April 28 2016.
 * File : ActivityListing.
 * File Type : .js.
 * Project : activityMgmt
 *
 * */

var actList = [];
var activityRportHTML = "";
var typeOfAct = "1";

var actList = [];
var GBL_ACT_OWNER = "1" //MY ACT - 1 -- ALL ACT - 2 -- CREATED BY - 3
var GBL_ACT_CATEGORY = "1" //TODAY - 1 -- DELAYED - 2 -- PLANNED - 3 -- INPROCESS - 4 -- COMPLETED - 5
var GBL_CREATED_BY = "0" 

var curLoadedCat = "";
var curLoadedOwn = "";

var buttonStat = false;
var GBL_ACT_LIST = [];

var GBL_INQ_ID_ACT = "";    // ID USED WHEN CREATE ACT FROM LISTING PAGE OF INQ , SERVICE 

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

/* init globals constants from gbl_ActivityAppGlobals  START */
//Static Globals
var SCS = gbl_ActivityAppGlobals.SCS;
var FAIL = gbl_ActivityAppGlobals.FAIL;
var actUrl = gbl_ActivityAppGlobals.URLV;
var ACT_MODE_PHONE = gbl_ActivityAppGlobals.ACT_MODE_PHONE;
var ACT_MODE_EMAIL = gbl_ActivityAppGlobals.ACT_MODE_EMAIL;
var ACT_MODE_SMS = gbl_ActivityAppGlobals.ACT_MODE_SMS;
var ACT_STATUS_PENDING = gbl_ActivityAppGlobals.ACT_STATUS_PENDING;
var ACT_STATUS_INPROCESS = gbl_ActivityAppGlobals.ACT_STATUS_INPROCESS;
var ACT_STATUS_CLOSED = gbl_ActivityAppGlobals.ACT_STATUS_CLOSED;


/* init globals constants from gbl_ActivityAppGlobals  END */

function activityListinPage() {

    var thisModule = this;

    /* use the functions of moduleForCreateActivity by creating an instance of it. START */
    //i've used this here to encapsulate this in Activity details module..
    var mdlCreateAct = new moduleForCreateActivity();

    /* use the functions of moduleForCreateActivity by creating an instance of it. END */


    /* public functions ----------------START--------------- */

    /* register all needed events for activity details page(this page) START */
    this.registerActListingPageEvents = function ()
    {
        mdlCreateAct.initCreateActModule(callBackCreateAct);

    }
    /* register all needed events for activity details page(this page) END */

    /* make 'create activity' submit call from commonModuleForActivityApp START */
    this.createActivity = function ()
    {
        var refId = "";
        mdlCreateAct.submitCreateActivity(refId); //params: refId

    }

    /* make 'create activity' submit call from commonModuleForActivityApp END */


    /* function to make ajax call and get activity data START */
    this.getActListin = function ()
    {
        var postData = {
            requestCase: "getActivityListingByCreatedBy",
            assignBy: localStorage.indeCampusActivityAppUserId,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            typeOfActListing:typeOfAct,
            orgId: checkAuth(2,6),
        };



        function callBackGetActListin(flag, results) {
            if (results.status == SCS) {
                //log(msg);
                //localStorage.POTGtempActData = JSON.stringify(results);
                var dataArr = results.data.actData;
                actList = results.data.actData;
                // params : array
                GBL_ACT_LIST = results.data;

                renderActivityData(actList);
                // renderActivityData(dataArr);
                // myAct();

            } else {
                var tmpDataArr = [];
                renderActivityData(tmpDataArr);

                //displayAttendenceListin(results);

                // REMOVED MSG WHEN NO ACTIVITY AVAILABLE
                // displayAPIErrorMsg( results.status , GBL_ERR_NO_ACT);
                if (flag) {
                    console.log(results.status);
                } else {
                    console.log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTLIST;
        commonAjax(actUrl, postData, callBackGetActListin, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackGetActListin, actAjaxMsg);
        /* ajax call end  */
    }
    
    function callBackCreateAct()
    {
        console.log("callBackCreateAct function called from activity dashboard page");
        // thisModule.getActListin();
        curLoadedCat = "";
        curLoadedOwn = "";
        getActListingData();

    }    

}

    function assignActivity(activityid,assigneTo)
    {
        console.log("Assign activity change event fired");
        //assignActCallBackOnChange

        if (!activityid)
        {
            console.error(" activityid - Not found");
            // alert("Required Activity data not found to Assign this activity");
            toastr.error("Required Activity data not found to Assign this activity");
            return false;
        }
        if( checkAuth(2, 107, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

        var postData = {
            requestCase: "assignActivity",
            actId: activityid,
            assigneTo: assigneTo,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,107),
        };



        function callBackAssgnAct(flag, results) {
            if (results.status == SCS) {
                // thisModule.getActListin();   //Old Code
                console.log(results.data.totalInqCount);
                localStorage.removeItem('POTGTotalInqCount');
                localStorage.setItem('POTGTotalInqCount', results.data.totalInqCount);
                $('#notifActivityCount').html(results.data.totalInqCount);                

                var index = findIndexByKeyValue(GBL_ACT_LIST.actData,'PK_ACT_ID',activityid);
                if( index != "-1" ){
                    actList[index] = results.data[0];

                    GBL_ACT_LIST.actData.splice( index , 1 );
                    renderActivityData( GBL_ACT_LIST.actData );
                }
            } else {

                console.log("====== FAIL ======");
                displayAPIErrorMsg( results.status , GBL_ERR_ASSIGN_ACT_FAIL);
                if (flag) {
                    console.log(results.status);
                } else {
                    console.log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTDATA;
        commonAjax(actUrl, postData, callBackAssgnAct, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackAssgnAct, actAjaxMsg);
        /* ajax call end  */



    }

/* function to make ajax call and get activity data END */

function renderActivityData(dataArr)
{

    dataArr.forEach(function(record,index) {
        if( record.ASSIGN_TO == localStorage.indeCampusActivityAppUserId ){
            // tempArr.push( record );
        }else{
            buttonStat = true;
            $('.actListBtn').show();
        }
    });

    var todayDate = new Date();

    var innerHtmlToday = '';
    var innerHtmlDelayed = '';
    var innerHtmlPlanned = '';
    var innerHtmlInprocess = '';
    var innerHtmlCompleted = '';

    var todayCount = 0;
    var delayedCount = 0;
    var plannedCount = 0;
    var inprocessCount = 0;
    var completedCount = 0;

    // var pendingFlag = $('#pendingStatChk').is(':checked'); 
    // var inprocessFlag = $('#inprocessStatChk').is(':checked');

    // var tempArr = [];
    // if( !pendingFlag ){
        
    //     dataArr.forEach(function(record , index){
    //         if( record.ACT_STATUS != ACT_STATUS_PENDING ){
    //             tempArr.push( record );
    //         }
    //     })
    //     dataArr = tempArr;
    // }

    // if( !inprocessFlag ){
    //     tempArr = [];
    //     dataArr.forEach(function(record , index){
    //         if( record.ACT_STATUS != ACT_STATUS_INPROCESS ){
    //             tempArr.push( record );
    //         }
    //     })
    //     dataArr = tempArr;
    // }
    // console.log(dataArr);

    var dataArrLen = dataArr.length;
    
    var innerHtml = '';
    var downloadtml = '';

    var tableStartHtml = '<table class="display datatables-alphabet-sorting actDataTable actListinDataTable">';
    var tableStartCompletedHtml = '<table id="actListinCompletedDataTable" class="display datatables-alphabet-sorting actDataTable">';

    var tableEndHtml = '</table>';
    var threadHtml = '<thead>'+
                        '<tr>'+
                        '<th>Sr No.</th>'+
                        '<th>Action</th>'+
                        '<th class="activity-title1">Title | Remark</th>'+
                        '<th>Ref-Type</th>'+
                        '<th>Customer Detail</th>'+
                        '<th>Contact Detail</th>'+
                        '<th>Last Activity</th>'+
                        '<th>Due Date</th>'+
                        '<th>Assigned To</th>'+
                        '</tr>'+
                    '</thead>';

    var threadHtmlStEnHtml = '<thead>'+
                        '<tr>'+
                        '<th>Sr No.</th>'+
                        '<th>Action</th>'+
                        '<th class="activity-title1">Title | Remark</th>'+
                        '<th>Ref-Type</th>'+
                        '<th>Customer Detail</th>'+
                        '<th>Contact Detail</th>'+
                        '<th>Last Activity</th>'+
                        '<th>Due Date</th>'+
                        '<th>Start Time</th>'+
                        '<th>End Time</th>'+
                        '<th>Assigned To</th>'+
                        '</tr>'+
                    '</thead>';

    var tbodyStart = '<tbody id="tbodyActListin">';
    var tbodyEnd = '</tbody>';

    var todayCount = 0;
    var delayedCount = 0;
    var plannedCount = 0;
    var inprocessCount = 0;
    var completedCount = 0;
    console.log(dataArr)
    console.log(dataArrLen)
    if (dataArrLen <= 0)
    {
        innerHtml = ' '; //<li class="nodata">No Data Found ..!</li> 
        todayCount = GBL_ACT_LIST.count.todays;
        delayedCount = GBL_ACT_LIST.count.delayed;
        plannedCount = GBL_ACT_LIST.count.planned;
        inprocessCount = GBL_ACT_LIST.count.inprocess;
        completedCount = GBL_ACT_LIST.count.completed;
    }else{
        todayCount = GBL_ACT_LIST.count.todays;
        delayedCount = GBL_ACT_LIST.count.delayed;
        plannedCount = GBL_ACT_LIST.count.planned;
        inprocessCount = GBL_ACT_LIST.count.inprocess;
        completedCount = GBL_ACT_LIST.count.completed;
    }

    var todayCountList = 0;
    var delayedCountList = 0;
    var plannedCountList = 0;
    var inprocessCountList = 0;
    var completedCountList = 0;

    if( dataArr != "" ){

        dataArr.forEach(function( record , index ){

            /* data init START */
            var activityId = record.PK_ACT_ID;
            var activityTitle = record.ACT_TITLE;
            var activityRefType = record.ACT_REF_TYPE_NAME;
            var activityMode = record.ACT_MODE;
            var activityModeName = record.ACTMODE;
            var activityDate = record.DUE_DATE;
            var activityDueDate = record.DUE_DATE;
            var lastActivityDate = record.LASTACTIVTYDATE;
            var activityAssignedBy = record.CREATEDBY;
            var activityStatus = record.ACT_STATUS;
            var activityAssignedTo = record.ASSIGNEDTO;
            var activityAssignedToId = record.ASSIGN_TO;
            var assignType = record.ASSIGN_TYPE;
            var lastRemark = (record.lastRemark);
            var custName = record.CUTOMERDETAIL;
            var custNum = record.PHONE_NO;
            var cpName = record.CONTACTPERNAME;
            var cpNum = record.CONTACTPERNO;

            var stTime = record.STARTTIME;
            var enTime = record.ENDTTIME;
            /* data init END */

           
           /* activity mode icon init START */
            var activityModeIcon;

            if (activityMode == ACT_MODE_PHONE)
            {
                activityModeIcon = 'fa fa-phone';
            }
            else if (activityMode == ACT_MODE_EMAIL)
            {
                activityModeIcon = 'fa fa-envelope';
            }
            else if (activityMode == ACT_MODE_SMS)
            {
                activityModeIcon = 'glyphicon glyphicon-send';

            }else{
                activityModeIcon = 'fa fa-male';
            }
            /* activity mode icon init END */


            if (activityDate && activityDate != "null" && activityDate != "undefined" && activityDate != "0000-00-00")
            {
                activityDate = new Date(activityDate).format('dd-MM-yyyy');
            }else{
                activityDate = "--";
            }

            if (lastActivityDate && lastActivityDate != "null" && lastActivityDate != "undefined" && lastActivityDate != "--" && lastActivityDate != "0000-00-00" )
            {
                lastActivityDate = new Date(lastActivityDate).format('dd-MM-yyyy');
            }else{
                lastActivityDate = "--";
            }

            //var dataAttr = 'data-activityId="' + activityId + '"';
            var dataAttr = 'data-activityId="' + activityId + '" data-value="' + activityAssignedToId + '"';

            var actAssignToTDHtml = '';

            var imgPath = (record.ASSIGNTOIMAGE) == "" ? httpHead + "//"+FINALPATH+"/assets/globals/img/avtar1.png" : record.ASSIGNTOIMAGE ;
            if (activityAssignedTo)
            {   
                if( activityStatus == 1 || activityStatus == 2 ){

                    actAssignToTDHtml = '<span class="useraImageBody"><img class="userProfile" id="" src="'+ imgPath +'"></span>&nbsp;&nbsp;<span>' + activityAssignedTo + '</span>';
                    actAssignToTDHtmlDownload =  activityAssignedTo ;

                }else if( assignType == "GROUP" )
                {
                    actAssignToTDHtml = 'Group Activity';
                    actAssignToTDHtmlDownload = 'Group Activity';
                    
                }else{

                    actAssignToTDHtml = '<span class="useraImageBody"><img class="userProfile" id="" src="'+ imgPath +'"></span>&nbsp;&nbsp;<a href="javascript:void(0)" ' + dataAttr + ' class="assignmentTd">' + activityAssignedTo + '</a>';
                    actAssignToTDHtmlDownload =  activityAssignedTo ;
                }
            }
            else if( assignType == "GROUP" )
            {
                actAssignToTDHtml = 'Group Activity';
                actAssignToTDHtmlDownload = 'Group Activity';
            }
            else
            {
                actAssignToTDHtml = '<div class="btns">' +
                        '<button ' + dataAttr + ' class="btn btn-primary pulsate-regular btn-sm assignmentTd"> Assign</button>' +
                        '</div>';
                actAssignToTDHtmlDownload =  "Assign";
            }

            /* activity status init START */

            var actStatusName = '';
            var actStatusClass = 'btn-success';
            var actBtns = ''; 
            var bgColor = '';
            var bgColorIc = '';
            var transferred = false;

            if( activityAssignedToId != localStorage.indeCampusActivityAppUserId ){
                // actStatusName = 'Transfer';
                // actBtns = '<span activityStatus="'+ activityStatus +'" class="hide">'+ actStatusName +'</span><i class="fa fa-exchange curPoint" style="font-size:40px;color:#fec310;"></i>';
                transferred = true ;
            }

            if (activityStatus == ACT_STATUS_PENDING)
            {   
                var action = '';
                var actDisabledIcon = 'color:grey;';
                if( !transferred ){
                    action = 'onclick="startActivity(this)"';
                    actDisabledIcon = "color:#fec006;";
                }

                actStatusName = 'Pending';
                actStatusClass = 'btn-warning';
                //actBtns = '<button type="button" id="btnStartActivity" class="btn btn-light-green btn-sm" actId="'+ activityId +'" onclick="startActivity(this);"><i class="fa fa-play" ></i> </button>';
                actBtns = '<span activityStatus="'+ activityStatus +'" class="hide">'+ actStatusName +'</span><i id="btnStartActivity" actId="'+ activityId +'" '+ action +' class="fa fa-play-circle curPoint" style="font-size:40px;'+ actDisabledIcon +' "></i>';
                bgColor = 'background-color:#f0ad4e;border-color: #f0ad4e;';
                bgColorIc = 'color: #f0ad4e;';
                
            }
            else if (activityStatus == ACT_STATUS_INPROCESS){
                var action = '';
                var remarkAction = '';
                var actDisabledIcon = 'color:grey;';
                if( !transferred ){
                    action = 'onclick="openCloseActPop(this)"';
                    remarkAction = 'onclick="openRemarkModal(this)"';
                    actDisabledIcon = "color:#8BC34A;";
                }

                actStatusName = 'In-Process';
                actStatusClass = 'btn-success';
                //actBtns = '<a href="javascript:void(0)" id="btnCloseActivity" actId="'+ activityId +'"  class="btn btn-danger btn-sm" onclick="closeActivity(this);"><i class="glyphicon glyphicon-off" ></i></a>';
                actBtns = '<span activityStatus="'+ activityStatus +'" class="hide">'+ actStatusName +'</span><i class="fa fa-stop-circle curPoint" id="btnCloseActivity" actId="'+ activityId +'" '+ action +' style="font-size:40px;'+ actDisabledIcon +'"></i><i class="fa fa-comments curPoint" id="btnCloseActivity" data-div="remarkfollowup" actId="'+ activityId +'" '+ remarkAction +' style="font-size:40px;'+ actDisabledIcon +'"></i>';
                bgColor = 'background-color:#5cb85c;border-color: #5cb85c;';
                bgColorIc = 'color: #5cb85c;';
            
            }
            else if (activityStatus == ACT_STATUS_CLOSED){   
                var actDisabledIcon = 'color:grey;';
                if( !transferred ){
                    actDisabledIcon = "color:#008ab1;";
                }

                actStatusName = 'Closed';
                actStatusClass = 'btn-danger';
                actBtns = '<span activityStatus="'+ activityStatus +'" class="hide">'+ actStatusName +'</span><i class="fa fa-thumbs-up curPoint" style="font-size:40px;'+ actDisabledIcon +'"></i>';
                bgColor = 'background-color:#d9534f;border-color: #008ab1;';
                bgColorIc = 'color: #008ab1;';
            }   

            /* activity status init END */
            // console.log(activityId);

            var lastRemarkRow = ((lastRemark != "No record found") ? '<tr>'+
                            '<td><i class="fa fa-comments curPoint" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">' + lastRemark + '</a></td>'+
                        '</tr>' : "" );
            
            if( GBL_ACT_CATEGORY == "4" ){

                inprocessCountList = inprocessCountList + 1;

                innerHtml += '<tr>' +
                    '<td>' +inprocessCountList+ '</td>' +
                    '<td>' +actBtns+ '</td>' +
                    '<td data-activityId="'+ activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;">'+
                    '<table>'+
                        '<tr>'+
                            '<td><i class="'+ activityModeIcon +'" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">'+' (Act : '+record.C_ACT_ID+' ) ' + activityTitle + '</a></td>'+
                        '</tr>'+
                            lastRemarkRow+
                    '</table>'+
                    '</td>' +
                    '<td>' + activityRefType + '</td>' +
                    '<td>' + custName + '<br>' + custNum + '</td>' +
                    '<td>' + cpName + '<br>' + cpNum + '</td>' +
                    '<td>' + ( (lastActivityDate == "aN-aN-NaN") ? "--" : lastActivityDate ) + '</td>' +
                    '<td>' + ( (activityDate == "aN-aN-NaN") ? "--" : activityDate ) + '</td>' +
                    '<td class="AssignPopover">' + actAssignToTDHtml + '</td>' +
                    '</tr>';
            
            }else if( GBL_ACT_CATEGORY == "5" ){

                completedCountList = completedCountList + 1; 

                var proActivityDate = "";
                var proStTime1 = "";

                var stTimeBr = "";
                var enTimeBr = "";
                if( stTime != "" ){
                    stTimeBr = stTime.split(" ")[0] + '<br>' + stTime.split(" ")[1] + ' ' + stTime.split(" ")[2];
                    
                    proActivityDate = new Date( ddmmyyToMysql( activityDate ) );
                    proStTime1 = new Date( ddmmyyToMysql( stTime.split(" ")[0] ) );

                    if( proStTime1 > proActivityDate ){
                        bgColor = 'background-color:rgb(217, 83, 79);border-color: rgb(217, 83, 79);';
                        bgColorIc = 'color: rgb(217, 83, 79);';
                        actBtns = '<span activityStatus="'+ activityStatus +'" class="hide">'+ actStatusName +'</span><i class="fa fa-thumbs-up curPoint" style="font-size:40px;color:rgb(217, 83, 79);"></i>';
                    }
                }

                if( enTime != "" ){

                    enTimeBr = enTime.split(" ")[0] + '<br>' + enTime.split(" ")[1] + ' ' + enTime.split(" ")[2];
                }

                var lastRemarkRow = ((lastRemark != "No record found") ? '<tr>'+
                            '<td><i class="fa fa-comments curPoint" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">' + lastRemark + '</a></td>'+
                        '</tr>' : "" );

                innerHtml += '<tr>' +
                    '<td>' +completedCountList+ '</td>' +
                    '<td>' +actBtns+ '</td>' +
                    '<td data-activityId="'+ activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;">'+
                    '<table>'+
                        '<tr>'+
                            '<td><i class="'+ activityModeIcon +'" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">'+' (Act : '+record.C_ACT_ID+' ) ' + activityTitle + '</a></td>'+
                        '</tr>'+
                            lastRemarkRow+
                    '</table>'+
                    '</td>' +
                    '<td>' + activityRefType + '</td>' +
                    '<td>' + custName + '<br>' + custNum + '</td>' +
                    '<td>' + cpName + '<br>' + cpNum + '</td>' +
                    '<td>' + lastActivityDate + '</td>' +
                    '<td>' + ( (activityDate == "aN-aN-NaN") ? "--" : activityDate ) + '</td>' +
                    '<td>' + stTimeBr + '</td>' +
                    '<td>' + enTimeBr + '</td>' +
                    '<td class="AssignPopover">' + actAssignToTDHtml + '</td>' +
                    '</tr>';
            
            }else if( GBL_ACT_CATEGORY == "1" ){

                todayCountList = todayCountList + 1;    

                innerHtml += '<tr>' +
                    '<td>' +todayCountList+ '</td>' +
                    '<td>' +actBtns+ '</td>' +
                    '<td data-activityId="'+ activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;">'+
                    '<table>'+
                        '<tr>'+
                            '<td><i class="'+ activityModeIcon +'" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">'+' (Act : '+record.C_ACT_ID+' ) ' + activityTitle + '</a></td>'+
                        '</tr>'+
                            lastRemarkRow+
                    '</table>'+
                    '</td>' +
                    '<td>' + activityRefType + '</td>' +
                    '<td>' + custName + '<br>' + custNum + '</td>' +
                    '<td>' + cpName + '<br>' + cpNum + '</td>' +
                    '<td>' + ( (lastActivityDate == "aN-aN-NaN") ? "-" : lastActivityDate ) + '</td>' +
                    '<td>' + activityDate + '</td>' +
                    '<td class="AssignPopover">' + actAssignToTDHtml + '</td>' +
                    '</tr>';

            }else if( GBL_ACT_CATEGORY == "2" ){

                delayedCountList = delayedCountList + 1;

                innerHtml += '<tr>' +
                    '<td>' +delayedCountList+ '</td>' +
                    '<td>' +actBtns+ '</td>' +
                    '<td data-activityId="'+ activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;">'+
                    '<table>'+
                        '<tr>'+
                            '<td><i class="'+ activityModeIcon +'" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">'+' (Act : '+record.C_ACT_ID+' ) ' + activityTitle + '</a></td>'+
                        '</tr>'+
                            lastRemarkRow+
                    '</table>'+
                    '</td>' +
                    '<td>' + activityRefType + '</td>' +
                    '<td>' + custName + '<br>' + custNum + '</td>' +
                    '<td>' + cpName + '<br>' + cpNum + '</td>' +
                    '<td>' + ( (lastActivityDate == "aN-aN-NaN") ? "-" : lastActivityDate ) + '</td>' +
                    '<td>' + activityDate + '</td>' +
                    '<td class="AssignPopover">' + actAssignToTDHtml + '</td>' +
                    '</tr>';

            }else if( GBL_ACT_CATEGORY == "3" ){

                plannedCountList = plannedCountList + 1;

                innerHtml += '<tr>' +
                    '<td>' +plannedCountList+ '</td>' +
                    '<td>' +actBtns+ '</td>' +
                    '<td data-activityId="'+ activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;">'+
                    '<table>'+
                        '<tr>'+
                            '<td><i class="'+ activityModeIcon +'" style="'+ bgColorIc +'"></i></td>'+
                            '<td><a href="javascript:void(0)" style="'+ bgColorIc +'">'+' (Act : '+record.C_ACT_ID+' ) ' + activityTitle + '</a></td>'+
                        '</tr>'+
                            lastRemarkRow+
                    '</table>'+
                    '</td>' +
                    '<td>' + activityRefType + '</td>' +
                    '<td>' + custName + '<br>' + custNum + '</td>' +
                    '<td>' + cpName + '<br>' + cpNum + '</td>' +
                    '<td>' + ( (lastActivityDate == "aN-aN-NaN") ? "-" : lastActivityDate ) + '</td>' +
                    '<td>' + activityDate + '</td>' +
                    '<td class="AssignPopover">' + actAssignToTDHtml + '</td>' +
                    '</tr>';
            }
             //For Download All activity Listing
            downloadtml += '<tr>' +
                    '<td>' +(index+1)+ '</td>' +
                    '<td>'+ actStatusName +'</td>'+
                    '<td>'+ activityTitle + '</td>'+
                    '<td>'+ (lastRemark == "No record found" ? "--" : lastRemark) + '</td>'+
                    '<td>' + activityRefType + '</td>' +
                    '<td>' + custName + '</td>' +
                    '<td>' + custNum + '</td>' +
                    '<td>' + cpName + '</td>' +
                    '<td>' + cpNum + '</td>' +
                    '<td>' + activityDate + '</td>' +
                    '<td>' + ( (lastActivityDate == "aN-aN-NaN") ? "-" : lastActivityDate ) + '</td>' +
                    '<td class="AssignPopover">' + actAssignToTDHtmlDownload + '</td>' +
                    '</tr>';
        });
    }

    var downloadthreadHtml = '<thead>'+
                        '<tr>'+
                        '<th>No.</th>'+
                        '<th>Status</th>'+
                        '<th class="activity-title">Title</th>'+
                        '<th>Last Remark</th>'+
                        '<th>Ref-Type</th>'+
                        '<th>Name</th>'+
                        '<th>Number</th>'+
                        '<th>Contact Name</th>'+
                        '<th>Contact Number</th>'+
                        '<th>Due Date</th>'+
                        '<th>LastActivity</th>'+
                        '<th>Assigned To</th>'+
                        '</tr>'+
                    '</thead>';

    if(downloadtml != ""){ 
        activityRportHTML = tableStartHtml + downloadthreadHtml + tbodyStart + downloadtml + tbodyEnd + tableEndHtml;
    }
    var finalHtmlAct = tableStartHtml + threadHtml + tbodyStart + innerHtml + tbodyEnd + tableEndHtml;
    var finalHtmlCompletedAct = tableStartCompletedHtml + threadHtmlStEnHtml + tbodyStart + innerHtml + tbodyEnd + tableEndHtml;

    $('#todayActListing').html('');
    $('#todayActListing').html(finalHtmlAct);
    
    $('#delayedActListing').html('');
    $('#delayedActListing').html(finalHtmlAct);
    
    $('#PlannedActListing').html('');
    $('#PlannedActListing').html(finalHtmlAct);
    
    $('#inprocessActListing').html('');
    $('#inprocessActListing').html(finalHtmlAct);

    $('#completedActListing').html('');
    $('#completedActListing').html(finalHtmlCompletedAct);

    $('#todayCnt').html( '(' +todayCount+ ')' );
    $('#delayCnt').html( '(' +delayedCount+ ')' );
    $('#upcmngCnt').html( '(' +plannedCount+ ')' );
    $('#inprocessCnt').html( '(' +inprocessCount+ ')' );
    $('#completeCnt').html( '(' +completedCount+ ')' );

//    setTimeout(function(){
//        registerAssignToPopUpEvent();
//        //generateDatatable("actListinDataTable");
//        // TablesDataTables.init();
//        // TablesDataTablesEditor.init();
//    },500);
    
    if( GBL_ACT_CATEGORY != "5" ){
        $('.actListinDataTable').DataTable({
            "deferRender": true,
            "stateSave": true,
            "fnInitComplete" : function(){
                var userList = JSON.parse(localStorage.POTGactivityAppUserListJson);

                $('.assignmentTd').editable({
                    type: 'select',
                    pk: 1,
                    value: '1',
                    placement: 'left',
                    source: userList,
                    title: 'Assign To',
                    validate: function (value) {

                        if( checkAuth(2, 107, 1) == -1 ){
                            toastr.error('You are not Authorised for this Activity');
                            return false;

                        }
                        //var dataValue = $.trim(value);
                        if (!value || value == "-1" ) {
                            return 'This field is required';
                        }  

                        var activityid = $(this).attr('data-activityid');
                        assignActivity(activityid,value);
                        //updateCommentInAct(remarkIndexId, remark);

                    }
                });
            }
        });
    }else{
        $('#actListinCompletedDataTable').DataTable({
            "deferRender": true,
            "stateSave": true
        });
    }
    
    // $('#actListinDataTable').DataTable( {
    //     "order": [[ 1, "asc" ]]
    // });

    
    // $('#actListinDataTable').dataTable();

    if( GBL_ACT_CATEGORY == "1" ){
        $('#todayActTab').click();

    }else if( GBL_ACT_CATEGORY == "2" ){
        $('#delayedActTab').click();

    }else if( GBL_ACT_CATEGORY == "3" ){
        $('#plannedActTab').click();

    }else if( GBL_ACT_CATEGORY == "4" ){
        $('#inprocessActTab').click();

    }else if( GBL_ACT_CATEGORY == "5" ){
        $('#completedActTab').click();
    }
}

    
/* funtion to bind 'Assign to' popup event START */
function registerAssignToPopUpEvent()
{
    var userList = JSON.parse(localStorage.POTGactivityAppUserListJson);

    $('.assignmentTd').editable({
        type: 'select',
        pk: 1,
        value: '1',
        placement: 'left',
        source: userList,
        title: 'Assign To',
        validate: function (value) {

            if( checkAuth(2, 107, 1) == -1 ){
                toastr.error('You are not Authorised for this Activity');
                return false;

            }
            //var dataValue = $.trim(value);
            if (!value || value == "-1" ) {
                return 'This field is required';
            }  

            var activityid = $(this).attr('data-activityid');
            assignActivity(activityid,value);
            //updateCommentInAct(remarkIndexId, remark);

        }
    });

}

/* funtion to bind 'Assign to' popup event END */

var actListin;

/* funtion to bind 'Assign to' popup event END */
function initActivityListingPage()
{
    
    actListin = new activityListinPage();
    // actListin.getActListin();
    actListin.registerActListingPageEvents();
    //setTimeout(registerAssignToPopUpEvent(),4000);

    $('#myAct').attr('disabled',true);
    $('#allAct').attr('disabled',false);

    $('#allAct').css('background-color','#d9534f');
    $('#allAct').css('border-color','#d9534f');

    $('#myAct').css('background-color','#8ac249');
    $('#myAct').css('border-color','#8fc551');

}

function openModalActivity(){
    
    if( checkAuth(2, 5) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    if( actModeHideShow() == "-1" ){  //  HIDE SHOW ACTIVITY MODE ACCORDING TO ROLE AUTH RIGHTS
        toastr.warning( NOACTMODERIGHTS );
        return false;
    }

    resetPopups();

    $('#ddActAssignedTo').html(GBL_ALL_USER_HTML);
    $('#ddActAssignedTo').selectpicker('refresh');
    
    var currTime = formatAMPMRoundOff( new Date() );
    // var currTime = formatAMPMRoundOff( new Date('Thu Nov 24 2016 11:55:01 GMT+0530 (IST)') );
    var d = new Date();
    var remindTime = d.setMinutes(d.getMinutes() - 20);
    remindTime = formatAMPMRoundOff( new Date(remindTime) );

    $('#dueTime').val( currTime );
    $('#endNormTime').val( currTime );
    $('#reminderTime').val( remindTime );

    $('#dueTime').on('pick.clockface', function() 
    {
        var d = new Date('01-01-1970 '+$('#dueTime').val());
        var remindTime = d.setMinutes(d.getMinutes() - 20);
        remindTime = formatAMPMRoundOff( new Date(remindTime) );
        $('#reminderTime').val( remindTime );
    });

    $('.scheduledActInq').show();

    $('#customerType').val('SINGLE').trigger('change');
    $('#customerType').selectpicker('refresh');

    $('#searchCustName').show();
    $('#unRegCustNameMobile').hide();

    $('#unregCustName').val("");
    $('#unregCustMobile').val("");
    
    $('#ddActAssignedTo').val(localStorage.indeCampusActivityAppUserId); 
    $('#ddActAssignedTo').selectpicker('refresh');    
    
    if( checkAuth(4,14) != "-1" ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getInqListingByUser',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,14),
            custId: tmp_json.data[0].PK_USER_ID,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }

        commonAjax(COMMONURL, postData, inquiryDataCallBack,INQUIRYDETAILGET);

        function inquiryDataCallBack(flag,data) {

            if(data.status == "Success" && flag){

                $('#inqListActDiv').show();
                
                var inqName = "No Inquiry Name";

                var inqLogOptions = '<option value=""> --Please Select Inquiry -- </option>';

                if( data.data != "No record found" ){

                    data.data.forEach( function( record , index ){

                        if( record.inqName != "" ){
                            inqName = record.inqName;
                        } 
                        inqLogOptions += '<option projId="'+ record.projectId +'" contactId="'+ record.contactId +'" custId="'+ record.custId +'" value="'+ record.inqId +'">'+ record.ClientLevelInqId +' - ' + inqName + '</option>';
                    });
                }

                $('#inqListAct').html( inqLogOptions );
                $('#inqListAct').selectpicker('refresh');

                // CHECK INQ ID REDIRECTED FROM LISTING PAGE - INQ , SERVICE , ORDER
                $('#inqListAct').attr('disabled',false);
                if( localStorage.POTGinqIdOpenPop != undefined && localStorage.POTGinqIdOpenPop != "" ){
                    $('#inqListAct').val(localStorage.POTGinqIdOpenPop);
                    $('#inqListAct').attr('disabled',true);
                    $('#inqListAct').selectpicker('refresh');

                    GBL_INQ_ID_ACT = localStorage.POTGinqIdOpenPop;

                    if( (localStorage.actCreateRefType).toLowerCase() == "service" || (localStorage.actCreateRefType).toLowerCase() == "order" ){
                        $('#inqListActDiv').hide();
                    }
                    localStorage.removeItem('POTGinqIdOpenPop');
                }

                $('#ddActAssignedTo').val(localStorage.indeCampusActivityAppUserId);  
                $('#ddActAssignedTo').selectpicker('refresh');     

            }else{
                if (flag)
                    // displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ);
                    console.log(data.status)
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }

    }

    var currClientType = JSON.parse( localStorage.indeCampusRoleAuth ).data.clientTypeId;
    // currClientType = 33
    if( currClientType == "32" ){
        $('#unRegOpt').show();
        $('#customerType').selectpicker('refresh');
    }
    if( currClientType == "32" && checkAuth(50,200) != "-1" ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getRealEstateMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(50,200),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }

        commonAjax(COMMONURL, postData, projectDataCallBack,INQUIRYDETAILGET);

        function projectDataCallBack(flag,data) {

            if(data.status == "Success" && flag){

                $('#projListActDiv').show();
                
                var options = '<option value=""> --Please Select Project -- </option>';

                if( data.data != "No record found" ){
                    data.data.forEach(function( record , index ){
                        options += '<option value="'+ record.projectId +'"> '+ record.projectName +' </option>';
                    });
                }

                $('#projectListAct').html( options );
                $('#projectListAct').selectpicker('refresh');

                $('#projectListAct').val('-1');
                $('#projectListAct').selectpicker('refresh'); 

            }else{
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_REAL_ESTATE);
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }

    }
    
    $('#txtActTitle').val('Call');

    $('#projectListAct').removeAttr('disabled').selectpicker('refresh');
    $('#inqListAct').removeAttr('disabled').selectpicker('refresh');
    $('.custInfoAct').show();
 
    addFocusId( 'txtActTitle' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    $('#creatactivity').modal('show');
}

function startActivity(context){

    if( checkAuth(2, 7) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    var actId = $(context).attr('actId');

    $('.fa-play-circle.curPoint').removeAttr('onclick');
    var postData = {
        requestCase: "startActivity",
        actId: actId,
        userId: localStorage.indeCampusActivityAppUserId,
        clientId: localStorage.indeCampusActivityAppClientId,
        orgId: checkAuth(2,7),
    };
    commonAjax(ACTURL, postData, callBackStartActivity, '');

    function callBackStartActivity(flag, results) {
        if (results.status == SCS) {

            var index = findIndexByKeyValue(GBL_ACT_LIST.actData,'PK_ACT_ID',actId);
            if( index != "-1" ){
                actList[index] = results.data[0];
                GBL_ACT_LIST.actData.splice( index , 1 );

                GBL_ACT_LIST.count.inprocess = parseInt( GBL_ACT_LIST.count.inprocess ) + 1;

                if( GBL_ACT_CATEGORY == "1" ){
                    GBL_ACT_LIST.count.todays = parseInt( GBL_ACT_LIST.count.todays ) - 1;
                
                }else if( GBL_ACT_CATEGORY == "2" ){
                    GBL_ACT_LIST.count.delayed = parseInt( GBL_ACT_LIST.count.delayed ) - 1;

                }else if( GBL_ACT_CATEGORY == "3" ){
                    GBL_ACT_LIST.count.planned = parseInt( GBL_ACT_LIST.count.planned ) - 1;
                }

                renderActivityData( GBL_ACT_LIST.actData );
            } 


        } else {
            if (flag) {
                console.log(results.status);
                displayAPIErrorMsg( results.status , GBL_ERR_START_ACT_FAIL);
            } else {
                toastr.error("====== No Records Found ======");
            }
        }
    }
}


var GBL_ACT_ID = "";
function openCloseActPop(context){


    if( checkAuth(2, 7) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    GBL_ACT_ID = $(context).attr('actId'); 
    $('#closeActRemarkModal').modal('show');
    addFocusId( 'closeActReasonTxt' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}


function closeActivity(context){

    var closeActReasonId = $('#closeActReason').val();
    var closeActReasonText = $('#closeActReasonTxt').val();
    
    if( closeActReasonId == "-1" ){
        toastr.warning('Please Select Outcome');
        $('#closeActReason').focus();
        return false;

    }else if( closeActReasonText == "" ){
        toastr.warning('Please Enter Close Text');
        $('#closeActReasonTxt').focus();
        return false;
    }

    var postData = {
        requestCase: "closeActivity",
        actId: GBL_ACT_ID,
        userId: localStorage.indeCampusActivityAppUserId,
        clientId: localStorage.indeCampusActivityAppClientId,
        closeActReasonId: closeActReasonId,
        closeActReasonText: closeActReasonText,
        orgId: checkAuth(2,7),
    };

    function callBackCloseActivity(flag, results) {
        if (results.status == SCS) {
            
            $('#closeActRemarkModal').modal('hide');
            
            var index = findIndexByKeyValue(GBL_ACT_LIST.actData,'PK_ACT_ID',GBL_ACT_ID);
            if( index != "-1" ){
                actList[index] = results.data[0];
                GBL_ACT_LIST.actData.splice( index , 1 );

                GBL_ACT_LIST.count.completed = parseInt( GBL_ACT_LIST.count.completed ) + 1;
                GBL_ACT_LIST.count.inprocess = parseInt( GBL_ACT_LIST.count.inprocess ) - 1;

                renderActivityData( GBL_ACT_LIST.actData );
            } 

            $('#closeActRemarkModal').modal('hide');
        } else {

            if (flag) {
                displayAPIErrorMsg( results.status , GBL_ERR_CLOSE_ACT_FAIL);
                // toastr.error(results.status);
            } else {
                toastr.error("====== No Records Found ======");
            }
        }
    }

    commonAjax(ACTURL, postData, callBackCloseActivity, '');;
    /* ajax call end  */
}

function resetPopups(){

    $('#birthdate').val('');
    $('#birthdays').val('');
    $('#anniversaryDate').val('');
    $('#anniversaryDays').val('');
    $('#ethnicity').val('-1');
    $('#ethnicity').selectpicker('refresh');
    $('#stateListing').val('-1');
    $('#stateListing').selectpicker('refresh');
    $('#cityListing').val('-1');
    $('#cityListing').selectpicker('refresh');
    $('#salesYear').val('-1');
    $('#salesYear').selectpicker('refresh');
    $('#salesAmtFrom').val('');
    $('#ltdytdAmt').val('');
    $('#ltdytdAmt').val('');
    $('#itemList').val('');
    $('#groupListing').val('');
    temp_inq_Trans = [];
    // addItemRow(temp_inq_Trans);
    $('#titleSmart').val('');
    $('#descriptionSmart').val('');
    $('#dueDateSmart').val('');
    $('#txtActStartDate').val('');
    $('#txtActEndDate').val('');
    $('#txtActTitle').val('');
    $('#txtActDesc').val('');
    $('#ddActAssignedToSmart').val('-1');
    $('#ddActAssignedToSmart').selectpicker('refresh');
    $('#ddActAssignedToSmart').selectpicker('refresh');
    $('#ddActAssignedTo').val('-1');
    $('#ddActAssignedTo').selectpicker('refresh');
    $('#scheduleAct').prop('checked',false);
    $('.scheduledActBlk').hide();

    $('#reminderActChk').prop('checked',false);
    $('.reminderActChkCls').hide();
    $('#reminderDate').val('');
    $('#reminderTime').val('');

    $('#txtActDueDate').val('');
    $('#txtActNormEndDate').val('');
    $('#actAttachment').val('');
    $('#txtCustGrpName').val('');
    $('#txtCustGrpName').attr('data-id','');

    $('#contPersonDivAct').hide();

    $('#radio-13').click();
    $('#radio-05').click();
}

function myAct(){

    GBL_ACT_OWNER = "1";
    getActListingData();
}

function allAct(){

    GBL_ACT_OWNER = "2";
    getActListingData();
}

function createdBy(){

    GBL_ACT_OWNER = "3";
    getActListingData();
}


function setRemindDate(actDate){
    $('#reminderDate').val( actDate );
    $(".remindDate").datetimepicker('remove');

    var actTrimDate = (( actDate ).split('|')[0]).trim();
    var actTime = (( actDate ).split('|')[1]).trim();

    $('.remindDate').datetimepicker({
        // startDate: 'today',
        // endDate: actDate,
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        endDate: new Date( ddmmyyToMysql( actTrimDate ) +" "+ actTime ),
        autoclose: true,
        minuteStep: 15,
    });
    $('.remindDate').prop("readonly",true);

    var date = (($('#txtActDueDate').val() ).split('|')[0]).trim();
    var time = (( $('#txtActDueDate').val() ).split('|')[1]).trim();

    $(".default-EndDateTime-picker").datetimepicker('remove');
    $('.default-EndDateTime-picker').datetimepicker({
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date( ddmmyyToMysql( date ) +" "+ time ),
        autoclose: true,
        minuteStep: 15,
    })
    $('.default-EndDateTime-picker').prop("readonly",true);

    if( $('input[type=radio][name="rdoActMode"]').filter(":checked").val() == "4" ){    // ADD 60 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#txtActDueDate').val() , 60 )
    }else{    // ADD 15 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#txtActDueDate').val() , 15 )
    }
}

function pageInitialEvents(){

    if( localStorage.POTGinqIdOpenPop != undefined && localStorage.POTGinqIdOpenPop != "" ){
        toastr.warning('Please Do not Refresh the Page');
        openModalActivity();
        setTimeout( function(){
            $('.custInfoAct').hide();  
        },200);
    }

    $('.dueDatedp').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true,
    }).on('changeDate', checkActList);

    $('.dueDatedp-dateTime').datetimepicker({
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        autoclose: true,
        minuteStep: 15,
    }).on('changeDate', checkActList);

    $('#dueTime').on('pick.clockface', checkActList);

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var actType = roleAuth.data.actType;

    setOption("0", "actTypeListSch", actType, "");
    $('#actTypeListSch').selectpicker( 'refresh' );

    if( localStorage.POTGInqToWhyInqStat != "" && localStorage.POTGInqToWhyInqStat != undefined ){
        
        localStorage.removeItem('POTGInqToWhyInqStat');
        $('.complelted a').click();
    }
    
    $(".panel-body .nav-tabs").on("click", "li", function(){
        var tabId = $(this).attr('hrefId');

        $('.tab-content .active').removeClass('active');
        $('#'+tabId).addClass('active');
    });
    
    // CHANGES FOR BUG ON LIVE SERVER - 3
    if( localStorage.POTGCreateActCustId != "" && localStorage.POTGCreateActCustId != undefined ){
        //Bind Customer
        setTimeout(function(){
            toastr.warning('Please Do not Refresh the Page');
            openModalActivity();

        },100);

        setTimeout(function(){

            $('#creatactivity .modal-body').animate({
                scrollTop: $("#txtCustGrpName").offset().top
            });

            // SET CUSTOMER NAME 
             $('#txtCustGrpName').val( localStorage.POTGCreateActCustName );
            $('#txtCustGrpName').attr( 'data-id' , localStorage.POTGCreateActCustId );

            // SET CONT PERS NAME 
            $('#actContPersonSear').val( localStorage.POTGCustContDetail );
            $('#actContPersonSear').attr( 'contperid' , localStorage.POTGCreateActContId );
            $('#contPersonDivAct').show();

            var cpAllList = [];
            var contactData = JSON.parse( localStorage.POTGCreateActContList );
            if( contactData != "No record found" ){

                contactData.forEach( function( record , index ){
                    var tempData = {
                        CONTACTNAME : record.fname +" "+ record.lname,
                        CONTACT_NUMBER : record.contactNumber,
                        PK_CONTACT_PERSON_ID : record.contactId,
                    }
                    cpAllList.push( tempData )
                });
            }

            var finalContactList = {
                cpAllList : cpAllList
            };  

            showCpAndInitAutocomplete( finalContactList );

            // $("input#txtCustGrpName").autocomplete('search', localStorage.POTGCreateActCustMob);
            setTimeout(function(){

            // REMOVED LOCAL STORAGE
            // $('.ui-autocomplete .ui-menu-item').click();
                localStorage.removeItem('POTGCreateActCustName');
                localStorage.removeItem('POTGCreateActCustMob');
                localStorage.removeItem('POTGCreateActCustId');
                localStorage.removeItem('POTGCreateActContId');
                localStorage.removeItem('localStorage.POTGCustContDetail');
                localStorage.removeItem('localStorage.POTGCreateActContList');
            },500);
        },2500);

    }

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var actType = roleAuth.data.actType;
    
    setOption("0", "actTypeListUpload", actType, "");
    $('#actTypeListUpload').selectpicker( 'refresh' );

    setOption("0", "closeActReason", roleAuth.data.actCloseType, "---Select Activity Outcome---");
    $('#closeActReason').selectpicker( 'refresh' );

    //assignAttachementEvent();
    uploadActivity(); // ADDED BY DEEPAK PATIL ON 21-02-2017
    
    $('input[name=rdoActMode]').change(function(){
        if($(this).val() == "1"){
            $('#txtActTitle').val('Call');

        }else if($(this).val() == "2"){
            $('#txtActTitle').val('Send Email');

        }else if($(this).val() == "3"){
            $('#txtActTitle').val('Send SMS');

        }else if($(this).val() == "4"){
            $('#txtActTitle').val('Meet');

        }else {
            $('#txtActTitle').val('');
        }
    });

    $('input[type=radio][name=singalradioassignSmart]').change(function() {
        if (this.value == '2') {
            $('.defaultSelect').hide();
        }
        else {
            $('.defaultSelect').show();
        }
    });

    $('#scheduleAct').change(function() {
        var remindchk = $('#reminderActChk').is(':checked');
        if($(this).is(":checked")) {
            $('.scheduledActBlk').show();
            $('.scheduledActInq').hide();
            $('.reminderActChkCls').hide();
            if( remindchk ){
                $('.reminderActChkCls').hide();
                $('.reminderSchTime').show();
            }
        }else{
            $('.scheduledActBlk').hide();
            $('.scheduledActInq').show();
            $('.reminderActChkCls').hide();
            if( remindchk ){
                $('.reminderDateDiv').show();
            }
        } 
    });

    $('#reminderActChk').change(function() {
        var scheduleChk = $('#scheduleAct').is(':checked');
        if($(this).is(":checked")) {
            $('.reminderActChkCls').show();
            $('.reminderSchTime').hide();
            if( scheduleChk ){
                $('#reminderDate').val('');
                $('.reminderDateDiv').hide();
                $('.reminderSchTime').show();
            }
        }else{
            $('.reminderActChkCls').hide();
        } 
    });

    initTelInput( 'unregCustMobile' );   // INITIALIZE COUNTRY WISE MOBILE NUM

}



function downloadActivity(){


    if( checkAuth(2, 6) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

    }

    if( activityRportHTML.length < 1 ){
        toastr.warning('You dont have any Activity for Download...');
    }else{
        customCsvFormatSave(activityRportHTML, "Activity.xls");
    }
}

function downloadActivitySample(){


    if( checkAuth(2, 6) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

    }

    var GBL_ACTJSON = [];
    var tempActJson = {
        actTitle : "Activity Title",
        actDesc : "Activity Description",
        dueDate : "Due Date",
        custName : "Customer Name",
        custNo : "Customer No",
        user : "User",
        dueTime : "Due Time",
        reminder : "Reminder",
        taskName : "Task Name",
    }
    GBL_ACTJSON.push( tempActJson );

    csvSave(GBL_ACTJSON, "ActivitySample.csv");
    
}

//START FUNCTION FOR ADD Activity
function uploadAct(){

    if( checkAuth(2, 5, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var uploadFileName = $("#attachmentName").val();
    if(!uploadFileName){
        toastr.warning('Please select file');
        $('#attachmentName').click();
        return false;
    }
    $("#addDocument").submit();
}


function uploadActivity(){

    addRemoveLoader(1);
    $("#addDocument").off().on('submit', (function (e) {
        //alert("asdasd");
            var actType = $('#actTypeListUpload').val();

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            var temp_json = {
                requestCase : "uploadMasterData",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(3, 10),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                uploadType : "activityUpload",
                actType : actType,
                monthId : "",
                yearId : "",
            };

            $("#postDataActUpload").val(JSON.stringify(temp_json));
        
            e.preventDefault();
            addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            $.ajax({
                url: UPLOADURL, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);
                    addRemoveLoader(1);
                    $("#ajaxloader").removeClass("hideajaxLoader");

                },
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    console.log(data);
                    
                    if(data.status == "Success"){
                        
                        console.log(data.data);
                        addRemoveLoader(0);
                        refreshFile();
                        $('#errorHideDiv').hide();
                        $('#errorDiv').html("");
                        $('#uploadAct').modal('hide');
                        toastr.warning( data.data );
                    }else{
                        toastr.warning(data.status);
                        addRemoveLoader(0);
                        refreshFile();
                        GBL_ERROR = data.data;
                        renderError(GBL_ERROR);
                    }
                    // $("body").addClass("loaded");
                    // $('#uploadAct').modal('hide');
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                    //$("body").addClass("loaded");
                    toastr.error(errdata);
                    if( jqXHR.responseText != "" ){
                        GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                        renderError(GBL_ERROR);
                    }
                    refreshFile();
                }
            });
        }));
}

function assignAttachementEvent(){
    
    $('#postDataActUpload').change(function () {
        
        var actType = $('#actTypeListUpload').val();

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 10),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            uploadType : "activityUpload",
            actType : actType,
            monthId : "",
            yearId : "",
        };

        $("#postDataActUpload").val(JSON.stringify(temp_json));
        

        var bfsendMsg = "Sending data to server..";

         $("#addDocument").off().on('submit', (function (e) {

           e.preventDefault();
            $.ajax({
                url: UPLOADURL, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    $("#msgDiv").html(bfsendMsg);
                    $("#ajaxloader").removeClass("hideajaxLoader");

                },
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    console.log(data);
                    
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                        }

                        attachmetData.push( tempData );

                        renderAttachment();                        
                        refreshFile();
                    }else{
                        toastr.warning(data.status);
                        refreshFile();
                        
                    }
                    $("body").addClass("loaded");
                    $('#uploadAct').modal('hide');
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    $("body").addClass("loaded");
                    refreshFile();
                }
            });
        }));
    });
}

//END FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA
function refreshFile(){

    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}


function openUploadDialog(){

    if( checkAuth(2, 5) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    } 
    $('#errorHideDiv').hide();
    $('#uploadAct').modal('show');
    addFocusId( 'actTypeListUpload' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var GBL_ACT_ID = "";
function openRemarkModal(context){


    if( checkAuth(2, 7) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }   

    GBL_ACT_ID = $(context).attr('actid'); 
    addFocusId( 'remarkbox' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    $('#remarkActModal').modal('show');
}

function saveNewRemark(){

    var remarkVal = $('#remarkbox').val();
    if (!remarkVal)
    {
        toastr.error("Please Insert the Remark");
        $('#remarkbox').focus();
        return false;
    }

    var postData = {
        requestCase: "insertCommentInAct",
        actId: GBL_ACT_ID,
        userId: localStorage.indeCampusActivityAppUserId,
        clientId: localStorage.indeCampusActivityAppClientId,
        remark: remarkVal,
        remarkIndexId: "0",
        orgId: checkAuth(2,7),
    };
    commonAjax(actUrl, postData, callBackInsertCommentInAct, "");

    function callBackInsertCommentInAct(flag, results) {
        if (results.status == SCS) {
            toastr.success('Your Remark is successfully added in this Activity');
            $('#remarkbox').val("");
            $('#remarkActModal').modal('hide');
             
            var index = findIndexByKeyValue(GBL_ACT_LIST.actData,'PK_ACT_ID',GBL_ACT_ID);
            if( index != "-1" ){
                actList[index] = results.data[0];
                GBL_ACT_LIST.actData[index] = results.data[0];
                renderActivityData( GBL_ACT_LIST.actData );
            }

        } else {
            if (flag) {
                console.log(results.status);
                displayAPIErrorMsg( results.status , GBL_ERR_REMARK_INSERT_ACT_FAIL);
                // toastr.error(results.status);
            } else {
                console.log("====== No Records Found ======");
            }
        }
    }
}


function getActListing(context){

    var actCategory = $(context).attr('type');
    GBL_ACT_CATEGORY = actCategory;
    getActListingData();
}

function getActListingData(){ 

    if( (curLoadedCat == GBL_ACT_CATEGORY) && (curLoadedOwn == GBL_ACT_OWNER) ){
        return false;   
    }
    
    if( GBL_ACT_OWNER == "1" ){
        $('#myAct').attr('disabled',true);
        $('#allAct').attr('disabled',false);
        $('#createdByAct').attr('disabled',false);
    
        $('#allAct').css('background-color','#d9534f');
        $('#allAct').css('border-color','#d9534f');

        $('#myAct').css('background-color','#8ac249');
        $('#myAct').css('border-color','#8fc551');

    }else if( GBL_ACT_OWNER == "3" ){
        $('#createdByAct').attr('disabled',true);
        $('#allAct').attr('disabled',false);
        $('#myAct').attr('disabled',false);
    
        $('#allAct').css('background-color','#d9534f');
        $('#allAct').css('border-color','#d9534f');

        $('#myAct').css('background-color','#8ac249');
        $('#myAct').css('border-color','#8fc551');

    }else{
        $('#allAct').attr('disabled',true);
        $('#myAct').attr('disabled',false);
        $('#createdByAct').attr('disabled',false);
        
        $('#allAct').css('background-color','#8ac249');
        $('#allAct').css('border-color','#8fc551');

        $('#myAct').css('background-color','#d9534f');
        $('#myAct').css('border-color','#d9534f');
    }

    var postData = {
        requestCase: "getActivityListingByCreatedBy",
        assignBy: localStorage.indeCampusActivityAppUserId,
        userId: localStorage.indeCampusActivityAppUserId,
        clientId: localStorage.indeCampusActivityAppClientId,
        ownerType : GBL_ACT_OWNER, // My Activity
        actCategory : GBL_ACT_CATEGORY, // My Activity
        orgId: checkAuth(2,6),
    };
    
    commonAjax(actUrl, postData, getActListingDatacallBack, "Lemme! Get the Act Listing for you");
    function getActListingDatacallBack(flag, results) {
        if (results.status == SCS) {
            //log(msg);
            //localStorage.tempActData = JSON.stringify(results);
            
            curLoadedCat = GBL_ACT_CATEGORY;
            curLoadedOwn = GBL_ACT_OWNER;

            var dataArr = results.data.actData;
            actList = results.data.actData;
            GBL_ACT_LIST = results.data;

            renderActivityData(actList);

        } else {

            curLoadedCat = GBL_ACT_CATEGORY;
            curLoadedOwn = GBL_ACT_OWNER;

            var tmpDataArr = [];
            GBL_ACT_LIST = results.data;
            renderActivityData(tmpDataArr);
            //displayAttendenceListin(results);
            console.log("====== No Records Found ======");

            // REMOVED MSG WHEN NO ACTIVITY AVAILABLE
            // displayAPIErrorMsg( results.status , GBL_ERR_NO_ACT);
            if (flag) {
                console.log(results.status);
            } else {
                console.log("====== No Records Found ======");
            }
        }
    }
}