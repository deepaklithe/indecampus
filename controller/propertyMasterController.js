/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : propertyMasterController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_PROPERTY_LIST = []; 
var GBL_TIMEZONE_DATA = []; 
var GBL_PROP_EDIT_ID = ''; 
var GBL_PROP_NAME_EDIT = ''; 

// GET TIME ZONE MASTER LISTING DATA
function getTimeZoneMasterData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getTimezoneData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31 , 117),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getTimeZoneMasterDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET TIME ZONE MASTER LISTING DATA CALLBACK
function getTimeZoneMasterDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        GBL_TIMEZONE_LIST = data.data;  
        renderTimezoneData(); 

    } else {
        GBL_TIMEZONE_LIST = [];  
        renderTimezoneData(); 
        if (flag)
            displayAPIErrorMsg( data.status , 'No Timezone Available' );
        else
            toastr.error(SERVERERROR);
    }
    
}

// RENDER TIMEZONE IN DROPDOWN
function renderTimezoneData(){  

    var options = '<option value="-1">Select Timezone</option>';
    if( GBL_TIMEZONE_LIST != "" && GBL_TIMEZONE_LIST != "No record found" ){
        GBL_TIMEZONE_LIST.forEach(function( record , index ) {
            options += '<option value="'+ record.timezoneId +'">'+ record.timezoneName +'</option>';
        });
    }
    $('#timeZone').html( options );
    $('#timeZone').selectpicker('refresh');
}

// GET PROPERTY MASTER DATA
function getPropertyMasterData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getPropertyData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(18 , 74),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getBranchListingDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET BRANCH LISTING DATA CALLBACK
function getBranchListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        GBL_PROPERTY_LIST = data.data;  
        propertyDataRender(); 
        addFocusId( 'clientList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    } else {
        GBL_PROPERTY_LIST = [];  
        propertyDataRender(); 
        if (flag)
            displayAPIErrorMsg( data.status , 'No Property Available' );
        else
            toastr.error(SERVERERROR);
    }
}

// RENDER PROPERTY DATA
function propertyDataRender(){

   var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Property Name</th>'+
                        '<th>CGST</th>'+
                        '<th>SGST</th>'+
                        '<th>IGST</th>'+
                        '<th>HSN Code</th>'+
                        '<th>Price</th>'+ 
                        '<th>Type</th>'+
                        // '<th>Timezone</th>'+
                        '<th>Tally Name</th>'+
                        '<th>Validity End Date</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = ''; 

    //RENDER PROPERTY LISTING
    if(GBL_PROPERTY_LIST != 'No record found'){
        GBL_PROPERTY_LIST.forEach( function( record , index ){ 
 
            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.propertyName +'</td>'+ 
                            '<td>'+ record.cgst +'%</td>'+ 
                            '<td>'+ record.sgst +'%</td>'+ 
                            '<td>'+ record.igst +'%</td>'+ 
                            '<td>'+ record.hsnCode +'</td>'+ 
                            '<td>'+ record.price +'</td>'+  
                            '<td>'+ record.typeName +'</td>'+ 
                            // '<td>'+ record.propTimezoneName +'</td>'+ 
                            '<td>'+ record.tallyName +'</td>'+ 
                            '<td>'+ mysqltoDesiredFormat( record.propertyValidityEndDate , 'dd-MM-yyyy' ) +'</td>'+ 
                            '<td>'+
                                '<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Assign Timezone" data-placement="top" class="btn-right-mrg btn btn-xs btn-amber btn-ripple" propId="'+ record.propertyId +'"  onclick="assignTimezone(this)"><i class="fa fa-exchange"></i></a>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" propId="'+ record.propertyId +'"  onclick="updateProperty(this)"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" propId="'+ record.propertyId +'" onclick="deleteProperty(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
        }); 
        
    } 

    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable({ "stateSave": true });
}

// CREATE A NEW BRANCH
function createProperty(){

    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(18,73,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){

        var propertyName = $('#propertyName').val();
        var cgst = $('#cgstTax').val();
        var sgst = $('#sgstTax').val();
        var igst = $('#igstTax').val();
        var hsnCode = $('#hsnCode').val();
        var price = $('#propertyPrice').val(); 
        var type = $('#propType').val();
        var tallyName = $('#tallyName').val();
        var propertyValidityEndDate = $('#clientList').val();
        var timezoneId = $('#timeZone').val();
        var timezoneName = $('#timeZone option:selected').text(); 
        var validityEndDate = $('#validityEndDate').val();

        if( propertyName == '' ){
            toastr.warning("Please Enter Property Name");
            addFocusId('propertyName');
            return false;

        }else if( cgst == '' || parseFloat( cgst ) > 100 ){
            toastr.warning("CGST can't be empty or more than 100 ");
            addFocusId('cgstTax');
            return false;

        }else if( sgst == '' || parseFloat( sgst ) > 100 ){
            toastr.warning("SGST can't be empty or more than 100 ");
            addFocusId('sgstTax');
            return false;

        }else if( igst == '' || parseFloat( igst ) > 100 ){
            toastr.warning("IGST can't be empty or more than 100 ");
            addFocusId('igst');
            return false;

        }else if( hsnCode == '' ){
            toastr.warning("Please Enter HSN Code");
            addFocusId('hsnCode');
            return false;

        }else if( price == '' || parseInt(price) == '0' ){
            toastr.warning("Please Enter Price");
            addFocusId('propertyPrice');
            return false;

        }else if( type == '-1' ){
            toastr.warning("Please Enter Type");
            addFocusId('propType');
            return false;

        // }else if( timezoneId == '-1' ){
        //     toastr.warning("Please Select Timezone");
        //     addFocusId('timeZone');
        //     return false;

        }else if( tallyName == '' ){
            toastr.warning("Please Enter Tally Name");
            addFocusId('tallyName');
            return false; 

        }else if( validityEndDate == '' ){
            toastr.warning("Please Select Validity End Date");
            // addFocusId('validityEndDate');
            $('#validityEndDate').datepicker('show');
            return false; 

        }else{

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "addProperty",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(18 ,73),
                propertyName : propertyName,
                cgst : cgst,
                sgst : sgst,
                igst : igst,
                price : price, 
                hsnCode : hsnCode, 
                type : type,
                tallyName : tallyName,
                propertyValidityEndDate : validityEndDate,
                // timezoneId : timezoneId,
                // timezoneName : timezoneName,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                // propertyId : branchMobile   
            };

            //UPDATE PROPERTY DATA
            if( GBL_PROP_EDIT_ID != "" ){
                postData.propertyId = GBL_PROP_EDIT_ID;
            }

            commonAjax(FOLLOWUPURL, postData, createUpdatePropertyCallback,"Please Wait... Create / Update Property");
        }
    }
}

function createUpdatePropertyCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        GBL_PROPERTY_LIST = data.data;
        GBL_PROP_EDIT_ID = '';
        
        resetData();
        propertyDataRender();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , 'Error in create / update property master' );
        else
            toastr.error(SERVERERROR);
    }
}
 
// DELETE SELECTED BRANCH
function deleteProperty(context){

    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(18 , 76 , 1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    var propertyId = "";
    if(flag){
        var deleteRow = confirm('Are You sure ? You want to delete this Record');

        if( deleteRow ){

            propertyId = $(context).attr('propid');
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
 
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "deleteProperty",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(18 , 76),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                propertyId : propertyId
            };

            commonAjax(FOLLOWUPURL, postData, deletePropertyCallback,"Please Wait... Getting Dashboard Detail");
            
            function deletePropertyCallback(flag,data){

                if (data.status == "Success" && flag) { 

                    var index = findIndexByKeyValue( GBL_PROPERTY_LIST,'propertyId',propertyId ); 
                    GBL_PROPERTY_LIST.splice(index,1);

                    toastr.success('Property Deleted Successfully');
                    propertyDataRender(); 
                    
                } else {
                    if (flag)
                        displayAPIErrorMsg( data.status , 'Error in deleting property' );
                    else
                        toastr.error(SERVERERROR);
                }
            }
        }
    }
}


// SET SELECTED BRANCH DATA TO UPDATE
function updateProperty(context){
    
    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(5,19,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){ 

        var propertyId = $(context).attr('propid'); 
        GBL_PROP_EDIT_ID = propertyId;
        var index = findIndexByKeyValue( GBL_PROPERTY_LIST,'propertyId',propertyId ); 
        if( index != "-1" ){ 
            $('#propertyName').val( GBL_PROPERTY_LIST[index].propertyName );
            $('#cgstTax').val( GBL_PROPERTY_LIST[index].cgst );
            $('#sgstTax').val( GBL_PROPERTY_LIST[index].sgst );
            $('#igstTax').val( GBL_PROPERTY_LIST[index].igst );
            $('#hsnCode').val( GBL_PROPERTY_LIST[index].hsnCode );
            $('#propertyPrice').val( GBL_PROPERTY_LIST[index].price ); 
            $('#tallyName').val( GBL_PROPERTY_LIST[index].tallyName );
            $('#validityEndDate').val( mysqltoDesiredFormat( GBL_PROPERTY_LIST[index].propertyValidityEndDate , 'dd-MM-yyyy' ) ); 
            $('#propType').val( GBL_PROPERTY_LIST[index].type ).selectpicker('refresh'); 
            $('#timeZone').val( GBL_PROPERTY_LIST[index].propTimezoneId ).selectpicker('refresh');  
            $('#createPropertyBtn').html( '<i class="fa fa-floppy-o"></i> Update' );
        }

    } 
} 

// RESET FIELDS
function resetData(){

    $('#propertyName').val('');
    $('#cgstTax').val('');
    $('#sgstTax').val('');
    $('#igstTax').val('');
    $('#hsnCode').val('');
    $('#propertyPrice').val('');
    $('#tallyName').val('');
    $('#validityEndDate').val(''); 
    $('#timeZone').val('').selectpicker('refresh');
    $('#createPropertyBtn').html( '<i class="fa fa-floppy-o"></i> Save' );
    GBL_PROP_EDIT_ID = "";
} 


function checkUnlimitedValidity(context){

    var checked = $(context).prop('checked');
    if( checked == true ){
        $('#validityEndDate').datepicker( 'update' , '31-12-9999' );
    }else{
        $('#validityEndDate').datepicker( 'update' , mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
        $('#validityEndDate').val('');
    }

}

function assignTimezone(context){

    var propertyId = $(context).attr('propid'); 
    GBL_PROP_EDIT_ID = propertyId;
    var index = findIndexByKeyValue( GBL_PROPERTY_LIST,'propertyId',propertyId ); 
    if( index != -1 ){ 
        var propertyName = GBL_PROPERTY_LIST[index].propertyName;
        GBL_PROP_NAME_EDIT = propertyName;
        GBL_TIMEZONE_DATA = [];
        $('#timeZone').val('-1').selectpicker('refresh');
        $('#assignTimezoneName').val('');
        getPropertyTimezone();
    }

}

function getPropertyTimezone(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getPropertyTimeZone",
        userId: tmp_json.data[0].PK_USER_ID,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(18, 74),
        itemId: GBL_PROP_EDIT_ID,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    commonAjax(FOLLOWUPURL, postData, getPropertyDataCallback, "Please wait...Adding your item.");

    function getPropertyDataCallback(flag, data) {
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_TIMEZONE_DATA = data.data;
            renderTimezoneList();

        } else {

            GBL_TIMEZONE_DATA = [];
            renderTimezoneList();

            if (flag)
                displayAPIErrorMsg(data.status, GBL_ERR_NO_PROP);
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER TIMEZONE IN DATATABLE
function renderTimezoneList(){

    var thead = '<table class="display datatables-alphabet-sorting" id="propTable">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>TimeZone Name</th>'+
                        // '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = ''; 

    //RENDER PROPERTY LISTING
    if(GBL_TIMEZONE_DATA != 'No record found'){
        GBL_TIMEZONE_DATA.forEach( function( record , index ){ 
 
            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.propTimezoneName +'</td>'+
                            // '<td>'+
                                // '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" propId="'+ record.propertyId +'"  onclick="updatePropertyTimezone(this)"><i class="fa fa-pencil"></i></a>'+
                                // '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" propId="'+ record.propertyId +'" onclick="deleteProperty(this);"><i class="fa fa-trash-o"></i></a>'+
                            // '</td>'+
                        '</tr>';
        }); 
        
    } 

    $('#timezoneListing').html(thead + newRow + tfoot);
    $("#propTable").DataTable();
    modalShow('assignTimezoneModal');

}

function saveTimeZone(){

    var timeZone = $('#timeZone').val();
    var assignTimezoneName = $('#assignTimezoneName').val();

    if( timeZone == "" || timeZone == "-1" ){
        toastr.warning("Please Select Timezone");
        addFocusId('timeZone');
        return false;

    }else if( assignTimezoneName == "" ){
        toastr.warning("Please Enter Timezone Name");
        addFocusId('assignTimezoneName');
        return false;

    }else{


        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "addTimeZoneToProperty",
            userId: tmp_json.data[0].PK_USER_ID,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            orgId: checkAuth(31, 116),
            itemId: GBL_PROP_EDIT_ID,
            timezoneId: timeZone,
            timzoneName: assignTimezoneName,
            propertyName: GBL_PROP_NAME_EDIT,
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }

        commonAjax(FOLLOWUPURL, postData, saveTimeZoneDataCallback, "Please wait...Adding your item.");

        function saveTimeZoneDataCallback(flag, data) {
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_TIMEZONE_DATA = data.data;
                renderTimezoneList();
                $('#timeZone').val('-1').selectpicker('refresh');
                $('#assignTimezoneName').val('');

            } else {

                renderTimezoneList();

                if (flag)
                    displayAPIErrorMsg(data.status, "Unable to assign timezone to this property");
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

}

function setTimezoneName(context){

    var timezoneName = $(context).val();
    if( timezoneName != "-1" ){
        $('#assignTimezoneName').val( $(context).find('option:selected').text() );
    }else{
        $('#assignTimezoneName').val( '' );
    }

}

function updatePropertyTimezone(context){

}