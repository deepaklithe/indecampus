/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 25 January 2018.
 * File : Schedule Activity.
 * File Type : activityAppSmartScheduleController.js.
 * Project : activityMgmt
 *
 * */

var GBL_SCHEDULER_ACT = [];

function chkValidPageLoad() { 
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}


function activitySchedulePage() {

    /* init globals constants from gbl_ActivityAppGlobals  START */
    //Static Globals
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;
    var ACT_MODE_PHONE = gbl_ActivityAppGlobals.ACT_MODE_PHONE;
    var ACT_MODE_EMAIL = gbl_ActivityAppGlobals.ACT_MODE_EMAIL;
    var ACT_MODE_SMS = gbl_ActivityAppGlobals.ACT_MODE_SMS;
    var ACT_MODE_PERSONALMEET = gbl_ActivityAppGlobals.ACT_MODE_PERSONALMEET;
    var ACT_STATUS_PENDING = gbl_ActivityAppGlobals.ACT_STATUS_PENDING;
    var ACT_STATUS_INPROCESS = gbl_ActivityAppGlobals.ACT_STATUS_INPROCESS;
    var ACT_STATUS_CLOSED = gbl_ActivityAppGlobals.ACT_STATUS_CLOSED;


    /* init globals constants from gbl_ActivityAppGlobals  END */

    /* this module related globals START */
    var mGbl_customerGrpList = [];
    var mGbl_userGrpList = [];
    var mGbl_customerList = [];
    var mGbl_userList = [];
    var thisModule = this;
    var mdlCommonModule = new commonModuleForActivityApp(); //create an instance of commonModuleForActivityApp .
    /* this module related globals END */
    
        /* element selector's defination START */
    var $txtActTitle = $('#txtActTitle');
    var $txtActDesc = $('#txtActDesc');
    var $txtActStartDate = $('#txtActStartDate');
    var $txtActEndDate = $('#txtActEndDate');
    var $rdoActMode = $('input[type=radio][data-name="rdoActMode"]');

    var $txtCustGrp = $('#txtCustGrpName');
    var $txtCustGrpLabel = $('#cg-label');
    //var $rdoCustType = $('input[type=radio][name="rdoIsCustomerGroup"]');
    var $rdoCustType = $('#customerType');
    var $rdoAssignType = $('input[type=radio][data-name="rdoIsUserGroup"]');
    var $ddActAssignedTo = $('#ddActAssignedTo');
    var $ddActFrequency = $('#ddActFrequency');
    var $formCreateUpdateAct = $("#formCreateCustomAct");


    var $postData = $("#postData"); // to attach json in form

    var $modalCreatactivity = $("#creatactivity"); // create activity modal
    var $btnCreateCustomActivity = $("#btnCreateCustomActivity");
    //$($modalCreatactivity).modal('hide');


    /* element selector's defination END */

    /* html variables for user/grp dropdown START  */
    var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
    var assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';
    /* html variables for user/grp dropdown END */
    
    
    /* public functions START */
    this.initSchPage = function()
    {
        initCreateCustomActModule();
        
    }
    
    /* public functions END */

    var initCreateCustomActModule = function ()
    {
        /* render ACT_FREQUENCY dropdown START */
        var actFreqOptionHtml = mdlCommonModule.getActFreqDropDown();
        $($ddActFrequency).html(actFreqOptionHtml);
        $($ddActFrequency).selectpicker();
        /* render ACT_FREQUENCY dropdown END */
 

        var callBackGetCustGrpDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {
                mGbl_customerGrpList = dataJson.custGrpArr;
                mGbl_userGrpList = dataJson.userGrpArr;
                assignTypeGroupOptionHtml = dataJson.assignTypeGroupOptionHtml;


                mdlCommonModule.getUserCustomerSingleData(callBackGetCustSingleDataForThisModule);
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }

        }

        var callBackGetCustSingleDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {
                mGbl_customerList = dataJson.customerListArr;
                mGbl_userList = dataJson.userListArr;
                assignTypeUserOptionHtml = dataJson.assignTypeUserOptionHtml;

                getSchActListin();
                registerUserGrpEvents(); // place in the last callBack after getting user/cust group and single data.
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }

        }

        mdlCommonModule.getUserCustomerGroupData(callBackGetCustGrpDataForThisModule);

    }

    /* function to make ajax call and get activity data START */
    var getSchActListin = function ()
    {
        var postData = {
            requestCase: "ScheduleSmartActivityListing",
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,6),
        };

        function callBackGetSchActListin(flag, results) {
            if (results.status == SCS) {
                //log(msg);
                //localStorage.POTGtempActData = JSON.stringify(results);
                var dataArr = results.data;
                GBL_SCHEDULER_ACT = dataArr;
                console.log(GBL_SCHEDULER_ACT);
                // params : array
                renderSchedulerActivityData(dataArr);

            } else {
                var tmpDataArr = {
                    data:[{}]
                };
                renderSchedulerActivityData(tmpDataArr);

                //displayAttendenceListin(results);
                log("====== FAIL ======");
                if (flag) {
                    //alert(results.status);
                    displayAPIErrorMsg( results.status , GBL_ERR_NO_SCHEDULE_ACT_LISTING );
                } else {
                    log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTLIST;
        commonAjax(actUrl, postData, callBackGetSchActListin, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackGetSchActListin, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to make ajax call and get activity data END */
    
    /* function to render sch act data START */
    function renderSchedulerActivityData(dataArr)
    {
        var dataArrLen = dataArr.length;

        var innerHtml = '';
        var tableStartHtml = '<table id="actSchListinDataTable" class="display datatables-alphabet-sorting" >';
        var tableEndHtml = '</table>';
        var threadHtml = '<thead>' +
                            '<th>No.</th>' +
                            '<th>Type</th>' +
                            '<th>Title</th>' +
                            '<th>Description</th>' +
                            '<th>Mode</th>' +
                            '<th>Date</th>' +
                            '<th>Last Activity</th>' +
                            '<th>Duration</th>' +
                            '<th>User Name</th>' +
                            '<th>Action</th>' +
                        '</thead>';
        var tbodyStart = '<tbody id="tbodySchActListin">';
        var tbodyEnd = '</tbody>';

        if (dataArrLen <= 0)
        {
            innerHtml = ' '; //<li class="nodata">No Data Found ..!</li> 
        }
        var count = ''
        for (var i = 0; i < dataArrLen; i++)
        {
            /* data init START */
                var activityType = "Smart Scheduler";
                var activityTitle = dataArr[i].actTiele;
                var activityMode = dataArr[i].actMode;
                var activityModeName = '';
                var activityDesc = dataArr[i].actDesc;
                var activityDate = dataArr[i].date;
                var lastActDate = dataArr[i].lastActDate;
                var activityFreqValue = dataArr[i].frequencyType;
                var activityUserName = dataArr[i].userName;
            /* data init END */
            
           
            /* activity mode icon init START */
            
            if (activityMode == ACT_MODE_PHONE)
            {
                activityModeName = 'Phone';
            }
            else if (activityMode == ACT_MODE_EMAIL)
            {
                activityModeName = 'E-Mail';
            }
            else if (activityMode == ACT_MODE_SMS)
            {
                activityModeName = 'SMS';
            }
            else if (activityMode == ACT_MODE_PERSONALMEET)
            {
                activityModeName = 'Personal Meeting';
            }
            /* activity mode icon init END */
            count = i + 1;

            innerHtml += '<tr class="gradeX">' +
                            '<td>' + count + '</td>' +
                            '<td>' + activityType + '</td>' +
                            '<td>' + activityTitle + '</td>' +
                            '<td>' + activityDesc + '</td>' +
                            '<td>' + activityModeName + '</td>' +
                            '<td>' + ( activityDate != '0000-00-00' ? mysqltoDesiredFormat( activityDate , 'dd-MM-yyyy' ) : '00-00-0000' ) + '</td>' +
                            '<td>' + ( lastActDate != '0000-00-00' ?  mysqltoDesiredFormat( lastActDate , 'dd-MM-yyyy' ) : '00-00-0000' ) + '</td>' +
                            '<td>' + activityFreqValue + '</td>' +
                            '<td>' + activityUserName + '</td>' +
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary" ind="'+ i +'" onclick="editScheduler(this);"><i class="fa fa-pencil"></i></a>&nbsp;'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-ripple" ind="'+ i +'" smartActId="'+ dataArr[i].smartId +'" onclick="deleteScheduler(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                          '</tr>'
        }

        var finalTableHtml = tableStartHtml + threadHtml + tbodyStart + innerHtml + tbodyEnd + tableEndHtml;

        $('#divSchedulerActListing').html('');
        $('#divSchedulerActListing').html(finalTableHtml);
        //registerAssignToPopUpEvent(); // uncomment this line to activate "Assign schedule activity".
        // $('#actSchListinDataTable').dataTable();
        generateDatatable("actSchListinDataTable");

    }
    /* function to render sch act data END */


    // create activity / close and create activity.
    var submitCreateCustomActivity = function (refId)
    {
        if (!refId)
        {
            refId = "";
        }

        var actTitle = $($txtActTitle).val();
        var actDesc = $($txtActDesc).val();
        var actMode = $rdoActMode.filter(":checked").val();
        var actStartDate = $($txtActStartDate).val();
        var actEndDate = $($txtActEndDate).val();
        var isUserGroup = $rdoAssignType.filter(":checked").val();
        //var isCustomerGroup = $rdoCustType.filter(":checked").val();
        var isCustomerGroup = $rdoCustType.val();
        var assigneTo = $($ddActAssignedTo).val();
        var actFrequency = $($ddActFrequency).val();
        var custName = $($txtCustGrp).val();
        var customerId = $($txtCustGrp).attr('data-id');
        var customerType = $('#customerType').val();
        var inqIdAct = $('#inqListAct option:selected').val();
        var custIdAct = $('#inqListAct option:selected').attr('custId');
        
        if (!actTitle)
        {
            toastr.warning("Please Enter the Activity Title");
            $($txtActTitle).focus();
            return false;
        }
        else if (!actDesc)
        {
            toastr.warning("Please Enter the Activity Description");
            $($txtActDesc).focus();
            return false;

        }
        else if (!actMode)
        {
            toastr.warning("Please Select the Activity Mode");
            $($rdoActMode).focus();
            return false;

        }
        else if (!actStartDate)
        {
            toastr.warning("Please Select the Activity StartDate");
            $($txtActStartDate).focus();
            return false;

        }
        else if (!actEndDate)
        {
            toastr.warning("Please Select the Activity EndDate");
            $($txtActEndDate).focus();
            return false;

        }
        else if(+dateActEndDate < +dateActStartDate) //dateActEndDate dateActStartDate
        {
            toastr.warning("End Date cannot be less than the StartDate");
            $('#txtActEndDate').datepicker('show');
            return false;
            
        }
        else if (!isUserGroup)
        {
            toastr.warning("Please select the Activity Assign Type");
            return false;

        }
        // else if (!isCustomerGroup)
        // {
        //     alert("Please select the activity customer type");
        //     $($rdoCustType).focus();
        //     return false;

        // }
        else if (!assigneTo || assigneTo == "-1")
        {
            if (gbl_ActivityAppRoleAuth.AUTH_ASSIGN)
            {
                toastr.warning("Please Assign the Activity");
                $($ddActAssignedTo).focus();
                return false;
            }

        }
        // else if (!custName || !customerId)
        // {
        //     alert("Please select the customer/group name for customer type");
        //     $($txtCustGrp).focus();
        //     return false;

        // }

        actStartDate = ddmmyyToMysql(actStartDate,'yyyy-MM-dd');
        actEndDate = ddmmyyToMysql(actEndDate,'yyyy-MM-dd');

        var dateActStartDate = new Date(actStartDate);
        var dateActEndDate = new Date(actEndDate);

        console.log("Create activity validation passed");

        /* generate postData to submit activity START */
        var requestCase = "createCustomActivity";
        if (refId)
        {
            //requestCase = "closeAndCreateNewActivity";
        }

        if( custName == "" || customerId == undefined ){
            customerType = ""; 
            customerId = ""; 
            isCustomerGroup = "";
        }else{
            if( customerType == 'group' ){
                isCustomerGroup = 1;
            }else{
                isCustomerGroup = 0;
            }
        }

        var postData = {
            requestCase: requestCase,
            orgId: checkAuth(2,5),
            actTitle: actTitle,
            actDesc: actDesc,
            actMode: actMode,
            actStartDate: actStartDate,
            actEndDate: actEndDate,
            actFrequency: actFrequency,
            isUserGroup: isUserGroup,
            isCustomerGroup: isCustomerGroup,
            assigneTo: assigneTo,
            customerId: customerId,
            refId: refId,
            actId: refId,
            clientId: localStorage.indeCampusActivityAppClientId,
            userId: localStorage.indeCampusActivityAppUserId,
            actType : customerType,
            inqId : inqIdAct,
            custId : custIdAct
        };
        /* generate postData to submit activity END */
        console.log(postData);
        $($postData).val(JSON.stringify(postData));

        $($formCreateUpdateAct).submit();
        //createActCallBackOnFormSubmit();

    }

    /* public functions of this module END */

    var registerUserGrpEvents = function ()
    { // init customer search here and render customer assign dd here. 


        $($btnCreateCustomActivity).off().on('click', (function (e) {

            submitCreateCustomActivity();

        }));


        /* for customer type START */
        // by default single radio would be selected in customer type search
        // so we are assigning the source array of single radio's customer search. : "mGbl_customerList"
        $($txtCustGrp).autocomplete({
            source: function (request, response)
            {
                var term = request.term;
                //var custData = custTermSearch(term);
                custTermSearch(term,response);
                //response(custData);
                

            },
            minChars : 2,
            focus: function (event, ui) {
                //event.preventDefault();
                // $(this).val(ui.item.label);

            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(this).attr("data-id", ui.item.value);

            }, change: function (event, ui) {
                if (ui.item == null) {
                    this.value = '';
                    $(this).val('');
                    $(this).attr("data-id", "");
                    toastr.warning('Please select a value from the list'); // or a custom message
                }
            }
        }).autocomplete("widget").addClass("custMenuList");



        // $($rdoCustType).off().on('change', (function (e) {
        //     if (this.value == '0') {
        //         //user //single
        //         $($txtCustGrpLabel).html('Customer Name');
        //         $($txtCustGrp).val('');
        //         $($txtCustGrp).attr('placeholder', 'Enter Customer Name');
        //         $($txtCustGrp).autocomplete("option", {
        //             source: function (request, response)
        //             {
        //                 var term = request.term;
        //                 //var custData = custTermSearch(term);
        //                 custTermSearch(term,response);
        //                 //response(custData);
                        

        //             },
        //             minChars : 2,
        //         });

        //     } else if (this.value == '1') {
        //         //group
        //         $($txtCustGrpLabel).html('Group Name');
        //         $($txtCustGrp).val('');
        //         $($txtCustGrp).attr('placeholder', 'Enter Group Name');
        //         $($txtCustGrp).autocomplete("option", {
        //             source: mGbl_customerGrpList
        //         });
        //     }

        // }));

        $('#customerType').on('change', (function (e) {
            if (this.value == 'single') {
                $($txtCustGrpLabel).html('Customer Name');
                $($txtCustGrp).val('');
                $($txtCustGrp).attr('placeholder', 'Enter Customer Name');
                $($txtCustGrp).autocomplete("option", {
                    source: function (request, response)
                    {
                        var term = request.term;
                        //var custData = custTermSearch(term);
                        custTermSearch(term,response);
                        //response(custData);
                        

                    },
                    minChars : 2,
                });

            } else if (this.value == 'group') {
                //group
                $($txtCustGrpLabel).html('Group Name');
                $($txtCustGrp).val('');
                $($txtCustGrp).attr('placeholder', 'Enter Group Name');
                $($txtCustGrp).autocomplete("option", {
                    source: mGbl_customerGrpList
                });
            }
        }));

        /* for customer type END */


        $($ddActAssignedTo).html(assignTypeUserOptionHtml); // default selection of user/single 's html
        // $($ddActAssignedTo).selectpicker(); // default selection of user/single 's html
        $("#customerType").selectpicker(); // default selection of user/single 's html


        $($rdoAssignType).off().on('change', (function (e) {
            if (this.value == '0') {
                //user //single
                $($ddActAssignedTo).html(assignTypeUserOptionHtml);

            } else if (this.value == '1') {
                //group
                $($ddActAssignedTo).html(assignTypeGroupOptionHtml);
            }

        }));

        /* for assign type START */


        /* register create/update activity form submit event START */
        $($formCreateUpdateAct).off().on('submit', (function (e) {
            e.preventDefault();
            $.ajax({
                url: actUrl, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);
                    $("#ajaxloader").removeClass("hideajaxLoader");

                },
                success: function (data)// A function to be called if request succeeds
                {


                    data = JSON.parse(data);
                    console.log(data);
                    if (data.status == SCS) {
                        //hide the modal here.

                        var msg = 'Your Activity is been Created.';

                        $($modalCreatactivity).modal('hide');
                        if( data.data != "" ){
                            
                            msg = 'Your Activity is been Created , but attached '+data.data;
                        }

                        toastr.success( msg );
                        
                        getSchActListin();
                        //createActCallBackOnFormSubmit();

                    } else {
                        toastr.warning(data.status);

                    }
                    $("#ajaxloader").addClass("hideajaxLoader");
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    $("#ajaxloader").addClass("hideajaxLoader");
                }
            });
        }));
        /* register create/update activity form submit event END */

    }
    /* funtion to bind 'Assign to' popup event START */
    function registerAssignToPopUpEvent()
    {
        //localStorage.POTGactivityAppUserListJson
        //localStorage.POTGactivityAppUserGrpJson
        
        //var userList = JSON.parse(localStorage.POTGactivityAppUserListJson);
        //var userGrpList = JSON.parse(localStorage.POTGactivityAppUserGrpJson);
        
        var source = [];
        $('.assignmentTd').editable({
            type: 'select',
            pk: 1,
            value: '1',
            placement: 'bottom',
            source: function () {
               var isUserGroup = $(this).attr('data-isUserGroup'); 
                console.log(isUserGroup);
                
                if (isUserGroup == '0') {
                    //user //single
                    return mGbl_userList;
                }
                else if(isUserGroup == '1')
                { // group
                    return mGbl_userGrpList;
                }
                else
                {
                    console.error("isUserGroup property value not found");
                    return source;
                }
                
                
            },
            title: 'Assign To',
            validate: function (value) {
                //var dataValue = $.trim(value);
                if (!value || value == "-1") {
                    return 'This field is required';
                }
                var activityid = $(this).attr('data-activityid');
                console.log("assigneTo : " + value);
                console.log("activityid :" + activityid);
                assignActivity(activityid, value);
                //updateCommentInAct(remarkIndexId, remark);

            }
        });

    }

    /* funtion to bind 'Assign to' popup event END */


    var assignActivity = function (activityid, assigneTo)
    {
        console.log("Assign activity change event fired");
        //assignActCallBackOnChange

        if (!activityid)
        {
            console.error(" activityid - Not found");
            toastr.error("Required Activity data not found to Assign this Activity");
            return false;
        }

        var postData = {
            requestCase: "assignActivity",
            actId: activityid,
            assigneTo: assigneTo,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,107),
        };


        function callBackAssgnAct(flag, results) {
            if (results.status == SCS) {

                getSchActListin();

            } else {

                log("====== FAIL ======");
                if (flag) {
                    // toastr.error(results.status);
                    displayAPIErrorMsg( results.status , GBL_ERR_ASSIGN_ACT_FAIL );
                } else {
                    log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTDATA;
        commonAjax(actUrl, postData, callBackAssgnAct, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackAssgnAct, actAjaxMsg);
        /* ajax call end  */


    }

}

var actSchPage;
function initSchActPage()
{

    actSchPage = new activitySchedulePage();
    actSchPage.initSchPage();
    
}


function openModalActivity(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqListingByUser',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        custId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, inquiryDataCallBack,INQUIRYDETAILGET);

    function inquiryDataCallBack(flag,data) {

        if(data.status == "Success" && flag){

            var inqLogOptions = '<option value=""> --Please Select Inquiry -- </option>';
            data.data.forEach( function( record , index ){ 
                inqLogOptions += '<option custId="'+ record.custId +'" value="'+ record.inqId +'">'+ record.inqId +'</option>';
            });

            $('#inqListAct').html( inqLogOptions );
            $('#inqListAct').selectpicker();

        }else{
            if (flag)
                displayAPIErrorMsg( results.status , GBL_ERR_NO_INQ );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    $('#creatactivity').modal('show');
}

function pageInitialEvents(){

    $('#radioBtn a').on('click', function () {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });
}

function createActivity() {

    $('#txtActTitle').val('');
    $('#txtActDesc').val('');
    $('#txtActStartDate').val('');
    $('#txtActEndDate').val('');
    $('#ddActAssignedTo').val('-1');
    $('#txtCustGrpName').val('');
    $(".default-date-picker").datepicker('remove');
    $('#creatactivity').modal('show');
}

function editScheduler( context ) {

    var indexPos = $(context).attr('ind');
    if( GBL_SCHEDULER_ACT != "No record found" && GBL_SCHEDULER_ACT != "" && GBL_SCHEDULER_ACT != undefined){
        localStorage.potgEditSchedulerActData = JSON.stringify(GBL_SCHEDULER_ACT[indexPos]);
        navigateToSmartActNewTab();
    }else{
        toastr.error("Data not found");
    }
}

function deleteScheduler( context ) {
        
    var indexPos = $(context).attr('ind');
    var smartId = $(context).attr('smartActId');
    var schedulerActTitle = GBL_SCHEDULER_ACT[indexPos].actTiele;
    var schedulerActDesc = GBL_SCHEDULER_ACT[indexPos].actDesc;
    var schedulerActMode = GBL_SCHEDULER_ACT[indexPos].actMode;
    var schedulerTypeList = GBL_SCHEDULER_ACT[indexPos].actType;
    var schedulerActFreq = GBL_SCHEDULER_ACT[indexPos].frequencyType;


    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'deleteCreateScheduleSmartActivity',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2,8),
        schedulerActTitle: schedulerActTitle,
        schedulerActDesc: schedulerActDesc,
        schedulerActMode: schedulerActMode,
        schedulerTypeList: schedulerTypeList,
        schedulerActFreq: schedulerActFreq,
        smartId : smartId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    // console.log(postData); return false;
    commonAjax(ACTURL, postData, deleteSchedulerCallBack,INQUIRYDETAILGET);

    function deleteSchedulerCallBack(flag,data) {

        if(data.status == "Success" && flag){
        console.log(data);
        // GBL_SCHEDULER_ACT.splice(indexPos,1);
        actSchPage = new activitySchedulePage();
        actSchPage.initSchPage();

        }else{
            if (flag)
                displayAPIErrorMsg( results.status , GBL_ERR_SCHEDULE_DELETE_ACT_FAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
}