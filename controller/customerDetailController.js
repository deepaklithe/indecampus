/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var custGroupFlag = '';

var customerData = [];
var custInfoData = [];
var currCustId = '';

var contactpersonData = [];

var multiPhoneData = [];
var multiEmailData = [];
var multiAddressData = [];
var multiGroupData = [];

var GBL_CUSTOMER_TOTAL_COUNT = 0;
var GBL_CUSTOMER_FETCH_FROM = 0;

var custCnt = 12;
var prvCnt = 0;
var gblCustId = '';
var gblCustName = '';
var GBL_USERLISTARR = [];

var GBL_Add = '';
var GBL_CURR_STAT_ID = "";
var GBL_CANCEL_INQ_STATID = "";
var GBL_NOTE_LIST = [];

var renderCustData = [];

var addCnt = 1;

function getCustInfoData(){

    var custId = localStorage.POTGCustDetailCustId;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getCustomerDetailByCustId',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,10),
        custId: custId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    commonAjax(FOLLOWUPURL, postData, getCustInfoDataCallback,"Please Wait... Getting Dashboard Detail");

    function getCustInfoDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);

            custInfoData = data.data;
            GBL_NOTE_LIST = (custInfoData[0].noteData != "No record found")  ? custInfoData[0].noteData : [];
            contactpersonData = custInfoData[0].CustomerInfo.multiContactData;
            renderCustInfoData();
            getLastLevelUserListing();
            renderNoteData();
        } else {
            if (flag){

                toastr.error(data.status);
                window.close();
            }
            else{
                toastr.error(SERVERERROR);
                window.close();
            }
        }
    }
}

function renderCustInfoData(){

    var custId = localStorage.POTGCustDetailCustId;

    gerCustGroup();
    custDetails();
    productDetails();
    ledgerDetails();
    paymentDetails();
    invoiceDetails(); 
    //agingDetails();  
    inqDetail();
    inventoryDetails();
}

function gerCustGroup(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userCustomerGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,66),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        searchType : "CUSTOMER"
    }

    commonAjax(FOLLOWUPURL, postData, gerCustGroupCallback,"Please Wait... Getting Dashboard Detail");
}


function gerCustGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        
        var newRow = '';
        for( var i=0; i<data.data.length; i++ ){

            var name = data.data[i].groupName;
            var groupId = data.data[i].groupId;
            
            newRow += '<option value="'+ groupId +'|--|'+ name +'"> '+ name +' </option>';
        }

        $('#custEditGroup').html(newRow);
        $('#custEditGroup').selectpicker();

        //getLastLevelUserListing();

        custGroupFlag = '1';

    } else {
        if (flag) {
            //toastr.error(data.status);
        } else {
            // toastr.error(SERVERERROR);
        }
    }
}


function custDetails(){

    var newRow = '';
    $('#custName').html( custInfoData[0].CustomerInfo.Name );
    
    var grpList = "";
    if( custInfoData[0].CustomerInfo.groupData != "No record found" ){
        for( var i=0; i<custInfoData[0].CustomerInfo.groupData.length; i++ ){
            grpList += '<span class="badge bg-amber" style="font-size:18px;">'+ custInfoData[0].CustomerInfo.groupData[i].groupName +'</span>';
        }
    }
    $('#custGroup').html( grpList );

    $('#turnOver').html( (custInfoData[0].CustomerInfo.TurnOver) == "" ? 0 : custInfoData[0].CustomerInfo.TurnOver );
    $('#outStand').html( (custInfoData[0].CustomerInfo.OutStanding) == "" ? 0 : custInfoData[0].CustomerInfo.OutStanding );

    var contPerHtml = "";
    if( custInfoData[0].CustomerInfo.multiContactData != "No record found" ){
        for( var j=0; j<custInfoData[0].CustomerInfo.multiContactData.length; j++ ){

            var contDob = "";
            if( custInfoData[0].CustomerInfo.multiContactData[j].contactDob != "" ){
                contDob = '<br><span style="font-size:14px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="DOB"><i class="fa fa-gift"></i>&nbsp; '+ custInfoData[0].CustomerInfo.multiContactData[j].contactDob +'</span>';
            }
            
            var contDoa = "";
            if( custInfoData[0].CustomerInfo.multiContactData[j].contactDoa != "" ){
                contDoa = '<br><span style="font-size:14px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="DOA"><i class="fa fa-gift"></i>&nbsp; '+ custInfoData[0].CustomerInfo.multiContactData[j].contactDoa +'</span>';
            }

            contPerHtml += '<div class="col-md-12 col-sm-6">'+
                                '<div class="panel custdetail">'+
                                    '<div class="panel-heading bg-primary">'+
                                        '<div class="panel-title">'+
                                            '<h4><a class="color-assign" style="font-size:14px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Contact Person"> <i class="fa fa-user"></i>&nbsp;'+ custInfoData[0].CustomerInfo.multiContactData[j].fname +' '+ custInfoData[0].CustomerInfo.multiContactData[j].lname +'</a></h4>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="panel-body">'+
                                        '<div class="col-md-12 col-sm-6">'+
                                            '<span style="font-size:14px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Designation"><i class="fa fa-briefcase"></i>&nbsp; '+ custInfoData[0].CustomerInfo.multiContactData[j].contactDesig +'</span>'+
                                            '<br>'+
                                            '<span style="font-size:14px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Number"><i class="fa fa-phone"></i>&nbsp; '+ custInfoData[0].CustomerInfo.multiContactData[j].contactNumber +'</span>'+
                                            '<br>'+
                                            '<span style="font-size:14px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Email"><i class="fa fa-envelope"></i>&nbsp; '+ custInfoData[0].CustomerInfo.multiContactData[j].contactEmail +'</span>'+
                                            contDob +
                                            contDoa +
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
        }
    }
    $('#contPerDiv').html( contPerHtml );

    currCustId = localStorage.POTGCustDetailCustId;

    var custData = custInfoData[0].CustomerInfo;
    var contactData = custInfoData[0].CustomerInfo.multiPhoneData;
    var emailData = custInfoData[0].CustomerInfo.multiEmailData;
    var addressData = custInfoData[0].CustomerInfo.multiAddressData;
    var groupData = custInfoData[0].CustomerInfo.groupData;

    multiGroupData = [];

    if( custData != "No record found" ){

        var landlineIndex = findIndexByKeyValue( custInfoData[0].CustomerInfo.multiPhoneData,'defaultFlag','1' )
        var mailIndex = findIndexByKeyValue( custInfoData[0].CustomerInfo.multiEmailData,'defaultFlag','1' )
        var addressIndex = findIndexByKeyValue( custInfoData[0].CustomerInfo.multiAddressData,'defaultFlag','1' )

        var defLandline = "";
        if( landlineIndex != -1 ){
            defLandline = custInfoData[0].CustomerInfo.multiPhoneData[landlineIndex].phone;
        }

        var defEmail = "";
        if( mailIndex != -1 ){
            defEmail = custInfoData[0].CustomerInfo.multiEmailData[mailIndex].emailAddress;
        }

        var defAddress = "";
        var defCA = "";
        if( addressIndex != -1 ){
            defAddress = custInfoData[0].CustomerInfo.multiAddressData[addressIndex].address;
            defCA = custInfoData[0].CustomerInfo.multiAddressData[addressIndex].catchArea;
        }

        $('#custName').html(custData.Name +' <a href="javascript:void(0)" style="font-size:20px;" onclick="groupEdit()"><i class="fa fa-pencil"></i></a>');
        // $('#custLandline').html(defLandline);
        $('#custMobile').html(defLandline);
        $('#custEmail').html(defEmail);
        $('#custAddress').html(defAddress);
        $('#catchAr').html(defCA);
    
        // var DOB = '';
        // var DOA = '';
        // if( custData.DOB != "1970-01-01" && custData.DOB != "" && custData.DOB != "0000-00-00" ){
        //     DOB = mysqltoDesiredFormat(custData.DOB,'dd-MM-yyyy');
        // }else{
        //     DOB = '';
        // }
        // if( custData.DOA != "1970-01-01" && custData.DOA != "" && custData.DOA != "0000-00-00" ){
        //     DOA = mysqltoDesiredFormat(custData.DOA,'dd-MM-yyyy');
        // }else{
        //     DOA = '';
        // }
        
        $('#custType').html(custData.CustomerTypeName);
        $('#custDesignation').html(custData.Designation);
        $('#custPan').html(custData.PanNo);
        $('#custGstTinNo').html(custData.gstTinNo);//ADD BY DARSHAN PATEL ON 23-3-2018
        $('#custEthinicity').html(custData.Ethinicity);
        // $('#custOccupation').html(custData.OccupationName);
        $('#indTypeTxt').html(custData.indTypeTxt);
        $('#rateTypeTxt').html(custData.rateTypeText);
        // $('#custDOB').html(DOB);
        // $('#custDOA').html(DOA);
        $('#custWebAdd').html('<a href="http://'+ custData.custWebsite +'">'+ custData.custWebsite +'</a>');
        $('#custCity').html(custData.cityName);
        $('#custState').html(custData.stateName);
        $('#custCountry').html(custData.countryName);
		$('#custtype').html(custData.custType);
		$('#inqCount').html(custData.totalInqCount);
		$('#orderCount').html(custData.totalOrderCount);
        if ( contactData != 'No record found' ){
            multiPhoneData = contactData;
        }

        if ( emailData != 'No record found' ){
            multiEmailData = emailData;
        }
        
        if ( addressData != 'No record found' ){
            multiAddressData = addressData;
        }

        if ( groupData != 'No record found' ){
            multiGroupData = groupData;
        }

        var contactPerson = "";
        if( custInfoData[0].CustomerInfo.Members != "No record found"){
            
            for ( var j=0; j<custInfoData[0].CustomerInfo.Members.length; j++ ){

                var userName = custInfoData[0].CustomerInfo.Members[j].userName;
                if(custInfoData[0].CustomerInfo.Members.length == (j+1)){
                    contactPerson += '<span><b><i class="fa fa-handshake-o"></i></b> '+ userName +'</span><br>';
                }else{
                    contactPerson += '<span><b><i class="fa fa-child"></i></b> '+ userName +'</span><br>';
                }
                
            }
            $("#customerUserHerierchy").html(contactPerson);
        }
        // custom customFieldsArr

        var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
        if(localJson.data.customFieldDetail != "No record found"){
            var divStruct = '<div class="col-md-4 no-pad-lr" style="font-size:14px;">';
            for(var j=0;j<localJson.data.customFieldDetail.length;j++){
                if(localJson.data.customFieldDetail[j]['tabType'] == "CUSTOMER"){
                    // console.log(localJson.data.customFieldDetail[j]);
                    divStruct += '<div>';
                    if(localJson.data.customFieldDetail[j].fieldType == "text"){
                        divStruct += '<div class="col-md-12">';
                        divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span >'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
                        divStruct += '<div class="col-md-12">';
                        divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
                        divStruct += '<div class="col-md-12">';
                        divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
                        divStruct += '<div class="col-md-12">';
                        divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
                        divStruct += '<div class="col-md-12">';
                        divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
                        divStruct += '<div class="col-md-12">';
                         divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
                        divStruct += '<div class="col-md-12">';
                        divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
                        divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
                    }
                    divStruct += '</div>'+
                    '</div>';
                    if(((j+1)%3) == 0){
                        divStruct += '</div><div class="col-md-4 no-pad-lr">';

                    }   
                }
                
            }
            $("#customFieldDataCust").html(divStruct);
        }


        var indexCustom = findIndexByKeyValue( localJson.data.customFieldDetail , "tabType" , "CUSTOMER" );

        if( indexCustom == "-1" ){
            $('#moreDet').hide();
        }
    }
    console.log(custInfoData);
}

function productDetails(){
    
    var custId = localStorage.POTGCustDetailCustId; 

    currCustId = custId;

    var productData = custInfoData[0].ProductDetail;
    var newRow = '';
    if( productData != "No record found" ){

        for( var i=0; i<productData.length; i++ ){

            newRow += '<tr>'+
                        '<td>'+ productData[i].ProductName +'</td>'+
                        //'<td>'+ productData[i].Price +'</td>'+
                        '<td>'+ productData[i].Quantity +'</td>'+
                        '<td>'+ productData[i].UOM +'</td>'+
                        '<td class="amtDisp">'+ numberFormat( productData[i].Value ) +'</td>'+
                   ' </tr>';
        }

    }else{
        // toastr.warning('No record found');
    }
    $('#productData').html(newRow);
    $('#productDT').DataTable();
}

function ledgerDetails(){
    
    var custId = localStorage.POTGCustDetailCustId; 
    currCustId = custId;

    var ledgerData = custInfoData[0].LedgerDetail;
    var newRow = '';
    var creditBal = '';
    var debitBal = '';
    if( ledgerData != 'No record found' ){

        ledgerData.forEach(function( record , index ){
            if( record.type == 'DR' ){
                debitBal = ( record.Amount != "" ? '&#8377; ' + numberFormat(record.Amount) : "--" );
                creditBal = '--';
            }else{
                creditBal = ( record.Amount != "" ? '&#8377; ' + numberFormat(record.Amount) : "--" );
                debitBal = '--';
            }
            newRow += '<tr>'+
                        '<td>'+ record.entryDate +'</td>'+
                        '<td>'+ record.vchType +'</td>'+
                        '<td>'+ record.vchNo +'</td>'+
                        '<td class="amtDisp">'+  creditBal  +'</td>'+
                        '<td class="amtDisp">'+  debitBal +'</td>'+
                        '<td class="amtDisp">&#8377; '+ record.closeAmt +'</td>'+
                   ' </tr>';
        });

    }else{
        // toastr.warning('No record found');
    }
    $('#ledgerData').html(newRow);
    // TablesDataTables.init();
    // TablesDataTablesEditor.init();
    $('#ledgerDT').DataTable();
    $('#tab4_2 .display.datatables-alphabet-sorting.dataTable').css('width','100%');
}

function paymentDetails(){
     
    var custId = localStorage.POTGCustDetailCustId; 
    currCustId = custId;

    var paymentData = custInfoData[0].paymentDetail;
    var newRow = '';
    if( paymentData != 'No record found' ){

        paymentData.forEach(function( record , index ){
            
            newRow += '<tr>'+
                            '<td>'+ record.receiptId +'</td>'+
                            '<td>'+ record.invoiceNum +'</td>'+
                            '<td class="amtDisp">&#8377; '+ numberFormat(record.totalAmt) +'</td>'+
                            '<td>'+ record.narration +'</td>'+
                            '<td>'+ mysqltoDesiredFormat( record.voucherDate , 'dd-MM-yyyy' ) +'</td>'+
                      '</tr>';
        });        
    
    }else{
        // toastr.warning('No record found');
    }
    $('#paymentHistory').html(newRow);
    $('#paymentDt').DataTable();
}

function invoiceDetails(context){

    var custId = $(context).attr('custId'); 
    currCustId = custId;

    var invoiceData = custInfoData[0].InvoiceDetail;
    // console.log(invoiceData)
    if( invoiceData != "No record found" ){

        var newRow = '';

        // for( var i=0; i<invoiceData.length; i++ ){

        invoiceData.forEach( function( record , index ){

            newRow += '<div class="panel">'+
                        '<div class="panel-heading">'+
                            '<a class="panel-title panel-width" data-toggle="collapse" invId="'+ record.invoiceId +'" data-parent="#accordion" href="#collapse'+ index +'">Invoice # : '+ record.voucherNumber +' <span class="pull-right"> <i class="fa fa-rupee"></i> '+ record.totalInvoiceAmt +'</span></a>'+
                        '</div>'+
                        '<div id="collapse'+ index +'" invId="'+ record.invoiceId +'" class="panel-collapse collapse">'+
                            '<div class="panel-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-6">'+
                                        '<a href="javascript:void(0)" inqid="'+ record.inqId +'" onclick="navigateToInquiryDetailOpen(this);">Order #: '+ record.orderNo +'</a>'+
                                    '</div>'+
                                    // '<div class="col-md-4">'+
                                    //     '<span>Voucher #: '+ record.voucherNumber +'</span>'+
                                    // '</div>'+
                                    '<div class="col-md-6">'+
                                        '<span class="pull-right">Voucher Date: '+ mysqltoDesiredFormat( record.voucherDate , 'dd-MM-yyyy' ) +'</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="overflow-table" id="Inv_'+ record.invoiceId +'">'+
                                    
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
        });

        $('#accordion.Invoice').html(newRow);

        $('#invoice-details').modal('show');    

        $('#accordion').on('show.bs.collapse', function () {

            setTimeout( function(){
                var invId = $('#accordion .panel-collapse.panel-collapse.collapse.in').attr('invId');
                getInvoiceData( invId );
            },500);
        });
    }
    else{
        // displayAPIErrorMsg( "" , GBL_ERR_NO_INVOICE );
    }
}

function getInvoiceData( invId ){

    var invoiceId = invId;
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'getInvoiceDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 16),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        invoiceId: invoiceId,
    };
    commonAjax(INQURL1, postData, getInvoiceDataCallBack,CREATINGINQDATA);

    function getInvoiceDataCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            renderInvoiceDataInTable( data.data , invoiceId );

        }else {
            if (flag){

                // toastr.error(data.status);
                // displayAPIErrorMsg( data.status , GBL_ERR_NO_INVOICE_DATA );
            }
            else{
                toastr.error(SERVERERROR);
            }
        }
    }
}

function renderInvoiceDataInTable( invoiceData , invoiceId ){


    var tableRow = '<table class="display datatables-alphabet-sorting" id="invoiceDataTbl_'+ invoiceId +'">'+
                        '<thead>'+
                            '<tr>'+
                                '<th>#</th>'+
                                '<th>Item</th>'+
                                '<th>Qty</th>'+
                                '<th>Discount</th>'+
                                '<th>Tax</th>'+
                                '<th>Price</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
    
    if( invoiceData.itemDetail != "No record found" ){

        invoiceData.itemDetail.forEach( function( record , index ){

                var amtTypeDis = ( (record.disType == "percentage") ? record.disAmount+"%" : "<i class='fa fa-rupee'></i>"+record.disAmount );
                var amtTypeAmt = ( (record.disType == "percentage") ? record.taxAmount+"%" : "<i class='fa fa-rupee'></i>"+record.taxAmount );

                tableRow += '<tr>'+
                            '<td>'+ record.srNo +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td>'+ ( (record.itemQty == 0) ? "--" : record.itemQty ) +'</td>'+
                            '<td>'+ amtTypeDis +'</td>'+
                            '<td>'+ amtTypeAmt +'</td>'+
                            '<td><i class="fa fa-rupee"></i>'+ record.itemRate +'</td>'+
                        '</tr>';
        });      
    }

    if( invoiceData.taxDetail != "No record found" ){
        
        invoiceData.taxDetail.forEach( function( record , index ){

                tableRow += '<tr>'+
                            '<td><span style="display:none">'+ parseInt( invoiceData.itemDetail.srNo ) + index +'</span></td>'+
                            '<td></td>'+ 
                            '<td></td>'+
                            '<td></td>'+
                            '<td>'+ record.taxName +'</td>'+
                            '<td><i class="fa fa-rupee"></i>'+ numberFormat( parseFloat( record.taxRate ).toFixed(2) ) +'</td>'+
                        '</tr>';
        });      
    }

    tableRow += '</tbody>'+
                    '</table>';  

    $('#Inv_'+invoiceId).html(tableRow);

    $('#invoiceDataTbl_'+ invoiceId ).DataTable({ "stateSave": true });
}

function agingDetails(context){
    
    var custId = $(context).attr('custId'); 
    currCustId = custId;
    if( custInfoData == "" ){
        return false;
    }
    var AgingDetail = custInfoData[0].AgingDetail;

    if( AgingDetail != "No record found" && AgingDetail != null){

        setTimeout(function(){
          google.charts.setOnLoadCallback(drawBarColors);
        },100);

        function drawBarColors() {

              var chartArr = [];
              var firstArr = [];

              // firstArr.push( 'Element' );
              // firstArr.push( 'Aging' );
              // firstArr.push( { role: 'style' } );
              // firstArr.push( { role: 'annotation' } );

              // chartArr.push( firstArr );
              //   AgingDetail.forEach(function(record,index) {
                    
              //     var clrCode = "";
              //     if( index == 0 ){
              //         clrCode = "#b87333"
              //     }else if( index == 1 ){
              //         clrCode = "silver"
              //     }else if( index == 2 ){
              //         clrCode = "gold"
              //     }else if( index == 3 ){
              //         clrCode = "#e5e4e2"
              //     }else if( index == 4 ){
              //         clrCode = "#b87333"
              //     }else{
              //         clrCode = "silver"
              //     }
              //     var temp = [];
              //     temp.push(record);
              //     temp.push(clrCode);
              //     chartArr.push( temp );
              // });
              var data = google.visualization.arrayToDataTable([
                 ['Element', 'Aging', { role: 'style' } , { role: 'annotation' }],
                 ['thirty' ,AgingDetail.thirty,  '#b87333',AgingDetail.thirty],            // RGB value
                 ['sixtty' ,AgingDetail.sixtty,  'silver',AgingDetail.sixtty],            // English color name
                 ['oneTwenty' ,AgingDetail.oneTwenty, 'gold',AgingDetail.oneTwenty],
                 ['oneEighty' ,AgingDetail.oneEighty,  'color: #e5e4e2' ,AgingDetail.oneEighty], // CSS-style declaration
                 ['oneEightyPlus' ,AgingDetail.oneEightyPlus,  'color: #e5e4e2' ,AgingDetail.oneEightyPlus], // CSS-style declaration
              ]);
              
              // var data = google.visualization.arrayToDataTable(chartArr);
              var options = {
                title: '',
                height : "500",
                width : "100%",
                colors: ['#b0120a', '#ffab91'],
                hAxis: {
                  title: 'Total Aging',
                  minValue: 0
                },
                vAxis: {
                  title: 'Aging'
                },
                animation: {
                    duration: 1000,
                    easing: 'out',
                    startup: true
                }
              };
              var chart = new google.visualization.BarChart(document.getElementById('barChartContainer'));
              chart.draw(data, options);
        } 
    }else{
        // toastr.warning('No record found');
        displayAPIErrorMsg( "" , GBL_ERR_NO_AGING );
    }
}

function inqDetail(){
    
    var custId = localStorage.POTGCustDetailCustId; 
    currCustId = custId;
    
    var inqData = custInfoData[0].inqListing;
    
    var tableHeader = '<table class="table table-bordered table-striped table-condensed cf" id="inqDataTable">'+
        '<thead>'+
            '<tr>'+
                '<th>Inquiry No</th>'+
                '<th>Inquiry Name</th>'+
                '<th>Inquiry Remark</th>'+
                '<th>Status</th>'+
                '<th>Owner</th>'+
            '</tr>'+
        '</thead>'+
        '<tbody id="inqDetailDataBody">';
    var tableFooter = '</tbody>'+
    '</table>';

    var newRow = '';
    if( inqData != "No record found" ){

        for( var i=0; i<inqData.length; i++ ){

            newRow += '<tr>'+
                            '<td><a href="javascript:void(0)" onclick="navigateToInquiryDetail(this)" custid="'+currCustId+'" inqid="'+inqData[i]['inqNo']+'">'+inqData[i]['clientLevelInqNo']+'</a></td>'+
                            '<td>'+inqData[i]['inqName']+'</td>'+
                            '<td>'+inqData[i]['inqRemark']+'</td>'+
                            '<td>'+inqData[i]['inqStatus']+'</td>'+
                            '<td>'+inqData[i]['inqOwner']+'</td>'+
                        '</tr>';
        }

        
    }
    else{
        // toastr.warning('No record found');
    }
    $('#inqDetailData').html(tableHeader + newRow + tableFooter);
    $("#inqDataTable").DataTable({ "stateSave": true });
}

function inventoryDetails(){

    var custId = localStorage.POTGCustDetailCustId; 
    currCustId = custId;
    
    var ActivityData = custInfoData[0].ActivityDetail;

    var newRow = '';
    var thead = '<table id="tableListingAct" class="table table-bordered table-striped table-condensed cf">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>'+
                                'Activity No'+
                            '</th>'+
                            '<th>Activity Title</th>'+
                            '<th>Mode</th>'+
                            '<th>Due Date</th>'+
                            '<th>Assigned To</th>'+
                            '<th>Status</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="inventoryData">';

    var tfoot = '</tbody></table>';

    if( ActivityData != "No record found" ){

        var count = 1 ;

        for( var i=0; i<ActivityData.length; i++ ){

            count = i + 1;
            newRow += '<tr>'+
                        '<td>'+ count +'</td>'+
                        '<td data-activityid="'+ ActivityData[i].activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;"><a href="javascript:void(0)">'+ ActivityData[i].clientLevelActId +'</a></td>'+
                        '<td>'+ ActivityData[i].activityTitle +'</td>'+
                        '<td>'+ ActivityData[i].activityMode +'</td>'+
                        '<td>'+ ( (ActivityData[i].activityDueDate == "0000-00-00") ? "--" : mysqltoDesiredFormat( ActivityData[i].activityDueDate , 'dd-MM-yyyy') ) +'</td>'+
                        '<td>'+ ActivityData[i].activityAssignTo +'</td>'+
                        '<td>'+ ActivityData[i].activityStatus +'</td>'+
                    ' </tr>';
        }

    }else{
        // toastr.warning('No record found');
    }
    $('#dtTableActivity').html(thead + newRow + tfoot);
    $("#tableListingAct").DataTable({ "stateSave": true });
}

function custEdit(){

    var MembersData = multiPhoneData;
    var newRow = '';

    $('#newPhoneNum').val('');
    $('#phoneList').html('');

    if( MembersData != 'No record found' ){

        for( var i=0; i<MembersData.length; i++ ){
            var defaultFlag = '';
            if ( MembersData[i].defaultFlag == "1" ){
                defaultFlag = 'checked';
            }

            newRow += '<tr>'+
                        '<td><div class="radioer form-inline">'+                                                    
                                '<input id="radio-'+ MembersData[i].phoneId +'" type="radio" '+ defaultFlag +' name="isDefaultContact">'+
                                '<label for="radio-'+ MembersData[i].phoneId +'"></label>'+
                            '</div></td>'+ 
                        '<td>'+ MembersData[i].phone +'</td>'+
                        '<td><a href="javascript:void(0)" id="'+ MembersData[i].phoneId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="phone" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                    ' </tr>';
        }
        $('#phoneList').html( newRow );
        
    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_MOBILE );
    }
    
    $('#cust-edit-phone').modal('show');
 
    addFocusId( 'newPhoneNum' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    changePhone(); // Initialize for Default change
}

function custEmailEdit(){

    var MembersData = multiEmailData;
    var newRow = '';

    $('#newEmail').val('');
    $('#emailList').html('');

    if( MembersData != 'No record found' ){

        for( var i=0; i<MembersData.length; i++ ){
            
            var defaultFlag = '';
            if ( MembersData[i].defaultFlag == "1" ){
                defaultFlag = 'checked';
            }

            newRow += '<tr>'+
                        '<td><div class="radioer form-inline">'+                                                    
                                '<input id="radio-'+ MembersData[i].emailAddressId +'" type="radio" '+ defaultFlag +' name="isDefaultMail">'+
                                '<label for="radio-'+ MembersData[i].emailAddressId +'"></label>'+
                            '</div></td>'+
                        '<td>'+ MembersData[i].emailAddress +'</td>'+
                        '<td><a href="javascript:void(0)" id="'+ MembersData[i].emailAddressId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="email" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                    ' </tr>';
        }

        $('#emailList').html( newRow );

    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_EMAIL );
    }
    
    $('#cust-edit-email').modal('show'); 
    addFocusId( 'newEmail' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    changeEmail();
}

function addressEdit(){

    var MembersData = multiAddressData;
    var newRow = '';

    $('#addressList').html('');
    resetPopUp();

    if( MembersData != 'No record found' ){

        for( var i=0; i<MembersData.length; i++ ){
            
            var defaultFlag = '';
            if ( MembersData[i].defaultFlag == "1" ){
                defaultFlag = 'checked';
            }

            var stateId = $('#addStateId option[value='+ MembersData[i].stateId +']').text();
            populateCity(MembersData[i].state);
            var cityId = $('#addCityId option[value='+ MembersData[i].cityId +']').text();

            newRow += '<tr>'+
                         '<td><div class="radioer form-inline">'+                                                    
                            '<input id="radio-'+ MembersData[i].addressId +'" type="radio" '+ defaultFlag +' name="isDefaultAddress">'+
                            '<label for="radio-'+ MembersData[i].addressId +'"></label>'+
                        '</div></td>'+
                        '<td>'+ MembersData[i].address +'</td>'+
                        '<td>'+ MembersData[i].catchArea +'</td>'+
                        '<td>'+ MembersData[i].city +'</td>'+
                        '<td>'+ MembersData[i].state +'</td>'+
                        '<td>'+ MembersData[i].country +'</td>'+
                        '<td><a href="javascript:void(0)" id="'+ MembersData[i].addressId +'" type="address" class="btn btn-xs btn-primary btn-ripple btn-danger" type="address" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                    ' </tr>';
        }

        $('#addressList').html( newRow );
        
    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_ADDRESS );
    }
    
    $('#address-edit').modal('show');
    addFocusId( 'newaddress' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

//    var state = $('#custState').html();
//    $("#custStateId option").filter(function() {
//        return $(this).text() == state; 
//    }).prop('selected', true);
//    $("#custStateId").selectpicker('refresh');
//
//    populateCity( $('#custStateId').val() );
//
//    var city = $('#custCity').html();
//    $("#custCityId option").filter(function() {
//        return $(this).text() == city; 
//    }).prop('selected', true);
//    $("#custCityId").selectpicker('refresh');

    changeAddress();
}

function groupEdit(){
    
    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    $('.nav-tabs a[href="#tab4_1"]').tab('show');

    openAttachment();
    
    initCustomField();  //init Custom field
    renderContactPerson();  //null init dataTable for contact customer
    
    /* element selector defination START */
    var $ddEmpList = $('#userData');
    /* element selector defination END */

    var userListArr = GBL_USERLISTARR;
    var optionHtml = '<option value="-1">Select Employee</option>';

    for (var i = 0; i < userListArr.length; i++)
    {
        var userName = userListArr[i].fullName;
        var userId = userListArr[i].userId;
        optionHtml += '<option value="' + userId + '">' + userName + '</option>';
    }

    
    $($ddEmpList).html(optionHtml);
    $($ddEmpList).selectpicker('refresh');
    
    // $('#custEditDOB').val('');
    // $('#custEditDOA').val('');
    
    var custData = custInfoData[0].CustomerInfo;
    var groupData = custInfoData[0].CustomerInfo.groupData;

    $('#custNameEdit').html( custData.Name );
    $('#custEditGroup').val( custData.CustomerGroup );

    $('#custEditEthinicity').val( custData.EthinicityId );
    $('#custEditEthinicity').selectpicker('refresh'); 

    $('#indType').val( custData.indTypeId );
    $('#indType').selectpicker('refresh');
    
    $('#rateType').val( custData.rateTypeId );
    $('#rateType').selectpicker('refresh');
    
    $('#custStateId').val( custData.stateId );
    $('#custStateId').selectpicker('refresh');
    
    $('#custWebsite').val( custData.custWebsite );

    populateCity(custData.stateId);
    
    $('#custCityId').val( custData.cityId );
    $('#custCityId').selectpicker('refresh');

    $('#custEditType').val( custData.CustomerType );
    $('#custEditType').selectpicker('refresh');

    // $('#custEditOccupation').val( custData.Occupation );
    // $('#custEditOccupation').selectpicker('refresh');

    $('#custEditDesignation').val( custData.Designation );
    $('#custEditPan').val( custData.PanNo );
    $('#custEditGst').val( custData.gstTinNo );

    // if( custData.DOB != "0000-00-00" && custData.DOB != "" && custData.DOB != "1970-01-01"){
    //     $('#custEditDOB').val( mysqltoDesiredFormat(custData.DOB,'dd-MM-yyyy') );
    // }

    // if( custData.DOA != "0000-00-00" && custData.DOA != "" && custData.DOA != "1970-01-01"){
    //     $('#custEditDOA').val( mysqltoDesiredFormat(custData.DOA,'dd-MM-yyyy') );
    // }

    for( var i=0; i<groupData.length; i++ ){
        $("#custEditGroup option[value='" + groupData[i].groupId + "|--|"+ groupData[i].groupName +"']").prop("selected", true);
    }
    $("#custEditGroup").selectpicker('refresh');
    
    $($ddEmpList).val( custData.selectedUserId );
    $($ddEmpList).selectpicker('refresh');
    
    $('#group-edit').modal('show');

    addFocusId( 'indType' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function updateCustContactDetail(type){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }else if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var custData = custInfoData[0].CustomerInfo;
    var custGroup = custData.CustomerGroup;
    var address = custData.FullAddress;
    var ethinicity = custData.EthinicityId;
    var cityId = custData.cityId;
    var stateId = custData.stateId;
    var custType = custData.CustomerType;
    // var occupation = custData.Occupation;
    var designation = custData.Designation;
    var panNo = custData.PanNo;
    var gstNo = custData.gstTinNo;
    // var DOB = custData.DOB;
    // var DOA = custData.DOA;
    var selectedUserId = custData.selectedUserId;
    var indTypeId = custData.indTypeId;
    var custTypeId = custData.custTypeId;
    var orderCount = custData.totalOrderCount;
    var inqCount = custData.totalInqCount;

    var custMobile = custData.MobileNo;
//    var first_name = custData.Name.split(' ')[0];
    var first_name = custData.Name.substr(0,custData.Name.indexOf(' '));
    var last_name = custData.Name.substr(custData.Name.indexOf(' ')+1);

    var cust_data = {
        address: address,
        contact_name: "",
        contact_number: "",
        cust_mobile: custMobile,
        first_name: first_name,
        last_name: last_name,
        rateTypeId: "",
        remark: "",
        special_feedback: "",
        ethnicity : ethinicity,
        custType : custType,
        // occupation : occupation,
        designation : designation,
        panNo : panNo,
        gstNo : gstNo,
        // birthdate : DOB,
        // anniversary_date : DOA,
        cityId : cityId,
        stateId : stateId,
        selectedUserId : selectedUserId,
        indusTypeId : indTypeId,
        custTypeId : custTypeId,
        orderCount : orderCount,
        inqCount : inqCount
    }

    if( type == 'phone' ){
        var index = findIndexByKeyValue( multiPhoneData , 'defaultFlag' , 1 );
        if( index == "-1" ){
            toastr.warning('Please Select Default Mobile Number');
            return false;
        }
    }else if( type == 'mail' ){
        var index = findIndexByKeyValue( multiEmailData , 'defaultFlag' , 1 );
        if( index == "-1" ){
            toastr.warning('Please Select Default Email Address');
            return false;
        }
    }else{
        var index = findIndexByKeyValue( multiAddressData , 'defaultFlag' , 1 );
        if( index == "-1" ){
            toastr.warning('Please Select Default Address');
            return false;
        }
    }

    multiPhone = multiPhoneData;
    multiEmail = multiEmailData;
    multiAddress = multiAddressData;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'editcust',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,11),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        custId: currCustId,
        cust_data : cust_data,
        groupData : multiGroupData,
        multiAddress: multiAddress,
        multiEmail: multiEmail,
        multiPhone: multiPhone
    }

    // console.log(postData); return false;
    commonAjax(FOLLOWUPURL, postData, getcustContactUpdateCallback,"Please Wait... Getting Dashboard Detail");

}

function getcustContactUpdateCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $('#cust-edit-email').modal('hide');
        $('#address-edit').modal('hide');
        $('#cust-edit-phone').modal('hide');

        multiPhoneData = [];
        multiEmailData = [];
        multiAddressData = [];

        getCustInfoData();
        
    } else {
        if (flag)
            displayAPIErrorMsg( "" , GBL_ERR_UPDATE_CUSTOMER_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

function UpdateCustomer(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }else if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    var indType = $('#indType').val();
    var rateType = $('#rateType').val();
    var custGroup = $('#custEditGroup').val();
    var ethinicity = $('#custEditEthinicity').val();
    var custType = $('#custEditType').val();
    // var occupation = $('#custEditOccupation').val();
    var designation = "";
    // var designation = $('#custEditDesignation').val();
    var panNo = $('#custEditPan').val();
    var gstNo = $('#custEditGst').val();//ADD BY DARSHAN ON 23-03-2018 FOR CUST GST NO
    // var DOB = $('#custEditDOB').val();
    // var DOA = $('#custEditDOA').val();
    var custWebsite = $('#custWebsite').val();
    // var cityId = $('#custCityId option:selected').val();
    // var stateId = $('#custStateId option:selected').val();
    var selectedUserId = $('#userData option:selected').val();

    // if( indType == "-1" ){
    //     toastr.warning('Please Select Industry Type Of Customer');
    //     $('#indType').focus();
    //     return false;
    
    // }else if( custGroup == null ){
    //     toastr.warning('Please Select Customer Group');
    //     $('#custEditGroup').focus();
    //     return false;
    
    // }else if( ethinicity == "-1" || ethinicity == null ){
    //     toastr.warning('Please Select Customer Ethnicity');
    //     $('#custEditEthinicity').focus();
    //     return false;
    
    // }else if( custType == "-1" || custType == null ){
    //     toastr.warning('Please Select Customer Type');
    //     $('#custEditType').focus();
    //     return false;
    
    // }else if( occupation == "-1" || occupation == null ){
    //     toastr.warning('Please Select Customer Occupation');
    //     $('#custEditOccupation').focus();
    //     return false;
    
    // }else if( designation == "" ){
    //     toastr.warning('Please Enter Customer Designation');
    //     $('#custEditDesignation').focus();
    //     return false;
    
    // }else if( panNo == "" ){
    //     toastr.warning('Please Enter Customer Pan No');
    //     $('#custEditPan').focus();
    //     return false;
    
    // }else if( DOB == "" ){
    //     toastr.warning('Please Enter Customer Date of Birth');
    //     $('#custEditDOB').focus();
    //     return false;
    
    // }else if( DOA == "" ){
    //     toastr.warning('Please Enter Customer Date of Anniversary');
    //     $('#custEditDOA').focus();
    //     return false;
    // }
    if( selectedUserId == "-1" ){
        toastr.warning('Please Select Responsible Manager');
        $('#userData').focus();
        return false;
    
    }else{
        
        multiGroupData = []; // ADDED BY DEEPAK ON 01-07-2016 ONLY SEND SELECTED.
        if( custGroup != null ){
            for( var j=0; j<custGroup.length; j++ ){
                var tempGroup = {
                    groupId : custGroup[j].split('|--|')[0],
                    groupName : custGroup[j].split('|--|')[1]
                }
                multiGroupData.push( tempGroup );
            }
        }

        var custData = custInfoData[0].CustomerInfo;

        var custMobile = custData.MobileNo;
        var FullAddress = custData.FullAddress;
        var custTypeId = custData.custTypeId;
        var orderCount = custData.totalOrderCount;
        var inqCount = custData.totalInqCount;
        
        var custExpName = custData.Name.split(' ');

        if( custExpName.length == 2 ){
            first_name  = custExpName[0];
            last_name = custExpName[1];
        
        }else if( custExpName.length == 3 ){
            first_name  = custExpName[0] +' '+custExpName[1];
            last_name = custExpName[2];

        }else if( custExpName.length == 4 ){
            first_name  = custExpName[0] +' '+custExpName[1];
            last_name = custExpName[2] +' '+custExpName[3];

        }else if( custExpName.length > 4 ){
            first_name  = custExpName[0] +' '+custExpName[1];
            last_name = custExpName[2] +' '+custExpName[3] +' '+custExpName[4];
        
        }else{
            first_name  = custData.Name;
        }

        // if( DOB != "" ){
        //     DOB = ddmmyyToMysql(DOB);
        // }

        // if( DOA != "" ){
        //     DOA = ddmmyyToMysql(DOA);
        // }

        var cust_data = {
            indusTypeId: indType,
            address: FullAddress,
            contact_name: "",
            contact_number: "",
            cust_mobile: custMobile,
            first_name: first_name,
            last_name: last_name,
            rateTypeId: rateType,
            remark: "",
            special_feedback: "",
            ethnicity : ethinicity,
            custType : custType,
            // occupation : occupation,
            designation : designation,
            panNo : panNo,
            gstNo : gstNo,
            // birthdate : DOB,
            // anniversary_date : DOA,
            selectedUserId : selectedUserId,
            custWebsite : custWebsite,
            custTypeId : custTypeId,
            orderCount : orderCount,
            inqCount : inqCount
        }

        console.log('Validation Passed');

        // START GETTING CUSTOM FILED AND IT DATA IF SET FOR INQUIRY
        var customFieldsArr = [];
        var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
        if(localJson.data.customFieldDetail != "No record found"){
            var customFieldValue = {};
            for(var f=0;f<localJson.data.customFieldDetail.length;f++){
                if(localJson.data.customFieldDetail[f]['tabType'] == "CUSTOMER"){

                    if(localJson.data.customFieldDetail[f]['fieldType'] == "radio"){
                        //var customFieldValueTemp = 
                        customFieldValue['field'+(localJson.data.customFieldDetail[f].priorityLevel)] =  $('input[name=field'+(localJson.data.customFieldDetail[f].priorityLevel)+']:checked').val();
                    }else if(localJson.data.customFieldDetail[f]['fieldType'] == "checkbox"){
                        var tempChck = '';
                        $('input[name="field'+(localJson.data.customFieldDetail[f].priorityLevel)+'"]:checked').each(function() {
                           // console.log(this.value);
                           if( tempChck.length == 0 ){
                                tempChck += $(this).val();
                           }else{
                                tempChck += ',' + $(this).val();
                           } 
                        });
                        if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && tempChck == "" ){
                            toastr.warning("Please Select "+ localJson.data.customFieldDetail[f].fieldName);
                            return false;
                        }else{
                            customFieldValue['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = tempChck; 
                        }
                    } else{
                        if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && ( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "" || $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "-1" ) ){
                            
                            var msg = "Please Enter "+ localJson.data.customFieldDetail[f].fieldName;
                            if( localJson.data.customFieldDetail[f].fieldType == "dropdown" || localJson.data.customFieldDetail[f].fieldType == "date"){
                                msg = "Please Select "+ localJson.data.customFieldDetail[f].fieldName;
                            }

                            toastr.warning( msg );
                            $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                            return false;
                        }else{

                            if( localJson.data.customFieldDetail[f]['fieldType'] == "email" && $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() != "" && !validateEmail( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() ) ){
                                toastr.warning( "Please Enter Valid Email Id in "+ localJson.data.customFieldDetail[f].fieldName );
                                $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                                return false;
                            }
                            
                            customFieldValue['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val();
                        }
                    }
                }
            }
            customFieldsArr.push(customFieldValue);
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'editcust',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3,11),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            custId: currCustId,
            cust_data : cust_data,
            groupData : multiGroupData,
            multiAddress: multiAddressData,
            multiEmail: multiEmailData,
            multiPhone: multiPhoneData,
            customField: customFieldsArr,
        }

        // console.log(postData); return false;
        commonAjax(FOLLOWUPURL, postData, getcustGroupUpdateCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function getcustGroupUpdateCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $('#group-edit').modal('hide');
        getCustInfoData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_CUSTOMER_FAILED );
        else
            toastr.warning(SERVERERROR);
    }
}

function addContactNumber(){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }


    var moNo = getIntlMobileNum("newPhoneNum");    // GET MOBILE NUM
    // var moNo = $('#newPhoneNum').val().trim();
    var defaultPhone = "0";

    if( !checkValidMobile( "newPhoneNum" ) ){
    // if( moNo == "" || moNo.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter valid Mobile Number');
        $('#newPhoneNum').focus();
        return false;
    }else{

        resetPopUp();

        var custData = custInfoData[0].CustomerInfo;

        if( multiPhoneData.length == 0 ){
            defaultPhone = "1";
        }

        var index = findIndexByKeyValue( multiPhoneData , "phone" , moNo );
        if( index != "-1" ){
            toastr.warning('This Mobile Number is already exist');
            return false;
        }

        var tempPhone = {
            comment: "",
            defaultFlag: defaultPhone,
            deleteFlag: 1,
            phone: moNo,
            phoneId: moNo,
        }
        multiPhoneData.push( tempPhone );

        console.log(multiPhoneData);

        var chk = '';
        if( defaultPhone == "1" ){
            chk = "checked";
        }

        $('#phoneList .dataTables_empty').parents('tr').remove();
        $('#phoneList').append('<tr><td><div class="radioer form-inline">'+                                                    
                                        '<input '+ chk +' id="radio-'+ moNo +'" type="radio" name="isDefaultContact">'+
                                        '<label for="radio-'+ moNo +'"></label>'+
                                    '</div></td>'+
                                '<td>'+ moNo +'</td>'+
                                '<td><a href="javascript:void(0)" id="'+ moNo +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="phone" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>')
    }
    changePhone();
}

function addEmail(){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var email = $('#newEmail').val().trim();
    var defaultMail = "0";

    if( email.length == 0 ){
        toastr.warning('Please Enter Email Number');
        $('#newEmail').focus();
        return false;
    }else if( !validateEmail(email) ){
        toastr.warning('Please Enter valid E-Mail');
        $('#newEmail').focus();
        return false;
    }else{

        resetPopUp();
        var custData = custInfoData[0].CustomerInfo;

        if( multiEmailData.length == 0 ){
            defaultMail = "1";
        }

        var tempEmail = {
            comment: "",
            defaultFlag: defaultMail,
            deleteFlag: 1,
            emailAddress: email,
            emailAddressId: email,
        }
        multiEmailData.push( tempEmail );

        console.log(multiEmailData);

        var chk = '';
        if( defaultMail == "1" ){
            chk = "checked";
        }

        $('#emailList .dataTables_empty').parents('tr').remove();
        $('#emailList').append('<tr><td><div class="radioer form-inline">'+                                                    
                                        '<input '+ chk +' id="radio-'+ email +'" type="radio" name="isDefaultMail">'+
                                        '<label for="radio-'+ email +'"></label>'+
                                    '</div></td>'+
                                '<td>'+ email +'</td>'+
                                '<td><a href="javascript:void(0)" id="'+ email +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="email" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>')
    }
    changeEmail();
}

function addAddress(){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var address = $('#newaddress').val().trim();
    var catArea = $('#catArea').val().trim();
    var custCountryId = $('#custCountryId').val();
    var custStateId = $('#custStateId').val();
    var custCityId = $('#custCityId').val();
    var custCountryTxt = $('#custCountryId option:selected').text();
    var custStateTxt = $('#custStateId option:selected').text();
    var custCityTxt = $('#custCityId option:selected').text();
    var defaultAddress = "0";

    if( address.length == 0 ){
        toastr.warning('Please Enter address');
        $('#newaddress').focus();
        return false;

    }else if( custCountryId == "-1" ){
        toastr.warning('Please Select Country');
        addFocusId( 'custCountryId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;

    }else if( custStateId == "-1" ){
        toastr.warning('Please Select State');
        $('#custStateId').focus();
        return false;

    }else if( custCityId == "-1" ){
        toastr.warning('Please Select City');
        $('#custCityId').focus();
        return false;

    }else{

        resetPopUp();

        var custData = custInfoData[0].CustomerInfo;

        if( multiAddressData.length == 0 ){
            defaultAddress = "1";
            $('#outAdd_'+currCustId).html( $("#custStateId option[value='"+ custStateId +"']").text() +' - '+ $("#custCityId option[value='"+ custCityId +"']").text() );
        }

        var tempaddress = {
            comment: "",
            defaultFlag: defaultAddress,
            deleteFlag: 1,
            address: address,
            catchArea: catArea,
            countryId: custCountryId,
            cityId: custCityId,
            stateId: custStateId,
            addressId: "add_"+addCnt,
        }
        multiAddressData.push( tempaddress );

        console.log(multiAddressData);

        var chk = '';
        if( defaultAddress == "1" ){
            chk = "checked";
        }

        $('#addressList .dataTables_empty').parents('tr').remove();
        $('#addressList').append('<tr><td><div class="radioer form-inline">'+                                                    
                                        '<input '+ chk +' id="radio-add_'+ addCnt +'" type="radio" name="isDefaultAddress">'+
                                        '<label for="radio-add_'+ addCnt +'"></label>'+
                                    '</div></td>'+
                                '<td>'+ address +'</td>'+
                                '<td>'+ catArea +'</td>'+
                                '<td>'+ custCityTxt +'</td>'+
                                '<td>'+ custStateTxt +'</td>'+
                                '<td>'+ custCountryTxt +'</td>'+
                                '<td><a href="javascript:void(0)" id="add_'+ addCnt +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="address" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>');
        addCnt = addCnt + 1;
    }
    $('#custCountryId').val('-1');
    $('#custCountryId').selectpicker('refresh');
    $('#custStateId').val('-1');
    $('#custStateId').selectpicker('refresh');
    $('#custCityId').val('-1');
    $('#custCityId').selectpicker('refresh');
    changeAddress();
}

function populateCity(stateId){
    
    if(stateId == "-1"){
        setOption("0", "custCityId", "", "--Select City --");
    }else{
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(data.data.cityStateListing,"ID",stateId);
       
        var cityListing = "";
        if(index != -1){
            cityListing  = (data.data.cityStateListing[index]['cityListing'] != "No record found") ? data.data.cityStateListing[index]['cityListing'] : "";
        }else{
            cityListing = "";
        }
        setOption("0", "custCityId", cityListing, "--Select City --");
    }
    
    $("#custCityId").selectpicker("refresh");    
}

function resetPopUp(){

    $('#newaddress').val('');
    $('#newPhoneNum').val('');
    $('#catArea').val('');
    $('#custStateId').val('-1');
    $('#custCityId').val('-1');
    $('#custStateId').selectpicker('refresh');
    $('#custCityId').selectpicker('refresh');
    $('#newEmail').val('');
}

function changePhone(){
    $('input[type=radio][name=isDefaultContact]').change(function() {
        var id = $(this).attr('id').split('-')[1];

        for( var i=0; i<multiPhoneData.length; i++ ){

            if( id !=  multiPhoneData[i].phoneId ){
                multiPhoneData[i].defaultFlag = 0;
            }else{
                multiPhoneData[i].defaultFlag = 1;
            }
        }
        console.log( multiPhoneData );
    });
}

function changeEmail(){
    $('input[type=radio][name=isDefaultMail]').change(function() {
        var id = $(this).attr('id').split('-')[1];

        for( var i=0; i<multiEmailData.length; i++ ){

            if( id !=  multiEmailData[i].emailAddressId ){
                multiEmailData[i].defaultFlag = 0;
            }else{
                multiEmailData[i].defaultFlag = 1;
            }
        }
        console.log( multiEmailData );
    });
}

function changeAddress(){
    $('input[type=radio][name=isDefaultAddress]').change(function() {
        var id = $(this).attr('id').split('-')[1];

        for( var i=0; i<multiAddressData.length; i++ ){

            if( id !=  multiAddressData[i].addressId ){
                multiAddressData[i].defaultFlag = 0;
            }else{
                multiAddressData[i].defaultFlag = 1;
            }
        }
        console.log( multiAddressData );
    });
}

function deleteMultiData(context){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var id =  $(context).attr('id');
    var type =  $(context).attr('type');
    console.log(id);

    if( type == 'phone' ){
        //Delete Contact Data
        var index = findIndexByKeyValue( multiPhoneData , "phoneId" , id );

        if( index != "-1" ){
            if( multiPhoneData[index].defaultFlag != "1" ){

                if( multiPhoneData[index].deleteFlag == 0 ){
                    multiPhoneData[index].deleteFlag = -1;
                }else{
                    multiPhoneData.splice( index , 1 );
                }
            }else{
                toastr.error( "You can't Delete Default Phone number" );
                return false;
            }
        }else{
            toastr.warning('Deletion is not Possible');
        }
        console.log( multiPhoneData );

    }else if( type == 'email' ){
        //Delete mail Data
        var index = findIndexByKeyValue( multiEmailData , "emailAddressId" , id );

        if( index != "-1" ){

            if( multiEmailData[index].defaultFlag != "1" ){
                if( multiEmailData[index].deleteFlag == 0 ){
                    multiEmailData[index].deleteFlag = -1;
                }else{
                    multiPhoneData.splice( index , 1 );
                }
            }else{
                toastr.error( "You can't Delete Default Email Address" );
                return false;
            }
        }else{
            toastr.warning('Deletion is not Possible');
        }
        console.log( multiEmailData );

    }else{
        //Delete address Data
        var index = findIndexByKeyValue( multiAddressData , "addressId" , id );

        if( index != "-1" ){

            if( multiAddressData[index].defaultFlag != "1" ){
                if( multiAddressData[index].deleteFlag == 0 ){
                    multiAddressData[index].deleteFlag = -1;
                }else{
                    multiAddressData.splice( index , 1 );
                }
            }else{
                toastr.error( "You can't Delete Default Address" );
                return false;
            }
        }else{
            toastr.warning('Deletion is not Possible');
        }
        console.log( multiAddressData );
    }
    $(context).parents('td').parent('tr').remove();
}

function getLastLevelUserListing()
{
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
    var postData = {
        requestCase: 'userListingActivityModule',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2, 6),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(COMMONURL, postData, callBackGetLevel10UserList, "Please Wait... Getting the level 10 user listing");
    
    function callBackGetLevel10UserList(flag, data) {
        if (data.status == SCS && flag) {

            if (data.data)
            {
                GBL_USERLISTARR = data.data;

            }
        }
        else {
            
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
        }

    }

}

function initCustomField(){
    
    var custData = custInfoData[0].CustomerInfo;
    var readOnlyClass = '';
    var disableClass = '';
    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        var divStruct = '<div class="row">';
        for(var j=0;j<localJson.data.customFieldDetail.length;j++){
            if(localJson.data.customFieldDetail[j]['tabType'] == "CUSTOMER"){
                divStruct += '<div class="col-lg-12">';
                if(localJson.data.customFieldDetail[j].fieldType == "text"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'" '+readOnlyClass+' />';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "number"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="number" min="0" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'"/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "email"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="email" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'"/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<textarea id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" class="form-control" '+readOnlyClass+' placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'">'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</textarea>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
                    divStruct += '<div class="form-group radioer">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+' '+disableClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectPckr" '+disableClass+'>';
                    divStruct += '<option value="-1">-- '+localJson.data.customFieldDetail[j]['fieldName']+' --</option>';
                    for(var g=0;g<explodeValue.length;g++){
                        var classSeleted = "";
                        if(explodeValue[g] == custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            classSeleted = "selected";
                        }
                        divStruct += '<option value="'+explodeValue[g]+'" '+classSeleted+'>'+explodeValue[g]+'</option>';
                    }
                    divStruct += "</select>";
                    $("#field"+(j+1)).selectpicker();
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" class="form-control normal-date-picker" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="'+custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]+'" '+readOnlyClass+' readOnly/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += requiredClass;
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+' '+disableClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span></br>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += requiredClass;
                    divStruct += '<div class="form-group checkboxer">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    var explodeGetData = custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)].split(",");
                    
                    for(var g=0;g<explodeValue.length;g++){
                        for(var h=0;h<explodeGetData.length;h++){
                            var selectedClass = "";
                            if(explodeValue[g] == explodeGetData[h] ){
                                selectedClass = "checked";
                                break;
                            }
                        }
                        divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ '" value="'+explodeValue[g]+'" '+selectedClass+' '+ disableClass +' /> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" '+readOnlyClass+'>'+explodeValue[g]+'</label>';
                    }
                }
                
                divStruct += '</div>'+
                '</div>';
                if(((j+1)%2) == 0){
                    divStruct += '</div><div class="row">';

                } 
                $("#customFieldDiv").show();  
            }
            
        }
        $("#customFieldDiv").html(divStruct);
        $('.selectPckr').selectpicker('refresh');
        
        $('.normal-date-picker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    }

    // var disableClass = "";
    // var readOnlyClass = "";
    // var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    // if(localJson.data.customFieldDetail != "No record found"){
    //     var divStruct = '<div class="row">';
    //     for(var j=0;j<localJson.data.customFieldDetail.length;j++){
    //         if(localJson.data.customFieldDetail[j]['tabType'] == "CUSTOMER"){
    //             console.log(localJson.data.customFieldDetail[j]);
    //             divStruct += '<div class="col-lg-6">';
    //             if(localJson.data.customFieldDetail[j].fieldType == "text"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<input type="text" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="" class="form-control" value="'+ localJson.data.customFieldDetail[j].fieldValue +'" '+readOnlyClass+' />';
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<textarea id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control" '+readOnlyClass+'>'+ localJson.data.customFieldDetail[j].fieldValue +'</textarea>';
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
    //                 divStruct += '<div class="form-group radioer" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var selectedClass = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         selectedClass = "checked";
    //                     }
    //                     divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+' '+disableClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
    //                 }
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectpicker" '+disableClass+'>';
    //                 divStruct += '<option value="-1">---'+localJson.data.customFieldDetail[j]['fieldName']+'---</option>';
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var classSeleted = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         classSeleted = "selected";
    //                     }
    //                     divStruct += '<option value="'+explodeValue[g]+'" classSeleted>'+explodeValue[g]+'</option>';
    //                 }
    //                 divStruct += "</select>";
    //                 $("#field"+(j+1)).selectpicker();
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<input type="text" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="" class="form-control" value="'+getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]+'" '+readOnlyClass+' />';
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var selectedClass = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         selectedClass = "checked";
    //                     }
    //                     divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+' '+disableClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
    //                 }
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
    //                 divStruct += '<div class="form-group checkboxer" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var selectedClass = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         selectedClass = "checked";
    //                     }
    //                     divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+'> <label for="'+explodeValue[g]+'" '+readOnlyClass+'>'+explodeValue[g]+'</label>';
    //                 }
    //             }
    //             divStruct += '</div>'+
    //             '</div>';
    //             if(((j+1)%2) == 0){
    //                 divStruct += '</div><div class="row">';

    //             }   
    //         }
            
    //     }
    //     $("#customFieldDiv").html(divStruct);
    // }
}

function addContactPerson(){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
// contactpersonData
    var contName = $('#conpersonname').val();
    var contNum = getIntlMobileNum("conpersonNum");    // GET MOBILE NUM
    // var contNum = $('#conpersonNum').val();
    var contEmail = $('#conpersonEmail').val();
    var contDesig = $('#conpersonDesig').val();
    var conpersonDob = $('#conpersonDob').val();
    var conpersonDoa = $('#conpersonDoa').val();

    if( contName.length == '0' ){
        toastr.warning('Please Enter Contact Name');
        $('#conpersonname').focus();
        return false;

    }else if( !checkValidMobile( "conpersonNum" ) ){
    // }else if( contNum == '' || contNum.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Contact Number');
        $('#conpersonNum').focus();
        return false;

    }else if( !validateEmail(contEmail) ){
        toastr.warning('Please Enter Valid Contact Email');
        $('#conpersonEmail').focus();
        return false;

    }else{

        var tempContData = {
            contactCustFName : contName,
            contactCustLName : "",
            contactCustMobile : contNum,
            custId : currCustId,
            contactCustEmail : contEmail,
            contactCustDesignation : contDesig,
            conpersonDob : conpersonDob,
            conpersonDoa : conpersonDoa,
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Inquiry Details
        var postData = {
            requestCase: 'addCreateContactPerson',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 9),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            conactPersonDetail : tempContData,
        };
        commonAjax(INQURL1, postData, addContactCallBack,CREATINGINQDATA);

        function addContactCallBack(flag, data){
            if (data.status == "Success" && flag) {
                
                var tempContact = {
                    contactId : data.data.contactId,
                    fname : contName,
                    lname : "",
                    contactNumber : contNum,
                    contactEmail : contEmail,
                    contactDesig : contDesig,
                    custId : currCustId,
                    contactDob : conpersonDob,
                    contactDoa : conpersonDoa,
                    deleteFlag : currCustId,
                }

                if( contactpersonData != 'No record found' ){

                    contactpersonData.push( tempContact );
                }else{
                    contactpersonData = [];
                    contactpersonData.push( tempContact );
                }

                $('#conpersonname').val('');
                $('#conpersonNum').val('');
                $('#conpersonEmail').val('');
                $('#conpersonDesig').val('');
                $('#contactDob').val('');
                $('#contactDoa').val('');

                renderContactPerson();

            }else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_CREATE_CONTACT_PERSON );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

function renderContactPerson(){

    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting" style="border:0px;">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>Name</th>'+
                            '<th>MobileNo</th>'+
                            '<th>Email</th>'+
                            '<th>Designation</th>'+
                            '<th>DOB</th>'+
                            '<th>DOA</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="inquiryDetailTable">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    if( contactpersonData != "No record found" ){

        for ( var i=0; i<contactpersonData.length; i++ ){

            newRow += '<tr>'+
                        '<td>'+ contactpersonData[i].fname +'</td>'+
                        '<td>'+ contactpersonData[i].contactNumber +'</td>'+
                        '<td>'+ contactpersonData[i].contactEmail +'</td>'+
                        '<td>'+ contactpersonData[i].contactDesig +'</td>'+
                        '<td>'+ contactpersonData[i].contactDob +'</td>'+
                        '<td>'+ contactpersonData[i].contactDoa +'</td>'+
                       '</tr>';
        }
    }

    $('#contTable').html(thead + newRow + tfoot);

    $("#tableListing").DataTable({ "stateSave": true });
}


function hideShow(){
    $('#customFieldDataCust').toggle('150');
    $('.moredetails').find('i').toggleClass('fa-sort-asc fa-sort-desc')
}

// CHANGES FOR BUG ON LIVE SERVER - 5
function navigateToActCreate(){

    if( checkAuth(2, 5) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    localStorage.POTGCreateActCustId = localStorage.POTGCustDetailCustId;
    localStorage.POTGCreateActContId = custInfoData[0].CustomerInfo.multiContactData[0].contactId;
    localStorage.POTGCreateActCustName = custInfoData[0].CustomerInfo.Name;
    localStorage.POTGCreateActCustMob = custInfoData[0].CustomerInfo.MobileNo;
    localStorage.POTGCreateActContList = JSON.stringify( custInfoData[0].CustomerInfo.multiContactData );
    localStorage.POTGCustContDetail = custInfoData[0].CustomerInfo.multiContactData[0].fname +' , '+ custInfoData[0].CustomerInfo.multiContactData[0].contactNumber;
    navigateToactivity();
}

function navigateToInqCreate(){

    if( checkAuth(4, 13) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    localStorage.POTGCreateInqCustId = localStorage.POTGCustDetailCustId;
    // localStorage.POTGCreateInqCustName = custInfoData[0].CustomerInfo.Name;

    var index = findIndexByKeyValue( custInfoData[0].CustomerInfo.multiAddressData , 'defaultFlag' , "1" );
    var emailIndex = findIndexByKeyValue( custInfoData[0].CustomerInfo.multiEmailData , 'defaultFlag' , "1" );
    var phoneIndex = findIndexByKeyValue( custInfoData[0].CustomerInfo.multiPhoneData , 'defaultFlag' , "1" );
    if( custInfoData[0].CustomerInfo.multiContactData != "" && custInfoData[0].CustomerInfo.multiContactData != "No record found" ){
        var custContPerId = custInfoData[0].CustomerInfo.multiContactData[0].contactId;
    }

    var cpAllList = [];
    //GENERATE CONTACT PERSONS
    if( custInfoData[0].CustomerInfo.multiContactData != "" && custInfoData[0].CustomerInfo.multiContactData != "No record found" ){

        custInfoData[0].CustomerInfo.multiContactData.forEach(function( contRecrd , contIndx ){
            var tempData = {
                "CONTACTNAME" : contRecrd.fname+' '+contRecrd.lname,
                "CONTACT_NUMBER" : contRecrd.contactNumber,
                "CONTACT_DESIGN" : contRecrd.contactDesig,
                "CONTACT_EMAIL" : contRecrd.contactEmail,
            }
            cpAllList.push( tempData );
        });
    }

    var tempJson = {
        custId :  custInfoData[0].CustId,
        phoneId :  ( (phoneIndex == "-1") ? "" : (custInfoData[0].CustomerInfo.multiPhoneData[phoneIndex].phoneId) ),
        custName : custInfoData[0].CustomerInfo.Name,
        custMobileNo : custInfoData[0].CustomerInfo.MobileNo,
        custAddress : custInfoData[0].CustomerInfo.FullAddress ,
        custCountry : custInfoData[0].CustomerInfo.countryId,
        custState : custInfoData[0].CustomerInfo.stateId,
        custCity : custInfoData[0].CustomerInfo.cityId,
        custContactName : custInfoData[0].CustomerInfo.multiContactData[0].fname + ' ' + custInfoData[0].CustomerInfo.multiContactData[0].lname,
        custContactNo : custInfoData[0].CustomerInfo.multiContactData[0].contactNumber,
        custEmail : ( (emailIndex == "-1" ) ? "" : (custInfoData[0].CustomerInfo.multiEmailData[emailIndex].emailAddress) ),
        custContEmail : custInfoData[0].CustomerInfo.multiContactData[0].contactEmail,
        custContDesg : custInfoData[0].CustomerInfo.multiContactData[0].contactDesig,
        custCatchArea : ( (index == "-1" ) ? "" : ( custInfoData[0].CustomerInfo.multiAddressData[index].catchArea) ),
        custContPerId : custContPerId,
        custGstinNo : custInfoData[0].CustomerInfo.gstTinNo,
        contactData : cpAllList
    };
    var custInqJson = [];
    custInqJson.push( tempJson );
    localStorage.custInqRedData = JSON.stringify( custInqJson );

    navigateToCreateInquiry();
}

function pageInitialEvents(){
    var data = JSON.parse(localStorage.indeCampusRoleAuth);

    setOption("0", "custEditEthinicity", data.data.ethinicity, "--Ethnicity--");
    // setOption("0", "custEditOccupation", data.data.occupation, "--Occupation--");
    setOption("0", "custEditType", data.data.customertype, "--Select Customer Type--");
    // setOption("0", "custStateId", data.data.cityStateListing, "--Select State --");
    setOption("0", "indType", data.data.industryType, "--Select Customer Industry --");
    $('#indType').selectpicker();
    setOption("0", "rateType", data.data.itemRateType, "--Select Customer Rate Type --");
    $('#rateType').selectpicker();

    var GBL_RATETYPEACTIVATE = data.data.itemRateTypeFlag;
    if( GBL_RATETYPEACTIVATE == "1" ){
        $('.rateTypeFlag').show();
    }else{
        $('.rateTypeFlag').hide();
    }
    
    $("#custStateId").selectpicker();    
    $("#custCityId").selectpicker(); 
    $('#inventoryData').dataTable({ "stateSave": true });

    $('#userImg').attr('src', httpHead + '//'+ FINALPATH +'/assets/globals/img/avtar1.png')
    setCountryList( 'custCountryId' );   // SET COUNTRY LISTING FROM LOAD AUTH SET DATA IN SELECT PICKER USING ID

    initTelInput( 'newPhoneNum' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput( 'conpersonNum' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    google.charts.load('current', {packages: ['corechart', 'bar' , 'gauge' , 'geochart']});
}


var attachmetData = [];

function openAttachment(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "attachmentListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        custId: currCustId,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, sendAttachmentCallBack,CREATINGINQDATA);

    function sendAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            attachmetData = data.data;

            renderAttachment();

        }else {
            if (flag){
                // toastr.error(data.status);
            }
            else{
                // toastr.error(SERVERERROR);
            }
        }
    }
    $('#attachmentAdd').modal('show');
    // assignAttachementEvent();
}

function renderAttachment(){
    var attachLi = '<div class="row">';
    
    for( var i=0; i<attachmetData.length; i++ ){
        var iconNm = '';
        if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "pdf" ){
            iconNm = 'file-pdf-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "doc" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "docx"  ){
            iconNm = 'file-word-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "txt" ){
            iconNm = 'file-text-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "csv" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "xls" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "xlsx" ){
            iconNm = 'file-excel-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "jpg" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "jpeg" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "png" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "gif" ){
            iconNm = 'file-image-o';
        }
        attachLi += '<div class="col-md-3 col-sm-6 col-xs-6">'+
                        '<div class="list-content">'+
                            '<a style="text-align:center" href="javascript:void(0)" data-link="'+ attachmetData[i].fileLink +'" onclick="openLinkInNewTab(this);">'+
                                '<i class="fa fa-'+ iconNm +' iconStyle"></i>'+
                                '<a href="javascript:void(0)" attchId="'+ attachmetData[i].fileId +'" onclick="deleteAttachment(this)">'+
                                    '<i style="margin-top: 5px;" class="fa fa-trash-o pull-right"></i>'+
                                    '<span>'+ attachmetData[i].attachTitle +'</span>'+
                                '</a>'+
                            '</a>'+                            
                        '</div>'+
                    '</div>';
    }
    attachLi += '</div>';

    $('#AttachmentListing').html( attachLi );
}

function submitInqAttach(){
    // grab your file object from a file input
    // $('#attachmentName').change(function () {

        var attachTitle = $('#attachmentTitle').val();
        var attachFile = $('#attachmentName').val()
        if( attachTitle == "" ){
            toastr.warning('Please Enter Title');
            $('#attachmentTitle').focus();
            return false;

        }else if( attachFile == "" ){
            toastr.warning('Please Select File');
            $('#attachmentName').click();
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "attachmentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7, 23),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            custId : currCustId,
            attachTitle :attachTitle,
            emailAdress : ""
        };
        $("#postData").val( JSON.stringify(temp_json));

        $("#fileupload").off().on('submit',(function(e) {
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",       // Type of request to be send, called as method
                data:  new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData:false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function(data)// A function to be called if request succeeds
                {
                    data = JSON.parse(data);
                    console.log(data);
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                            attachTitle: data.data.attachTitle,
                        }

                        attachmetData.push( tempData );

                        renderAttachment();
                    }else{
                        toastr.warning(data.status);

                    }
                    addRemoveLoader(0);
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                }
            });
        }));
        $("#fileupload").submit();
        $('#attachmentName').val('');
        $('#attachmentTitle').val('');
    // });
}

function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attchId');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'attachmentListingDelete',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 16),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        custId: currCustId,
        attachId : attachmentId
    };
    commonAjax(INQURL1, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var index = findIndexByKeyValue( attachmetData , "fileId" , attachmentId );
            
            attachmetData.splice( index , 1 );

            renderAttachment();

        }else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

function openLinkInNewTab(linkData){
    var link = $(linkData).attr("data-link");
    if(link==""){
        toastr.warning("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}

function navigateToInquiryDetailOpen(context){
    
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        var inqId = $(context).attr('inqId');
        var custId = $(context).attr('custId');
        
        localStorage.POTGclickInqId = inqId;
        localStorage.POTGcustId = custId;
        
        window.open("inquirydetail.html");
    }
}

function addNote() {
    
    if( checkAuth(4,15) == -1 ){
        toastr.error(NOAUTH);
        return false;
    }   

//    if( GBL_CURR_STAT_ID == GBL_CANCEL_INQ_STATID ){
//        toastr.error('You are not Authorised for this Activity, Because this Inquiry is in Lost Status');
//        return false;
//    }

    var noteText = $('#noteArea').val();

    if( noteText == "" ){
        toastr.warning("Please add note");
        $('#noteArea').focus();
        return false;
    }
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'addEditNoteOnCustomer',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,11),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
        noteText : noteText,
        custId : currCustId,
        noteId : (GBL_EDIT_NOTE_ID != "-1") ? GBL_EDIT_NOTE_ID : 0
    }

    if( GBL_EDIT_NOTE_ID != "-1" && GBL_EDIT_NOTE_ID != undefined ){
        commonAjax(COMMONURL, postData, editNoteCallBack,"Please Wait... Getting Inquiry Data");
    }else{
        commonAjax(COMMONURL, postData, addNoteCallBack,"Please Wait... Getting Inquiry Data");
    }

    function addNoteCallBack(flag,data){

        if (data.status == "Success" && flag) {
                    
            toastr.success('Note Added Successfully');
            GBL_NOTE_LIST.push(data.data[0]);     
            $('#noteArea').val('');  
            renderNoteData();  

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_UPDATE_NOTE );
            else
                toastr.error(SERVERERROR);
        }
    }

    function editNoteCallBack(flag,data){

        if (data.status == "Success" && flag) {
                    
            toastr.success('Note Updated Successfully');
            if(GBL_EDIT_NOTE_ID == "-1"){
                GBL_NOTE_LIST.push(data.data);     
            }else{ // UPDATE DATA
                GBL_NOTE_LIST[GBL_EDIT_NOTE_INDEX] = data.data[0];     
            }
            GBL_EDIT_NOTE_ID = "-1";
            $('#noteArea').val('');  
            renderNoteData();

        } else {
            if (flag)  
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_UPDATE_NOTE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderNoteData() {
   
   var theader = '<table class="display table datatables-alphabet-sorting" id="noteTble">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Note</th>'+
                            '<th>Created By</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';

    var newRow = '';
    var tfoot = '</tbody></table>';
    if(GBL_NOTE_LIST != "No record found" && GBL_NOTE_LIST != null){

        GBL_NOTE_LIST.forEach(function( record , index ){

            newRow += '<tr>'+
                            '<td>'+ (index+1)  +'</td>'+
                            '<td>'+ record.noteText  +'</td>'+
                            '<td>'+ record.createdBy +'</td>'+
                            '<td>'+ 
                             '<a href="javascript:void(0)" onclick="editNote(this)" noteId="'+ record.noteId +'" index="'+ index +'" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>&nbsp;'+
                              '<a href="javascript:void(0)" onclick="deleteNote(this)" noteId="'+ record.noteId +'" index="'+ index +'" class="btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                             '</td>'+
                      '</tr>';
        });
    }              

    $('#noteTable').html( theader + newRow + tfoot );
    $('#noteTble').dataTable({ "stateSave": true });
}

var GBL_EDIT_NOTE_ID = "-1";
var GBL_EDIT_NOTE_INDEX = "-1";
function editNote(context){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;

    }

    GBL_EDIT_NOTE_ID = $(context).attr('noteId');
    GBL_EDIT_NOTE_INDEX = $(context).attr('index');
    
    $('#noteArea').val( GBL_NOTE_LIST[GBL_EDIT_NOTE_INDEX].noteText );
}

function deleteNote(context){
    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error( NOAUTH );
        return false;
    }
    var deleteNoteId = $(context).attr('noteId');
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'deleteNoteOnCustomer',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,11),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        custId : currCustId,
        noteId : deleteNoteId
    }
    commonAjax(COMMONURL, postData, deleteNoteCallback,"Please Wait... deleting note detail.");
    
    
    function deleteNoteCallback(flag,data){
        if (data.status == "Success" && flag) {

            toastr.success('Note deleted successfully.');
            var index = findIndexByKeyValue( GBL_NOTE_LIST,'noteId',deleteNoteId );
            GBL_NOTE_LIST.splice(index, 1);
            
            renderNoteData();

        } else {
            if (flag)  
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_UPDATE_NOTE );
            else
                toastr.error(SERVERERROR);
        }
    }
}
