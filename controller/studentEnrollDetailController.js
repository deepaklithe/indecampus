    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-07-2018.
 * File : studentEnrollDetailController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_STUD_DATA = [];
var GBL_ALLOTMENT_DATA = [];
var GBL_STD_ID = "";
var curTab = $('.colorTab li.active a').attr('hrefId');
var GBL_INVOICE_DATA = [];
var GBL_SUSPENSION_ID = '';
var GBL_LEDGER_DATA = [];
var GBL_ATTACH_TYPE = '';
var GBL_USAGE_DATA = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    initTelInput('parentMobile');
    initTelInput('guarantorMobile1');
    initTelInput('guarantorMobile2');
    setCountryList('country');
    $('#parentState').selectpicker('refresh');
    $('#parentCity').selectpicker('refresh');
    $('#guarantorState').selectpicker('refresh');
    $('#guarantorCity').selectpicker('refresh');
    $('#collegeCity').selectpicker('refresh');
    $('#collegeState').selectpicker('refresh');
    $('#uploadMode').selectpicker('refresh');
    $('#semester').selectpicker('refresh');
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var studentRelation = roleAuth.data.studentRelation;
    setOption("0",'parentRelation',studentRelation,'');
    $('#parentRelation').selectpicker('refresh'); 
    setOption("0",'guarantorRelation',studentRelation,'');
    $('#guarantorRelation').selectpicker('refresh'); 
        
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var designationList = roleAuth.data.designation;
    setOption( '0' , 'recommendedByDesignation' , designationList , 'Select Designation' );
    $('#recommendedByDesignation').selectpicker('refresh');
    setOption( '0' , 'revokeByDesgination' , designationList , 'Select Designation' );
    $('#revokeByDesgination').selectpicker('refresh');
    getUsersList();


    if( localStorage.indeCampusEnrollDetailStdId == undefined || localStorage.indeCampusEnrollDetailStdId == "" ){
        navigateToEnrollmentMaster();
    }else{

        getStudentDetail();
        assignAttachmentEvent();

        var designationData = roleAuth.data.designation;
        setOption('0','activateDesignation',designationData,'');
        $('#activateDesignation').selectpicker('refresh');
        setOption('0','revokeByDesg',designationData,'');
        $('#revokeByDesg').selectpicker('refresh');
        setOption('0','deActivateDesignation',designationData,'');
        $('#deActivateDesignation').selectpicker('refresh');

        var start = moment();   
        var end = moment();

        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var lastDay = new Date(y, m + 1, 0);

        endDay = (mysqltoDesiredFormat(lastDay,'dd-MM-yyyy')).split('-')[0];    // END DAY VARIABLE

        //DATE RANGE PICKER INITIALIZATION
        // $('.bootstrap-daterangepicker-basic span').html(moment().format(01+', MMMM, YYYY') + ' - ' + moment().format(endDay+', MMMM, YYYY'));
        // $('.bootstrap-daterangepicker-basic').daterangepicker({

        //     minViewMode : 'month',
        //     startDate: start,
        //     endDate: end,
        //     opens:'left',
        //     locale: {
        //             format: 'YYYY-MM-DD'
        //             },
        //     }, function(start, end, label) {

        //         // $('#dateRangePicker span').html(start.format('DD, MMMM, YYYY') + ' - ' + end.format('DD, MMMM, YYYY'));
        //         // var stDate = start.format('YYYY-MM-DD');
        //         // var enDate = end.format('YYYY-MM-DD');

        //         // console.log(stDate);
        //         // console.log(enDate);
        //         // getGoalData(stDate,enDate);

        //         $('.bootstrap-daterangepicker-basic').html( start.format('DD, MMMM, YYYY') + " to " + end.format('DD, MMMM, YYYY') );

        //         // $('.month').removeClass('active');
        //         // $('#dateRangePicker').addClass('active');
        // });
        if( localStorage.indeCampusEnrollInvoiceView == true ||  localStorage.indeCampusEnrollInvoiceView == "true" ){
            $('#InvoiceDetail').click();
            localStorage.removeItem('indeCampusEnrollInvoiceView');
        }else if( localStorage.indeCampusEnrollLedgerView == true ||  localStorage.indeCampusEnrollLedgerView == "true" ){
            $('#ledgerDetail').click();
            localStorage.removeItem('indeCampusEnrollLedgerView');
        }

    }

}

// DISPLAYS SELECTED TAB DATA
function showCurTab(context,hrefId){

    $('.colorTab li').removeClass('active');
    $(context).parent('li').addClass('active');
    $('.StudentTabbing .tab-pane').removeClass('active');
    $('.StudentTabbing .tab-pane').css('display','none');
    $('#'+hrefId).addClass('active');
    $('#'+hrefId).show();

    if( hrefId == "tab4_4" ){ // INVOICE TAB
        if( GBL_INVOICE_DATA.length == 0 ){
            getInvoiceDetail();   
        }else{
            renderInvoiceData();
        }
             
    }else if( hrefId == "tab4_6" ){ // LEDGER TAB
        if( GBL_LEDGER_DATA.length == 0 ){
            getLedgerDetails();   
        }else{
            renderLedgerDetails();
        }
             
    }else if( hrefId == "tab4_7" ){ // USAGE HISTORY TAB
        if( GBL_USAGE_DATA.length == 0 ){
            getUsageHistory();   
        }else{
            renderUsageHistory();
        }
             
    }

}

// GET STUDENT DETAIL DATA
function getStudentDetail() {

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

 	var postData = {
        requestCase : 'getStudentData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        studentId : localStorage.indeCampusEnrollDetailStdId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getStudentDetailCallback,"Please wait...Adding your item.");

    function getStudentDetailCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_STUD_DATA = data.data;
            renderStudentDetails();
            
        }else{

            GBL_STUD_DATA = [];
            renderStudentDetails();
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_STUDENT_DET );
            else
                toastr.error(SERVERERROR);
        }
        getStudentAlotment();
    }

}

// RENDER AVAILABLE STUDENT DETAIL
function renderStudentDetails(){

	if( GBL_STUD_DATA != "" && GBL_STUD_DATA != "No record found" ){
        ITEMS = GBL_STUD_DATA;
        GBL_INDEX = 0;

        GBL_STD_ID = GBL_STUD_DATA[0].studId;
        var img = "assets/globals/img/avtar1.png";
        if( GBL_STUD_DATA[0].profileImage != "" ){
            img = GBL_STUD_DATA[0].profileImage;
        }
        $('#userImg').attr('src',img);

        $('#studentName').html( (GBL_STUD_DATA[0].firstName).trim() + ' ' + (GBL_STUD_DATA[0].lastName).trim() );
		$('#membershipid').html( GBL_STUD_DATA[0].memberShipNumber );
		$('#birthDate').html( mysqltoDesiredFormat(GBL_STUD_DATA[0].birthDate,'dd-MM-yyyy') );
		$('#gender').html( GBL_STUD_DATA[0].gender );
		$('#bloodGroup').html( GBL_STUD_DATA[0].bloodGroup );
		$('#fullAddress').html( GBL_STUD_DATA[0].addressLine1 + ', ' + GBL_STUD_DATA[0].addressLine2 + ', ' + GBL_STUD_DATA[0].addressLine3 + ' - ' + GBL_STUD_DATA[0].cityName + ' ' + GBL_STUD_DATA[0].stateName + ' ' + GBL_STUD_DATA[0].countryName  );
		$('#countryName').html( GBL_STUD_DATA[0].countryName );
        $('#state').html( GBL_STUD_DATA[0].stateName );
        $('#city').html( GBL_STUD_DATA[0].cityName );
        $('#pincode').html( GBL_STUD_DATA[0].pincode );
        $('#mobileNumber').html( GBL_STUD_DATA[0].mobileNo );
        $('#email').html( GBL_STUD_DATA[0].email );
        $('#passportNumber').html( GBL_STUD_DATA[0].passportNo );
        $('#aadharNumber').html( GBL_STUD_DATA[0].aadharNo );
        $('#emergencyName').html( GBL_STUD_DATA[0].emeregencyContactPersonName );
        $('#emergencyNumber').html( GBL_STUD_DATA[0].emeregencyContactPersonNo );
        $('#roomNumber').html( GBL_STUD_DATA[0].roomNo );
        $('#userName').html( GBL_STUD_DATA[0].userName );

        var medicalData = roleAuth.data.medicalCondition;
        var medIndex = findIndexByKeyValue( medicalData , 'ID' , GBL_STUD_DATA[0].medicalCondition );
        if( medIndex != -1 ){
            $('#medicalCondition').html( medicalData[medIndex].VALUE );
        }

        $('#country').val( GBL_STUD_DATA[0].countryId ).selectpicker('refresh');
        populateState('country','collegeState');
        $('#collegeState').val( GBL_STUD_DATA[0].collegeStateId ).selectpicker('refresh');
        populateCityOnState('collegeState','collegeCity');
        $('#collegeCity').val( GBL_STUD_DATA[0].collegeCityId ).selectpicker('refresh');

        $('#guarantorState').val( GBL_STUD_DATA[0].guarantorStateId ).selectpicker('refresh');
        populateCityOnState('guarantorState','guarantorCity');
        $('#guarantorCity').val( GBL_STUD_DATA[0].guarantorCityId ).selectpicker('refresh');

        $('#parentRelation').val( GBL_STUD_DATA[0].parentRelation ).selectpicker('refresh');
        $('#parentName').val( GBL_STUD_DATA[0].parentName );
        setNumberInIntl('parentMobile',GBL_STUD_DATA[0].parentMobileNo);
        $('#parentEmail').val( GBL_STUD_DATA[0].parentEmailId );
        $('#parentUsername').val( GBL_STUD_DATA[0].parentUserName );
        $('#parentAddress1').val( GBL_STUD_DATA[0].parentAddressLine1 );
        $('#parentAddress2').val( GBL_STUD_DATA[0].parentAddressLine2 );
        $('#parentAddress3').val( GBL_STUD_DATA[0].parentAddressLine3 );
        $('#parentPincode').val( GBL_STUD_DATA[0].parentPincode );

        $('#collegeName').val( GBL_STUD_DATA[0].collegeName );
        $('#courseName').val( GBL_STUD_DATA[0].course );
        $('#academicYearFrom').val( (GBL_STUD_DATA[0].acdemicStartYear != "0000-00-00" ?  (GBL_STUD_DATA[0].acdemicStartYear) : "" )  );
        $('#academicYearTo').val( (GBL_STUD_DATA[0].acdemicEndYear != "0000-00-00" ?  (GBL_STUD_DATA[0].acdemicEndYear) : "" )  );
        $('#semester').val( GBL_STUD_DATA[0].semester  ).selectpicker('refresh');
        $('#collegeAddressLine1').val( GBL_STUD_DATA[0].collegeAddressLine1 );
        $('#collegeAddressLine2').val( GBL_STUD_DATA[0].collegeAddressLine2 );
        $('#collegeAddressLine3').val( GBL_STUD_DATA[0].collegeAddressLine3 );
        $('#collegePincode').val( GBL_STUD_DATA[0].collegePinCode );

        $('#guarantorRelation').val( GBL_STUD_DATA[0].guarantorRelation ).selectpicker('refresh');
        $('#guarantorName').val( GBL_STUD_DATA[0].guarantorName );
        setNumberInIntl('guarantorMobile1',GBL_STUD_DATA[0].guarantorMobileNo1);
        setNumberInIntl('guarantorMobile2',GBL_STUD_DATA[0].guarantorMobileNo2);
        $('#guarantorEmail').val( GBL_STUD_DATA[0].guarantorEmail );
        $('#guarantorAddress1').val( GBL_STUD_DATA[0].guarantorAddress1 );
        $('#guarantorAddress2').val( GBL_STUD_DATA[0].guarantorAddress2 );
        $('#guarantorAddress3').val( GBL_STUD_DATA[0].guarantorAddress3 );
        $('#guarantorCity').val( GBL_STUD_DATA[0].guarantorCityId ).selectpicker('refresh').trigger('change');
        $('#guarantorState').val( GBL_STUD_DATA[0].guarantorStateId ).selectpicker('refresh');
        $('#guarantorPincode').val( GBL_STUD_DATA[0].guarantorPinCode );

        if( GBL_STUD_DATA[0].status == "1" ){ // ACTIVE
            $('#activateSuspension').show();
            $('#deactivateSuspension').hide();
            $('#viewAttachBtn').show();
            $('#collectPayBtn').show();
        }else if( GBL_STUD_DATA[0].status == "2" || GBL_STUD_DATA[0].status == "3" ){ // 3 = SUSPENDED , 2 = TERMINATED
            $('.StudentEdit').hide(); // HIDE EDIT BTN
            $('#activateSuspension').hide();
            $('#viewAttachBtn').hide();
            $('#collectPayBtn').hide();

            if( GBL_STUD_DATA[0].suspenssionData == "No record found" || GBL_STUD_DATA[0].suspenssionData == undefined || GBL_STUD_DATA[0].suspenssionData == ""  ){
                $('#deactivateSuspension').hide();
            }else{
                if( GBL_STUD_DATA[0].status != "2" ){
                    $('#deactivateSuspension').show();
                }
            }
        }else{ // 0 = INACTIVE
            $('.StudentEdit').hide(); // HIDE EDIT BTN
            $('#activateSuspension').hide();
            $('#deactivateSuspension').hide();
            $('#viewAttachBtn').hide();
            $('#collectPayBtn').hide();
        }

	}

}

// EDIT STUDENT DETAIL
function editStudent(){

    localStorage.indeCampusEditStudId = GBL_STD_ID;
    localStorage.indeCampusEditFromDetail = true;
    navigateToCreateEnrollment('edit');

}


// VIEW SELECTED STUDENT'S ATTACHMENT DATA MODAL
function viewAttachment(){

    if( GBL_STUD_DATA[0].attachmentData.length != 0 && GBL_STUD_DATA[0].attachmentData != "No record found" ){
        renderAttachment(GBL_STUD_DATA[0].attachmentData);
    }else{
        toastr.error( GBL_ERR_NO_ATTACH );
    }
    $('#uploadMode').val( $("#uploadMode option:first").val() ).selectpicker('refresh');
    $('#uploadMode').trigger('change');
    refreshFile();
    $('#attachmentAdd').modal('show');

}

// RENDER ATTACHMENT DATA
function renderAttachment(attachmentData){ 
    var photoAttachLi = '';
    var medicalAttachLi = '' ;

    // STUDENT'S PHOTO ATTACHMENT DISPLAY
    if( attachmentData.photosArr != "No record found" && attachmentData.photosArr != "" ){
        attachmentData.photosArr.forEach(function( photoRecord , photoIndex ){

           
            photoAttachLi += "<div class='card card-image card-light-blue bg-image bg-opaque8 col-md-4' style='width: 30%;height: 156px; margin-right: 10px;'>"+
                                "<img src='"+photoRecord.attachmentLink+"' alt='' class='gallery-image'>"+
                                "<div class='context has-action-left has-action-right'>"+
                                    "<div class='tile-content'>"+
                                        "<span class='text-title'>"+photoRecord.title+"</span>"+
                                        "<span class='text-subtitle'>png</span>"+
                                    "</div>"+
                                    "<div class='tile-action right'>"+
                                        "<a data-div='imagepopup' data-link='"+photoRecord.attachmentLink+"' onClick='viewImageLinkData(this);' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>"+
                                        "<a onClick='deleteAttachment(this)' attachPath='"+photoRecord.attachmentLink+"' attachType='PHOTO' attachmentId='"+photoRecord.attachmentId+"' class='btn btn-sm btn-warning'><i class='fa fa-trash'></i></a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
        });
    }else{
        photoAttachLi = '<ul class="list-material">'+
                            '<li class="">'+
                                '<a href="javascript:void(0)" class="visible">'+
                                    '<div class="list-content">'+
                                        '<span class="title"> No Attachment Available</span>'+
                                    '</div>'+
                                '</a>'+
                            '</li>'+
                        '</ul>';
    }

    // STUDENT'S MEDICAL CERTIFICATE ATTACHMENT DISPLAY
    if( attachmentData.certificateArr != "No record found" && attachmentData.certificateArr != ""  ){
        attachmentData.certificateArr.forEach(function( medicalRecord , medicalIndex ){
            
            medicalAttachLi += '<li class="">'+
                                    '<div class="list-content">'+
                                        '<div class="col-lg-8 col-md-4">'+
                                            '<a href="javascript:void(0)" data-link="'+ medicalRecord.attachmentLink +'" onclick="openLinkInNewTab(this);"><span class="title">'+ medicalRecord.title +'</span></a></div><div class="col-lg-4 col-md-2"><a href="javascript:void(0)" attachPath="'+ medicalRecord.attachmentLink +'" attachType="MEDICAL" attachmentId="'+ medicalRecord.attachmentId +'" onclick="deleteAttachment(this)"><i class="fa fa-trash-o pull-right"></i></a>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
        });
    }else{
        medicalAttachLi = '<li class="">'+
                            '<a href="javascript:void(0)" class="visible">'+
                                '<div class="list-content">'+
                                    '<span class="title"> No Attachment Available</span>'+
                                '</div>'+
                            '</a>'+
                        '</li>';
    }

    $('#photoAttachmentListing').html( photoAttachLi );
    $('#medicalAttachmentListing').html( medicalAttachLi );

    if( GBL_ATTACH_TYPE == "medicalCerti" ){
        $('#certiAttachDiv').show(200);
        $('#photoAttachDiv').hide(200);
    }else if( GBL_ATTACH_TYPE == "photo" ){
        $('#photoAttachDiv').show(200);
        $('#certiAttachDiv').hide(200);
    }
}

// DISPLAY ATTACHMENT ACC TO SELECTED UPLOAD TYPE - ADDED ON 11-09-2018 BY VISHAKHA TAK
function renderAttachmentType(context){

    var attachmentType = $(context).val();
    if( attachmentType == "photo" ){
        GBL_ATTACH_TYPE = "photo";
        $('#photoAttachDiv').show(200);
        $('#certiAttachDiv').hide(200);
    }else if( attachmentType == "medicalCerti" ){
        GBL_ATTACH_TYPE = "medicalCerti";
        $('#certiAttachDiv').show(200);
        $('#photoAttachDiv').hide(200);
    }

}

// UPLOAD ATTACHMENT ON SUBMIT CALL
function uploadAttachment(){

    var attachmentTitle = $('#uploadTitle').val();
    var attachmentName = $('#attachmentName').val();
    
    if( attachmentTitle == "" ){
        toastr.warning('Please Enter Attachment Title');
        addFocusId('uploadTitle');
        return false;

    }else if( attachmentName == "" ){
        toastr.warning('Please Attach File To Submit');
        return false;

    }else{

        $("#fileupload").submit();

    }

}

//  ATTACHMENT UPLOAD EVENT
function assignAttachmentEvent(){
    
    $("#fileupload").off().on('submit', (function (e) {
            
        var stdIndex = findIndexByKeyValue( GBL_STUD_DATA , 'studId' , GBL_STD_ID );

        var uploadTitle = $('#uploadTitle').val();
        var uploadType = $('#uploadMode').val();
        var uploadTypeVal = "";

        if( uploadType == "photo" ){
            uploadTypeVal = "PHOTO";

        }else{
            uploadTypeVal = "MEDICAL";
            
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "attachmentStudentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            type : uploadTypeVal,
            studentId : GBL_STD_ID,
            title : uploadTitle
        };

        $("#postDataAttach").val(JSON.stringify(temp_json));
        // console.log(temp_json);return false;

        var bfsendMsg = "Sending data to server..";
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1);
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                
                if(data.status == "Success"){
                    
                    console.log(data.data);
                    // ADD ARRAY TO ATTACHMENT JSON
                    var attachFile = {
                        attachmentId : data.data.attachmentId,
                        attachmentLink : data.data.fileLink,
                        attachmentName : data.data.fileName,
                        studId : GBL_STD_ID,
                        attachType : uploadTypeVal,
                        title : data.data.title
                    }

                    if( stdIndex != -1 ){
                        // ADD ARRAY TO ATTACHMENT JSON ACCORDING TO UPLOAD TYPE
                        if( uploadType == "photo" ){
                            if( GBL_STUD_DATA[stdIndex].attachmentData['photosArr'] == "No record found" ){
                                GBL_STUD_DATA[stdIndex].attachmentData['photosArr'] = [];
                            }
                            GBL_STUD_DATA[stdIndex].attachmentData['photosArr'].push( attachFile );                            
                        }else{
                            if( GBL_STUD_DATA[stdIndex].attachmentData['certificateArr'] == "No record found" ){
                                GBL_STUD_DATA[stdIndex].attachmentData['certificateArr'] = [];
                            }
                            GBL_STUD_DATA[stdIndex].attachmentData['certificateArr'].push( attachFile );
                        }
                        renderAttachment( GBL_STUD_DATA[stdIndex].attachmentData ); // RENDER ATTACHMENT DATA
                    }

                    refreshFile(); // REFRESH ATTACHMENT FILE
                    toastr.success( "Your attachment uploaded successfully" );
                }else{
                    toastr.warning(data.status);
                    refreshFile(); // REFRESH ATTACHMENT FILE
                    
                }
                addRemoveLoader(0); // REMOVE LOADER
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                if( jqXHR.responseText != "" && jqXHR.responseText != undefined ){
                    if( JSON.parse(jqXHR.responseText).status != "" ){
                        toastr.error(JSON.parse(jqXHR.responseText).status);
                    }else{
                        toastr.error(errdata);
                    }
                    
                }else{
                    toastr.error(errdata);
                }
                refreshFile(); // REFRESH ATTACHMENT FILE
            }
        });
    }));
}

// DELETE SELECTED ATTACHMENT
function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attachmentId');
    var attachType = $(context).attr('attachType');
    var attachPath = $(context).attr('attachPath');
    var attachmentName = ( attachPath != "" ? basenameWithExt(attachPath) : "" );

    var confmDelete = confirm("Are you sure you want to delete this attachment ?");
    if( !confmDelete ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'deleteStudentAttachment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11, 47),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        attachmentId: attachmentId,
        studentId : GBL_STD_ID,
        type : attachType,
        attachPath : attachPath,
        attachName : attachmentName
    };
    commonAjax(COMMONURL, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var studIndex = findIndexByKeyValue( GBL_STUD_DATA , 'studId' , GBL_STD_ID );

            // REMOVE ATTACHMENT FROM JSON ACCORDING TO UPLOAD TYPE
            if( studIndex != -1 ){
                if( attachType == "PHOTO" ){

                    var index = findIndexByKeyValue( GBL_STUD_DATA[studIndex].attachmentData['photosArr'] , "attachmentId" , attachmentId );
            
                    GBL_STUD_DATA[studIndex].attachmentData['photosArr'].splice( index , 1 );

                }else{

                    var index = findIndexByKeyValue( GBL_STUD_DATA[studIndex].attachmentData['certificateArr'] , "attachmentId" , attachmentId );
            
                    GBL_STUD_DATA[studIndex].attachmentData['certificateArr'].splice( index , 1 );
                }

                renderAttachment(GBL_STUD_DATA[studIndex].attachmentData); // RENDER ATTACHMENT DATA
            }

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

// REFRESH ATTACHMENT FILE
function refreshFile(){

    $('#uploadTitle').val('');
    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

// OPEN SELECTED ATTACHMENT LINK IN NEW TAB
function openLinkInNewTab(context){
    var link = $(context).attr("data-link");
    if(link==""){
        toastr.warnings("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}

// VIEW IMAGE LINK IN MODAL
function viewImageLinkData(docData){
    var link = $(docData).attr("data-link");
    $("#imageLink").attr("src",link);
    openModal(docData);   
}

// INITIALIZE DATERANGEPICKER
function initDateRangePicker(){

    // START INITIALIZATION OF DATE RANGE PICKER
    var start = moment();
    var end = moment();
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var lastDay = new Date(y, m + 1, 0);

    endDay = (mysqltoDesiredFormat(lastDay,'dd-MM-yyyy')).split('-')[0];

    $('.suspensionPeriod span').html(moment().format(01+', MMMM, YYYY') + ' to ' + moment().format(endDay+', MMMM, YYYY'));
    $('.suspensionPeriod').daterangepicker( // GIVES DATE VALUE IN FORMAT "MM/DD/YYYY - MM/DD/YYYY"
    {
        // minViewMode : 'month',
        startDate: start,
        endDate: end,
        opens:'left',
        minDate:new Date(),
        format: 'DD-MM-YYYY',
        separator:' to ',
        // locale: {
        //         format: 'DD-MM-YYYY'
        //         },
        }, function(start, end, label) {

            $('.suspensionPeriod span').html(start.format('DD, MMMM, YYYY') + ' to ' + end.format('DD, MMMM, YYYY'));
            $('.suspensionPeriod').val(start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
            var stDate = start.format('DD-MM-YYYY');
            var enDate = end.format('DD-MM-YYYY');

            console.log(stDate);
            console.log(enDate);
            $('.suspensionPeriod').addClass('active');
    });
    $('.suspensionPeriod').attr('readonly',true);
    // $('.suspensionPeriod').val( mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') + ' to ' + mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
    // END INITIALIZATION OF DATE RANGE PICKER

}

// OPEN ACTIVATE SUSPENSION / TERMINATION MODAL
function openActivateModal(){

    initDateRangePicker();
    $('#suspensionMembershipid').val( GBL_STUD_DATA[0].memberShipNumber ).css('pointer-events','none');
    var studentName = (GBL_STUD_DATA[0].firstName).trim() + ' ' + (GBL_STUD_DATA[0].lastName).trim();
    $('#suspensionStudName').val( studentName ).css('pointer-events','none');
    $('#suspensionRoomNumber').val( GBL_STUD_DATA[0].roomNo ).css('pointer-events','none');
    $('#activateSuspPeriod').val('');
    $('#recommendedByUser').val('-1').selectpicker('refresh');
    $('#recommendedByDesignation').val('-1').selectpicker('refresh');
    var suspensionReason = roleAuth.data.suspenssionTerminationReason;
    setOption( '0' , 'susTermReason' , suspensionReason , '' );
    $('#susTermReason').selectpicker('refresh');
    $('#terminationWefDate').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true
    });
    $('#terminationWefDate').attr('readonly',true);
    // $('#activateSuspPeriod').val( mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') + ' to ' + mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
    modalShow('ActivateModal');

}

// OPEN DEACTIVATE SUSPENSION / TERMINATION MODAL
function openDeActivateModal(){

    initDateRangePicker();
    $('#deactivateSuspPeriod').val( mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') + ' to ' + mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
    $('#revokeMemId').val( GBL_STUD_DATA[0].memberShipNumber ).css('pointer-events','none');
    var studentName = (GBL_STUD_DATA[0].firstName).trim() + ' ' + (GBL_STUD_DATA[0].lastName).trim();
    $('#revokeStudName').val( studentName ).css('pointer-events','none');
    $('#revokeRoomNo').val( GBL_STUD_DATA[0].roomNo ).css('pointer-events','none');
    $('#terminationWef').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true
    });
    $('#terminationWef').attr('readonly',true);
    var suspensionReason = roleAuth.data.suspenssionTerminationReason;
    setOption( '0' , 'susTermReasonDeact' , suspensionReason , '' );
    $('#susTermReasonDeact').selectpicker('refresh');

    var suspensionData = GBL_STUD_DATA[0].suspenssionData[0];
    var suspensionStartDate = suspensionData.startDate;
    var suspensionEndDate = suspensionData.endDate;
    var suspensionPeriodDate = mysqltoDesiredFormat(suspensionStartDate,'dd-MM-yyyy') + ' to ' + mysqltoDesiredFormat(suspensionEndDate,'dd-MM-yyyy');
    $('#deactivateSuspPeriod').data('daterangepicker').setStartDate(mysqltoDesiredFormat(suspensionStartDate,'MM/dd/yyyy'));
    $('#deactivateSuspPeriod').data('daterangepicker').setEndDate(mysqltoDesiredFormat(suspensionEndDate,'MM/dd/yyyy'));
    $('#deactivateSuspPeriod').val( suspensionPeriodDate ).css('pointer-events','none');
        
    var suspTermReason = suspensionData.reason;
    if( suspTermReason != "" && suspTermReason != null && suspTermReason != "-1" && suspTermReason != undefined ){
        var suspTermReasonIds = suspTermReason.split(',');
        $('#susTermReasonDeact').val( suspTermReasonIds ).selectpicker('refresh');
    }

    $('#terminationWef').datepicker( 'update' , mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy'));
    $('#revokeByUser').val('-1').selectpicker('refresh');
    $('#revokeByDesgination').val('-1').selectpicker('refresh');
    $('#susTermTypeDeactive').attr('disabled',true);   
    modalShow('DeactivateModal');

}

// REDIRECT TO COLLECT PAYMENT DATA
function navigateToCollectPayment(){

    localStorage.indeCampusEditStudId = GBL_STD_ID;
    window.location.href = "collectpayment.html";

}

// GET INVOICE LISTING DATA
function getInvoiceDetail(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'listInvoiceData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        studentId : localStorage.indeCampusEnrollDetailStdId,
    };
    commonAjax(COMMONURL, postData, getInvoiceDetailCallBack,CREATINGINQDATA);

    function getInvoiceDetailCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            GBL_INVOICE_DATA = data.data;
            renderInvoiceData();
        }else {

            GBL_INVOICE_DATA = [];
            renderInvoiceData();
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_INVOICE );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

// RENDER INVOICE LISTING
function renderInvoiceData(){

    var tHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="InvoiceTable">'+
                   '<thead>'+
                      '<tr>'+
                         '<th>#</th>'+
                         '<th>Invoice No</th>'+
                         '<th>Invoice Date</th>'+                                         
                         '<th>Invoice Name</th>'+
                         '<th>Invoice Name</th>'+
                         '<th>Status</th>'+
                         '<th>Action</th>'+
                      '</tr>'+
                   '</thead>'+
                   '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

    if( GBL_INVOICE_DATA.length != 0 && GBL_INVOICE_DATA != "No record found" ){

        GBL_INVOICE_DATA.forEach(function( record , index ){

            newRow += '<tr>'+
                       '<td>'+ (index+1) +'</td>' +
                       '<td>'+ record.clientInvoiceId +'</td>'+
                       '<td>'+ mysqltoDesiredFormat(record.invoiceDate,'dd-MM-yyyy') +'</td>'+
                       '<td>'+ record.invoiceName +'</td>'+
                       '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(record.totalCharges)) +'</td>'+
                       '<td>'+ record.status +'</td>'+
                       '<td>'+    
                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-ripple btn-amber"  data-original-title="View" invoiceId="'+ record.clientInvoiceId +'" onClick="viewInvoice(this,0);"><i class="fa fa-eye"></i></a>'+
                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" data-toggle="tooltip" data-original-title="Transaction History" invoiceId="'+ record.clientInvoiceId +'" onClick="viewInvoiceTransaction(this,0);"><i class="fa fa-undo"></i></a> '+
                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-teal btn-ripple" data-toggle="tooltip" data-original-title="Email" invoiceId="'+ record.clientInvoiceId +'" onclick="sendInvoiceMail(this);"><i class="fa fa-envelope"></i></a>'+
                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-success btn-ripple" data-toggle="tooltip" data-original-title="Print" invoiceId="'+ record.clientInvoiceId +'" onClick="viewInvoice(this,1);"><i class="fa fa-print"></i></a>'+
                            '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple" data-toggle="tooltip" data-original-title="Edit" invoiceId="'+ record.clientInvoiceId +'" onClick="viewInvoiceTransaction(this,1);"><i class="fa fa-pencil"></i></a>'+
                       '</td>'+
                   '</tr>';

        });
    }
    $('#invoiceDatatable').html( tHead + newRow + tEnd );
    $('#InvoiceTable').dataTable();
}

// GET INVOICE DATA
function viewInvoice(context,type){

    var invoiceId = $(context).attr('invoiceId'); 

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'getStudentInvoiceData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(16, 66),
        studentId : GBL_STD_ID,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, viewInvoiceCallBack,CREATINGINQDATA);

    function viewInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            renderInvoiceDetail(data.data,type);
        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INVOICE );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER SELECTED PRINT INVOICE IN MODAL
function renderInvoiceDetail(invoiceData,type){

    var invoiceFlag = false;
    // type : 0 = SET HTML & MODAL OPEN , 1 = SET HTML & DIRECT PRINT
    if( invoiceData != "" && invoiceData != "No record found" ){ 

        var invoiceDetails = invoiceData[0];
        $('#invoiceNoPrint').html( '# ' + invoiceDetails.cInvoiceId );
        $('#invoiceDatePrint').html( mysqltoDesiredFormat(invoiceDetails.invoiceDate,'dd-MM-yyyy') );
        if( invoiceDetails.periodFrom != "" ){
            $('#invPeriodPrint').html(  'Period From: '+ mysqltoDesiredFormat(invoiceDetails.periodFrom,'dd-MM-yyyy') +' To ' + ( invoiceDetails.periodTo != "" ? mysqltoDesiredFormat(invoiceDetails.periodTo,'dd-MM-yyyy') : "") );   
        }else{
            $('#invPeriodPrint').html(  'Period : '+  ( invoiceDetails.periodTo != "" ? mysqltoDesiredFormat(invoiceDetails.periodTo,'dd-MM-yyyy') : "") );  
        }
        $('#studInvNamePrint').html( invoiceDetails.fullName );
        $('#address1Print').html( invoiceDetails.address );
        $('#address2Print').html( invoiceDetails.cityName + ' ' + invoiceDetails.stateName + ' ' + invoiceDetails.countryName + ' - ' + invoiceDetails.pinCode );
        $('#studMemIdPrint').html( 'Student Membership No : ' + invoiceDetails.memberShipNo  );

        var tBody = '';
        var totalCgstAmount = 0;
        var totalSgstAmount = 0;
        var totalIgstAmount = 0;
        var subTotal = 0;
        var preBalance = 0.00;
        var collection = 0.00;
        var balDue = 0.00;
        
        if( invoiceDetails.itemData != "" && invoiceDetails.itemData != "No record found" ){

            invoiceDetails.itemData.forEach(function( record , index ){
                var cgstPer = ( record.cgst != "" ? fixedTo2(record.cgst) : 0 );
                var sgstPer = ( record.sgst != "" ? fixedTo2(record.sgst) : 0 );
                var igstPer = ( record.igst != "" && record.igst != undefined ? fixedTo2(record.igst) : 0 );
                var itemTotal = parseFloat(record.itemPrice) * parseFloat(record.qty);
                var cgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(cgstPer)) / 100);
                var sgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(sgstPer)) / 100);
                var igstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(igstPer)) / 100);
                totalCgstAmount = fixedTo2(parseFloat(totalCgstAmount) + parseFloat(cgstAmt));
                totalSgstAmount = fixedTo2(parseFloat(totalSgstAmount) + parseFloat(sgstAmt));
                totalIgstAmount = fixedTo2(parseFloat(totalIgstAmount) + parseFloat(igstAmt));
                var itemQty = parseFloat(record.qty);
                var itemSub = parseFloat(itemTotal) + parseFloat(cgstAmt) + parseFloat(sgstAmt) + parseFloat(igstAmt);
                subTotal = fixedTo2(parseFloat(subTotal) + parseFloat(itemTotal));

                tBody += '<tr>'+
                            '<td style="padding: 5px 0 5px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+ (index+1) +'</td>'+
                            '<td style="padding: 5px 0px 5px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+
                                '<div>'+
                                   '<div><span style="word-wrap: break-word;" id="item_name">'+ record.itemName +'</span><br><span class="item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;" > '+record.hsnCode+'</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount">₹<span></span> '+ parseFloat(record.itemPrice) +'</div>'+
                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+                            
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span></span> '+ itemQty +'</div>'+
                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ cgstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ sgstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ sgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ igstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ igstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">'+
                                '<span id="item_amount">'+
                                '<span>₹</span> '+ formatPriceInIndianFormat(fixedTo2(itemTotal)) +'</span>'+
                            '</td>'+
                        '</tr>';

                invoiceFlag = true;
            });        
        }

        var totalPayment = ( invoiceDetails.totalPayment != null && invoiceDetails.totalPayment != "" ? invoiceDetails.totalPayment : 0.00 );
        var finalTotal = fixedTo2(parseFloat(subTotal) + parseFloat(totalCgstAmount) + parseFloat(totalSgstAmount) + parseFloat(totalIgstAmount));
        balDue = (parseFloat(preBalance) + parseFloat(totalPayment)) - parseFloat(finalTotal);
        var balanceDueType = (parseFloat(balDue) > 0) ? 'CR' : 'DR';
        collection = parseFloat(totalPayment);

        tBody += '<tr>'+
                  '<td colspan="8" style="text-align:right;">'+
                     '<table class="totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">'+
                        '<tbody>'+
                           '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>'+
                              '<td colspan="2" id="subtotal" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(subTotal) +'</td>'+
                           '</tr>'+
                           '<tr style="height:10px;">'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>'+
                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(totalCgstAmount) +'</td>'+
                           '</tr>'+
                           '<tr style="height:10px;">'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>'+
                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(totalSgstAmount) +'</td>'+
                           '</tr>'+
                           '<tr style="height:10px;">'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>'+
                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(totalIgstAmount) +'</td>'+
                           '</tr>'+
                           '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>'+
                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ formatPriceInIndianFormat(finalTotal) +'</b></td>'+
                           '</tr>'+
                           //  '<tr>'+
                           //    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Pre Balance</b></td>'+
                           //    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                           //    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ preBalance +'</b></td>'+
                           // '</tr>'+
                             '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>'+
                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>CR</b></td>'+
                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ formatPriceInIndianFormat(fixedTo2(collection)) +'</b></td>'+
                           '</tr>'+
                            '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right background-color: #f5f4f3; height: 40px;font-size: 9pt;" ><b>Balance Due</b></td>'+
                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><b>'+ balanceDueType +'</b></td>'+
                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><span>₹</span><b> '+ formatPriceInIndianFormat(fixedTo2( Math.abs(balDue) )) +'</b></td>'+
                           '</tr>'+
                            '<tr><td colspan="5"  style="font-size:16px;text-align: left;"><b><u>Terms and Conditions</u></b></td></tr>'+
                            '<tr><td colspan="5" style="padding-top:10px;padding-bottom: 4px;text-align: left;"><span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus</span></b>".</span></td></tr>'+
                         '</tbody>'+
                     '</table>'+
                  '</td>'+
                '</tr>';

        $('#printItemBody').html( tBody );
        if( invoiceFlag ){
            if( type == 0 ){
                modalShow('viewInvoiceDetail');
            }else{
                printInvoice();
            }
        }else{
            var invName = "display";
            if( type != 0 ){
                invName = 'print';
            }
            toastr.warning("No invoice details found to "+invName+" !");
        }
        

    }

}

// PRINT SELECTED INVOICE
function printInvoice(){

    var data = $('#receiptPrint').html();
    var myWindow=window.open('','','height=800,width=1200');
        
    myWindow.document.write(data);
    myWindow.document.close();
    setTimeout(function() {
        myWindow.print();
        myWindow.close();
    }, 250);

}

// GET INVOICE TRANS DATA FOR DISPLAY
function viewInvoiceTransaction(context,type){

    var invoiceId = $(context).attr('invoiceId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'getInvoiceTrans',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        studentId : GBL_STD_ID,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, viewInvoiceTransactionCallBack,CREATINGINQDATA);

    function viewInvoiceTransactionCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            var transactionData = data.data;
            renderTransactionData(transactionData,type);

        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INV_TRANS );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RENDER INVOICE TRANSACTION DATA
function renderTransactionData(transactionData,type){

    var tHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="transactionTable">'+
                   '<thead>'+
                      '<tr>'+
                         '<th>#</th>'+
                         '<th>Item Name</th>'+
                         '<th>Type</th>'+                                         
                         '<th>Charge Date</th>'+
                         '<th>Charge Qty</th>'+
                         '<th>Total Price</th>'+
                         '<th class="editInvoice">Action</th>'+
                      '</tr>'+
                   '</thead>'+
                   '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

    if( transactionData.length != 0 && transactionData != "No record found" ){

        transactionData.forEach(function( record , index ){

            var canceledInv = '';
            if( record.status == "2" ){
                canceledInv = 'disabled';
            }
            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td>'+ record.type +'</td>'+
                            '<td>'+ mysqltoDesiredFormat(record.chargeDate,'dd-MM-yyyy') +'</td>'+
                            '<td>'+ record.qty +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(record.totalPrice)) +'</td>'+
                            '<td class="editInvoice">'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-ripple" data-toggle="tooltip" data-original-title="Cancel Invoice" chargeId="'+ record.chargeId +'" invoiceId="'+ record.clientInvoiceId +'" onClick="cancelInvoice(this);" '+canceledInv+'><i class="fa fa-times"></i> Cancel</a>'+
                            '</td>'+
                        '</tr>';
        });

    }

    $('#invoiceTransaction').html( tHead + newRow + tEnd );
    if( type == "0" ){
        $('.editInvoice').hide();
    }else{
        $('#transactionModalHeading').html('Edit Invoice');
    }
    $('#transactionTable').dataTable();
    modalShow('transactionModal');
}

// CANCEL SELECTED INVOICE
function cancelInvoice(context){

    var invoiceId = $(context).attr('invoiceId');
    var chargeId = $(context).attr('chargeId');
    var cnfrmCancel = confirm("Are you sure you want to cancel invoice ?");
    if( !cnfrmCancel ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'cancelInvoiceTrans',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 96),
        chargeId : chargeId,
        studentId : GBL_STD_ID,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, cancelInvoiceCallBack,CREATINGINQDATA);

    function cancelInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            toastr.success("Invoice cancelled successfully");
            $(context).attr('disabled',true);

        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INV_TRANS_CANCEL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

}

// SEND INVOICE MAIL
function sendInvoiceMail(context){

    var invoiceId = $(context).attr('invoiceId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'sendStudentInvoiceMail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(16, 66),
        studentId : GBL_STD_ID,
        invoiceNo : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, cancelInvoiceCallBack,CREATINGINQDATA);

    function cancelInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);

        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INV_MAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

}

// GET ALLOTMENT DETAIL OF SELECTED STUDENT
function getStudentAlotment(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getStudentAllotment', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        studentId : localStorage.indeCampusEnrollDetailStdId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getAllotmentDetailCallback,"Please wait...Adding your item.");

    function getAllotmentDetailCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ALLOTMENT_DATA = data.data;
            renderAllotmentData();  // RENDER ALLOTMENT DATA 
           
        }else{

            GBL_ALLOTMENT_DATA = [];
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ALLOT_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER ALLOTMENT DATA 
function renderAllotmentData(){

    if( GBL_ALLOTMENT_DATA != "" && GBL_ALLOTMENT_DATA != "No record found" ){

         // SET TENANCY TAB DETAILS
        $('#tenancyStartDate').val( ( GBL_ALLOTMENT_DATA[0].tenancyStartDate != "" && GBL_ALLOTMENT_DATA[0].tenancyStartDate != "0000-00-00" ? mysqltoDesiredFormat(GBL_ALLOTMENT_DATA[0].tenancyStartDate,'dd-MM-yyyy') : "" ) );
        $('#tenancyEndDate').val( ( GBL_ALLOTMENT_DATA[0].tenancyEndDate != "" && GBL_ALLOTMENT_DATA[0].tenancyEndDate != "0000-00-00" ? mysqltoDesiredFormat(GBL_ALLOTMENT_DATA[0].tenancyEndDate,'dd-MM-yyyy') : "" ) );
        if( GBL_ALLOTMENT_DATA[0].tenancyDeposit != "" ){
            $('#tenancyDeposit').val( formatPriceInIndianFormat(fixedTo2(GBL_ALLOTMENT_DATA[0].tenancyDeposit)) );
            $('#totalDeposit').html( '<i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(fixedTo2(GBL_ALLOTMENT_DATA[0].tenancyDeposit)) );        
        }
        if( GBL_ALLOTMENT_DATA[0].tenancyRent != "" ){
            $('#totalRent').val( formatPriceInIndianFormat(fixedTo2(GBL_ALLOTMENT_DATA[0].tenancyRent)) );
        }

    }

}

// GET USERS LISTING DATA
function getUsersList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "userSearch",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8, 30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getUserCallback,"Please wait...Adding rooms to your item.");

    function getUserCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_USERS = data.data;
            renderUsers(); // RENDER USER LISTING

        }else{

            GBL_USERS = []; 
            renderUsers(); // RENDER USER LISTING
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER USER LISTING
function renderUsers(){ 

    var options = '<option value="-1">Select User</option>';
    if( GBL_USERS != "" && GBL_USERS != "No record found" ){

        GBL_USERS.forEach(function( record , index ){
            
            options += '<option value="'+ record.userId +'">'+ record.fullName +'</option>';

        });
    }
    $('#recommendedByUser').html( options );
    $('#recommendedByUser').selectpicker('refresh');
    $('#revokeByUser').html( options );
    $('#revokeByUser').selectpicker('refresh');

}

// SET DESIGNATION DATA OF SELECTED USER
function setDesignation(context,referenceDesgEle){

    var userId = $(context).val();
    if( userId != "-1" && userId != "" ){

        var index = findIndexByKeyValue( GBL_USERS , 'userId' , userId );
        if( index != -1 ){

            $('#'+referenceDesgEle).val( GBL_USERS[index].designationId ).selectpicker('refresh');
            $('#'+referenceDesgEle).closest('div').find('button').css('pointer-events','none');

        }else{
            $('#'+referenceDesgEle).val( '-1' ).selectpicker('refresh');
            $('#'+referenceDesgEle).closest('div').find('button').css('pointer-events','');

        }

    }else{

        $('#'+referenceDesgEle).val( '-1' ).selectpicker('refresh');
        $('#'+referenceDesgEle).closest('div').find('button').css('pointer-events','');

    }

}

// CREATE SUSPENSION / TERMINATION OF STUDENT
function createSuspensionTermination(){

    var susTermType = $('#susTermType').val();
    var susTermReason = $('#susTermReason').val();
    var membershipid = $('#suspensionMembershipid').val();
    var studentName = $('#suspensionStudName').val();
    var roomNumber = $('#suspensionRoomNumber').val();
    var suspensionPeriod = $('#activateSuspPeriod').val();
    var recommendedByUser = $('#recommendedByUser').val();
    var recommendedByDesignation = $('#recommendedByDesignation').val();
    var roomServiceAllow = $("input[name=roomServiceOption]:checked").val();
    var fBAllow = $("input[name=fBServiceOption]:checked").val();
    var laundryAllow = $("input[name=laundryOption]:checked").val();
    var eventAllow = $("input[name=eventOption]:checked").val();
    var terminationWefDate = $('#terminationWefDate').val();

    if( susTermReason == "-1" || susTermReason == "" || susTermReason == null ){
            toastr.warning("Please select suspension /  termination reason");
            addFocusId('susTermReason');
            return false;

    }else if( recommendedByUser == "-1" || recommendedByUser == "" ){
            toastr.warning("Please select recommended by user");
            addFocusId('recommendedByUser');
            return false;

        }

    if( susTermType == "1" ){ // SUSPENSION
        
        if( suspensionPeriod == "" ){
            toastr.warning("Please select suspension / termination period");
            addFocusId('activateSuspPeriod');
            return false;

        }
        terminationWefDate = '';
    }else{ // TERMINATION

        if( terminationWefDate == "" ){
            toastr.warning("Please select termination date");
            addFocusId('terminationWefDate');
            return false;

        }

    }

    var suspensionStDate = "";
    var suspensionEnDate = "";

    if( susTermType == "1" && suspensionPeriod != "" ){

        suspensionStDate = $('#activateSuspPeriod').val().split('to')[0].trim();
        suspensionEnDate = $('#activateSuspPeriod').val().split('to')[1].trim();

    }

    var susTermReasonIds = ''; // COMMA SEPARATED REASONS
    if( susTermReason != "-1" && susTermReason != "" && susTermReason != null ){
        susTermReason.forEach(function(record,index){
            if( susTermReasonIds ){
                susTermReasonIds += ','+record;
            }else{
                susTermReasonIds += record;
            }
        });
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "addEditStudentSuspension",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(25, 101),
        studentId : GBL_STD_ID,
        type : susTermType,
        reason : susTermReasonIds,
        suspenStartDate : suspensionStDate,
        suspenEndDate : suspensionEnDate,
        roomServiceAllow : roomServiceAllow,
        fbAllow : fBAllow,
        laundryAllow : laundryAllow,
        eventBookingAllow : eventAllow,
        recommendedId : recommendedByUser,
        terminationDate : terminationWefDate,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, suspensionTerminationCallback,"Please wait...Adding rooms to your item.");

    function suspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data);
            GBL_SUSPENSION_ID = data.data[data.data.length-1].suspensionId;
            modalHide('ActivateModal');
            if( susTermType != "2" ){
                $('#deactivateSuspension').show();
            }            
            $('#activateSuspension').hide();
            getStudentDetail();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_ADD_SUS_TER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// REVOKE SUSPENSION / TERMINATION
function revokeSuspensionTermination(){

    var revokeByUser = $('#revokeByUser').val();
    var revokeByDesgination = $('#revokeByDesgination').val();
    var terminationWef = $('#terminationWef').val();
    var suspensionId = GBL_STUD_DATA[0].suspenssionData[0].suspenssionId;

    if( revokeByUser == "-1" || revokeByUser == "" ){
        toastr.warning("Please select revoke recommended by user");
        addFocusId('revokeByUser');
        return false;

    }else if( terminationWef == "" ){
        toastr.warning("Please select termination with the effect date");
        addFocusId('terminationWef');
        return false;

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "revokeStudentSuspension",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(25, 103),
        studentId : GBL_STD_ID,
        suspensionId : suspensionId,
        revokeBy : revokeByUser,
        revokeDate : terminationWef,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, revokeSuspensionTerminationCallback,"Please wait...Adding rooms to your item.");

    function revokeSuspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data);
            modalHide('DeactivateModal');
            $('#deactivateSuspension').hide();
            $('#activateSuspension').show();
            $('#viewAttachBtn').show();
            $('#collectPayBtn').show();
            getStudentDetail();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_REVOKE_SUS_TER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

//START FUNCTION FOR ADD DOCUMENT
function uploadProfileImg(){

    registerCanvasEvent();
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);         
    var temp_json = {
        requestCase: "attachmentStudentProfileUpload",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: 1,
        studentId: GBL_STD_ID
    };
    $("#postData").val(JSON.stringify(temp_json));
    $("#addDocument").submit();
}
//END FUNCTION FOR ADD DOCUMENT

//START FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA
function registerCanvasEvent(){

    var bfsendMsg = "Sending data to server..";
    var profileImg = $('#docFile').val();
    $("#addDocument").off().on('submit', (function (e) {

        if( profileImg == "" ){
            toastr.warning('Please select image');
            return false;
        }
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                addRemoveLoader(1);
                // $("#ajaxloader").removeClass("hideajaxLoader");
            },
            success: function (data)// A function to be called if request succeeds
            {
                data = JSON.parse(data);
                console.log(data);

                if (data.status == "Success") {

                    var image = data.data.fileLink;
                    $('#userImg').attr('src',image);
                    GBL_STUD_DATA[0].profileImage = image;

                } else {
                    toastr.success(data.status);
                }
                // $("#ajaxloader").addClass("hideajaxLoader");
                addRemoveLoader(0);
                $('#uploadProfilePop').modal('hide');
                
                $(".fileinput-exists").trigger("click");
                $('#docName').val("");
                $('#docFile').val("");
                $('#docTypeAdd').val("1");
                $("#docType").val("0").selectpicker('refresh');
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                // $("#ajaxloader").addClass("hideajaxLoader");
            }
        });
    }));
}
//END FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA

// DISPLAY SUSPENSION / TERMINATION BLOCK
function setSupTerm(context){

    var suspensionTerminationType = $(context).val();

    if( suspensionTerminationType == "2" ){ // WHILE TERMINATION
        $('.suspensionBlock').hide(300);
        $('#terminationDateDiv').show(300);
    }else{                                  // WHILE SUSPENSION
        $('.suspensionBlock').show(300);
        $('#terminationDateDiv').hide(300);
    }

}

// SET ACADEMIC END DATE AS PER SELECTED TENANCY START DATE
function setAcademicYear(context){
    var startDate = $(context).val();

    $('#academicYearTo').datepicker('remove');
    $("#academicYearTo").datepicker( {
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months",
        startDate: startDate,
        autoclose:true
    });
    $('#academicYearTo').attr('readonly',true);
}

// GET STUDENT'S LEDGER DETAILS
function getLedgerDetails() {
   
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "ledgerHistory",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        studentId : localStorage.indeCampusEnrollDetailStdId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getLedgerDetailsCallback,"Please wait...Adding rooms to your item.");

    function getLedgerDetailsCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data);
            GBL_LEDGER_DATA = data.data;
            renderLedgerDetails();

        }else{

            GBL_LEDGER_DATA = [];
            renderLedgerDetails();
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_LEDGER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER LEDGER
function renderLedgerDetails() {
    
    var thead = '<table class="table table-striped display" id="ledgerTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th class="no-sort">#</th>'+
                        '<th>Date</th>'+
                        '<th class="no-sort">Voucher Type</th>'+
                        '<th class="no-sort">Voucher Number</th>'+
                        '<th class="no-sort">Invoice Name</th>'+
                        '<th class="no-sort">Credit</th>'+
                        '<th class="no-sort">Debit</th>'+
                        '<th class="no-sort">Closing Balance</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 0 ;
    var clBalance = 0 ;
    
    if( GBL_LEDGER_DATA != "No record found" && GBL_LEDGER_DATA != "" ){

        $('#billedAmount').html( '<i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(fixedTo2(GBL_LEDGER_DATA[0].billedAmt)) );
        $('#receiptAmount').html( '<i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(fixedTo2(GBL_LEDGER_DATA[0].receiptAmt)) );
        $('#unbilledAmount').html( '<i class="fa fa-rupee"></i> ' + formatPriceInIndianFormat(fixedTo2(GBL_LEDGER_DATA[0].unbillerAmt)) );
        $('#legderBillAmtDiv').show();

        if( GBL_LEDGER_DATA[0].ledgerData != "No record found" && GBL_LEDGER_DATA[0].ledgerData != "" ){
            // GBL_LEDGER_DATA[0].ledgerData.forEach(function( record , index ){
            $.each(GBL_LEDGER_DATA[0].ledgerData,function (index,record) {
            
                
                // GET PAMENT DETAILS
                var paymentData = record.paymentData ;
                if(record.credit != '-') {
                    var amount =  record.credit ;
                    clBalance = parseFloat(clBalance) + parseFloat(amount) ;
                } else {
                    var amount =  record.debit ;
                    clBalance = (parseFloat(clBalance) - parseFloat(amount)) ;
                }
                var disSym = ' CR' ;
                if(parseFloat(clBalance) < 0){
                    disSym = ' DR' ;
                }
                
                var crVal = Math.abs(record.credit);
                if(isNaN(record.credit)) { crVal = record.credit ; } 
                
                var drVal = Math.abs(record.debit);
                if(isNaN(record.debit)) { drVal = record.debit ; } 
                
                // console.log(record.credit) ;
                // console.log(isNaN(record.credit)) ;
                count = count+1;
                newRow += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td>'+ (record.ledgerDate != "0000-00-00" && record.ledgerDate != "1970-01-01" && record.ledgerDate != "" ? mysqltoDesiredFormat(record.ledgerDate,'dd-MM-yyyy') : "" ) +'</td>'+
                                '<td>'+ record.voucherType +'</td>'+
                                '<td>'+ record.voucherNo +'</td>'+
                                '<td>'+ record.invoiceName +'</td>'+
                                '<td>'+ ( crVal != '-' ? '&#8377; ' + formatPriceInIndianFormat(fixedTo2(crVal)) : '-' ) +'</td>'+
                                '<td>'+ ( drVal != '-' ? '&#8377; ' + formatPriceInIndianFormat(fixedTo2(drVal)) : '-' ) +'</td>'+
                                '<td>&#8377; '+ formatPriceInIndianFormat(fixedTo2(Math.abs(clBalance)))+disSym+'</td>'+
                            '</tr>';
                    
                // if(paymentData != GBL_PAYMENT_ARR) {
                //     paymentData.forEach(function( paymentData , index1 ){
                        
                //         if(paymentData.credit != '-') {
                //             var amount =  paymentData.credit ;
                //             clBalance = parseFloat(clBalance) + parseFloat(amount) ; 
                //         } else {
                //             var amount =  paymentData.debit ;
                //             clBalance = (parseFloat(clBalance) - parseFloat(amount)) ;
                //         }
                //         if(clBalance < 0){
                //             disSym = ' DR' ;
                //         }
                        
                //         var crVal = Math.abs(paymentData.credit).toLocaleString('en') ;
                //         if(isNaN(paymentData.credit)) { crVal = paymentData.credit ; } 

                //         var drVal = Math.abs(paymentData.debit).toLocaleString('en') ;
                //         if(isNaN(paymentData.debit)) { drVal = paymentData.debit ; }
                //         count = count+1;
                //         newRow += '<tr>'+
                //                  '<td>'+ count +'</td>'+
                //                 '<td>'+ (paymentData.ledgerDate != "0000-00-00" && paymentData.ledgerDate != "1970-01-01" && paymentData.ledgerDate != "" ? mysqltoDesiredFormat(paymentData.ledgerDate,'dd-MM-yyyy') : "" ) +'</td>'+
                //                 '<td>'+ paymentData.voucherType +'</td>'+
                //                 '<td>'+ paymentData.voucherNo +'</td>'+
                //              '<td>'+ crVal +'</td>'+
                //              '<td>'+ drVal +'</td>'+
                //                 '<td>'+ Math.abs(clBalance).toLocaleString('en')+disSym+'</td>'+
                //             '</tr>';
                //     });
                // }    
            });
        }
    }
    
    $('#ledgerDatatable').html(thead + newRow + tfoot);
    $('#ledgerTbl').DataTable({

        "ordering": true,
        columnDefs: [{
          orderable: false, // ORDERING ALLOW ONLY ON DATE COLUMN
          targets: "no-sort"
        }]

    });

}
  
//GET STUDENT'S USAGE HISTORY DATA - ADDED ON 14-09-2018 BY VISHAKHA TAK
function getUsageHistory(){  
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "getUsageHistory",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        studentId : localStorage.indeCampusEnrollDetailStdId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getUsageHistoryCallBack,"Please wait...Getting usage history data.");

    // console.log(postdata);return false; 
    function getUsageHistoryCallBack(flag,data){
        
        if(data.status == "Success" && flag){
            
            console.log(data);
            GBL_USAGE_DATA = data.data;
            renderUsageHistory(); // RENDER USAGE HISTORY

        }else{

            GBL_USAGE_DATA = [];
            renderUsageHistory(); // RENDER USAGE HISTORY

            if(flag){
                displayAPIErrorMsg( data.status , GBL_ERR_USAGE_DATA );
                //location.reload();
            }else{
                toastr.warning(SERVERERROR); 
            }
        }
    }
} 

// RENDER NON AVAILING SERVICES LIST DATA
function renderUsageHistory(){

    var tHead = '<table class="table table-striped display" id="usageTbl">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Usage Item Name</th>'+
                            '<th>Date</th>'+
                            '<th class="text-right">Total Monthly Credits</th>'+
                            '<th class="text-right">Used Credits</th>'+
                            '<th class="text-right">Balance Credits</th>'+
                            '<th class="text-right">Excess Usage</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';

    var tEnd = '</tbody></table>';

    var newRow = '';

    if( GBL_USAGE_DATA != "" && GBL_USAGE_DATA != "No record found" ){

        GBL_USAGE_DATA.forEach(function( record , index ){

          
            newRow +=  '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td>'+ mysqltoDesiredFormat(record.startDate,'dd-MM-yyyy') + ' - ' + mysqltoDesiredFormat(record.endDate,'dd-MM-yyyy') +'</td>'+
                            // '<td>'+ record.endDate +'</td>'+
                            '<td class="text-right">'+ record.qty +'</td>'+
                            '<td class="text-right">'+ record.usedQty +'</td>'+
                            '<td class="text-right">'+ record.balanceQty +'</td>'+
                            '<td class="text-right">'+ record.excessQty +'</td>'+
                        '</tr>';       
                                                
        });

    }   
    $('#usageHistoryDiv').html( tHead + newRow + tEnd );
    $('#usageTbl').dataTable();

}