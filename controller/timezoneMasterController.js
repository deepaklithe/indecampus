/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 24-08-2018.
 * File : timezoneMasterController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_TIMEZONE_LIST = []; 
var GBL_TIMEZONE_EDIT_ID = ''; 

// GET TIME ZONE MASTER LISTING DATA
function getTimeZoneMasterData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getTimezoneData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31 , 117),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getTimeZoneMasterDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET TIME ZONE MASTER LISTING DATA CALLBACK
function getTimeZoneMasterDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        GBL_TIMEZONE_LIST = data.data;  
        renderTimezoneData(); 
        addFocusId( 'timezoneName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    } else {
        GBL_TIMEZONE_LIST = [];  
        renderTimezoneData(); 
        if (flag)
            displayAPIErrorMsg( data.status , 'No Timezone Available' );
        else
            toastr.error(SERVERERROR);
    }
}

// RENDER TIMEZONE DATA
function renderTimezoneData(){

   var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Timezone Name</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = ''; 

    //RENDER PROPERTY LISTING
    if(GBL_TIMEZONE_LIST != 'No record found'){
        GBL_TIMEZONE_LIST.forEach( function( record , index ){ 
 
            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.timezoneName +'</td>'+ 
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" timezoneId="'+ record.timezoneId +'"  onclick="updateTimezone(this)"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" timezoneId="'+ record.timezoneId +'" onclick="deleteTimezone(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
        }); 
        
    } 

    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable({ "stateSave": true });
}

// CREATE A NEW TIMZONE
function createTimezone(){

    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(18,73,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){

        var timezoneName = $('#timezoneName').val();

        if( timezoneName == '' ){
            toastr.warning("Please Enter Timezone Name");
            addFocusId('timezoneName');
            return false;

        }else{

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "addTimezone",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(31 ,116),
                timezoneName : timezoneName,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                
            };

            commonAjax(FOLLOWUPURL, postData, createUpdateTimezoneCallback,"Please Wait... Create / Update Property");

            function createUpdateTimezoneCallback(flag,data){

                if (data.status == "Success" && flag) {
                    console.log(data);

                    toastr.success("Timezone Created Successfully");
                    GBL_TIMEZONE_LIST = data.data;
                    GBL_TIMEZONE_EDIT_ID = '';
                    
                    resetData();
                    renderTimezoneData();

                } else {
                    if (flag)
                        displayAPIErrorMsg( data.status , 'Error in create / update timezone master' );
                    else
                        toastr.error(SERVERERROR);
                }
            }
        }
    }
}

// UPDATE NEW TIMZONE
function updateTimezoneData(){

    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(18,75,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){

        var timezoneName = $('#timezoneName').val();

        if( timezoneName == '' ){
            toastr.warning("Please Enter Timezone Name");
            addFocusId('timezoneName');
            return false;

        }else{

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "editTimezone",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(31 ,118),
                timezoneId : GBL_TIMEZONE_EDIT_ID,
                timezoneName : timezoneName,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                
            };

            commonAjax(FOLLOWUPURL, postData, createUpdateTimezoneCallback,"Please Wait... Create / Update Property");

            function createUpdateTimezoneCallback(flag,data){

                if (data.status == "Success" && flag) {
                    console.log(data);

                    toastr.success("Timezone Updated Successfully");
                    GBL_TIMEZONE_LIST = data.data;
                    GBL_TIMEZONE_EDIT_ID = '';
                    
                    resetData();
                    renderTimezoneData();

                } else {
                    if (flag)
                        displayAPIErrorMsg( data.status , 'Error in create / update timezone master' );
                    else
                        toastr.error(SERVERERROR);
                }
            }
        }
    }
}
 
// DELETE SELECTED BRANCH
function deleteTimezone(context){

    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(18 , 76 , 1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    var timezoneId = "";
    if(flag){
        var deleteRow = confirm('Are You sure ? You want to delete this Record');

        if( deleteRow ){

            timezoneId = $(context).attr('timezoneId');
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
 
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "deleteTimezone",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(31 , 119),
                timezoneId : timezoneId,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            };

            commonAjax(FOLLOWUPURL, postData, deleteTimezoneCallback,"Please Wait... Getting Dashboard Detail");
            
            function deleteTimezoneCallback(flag,data){

                if (data.status == "Success" && flag) { 

                    var index = findIndexByKeyValue( GBL_TIMEZONE_LIST,'timezoneId',timezoneId ); 
                    GBL_TIMEZONE_LIST.splice(index,1);

                    toastr.success('Timezone Deleted Successfully');
                    renderTimezoneData(); 
                    
                } else {
                    if (flag)
                        displayAPIErrorMsg( data.status , 'Error in deleting timezone' );
                    else
                        toastr.error(SERVERERROR);
                }
            }
        }
    }
}


// SET SELECTED TIMEZONE DATA TO UPDATE
function updateTimezone(context){
    
    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(5,19,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){ 

        var timezoneId = $(context).attr('timezoneId'); 
        GBL_TIMEZONE_EDIT_ID = timezoneId;
        var index = findIndexByKeyValue( GBL_TIMEZONE_LIST,'timezoneId',timezoneId ); 
        if( index != "-1" ){ 
            $('#timezoneName').val( GBL_TIMEZONE_LIST[index].timezoneName );
            $('#createPropertyBtn').html( '<i class="fa fa-floppy-o"></i> Update');
            $('#createPropertyBtn').attr( 'onclick' , 'updateTimezoneData()');
        }

    } 
} 

// RESET FIELDS
function resetData(){

    $('#timezoneName').val('');
    $('#createPropertyBtn').html( '<i class="fa fa-floppy-o"></i> Save');
    $('#createPropertyBtn').attr( 'onclick' , 'createTimezone()');
    GBL_TIMEZONE_EDIT_ID = "";
} 