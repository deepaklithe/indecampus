/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 02-Jun-2016.
 * File : targetController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var startDate = new Date().format('dd-MM-yyyy');
var endDate = new Date().format('dd-MM-yyyy');
var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
var TARGETJSON = [];
var GBL_TARGETLISTARR = [];
var GBL_USERLISTARR = [];
var GBL_UserListOptionHtml = "";
var GBL_multipleMonthSelHtml = "";
var GBL_USERYEARMONTHDATAARR = [];
var monthView=0;
var GBL_CSVJSON = [];

function setNext10Years(){
    var date = new Date();
    var currYear = date.getFullYear();

    var next10Years = [currYear - 2 , currYear - 1 ];
    for( var i=0; i<10; i++ ){
        next10Years.push( currYear + i );
    }
    // console.log(next10Years);
    
    var yearList = '';
    for( var j=0; j<next10Years.length; j++ ){
        if( currYear == next10Years[j] ){
            yearList += '<option value="'+ next10Years[j] +'" selected> '+ next10Years[j] +' </option>';
        }else{            
            yearList += '<option value="'+ next10Years[j] +'"> '+ next10Years[j] +' </option>';
        }
    }
    $('#ddTargetListYear').html( yearList );
    $('#ddTargetListYear').selectpicker('refresh');
}

//START FUNCTION FOR GETTING CURRENT FISCAL YEAR BY VAIBHAV
function getCurrentFiscalYear() {
    //get current date
    var today = new Date();
    //get current month
    var curMonth = today.getMonth() + 1;
    var fiscalYr = "";
    if (curMonth > 3) { //
        fiscalYr = today.getFullYear().toString();
    } else {
        fiscalYr = (today.getFullYear() - 1).toString();
    }
    $("#idyrspc").val(fiscalYr);
}
//END FUNCTION FOR GETTING CURRENT FISCAL YEAR BY VAIBHAV

//START FUNCTION FOR GET TARGET DATA 
function getTargetData(yearMonthFilter, month, year, group, salesTargetType) {

    var salesTargetType = $('#salesTargetType').val();
    var postData = {
        requestCase: 'getTargetListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(6, 22),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        filter: yearMonthFilter,
        filtervalue: month,
        year: year,
        filterGroupId: group,
        salesTargetType: salesTargetType,
    }
    // console.log(postData);
    // return false;

    function callBackGetGroupListing(flag, data) {
        console.log(data);
        if (data.status == SCS && flag) {
            // TARGETJSON.push(data.data);
            TARGETJSON = [];
            var tmpArr = data.data;
            if (tmpArr.length > 0) {
                TARGETJSON = tmpArr;
                displayTarget();
            }

            addFocusId( 'salesTargetType' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

        }else {
            if (flag){
                TARGETJSON = [];            
                displayTarget();            
                //alert(data.status);                
            }else{
                TARGETJSON = [];            
                displayTarget();
                toastr.error(SERVERERROR);
            }
                
        }

    }

    commonAjax(COMMONURL, postData, callBackGetGroupListing, "Please Wait... Getting the group listing");
}
//END FUNCTION FOR GET TARGET DATA 

function getGroupList(){

    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        targetFlag: 1,
        orgId: checkAuth(13,50),

        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(COMMONURL, postData, callBackGetGroupList, "Please Wait... Getting the group listing");

    function callBackGetGroupList(flag, data) {
        console.log(data);
        if (data.status == SCS && flag) {
            
            var groupList = '<option value="-1">Select Group</option>';
            for( var i=0; i<data.data.length; i++ ){
                groupList += '<option value="'+ data.data[i].PK_ITEM_GROUP_ID +'">'+ data.data[i].GROUP_NAME +'</option>' ;
            }

            $('#groupListing').html(groupList);
            $('#groupListing').selectpicker('refresh');

        }else {
            
            var groupList = '<option value="-1">Select Group</option>';
            $('#groupListing').html(groupList);
            $('#groupListing').selectpicker('refresh');
            
            if (flag){
                // displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );            
            }else{
                toastr.error(SERVERERROR);
            }    
        }
    }

}

//getTargetData("1","2016");

function displayTarget() {
    var html = "";
    //if (TARGETJSON.length > 0) {
        
        var salesTargetType = $('#salesTargetType').val();

        var amtType = "";
        if( salesTargetType == "1" ){
            amtType = "(in Lakhs)";
        }else{
            amtType = "(in Qty)";
        }

        var startHtml = "";
        // if( monthView == 1 ){
        if( monthView == 1 || salesTargetType == "2" ){
            startHtml ="<table class='display datatables-alphabet-sorting'>"+
                            "<thead>"+
                                "<tr>"+
                                    "<th>Employee</th>"+
                                    "<th>Group</th>"+
                                    "<th>Monthly Target "+ amtType +"</th>"+
                                    "<th>Monthly Achieved "+ amtType +"</th>"+
                                "</tr>"+
                            "</thead><tbody>";
        }else{
            startHtml ="<table class='display datatables-alphabet-sorting'>"+
                            "<thead>"+
                                "<tr>"+
                                    "<th>Employee</th>"+
                                    "<th>Group</th>"+
                                    "<th>Yearly Target "+ amtType +"</th>"+
                                    "<th>Yearly Achieved "+ amtType +"</th>"+
                                "</tr>"+
                            "</thead><tbody>";
        }
        
                    
        var html= "";
        var yearlyTot = 0;
        var yearlyTarget = 0;
        var monthlyTarget = 0;
        var monthlyTot = 0;

        GBL_CSVJSON = [];

        if( monthView == 1 || salesTargetType == "2" ){
            var tempCSV = { // HEADER FOR PDF AND CSV
                fullName : "Full Name",
                GROUP_NAME : "Group Name",
                MONTH_NAME : "Month Name",
                YEAR_TARGET : "Target Year",
                TARGET_MONTH : "Monthly Target",
                MONTHLY_ACHIEVED : "Monthly Achieved",
            };
            GBL_CSVJSON.push(tempCSV);

        }else{

            var tempCSV = { // HEADER FOR PDF AND CSV
                fullName : "Full Name",
                GROUP_NAME : "Group Name",
                YEAR_TARGET : "Target Year",
                TARGET_YEAR : "Yearly Target",
                YEARLY_ACHIEVED : "Yearly Achieved",
            };
            GBL_CSVJSON.push(tempCSV);
        }   

        console.log(GBL_CSVJSON);

        var monthlyTot = 0;
        var monthlyTarget = 0;
        for (var i = 0; i < TARGETJSON.length; i++) {

            //var empName = TARGETJSON[i]['USERNAME']; COMMENTED BY DEEPAK
            var empName = TARGETJSON[i]['userName']; // ADDED BY DEEPAK
            var departMent = TARGETJSON[i]['groupName'];
            var targetYear = TARGETJSON[i]['year'];

            var target = parseFloat(TARGETJSON[i]['target']);
            var achivememt = parseFloat(TARGETJSON[i]['achivememt']);

            monthlyTarget = monthlyTarget + target;
            monthlyTot = monthlyTot + achivememt;

            if( monthView == 1 || salesTargetType == "2" ){

                var targetAmt = target;
                var achivememtAmt = achivememt;
                if( salesTargetType == "1" ){
                    targetAmt = numberFormat( parseFloat( target/100000 ).toFixed(2) );
                    achivememtAmt = numberFormat( parseFloat( achivememt/100000 ).toFixed(2) );
                }

                html += "<tr>" +
                        "<td>" + empName + "</td>" +
                        "<td>" + departMent + "</td>" +
                        "<td class='amtDisp'>" + targetAmt + "</td>" +
                        "<td class='amtDisp'>" + achivememtAmt  + "</td>"+
                        "</tr>";

                var tempJSON = { // HEADER FOR PDF AND CSV
                    fullName : empName,
                    GROUP_NAME : departMent,
                    MONTH_NAME : $('#ddTargetListMonth option:selected').text(),
                    YEAR_TARGET : targetYear,
                    TARGET_MONTH : target,
                    MONTHLY_ACHIEVED : achivememt,
                };
                GBL_CSVJSON.push( tempJSON );

            }else{

                var targetAmt = target;
                var achivememtAmt = achivememt;
                if( salesTargetType == "1" ){
                    targetAmt = numberFormat( parseFloat( target/100000 ).toFixed(2) );
                    achivememtAmt = numberFormat( parseFloat( achivememt/100000 ).toFixed(2) );
                }

                html += "<tr>" +
                        "<td>" + empName + "</td>" +
                        "<td>" + departMent + "</td>" +
                        "<td class='amtDisp'>" + targetAmt + "</td>" +
                        "<td class='amtDisp'>" + achivememtAmt + "</td>" +
                        "</tr>";

                var tempJSON = { // HEADER FOR PDF AND CSV
                    fullName : empName,
                    GROUP_NAME : departMent,
                    MONTH_NAME : $('#ddTargetListMonth option:selected').text(),
                    YEAR_TARGET : targetYear,
                    TARGET_YEAR : target,
                    YEARLY_ACHIEVED : achivememt,
                };
                GBL_CSVJSON.push( tempJSON );

            }

            var MONTH_NAME = $('#ddTargetListMonth option:selected').text();
            if( MONTH_NAME == 'Month' ){
                MONTH_NAME = "-";
            }                    
        }
        
        var percentage = ((monthlyTot != 0) ? monthlyTot*100/monthlyTarget : 0).toFixed(2);
        
        log("percentage ->"+percentage);
        if(percentage == NaN || percentage ==undefined || percentage==""){
            percentage = 0;
        }
        $("#progressbardis").attr("style","width:"+percentage+"%");
        $("#progressbardis .sr-onlyInq").html(percentage+"%");
        var endHtml = "</tbody></table>"; 
        var finalHtml = startHtml + html + endHtml;
        $("#targetListing").html(finalHtml);
        TablesDataTables.init();
        TablesDataTablesEditor.init();
    //}
}


//START FUNCTION FOR GET TARGET DATA 
function getLevel10UserList() {

    var postData = {
        requestCase: 'getLevel10UserList',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(19, 74),
        activeInactiveFlag : "1",
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    function callBackGetLevel10UserList(flag, data) {
        console.log(data);
        if (data.status == SCS && flag) {

            if (data.data)
            {
                GBL_USERLISTARR = data.data;
            }

            generateUserEmpListDropDown();
        }
        else {
            
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );  
            else
                toastr.error(SERVERERROR);
        }

    }

    commonAjax(COMMONURL, postData, callBackGetLevel10UserList, "Please Wait... Getting the level 10 user listing");
}
//END FUNCTION FOR GET TARGET DATA 

function registerAllTargetListEvents()
{
    /* element selector defination START */
    var $ddTargetListMonth = $('#ddTargetListMonth');
    var $ddGroupList = $('#groupListing');
    var $ddTargetListYear = $('#ddTargetListYear');
    var $divMultipleMonthsSelWrapper = $('#divMultipleMonthsSelWrapper');
    var $btnshowCreateTargetModal = $('#btnshowCreateTargetModal');
    var $btnAddMonthsSelDiv = $('#btnAddMonthsSelDiv');
    var $ddEmpList = $('#ddEmpList');
    var $targetType = $('#targetTypeFor');
    var $ddCreateTargetYear = $('#ddCreateTargetYear');
    var $txtCreateTargetYearAmount = $('#txtCreateTargetYearAmount');
    var $btnSaveCreateTargetModal = $('#btnSaveCreateTargetModal');
    var $createTargetModal = $('#createTarget');
    /* element selector defination END */

    var userMonthData = [];
    var userYearData = [];



    /* target listing related events START */
    var date = new Date();
    var currYear = date.getFullYear();
    var defaultTargetYear = currYear;
    var defaultTargetMonth = "";
    var defaultTargetGroup = "";
    var yearMonthFilter = 1; // filter =1 = yearly , 2 = month


    $($ddTargetListMonth).selectpicker();
    $($ddTargetListYear).selectpicker();

    var salesTargetType = $('#salesTargetType').val();

    $($ddTargetListYear).val(defaultTargetYear);
    $($ddTargetListMonth).val(defaultTargetMonth);
    $($ddGroupList).val(defaultTargetGroup);

    getTargetData(yearMonthFilter, defaultTargetMonth, defaultTargetYear, salesTargetType);


    /* target listing related events END */




    /* multiple months selection div related events START */
    GBL_multipleMonthSelHtml = $($divMultipleMonthsSelWrapper).html(); // get the template html to use it further.

    //$btnAddMonthsSelDiv

    $($btnAddMonthsSelDiv).click(function () {

        $($divMultipleMonthsSelWrapper).append(GBL_multipleMonthSelHtml);

    });


    /* multiple months selection div related events END */


    /* btn click event to show create target modal START */
    $($btnshowCreateTargetModal).click(function () {

        showCreateTargetModal(this);

    });
    /* btn click event to show create target modal END */

    /* get user's target details by userId on change event START */
    $($ddEmpList).change(function () {
        
        //console.log(this.value);
        var empId = this.value;
        resetCreateTargetModal();

        if (empId == "-1" || !empId)
        {
            //alert("Please select the Empolyee");
            return false;
        }


        getTargetDetailsByUserId(empId, initUserYearMonthData);

    });
    /* get user's target details by userId on change event END */


    /* get user's target details by userId on change event START */
    $($ddCreateTargetYear).change(function () {

        //console.log(this.value);
        var createTargetYear = this.value;
        var empId = $($ddEmpList).val();

        if (empId == "-1" || !empId)
        {
            //alert("Please select the Empolyee first");
            return false;

        }

        if (createTargetYear == "-1" || !createTargetYear)
        {
            //alert("Please select the year first");
            return false;
        }

        renderUserYearMonthData();

    });
    /* get user's target details by userId on change event END */


    /* btn click event to show create target modal START */
    $($btnSaveCreateTargetModal).click(function () {
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        if( tmp_json.data[0].PK_USER_ID == "-1" ){
            toastr.error('Master User is not Authorised for this Activity');
            return false;
        }
        saveCreateTargetModal();

    });
    /* btn click event to show create target modal END */



    /* function registaration START */

    function showCreateTargetModal(elem)
    {
        resetCreateTargetModal();
        openModal(elem);
    }

    function resetCreateTargetModal()
    {
        $($divMultipleMonthsSelWrapper).html(GBL_multipleMonthSelHtml);
        $($ddCreateTargetYear).val("-1");
        $($txtCreateTargetYearAmount).val("");
        GBL_USERYEARMONTHDATAARR = [];

    }

    function initUserYearMonthData()
    {
        ////GBL_USERYEARMONTHDATAARR

        console.log("initUserYearMonthData");

        userMonthData = [];
        userYearData = [];

        if (GBL_USERYEARMONTHDATAARR.monthData)
        {
            userMonthData = GBL_USERYEARMONTHDATAARR.monthData;
        }

        if (GBL_USERYEARMONTHDATAARR.yearData)
        {
            userYearData = GBL_USERYEARMONTHDATAARR.yearData;
        }


        renderUserYearMonthData();

    }

    function renderUserYearMonthData()
    {
        console.log("renderUserYearMonthData");
        //GBL_USERYEARMONTHDATAARR

        function renderUserYearData()
        {
            var targetYearAmt = userYearData[yearIndex].yearAmt;
            var targetType = userYearData[yearIndex].typeOfTarget;
            $($txtCreateTargetYearAmount).val(targetYearAmt);
            $($targetType).val(targetType);
        }

        function renderUserMonthData()
        {
            var selMonthArr = [];
            var multipleMonthHtml = "";
            if(userMonthData != "No record found"){
                for (var i = 0; i < userMonthData.length; i++)
                {
                    var year = userMonthData[i].year;

                    if (year == createTargetYear)
                    {
                        multipleMonthHtml += GBL_multipleMonthSelHtml;
                        selMonthArr.push(userMonthData[i]);
                    }

                }
                $($divMultipleMonthsSelWrapper).html(multipleMonthHtml);
            }
            

            //data-multipleMonthTargetSel

            var $multipleMonthInnerDivs = $($divMultipleMonthsSelWrapper).find('[data-multipleMonthTargetSel]');


            $($multipleMonthInnerDivs).each(function (index) {

                //console.log(index);
                if (selMonthArr[index])
                {
                    var pkTargetId = selMonthArr[index].pkTargetId;
                    var month = selMonthArr[index].month;
                    var monthAmt = selMonthArr[index].monthAmt;
                    
                    var $ddMultipleTagetMonth = $(this).find('#ddMultipleTagetMonth');
                    var $txtCreateTargetMonthAmount = $(this).find('#txtCreateTargetMonthAmount');

                    $($ddMultipleTagetMonth).val(month);
                    $($txtCreateTargetMonthAmount).val(monthAmt);
                    $($txtCreateTargetMonthAmount).attr('data-targetid', pkTargetId);


                }

            });

            $($divMultipleMonthsSelWrapper).append(GBL_multipleMonthSelHtml);//add empty row of month/amt to add new entry

        }

        var createTargetYear = $($ddCreateTargetYear).val();
        var empId = $($ddEmpList).val();

        if (!createTargetYear || createTargetYear == "-1")
        {
            return false;
        }

        if (!empId || empId == "-1")
        {
            return false;
        }

        var yearIndex = findIndexByKeyValue(userYearData, "year", createTargetYear);
        if (yearIndex == -1)
        {
            console.log("No year data found for year :->  " + createTargetYear + " and empId :-> " + empId);
            $($txtCreateTargetYearAmount).val("");
            $($targetType).val("INQUIRY");
        }
        else
        {
            renderUserYearData();
        }

        var monthIndex = findIndexByKeyValue(userMonthData, "year", createTargetYear);
        if (monthIndex == -1)
        {
            console.log("No month data found for year :->  " + createTargetYear + " and empId :-> " + empId);
            $($divMultipleMonthsSelWrapper).html(GBL_multipleMonthSelHtml);
        }
        else
        {
            renderUserMonthData();
        }




    }

    function saveCreateTargetModal()
    {
        
        function submitCreateTargetDataForApi()
        {
            
            var postData = {
                requestCase: 'addUpdateTarget',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(6, 21),
                dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                empId: empId,
                targetYear:createTargetYear,
                targetYearAmt:createTargetYearAmt,
                monthTargetArr:monthTargetArr,
                targetType:targetType,
            }

            function callBackCreateTarget(flag, data) {
                console.log(data);
                if (data.status == "Success" && flag) {

                    $($createTargetModal).modal('hide');
                    // displayTarget();

                    var targetMonth = $($ddTargetListMonth).val();
                    var targetYear = $($ddTargetListYear).val();
                    var yearMonthFilter = 1; // filter =1 = yearly , 2 = month
                    var salesTargetType = $('#salesTargetType').val();
                    getTargetData(yearMonthFilter, targetMonth, targetYear, salesTargetType);

                }
                else {
                    if (flag)
                        displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_TARGET_FAIL );  
                    else
                        toastr.error(SERVERERROR);
                }


            }

            commonAjax(COMMONURL, postData, callBackCreateTarget, "Please Wait... Getting user details");
            
        }
        
        
        
        
        var empId = $($ddEmpList).val();
        var createTargetYear = $($ddCreateTargetYear).val();
        var createTargetYearAmt = $($txtCreateTargetYearAmount).val();
        var targetType = $($targetType).val();
        var monthTargetArr = [];

        if (!empId || empId == "-1")
        {
            toastr.warning("Please select an employee");
            return false;
        }
        else if (!createTargetYear || createTargetYear == "-1")
        {
            toastr.warning("Please select the target year");
            return false;
        }
        else if(!targetType){
            toastr.warning("Please select the target for.");
            $($targetType).focus();
            return false;
        }
        else if (!createTargetYearAmt)
        {
            toastr.warning("Please enter the target year amount");
            $($txtCreateTargetYearAmount).focus();
            return false;
        }
        else if (isNaN(createTargetYearAmt))
        {
            toastr.warning("Please enter the valid value for target year amount");
            $($txtCreateTargetYearAmount).focus();
            return false;
        }
        else if (createTargetYearAmt.charAt(0) == ".") {

            toastr.warning("Please enter the valid value for target year amount");
            $($txtCreateTargetYearAmount).focus();
            return false;
        }

        /* create monthTargetArr START  */
        var $multipleMonthInnerDivs = $($divMultipleMonthsSelWrapper).find('[data-multipleMonthTargetSel]');

        var proceed = true;
        $($multipleMonthInnerDivs).each(function (index) {

            //console.log(index);

            var $ddMultipleTagetMonth = $(this).find('#ddMultipleTagetMonth');
            var $txtCreateTargetMonthAmount = $(this).find('#txtCreateTargetMonthAmount');

            var month = $($ddMultipleTagetMonth).val();
            var monthAmt = $($txtCreateTargetMonthAmount).val();
            var pkTargetId = $($txtCreateTargetMonthAmount).attr('data-targetid');

            if (pkTargetId != "0" && !monthAmt)
            { // if user clears the populated/existing month data
                toastr.warning("Please fill up the existing month amount fields");
                $($txtCreateTargetMonthAmount).focus();
                proceed = false;
                return false;
            }

            if (monthAmt)
            {
                if(!month || month == "-1")
                {
                    toastr.warning("Please select the month of the row in which you have entered the month target amount");
                    $($ddMultipleTagetMonth).focus();
                    proceed = false;
                    return false;
                    
                }
                else if (isNaN(monthAmt))
                {
                    toastr.warning("Please enter the valid value for target month amount");
                    $($txtCreateTargetMonthAmount).focus();
                    proceed = false;
                    return false;
                }
                else if (monthAmt.charAt(0) == ".") {

                    toastr.warning("Please enter the valid value for target month amount");
                    $($txtCreateTargetMonthAmount).focus();
                    proceed = false;
                    return false;
                }
                
                var monthjson = {
                    monthId:month,
                    monthAmount:monthAmt,
                    PK_TARGET_ID:pkTargetId
                };
                
                monthTargetArr.push(monthjson);

            }




        });
        
        if(proceed)
        {
            console.log("validation passed");
            console.log(monthTargetArr);
            submitCreateTargetDataForApi();
        }
        else
        {
            console.log("validation didn't pass");
            return false;
        }

        



    }

    /* function registaration END */

}


function generateUserEmpListDropDown()
{
    /* element selector defination START */
    var $ddEmpList = $('#ddEmpList');
    /* element selector defination END */

    var userListArr = GBL_USERLISTARR;
    var optionHtml = '<option value="-1">Select Employee</option>';

    for (var i = 0; i < userListArr.length; i++)
    {
        var userName = userListArr[i].USERNAME;
        var userId = userListArr[i].PK_USER_ID;
        optionHtml += '<option value="' + userId + '">' + userName + '</option>';
    }

    GBL_UserListOptionHtml = optionHtml;


    $($ddEmpList).html(GBL_UserListOptionHtml);


}

function getTargetDetailsByUserId(empId, callBack)
{
    GBL_USERYEARMONTHDATAARR = [];

    if (!callBack)
    {
        console.error("callBack function of getTargetDetailsByUserId not found in the parameter ");
        return false;
    }


    var postData = {
        requestCase: 'getTargetDetailsByUserId',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(6, 22),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        empId: empId,
    }

    function callBackGetTargetDetailsByUserId(flag, data) {
        console.log(data);
        if (data.status == "Success" && flag) {

            if (data.data)
            {
                GBL_USERYEARMONTHDATAARR = data.data;


            }

        }
        else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );  
            else
                toastr.error(SERVERERROR);
        }

        callBack();

    }

    commonAjax(COMMONURL, postData, callBackGetTargetDetailsByUserId, "Please Wait... Getting user details");

}

function generateJson(){
    
    if(checkAuth(28,117,1) == -1){
         toastr.error( NOAUTH );
    }else{
        // GBL_CSVJSON = [];
        // var tempCSV = { // HEADER FOR PDF AND CSV
        //     fullName : "Full Name",
        //     USERNAME : "User Name",
        //     CONTACT_NO : "Mobile No.",
        //     GROUP_NAME : "Group Name",
        //     MONTH_NAME : "Month Name",
        //     TARGET_MONTH : "Monthly Target",
        //     MONTHLY_ACHIEVED : "Monthly Achieved",
        //     TARGET_YEAR : "Yearly Target",
        //     YEARLY_ACHIEVED : "Yearly Achieved",
        //     ADDRESS : "Address"
        // };
        // GBL_CSVJSON.push(tempCSV);


        // for(var i=0;i<TARGETJSON.length;i++){

        //     var MONTH_NAME = $('#ddTargetListMonth option:selected').text();
        //     if( MONTH_NAME == 'Month' ){
        //         MONTH_NAME = "-";
        //     }

        //     var monthAchived = TARGETJSON[i].MONTHLY_ACHIEVED;
        //     var monthTarget = TARGETJSON[i].TARGET_MONTH;
        //     var yearlyTrgt = TARGETJSON[i].TARGET_YEAR;
        //     var yearlyAchvd = TARGETJSON[i].YEARLY_ACHIEVED;

        //     if( monthAchived == "" ){
        //         monthAchived = 0;
        //     }
        //     if( yearlyTrgt == "" ){
        //         yearlyTrgt = 0;
        //     }
        //     if( yearlyAchvd == "" ){
        //         yearlyAchvd = 0;
        //     }

        //     var groupName = $('#groupListing option[value="'+ TARGETJSON[i].FK_GROUP_ID +'"]').text();

        //     if( TARGETJSON[i].DEPARTMENT == "" ){
        //         groupName = "";
        //     }

        //     var tempJSON = { // HEADER FOR PDF AND CSV
        //         fullName : TARGETJSON[i].FULLNAME,
        //         USERNAME : TARGETJSON[i].USERNAME,
        //         CONTACT_NO : TARGETJSON[i].CONTACT_NO,
        //         GROUP_NAME : groupName,
        //         MONTH_NAME : MONTH_NAME,
        //         MONTHLY_TARGET : monthTarget,
        //         MONTHLY_ACHIEVED : monthAchived,
        //         TARGET_YEAR : yearlyTrgt,
        //         YEARLY_ACHIEVED : yearlyAchvd,
        //         ADDRESS : TARGETJSON[i].ADDRESS
        //     };
        //     GBL_CSVJSON.push( tempJSON );
        // }
        // console.log(GBL_CSVJSON);
        // return false;
        downLoadPDF();
    }
}

function downLoadPDF(){
    
    if( GBL_CSVJSON.length == 1 ){

        toastr.error('No data for Download');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Target_Report.pdf",20);
    }
}

function getTragetOnSumbit(){
    var groupId = ($("#groupListing").val() != "-1") ? $("#groupListing").val() : "";
    var monthId = $("#ddTargetListMonth").val();
    var yearId = $("#ddTargetListYear").val();
    var salesTargetType = $("#salesTargetType").val();
    var yearMonthFilter = 1;
    if(monthId.length != undefined && (monthId.length != "" && monthId.length != "0")){
        monthView = 1;
    }else{
        monthView = 0;
    }

//    var groupIdCs = "";
//    if( groupId &&  groupId != "-1"){
//        for( var j=0; j<groupId.length; j++ ){
//            if( groupIdCs ){
//                groupIdCs += ','+groupId[j];
//            }else{
//                groupIdCs = groupId[j];
//            }
//        }
//    }
    
   getTargetData(yearMonthFilter, monthId, yearId, groupId, salesTargetType);
}