/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : userManagePermisionController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var permissionData = [];

function getModuleData(){

    $('#modules').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getModuleListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2,6),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getModuleDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getModuleDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        var options = '';

        for( var i=0; i<data.data.length; i++ ){

            options += '<option value="' + data.data[i].moduleId + '" >' + data.data[i].moduleName + '</option>';
        }

        $('#modules').html(options);
        $('#modules').selectpicker();

        getModulePermissionList();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_MODULE );
        else
            toastr.error(SERVERERROR);
    }
}

      
function createModulePermission(){
    
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(2,5,1) == -1){
            toastr.error( NOAUTH );
            return false;
        }
    }
    
    var permissionName = $('#permissionName').val().trim();
    var moduleId = $('#modules').val();

    if( permissionName.length == 0 ){
        toastr.warning('Please Enter Permission Name');
        $('#permissionName').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'createAvtivity',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3,9),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            moduleId : moduleId,
            activityName : permissionName
        }

        commonAjax(FOLLOWUPURL, postData, createModulePermissionCallback,"Please Wait... Getting Module Detail");
    }
}


function createModulePermissionCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('New Activity has been Created');
        console.log(data); 

        var activityName = $('#permissionName').val().trim();
        var moduleName = $('#modules option:selected').text();

        var tempData = {
            activityId: data.data.activityId,
            activityName: activityName,
            moduleId: data.data.moduleId,
            moduleName: moduleName
        }

        permissionData.push( tempData ); 

        renderPermission(); 

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_PERMISSION );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteModulePermission(context){
    
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(22,88,1) == -1){
            toastr.error( NOAUTH );
            return false;
        }
    }
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        var moduleId = $(context).attr('module-id');
        var activityName = $(context).attr('activity-name');
        var activityId = $(context).attr('activity-id');

        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteAvtivity',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3,12),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID), 
            activityId : activityId
        }

        commonAjax(FOLLOWUPURL, postData, deleteModulePermissionCallback,"Please Wait... Getting Module Detail");
    }
}


function deleteModulePermissionCallback(flag,data){

    if (data.status == "Success" && flag) {

        $(gblContext).parents('td').parents('tr').remove(); 
        toastr.success('Module Permission has been Deleted');
        console.log(data); 
       // location.reload();  

       var moduleId = $(gblContext).attr('module-id');
       var activityId = $(gblContext).attr('activity-id');

       var index = findIndexByKeyValueAndCond( permissionData,'moduleId','activity-id',moduleId,activityId );

        permissionData.splice(index,1);

        renderPermission(); 

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_PERMISSION );
        else
            toastr.error(SERVERERROR);
    }
}

function getModulePermissionList(){

     var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getPermissionListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,10),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getModulePermissionListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getModulePermissionListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        permissionData = data.data;

        renderPermission(); 

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_PERMISSION_LIST );
        else
            toastr.error(SERVERERROR);
    }
}

function renderPermission(){
    
    addFocusId( 'modules' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Name</th>'+
                        '<th>Module</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    if(permissionData != 'No record found'){
        
        for( var i=0; i<permissionData.length; i++ ){

            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ permissionData[i].activityName +'</td>'+
                            '<td>'+ permissionData[i].moduleName +'</td>'+
                            // '<td>'+ data.data[i].moduleName +'</td>'+
                            '<td><a href="javascript:void(0)" activity-name="'+ permissionData[i].activityName +'" activity-Id="'+ permissionData[i].activityId +'" module-id="'+ permissionData[i].moduleId +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" onclick="updatePermission(this)"><i class="fa fa-pencil"></i></a>'+
                            '<a href="javascript:void(0)" activity-name="'+ permissionData[i].activityName +'" activity-Id="'+ permissionData[i].activityId +'" module-id="'+ permissionData[i].moduleId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="deleteModulePermission(this)"><i class="fa fa-trash-o"></i></a></td>'+
                        '</tr>';
        }

        $('#dtTable').html(thead + newRow + tfoot);

    }


    TablesDataTables.init();
    TablesDataTablesEditor.init();
}

function updatePermission(context){

    
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(22,87,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            return false;
        }
    }
    
    localStorage.POTGActivityIdForUpdate = "";

    var activityId = $(context).attr('activity-id');
    var moduleId = $(context).attr('module-id');
    var activityName = $(context).attr('activity-name');

    localStorage.POTGActivityIdForUpdate = activityId;

    $('#permissionBtn').attr('onclick','updateModulePermission()');

    $('#permissionName').val(activityName);
    $('#modules').val(moduleId);
    addFocusId( 'modules' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var moduleIdForUpdate = '';
function updateModulePermission(context){

    moduleIdForUpdate = $('#modules').val();
    var activityName = $('#permissionName').val();
    var activityId = localStorage.POTGActivityIdForUpdate;

    gblContext = context;

    if( activityName == ""){
        toastr.warning('Please Enter activity Name');
        $('#permissionName').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateAvtivity',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3,11),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            moduleId : moduleIdForUpdate,
            activityName : activityName,
            activityId : activityId,
        }
        // console.log(postData); return false;
        commonAjax(FOLLOWUPURL, postData, updateModulePermissionCallback,"Please Wait... Getting Module Detail");
    }
}


function updateModulePermissionCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('Module Permission has been Updated');
        console.log(data); 

        var activityName = $('#permissionName').val();
        var moduleName = $('#modules option:selected').text();

        var index = findIndexByKeyValueAndCond( permissionData,'moduleId','activityId',moduleIdForUpdate,localStorage.POTGActivityIdForUpdate );

        var tempArray = {
            activityId: localStorage.POTGActivityIdForUpdate,
            activityName: activityName,
            moduleId: moduleIdForUpdate,
            moduleName: moduleName
        }

        permissionData[index] = tempArray;

        renderPermission();

        $('#permissionBtn').attr('onclick','createModulePermission()'); 

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_PERMISSION );
        else
            toastr.error(SERVERERROR);
    }
}


function resetData(){

    $('#permissionName').val('');
    $('#permissionBtn').attr('onclick','createModulePermission()');
}