/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : clientManagementController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
var CLIENTJSON = [];
var newClientId = "";

var GBL_BRANCH_LIST = [];
var GBL_ULEVEL_LIST = [];
var GBL_USER_CREATE = false;

//START FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO CLIENTJSON
function callBackGetClientMgmtListing(flag, data) {
    if (data.status == SCS && flag) {
        if (data.data) {

            var popOverContent = "<u>Provides syncing of Following Modules </u></br><ul>";
            
            CLIENTJSON = data.data;
            for (var i = 0; i < CLIENTJSON.length; i++) {

                var clientId = CLIENTJSON[i].clientId;
                var clientName = CLIENTJSON[i].clientName;
                var clientEmail = CLIENTJSON[i].clientEmail;
                var clientAddress = CLIENTJSON[i].clientAddress;
                var clientContact = CLIENTJSON[i].clientContact;
                // var clientType = CLIENTJSON[i].clientType;
                var webAddress = CLIENTJSON[i].webAddress;
                var clientValidity = CLIENTJSON[i].clientValidity;

                // var clientTypeTxt = "";

                var data = JSON.parse(localStorage.indeCampusRoleAuth);

                // var index = findIndexByKeyValue(data.data.clientType,'ID',clientType);

                CLIENTJSON[i].clientId = clientId;
                CLIENTJSON[i].clientName = clientName;
                CLIENTJSON[i].clientEmail = clientEmail;
                CLIENTJSON[i].clientAddress = clientAddress;
                CLIENTJSON[i].clientContact = clientContact;
                // CLIENTJSON[i].clientType = clientType;
                CLIENTJSON[i].webAddress = webAddress;
                CLIENTJSON[i].clientValidity = clientValidity;
                // CLIENTJSON[i].clientTypeTxt = ((index != undefined && index != -1) ? data.data.clientType[index].VALUE : 'Master User');

                if( CLIENTJSON[0].masterSmsFlag == "1" ){
                    $('#masterSmsFlag').prop('checked',true);
                }
                if( CLIENTJSON[0].masterEmailFlag == "1" ){
                    $('#masterEmailFlag').prop('checked',true);
                }
                if( CLIENTJSON[0].developerFlag == "1" ){
                    $('#developerFlag').prop('checked',true);
                }
                if( CLIENTJSON[0].orderEposFlag == "1" ){ // ORDER TO EPOS FLAG - ADDED ON 21-08-2018 BY VISHAKHA TAK
                    $('#orderEposFlag').prop('checked',true);
                }
                if( CLIENTJSON[0].tallyFlag == "1" ){
                    $('#tallyFlag').prop('checked',true); 
                    $('#tallyIdDiv').show();
                    $('#tallyId').val( CLIENTJSON[0].tallyFlagId );
                }else{
                    $('#tallyIdDiv').hide();
                    $('#tallyId').val( '' );
                }
                $('#eposClientId').val( ( CLIENTJSON[0].eposClientId != "0" ?  CLIENTJSON[0].eposClientId : "" ) ); // ORDER TO EPOS CLIENT ID - ADDED ON 05-09-2018 BY VISHAKHA TAK
                $('#fnbSpaceId').val( ( CLIENTJSON[0].fnbSpaceId != "0" ? CLIENTJSON[0].fnbSpaceId : "" ) ); // EPOS FNB SPACE ID - ADDED ON 05-09-2018 BY VISHAKHA TAK
                $('#laundrySpaceId').val( ( CLIENTJSON[0].fnbLaundryId != "0" ? CLIENTJSON[0].fnbLaundryId : "" ) ); // EPOS LAUNDRY SPACE ID - ADDED ON 05-09-2018 BY VISHAKHA TAK
                $('#eposUrl').val( CLIENTJSON[0].eposUrl ); // EPOS URL - ADDED ON 05-09-2018 BY VISHAKHA TAK
                
                if( CLIENTJSON[0].apiLogFlag == "1" ){ // API LOG FLAG - ADDED ON 06-09-2018 BY VISHAKHA TAK
                    $('#apiLogFlag').prop('checked',true);
                }
            }
        }
    }else {
        if (flag){
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT );
        }else{
            toastr.error(SERVERERROR);
        }
    }
}
//END FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO CLIENTJSON


//START FUNCTION FOR GET CLIENT DETAILS BY ARRAY INDEX AND ALL DATA
function getClientDetails(){
    if(checkAuth(4,13,1) == -1){
        toastr.error('You are not authorised to perform this activity');
    }else{

        CLIENTJSON = JSON.parse( localStorage.indeCampusClientData );

        // var arrIndex = $(clientData).attr("data-arrIndex");
        var clientId = CLIENTJSON['clientId'];
        var clientName = CLIENTJSON['clientName'];
        var clientEmail = CLIENTJSON['clientEmail'];
        var clientAddress = CLIENTJSON['clientAddress'];
        var clientContact = CLIENTJSON['clientContact'];    
        var customerLevel = CLIENTJSON['customerLevel'];  
        var customerAccess = CLIENTJSON['customerAccess'];  
        var webAddress = CLIENTJSON['webAddress'];  
        var clientValidity = CLIENTJSON['clientValidity'];  

        clientIdNew = clientId;
        newClientId = clientIdNew;
        
        // $("#arrIndex").val(arrIndex);
        $("#clientId").val(clientId);
        $("#clientName").val(clientName);
        $("#clientEmail").val(clientEmail);
        $("#clientAddress").val(clientAddress);
//        $("#clientContact").val(clientContact);
        
        setTimeout(function(){
            setNumberInIntl( 'contactNum' , clientContact );
        },50) 
        // $("#clientType").val(clientType);    
        $("#webAddress").val(webAddress);    
        $("#txtClientValidity").val( mysqltoDesiredFormat( clientValidity , 'dd-MM-yyyy' ) );    
        $('input:radio[name=rdoInqMode][value='+customerLevel+']').prop('checked', true);   
        // $("#clientType").selectpicker('refresh');    

        if( customerLevel == "1" ){
            $('#custAccLab').show();
        }
        if( customerAccess != "0" ){
            $('#customerAccessChk').prop('checked',true);
        }

        $('#nextBtn').show();
    }

    addFocusId( 'clientName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    getClientListData(); //CALL FUNCTION FOR DISPLAY DATA
    
}
//END FUNCTION FOR GET CLIENT DETAILS BY ARRAY INDEX AND ALL DATA

//START FUNCTION FOR CALL API FOR CLIENT LISTING
function getClientListData() {

    $('#clientName').focus();
    
    var postData = {
        requestCase: 'getClientListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        dataValue: generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID)
    }
    if( localStorage.indeCampusClientCreateType == "1" ){
        commonAjax(COMMONURL, postData, callBackGetClientMgmtListing, GETTINGCLIENT);
    }
}
//END FUNCTION FOR CALL API FOR CLIENT LISTING

//START FUNCTION FOR CREATE CLIENT 
function createClient() {

    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(4,13,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            return false;
        }
    }
    var arrIndex = $("#arrIndex").val().trim(); //DEVICE NAME
    var clientId = $("#clientId").val().trim(); //DEVICE NAME
    var clientNameadd = $("#clientName").val().trim(); //DEVICE NAME
    var clientEmailadd = $("#clientEmail").val().trim(); //DEVICE TYPE
    var clientAddressadd = $("#clientAddress").val().trim(); //DEVICE TYPE
    var clientContactadd = getIntlMobileNum("contactNum");    // GET MOBILE NUM
    // var clientContactadd = $("#clientContact").val().trim(); //DEVICE TYPE
    // var clientType = $("#clientType").val();
    var webAddress = $("#webAddress").val();
    var tallyId = $("#tallyId").val();

    var clientValidity = $("#txtClientValidity").val();
    // var clientTypeTxt = $("#clientType option:selected").text();
    var customerLevel = $('input[type=radio][name="rdoInqMode"]').filter(":checked").val();
    var custAccessVal = "0";

    var masterSmsFlag = ($('#masterSmsFlag').is(':checked')) == true ? "1" : "0";
    var masterEmailFlag = ($('#masterEmailFlag').is(':checked')) == true ? "1" : "0";
    var developerFlag = ($('#developerFlag').is(':checked')) == true ? "1" : "0";
    var tallyFlag = ($('#tallyFlag').is(':checked')) == true ? "1" : "0";
    var orderEposFlag = ($('#orderEposFlag').is(':checked')) == true ? "1" : "0";
    
    var apiLogFlag = ($('#apiLogFlag').is(':checked')) == true ? "1" : "0";
    
    var eposClientId = $("#eposClientId").val();
    var fnbSpaceId = $("#fnbSpaceId").val();
    var laundrySpaceId = $("#laundrySpaceId").val();
    var eposUrl = $("#eposUrl").val();
    // if (clientType == "-1"){
    //     toastr.warning("Please Select Client Type");
    //     $("#clientId").focus();
    //     return false;

    // }else 
    if( clientNameadd.length == 0 ){
        toastr.warning("Please Enter Client Name");
        $("#clientName").focus();
        return false;

    }else if( clientEmailadd.length == 0){
        toastr.warning("Please Enter Email Address");
        $("#clientEmail").focus();
        return false;

    }else if(clientAddressadd.length == 0){
        toastr.warning("Please Enter Address");
        $("#clientAddress").focus();
        return false;

    }else if( !checkValidMobile( "contactNum" ) ){
    // }else if(clientContactadd.length != MOBILE_LENGTH) {
        toastr.warning("Please Enter Valid contact Number");
        $("#contactNum").focus();
        return false;

    }else if(!validateEmail(clientEmailadd)){
        toastr.warning('Please Enter Valid Email Id');
        return false;
    
    }else if( clientValidity == "" ){
        toastr.warning("Please Select Client Validity Date");
        $('#txtClientValidity').datepicker('show');
        return false;
        
    }else if( developerFlag == "1" && (masterSmsFlag == "1" || masterEmailFlag == "1")) {
        toastr.warning("You can not select developer flag along with master email & sms flag");
        return false;
    
    }else if( customerLevel == "1" ){
        var cstAccChk = $('#customerAccessChk').is(':checked');
        if( cstAccChk ){   
            custAccessVal = "1";
        }
    }

    if( tallyFlag == "1" && (tallyId == "" || tallyId == "0") ){
        toastr.warning("Please Enter Tally POTG Id");
        $('#tallyId').focus();
        return false;
    }

    var syncOptionJson = [];
    
    // ADD CLIENT DATA TO JSON
    function addValueToJSON(flag,data){
        if(data.status == SCS && flag){

            var clientIdNew = data.data[0].clientId;
            newClientId = clientIdNew;

            var newClient = {
                clientId:clientIdNew,
                clientName:clientNameadd,
                clientEmail:clientEmailadd,
                clientAddress:clientAddressadd,
                clientContact:clientContactadd,
                webAddress:webAddress,
                tallyId:(tallyFlag == "1") ? tallyId : "",
                clientValidity:ddmmyyToMysql(clientValidity),
                masterSmsFlag : masterSmsFlag,
                masterEmailFlag : masterEmailFlag,
                developerFlag : developerFlag,
                tallyFlag : tallyFlag,
                orderEposFlag : orderEposFlag,
                eposClientId : eposClientId,
                fnbSpaceId : fnbSpaceId,
                laundrySpaceId : laundrySpaceId,
                eposUrl : eposUrl,
                apiLogFlag : apiLogFlag,
            }
            CLIENTJSON.push(newClient);
        }
        toastr.success('New Client has been Created');
        $('#nextBtn').show();
        $('#nextBtn').click();

    }
    // UPDATE CLIENT VALUE TO JSON
    function updateValueToJSON(flag,data){
        if(data.status == SCS && flag){

            newClientId = clientId;
            var tempClient = {
                clientId : clientId,
                clientName : clientNameadd,
                clientEmail : clientEmailadd,
                clientAddress : clientAddressadd,
                clientContact : clientContactadd,
                customerLevel:customerLevel,
                customerLevelTxt:(customerLevel == "1") ? "Customer Level" : "User Level",
                customerAccess:custAccessVal,
                webAddress:webAddress,
                tallyId:(tallyFlag == "1") ? tallyId : "",
                clientValidity:ddmmyyToMysql(clientValidity),
                masterSmsFlag : masterSmsFlag,
                masterEmailFlag : masterEmailFlag,
                developerFlag : developerFlag,
                tallyFlag : tallyFlag,
                orderEposFlag : orderEposFlag,
                eposClientId : eposClientId,
                fnbSpaceId : fnbSpaceId,
                laundrySpaceId : laundrySpaceId,
                eposUrl : eposUrl,
                apiLogFlag : apiLogFlag,
                
            }

            CLIENTJSON[ arrIndex ] = tempClient;

            toastr.success('Client has been Updated');
            $('#nextBtn').show();
            $('#nextBtn').click();
        }else{

            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_CLIENT_FAIL );
        }
    }
    
    if(clientId==""){ // INSERT
        var postData = {
            requestCase: 'createEditClient',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4, 13),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            clientName: clientNameadd,
            clientEmail: clientEmailadd,
            clientAddress: clientAddressadd,
            clientContact: clientContactadd,
            webAddress:webAddress,
            clientValidity:ddmmyyToMysql(clientValidity),
            smsFlag : masterSmsFlag,
            emailFlag : masterEmailFlag,
            developerFlag : developerFlag,
            tallyFlag : tallyFlag,
            tallyFlagId:(tallyFlag == "1") ? tallyId : "",
            orderEposFlag : orderEposFlag,
            eposClientId : eposClientId,
            fnbSpaceId : fnbSpaceId,
            laundrySpaceId : laundrySpaceId,
            eposUrl : eposUrl,
            apiLogFlag : apiLogFlag,
        }
        //console.log(postData); return false;
        commonAjax(COMMONURL, postData, addValueToJSON, CREATECLIENT);
    }
    else{ //UPDATE
        var postData = {
            requestCase: 'createEditClient',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4, 15),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            clientName: clientNameadd,
            clientEmail: clientEmailadd,
            clientAddress: clientAddressadd,
            clientContact: clientContactadd,
            webAddress:webAddress,
            clientValidity:ddmmyyToMysql(clientValidity),
            smsFlag : masterSmsFlag,
            emailFlag : masterEmailFlag,
            developerFlag : developerFlag,
            tallyFlag : tallyFlag,
            tallyFlagId:(tallyFlag == "1") ? tallyId : "",
            requestedClientId : newClientId,
            orderEposFlag : orderEposFlag,
            eposClientId : eposClientId,
            fnbSpaceId : fnbSpaceId,
            laundrySpaceId : laundrySpaceId,
            eposUrl : eposUrl,
            apiLogFlag : apiLogFlag,
        }
        // console.log(postData); return false;
        commonAjax(COMMONURL, postData, updateValueToJSON, UPDATECLIENT);
    }

    // resetClientData();

}
//END FUNCTION FOR CREATE CLIENT 

// //SATRT FUNCTION FOR RESET DATA OF CLIENT AFETR UPDATE/ADD
function resetClientContent(){
    $("#arrIndex").val(''); //ARRAY INDEX
    $("#clientId").val(''); //CLIENT ID
    $("#clientName").val(''); //CLIENT NAME
    $("#clientEmail").val(''); //CLIENT EMAIL
    $("#clientAddress").val(''); // CLIENT ADDRESS
    $("#clientContact").val(''); //CLIENT CONTACT
    $("#tallyId").val(''); //tallyId
    $('#radio-05').prop('checked',true);
    $('#customerAccessChk').prop('checked',false);
    $('#custAccLab').hide();
    $('#masterSmsFlag').prop('checked',false);
    $('#masterEmailFlag').prop('checked',false);
    $('#developerFlag').prop('checked',false);
    $('#tallyFlag').prop('checked',false);
    $('#tallyIdDiv').hide(300);
    $('#orderEposFlag').prop('checked',false);
    $('#apiLogFlag').prop('checked',false);
    $("#eposClientId").val(''); 
    $("#fnbSpaceId").val('');
    $("#laundrySpaceId").val('');
    $("#eposUrl").val('');

    setNumberInIntl( 'contactNum' , "" );
    $("#webAddress").val(''); //CLIENT CONTACT
    $("#txtClientValidity").val(''); //CLIENT CONTACT
}
// //END FUNCTION FOR RESET DATA OF CLIENT AFETR UPDATE/ADD


function pageInitialEvents(){

    if( localStorage.indeCampusClientData != undefined && localStorage.indeCampusClientData != "" && localStorage.indeCampusClientCreateType == "1" ){
        getClientDetails();
        $('#logoAttach').show();
        $('.clienCrUpLbl').html('Update Client');
    }
    
    assignCustAttachementEvent();
    assignItemAttachementEvent();
    assignPriceAttachementEvent();
    assignLogoAttachementEvent();
    
    $('input[name=rdoInqMode]').change(function() {
        var custConfig =  $('input[name=rdoInqMode]:checked').val()
        if( custConfig == "1" ) {
            $('#custAccLab').show();
        }else{
            $('#customerAccessChk').prop('checked',false);
            $('#custAccLab').hide();
        } 
    });

    var currTab = $("#formCreateActSmart .tab-content .tab-pane.active").attr('id');
    
    $('#nextBtn').click(function(){
        // $('#nextBtn').hide();
    })
    
    addFocusId( 'clientName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

// Customer File Upload Functionality starts here
function assignCustAttachementEvent(){
    // grab your file object from a file input
    $('#attachmentNameCust').change(function () {

        if( newClientId == "" || newClientId == undefined || newClientId == "-1" ){
            toastr.warning('Please Select Client Before Uploading a Customer Sheet');
            return false;
        }
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: newClientId,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 9),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            uploadType : "customerUpload",
        };

        $("#postDataCustUpload").val(JSON.stringify(temp_json));

        $("#fileuploadCustomer").off().on('submit',(function(e) {
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);
                    // $("#ajaxloader").removeClass("hideajaxLoader");
                    addRemoveLoader(1);
                },
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    console.log(data);
                    
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                        }

                        attachmetData.push( tempData );

                        // renderAttachment();                        
                        refreshFile();

                        toastr.success("Customer Uploaded Successfully");
                    }else{
                        toastr.warning(data.status);
                        refreshFile();
                        GBL_ERROR = data.data;
                        renderError(GBL_ERROR);
                    }
                    // $("body").addClass("loaded");
                    addRemoveLoader(0);
                    $('#attachmentNameCust').attr('name','attachmentName');
                },
                error: function (jqXHR, errdata, errorThrown) {
                    $('#attachmentNameCust').attr('name','attachmentName');
                    log("error");
                    refreshFile();
                    if( jqXHR.responseText != "" ){
                        GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                        renderErrorCust(GBL_ERROR);
                    }
                    addRemoveLoader(0);
                    // $("body").addClass("loaded");
                }
            });
        }));
        $("#fileuploadCustomer").submit();
        $('#attachmentNameCust').val('');
    });
}
// Customer File Upload Functionality ends here

// Item File Upload Functionality starts here
function assignItemAttachementEvent(){
    // grab your file object from a file input
    $('#attachmentNameItem').change(function () {

        if( newClientId == "" || newClientId == undefined || newClientId == "-1" ){
            toastr.warning('Please Select Client Before Uploading a Item Sheet');
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: newClientId,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(14, 53),
            uploadType: "Item",
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };

        $("#postDataItemUpload").val(JSON.stringify(temp_json));

        $("#fileuploadItem").off().on('submit',(function(e) {
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);
                    // $("#ajaxloader").removeClass("hideajaxLoader");
                    addRemoveLoader(1);
                },
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    console.log(data);
                    
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                        }

                        attachmetData.push( tempData );

                        // renderAttachment();                        
                        refreshFile();

                        toastr.success("Item Uploaded Successfully");
                    }else{
                        toastr.warning(data.status);
                        refreshFile();
                        GBL_ERROR = data.data;
                        renderError(GBL_ERROR);
                        
                    }
                    // $("body").addClass("loaded");
                    addRemoveLoader(0);
                    $('#attachmentNameItem').attr('name','attachmentName');
                },
                error: function (jqXHR, errdata, errorThrown) {
                    $('#attachmentNameItem').attr('name','attachmentName');
                    log("error");
                    refreshFile();
                    addRemoveLoader(0);
                    if( jqXHR.responseText != "" ){
                        GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                        renderError(GBL_ERROR);
                    }
                    // $("body").addClass("loaded");
                }
            });
        }));
        $("#fileuploadItem").submit();
        $('#attachmentNameItem').val('');
    });
}
// Item File Upload Functionality ends here

// Price File Upload Functionality starts here
function assignPriceAttachementEvent(){
    // grab your file object from a file input
    $('#attachmentNamePrice').change(function () {

        if( newClientId == "" || newClientId == undefined || newClientId == "-1" ){
            toastr.warning('Please Select Client Before Uploading a Price Sheet');
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: newClientId,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(14, 53),
            uploadType: "Price",
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };

        $("#postDataPriceUpload").val(JSON.stringify(temp_json));

        $("#fileuploadPrice").off().on('submit',(function(e) {
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);
                    // $("#ajaxloader").removeClass("hideajaxLoader");
                    addRemoveLoader(1);
                },
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    console.log(data);
                    
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                        }

                        attachmetData.push( tempData );

                        // renderAttachment();                        
                        refreshFile();


                        toastr.success("Price Uploaded Successfully");
                    }else{
                        toastr.warning(data.status);
                        refreshFile();
                        GBL_ERROR = data.data;
                        renderError(GBL_ERROR);
                        
                    }
                    // $("body").addClass("loaded");
                    addRemoveLoader(0);
                    $('#attachmentNameItem').attr('name','attachmentName');
                },
                error: function (jqXHR, errdata, errorThrown) {
                    $('#attachmentNamePrice').attr('name','attachmentName');
                    log("error");
                    refreshFile();
                    if( jqXHR.responseText != "" ){
                        GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                        renderError(GBL_ERROR);
                    }
                    // $("body").addClass("loaded");
                    addRemoveLoader(0);
                }
            });
        }));
        $("#fileuploadPrice").submit();
        $('#attachmentNamePrice').val('');
    });
}
// Price File Upload Functionality ends here

function refreshFile(){

    $('#attachmentNameCust').val('');
    $("#postDataCustUpload").val('');
    $('#attachmentNameItem').attr('name','attachmentName');
    $("#postDataCustUpload").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');

    $('#errorHideDiv').hide();
    $('#errorDiv').html('');
    $('#errorHideDivCust').hide();
    $('#errorDivCust').html('');
}


// Download Item CSV Sample file starts here
function downloadCSV(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getItemSample',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14,53),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(UPLOADURL, postData, downloadCSVDataCallback,"Please Wait... Getting Dashboard Detail");

    function downloadCSVDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            console.log(data);

            var GBL_SAMPLEJSON1 = [];
            var GBL_SAMPLEJSON2 = [];

            itemData = data.data.itemMaster;
            groupData = data.data.groupUomMaster;
            
            for( var i=0; i<itemData.length; i++ ){
            
                var tempEXL = {
                    // itemId: itemData[i].ITEMID,
                    itemName: itemData[i].ITEMNAME,
                    // groupId: itemData[i].GROUPID,
                    groupName: itemData[i].GROUPNAME,
                    itemPrice: itemData[i].ITEMPRICE,
                    // uomId: itemData[i].UOMID,
                    uomName: itemData[i].UOMNAME,
                    itemTm: itemData[i].ITEMTM,
                    itemValidityEndDate: itemData[i].ITEMVALIDITYENDDATE,
                    priceValidityEndDate: itemData[i].PRICEVALIDITYENDDATE,
                };
                GBL_SAMPLEJSON1.push(tempEXL);
            }

            for( var i=0; i<groupData.length; i++ ){
                    
                var tempEXL = {
                    groupId: groupData[i].GROUPID,
                    groupName: groupData[i].GROUPNAME,
                };
                GBL_SAMPLEJSON2.push(tempEXL);
            }

            // if( GBL_SAMPLEJSON1.length < 2 ){
            //     toastr.warning('Please Search Report Before Downloading...');
            // }else{
                csvSave(GBL_SAMPLEJSON1, "ItemSample.csv");
            // }

            // if( GBL_SAMPLEJSON2.length < 2 ){
            //     toastr.warning('Please Search Report Before Downloading...');
            // }else{
                csvSave(GBL_SAMPLEJSON2, "GroupUOMSample.csv");
            // }

        } else {

            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEM_SAMPLE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// Download Item CSV Sample file ends here

// Download Price CSV Sample file starts here

function downloadPriceCSV(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getItemPriceDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14,54),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(COMMONURL, postData, downloadPriceCSVDataCallback,"Please Wait... Getting Dashboard Detail");

    function downloadPriceCSVDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            // console.log(data);return false;

            var GBL_SAMPLEJSON4 = [];

            itemData = data.data;
            
            for( var i=0; i<itemData.length; i++ ){
            
                var tempEXL = {
                    itemId: itemData[i].itemId,
                    priceId: itemData[i].priceId,
                    itemName: itemData[i].itemName,
                    itemPrice: itemData[i].itemPrice,
                    itemStartDate: itemData[i].itemStartDate,
                };
                GBL_SAMPLEJSON4.push(tempEXL);
            }

            // if( GBL_SAMPLEJSON1.length < 2 ){
            //     toastr.warning('Please Search Report Before Downloading...');
            // }else{
                csvSave(GBL_SAMPLEJSON4, "PriceSample.csv");
            // }

        } else {

            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_PRICE_SAMPLE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// Download Price CSV Sample file ends here

// Download Customer CSV Sample file starts here

var GBL_USERLIST = [];
function downloadCustomerCSV(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getUserListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, downloadCustomerCSVCallback,"Please Wait... Getting Dashboard Detail");

    function downloadCustomerCSVCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            console.log(data);
            GBL_USERLIST = data.data;

            getGroupListing();

        } else {

            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USERLIST );
            else
                toastr.error(SERVERERROR);
        }
    }
            
}

// Download Customer CSV Sample file ends here
function getGroupListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userCustomerGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,66),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        searchType : "CUSTOMER"
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getGroupListingCallback,"Please Wait... Getting Dashboard Detail");

    function getGroupListingCallback(flag,data){

        if (data.status == "Success" && flag) {

            var groupListData = data.data;

            downloadCustFiles( groupListData ); //GROUP LISTING API
        } else {

            downloadCustFiles( "" ); //GROUP LISTING API
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CUST_GROUP );
            else
                toastr.error(SERVERERROR);
        }
    }           
}

//DOWNLOAD CUSTOMER FILES
function downloadCustFiles( groupListData ){

    var GBL_SAMPLEJSON3 = [];
    var tempEXL = { // HEADER FOR PDF AND CSV
        name : "Name",
        mobile : "Mobile",
        email : "Email",
        address : "Address",
        country : "Country",
        state : "State",
        city : "City",
        contPerName : "ContactName",
        contPerMob : "ContactMobile",
        contEmail : "ContactEmail",
        contDesig : "ContactDesignation",
        assUser : "assign User Name",
        industryType : "industryType",
        rateType : "rateType",
        catchmentArea : "catchmentArea",
        custGroupName : "custGroupName",
    };
    GBL_SAMPLEJSON3.push(tempEXL);

    GBL_SAMPLEJSON4 = [];

    userData = GBL_USERLIST;
    // console.log(data.data);return false;
    var tempEXLUser = { // HEADER FOR PDF AND CSV
        userId : "User Id",
        userName : "User Full Name",
    };
    GBL_SAMPLEJSON4.push(tempEXLUser);

    for( var i=0; i<userData.length; i++ ){
    
        var tempEXLUser = {
            userId: userData[i].userId,
            userName: userData[i].userFullName,
        };
        GBL_SAMPLEJSON4.push(tempEXLUser);
    }

    var tempEXLUser = { // HEADER FOR PDF AND CSV
        userId : "",
        userName : "",
    };
    GBL_SAMPLEJSON4.push(tempEXLUser);
    
    var tempEXLUser = { // HEADER FOR PDF AND CSV
        userId : "Group Id",
        userName : "Group Name",
    };
    GBL_SAMPLEJSON4.push(tempEXLUser);

    if( groupListData != "" ){
        groupListData.forEach(function( record , index ){
            var tempEXLUser = {
                userId: record.groupId,
                userName: record.groupName,
            };
            GBL_SAMPLEJSON4.push(tempEXLUser);
        });
    }
    
    csvSave(GBL_SAMPLEJSON4, "user&GroupListing.csv");
    csvSave(GBL_SAMPLEJSON3, "customerSample.csv");
}   

var attachmetData = [];
function openAttachment(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "logoAttachmentListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9, 34),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(COMMONURL, postData, sendAttachmentCallBack,CREATINGINQDATA);

    function sendAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            attachmetData = data.data;
            renderLogoAttachment();

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_FAILED );
            else
                toastr.error(SERVERERROR);
        }
    }
    $('#attachmentAddLogo').modal('show');
}

function renderLogoAttachment(){
    var attachLi = ''
    for( var i=0; i<attachmetData.length; i++ ){
        attachLi += '<li class="">'+
                        '<div class="col-lg-12">'+
                            '<div class="card card-image card-light-blue bg-image bg-opaque8">'+
                                '<img src="'+ attachmetData[i].fileLink +'" alt="" class="gallery-image">'+
                                '<div class="context has-action-left has-action-right">'+
                                    '<div class="tile-content">'+
                                        '<span class="text-title">'+ attachmetData[i].fileName +'</span>'+
                                        '<span class="text-subtitle">'+ (attachmetData[i].fileName).split('.')[1] +'</span>'+
                                    '</div>'+
                                    '<div class="tile-action right">'+
                                        '<a href="javascript:void(0)" attchId="'+ attachmetData[i].fileId +'" onclick="deleteAttachment(this)" class="btn btn-sm btn-warning">'+
                                            '<i class="fa fa-trash"></i>'+
                                        '</a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</li>';
    }

    $('#AttachmentListing').html( attachLi );
}

function assignLogoAttachementEvent(){
    // grab your file object from a file input
    $('#attachmentLogo').change(function () {
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "logoAttachmentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9, 35),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        $("#postDataAttachLogo").val( JSON.stringify(temp_json));

        $("#fileuploadLogo").off().on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",       // Type of request to be send, called as method
                data:  new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData:false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function(data)// A function to be called if request succeeds
                {
                    data = JSON.parse(data);
                    console.log(data);
                    if(data.status == "Success"){
                        
                        console.log(data.data);
                        attachmetData = [];
                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            clientId: data.data.clientId,
                        }

                        attachmetData.push( tempData );

                        renderLogoAttachment();
                        toastr.success("Logo Uploaded Successfully");
                    }else{
                        toastr.warning(data.status);

                    }
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                }
            });
        }));
        $("#fileuploadLogo").submit();
        $('#attachmentLogo').val('');
    });
}

function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attchId');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'logoAttachmentListingDelete',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9, 35),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        attachId : attachmentId
    };
    commonAjax(UPLOADURL, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var index = findIndexByKeyValue( attachmetData , "fileId" , attachmentId );
            
            attachmetData.splice( index , 1 );

            renderLogoAttachment();

        }else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

function getPageWiseData(){

    setTimeout( function(){
        var currTab = $(".bs-wizard .tab-content .tab-pane.active").attr('id');

        if( currTab == "branchCreate" ){

            // localLoadAuth( setBranchData );
            setBranchData();
            function setBranchData(){
                getClientData();
                assignLogoAttachementEvent();
                initTelInput( 'branchMobile' );   // INITIALIZE COUNTRY WISE MOBILE NUM
            }
            
        }else if( currTab == "userLevelCreate" ){ 

            if( GBL_BRANCH_LIST == "" ){
                toastr.error('Please add Branches to proceed');
                setTimeout( function(){ $('#prevBtn').click() },10 );
                return false;
            }

            localLoadAuth( getUserLevelData );
            // getUserLevelData();

        }else if( currTab == "userCreate" ){ 

            if( GBL_ULEVEL_LIST == "" ){
                toastr.error('Please add User Levels to proceed');
                setTimeout( function(){ $('#prevBtn').click() },10 );
                return false;
            }

            localLoadAuth( setUserCreateData );

            
            function setUserCreateData(){
                
                $('.lowerLevel').hide();
                $('#supervisorLevel').hide();

                var data = JSON.parse(localStorage.indeCampusRoleAuth);
                setOption("0", "desLevel", data.data.designation, "---Select Designation---");
                $('#desLevel').selectpicker('refresh'); 
                
                autoCompleteForSupervisor("supName",saveSuperWiserData);

                // if( data.data.clientTypeId == "32" ){
                //     setProjectListing();
                //     $('#projectListDiv').show();
                // }else{
                getUserData();  
                // $('#projectListDiv').remove();
                // }
                initTelInput( 'mobile' );   // INITIALIZE COUNTRY WISE MOBILE NUM

                setTimeout(function(){
                    resetUserData();
                },500); 

                getUserListingData();
            }
        }else if( currTab == "itemUpload" ){ 
            if( GBL_USER_LISTING == "" ){
            // if( localStorage.indeCampusClientCreateType == "0" && GBL_USER_CREATE == false ){
                toastr.error('Please add User to proceed');
                setTimeout( function(){ $('#prevBtn').click() },10 );
                return false;
            }
        }
    },10);
}

// CALL GET LOAD AUTH DATA API
function localLoadAuth( callBack ){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase : "loadauth",
        userId : tmp_json.data[0].PK_USER_ID,
        clientId : newClientId,
        orgId : localStorage.indeCampusBranchIdForChkAuth,
        dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(COMMONURL,postData, 
    function(a,b){
        //log(b); return false;
        if(b.status==SCS){

            localStorage.indeCampusRoleAuth = JSON.stringify(b);
            localStorage.indeCampusUserDetail = JSON.stringify(tmp_json);
            
            callBack();

        }else{
            toastr.warning(b.status);
            return false;
        }
    }, "Please wait... Retriving Permission Data.");
}

// RENDER ERROR LIST ON CUSTOMER UPLOAD
function renderErrorCust(GBL_ERROR){ //added on 07-07-2017
    
    var errorList = "";
    if( GBL_ERROR != undefined ){    
        GBL_ERROR.forEach(function(record,index){
            errorList += '<span class="text-red">'+record+'</span><br/>';
        });
        $('#errorHideDivCust').show();
        $('#errorDivCust').html(errorList);
    }    
}

//TALLY ID DIV OF INPUT HIDE SHOW
function tallyDivChanges(){
    var tallyFlag = $('#tallyFlag').prop('checked');
    if( tallyFlag ){
        $('#tallyIdDiv').show(300);
    } else{
        $('#tallyIdDiv').hide(300);
    }
}