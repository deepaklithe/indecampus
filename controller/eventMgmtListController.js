    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 31-07-2018.
 * File : eventMgmtListController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';

// PAGE LOAD EVENTS
function pageInitialEvents(){

    $('#uploadMode').selectpicker('refresh');
    $('#filterStatus').selectpicker();
    getEventList(); // GET EVENTS LISTING DATA
    autoCompleteForStudentId('studentMemId',saveStudentNameData); // PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID
    localStorage.removeItem('indeCampusViewEventId');
    localStorage.removeItem('indeCampusEditStudId');
    localStorage.removeItem('indeCampusBookEventPayment');
}

// GET EVENTS LISTING DATA
function getEventList(){

    var itemDataType = localStorage.indeCampusAddItemType;
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "EVENTS",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getEventListCallback,"Please wait...Adding your item.");

    function getEventListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_EVENTS = data.data;
            renderEventList(GBL_EVENTS); // RENDER EVENTS

        }else{

            GBL_EVENTS = [];
            renderEventList(GBL_EVENTS); // RENDER EVENTS
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_EVENTS );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER ITEM LISTING DATA
function renderEventList(eventData){

    var thead = '<table class="display datatables-alphabet-sorting" id="itemListTable">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Event ID</th>'+
                        '<th>Event Name</th>'+
                        '<th>Start Date | Time</th>'+
                        '<th>End Date | Time</th>'+
                        '<th>Rate</th>'+
                        '<th>Max Capacity</th>'+
                        '<th>Available Seats</th>'+
                        '<th>Status</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var innerRow = '';
    if( eventData.length != 0 && eventData != "No record found" ){

        eventData.forEach(function( record , index ){
            var eventStatus = 'Active';
            var disabledClass = '';
            var btnClass = 'btn-success';
            if( record.status == "0" ){
                eventStatus = 'Inactive';
                disabledClass = 'disabled';
                btnClass = 'btn-danger';
            }
            var eventStartDateTime = mysqltoDesiredFormat(record.eventStartDateTime.split(' ')[0].trim(),'dd-MM-yyyy') + ' | ' + record.eventStartDateTime.split(' ')[1].trim() + ' ' + record.eventStartDateTime.split(' ')[2].trim();
            var eventEndDateTime = mysqltoDesiredFormat(record.eventEndDateTime.split(' ')[0].trim(),'dd-MM-yyyy') + ' | ' + record.eventEndDateTime.split(' ')[1].trim() + ' ' + record.eventEndDateTime.split(' ')[2].trim();
            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.eventNo +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td>'+ eventStartDateTime +'</td>'+
                            '<td>'+ eventEndDateTime +'</td>'+
                            '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.itemPrice) +'</td>'+
                            '<td>'+ record.qty +'</td>'+
                            '<td>'+ record.remainingSeats +'</td>'+
                            '<td>'+ 
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs '+btnClass+' btn-ripple" itemId="'+ record.itemId +'" onclick="changeEventStatus(this);">'+ eventStatus +'</a>'+
                            '</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" itemId="'+ record.itemId +'" class="btn-right-mrg btn btn-xs btn-amber btn-ripple" data-toggle="tooltip" data-original-title="View" '+disabledClass+' onclick="viewEvent(this);"><i class="fa fa-eye"></i></a>'+
                                '<a href="javascript:void(0)" itemId="'+ record.itemId +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" data-toggle="tooltip" data-original-title="Edit" onclick="navigateToEditEvent(this);" '+disabledClass+'><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" itemId="'+ record.itemId +'" class="btn-right-mrg btn btn-xs btn-success btn-ripple" data-toggle="tooltip" data-original-title="Book" '+disabledClass+' onclick="openBookEventModal(this);"><i class="fa fa-ticket"></i></a>'+
                                '<a href="javascript:void(0)" itemId="'+ record.itemId +'" class="btn-right-mrg btn btn-xs btn-danger btn-ripple" data-toggle="tooltip" data-original-title="Delete" '+disabledClass+' onclick="deleteEvent(this);"><i class="fa fa-trash"></i></a>'+
                            '</td>'+
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    $('#itemListTable').DataTable();

}

// EDIT FUNCTION - NAVIGATE TO UPDATE EVENT PAGE
function navigateToEditEvent(context){

    var itemId = $(context).attr('itemId');
    var index = findIndexByKeyValue( GBL_EVENTS , 'itemId' , itemId );
    if( index != -1 ){
        localStorage.indeCampusUpdateItemJson = JSON.stringify(GBL_EVENTS[index]);
        localStorage.indeCampusUpdateItemId = itemId;
        window.location.href = "eventManagementDetail.html";
    }

}

// CHANGE EVENT STATUS
function changeEventStatus(context){

    var itemId = $(context).attr('itemId');
    var index = findIndexByKeyValue( GBL_EVENTS , 'itemId' , itemId );
    if( index != -1 ){   

        var curStatus = GBL_EVENTS[index].status;
        var confimActInAct = "";
        var reqStatus = "";
        if( curStatus == "1" ){  // 1 - ACTIVE EVENT -- 0 - IN ACTIVE EVENT
            confimActInAct = confirm("Are you sure? You wants to De-Activate this Event");
            reqStatus = "0";
        }else{
            confimActInAct = confirm("Are you sure? You wants to Activate this Event");
            reqStatus = "1";
        }
        if( !confimActInAct ){
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateEventStatus',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            eventId: itemId, 
            status: reqStatus,
            orgId: checkAuth(21,87),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }
        commonAjax(FOLLOWUPURL, postData, changeEventStatusCallback,"");
        function changeEventStatusCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data); 

                GBL_EVENTS[index].status = reqStatus;
                renderEventList(GBL_EVENTS); // RENDER EVENTS

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_ACT_INACT_EVENT );
                else
                    toastr.error(SERVERERROR);
            }
        }

    }
}

// OPEN MODAL FOR BOOK EVENT
function openBookEventModal(context){

    var itemId = $(context).attr('itemId');
    GBL_EVENT_ID = itemId;
    $('#noOfSeats').val('');
    $('#studentMemId').val('');
    $('#studentMemId').attr('studId','');
    modalShow('bookEventModal');

}

// BOOK SELECTED EVENT
function bookEvent(){

    var studentMemId = $('#studentMemId').val();
    var studId = $('#studentMemId').attr('studId');
    var noOfSeats = $('#noOfSeats').val();

    if( studentMemId == "" ){
        toastr.warning("Enter student membership Id");
        addFocusId('studentMemId');
        return false;

    }else if( studId == "" || studId == undefined ){
        toastr.warning("Please select student from search option");
        addFocusId('studentMemId');
        return false;

    }else if( noOfSeats == "" ){
        toastr.warning("Enter no of seats for booking");
        addFocusId('noOfSeats');
        return false;

    }else if( noOfSeats != "" && parseInt(noOfSeats) < 1 ){
        toastr.warning("Enter valid no of seats for booking");
        addFocusId('noOfSeats');
        return false;

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'eventBooking',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID, 
        studentId: studId, 
        qty: noOfSeats,
        itemId: GBL_EVENT_ID,
        orgId: checkAuth(21,85),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, bookEventCallback,"");
    function bookEventCallback(flag,data){

        if (data.status == "Success" && flag) {
            toastr.success("Your booking was successful");
            addRemoveLoader(1); // ADD LOADER
            console.log(data); 
            GBL_EVENT_ID = "";
            modalHide('bookEventModal');
            $('#noOfSeats').val('');
            $('#studentMemId').val('');
            $('#studentMemId').attr('studId','');
            // if( data.data != "" && data.data != "No record found" ){
            //     localStorage.indeCampusBookEventPayment = data.data;
            //     navigateToCollectPayment(studId);
            // }
            getEventList();

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_BOOK_EVENT );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function saveStudentNameData(){
}



// GET EVENTS ON SELECTED FILTERS
function getFilteredEvents(){

    var filterEventStDate = $('#filterEventStDate').val();
    // var filterEventEnDate = $('#filterEventEnDate').val();
    var filterStatus = $('#filterStatus').val();

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getItemsData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(15,62),
        itemType : "EVENTS",
        eventDate : filterEventStDate,
        // eventEndDateTime : filterEventEnDate,
        status : filterStatus,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getFilteredStudentsDataCallback,"Please Wait... Getting Dashboard Detail");

    function getFilteredStudentsDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
            console.log(data);
            renderEventList(data.data); // RENDER FILTERED STUDENT LIST
            $('.Sidenav').removeClass('open');  
            resetFilters(); // RESET FILTER VALUES

        }else{

            if (flag){
                if( data.status == "No record found" ){
                    renderEventList([]); // RENDER FILTERED STUDENT LIST
                    $('.Sidenav').removeClass('open');  
                    resetFilters(); // RESET FILTER VALUES
                }
                displayAPIErrorMsg( data.status , GBL_ERR_NO_EVENT_FILTER );
                // toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }
        }
    }

}

// RESET FILTER
function resetFilters(){

    $('#filterEventStDate').val('');
    // $('#filterEventEnDate').val('');
    $('#filterStatus').val('').selectpicker('refresh');

}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderEventList(GBL_EVENTS); // RENDER ORIGINAL WITHOUT FILTER DATA

}

// VIEW PARTICULAR EVENT
function viewEvent(context){

    var itemId = $(context).attr('itemId');
    localStorage.indeCampusViewEventId = itemId;
    navigateToEventView();

}

// REDIRECT TO COLLECT PAYMENT PAGE AFTER BOOKING AN EVENT
function navigateToCollectPayment(studId){

    localStorage.indeCampusEditStudId = studId;
    window.location.href = "collectpayment.html";

}


// DELETE EVENT
function deleteEvent(context){

    var itemId = $(context).attr('itemId');
    var index = findIndexByKeyValue( GBL_EVENTS , 'itemId' , itemId );
    if( index != -1 ){   

        
        if( parseInt(GBL_EVENTS[index].remainingSeats) < parseInt(GBL_EVENTS[index].qty) ){
            toastr.error("This Event cannot be deleted as it is booked");
            return false;

        }
        var confimDelete = confirm("Are you sure? You wants to Delete this Event");
        if( !confimDelete ){
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteEvent',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            orgId: checkAuth(21,88),
            itemId: itemId, 
            itemType:"EVENTS",
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }
        commonAjax(FOLLOWUPURL, postData, deleteEventCallback,"");
        function deleteEventCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data); 

                GBL_EVENTS = data.data;
                renderEventList(GBL_EVENTS); // RENDER EVENTS

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DEL_EVENTS );
                else
                    toastr.error(SERVERERROR);
            }
        }

    }
}
