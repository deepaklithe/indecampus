/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : emailController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var branchData = [];
var GBL_TEMPLATE_LIST = [];
var GBL_SEND_TEMPLATE_DATA = [];

// GET CLIENT LISTING DATA
function getClientData(){

    $('#clientList').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getClientListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getClientDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET CLIENT LISTING DATA CALLBACK
function getClientDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var newRow = '';
        if(tmp_json.data[0].FK_CLIENT_ID == -1){
            for( var i=0; i<data.data.length; i++ ){
                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
            }
        }else{
            for( var i=0; i<data.data.length; i++ ){
                if(tmp_json.data[0].FK_CLIENT_ID ==data.data[i].clientId ){

                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
                }
            }
        }
        

        $('#clientList').html(newRow);
        //$('#clientList').val(tmp_json.data[0].FK_CLIENT_ID);
        $('#clientList').selectpicker();
        $('#portNo').selectpicker();

        // getUserListing();
        getEMAILdata();

    } else {
        // getUserListing();
        getEMAILdata();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT );
        else
            toastr.error(SERVERERROR);
    }
}

// GET EMAIL CONFIG DATA
function getEMAILdata(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "EMAILData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getEMAILdataCallback,"Please Wait... Getting Dashboard Detail");

    function getEMAILdataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);

            if( data.data != 'No record found' ){
                
                GBL_TEMPLATE_LIST = data.data[0].emailTrans;

                $('#clientList').val(data.data[0].client);
                $('#hostName').val(data.data[0].hostName);
                $('#portNo').val(data.data[0].portNo);
                $('#email').val(data.data[0].email);
                $('#password').val(data.data[0].password);
                $('#sender').val(data.data[0].sender);

                $('#clientList').selectpicker('refresh');
                $('#portNo').selectpicker('refresh');

                if( data.data[0].inqMailFlag == "1" ){
                    $('#allowInquiryMail').prop('checked',true);
                }
                if( data.data[0].actMailFlag == "1" ){
                    $('#allowActivityMail').prop('checked',true);
                }
                if( data.data[0].syncActiveFlag == "1" ){
                    $('#syncActive').prop('checked',true);
                }
                if( data.data[0].rateTypeActiveFlag == "1" ){
                    $('#rateTypeActive').prop('checked',true);
                }
            }

            addFocusId( 'clientList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_EMAIL_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// CREATE / UPDATE EMAIL CONFIG DATA
function sendEMAILdata(){

    var clientName = $('#clientList').val();
    var hostName = $('#hostName').val().trim();
    var portNo = $('#portNo').val();
    var email = $('#email').val().trim();
    var password = $('#password').val().trim();
    var sender = $('#sender').val().trim();
    var inqMailFlag = ($('#allowInquiryMail').is(':checked')) == true ? "1" : "0";
    var actMailFlag = ($('#allowActivityMail').is(':checked')) == true ? "1" : "0";
    var syncActiveFlag = ($('#syncActive').is(':checked')) == true ? "1" : "0";
    var rateTypeActiveFlag = ($('#rateTypeActive').is(':checked')) == true ? "1" : "0";

    if( hostName.length == 0 ){
        toastr.warning("Please Enter host Name");
        $('#hostName').focus();
        return false;

    }else if( email.length == 0 ){
        toastr.warning("Please Enter email");
        $('#email').focus();
        return false;
    
    }else if( password.length == 0 ){
        toastr.warning("Please Enter password");
        $('#password').focus();
        return false;

    }else if( sender.length == 0 ){
        toastr.warning("Please Enter sender");
        $('#sender').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createUpdateEmail",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            reqClientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,13),
            clientName : clientName,
            hostName : hostName,
            portNo : portNo,
            email : email,
            password : password,
            sender : sender,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, sendEMAILdataCallback,"Please Wait... Getting Dashboard Detail");

        function sendEMAILdataCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data);
                
                toastr.success('E-mail data Sent Successfully');
                getEMAILdata(); // GET THE EMAIL DATA

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_FAIL_CREATE_EMAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

// RESET FIELDS
function resetData(){

    $('#hostName').val('');
    $('#password').val('');
    $('#email').val('');
    $('#sender').val('');
}

function pageInitialEvents(){

}
