/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : userLevelController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var userLevelData = [];
// GET USER LEVEL LISTING DATA
function getUserLevelData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getUserLevel',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET USER LEVEL LISTING DATA CALLBACK
function getUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        userLevelData = data.data;

        renderUserLevel();

    } else {
        renderUserLevel();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_LEVEL );
        else
            toastr.error(SERVERERROR);
    }
}

// RENDER USER LEVEL DATA
function renderUserLevel(){

    var thead = '<table id="tableListingULevel" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Level Id</th>'+
                        '<th>Name</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;
    var nullCount = 0 ;

    if(userLevelData != 'No record found'){
        
        for( var i=0; i<userLevelData.length; i++ ){

            var name = userLevelData[i].levelName;
            var levelDefaultName = userLevelData[i].levelDefaultName;

            if( name == "" ){
                name = "";
                nullCount++;
            }
            var LevelCount = '';
            count = i + 1;
            LevelCount = 11 - count;
            newRow += '<tr>'+
                            '<td>'+ (i+1) +'</td>'+
                            '<td>'+ levelDefaultName +'</td>'+
                            '<td>'+ name +'</td>';

            newRow += ( name != "" || nullCount == 1) ? '<td><a href="javascript:void(0)" index="'+ i +'" name="'+ name +'" count="'+ LevelCount +'" levelId="'+ userLevelData[i].levelId +'" class="btn btn-xs btn-primary btn-ripple" onclick="updateUserLevel(this)"><i class="fa fa-pencil"></i></a></td>' : '<td></td>';
            newRow += '</tr>';
        }

        $('#ULeveldtTable').html(thead + newRow + tfoot);

    }

    $('#tableListingULevel').DataTable({ "stateSave": true });
}   

var indexForCreate = '';
// SET SELECTED USER LEVEL DATA FOR UPDATE
function updateUserLevel(context){
    
    var levelId = $(context).attr('levelId');
    var levelPriority =  $(context).attr('count');
    var name =  $(context).attr('name');
    
    indexForCreate = $(context).attr('index');

    $('#userLevelName').val(name)
    $('#editlevel').modal('show');

    $('#createLevel').attr('levelId',levelId);
    $('#createLevel').attr('levelPriority',levelPriority);
 
    addFocusId( 'userLevelName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var gblContext = '';
// CREATE NEW USER LEVEL
function createLevel(context){

    var levelId = $(context).attr('levelId');
    var levelPriority = $(context).attr('levelPriority');
    var levelName = $('#userLevelName').val().trim();
    gblContext = context;

    var reqCase = "";
    var auth = "";
    if( levelId == "undefined" ){
        if( checkAuth(8, 29, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;
        }
        reqCase = "insertUserLevel"
        auth = checkAuth(8,29);
    }else{
        if( checkAuth(8, 31, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;
        }
        reqCase = "updateUserLevel";
        auth = checkAuth(8,31);
    }

    if( levelName.length == 0 ){
        toastr.warning('Please Enter Level Name');
        $('#userLevelName').focus();
        return false;

    }else{
        
        $('#editlevel').modal('hide');

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: reqCase,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: auth,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            levelName : levelName,
            lookupId : levelId == "undefined" ? "" : levelId ,
            priorityLevel : levelPriority
        }  
        console.log(postData);
       // return false;
        commonAjax(FOLLOWUPURL, postData, createLevelDataCallback,"Please Wait... Getting Dashboard Detail");
    }
}

// CALLBACK OF CREATE NEW USER LEVEL
function createLevelDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // location.reload();

        var levelPriority = $(gblContext).attr('levelPriority');
        var levelName = $('#userLevelName').val().trim();

        var levelId = '';

        getUserLevelData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAIL_CREATE_EDIT_USERLEVEL );
        else
            toastr.error(SERVERERROR);
    }
}