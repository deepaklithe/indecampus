    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : userManageController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

// GLOBAL VARIABLE DEFINED
var GBL_REQ_USER_ID = "";
var groupData = [];
var GBL_USER_LIST_HIER_WISE = [];

//API FOR USER LISTING CALL 
function getUserData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getUserListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
}

//USER LISTING CALLBACK FUNCTION
function getUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // return false;
        groupData = data.data;
        renderUsers();
        // userListHierarchy();
    } else {

        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }
}

//RENDER USERS FROM JSON
function renderUsers(){

    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Full Name</th>'+
                        '<th>User Name</th>'+
                        '<th>Level</th>'+
                        '<th>Designation</th>'+
                        '<th>Branch</th>'+
                        // '<th>User Type</th>'+
                        '<th>Status</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    if(groupData != 'No record found'){
        
        for( var i=0; i<groupData.length; i++ ){

            var fullName = groupData[i].userFullName;
            var name = groupData[i].userName;
            var userLevel = groupData[i].userLevelName;
            var userDesignation = groupData[i].userDesignationName;
            var userId = groupData[i].userId;
            var profileImg = groupData[i].userProfileImage;
            var branch = groupData[i].userBranchName;
            // var userType = ( (groupData[i].userLoginType == "1") ? "CRM" : "Activitiy" );
            
            if( profileImg == "" ){
                profileImg = "http://"+FINALPATH+"/assets/globals/img/avtar1.png";
            }
            
            var actInActLable = "Active";
            var actInActCls = "success";
            var transferDisabled = "disabled";
            var userEditDisabled = "";
            if( groupData[i].userStatus != "1" ){
                actInActLable = "InActive";
                actInActCls = "danger";
                transferDisabled = "";
                userEditDisabled = "disabled";
            }
            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><span class="useraImageBody"><img class="userProfile" id="" src="'+ profileImg +'"></span>&nbsp;&nbsp;'+ fullName +'</td>'+
                            '<td>'+ name +'</td>'+
                            '<td>'+ userLevel +'</td>'+
                            '<td>'+ userDesignation +'</td>'+
                            '<td>'+ branch +'</td>'+
                            '<td>'+
                                '<a title="Status" href="javascript:void(0)" data-userId="'+userId+'" onclick="activeInActive(this , '+ groupData[i].userStatus +')" class="btn-right-mrg  btn btn-xs btn-'+ actInActCls +' btn-ripple">'+ actInActLable +'</a>'+ 
                            '</td>'+
                            // '<td>'+ userType +'</td>'+
                            '<td>'+
                                // '<a title="Transfer Data" '+ transferDisabled +' href="javascript:void(0)" data-userId="'+userId+'" onclick="openTransferModal(this)" class="btn-right-mrg  btn btn-xs btn-amber btn-ripple"><i class="fa fa-exchange"></i></a>'+ 
                                '<a title="Edit" '+ userEditDisabled +' href="javascript:void(0)" userLevel="'+ userLevel +'" data-userId="'+userId+'" onclick="navigateToEditUser(this)" class="btn-right-mrg  btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>'+ 
                                '<a title="Delete" href="javascript:void(0)" userLevel="'+ userLevel +'" data-userId="'+userId+'" onclick="deleteUser(this)" class="btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
        }

    }
    $('#dtTable').html(thead + newRow + tfoot);

    TablesDataTables.init();
    TablesDataTablesEditor.init();
}

var gblContext = "";
// DELETE SELECTED USER
function deleteUser(context){
    
    if(checkAuth(8,32,1) == -1){
         toastr.error('You are not authorised for this Activitiy');
    }else{
        var deleteRow = confirm('Are You sure ? You want to delete this Record');

        if( deleteRow ){

            gblContext = context;
            var userId = $(context).attr('data-userId');

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'deleteUser',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                requestedUserId: userId,
                orgId: checkAuth(8,32),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
            }

            commonAjax(FOLLOWUPURL, postData, deleteUserCallback,"Please Wait... Getting Dashboard Detail");
        }
    }
}

// DELETE USER CALLBACK
function deleteUserCallback(flag,data){

    if (data.status == "Success" && flag) {
        
        console.log(data);
        $(gblContext).parents('td').parents('tr').remove();

        var userId = $(gblContext).attr('data-userId');

        var index = findIndexByKeyValue( groupData,'userId',userId );

        groupData.splice(index,1);

        renderUsers();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_USER );
        else
            toastr.error(SERVERERROR);
    }
}

function userListHierarchy(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getUserListingByName',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8 , 30),
        supervisorFlag: "1",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, userListHierarchyDataCallback,"Please Wait... Getting Dashboard Detail");
    function userListHierarchyDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
            // return false;
            GBL_USER_LIST_HIER_WISE = data.data; 
            setUserTransferData();

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function activeInActive( context , type ){

    var userId = $(context).attr('data-userId');
    
    var confimActInAct = "";
    var reqStatus = "";
    if( type == "1" ){  // 1 - ACTIVE USER -- 0 - IN ACTIVE USER
        confimActInAct = confirm("Are you sure? You wants to De-Activate this User");
        reqStatus = "0";
    }else{
        confimActInAct = confirm("Are you sure? You wants to Activate this User");
        reqStatus = "1";
    }

    if( confimActInAct ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'userActiveDeActive',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            selectedUserId: userId, 
            status: reqStatus,
            orgId: checkAuth(8,32),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }
        commonAjax(FOLLOWUPURL, postData, activeInActiveCallback,"");
        function activeInActiveCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data); 

                var index = findIndexByKeyValue( groupData , "userId" , userId );
                if( index != "-1" ){
                    groupData[index].userStatus = reqStatus;
                    renderUsers();
                }
            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_ACT_INACT_USER );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
} 

function setUserTransferData(){

    setOptionWithRef("0", 'actUserList', GBL_USER_LIST_HIER_WISE, "---Select User---", 'userId', 'fullName');
    $('#actUserList').attr('disabled',true).selectpicker('refresh');
    
    setOptionWithRef("0", 'inqUserList', GBL_USER_LIST_HIER_WISE, "---Select User---", 'userId', 'fullName');
    $('#inqUserList').attr('disabled',true).selectpicker('refresh');
    
    setOptionWithRef("0", 'ordUserList', GBL_USER_LIST_HIER_WISE, "---Select User---", 'userId', 'fullName');
    $('#ordUserList').attr('disabled',true).selectpicker('refresh');
    
    setOptionWithRef("0", 'serUserList', GBL_USER_LIST_HIER_WISE, "---Select User---", 'userId', 'fullName');
    $('#serUserList').attr('disabled',true).selectpicker('refresh');
    
    setOptionWithRef("0", 'leadUserList', GBL_USER_LIST_HIER_WISE, "---Select User---", 'userId', 'fullName');
    $('#leadUserList').attr('disabled',true).selectpicker('refresh');
    
    setOptionWithRef("0", 'custUserList', GBL_USER_LIST_HIER_WISE, "---Select User---", 'userId', 'fullName');
    $('#custUserList').attr('disabled',true).selectpicker('refresh');

}

function openTransferModal( context ){

    if (checkAuth(8,34) == -1) {
        toastr.error(NOAUTH);
        return false;
    }

    var usreId = $(context).attr('data-userid');
    GBL_REQ_USER_ID = usreId;

    $('#actChk').prop('checked',false);
    $('#inqChk').prop('checked',false);
    $('#ordChk').prop('checked',false);
    $('#serChk').prop('checked',false);
    $('#leadChk').prop('checked',false);
    $('#custChk').prop('checked',false);

    $('#actUserList').val('-1').attr('disabled',true).selectpicker('refresh');
    $('#inqUserList').val('-1').attr('disabled',true).selectpicker('refresh');
    $('#ordUserList').val('-1').attr('disabled',true).selectpicker('refresh');
    $('#serUserList').val('-1').attr('disabled',true).selectpicker('refresh');
    $('#leadUserList').val('-1').attr('disabled',true).selectpicker('refresh');
    $('#custUserList').val('-1').attr('disabled',true).selectpicker('refresh');

    modalShow('transferModal');
}

function chkUnchk( chkId , selId ){
    var curChkVal = $('#'+chkId).prop('checked');
    if( curChkVal ){
        $('#'+selId).attr('disabled',false).selectpicker('refresh');
    }else{
        $('#'+selId).val('-1').attr('disabled',true).selectpicker('refresh');
    }
}

function transferUsers(){

    var actTransfer = ( ($('#actChk').prop('checked') == true ) ? "1" : "0" )
    var actTransferUserId = ( (actTransfer == "1") ? $('#actUserList').val() : "" );
    
    var inqTransfer = ( ($('#inqChk').prop('checked') == true ) ? "1" : "0" )
    var inqTransferUserId = ( (inqTransfer == "1") ? $('#inqUserList').val() : "" );
    
    var ordTransfer = ( ($('#ordChk').prop('checked') == true ) ? "1" : "0" )
    var ordTransferUserId = ( (ordTransfer == "1") ? $('#ordUserList').val() : "" );
    
    var serTransfer = ( ($('#serChk').prop('checked') == true ) ? "1" : "0" )
    var serTransferUserId = ( (serTransfer == "1") ? $('#serUserList').val() : "" );
    
    var leadTransfer = ( ($('#leadChk').prop('checked') == true ) ? "1" : "0" )
    var leadTransferUserId = ( (leadTransfer == "1") ? $('#leadUserList').val() : "" );

    var custTransfer = ( ($('#custChk').prop('checked') == true ) ? "1" : "0" )
    var custTransferUserId = ( (custTransfer == "1") ? $('#custUserList').val() : "" );

    if( actTransfer != "1" && inqTransfer != "1" && ordTransfer != "1" && serTransfer != "1" && leadTransfer != "1" && custTransfer != "1" ){
        toastr.warning('Please Select atleast 1 Module to Transfer Data');
        return false;
    }

    if( actTransfer == "1" && actTransferUserId == "-1" ){
        toastr.warning('Please Select User for Activitiy Transfer');
        addFocusId('actUserList');
        return false;
    }

    if( inqTransfer == "1" && inqTransferUserId == "-1" ){
        toastr.warning('Please Select User for Inquiry Transfer');
        addFocusId('inqUserList');
        return false;
    }

    if( ordTransfer == "1" && ordTransferUserId == "-1" ){
        toastr.warning('Please Select User for Order Transfer');
        addFocusId('ordUserList');
        return false;
    }

    if( serTransfer == "1" && serTransferUserId == "-1" ){
        toastr.warning('Please Select User for Service Transfer');
        addFocusId('serUserList');
        return false;
    }

    if( leadTransfer == "1" && leadTransferUserId == "-1" ){
        toastr.warning('Please Select User for Lead Transfer');
        addFocusId('leadUserList');
        return false;
    }

    if( custTransfer == "1" && custTransferUserId == "-1" ){
        toastr.warning('Please Select User for Customer Transfer');
        addFocusId('custUserList');
        return false;
    }

    // SELECT CURR USER NOT ALLOWED
    if( actTransferUserId == GBL_REQ_USER_ID ){
        toastr.warning('You cant Transfer Activity to Same User');
        addFocusId('actUserList');
        return false;
    }

    if( inqTransferUserId == GBL_REQ_USER_ID ){
        toastr.warning('You cant Transfer Inquiry to Same User');
        addFocusId('inqUserList');
        return false;
    }

    if( ordTransferUserId == GBL_REQ_USER_ID ){
        toastr.warning('You cant Transfer Order to Same User');
        addFocusId('ordUserList');
        return false;
    }

    if( serTransferUserId == GBL_REQ_USER_ID ){
        toastr.warning('You cant Transfer Service to Same User');
        addFocusId('serUserList');
        return false;
    }

    if( leadTransferUserId == GBL_REQ_USER_ID ){
        toastr.warning('You cant Transfer Lead to Same User');
        addFocusId('leadUserList');
        return false;
    }

    if( custTransferUserId == GBL_REQ_USER_ID ){
        toastr.warning('You cant Transfer Customer to Same User');
        addFocusId('custUserList');
        return false;
    }

    var tempData = {
        actTransfer : actTransfer,
        actTransferUserId : actTransferUserId,
        inqTransfer : inqTransfer,
        inqTransferUserId : inqTransferUserId,
        ordTransfer : ordTransfer,
        ordTransferUserId : ordTransferUserId,
        serTransfer : serTransfer,
        serTransferUserId : serTransferUserId,
        leadTransfer : leadTransfer,
        leadTransferUserId : leadTransferUserId,
        custTransfer : custTransfer,
        custTransferUserId : custTransferUserId,
    }
    var trasnferMasterData = [];
    trasnferMasterData.push( tempData );

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'transferUserData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        requestedUserId: GBL_REQ_USER_ID,
        orgId: checkAuth(19,232),
        trasnferMasterData : trasnferMasterData,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, transferUsersDataCallback,"");

    function transferUsersDataCallback(flag,data){

        if (data.status == "Success" && flag) { 
            toastr.success('Transfer Data Successfully');
            modalHide('transferModal');

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_TRANSFER_USER );
            else
                toastr.error(SERVERERROR);
        }
    }
}
