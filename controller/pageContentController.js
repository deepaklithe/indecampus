    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 10-08-2018.
 * File : pageContentController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_PAGE_DATA = [];

function pageInitialEvents() {
	getPageData();
}

//GET THE PAGE CREATED DATA LISTING
function getPageData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "listCmsData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14,58),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getPageDataCallback,"Please Wait... Getting Dashboard Detail");
}
// PAGE DATA CALLBACK
function getPageDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        GBL_PAGE_DATA = data.data;

        renderPageContent();   //RENDER PAGE DATA

    } else {
        renderPageContent();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_PAGE_CONTENT );
        else
            toastr.error(SERVERERROR);
    }
}


function renderPageContent(){

	var newRow = '';
    var count = 1 ;

    //PAGE JSON CHEK
    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
		            '<thead>'+
		                '<tr>'+
		                    '<th>No</th>'+
		                    '<th>Name</th>'+
		                    '<th>Action</th>'+
		                '</tr>'+
		            '</thead>'+
		            '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    if( GBL_PAGE_DATA != 'No record found' && GBL_PAGE_DATA != '' ){

        GBL_PAGE_DATA.forEach(function( record , index ){

            var name = record.sectionName;
            var sectionId = record.sectionId;
            
            count = index + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ name +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" sectionName="'+ name +'" sectionId="'+ sectionId +'" onclick="editPage(this)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>'+ 
                                // '<a href="javascript:void(0)" sectionId="'+ sectionId +'" onclick="deletePage(this)" class="btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';

            
        });

    	
    }else{
         displayAPIErrorMsg("" , GBL_ERR_NO_PAGE_CONTENT );
    }
    $('#dtTable').html(thead + newRow + tfoot);

    TablesDataTables.init();
    TablesDataTablesEditor.init();

}

//ADD PAGE CONTENT MODAL OPEN
function addPageContent(){

    if( checkAuth(1, 2, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    $('#saveCms').attr('onclick','createPage()');

    // $('#pageName').val('-1').selectpicker('refresh');
    $('#pageName').val('');
    $('#pageName').attr('disabled',false);

    tinyMCE.get('pageContentData').setContent('');

    $('#pageContentModal').modal('show');
    addFocusId( 'pageName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

//CREATE PAGE CONTENT API CALL
function createPage(context){

    var pageName = $('#pageName').val().trim();
    var sectionDetail = tinyMCE.get('pageContentData').getContent();

    if( pageName == " " ){
        toastr.warning('Please Enter Page');
        $('#pageName').focus();
        return false;

    }else if( sectionDetail == ""){
        toastr.warning('Please Enter data');
        $('#pageContentData').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addCmsData',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            orgId: checkAuth(14, 57),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            sectionName : pageName,
            sectionDetail : sectionDetail,
        }  
        $('#pageContentModal').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, createSectionCallback,"Please Wait... Getting Dashboard Detail");
    }
}

//CREATE PAGE API CALLBACK
function createSectionCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // location.reload();

        var pageName = $('#pageName').val().trim();
        var sectionDetail = tinyMCE.get('pageContentData').getContent();

        // GBL_PAGE_DATA.push( data.data[0] ); 

        // renderPageData();
        getPageData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_Update_PAGE_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

//EDIT PAGE API DATA SEt FOR UPDATE
function editPage(context){

    if( checkAuth(1, 2, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var sectionId = $(context).attr('sectionId');
    var sectionName = $(context).attr('sectionName');

    $('#saveCms').attr('onclick','editSavePage(this)');
    $('#saveCms').attr('sectionId',sectionId);

    var index = findIndexByKeyValue(GBL_PAGE_DATA,'sectionId',sectionId);
    
    var proposalContent = GBL_PAGE_DATA[index].sectionDetail;

    $('#pageName').val(sectionName);
    $('#pageName').attr('disabled',true);

    // $('.ng-pristine.ng-valid.ta-bind.ng-not-empty').html( proposalContent ); 
    tinyMCE.get('pageContentData').setContent(proposalContent);

    $('#pageContentModal').modal('show'); 
    addFocusId( 'pageName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}


//EDIT PAGE API DATA SEt FOR UPDATE CALL
var secIdforEdit = '';
function editSavePage(context){

    var pageName = $('#pageName').val();
    secIdforEdit = $(context).attr('sectionId');
    var sectionDetail = tinyMCE.get('pageContentData').getContent();

    if( pageName == " "){
        toastr.warning('Please Enter Page');
        $('#pageName').focus();
        return false;

    }else{
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addCmsData',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            orgId: checkAuth(14,57),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            sectionName : pageName,
            sectionDetail : sectionDetail,
            sectionId : secIdforEdit,
        }  

        $('#pageContentModal').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, editPageCallback,"Please Wait... Getting Dashboard Detail");
    }
}


//EDIT PAGE API DATA SEt FOR UPDATE CALLBACK
function editPageCallback(flag,data){

    if (data.status == "Success" && flag) {     
 
        // var index = findIndexByKeyValue( GBL_PAGE_DATA,'sectionId',secIdforEdit );
        // GBL_PAGE_DATA[index] = data.data[0];
        // renderPageData();
        getPageData();

        $('#saveCms').attr('onclick','createSection()');
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_SECTION_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

//DELETE PAGE API CALLIMG
var gblContext = "";
function deletePage(context){
 if( checkAuth(1, 2, 1) == -1 ){
    toastr.error('You are not Authorised for this Activity');
    return false;

}
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
       
        var sectionId = $(context).attr('sectionId');
        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'removePageSectionDetail',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            orgId: checkAuth(1,2),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            sectionId : sectionId,
        }  

        console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, deletePageCallback,"Please Wait... Getting Dashboard Detail");
    }
}


//DELETE PAGE API CALLIBACK
function deletePageCallback(flag,data){

    if (data.status == "Success" && flag) { 

        var sectionId = $(gblContext).attr('sectionId');
        toastr.success('Proposal Deleted Successfully.');

        var index = findIndexByKeyValue( GBL_PAGE_DATA,'sectionId',sectionId );

        GBL_PAGE_DATA.splice(index,1);

        renderPageData();       //RENDER PAGE DATA

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_Delete_PAGE_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}