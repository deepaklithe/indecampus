/*
 *
 * Created By : Sachin Jani.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : April 28 2016.
 * File : ActivityListing.
 * File Type : .js.
 * Project : activityMgmt
 *
 * */
var itemJson = [];
var itemJson = [];
var itemGroupJson = [];
var groupJson = [];
var temp_inq_Trans = [];
var ITEMS = [];
var customerListData = [];
var custListComma = [];
var oTable = [];
var custGroupJson;
var editSchedulerFlag = 0;

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

function openSmartModalActivity(){
     // START GETTING ETHNICITY FROM LOAD AUTH
    var loadAuthjson = JSON.parse(localStorage.indeCampusRoleAuth);

    resetPopups();
    setOption("0", "ethnicity", loadAuthjson.data.ethinicity, "---Select Ethnicity ---");
    $('#ethnicity').selectpicker();
    // setOption("0", "stateListing", loadAuthjson.data.cityStateListing, "--Select State --");
    // $("#stateListing").selectpicker();
    // $("#cityListing").selectpicker();

    // $("#stateListing").selectpicker('refresh');
    $('#ethnicity').selectpicker('refresh');
    AutoCompleteForItem("itemList", saveItemDetail);
    autoCompleteForGroup("groupListing", saveItemGroupDetail);
    getSalesYear();
    gerCustGroup();
    getCustomerData();
    
    $('input[type=radio][name="lytd"]').filter('[value=1]').click(); // set checked
    $('input[type=radio][name="rdoActModeSmart"]') .filter('[value=1]').click(); // set checked
    $('input[type=radio][name="singalradioassignSmart"]').filter('[value=0]').click(); // set checked
    $("#ddActAssignedToSmart").html(localStorage.POTGactivityAppAssignTypeUserOptionHtml).selectpicker('refresh');
    $("#ddActAssignedToSmart").selectpicker("refresh"); 

    $('[href="#tab1"]').tab('show');
    $('#ddActAssignedToSmart').val(localStorage.indeCampusActivityAppUserId).selectpicker('refresh');
    $('#creatactivitySmart').modal('show');
               
}

function populateCity(stateId){
    
    if(stateId == "-1"){
        setOptionCity("0", "cityListing", "", "--Select City --");
    }else{
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(data.data.cityStateListing,"ID",stateId);
       
        var cityListing = "";
        if(index != -1){
            cityListing  = (data.data.cityStateListing[index]['cityListing'] != "No record found") ? data.data.cityStateListing[index]['cityListing'] : "";
        }else{
            cityListing = "";
        }
        setOptionCity("0", "cityListing", cityListing, "--Select City --");
    }
    
    $("#cityListing").selectpicker("refresh");    
    $('html').click( function() {
        setTimeout(function(){
            if( $('.citySelect .btn-group.bootstrap-select').hasClass('open') ){
                $('#nextBtn').hide();
            }else{
                $('#nextBtn').show();
            }
        },100)
        
    });
}

function setOptionCity(idclass,id,data,defaultMsg){
    //idclass  = 0 (id)
    //idclass  = 1 (class)
    //log("caller is " + arguments.callee.caller.name);
    //log(data); return false;
    var innerhtml = "";
    if(defaultMsg.trim().length <= 0)
        innerhtml = "";
    else
        innerhtml += '<option value="-1" disabled>'+defaultMsg+'</option>' ;
        

    for(var i=0;i<data.length;i++){
        
        var selected = "";
        innerhtml += '<option '+selected+' value="'+data[i].ID+'">'+data[i].VALUE+'</option>' ;
    }
    //log("inner html"+innerhtml)
    if(idclass == 0){
        $("#"+id).html(innerhtml);
    }else{
        $("."+id).html(innerhtml);
    }
}

function AutoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#itemList').attr('itemId',item.value);
            //var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
            //$('#itemGroupText').val(ITEMS[itemIndex].itemGroupName);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getItemListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(4, 14),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                console.log(data.status);
                // // We good!
                ITEMS = [];
                if (data.status != "No record found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.itemId,
                            name: data.itemName,
                            data: data.itemName,
                            clientId: data.clientId,
                            itemId: data.itemId,
                            itemName: data.itemName,
                            itemPrice: data.itemPrice,
                            itemSubType: data.itemSubType,
                            itemType: data.itemType,
                            orgId: data.orgId,
                            validityEndDate: data.validityEndDate,
                            itemGroupName: data.itemGroupName
                        }
                        itemJson.push(tempJson);
                        ITEMS.push(tempJson);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}


function saveItemDetail(){

}

function autoCompleteForGroup(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#groupListing').attr('groupId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getGroupListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(13, 50),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMSGROUP= [];
                if (data.status != "No Item Group Found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.PK_ITEM_GROUP_ID,
                            name: data.GROUP_NAME,
                            data: data.GROUP_NAME,
                            clientId: data.FK_CLIENT_ID
                        }
                        itemGroupJson.push(tempJson);
                        ITEMSGROUP.push(tempJson);
                    });

                    return ITEMSGROUP;
                }
                console.log(data);
            }
        }
    });
}

function saveItemGroupDetail(){
    
}



function addItemRow(){
    
    var selectedItemName = $('#itemList').attr('itemId');
    
    
    if((selectedItemName == '' || selectedItemName == undefined)){
        toastr.warning('Please select Item from Autocomplete');
        $('#itemList').focus();
        return false;

    }else{
        if(selectedItemName != "" && selectedItemName != undefined){
            var checkValidationIndex = findIndexByKeyValue(temp_inq_Trans,"itemId",selectedItemName);
            if(checkValidationIndex == "-1"){
                var index = findIndexByKeyValue(itemJson,"itemId",selectedItemName);
                var  tempData = {
                    itemId : itemJson[index].itemId,
                    itemName : itemJson[index].itemName,
                    groupName : '',
                    itemType : "ITEM",
                }
                temp_inq_Trans.push(tempData);
                addRowTable( temp_inq_Trans );
            }else{
                toastr.warning('Oops. Item is already selected.');
                $('#itemList').focus();
            }
            
        }
        
        // $('.dataTables_empty').hide();
        // $('#itemList').append(newRow);

        // $("#tableListing").DataTable();

        $('#itemList').val('');
        $('#itemList').attr('itemId','');
        
    }
    
}

function addGroupRow(){
    
    
    var selectedGroupName = $('#groupListing').attr('groupid');
    
    if((selectedGroupName == '' || selectedGroupName == undefined)){
        toastr.warning('Please select Item from Autocomplete');
        $('#groupListing').focus();
        return false;

    }else{
        if(selectedGroupName != "" && selectedGroupName != undefined){
            var checkValidationIndex = findIndexByKeyValue(temp_inq_Trans,"itemId",selectedGroupName);
            if(checkValidationIndex == "-1"){
                var index = findIndexByKeyValue(itemGroupJson,"id",selectedGroupName);
                var  tempData = {
                    itemId : itemGroupJson[index].id,
                    itemName : "",
                    groupName : itemGroupJson[index].name,
                    itemType : "GROUP",
                }
                temp_inq_Trans.push(tempData);
                addRowTable( temp_inq_Trans );
            }else{
                toastr.warning('Oops. Group is already Selected.');
                $('#groupListing').focus();
            }
            
        }
        
       
        $('#groupListing').val('');
        $('#groupListing').attr('groupid','');

        
    }
    
}
function addRowTable(itemJson){

    var thead = '<table id="tableItemListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Item Name</th>'+
                            '<th>Group Name</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    for ( var i=0; i<itemJson.length; i++ ){

        newRow += '<tr>'+
                    '<td>'+ (i+1) +'</td>'+
                    '<td>'+ itemJson[i].itemName +'</td>'+
                    '<td>'+ itemJson[i].groupName +'</td>'+
                    '<td>'+
                        '<button item-id="'+ itemJson[i].itemId +'" type="button" class="btn btn-primary btn-ripple btn-sm btn-danger" onclick="removeRow(this)"><i class="fa fa-trash-o"></i></button>'+
                    '</td>'+
                '</tr>';
    }

    $('#itemGroupSelection').html(thead + newRow + tfoot);
    // TablesDataTables.init();
    // TablesDataTablesEditor.init();
    $("#tableItemListing").DataTable({ "stateSave": true });
}


function gerCustGroup(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userCustomerGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,66),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        searchType : "CUSTOMER"
    }

    commonAjax(FOLLOWUPURL, postData, gerCustGroupCallback,"Please Wait... Getting Dashboard Detail");
}


function gerCustGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        
        var newRow = '';
        custGroupJson = data.data;
        for( var i=0; i<data.data.length; i++ ){
            var name = data.data[i].groupName;
            var groupId = data.data[i].groupId;
			
            newRow += '<option value="'+ groupId +'"> '+ name +' </option>';
        }
        $('#customerGroup').html(newRow);
        $('#customerGroup').select2();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
    checkEditScheduler();
}

function getCustomerData(){

    // var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // // Followup Listing Todays, Delayed, Upcomming
    // var postData = {
    //     requestCase: 'getCustomerDetailBySearch',
    //     clientId: tmp_json.data[0].FK_CLIENT_ID,
    //     userId: tmp_json.data[0].PK_USER_ID,
    //     orgId: checkAuth(3,10),
    //     dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    // }

    // commonAjax(FOLLOWUPURL, postData, getCustomerDataCallback,"Please Wait... Getting Dashboard Detail");

    $("#customerDetail").select2({
        minimumInputLength: 3,
        multiple :true,
        tags: [],
        ajax: {
            url: FOLLOWUPURL,
            dataType: 'json',
            type: 'POST',
            quietMillis: 50000000,
            allowClear: true,
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            id: function(object) {
                return object.text;
            },
            createSearchChoice:function(term, data) {
                if ( $(data).filter( function() {
                    return this.text.localeCompare(term)===0;
                }).length===0) {
                    return {id:term, text:term};
                }
            },
            data: function (term) {
                var tempJson  = {
                    keyword: term,
                    requestCase: 'getCustomerDetailBySearch',
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    orgId: checkAuth(3,10),
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
                };
                return {postData : tempJson};
            },
            processResults: function (data, page) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
                var itemJson = [];
                if( data.status != "No record found" ){
                    
                    console.log(data.data);
                    console.log(page);
                    for(var f=0;f<data.data.length;f++){
                        var items = {
                            id: data.data[f].CustId,
                            text : data.data[f].CustomerInfo.Name
                        }
                        itemJson.push(items);
                    }
                    console.log(itemJson);
                    setTimeout(function(){
                        $('.select2-results__options li:first-child').remove();
                    },100);
                }else{
                    setTimeout(function(){
                        $('.select2-results__options li:first-child').remove();
                        $('.select2-results__options').html('<li role="treeitem" aria-live="assertive" class="select2-results__option select2-results__message"> No results found </li>');
                    },100);
                }
                

                return {results: itemJson};
            }
        }
    });
}

function getCustomerDataCallback(flag,data){
    if (data.status == "Success" && flag) {
        var newRow = '';

        GBL_CUSTOMER_MASTER = data.data;
   //      for( var i=0; i<data.data.length; i++ ){

   //          var customerDetail = data.data[i]['CustomerInfo'];
			// newRow += '<option value="'+ customerDetail['CustId'] +'|--|'+ customerDetail['Name'] +'"> '+ customerDetail['Name'] +' </option>';
   //      }

        // $('#customerDetail').html(newRow);
        // $("#customerDetail").select2();
    }else {
        if (flag)
            toastr.error(data.status);
        else
            toastr.error(SERVERERROR);
    }
}

function getSalesYear(){

    var currentYear = new Date().getFullYear()
    var tempArr = [];
    for(var i=0;i<10; i++){
        var tempJson = {
            ID : currentYear-i,
            VALUE : currentYear-i
        }
        tempArr.push(tempJson);
    }
    setOption("0", "salesYear", tempArr, "--Select Year --");
    $("#salesYear").selectpicker("refresh");
}

function removeRow(context){

    var itemId = $(context).attr('item-id');

    var index = findIndexByKeyValue(temp_inq_Trans,"itemId",itemId);    

    $(context).parents('tr').remove();
    temp_inq_Trans.splice(index,1);

    addRowTable( temp_inq_Trans );
}

var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
var assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';
    
$('input[type=radio][name="singalradioassignSmart"]').off().on('change', (function (e) {
    if (this.value == '0') {
        //user //single
        $("#ddActAssignedToSmart").html(localStorage.POTGactivityAppAssignTypeUserOptionHtml).selectpicker('refresh');

    } else if (this.value == '1') {
        //group
        $("#ddActAssignedToSmart").html(localStorage.POTGactivityAppAssignTypeGroupOptionHtml).selectpicker('refresh');
    }
    // $("#ddActAssignedToSmart").selectpicker("refresh");

}));

function prevBtnClik(){
    var currTab = $("#formCreateActSmart .tab-content .tab-pane.active").attr('id');
    $('#schedulerBtn').hide();
    $('#campaignBtn').hide();
    if( currTab == 'tab3' ){
        $('#schedulerBtn').show();
        $('#campaignBtn').show();
    }
}

function getCustomerList(){

    var currTab = $("#formCreateActSmart .tab-content .tab-pane.active").attr('id');

    $('#schedulerBtn').hide();
    $('#campaignBtn').hide();
    if( currTab == 'tab1' ){
        $('#schedulerBtn').show();
        $('#campaignBtn').show();
        addFocusId( 'salesYear' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    
    }else if( currTab == 'tab2' ){

        var ethnicity = $('#ethnicity').val();
        var customerGroup = $('#customerGroup').val();
        var countryListing = $('#custCountryId').val();
        var stateListing = $('#custStateId').val();
        var cityListing = $('#custCityId').val();

        var birthdate = $('#birthdate').val();
        var birthdays = $('#birthdays').val();
        var anniversaryDate = $('#anniversaryDate').val();
        var anniversaryDays = $('#anniversaryDays').val();
        var customerDetail = $('#customerDetail').val();
        var salesYear = $('#salesYear').val();
        var salesAmtFrom = $('#salesAmtFrom').val();
        var salesAmtTo = $('#salesAmtTo').val();
        var custTypeList = $('#custTypeList').val();
        var lytd = $('input[type=radio][name="lytd"]').filter(":checked").val();
        // 1- ltd 2 - ytd
        var ltdytdAmt = $('#ltdytdAmt').val();
        
        var catchArea = $("#catchArea").val();
        var notSelected = "0";

        /// CODE REMAIN FROM HERE
        var sendFlag = "";

        if( ethnicity != "-1" ){
            sendFlag = "true";

        }if( custTypeList != "-1" ){
            sendFlag = "true";

        }else if( customerGroup != null ){
            sendFlag = "true";

        }else if( countryListing != "-1" ){
            sendFlag = "true";

        }else if( stateListing != null &&  stateListing != "-1" ){
            sendFlag = "true";

        }else if( cityListing != null &&  cityListing != "-1" ){
            sendFlag = "true";

        }else if( birthdate != "" ){
            sendFlag = "true";

        }else if( anniversaryDate != "" ){
            sendFlag = "true";

        }else if( catchArea != "" ){
            sendFlag = "true";
            
        }else if( customerDetail != null ){
            sendFlag = "true";

        }else if( salesAmtFrom != "" ){
            sendFlag = "true";

        }else if( salesAmtTo != "" ){
            sendFlag = "true";

        }else if( ltdytdAmt != "" ){
            sendFlag = "true";

        }else if( temp_inq_Trans.length != 0 ){
            sendFlag = "true";

        }

        if( birthdays != "" && birthdate == "" ){
            toastr.warning("Please Select birthdate along with birth days");
            $('#prevBtn').click();
            $('#birthdate').datepicker('show');
            return false;
        }
        if( anniversaryDays != "" && anniversaryDate == "" ){
            toastr.warning("Please Select anniversaryDate along with anniversary days");
            $('#prevBtn').click();
            $('#anniversaryDate').datepicker('show');
            return false;
        }

        var cityList = "";

        if( cityListing != null ){
            for( var j=0; j<cityListing.length; j++ ){
                if( cityList.length == 0){
                    cityList += cityListing[j] ;
                }else{
                    cityList += "," + cityListing[j] ;
                }
            }
        }

        if( salesAmtFrom != "" && salesAmtFrom != "0" && salesAmtTo != "" && salesAmtTo != "0" ){
            if( parseInt( salesAmtFrom ) >= parseInt( salesAmtTo ) ){
                toastr.warning("Sales Amount From must be less than Sales Amount To");
                $('#prevBtn').click();
                $("#salesAmtFrom").focus();
                return false;
            }

        }

        if( sendFlag != "true" ){
            notSelected = "1";
        }
            // toastr.warning('Please Select Atleast One Smart Option');
            // customerListData = [];
            renderCustomerList();
            // $("#ethnicity").focus();

            // $('#prevBtn').click();
            // return false;

        // }else{

            console.log("Validation Passed");
            console.log( sendFlag );

            var customerGrp = "";

            if( customerGroup != null ){
                for( var j=0; j<customerGroup.length; j++ ){
                    if( customerGrp.length == 0){
                        customerGrp += customerGroup[j] ;
                    }else{
                        customerGrp += "," + customerGroup[j] ;
                    }
                }
            }

            var customerNm = "";

            if( customerDetail != null ){
                for( var k=0; k<customerDetail.length; k++ ){
                    if( customerNm.length == 0){
                        customerNm += customerDetail[k].split('|--|')[0] ;
                    }else{
                        customerNm += "," + customerDetail[k].split('|--|')[0] ;
                    }
                }
            }

            if( ethnicity == "-1" ){
                ethnicity = ""
            }
            if( stateListing == "-1" ){
                stateListing = ""
            }
            if( custTypeList == "-1" ){
                custTypeList = ""
            }
            if( birthdate != "" ){
                birthdate = ddmmyyToMysql(birthdate);
            }
            if( anniversaryDate != "" ){
                anniversaryDate = ddmmyyToMysql(anniversaryDate);
            }
            if( cityListing == "-1" || cityListing == "" ){
                cityListing = ""
            }

            var itemList = "";
            var groupList = "";
            
            if( temp_inq_Trans.length != 0 ){
                for( var i=0; i<temp_inq_Trans.length; i++ ){
                    if( temp_inq_Trans[i].itemType == "ITEM" ){
                        if( itemList.length == 0){
                            itemList += temp_inq_Trans[i].itemId ;
                        }else{
                            itemList += "," + temp_inq_Trans[i].itemId ;
                        }
                    }else{
                        if( groupList.length == 0){
                            groupList += temp_inq_Trans[i].itemId ;
                        }else{
                            groupList += "," + temp_inq_Trans[i].itemId ;
                        }
                    }
                }
            }

            /* generate postData to submit activity START */
            var requestCase = "getCustomerForSmartAct";
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

            var postData = {
                requestCase : "getCustomerForSmartAct",
                orgId: checkAuth(2,6),
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId : localStorage.indeCampusBranchIdForChkAuth,
                ethnicity : ethnicity,
                customerGroup : customerGrp,
                countryId : countryListing,
                state : stateListing,
                city : cityList,
                birthdate : birthdate,
                birthdays : ( birthdays ) == ""  ? 0 : birthdays,
                anniversaryDate : anniversaryDate,
                anniversaryDays : ( anniversaryDays) == "" ? 0 : anniversaryDays,
                customerName : customerNm,
                salesYear : salesYear,
                salesAmtFrom : salesAmtFrom,
                salesAmtTo : salesAmtTo,
                custType : custTypeList,
                lytd : lytd,
                ltdytdAmt : ltdytdAmt,
                itemId : itemList,
                groupId : groupList,
                catchArea : catchArea, // Catchment Area
                notSelected : notSelected, // not selected any filter = 1 , any selected = 0
            };
            console.log(postData);
            // return false;
            commonAjax(ACTURL, postData, getCustomerDataCallBack,"");
        // }

    }else if( currTab == 'tab3' ){
        custListComma = "";
        var allPages = oTable.fnGetNodes();
        $('.custChk', allPages).each(function(){
            var currCheck = $(this).prop('checked');
            var checkId = $(this).attr('id');
            
            if( currCheck ){
                if( custListComma == ""){
                    custListComma += checkId;
                }else{
                    custListComma += ','+checkId;
                }
            }
        });
        console.log(custListComma);
        
        if( custListComma == "" ){
            toastr.warning('Please Select Atleast One Customer');
            $('#prevBtn').click();
            $('#itemList').focus();
            $('#schedulerBtn').hide();
            $('#campaignBtn').hide();
            return false;
        }

        if( actModeHideShow( "1" ) == "-1" ){  //  HIDE SHOW ACTIVITY MODE ACCORDING TO ROLE AUTH RIGHTS
            toastr.warning( NOACTMODERIGHTS );
            $('#prevBtn').click();
            $('#itemList').focus();
            $('#schedulerBtn').hide();
            $('#campaignBtn').hide();
            return false;
        }

        addFocusId( 'titleSmart' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

function getCustomerDataCallBack(flag,data){

    if (data.status == "Success" && flag) {

        customerListData = data.data;
        renderCustomerList();

        if( localStorage.POTGactivityAppAssignTypeGroupOptionHtml == "" || localStorage.POTGactivityAppAssignTypeGroupOptionHtml == undefined || localStorage.POTGactivityAppAssignTypeUserOptionHtml == "" || localStorage.POTGactivityAppAssignTypeUserOptionHtml == undefined ){
            getUserGroup();
        }

    } else {
        customerListData = [];
        renderCustomerList();
        if (flag){ 
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CUST_DATA );
            $('#prevBtn').click();
            $('#itemList').focus();
        }
        else{   
            toastr.error(SERVERERROR);
        }
    }
}

function getUserGroup(){

    var postData = {
        requestCase: "getUserCustomerGroup",
        userId: localStorage.indeCampusActivityAppUserId,
        clientId: localStorage.indeCampusActivityAppClientId,
        orgId: checkAuth(2,6),
    };



    function callBackGetUserCustomerGroupData(flag, results) {
        if (results.status == SCS) {

            custGrpArr = results.data.customerGroupList;
            userGrpArr = results.data.userGroupList;
            assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';

            if (custGrpArr)
            {
                for (var i = 0; i < custGrpArr.length; i++)
                {
                    custGrpArr[i].label = custGrpArr[i].GROUP_NAME;
                    custGrpArr[i].text = custGrpArr[i].GROUP_NAME;
                    custGrpArr[i].value = custGrpArr[i].PK_GROUP_ID;
                    custGrpArr[i].grpid = custGrpArr[i].PK_GROUP_ID;
                }


            }

            if (userGrpArr)
            {

                for (var i = 0; i < userGrpArr.length; i++)
                {
                    userGrpArr[i].label = userGrpArr[i].GROUP_NAME;
                    userGrpArr[i].text = userGrpArr[i].GROUP_NAME;
                    userGrpArr[i].value = userGrpArr[i].PK_GROUP_ID;
                    userGrpArr[i].grpid = userGrpArr[i].PK_GROUP_ID;

                    var grpid = userGrpArr[i].value;
                    var grpName = userGrpArr[i].label;

                    assignTypeGroupOptionHtml += '<option value="' + grpid + '">' + grpName + '</option>';
                }


            }

            getUserList();      //Getting User List

        }
        else {

            //displayAttendenceListin(results);
            console.log("====== FAIL ======");
            if (flag) {
                console.log(results.status);
            } else {
                //console.log("====== No Records Found ======");
            }
        }

        /* if success then arrays will data else make callBack with blank data */
        var dataJson = {
            custGrpArr: custGrpArr,
            userGrpArr: userGrpArr,
            assignTypeGroupOptionHtml: assignTypeGroupOptionHtml,
        };

        /* caching/storing the user/cust grp listing array in localStorage for further use START */
        localStorage.POTGactivityAppUserGrpJson = JSON.stringify(userGrpArr);
        localStorage.POTGactivityAppCustGrpJson = JSON.stringify(custGrpArr);
        localStorage.POTGactivityAppAssignTypeGroupOptionHtml = assignTypeGroupOptionHtml;

        /* caching/storing the user/cust grp listing array in localStorage for further use END */


        // callBack(dataJson);

    }

    var actAjaxMsg = "Getting User Group";
    var actUrl = gbl_ActivityAppGlobals.URLV;
    commonAjax(actUrl, postData, callBackGetUserCustomerGroupData, actAjaxMsg);
    // commonAjaxActiivity(actUrl, postData, callBackGetUserCustomerGroupData, actAjaxMsg);
    
}

function getUserList(){

    var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD
    var postData ={
        requestCase : "userListingActivityModule",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(19,74),
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    function callBackGetUserSingleData(flag, results) {
        if (results.status == SCS) {

            userListArr = results.data;

            if (userListArr)
            {
                for (var i = 0; i < userListArr.length; i++)
                {
                    userListArr[i].label = userListArr[i].fullName;
                    userListArr[i].text = userListArr[i].fullName;
                    userListArr[i].value = userListArr[i].userId;
                    userListArr[i].custId = userListArr[i].userId;

                    var userId = userListArr[i].value;
                    var userName = userListArr[i].label;
                    
                    assignTypeUserOptionHtml += '<option value="' + userId + '">' + userName + '</option>';
                }
                
                localStorage.POTGactivityAppAssignTypeUserOptionHtml = assignTypeUserOptionHtml;
                localStorage.POTGactivityAppUserListJson = JSON.stringify(userListArr);
                $("#ddActAssignedToSmart").html(localStorage.POTGactivityAppAssignTypeUserOptionHtml).selectpicker('refresh');
                $('#ddActAssignedToSmart').val(localStorage.indeCampusActivityAppUserId).selectpicker('refresh');
            }
        }
        else {

            //displayAttendenceListin(results);
            console.log("====== FAIL ======");
            if (flag) {
                console.log(results.status);
            } else {
                //console.log("====== No Records Found ======");
            }
        }
        
        /* caching/storing the user/cust grp listing array in localStorage for further use START */
    }
    var actAjaxMsg = "Getting User Group";
    commonAjax(URL2, postData, callBackGetUserSingleData, actAjaxMsg);
        
}

function renderCustomerList(){

    var thead = '<table id="custListTable" class="display datatables-alphabet-sorting" style="border:0px;">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Name</th>'+
                            '<th>Number</th>'+
                            '<th>Last Activity</th>'+
                            '<th>Total Inquiry</th>'+
                            '<th>Total Order</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';

    var tfoot = '</tbody></table>';

    var newRow = '';

    if( customerListData != "No record found"){

        var index = 0;
        customerListData.forEach(function(record,index) {   

            newRow += '<tr>'+
                        '<td>'+ (index+1) +'</td>'+
                        '<td><div class="checkboxer">'+
                                '<input type="checkbox" class="custChk" id="'+ record.custId +'"/>'+
                                '<label for="'+ record.custId +'">'+ record.custName +'</label>'+
                        '</td>'+
                        '<td>'+ record.custNumber +'</td>'+
                        '<td>'+ record.lastActDate +'</td>'+
                        '<td>'+ record.totalInqCount +'</td>'+
                        '<td>'+ record.totalOrderCount +'</td>'+
                       '</tr>';
        });
    }
    $('#custListHtml').html(thead + newRow + tfoot);
    oTable = $('#custListTable').dataTable({
        stateSave: true
    });

    $('#ddActAssignedToSmart').val(localStorage.indeCampusActivityAppUserId).selectpicker('refresh');
    
}


function createSmartActivity(){

    if( localStorage.indeCampusActivityAppUserId == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }
    
    //Activity Last page form component
    var acttitle = $("#titleSmart").val();
    var actdesc = $("#descriptionSmart").val();
    var actMode = $('input[type=radio][name="rdoActModeSmart"]').filter(":checked").val();
    var actDueDate = $("#dueDateSmart").val();
    var isUserGroup = $('input[type=radio][name="singalradioassignSmart"]').filter(":checked").val();
    var assigneTo = $("#ddActAssignedToSmart").val();
    var actDueTime = "";
    var remindFlag = $('#reminderActChk').is(':checked');
    var reminderDate = $('#reminderDate').val();
    var reminderTime = "";
    var actTypeSch = $('#actTypeListSch').val();
   
    var actNormEndDate = $('#txtActNormEndDate').val();
    var actNormEndTime = "";

    if( customerListData.length == 0 ){
        toastr.warning('No Customer were Found in Your Search Criteria');
        return false;

    }else if( custListComma.length == 0 ){
        toastr.warning('Please Select Atleast One Customer');
        return false;

    }else if( acttitle.length == 0 ){
        toastr.warning('Please Enter Actvity Title');
        $("#titleSmart").focus();
        return false;

    }else if( actdesc.length == 0 ){
        toastr.warning('Please Enter Actvity Description');
        $("#descriptionSmart").focus();
        return false;
    
    }else if( actDueDate == "" ){
        toastr.warning('Please Enter Actvity Due Date/Time');
        $("#dueDateSmart").focus();
        return false;

    // }else if( actDueTime == "" ){
    //     toastr.warning('Please Enter Actvity Due Time');
    //     $("#dueTime").focus();
    //     return false;
    
    // }else if( !validateDateTimeFormat(actDueTime) ){
    //     toastr.warning('Please Select Proper Due Time eg:HH:MM (AM/PM) like 10:00 AM');
    //     return false;

    }else if( actNormEndDate == "" ){
        toastr.warning('Please Enter Actvity End Date/Time');
        $("#txtActNormEndDate").focus();
        return false;

    // }else if( actNormEndTime == "" ){
    //     toastr.warning('Please Enter Actvity End Time');
    //     $("#endNormTime").focus();
    //     return false;
    
    // }else if( !validateDateTimeFormat(actNormEndTime) ){
    //     toastr.warning('Please Select Proper End Time eg:HH:MM (AM/PM) like 10:00 AM');
    //     return false;

    }else if ( remindFlag  && !reminderDate ){
        toastr.warning("Please Select the Reminder Date/Time");
        $('#reminderDate').focus();
        return false;

    // }else if ( remindFlag && !reminderTime ){
    //     toastr.warning("Please Select the Reminder Time");
    //     $('#reminderTime').focus();
    //     return false;

     // }else if( remindFlag && !validateDateTimeFormat(reminderTime) ){
     //    toastr.warning('Please Select Proper reminder Time eg:HH:MM (AM/PM) like 10:00 AM');
     //    return false;

    }else if( isUserGroup != 2 && assigneTo == "-1"){
        toastr.warning('Please Select Assigned Person');
        $("#ddActAssignedToSmart").focus();
        return false;

    }else{

        actDueDate = (($('#dueDateSmart').val() ).split('|')[0]).trim();
        actDueTime = (($('#dueDateSmart').val() ).split('|')[1]).trim();

        actNormEndDate = (($('#txtActNormEndDate').val() ).split('|')[0]).trim();
        actNormEndTime = (($('#txtActNormEndDate').val() ).split('|')[1]).trim();

        if ( remindFlag && !reminderTime ){
            reminderDate = (($('#reminderDate').val() ).split('|')[0]).trim();
            reminderTime = (($('#reminderDate').val() ).split('|')[1]).trim();
        }

        console.log("Validation Passed");
       
        if( assigneTo == "-1" ){
            assigneTo = ""
        }
        
        var dueStart = new Date( ddmmyyToMysql( actDueDate ) +' '+ actDueTime );
        var currTime = new Date( );
        var remindStart = new Date( ddmmyyToMysql( reminderDate ) +' '+ reminderTime );

        var actEnd = new Date( ddmmyyToMysql( actNormEndDate ) +' '+ actNormEndTime );

        if( +dueStart < +currTime ){
            toastr.warning("Activity Due Date/Time should not be less than Current Date/Time");
            $('#txtActDueDate').datetimepicker('show');
            return false;

        }else if( +actEnd < +dueStart ){
            toastr.warning("Activity End Date/Time should not be less than Due Date/Time");
            $('#txtActNormEndDate').datetimepicker('show');
            return false;

        }
        if (remindFlag){ 
            
            if( +dueStart < +currTime ){
                toastr.warning("Activity Due Date/Time should not be less than Current Date/Time");
                $('#dueDateSmart').datetimepicker('show');
                return false;

            }else if( +dueStart < +remindStart ){
                toastr.warning("Reminder Date/Time should not be greater than Activity Due Date/Time");
                $('#dueDateSmart').datetimepicker('show');
                return false;

            }else if( +remindStart < +currTime ){ //dateActEndDate dateActStartDate
                toastr.warning("Reminder Date/Time should not be less Current Date/Time");
                $('#reminderDate').datetimepicker('show');
                return false;

            } 
        }

        var remindTimeSend = ddmmyyToMysql( reminderDate ) +'|'+reminderTime;
        if( !remindFlag ){
            remindTimeSend = "";
        }

        //Activity Type data gathering Code ends here Nazim khan

        /* generate postData to submit activity START */
        var requestCase = "createSmartActivity";
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase: requestCase,
            orgId: checkAuth(2,5),
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            actTitle: acttitle,
            actDesc: actdesc,
            actMode: actMode,
            orgId : localStorage.indeCampusBranchIdForChkAuth,
            actDueDate: ddmmyyToMysql(actDueDate,'yyyy-MM-dd'),
            isUserGroup: isUserGroup,
            assigneTo: assigneTo,
            customerIds: custListComma,
            actDueTime: actDueTime,
            reminderTime : remindTimeSend,
            refId: "",
            actId: "",
            actType : actTypeSch, // activty type from role auth
            custType : "SINGLE", // Customer type
            actEndDate : ddmmyyToMysql( actNormEndDate ),
            actEndTime : actNormEndTime,
        };
        console.log(postData);
        /* generate postData to submit activity END */
        // return false;
        postData = JSON.stringify(postData);
        // $("#postDataActivtySmart").val(JSON.stringify(postData));
        $("#postDataActivtySmart").val(postData);
        // console.log(postData); return false;
        $("#formCreateActSmart").submit();
    }
}


$("#formCreateActSmart").off().on('submit',(function (e) {
    e.preventDefault();
    addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
    $.ajax({
        url: ACTURL, // Url to which the request is send
        type: "POST", // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
        contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
        cache: false, // To unable request pages to be cached
        processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
        headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
        beforeSend: function () {
            //alert(bfsendMsg);
            //$("#msgDiv").html(bfsendMsg);
            addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            $("#ajaxloader").removeClass("hideajaxLoader");

        },
        success: function (data)// A function to be called if request succeeds
        {
            data = JSON.parse(data);
            console.log(data);
            addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
            if (data.status == SCS) {

                var msg = 'Your Smart Activity is Successfully Created.';

                toastr.success( msg );
                navigateToactivity();

            } else {
                toastr.warning(data.status);

            }
            $("#ajaxloader").addClass("hideajaxLoader");
        },
        error: function (jqXHR, errdata, errorThrown) {
            log("error");
            addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
            $("#ajaxloader").addClass("hideajaxLoader");
        }
    });
}));

function resetPopups(){

    $('#birthdate').val('');
    $('#birthdays').val('');
    $('#anniversaryDate').val('');
    $('#anniversaryDays').val('');
    $('#catchArea').val('');
    $('#gram').val('');
    $('#carat').val('');
    $('#ethnicity').val('-1');
    $('#ethnicity').selectpicker('refresh');
    $('#custCountryId').val('-1');
    $('#custCountryId').selectpicker('refresh');
    $('#custStateId').val('-1');
    $('#custStateId').selectpicker('refresh');
    $('#custCityId').val('-1');
    $('#custCityId').selectpicker('refresh');
    $('#salesYear').val('-1');
    $('#salesYear').selectpicker('refresh');
    $('#salesAmtFrom').val('');
    $('#ltdytdAmt').val('');
    $('#ltdytdAmt').val('');
    $('#itemList').val('');
    $('#groupListing').val('');
    temp_inq_Trans = [];
    // addItemRow(temp_inq_Trans);
    $('#titleSmart').val('');
    $('#descriptionSmart').val('');
    $('#dueDateSmart').val('');
    $('#txtActStartDate').val('');
    $('#txtActEndDate').val('');
    $('#txtActTitle').val('');
    $('#txtActDesc').val('');
    $('#ddActAssignedToSmart').val('-1').selectpicker('refresh');
    // $('#ddActAssignedToSmart').selectpicker('refresh');
    $('#ddActAssignedTo').val('-1');
    $('#scheduleAct').prop('checked',false);
    $('.scheduledActBlk').hide();
    $('#txtActDueDate').val('');
    // $('#dueDateSmart').val('');
    $('#ddActAssignedTo').val('-1');
    // $('#ddActAssignedTo').selectpicker('refresh')
    $('.celebrate').hide();
    $('#celebration').prop('checked',false);
    $('#specGift').prop('checked',false);
    $('#celebrationSch').prop('checked',false);
    $('#specGiftSch').prop('checked',false);
    $('#catchArea').attr('catchId','');

    $('#reminderActChk').prop('checked',false);
    $('.reminderActChkCls').hide();
    $('#reminderDate').val('');
    $('#reminderTime').val('');
}

function autoCompleteForCatchment(id, callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var index = findIndexByKeyValue(ITEMS, "id", item.value);

            setTimeout(function(){
                $('#catchArea').attr('catchId',item.value);
            },50)
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getCatchmentAreaByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(24, 91),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data, function (data) {
                        var group = {
                            id: data.ID,
                            name: data.VALUE,
                            data: data.ID + " " + data.VALUE,
                        };
                        ITEMS.push(group);
                    });
                    return ITEMS;
                }

            }
        }
    });
}

function saveCatch(){

}

function selectAllFun(){

    var allPages = oTable.fnGetNodes();
    $('.custChk', allPages).prop('checked',true);
}

function deselectAllFun(){
    var allPages = oTable.fnGetNodes();
    $('.custChk', allPages).prop('checked',false);
}

function setRemindDate(actDate){
    $('#reminderDate').val( actDate );
    $(".remindDate").datetimepicker('remove');

    var actTrimDate = (( actDate ).split('|')[0]).trim();
    var actTime = (( actDate ).split('|')[1]).trim();

    $('.remindDate').datetimepicker({
        // startDate: 'today',
        // endDate: actDate,
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        endDate: new Date( ddmmyyToMysql( actTrimDate ) +" "+ actTime ),
        autoclose: true,
        minuteStep: 15,
    });
    $('.remindDate').prop("readonly",true);

    if( $('input[type=radio][name="rdoActModeSmart"]').filter(":checked").val() == "4" ){    // ADD 60 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#dueDateSmart').val() , 60 )
    }else{    // ADD 15 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#dueDateSmart').val() , 15 )
    }
    
    callActListFun( 'dueDateSmart' );
}

function pageInitialEvents(){

    $('#salesAmtFrom').attr('disabled',true);
    $('#salesAmtTo').attr('disabled',true);
    //hide sales data if not selected sales year
    
    $('#dueTime').clockface();
    $('#endNormTime').clockface();
    $('#reminderTime').clockface();
    
    initCommonFunction();

    openSmartModalActivity();

    initBootStrapDatePicker();
    addRowTable(itemJson);
    
    FormsWizard.init();

    var currTime = formatAMPMRoundOff( new Date() );
    
    var d = new Date();
    var remindTime = d.setMinutes(d.getMinutes() - 20);
    remindTime = formatAMPMRoundOff( new Date(remindTime) );

    $('#dueTime').val( currTime );
    $('#endNormTime').val( currTime );
    $('#reminderTime').val( remindTime );

    $('#dueTime').on('pick.clockface', function() 
    {
        var d = new Date('01-01-1970 '+$('#dueTime').val());
        var remindTime = d.setMinutes(d.getMinutes() - 20);
        remindTime = formatAMPMRoundOff( new Date(remindTime) );
        $('#reminderTime').val( remindTime );
    });


    $('#salesYear').change(function(){
        if($(this).val() == "-1"){
            $('#salesAmtFrom').attr('disabled',true);
            $('#salesAmtTo').attr('disabled',true);
            $('#salesAmtFrom').val("");
            $('#salesAmtTo').val("");
        }else{
            $('#salesAmtFrom').attr('disabled',false);
            $('#salesAmtTo').attr('disabled',false);
        }
    });

   
    $('input[type=radio][name=singalradioassignSmart]').change(function() {
        if (this.value == '2') {
            $('.defaultSelect').hide();
        }
        else {
            $('.defaultSelect').show();
        }
    });

    $('#reminderActChk').change(function() {
        if($(this).is(":checked")) {
            $('.reminderActChkCls').show();
        }else{
            $('.reminderActChkCls').hide();
        } 
    });

    $('input[name=rdoActModeSmart]').change(function(){

        if($(this).val() == "1"){
            $('#titleSmart').val('Call');

        }else if($(this).val() == "2"){
            $('#titleSmart').val('Send Email');

        }else if($(this).val() == "3"){
            $('#titleSmart').val('Send SMS');

        }else if($(this).val() == "4"){
            $('#titleSmart').val('Meet');

        }else {
            $('#titleSmart').val('');
        }
    });

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var actType = roleAuth.data.actType;

    
    setOption("0", "actTypeListSch", actType, "");
    $('#actTypeListSch').selectpicker( 'refresh' );

    setOption("0", "schedulerTypeListSch", actType, "");
    $('#schedulerTypeListSch').selectpicker( 'refresh' );

    setOption("0", "campaignActTypeList", actType, "");
    $('#campaignActTypeList').selectpicker( 'refresh' );

    setOption("0", "custTypeList", roleAuth.data.customerTypeFlag, "---Select Customer Type---");
    $('#custTypeList').selectpicker( 'refresh' );

    $('#titleSmart').val('Call');

    var optSchFreq = "";
    SCHEDULER_FREQUENCY.forEach( function( record , index ){
        optSchFreq += "<option value="+ record.value +">"+ record.label +"</option>";
    });
    $('#scheduleFrequency').html( optSchFreq );
    $('#scheduleFrequency').selectpicker( "refresh" );


    if( roleAuth.data.clientTypeId == "32" && checkAuth(50,200) != "-1" ){

        assignProjectChangeEvent();
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getRealEstateMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(50,200),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }

        commonAjax(COMMONURL, postData, projectDataCallBack,INQUIRYDETAILGET);

        function projectDataCallBack(flag,data) {

            if(data.status == "Success" && flag){

                $('#projListActDiv').show();
                
                var options = '<option value=""> --Please Select Project -- </option>';

                if( data.data != "No record found" ){
                    data.data.forEach(function( record , index ){
                        options += '<option value="'+ record.projectId +'"> '+ record.projectName +' </option>';
                    });
                }

                $('#projectListAct').html( options );
                $('#projectListAct').selectpicker();

                $('#projectListAct').val('-1');
                $('#projectListAct').selectpicker('refresh'); 

            }else{
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_PROJECT );
                else
                    toastr.error(SERVERERROR);
            }
        }

    }

    $('#customerDetail').focus(); 
    addFocusId( 'customerDetail' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    setCountryList( 'custCountryId' );   // SET COUNTRY LISTING FROM LOAD AUTH SET DATA IN SELECT PICKER USING ID
    $('#custCountryId').selectpicker('refresh');
    $('#custStateId').selectpicker();
    $('#custCityId').selectpicker();

}

function openSchedulerModal(){
    $('#prevBtn').click();
    $('#schedulerBtn').show();
    $('#campaignBtn').show();

    if( actModeHideShow() == "-1" ){  //  HIDE SHOW ACTIVITY MODE ACCORDING TO ROLE AUTH RIGHTS
        toastr.warning( NOACTMODERIGHTS );
        return false;
    }

    modalShow('schedulerModal');
    $('#schedulerActTitle').focus();
    addFocusId( 'schedulerActTitle' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function openCampaignModal(){
    $('#prevBtn').click();
    $('#campaignBtn').show();
    $('#schedulerBtn').show();

    if( actModeHideShow("2") == "-1" ){  //  HIDE SHOW ACTIVITY MODE ACCORDING TO ROLE AUTH RIGHTS
        toastr.warning( NOACTMODERIGHTS );
        return false;
    }

    modalShow('campaignModal');
    $('#campaignTitle').focus();
    addFocusId( 'campaignTitle' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function createScheduler(){ 

    var ethnicity = $('#ethnicity').val();
    var customerGroup = $('#customerGroup').val();
    var countryListing = $('#custCountryId').val();
    var stateListing = $('#custStateId').val();
    var cityListing = $('#custCityId').val();

    var birthdate = $('#birthdate').val();
    var birthdays = $('#birthdays').val();
    var anniversaryDate = $('#anniversaryDate').val();
    var anniversaryDays = $('#anniversaryDays').val();
    var customerDetail = $('#customerDetail').val();
    var salesYear = $('#salesYear').val();
    var salesAmtFrom = $('#salesAmtFrom').val();
    var salesAmtTo = $('#salesAmtTo').val();
    var custTypeList = $('#custTypeList').val();
    var lytd = $('input[type=radio][name="lytd"]').filter(":checked").val();
    // 1- ltd 2 - ytd
    var ltdytdAmt = $('#ltdytdAmt').val();
    
    var catchArea = $("#catchArea").val();
    var notSelected = "0";

    /// CODE REMAIN FROM HERE
    var sendFlag = "";

    if( ethnicity != "-1" ){
        sendFlag = "true";

    }if( custTypeList != "-1" ){
        sendFlag = "true";

    }else if( customerGroup != null ){
        sendFlag = "true";

    }else if( countryListing != "-1" ){
        sendFlag = "true";

    }else if( stateListing != "-1" ){
        sendFlag = "true";

    }else if( cityListing != null &&  cityListing != "-1" ){
        sendFlag = "true";

    }else if( birthdate != "" ){
        sendFlag = "true";

    }else if( anniversaryDate != "" ){
        sendFlag = "true";

    }else if( catchArea != "" ){
        sendFlag = "true";
        
    }else if( customerDetail != null ){
        sendFlag = "true";

    }else if( salesAmtFrom != "" ){
        sendFlag = "true";

    }else if( salesAmtTo != "" ){
        sendFlag = "true";

    }else if( ltdytdAmt != "" ){
        sendFlag = "true";

    }else if( temp_inq_Trans.length != 0 ){
        sendFlag = "true";

    }

    if( birthdays != "" && birthdate == "" ){
        toastr.warning("Please Select birthdate along with birth days");
        $('#prevBtn').click();
        $('#birthdate').datepicker('show');
        return false;
    }
    if( anniversaryDays != "" && anniversaryDate == "" ){
        toastr.warning("Please Select anniversaryDate along with anniversary days");
        $('#prevBtn').click();
        $('#anniversaryDate').datepicker('show');
        return false;
    }

    var cityList = "";

    if( cityListing != null ){
        for( var j=0; j<cityListing.length; j++ ){
            if( cityList.length == 0){
                cityList += cityListing[j] ;
            }else{
                cityList += "," + cityListing[j] ;
            }
        }
    }

    if( salesAmtFrom != "" && salesAmtFrom != "0" && salesAmtTo != "" && salesAmtTo != "0" ){
        if( parseInt( salesAmtFrom ) >= parseInt( salesAmtTo ) ){
            toastr.warning("Sales Amount From must be less than Sales Amount To");
            $('#prevBtn').click();
            $("#salesAmtFrom").focus();
            return false;
        }

    }

    if( sendFlag != "true" ){
        toastr.warning('Please Select Atleast One Smart Option to create scheduler');
        return false;
    }
    // customerListData = [];
    renderCustomerList();
    // $("#ethnicity").focus();

    // $('#prevBtn').click();
    // return false;

// }else{

    console.log("Validation Passed");
    console.log( sendFlag );

    var customerGrp = "";

    if( customerGroup != null ){
        for( var j=0; j<customerGroup.length; j++ ){
            if( customerGrp.length == 0){
                customerGrp += customerGroup[j] ;
            }else{
                customerGrp += "," + customerGroup[j] ;
            }
        }
    }

    var customerNm = "";

    if( customerDetail != null ){
        for( var k=0; k<customerDetail.length; k++ ){
            if( customerNm.length == 0){
                customerNm += customerDetail[k].split('|--|')[0] ;
            }else{
                customerNm += "," + customerDetail[k].split('|--|')[0] ;
            }
        }
    }

    if( ethnicity == "-1" ){
        ethnicity = ""
    }
    if( countryListing == "-1" ){
        countryListing = ""
    }
    if( stateListing == "-1" ){
        stateListing = ""
    }
    if( custTypeList == "-1" ){
        custTypeList = ""
    }
    if( birthdate != "" ){
        birthdate = ddmmyyToMysql(birthdate);
    }
    if( anniversaryDate != "" ){
        anniversaryDate = ddmmyyToMysql(anniversaryDate);
    }
    if( cityListing == "-1" || cityListing == "" ){
        cityListing = ""
    }

    var itemList = "";
    var groupList = "";
    
    if( temp_inq_Trans.length != 0 ){
        for( var i=0; i<temp_inq_Trans.length; i++ ){
            if( temp_inq_Trans[i].itemType == "ITEM" ){
                if( itemList.length == 0){
                    itemList += temp_inq_Trans[i].itemId ;
                }else{
                    itemList += "," + temp_inq_Trans[i].itemId ;
                }
            }else{
                if( groupList.length == 0){
                    groupList += temp_inq_Trans[i].itemId ;
                }else{
                    groupList += "," + temp_inq_Trans[i].itemId ;
                }
            }
        }
    }

    var schedulerActTitle = $("#schedulerActTitle").val();
    var schedulerActDesc = $("#schedulerActDesc").val();
    var schedulerActMode = $('input[type=radio][name="rdoActMode"]').filter(":checked").val();
    var schedulerTypeList = $('#schedulerTypeListSch').val();
    var schedulerActFreq = $('#scheduleFrequency').val();

    if ( schedulerActTitle == "" ){
        toastr.warning("Please Enter the Activity Title");
        $('#schedulerActTitle').focus();
        return false;

    }else if ( schedulerActDesc == "" ){
        toastr.warning("Please Enter the Activity Description");
        $('#schedulerActDesc').focus();
        return false;

    }

    var customerDetailTxt = $('#customerDetail').select2('data');
    var custTxt = [];
    customerDetailTxt.forEach(function (record , index){
        var tempArray = {
            id : record.id,
            text : record.text
        }
        custTxt.push(tempArray);
    })
    console.log(custTxt);

    /* generate postData to submit activity START */
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    if( editSchedulerFlag != 1 ){

        var postData = {
            requestCase : "createScheduleSmartActivity",
            orgId: checkAuth(2,5),
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId : localStorage.indeCampusBranchIdForChkAuth,
            ethnicity : ethnicity,
            customerGroup : customerGrp,
            countryId : countryListing,
            state : stateListing,
            city : cityList,
            birthdate : birthdate,
            birthdays : ( birthdays ) == ""  ? 0 : birthdays,
            anniversaryDate : anniversaryDate,
            anniversaryDays : ( anniversaryDays) == "" ? 0 : anniversaryDays,
            customerName : customerNm,
            salesYear : salesYear,
            salesAmtFrom : salesAmtFrom,
            salesAmtTo : salesAmtTo,
            custType : custTypeList,
            lytd : lytd,
            ltdytdAmt : ltdytdAmt,
            itemId : itemList,
            groupId : groupList,
            catchArea : catchArea, // Catchment Area
            schedulerActTitle : schedulerActTitle,
            schedulerActDesc : schedulerActDesc,
            schedulerActMode : schedulerActMode,
            schedulerTypeList : schedulerTypeList,
            schedulerActFreq : schedulerActFreq,
            customerDetailJson : custTxt,
            item_Trans : temp_inq_Trans
            // notSelected : notSelected, // not selected any filter = 1 , any selected = 0
        };
    }else{

        var postData = {
            requestCase : "editCreateScheduleSmartActivity",
            orgId: checkAuth(2,7),
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId : localStorage.indeCampusBranchIdForChkAuth,
            ethnicity : ethnicity,
            customerGroup : customerGrp,
            countryId : countryListing,
            state : stateListing,
            city : cityList,
            birthdate : birthdate,
            birthdays : ( birthdays ) == ""  ? 0 : birthdays,
            anniversaryDate : anniversaryDate,
            anniversaryDays : ( anniversaryDays) == "" ? 0 : anniversaryDays,
            customerName : customerNm,
            salesYear : salesYear,
            salesAmtFrom : salesAmtFrom,
            salesAmtTo : salesAmtTo,
            custType : custTypeList,
            lytd : lytd,
            ltdytdAmt : ltdytdAmt,
            itemId : itemList,
            groupId : groupList,
            catchArea : catchArea, // Catchment Area
            schedulerActTitle : schedulerActTitle,
            schedulerActDesc : schedulerActDesc,
            schedulerActMode : schedulerActMode,
            schedulerTypeList : schedulerTypeList,
            schedulerActFreq : schedulerActFreq,
            customerDetailJson : custTxt,
            item_Trans : temp_inq_Trans,
            smartId : gbl_smartId
        };
    }
    console.log(postData);
    // return false;
    commonAjax(ACTURL, postData, createSchedulerCallBack,"");

    function createSchedulerCallBack(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);

            toastr.success('Scheduled Successfully');
            modalHide('schedulerModal');
            resetSchedulerModal();


        } else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_SCH_ACT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function resetSchedulerModal(){

    $("#schedulerActTitle").val('');
    $("#schedulerActDesc").val('');
}


// CREATE CAMPAING MODAL CREATE CODE

function createCampaign(){ 

    var ethnicity = $('#ethnicity').val();
    var customerGroup = $('#customerGroup').val();
    var countryListing = $('#custCountryId').val();
    var stateListing = $('#custStateId').val();
    var cityListing = $('#custCityId').val();

    var birthdate = $('#birthdate').val();
    var birthdays = $('#birthdays').val();
    var anniversaryDate = $('#anniversaryDate').val();
    var anniversaryDays = $('#anniversaryDays').val();
    var customerDetail = $('#customerDetail').val();
    var salesYear = $('#salesYear').val();
    var salesAmtFrom = $('#salesAmtFrom').val();
    var salesAmtTo = $('#salesAmtTo').val();
    var custTypeList = $('#custTypeList').val();
    var lytd = $('input[type=radio][name="lytd"]').filter(":checked").val();
    // 1- ltd 2 - ytd
    var ltdytdAmt = $('#ltdytdAmt').val();
    
    var catchArea = $("#catchArea").val();
    var notSelected = "0";

    /// CODE REMAIN FROM HERE
    var sendFlag = "";

    if( ethnicity != "-1" ){
        sendFlag = "true";

    }if( custTypeList != "-1" ){
        sendFlag = "true";

    }else if( customerGroup != null ){
        sendFlag = "true";

    }else if( countryListing != "-1" ){
        sendFlag = "true";

    }else if( stateListing  != null &&  cityListing != "-1" ){
        sendFlag = "true";

    }else if( cityListing != null &&  cityListing != "-1" ){
        sendFlag = "true";

    }else if( birthdate != "" ){
        sendFlag = "true";

    }else if( anniversaryDate != "" ){
        sendFlag = "true";

    }else if( catchArea != "" ){
        sendFlag = "true";
        
    }else if( customerDetail != null ){
        sendFlag = "true";

    }else if( salesAmtFrom != "" ){
        sendFlag = "true";

    }else if( salesAmtTo != "" ){
        sendFlag = "true";

    }else if( ltdytdAmt != "" ){
        sendFlag = "true";

    }else if( temp_inq_Trans.length != 0 ){
        sendFlag = "true";

    }

    if( birthdays != "" && birthdate == "" ){
        toastr.warning("Please Select birthdate along with birth days");
        $('#prevBtn').click();
        $('#birthdate').datepicker('show');
        return false;
    }
    if( anniversaryDays != "" && anniversaryDate == "" ){
        toastr.warning("Please Select anniversaryDate along with anniversary days");
        $('#prevBtn').click();
        $('#anniversaryDate').datepicker('show');
        return false;
    }

    var cityList = "";

    if( cityListing != null ){
        for( var j=0; j<cityListing.length; j++ ){
            if( cityList.length == 0){
                cityList += cityListing[j] ;
            }else{
                cityList += "," + cityListing[j] ;
            }
        }
    }

    if( salesAmtFrom != "" && salesAmtFrom != "0" && salesAmtTo != "" && salesAmtTo != "0" ){
        if( parseInt( salesAmtFrom ) >= parseInt( salesAmtTo ) ){
            toastr.warning("Sales Amount From must be less than Sales Amount To");
            $('#prevBtn').click();
            $("#salesAmtFrom").focus();
            return false;
        }

    }

    if( sendFlag != "true" ){
        toastr.warning('Please Select Atleast One Smart Option to create scheduler');
        return false;
    }
    // customerListData = [];
    renderCustomerList();
    // $("#ethnicity").focus();

    // $('#prevBtn').click();
    // return false;

// }else{

    console.log("Validation Passed");
    console.log( sendFlag );

    var customerGrp = "";

    if( customerGroup != null ){
        for( var j=0; j<customerGroup.length; j++ ){
            if( customerGrp.length == 0){
                customerGrp += customerGroup[j] ;
            }else{
                customerGrp += "," + customerGroup[j] ;
            }
        }
    }

    var customerNm = "";

    if( customerDetail != null ){
        for( var k=0; k<customerDetail.length; k++ ){
            if( customerNm.length == 0){
                customerNm += customerDetail[k].split('|--|')[0] ;
            }else{
                customerNm += "," + customerDetail[k].split('|--|')[0] ;
            }
        }
    }

    if( ethnicity == "-1" ){
        ethnicity = ""
    }
    if( countryListing == "-1" ){
        countryListing = ""
    }
    if( stateListing == "-1" ){
        stateListing = ""
    }
    if( custTypeList == "-1" ){
        custTypeList = ""
    }
    if( birthdate != "" ){
        birthdate = ddmmyyToMysql(birthdate);
    }
    if( anniversaryDate != "" ){
        anniversaryDate = ddmmyyToMysql(anniversaryDate);
    }
    if( cityListing == "-1" || cityListing == "" ){
        cityListing = ""
    }

    var itemList = "";
    var groupList = "";
    
    if( temp_inq_Trans.length != 0 ){
        for( var i=0; i<temp_inq_Trans.length; i++ ){
            if( temp_inq_Trans[i].itemType == "ITEM" ){
                if( itemList.length == 0){
                    itemList += temp_inq_Trans[i].itemId ;
                }else{
                    itemList += "," + temp_inq_Trans[i].itemId ;
                }
            }else{
                if( groupList.length == 0){
                    groupList += temp_inq_Trans[i].itemId ;
                }else{
                    groupList += "," + temp_inq_Trans[i].itemId ;
                }
            }
        }
    }

    var actTitle = $("#campaignActTitle").val();
    var actDesc = $("#campaignActDesc").val();
    var actMode = $('input[type=radio][name="rdoCampaignMode"]').filter(":checked").val();
    var actTypeList = $('#campaignActTypeList').val();
    var campaignTitle = $('#campaignTitle').val();
    var campaignDesc = $('#campaignDesc').val();

    if ( campaignTitle == "" ){
        toastr.warning("Please Enter the Campaign Title");
        $('#campaignTitle').focus();
        return false;

    }else if ( campaignDesc == "" ){
        toastr.warning("Please Enter the Campaign Description");
        $('#campaignDesc').focus();
        return false;

    }else if ( actTitle == "" ){
        toastr.warning("Please Enter the Activity Title");
        $('#campaignActTitle').focus();
        return false;

    }else if ( actDesc == "" ){
        toastr.warning("Please Enter the Activity Description");
        $('#campaignActDesc').focus();
        return false;

    }

    /* generate postData to submit activity START */
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "createCampaignMaster",
        orgId: checkAuth(51,210),
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId : localStorage.indeCampusBranchIdForChkAuth,
        ethnicity : ethnicity,
        customerGroup : customerGrp,
        countryId : countryListing,
        state : stateListing,
        city : cityList,
        birthdate : birthdate,
        birthdays : ( birthdays ) == ""  ? 0 : birthdays,
        anniversaryDate : anniversaryDate,
        anniversaryDays : ( anniversaryDays) == "" ? 0 : anniversaryDays,
        customerName : customerNm,
        salesYear : salesYear,
        salesAmtFrom : salesAmtFrom,
        salesAmtTo : salesAmtTo,
        custType : custTypeList,
        lytd : lytd,
        ltdytdAmt : ltdytdAmt,
        itemId : itemList,
        groupId : groupList,
        catchArea : catchArea, // Catchment Area
        actTitle : actTitle,
        actDesc : actDesc,
        actMode : actMode,
        actTypeList : actTypeList,
        campaignTitle : campaignTitle,
        campaignDesc : campaignDesc,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        // notSelected : notSelected, // not selected any filter = 1 , any selected = 0
    };
    console.log(postData);
    // return false;
    commonAjax(FOLLOWUPURL, postData, createCampaignCallBack,"");

    function createCampaignCallBack(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);

            modalHide('campaignModal');
            resetCampaingModal();

        } else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_CREATE_CAMPAIGN );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function resetCampaingModal(){

    $("#campaignActTitle").val('');
    $("#campaignActDesc").val('');
    $("#campaignTitle").val('');
    $("#campaignDesc").val('');
}

function assignProjectChangeEvent(){

    $('#projectListAct').change(function(){
        var currProjID = $('#projectListAct').val();
        
        var options = '<option value="-1">Select User</option>';
        if( currProjID != "" ){
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            //SET GROUP LOAD
            var postData ={
                requestCase : "userListingActivityModule",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                orgId: checkAuth(2,6),
                userId: tmp_json.data[0].PK_USER_ID,
                projectId: currProjID,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
            };
            commonAjax(URL2, postData, callBackUserListingProjData, "");
            function callBackUserListingProjData(flag, data) {
                if (data.status == "Success" && flag) {

                    if( data.data != "No record found" ){

                        data.data.forEach( function( record , index ){

                            var selectedUser = "";
                            if( tmp_json.data[0].PK_USER_ID == record.userId ){
                                selectedUser = "selected";
                            }
                            options += '<option value="'+ record.userId +'" '+ selectedUser +'>'+ record.fullName +'</option>';
                        });
                    }

                    $('#ddActAssignedToSmart').html( options ).selectpicker('refresh');

                } else {
                    $('#ddActAssignedToSmart').html( options ).selectpicker('refresh');
                    if (flag)
                        toastr.error(data.status);
                    else
                        toastr.error(SERVERERROR);
                }
            }
        }else{
            $('#ddActAssignedToSmart').html( localStorage.POTGactivityAppAssignTypeUserOptionHtml ).selectpicker('refresh');
        }   

    });
}

function checkEditScheduler() {
    
    if(localStorage.potgEditSchedulerActData != undefined && localStorage.potgEditSchedulerActData != ""){
        var postDataMainJson= JSON.parse(localStorage.potgEditSchedulerActData);
        var postDataJson= postDataMainJson.filterValue.postData;

        if( postDataJson.customerDetailJson != undefined && postDataJson.customerDetailJson != "" ){

            postDataJson.customerDetailJson.forEach(function(record,index){

                var $option = $("<option selected></option>").val(record.id).text(record.text);
                $('#customerDetail').append($option).trigger('change');
            });
        }
        if( postDataJson.customerGroup != "" ){
            var custTempArr = postDataJson.customerGroup.split(',');
            custTempArr.forEach(function(custRecord,custIndex){

                if( custGroupJson != "" ){
                    var idx = findIndexByKeyValue(custGroupJson,'groupId',custRecord);
                    if( idx != "-1" ){
                        var $option = $("<option selected></option>").val( custGroupJson[idx].groupId ).text( custGroupJson[idx].groupName );
                        $('#customerGroup').append($option).trigger('change');
                    }
                }

            });
        }
        $('#custTypeList').val(postDataJson.custType);
        $('#custTypeList').selectpicker('refresh');
        $('#catchArea').val( postDataJson.catchArea );
        $('#birthdate').val( postDataJson.birthdate != "" ? postDataJson.birthdate : "" );
        $('#birthdays').val( (postDataJson.birthdays != "0" ? postDataJson.birthdays : "" ));
        $('#birthdays').val( (postDataJson.anniversaryDays != "0" ? postDataJson.anniversaryDays : "" ));
        $('#anniversaryDate').val( postDataJson.anniversaryDate != "" ? postDataJson.anniversaryDate : "" );
        $('#ethnicity').val( postDataJson.ethnicity ).selectpicker('refresh');
        $('#custCountryId').val( postDataJson.countryId ).selectpicker('refresh');
        if( postDataJson.countryId != "" && postDataJson.countryId != undefined ){
            populateState( 'custCountryId' , 'custStateId');
        }
        $('#custStateId').val( postDataJson.state ).selectpicker('refresh');
        populateCityOnState('custStateId', 'custCityId');
        $('#custCityId').val( postDataJson.city ).selectpicker('refresh');
        $('#salesYear').val( postDataJson.salesYear ).selectpicker('refresh');
        if( postDataJson.salesYear != "-1" ){
            $('#salesAmtFrom').attr('disabled',false);
            $('#salesAmtFrom').val( postDataJson.salesAmtFrom );
            $('#salesAmtTo').attr('disabled',false);
            $('#salesAmtTo').val( postDataJson.salesAmtTo );
        }
        
        $('input[type=radio][name="lytd"]').filter('[value='+postDataJson.lytd+']').click()
        $('#ltdytdAmt').val( postDataJson.ltdytdAmt );
        if( postDataJson.item_Trans != undefined && postDataJson.item_Trans.length != 0 ){

            postDataJson.item_Trans.forEach(function (record,index){
                var tempData = {
                    itemId : record.itemId,
                    itemName : record.itemName,
                    groupName : record.groupName,
                    itemType : record.itemType,
                }
                temp_inq_Trans.push(tempData);
            });
            addRowTable(temp_inq_Trans);
        }
        $('#schedulerActTitle').val( postDataMainJson.actTiele );
        $('#schedulerTypeListSch').val( postDataMainJson.actType ).selectpicker('refresh');
        $('#schedulerActDesc').val( postDataMainJson.actDesc );
        $('#scheduleFrequency').val( postDataJson.schedulerActFreq ).selectpicker('refresh');
        editSchedulerFlag = 1;
        gbl_smartId = postDataMainJson.smartId;
        localStorage.removeItem('potgEditSchedulerActData');
    }
}