/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 11-01-2018.
 * File : dynamicProposalCreateController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_TEMPLATE_DATA = []; // SELECTED TEMPLATED DATA
var GBL_CONSTANT_DATA = []; // CONSTANT LISTING DATA
var GBL_TEMPLATE_HTML = []; // DYNAMIC TEMPLATE HTML

function getTemplateData(){
    
    if( localStorage.selectedTemplateData != undefined && localStorage.selectedTemplateData != "" ){
        GBL_TEMPLATE_DATA = JSON.parse(localStorage.selectedTemplateData);
        GBL_TEMPLATE_HTML = JSON.parse(localStorage.selectedTemplateData).templateHtml;
        GBL_CONSTANT_DATA = JSON.parse(localStorage.selectedTemplateData).templateConstant;

        setTimeout( function(){
            tinyMCE.get('templateData').setContent( GBL_TEMPLATE_HTML );
        },500);

        if( GBL_TEMPLATE_DATA.templateType != "0" ){
            $('#creatUpdTemBtn').html( 'Update Template' );
            $('#templateName').val( GBL_TEMPLATE_DATA.templateName );
        }
        renderTemplatSideBar();
    }     
}

function renderTemplatSideBar(){

    if( GBL_CONSTANT_DATA != "No record found" ){

        var constantHtml = "<div id='dynamicConstant'><h3> Constants </h3><div>";

        if( GBL_CONSTANT_DATA != "No record found" ){
            GBL_CONSTANT_DATA.forEach( function( record , index ){
                constantHtml += '<b>'+ record.lable +'</b><hr>';
                record.constant.forEach( function( innrRecrd , innrIndx ){
                    constantHtml += '<label>'+ innrRecrd +'</label><br>';
                });
            });
            constantHtml += "</div>";
        }

        $('#constantList').html( constantHtml );
        $( "#dynamicConstant" ).accordion({collapsible: true});
    }

    if( localStorage.POTGtemplateTour == 1 ){
        initSelfTourTemplate();
        localStorage.POTGtemplateTour = 0;
    }
}

function createTemplate(context){

    var templateName = $('#templateName').val().trim();
    var templateHtml = tinyMCE.get('templateData').getContent();

    if( templateName.length == 0 ){
        toastr.warning('Please Enter Template Name');
        $('#templateName').focus();
        return false;

    }else if( templateHtml == ""){
        toastr.warning('Please Enter Template Data');
        $('#templateData').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'saveClientTemplate',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID, 
            orgId: checkAuth(4,14),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            defaultTemplateId : ( (GBL_TEMPLATE_DATA.templateType == "0") ? GBL_TEMPLATE_DATA.templateId : GBL_TEMPLATE_DATA.defaultTemplateId ),
            clientTemplateId : ( (GBL_TEMPLATE_DATA.templateType == "0") ? "" : GBL_TEMPLATE_DATA.templateId ),
            templateName : templateName,
            templateType : "1", // FUTURE PURPOSE --  //0:DEFAULT , 1: CLIENT SELECTED 
            templateHtml : templateHtml,
        }   
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, createTemplateCallback,"Please Wait... Getting Dashboard Detail");

        function createTemplateCallback(flag,data){

            if (data.status == "Success" && flag) { 
                navigateToDynamicProposalTemplateList();
            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_FAIL_CREATE_TEMPLATE );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

// INITIALIZE TOUR - ADDED ON 09-MAY-2018 BY VISHAKHA TAK
function initSelfTourTemplate() {

    if( localStorage.GBL_TOUR_STEPS_USER != undefined ){

        GBL_TOUR_STEPS = JSON.parse(localStorage.GBL_TOUR_STEPS_USER);
        // Instance the tour
        instanceTour();
        tour._options.name = "Dynamic Template Tour";
        // tour._options.debug = true;

        // Initialize the tour
        tour.init();

        tour.setCurrentStep(6); // START FROM THIS STEP TO PREVENT FROM DISPLAYING LAST STORED STEP

        // Start the tour
        tour.start(true);
        localStorage.removeItem('GBL_TOUR_STEPS_USER');
    }
    
}