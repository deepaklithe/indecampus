    /*
 *
 * Created By : Kausha Shah
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 16-08-2018.
 * File : createReceiptController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
GBL_CENTRALIZED_DATA = [];
var GBL_INV_LIST = [];
var GBL_INV_TOTAL_CHG = 0;
var GBL_REMAIN_AMT = 0;
var GBL_REMAIN_PAYABLE_AMT = 0;
var GBL_CLOS_BAL_DATA = [];
var GBL_CLOS_BAL_AMT = 0;

function pageInitialEvents() {
    
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    console.log(roleAuth) ;
    autoCompleteForStudentId('studentName',saveStudentNameData);
    
    var paymentModeData = roleAuth.data.paymentMode;
    setOption("0",'paymentMode',paymentModeData,''); // SET ASSETS TYPE
    $('#paymentMode').selectpicker('refresh');
    
    var bankNameData = roleAuth.data.bankName ;
    setOption("0",'bankName',bankNameData,''); // SET ASSETS TYPE
    $('#bankName').selectpicker('refresh');

    $('#receiptDate').datepicker('update' , mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );

    $('#studentName').on('keydown', function (e) {
        if (e.which == 8 || e.which == 46) {

            if ($('#studentName').val() == "" || $('#studentName').val().length <= 2) {

                $('#studentName').attr("studId", "");
                $('#studName').val('');
                $('#amount').val('');
                $('#invoiceType').val('-1').selectpicker('refresh');
                $('#invoiceType').trigger('change');
                $('#invoiceList').val('-1').selectpicker('refresh');
                $('.studDetail').hide();
                $('.studentNameDiv').removeClass('col-md-4').addClass('col-md-8');

            }

        }
    });

    if( $('#paymentMode').val() != "" && $('#paymentMode').val() != null && $('#paymentMode').val() != "-1" ){
        var modeOfPaymentName = $('#paymentMode').find('option:selected').text().trim();
        if( modeOfPaymentName.toLowerCase() != "cash" ){
            $('.chequeNeftDiv').show();
            $('.bankNameDiv').show();
            $('.cardDetailDiv').hide();
        }

    }
}

// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID 
function autoCompleteForStudentId(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( index != -1 ){
                setTimeout(function() {
                    $('#'+id).val( ITEMS[index].studentName + ' - ' + ITEMS[index].membershipNumber );
                    $('#'+id).attr( 'studId' , ITEMS[index].id ); 
                    $('.studDetail').show();
                    $('.studentNameDiv').removeClass('col-md-8').addClass('col-md-4');
                    $('#studName').val( ITEMS[index].studentName ); 
                    if( $('#invoiceType').val() == "1" ){
                        getInvoiceListing();
                    }else if( $('#invoiceType').val() == "2" ){
                        renderInvoiceList([]);
                        getClosingBalStudent();
                    }else{
                        renderInvoiceList([]);
                    }
                },1);
            }else{
                setTimeout(function() {
                    $('#'+id).val( '' );
                    $('#'+id).attr( 'studId' , '' ); 
                    $('#studName').val(''); 
                    $('.studDetail').hide();
                    $('.studentNameDiv').removeClass('col-md-4').addClass('col-md-8');
                    
                },1);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId:  checkAuth(11,45),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            var tempJson = {
                                id: data.studId,
                                name: data.studName + ' - ' + data.membershipNumber,
                                data: data.studName,
                                studentName : data.studName,
                                membershipNumber : data.membershipNumber,
                            }
                            ITEMS.push(tempJson);

                           
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}

function resetFields(){
    
    $('#studentName').val('');
    $('#studentName').attr('studId','');
    $('.studDetail').hide();
    $('.studentNameDiv').removeClass('col-md-4').addClass('col-md-8');
    $('#receiptDate').val('') ;
    $('#receiptDate').datepicker('update' , mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );
    $('#paymentMode').val('').selectpicker('refresh') ;
    $('#bankName').val('').selectpicker('refresh') ;
    $('#bankBranchName').val('') ;
    $('#chequeNo').val('') ;
    $('#chequeDate').val('');
    $('#amount').val('') ;
    $('#narration').val('') ;
    $('#lastFourDigitCardNo').val('') ;
    $('#invoiceType').val('-1').selectpicker('refresh');
    $('#invoiceType').trigger('change');
    $('#paymentMode').trigger('change');
    // $('#acSuspense').removeAttr('checked'); 
}

function createReceipt() {
     
    var studentName = $('#studentName').attr('studId');
    var invoiceType = $('#invoiceType').val() ; // ADDED BY KAUSHA SHAH ON 22-08-2018
    var invoiceList = $('#invoiceList').val() ; // ADDED BY KAUSHA SHAH ON 22-08-2018
    var receiptDate = $('#receiptDate').val() ;
    var paymentMode = $('#paymentMode').val() ;
    var bankName = $('#bankName').val() ;
    var bankBranchName = $('#bankBranchName').val() ;
    var chequeNo = $('#chequeNo').val() ;
    var chequeDate = $('#chequeDate').val() ;
    var payAmount = $('#payAmount').val() ; // ADDED BY KAUSHA SHAH ON 22-08-2018
    var pendingAmount = $('#pendingAmount').val() ; // ADDED BY KAUSHA SHAH ON 22-08-2018
    var amount = $('#amount').val() ;
    var narration = $('#narration').val() ;

    var lastFourDigitCardNo = $('#lastFourDigitCardNo').val();

    // if($('#acSuspense').is(':checked')) { var acSuspense = 1 ;} else { var acSuspense = 0 ;}
    var paymentModeName = ( paymentMode != "" && paymentMode != null ? $('#paymentMode option:selected').text().trim() : "" );
   
    if( receiptDate == "" ){
        toastr.warning("Please select receipt date");
        addFocusId('receiptDate');
        return false;

    }else if( studentName == "" || studentName == undefined ){
        toastr.warning("Please enter student name");
        addFocusId('studentName');
        return false;

    } else if(invoiceType == "" || invoiceType == "-1") { // ADDED BY KAUSHA SHAH ON 22-08-2018
        toastr.warning("Please select receipt type");
        addFocusId('invoiceType');
        return false;
        
    } else if( invoiceType == "1" && (invoiceList == "-1" || invoiceList == null )) { // ADDED BY KAUSHA SHAH ON 22-08-2018
        toastr.warning("Please select invoice no");
        addFocusId('invoiceList');
        return false;
        
    } else if(amount == "" ){
        toastr.warning("Please enter amount");
        addFocusId('amount');
        return false;

    } else if( amount != "" && (parseFloat(amount) < 1) ){
        toastr.warning("Please enter valid amount");
        addFocusId('amount');
        return false;

    } else if( paymentMode == "" ){
        toastr.warning("Please select payment mode");
        addFocusId('paymentMode');
        return false;

    } else if( ( paymentMode != "" && paymentModeName.toLowerCase() != "cash" &&  (bankName == "" || bankName == "-1") ) ){
        toastr.warning("Please select bank name");
        addFocusId('bankName');
        return false;

    } else if( ( paymentMode != "" ) && ( paymentModeName.toLowerCase() != "cash" ) && ( paymentModeName.toLowerCase().indexOf('credit') == -1 ) && ( paymentModeName.toLowerCase().indexOf('debit') == -1 ) && (bankBranchName == "") ){
        toastr.warning("Please enter bank branch name");
        addFocusId('bankBranchName');
        return false;

    } else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase() != "cash") && ( paymentModeName.toLowerCase().indexOf('credit') == -1 ) && ( paymentModeName.toLowerCase().indexOf('debit') == -1 ) && (chequeNo == "") ){
        toastr.warning("Please enter cheque / dd / neft no");
        addFocusId('chequeNo');
        return false;

    } else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase() != "cash") && ( paymentModeName.toLowerCase().indexOf('credit') == -1 ) && ( paymentModeName.toLowerCase().indexOf('debit') == -1 ) && (chequeDate == "") ){
        toastr.warning("Please select cheque / dd / neft date");
        addFocusId('chequeDate');
        return false;

    }else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase().indexOf('credit') != -1 || paymentModeName.toLowerCase().indexOf('debit') != -1 ) && (lastFourDigitCardNo == "") ){
        toastr.warning("Please enter last four digits of card number");
        addFocusId('lastFourDigitCardNo');
        return false;

    }else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase().indexOf('credit') != -1 || paymentModeName.toLowerCase().indexOf('debit') != -1 ) && (lastFourDigitCardNo != "") && (lastFourDigitCardNo.length < 4  ) ){
        toastr.warning("Please enter valid last four digits of card number");
        addFocusId('lastFourDigitCardNo');
        return false;

    } else if( payAmount == "" ) {
        toastr.warning("Please enter pay amount");
        addFocusId('payAmount');
        return false;
        
    } else if( payAmount != "" && (parseFloat(payAmount) < 1)  ) {
        toastr.warning("Please enter valid pay amount");
        addFocusId('payAmount');
        return false;
        
    // } else if( pendingAmount == "") {
    //     toastr.warning("Please emter pending amount");
    //     addFocusId('pendingAmount');
    //     return false;
        
    // } else if( pendingAmount != "" && (parseFloat(pendingAmount) < 1) ) {
    //     toastr.warning("Please emter valid pending amount");
    //     addFocusId('pendingAmount');
    //     return false;
        
    } 

    var invTypeVal = "1";
    if( invoiceType == "1" ){ // AS PER CHANGES FOR API
        invTypeVal = "2";
    }

    if( paymentModeName.toLowerCase() == "cash" ){
        bankName = "";
        bankBranchName = "";
        chequeNo = "";
        chequeDate = "";
        lastFourDigitCardNo = "";
    }
    if( ( paymentModeName.toLowerCase().indexOf('credit') != -1 ) || ( paymentModeName.toLowerCase().indexOf('debit') != -1 ) ){
        bankBranchName = "";
        chequeNo = "";
        chequeDate = "";
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase : 'createReceipt' ,
        userId : tmp_json.data[0].PK_USER_ID,
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        orgId : checkAuth(23, 94),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        studentId : studentName,
        invoiceType : invTypeVal, // ADDED BY KAUSHA SHAH ON 22-08-2018
        invoiceList : invoiceList, // ADDED BY KAUSHA SHAH ON 22-08-2018
        receiptDate : receiptDate,
        paymentMethodId : paymentMode,
        paymentMethod : paymentModeName,
        bankName : bankName,
        bankBranchName : bankBranchName,
        chequeNo : chequeNo,
        chequeDate : chequeDate,
        lastFourDigitCardNo : lastFourDigitCardNo,
        payAmount : payAmount, // ADDED BY KAUSHA SHAH ON 22-08-2018
        pendingAmount : pendingAmount, // ADDED BY KAUSHA SHAH ON 22-08-2018
        amount : amount,
        narration : narration,
        // suspenseAccountflag: acSuspense
    }
    // console.log(postData); return false;
    commonAjax(FOLLOWUPURL, postData, createReceiptCallback,"Please wait...Adding your item.");
    
    function createReceiptCallback(flag , data) {
        
        if (data.status == "Success" && flag) {
            
            // navigateToChargePostMaster();
            toastr.success("Your receipt created successfully");
            resetFields();
            
        } else {
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_RECEIPT );
            else
                toastr.error(SERVERERROR);
        }
        
        // resetFields() ;
    }
}
// DISPLAY INVOICE DROPDWON ON CHANGE OF INVOICE TYPE - ADDED BY VISHAKHA TAK ON 21-08-2018
function checkInvoiceType(context){
    
    $('#againstInvoiceDiv').hide();
    $('#amount').val('');
    $('#payAmount').val('');
    $('#pendingAmount').val('');
    $('#pendingAmount').removeAttr('readonly');

    var invType = $(context).val();
    if( invType == "1" ){
        getInvoiceListing();
        $('#againstInvoiceDiv').show();

    }else{
        if( invType == "2" ){
            getClosingBalStudent();
        }
        
        renderInvoiceList([]);
        $('#againstInvoiceDiv').hide();
        
    }
}

// GET INVOICE OF SELECTED STUDENT - ADDED BY VISHAKHA TAK ON 21-08-2018
function getInvoiceListing() {
        
    var studentId = $('#studentName').attr('studId');
    var studentName = $('#studentName').val();
    if( studentName == "" || studentId == undefined || studentId == "" ){
        toastr.warning("Please search student first");
        $('#invoiceType').val('-1').selectpicker('refresh');
        $('#invoiceType').trigger('change');
        addFocusId('studentName');
        return false;

    }else{    

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase : 'getInvoiceListByStudent' ,
            userId : tmp_json.data[0].PK_USER_ID,
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            orgId : checkAuth(23, 94),
            studentId : studentId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
        
        commonAjax(FOLLOWUPURL, postData, getInvoiceListingCallback,"Please wait...Adding your item.");
        
        function getInvoiceListingCallback(flag , data) {
            
            if (data.status == "Success" && flag) {
                
                console.log(data);
                GBL_INV_LIST = data.data;
                renderInvoiceList(GBL_INV_LIST);
                
                
            } else {

                GBL_INV_LIST = [];
                renderInvoiceList([]);

                if (flag)
                  displayAPIErrorMsg( data.status , GBL_ERR_NO_INVOICE );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

}

// RENDER INVOICE DROPDOWN - ADDED BY VISHAKHA TAK ON 21-08-2018
function renderInvoiceList(invoiceList) {
    
    var innerHtml = '<option value="-1">Select Invoice</option>';
    if( invoiceList != "" && invoiceList != "No record found" ){

        invoiceList.forEach(function( record , index ){
            var remainingAmt = "";
            var totalCharges = "";
            if( record.remainingAmt != undefined && record.remainingAmt != "" ){
                remainingAmt = record.remainingAmt;
            }
            if( record.totalCharges != undefined && record.totalCharges != "" ){
                totalCharges = record.totalCharges;
            }

            innerHtml += '<option value="'+ record.clientInvoiceId +'" remainingAmt="'+ remainingAmt +'" totalCharges="'+ totalCharges +'">'+ record.clientInvoiceId +'</option>';

        });
    }
    $('#invoiceList').html( innerHtml );
    $('#invoiceList').selectpicker('refresh');

}

// GET PAYABLE AMOUNT OF SELECTED INVOICE - ADDED BY VISHAKHA TAK ON 21-08-2018
function getRemainingAmt(context){

    var invoiceId = $(context).val();

    if( invoiceId != "" && invoiceId != "-1" && invoiceId != null ){

        var remainingAmt = $(context).find(":selected").attr('remainingAmt');
        var totalCharges = $(context).find(":selected").attr('totalCharges');
        if( remainingAmt == undefined || remainingAmt == "undefined" || remainingAmt == "" || remainingAmt == "0" ){

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

            var postData = {
                requestCase : 'invoiceRemainingPayment', 
                clientId : tmp_json.data[0].FK_CLIENT_ID,
                userId : tmp_json.data[0].PK_USER_ID,
                orgId : checkAuth(23, 94),
                invoiceId : invoiceId,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            };
            // return false; console.log(postData);
            commonAjax(COMMONURL, postData, getRemainingAmtCallback,"Please Wait... Getting Dashboard Detail");

            function getRemainingAmtCallback( flag , data ){

                if (data.status == "Success" && flag) {
                    console.log(data);
                    GBL_INV_TOTAL_CHG = data.data[0].remainingAmt;
                    GBL_REMAIN_AMT = data.data[0].remainingAmt;
                    GBL_REMAIN_PAYABLE_AMT = GBL_REMAIN_AMT;
                    $('#pendingAmount').val( fixedTo2(remainingAmt) ).attr('readonly',true);
                    $('#amount').val( fixedTo2(data.data[0].remainingAmt) ).attr('readonly',true);
                } else {
                        
                    if (flag){
                        // displayAPIErrorMsg( data.status , GBL_ERR_NO_PAY_INVOICE );
                    }
                    else{
                        toastr.error(SERVERERROR);
                    }
                }

            }
        }else{
            GBL_INV_TOTAL_CHG = totalCharges;
            GBL_REMAIN_AMT = remainingAmt;
            GBL_REMAIN_PAYABLE_AMT = GBL_REMAIN_AMT;
            $('#pendingAmount').val( fixedTo2(remainingAmt) ).attr('readonly',true);
            $('#amount').val( fixedTo2(totalCharges) ).attr('readonly',true);
        }
    }else{
        GBL_REMAIN_PAYABLE_AMT = 0;
        $('#pendingAmount').val( '' ).removeAttr('readonly');
        $('#amount').val( '' ).removeAttr('readonly');
    }

}

// NOT ALLOW TO ENTER VALUE MORE THAN SPECIFIED AMOUNT
function limitNumberInRange(input,maxRange) { // ADDED BY VISHAKHA TAK ON 22-08-2018

    var MINNUMFORRANGE = 0;
    var MAXFORRANGE = maxRange;
    if (parseInt(input.value) < parseInt(MINNUMFORRANGE)) {
        input.value = MINNUMFORRANGE;
    }
    if (parseInt(input.value) > parseInt(MAXFORRANGE)) {
        input.value = MAXFORRANGE;
    }
    if ( input.value != GBL_REMAIN_PAYABLE_AMT ){
        if( input.value != "" ){
            var pendingAmt = parseFloat(GBL_REMAIN_PAYABLE_AMT) - parseFloat(input.value);
            $('#pendingAmount').val( fixedTo2(pendingAmt) );
            $('#pendingAmount').attr('readonly',true);
        }else{
            $('#pendingAmount').val( fixedTo2(GBL_REMAIN_PAYABLE_AMT) );
            $('#pendingAmount').attr('readonly',true);
        }
        
    }else{
        $('#pendingAmount').val( '0.00' );
        $('#pendingAmount').attr('readonly',true);
    }
}

// GET CLOSING BALANCE PAYABLE BY SELECTED STUDENT - ADDED BY VISHAKHA TAK ON 22-08-2018
function getClosingBalStudent(){
        
    var studentId = $('#studentName').attr('studId');
    var studentName = $('#studentName').val();
    if( studentName == "" || studentId == undefined || studentId == "" ){
        toastr.warning("Please search student first");
        $('#invoiceType').val('-1').selectpicker('refresh');
        $('#invoiceType').trigger('change');
        addFocusId('studentName');
        return false;

    }else{    

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase : 'getclosingbalanceByStudent',
            userId : tmp_json.data[0].PK_USER_ID,
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            orgId : checkAuth(23, 94),
            studentId : studentId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
        
        commonAjax(FOLLOWUPURL, postData, getClosingBalStudentCallback,"Please wait...Adding your item.");
        
        function getClosingBalStudentCallback(flag , data) {
            
            if (data.status == "Success" && flag) {
                
                console.log(data);
                GBL_CLOS_BAL_DATA = data.data;
                if( GBL_CLOS_BAL_DATA != "" && GBL_CLOS_BAL_DATA != "No record found" ){
                    GBL_CLOS_BAL_AMT = GBL_CLOS_BAL_DATA[0].closingBalAmt;
                    GBL_REMAIN_PAYABLE_AMT = GBL_CLOS_BAL_AMT;
                    $('#amount').val( fixedTo2(GBL_CLOS_BAL_DATA[0].closingBalAmt) );
                }
                
            } else {

                GBL_CLOS_BAL_DATA = [];
                GBL_CLOS_BAL_AMT = 0;
                GBL_REMAIN_PAYABLE_AMT = 0;
                $('#amount').val( fixedTo2(GBL_CLOS_BAL_DATA[0].closingBalAmt) );

                if (flag){
                  // displayAPIErrorMsg( data.status , GBL_ERR_NO_INVOICE );
                }
                else{
                    toastr.error(SERVERERROR);
                }
            }
        }
    }

}

// CHECK MODE OF PAYMENT AND SHOW/HIDE FIELDS ACCORDINGLY - ADDED BY VISHAKHA TAK
function checkModeOfPayment(context) {
    
    var modeOfPayment = $(context).val();
    var modeOfPaymentName = $(context).find('option:selected').text();

    if( modeOfPayment != "" && modeOfPayment != "-1" ){

        if( modeOfPaymentName.toLowerCase() == 'cash' ){
            $('.chequeNeftDiv').hide(300); // HIDE CHEQUE/DD/NEFT FIELDS
            $('.bankNameDiv').hide(300); // HIDE BANK NAME FIELD
            $('.cardDetailDiv').hide(300); // HIDE CARD DETAIL FIELDS
        }else if( modeOfPaymentName.toLowerCase().indexOf('credit') != -1 ||  modeOfPaymentName.toLowerCase().indexOf('debit') != -1  ) {
            $('.chequeNeftDiv').hide(300); // HIDE CHEQUE/DD/NEFT FIELDS
            $('.cardDetailDiv').show(300); // SHOW CARD DETAIL FIELDS
            $('.bankNameDiv').show(300); // SHOW BANK NAME FIELD
        }else{
            $('.chequeNeftDiv').show(300); // SHOW CHEQUE/DD/NEFT FIELDS
            $('.cardDetailDiv').hide(300); // HIDE CARD DETAIL FIELDS
            $('.bankNameDiv').show(300); // SHOW BANK NAME FIELD
        }
        $('#bankBranchName').val('');
        $('#chequeNo').val('');
        $('#chequeDate').val('');
        $('#lastFourDigitCardNo').val('');

    }else{
        $('.chequeNeftDiv').show(300); // SHOW CHEQUE/DD/NEFT FIELDS
        $('.cardDetailDiv').hide(300); // HIDE CARD DETAIL FIELDS
    }
}