    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-07-2018.
 * File : collectPaymentController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_PAYMENT_DATA = []; // PAYMENT DATA
var GBL_INVOICE_LIST = []; // INVOICE LISTING
var GBL_REMAIN_AMT = "";

// GET PAYMENT DATA
function getPaymentData(){

    if( localStorage.indeCampusEditStudId != undefined ){
            
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : 'listPaymentData', 
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID,
            orgId : checkAuth(17, 70),
            studentId : localStorage.indeCampusEditStudId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        // return false; console.log(postData);
        commonAjax(COMMONURL, postData, paymentGenerationCallback,"Please Wait... Getting Dashboard Detail");

        function paymentGenerationCallback( flag , data ){

            if (data.status == "Success" && flag) {
                console.log(data);
                GBL_PAYMENT_DATA = data.data;
                renderPaymentData();

            } else {
                    

                GBL_PAYMENT_DATA = [];
                renderPaymentData();
                if (flag){
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_PAYMENT );
                }
                else{
                    toastr.error(SERVERERROR);
                }
            }

            if( localStorage.indeCampusBookEventPayment != undefined && localStorage.indeCampusBookEventPayment != "" ){
                    
                // WHEN REDIRECTED AFTER BOOK EVENT SEATS
                var tempInvoice = {
                    clientInvoiceId : localStorage.indeCampusBookEventPayment
                }
                GBL_INVOICE_LIST.push(tempInvoice);
                renderInvoice();
            }else{
                
                getInvoiceListing();

            }

        }
    }else if( localStorage.indeCampusPropCustId != undefined ){ // WHEN REDIRECTED AFTER BOOKING PROPERTY

        if( localStorage.indeCampusBookPropPayment != undefined && localStorage.indeCampusBookPropPayment != "" ){

                var propertData = JSON.parse(localStorage.indeCampusBookPropPayment);
                var tempInvoice = {
                    clientInvoiceId : propertData.clientInvoiceId
                }
                GBL_INVOICE_LIST.push(tempInvoice);
                renderInvoice();
                $('#referenceInvoiceNo').val( propertData.clientInvoiceId ).selectpicker('refresh');
                $('#referenceInvoiceNo').trigger('change');
        }

    }else{
        displayAPIErrorMsg( '' , GBL_ERR_NO_PAYMENT );
    }

}

// RENDER PAYMENT DATA
function renderPaymentData(){

    var tHead = '<table class="display datatables-basic" id="PaymentTable">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Mode of Payment</th>'+
                            '<th>Name of Bank</th>'+
                            '<th>Name of Branch</th>'+
                            '<th>Cheque No. / DD No. / NEFT ID</th>'+
                            '<th>Cheque / DD Date / NEFT Date</th>'+
                            '<th>Amount </th>'+
                            '<th>Reference Invoice Number</th>'+
                            '<th>Remarks</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="paymentData">';

    var tEnd = '</tbody></table>';
    var newRow = '';
    if( GBL_PAYMENT_DATA != "" && GBL_PAYMENT_DATA != "No record found" ){

        GBL_PAYMENT_DATA.forEach(function( record , index ){

            var paymentAmount = (record.paidAmount).split(',').join('');
            var bankName = "";
            var idx = findIndexByKeyValue( roleAuth.data.bankName , 'ID' , record.bankName );
            if( idx != -1 ){
                bankName = roleAuth.data.bankName[idx].VALUE;
            }

            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.paymentModeName +'</td>'+
                            '<td>'+ bankName +'</td>'+
                            '<td>'+ record.branchName +'</td>'+
                            '<td>'+ record.paymentReferenceNo +'</td>'+
                            '<td>'+ (( record.paymentReferenceDate != "" && record.paymentReferenceDate != "0000-00-00" && record.paymentReferenceDate != "1970-01-01"  )? mysqltoDesiredFormat(record.paymentReferenceDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(paymentAmount)) +'</td>'+
                            '<td>'+ record.inoiceId +'</td>'+
                            '<td>'+ record.paymentRemark +'</td>'+
                        '</tr>';

        });

    }
    $('#paymentListDiv').html( tHead + newRow + tEnd );
    $('#PaymentTable').dataTable();

}

// GENERATE PAYMENT DETAILS 
function generatePayment() {    

    // PAYMENT DETAILS TAB
    var paymentMode = $('#paymentMode').val();
    var bankName = $('#bankName').val();
    var branchName = $('#branchName').val();
    var chequeDDNeftNo = $('#chequeDDNeftNo').val();
    var chequeDDNeftDate = $('#chequeDDNeftDate').val();
    var paymentAmount = $('#paymentAmount').val();
    var referenceInvoiceNo = $('#referenceInvoiceNo').val();
    var paymentRemark = $('#paymentRemark').val();
    var lastFourDigitCardNo = $('#lastFourDigitCardNo').val();

    var refInvTotalCharges = $('#referenceInvoiceNo option:selected').attr('totalcharges');

    var paymentModeName = "";
    if( paymentMode != "" ){
        paymentModeName = $('#paymentMode option:selected').text().trim();
    }

    if( referenceInvoiceNo == "-1" || referenceInvoiceNo == null || referenceInvoiceNo == "" ){
        toastr.warning("Please select reference invoice number");
        addFocusId('referenceInvoiceNo');
        return false;

    }else if( paymentAmount == "" ){
        toastr.warning("Please enter payment amount");
        addFocusId('paymentAmount');
        return false;

    }else if( parseFloat(paymentAmount) != "" && parseFloat(GBL_REMAIN_AMT) != "" && parseFloat(paymentAmount) > parseFloat(GBL_REMAIN_AMT) ){
        toastr.warning("Payment amount cannot be greater than invoice amount");
        addFocusId('paymentAmount');
        return false;

    }else if( paymentMode == "" || paymentMode == "-1" || paymentMode == null ){
        toastr.warning("Please select payment mode");
        addFocusId('paymentMode');
        return false;

    }else if( ( paymentMode != "" && paymentModeName.toLowerCase() != "cash" &&  (bankName == "" || bankName == "-1") ) ){
        toastr.warning("Please select bank name");
        addFocusId('bankName');
        return false;

    }else if( ( paymentMode != "" ) && ( paymentModeName.toLowerCase() != "cash" ) && ( paymentModeName.toLowerCase().indexOf('credit') == -1 ) && ( paymentModeName.toLowerCase().indexOf('debit') == -1 ) && (branchName == "") ){
        toastr.warning("Please enter branch name");
        addFocusId('branchName');
        return false;

    }else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase() != "cash") && ( paymentModeName.toLowerCase().indexOf('credit') == -1 ) && ( paymentModeName.toLowerCase().indexOf('debit') == -1 ) && (chequeDDNeftNo == "") ){
        toastr.warning("Please enter cheque / dd / neft no");
        addFocusId('chequeDDNeftNo');
        return false;

    }else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase() != "cash") && ( paymentModeName.toLowerCase().indexOf('credit') == -1 ) && ( paymentModeName.toLowerCase().indexOf('debit') == -1 ) && (chequeDDNeftDate == "") ){
        toastr.warning("Please select cheque / dd / neft date");
        addFocusId('chequeDDNeftDate');
        return false;

    }else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase().indexOf('credit') != -1 || paymentModeName.toLowerCase().indexOf('debit') != -1 ) && (lastFourDigitCardNo == "") ){
        toastr.warning("Please enter last four digits of card number");
        addFocusId('lastFourDigitCardNo');
        return false;

    }else if( (paymentMode != "" ) && ( paymentModeName.toLowerCase().indexOf('credit') != -1 || paymentModeName.toLowerCase().indexOf('debit') != -1 ) && (lastFourDigitCardNo != "") && (lastFourDigitCardNo.length < 4  ) ){
        toastr.warning("Please enter valid last four digits of card number");
        addFocusId('lastFourDigitCardNo');
        return false;

    } 

    if( paymentModeName.toLowerCase() == "cash" ){
        bankName = "";
        branchName = "";
        chequeDDNeftNo = "";
        chequeDDNeftDate = "";
        lastFourDigitCardNo = "";
    }
    if( ( paymentModeName.toLowerCase().indexOf('credit') != -1 ) || ( paymentModeName.toLowerCase().indexOf('debit') != -1 ) ){
        branchName = "";
        chequeDDNeftNo = "";
        chequeDDNeftDate = "";
    }
    if( refInvTotalCharges == undefined || refInvTotalCharges == "undefined" ){
        refInvTotalCharges = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    if( localStorage.indeCampusPropCustId != undefined && localStorage.indeCampusPropCustId != "" ){

        var postData = {
            requestCase : 'addPaymentData', 
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID,
            orgId : checkAuth(17, 69),
            studentId : localStorage.indeCampusPropCustId,
            customerId : localStorage.indeCampusPropCustId,
            type : 'customer',
            invoiceId : referenceInvoiceNo,
            bankName : bankName,
            branchName : branchName,
            chequeDDNeftNo : chequeDDNeftNo,
            chequeDDNeftDate : chequeDDNeftDate,
            paidAmount : paymentAmount,
            paymentMode : paymentMode,
            paymentModeName : paymentModeName,
            paymentRemark : paymentRemark,
            invoiceTotalAmt : refInvTotalCharges,
            lastFourDigitCardNo : lastFourDigitCardNo,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        // return false; console.log(postData);
        commonAjax(COMMONURL, postData, paymentGenerationCallback,"Please Wait... Getting Dashboard Detail");

    }else{

        var postData = {
            requestCase : 'addPaymentData', 
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID,
            orgId : checkAuth(17, 69),
            studentId : localStorage.indeCampusEditStudId,
            invoiceId : referenceInvoiceNo,
            bankName : bankName,
            branchName : branchName,
            chequeDDNeftNo : chequeDDNeftNo,
            chequeDDNeftDate : chequeDDNeftDate,
            paidAmount : paymentAmount,
            paymentMode : paymentMode,
            paymentModeName : paymentModeName,
            paymentRemark : paymentRemark,
            invoiceTotalAmt : refInvTotalCharges, 
            lastFourDigitCardNoNo : lastFourDigitCardNo,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        // console.log(postData); return false; 
        commonAjax(COMMONURL, postData, paymentGenerationCallback,"Please Wait... Getting Dashboard Detail");

    }

    function paymentGenerationCallback( flag , data ){

        if (data.status == "Success" && flag) {
            console.log(data);
            // GBL_PAYMENT_DATA = data.data;
            // renderPaymentData();
            resetData();
            if( localStorage.indeCampusBookEventPayment != undefined && localStorage.indeCampusBookEventPayment != "" ){
                
                    localStorage.removeItem('indeCampusBookEventPayment');
                    navigateToEventManagementList(); // REDIRECT TO EVENT MGMT LIST PAGE AFTER BOOK EVENT SEATS

            }else{

                if( localStorage.indeCampusPropCustId != undefined && localStorage.indeCampusPropCustId != "" ){

                    localStorage.removeItem('indeCampusPropCustId');
                    navigateToInquiryCalender(); // REDIRECT TO INQ CALENDAR PAGE AFTER COLLECTING PAYMENT 

                }else if( localStorage.indeCampusRenewEnrollFlag != undefined && localStorage.indeCampusRenewEnrollFlag != "" && localStorage.indeCampusRenewEnrollFlag != false ){
                    localStorage.removeItem('indeCampusRenewEnrollFlag');
                    navigateToEnrollmentMaster();
                }else{
                    getPaymentData(); // GET ALL DATA
                }

            }
            

        } else {
            
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_ADD_PAY );
            }
            else{
                toastr.error(SERVERERROR);
            }
        }

    }
}

// RESET FIELD VALUES
function resetData(){

    $('#paymentMode').val('').selectpicker('refresh');
    $('#paymentMode').trigger('change');
    $('#bankName').val('-1').selectpicker('refresh');
    $('#branchName').val('');
    $('#chequeDDNeftNo').val('');
    $('#chequeDDNeftDate').val('');
    $('#paymentAmount').val('');
    $('#referenceInvoiceNo').val('').selectpicker('refresh');
    $('#paymentRemark').val('');
    $('#lastFourDigitCardNo').val('');
    GBL_REMAIN_AMT = "";

}

// PAGE LOAD EVENTS
function pageInitialEvents(){

    var paymentMode = roleAuth.data.paymentMode;
    setOption("0",'paymentMode',paymentMode,''); // SET PAYMENT MODE
    $('#paymentMode').selectpicker('refresh');

    var bankName = roleAuth.data.bankName;
    setOption("0",'bankName',bankName,'Select Bank'); // SET BANK NAME
    $('#bankName').selectpicker('refresh');

    $('#PaymentTable').dataTable();
    $('#referenceInvoiceNo').selectpicker('refresh');
    getPaymentData();

    if( $('#paymentMode').val() != "" && $('#paymentMode').val() != null && $('#paymentMode').val() != "-1" ){
        var modeOfPaymentName = $('#paymentMode').find('option:selected').text().trim();
        if( modeOfPaymentName.toLowerCase() != "cash" ){
            $('.chequeNeftDiv').show();
            $('.bankNameDiv').show();
            $('.cardDetailDiv').hide();
        }

    }
}

function getInvoiceListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getInvoiceListByStudent', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(16, 66),
        studentId : localStorage.indeCampusEditStudId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // return false; console.log(postData);
    commonAjax(COMMONURL, postData, getInvoiceListingCallback,"Please Wait... Getting Dashboard Detail");

    function getInvoiceListingCallback( flag , data ){

        if (data.status == "Success" && flag) {
            console.log(data);
            GBL_INVOICE_LIST = data.data;
            renderInvoice();

        } else {
                

            GBL_INVOICE_LIST = [];
            renderInvoice();
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NO_PAY_INVOICE );
            }
            else{
                toastr.error(SERVERERROR);
            }
        }

    }

}

// RENDER INVOICE LIST
function renderInvoice(){

    var innerHtml = '<option value="-1">Select Invoice</option>';
    if( GBL_INVOICE_LIST != "" && GBL_INVOICE_LIST != "No record found" ){

        GBL_INVOICE_LIST.forEach(function( record , index ){
            var remainingAmt = "" , totalCharges = "";
            if( record.remainingAmt != undefined && record.remainingAmt != "" ){
                remainingAmt = record.remainingAmt;
            }
            if( record.totalCharges != undefined && record.totalCharges != "" ){
                totalCharges = record.totalCharges;
            }

            innerHtml += '<option value="'+ record.clientInvoiceId +'" remainingAmt="'+ remainingAmt +'" totalCharges="'+ totalCharges +'">'+ record.clientInvoiceId +'</option>';

        });
    }
    $('#referenceInvoiceNo').html( innerHtml );
    $('#referenceInvoiceNo').selectpicker('refresh');
    if( localStorage.indeCampusRenewEnrollFlag != undefined && localStorage.indeCampusRenewEnrollFlag != "" && localStorage.indeCampusRenewEnrollFlag != false ){
        $('#referenceInvoiceNo').val( GBL_INVOICE_LIST[GBL_INVOICE_LIST.length-1].clientInvoiceId ).selectpicker('refresh');
        $('#referenceInvoiceNo').trigger('change');
    }
}

function getRemainingAmt(context){

    var invoiceId = $(context).val();

    if( invoiceId != "" && invoiceId != "-1" && invoiceId != null ){

        var remainingAmt = $(context).find(":selected").attr('remainingAmt');
        var totalCharges = $(context).find(":selected").attr('totalCharges');
        if( remainingAmt == undefined || remainingAmt == "undefined" || remainingAmt == "" || remainingAmt == "0" ){

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

            var postData = {
                requestCase : 'invoiceRemainingPayment', 
                clientId : tmp_json.data[0].FK_CLIENT_ID,
                userId : tmp_json.data[0].PK_USER_ID,
                orgId : checkAuth(23, 94),
                invoiceId : invoiceId,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            };
            // return false; console.log(postData);
            commonAjax(COMMONURL, postData, getRemainingAmtCallback,"Please Wait... Getting Dashboard Detail");

            function getRemainingAmtCallback( flag , data ){

                if (data.status == "Success" && flag) {
                    console.log(data);
                    GBL_REMAIN_AMT = data.data[0].remainingAmt;
                    $('#paymentAmount').val( data.data[0].remainingAmt );
                    $(context).find(":selected").attr('totalCharges',data.data[0].totalCharges);
                } else {
                        
                    if (flag){
                        // displayAPIErrorMsg( data.status , GBL_ERR_NO_PAY_INVOICE );
                    }
                    else{
                        toastr.error(SERVERERROR);
                    }
                }

            }
        }else{
            GBL_REMAIN_AMT = remainingAmt;
            $('#paymentAmount').val( remainingAmt );
            if( totalCharges != undefined && totalCharges != "undefined" ){
                $(context).find(":selected").attr('totalCharges',totalCharges);
            }
        }
    }else{
        $('#paymentAmount').val( '' );
        $(context).find(":selected").attr('totalCharges','');
    }

}

// CHECK MODE OF PAYMENT AND SHOW/HIDE FIELDS ACCORDINGLY
function checkModeOfPayment(context) {
    
    var modeOfPayment = $(context).val();
    var modeOfPaymentName = $(context).find('option:selected').text();

    if( modeOfPayment != "" && modeOfPayment != "-1" ){

        if( modeOfPaymentName.toLowerCase() == 'cash' ){
            $('.chequeNeftDiv').hide(300); // HIDE CHEQUE/DD/NEFT FIELDS
            $('.bankNameDiv').hide(300); // HIDE BANK NAME FIELD
            $('.cardDetailDiv').hide(300); // HIDE CARD DETAIL FIELDS
        }else if( modeOfPaymentName.toLowerCase().indexOf('credit') != -1 ||  modeOfPaymentName.toLowerCase().indexOf('debit') != -1  ) {
            $('.chequeNeftDiv').hide(300); // HIDE CHEQUE/DD/NEFT FIELDS
            $('.cardDetailDiv').show(300); // SHOW CARD DETAIL FIELDS
            $('.bankNameDiv').show(300); // SHOW BANK NAME FIELD
        }else{
            $('.chequeNeftDiv').show(300); // SHOW CHEQUE/DD/NEFT FIELDS
            $('.cardDetailDiv').hide(300); // HIDE CARD DETAIL FIELDS
            $('.bankNameDiv').show(300); // SHOW BANK NAME FIELD
        }
        $('#branchName').val('');
        $('#chequeDDNeftNo').val('');
        $('#chequeDDNeftDate').val('');
        $('#lastFourDigitCardNo').val('');

    }else{
        $('.chequeNeftDiv').show(300); // SHOW CHEQUE/DD/NEFT FIELDS
        $('.cardDetailDiv').hide(300); // HIDE CARD DETAIL FIELDS
    }
}