/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : workflowController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportsList = [];
var reportsUserList = [];

var stDate , enDate = "";

var wonStatId = "";
var pivotData = [];
var GBL_EXCELJSON = [];

function getReportData(){
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    // var requestedDate = ( $("#requestedDate").val().length > 0) ? ddmmyyToMysql($("#requestedDate").val()) : "";
   // var requestedUserId = $("#userList").val();
    
//    if( requestedUserId == null ){
//        requestedUserId = $("#userList").val();
//    } 

    var postData = {
        requestCase: 'getWhyReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(28,116),
        requestedStDate: stDate,
        requestedEnDate: enDate,
        //requestedUserId: requestedUserId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getReportDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getReportDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        console.log(data);
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        yesterday = mysqltoDesiredFormat(yesterday,'dd-MM-yyyy')

        $('#requestedDate').val( yesterday );

        var user = JSON.parse( localStorage.indeCampusUserDetail );
        var userId = user.data[0].PK_USER_ID;

        reportsList = data.data.userWiseDetail;
        reportsUserList = data.data.allUserDetail;
        
        renderUserList();
        
        var index = findIndexByKeyValue( reportsList , 'userId' , userId )
        if( index != "-1" ){
            renderReportData(reportsList[index]);
        }else{
            renderReportData(reportsList[0]);
        }

        var localStatus = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(localStatus['data']['statusListing'],'statusCatName','INQUIRY');
        if( index != "-1" ){

            var innerIndex = findIndexByKeyValue(localStatus['data']['statusListing'][index]['statusListing'],'thirdPartyFlag','1');
            if( innerIndex != "-1" ){
                $('#statusId').attr("statusId",localStatus['data']['statusListing'][index]['statusListing'][innerIndex]['ID']);
            }
        }

        // wonStatId
        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        var statListInd = findIndexByKeyValue( roleAuth.data.statusListing , "statusCatName" , "INQUIRY" );
        if( statListInd != "-1" ){
            var thirdPIndex = findIndexByKeyValue( roleAuth.data.statusListing[statListInd].statusListing , "thirdPartyFlag" , "1" );
            if( thirdPIndex != "-1" ){
                wonStatId = roleAuth.data.statusListing[statListInd].statusListing[thirdPIndex].ID;
            }
        }

        var tempData = [];
        pivotData = [];
        data.data.userWiseDetail.forEach(function( record , index ){
            tempData = [];
            if( record.activityPerformedMissed != undefined ){
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Activity Performed",
                                Count : record.activityPerformedMissed[0].performedActivity,
                            });

                pivotData.push({
                                UserName : record.userName,
                                Type : "Activity Missed",
                                Count : record.activityPerformedMissed[0].missedActivity,
                            });
            }
            
            if( record.dealClosedCount != undefined ){
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Deals Closed",
                                Count : record.dealClosedCount[0].dealClosedByUser,
                            });

            }

            if( record.dealCancelCount != undefined ){
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Deals Cancelled",
                                Count : record.dealCancelCount[0].dealCancelledByUser,
                            });

            }

            if( record.distanceTravelByUser != undefined ){
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Distance Travel",
                                Count : record.distanceTravelByUser[0].totalDistanceTraveld,
                            });
            }
            
            // if( record.followpCountMiss != undefined ){
                
            //     pivotData.push({
            //                     UserName : record.userName,
            //                     Type : "Followup Completed",
            //                     Count : record.activityPerformedMissed[0].completeFollowup,
            //                 });
            //     pivotData.push({
            //                     UserName : record.userName,
            //                     Type : "Followup Not Completed",
            //                     Count : record.followpCountMiss[0].notCompleteFollowup,
            //                 });

            // }

            if( record.proposalCount != undefined ){
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Proposal Count",
                                Count : record.proposalCount[0].totalProposalCount,
                            });

            }

            if( record.totalInquiryConfirmed != undefined ){
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Inquiry Generated",
                                Count : record.totalInquiryConfirmed[0].inqCount,
                            });
                pivotData.push({
                                UserName : record.userName,
                                Type : "Total Confirmed POT Amount",
                                Count : record.totalInquiryConfirmed[0].totalPotAmount,
                            });

            }

            if( record.totalInquiryPassOn != undefined ){

                pivotData.push({
                                UserName : record.userName,
                                Type : "Inquiry on Hand Count",
                                Count : record.totalInquiryPassOn[0].inqCount,
                            });
                
                pivotData.push({
                                UserName : record.userName,
                                Type : "Total Passon POT Amount",
                                Count : record.totalInquiryPassOn[0].totalPotAmountPasson,
                            });
            }
        });
        
        GBL_EXCELJSON = [];
        GBL_EXCELJSON = "Success";

        GeneratePivotTable(pivotData);

        addFocusId( 'userList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_REPORT );
        else
            toastr.error(SERVERERROR);
    }
}

function renderUserList(){
    if( reportsList != 'No record found' ){

        var report = reportsUserList;

        var options = ''
        for ( var i=0; i<report.length; i++ ){

            var user = JSON.parse( localStorage.indeCampusUserDetail );
            var userId = user.data[0].PK_USER_ID;

            //var defaultSel = '';
//            if( userId == report[i].PK_USER_ID ){
//                defaultSel = 'selected';
//            }

            options += '<option value="'+ report[i].PK_USER_ID +'">'+ report[i].FULLNAME +'</option>';
        }

        $('#userList').html( options );
        $('#userList').val( userId );
        $('#userList').selectpicker("refresh");

        changeUserReport();
    }
}

function renderReportData(data){

    if(data != 'No record found'){

        var reportData = data;
        var activityPerformedMissed = reportData.activityPerformedMissed[0].performedActivity;
        var activityMissed = reportData.activityPerformedMissed[0].missedActivity;
        var dealClosedCount = reportData.dealClosedCount[0].dealClosedByUser;
        var dealCancelCount = reportData.dealCancelCount[0].dealCancelledByUser;
        var distanceTravelByUser = reportData.distanceTravelByUser[0].totalDistanceTraveld;
        var followpCountMiss = reportData.followpCountMiss[0].completeFollowup;
        var followpCountMissed = reportData.followpCountMiss[0].notCompleteFollowup;
        var proposalCount = reportData.proposalCount[0].totalProposalCount;
        var totalInquiryCreateCount = reportData.totalInquiryConfirmed[0].inqCount;
        var totalInquiryTransferCount = reportData.totalInquiryPassOn[0].inqCount;
        var totalInquiryCreatePotAmt = parseFloat(reportData.totalInquiryConfirmed[0].totalPotAmount).toFixed(2);
        var totalInquiryTransferPotAmt = parseFloat(reportData.totalInquiryPassOn[0].totalPotAmountPasson).toFixed(2);

        $('#activityPerformed').html( activityPerformedMissed );
        $('#activityMissed').html( activityMissed );
        $('#dealClosed').html( dealClosedCount );
        $('#distTravel').html( distanceTravelByUser );
        $('#followpCountMiss').html( followpCountMiss );
        $('#followupMissed').html( followpCountMiss );
        $('#propCount').html( proposalCount );
        $('#dealCancel').html( dealCancelCount );
        $('#openInqDashboard').html( totalInquiryCreateCount+" / "+  totalInquiryCreatePotAmt);
        $('#totalInqPassOn').html( totalInquiryTransferCount + " / " +  totalInquiryTransferPotAmt);
        
    }else{
        toastr.warning('No record found');
    }

}


function GeneratePivotTable(mps) {
    // console.log(mps);
    var Sum = $.pivotUtilities.aggregators["Sum"];
    $("#inqReportList").pivot(mps, {
        aggregator: Sum(["Count"]),
        rows: ["UserName"],
        cols: ["Type",]
    });
}


function changeUserReport(){

    $('#userList').change(function(){
        var userId = $(this).val();
        var userName = $("option:selected", this).text();

        var index = findIndexByKeyValue( reportsList , 'userId' , userId )
        if( index != "-1" ){
            renderReportData(reportsList[index]);
            $('#userList').selectpicker();
        }
    });
}


function clientopenInq(context){
    var statusId = $(context).attr("statusId");
    localStorage.POTGcurrentStatusId = statusId;
    navigateToInquiry();
}

function navigateToInqReportWithFilter( context , type ){
    
    if (checkAuth(28, 116, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        var uname = $('#userList').val();
        var unameTxt = $('#userList option:selected').text();
        localStorage.POTGInqToWhyType = type;
        localStorage.POTGInqToWhyUnameId = uname;
        localStorage.POTGInqToWhyUname = unameTxt;
        localStorage.POTGInqToWhyStDate = stDate;
        localStorage.POTGInqToWhyEnDate = enDate;

        window.open("inqReport.html");
        // window.location.href = "inqReport.html";
    }
}

function navigateToInqReportWithDealClosed(){
    
    if (checkAuth(28, 116, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        var uname = $('#userList').val();
        var unameTxt = $('#userList option:selected').text();
        localStorage.POTGInqToWhyUnameId = uname;
        localStorage.POTGInqToWhyUname = unameTxt;
        localStorage.POTGInqToWhyStDate = stDate;
        localStorage.POTGInqToWhyEnDate = enDate;
        localStorage.POTGInqToWhyStatus= wonStatId;

        window.open("inqWonReport.html");
        // window.location.href = "inqWonReport.html";
    }
}

function navigateToactivityWithFilter(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGInqToWhyActUser = $('#userList').val();
        localStorage.POTGInqToWhyActUserTxt = $('#userList option:selected').text();
        localStorage.POTGInqToWhyActstDate = stDate;
        localStorage.POTGInqToWhyActenDate = enDate;
        localStorage.POTGInqToWhyActstatType = wonStatId;

       window.open("activityMainReport.html");
        // window.location.href = "activityMainReport.html";
    }
}

function navigateToactivityWithFilterMiss(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGInqToWhyActUser = $('#userList').val();
        localStorage.POTGInqToWhyActUserTxt = $('#userList option:selected').text();
        localStorage.POTGInqToWhyActstDate = stDate;
        localStorage.POTGInqToWhyActenDate = enDate;
        localStorage.POTGInqToWhyActDelayType = "1";
        localStorage.POTGInqToWhyActMiss = true;

        window.open("activityMainReport.html");
        // window.location.href = "activityMainReport.html";
    }
}

function navigateToPropReportWithFilter(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGPropToWhyUser = $('#userList').val();
        localStorage.POTGPropToWhyUserTxt = $('#userList option:selected').text();
        localStorage.POTGPropToWhystDate = stDate;
        localStorage.POTGPropToWhyenDate = enDate;

        window.open("proposalReport.html");
        // window.location.href = "proposalReport.html";
    }
}


function downLoadExcel(){
    if( GBL_EXCELJSON.length < 2 ){
        toastr.warning('Please Search Report Before Downloading...');
    }else{
        customCsvFormatSave($('#inqReportList').html(), "Why_Report.xls");
    }
}

function hideShow(type){
    if( type == "tile" ){
        $('.tileDesign').show(500);
        $('.pivotChart').hide(500);
    }else{
        $('.tileDesign').hide(500);
        $('.pivotChart').show(500);
    }
}

function pageInitialEvents(){
    
    var start = moment();
    var end = moment();

    start = moment().subtract(1, 'day');
    end = moment().subtract(1, 'day');
    
    stDate = moment().subtract(1, 'day').format('YYYY-MM-DD');
    enDate = moment().subtract(1, 'day').format('YYYY-MM-DD');
    
    $('#dateRangePicker span').html(moment().subtract(1, 'day').format('DD, MMMM, YYYY') + ' - ' + moment().subtract(1, 'day').format('DD, MMMM, YYYY'));
    $('#dateRangePicker').daterangepicker(
    {   
        maxDate: end,
        startDate: start,
        endDate: end,
        opens:'left',
        locale: {
                format: 'YYYY-MM-DD'
            },
        }, function(start, end, label) {

             stDate = start.format('YYYY-MM-DD');
             enDate = end.format('YYYY-MM-DD');

            $('#dateRangePicker span').html(start.format('DD, MMMM, YYYY') + ' - ' + end.format('DD, MMMM, YYYY'));
            console.log(stDate);
            console.log(enDate);

            getReportData();
    });

    getReportData();
}