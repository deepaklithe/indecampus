/*
 *
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 01-01-2014.
 * File : lib.
 * File Type : .js.
 * Project : ePOS
 *
 * */
//removes \ from given string
function stripslashes(value) {
    return value.replace(/\\/g, '');
}

/* function to open a URL in a new tab START */
function OpenInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}
/* function to open a URL in a new tab END */

/*(Added by Sachin Jani) functions to communicate with the web worker of sql.js START */

function messageWorker(message, callback) {

    //console.log(message);
    function listen(e) {
        //console.log("Message from worker received...");
        if (e.data.id === message.id) {
            worker.removeEventListener("message", listen); // .
            callback(e.data);
            //$("#ajaxloader").addClass("hideajaxLoader");
        }
    }
    //changeAjaxMsg("loading...please wait..");
    //$("#ajaxloader").removeClass("hideajaxLoader");
    worker.addEventListener("message", listen);
    worker.postMessage(message);

}



function messageWorkerWithoutCallBack(message) {

    worker.postMessage(message);
}
/* functions to communicate with the web worker of sql.js START */

/* functions to block/unblock the whole page START */

// dependency : jquery.blockUI.js

function blockTheWholeDamnPage()
{
     $.blockUI({ message: null }); 
     DISABLEKEYCODESHORTCUTS = true;
}

function unBlockTheWholeDamnPage()
{
    $.unblockUI();
    DISABLEKEYCODESHORTCUTS = false;
}
/* functions to block/unblock the whole page END */

//disable right click on page
function disableContextMenu() {
    $(document).bind("contextmenu", function (e) {
        e.preventDefault();
    });
    window.addEventListener('keydown', function (event) {

        // if the keyCode is 123 ( f12 key was pressed )
        //16 17 18 cltr+shift+alter
        if ((event.ctrlKey && event.keyCode == 77)) {
            window.print();
        } else if ((event.ctrlKey && event.keyCode == 66)) {
            reloadWin();
        }
        if ((event.ctrlKey && event.keyCode == 80) || (event.ctrlKey && event.keyCode == 82) || (event.ctrlKey && event.keyCode == 67) || (event.ctrlKey && event.keyCode == 86) || (event.keyCode >= 112 && event.keyCode <= 123) || (event.ctrlKey && event.shiftKey && event.keyCode == 73)) {

            // prevent default behaviour
            event.preventDefault();
            toastr.warning(DISABLEKEY);
            return false;
        }

        /*Alt + left arrow and right arrow key are disabled : code added by Sachin Jani 30/07/215 */
        if (event.altKey && event.keyCode == 37) {
            event.preventDefault();
            toastr.warning(DISABLEKEY);
            return false;
        } else if (event.altKey && event.keyCode == 39) {
            event.preventDefault();
            toastr.warning(DISABLEKEY);
            return false;
        }
        /*code added by sachin jani END */

    });
}

//10 digit time stamp like php
function getPhpTimeStamp() {
    return parseInt((new Date().getTime()) / 1000);
}

//chk compilable browser for sqlite db
function browserChk() {
//	if (navigator.appVersion.indexOf("Chrome") != -1) {} else if (navigator.userAgent.indexOf("Safari") != -1) {} else {
//		alert(WRONGBROWSER);
//	}
}

//minified version of console.log
function log(msg) {
    console.log(msg);
}

// return date in YYYYMONDDMMSS format
function getDateValue() {
    var date = new Date();
    var datevalue = date.getFullYear().toString() + (date.getMonth() + 1).toString() + date.getDate().toString() + date.getMinutes() + date.getSeconds();
    return datevalue;
}

/* return date in YYYYMONDD format
 function getDateValue() {
 var date = new Date();
 var datevalue = date.getFullYear().toString() + (date.getMonth() + 1).toString() + date.getDate().toString();
 return datevalue;
 }*/

//return timestamp in YYYY-Mon-dd hh:mm:ss
function getTodaysTimeStamp(timestamp) {
    var today;
    if (timestamp == undefined) {
        today = new Date();
    } else {
        today = new Date(parseInt(timestamp) * 1000);
    }

    var dd = today.getDate();
    var mon = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var mm = today.getMinutes();
    var ss = today.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mon < 10) {
        mon = '0' + mon
    }
    if (hh < 10) {
        hh = '0' + hh
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    if (ss < 10) {
        ss = '0' + ss
    }

    today = yyyy + '-' + mon + '-' + dd + " " + hh + ":" + mm + ":" + ss;
    return today;
}

//return date in php format
function getTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

//return localStorage.POTGdayDate
function getDayDate()
{
    return localStorage.POTGdayDate;
}

//return string in Title Case
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

//fix precision to 2 decimal point
function fixedTo2(value) {
    return parseFloat(value).toFixed(2);
}

//gives upper rounded value for eg 10.5 = 10 and 10.6 = 11
function ceil(value) {
    return Math.round(value);
}

//get basename (php) of url - rpatel - 04/22/14
function baseName(str) {
    var base = new String(str).substring(str.lastIndexOf('/') + 1);
    if (base.lastIndexOf(".") != -1)
        base = base.substring(0, base.lastIndexOf("."));
    return base;
}

//check user session - rpatel - 04/22/14
function chkusr_session() {
    var cururl = location.href;
    var login_Username = localStorage.POTGgetItem('username');
    var login_Session = localStorage.POTGgetItem('loginSession');
    var login_Status = localStorage.POTGgetItem('loginStatus');
    var last_Url = localStorage.POTGgetItem('lastUrl');
    if (login_Username && last_Url && login_Session && login_Status && login_Status == CryptoJS.AES.decrypt(login_Session, login_Username) && (cururl.indexOf('index.html') > -1 || baseName(cururl).length == 0)) {
        // if the page is "index.html" and the required localStorage parameters are available .
        //window.location.href = last_Url;
        logout();
    } else if ((baseName(cururl).length > 0 && baseName(cururl) != 'index') && (!login_Username || !login_Session || !login_Status || login_Status != CryptoJS.AES.decrypt(login_Session, login_Username))) {
        //if the page is NOT "index.html" and the required localStorage parameters are NOT available.
        logout();
    }
}

// this function remove place holder value and return blank value
$.fn.valPlaceHolder = function () {
    var $this = $(this),
            val = $this.eq(0).val();
    if (val == $this.attr('placeholder'))
        return '';
    else
        return val;
}

//Returns index or -1 if the key doesn't exist
function findIndexByKeyValue(obj, key, value) {
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key] == value) {
            return i;
        }
    }
    return -1;
}
//fix range in inputbox
function limitNumberInRang(input) {
    if (parseInt(input.value) < parseInt(MINNUMFORRANGE)) {
        input.value = MINNUMFORRANGE;
    }
    if (parseInt(input.value) > parseInt(MAXNUMFORRANGE)) {
        input.value = MAXNUMFORRANGE;
    }
}

//pdf save file data  Created by Mishil Patel - 16-6-2014
function pdfSave(pdfdata, fieldwidth, filename, pedding) {
    pedding = pedding || 0;
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    var index = 0;
    var doc = new jsPDF('p', 'pt', 'a4', true);
    doc.setFontSize(10)
    //.splitTextToSize('loremipsum', 7.5)
    doc.cellInitialize();
    $.each(pdfdata, function (i, row) {
        //console.debug(row);
        index = 0;
        $.each(row, function (j, cell) {
            //console.log(cell);
            console.log("i =" + j);
            //doc.margins =10;
            doc.cell(pedding, 2, fieldwidth[index], 20, cell, i); // 2nd parameter=top margin,1st=left margin 3rd=row cell width 4th=Row height
            index++;
        })
    });


    doc.save(filename);
}
//end pdf save file data

//convert json data to csv string  Created by Mishil Patel - 16-6-2014
function JSON2CSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

    var str = '';
    var line = '';

    if ($("#labels").is(':checked')) {
        var head = array[0];
        if ($("#quote").is(':checked')) {
            for (var index in array[0]) {
                var value = index + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {
            for (var index in array[0]) {
                line += index + ',';
            }
        }

        line = line.slice(0, -1);
        str += line + '\r\n';
    }

    for (var i = 0; i < array.length; i++) {
        var line = '';

        if ($("#quote").is(':checked')) {
            for (var index in array[i]) {
                var value = array[i][index] + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {
            for (var index in array[i]) {
                line += array[i][index] + ',';
            }
        }

        line = line.slice(0, -1);
        str += line + '\r\n';
    }
    return str;

}
//convert json data to csv string

//csv save file data  Created by Mishil Patel - 16-6-2014
function csvSave(csvdata, filename) {
    var csv = JSON2CSV(csvdata);
    window.URL = window.URL || window.webkiURL;
    var blob = new Blob([csv]);
    var blobURL = window.URL.createObjectURL(blob);
    //to use this function you need to add this html in your page in footer. so it will append download link in the div and click it
    //<div id="downloadLink"></div>
    $("#downloadLink").html("");
    $("<a id='mishil'></a>").
            attr("href", blobURL).
            attr("download", filename).
            text("Download Data").
            appendTo('#downloadLink');
    //alert("hii");
    //setTimeout(function(){
    $('#mishil')[0].click();
    //},10);
}
//csv save file data

//print report data  Created by Mishil Patel - 16-6-2014
function printReport() {

    var display_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    display_setting += "scrollbars=yes,width=280px, height=400px, left=100, top=25";
    var document_print = window.open("printreport.html", "Print", display_setting);
    var timer = setInterval(checkChild, 500);

    function checkChild() {
        if (document_print.closed) {
            clearInterval(timer);
            navigateToDashboard();
        }
    }
}

//give time stamp of php of give date from 00:00 to 23:59 hrs. you need to split using , Created by Vrushank Rindani - 21-6-2014
function startToEndTimestampOfGivanDate(date1, date2) {
    var tmp_sdate = ddmmyyToMysql(date1);
    var tmp_edate = ddmmyyToMysql(date2);
    var sdate = parseInt((new Date(tmp_sdate + " 00:00").getTime()) / 1000);
    var edate = parseInt((new Date(tmp_edate + " 23:59").getTime()) / 1000);

    return sdate + "," + edate;
}

// date formater Vrushank 28-04-2014
//Eg. new Date().format("MM/dd/yy");
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(), //day
        "h+": this.getHours(), //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds() //millisecond
    }

    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
                    RegExp.$1.length == 1 ? o[k] :
                    ("00" + o[k]).substr(("" + o[k]).length));
    return format;
}

/* This function is convert timestamp to date in dd-mm-yyyy format START*/
function getDateFromPhpTimeStamp(timestamp) {
    return new Date(parseInt(timestamp) * 1000).format(localStorage.indeCampusDateFormat);
}
/* This function is convert timestamp to date in dd-mm-yyyy format END*/



//give time stamp of php of give date from 00:00 hrs. Created by Vrushank Rindani - 21-6-2014
function dateTOPhpTimeStamp(date) {
    return parseInt((new Date(date + " 00:00").getTime()) / 1000);
}

/*this function gives date in desired format START*/
function mysqltoDesiredFormat(date, format) {
    if (format == undefined)
        format = localStorage.indeCampusDateFormat;
    return new Date(date).format(format);
}
/*this function gives date in desired format END*/


/*this function gives date in desired format START*/
function ddmmyyToMysql(date, format) {
    date = date.split("-").reverse().join("-");
    if (format == undefined)
        format = "yyyy-MM-dd";
    return new Date(date).format(format);

}
/*this function gives date in desired format END*/



/*SQL demp Insert Support START*/
function insertSQLDump(insertValue) {
    var tmp_start_value = insertValue.split("VALUES");
    var tmp_value = tmp_start_value[1].split("),");

    var query = "";
    for (var i = 0; i < tmp_value.length; i++) {
        if (i == tmp_value.length - 1) {
            query += tmp_start_value[0] + " VALUES " + tmp_value[i];
        } else {
            query += tmp_start_value[0] + " VALUES " + tmp_value[i] + ")," + "|";
        }
    }

    var q = query.split(",|");


    ePOSDB.transaction(function (tx) {
        for (var a = 0; a < q.length; a++) {
            log(q[a] + " ====== " + a);
            tx.executeSql(q[a], [], function (tx, result) {
            }, dbErrorHandler);
        }
    }, dbErrorHandler);


}

/*SQL demp Insert Support END*/


/* + - * / on array start*/
function aggregationOnArray(arr, field, valType) {
    log(arr);
    //alert(arr[0][field]);
    // valType == 1 INT
    // valType == 0 FLOAT
    if (valType == undefined)
        valType = 1;
    //valType = valType || 1;

    var tmp_ans = 0;
    for (var i = 0; i < arr.length; i++) {
        if (valType == 1) {
            tmp_ans += parseInt(arr[i][field]);
        } else {
            tmp_ans += parseFloat(arr[i][field]);
        }
    }

    return tmp_ans;
}


/* + - * / on array end*/

//replace all occurrence from given string
function replaceAll(str, search, replace) {

    var reg = new RegExp(search, 'g');
    return str.replace(reg, replace);

}

//calculate difference between to date in days
function findDateDifferenceInDays(sdate, edate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(sdate);
    var secondDate = new Date(edate);

    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
}

//fix range in inputbox by Given Value
function limitNumberInRangByValue(input, maxval) {
    //var MAX = $("#"+id).val();
    log(input.value);
    if (parseInt(input.value) > parseInt(maxval)) {
        input.value = parseInt(maxval);
    } else if (input.value == " " || input.value == "") {
        input.value = parseInt(0);
    } else {
        input.value = parseInt(input.value);
    }
}

/*this function reload page created by,vrushank START*/
function reloadWin() {
    window.location.reload();
}
/*this function reload page created by,vrushank END*/


/*give 12hr time from given date 12:30 PM */
function formatAMPM(date) {
    date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
/*give 12hr time from given date 12:30 PM */

function waitWhileAndExecuite(callback) {
    setTimeout(function () {
        callback();
    }, 500);
}


function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else {
            return true;
        }
        if ((charCode > 64 && charCode < 91) || charCode == 8 || charCode == 46)
            return true;
        else
            return false;
    } catch (err) {
        toastr.warning(err.Description);
    }
}

function onlyNos(e, t) { //start function for number validation for number text box

    try {
        if (window.event) {
            var charCode = window.event.keyCode;
            var event = window.event;
        }
        else if (e) {
            var charCode = e.which;
            var event = e;
        }
        else {
            return true;
        }
        //console.log(charCode);
        //console.log(event);
        if (charCode > 31 && ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105))) {
            return false;
        }
        if (event.shiftKey)
        {
            return false;
        }

        return true;
    }
    catch (err) {
        toastr.warning(err.Description);
    }

} //end function for number validation for number text box

/*char count limit*/
(function ($) {
    $.fn.extend({
        limiter: function (limit) {
            $(this).on("keyup focus blur", function () {
                setCount(this);
            });

            function setCount(src) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    toastr.warning("MAX Limit Reached...!!!");
                }
            }
            setCount($(this)[0]);
        }
    });
})(jQuery);


//START FUNCTION FOR CHANGE DATE FORMATE dd-mm-yyyy to yyyy-mm-dd by VAIBHAV PATE ON 27-4-2015
function convertDateFormatyyyymmdd(date) {
    var newdate = date.split("-").reverse().join("-");
    return newdate;
}
//END FUNCTION FOR CHANGE DATE FORMATE dd-mm-yyyy to yyyy-mm-dd by VAIBHAV PATE ON 27-4-2015

/* function to generate <img> tag's src of  for item/group START */
function genImgSrcFromImgString(imgStr, imgType) {

    if (!imgStr || !imgType || imgStr == "undefined" || imgType == "undefined" || imgStr == "NULL" || imgType == "NULL") {
        return false;
    }


    var finalString = "data:" + imgType + ";base64," + imgStr;
    return finalString;
}
/* function to generate <img> tag's src for item/group END */


function commonAjaxActiivity(url, data, callback, msg) {

    data.clientId = localStorage.indeCampusActivityAppClientId;
    data.userId = localStorage.indeCampusActivityAppUserId;
    console.log(data);
    // changeAjaxMsg(msg);
    //  var progress = $(".loading-progress").progressTimer({
    //     timeLimit: TIMEOUT
    // });
    $("#ajaxloader").removeClass("hideajaxLoader");
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            postData: data
        },
        dataType: 'json',
        headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
        crossDomain: true,
        // beforeSend: function() {
        //     //alert(bfsendMsg);
        //     $("#msgDiv").html(msg);
        //     $("#ajaxloader").removeClass("hideajaxLoader");
        //     var date = new Date();
        //     localStorage.POTGcurrentTime = date.getTime();
        // },
        success: function (data, textStatus, jqXHR) {
            // progress.progressTimer('complete', {
            //     successText: msg,
            //     onFinish: function () {
            //         var successText = msg;
            //         //console.log(successText);
            //         var glyph = $('<span></span>').addClass('glyphicon glyphicon-ok');
            //         $(".loading-progress").append($('<p style="margin-top:10px"></p>').append(glyph).append(' ' + successText + " - " + "Success"));
            //         $("#msgDiv").html('');
            //         setTimeout(function(){
                        console.log(data);
                        $("#ajaxloader").addClass("hideajaxLoader");
                        callback(true, data);
            //         },500);                    
            //    }
            // });
        },
        error: function (jqXHR, errdata, errorThrown) {
            // console.log("error");
            // progress.progressTimer('error', {
            //     errorText: 'Please try again... Cannot connect to api',
            //     onFinish: function () {
                        console.log("error");
                        console.log(jqXHR);
                        $("#ajaxloader").addClass("hideajaxLoader");
                        callback(false, errdata, errorThrown);
                //     }
                // });
        }
    });


}

/* A new lib function to open the dropdown manually added by sachin jani 24/09/2015 START */
function openSelect(elem) {
    if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
    } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
    }
}

/*lib function to open the dropdown manually END */

/* function to disable negatives and zero in input START */
function disableNegativeZero(elem)
{
    var value = $(elem).val();
    if (value <= 0)
    {
        $(elem).val('');
    }

}
/* function to disable negatives and zero in input END */

//this will chk email id format
function validateEmail(email) {

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



/*keyCodeShortcut function added by Sachin Jani 18/07/2015 */

//START FUNCTION FOR ACTIVATING KEY CODE SHORT CUT FOR HEADER AND SIDEBAR


//START FUNCTION FOR ACTIVATING KEY CODE SHORT CUT FOR HEADER AND SIDEBAR


function keyCodeShortcut() {

    //$("#").is(':focus'); // GET FOCUSSED ELEMENT TRUE/FALSE
    localStorage.POTGsideMenu = false;
    localStorage.POTGalterB = false;
    var tablelipagecountnext = 33; //no of tables per page.
    var btmMenuStepCnt = 0; // text screen bottom menu scroll count

    window.addEventListener('keydown', function (event) {
        
        /* disable all keycodes START */
        if(DISABLEKEYCODESHORTCUTS){
            return false;
        }
        /* disable all keycodes END */

        var location = window.location.href;
        if (location.indexOf("dashboard.html") > -1) { // IF DASHBOARD.HTML PAGE
            if ($("#homeDeliveryCustLoginPopUp").is(":visible") == true) { // IF CUSTOMER POP UP IS OPENED

                var new1 = $("#homepopupKey").find(".radioButton").find('input#home_radio_new').attr('checked'); // NEW RADIO CHECKED
                var existing = $("#homepopupKey").find(".radioButton").find('input#home_radio_existion').attr('checked'); // EXISTING RADIO CHECKED




                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED

                    if (new1 == 'checked') {
                        $("#homepopupKey").find(".radioButton").find('input#home_radio_existion').click();


                        $("#home_cust_name").focus();

                    } else if (existing == 'checked') {

                        $("#homepopupKey").find(".radioButton").find('input#home_radio_new').click();

                        $("#home_cust_fname").focus();

                    }



                } else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (new1 == 'checked') {
                        $("#homepopupKey").find(".radioButton").find('input#home_radio_existion').click();


                        $("#home_cust_name").focus();

                    } else if (existing == 'checked') {

                        $("#homepopupKey").find(".radioButton").find('input#home_radio_new').click();

                        $("#home_cust_fname").focus();

                    }

                } else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (new1 == 'checked') {
                        // $("#cust_fname").focus(); //Orignal

                        //Code added by sachin jani

                        if ($("#home_cust_fname").is(':focus')) {
                            $("#home_cust_lname").focus();
                        } else if ($("#home_cust_lname").is(':focus')) {
                            $("#home_cust_phone").focus();
                        } else if ($("#home_cust_phone").is(':focus')) {
                            $("#home_cust_houseno").focus();
                        } else if ($("#home_cust_houseno").is(':focus')) {
                            $("#home_cust_streetno").focus();

                        } else if ($("#home_cust_streetno").is(':focus')) {
                            $("#home_cust_area").focus();

                        } else if ($("#home_cust_area").is(':focus')) {
                            $("#home_cust_captionassigned").focus();

                        } else if ($("#home_cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#homeDelivery").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#homeDelivery").find(".closeRefreshIcons ").find('a').eq(0).click();
                        }


                        // Code adding over


                    } else if (existing == 'checked') {



                        if ($("#home_cust_name").is(':focus')) {
                            $("#home_cust_phone").focus();
                        } else if ($("#home_cust_phone").is(':focus')) {
                            $("#home_cust_houseno").focus();
                        } else if ($("#home_cust_houseno").is(':focus')) {
                            $("#home_cust_streetno").focus();

                        } else if ($("#home_cust_streetno").is(':focus')) {
                            $("#home_cust_area").focus();

                        } else if ($("#home_cust_area").is(':focus')) {
                            $("#home_cust_captionassigned").focus();

                        } else if ($("#home_cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#homeDelivery").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#homeDelivery").find(".closeRefreshIcons ").find('a').eq(0).click();
                        }



                    }
                } else if (event.keyCode == 27) {
                    $("#homeDelivery").find('.iconClose').click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#homeDelivery").find(".closeRefreshIcons ").find('a').eq(1).click();
                }

            }
            else if ($("#kioskFastmodecoustmerLoginPopUp").is(":visible") == true) { // IF CUSTOMER POP UP IS OPENED

                var guest = $("#custpopupKey").find(".radioButton").find('input#cust_radio_guest').attr('checked'); //GUEST CHECKDE
                var new1 = $("#custpopupKey").find(".radioButton").find('input#cust_radio_new').attr('checked'); // NEW RADIO CHECKED
                var existing = $("#custpopupKey").find(".radioButton").find('input#cust_radio_existing').attr('checked'); // EXISTING RADIO CHECKED



                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED

                    if (guest == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_new').click();


                        $("#cust_fname").focus();



                    } else if (new1 == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_existing').click();


                        $("#cust_name").focus();

                    } else if (existing == 'checked') {

                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_guest').click();

                        $("#cust_numofguest").focus();

                    }



                } else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (guest == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_existing').click();

                        $("#cust_name").focus();


                    } else if (new1 == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_guest').click();

                        $("#cust_numofguest").focus();

                    } else if (existing == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_new').click();

                        $("#cust_fname").focus();


                    }

                } else if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    if (guest == 'checked') {
                        if ($("#cust_numofguest").is(':focus')) {
                            //$("#custpopupKey").find("li").eq(5).focus();
                            $("#cust_captionassigned").focus();


                            //$(".jquery-selectbox-moreButton").trigger("click");
                            //$(".jquery-selectbox-list").find('span').eq(1).addClass('listelementhover');
                            //$(".listelementhover").focus();
                        } else if ($("#cust_captionassigned").is(':focus')) {
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();

                            $("#ordersource").focus();
                            //$("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();

                            $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else {
                            //$("#cust_numofguest").focus();
                        }

                    } else if (new1 == 'checked') {
                        // $("#cust_fname").focus(); //Orignal

                        //Code added by sachin jani

                        if ($("#cust_fname").is(':focus')) {
                            $("#cust_lname").focus();
                        } else if ($("#cust_lname").is(':focus')) {
                            $("#cust_phone").focus();
                        } else if ($("#cust_phone").is(':focus')) {
                            $("#cust_numofguest").focus();
                        } else if ($("#cust_numofguest").is(':focus')) {
                            $("#cust_captionassigned").focus();

                        } else if ($("#cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else {
                            //$("#cust_fname").focus();
                        }

                        // Code adding over


                    } else if (existing == 'checked') {
                        // $("#cust_name").focus(); //Orignal

                        //Code added by sachin jani
                        if ($("#cust_name").is(':focus')) {
                            $("#cust_numofguest").focus();
                        } else if ($("#cust_numofguest").is(':focus')) {
                            $("#cust_captionassigned").focus();
                        } else if ($("#cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else {
                            //$("#cust_name").focus();
                        }

                        // Code adding over
                    }
                } else if (event.keyCode == 27) {
                    $("#coustmerLogin").find('.iconClose').click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(1).click();
                }

            }
            else {

                if (event.altKey && event.keyCode == 90) { // ALT + Z = open dinein
                    $("#dineinlink").click();
                } else if (event.altKey && event.keyCode == 88) { // ALT + X = open Fast mode
                    $("#fmlink").click();
                } else if (event.altKey && event.keyCode == 67) { // ALT + C = open Home Delivery
                    $("#hdlink").click();
                }
                else if (event.altKey && event.keyCode == 84) { // ALT + T = open Text Screen
                    $("#txtscrnlink").click();
                }
            }
        }
        else if (location.indexOf("table.html") > -1) { // IF TABLE.HTML PAGE
            localStorage.POTGopenTblMenu = true;

            if ($(".custmerLogin").is(":visible") == true) { // IF CUSTOMER POP UP IS OPENED

                var guest = $("#custpopupKey").find(".radioButton").find('input#cust_radio_guest').attr('checked'); //GUEST CHECKDE
                var new1 = $("#custpopupKey").find(".radioButton").find('input#cust_radio_new').attr('checked'); // NEW RADIO CHECKED
                var existing = $("#custpopupKey").find(".radioButton").find('input#cust_radio_existing').attr('checked'); // EXISTING RADIO CHECKED



                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED

                    if (guest == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_new').click();


                        $("#cust_fname").focus();



                    } else if (new1 == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_existing').click();


                        $("#cust_name").focus();

                    } else if (existing == 'checked') {

                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_guest').click();

                        $("#cust_numofguest").focus();

                    }



                } else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (guest == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_existing').click();

                        $("#cust_name").focus();


                    } else if (new1 == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_guest').click();

                        $("#cust_numofguest").focus();

                    } else if (existing == 'checked') {
                        $("#custpopupKey").find(".radioButton").find('input#cust_radio_new').click();

                        $("#cust_fname").focus();


                    }

                } else if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    if (guest == 'checked') {
                        if ($("#cust_numofguest").is(':focus')) {
                            //$("#custpopupKey").find("li").eq(5).focus();
                            
                            $("#cust_captionassigned").focus();


                            //$(".jquery-selectbox-moreButton").trigger("click");
                            //$(".jquery-selectbox-list").find('span').eq(1).addClass('listelementhover');
                            //$(".listelementhover").focus();
                        } else if ($("#cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$("#ordersoure").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else {
                            //$("#cust_numofguest").focus();
                        }

                    } else if (new1 == 'checked') {
                        // $("#cust_fname").focus(); //Orignal

                        //Code added by sachin jani

                        if ($("#cust_fname").is(':focus')) {
                            $("#cust_lname").focus();
                        } else if ($("#cust_lname").is(':focus')) {
                            $("#cust_phone").focus();
                        } else if ($("#cust_phone").is(':focus')) {
                            $("#cust_numofguest").focus();
                        } else if ($("#cust_numofguest").is(':focus')) {
                            $("#cust_captionassigned").focus();

                        } else if ($("#cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$("#ordersoure").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else {
                            //$("#cust_fname").focus();
                        }

                        // Code adding over


                    } else if (existing == 'checked') {
                        // $("#cust_name").focus(); //Orignal

                        //Code added by sachin jani
                        if ($("#cust_name").is(':focus')) {
                            $("#cust_numofguest").focus();
                        } else if ($("#cust_numofguest").is(':focus')) {
                            $("#cust_captionassigned").focus();
                        } else if ($("#cust_captionassigned").is(':focus')) {
                            $("#ordersource").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            //$("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else if ($("#ordersource").is(':focus')) {
                            //$("#ordersoure").focus();
                            //$(".custmerLogin").find(".closeRefreshIcons ").find('.iconAdd').click();
                            $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(0).click();
                        } else {
                            //$("#cust_name").focus();
                        }

                        // Code adding over
                    }
                } else if (event.keyCode == 27) {
                    $("#coustmerLogin").find('.iconClose').click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#coustmerLogin").find(".closeRefreshIcons ").find('a').eq(1).click();
                }

            }

            /* follwing code is added by sachin jani 16/07/2015 */
            else if ($('#printBill').is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $('#printBill').find('.ez-radio').find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $('#printBill').find('.radioButton').find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $('#printBill').find('.radioButton').find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                /* happy hours keycode added on 03/11/2015*/
                var hpyhrs = $('#printBill').find('#chkHappyHours').attr('checked');
                var coupn = $('#printBill #discountypeCoupen').attr('checked');
                var hpyhrsIsVisible = $('#printBill').find('#chkHappyHours').is(':visible');

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $('#printBill').find('.radioButton').find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (Per == 'checked') {
                        $('#printBill').find('.radioButton').find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    }
                    else if (Amt == 'checked') {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                            $("#billremark").focus();

                        }

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                    }
                    else {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();

                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {

                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill #discountypeCoupen').click();
                            $("#discounttypevalue").focus();

                        }

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }
                    else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }
                }
                else if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#billremark").focus();
                        }




                    }
                    else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            $("#billremark").focus();
                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    }
                    else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            $("#billremark").focus();
                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else if (coupn == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            $("#billremark").focus();
                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else {
                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15
                    }

                }
                else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();
                }


                /* Code added by Sachin jani Over */






            }
            else if ($("#permissionPassword").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPassword").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPassword").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if ($("#createingredient").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    //searchTable
                    if ($("#searchTable").is(':focus')) {

                        /*
                         $("#searchTable").data("ui-autocomplete")._trigger("change");
                         mergeTablesMenuList
                         if($("#mergeTablesMenuList").is(':visible') == true)
                         {
                         $('.addPrice').click();
                         }
                         */


                        $("#searchTable").blur();
                        if ($("#searchTable").val()) {
                            $('.addPrice').click();
                            $("#searchTable").focus();
                        } else {

                            //$("#createingredient").find(".closeRefreshIcons ").find('a').eq(0).click();
                        }




                    } else {
                        $("#searchTable").focus();
                    }



                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#createingredient").find(".closeRefreshIcons ").find('a').eq(1).click();
                }
                else if (event.altKey && event.keyCode == 83) { //ALTR + S
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#createingredient").find(".closeRefreshIcons ").find('a').eq(0).click();
                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#createingredient").find(".closeRefreshIcons").find('a').eq(2).click();
                    closeStaticPopup("createingredient");

                }






            }
            else if ($("#transferTable").is(':visible') == true) {

                //activeShiftTable
                if (event.keyCode == 39) {
                    //event.preventDefault();

                    var activeli = $('.activeShiftTable');
                    var activeliNext = $('.activeShiftTable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $(activeliNext).addClass('activeShiftTable');
                    } else {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $('#tableList li').eq(0).addClass('activeShiftTable');
                    }


                }
                else if (event.keyCode == 37) {
                    //event.preventDefault();
                    var activeli = $('.activeShiftTable');
                    var activeliPrev = $('.activeShiftTable').prev('li');
                    var liLen = $('#tableList li').length;

                    if (activeliPrev.length > 0) {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $(activeliPrev).addClass('activeShiftTable');
                    } else {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $('#tableList li').eq(liLen - 1).addClass('activeShiftTable');
                    }


                }
                else if (event.keyCode == 13) {
                    $('.activeShiftTable a').click();
                }
                else if (event.keyCode == 27) {
                    $('.activeShiftTable').removeClass('activeShiftTable');
                    $("#transferTable").find('.iconClose').click();

                }






            }
            else if ($("#splitTable").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    $("#splitTable").find(".closeRefreshIcons ").find('a').eq(0).click();

                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#splitTable").find(".closeRefreshIcons ").find('a').eq(1).click();
                } else if (event.keyCode == 27) { //ESC keypress
                    //$("#splitTable").find(".closeRefreshIcons ").find('a').eq(2).click();
                    $("#splitTable").find(".closeRefreshIcons ").find('.iconClose').click();

                }






            }

            /* code added by sachin jani over  */
            // IF POP UP IS CLOSED
            else {
                if (event.keyCode == 39) { //IF KEY NEXT ARROW WITH ALTR + M ACTIVATED     
                    if (event.shiftKey && event.keyCode == 39) {
                        event.preventDefault();
                        $('.rightArrow a').click();

                        var nextpageli = $('ul#tblList li.openTooltip').eq(tablelipagecountnext);
                        if (nextpageli.length > 0)
                        {
                            $('.activetable').removeClass('activetable');
                            $(nextpageli).addClass('activetable');
                            tablelipagecountnext += 33;

                        }
                    }
                    else
                    {
                        var next = $('.activetable').next('li');
                        var currpos = $('.activetable').attr("data-litablecount");
                        var newpos = parseInt(currpos) + 1;
//var prevlicount = $('.activetable').prev('li').attr("data-litablecount");
                        var nextlicount = $("ul#tblList li.openTooltip[data-litablecount='" + newpos + "']").attr("data-litablecount");
                        tergetTitle = $('.activetable').attr('title');
                        if (tergetTitle == 'yes') {
                            $('.activetable').click();
                            localStorage.POTGopenTblMenu = false;
                        }

                        if (nextlicount)
                        {
                            if (parseInt(nextlicount) >= tablelipagecountnext)
                            {
                                $('.activetable').removeClass('activetable');
                                var lstcnt = parseInt(tablelipagecountnext) - 33;
                                var lastpageli = $("ul#tblList li.openTooltip[data-litablecount='" + lstcnt + "']");
                                if (lastpageli.length > 0)
                                {

                                    $(lastpageli).addClass('activetable');

                                }
                                else
                                {
                                    //$('ul#tblList li.openTooltip:last').addClass('activetable');
                                }

                            }
                            else
                            {
                                $('.activetable').removeClass('activetable');
                                next.addClass('activetable');

                            }
                        }
                        else
                        {

                            $('.activetable').removeClass('activetable');
                            $('ul#tblList li.openTooltip').eq(tablelipagecountnext - 33).addClass('activetable');



                        }







                        //$('.activetable').removeClass('activetable');
                        //next = next.length > 0 ? next : $('.focus li:eq(0)');
                        //next = next.length > 0 ? next : $('ul#tblList li.openTooltip:first');
                        //next.addClass('activetable');
                        //alert(next.length);
                    }
                }
                else if (event.keyCode == 37) { //IF PREV LEFT  ARROW WITH ALTR + M ACTIVATED
                    if (event.shiftKey && event.keyCode == 37) {
                        event.preventDefault();
                        $('.leftArrow a').click();
                        //var tablelipagecountprev = $('ul#tblList li.openTooltip').length;
                        //var prev = $('.activetable').prev('li');
                        //console.log("tablelipagecountprev length :" + tablelipagecountprev);
                        /*
                         setTimeout(function () {
                         
                         }, 100);
                         */
                        //var prevpageli = $('ul#tblList li.openTooltip').eq(tablelipagecountnext -33);
                        if (tablelipagecountnext - 33 >= 0)
                        {
                            tablelipagecountnext -= 33;
                            $('.activetable').removeClass('activetable');
                            $('ul#tblList li.openTooltip').eq(tablelipagecountnext - 33).addClass('activetable');



                        }


                    }
                    else
                    {

                        var prev = $('.activetable').prev('li');
                        var currpos = $('.activetable').attr("data-litablecount");
                        var newpos = parseInt(currpos) - 1;
//var prevlicount = $('.activetable').prev('li').attr("data-litablecount");
                        var prevlicount = $("ul#tblList li.openTooltip[data-litablecount='" + newpos + "']").attr("data-litablecount");
                        tergetTitle = $('.activetable').attr('title');
                        if (tergetTitle == 'yes') {
                            $('.activetable').click();
                            localStorage.POTGopenTblMenu = false;
                        }


                        //prev = prev.length > 0 ? prev : $('.focus li').last();
                        //prev = prev.length > 0 ? prev : $('ul#tblList li.openTooltip:last');
                        //var liLen = $('ul#tblList li.openTooltip');
                        //prev.addClass('activetable');
                        if (prevlicount)
                        {
                            if (parseInt(prevlicount) < tablelipagecountnext - 33)
                            {
                                $('.activetable').removeClass('activetable');
                                var lstcnt = tablelipagecountnext - 1;
                                var lastpageli = $("ul#tblList li.openTooltip[data-litablecount='" + lstcnt + "']");
                                if (lastpageli.length > 0)
                                {

                                    $(lastpageli).addClass('activetable');

                                }
                                else
                                {
                                    $('ul#tblList li.openTooltip:last').addClass('activetable');
                                }

                            }
                            else
                            {
                                $('.activetable').removeClass('activetable');
                                prev.addClass('activetable');

                            }
                        }
                        else
                        {
                            if (prevlicount == "0")
                            {
                                $('.activetable').removeClass('activetable');
                                prev.addClass('activetable');

                            }
                            else
                            {
                                $('.activetable').removeClass('activetable');
//$('ul#tblList li.openTooltip').eq(tablelipagecountnext - 1).addClass('activetable');
                                var lstcnt = tablelipagecountnext - 1;
                                var lastpageli = $("ul#tblList li.openTooltip[data-litablecount='" + lstcnt + "']");
                                if (lastpageli.length > 0)
                                {

                                    $(lastpageli).addClass('activetable');

                                }
                                else
                                {
                                    $('ul#tblList li.openTooltip:last').addClass('activetable');
                                }

                            }



                        }


                    }

                }
                else if (event.keyCode == 13 && localStorage.POTGopenTblMenu == 'true') { //IF ENTER KEY PRESS WITH TBL MENU ACTIVATED
                    localStorage.POTGopenTblMenu = false;
                    //$('.activetable').click(); orignal
                    $('.activetable a').eq(0).click(); //added by sachin jani 08/09/2015
                }
                else if (event.keyCode == 13 && localStorage.POTGopenTblMenu == 'false') { //IF ENTER KEY PRESS WITH TBL MENU NOT ACTIVATED
                    localStorage.POTGopenTblMenu = true;
                } else if (event.altKey && event.keyCode == 67) { //IF ALTER + C  PRESSED

                    tergetTitle1 = $('.activetable').attr('title');

                    if (tergetTitle1 == 'yes') {

                        $("li.activetable a").eq(1).click();
                    }
                }
                else if (event.altKey && event.keyCode == 69) { //IF ALTER + E  PRESSED
                    event.preventDefault();
                    tergetTitle2 = $('.activetable').attr('title');
                    if (tergetTitle2 == 'yes') {
                        $("li.activetable a").eq(2).click();
                    }
                } else if (event.altKey && event.keyCode == 80) { //IF ALTER + P  PRESSED
                    tergetTitle3 = $('.activetable').attr('title');
                    if (tergetTitle3 == 'yes') {
                        console.log("print button click");
                        $("li.activetable a").eq(3).click();
                    }
                } else if (event.altKey && event.keyCode == 88) { //IF ALTER + X  PRESSED
                    tergetTitle4 = $('.activetable').attr('title');
                    if (tergetTitle4 == 'yes') {
                        $("li.activetable a").eq(6).click();
                    }
                } else if (event.altKey && event.keyCode == 73) //I 
                {
                    //reason: alt+s is for shift close
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(3).click();

                    }

                } else if (event.altKey && event.keyCode == 84) //T
                {
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(4).click();

                    }

                } else if (event.shiftKey && event.keyCode == 82) { //R	
                    $('#removeFilterMenuIcon').click();
                } else if (event.shiftKey && event.keyCode == 79) { //O	
                    $('#filterOccupiedMenuIcon').click();
                } else if (event.shiftKey && event.keyCode == 66) { //B	
                    $('#filterBillingMenuIcon').click();
                } else if (event.shiftKey && event.keyCode == 86) { //V	
                    $('#filterVacantMenuIcon').click();
                } else if (event.shiftKey && event.keyCode == 77) { //M	
                    $('#mergeTableMenuIcon').click();
                    setTimeout(function () {
                        resetTableMergePopup();
                    }, 100);

                }





            }


        }
        else if (location.indexOf("additem.html") > -1) { // IF additem.html PAGE
            //alert('dsfsdf');

            if ($("#addItem").is(":visible") == true) {

                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $(".iconClose").click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    //$(".iconRefresh").parent().click();
                    $("#addItem .iconRefresh").click();

                }
                else if (event.keyCode == 13) { // ENTER KEY PRESSES		
                    if ($("#qtyvalue").is(':focus')) {
                        $("#remarkinpopup").focus();
                    } else if ($("#remarkinpopup").is(':focus')) {
                        $("#closeadd").click();

                    } else {
                        $("#qtyvalue").focus();
                        $("#qtyvalue").select(); //added by sachin jani 29/07/2015
                    }
                }
                else if (event.keyCode == 32) { //Space key
                    if ($("#qtyvalue").is(':focus')) {
                        event.preventDefault(); //disable space on quantity textbox
                    }

                }
                /* else if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) || event.keyCode == 32) {
                 // DECLINE ATOZ ,0 TO 9 AND SPACE BAR
                 event.preventDefault(); // disable search item box.
                 } */
                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }


            }
            else if ($("#editItem").is(":visible") == true) {

                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $(".iconClose").click();
                } else if (event.keyCode == 13) { // ENTER KEY PRESSES		
                    if ($("#qtyvalue").is(':focus')) {
                        $("#remarkinpopup").focus();
                    } else if ($("#remarkinpopup").is(':focus')) {
                        //$("#EditItemAdd").click();
                        //$("#editItem").find(".closeRefreshIcons ").find('a').eq(0).click();
                        $("#editItem").find(".closeRefreshIcons ").find(".iconAdd").click(); //this element has onClick function not its parent anchor tag. 
                    } else {
                        $("#qtyvalue").focus();
                        $("#qtyvalue").select(); //added by sachin jani 29/07/2015
                    }
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D

                    event.preventDefault();
                    //$("#editItem").find(".closeRefreshIcons ").find(".iconRefresh").parent().click();
                    $("#editItem .iconRefresh").click();
                } else if (event.keyCode == 32) { //Space key
                    if ($("#qtyvalue").is(':focus')) {
                        event.preventDefault(); //disable space on quantity textbox
                    }

                }
                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }

            }
            /*	Code added by sachin jani 15/07/2015 */
            else if ($("#printBill").is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $("#printBill").find(".ez-radio").find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $("#printBill").find(".radioButton").find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $("#printBill").find(".radioButton").find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                /* happy hours keycode added on 03/11/2015*/
                var hpyhrs = $('#printBill').find('#chkHappyHours').attr('checked');
                var coupn = $('#printBill #discountypeCoupen').attr('checked');
                var hpyhrsIsVisible = $('#printBill').find('#chkHappyHours').is(':visible');


                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    }
                    else if (Amt == 'checked') {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                            $("#billremark").focus();

                        }

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                    }

                    else {
                        /* orignal START */

                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                        /*orignal END */
                        /*
                         if(('#inwardWastageInPrintBill').is(':visible'))
                         {
                         $("#printBill").find("input#inwardBillPB").click();
                         }
                         else
                         {
                         $("#printBill").find("input#discounttypeNC").click();
                         $("#billremark").focus();
                         
                         }
                         */


                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {

                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill #discountypeCoupen').click();
                            $("#discounttypevalue").focus();

                        }

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }
                    else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }
                }
                else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {


                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();

                        } else {
                            $("#billremark").focus();
                        }




                    }
                    else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    }
                    else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else if (coupn == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else {
                        /* Orignal START */

                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15

                        /* Orignal END */

                        /*
                         if($('#inwardWastageInPrintBill').is(':visible'))
                         {
                         //$("#printBill").find("input#inwardBillPB").click();
                         }
                         else
                         {
                         $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill.
                         } */


                    }
                }
                else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();

                }
                else if (event.altKey && event.keyCode == 69) //E
                {
                    event.preventDefault();
                    if ($("#packingCharges").is(':focus')) {
                        if ($('#delC').is(':visible') == true) {

                            $("#deliveryCharges").focus();
                        } else {
                            $("#packingCharges").focus();
                        }

                    }
                    else if ($("#deliveryCharges").is(':focus')) {
                        $("#packingCharges").focus();

                    }
                    else
                    {
                        $("#packingCharges").focus();

                    }


                }
                else if (event.altKey && event.keyCode == 73) //I
                {
                    event.preventDefault();
                    var inwardPB = $("#printBill").find("input#inwardBillPB");
                    var wastagePB = $("#printBill").find("input#wastageBillPB");
                    var inwardPBChk = $(inwardPB).attr('checked');
                    var wastagePBChk = $(wastagePB).attr('checked');

                    if ($('#inwardWastageInPrintBill').is(':visible'))
                    {
                        if (inwardPBChk)
                        {
                            $(wastagePB).click();
                        }
                        else if (wastagePBChk)
                        {
                            $(inwardPB).click();
                        }
                        else
                        {
                            $(inwardPB).click();
                        }




                    }


                }

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }

                /* Code added by Sachin jani Over */


            }
            else if ($("#popupBoxDelete").is(":visible") == true) {
                var vod = $("#popupBoxDelete").find(".radioButton").find('input#deletevoid').attr('checked'); //Void CHECKDE
                var cancel = $("#popupBoxDelete").find(".radioButton").find('input#deletecan').attr('checked'); //Cancel CHECKED
                var del = $("#popupBoxDelete").find(".radioButton").find('input#deletedel').attr('checked'); // Delete RADIO CHECKED
                if (event.keyCode == 13) // ENTER KEY PRESSES
                {


                    if (vod == 'checked') {

                    } else if (cancel == 'checked') {
                        if ($("#kot_voidnc").is(':focus')) {
                            //$("#closeDelete").click(); //orignal
                            $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click(); //modified 29/07/2015
                            closeStaticPopup("popupBoxDelete");
                        } else {
                            $("#kot_voidnc").focus();
                        }




                    } else if (del == 'checked') {
                        if ($("#kot_voidnc").is(':focus')) {
                            //$("#closeDelete").click(); //orignal
                            $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click(); //modified 29/07/2015
                            closeStaticPopup("popupBoxDelete");
                        } else {
                            $("#kot_voidnc").focus();
                        }
                    }
                    //Code adding over




                } else if (event.keyCode == 27) // ESC KEY PRESSES
                {
                    deSelectAll('rightColum');
                    closeStaticPopup("popupBoxDelete");

                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $("#popupBoxDelete .iconRefresh").click();
                    $("#kot_voidnc").blur();
                }

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }
            }
            else if ($("#popupBoxInwardWastage").is(":visible") == true) {

                var inward = $("#popupBoxInwardWastage").find('input#inwardBill').attr('checked'); //inward CHECKED
                var wastage = $("#popupBoxInwardWastage").find('input#wastageBill').attr('checked'); //wastage CHECKED

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    event.preventDefault();

                    if (inward == 'checked') {
                        $("#popupBoxInwardWastage").find('input#wastageBill').click();


                    } else if (wastage == 'checked') {
                        $("#popupBoxInwardWastage").find('input#inwardBill').click();

                    } else {
                        $("#popupBoxInwardWastage").find('input#inwardBill').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    event.preventDefault();

                    if (inward == 'checked') {
                        $("#popupBoxInwardWastage").find('input#wastageBill').click();


                    } else if (wastage == 'checked') {
                        $("#popupBoxInwardWastage").find('input#inwardBill').click();

                    } else {
                        $("#popupBoxInwardWastage").find('input#wastageBill').click();
                    }
                }
                else if (event.keyCode == 27) { // ESC KEY PRESSED
                    $("#popupBoxInwardWastage .iconClose").click();
                }
                else if (event.keyCode == 13) { // ENTER KEY PRESSES		
                    $("#popupBoxInwardWastage .iconAdd").closest('a').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#popupBoxInwardWastage .iconRefresh").closest('a').click();
                }
                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }

            }
            else if ($("#pkgSelPopUp").is(":visible") == true) {

                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $(".iconClose").click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#pkgSelPopUp .iconRefresh").click();

                }
                else if (event.altKey && event.keyCode == 83) { //ALTR + S
                    event.preventDefault();
                    $("#pkgSelPopUp #savePkg").click();
                    return false; //to prevent shift close popup from opening.

                }
                else if (event.keyCode == 13) { // ENTER KEY PRESSES
                    if ($("#package_qty").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#searchItemPkg").addClass('chkboxfocus');
                        $("#pkg_dropdown").focus();
                    }
                    else if ($("#pkg_dropdown").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#searchItemPkg").addClass('chkboxfocus');
                        $("#searchItemPkg").focus();
                    }
                    else if ($("#searchItemPkg").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#pkg_qty").addClass('chkboxfocus');
                        $("#pkg_qty").focus();
                        $("#pkg_qty").select();

                    }
                    else if ($("#pkg_qty").is(':focus')) {
                        $("#pkgSelPopUp #savePkgItm").click();
                        //$("#pkgSelPopUp #savePkgItm").blur();
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        $("#pkg_qty").blur();
                        $("#package_qty").focus();
                        $("#package_qty").select();
                        //$("#pkg_dropdown").focus();
                    }
                    else {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#pkg_dropdown").addClass('chkboxfocus');
                        $("#package_qty").focus();
                        $("#package_qty").select();


                    }
                }

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }


            }

            /*	Code added by sachin jani over */
            else if (event.altKey && event.keyCode == 66) { // altr + b = focus side bar
                localStorage.POTGalterB = true;
                $('.chkboxfocus').removeClass('chkboxfocus');
                $("#selecteditem .ez-checkbox").eq(0).addClass('chkboxfocus');
                var elem = $('#selecteditem tr').eq(0).find('input');
                $(".odd").customScrollbar("scrollTo", elem);
            }
            else if (event.altKey && event.keyCode == 75) { //IF ALTER KEY + K PRESSED CLICK SEND TO KITCHEN BUTTON
                $(".iconKitchen").click();
            }
            else if (event.altKey && event.keyCode == 80) { //IF ALTER KEY + P PRESSED
                $(".printerIcon").click();
            }
            else if (event.keyCode == 68 && event.altKey && localStorage.POTGalterB == "true") { // ALTER + D TO REMOVE SELECTED ITEM
                event.preventDefault();
                localStorage.POTGalterB = false;
                chkStatusWise();
            }




            if (event.keyCode == 40 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY DOWN ARROW WITH
                var next = $('.chkboxfocus').parent().parent().parent().next().find('.ez-checkbox');
                $('.chkboxfocus').removeClass('chkboxfocus');
                var elem = $(next).find('input');

                if (next.length <= 0)
                {
                    next = $("#selecteditem .ez-checkbox").eq(0);
                    elem = $('#selecteditem tr').eq(0).find('input');

                }

                //next = next.length > 0 ? next : $("#selecteditem .ez-checkbox").eq(0);
                next.addClass('chkboxfocus');
                $(".odd").customScrollbar("scrollTo", elem);
            }
            else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY UP ARROW WITH
                var prev = $('.chkboxfocus').parent().parent().parent().prev().find('.ez-checkbox');
                $('.chkboxfocus').removeClass('chkboxfocus');

                var elem = $(prev).find('input');

                if (prev.length <= 0)
                {
                    prev = $("#selecteditem .ez-checkbox:last");
                    elem = $('#selecteditem tr:last').find('input');

                }
                //prev = prev.length > 0 ? prev : $("#selecteditem .ez-checkbox:last");
                prev.addClass('chkboxfocus');
                $(".odd").customScrollbar("scrollTo", elem);
            }
            else if (event.keyCode == 13 && localStorage.POTGalterB == "true") {
                if ($('.chkboxfocus input').attr('checked'))
                    $('.chkboxfocus input').removeAttr('checked');
                else
                    $('.chkboxfocus input').attr('checked', true);
                $('.chkboxfocus input').closest("div").toggleClass("ez-checked");
            }
            else if (event.keyCode == 69 && localStorage.POTGalterB == "true") { // "E" keycode on item focus.
                event.preventDefault();
                $(".chkboxfocus").closest("tr").click();
                //$("#itm_qty").focus();
                $('.chkboxfocus').removeClass('chkboxfocus');
                localStorage.POTGalterB = false;


            }
            else {
                if (localStorage.POTGpopupflag == "false") {
                    $("#searchtag").focus();
                } else {
                    $("#searchtag").focusout();
                }
            }


        }
        else if (location.indexOf("additemFM.html") > -1) { // IF additem.html PAGE
            //alert('dsfsdf');

            if ($("#addItem").is(":visible") == true) {
                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $(".iconClose").click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#addItem").find(".closeRefreshIcons ").find('a').eq(1).click();
                } else if (event.keyCode == 13) { // ENTER KEY PRESSES		
                    if ($("#qtyvalue").is(':focus')) {
                        $("#remarkinpopup").focus();
                    } else if ($("#remarkinpopup").is(':focus')) {
                        $("#addItem").find(".closeRefreshIcons ").find('a').eq(0).click();
                    } else {
                        $("#qtyvalue").focus();
                        $("#qtyvalue").select(); //added by sachin jani 29/07/2015
                    }
                } else if (event.keyCode == 32) { //Space key
                    if ($("#qtyvalue").is(':focus')) {
                        event.preventDefault(); //disable space on quantity textbox
                    }

                }
                /*
                 else if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) || event.keyCode == 32) { // DECLINE ATOZ ,0 TO 9 AND SPACE BAR
                 } */

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }

            }
            else if ($("#editItem").is(":visible") == true) {

                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $(".iconClose").click();
                } else if (event.keyCode == 13) { // ENTER KEY PRESSES		
                    if ($("#qtyvalue").is(':focus')) {
                        $("#remarkinpopup").focus();
                    } else if ($("#remarkinpopup").is(':focus')) {
                        //$("#EditItemAdd").click();
                        $("#editItem").find(".closeRefreshIcons ").find(".iconAdd").click(); //this element has onClick function not its parent anchor tag.
                    } else {
                        $("#qtyvalue").focus();
                        $("#qtyvalue").select(); //added by sachin jani 29/07/2015
                    }
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D

                    event.preventDefault();
                    $("#editItem").find(".closeRefreshIcons ").find(".iconRefresh").parent().click();

                } else if (event.keyCode == 32) { //Space key
                    if ($("#qtyvalue").is(':focus')) {
                        event.preventDefault(); //disable space on quantity textbox
                    }

                }



                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }

            }
            else if ($("#popupBoxDelete").is(":visible") == true) {
                if (event.keyCode == 13) {

                    /*code added by sachin jani 16/07/2015 */
                    if ($("#kot_voidnc").is(':focus')) {
                        //$("#closeDelete").click(); //orignal
                        $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click(); //modified 29/07/2015
                        closeStaticPopup("popupBoxDelete");
                    } else {
                        $("#kot_voidnc").focus();
                    }


                    /* Code added by sachin jani over */

                    /*
                     //Follwing is the original code.
                     
                     //takeActionOnDeleteItem();
                     $("#closeDelete").click();
                     //closeStaticPopup("popupBoxDelete"); */
                } else if (event.keyCode == 27) {
                    deSelectAll('rightColum');
                    closeStaticPopup("popupBoxDelete");

                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $("#popupBoxDelete .iconRefresh").click();
                    $("#kot_voidnc").blur();
                }

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }
            }
            else if ($("#printBill").is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $("#printBill").find(".ez-radio").find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $("#printBill").find(".radioButton").find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $("#printBill").find(".radioButton").find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                /* happy hours keycode added on 03/11/2015*/
                var hpyhrs = $('#printBill').find('#chkHappyHours').attr('checked');
                var coupn = $('#printBill #discountypeCoupen').attr('checked');
                var hpyhrsIsVisible = $('#printBill').find('#chkHappyHours').is(':visible');

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    }
                    else if (Amt == 'checked') {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                            $("#billremark").focus();

                        }

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {

                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill #discountypeCoupen').click();
                            $("#discounttypevalue").focus();

                        }

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }
                    else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }
                }
                else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {


                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();

                        } else {
                            $("#billremark").focus();
                        }




                    }
                    else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    }
                    else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else if (coupn == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else {
                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15
                    }
                }
                else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();

                }
                else if (event.altKey && event.keyCode == 69) //E
                {
                    event.preventDefault();
                    if ($("#packingCharges").is(':focus')) {
                        if ($('#delC').is(':visible') == true) {

                            $("#deliveryCharges").focus();
                        } else {
                            $("#packingCharges").focus();
                        }

                    }
                    else if ($("#deliveryCharges").is(':focus')) {
                        $("#packingCharges").focus();

                    }
                    else
                    {
                        $("#packingCharges").focus();

                    }


                }

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }

                /* Code added by Sachin jani Over */






            }
            else if ($("#pkgSelPopUp").is(":visible") == true) {

                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $(".iconClose").click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#pkgSelPopUp .iconRefresh").click();

                }
                else if (event.altKey && event.keyCode == 83) { //ALTR + S
                    event.preventDefault();
                    $("#pkgSelPopUp #savePkg").click();
                    return false; //to prevent shift close popup from opening.

                }
                else if (event.keyCode == 13) { // ENTER KEY PRESSES
                    if ($("#package_qty").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#searchItemPkg").addClass('chkboxfocus');
                        $("#pkg_dropdown").focus();
                    }
                    else if ($("#pkg_dropdown").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#searchItemPkg").addClass('chkboxfocus');
                        $("#searchItemPkg").focus();
                    }
                    else if ($("#searchItemPkg").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#pkg_qty").addClass('chkboxfocus');
                        $("#pkg_qty").focus();
                        $("#pkg_qty").select();

                    }
                    else if ($("#pkg_qty").is(':focus')) {
                        $("#pkgSelPopUp #savePkgItm").click();
                        //$("#pkgSelPopUp #savePkgItm").blur();
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        $("#pkg_qty").blur();
                        $("#package_qty").focus();
                        $("#package_qty").select();
                        //$("#pkg_dropdown").focus();
                    }
                    else {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#pkg_dropdown").addClass('chkboxfocus');
                        $("#package_qty").focus();
                        $("#package_qty").select();


                    }
                }

                if (localStorage.POTGpopupflag == "true") {
                    if ($("#searchtag").is(":focus")) {
                        event.preventDefault();
                        $("#searchtag").blur();
                        $("#searchtag").focusout();

                    }
                    $("#searchtag").blur();
                    $("#searchtag").focusout();

                }


            }
            else {
                //alert('fgfdg');
                if (event.altKey && event.keyCode == 66) { // altr + b = focus side bar
                    localStorage.POTGalterB = true;
                    $('.chkboxfocus').removeClass('chkboxfocus');
                    $("#selecteditem .ez-checkbox").eq(0).addClass('chkboxfocus');
                    var elem = $('#selecteditem tr').eq(0).find('input');
                    $(".odd").customScrollbar("scrollTo", elem);
                }
                else if (event.altKey && event.keyCode == 75) { //IF ALTER KEY + K PRESSED CLICK SEND TO KITCHEN BUTTON
                    $(".iconKitchen").click();
                }
                else if (event.altKey && event.keyCode == 80) { // ALETER + P 
                    $(".printerIcon").click();
                }
                else if (event.keyCode == 68 && event.altKey && localStorage.POTGalterB == "true") { // ALTER + D TO REMOVE SELECTED ITEM
                    event.preventDefault();
                    localStorage.POTGalterB = false;
                    chkStatusWise();
                }
                else if (event.altKey && event.keyCode == 65) { //ALT + A discount
                    $("#discountPop").click();
                }

                if (event.keyCode == 40 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY DOWN ARROW WITH
                    var next = $('.chkboxfocus').parent().parent().parent().next().find('.ez-checkbox');
                    $('.chkboxfocus').removeClass('chkboxfocus');
                    var elem = $(next).find('input');

                    if (next.length <= 0)
                    {
                        next = $("#selecteditem .ez-checkbox").eq(0);
                        elem = $('#selecteditem tr').eq(0).find('input');

                    }

                    //next = next.length > 0 ? next : $("#selecteditem .ez-checkbox").eq(0);
                    next.addClass('chkboxfocus');
                    $(".odd").customScrollbar("scrollTo", elem);
                }
                else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY UP ARROW WITH
                    var prev = $('.chkboxfocus').parent().parent().parent().prev().find('.ez-checkbox');
                    $('.chkboxfocus').removeClass('chkboxfocus');

                    var elem = $(prev).find('input');

                    if (prev.length <= 0)
                    {
                        prev = $("#selecteditem .ez-checkbox:last");
                        elem = $('#selecteditem tr:last').find('input');

                    }
                    //prev = prev.length > 0 ? prev : $("#selecteditem .ez-checkbox:last");
                    prev.addClass('chkboxfocus');
                    $(".odd").customScrollbar("scrollTo", elem);
                }
                else if (event.keyCode == 13 && localStorage.POTGalterB == "true") {
                    if ($('.chkboxfocus input').attr('checked'))
                        $('.chkboxfocus input').removeAttr('checked');
                    else
                        $('.chkboxfocus input').attr('checked', true);
                    $('.chkboxfocus input').closest("div").toggleClass("ez-checked");
                }
                else if (event.keyCode == 69 && localStorage.POTGalterB == "true") { // "E" keycode on item focus.
                    event.preventDefault();
                    $(".chkboxfocus").closest("tr").click();
                    //$("#itm_qty").focus();
                    $('.chkboxfocus').removeClass('chkboxfocus');
                    localStorage.POTGalterB = false;


                }
                else {

                    if (localStorage.POTGpopupflag == "false") {
                        $("#searchtag").focus();
                    } else {
                        $("#searchtag").focusout();
                    }
                }

            }

        }
        else if (location.indexOf("pendingorder.html") > -1) { // IF pendingorder.html PAGE


            /*	Following Code added by sachin jani 15/07/2015 */

            if ($("#popupBoxDelete").is(":visible") == true) {
                var vod = $("#popupBoxDelete").find(".radioButton").find('input#deletevoid').attr('checked'); //Void CHECKDED
                var cancel = $("#popupBoxDelete").find(".radioButton").find('input#deletecancel').attr('checked'); //Cancel CHECKED
                var inward = $("#popupBoxDelete").find('input#inwardBill').attr('checked'); //inward CHECKED
                var wastage = $("#popupBoxDelete").find('input#wastageBill').attr('checked'); //wastage CHECKED
                /* Orignal*/
                /*
                 if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                 
                 if (vod == 'checked') {
                 $("#popupBoxDelete").find(".radioButton").find('input#deletecancel').click();
                 
                 $("#order_voidnc").blur();
                 } else if (cancel == 'checked') {
                 $("#popupBoxDelete").find(".radioButton").find('input#deletevoid').click();
                 $("#order_voidnc").blur();
                 } else {
                 $("#popupBoxDelete").find(".radioButton").find('input#deletecancel').click();
                 }
                 
                 } 
                 else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                 if (vod == 'checked') {
                 $("#popupBoxDelete").find(".radioButton").find('input#deletecancel').click();
                 $("#order_voidnc").blur();
                 } else if (cancel == 'checked') {
                 $("#popupBoxDelete").find(".radioButton").find('input#deletevoid').click();
                 $("#order_voidnc").blur();
                 } else {
                 $("#popupBoxDelete").find(".radioButton").find('input#deletecancel').click();
                 }
                 } 
                 */
                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    event.preventDefault();
                    $("#order_voidnc").blur();
                    if (inward == 'checked') {
                        $("#popupBoxDelete").find('input#wastageBill').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    event.preventDefault();
                    $("#order_voidnc").blur();

                    if (inward == 'checked') {
                        $("#popupBoxDelete").find('input#wastageBill').click();

                        //$("#order_voidnc").blur();
                    }
                    else if (wastage == 'checked') {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                        //$("#order_voidnc").blur();
                    }
                    else {
                        $("#popupBoxDelete").find('input#wastageBill').click();
                    }
                }
                else if (event.keyCode == 13) // ENTER KEY PRESSES
                {


                    if (vod == 'checked') {
                        if ($("#order_voidnc").is(':focus')) {

                            console.log("on Enter" + $("#order_voidnc").is(':focus'));
                            $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();
                            $("#order_voidnc").blur();
                            console.log($("#order_voidnc").is(':focus'));
                            //$("#closebtn").click();
                        } else {
                            $("#order_voidnc").focus();
                        }

                    }
                    else if (cancel == 'checked') {
                        if ($("#order_voidnc").is(':focus')) {
                            console.log("on Enter" + $("#order_voidnc").is(':focus'));

                            $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();
                            $("#order_voidnc").blur();
                            console.log("after Enter: blur" + $("#order_voidnc").is(':focus'));
                            //$("#closebtn").click();
                        } else {
                            $("#order_voidnc").focus();
                            console.log("on Enter: Else" + $("#order_voidnc").is(':focus'));
                        }


                    }






                }
                else if (event.keyCode == 27) // ESC KEY PRESSES
                {

                    closeStaticPopup("popupBoxDelete");

                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $("#order_voidnc").blur();
                }


            }
            else if ($("#printBill").is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $("#printBill").find(".ez-radio").find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $("#printBill").find(".radioButton").find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $("#printBill").find(".radioButton").find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                /* happy hours keycode added on 03/11/2015*/
                var hpyhrs = $('#printBill').find('#chkHappyHours').attr('checked');
                var coupn = $('#printBill #discountypeCoupen').attr('checked');
                var hpyhrsIsVisible = $('#printBill').find('#chkHappyHours').is(':visible');


                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    }
                    else if (Amt == 'checked') {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                            $("#billremark").focus();

                        }

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {

                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill #discountypeCoupen').click();
                            $("#discounttypevalue").focus();

                        }

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }
                    else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }
                }
                else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {


                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();

                        } else {
                            $("#billremark").focus();
                        }




                    }
                    else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    }
                    else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        }
                        else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");
                        }
                        else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else if (coupn == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        }
                        else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");
                        }
                        else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else {
                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15
                    }
                }
                else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();

                }
                else if (event.altKey && event.keyCode == 69) //E
                {
                    event.preventDefault();
                    if ($("#packingCharges").is(':focus')) {
                        if ($('#delC').is(':visible') == true) {

                            $("#deliveryCharges").focus();
                        } else {
                            $("#packingCharges").focus();
                        }

                    }
                    else if ($("#deliveryCharges").is(':focus')) {
                        $("#packingCharges").focus();

                    }
                    else
                    {
                        $("#packingCharges").focus();

                    }


                }


                /* Code added by Sachin jani Over */






            }
            else if ($("#permissionPasswordCancel").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {

                        $("#permissionPasswordCancel").find(".closeRefreshIcons ").find('a').eq(0).click();

                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordCancel").find('.iconClose').click();
                }






            }
            else if ($("#permissionPasswordEdit").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordEdit").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordEdit").find('.iconClose').click();
                }






            }
            else if ($("#deliverBox").is(":visible") == true) {

                var deliveryboy = $("#deliverBox").find("#deliveryboy");

                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(deliveryboy).is(':focus')) {
                        $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(deliveryboy).focus();
                    }


                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("deliverBox");
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(deliveryboy).blur();
                }


            }
            else if (event.keyCode == 40 && localStorage.POTGsideMenu == "false") { //IF KEY DOWN ARROW WITH ALTR + M NOT ACTIVATED
                var next = $('.active').next('tr');
                $('.active').removeClass('active');
                next = next.length > 0 ? next : $('.tableData tr').eq(1);
                next.addClass('active').focus();
            } else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false") { //IF KEY UP ARROW WITH ALTR + M NOT ACTIVATED
                var prev = $('.active').prev('tr');
                $('.active').removeClass('active');
                prev = prev.length > 0 ? prev : $('.tableData tr:last');
                prev.addClass('active').focus();
            } else if (event.altKey && event.keyCode == 80) { // ALETER + P FOR PRINT FOR ACTIVE CLASS
                $("tr.active").find('.billingPrinterIcon').closest('a').click();



            } else if (event.altKey && event.keyCode == 68) { // ALETER + D FOR DELETE BILL
                event.preventDefault();
                $("tr.active").find('.deleteIcon').closest('a').click();

            } else if (event.altKey && event.keyCode == 69) { // ALETER + E FOR EDIT BILL
                event.preventDefault();
                $("tr.active").find('.tagIcon').closest('a').click();
            } else if (event.altKey && event.keyCode == 89) { // ALETER + Y FOR DEl BOY
                event.preventDefault();
                $("tr.active").find('.runMenIcon').closest('a').click();
            }

        }
        else if (location.indexOf("billing.html") > -1) { // IF billing.html PAGE



            if ($("#billPaymentBox").is(":visible") == true) { // IF PAYMENT POP UP OPEN
                //$(".price").focus();				
                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP					
                    //$("#saveDisable").parent().click();
                    $("#billPaymentBox").find(".closeRefreshIcons ").find('a').eq(0).click();
                    //closeStaticPopup("billPaymentBox");
                }
                else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    $("#billPaymentBox").find(".closeRefreshIcons ").find(".iconClose").click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#billPaymentBox").find(".closeRefreshIcons ").find('a').eq(1).click();

                }

            }
            else if ($("#popupBoxDelete").is(":visible") == true) {

                /*Following code is added by sachin jani 16/07/2015 */
                var inward = $("#popupBoxDelete").find('input#inwardBill').attr('checked'); //inward CHECKED
                var wastage = $("#popupBoxDelete").find('input#wastageBill').attr('checked'); //wastage CHECKED
                /*Following code is added by sachin jani 16/07/2015 */
                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    event.preventDefault();
                    $("#bill_voidnc").blur();

                    if (inward == 'checked') {
                        $("#popupBoxDelete").find('input#wastageBill').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    event.preventDefault();
                    $("#bill_voidnc").blur();

                    if (inward == 'checked') {
                        $("#popupBoxDelete").find('input#wastageBill').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#popupBoxDelete").find('input#wastageBill').click();
                    }
                }

                else if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($("#bill_voidnc").is(':focus')) {
                        $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $("#bill_voidnc").focus();
                    }


                }
                else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("popupBoxDelete");
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $("#bill_voidnc").blur();
                }





                /* code added by sachin jani over */


                /* //Orignal code
                 
                 if(event.keyCode == 13  && localStorage.POTGsideMenu == "false"){ // IF ENTER PRESSED WHEN PAYMENT POPUP
                 $("#closespan").click();
                 }else if(event.keyCode == 27 && localStorage.POTGsideMenu == "false"){ // IF ESC PRESSED
                 closeStaticPopup("popupBoxDelete");
                 } */


            }
            else if ($("#permissionPasswordCancel").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordCancel").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordCancel").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if ($("#permissionPasswordEdit").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordEdit").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordEdit").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if ($("#permissionPasswordPrint").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordPrint").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordPrint").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if (event.keyCode == 40 && localStorage.POTGsideMenu == "false") { //IF KEY DOWN ARROW WITH ALTR + M NOT ACTIVATED
                var next = $('.active').next('tr');
                $('.active').removeClass('active');
                next = next.length > 0 ? next : $('.tableData tr').eq(1);
                next.addClass('active').focus();
            }
            else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false") { //IF KEY UP ARROW WITH ALTR + M NOT ACTIVATED
                var prev = $('.active').prev('tr');
                $('.active').removeClass('active');
                prev = prev.length > 0 ? prev : $('.tableData tr:last');
                prev.addClass('active').focus();
            }
            else if (event.altKey && event.keyCode == 69) { //IF ALTER + E  PRESSED
                event.preventDefault();
                $("tr.active").find('.tagIcon').closest('a').click();
            }
            else if (event.altKey && event.keyCode == 80) { // ALETER + P FOR PRINT FOR ACTIVE CLASS
                $("tr.active").find('.billingPrinterIcon').closest('a').click();
            }
            else if (event.altKey && event.keyCode == 65) { //ALT + A
                $("tr.active").find('.rupeesIcon').closest('a').click();
            }
            else if (event.altKey && event.keyCode == 68) { // ALETER + D FOR DELETE BILL
                event.preventDefault();
                $("tr.active").find('.deleteIcon').closest('a').click();
            }


        }
        else if (location.indexOf("additemFM2.html") > -1) { // IF additem.html PAGE

            if (localStorage.POTGpopupflag == "false" && localStorage.POTGorderMenuPopupflag == "false") {
                //orderMenuPopupflag
                if (!localStorage.POTGorderIdForEdit) {
                    var dineIn = $('#dine').attr('checked');
                    var kiosk = $('#kos').attr('checked');
                    var HD = $('#homed').attr('checked');
                    var dineInFocus = $('#dine').closest('div').hasClass('radiobuttonfocus');
                    var kioskFocus = $('#kos').closest('div').hasClass('radiobuttonfocus');
                    var HDFocus = $('#homed').closest('div').hasClass('radiobuttonfocus');


                    if (!dineIn && !kiosk && !HD) {
                        //check for focus


                        if (!dineInFocus && !kioskFocus && !HDFocus) {
                            // default selection if nothing is selected
                            if (event.keyCode == 37 || event.keyCode == 39) {
                                $('#dine').closest('div').addClass('radiobuttonfocus');
                                //$('#dine').click(); 
                                //$('#dine').focus();

                            }


                        } else if (dineInFocus) {
                            if (event.keyCode == 39) {
                                $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                $('#kos').closest('div').addClass('radiobuttonfocus');

                            } else if (event.keyCode == 37) {
                                $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                $('#homed').closest('div').addClass('radiobuttonfocus');

                            } else if (event.keyCode == 13) {
                                $('#dine').click();
                                if (!FASTCHECKIN) {
                                    $('#cust_radio_guest').click();
                                    $('#cust_radio_guest').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = true;
                                }
                            }


                        } else if (kioskFocus) {
                            if (event.keyCode == 39) {
                                $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                $('#homed').closest('div').addClass('radiobuttonfocus');

                            } else if (event.keyCode == 37) {
                                $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                $('#dine').closest('div').addClass('radiobuttonfocus');

                            } else if (event.keyCode == 13) {
                                $('#kos').click();
                                if (LOYALTY_FLAG) {
                                    $('#cust_radio_guest1').click();
                                    $('#cust_radio_guest1').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = true;
                                }

                            }

                        } else if (HDFocus) {
                            if (event.keyCode == 39) {
                                $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                $('#dine').closest('div').addClass('radiobuttonfocus');

                            } else if (event.keyCode == 37) {
                                $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                $('#kos').closest('div').addClass('radiobuttonfocus');

                            } else if (event.keyCode == 13) {
                                $('#homed').click();

                                $('#cust_radio_existing2').click();
                                $('#cust_radio_existing2').focus();
                                localStorage.POTGitemsPanel = false;
                                localStorage.POTGcustPanel = true;

                            }

                        }

                    } else if (localStorage.POTGitemsPanel == "false") {

                        if (dineIn) {
                            if (FASTCHECKIN) {
                                if (event.keyCode == 13) { //ENTER
                                    //$('#cust_radio_guest').click();

                                    /* added on 14/10/2015 START */
                                    var cust_numofguest = $("#dine1").find("#cust_guest");
                                    var cust_tableno = $("#dine1").find("#cust_tableno");
                                    var cust_captin = $("#dine1").find("#cust_captin");
                                    var ordersource = $("#dine1").find("#ordersource");


                                    if ($(cust_numofguest).is(':focus')) {
                                        $('#saveCustDineIn').click();
                                    } else if ($(cust_tableno).is(':focus')) {
                                        $(cust_tableno).blur();
                                        $('#saveCustDineIn').click();
                                        //$(cust_captin).focus();
                                        //openSelect(cust_captin);
                                    } else if ($(cust_captin).is(':focus')) {
                                        $('#saveCustDineIn').click();
                                    } else if ($(ordersource).is(':focus')) {
                                        $('#saveCustDineIn').click();
                                    }
                                    /* added on 14/10/2015 END */


                                }


                            } else {

                                if (localStorage.POTGcustPanel == "false") {

                                    if (event.keyCode == 13) { //ENTER
                                        $('#cust_radio_guest').click();
                                        $('#cust_radio_guest').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = true;


                                    }
                                } else if (localStorage.POTGcustPanel == "true") {

                                    var guest = $("#dine1").find("#cust_radio_guest").attr("checked");
                                    var isNew = $("#dine1").find("#cust_radio_new").attr("checked");
                                    var isexisting = $("#dine1").find("#cust_radio_existing").attr("checked");

                                    var isGuestFocus = $("#dine1").find("#cust_radio_guest").is(':focus');
                                    var isNewFocus = $("#dine1").find("#cust_radio_new").is(':focus');
                                    var isexistingFocus = $("#dine1").find("#cust_radio_existing").is(':focus');
                                    var cust;
                                    if (isGuestFocus || isNewFocus || isexistingFocus) {
                                        cust = true;
                                    } else {
                                        cust = false;
                                    }

                                    if (cust && event.keyCode == 27) //ESC
                                    {
                                        $('#dine').click();
                                        $('#dine').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = false;


                                    } else if (cust && event.keyCode == 13) //Enter
                                    { //initial focus on ENTER 
                                        if (guest) {
                                            $("#dine1").find("#cust_guest").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');


                                        } else if (isNew) {
                                            $("#dine1").find("#cust_fname").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        } else if (isexisting) {
                                            $("#dine1").find("#cust_name").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        }

                                    } else if (!cust && event.keyCode == 13) //Enter
                                    {
                                        //$('#cust_radio_guest').click();
                                        //$('#cust_radio_guest').focus();
                                        //localStorage.POTGitemsPanel = false;
                                        //localStorage.POTGcustPanel = true;

                                        if (guest) {

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $(cust_tableno).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustDineIn').click();
                                            } else {
                                                $(cust_numofguest).focus();

                                            }






                                        } else if (isNew) {


                                            var cust_fname = $("#dine1").find("#cust_fname");
                                            var cust_lname = $("#dine1").find("#cust_lname");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_tableno).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustDineIn').click();
                                            } else {
                                                $(cust_fname).focus();

                                            }


                                        } else if (isexisting) {
                                            var cust_name = $("#dine1").find("#cust_name");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_tableno).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustDineIn').click();
                                            } else {
                                                $(cust_name).focus();

                                            }




                                        }



                                    } else if (!cust && event.keyCode == 27) //ESC
                                    {
                                        if (guest) {

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $("#dine1").find("#cust_radio_guest").click();
                                                $("#dine1").find("#cust_radio_guest").focus();

                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_tableno).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                //event.which =13;
                                                //event.keyCode =13;
                                                //$(cust_captin).trigger(event);
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }






                                        } else if (isNew) {
                                            var cust_fname = $("#dine1").find("#cust_fname");
                                            var cust_lname = $("#dine1").find("#cust_lname");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $("#dine1").find("#cust_radio_new").click();
                                                $("#dine1").find("#cust_radio_new").focus();

                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_fname).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_tableno).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }





                                        } else if (isexisting) {
                                            var cust_name = $("#dine1").find("#cust_name");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $("#dine1").find("#cust_radio_existing").click();
                                                $("#dine1").find("#cust_radio_existing").focus();

                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_name).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_tableno).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }




                                        }





                                    }
                                }

                            }

                        } else if (kiosk) {
                            /*
                             if(localStorage.POTGcustPanel == "false")
                             {						
                             
                             if (event.keyCode == 13) {
                             $('#cust_radio_guest1').click();
                             $('#cust_radio_guest1').focus();
                             localStorage.POTGitemsPanel = false;
                             localStorage.POTGcustPanel = true;
                             
                             }
                             }
                             else if(localStorage.POTGcustPanel == "true")
                             {
                             
                             if(event.keyCode == 27)
                             {
                             $('#kos').click(); 
                             $('#kos').focus();
                             localStorage.POTGitemsPanel = false;
                             localStorage.POTGcustPanel = false;
                             
                             }
                             }
                             */

                            if (LOYALTY_FLAG) {
                                if (localStorage.POTGcustPanel == "false") {

                                    if (event.keyCode == 13) { //ENTER
                                        $('#cust_radio_guest1').click();
                                        $('#cust_radio_guest1').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = true;


                                    }
                                } else if (localStorage.POTGcustPanel == "true") {

                                    var guest = $("#kos1").find("#cust_radio_guest1").attr("checked");
                                    var isNew = $("#kos1").find("#cust_radio_new1").attr("checked");
                                    var isexisting = $("#kos1").find("#cust_radio_existing1").attr("checked");

                                    var isGuestFocus = $("#kos1").find("#cust_radio_guest1").is(':focus');
                                    var isNewFocus = $("#kos1").find("#cust_radio_new1").is(':focus');
                                    var isexistingFocus = $("#kos1").find("#cust_radio_existing1").is(':focus');
                                    var cust;
                                    if (isGuestFocus || isNewFocus || isexistingFocus) {
                                        cust = true;
                                    } else {
                                        cust = false;
                                    }

                                    if (cust && event.keyCode == 27) //ESC
                                    {
                                        $('#kos').click();
                                        $('#kos').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = false;


                                    } else if (cust && event.keyCode == 13) //Enter
                                    { //initial focus on ENTER 
                                        if (guest) {
                                            $("#kos1").find("#cust_guest").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');


                                        } else if (isNew) {
                                            $("#kos1").find("#cust_fname").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        } else if (isexisting) {
                                            $("#kos1").find("#cust_name").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        }

                                    } else if (!cust && event.keyCode == 13) //Enter
                                    {
                                        //$('#cust_radio_guest').click();
                                        //$('#cust_radio_guest').focus();
                                        //localStorage.POTGitemsPanel = false;
                                        //localStorage.POTGcustPanel = true;

                                        if (guest) {

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $(cust_captin).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustKiosk').click();
                                            } else {
                                                $(cust_numofguest).focus();

                                            }






                                        } else if (isNew) {


                                            var cust_fname = $("#kos1").find("#cust_fname");
                                            var cust_lname = $("#kos1").find("#cust_lname");
                                            var cust_phone = $("#kos1").find("#cust_phone");

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_captin).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustKiosk').click();
                                            } else {
                                                $(cust_fname).focus();

                                            }


                                        } else if (isexisting) {
                                            var cust_name = $("#kos1").find("#cust_name");
                                            var cust_phone = $("#kos1").find("#cust_phone");
                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_captin).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustKiosk').click();
                                            } else {
                                                $(cust_name).focus();

                                            }




                                        }



                                    } else if (!cust && event.keyCode == 27) //ESC
                                    {
                                        if (guest) {

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $("#kos1").find("#cust_radio_guest1").click();
                                                $("#kos1").find("#cust_radio_guest1").focus();

                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                //event.which =13;
                                                //event.keyCode =13;
                                                //$(cust_captin).trigger(event);
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }






                                        } else if (isNew) {
                                            var cust_fname = $("#kos1").find("#cust_fname");
                                            var cust_lname = $("#kos1").find("#cust_lname");
                                            var cust_phone = $("#kos1").find("#cust_phone");

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $("#kos1").find("#cust_radio_new1").click();
                                                $("#kos1").find("#cust_radio_new1").focus();

                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_fname).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }





                                        } else if (isexisting) {
                                            var cust_name = $("#kos1").find("#cust_name");
                                            var cust_phone = $("#kos1").find("#cust_phone");

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $("#kos1").find("#cust_radio_existing1").click();
                                                $("#kos1").find("#cust_radio_existing1").focus();

                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_name).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }




                                        }





                                    }
                                }
                            }

                        } else if (HD) {

                            if (localStorage.POTGcustPanel == "false") {

                                if (event.keyCode == 13) { //ENTER
                                    $('#cust_radio_existing2').click();
                                    $('#cust_radio_existing2').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = true;


                                }
                            } else if (localStorage.POTGcustPanel == "true") {


                                var isNew = $("#homed1").find("#cust_radio_new2").attr("checked");
                                var isexisting = $("#homed1").find("#cust_radio_existing2").attr("checked");


                                var isNewFocus = $("#homed1").find("#cust_radio_new2").is(':focus');
                                var isexistingFocus = $("#homed1").find("#cust_radio_existing2").is(':focus');
                                var cust;
                                if (isNewFocus || isexistingFocus) {
                                    cust = true;
                                } else {
                                    cust = false;
                                }

                                if (cust && event.keyCode == 27) //ESC
                                {
                                    $('#homed').click();
                                    $('#homed').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = false;


                                } else if (cust && event.keyCode == 13) //Enter
                                { //initial focus on ENTER 

                                    if (isNew) {
                                        $("#homed1").find("#cust_fname").focus();
                                        $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    } else if (isexisting) {
                                        $("#homed1").find("#cust_name").focus();
                                        $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    }

                                } else if (!cust && event.keyCode == 13) //Enter
                                {
                                    //$('#cust_radio_guest').click();
                                    //$('#cust_radio_guest').focus();
                                    //localStorage.POTGitemsPanel = false;
                                    //localStorage.POTGcustPanel = true; 


                                    if (isNew) {


                                        var cust_fname = $("#homed1").find("#cust_fname");
                                        var cust_lname = $("#homed1").find("#cust_lname");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;

                                        if ($(cust_fname).is(':focus')) {
                                            $(cust_lname).focus();
                                        } else if ($(cust_lname).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_houseno).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_street).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_captin).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                            } else {
                                                $(ordersource).focus();
                                            }



                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(ordersource).focus();
                                        } else if ($(ordersource).is(':focus')) {
                                            $('#saveCustHomeDel').click();
                                        } else {
                                            $(cust_fname).focus();

                                        }


                                    } else if (isexisting) {


                                        var cust_name = $("#homed1").find("#cust_name");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;

                                        if ($(cust_name).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_houseno).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_street).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_captin).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                            } else {
                                                $(ordersource).focus();
                                            }



                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(ordersource).focus();
                                        } else if ($(ordersource).is(':focus')) {
                                            $('#saveCustHomeDel').click();
                                        } else {
                                            $(cust_name).focus();

                                        }




                                    }



                                } else if (!cust && event.keyCode == 27) //ESC
                                {

                                    if (isNew) {


                                        var cust_fname = $("#homed1").find("#cust_fname");
                                        var cust_lname = $("#homed1").find("#cust_lname");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;




                                        if ($(cust_fname).is(':focus')) {
                                            $("#homed1").find("#cust_radio_new2").click();
                                            $("#homed1").find("#cust_radio_new2").focus();
                                        } else if ($(cust_lname).is(':focus')) {
                                            $(cust_fname).focus();
                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_lname).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_houseno).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            $(cust_street).focus();

                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(cust_captin).focus();
                                            openSelect(cust_captin);
                                        } else if ($(ordersource).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                                openSelect(home_cust_area);
                                            } else {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }


                                        }


                                    } else if (isexisting) {


                                        var cust_name = $("#homed1").find("#cust_name");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;



                                        if ($(cust_name).is(':focus')) {
                                            $("#homed1").find("#cust_radio_existing2").click();
                                            $("#homed1").find("#cust_radio_existing2").focus();

                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_name).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_houseno).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            $(cust_street).focus();


                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(cust_captin).focus();
                                            openSelect(cust_captin);
                                        } else if ($(ordersource).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                                openSelect(home_cust_area);
                                            } else {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }

                                        }




                                    }






                                }
                            }



                        }

                    }

                }






                if ($("#itm_qty").is(':focus')) {
                    console.log($("#itm_qty").is(':focus'));
                    if (event.keyCode == 13) {
                        $("#itm_qty").blur();
                        $("#itm_qty").focusout();
                        $("#additm").click();
                        //$("#itm_name").focus();


                    } else if (event.keyCode == 27) {
                        $("#itm_name").focus();

                    } else if (event.keyCode == 110 || event.keyCode == 190) {
                        event.preventDefault();
                        //return false;
                    }

                } else if ($("#itm_name").is(':focus')) {
                    if (event.keyCode == 13) {
                        var itm_name = $("#itm_name").val();
                        if (itm_name) {
                            $("#itm_qty").focus();
                        }

                    } else if (event.altKey) {
                        $("#itm_name").focusout();
                        $("#itm_name").blur();
                        altkeyFM2(event);

                    }
                }




            }







            if ($("#popupBoxDelete").is(":visible") == true) {
                if (event.keyCode == 13) {
                    var kot_voidnc = $("#popupBoxDelete").find("#kot_voidnc");
                    /*code added by sachin jani 16/07/2015 */
                    if ($(kot_voidnc).is(':focus')) {
                        //$("#closeDelete").click(); //orignal
                        $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click(); //modified 29/07/2015
                        closeStaticPopup("popupBoxDelete");
                    } else {
                        $(kot_voidnc).focus();
                    }


                    /* Code added by sachin jani over */

                } else if (event.keyCode == 27) {
                    $("#popupBoxDelete").find(".iconClose").click();


                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#popupBoxDelete").find(".iconRefresh").click();
                    $("#kot_voidnc").blur();
                }

                if (localStorage.POTGpopupflag == "true") {
                    //$("#searchtag").blur();
                    //$("#searchtag").focusout();
                }
            } else if ($("#billingpopupBoxDelete").is(":visible") == true) {

                var bill_voidnc = $("#billingpopupBoxDelete").find("#bill_voidnc");

                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(bill_voidnc).is(':focus')) {
                        $("#billingpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(bill_voidnc).focus();
                    }


                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("billingpopupBoxDelete");
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#billingpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(bill_voidnc).blur();
                }


            } else if ($("#pendingOrderpopupBoxDelete").is(":visible") == true) {

                var order_voidnc = $("#pendingOrderpopupBoxDelete").find("#order_voidnc");

                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(order_voidnc).is(':focus')) {
                        $("#pendingOrderpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(order_voidnc).focus();
                    }


                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("pendingOrderpopupBoxDelete");
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#pendingOrderpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(order_voidnc).blur();
                }


            } else if ($("#deliverBox").is(":visible") == true) {

                var deliveryboy = $("#deliverBox").find("#deliveryboy");

                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(deliveryboy).is(':focus')) {
                        $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(deliveryboy).focus();
                    }


                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("deliverBox");
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(deliveryboy).blur();
                }


            } else if ($("#printBill").is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $("#printBill").find(".ez-radio").find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $("#printBill").find(".radioButton").find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $("#printBill").find(".radioButton").find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    } else if (Per == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    } else if (Amt == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();



                    } else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }

                } else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    } else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    } else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    } else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }
                } else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {


                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();

                        } else {
                            $("#billremark").focus();
                        }




                    } else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    } else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    } else {
                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15
                    }
                } else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();

                }

                if (localStorage.POTGpopupflag == "true") {
                    //$("#searchtag").blur();
                    //$("#searchtag").focusout();
                }

                /* Code added by Sachin jani Over */






            } else if ($("#billPaymentBox").is(":visible") == true) { // IF PAYMENT POP UP OPEN
                //$(".price").focus();				
                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP					
                    //$("#saveDisable").parent().click();
                    $("#billPaymentBox").find(".closeRefreshIcons ").find('a').eq(0).click();
                    closeStaticPopup("billPaymentBox");
                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    $("#billPaymentBox").find(".closeRefreshIcons ").find(".iconClose").click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#billPaymentBox").find(".closeRefreshIcons ").find('a').eq(1).click();

                }

            } else if ($("#permissionPasswordCancel").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordCancel").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordCancel").find('.iconClose').click();
                }






            } else if ($("#permissionPasswordEdit").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordEdit").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordEdit").find('.iconClose').click();
                }






            } else if ($("#permissionPasswordPrint").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordPrint").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordPrint").find('.iconClose').click();
                }






            } else if ($("#editItem").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    var isremarkfocus = $("#txtitemRemark").is(':focus');
                    console.log("Txt remark is focus : " + isremarkfocus);
                    if ($("#txtitemRemark").is(':focus')) {
                        event.preventDefault();
                        $("#editItem").find("#itmRemarkSave").click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#editItem").find('.iconClose').click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#editItem").find('.iconRefresh').click();
                }



            } else if ($("#dineInMenuDiv").is(':visible') == true) {

                if (event.keyCode == 39) {
                    event.preventDefault();
                    //$('#dineInMenutxtscrn li').eq(0).prev('li');
                    var activeli = $('.activetable');
                    var activeliNext = $('.activetable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activetable').removeClass('activetable');
                        $(activeliNext).addClass('activetable');
                    } else {
                        $('.activetable').removeClass('activetable');
                        $('#dineInMenutxtscrn li').eq(0).addClass('activetable');
                    }


                } else if (event.keyCode == 37) {
                    event.preventDefault();
                    //$('#dineInMenutxtscrn li').eq(0).prev('li');
                    var activeli = $('.activetable');
                    var activeliPrev = $('.activetable').prev('li');
                    var liLen = $('#dineInMenutxtscrn li').length;

                    if (activeliPrev.length > 0) {
                        $('.activetable').removeClass('activetable');
                        $(activeliPrev).addClass('activetable');
                    } else {
                        $('.activetable').removeClass('activetable');
                        $('#dineInMenutxtscrn li').eq(liLen - 1).addClass('activetable');
                    }


                } else if (event.keyCode == 13) {
                    event.preventDefault();
                    $('.activetable a').click();
                } else if (event.keyCode == 27) {
                    event.preventDefault();
                    $('.dineInMenuIcon').click();
                } else if (event.shiftKey) {

                    shiftkeyFM2(event);

                }




            } else if ($("#homeDelMenuDiv").is(':visible') == true) {

                if (event.keyCode == 39) {
                    event.preventDefault();

                    var activeli = $('.activetable');
                    var activeliNext = $('.activetable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activetable').removeClass('activetable');
                        $(activeliNext).addClass('activetable');
                    } else {
                        $('.activetable').removeClass('activetable');
                        $('#homeDelMenutxtscrn li').eq(0).addClass('activetable');
                    }


                } else if (event.keyCode == 37) {
                    event.preventDefault();

                    var activeli = $('.activetable');
                    var activeliPrev = $('.activetable').prev('li');
                    var liLen = $('#homeDelMenutxtscrn li').length;

                    if (activeliPrev.length > 0) {
                        $('.activetable').removeClass('activetable');
                        $(activeliPrev).addClass('activetable');
                    } else {
                        $('.activetable').removeClass('activetable');
                        $('#homeDelMenutxtscrn li').eq(liLen - 1).addClass('activetable');
                    }


                } else if (event.keyCode == 13) {
                    event.preventDefault();
                    $('.activetable a').click();
                } else if (event.keyCode == 27) {
                    event.preventDefault();
                    $('.HDMenuIcon').click();
                } else if (event.shiftKey) {

                    shiftkeyFM2(event);

                }




            } else if ($("#kioskMenuDiv").is(':visible') == true) {

                if (event.keyCode == 39) {
                    event.preventDefault();

                    var activeli = $('.activetable');
                    var activeliNext = $('.activetable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activetable').removeClass('activetable');
                        $(activeliNext).addClass('activetable');
                    } else {
                        $('.activetable').removeClass('activetable');
                        $('#kioskMenutxtscrn li').eq(0).addClass('activetable');
                    }


                } else if (event.keyCode == 37) {
                    event.preventDefault();

                    var activeli = $('.activetable');
                    var activeliPrev = $('.activetable').prev('li');
                    var liLen = $('#kioskMenutxtscrn li').length;

                    if (activeliPrev.length > 0) {
                        $('.activetable').removeClass('activetable');
                        $(activeliPrev).addClass('activetable');
                    } else {
                        $('.activetable').removeClass('activetable');
                        $('#kioskMenutxtscrn li').eq(liLen - 1).addClass('activetable');
                    }


                } else if (event.keyCode == 13) {
                    event.preventDefault();
                    $('.activetable a').click();
                } else if (event.keyCode == 27) {
                    event.preventDefault();
                    $('.kioskMenuIcon').click();
                } else if (event.shiftKey) {

                    shiftkeyFM2(event);

                }




            } else {
                if (localStorage.POTGpopupflag == "false") {
                    altkeyFM2(event);
                    shiftkeyFM2(event);
                }
            }




            function altkeyFM2(event) {



                if (event.altKey && event.keyCode == 66) { // altr + b = focus side bar
                    localStorage.POTGalterB = true;
                    $("#selecteditem .ez-checkbox").eq(0).addClass('chkboxfocus');
                } else if (event.altKey && event.keyCode == 75) { //IF ALTER KEY + K PRESSED CLICK SEND TO KITCHEN BUTTON
                    $(".iconKitchen").click();
                } else if (event.altKey && event.keyCode == 80) { // ALETER + P 
                    $(".printerIcon").click();
                } else if (event.keyCode == 68 && event.altKey && localStorage.POTGalterB == "true") { // ALTER + D TO REMOVE SELECTED ITEM
                    event.preventDefault();
                    localStorage.POTGalterB = false;
                    $(".deleteIcon").click();

                } else if (event.altKey && event.keyCode == 65) { //ALT + A discount
                    //$("#discountPop").click();
                }

                if (event.keyCode == 40 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY DOWN ARROW WITH
                    //var next = $('.chkboxfocus').parent().parent().parent().next().find('.ez-checkbox');
                    var next = $('.chkboxfocus').closest('tr').next().find('.ez-checkbox');
                    $('.chkboxfocus').removeClass('chkboxfocus');
                    next = next.length > 0 ? next : $("#selecteditem .ez-checkbox").eq(0);
                    next.addClass('chkboxfocus');
                } else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY UP ARROW WITH
                    //var prev = $('.chkboxfocus').parent().parent().parent().prev().find('.ez-checkbox');
                    var prev = $('.chkboxfocus').closest('tr').prev().find('.ez-checkbox');

                    $('.chkboxfocus').removeClass('chkboxfocus');
                    prev = prev.length > 0 ? prev : $("#selecteditem .ez-checkbox:last");
                    prev.addClass('chkboxfocus');
                } else if (event.keyCode == 13 && localStorage.POTGalterB == "true") {
                    if ($('.chkboxfocus input').attr('checked'))
                        $('.chkboxfocus input').removeAttr('checked');
                    else
                        $('.chkboxfocus input').attr('checked', true);
                    $('.chkboxfocus input').closest("div").toggleClass("ez-checked");
                } else if (event.keyCode == 69 && localStorage.POTGalterB == "true") { // "E" keycode on item focus.
                    event.preventDefault();
                    $(".chkboxfocus").closest("tr").click();
                    $("#itm_qty").focus();
                    $('.chkboxfocus').removeClass('chkboxfocus');
                    localStorage.POTGalterB = false;


                } else if (event.keyCode == 82 && localStorage.POTGalterB == "true") { // "R" keycode on item focus.
                    event.preventDefault();
                    $(".chkboxfocus").closest("tr").find('button').click();
                    $('.chkboxfocus').removeClass('chkboxfocus');
                    localStorage.POTGalterB = false;


                } else {
                    /*
                     if(localStorage.POTGpopupflag == "false")
                     {
                     $("#searchtag").focus();
                     }
                     else
                     {
                     $("#searchtag").focusout();
                     } */
                }







            }

            function shiftkeyFM2(event) {

                if (event.shiftKey && event.keyCode == 65) { // // shiftKey + A to Collect Payment Bill/Order
                    $(".billPayment").click();
                } else if (event.shiftKey && event.keyCode == 80) { // shiftKey + P to PRINT Bill/Order
                    $(".orderPrint").click();
                } else if (event.keyCode == 68 && event.shiftKey) { // shiftKey + D to Cancel Bill/Order
                    $(".orderCancel").click();

                } else if (event.keyCode == 69 && event.shiftKey) { // shiftKey + E to EDIT Bill/Order
                    $(".orderEdit").click();
                } else if (event.shiftKey && event.keyCode == 89) { // shiftKey + Y  to assign delivery boy
                    $(".delBoy").click();
                } else if (event.shiftKey && event.keyCode == 73) { // shiftKey + I  to Open DineIn Menu
                    event.preventDefault();
                    $(".dineInMenuIcon").click();

                } else if (event.shiftKey && event.keyCode == 75) { // shiftKey + K  to Open DineIn Menu
                    event.preventDefault();
                    $(".kioskMenuIcon").click();

                } else if (event.shiftKey && event.keyCode == 72) { // shiftKey + H  to Open DineIn Menu
                    event.preventDefault();
                    $(".HDMenuIcon").click();

                } else if (event.shiftKey && event.keyCode == 82) { // shiftKey + R  to Open DineIn Menu
                    event.preventDefault();
                    $(".iconTime").click();

                }
            }


        }
        else if (location.indexOf("additemFM3.html") > -1) {


            if (localStorage.POTGpopupflag == "false" && localStorage.POTGbottomMenuflag == "false") {

                if (!localStorage.POTGorderIdForEdit) {
                    var dineIn = $('#dine').attr('checked');
                    var kiosk = $('#kos').attr('checked');
                    var HD = $('#homed').attr('checked');
                    var dineInFocus = $('#dine').closest('div').hasClass('radiobuttonfocus');
                    var kioskFocus = $('#kos').closest('div').hasClass('radiobuttonfocus');
                    var HDFocus = $('#homed').closest('div').hasClass('radiobuttonfocus');
                    /* mode visiblity init 20/11/2015 START */
                    var dineInIsVisible = $('#dine').is(':visible');
                    var kioskIsVisible = $('#kos').is(':visible');
                    var homeDelIsVisible = $('#homed').is(':visible');
                    /* mode visiblity init 20/11/2015 END */


                    if (!dineIn && !kiosk && !HD) {
                        //check for focus


                        if (!dineInFocus && !kioskFocus && !HDFocus) {
                            // default selection if nothing is selected
                            if (event.keyCode == 37 || event.keyCode == 39) {

                                if (dineInIsVisible)
                                {
                                    $('#dine').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (kioskIsVisible)
                                {
                                    $('#kos').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (homeDelIsVisible)
                                {
                                    $('#homed').closest('div').addClass('radiobuttonfocus');
                                }



                                //$('#dine').closest('div').addClass('radiobuttonfocus');
                                //$('#dine').click(); 
                                //$('#dine').focus();

                            }


                        }
                        else if (dineInFocus) {
                            if (event.keyCode == 39) {

                                if (kioskIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#kos').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (homeDelIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#homed').closest('div').addClass('radiobuttonfocus');
                                }
                                /* orignal START */
                                /*							$('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                 $('#kos').closest('div').addClass('radiobuttonfocus');
                                 */
                                /* orignal END */

                            }
                            else if (event.keyCode == 37) {


                                if (homeDelIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#homed').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (kioskIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#kos').closest('div').addClass('radiobuttonfocus');
                                }

                                /*
                                 $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                 
                                 $('#homed').closest('div').addClass('radiobuttonfocus');
                                 */

                            }
                            else if (event.keyCode == 13) {
                                $('#dine').click();
                                if (!FASTCHECKIN) {
                                    $('#cust_radio_guest').click();
                                    $('#cust_radio_guest').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = true;
                                }
                            }


                        }
                        else if (kioskFocus) {
                            if (event.keyCode == 39) {

                                if (homeDelIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#homed').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (dineInIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#dine').closest('div').addClass('radiobuttonfocus');
                                }

                                /*								
                                 $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                 
                                 $('#homed').closest('div').addClass('radiobuttonfocus');
                                 */

                            }
                            else if (event.keyCode == 37) {

                                if (dineInIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#dine').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (homeDelIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#homed').closest('div').addClass('radiobuttonfocus');
                                }

                                /*
                                 $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                 
                                 $('#dine').closest('div').addClass('radiobuttonfocus');
                                 */

                            }
                            else if (event.keyCode == 13) {
                                $('#kos').click();
                                if (LOYALTY_FLAG) {
                                    $('#cust_radio_guest1').click();
                                    $('#cust_radio_guest1').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = true;
                                }

                            }

                        }
                        else if (HDFocus) {
                            if (event.keyCode == 39) {

                                if (dineInIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#dine').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (kioskIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#kos').closest('div').addClass('radiobuttonfocus');
                                }

                                /*								
                                 $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                 
                                 $('#dine').closest('div').addClass('radiobuttonfocus');
                                 */

                            }
                            else if (event.keyCode == 37) {

                                if (kioskIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#kos').closest('div').addClass('radiobuttonfocus');
                                }
                                else if (dineInIsVisible)
                                {
                                    $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    $('#dine').closest('div').addClass('radiobuttonfocus');
                                }

                                /*								
                                 $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                 
                                 $('#kos').closest('div').addClass('radiobuttonfocus');
                                 */

                            }
                            else if (event.keyCode == 13) {
                                $('#homed').click();

                                $('#cust_radio_existing2').click();
                                $('#cust_radio_existing2').focus();
                                localStorage.POTGitemsPanel = false;
                                localStorage.POTGcustPanel = true;

                            }

                        }

                    }
                    else if (localStorage.POTGitemsPanel == "false") {

                        if (dineIn) {
                            if (FASTCHECKIN) {
                                if (event.keyCode == 13) { //ENTER
                                    //$('#cust_radio_guest').click();

                                    /* added on 14/10/2015 START */
                                    var cust_numofguest = $("#dine1").find("#cust_guest");
                                    var cust_tableno = $("#dine1").find("#cust_tableno");
                                    var cust_captin = $("#dine1").find("#cust_captin");
                                    var ordersource = $("#dine1").find("#ordersource");


                                    if ($(cust_numofguest).is(':focus')) {
                                        $('#saveCustDineIn').click();
                                    } else if ($(cust_tableno).is(':focus')) {
                                        $(cust_tableno).blur();
                                        $('#saveCustDineIn').click();
                                        //$(cust_captin).focus();
                                        //openSelect(cust_captin);
                                    } else if ($(cust_captin).is(':focus')) {
                                        $('#saveCustDineIn').click();
                                    } else if ($(ordersource).is(':focus')) {
                                        $('#saveCustDineIn').click();
                                    }
                                    /* added on 14/10/2015 END */


                                }


                            } else {

                                if (localStorage.POTGcustPanel == "false") {

                                    if (event.keyCode == 13) { //ENTER
                                        $('#cust_radio_guest').click();
                                        $('#cust_radio_guest').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = true;


                                    }
                                } else if (localStorage.POTGcustPanel == "true") {

                                    var guest = $("#dine1").find("#cust_radio_guest").attr("checked");
                                    var isNew = $("#dine1").find("#cust_radio_new").attr("checked");
                                    var isexisting = $("#dine1").find("#cust_radio_existing").attr("checked");

                                    var isGuestFocus = $("#dine1").find("#cust_radio_guest").is(':focus');
                                    var isNewFocus = $("#dine1").find("#cust_radio_new").is(':focus');
                                    var isexistingFocus = $("#dine1").find("#cust_radio_existing").is(':focus');
                                    var cust;
                                    if (isGuestFocus || isNewFocus || isexistingFocus) {
                                        cust = true;
                                    } else {
                                        cust = false;
                                    }

                                    if (cust && event.keyCode == 27) //ESC
                                    {
                                        $('#dine').click();
                                        $('#dine').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = false;


                                    } else if (cust && event.keyCode == 13) //Enter
                                    { //initial focus on ENTER 
                                        if (guest) {
                                            $("#dine1").find("#cust_guest").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');


                                        } else if (isNew) {
                                            $("#dine1").find("#cust_fname").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        } else if (isexisting) {
                                            $("#dine1").find("#cust_name").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        }

                                    } else if (!cust && event.keyCode == 13) //Enter
                                    {
                                        //$('#cust_radio_guest').click();
                                        //$('#cust_radio_guest').focus();
                                        //localStorage.POTGitemsPanel = false;
                                        //localStorage.POTGcustPanel = true;

                                        if (guest) {

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                if ($(cust_tableno).is(':disabled')) {
                                                    $(cust_captin).focus();
                                                    openSelect(cust_captin);

                                                } else {
                                                    $(cust_tableno).focus();
                                                }

                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustDineIn').click();
                                            } else {
                                                $(cust_numofguest).focus();

                                            }






                                        } else if (isNew) {


                                            var cust_fname = $("#dine1").find("#cust_fname");
                                            var cust_lname = $("#dine1").find("#cust_lname");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                if ($(cust_tableno).is(':disabled')) {
                                                    $(cust_captin).focus();
                                                    openSelect(cust_captin);

                                                } else {
                                                    $(cust_tableno).focus();
                                                }

                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustDineIn').click();
                                            } else {
                                                $(cust_fname).focus();

                                            }


                                        } else if (isexisting) {
                                            var cust_name = $("#dine1").find("#cust_name");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                if ($(cust_tableno).is(':disabled')) {
                                                    $(cust_captin).focus();
                                                    openSelect(cust_captin);

                                                } else {
                                                    $(cust_tableno).focus();
                                                }

                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustDineIn').click();
                                            } else {
                                                $(cust_name).focus();

                                            }




                                        }



                                    } else if (!cust && event.keyCode == 27) //ESC
                                    {
                                        if (guest) {

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $("#dine1").find("#cust_radio_guest").click();
                                                $("#dine1").find("#cust_radio_guest").focus();

                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                if ($(cust_tableno).is(':disabled')) {
                                                    $(cust_numofguest).focus();
                                                } else {
                                                    $(cust_tableno).focus();
                                                }

                                            } else if ($(ordersource).is(':focus')) {
                                                //event.which =13;
                                                //event.keyCode =13;
                                                //$(cust_captin).trigger(event);
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }






                                        } else if (isNew) {
                                            var cust_fname = $("#dine1").find("#cust_fname");
                                            var cust_lname = $("#dine1").find("#cust_lname");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $("#dine1").find("#cust_radio_new").click();
                                                $("#dine1").find("#cust_radio_new").focus();

                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_fname).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                if ($(cust_tableno).is(':disabled')) {
                                                    $(cust_numofguest).focus();
                                                } else {
                                                    $(cust_tableno).focus();
                                                }

                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }





                                        } else if (isexisting) {
                                            var cust_name = $("#dine1").find("#cust_name");
                                            var cust_phone = $("#dine1").find("#cust_phone");

                                            var cust_numofguest = $("#dine1").find("#cust_guest");
                                            var cust_tableno = $("#dine1").find("#cust_tableno");
                                            var cust_captin = $("#dine1").find("#cust_captin");
                                            var ordersource = $("#dine1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $("#dine1").find("#cust_radio_existing").click();
                                                $("#dine1").find("#cust_radio_existing").focus();

                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_name).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_tableno).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                if ($(cust_tableno).is(':disabled')) {
                                                    $(cust_numofguest).focus();
                                                } else {
                                                    $(cust_tableno).focus();
                                                }

                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }




                                        }





                                    }
                                }

                            }

                        }
                        else if (kiosk) {
                            /*
                             if(localStorage.POTGcustPanel == "false")
                             {						
                             
                             if (event.keyCode == 13) {
                             $('#cust_radio_guest1').click();
                             $('#cust_radio_guest1').focus();
                             localStorage.POTGitemsPanel = false;
                             localStorage.POTGcustPanel = true;
                             
                             }
                             }
                             else if(localStorage.POTGcustPanel == "true")
                             {
                             
                             if(event.keyCode == 27)
                             {
                             $('#kos').click(); 
                             $('#kos').focus();
                             localStorage.POTGitemsPanel = false;
                             localStorage.POTGcustPanel = false;
                             
                             }
                             }
                             */

                            if (LOYALTY_FLAG) {
                                if (localStorage.POTGcustPanel == "false") {

                                    if (event.keyCode == 13) { //ENTER
                                        $('#cust_radio_guest1').click();
                                        $('#cust_radio_guest1').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = true;


                                    }
                                } else if (localStorage.POTGcustPanel == "true") {

                                    var guest = $("#kos1").find("#cust_radio_guest1").attr("checked");
                                    var isNew = $("#kos1").find("#cust_radio_new1").attr("checked");
                                    var isexisting = $("#kos1").find("#cust_radio_existing1").attr("checked");

                                    var isGuestFocus = $("#kos1").find("#cust_radio_guest1").is(':focus');
                                    var isNewFocus = $("#kos1").find("#cust_radio_new1").is(':focus');
                                    var isexistingFocus = $("#kos1").find("#cust_radio_existing1").is(':focus');
                                    var cust;
                                    if (isGuestFocus || isNewFocus || isexistingFocus) {
                                        cust = true;
                                    } else {
                                        cust = false;
                                    }

                                    if (cust && event.keyCode == 27) //ESC
                                    {
                                        $('#kos').click();
                                        $('#kos').focus();
                                        localStorage.POTGitemsPanel = false;
                                        localStorage.POTGcustPanel = false;


                                    } else if (cust && event.keyCode == 13) //Enter
                                    { //initial focus on ENTER 
                                        if (guest) {
                                            $("#kos1").find("#cust_guest").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');


                                        } else if (isNew) {
                                            $("#kos1").find("#cust_fname").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        } else if (isexisting) {
                                            $("#kos1").find("#cust_name").focus();
                                            $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                        }

                                    } else if (!cust && event.keyCode == 13) //Enter
                                    {
                                        //$('#cust_radio_guest').click();
                                        //$('#cust_radio_guest').focus();
                                        //localStorage.POTGitemsPanel = false;
                                        //localStorage.POTGcustPanel = true;

                                        if (guest) {

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $(cust_captin).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustKiosk').click();
                                            } else {
                                                $(cust_numofguest).focus();

                                            }






                                        } else if (isNew) {


                                            var cust_fname = $("#kos1").find("#cust_fname");
                                            var cust_lname = $("#kos1").find("#cust_lname");
                                            var cust_phone = $("#kos1").find("#cust_phone");

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_captin).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustKiosk').click();
                                            } else {
                                                $(cust_fname).focus();

                                            }


                                        } else if (isexisting) {
                                            var cust_name = $("#kos1").find("#cust_name");
                                            var cust_phone = $("#kos1").find("#cust_phone");
                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_captin).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(ordersource).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $('#saveCustKiosk').click();
                                            } else {
                                                $(cust_name).focus();

                                            }




                                        }



                                    } else if (!cust && event.keyCode == 27) //ESC
                                    {
                                        if (guest) {

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");


                                            if ($(cust_numofguest).is(':focus')) {
                                                $("#kos1").find("#cust_radio_guest1").click();
                                                $("#kos1").find("#cust_radio_guest1").focus();

                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                //event.which =13;
                                                //event.keyCode =13;
                                                //$(cust_captin).trigger(event);
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }






                                        } else if (isNew) {
                                            var cust_fname = $("#kos1").find("#cust_fname");
                                            var cust_lname = $("#kos1").find("#cust_lname");
                                            var cust_phone = $("#kos1").find("#cust_phone");

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_fname).is(':focus')) {
                                                $("#kos1").find("#cust_radio_new1").click();
                                                $("#kos1").find("#cust_radio_new1").focus();

                                            } else if ($(cust_lname).is(':focus')) {
                                                $(cust_fname).focus();
                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_lname).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }





                                        } else if (isexisting) {
                                            var cust_name = $("#kos1").find("#cust_name");
                                            var cust_phone = $("#kos1").find("#cust_phone");

                                            var cust_numofguest = $("#kos1").find("#cust_guest");
                                            var cust_captin = $("#kos1").find("#cust_captin");
                                            var ordersource = $("#kos1").find("#ordersource");

                                            if ($(cust_name).is(':focus')) {
                                                $("#kos1").find("#cust_radio_existing1").click();
                                                $("#kos1").find("#cust_radio_existing1").focus();

                                            } else if ($(cust_phone).is(':focus')) {
                                                $(cust_name).focus();
                                            } else if ($(cust_numofguest).is(':focus')) {
                                                $(cust_phone).focus();
                                            } else if ($(cust_captin).is(':focus')) {
                                                $(cust_numofguest).focus();
                                            } else if ($(ordersource).is(':focus')) {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }




                                        }





                                    }
                                }
                            }

                        }
                        else if (HD) {

                            if (localStorage.POTGcustPanel == "false") {

                                if (event.keyCode == 13) { //ENTER
                                    $('#cust_radio_existing2').click();
                                    $('#cust_radio_existing2').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = true;


                                }
                            } else if (localStorage.POTGcustPanel == "true") {


                                var isNew = $("#homed1").find("#cust_radio_new2").attr("checked");
                                var isexisting = $("#homed1").find("#cust_radio_existing2").attr("checked");


                                var isNewFocus = $("#homed1").find("#cust_radio_new2").is(':focus');
                                var isexistingFocus = $("#homed1").find("#cust_radio_existing2").is(':focus');
                                var cust;
                                if (isNewFocus || isexistingFocus) {
                                    cust = true;
                                } else {
                                    cust = false;
                                }

                                if (cust && event.keyCode == 27) //ESC
                                {
                                    $('#homed').click();
                                    $('#homed').focus();
                                    localStorage.POTGitemsPanel = false;
                                    localStorage.POTGcustPanel = false;


                                } else if (cust && event.keyCode == 13) //Enter
                                { //initial focus on ENTER 

                                    if (isNew) {
                                        $("#homed1").find("#cust_fname").focus();
                                        $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    } else if (isexisting) {
                                        $("#homed1").find("#cust_name").focus();
                                        $('.radiobuttonfocus').removeClass('radiobuttonfocus');
                                    }

                                } else if (!cust && event.keyCode == 13) //Enter
                                {
                                    //$('#cust_radio_guest').click();
                                    //$('#cust_radio_guest').focus();
                                    //localStorage.POTGitemsPanel = false;
                                    //localStorage.POTGcustPanel = true; 


                                    if (isNew) {


                                        var cust_fname = $("#homed1").find("#cust_fname");
                                        var cust_lname = $("#homed1").find("#cust_lname");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;

                                        if ($(cust_fname).is(':focus')) {
                                            $(cust_lname).focus();
                                        } else if ($(cust_lname).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_houseno).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_street).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_captin).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                            } else {
                                                $(ordersource).focus();
                                            }



                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(ordersource).focus();
                                        } else if ($(ordersource).is(':focus')) {
                                            $('#saveCustHomeDel').click();
                                        } else {
                                            $(cust_fname).focus();

                                        }


                                    } else if (isexisting) {


                                        var cust_name = $("#homed1").find("#cust_name");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;

                                        if ($(cust_name).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_houseno).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_street).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_captin).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                            } else {
                                                $(ordersource).focus();
                                            }



                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(ordersource).focus();
                                        } else if ($(ordersource).is(':focus')) {
                                            $('#saveCustHomeDel').click();
                                        } else {
                                            $(cust_name).focus();

                                        }




                                    }



                                } else if (!cust && event.keyCode == 27) //ESC
                                {

                                    if (isNew) {


                                        var cust_fname = $("#homed1").find("#cust_fname");
                                        var cust_lname = $("#homed1").find("#cust_lname");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;




                                        if ($(cust_fname).is(':focus')) {
                                            $("#homed1").find("#cust_radio_new2").click();
                                            $("#homed1").find("#cust_radio_new2").focus();
                                        } else if ($(cust_lname).is(':focus')) {
                                            $(cust_fname).focus();
                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_lname).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_houseno).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            $(cust_street).focus();

                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(cust_captin).focus();
                                            openSelect(cust_captin);
                                        } else if ($(ordersource).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                                openSelect(home_cust_area);
                                            } else {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }


                                        }


                                    } else if (isexisting) {


                                        var cust_name = $("#homed1").find("#cust_name");
                                        var cust_phone = $("#homed1").find("#cust_phone");

                                        var cust_houseno = $("#homed1").find("#cust_houseno");
                                        var cust_street = $("#homed1").find("#cust_street");
                                        var cust_captin = $("#homed1").find("#cust_captin");
                                        var home_cust_area = $("#homed1").find("#home_cust_area");
                                        var ordersource = $("#homed1").find("#ordersource");
                                        var isAreaAvail = $('#home_cust_area').length > 0;



                                        if ($(cust_name).is(':focus')) {
                                            $("#homed1").find("#cust_radio_existing2").click();
                                            $("#homed1").find("#cust_radio_existing2").focus();

                                        } else if ($(cust_phone).is(':focus')) {
                                            $(cust_name).focus();
                                        } else if ($(cust_houseno).is(':focus')) {
                                            $(cust_phone).focus();
                                        } else if ($(cust_street).is(':focus')) {
                                            $(cust_houseno).focus();

                                        } else if ($(cust_captin).is(':focus')) {
                                            $(cust_street).focus();


                                        } else if ($(home_cust_area).is(':focus')) {
                                            $(cust_captin).focus();
                                            openSelect(cust_captin);
                                        } else if ($(ordersource).is(':focus')) {
                                            if (isAreaAvail) {
                                                $(home_cust_area).focus();
                                                openSelect(home_cust_area);
                                            } else {
                                                $(cust_captin).focus();
                                                openSelect(cust_captin);
                                            }

                                        }




                                    }






                                }
                            }



                        }

                    }

                }






                if ($("#itm_qty").is(':focus')) {
                    //console.log($("#itm_qty").is(':focus'));
                    if (event.keyCode == 13) {
                        $("#itm_amt").focus();
                        $("#itm_amt").select();

                    } else if (event.keyCode == 27) {
                        $("#itm_name").focus();

                    } else if (event.keyCode == 110 || event.keyCode == 190) {
                        //event.preventDefault(); //commented to enable decimal point
                        //return false;
                    }

                }
                else if ($("#itm_amt").is(':focus')) {
                    if (event.keyCode == 13) {

                        $("#itm_qty").blur();
                        $("#itm_qty").focusout();
                        $("#additm").click();

                    }
                    else if (event.keyCode == 27) {
                        $("#itm_qty").focus();

                    }

                }
                else if ($("#itm_name").is(':focus')) {
                    if (event.keyCode == 13) {
                        var itm_name = $("#itm_name").val();
                        if (itm_name) {
                            $("#itm_qty").focus();
                            $("#itm_qty").select();
                        }

                    } else if (event.altKey) {
                        $("#itm_name").focusout();
                        $("#itm_name").blur();
                        altkeyFM3(event);

                    }
                }




            }







            if ($("#popupBoxDelete").is(":visible") == true) {
                if (event.keyCode == 13) {
                    var kot_voidnc = $("#popupBoxDelete").find("#kot_voidnc");
                    /*code added by sachin jani 16/07/2015 */
                    if ($(kot_voidnc).is(':focus')) {
                        //$("#closeDelete").click(); //orignal
                        $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click(); //modified 29/07/2015
                        closeStaticPopup("popupBoxDelete");
                    } else {
                        $(kot_voidnc).focus();
                    }


                    /* Code added by sachin jani over */

                    /*
                     //Follwing is the original code.
                     
                     //takeActionOnDeleteItem();
                     $("#closeDelete").click();
                     //closeStaticPopup("popupBoxDelete"); */
                } else if (event.keyCode == 27) {
                    deSelectAll('selecteditem');
                    closeStaticPopup("popupBoxDelete");

                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $("#popupBoxDelete .iconRefresh").click();
                    $("#kot_voidnc").blur();
                }

                if (localStorage.POTGpopupflag == "true") {
                    //$("#searchtag").blur();
                    //$("#searchtag").focusout();
                }
            }
            else if ($("#billingpopupBoxDelete").is(":visible") == true) {
                var inward = $("#billingpopupBoxDelete").find('input#inwardBillBC').attr('checked'); //inward CHECKED
                var wastage = $("#billingpopupBoxDelete").find('input#wastageBillBC').attr('checked'); //wastage CHECKED

                var bill_voidnc = $("#billingpopupBoxDelete").find("#bill_voidnc");

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    event.preventDefault();
                    $(bill_voidnc).blur();

                    if (inward == 'checked') {
                        $("#billingpopupBoxDelete").find('input#wastageBillBC').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#billingpopupBoxDelete").find('input#inwardBillBC').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#billingpopupBoxDelete").find('input#inwardBillBC').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    event.preventDefault();
                    $(bill_voidnc).blur();

                    if (inward == 'checked') {
                        $("#billingpopupBoxDelete").find('input#wastageBillBC').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#billingpopupBoxDelete").find('input#inwardBillBC').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#billingpopupBoxDelete").find('input#wastageBillBC').click();
                    }
                }
                else if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(bill_voidnc).is(':focus')) {
                        $("#billingpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(bill_voidnc).focus();
                    }


                }
                else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("billingpopupBoxDelete");
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#billingpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(bill_voidnc).blur();
                }


            }
            else if ($("#pendingOrderpopupBoxDelete").is(":visible") == true) {

                var inward = $("#pendingOrderpopupBoxDelete").find('input#inwardBillOC').attr('checked'); //inward CHECKED
                var wastage = $("#pendingOrderpopupBoxDelete").find('input#wastageBillOC').attr('checked'); //wastage CHECKED

                var order_voidnc = $("#pendingOrderpopupBoxDelete").find("#order_voidnc");

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    $(order_voidnc).blur();

                    if (inward == 'checked') {
                        $("#pendingOrderpopupBoxDelete").find('input#wastageBillOC').click();

                        //$("#order_voidnc").blur();
                    }
                    else if (wastage == 'checked') {
                        $("#pendingOrderpopupBoxDelete").find('input#inwardBillOC').click();
                        //$("#order_voidnc").blur();
                    }
                    else {
                        $("#pendingOrderpopupBoxDelete").find('input#inwardBillOC').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    $(order_voidnc).blur();

                    if (inward == 'checked') {
                        $("#pendingOrderpopupBoxDelete").find('input#wastageBillOC').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#pendingOrderpopupBoxDelete").find('input#inwardBillOC').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#pendingOrderpopupBoxDelete").find('input#wastageBillOC').click();
                    }
                }
                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(order_voidnc).is(':focus')) {
                        $("#pendingOrderpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(order_voidnc).focus();
                    }


                }
                else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("pendingOrderpopupBoxDelete");
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#pendingOrderpopupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(order_voidnc).blur();
                }


            }
            else if ($("#deliverBox").is(":visible") == true) {

                var deliveryboy = $("#deliverBox").find("#deliveryboy");

                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(deliveryboy).is(':focus')) {
                        $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(deliveryboy).focus();
                    }


                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("deliverBox");
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(deliveryboy).blur();
                }


            }
            else if ($("#printBill").is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $("#printBill").find(".ez-radio").find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $("#printBill").find(".radioButton").find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $("#printBill").find(".radioButton").find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                /* happy hours keycode added on 03/11/2015*/
                var hpyhrs = $('#printBill').find('#chkHappyHours').attr('checked');
                var coupn = $('#printBill #discountypeCoupen').attr('checked');
                var hpyhrsIsVisible = $('#printBill').find('#chkHappyHours').is(':visible');


                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    }
                    else if (Amt == 'checked') {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                            $("#billremark").focus();

                        }

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {

                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill #discountypeCoupen').click();
                            $("#discounttypevalue").focus();

                        }

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }
                    else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }

                }
                else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {


                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();

                        } else {
                            $("#billremark").focus();
                        }




                    }
                    else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    }
                    else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else if (coupn == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");							
                        } else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else {
                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15
                    }
                }
                else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();

                }
                else if (event.altKey && event.keyCode == 69) //E
                {
                    event.preventDefault();
                    if ($("#packingCharges").is(':focus')) {
                        if ($('#delC').is(':visible') == true) {

                            $("#deliveryCharges").focus();
                        } else {
                            $("#packingCharges").focus();
                        }

                    }
                    else if ($("#deliveryCharges").is(':focus')) {
                        $("#packingCharges").focus();

                    }
                    else
                    {
                        $("#packingCharges").focus();

                    }


                }
                else if (event.altKey && event.keyCode == 73) //I
                {
                    event.preventDefault();
                    var inwardPB = $("#printBill").find("input#inwardBillPB");
                    var wastagePB = $("#printBill").find("input#wastageBillPB");
                    var inwardPBChk = $(inwardPB).attr('checked');
                    var wastagePBChk = $(wastagePB).attr('checked');

                    if ($('#inwardWastageInPrintBill').is(':visible'))
                    {
                        if (inwardPBChk)
                        {
                            $(wastagePB).click();
                        }
                        else if (wastagePBChk)
                        {
                            $(inwardPB).click();
                        }
                        else
                        {
                            $(inwardPB).click();
                        }




                    }


                }


                if (localStorage.POTGpopupflag == "true") {
                    //$("#searchtag").blur();
                    $("#searchtag").focusout();
                }

                /* Code added by Sachin jani Over */






            }
            else if ($("#billPaymentBox").is(":visible") == true) { // IF PAYMENT POP UP OPEN
                //$(".price").focus();				
                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP					
                    //$("#saveDisable").parent().click();
                    $("#billPaymentBox").find(".closeRefreshIcons ").find('a').eq(0).click();
                    //closeStaticPopup("billPaymentBox");
                } else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    $("#billPaymentBox").find(".closeRefreshIcons ").find(".iconClose").click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#billPaymentBox").find(".closeRefreshIcons ").find('a').eq(1).click();

                }

            }
            else if ($("#permissionPasswordCancel").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordCancel").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordCancel").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if ($("#permissionPasswordEdit").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordEdit").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordEdit").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if ($("#permissionPasswordPrint").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordPrint").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordPrint").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#permissionPasswordCancel .iconRefresh").closest('a').click();
                    resetPremissionPopup();
                }






            }
            else if ($("#editItem").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#txtitemRemark").is(':focus')) {
                        event.preventDefault();
                        $("#editItem").find("#itmRemarkSave").click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#editItem").find('.iconClose').click();
                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#editItem").find('.iconRefresh').click();
                }



            }
            else if ($("#createingredient").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    //searchTable
                    if ($("#searchTable").is(':focus')) {

                        /*
                         $("#searchTable").data("ui-autocomplete")._trigger("change");
                         mergeTablesMenuList
                         if($("#mergeTablesMenuList").is(':visible') == true)
                         {
                         $('.addPrice').click();
                         }
                         */


                        $("#searchTable").blur();
                        if ($("#searchTable").val()) {
                            $('#createingredient .addPrice').click();
                            $("#searchTable").focus();
                        } else {

                            //$("#createingredient").find(".closeRefreshIcons ").find('a').eq(0).click();
                        }




                    } else {
                        $("#searchTable").focus();
                    }



                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#createingredient").find(".closeRefreshIcons ").find('a').eq(1).click();
                }
                else if (event.altKey && event.keyCode == 83) { //ALTR + S
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#createingredient").find(".closeRefreshIcons ").find('a').eq(0).click();
                }
                else if (event.keyCode == 27) { //ESC keypress
                    $("#createingredient").find(".closeRefreshIcons").find('a').eq(2).click();
                    closeStaticPopup("createingredient");

                }






            }
            else if ($("#transferTable").is(':visible') == true) {

                //activeShiftTable
                if (event.keyCode == 39) {
                    //event.preventDefault();

                    var activeli = $('.activeShiftTable');
                    var activeliNext = $('.activeShiftTable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $(activeliNext).addClass('activeShiftTable');
                    } else {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $('#tableList li').eq(0).addClass('activeShiftTable');
                    }


                } else if (event.keyCode == 37) {
                    //event.preventDefault();
                    var activeli = $('.activeShiftTable');
                    var activeliPrev = $('.activeShiftTable').prev('li');
                    var liLen = $('#tableList li').length;

                    if (activeliPrev.length > 0) {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $(activeliPrev).addClass('activeShiftTable');
                    } else {
                        $('.activeShiftTable').removeClass('activeShiftTable');
                        $('#tableList li').eq(liLen - 1).addClass('activeShiftTable');
                    }


                } else if (event.keyCode == 13) {
                    $('.activeShiftTable a').click();
                } else if (event.keyCode == 27) {
                    $('.activeShiftTable').removeClass('activeShiftTable');
                    $("#transferTable").find('.iconClose').click();

                }






            }
            else if ($("#splitTable").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED
                    $("#splitTable").find(".closeRefreshIcons ").find('a').eq(0).click();

                } else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    //$("#resetbtn").click();
                    $("#splitTable").find(".closeRefreshIcons ").find('a').eq(1).click();
                } else if (event.keyCode == 27) { //ESC keypress
                    $("#splitTable .iconClose").click();

                }






            }
            else if ($("#popupBoxInwardWastage").is(":visible") == true) {

                var inward = $("#popupBoxInwardWastage").find('input#inwardBill').attr('checked'); //inward CHECKED
                var wastage = $("#popupBoxInwardWastage").find('input#wastageBill').attr('checked'); //wastage CHECKED

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    event.preventDefault();

                    if (inward == 'checked') {
                        $("#popupBoxInwardWastage").find('input#wastageBill').click();


                    } else if (wastage == 'checked') {
                        $("#popupBoxInwardWastage").find('input#inwardBill').click();

                    } else {
                        $("#popupBoxInwardWastage").find('input#inwardBill').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    event.preventDefault();

                    if (inward == 'checked') {
                        $("#popupBoxInwardWastage").find('input#wastageBill').click();


                    } else if (wastage == 'checked') {
                        $("#popupBoxInwardWastage").find('input#inwardBill').click();

                    } else {
                        $("#popupBoxInwardWastage").find('input#wastageBill').click();
                    }
                }
                else if (event.keyCode == 27) { // ESC KEY PRESSED
                    $("#popupBoxInwardWastage .iconClose").click();
                }
                else if (event.keyCode == 13) { // ENTER KEY PRESSES		
                    $("#popupBoxInwardWastage .iconAdd").closest('a').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#popupBoxInwardWastage .iconRefresh").closest('a').click();
                }


            }
            else if ($("#pkgSelPopUp").is(":visible") == true) {

                if (event.keyCode == 27) { // ESC KEY PRESSED
                    $("#pkgSelPopUp .iconClose").click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#pkgSelPopUp .iconRefresh").click();

                }
                else if (event.altKey && event.keyCode == 83) { //ALTR + S
                    event.preventDefault();
                    $("#pkgSelPopUp #savePkg").click();
                    return false; //to prevent shift close popup from opening.

                }
                else if (event.keyCode == 13) { // ENTER KEY PRESSES
                    if ($("#package_qty").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#searchItemPkg").addClass('chkboxfocus');
                        $("#pkg_dropdown").focus();
                    }
                    else if ($("#pkg_dropdown").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#searchItemPkg").addClass('chkboxfocus');
                        $("#searchItemPkg").focus();
                    }
                    else if ($("#searchItemPkg").is(':focus')) {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#pkg_qty").addClass('chkboxfocus');
                        $("#pkg_qty").focus();
                        $("#pkg_qty").select();

                    }
                    else if ($("#pkg_qty").is(':focus')) {
                        $("#pkgSelPopUp #savePkgItm").click();
                        //$("#pkgSelPopUp #savePkgItm").blur();
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        $("#pkg_qty").blur();
                        $("#package_qty").focus();
                        $("#package_qty").select();
                        //$("#pkg_dropdown").focus();
                    }
                    else {
                        //$('.chkboxfocus').removeClass('chkboxfocus');
                        //$("#pkg_dropdown").addClass('chkboxfocus');
                        $("#package_qty").focus();
                        $("#package_qty").select();


                    }
                }




            }
            else if (event.shiftKey && event.keyCode == 84 && localStorage.POTGsideMenu == "false") {

                if (localStorage.POTGbottomMenuflag == "false") {
                    localStorage.POTGbottomMenuflag = true;
                    $('#bottomNavMenuData li.orderli').eq(0).addClass('activetable');
                    $('#bottomNavMenuData li.orderli').eq(0).focus();
                } else {
                    localStorage.POTGbottomMenuflag = false;
                    $('.activetable').removeClass('activetable');
                }

            }
            else if (localStorage.POTGbottomMenuflag == "true" && localStorage.POTGsideMenu == "false") {
                if (event.keyCode == 39) { //right
                    event.preventDefault();
                    $('#tooltip').remove();
                    //$('#dineInMenutxtscrn li').eq(0).prev('li');
                    var activeli = $('.activetable');
                    var activeliNext = $('.activetable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activetable').removeClass('activetable');
                        //var offset = $(activeliNext).offset().left;
                        btmMenuStepCnt += 40;
                        console.log("btmMenuStepCnt = " + btmMenuStepCnt)
                        //$('#bottomNavMenuData').scrollLeft(btmMenuStepCnt);
                        $(activeliNext).addClass('activetable');
                        $(activeliNext).focus();
                    } else {
                        btmMenuStepCnt = 0;
                        //$('#bottomNavMenuData').scrollLeft(0);
                        $('.activetable').removeClass('activetable');
                        $('#bottomNavMenuData li.orderli').eq(0).addClass('activetable');
                        $('#bottomNavMenuData li.orderli').eq(0).focus();

                    }


                }
                else if (event.shiftKey && event.keyCode == 9) {
                    $('#tooltip').remove();
                    //$('#dineInMenutxtscrn li').eq(0).prev('li');
                    var activeli = $('.activetable');
                    var activeliPrev = $('.activetable').prev('li');
                    var liLen = $('#bottomNavMenuData li.orderli').length;
                    var prevLiTabIndx = $('.activetable').prev('li').attr('tabindex');
                    if (activeliPrev.length > 0) {
                        $('.activetable').removeClass('activetable');
                        var offset = $(activeliPrev).offset().left;
                        //$('#bottomNavMenuData').scrollLeft(offset - 500);
                        $(activeliPrev).addClass('activetable');
                    }
                    else if (prevLiTabIndx == "2")
                    {
                        $('#bottomNavMenuData').scrollLeft(0);
                    }
                    else {
                        //event.preventDefault();
                        var offset = $('#bottomNavMenuData li.orderli').eq(liLen - 1).offset().left;
                        //$('#bottomNavMenuData').scrollLeft(offset);
                        $('.activetable').removeClass('activetable');
                        $('#bottomNavMenuData li.orderli').eq(liLen - 1).addClass('activetable');
                        $('.activetable').focus();
                    }
                }
                else if (event.keyCode == 9) { //tab
                    //event.preventDefault();
                    $('#tooltip').remove();
                    //$('#dineInMenutxtscrn li').eq(0).prev('li');
                    var activeli = $('.activetable');
                    var activeliNext = $('.activetable').next('li');


                    if (activeliNext.length > 0) {
                        $('.activetable').removeClass('activetable');
                        //var offset = $(activeliNext).offset().left;
                        btmMenuStepCnt += 40;
                        //console.log("btmMenuStepCnt = " +btmMenuStepCnt)
                        //$('#bottomNavMenuData').scrollLeft(btmMenuStepCnt);
                        $(activeliNext).addClass('activetable');
                    } else {
                        btmMenuStepCnt = 0;
                        $('#bottomNavMenuData').scrollLeft(0);
                        $('.activetable').removeClass('activetable');
                        $('#bottomNavMenuData li.orderli').eq(0).addClass('activetable');
                        $('.activetable').focus();

                    }


                }
                else if (event.keyCode == 37) { //left
                    event.preventDefault();
                    $('#tooltip').remove();
                    //$('#dineInMenutxtscrn li').eq(0).prev('li');
                    var activeli = $('.activetable');
                    var activeliPrev = $('.activetable').prev('li');
                    var liLen = $('#bottomNavMenuData li.orderli').length;

                    if (activeliPrev.length > 0) {
                        $('.activetable').removeClass('activetable');
                        var offset = $(activeliPrev).offset().left;
                        //$('#bottomNavMenuData').scrollLeft(offset - 500);
                        $(activeliPrev).addClass('activetable');
                        $(activeliPrev).focus();
                    } else {
                        var offset = $('#bottomNavMenuData li.orderli').eq(liLen - 1).offset().left;
                        //$('#bottomNavMenuData').scrollLeft(offset);
                        $('.activetable').removeClass('activetable');
                        $('#bottomNavMenuData li.orderli').eq(liLen - 1).addClass('activetable');
                        $('#bottomNavMenuData li.orderli').eq(liLen - 1).focus();
                    }


                }
                else if (event.keyCode == 13) { //Enter
                    event.preventDefault();
                    $('.activetable').find('a').eq(0).click();
                }
                else if (event.keyCode == 27) { //Esc
                    event.preventDefault();
                    var activeLen = $('.activetable').length;
                    if ($('#tooltip').is(':visible') == true) {
                        $('#tooltip').remove();

                    } else if (activeLen > 0) {
                        localStorage.POTGbottomMenuflag = false;
                        $('.activetable').removeClass('activetable');

                    }

                    //$('.dineInMenuIcon').click();
                }
                else if (event.altKey && event.keyCode == 67) //C
                {
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(0).click();

                    }

                }
                else if (event.altKey && event.keyCode == 69) //E
                {
                    event.preventDefault();
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(1).click();

                    }
                }
                else if (event.altKey && event.keyCode == 80) //P
                {
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(2).click();

                    }

                }
                else if (event.altKey && event.keyCode == 73) //I 
                {
                    //reason: alt+s is for shift close
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(3).click();

                    }

                }
                else if (event.altKey && event.keyCode == 84) //T
                {
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(4).click();

                    }

                }
                else if (event.altKey && event.keyCode == 88) //X
                {
                    if ($('#tooltip').is(':visible') == true) {

                        $('.activetable').find('.tableBox li a').eq(5).click();

                    }

                }






            }
            else {
                if (localStorage.POTGpopupflag == "false" && localStorage.POTGbottomMenuflag == "false") {
                    altkeyFM3(event);
                    shiftkeyFM3(event);
                    tabkeyFM3(event);
                }
            }





        }
        else if (location.indexOf("futureorder.html") > -1) { // IF futureorder.html PAGE


            /*	Following Code added by sachin jani 15/07/2015 */

            if ($("#popupBoxDelete").is(":visible") == true) {
                var vod = $("#popupBoxDelete").find(".radioButton").find('input#deletevoid').attr('checked'); //Void CHECKDED
                var cancel = $("#popupBoxDelete").find(".radioButton").find('input#deletecancel').attr('checked'); //Cancel CHECKED
                var inward = $("#popupBoxDelete").find('input#inwardBill').attr('checked'); //inward CHECKED
                var wastage = $("#popupBoxDelete").find('input#wastageBill').attr('checked'); //wastage CHECKED

                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    event.preventDefault();
                    $("#order_voidnc").blur();
                    if (inward == 'checked') {
                        $("#popupBoxDelete").find('input#wastageBill').click();

                        //$("#order_voidnc").blur();
                    } else if (wastage == 'checked') {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                        //$("#order_voidnc").blur();
                    } else {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED
                    event.preventDefault();
                    $("#order_voidnc").blur();

                    if (inward == 'checked') {
                        $("#popupBoxDelete").find('input#wastageBill').click();

                        //$("#order_voidnc").blur();
                    }
                    else if (wastage == 'checked') {
                        $("#popupBoxDelete").find('input#inwardBill').click();
                        //$("#order_voidnc").blur();
                    }
                    else {
                        $("#popupBoxDelete").find('input#wastageBill').click();
                    }
                }
                else if (event.keyCode == 13) // ENTER KEY PRESSES
                {


                    if (vod == 'checked') {
                        if ($("#order_voidnc").is(':focus')) {

                            console.log("on Enter" + $("#order_voidnc").is(':focus'));
                            $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();
                            $("#order_voidnc").blur();
                            console.log($("#order_voidnc").is(':focus'));
                            //$("#closebtn").click();
                        } else {
                            $("#order_voidnc").focus();
                        }

                    }
                    else if (cancel == 'checked') {
                        if ($("#order_voidnc").is(':focus')) {
                            console.log("on Enter" + $("#order_voidnc").is(':focus'));

                            $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(0).click();
                            $("#order_voidnc").blur();
                            console.log("after Enter: blur" + $("#order_voidnc").is(':focus'));
                            //$("#closebtn").click();
                        } else {
                            $("#order_voidnc").focus();
                            console.log("on Enter: Else" + $("#order_voidnc").is(':focus'));
                        }


                    }






                }
                else if (event.keyCode == 27) // ESC KEY PRESSES
                {

                    closeStaticPopup("popupBoxDelete");

                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#popupBoxDelete").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $("#order_voidnc").blur();
                }


            }
            else if ($("#printBill").is(':visible') == true) { //IF PRINTBILL POPUP IS OPEN


                var NC = $("#printBill").find(".ez-radio").find('input#discounttypeNC').attr('checked'); //Non-chargeable CHECKDE
                var Per = $("#printBill").find(".radioButton").find('input#discounttyprper').attr('checked'); // PER RADIO CHECKED

                var Amt = $("#printBill").find(".radioButton").find('input#discounttypramt').attr('checked');
                // AMt RADIO CHECKED
                /* happy hours keycode added on 03/11/2015*/
                var hpyhrs = $('#printBill').find('#chkHappyHours').attr('checked');
                var coupn = $('#printBill #discountypeCoupen').attr('checked');
                var hpyhrsIsVisible = $('#printBill').find('#chkHappyHours').is(':visible');


                if (event.keyCode == 39) { //IF NEXT ARROW KEY PRESSED
                    if (NC == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();


                    }
                    else if (Amt == 'checked') {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                            $("#billremark").focus();

                        }

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill').find('.ez-radio').find('input#discounttypeNC').click();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }

                }
                else if (event.keyCode == 37) { // IF BACK ARROW KEY PRESSED					
                    if (NC == 'checked') {

                        if (hpyhrsIsVisible)
                        {
                            $('#printBill').find('#chkHappyHours').click();
                        }
                        else
                        {
                            $('#printBill #discountypeCoupen').click();
                            $("#discounttypevalue").focus();

                        }

                    }
                    else if (Per == 'checked') {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();

                    }
                    else if (Amt == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttyprper').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (coupn == 'checked') {
                        $("#printBill").find(".radioButton").find('input#discounttypramt').click();
                        $("#discounttypevalue").focus();

                    }
                    else if (hpyhrs == 'checked')
                    {
                        $('#printBill #discountypeCoupen').click();
                        $("#discounttypevalue").focus();
                    }
                    else {
                        $("#printBill").find(".ez-radio").find('input#discounttypeNC').click();
                        $("#billremark").focus();
                    }
                }
                else if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if (NC == 'checked') {
                        if ($("#billremark").is(':focus')) {


                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();

                        } else {
                            $("#billremark").focus();
                        }




                    }
                    else if (Per == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        } else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        } else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");
                        } else {
                            $("#discounttypevalue").focus();
                        }




                    }
                    else if (Amt == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        }
                        else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");
                        }
                        else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else if (coupn == 'checked') {

                        if ($("#discounttypevalue").is(':focus')) {
                            if ($('#pkingC').is(':visible') == true) {

                                $("#packingCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#packingCharges").is(':focus')) {
                            if ($('#delC').is(':visible') == true) {

                                $("#deliveryCharges").focus();
                            } else {
                                $("#billremark").focus();
                            }

                        }
                        else if ($("#deliveryCharges").is(':focus')) {
                            $("#billremark").focus();

                        }
                        else if ($("#billremark").is(':focus')) {
                            $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click();
                            //closeStaticPopup("printBill");
                        }
                        else {
                            $("#discounttypevalue").focus();
                        }

                    }
                    else {
                        $("#printBill").find(".closeRefreshIcons ").find('a').eq(0).click(); //if nothing is checked then print bill. 03/08/15
                    }
                }
                else if (event.keyCode == 27) {
                    $("#printBill").find('.iconClose').click();
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#printBill").find(".closeRefreshIcons ").find('a').eq(1).click();

                }
                else if (event.altKey && event.keyCode == 69) //E
                {
                    event.preventDefault();
                    if ($("#packingCharges").is(':focus')) {
                        if ($('#delC').is(':visible') == true) {

                            $("#deliveryCharges").focus();
                        } else {
                            $("#packingCharges").focus();
                        }

                    }
                    else if ($("#deliveryCharges").is(':focus')) {
                        $("#packingCharges").focus();

                    }
                    else
                    {
                        $("#packingCharges").focus();

                    }


                }


                /* Code added by Sachin jani Over */






            }
            else if ($("#permissionPasswordCancel").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {

                        $("#permissionPasswordCancel").find(".closeRefreshIcons ").find('a').eq(0).click();

                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordCancel").find('.iconClose').click();
                }






            }
            else if ($("#permissionPasswordEdit").is(':visible') == true) {

                if (event.keyCode == 13) { // IF ENTER KEY PRESSED

                    if ($("#usrPwd").is(':focus')) {
                        $("#permissionPasswordEdit").find(".closeRefreshIcons ").find('a').eq(0).click();
                    }

                } else if (event.keyCode == 27) { //ESC keypress
                    $("#permissionPasswordEdit").find('.iconClose').click();
                }






            }
            else if ($("#deliverBox").is(":visible") == true) {

                var deliveryboy = $("#deliverBox").find("#deliveryboy");

                if (event.keyCode == 13 && localStorage.POTGsideMenu == "false") { // IF ENTER PRESSED WHEN PAYMENT POPUP
                    if ($(deliveryboy).is(':focus')) {
                        $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(0).click();

                    } else {
                        $(deliveryboy).focus();
                    }


                }
                else if (event.keyCode == 27 && localStorage.POTGsideMenu == "false") { // IF ESC PRESSED
                    closeStaticPopup("deliverBox");
                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $("#deliverBox").find(".closeRefreshIcons ").find('a').eq(1).click();
                    $(deliveryboy).blur();
                }


            }
            else if ($("#futureOrderCustLoginPopUp").is(":visible") == true) {
                /* text boxes START */
                var fDate = $('#futureOrderDateTimePickr');
                var fname = $('#cust_fname');
                var lname = $('#cust_lname');
                var name = $('#cust_name');
                var phone = $('#cust_phone');
                var noofguest = $('#cust_numofguest');
                var captain = $('#cust_captionassigned');
                var tableno = $('#cust_tableno');
                var houseno = $('#cust_houseno');
                var street = $('#cust_streetno');
                var area = $('#home_cust_area');
                var ordersource = $('#ordersource');
                /* text boxes END */
                var orderId = $('#futureSaveBtn').attr('data-id');
                /* radio buttons START */
                var dineIn = $('#dine').attr('checked');
                var kiosk = $('#kos').attr('checked');
                var HD = $('#homed').attr('checked');
                var dineInIsVisible = $('#dine').is(':visible:enabled');
                var kioskIsVisible = $('#kos').is(':visible:enabled');
                var homeDelIsVisible = $('#homed').is(':visible:enabled');
                /* radio buttons END */


                var elems = $('#textPanel :input:enabled:visible');
                var modeElems = $('#modePanel :input:enabled:visible');
                var custElems = $('#custPanel :input:enabled:visible');
                var focusFlag = false;
                var modeCheckedFlag = false;
                var custCheckedFlag = false;
                if (event.altKey && event.keyCode == 77) //M
                {
                    if (modeElems.length > 0)
                    {
                        $(modeElems).each(function (index) {
                            if ($(this).is(':checked'))
                            {
                                var nextElem = $(modeElems).eq(index + 1);
                                if (nextElem.length > 0)
                                {

                                    $(nextElem).click();
                                    modeCheckedFlag = true;
                                    return false;
                                }
                                else
                                {
                                    //loop end:last elem
                                    $(modeElems).eq(0).click();
                                    modeCheckedFlag = true;
                                    //alert('TEST OVER');   

                                }

                            }
                            else
                            {
                                modeCheckedFlag = false;
                            }

                        });

                        if (!modeCheckedFlag)
                        {
                            $(modeElems).eq(0).click();
                        }

                    }
                }
                else if (event.altKey && event.keyCode == 67) //C
                {
                    if (custElems.length > 0)
                    {
                        $(custElems).each(function (index) {
                            if ($(this).is(':checked'))
                            {
                                var nextElem = $(custElems).eq(index + 1);
                                if (nextElem.length > 0)
                                {

                                    $(nextElem).click();
                                    custCheckedFlag = true;
                                    return false;
                                }
                                else
                                {
                                    //loop end:last elem
                                    $(custElems).eq(0).click();
                                    custCheckedFlag = true;
                                    //alert('TEST OVER');   

                                }

                            }
                            else
                            {
                                custCheckedFlag = false;
                            }

                        });

                        if (!custCheckedFlag)
                        {
                            $(custElems).eq(0).click();
                        }

                    }
                }
                else if (event.keyCode == 13)
                {
                    if (dineIn || kiosk || HD)
                    {

                        if (elems.length > 0)
                        {
                            $(elems).each(function (index) {
                                if ($(this).is(':focus'))
                                {
                                    var nextElem = $(elems).eq(index + 1);
                                    if (nextElem.length > 0)
                                    {
                                        $(this).blur();
                                        $(nextElem).focus();
                                        focusFlag = true;
                                        return false;
                                    }
                                    else
                                    {
                                        //loop end:last elem
                                        focusFlag = true;
                                        $(elems).blur();
                                        $('#futureSaveBtn').click();
                                        //alert('TEST OVER');   

                                    }

                                }
                                else
                                {
                                    focusFlag = false;
                                }

                            });

                            if (!focusFlag)
                            {
                                $(elems).eq(0).focus();
                            }

                        }
                    }
                }
                else if (event.keyCode == 27) // ESC KEY PRESSES
                {

                    closeStaticPopup("futureOrderCustLoginPopUp");

                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $(elems).blur();
                    $("#futureOrderCustLoginPopUp .iconRefresh").closest('a').click();

                }



                if ($(fDate).is(':focus')) {
                    if (event.keyCode == 8 || event.keyCode == 46)
                    {

                        $(fDate).val('');

                    }
                    else if (event.keyCode == 13)
                    { /*do nothing */
                    }
                    else
                    {
                        event.preventDefault();
                    }

                }


            }

            else if (event.keyCode == 40 && localStorage.POTGsideMenu == "false") { //IF KEY DOWN ARROW WITH ALTR + M NOT ACTIVATED
                var next = $('.tableData .active').next('tr');
                $('.tableData .active').removeClass('active');
                next = next.length > 0 ? next : $('.tableData tr').eq(1);
                next.addClass('active').focus();
            }
            else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false") { //IF KEY UP ARROW WITH ALTR + M NOT ACTIVATED
                var prev = $('.tableData .active').prev('tr');
                $('.tableData .active').removeClass('active');
                prev = prev.length > 0 ? prev : $('.tableData tr:last');
                prev.addClass('active').focus();
            }
            else if (event.altKey && event.keyCode == 80) { // ALETER + P FOR PRINT FOR ACTIVE CLASS
                $("tr.active").find('.billingPrinterIcon').closest('a').click();



            }
            else if (event.altKey && event.keyCode == 68) { // ALETER + D FOR DELETE BILL
                event.preventDefault();
                $("tr.active").find('.deleteIcon').closest('a').click();

            }
            else if (event.altKey && event.keyCode == 69) { // ALETER + E FOR EDIT BILL
                event.preventDefault();
                $("tr.active").find('.tagIcon').closest('a').click();
            }
            else if (event.altKey && event.keyCode == 89) { // ALETER + Y FOR DEl BOY
                event.preventDefault();
                $("tr.active").find('.runMenIcon').closest('a').click();
            }
            else if (event.altKey && event.keyCode == 70) { // ALT + F
                event.preventDefault();
                $("tr.active").find('.watchIconInline').closest('a').click();
            }

        }
        else if (location.indexOf("custListin.html") > -1) { // IF custListin.html PAGE


            

            if ($("#custListinEditPopUp").is(":visible") == true) {

                var elems = $('#textPanel :input:enabled:visible');

                var focusFlag = false;

                if (event.keyCode == 13)
                {
                  

                        if (elems.length > 0)
                        {
                            $(elems).each(function (index) {
                                if ($(this).is(':focus'))
                                {
                                    var nextElem = $(elems).eq(index + 1);
                                    if (nextElem.length > 0)
                                    {
                                        $(this).blur();
                                        $(nextElem).focus();
                                        focusFlag = true;
                                        return false;
                                    }
                                    else
                                    {
                                        //loop end:last elem
                                        focusFlag = true;
                                        $(elems).blur();
                                        $('#custSaveBtn').click();
                                        //alert('TEST OVER');   

                                    }

                                }
                                else
                                {
                                    focusFlag = false;
                                }

                            });

                            if (!focusFlag)
                            {
                                $(elems).eq(0).focus();
                            }

                        }
                    
                }
                else if (event.keyCode == 27) // ESC KEY PRESSES
                {

                    closeStaticPopup("custListinEditPopUp");

                }
                else if (event.altKey && event.keyCode == 68) { //ALTR + D
                    event.preventDefault();
                    $(elems).blur();
                    $("#custListinEditPopUp #custResetBtn").click();

                }






            }


        }

        if (localStorage.POTGpopupflag == "false") //added by sachin jani 03/08/15
        {

            if (event.altKey && event.keyCode == 77) { // altr + M = focus side bar
                if (localStorage.POTGsideMenu == "true") { // IF ALTR + M PRESS AGAIN THEN DISABLE /ENABLE TOGGLE
                    localStorage.POTGsideMenu = false;
                } else {
                    localStorage.POTGsideMenu = true;
                }
            }
            else if (event.keyCode == 40 && localStorage.POTGsideMenu == "true") { //IF KEY DOWN ARROW WITH ALTR + M ACTIVATED
                //alert('sdfd');
                var next = $('.active').next('li');
                $('.active').removeClass('active');
                //next = next.length > 0 ? next : $('.focus li:eq(0)');
                next = next.length > 0 ? next : $('ul.asideIcons li:first');
                next.addClass('active').children('a').focus();

            }
            else if (event.keyCode == 38 && localStorage.POTGsideMenu == "true") { //IF KEY UP ARROW WITH ALTR + M ACTIVATED
                var prev = $('.active').prev('li');
                $('.active').removeClass('active');
                //prev = prev.length > 0 ? prev : $('.focus li').last();
                prev = prev.length > 0 ? prev : $('ul.asideIcons li:last');
                prev.addClass('active').children('a').focus();
            }
            else if (event.altKey && event.keyCode == 83) { // alter + s = open dayshift closure
                $("#swiftCloserlink").click();
            }
            else if (event.altKey && event.keyCode == 82) { // alter + r = open reports
                $("#reportlink").click();
            }
            else if (event.altKey && event.keyCode == 76) { // alter + l = open setting
                $("#settinglink").click();
            }
            else if (event.keyCode == 13 && localStorage.POTGsideMenu == "true") { // ENTER KEY ON FOCUSED ICON
                $('li.active a').click();
            }

        }

    });

    $(".asideIcons").keydown(function (e) { // 
        if (e.keyCode == 40) {
            $('.active').removeClass('active').next('li').addClass('active').children('a').focus();
        }
    });

}

function tabkeyFM3(event)
{
    if (event.keyCode == 9) { // tab
        //prevent
    }

}

function altkeyFM3(event) {



    if (event.altKey && event.keyCode == 66) { // altr + b = focus side bar
        localStorage.POTGalterB = true;
        $('.chkboxfocus').removeClass('chkboxfocus');
        $("#selecteditem .ez-checkbox").eq(0).addClass('chkboxfocus');
        //$('#selecteditem .ez-checkbox').eq(0).closest('tr').focus();
        $('#selecteditem .ez-checkbox input').eq(0).focus();
    }
    else if (event.altKey && event.keyCode == 75) { //IF ALTER KEY + K PRESSED CLICK SEND TO KITCHEN BUTTON
        $(".iconKitchen").click();
    }
    else if (event.altKey && event.keyCode == 80) { // ALETER + P 

        $('.totalBillIcon').find('.printerIcon').click();
    }
    else if (event.keyCode == 68 && event.altKey && localStorage.POTGalterB == "true") { // ALTER + D TO REMOVE SELECTED ITEM
        event.preventDefault();
        localStorage.POTGalterB = false;
        $(".deleteIcon").click();

    }
    else if (event.altKey && event.keyCode == 65) { //ALT + A discount
        //$("#discountPop").click();
    }

    if (event.keyCode == 40 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY DOWN ARROW WITH

        /*old START */
        /*
         var next = $('.chkboxfocus').closest('tr').next().find('.ez-checkbox');
         $('.chkboxfocus').removeClass('chkboxfocus');
         next = next.length > 0 ? next : $("#selecteditem .ez-checkbox").eq(0);
         next.addClass('chkboxfocus');
         */
        /*old END */
        /*new  START*/
        //var scrollOffsetTop = $('#selecteditem').offset().top;
        var next = $('.chkboxfocus').closest('tr').next().find('.ez-checkbox');


        if (next.length > 0) {
            $('.chkboxfocus').removeClass('chkboxfocus');
            //var nextOffsetTop = $(next).closest('tr').offset().top;
            //console.log(" Scroll offset :" + scrollOffsetTop);
            //console.log(" Next offset :" + nextOffsetTop);
            //$('#selecteditem').scrollTop((nextOffsetTop-20)-scrollOffsetTop);
            $(next).addClass('chkboxfocus');
            //$(next).closest('tr').focus();
            $(next).find('input').focus();
        } else {
            $('.chkboxfocus').removeClass('chkboxfocus');
            $('#selecteditem .ez-checkbox').eq(0).addClass('chkboxfocus');
            //$('#selecteditem .ez-checkbox').eq(0).closest('tr').focus();
            //$('#selecteditem .ez-checkbox input').eq(0).focus();
            setTimeout(function () {
                $('#selecteditem').scrollTop(0);
            }, 100);
            //$('#selecteditem').scrollTop(0);

        }


        /*new  END*/






    }
    else if (event.keyCode == 38 && localStorage.POTGsideMenu == "false" && localStorage.POTGalterB == "true") { //IF KEY UP ARROW WITH
        //var prev = $('.chkboxfocus').parent().parent().parent().prev().find('.ez-checkbox');
        /* old code START */
        /*
         var prev = $('.chkboxfocus').closest('tr').prev().find('.ez-checkbox');
         
         $('.chkboxfocus').removeClass('chkboxfocus');
         prev = prev.length > 0 ? prev : $("#selecteditem .ez-checkbox:last");
         prev.addClass('chkboxfocus');
         */

        /* old code END */
        var prev = $('.chkboxfocus').closest('tr').prev().find('.ez-checkbox');

        if (prev.length > 0) {
            $('.chkboxfocus').removeClass('chkboxfocus');
            $(prev).addClass('chkboxfocus');
            //$(prev).closest('tr').focus();
            $(prev).find('input').focus();
        } else {
            $('.chkboxfocus').removeClass('chkboxfocus');
            $("#selecteditem .ez-checkbox:last").addClass('chkboxfocus');
            //$("#selecteditem .ez-checkbox:last").closest('tr').focus();
            //$("#selecteditem .ez-checkbox:last input").focus();

            var maxscroll = $('#selecteditem').prop('scrollHeight') - $('#selecteditem').innerHeight();

            setTimeout(function () {
                $('#selecteditem').scrollTop(maxscroll);
            }, 100);

            //$('#selecteditem').scrollTop(maxscroll);
        }







    }
    else if (event.keyCode == 13 && localStorage.POTGalterB == "true") {
        if ($('.chkboxfocus input').attr('checked'))
            $('.chkboxfocus input').removeAttr('checked');
        else
            $('.chkboxfocus input').attr('checked', true);

        $('.chkboxfocus input').closest("div").toggleClass("ez-checked");
    }
    else if (event.keyCode == 69 && localStorage.POTGalterB == "true") { // "E" keycode on item focus.
        event.preventDefault();
        $(".chkboxfocus").closest("tr").click();
        $("#itm_qty").focus();
        $('.chkboxfocus').removeClass('chkboxfocus');
        localStorage.POTGalterB = false;


    }
    else if (event.keyCode == 82 && localStorage.POTGalterB == "true") { // "R" keycode on item focus.
        event.preventDefault();
        $(".chkboxfocus").closest("tr").find('button').click();
        $('.chkboxfocus').removeClass('chkboxfocus');
        localStorage.POTGalterB = false;


    }
    else if (event.keyCode == 27 && localStorage.POTGalterB == "true") { // "ESC" keycode on item focus.
        event.preventDefault();
        $('.chkboxfocus').removeClass('chkboxfocus');
        deSelectAll('selecteditem');
        localStorage.POTGalterB = false;
        focusOnItemSearch();


    }
    else {
        /*
         if(localStorage.POTGpopupflag == "false")
         {
         $("#searchtag").focus();
         }
         else
         {
         $("#searchtag").focusout();
         } */
    }







}

function shiftkeyFM3(event) {

    if (event.shiftKey && event.keyCode == 65) { // // shiftKey + A to Collect Payment Bill/Order
        $(".billPayment").click();
    } else if (event.shiftKey && event.keyCode == 80) { // shiftKey + P to PRINT Bill/Order
        $(".orderPrint").click();
    } else if (event.keyCode == 68 && event.shiftKey) { // shiftKey + D to Cancel Bill/Order
        $(".orderCancel").click();

    } else if (event.keyCode == 69 && event.shiftKey) { // shiftKey + E to EDIT Bill/Order
        $(".orderEdit").click();
    } else if (event.shiftKey && event.keyCode == 89) { // shiftKey + Y  to assign delivery boy
        $(".delBoy").click();
    } else if (event.shiftKey && event.keyCode == 82) { //R	
        $('#removeFilterMenuIcon').click();
    } else if (event.shiftKey && event.keyCode == 79) { //O	
        $('#filterOccupiedMenuIcon').click();
    } else if (event.shiftKey && event.keyCode == 66) { //B	
        $('#filterBillingMenuIcon').click();
    } else if (event.shiftKey && event.keyCode == 86) { //V	
        $('#filterVacantMenuIcon').click();
    } else if (event.shiftKey && event.keyCode == 77) { //M	
        $('#mergeTableMenuIcon').click();
        setTimeout(function () {
            resetTableMergePopup();
        }, 100);
    }
    else if (event.shiftKey && event.keyCode == 73) { // shiftKey + I  to Open DineIn Menu
        event.preventDefault();
        $("#dineInMenuIcon").click();

    } else if (event.shiftKey && event.keyCode == 75) { // shiftKey + K  to Open DineIn Menu
        event.preventDefault();
        $("#kioskMenuIcon").click();

    } else if (event.shiftKey && event.keyCode == 72) { // shiftKey + H  to Open DineIn Menu
        event.preventDefault();
        $("#HDMenuIcon").click();

    }

}




/* keyCodeShortcut function OVER */
 
