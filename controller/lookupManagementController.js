/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 03-Jun-2016.
 * File : lookupManagementController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
var LOOKUPJSON = [];
var GBL_SELECTED_LKUP = [];
var GBL_SELECTED_LKUP_WEIGHT = 0;

var getLookupManagementReqCase = 'getLookupManagement';
var createLookupReqCase = 'createLookup';
var updateLookupReqCase = 'updateLookup';
var deleteLookupReqCase = 'deleteLookup';

//START FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO LOOKUPJSON
function callBackGetLookupListing(flag, data) {
    if (data.status == SCS && flag) {
        if (data.data) {
            var lookupData = data.data;
            console.log(lookupData);
            for (var i = 0; i < lookupData.length; i++) {
                var lookup = {
                    lookupType : lookupData[i]['lookupType'],
                    lookupValues : lookupData[i]['lookupValues'],
                    lookupTypeName : lookupData[i]['lookupTypeName'],
                    lookupTypeId : lookupData[i]['lookupTypeId'],
                    scoreFlag : lookupData[i]['scoreFlag'],
                    lookupTypeScore : lookupData[i]['lookupTypeScore'],
                }
                // LOOKUPJSON[i] = lookup;
                LOOKUPJSON.push( lookup );
            }
            displayLookupTypeData();
        }
    }else {

        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_NO_LOOKUP );
        }else{
            toastr.error(SERVERERROR);
        }
    }
}
//END FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO LOOKUPJSON

//START FUNCTION FOR DISPLAY DATA OF LOOKUP TYPE
function displayLookupTypeData() {
    var startHtml ="<ul class='nav nav-tabs tabs-left'>";
    var deviceData = LOOKUPJSON;
    console.log(deviceData);
    var html = "";
    for (var i = 0; i < deviceData.length; i++) {
        var arrIndex = i;        
        var lookupType = deviceData[i]['lookupType'];
        var displayValue = deviceData[i]['lookupTypeName'];
        var lookupTypeId = deviceData[i]['lookupTypeId'];
        var actClass = "";
        if( i == 0 ){
            actClass = "active";
        }
        html += "<li style='width:100%' class="+ actClass +"><a href='javascript:void(0)' data-toggle='tab' data-arrIndex='"+arrIndex+"' data-lookupType='"+lookupType+"' lookupTypeId='"+lookupTypeId+"' onclick='getLookupDetails(this)'>"+displayValue+"</a></li>";
    }
    var endHtml = "</ul>";
    var finalHtml = startHtml + html + endHtml;

    $("#lookuptypelist").html(finalHtml);
    $('#lookuptypelist ul li.active a').click();
    addFocusId( 'lookupValueName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}
//END FUNCTION FOR DISPLAY DATA OF LOOKUP TYPE

//START FUNCTION FOR DIAPLAY LOOKUP DATA BY LOOKUP TYPE
function displayLookupValueData(lookupValueData,arrIndex){
    
    resetLookupData(); //CALL FUNCTION FOR RESET LOOKUP DATA    
    var displayValue = LOOKUPJSON[arrIndex]['lookupValues']['displayValue']; // LOOKUP TYPE DISPLAY VALUE
    //var displayValue = LOOKUPJSON[arrIndex]['lookupType'];
    var lookupType = LOOKUPJSON[arrIndex]['lookupType']; // LOOKUP TYPE
    var startHtml ="<table class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>No</th>"+
                            "<th>Name</th>"+
                            "<th>Is Default Value?</th>"+
                            "<th>Action</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>";
    var html=""; 

    if( LOOKUPJSON[arrIndex]['scoreFlag'] == "1" ){
        $('#distBtn').show();
    }else{
        $('#distBtn').hide();
    }

    GBL_SELECTED_LKUP_WEIGHT = LOOKUPJSON[arrIndex]['lookupTypeScore']
    GBL_SELECTED_LKUP = lookupValueData;

    for(var i=0;i<lookupValueData.length;i++){
        var innerArrIndex = i; // lookupValues ARRAY INDEX
        var lookupId = lookupValueData[i]['lookupId'];  // LOOKUP ID
        var lookupValue = lookupValueData[i]['lookupValue']; //LOOKUP VALUE
        var defaultValue = lookupValueData[i]['isDefaultValue']; // LOOKUP DEFAULT VALUE
        var lookUpDisplayValue = lookupValueData[i]['displayValue']; // LOOKUP DEFAULT VALUE
        var defaultValueHtml = defaultValue == 1? "Yes" : "No"; // SET TEXT FOR DEFAULT VALUE IF 1 THEN YES ELSE NO

        html += "<tr>" +
                "<td>" + (i+1) + "</td>" +
                "<td>" + lookUpDisplayValue + "</td>" +
                "<td>" + defaultValueHtml + "</td>" +
                "<td><a class='btn btn-xs btn-primary btn-ripple' data-arrIndex='"+arrIndex+"' data-innerArrIndex='"+innerArrIndex+"' data-lookupId='"+lookupId+"' data-lookupValue='"+lookUpDisplayValue+"' data-defaultValue='"+defaultValue+"' onclick='setLookupDetails(this)'><i class='fa fa-pencil'></i></a></td>" +
                "<td><a class='btn btn-xs btn-danger btn-ripple' data-arrIndex='"+arrIndex+"' data-innerArrIndex='"+innerArrIndex+"' data-lookupId='"+lookupId+"' data-lookupValue='"+lookUpDisplayValue+"' data-defaultValue='"+defaultValue+"' onclick='deleteLookup(this)'><i class='fa fa-trash-o'></i></a></td>" +
                "</tr>";
    }
    
    var endHtml = "</table>";    
    var finalHtml = startHtml + html + endHtml;
    $("#lookupValueList").html(finalHtml); // LOOKUP LISTING TABLE 
    $("#lookupType").html(displayValue); // DISPLAY LOOKUP DISPLAY VALUE
    // $("#lookupTypeName").val(lookupType); // LOOKUP TYPE  FOR HIDDEN FIELD
    TablesDataTables.init();
    TablesDataTablesEditor.init();
    
}
//END FUNCTION FOR DIAPLAY LOOKUP DATA BY LOOKUP TYPE

//START FUNCTION FOR DELETE LOOKUP BY LOOKUP ID
function deleteLookup(lookupData){
    if(checkAuth(8,32,1) == -1){
        toastr.error( NOAUTH );
    }else{
        var lookupId = $(lookupData).attr("data-lookupId"); // LOOKUP ID
        var lookupValue = $(lookupData).attr("data-lookupValue"); //LOOKUP VALUE
        var defaultValue = $(lookupData).attr("data-defaultValue"); //DEFAULT VALUE
        var arrIndex = $(lookupData).attr("data-arrIndex");    //ARRAY INDEX
        var innerArrIndex = $(lookupData).attr("data-innerArrIndex"); //ARRAY INNER INDEX FOR lookupValues
        var cnfrm = confirm(CNFDELETE);
        if( !cnfrm ){
            return false;
        }
        //START FUNCTION FOR DELETE DATA AFTER AJAX SUCCESS
        function callBackAfterDelete(){
            var lookupInnerArr = LOOKUPJSON[arrIndex]['lookupValues']; // lookupValues ARRAY BY INDEX
            lookupInnerArr.splice(innerArrIndex, 1); // REMOVE ELEMENT FROM AN ARRAY BY INDEX
            GBL_SELECTED_LKUP_WEIGHT = LOOKUPJSON[arrIndex]['lookupTypeScore'];
            displayLookupValueData(LOOKUPJSON[arrIndex]['lookupValues'],arrIndex); // CALL FUNCTION FOR DISPLAY DATA AFTER DELETION FROM AN ARRAY
            resetLookupData(); //CALL FUNCTION RESET LOOKUP DATA
        }
        //END FUNCTION FOR DELETE DATA AFTER AJAX SUCCESS

        //START CALL AJAX FOR DELETE LOOKUP
        var postData = {
            requestCase: deleteLookupReqCase,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7, 28),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            lookupId: lookupId
        }
        commonAjax(COMMONURL, postData, callBackAfterDelete, DELETELOOKUP);
        //START CALL AJAX FOR DELETE LOOKUP
    } 
}
//END FUNCTION FOR DELETE LOOKUP BY LOOKUP ID

//START FUNCTION FOR SET LOOKUP DETAILS FOR EDIT BUTTON CLICK
function setLookupDetails(lookupData){
    if(checkAuth(8,30,1) == -1){
        toastr.error('You are not authorised to perform this activity');
    }else{
        var lookupId = $(lookupData).attr("data-lookupId"); //LOOKUP ID
        var lookupValue = $(lookupData).attr("data-lookupValue"); //LOOKUP VALUE
        var defaultValue = $(lookupData).attr("data-defaultValue"); //LOOKUP DEFAULT VALUE
        var arrIndex = $(lookupData).attr("data-arrIndex");    //ARRAY INDEX
        var innerArrIndex = $(lookupData).attr("data-innerArrIndex");  //INNER ARRAY INDEXS

        $("#lookupValueName").val(lookupValue); //FILL
        $("#lookupId").val(lookupId);
        $("#arrIndex").val(arrIndex);
        $("#innerArrIndex").val(innerArrIndex);
        defaultValue == 1 ? $( "#isDefaultValue" ).prop( "checked", true ) : $( "#isDefaultValue" ).prop( "checked", false );   

        addFocusId( 'lookupValueName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT      
    }
}
//END FUNCTION FOR SET LOOKUP DETAILS FOR EDIT BUTTON CLICK

//START FUNCTION FOR RESET LOOKUP DATA
function resetLookupData(){
    $("#lookupValueName").val("");
    // $("#lookupId").val("");
    // //$("#lookupTypeName").val("");
    // $("#arrIndex").val("");
    // $("#innerArrIndex").val("");
    $("#lookupId").val('');
    $( "#isDefaultValue" ).prop( "checked", false );
}
//END FUNCTION FOR RESET LOOKUP DATA

//START FUNCTION FOR GET LOOKUP VALUE DATA BY TYPE 
function getLookupDetails(lookupData){
    var arrIndex = $(lookupData).attr("data-arrIndex");
    var lookupType = $(lookupData).attr("data-lookupType");
    var lookupTypeId = $(lookupData).attr("lookupTypeId");
    
    var lookupValueData = LOOKUPJSON[arrIndex]['lookupValues'];
    console.log(lookupValueData);

    $('#lookupTypeName').val(lookupTypeId);
    $('#arrIndex').val(arrIndex);
    if( lookupValueData != 'No record found' ){
        displayLookupValueData(lookupValueData,arrIndex);
    }else{
        toastr.warning('No record found');
        $("#lookupValueList").html(""); // LOOKUP LISTING TABLE 
    }

    addFocusId( 'lookupValueName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}
//END FUNCTION FOR GET LOOKUP VALUE DATA BY TYPE 

//START FUNCTION FOR CALL API FOR LOOKUP LISTING
function getLookupData() {
    
    if( localStorage.indeCampusLookupType == 'lithe' ){
        getLookupManagementReqCase = 'getlitheLookupManagement';
        createLookupReqCase = 'createLitheLookup';
        updateLookupReqCase = 'updateLitheLookup';
        deleteLookupReqCase = 'deleteLitheLookup';
        $('#pageTitle').html( 'IndeCampus Lookup' );
        $('.panel-heading').hide();
        $('#backBtn').attr('onclick','navigateToMasterDeveloper()');
        
    }else{
        $('#pageTitle').html( 'Lookup' );
    }

    var postData = {
        requestCase: getLookupManagementReqCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(7, 26),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, callBackGetLookupListing, GETTINGLOOKUP);
}
//END FUNCTION FOR CALL API FOR LOOKUP LISTING

//START FUNCTION FOR CREATE / UPDATE LOOKUP 
function createLookup() {
    
    if(checkAuth(8,29,1) == -1){
        toastr.error( NOAUTH );
        return false;
    }
        
    var lookupValueName = $("#lookupValueName").val(); //DEVICE NAME
    var lookupId = $("#lookupId").val(); //DEVICE NAME
    var arrIndex = $("#arrIndex").val(); //DEVICE NAME
    var innerArrIndex = $("#innerArrIndex").val(); //DEVICE TYPE
    var isDefaultValue = $("#isDefaultValue").is(":checked") == true ? 1 : 0; 
    var lookupType = $("#lookupTypeName").val();
    //log("array Index=="+arrIndex);
    //lookupType =="" ? return false : return true;
    if(lookupType==""){
        toastr.warning("Please Select Lookup Type");
        return false;
    }
    if (lookupValueName == "") {
        toastr.warning("Please Fill all Details");
        return false;
    }
    
    function addValueToJSON(flag,data){
        if(data.status== SCS && flag){
            var lookupIdNew = data.data.lookupId;
            
            if((arrIndex == undefined) || arrIndex == null || arrIndex == ""){
                var newLookup = [{
                    lookupId:lookupIdNew,
                    isDefaultValue:isDefaultValue,
                    lookupValue:lookupValueName,
                    displayValue:lookupValueName,
                }];
                delete LOOKUPJSON[arrIndex]["lookupValues"];
                LOOKUPJSON[arrIndex]['lookupValues'] = newLookup;
            }else{
                
                if(LOOKUPJSON[arrIndex]["lookupValues"] == "No record found"){
                    var newLookup = [{
                        lookupId:lookupIdNew,
                        isDefaultValue:isDefaultValue,
                        lookupValue:lookupValueName,
                        displayValue:lookupValueName,
                    }];
                    delete LOOKUPJSON[arrIndex]["lookupValues"];
                    LOOKUPJSON[arrIndex]['lookupValues'] = newLookup;
                }else{
                    var newLookup = {
                        lookupId:lookupIdNew,
                        isDefaultValue:isDefaultValue,
                        lookupValue:lookupValueName,
                        displayValue:lookupValueName,
                    }
                    LOOKUPJSON[arrIndex]['lookupValues'].push(newLookup);
                }

            }
            
            console.log("add lookup");
            console.log(LOOKUPJSON);

            GBL_SELECTED_LKUP_WEIGHT = LOOKUPJSON[arrIndex]['lookupTypeScore'];
            displayLookupValueData(LOOKUPJSON[arrIndex]['lookupValues'],arrIndex);
        }else{
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ADD_LOOKUP );
            else
                toastr.error(SERVERERROR);
        }
    }
    
    function updateValueToJSON(flag,data){
        
        $("#lookupId").val('');
        console.log(data);
        console.log(flag);
        console.log(lookupValueName);
        console.log(isDefaultValue);
        console.log("arr index->"+arrIndex);
        console.log("arr innerArrIndex->"+innerArrIndex);
        if(data.status== SCS && flag){
           
            var lookupData = LOOKUPJSON[arrIndex];
            //LOOKUPJSON[arrIndex]['lookupValues'][innerArrIndex]['displayValue'] =isDefaultValue ;
           
            // LOOKUPJSON[arrIndex]['lookupValues'][innerArrIndex]['defaultValue'] = isDefaultValue;
            // LOOKUPJSON[arrIndex]['lookupValues'][innerArrIndex]['lookupValue'] = lookupValueName;
            // LOOKUPJSON[arrIndex]['lookupValues'][innerArrIndex]['displayValue'] = lookupValueName;
            // console.log("update lookup");
            // console.log(LOOKUPJSON);
            
            var tempLkup = {
                displayValue: lookupValueName,
                isDefaultValue: isDefaultValue,
                lookupId: LOOKUPJSON[arrIndex]['lookupValues'][innerArrIndex]['lookupId'],
                lookupValue: lookupValueName,
            }
            LOOKUPJSON[arrIndex]['lookupValues'][innerArrIndex] = tempLkup ;
            GBL_SELECTED_LKUP_WEIGHT = LOOKUPJSON[arrIndex]['lookupTypeScore'];
            displayLookupValueData(LOOKUPJSON[arrIndex]['lookupValues'],arrIndex);
            
        } else{
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_LOOKUP );
            else
                toastr.error(SERVERERROR);
        }
        
    }
    
    if(lookupId==""){ // INSERT
        var postData = {
            requestCase: createLookupReqCase,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7, 25),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            lookupType: lookupType,
            lookupValue: lookupValueName,
            isDefaultValue: isDefaultValue,
        }
        commonAjax(COMMONURL, postData, addValueToJSON, CREATELOOKUP);
        
    }else{ //UPDATE
        var postData = {
            requestCase: updateLookupReqCase,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7, 27),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            lookupType: lookupType,
            lookupValue: lookupValueName,
            isDefaultValue: isDefaultValue,
            lookupId: lookupId,
        }
        commonAjax(COMMONURL, postData, updateValueToJSON, UPDATELOOKUP);
    }

    resetLookupData();

}
//START FUNCTION FOR CREATE / UPDATE LOOKUP 

getLookupData(); //CALL FUNCTION FOR DISPLAY DATA

function openScoreDistModal(){
    
    var scoreDistHtml = "";
    if( GBL_SELECTED_LKUP_WEIGHT != "" && GBL_SELECTED_LKUP_WEIGHT != "0" ){
        GBL_SELECTED_LKUP.forEach( function( record , index ){
            scoreDistHtml += '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                    '<label class="control-label">'+ record.displayValue +'</label>'+
                                    '<input type="text" id="scoreDistInp_'+ record.lookupId +'" class="form-control onlyNumber" value="'+ record.lookDistAmt +'" placeholder="Enter '+ record.displayValue +' Distribution">'+
                                '</div>'+
                            '</div>';
        });
        $('#scoreDistList').html( scoreDistHtml );
        modalShow('scoreDistModal');
    }else{
        toastr.warning('weightage is Not available');
        return false;
    }
}

function addDistribution(){

    var distScoreTotal = 0;

    var scoreDistArr = [];
    var sendFlag = true;
    GBL_SELECTED_LKUP.forEach( function( record , index ){

        var distScore = (($('#scoreDistInp_'+record.lookupId).val() == "") ? 0 :$('#scoreDistInp_'+record.lookupId).val());
        var tempArray = {
            lookupid : record.lookupId,
            lookupValue : record.lookupValue,
            distScore : distScore,
        }
        if( sendFlag ){
            if( parseFloat( $('#scoreDistInp_'+record.lookupId).val() ) > 100 ){
                toastr.warning('You are not allowed to Distribute weightage more than 100');
                $('#scoreDistInp_'+record.lookupId).focus()
                sendFlag = false;
                return false;
            }else{
                scoreDistArr.push( tempArray );
                distScoreTotal = parseInt( distScoreTotal ) + parseInt( distScore );
            }
        }
    });

    // if( distScoreTotal > 100 ){
    //     toastr.warning('You cannot Distribute More than 100');
    //     return false;
    // }else if( distScoreTotal == 0 ){
    //     toastr.warning('Please Distribute Before Saving');
    //     return false;
    // }else if( distScoreTotal > 100 ){
    //     toastr.info('You have Distributed only '+ distScoreTotal);
    // }

    if( sendFlag ){
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addLookupDistribution',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7,25),
            scoreDistArr: scoreDistArr,
            distScoreTotal: distScoreTotal,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }

        commonAjax(FOLLOWUPURL, postData, addDistributionCallback,"");
        function addDistributionCallback(flag,data){

            if (data.status == "Success" && flag) {
                LOOKUPJSON = data.data;
                modalHide('scoreDistModal');
                displayLookupTypeData();
            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_ADD_WEIGHTAGE );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

}