    /*
 *
 * Created By :  Kavita Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 08-08-2018.
 * File : rentalSpaceInquiryReportController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';
var GBL_REPORT_DATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    $('#uploadMode').selectpicker('refresh');
    $('#filterStatus').selectpicker();
    getPropertyList();
    getRentalSpaceInqReportList(); // GET EVENTS LISTING DATA
    
    commonAutoCompleteForCustInqSearch('partyName',saveCust); // SEARCH FOR PARTY NAME

    // SET INQ STATUS 
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var index = findIndexByKeyValue(roleAuth.data.statusListing, "statusCatName", "BOOKING");
    if (index != "-1") {
        var inqBookingStatus = roleAuth.data.statusListing[index].statusListing;
        setOption('0','inquiryStatus',inqBookingStatus,'Select Inquiry Status');
    }    
    $('#inquiryStatus').selectpicker('refresh');
    
}
// GET EVENTS LISTING DATA
function getRentalSpaceInqReportList(){

    var propertyList = $('#propertyList').val();
    var inquiryFromDate = $('#inquiryFromDate').val();
    var inquiryToDate = $('#inquiryToDate').val();
    var partyId = $('#partyName').attr('partyId');
    var inquiryStatus = $('#inquiryStatus').val();
    if( inquiryStatus == "-1" ){
        inquiryStatus = "";
    }
    if( propertyList == "-1" ){
        propertyList = "";
    }
    if( inquiryFromDate != "" && inquiryFromDate != "" ){

        var dateStartDate = new Date(ddmmyyToMysql(inquiryFromDate));
        var dateEndDate = new Date(ddmmyyToMysql(inquiryToDate));

        //START END DATE VALIDATION
        if (+dateEndDate < +dateStartDate) { 
            toastr.warning("Inquiry end date cannot be less than the Inquiry start date");
            addFocusId('inquiryToDate');
            return false;

        }
    }
    if( $('#partyName').val() == "" ){
        partyId = "";
    }

    var itemDataType = localStorage.indeCampusAddItemType;
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'rentalSpaceReport', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(26,105),
    	fromMonth : inquiryFromDate ,	 
    	toMonth : inquiryToDate,
    	partyId : partyId,
    	propertyId : propertyList,
    	inquiryStatus : inquiryStatus,
    	dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestListCallback,"Please wait...Adding your item.");

    function getRequestListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_REPORT_DATA = data.data;
            renderRentalSpaceInqReportList(GBL_REPORT_DATA); 
            $('.Sidenav').removeClass('open');        

        }else{

            GBL_REPORT_DATA = [];
            GBL_EXCELJSON = [];
            renderRentalSpaceInqReportList(GBL_REPORT_DATA); 
            if (flag)
              displayAPIErrorMsg( data.status , "No inquiries available" );
            else
                toastr.error(SERVERERROR);
        }
	    // resetFilters();
    }

}

// RENDER ITEM LISTING DATA
function renderRentalSpaceInqReportList(eventData){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="enrollmentViewRepoTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Property Name</th>'+
                        '<th>Academic Year</th>'+
                        '<th>Inquiry Date</th>'+
                        '<th>Party Name</th>'+
                        '<th>Inquiry Status</th>'+
		            '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var tempJson = {
        srNo : "#",
        propertyName : "Property Name",
        academicYear : "Academic Year",
        inquiryDate : "Inquiry Date",
        partyName : "Party Name",
        inquiryStatus : "Inquiry Status",
    }
    GBL_CSVJSON.push(tempJson);

    var innerRow = '';
    if( eventData.length != 0 && eventData != "No record found" ){

        eventData.forEach(function( record , index ){
            
            var tempJson = {
                srNo : (index+1),
                propertyName : record.propertyName,
                academicYear : record.acedamicyear,
                inquiryDate : (record.inquiryDate != "0000-00-00" && record.inquiryDate != "1970-01-01" && record.inquiryDate != "" ? mysqltoDesiredFormat(record.inquiryDate,'dd-MM-yyyy') : "" ),
                partyName : record.partyName,
                inquiryStatus : record.inquiryStatus,
            }
            GBL_CSVJSON.push(tempJson);
            
            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.propertyName +'</td>'+
                            '<td>'+ record.acedamicyear +'</td>'+
                            '<td>'+ (record.inquiryDate != "0000-00-00" && record.inquiryDate != "1970-01-01" && record.inquiryDate != "" ? mysqltoDesiredFormat(record.inquiryDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ record.partyName +'</td>'+
                            '<td>'+ record.inquiryStatus +'</td>'+
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    GBL_EXCELJSON = thead + innerRow + tEnd;
    $('#enrollmentViewRepoTbl').DataTable();

}

// RESET FILTER
function resetFilters(){

    // $('#year').val('');
    $('#studentName').attr('studId','');
    $('#studentName').val('')
    $('#propertyList').val('-1').selectpicker('refresh');
    $('#inquiryFromDate').val('');
    $('#inquiryToDate').val('');
    $('#partyName').val('');
    $('#partyName').attr('partyId','');
    $('#inquiryStatus').val('-1').selectpicker('refresh');
    $('#inquiryToDate').datepicker('remove');
    $('#inquiryToDate').datepicker({
        format: 'dd-mm-yyyy',
        // endDate: startDate,
        autoclose: true
    });
   
}

// GET PROPERTY LISTING DATA
function getPropertyList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getPropertyData",
        userId: tmp_json.data[0].PK_USER_ID,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(2,6),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    } 

    commonAjax(FOLLOWUPURL, postData, getPropertyDataCallback,"Please wait...Adding your item.");

    function getPropertyDataCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_PROPERTY_DATA = data.data;
            renderPropertyList();
            
        }else{

            GBL_PROPERTY_DATA = [];
            renderPropertyList();
            
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_PROP );
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

// RENDER PROPERTY LISTING DATA 
function renderPropertyList(){

    var innerHtml = '<option value="-1">Select Property</option>';
    if( GBL_PROPERTY_DATA != "No record found" && GBL_PROPERTY_DATA != "" ){
        GBL_PROPERTY_DATA.forEach(function( record , index ){

            innerHtml += '<option value="'+ record.propertyId +'">'+ record.propertyName +'</option>';

        });
    }
    $('#propertyList').html( innerHtml ); // SET LIST IN SELECTPICKER
    $('#propertyList').selectpicker('refresh');

}

function saveCust(item){
    
    $('#partyName').attr('partyId',item.value);
}

function saveStudent(){
    
}

function setEndDate(context){

    var startDate = $(context).val();

    $('#inquiryToDate').datepicker('remove');
    $('#inquiryToDate').datepicker({
        format: 'dd-mm-yyyy',
        startDate: startDate,
        // endDate: startDate,
        autoclose: true
    });
    $('#inquiryToDate').attr('readonly',true);

}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Rental Space Inquiry Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Rental_Space_Inquiry_Report.pdf",20);
    }

}