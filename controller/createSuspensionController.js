    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-08-2018.
 * File : createSuspensionController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_SUS_TERM_DATA = []; // SUSPENSION  / TERMINATION DATA
var GBL_USERS = []; // USERS DATA

// PAGE LOAD EVENTS  
function pageInitialEvents(){ 
	
	// START INITIALIZATION OF DATE RANGE PICKER
	var start = moment();
    var end = moment();
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var lastDay = new Date(y, m + 1, 0);

    endDay = (mysqltoDesiredFormat(lastDay,'dd-MM-yyyy')).split('-')[0];

    $('#suspensionPeriod span').html(moment().format(01+', MMMM, YYYY') + ' to ' + moment().format(endDay+', MMMM, YYYY'));
    $('#suspensionPeriod').daterangepicker( // GIVES DATE VALUE IN FORMAT "MM/DD/YYYY - MM/DD/YYYY"
    {
        // minViewMode : 'month',
        startDate: start,
        endDate: end,
        opens:'right',
        minDate:new Date(),
        format: 'DD-MM-YYYY',
        separator:' to ',
        // locale: {
        //         format: 'DD-MM-YYYY',

        //         },
        }, function(start, end, label) {

            var stDate = start.format('DD-MM-YYYY');
            var enDate = end.format('DD-MM-YYYY');
            $('#suspensionPeriod span').html(start.format('DD, MMMM, YYYY') + ' to ' + end.format('DD, MMMM, YYYY'));
            $('#suspensionPeriod').val(stDate + ' to ' + enDate);

            console.log(stDate);
            console.log(enDate);
            $('#suspensionPeriod').addClass('active');
    });
    // END INITIALIZATION OF DATE RANGE PICKER
    $('#suspensionPeriod').attr('readonly',true);
    // $('#suspensionPeriod').val( mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') + ' to ' + mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy') );

    
    $('#recommendBlock').show();
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var designationList = roleAuth.data.designation;
    setOption( '0' , 'recommendedByDesignation' , designationList , 'Select Designation' );
    $('#recommendedByDesignation').selectpicker('refresh');
    setOption( '0' , 'revokeByDesgination' , designationList , 'Select Designation' );
    $('#revokeByDesgination').selectpicker('refresh');

    var suspensionReason = roleAuth.data.suspenssionTerminationReason;
    setOption( '0' , 'susTermReason' , suspensionReason , '' );
    $('#susTermReason').selectpicker('refresh');

    $('#terminationWefDate').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true
    });    
    $('#terminationWefDate').attr('readonly',true);
    // $('#terminationWefDate').datepicker( 'update' , mysqltoDesiredFormat(getTodaysDate(),'dd/MM/yyyy'));

	// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID / STUDENT NAME IN FILTERS
    autoCompleteForStudentFilter('membershipid',saveStudentNameData);
    autoCompleteForStudentFilter('studentName',saveStudentNameData);

    getUsersList(); // GET USERS LISTING DATA
    if( localStorage.indeCampusEditSuspensionId != undefined && localStorage.indeCampusEditSuspensionId != "" ){
		getSuspensionTermination();  // GET SUSPENSION / TERMINATION DATA FOR UPDATE
	}

}

// GET USERS LISTING DATA
function getUsersList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "userSearch",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8, 30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getUserCallback,"Please wait...Adding rooms to your item.");

    function getUserCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_USERS = data.data;
            renderUsers(); // RENDER USER LISTING

        }else{

            GBL_USERS = []; 
            renderUsers(); // RENDER USER LISTING
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER USER LISTING
function renderUsers(){ 

	var options = '<option value="-1">Select User</option>';
    if( GBL_USERS != "" && GBL_USERS != "No record found" ){

        GBL_USERS.forEach(function( record , index ){
            
            options += '<option value="'+ record.userId +'">'+ record.fullName +'</option>';

        });
    }
    $('#recommendedByUser').html( options );
    $('#recommendedByUser').selectpicker('refresh');
    $('#revokeByUser').html( options );
    $('#revokeByUser').selectpicker('refresh');
    if( GBL_SUS_TERM_DATA != "" && GBL_SUS_TERM_DATA != "No record found" ){
        $('#recommendedByUser').val( GBL_SUS_TERM_DATA[0].recommendedBy ).selectpicker('refresh');
        $('#recommendedByUser').trigger('change');
    }

}

// SET DESIGNATION DATA OF SELECTED USER
function setDesignation(context,referenceDesgEle){

	var userId = $(context).val();
	if( userId != "-1" && userId != "" ){

		var index = findIndexByKeyValue( GBL_USERS , 'userId' , userId );
		if( index != -1 ){

			$('#'+referenceDesgEle).val( GBL_USERS[index].designationId ).selectpicker('refresh');
			$('#'+referenceDesgEle).closest('div').find('button').css('pointer-events','none');

		}else{
			$('#'+referenceDesgEle).val( '-1' ).selectpicker('refresh');
			$('#'+referenceDesgEle).closest('div').find('button').css('pointer-events','');

		}

	}else{

		$('#'+referenceDesgEle).val( '-1' ).selectpicker('refresh');
		$('#'+referenceDesgEle).closest('div').find('button').css('pointer-events','');

	}

}

// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID / STUDENT NAME IN FILTERS
function autoCompleteForStudentFilter(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( id == "membershipid" ){
	            setTimeout(function() {
	                $('#'+id).val( item.text );
	                $('#'+id).attr( 'studId' , item.value );
	                $('#studentName').val( ITEMS[index].studentName );
	                $('#roomNumber').val( ITEMS[index].roomName );
	            },10);
        	}else{
        		setTimeout(function() {
	                $('#'+id).val( item.text );
	                $('#membershipid').attr( 'studId' , item.value );
	                $('#membershipid').val( ITEMS[index].membershipNumber );
	                $('#roomNumber').val( ITEMS[index].roomName );
	            },10);
        	}
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(1,2),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            if( id == "membershipid" ){

                                var tempJson = {
                                    id: data.studId,
                                    name: data.membershipNumber,
                                    data: data.membershipNumber,
                                    studentName : data.studName,
                                    membershipNumber : data.membershipNumber,
                                    roomName : data.roomName
                                }
                                ITEMS.push(tempJson);

                            }else{

                                var tempJson = {
                                    id: data.studId,
                                    name: data.studName,
                                    data: data.studName,
                                    studentName : data.studName,
                                    membershipNumber : data.membershipNumber,
                                    roomName : data.roomName
                                }
                                ITEMS.push(tempJson);

                            }
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}

// CREATE SUSPENSION / TERMINATION OF STUDENT
function createSuspensionTermination(){

    var susTermType = $('#susTermType').val();
    var susTermReason = $('#susTermReason').val();
	var membershipid = $('#membershipid').val();
	var studId = $('#membershipid').attr('studId');
	var studentName = $('#studentName').val();
	var roomNumber = $('#roomNumber').val();
	var suspensionPeriod = $('#suspensionPeriod').val();
	var recommendedByUser = $('#recommendedByUser').val();
    var recommendedByDesignation = $('#recommendedByDesignation').val();
    var roomServiceAllow = $("input[name=roomServiceOption]:checked").val();
    var fBAllow = $("input[name=fBServiceOption]:checked").val();
    var laundryAllow = $("input[name=laundryOption]:checked").val();
    var eventAllow = $("input[name=eventOption]:checked").val();
	var terminationWefDate = $('#terminationWefDate').val();


    if( susTermReason == "-1" || susTermReason == "" || susTermReason == null ){
        toastr.warning("Please select atleast one suspension /  termination reason");
        addFocusId('susTermReason');
        return false;

    }

    if( susTermType == "1" ){ // SUSPENSION
        
    	if( membershipid == "" || studentName == "" || studId == "" || studId == undefined ){
    		toastr.warning("Please enter student details");
    		addFocusId('membershipid');
    		return false;

    	}else if( suspensionPeriod == "" ){
            toastr.warning("Please select suspension / termination period");
            addFocusId('suspensionPeriod');
            return false;

        }else if( suspensionPeriod != "" && ((suspensionPeriod.split('to')[0].trim().indexOf("01/01/1970") != -1) || (suspensionPeriod.split('to')[1].trim().indexOf("01/01/1970") != -1) || (suspensionPeriod.split('to')[0].trim().indexOf("Invalid") != -1) || (suspensionPeriod.split('to')[1].trim().indexOf("Invalid") != -1)) ){
            toastr.warning("Please select proper suspension / termination period");
            addFocusId('suspensionPeriod');
            return false;

        }else if( recommendedByUser == "-1" || recommendedByUser == "" ){
    		toastr.warning("Please select recommended by user");
    		addFocusId('recommendedByUser');
    		return false;

    	}
        terminationWefDate = '';
    }else{

        if( membershipid == "" || studentName == "" || studId == "" || studId == undefined ){
            toastr.warning("Please enter student details");
            addFocusId('membershipid');
            return false;

        }else if( recommendedByUser == "-1" || recommendedByUser == "" ){
            toastr.warning("Please select recommended by user");
            addFocusId('recommendedByUser');
            return false;

        }else if( terminationWefDate == "" ){
            toastr.warning("Please select termination date");
            addFocusId('terminationWefDate');
            return false;

        }

    }
 //    else if( recommendedByDesignation == "-1" || recommendedByDesignation == "" ){
	// 	toastr.warning("Please select recommended by user designation");
	// 	addFocusId('recommendedByDesignation');
	// 	return false;

	// }

	var suspensionStDate = "";
	var suspensionEnDate = "";

	if( suspensionPeriod != "" && susTermType == "1" ){

        // if( $('#suspensionPeriod').val().indexOf('to') != -1 ){
        //     suspensionStDate = $('#suspensionPeriod').val().split('to')[0].trim();
        //     suspensionEnDate = $('#suspensionPeriod').val().split('to')[1].trim();
        // }else{
        //     var suspStDate = $('#suspensionPeriod').val().split('-')[0].trim();
        //     var suspEnDate = $('#suspensionPeriod').val().split('-')[1].trim();
        //     if( suspStDate.indexOf('/') != -1 && suspEnDate.indexOf('/') != -1 ){
        //         suspensionStDate = suspStDate.split('/')[1] + '-' + suspStDate.split('/')[0] + '-' + suspStDate.split('/')[2];
        //         suspensionEnDate = suspEnDate.split('/')[1] + '-' + suspEnDate.split('/')[0] + '-' + suspEnDate.split('/')[2];
        //     }else{
        //         suspensionStDate = mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy');
        //         suspensionEnDate = mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy');
        //     }
        // }
        suspensionStDate = $('#suspensionPeriod').val().split('to')[0].trim();
        suspensionEnDate = $('#suspensionPeriod').val().split('to')[1].trim();

	}

    var susTermReasonIds = ''; // COMMA SEPARATED REASONS
    if( susTermReason != "-1" && susTermReason != "" &&  susTermReason != null ){
        susTermReason.forEach(function(record,index){
            if( susTermReasonIds ){
                susTermReasonIds += ','+record;
            }else{
                susTermReasonIds += record;
            }
        });
    }

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "addEditStudentSuspension",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(25, 101),
        studentId : studId,
        type : susTermType,
        reason : susTermReasonIds,
        suspenStartDate : suspensionStDate,
        suspenEndDate : suspensionEnDate,
        roomServiceAllow : roomServiceAllow,
        fbAllow : fBAllow,
        laundryAllow : laundryAllow,
        eventBookingAllow : eventAllow,
        recommendedId : recommendedByUser,
        terminationDate : terminationWefDate,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(FOLLOWUPURL, postData, suspensionTerminationCallback,"Please wait...Adding rooms to your item.");

    function suspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data);
            var studName = $('#studentName').val();
            var type = "suspended";
            if( susTermType == "2" ){
                type = "terminated";
            }
            toastr.success(studName + " " + type + " successfully");
            resetSuspension();
            // navigateToSuspensionTermination();  // REDIRECT TO LISTING PAGE

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_ADD_SUS_TER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// GET SUSPENSION DATA OF STUDENT FOR UPDATE
function getSuspensionTermination(){


	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'listStudentSuspensionData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        suspensionId : localStorage.indeCampusEditSuspensionId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getSuspensionTerminationCallback,"Please wait...Adding your item.");

    function getSuspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_SUS_TERM_DATA = data.data;
            renderDataForEdit(); // RENDER DATA FOR UPDATE SUSPENSION

        }else{


            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_SUS_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER DATA FOR UPDATE SUSPENSION
function renderDataForEdit(){

	if( GBL_SUS_TERM_DATA != "" && GBL_SUS_TERM_DATA != "No record found" ){

		var editData = GBL_SUS_TERM_DATA[0];
		$('#membershipid').val( editData.studentMembershipNo );
		$('#membershipid').attr( 'studId' , editData.studentId );
		$('#studentName').val( editData.studentName );
		$('#roomNumber').val( editData.roomName );
        $('#membershipid').attr( 'disabled' , true );
        $('#studentName').attr( 'disabled' , true );
        $('#roomNumber').attr( 'disabled' , true );

        // SET DATERANGEPICKER DATE
		var suspensionStartDate = editData.suspensionStartDate;
		var suspensionEndDate = editData.suspensionEndDate;
		var suspensionPeriodDate = mysqltoDesiredFormat(suspensionStartDate,'dd-MM-yyyy') + ' to ' + mysqltoDesiredFormat(suspensionEndDate,'dd-MM-yyyy');
        $('#suspensionPeriod').data('daterangepicker').setStartDate(mysqltoDesiredFormat(suspensionStartDate,'MM/dd/yyyy'));
        $('#suspensionPeriod').data('daterangepicker').setEndDate(mysqltoDesiredFormat(suspensionEndDate,'MM/dd/yyyy'));
        $('#suspensionPeriod').val( suspensionPeriodDate );
        $('#susTermType').val( editData.type ).selectpicker('refresh');
        $('#susTermType').attr('disabled',true);          
        // $('#susTermReason').val( editData.reason ).selectpicker('refresh');
        var suspTermReason = editData.reason;
        if( suspTermReason != "" && suspTermReason != null && suspTermReason != "-1" ){
            var suspTermReasonIds = suspTermReason.split(',');
            $('#susTermReason').val( suspTermReasonIds ).selectpicker('refresh');
        }
        $('#susTermReason').closest('div').find('button').css('pointer-events','');
        
        // UPDATE SUSPENSION
        if( localStorage.indeCampusRevokeSusFlag == undefined || localStorage.indeCampusRevokeSusFlag == "" || localStorage.indeCampusRevokeSusFlag == false ){
                
            $('#recommendedByUser').val( editData.recommendedBy ).selectpicker('refresh');
            $('#recommendedByUser').trigger('change');
            $('#recommendedByDesignation').val( editData.studentMembershipNo );
            $("input[name=roomServiceOption]").filter('[value='+editData.roomServiceAllow+']').prop('checked', true);
            $("input[name=fBServiceOption]").filter('[value='+editData.FBAllow+']').prop('checked', true);
            $("input[name=laundryOption]").filter('[value='+editData.laundryAllow+']').prop('checked', true);
            $("input[name=eventOption]").filter('[value='+editData.eventBooking+']').prop('checked', true);

            $('#addSuspensionBtn').html( '<i class="fa fa-floppy-o"></i> Update' );
            $('#addSuspensionBtn').attr( 'onclick' , 'updateSuspensionTermination()' );

        }else{

            // REVOKE SUSPENSION
            $('#recommendBlock').hide(300);
            $('#revokeBlock').show(300);  
            $('#suspensionPeriod').css('pointer-events','none');
            $('#terminationWef').datepicker({
                format: 'dd-mm-yyyy',
                startDate: new Date(),
                autoclose: true
            });
            $('#terminationWef').datepicker( 'update' , mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy'));
            $('#terminationWef').attr('readonly',true);

            $('#pageHeaderTitle').html( 'Revoke' );
            $('#panelTitle').html( 'Student Suspension Termination Revoke' );
            $('#addSuspensionBtn').html( '<i class="fa fa-undo"></i> Revoke' );
            $('#addSuspensionBtn').attr( 'onclick' , 'revokeSuspensionTermination()' );
        }

	}
}

// UPDATE SUSPENSION / TERMINATION OF STUDENT
function updateSuspensionTermination(){

    var susTermType = $('#susTermType').val();
    var susTermReason = $('#susTermReason').val();
	var membershipid = $('#membershipid').val();
	var studId = $('#membershipid').attr('studId');
	var studentName = $('#studentName').val();
	var roomNumber = $('#roomNumber').val();
	var suspensionPeriod = $('#suspensionPeriod').val();
	var recommendedByUser = $('#recommendedByUser').val();
	var recommendedByDesignation = $('#recommendedByDesignation').val();
	var roomServiceAllow = $("input[name=roomServiceOption]:checked").val();
	var fBAllow = $("input[name=fBServiceOption]:checked").val();
	var laundryAllow = $("input[name=laundryOption]:checked").val();
	var eventAllow = $("input[name=eventOption]:checked").val();

	if( susTermReason == "-1" || susTermReason == "" || susTermReason == null ){
        toastr.warning("Please select atleast one suspension /  termination reason");
        addFocusId('susTermReason');
        return false;

    }else if( membershipid == "" || studentName == "" || studId == "" || studId == undefined ){
		toastr.warning("Please enter student details");
		addFocusId('membershipid');
		return false;

	// }else if( roomNumber == "" ){
	// 	toastr.warning("Please enter student room detail");
	// 	addFocusId('roomNumber');
	// 	return false;

	}else if( suspensionPeriod == "" ){
		toastr.warning("Please select suspension / termination period");
		addFocusId('suspensionPeriod');
		return false;

	}else if( suspensionPeriod != "" &&  ($('#suspensionPeriod').val().indexOf('to') != -1) && ((suspensionPeriod.split('to')[0].trim().indexOf("01/01/1970") != -1) || (suspensionPeriod.split('to')[1].trim().indexOf("01/01/1970") != -1) || (suspensionPeriod.split('to')[0].trim().indexOf("Invalid") != -1) || (suspensionPeriod.split('to')[1].trim().indexOf("Invalid") != -1)) ){
            toastr.warning("Please select proper suspension / termination period");
            addFocusId('suspensionPeriod');
            return false;

    }else if( recommendedByUser == "-1" || recommendedByUser == "" ){
		toastr.warning("Please select recommended by user");
		addFocusId('recommendedByUser');
		return false;

	}
 //    else if( recommendedByDesignation == "-1" || recommendedByDesignation == "" ){
	// 	toastr.warning("Please select recommended by user designation");
	// 	addFocusId('recommendedByDesignation');
	// 	return false;

	// }

	var suspensionStDate = "";
	var suspensionEnDate = "";

	if( suspensionPeriod != "" ){

		// if( $('#suspensionPeriod').val().indexOf('to') != -1 ){
  //           suspensionStDate = $('#suspensionPeriod').val().split('to')[0].trim();
  //           suspensionEnDate = $('#suspensionPeriod').val().split('to')[1].trim();
  //       }else{
  //           var suspStDate = $('#suspensionPeriod').val().split('-')[0].trim();
  //           var suspEnDate = $('#suspensionPeriod').val().split('-')[1].trim();
  //           if( suspStDate.indexOf('/') != -1 && suspEnDate.indexOf('/') != -1 ){
  //               suspensionStDate = suspStDate.split('/')[1] + '-' + suspStDate.split('/')[0] + '-' + suspStDate.split('/')[2];
  //               suspensionEnDate = suspEnDate.split('/')[1] + '-' + suspEnDate.split('/')[0] + '-' + suspEnDate.split('/')[2];
  //           }else{
  //               suspensionStDate = mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy');
  //               suspensionEnDate = mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy');
  //           }
  //       }
        suspensionStDate = $('#suspensionPeriod').val().split('to')[0].trim();
        suspensionEnDate = $('#suspensionPeriod').val().split('to')[1].trim();
	}   

    var susTermReasonIds = ''; // COMMA SEPARATED REASONS
    if( susTermReason != "-1" && susTermReason != "" &&  susTermReason != null ){
        susTermReason.forEach(function(record,index){
            if( susTermReasonIds ){
                susTermReasonIds += ','+record;
            }else{
                susTermReasonIds += record;
            }
        });
    }

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "addEditStudentSuspension",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(25, 101),
        suspensionId : localStorage.indeCampusEditSuspensionId,
        type : susTermType,
        reason : susTermReasonIds,
        studentId : studId,
        suspenStartDate : suspensionStDate,
        suspenEndDate : suspensionEnDate,
        roomServiceAllow : roomServiceAllow,
        fbAllow : fBAllow,
        laundryAllow : laundryAllow,
        eventBookingAllow : eventAllow,
        recommendedId : recommendedByUser,
        terminationDate : "",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, suspensionTerminationCallback,"Please wait...Adding rooms to your item.");

    function suspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data);
            localStorage.removeItem('indeCampusEditSuspensionId');
            navigateToSuspensionTermination(); // REDIRECT TO LISTING PAGE

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_ADD_SUS_TER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// REVOKE SUSPENSION / TERMINATION
function revokeSuspensionTermination(){

    var studId = $('#membershipid').attr('studId');
    var revokeByUser = $('#revokeByUser').val();
    var revokeByDesgination = $('#revokeByDesgination').val();
    var terminationWef = $('#terminationWef').val();

    if( studId == "" ){
        toastr.warning("No suspension found for revoke");
        return false;

    }else if( revokeByUser == "-1" || revokeByUser == "" ){
        toastr.warning("Please select revoke recommended by user");
        addFocusId('revokeByUser');
        return false;

    }else if( terminationWef == "" ){
        toastr.warning("Please select termination with the effect date");
        addFocusId('terminationWef');
        return false;

    }


    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "revokeStudentSuspension",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(25, 103),
        studentId : studId,
        suspensionId : localStorage.indeCampusEditSuspensionId,
        revokeBy : revokeByUser,
        revokeDate : terminationWef,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, revokeSuspensionTerminationCallback,"Please wait...Adding rooms to your item.");

    function revokeSuspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data);
            localStorage.removeItem('indeCampusEditSuspensionId');
            localStorage.removeItem('indeCampusRevokeSusFlag');
            navigateToSuspensionTermination(); // REDIRECT TO LISTING PAGE

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_REVOKE_SUS_TER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// DISPLAY SUSPENSION / TERMINATION BLOCK
function setSupTerm(context){

    var suspensionTerminationType = $(context).val();

    if( suspensionTerminationType == "2" ){ // WHILE TERMINATION
        $('.suspensionBlock').hide(300);
        $('#terminationDateDiv').show(300);
    }else{                                  // WHILE SUSPENSION
        $('.suspensionBlock').show(300);
        $('#terminationDateDiv').hide(300);
    }

}

function resetSuspension(){

    $('#susTermType').val('').selectpicker('refresh');
    $('#susTermType').trigger('change');
    $('#susTermReason').val('-1').selectpicker('refresh');
    $('#membershipid').val('');
    $('#membershipid').attr('studId','');
    $('#studentName').val('');
    $('#roomNumber').val('');
    $('#suspensionPeriod').val('');
    $('#suspensionPeriod').data('daterangepicker').setStartDate(mysqltoDesiredFormat(getTodaysDate(),'dd/MM/yyyy'));
    $('#suspensionPeriod').data('daterangepicker').setEndDate(mysqltoDesiredFormat(getTodaysDate(),'dd/MM/yyyy'));
    $('#recommendedByUser').val('-1').selectpicker('refresh');
    $('#recommendedByUser').trigger('change');
    $('#recommendedByDesignation').val('-1').selectpicker('refresh');
    $('#radioColor1').click();
    $('#radioColor3').click();
    $('#radioColor5').click();
    $('#radioColor7').click();


}

