/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : inquiryCreateController.
 * File Type : .js.
 * Project : EVENTZ
 *
 * */

var custId = "-2";
var phoneId = "";
var itemJson = [];
var FINAL_ITEMJSON = [];
var bomJson = [];
var itemGroupJson = [];
var temp_inq_Trans = [];
var itemIdList = [];
var exhibProjectData = [];
var GBLRATETYPEID = "-1";
var GBL_CLIENT_TYPE = "";

var inqData = [];

var contPerId = "";

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}


function saveCustDetail() {
    //log(ITEMS);
    var searchValue = GBL_SELECTEDCUSTMOBNO.split("|--|");
    //var index = findIndexByKeyValue(ITEMS, "id", localStorage.POTGselectedCustIdInSearch);
    var index = findIndexByKeyValueAndCond(ITEMS, "id","mobile", searchValue[0],searchValue[1]);

    var data = JSON.parse(localStorage.indeCampusRoleAuth); 
    
//    $('#mobileNumber').val(ITEMS[index].mobile);
    setNumberInIntl( 'mobileNumber' , ITEMS[index].mobile );


    $('#custAddress').val(ITEMS[index].custAddress);
    $('#custCatchArea').val(ITEMS[index].catchArea);
    $('#contPersonDesig').val(ITEMS[index].contPerDesig);
    $('#contPersonEmail').val(ITEMS[index].contPerEmail);
    
    $('#gstinNumber').val(ITEMS[index].gstTinNo);

    custId = ITEMS[index].id;
    phoneId = ITEMS[index].phoneId;
    if( ITEMS[index].cpAllList != "" && ITEMS[index].cpAllList != "No record found" ){
        contPerId = ITEMS[index].cpAllList[0].PK_CONTACT_PERSON_ID;
    }
    GBLRATETYPEID = ITEMS[index].rateTypeId;

    contactPerSonData = ITEMS[index];
    autoCompleteForContactPerson("contPerson", saveContactPerDetail);

    localStorage.removeItem('POTGselectedCustIdInSearch');

    $('#inquiryRateType').val(GBLRATETYPEID);
    $('#inquiryRateType').selectpicker('refresh');
    // $('#inquiryRateTypeDiv').show();

    console.log(ITEMS[index])

    callIntransCustomer(custId);
}

function callIntransCustomer(custId){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
         requestCase: "intransactionCustomerSearch",
         clientId: tmp_json.data[0].FK_CLIENT_ID,
         userId: tmp_json.data[0].PK_USER_ID,
         orgId: checkAuth(4, 13),
         custId: custId,
         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(URL1, postData, callIntransCustomerCallBack,CREATINGINQDATA);

    function callIntransCustomerCallBack(flag, data){
        
         if (data.status == "Success" && flag) {
            
            var custData = data.data;
            var purHistory = data.data.purHistory
            var topCustomer = data.data.topCustomer

            $('#noOfInq').html( custData.totalInq );
            $('#noOfOrder').html( custData.totalOrder );
            $('#totThYr').html( custData.totalTOCurrYear );
            $('#totLsYr').html( custData.totalTOPrevYear );

            var purHis = '<li class="list-group-item">'+
                                '<a href="javascript:void(0)" class="visible">'+
                                    '<span class=""> No Purchase Available </span>'+
                                '</a>'+
                            '</li>';
            if( purHistory != 'No record found' ){
                purHis = "";
                for( var k=0; k<purHistory.length; k++ ){
                    purHis += '<li class="list-group-item"><span class="badge">'+ purHistory[k].itemPrice +'</span>'+ purHistory[k].itemName +'</li>';
                }
            } 
            $('#purHist').html( purHis );

            var topCust = '<li class="list-group-item">'+
                                '<a href="javascript:void(0)" class="visible">'+
                                    '<span class=""> No Customers Available </span>'+
                                '</a>'+
                            '</li>';
            if( topCustomer != 'No record found' ){
                topCust = "";
                for( var l=0; l<topCustomer.length; l++ ){
                    topCust += '<li class="list-group-item">'+
                                    '<a href="javascript:void(0)" class="visible">'+
                                        '<span class="">'+ topCustomer[l].custName +'</span>'+
                                    '</a>'+
                                '</li>';
                }
            }
            $('#topCusList').html( topCust )
            
         }else {
             if (flag)
                 console.log(data.status);
             else
                 console.log(SERVERERROR);
         }
    }

}

function initRoleAuthData(){
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    setOption("0", "inquirytype", data.data.inqType, "---Select Inquiry Type---");
    setOption("0", "inquirysource", data.data.inqSource, "---Select Inquiry Source---");

    $('#inquirytype').selectpicker();
    $('#inquirysource').selectpicker();

    if ($(data.data.inqDefaultSetting).length > 0) {
        $("#inquirytype").val(data.data.inqDefaultSetting.inqTypeId);
        $("#inquirysource").val(data.data.inqDefaultSetting.inqSrcId);
    }
}

function getExhibProjectData( clientType ){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var reqCase = "";
    if( clientType == "32" ){
        reqCase = "cityStateProject";
        $('#exhibPrjctState').html( 'Project Country | State' );
        $('#exhibPrjctCity').html( 'Project City' );
        $('#exhibPrjctList').html( 'Project List' );
    }else{
        reqCase = "cityStateExhibition";
    }

    var postData = {
        requestCase: reqCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        orgId: checkAuth(4, 14)
    };
    commonAjax(URL1, postData, getExhibProjectDataCallBack,"Getting Exhibition Data");

    function getExhibProjectDataCallBack(flag, data){

        if (data.status == "Success" && flag) {
            
            exhibProjectData = data.data;
            var option = "<option value='-1'>--- Please Select Country - State ---</option>"

            exhibProjectData.forEach(function(record,index) {
                option += "<option value='"+ record.stateId +"'>"+ record.countryName +" - "+ record.stateName +"</option>";
            });
            console.log(exhibProjectData);
            $('#stateExhibList').html(option);
            $('#stateExhibList').selectpicker();
            $('#cityExhibList').html("<option value='-1'>--- Please Select City ---</option>");
            $('#cityExhibList').selectpicker();
            $('#exhibList').html("<option value='-1'>--- Please Select "+ ( (GBL_CLIENT_TYPE == "32") ? "Project" : "Exhibition" ) +" ---</option>");
            $('#exhibList').selectpicker();


            $('#projectStateForItem').html(option);
            $('#projectStateForItem').selectpicker();
            $('#projectCityForItem').html("<option value='-1'>--- Please Select City ---</option>");
            $('#projectCityForItem').selectpicker();
            $('#projectForItem').html("<option value='-1'>--- Please Select Project ---</option>");
            $('#projectForItem').selectpicker();

            getEmpListingData();
            
        }else {
            resetExhibList();
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CITY_STATE );
            else
                toastr.warning(SERVERERROR);
        }
    }
}

var state = "";
function getCity(context){

    state = $('#stateExhibList').val();
    if( state == "-1" ){
        $('#cityExhibList').val('-1');
        $('#cityExhibList').selectpicker('refresh');
    }else{
        var index = findIndexByKeyValue( exhibProjectData ,"stateId",state);
        if( index != "-1" ){
            
            cityData = exhibProjectData[index].cityList;
            var option = "<option value='-1'>--- Please Select City ---</option>"
            cityData.forEach(function(record,index) {
                if( record != "No record found" ){
                    option += "<option value='"+ record.cityId +"'>"+ record.cityName +"</option>";
                }
            });
            $('#cityExhibList').html(option);
            $('#cityExhibList').selectpicker('refresh');
        }
    }
}

function getExhibProject(context){

    var city = $('#cityExhibList').val();
    if( city == "-1" ){
        $('#exhibList').val('-1');
        $('#exhibList').selectpicker('refresh');
    }else{
        var index = findIndexByKeyValue( exhibProjectData ,"stateId",state);
        if( index != "-1" ){
            
            var innrIndex = findIndexByKeyValue( exhibProjectData[index].cityList ,"cityId",city);
            if( innrIndex != "-1" ){

                var exhibListData = "";
                var option = "";

                if( GBL_CLIENT_TYPE == "32" ){
                    projectListData = exhibProjectData[index].cityList[innrIndex].projectList;
                    option = "<option value='-1'>--- Please Select Project ---</option>";
                    projectListData.forEach(function(record,index) {
                        if( record != "No record found" ){
                            option += "<option value='"+ record.projectId +"'>"+ record.projectName +"</option>";
                        }
                    });
                }else{
                    exhibListData = exhibProjectData[index].cityList[innrIndex].exhibitionList;
                    option = "<option value='-1'>--- Please Select Exhibition ---</option>";
                    exhibListData.forEach(function(record,index) {
                        if( record != "No record found" ){
                            option += "<option value='"+ record.exhibId +"'>"+ record.exhibitionName +"</option>";
                        }
                    });
                }

                $('#exhibList').html(option);
                $('#exhibList').selectpicker('refresh');
            }
        }
    }
}

// function getitemListing(){
    
//     var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//     var postData = {
//         requestCase: "getItemListing",
//         clientId: tmp_json.data[0].FK_CLIENT_ID,
//         userId: tmp_json.data[0].PK_USER_ID,
//         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//         orgId: checkAuth(4, 14)
//     };
    
//     commonAjax(URL1, postData, itemListingCallBack,CREATINGINQDATA);
// }

// function itemListingCallBack(flag, data){
//     if (data.status == "Success" && flag) {
        
//         var option = "<option value='-1'>--- Please Select Item ---</option>"
//         for( var i=0; i<data.data.length; i++ ){
            
//             var tempJson = '';
//             tempJson = {
//                 clientId: data.data[i].clientId,
//                 itemId: data.data[i].itemId,
//                 itemName: data.data[i].itemName,
//                 itemPrice: data.data[i].itemPrice,
//                 itemSubType: data.data[i].itemSubType,
//                 itemType: data.data[i].itemType,
//                 orgId: data.data[i].orgId,
//                 validityEndDate: data.data[i].validityEndDate,
//             }
            
//             itemJson.push(tempJson);
            
//             option += "<option value='"+ data.data[i].itemId +"'>"+ data.data[i].itemName +"</option>";
            
//         }
//         console.log(data.data);
        
        
//         $('#itemName').html(option);
//         $('#itemName').selectpicker();
        
//     }else {
//         if (flag)
//             alert(data.status);
//         else
//             alert(SERVERERROR);
//     }
// }

function AutoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            // console.log(item);
            var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
        
            var endDate = mysqltoDesiredFormat( ITEMS[itemIndex].priceValidityEndDate , "dd-MM-yyyy" );
            // if( new Date(ITEMS[itemIndex].priceValidityEndDate +" 11:59 PM") < new Date() ){
            //     toastr.warning(ITEMS[itemIndex].itemName+' Price validity is Over on <b>"'+ endDate +'"</b>, So you need to set it from Client Master page');
            //     $('#searchitem').val('');
            //     return false;
            // }
            
            $('#itemName').attr('itemId',item.value);
            $('#itemGroupText').val(ITEMS[itemIndex].itemGroupName);

            $('#itemPrice').val(ITEMS[itemIndex].itemPrice);
            
            bomJson = ITEMS[itemIndex].bomInnerItem;
            
            callback(ITEMS[itemIndex].bomInnerItem);

            if( ITEMS[itemIndex].detailing != "No record found" && ITEMS[itemIndex].detailing != "" && ITEMS[itemIndex].detailing != null ){
                openDetailModal(ITEMS[itemIndex].detailing);
            }

        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {

                var type = "1";
                var itemType = $('#itemType').val();
                if( itemType == 3 ){
                    type = "3";
                }
                addRemoveLoader(1);
                GBLRATETYPEID = $('#inquiryRateType').val();
                var projectExhibId = "";
                if( currClientType == "32" ){
                    projectExhibId = $('#exhibList').val();
                }
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getItemListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(4, 14),
                    keyWord: query,
                    type: type,
                    projectId: projectExhibId,
                    custRateType : GBLRATETYPEID,
                };

                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data,jqXHR) {
                console.log()
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                console.log(data.status);
                // // We good!
                ITEMS = [];
                if (data.status != "No record found") {
                    $.map(data.data, function (data) {
                        if( data != "No record found" ){

                            var tempJson = '';
                            tempJson = {
                                id: data.itemId,
                                name: data.itemName,
                                data: data.itemName,
                                clientId: data.clientId,
                                itemId: data.itemId,
                                itemName: data.itemName,
                                itemPrice: data.itemPrice,
                                itemSubType: data.itemSubType,
                                itemType: data.itemType,
                                orgId: data.orgId,
                                validityEndDate: data.validityEndDate,
                                itemGroupName: data.itemGroupName,
                                bomInnerItem: data.bomInnerItem,
                                priceValidityEndDate: data.priceValidityEndDate,
                                detailing: data.detailing,
                            }
                            // if( data.bomInnerItem != undefined ){
                            //     bomJson.push(tempJson)
                            // }else{
                                itemJson.push(tempJson);
                            // }
                            ITEMS.push(tempJson);
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            },
            fail: function() {
              addRemoveLoader(0);
            }
        }
    });
}

function saveItemDetail(){
    
}

function saveUserDetail(){

}

function addItemRow(){
    
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    if( roleAuth.data.itemRateTypeFlag == "1"  && custId == "-2" && $('#custSearch').val() == "" ){
        toastr.warning('Please Search Customer Before entering the item in to the Cart');
        $('#itemSelect').click();
        $('#itemName').focus();
        return false;
    }

    var exhibList = $('#exhibList').val();  // ONLY FOR REAL ESTATE & EXHIBITION CLIENT
    if( ( roleAuth.data.clientTypeId == "32" || roleAuth.data.clientTypeId == "34" ) && ( exhibList == "-1" || exhibList == "" || exhibList == null ) ){
        toastr.warning('Please Select Project');
        addFocusId( 'exhibList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;
    }

    var selectedItemName = $('#itemName').attr('itemId');
    var selectedGroupName = $('#itemGroupText').attr('groupid');
    var itemQty =  $('#itemQty').val();
    var itemRemark =  $('#itemLevelRemark').val();
    var itemHeaderText =  $('#itemHeaderText').val();
    var itemType =  $('#itemType').val();
    var itemPrice =  $('#itemPrice').val();
    if(itemType == 1){
        
        if((selectedItemName == '' || selectedItemName == undefined) && (selectedGroupName == "" || selectedGroupName == undefined )){
            toastr.warning('Please Select Item');
            $('#itemName').focus();
            return false;
        }else if( (selectedItemName != '' && selectedItemName != undefined )  && itemPrice == ""){
            toastr.warning('Please Enter Price');
            $('#itemPrice').focus();
            return false; 
        }else if( itemQty == "" || parseFloat( itemQty ) < 0.01 ){
            toastr.warning('Please Enter Valid Quantity');
            $('#itemQty').focus();
            return false;
        }else{
            if(selectedItemName != "" && selectedItemName != undefined){
                
                var index = findIndexByKeyValue(itemJson,"itemId",selectedItemName);
                var sequenceId = (temp_inq_Trans.length+1);
                var searchParam = [];
                var  tempData = {
                    cancleFlag : "1",
                    itemId : itemJson[index].itemId,
                    itemName : itemJson[index].itemName,
                    groupName : itemJson[index].itemGroupName,
                    itemQty : itemQty,
                    eventStatus : "",
                    itemPrice : (Number( parseFloat( itemPrice ) * parseFloat( itemQty ) )).toFixed(2),
                    itemBasePrice : itemPrice,
                    itemType : "ITEM",
                    itemRemark : itemRemark,
                    itemCat : itemType, // 1 = item 2 - header text
                    sequenceId : sequenceId,
                    itemDetail : detailJSON,
                }
                var dupInd = findIndexByKeyValue( temp_inq_Trans,'itemId',itemJson[index].itemId );
                if( dupInd == "-1" ){
                    temp_inq_Trans.push(tempData); 
                }else{
                    searchParam.push(tempData);
                    if( roleAuth.data.clientTypeId == "35" ){
                        //var loopFlag = true;
                        var addFlag = true;
//                        temp_inq_Trans.forEach( function( record , index ){
//                            if( record.itemId == tempData.itemId && loopFlag == true ){
//                                record.itemDetail.forEach( function( innrRecord , innrIndex ){
//                                    if( loopFlag == true ){
//                                        if( jsonEqual(record.itemDetail, tempData.itemDetail) ){
//                                            loopFlag = false;
//                                            addFlag = false;
//                                        }
//                                    }
//                                });
//                            }
//                        });
                        searchParam.forEach(function (recordSearch , indexSearch ){
                            temp_inq_Trans.forEach( function( recordProduct , indexProduct ){
                                if( jsonEqual(recordProduct.itemDetail, recordSearch.itemDetail) ){
                                    addFlag = false;
                                    return false; 
                                }
                            });
                        });
                        
                        //console.log("asda ");
                        if( addFlag == true ){
                            temp_inq_Trans.push(tempData);
                        }else{
                            toastr.warning('This Item already Added in the Cart');
                        }
                        addRowTable( temp_inq_Trans );
                    }else{
                        toastr.warning('This Item already Added in the Cart');
                    }

                }
            }else if(selectedGroupName != "" && selectedGroupName != undefined){
                
                var index = findIndexByKeyValue(itemGroupJson,"id",selectedGroupName);
                var sequenceId = (temp_inq_Trans.length+1);
                var  tempData = {
                    cancleFlag : "1",
                    itemId : itemGroupJson[index].id,
                    itemName : "",
                    groupName : itemGroupJson[index].name,
                    itemQty : itemQty,
                    eventStatus : "",
                    itemPrice : "0.00",
                    itemBasePrice : "0.00",
                    itemType : "GROUP",
                    itemRemark : itemRemark,
                    itemCat : itemType, // 1 = item 2 - header text
                    sequenceId : sequenceId,
                }
                var dupInd = findIndexByKeyValue( temp_inq_Trans,'itemId',itemGroupJson[index].id );
                if( dupInd == "-1" ){
                    temp_inq_Trans.push(tempData); 
                }else{
                    toastr.warning('This group already Added in the Cart');
                }
            }

           
            //CATALOG SCREEN LAST SELECTED
            $('#projectStateForItem').val( $('#stateExhibList').val() );
            $('#projectStateForItem').selectpicker('refresh');
            getProjCityForItem( $('#stateExhibList').val() );

            $('#projectCityForItem').val( $('#cityExhibList').val() );
            $('#projectCityForItem').selectpicker('refresh');
            getProjListForItem( $('#exhibList').val() );
            
            $('#projectForItem').val( $('#exhibList').val() );
            $('#projectForItem').selectpicker('refresh');

            // $('.dataTables_empty').hide();
            // $('#itemList').append(newRow);

            // $("#tableListing").DataTable();

            $('#itemName').val('');
            $('#itemName').attr('itemId','');
            $('#itemQty').val('1');
            $('#itemGroupText').val('');
            $('#itemLevelRemark').val('');
            $('#itemPrice').val('');
            $('#itemGroupText').attr('groupid','');

            
        }

    callIntransaction();

    }else if(itemType == 2){
         
         if( itemHeaderText == ""){
            toastr.warning('Please enter value');
            $('#itemHeaderText').focus();
            return false;
         }else{
            var sequenceId = (temp_inq_Trans.length+1);
            var  tempData = {
                cancleFlag : "1",
                itemId : (itemType+itemHeaderText),
                itemName : itemHeaderText,
                groupName : "",
                itemQty : "",
                eventStatus : "",
                itemPrice : 0,
                itemBasePrice : 0,
                itemType : "SECTION",
                itemRemark : "",
                itemCat : itemType, // 1 = item 2 - header text
                sequenceId : sequenceId
            }
            temp_inq_Trans.push(tempData) 
         }
         $('#itemHeaderText').val('');
    }
    
    addRowTable( temp_inq_Trans );
    
}


function addRowTable(itemJson , bomId){

    var totAmt = 0;

    var thead = '<table id="tableListing" class="tableListing display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th style="display:none"></th>'+
                            '<th>Name</th>'+
                            '<th>Group Name</th>'+
                            '<th>Quantity</th>'+
                            '<th>Price</th>'+
                            '<th>Amount</th>'+
                            '<th>Remark</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    //ROLE AUTH
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);

    var newRow = '';
    var potPrice = 0;
    for ( var i=0; i<itemJson.length; i++ ){
        if(itemJson[i].itemCat == 1){

            totAmt = parseFloat( totAmt ) + parseFloat( itemJson[i].itemPrice );

            //INPUT ACCORDING TO DECIMAL ALLOW ON QTY
            var inputHtmlDiv = '<input type="number" min="1" id="editQty_'+ itemJson[i].itemId +'_'+i+'" value="'+ itemJson[i].itemQty +'" class="form-control onlyNumber editBlock_'+ itemJson[i].itemId +'_'+i+'" placeholder="Quantity (eg. 1)"  style="display:none;">';
            //CHECK DECIMAL ALLOWED OR NOT
            // roleAuth.data.decimalQtyFlag = 1;
            if( roleAuth.data.decimalQtyFlag == "1" ){
                inputHtmlDiv = '<input type="number" min=".01" step=".01" id="editQty_'+ itemJson[i].itemId +'_'+i+'" value="'+ itemJson[i].itemQty +'" class="form-control editBlock_'+ itemJson[i].itemId +'_'+i+'" placeholder="Quantity (eg. 1)"  style="display:none;">';
            }

            newRow += '<tr>'+
                    '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                    '<td>'+ itemJson[i].itemName +'</td>'+
                    '<td>'+ itemJson[i].groupName +'</td>'+
                    '<td><span class="normalBlock_'+ itemJson[i].itemId +'_'+i+'">'+ itemJson[i].itemQty +'</span>'+ inputHtmlDiv +'</td>'+
                    '<td class="amtDisp"><span class="normalBlock_'+ itemJson[i].itemId +'_'+i+'">'+ numberFormat( itemJson[i].itemBasePrice ) +'</span><input type="number" min="1" id="editPrice_'+ itemJson[i].itemId +'_'+i+'" value="'+ itemJson[i].itemBasePrice +'" class="form-control editBlock_'+ itemJson[i].itemId +'_'+i+'" placeholder="Price (eg. 500)"  style="display:none;"></td>'+
                    '<td class="amtDisp">'+ numberFormat( itemJson[i].itemPrice ) +'</td>'+
                    '<td><span class="normalBlock_'+ itemJson[i].itemId +'_'+i+'">'+ itemJson[i].itemRemark +'</span><input type="text" id="editRemark_'+ itemJson[i].itemId +'_'+i+'" value="'+ itemJson[i].itemRemark +'" class="form-control editBlock_'+ itemJson[i].itemId +'_'+i+'" placeholder="Enter Remark"  style="display:none;"></td>'+
                    '<td>'+
                        '<a data-placement="top" item-id="'+ itemJson[i].itemId +'" data-toggle="tooltip" id="varConfigBtn" href="javascript:void(0)" onclick="openVCDialog(this,'+i+')" data-original-title="Item Detailing" title="" class="tooltips btn btn-blue-grey btn-xs btn-ripple e2b btn-right-mrg" style=""><i class="fa fa-sitemap"></i></a>'+
                        '<button item-id="'+ itemJson[i].itemId +'" type="button" class="btn btn-primary btn-ripple btn-xs normalBlock_'+ itemJson[i].itemId +'_'+i+'" onclick="editItem(this , '+ i +')"><i class="fa fa-pencil"></i></button>&nbsp;'+
                        '<button item-id="'+ itemJson[i].itemId +'" type="button" class="btn btn-primary btn-ripple btn-xs editBlock_'+ itemJson[i].itemId +'_'+i+'" onclick="saveItem(this , '+ i +')" style="display:none;"><i class="fa fa-save"></i></button>&nbsp;'+
                        '<button item-id="'+ itemJson[i].itemId +'" type="button" class="btn btn-ripple btn-xs btn-danger" onclick="removeRow(this)"><i class="fa fa-trash-o"></i></button>'+
                    '</td>'+
                '</tr>';
        }else{
            newRow += '<tr class="group">'+
                    '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                    '<td colspan="6">'+ itemJson[i].itemName +'</td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td>'+
                        '<button item-id="'+ itemJson[i].itemId +'" bomId="'+ itemJson[i].bomId +'" type="button" class="btn btn-primary btn-ripple btn-sm btn-danger" onclick="removeRow(this)"><i class="fa fa-trash-o"></i></button>'+
                    '</td>'+
                '</tr>';
        }
        potPrice = parseFloat( potPrice ) + parseFloat( itemJson[i].itemPrice );
    }
    
    newRow += '<tr class="group">'+
                    '<td style="display:none"></td>'+
                    '<td colspan="4"> Sub Total </td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td style="display:none"></td>'+
                    '<td class="amtDisp">'+ numberFormat( totAmt ) +'</td>'+
                    '<td></td>'+
                    '<td></td>'+
                '</tr>';
                
    $('#potAmt').val( potPrice );
    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable({
        bSort : false,
        "stateSave": true
    });

    //CATALOG TABLE LISTING AND RENDERING
    var theadCataLog = '<table id="tableCatalogListing" class="tableListing display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th style="display:none"></th>'+
                            '<th>Item Name</th>'+
                            '<th>Quantity</th>'+
                            '<th>Amount</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

    var tfootCataLog = '</tbody></table>';
    var newRowCataLog = '';

    for ( var i=0; i<itemJson.length; i++ ){
        if(itemJson[i].itemCat == 1){

            newRowCataLog += '<tr>'+
                                '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                                '<td>'+ itemJson[i].itemName +'</td>'+
                                '<td>'+ itemJson[i].itemQty +'</td>'+
                                '<td class="amtDisp">'+ numberFormat( itemJson[i].itemPrice ) +'</td>'+
                            '</tr>';
        }else{
            newRowCataLog += '<tr class="group">'+
                                '<td style="display:none">'+ itemJson[i].sequenceId +'</td>'+
                                '<td>'+ itemJson[i].itemName +'</td>'+
                                '<td></td>'+
                                '<td></td>'+
                            '</tr>';
        }
    }

    newRowCataLog += '<tr class="group">'+
                        '<td style="display:none"></td>'+
                        '<td> Sub Total </td>'+
                        '<td class="amtDisp">'+ numberFormat( totAmt ) +'</td>'+
                        '<td></td>'+
                    '</tr>';

    $('#dtCatalogTable').html(theadCataLog + newRowCataLog + tfootCataLog);
    $("#tableCatalogListing").DataTable({
        bSort : false,
        "stateSave": true
    });

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    console.log(currClientType);
    if( currClientType == 35 ){
        $('.e2b').show();
    }else{
        $('.e2b').hide();
    }

    if( itemJson != "" ){
        
        var firstItem = itemJson[0].itemName;
        var firstItemRemark = itemJson[0].itemRemark;

        if( itemJson[0].itemCat != 1 ){
            firstItem = itemJson[0].itemName;
            firstItemRemark = itemJson[0].itemType;
        }
        
        $('#projectName').val(firstItem);
        $('#inquiryRemarks').val(firstItemRemark);
        $('#remarks').val(firstItem);
    }

    //DISABLE PROJECT WHEN ANY ITEM IS INSERTED
    if( temp_inq_Trans.length != 0 ){
        $('.disEnaCls').attr('disabled',true);
        $('.disEnaCls').selectpicker('refresh');
    }else{
        $('.disEnaCls').attr('disabled',false);
        $('.disEnaCls').selectpicker('refresh');
    }

    // it allow only number and plus symbol just place in input fields
    $('.onlyNumber').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });
}

function editItem(context , index){

    var itemId = $(context).attr('item-id');
    $('.editBlock_'+itemId+'_'+index).show();
    $('.normalBlock_'+itemId+'_'+index).hide();
}

function saveItem(context , index){

    var itemId = $(context).attr('item-id');

    var qty = $('#editQty_'+itemId+'_'+index).val();
    var price = $('#editPrice_'+itemId+'_'+index).val();
    var Remark = $('#editRemark_'+itemId+'_'+index).val();
    
    if( qty == "" || parseFloat( qty ) < 0.01 ){
        toastr.warning("Please Enter Valid Quantity");
        $('#editQty_'+itemId+'_'+index).focus();
        return false;

    }else if( price == ""|| parseFloat( price ) < 0.01  ){
        toastr.warning("Please Enter price");
        $('#editPrice_'+itemId+'_'+index).focus();
        return false;
    }

    var multipl = ( parseFloat(qty) * parseFloat(price) ).toFixed(2);

    temp_inq_Trans[index].itemBasePrice = price;
    temp_inq_Trans[index].itemQty = qty;
    temp_inq_Trans[index].itemPrice = ( multipl );
    temp_inq_Trans[index].itemRemark = Remark;

    addRowTable( temp_inq_Trans );

    $('.normalBlock_'+itemId+'_'+index).show();
    $('.editBlock_'+itemId+'_'+index).hide();
}

function callIntransaction(){
    
    itemIdList = [];

    for( var i=0; i<temp_inq_Trans.length; i++ ){

        if( itemIdList.length == 0 ){
            itemIdList = temp_inq_Trans[i].itemId;
        }else{
            itemIdList += ','+temp_inq_Trans[i].itemId;
        }
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
         requestCase: "intransactionItemSearch",
         clientId: tmp_json.data[0].FK_CLIENT_ID,
         userId: tmp_json.data[0].PK_USER_ID,
         orgId: checkAuth(4, 14),
         itemId: itemIdList,
         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(URL1, postData, itemListingCallBack,CREATINGINQDATA);

    function itemListingCallBack(flag, data){
        
         if (data.status == "Success" && flag) {
            
            var BOMData = data.data.salesBomSuggetion;    
            var suggestData = data.data.genBroughtTogether;    
            var similarData = data.data.similarSuggetion;    

            var salesBom = '';
            if( BOMData != 'No record found' && BOMData != undefined ){

                for( var j=0; j<BOMData.length; j++ ){
                    
                    if( BOMData[j] != null ){
                        salesBom += '<li class="list-group-item">'+
                                        '<a href="javascript:void(0)" item-Id= "'+ BOMData[j].itemId +'" class="visible">'+
                                            '<span class="">'+ BOMData[j].itemName +'</span>'+
                                        '</a>'+
                                    '</li>';
                    }
                }
            }else{
                salesBom = '<li class="list-group-item">'+
                                    '<a href="javascript:void(0)" class="visible">'+
                                        '<span class="">No Sales BOM Available</span>'+
                                    '</a>'+
                                '</li>';
            }

            $('#salesBomList').html( salesBom );

            var suggestion = '';
            if( suggestData != 'No record found' && suggestData != undefined ){

                for( var k=0; k<suggestData.length; k++ ){
                    suggestion += '<li class="list-group-item">'+
                                    '<a href="javascript:void(0)" item-Id= "'+ suggestData[k].itemId +'" class="visible">'+
                                        '<span class="">'+ suggestData[k].itemName +'</span>'+
                                    '</a>'+
                                '</li>';
                }
            }else{
                suggestion = '<li class="list-group-item">'+
                                    '<a href="javascript:void(0)" class="visible">'+
                                        '<span class="">No Sales Suggestion Available</span>'+
                                    '</a>'+
                                '</li>';
            }

            $('#suggItem').html( suggestion );

            var similar = '';
            if( similarData != 'No record found' && similarData != undefined ){
                
                for( var l=0; l<similarData.length; l++ ){
                    similar += '<li class="list-group-item">'+
                                    '<a href="javascript:void(0)" item-Id= "'+ similarData[l].itemId +'" class="visible">'+
                                        '<span class="">'+ similarData[l].itemName +'</span>'+
                                    '</a>'+
                                '</li>';
                }
            }else{
                similar = '<li class="list-group-item">'+
                                    '<a href="javascript:void(0)" class="visible">'+
                                        '<span class="">No Similar Product Available</span>'+
                                    '</a>'+
                                '</li>';
            }

            $('#simmProd').html( similar );

         }else {
             if (flag){
                console.log(data.status);
                $('#salesBomList').html( '<li class="list-group-item">'+
                                            '<a href="javascript:void(0)" class="visible">'+
                                                '<span class="">No Sales BOM Available</span>'+
                                            '</a>'+
                                        '</li>' );

                $('#suggItem').html( '<li class="list-group-item">'+
                                        '<a href="javascript:void(0)" class="visible">'+
                                            '<span class="">No Sales Suggestion Available</span>'+
                                        '</a>'+
                                    '</li>');
                
                $('#simmProd').html( '<li class="list-group-item">'+
                                            '<a href="javascript:void(0)" class="visible">'+
                                                '<span class="">No Similar Product Available</span>'+
                                            '</a>'+
                                        '</li>' );
             }
             else{  
                 console.log(SERVERERROR);
             }
         }
    }
}

function createQuickInquiry(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }else if( checkAuth(4, 14, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }

    var custName = $('#custSearch').val().trim();
    var contPerson = $('#contPerson').val().trim();
    var mobileNumber = getIntlMobileNum("mobileNumber");    // GET MOBILE NUM
    var ContmobileNumber = getIntlMobileNum("ContmobileNumber");    // GET MOBILE NUM
    // var mobileNumber = $('#mobileNumber').val().trim();
    // var ContmobileNumber = $('#ContmobileNumber').val().trim();
    var email = $('#email').val().trim();
    var custStateId = $('#custStateId').val();
    var custCityId = $('#custCityId').val();
    var custcountryId = $('#custCountryId').val();
    var inquirytype = $('#inquirytype').val();
    var inquirysource = $('#inquirysource').val();
    var projectName = $('#projectName').val().trim();
    var inquiryRemarks = $('#inquiryRemarks').val().trim();
    var followupDate = $('#followupDate').val();
    var remarks = $('#remarks').val().trim();
    // var followUpTime = $('#dueTime').val();
    var gstinNumber = $('#gstinNumber').val();

    var custAddress = $('#custAddress').val().trim();
    var contPersonEmail = $('#contPersonEmail').val().trim();
    var contPersonDesig = $('#contPersonDesig').val();

    var catchArea = $('#custCatchArea').val();
    
    var custRateType = $('#inquiryRateType').val();

    var potAmt = $('#potAmt').val();
    var closerBy = $('#closerBy').val();

    var assUserId = $('#assUser').val();

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    var custSearchFlag = false;

    var stateExhib = $('#stateExhibList').val();
    var cityExhib = $('#cityExhibList').val();
    var exhibId = $('#exhibList').val();

    var smsCheck = ($('#smsCheck').is(':checked')) == true ? "1" : "0";
    var emailCheck = ($('#emailCheck').is(':checked')) == true ? "1" : "0";

    var confInqCb = $('#confirmInq').prop('checked');

    if( currClientType != 34 && currClientType != 32 ){
        stateExhib = "";
        cityExhib = "";
        exhibId = "";
    }

    if( ( currClientType == 34 || currClientType == 32 )  && stateExhib == "-1" ){
        toastr.warning('Please Select State for '+ ( (currClientType == "32") ? "Project" : "Exhibition" ) );
        $('#itemSelect').click();
        $('#stateExhibList').focus();
        return false;

    }else if( ( currClientType == 34 || currClientType == 32 ) && cityExhib == "-1" ){
        toastr.warning('Please Select City for '+ ( (currClientType == "32") ? "Project" : "Exhibition" ));
        $('#itemSelect').click();
        $('#cityExhibList').focus();
        return false;

    }else if( ( currClientType == 34 || currClientType == 32 ) && exhibId == "-1" ){
        toastr.warning('Please Select '+ ( (currClientType == "32") ? "Project" : "Exhibition" ));
        $('#itemSelect').click();
        $('#exhibList').focus();
        return false;
    }

    if ($('#ContactData').css('display') == 'none') {
        custSearchFlag = true;
    } 

    if( temp_inq_Trans.length == 0 ){
        toastr.warning('Please Enter Items');
        $('#itemSelect').click();
        $('#itemName').focus();
        return false;
    
    }else if( custName.length == 0 ){
        toastr.warning('Please Enter Customer Name');
        $('#custDetail').click();
        $('#custSearch').focus();
        return false;
    
    // }else if( contPerson.length == 0 ){
    //     alert('Please Enter Contact Person Name');
    //     $('#contPerson').focus();
    //     return false;
    
    }else if( custSearchFlag == false && custcountryId == "-1"  ){
        toastr.warning('Please select Country');
        $('#custDetail').click(); 
        addFocusId( 'custCountryId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;
    
    }else if( custSearchFlag == false && custStateId == "-1"  ){
        toastr.warning('Please select State');
        $('#custDetail').click(); 
        addFocusId( 'custStateId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;
    
    }else if( custSearchFlag == false && custCityId == "-1" ){
        toastr.warning('Please select City');
        $('#custDetail').click(); 
        addFocusId( 'custCityId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;
    
    }else if( !checkValidMobile( "mobileNumber" ) ){
    // }else if( mobileNumber.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Mobile Number');
        $('#custDetail').click();
        $('#mobileNumber').focus();
        return false;
    
//    }else if( email.length == 0 ){
//        toastr.warning('Please Enter email');
//        $('#email').focus();
//        return false;
//    
    }else if( email.length != 0 && !validateEmail(email) ){
        toastr.warning('Please Enter Valid Customer Email');
        $('#custDetail').click();
        $('#email').focus();
        return false;
//    
    }else if( custAddress.length == 0){
        toastr.warning('Please Enter Customer Address');
        $('#custDetail').click();
        $('#custAddress').focus();
        return false; 

    }else if( contPerson.length == 0){
        toastr.warning('Please Enter Contact Name');
        $('#custDetail').click();
        $('#contPerson').focus();
        return false;  

    }else if( !checkValidMobile( "ContmobileNumber" ) ){
    // }else if( ContmobileNumber.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Contact mobile Number');
        $('#custDetail').click();
        $('#ContmobileNumber').focus();
        return false;

    }else if( contPersonEmail.length == 0 ){
        toastr.warning('Please Enter Contact Email');
        $('#custDetail').click();
        $('#contPersonEmail').focus();
        return false;
   
   }else if( !validateEmail(contPersonEmail) ){
        toastr.warning('Please Enter Valid Contact Email');
        $('#custDetail').click();
        $('#contPersonEmail').focus();
        return false;

    }else if( projectName.length == 0 ){
        toastr.warning('Please Enter Inquiry Name');
        $('#InqDetail').click();
        $('#projectName').focus();
        return false;
    
    }else if( inquirytype == "-1" ){
        toastr.warning('Please select Inquiry Type');
        $('#InqDetail').click();
        $('#inquirytype').focus();
        return false;
    
    }else if( inquirysource == "-1" ){
        toastr.warning('Please select Inquiry Source');
        $('#InqDetail').click();
        $('#inquirytype').focus();
        return false;

    }else if( !confInqCb && followupDate == "" ){
        toastr.warning('Please Select followup Date/Time');
        $('#InqDetail').click();
        $('#followupDate').datetimepicker('show');
        return false;
    
    // }else if( followUpTime == "" ){
    //     toastr.warning('Please Select followup Time');
    //     $('#dueTime').focus();
    //     return false;

    // }else if( !validateDateTimeFormat(followUpTime) ){
    //     toastr.warning('Please Select Proper followup Time eg:HH:MM (AM/PM) like 10:00 AM');
    //     return false;
    
    }else if( !confInqCb && remarks.length == 0 ){
        toastr.warning('Please Enter Remarks');
        $('#InqDetail').click();
        $('#remarks').focus();
        return false;

    // }else if( inquiryRemarks.length == 0 ){
    //     toastr.warning('Please Enter Inquiry Remarks');
    //     $('#inquiryRemarks').focus();
    //     return false;


    }else if( potAmt == "" ){
        toastr.warning('Please Enter Potential Amount');
        $('#InqDetail').click();
        $('#potAmt').focus();
        return false;

    }else if( closerBy == "" ){
        toastr.warning('Please Select Closure');
        $('#InqDetail').click();
        $('#closerBy').focus();
        return false;
    
    }else if( assUserId == "-1" ){
        toastr.warning('Please Assign User');
        $('#InqDetail').click();
        $('#assUser').focus();
        return false;
    
    }else{

        var followupDate = "";
        var followUpTime = "";

        if( $('#followupDate').val() != "" ){

            var followupDate = (( $('#followupDate').val() ).split('|')[0]).trim();
            var followUpTime = (( $('#followupDate').val() ).split('|')[1]).trim();

            var remindDateTime = ddmmyyToMysql(followupDate) +" "+followUpTime;
            remindTimeSend = minusFromTime( new Date( remindDateTime ) , 30 );

            var dueStart = new Date( ddmmyyToMysql(followupDate) +" "+followUpTime );
            var currTime = new Date( );
            
            if( +dueStart < +currTime ){
                toastr.warning("Followup Date/Time should not be less than Current Date/Time");
                return false;
            }

            var diffTime = ( (dueStart.getTime() - currTime.getTime())/ 1000 ) / 60;
            if( diffTime < 30 ){
                remindTimeSend = followUpTime;
            }
        }

        var today = new Date();
        var todayDMYdate = mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' );
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

        var closerByDateFormat = new Date( ddmmyyToMysql( closerBy )+" "+ time );
        if( ( todayDMYdate != closerBy ) && (+closerByDateFormat < +currTime) ){
            toastr.warning("Closure By Date should not be less than Current Date");
            $('#closerBy').datepicker('show');
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
       
        var catName = "INQUIRY";
        var statusInd = findIndexByKeyValue(data.data.statusListing,"statusCatName",catName);
       
        var priorityLevel = "1";
       
        var statusPriority = findIndexByKeyValue(data.data.statusListing[statusInd].statusListing,"statusPriority",priorityLevel);
        var inqStatus = data.data.statusListing[statusInd].statusListing[statusPriority].ID;

        var custType = "5"; // 5 - Other Type Customer  ||  4 - Corporate Type Customer
        if( contPerson != "" ){
            custType = "4";
        }

        if( custSearchFlag == true ){
            custCityId = '';
            custStateId = '';
        }

        //GENERATE CUSTOMER FNAME LNAME
        var custFName = "";
        var custLName = "";
        if( custName.split(" ").length == 1 ){
            custFName = custName.split(" ")[0];
            custLName = "";

        }else {

            custName.split(" ").forEach( function( nameRecrd , nameIndx ){
                if( nameIndx == 0 ){
                    custFName = nameRecrd;
                }else{
                    if( custLName ){
                        custLName += ' '+nameRecrd;
                    }else{
                        custLName = nameRecrd;
                    }
                }
            });
        }

        var custMaster = {
            custfName : custFName,
            custLName : custLName,
            custType : custType,
            custMobile : mobileNumber,
            custEmail : email,
            custEthnicity : "",
            contactName : contPerson,
            contactNumber : ContmobileNumber,
            phoneId : phoneId,
            cityId : custCityId,
            stateId : custStateId,
            countryId : custcountryId,
            custAddress : custAddress,
            contPersonEmail : contPersonEmail,
            contPersonDesig : contPersonDesig,
            custRateType : custRateType,
            catchArea : catchArea,
            gstTinNo : gstinNumber
        }
        
        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        var confirmInqFlag = ($('#confirmInq').is(':checked')) == true ? "1" : "0";
        if( roleAuth.data.confirmInqFlag == "1" && confirmInqFlag == "1" ){
            var statusConfirmFlag = findIndexByKeyValue(roleAuth.data.statusListing[statusInd].statusListing,"thirdPartyFlag","1");
            inqStatus = roleAuth.data.statusListing[statusInd].statusListing[statusConfirmFlag].ID;
        }

        // console.log(custMaster);return false;
        var inqMasterData = {
            inqDate : mysqltoDesiredFormat(new Date(),'yyyy-MM-dd'),
            sourceOf : inquirysource,
            followupFlag : "0",
            orgAssign : localStorage.indeCampusBranchIdForChkAuth,
            companyId : tmp_json.data[0].FK_CLIENT_ID,
            inqType : inquirytype,
            inqMode : "1", // MODE OF GENERATION
            inqModeType : "Normal Customer Inquiry", // INQ MODE WHERE INQ CREATE
            projectName : projectName,
            inquiryRemark : inquiryRemarks,
            inqAssignTo : assUserId,
            // contactId : 0,  // NA
            custId : custId,
            contactId :contPerId,
            inqStatus : inqStatus,
            inq_trans : temp_inq_Trans,
            custMaster : custMaster,
            closerBy : ddmmyyToMysql(closerBy),
            potAmt : potAmt,
            followUpTime : followUpTime,
            stateExhib : stateExhib,    // Both Exhibition & Project
            cityExhib : cityExhib,    // Both Exhibition & Project
            exhibId : exhibId,    // Both Exhibition & Project
            taxDiscDetail : GBLheaderLevelData,    // TAX DISCOUNT DETAIL
            smsCheck : smsCheck,    // SMS SEND
            emailCheck : emailCheck,    // MAIL SEND
        }
        
        // START GETTING CUSTOM FILED AND IT DATA IF SET FOR INQUIRY
        var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
        if(localJson.data.customFieldDetail != "No record found"){
            for(var f=0;f<localJson.data.customFieldDetail.length;f++){
                if(localJson.data.customFieldDetail[f]['tabType'] == "INQMASTER"){
                    var customFieldValue = "";
                    if(localJson.data.customFieldDetail[f]['fieldType'] == "radio"){
                        customFieldValue = $('input[name=field'+(localJson.data.customFieldDetail[f].priorityLevel)+']:checked').val();
                    }else if(localJson.data.customFieldDetail[f]['fieldType'] == "checkbox"){
                        var tempChck = '';
                        $('input[name="field'+(localJson.data.customFieldDetail[f].priorityLevel)+'"]:checked').each(function() {
                           if( tempChck.length == 0 ){
                                tempChck += $(this).val();
                           }else{
                                tempChck += ',' + $(this).val();
                           } 
                        });
                        if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && tempChck == "" ){
                            toastr.warning("Please Select "+ localJson.data.customFieldDetail[f].fieldName);
                            return false;
                        }else{
                            customFieldValue = tempChck; 
                        }
                    } else{
                        if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && ( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "" || $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "-1" ) ){
                            
                            var msg = "Please Enter "+ localJson.data.customFieldDetail[f].fieldName;
                            if( localJson.data.customFieldDetail[f].fieldType == "dropdown" || localJson.data.customFieldDetail[f].fieldType == "date"){
                                msg = "Please Select "+ localJson.data.customFieldDetail[f].fieldName;
                            }

                            toastr.warning( msg );
                            $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                            return false;
                        }else{

                            if( localJson.data.customFieldDetail[f]['fieldType'] == "email" && $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() != "" && !validateEmail( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() ) ){
                                toastr.warning( "Please Enter Valid Email Id in "+ localJson.data.customFieldDetail[f].fieldName );
                                $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                                return false;
                            }

                            customFieldValue = $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val();
                            // customFieldValue = $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)+' input[type=text]').val();
                        }
                    }
                    
                    inqMasterData['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = customFieldValue;
                }
            }
        }
        

        var reminderTimeFinal = "";
        var finalFollowupDate = "";
        if( $('#followupDate').val() != "" ){
            reminderTimeFinal = ddmmyyToMysql(followupDate)+ '|' +remindTimeSend;
            finalFollowupDate = ddmmyyToMysql(followupDate);
        }
        
        var postData = {
            requestCase: "createInquiry",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4, 13),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            followupDate: finalFollowupDate,
            followupRemark: remarks,
            inq_master : inqMasterData,
            reminderTime : reminderTimeFinal
        };
        // console.log(postData); return false;
        commonAjax(INQURL, postData, createInquiryTroughPopupCallBack,CREATINGINQDATA);
    }
    
}


function createInquiryTroughPopupCallBack(flag, data){
    if (data.status == "Success" && flag) {
        localStorage.removeItem('POTGTotalInqCount');
        toastr.success("Inquiry Created Successfully")
        //console.log(data);
        resetActiviytData();
        localStorage.removeItem( 'POTGselectedCustIdInSearch' );
        localStorage.setItem('POTGTotalInqCount', data.data.totalInqCount);
        console.log(data.data.totalInqCount);
        // return false;

        // SAVE RECECNT SELECTED OPTION FOR NEXT INQUIRY STARTS HERE

        var recentSelOpt = {
            stateIdExhibProj : $('#stateExhibList').val(),
            cityIdExhibProj : $('#cityExhibList').val(),
            listIdExhibProj : $('#exhibList').val(),
            inqName : $('#projectName').val(),
            inqRemark : $('#inquiryRemarks').val(),
            inqType : $('#inquirytype').val(),
            inqSource : $('#inquirysource').val(),
            followupRemark : $('#remarks').val(),
            followupDateTime : $('#followupDate').val(),
            potAmt : $('#potAmt').val(),
            closerBy : $('#closerBy').val(),
            confirmInq : (($('#confirmInq').prop('checked') == true) ? "1" : "0" ),  // 0 for CHECKED -- 1 for UNCHECKED
            inqAssignToId : $('#assUser').val(),
        }

        var jsonLastSelection = [];
        jsonLastSelection.push( recentSelOpt );
        localStorage.lastSelectedInqFields = JSON.stringify( jsonLastSelection );

        // SAVE RECECNT SELECTED OPTION FOR NEXT INQUIRY ENDS HERE

        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        var currClientType = roleAuth.data.clientTypeId;

        var confirmInqFlag = ($('#confirmInq').is(':checked')) == true ? "1" : "0";

        if( (currClientType == 34 || currClientType == 32 ) && roleAuth.data.confirmInqFlag == "1" && confirmInqFlag == "1" ){
            
            var exhibId = $('#exhibList').val();

            setTimeout(function(){
                if( currClientType == 32 ){
                    localStorage.projectId = exhibId;
                    navigateToRealEstateBookInq( exhibId , data.data.inqId , data.data.clientLevelInqId );
                }else{
                    localStorage.exhibitionId = exhibId;
                    localStorage.POTGinqIdForStallBook = data.data.inqId;
                    navigateToCreateStallsWithExhbId();
                }
            },300);

        }else{
            setTimeout(function(){
                $('#notifActivityCount').html(localStorage.POTGTotalInqCount);
                
                // window.location.href = "inquiry.html";
                localStorage.POTGclickInqId = data.data.inqId;
                navigateToInquiryDetailOnly();
            },300);
        }
        
    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_CREATE_INQ );
        else
            toastr.error(SERVERERROR);
    }
}


function removeRow(context){

    var deleteRow = confirm('Are You sure ? You want to delete this Item from Cart');

    if( !deleteRow ){
        return false;
    }
            
    var itemId = $(context).attr('item-id');
    var bomId = $(context).attr('bomid');

    var inqTrans = [];
    inqTrans = temp_inq_Trans;

    if( bomId != "" && bomId != "undefined" && bomId != undefined ){

        for( var i=temp_inq_Trans.length-1; i>=0; i-- ){
            var index = findIndexByKeyValue(temp_inq_Trans,"bomId",bomId);
            if( index != "-1" ){
                temp_inq_Trans.splice(index,1);
            }
        }
    }else{
        var index = findIndexByKeyValue(temp_inq_Trans,"itemId",itemId);    
        $(context).parents('tr').remove();
        temp_inq_Trans.splice(index,1);
    }

    addRowTable( temp_inq_Trans );
    callIntransaction();

    renderItemCatalog();
}

function autoCompleteForGroup(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#itemGroupText').attr('groupId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getGroupListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(13, 50),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMSGROUP= [];
                if (data.status != "No Item Group Found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.PK_ITEM_GROUP_ID,
                            name: data.GROUP_NAME,
                            data: data.GROUP_NAME,
                            clientId: data.FK_CLIENT_ID
                        }
                        itemGroupJson.push(tempJson);
                        ITEMSGROUP.push(tempJson);
                    });

                    return ITEMSGROUP;
                }
                console.log(data);
            }
        }
    });
}

function saveItemGroupDetail(){
    
}

function initCustomField(){
    
    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        var divStruct = '<div class="row">';
        for(var j=0;j<localJson.data.customFieldDetail.length;j++){
            if(localJson.data.customFieldDetail[j]['tabType'] == "INQMASTER"){
                divStruct += '<div class="col-lg-6">';
                if(localJson.data.customFieldDetail[j].fieldType == "text"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="" />';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "number"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="number" min="0" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value=""/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "email"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="email" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value=""/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<textarea maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'"></textarea>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
                    divStruct += '<div class="form-group radioer">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(g==0){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectpicker selectPckr">';
                    divStruct += '<option value="-1">-- '+localJson.data.customFieldDetail[j]['fieldName']+' --</option>';
                    for(var g=0;g<explodeValue.length;g++){
                        var classSeleted = "";
                        divStruct += '<option value="'+explodeValue[g]+'" '+classSeleted+'>'+explodeValue[g]+'</option>';
                    }
                    divStruct += "</select>";
                    $("#field"+(j+1)).selectpicker();
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" class="form-control normal-date-picker" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="" readOnly/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += requiredClass;
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+' '+disableClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span></br>";
                    }else{
                        requiredClass = "";
                    }
                    
                    divStruct += requiredClass;
                    divStruct += '<div class="form-group checkboxer">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ '" value="'+explodeValue[g]+'"/> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
                }
                divStruct += '</div>'+
                '</div>';
                if(((j+1)%2) == 0){
                    divStruct += '</div><div class="row">';

                }   
                if( localJson.data.clientListing[0].inqCustomLevelTab != "" ){
                    if( localJson.data.clientListing[0].inqCustomLevelTab == "inqCustomLevelTab" ){
                        $("#customFieldDivCustDetail").show();

                    }else if( localJson.data.clientListing[0].inqCustomLevelTab == "inqItemLevelTab" ){
                        $("#customFieldDivItemDetail").show();

                    }else if( localJson.data.clientListing[0].inqCustomLevelTab == "inqDetailLevelTab" ){
                        $("#customFieldDivInqDetail").show();
                    }
                }
            }
            
        }

        if( localJson.data.clientListing[0].inqCustomLevelTab != "" ){
            if( localJson.data.clientListing[0].inqCustomLevelTab == "inqCustomLevelTab" ){
                $("#customFieldDivCustDetail").html(divStruct);

            }else if( localJson.data.clientListing[0].inqCustomLevelTab == "inqItemLevelTab" ){
                $("#customFieldDivItemDetail").html(divStruct);

            }else if( localJson.data.clientListing[0].inqCustomLevelTab == "inqDetailLevelTab" ){
                $("#customFieldDivInqDetail").html(divStruct);
            }
        }
        $('.normal-date-picker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    }
}


function populateCity(stateId){
    
    if(stateId == "-1"){
        setOption("0", "custCityId", "", "--Select City --");
    }else{
        setOption("0", "custCityId", "", "--Select City --");
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(data.data.cityStateListing,"ID",stateId);
        cityListing = "";
        if(index != -1){
            var cityListing  = data.data.cityStateListing[index]['cityListing'];
        }
        if( cityListing != "No record found" ){
            setOption("0", "custCityId", cityListing, "--Select City --");  
        }
    }
    
    $("#custCityId").selectpicker("refresh");    
}

function tabChange(context){
    if( $(context).attr('id') == "itemSelect" ){
        addFocusId( 'itemName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    
    }else if( $(context).attr('id') == "custDetail" ){
        // addFocusId( 'custSearch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    }else if( $(context).attr('id') == "InqDetail" ){
        addFocusId( 'projectName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    }else if( $(context).attr('id') == "catalogView" ){
        addFocusId( 'projectStateForItem' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

function fieldsChange(selectedValue){
    var itemSelectionDiv = "";
    var className = "display: none";
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    if(selectedValue == 1 ){
        for(var i=0;i<roleAuth.data.clientConfig.length;i++){
            if(roleAuth.data.clientConfig[i].FLAG == "1" && roleAuth.data.clientConfig[i].TYPE_OF_CONFIG == "INQGROUP"){
                className = "display: block";
            }
        } 

        //CHECK DECIMAL ALLOWED OR NOT
        // roleAuth.data.decimalQtyFlag = 1;
        var inputQtyHtml = '<input type="number" id="itemQty" value="1" class="onlyNumber form-control"  placeholder="Quantity (eg. 1)"/>';
        if( roleAuth.data.decimalQtyFlag == "1" ){
            inputQtyHtml = '<input type="number" id="itemQty" min=".01" step=".01" value="1" class="onlyNumber form-control"  placeholder="Quantity (eg. 1)"/>';
        }
        
        itemSelectionDiv += '<div class="col-lg-3 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Type</label>'+
                                            '<select id="itemType" onchange="fieldsChange(this.value)" data-width="100%">'+
                                                '<option value="1" class="fa fa-cube">&nbsp;&nbsp;I</option>'+
                                                '<option value="2" class="fa fa-file-text-o">&nbsp;&nbsp; T</option>'+
                                                '<option value="3" class="fa fa-sitemap">&nbsp;&nbsp;B</option>'+
                                            '</select>'+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-6 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-cube"></i> '+
                                            '<label class="control-label">Item Name</label>'+
                                            '<input type="text" id="itemName" class="form-control" autocomplete="off" placeholder="Search Item (eg. Pencil)"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-3 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-rupee"></i> '+
                                            '<label class="control-label">Item Price </label>'+
                                            '<input type="text" id="itemPrice" class="form-control onlyNumberWithDec" autocomplete="off" placeholder="Price (eg. 500)" />'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-sm-6" id="itemGroup" style="'+className+'">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-cubes"></i> '+
                                            '<label class="control-label">Item Group </label>'+
                                            '<input type="text" id="itemGroupText" class="form-control" autocomplete="off"  placeholder="Search Group (eg. Groups)"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-2 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-hand-peace-o"></i> '+
                                            '<label class="control-label">Quantity </label>'+
                                            inputQtyHtml +
                                        '</div>'+
                                    '</div>'+  
                                    '<div class="col-lg-5 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<i class="fa fa-file-text-o"></i> '+
                                            '<label class="control-label">Item Remark </label>'+
                                            '<input type="text" id="itemLevelRemark" class="form-control" autocomplete="off"  placeholder="Enter Item Remark"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-1 col-md-1 col-sm-1">'+                                  
                                    '<a href="javascript:void(0)" class="btn btn-light-green" onclick="addItemRow();" style="margin-top:25px"><i class="fa fa-plus"></i></a></div>'; 
    }else if(selectedValue == 2){
        itemSelectionDiv += '<div class="col-lg-3 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Type</label>'+
                                            '<select id="itemType" onchange="fieldsChange(this.value)" data-width="100%">'+
                                                '<option value="1" class="fa fa-cube">&nbsp;&nbsp;I</option>'+
                                                '<option value="2" class="fa fa-file-text-o">&nbsp;&nbsp; T</option>'+
                                                '<option value="3" class="fa fa-sitemap">&nbsp;&nbsp;B</option>'+
                                            '</select>'+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-8 col-sm-5">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label"><i class="fa fa-file-text-o"></i> Section Name</label>'+
                                            '<input type="text" id="itemHeaderText" class="form-control" autocomplete="off" placeholder="Section (eg. xyz)"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-1 col-md-1 col-sm-1">'+ 
                                    '<a href="javascript:void(0)" class="btn btn-light-green" onclick="addItemRow();" style="margin-top:25px"><i class="fa fa-plus"></i></a></div>'; 
    }else{
        itemSelectionDiv += '<div class="col-lg-3 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Type</label>'+
                                            '<select id="itemType" onchange="fieldsChange(this.value)" data-width="100%">'+
                                                '<option value="1" class="fa fa-cube">&nbsp;&nbsp;I</option>'+
                                                '<option value="2" class="fa fa-file-text-o">&nbsp;&nbsp; T</option>'+
                                                '<option value="3" class="fa fa-sitemap">&nbsp;&nbsp;B</option>'+
                                            '</select>'+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-8 col-sm-5">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label"><i class="fa fa-sitemap"></i> BOM Search</label>'+
                                            '<input type="text" id="itemName" class="form-control" autocomplete="off" placeholder="Search BOM (eg. Computer)"/>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-1 col-md-1 col-sm-1">'+ 
                                    '<a href="javascript:void(0)" class="btn btn-light-green" onclick="addItemRow();" style="margin-top:25px"><i class="fa fa-plus"></i></a></div>'; 

    }
    
    $("#itemSelectionDiv").html(itemSelectionDiv);
    
    if( selectedValue == 3 ){
        AutoCompleteForItem("itemName", openBOMDetail);                    
    }else{
        AutoCompleteForItem("itemName", saveItemDetail);
    }
    
    if(className == "display: block"){
        autoCompleteForGroup("itemGroupText", saveItemGroupDetail);
        AutoCompleteForItem("itemName", saveItemDetail);
    }

    $("#itemType").val(selectedValue); 
    $("#itemType").selectpicker(); 

    // it allow only number and plus symbol just place in input fields
    if( roleAuth.data.decimalQtyFlag != "1" ){
        $('.onlyNumber').keypress(function (e) {
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                       return false;
            }
        });
    }
}

function openBOMDetail(){
    console.log(bomJson);
    var startHtml = "<table id='BomListTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>Sr No.</th>"+
                            "<th>Item Name</th>"+
                            "<th>Item Price</th>"+
                            "<th>Group Name</th>"+
                            "<th>UOM Name</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";
    var inqRow = '';
    var bomListData = '';

    for( var i=0; i<bomJson.length; i++ ){
        bomListData += '<tr>'+
                            '<td>'+ (i+1) +'</td>'+
                            '<td>'+ bomJson[i].itemName +'</td>'+
                            '<td>'+ bomJson[i].itemPrice +'</td>'+
                            '<td>'+ bomJson[i].groupName +'</td>'+
                            '<td>'+ bomJson[i].uomName +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" onclick="DeleteBomItem(this)" item-Id="'+ bomJson[i].itemId +'" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';   
    }
    $('#bomListData').html( startHtml + bomListData + endHtml);
    $('#BomListTable').DataTable();
    $('#addBOM').modal('show');
}

function DeleteBomItem(context){

    var itemId = $(context).attr('item-Id');
    var index = findIndexByKeyValue( bomJson , 'itemId' , itemId );

    if( index != '-1' ){
        bomJson.splice(index,1);
    }

    openBOMDetail();
}

function saveBomDetails(){

    // CHECK VALIDATION
    for( var i=0; i<temp_inq_Trans.length; i++ ){
        if(temp_inq_Trans[i].itemType == "ITEM"){
            for( var j=0; j<bomJson.length; j++ ){
                var newIndex = findIndexByKeyValue(temp_inq_Trans,"itemId",bomJson[j].itemId);
                if(newIndex != "-1"){
                    toastr.error('Some/All Item of BOM is already available in the cart');
                    $('#itemName').val('');
                    $('#addBOM').modal('hide');
                    return false;
                }
            }
        }
    }

    var sequenceId = (temp_inq_Trans.length+1);
    var  tempData = {
        cancleFlag : "1",
        itemId : $('#itemName').attr('itemid'),
        bomId : $('#itemName').attr('itemid'),
        itemName : $('#itemName').val(),
        groupName : "",
        itemQty : "",
        eventStatus : "",
        itemPrice : 0,
        itemBasePrice : 0,
        itemType : "BOM",
        itemRemark : "",
        itemCat : 3, // 1 = item 2 - header text 3 - BOM
        sequenceId : sequenceId 
    }
    temp_inq_Trans.push(tempData);

    for( var i=0; i<bomJson.length; i++ ){
        sequenceId = (temp_inq_Trans.length+1);
        var  tempData = {
            cancleFlag : "1",
            itemId : bomJson[i].itemId,
            bomId : $('#itemName').attr('itemid'),
            itemName : bomJson[i].itemName,
            groupName : bomJson[i].groupName,
            itemQty : 1,
            eventStatus : "",
            itemPrice : (Number( bomJson[i].itemPrice * 1)).toFixed(2),
            itemBasePrice : bomJson[i].itemPrice,
            itemType : "ITEM",
            itemRemark : '',
            itemCat : 1, // 1 = item 2 - header text
            sequenceId : sequenceId 
        }
        temp_inq_Trans.push( tempData );
    } 
    addRowTable( temp_inq_Trans , $('#itemName').attr('itemid') );
    $('#itemName').val('');
    $('#itemName').attr('itemid','');
    $('#addBOM').modal('hide');

    callIntransaction();
}

function autoCompleteForContactPerson(id,callback) {
    
    var source = [];
    if( contactPerSonData.length != 0 ){
        for( var i=0; i<contactPerSonData.cpAllList.length; i++ ){
            source.push( contactPerSonData.cpAllList[i].CONTACTNAME+ ':' + contactPerSonData.cpAllList[i].CONTACT_NUMBER+ ':' + contactPerSonData.cpAllList[i].CONTACT_DESIGN+ ':' + contactPerSonData.cpAllList[i].CONTACT_EMAIL );
        }
    }
    
    $("#" + id).typeahead({
        onSelect: function (item) {
            console.log(item);
            setTimeout(function(){
                var contName = (item.value).split(':')[0];
                var contMobile = (item.value).split(':')[1];
                var contDesig = (item.value).split(':')[2];
                var contEmail = (item.value).split(':')[3];
                $('#contPerson').val(contName);  
//                $('#ContmobileNumber').val(contMobile);  
                
                setNumberInIntl( 'ContmobileNumber' , contMobile );
                $('#contPersonDesig').val(contDesig);  
                $('#contPersonEmail').val(contEmail);  
            },50);
        },
        source: source,
    });
}

function saveContactPerDetail(){

}

var lastDate = '';
function callIntransFlwp(type){

    var flwpDate = $('#followupDate').val();
    var selectedUserIdTrans = $('#assUser').val();
    if( type != 'noCheck' ){
        if( lastDate == flwpDate ){
            return false;
        }
        lastDate = flwpDate;
    }
    
    flwpDate = ddmmyyToMysql( ((flwpDate).split('|')[0]).trim() );

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
         requestCase: "intransactionInqDetailSearch",
         clientId: tmp_json.data[0].FK_CLIENT_ID,
         userId: tmp_json.data[0].PK_USER_ID,
         orgId: checkAuth(4, 13),
         followupDate: flwpDate,
         selectedUserId: selectedUserIdTrans,
         custId: custId,
         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(URL1, postData, callIntransInqCallBack,CREATINGINQDATA);

    function callIntransInqCallBack(flag, data){
        
         if (data.status == "Success" && flag) {
            
            var flwpData = data.data[0];

            $('#todayFlwp').html( flwpData.totalPendingFollowup );
            if( flwpData.totalPendingFollowup != "0" ){
                $('#moreInfoBtn').show();
            }else{
                $('#moreInfoBtn').hide();
            }
            $('#todayFlwpDn').html( flwpData.totalDoneFollowup );
            $('#avgFlwp').html( flwpData.avgFollowupRequird );
            
         }else {
             if (flag)
                 console.log(data.status);
             else
                 console.log(SERVERERROR);
         }
    }
}

function getEmpListingData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userListingActivityModule',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2,6),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getEmpListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getEmpListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // console.log(empData);return false;
            var brListOpt = '';
            for( var i=0;i<data.data.length; i++ ){
                if( data.data[i].userBranchId.search( localStorage.indeCampusBranchIdForChkAuth ) > "-1" ){
                    if( tmp_json.data[0].PK_USER_ID == data.data[i].userId ){
                        brListOpt += '<option value="'+ data.data[i].userId +'" selected>'+ data.data[i].fullName +'</option>';
                    }else{   
                        brListOpt += '<option value="'+ data.data[i].userId +'">'+ data.data[i].fullName +'</option>';
                    }
                }
            }
            $('#assUser').html( brListOpt );
            $('#assUser').selectpicker();

            if( localStorage.POTGCopyInqId != undefined && localStorage.POTGCopyInqId != "" ){
                getinquiryDetailListing();
                localStorage.removeItem('POTGCopyInqId');
                toastr.warning('Please Do not Refresh this page');
            }

            setCountryList( 'custCountryId' );   // SET COUNTRY LISTING FROM LOAD AUTH SET DATA IN SELECT PICKER USING ID

        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        }

        setRecentInpInFields();
        $('.disEnaCls').selectpicker('refresh');

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }
}

function setRecentInpInFields(){

    if( localStorage.lastSelectedInqFields != undefined && localStorage.lastSelectedInqFields != "" ){
        var recentSelectedFields = JSON.parse( localStorage.lastSelectedInqFields )[0];
        
        $('#stateExhibList').val( recentSelectedFields.stateIdExhibProj );
        $('#stateExhibList').selectpicker('refresh');
        getCity(recentSelectedFields.exhibState);
        $('#cityExhibList').val( recentSelectedFields.cityIdExhibProj );
        $('#cityExhibList').selectpicker('refresh');
        getExhibProject(recentSelectedFields.cityIdExhibProj);
        $('#exhibList').val( recentSelectedFields.listIdExhibProj );
        $('#exhibList').selectpicker('refresh');

        //CATALOG SCREEN LAST SELECTED
        $('#projectStateForItem').val( recentSelectedFields.stateIdExhibProj );
        $('#projectStateForItem').selectpicker('refresh');
        getProjCityForItem(recentSelectedFields.exhibState);
        $('#projectCityForItem').val( recentSelectedFields.cityIdExhibProj );
        $('#projectCityForItem').selectpicker('refresh');
        getProjListForItem(recentSelectedFields.cityIdExhibProj);
        $('#projectForItem').val( recentSelectedFields.listIdExhibProj );
        $('#projectForItem').selectpicker('refresh');

        getAllItems();  // CALL API OF ITEM LISTING WHEN CLIENT TYPE IS REAL

        //Inquiry Tab
        $('#projectName').val( recentSelectedFields.inqName );
        $('#inquiryRemarks').val( recentSelectedFields.inqRemark );
        $('#inquirytype').val( recentSelectedFields.inqType );
        $('#inquirytype').selectpicker('refresh');
        $('#inquirysource').val( recentSelectedFields.inqSource );
        $('#inquirysource').selectpicker('refresh');
        $('#remarks').val( recentSelectedFields.followupRemark );
        $('#followupDate').val( recentSelectedFields.followupDateTime );
        $('#potAmt').val( recentSelectedFields.potAmt );
        $('#closerBy').val( recentSelectedFields.closerBy );
        $('#assUser').val( recentSelectedFields.inqAssignToId );
        $('#assUser').selectpicker('refresh');

        // if( recentSelectedFields.confirmInq == "1" ){
        //     $('#confirmInq').prop('checked',true);
        // }
    }
}

function callIntransUser(){

    var flwpDate = $('#followupDate').val();
    if( flwpDate == "" ){
        toastr.warning('Please Select Followup Date to get Trans Data of this User');
        $('#followupDate').datetimepicker('show');
        return false;
    }
    callIntransFlwp('noCheck');
}

function resetExhibList(){

    $('#stateExhibList').val('-1');
    $('#stateExhibList').selectpicker('refresh');
    $('#cityExhibList').val('-1');
    $('#cityExhibList').selectpicker('refresh');
    $('#exhibList').val('-1');
    $('#exhibList').selectpicker('refresh');
}

function pageInitialEvents(){

    initTelInput( 'mobileNumber' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput( 'ContmobileNumber' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    
    $('#mobileNumber').blur(function(){
        if( custId == "-2" ){
//            $('#ContmobileNumber').val( $(this).val() );
              setNumberInIntl( 'ContmobileNumber' , $(this).val() );
        }
    });

    $('#email').blur(function(){
        if( custId == "-2" ){
            $('#contPersonEmail').val( $(this).val() );
        }
    });

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    $("#custSearch").keypress(function (e) {
        if( $("#custSearch").val() == "" ){
            if (e.keyCode == 13) {
                $('#ContactData').show();
                $('#contPerson').val('');
                $('#email').val('');
                $('#custAddress').val('');
                $('#custCatchArea').val('');
//                $('#ContmobileNumber').val('+91');
                
//                $('#mobileNumber').val('+91');
                $('#custStateId').val('-1');
                $('#custCityId').val('-1');
                $('#custStateId').selectpicker('refresh');
                $('#custCityId').selectpicker('refresh');
                $('#contPersonEmail').val('');
                $('#contPersonDesig').val('');
                $('#inquiryRateType').val('');
                $('#inquiryRateType').selectpicker('refresh');
                $('#gstinNumber').val('');
                
                if( roleAuth.data.itemRateTypeFlag == "1" ){
                    temp_inq_Trans = [];
                    addRowTable(temp_inq_Trans);
                }

                $('#inquiryRateTypeDiv').show();
            }
        }
    });

    $('#custSearch').keyup(function (e) {
        if (e.which == 8) {
            if( $('#custSearch').val() == "" ){
                $('#custSearch').val('');
                $('#email').val('');
                $('#custAddress').val('');
                $('#custCatchArea').val('');
//                $('#mobileNumber').val('+91');
                $('#contPerson').val('');
//                $('#ContmobileNumber').val('+91');
                $('#contPersonEmail').val('');
                $('#contPersonDesig').val('');
                $('#inquiryRateType').val('');
                $('#inquiryRateType').selectpicker('refresh');
                $('#inquiryRateTypeDiv').show();
                custId = "-2";
                phoneId = "-2";
                if( roleAuth.data.itemRateTypeFlag == "1" ){
                    temp_inq_Trans = [];
                    addRowTable(temp_inq_Trans);
                }
                $("#contPerson").data('typeahead').source = [];
            }
        }
    });

    // var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    // setOption("0", "custStateId", roleAuth.data.cityStateListing, "--Select State --");

    // setOption("0", "closerBy", roleAuth.data.closureDays, "--Select Closure Days --");
    // $('#closerBy').selectpicker();
    $("#custStateId").selectpicker();    
    $("#custCityId").selectpicker(); 
    $("#itemType").selectpicker(); 

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    console.log(currClientType);
    GBL_CLIENT_TYPE = currClientType;
    
    if( currClientType == 34 || currClientType == 32 ){
        $('#exhibModule').show();
        getExhibProjectData( currClientType );
        addFocusId( 'projectStateForItem' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }else{
        getEmpListingData();
        addFocusId( 'itemName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    } 

    if( data.data.itemRateTypeFlag == "1" ){

        var rateTypeInqHeadTab = '<ul class="colorTab nav nav-tabs nav-justified" role="tablist">'+
                                    '<li class="active" id="custDetail" href="#tab4_2" data-toggle="tab" onclick="tabChange(this)"><a href="javascript:void(0)">Customer Detail</a></li>'+
                                    '<li id="catalogView" href="#tab4_4" data-toggle="tab" onclick="tabChange(this)" style="display:none"><a href="javascript:void(0)">Catalog View</a></li>'+
                                    '<li id="itemSelect" href="#tab4_1" data-toggle="tab" onclick="tabChange(this)"><a href="javascript:void(0)">Item Selection</a></li>'+
                                    '<li id="InqDetail" href="#tab4_3" data-toggle="tab" onclick="tabChange(this)"><a href="javascript:void(0)">Inquiry Detail</a></li>'+
                                '</ul>';
        $('#inqHeadTab').html( rateTypeInqHeadTab );
        $('#inqHeadTab ul .active').click();
        addFocusId( 'custSearch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }

    //CHECK DECIMAL ALLOWED OR NOT
    // data.data.decimalQtyFlag = 1;
    if( data.data.decimalQtyFlag == "1" ){
        $('#itemQtyHead').html('');
        $('#itemQtyHead').html('<input type="number" min=".01" step=".01" id="itemQty" value="1" class="form-control" placeholder="Quantity (eg. 1)">');
    }

    //CHECK SMS / EMAIL FLAG IS ON OR OFF
    // data.data.smsCheck = 1;
    if( data.data.smsCheck == "1" ){ 
        $('#smsSendDiv').show();
    }

    //CHECK SMS / EMAIL FLAG IS ON OR OFF
    // data.data.emailCheck = 1;
    if( data.data.emailCheck == "1" ){ 
        $('#emailSendDiv').show();
    }


    // it allow only number and plus symbol just place in input fields
    $('.onlyNumber').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });

    if( currClientType == 32 ){
        $('#catalogView').show();
        // $('#tab4_4').show();
        addRowTable([]);
        addFocusId( 'projectStateForItem' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }else{
        $('#catalogView').remove();
        $('#tab4_4').remove();

        if( data.data.itemRateTypeFlag == "1" ){
            $('#custDetail').click(); 
            addFocusId( 'custSearch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        }else{
            $('#itemSelect').click(); 
            addFocusId( 'itemName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        }
    }

    if( localStorage.POTGCreateInqCustId != "" && localStorage.POTGCreateInqCustId != undefined ){
        //Bind Customer
        
        localStorage.removeItem('POTGCreateInqCustId');

        if( localStorage.custInqRedData != "" && localStorage.custInqRedData != undefined ){
            var custInqData = JSON.parse( localStorage.custInqRedData );

            console.log(custInqData)

            $('#custSearch').val( custInqData[0].custName );

            // $('#custStateId').val( custInqData[0].custState );
            // $('#custStateId').selectpicker('refresh');
            // populateCity( custInqData[0].custState );
            // $('#custCityId').val( custInqData[0].custCity ); 
            // $('#custCityId').selectpicker('refresh');

            setTimeout( function(){
                // STATIC DATA SET FOR LOAD STARTS 
                ITEMS = [];
                GBL_INDEX = 0;
                var temp = {
                    stateId : custInqData[0].custState,
                    cityId : custInqData[0].custCity,
                }
                ITEMS.push( temp );
                // STATIC DATA SET FOR LOAD STARTS
                
                $('#custCountryId').val( custInqData[0].custCountry ).trigger("change");
                $('#custCountryId').selectpicker('refresh');
            },1000);

            custId = custInqData[0].custId;
            phoneId = custInqData[0].phoneId;
            contPerId = custInqData[0].custContPerId;

            //$('#mobileNumber').val( custInqData[0].custMobileNo );
            // $("#mobileNumber").intlTelInput("setNumber", custInqData[0].custMobileNo);
            setNumberInIntl( 'mobileNumber' , custInqData[0].custMobileNo );
            $('#custAddress').val( custInqData[0].custAddress );
            $('#contPerson').val( custInqData[0].custContactName );

            //SET CONTACT PERSON SEARCH
            contactPerSonData = { 
                "cpAllList" : custInqData[0].contactData,
            }
            
            autoCompleteForContactPerson("contPerson", saveContactPerDetail);

//            $('#ContmobileNumber').val( custInqData[0].custContactNo );
            $('#gstinNumber').val( custInqData[0].custGstinNo );

            setNumberInIntl( 'ContmobileNumber' , custInqData[0].custContactNo );
            $('#email').val( custInqData[0].custEmail );
            $('#custCatchArea').val( custInqData[0].custCatchArea );
            $('#contPersonDesig').val( ( (custInqData[0].custContDesg == "--") ? " " : custInqData[0].custContDesg ) );
            $('#contPersonEmail').val( ( (custInqData[0].custContEmail == "--") ? " " : custInqData[0].custContEmail ) );

            localStorage.removeItem('custInqRedData');
            callIntransCustomer( custId );
        }
    }

    if( localStorage.leadCustData != "" && localStorage.leadCustData != undefined ){

        var custInqData = JSON.parse( localStorage.leadCustData );
        $('#custSearch').val( custInqData.fName +" "+ custInqData.lName );
        // $('#custStateId').val( custInqData.stateId );
        // $('#custStateId').selectpicker('refresh');
        // populateCity( custInqData.custState );
        // $('#custCityId').val( custInqData.cityId ); 
        // $('#custCityId').selectpicker('refresh');

        custId = custInqData.custId;
        phoneId = custInqData.phoneId;

        setTimeout( function(){
            GBL_INDEX = 0;
            tempData = {
                stateId : custInqData.stateId,
                cityId : custInqData.cityId
            }
            ITEMS.push( tempData );
            
            if( custInqData.countryId != "" && custInqData.countryId != "0" ){
                $('#custCountryId').val( custInqData.countryId ).trigger("change");
                $('#custCountryId').selectpicker('refresh');
            }
        },500);
        
//        $('#mobileNumber').val( custInqData.custMobile );
//        $("#mobileNumber").intlTelInput("setNumber", custInqData.custMobile);
        setNumberInIntl( 'mobileNumber' , custInqData.custMobile );
        $('#custAddress').val( custInqData.custAddress ); 
        $('#contPerson').val( custInqData.cpName );   
        //$('#ContmobileNumber').val( custInqData.cpMobile );    
//        $("#ContmobileNumber").intlTelInput("setNumber", custInqData.cpMobile);  
        setNumberInIntl( 'ContmobileNumber' , custInqData.cpMobile );
        $('#email').val( custInqData.custEmail );
        $('#custCatchArea').val( custInqData.catchArea );   
        $('#contPersonDesig').val( ( (custInqData.contPerDesig == "--") ? " " : custInqData.contPerDesig ) );    //REMAIN
        $('#contPersonEmail').val( ( (custInqData.contPerEmail == "--") ? " " : custInqData.contPerEmail ) );    //REMAIN
        localStorage.removeItem('leadCustData');

        custId = custInqData.customerId;
        phoneId = custInqData.phoneId;
    }

    $('#email').blur(function(){
        $('#contPersonEmail').val( $(this).val() );
    });

    if( data.data.itemRateTypeFlag == "1" ){
        $('#inquiryRateTypeDiv').show();
        setOption("0", "inquiryRateType", data.data.itemRateType, "-- Rate Type --");
        $("#inquiryRateType").selectpicker();
    }

    if( data.data.confirmInqFlag == "1" ){
        $('#confirmInqFlag').show();
    }

    if( localStorage.POTGinqCreateTour == 1 ){
        initSelfTour();
        localStorage.POTGinqCreateTour = 0;
    }

}

var itemDetailData = [];
function openDetailModal( itemDetail ){
    itemDetailData = itemDetail;

    var itemDetailInput = "";

    itemDetail.forEach( function( record , index ){

        if( record.inputType == "LIST" ){
            itemDetailInput += '<div class="col-lg-12">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+ record.title +'</label>'+
                                        '<select data-width="100%" id="select_'+ record.key +'">';
                                        record.children.forEach( function( innrRecord , innrIndex ){
                                            itemDetailInput += '<option key="'+ innrRecord.key +'" inpTitle="'+ innrRecord.title +'" value="'+ innrRecord.price +'">'+ innrRecord.title +' -- '+ innrRecord.price +'</option>';
                                        });
                                        itemDetailInput += '</select>'+
                                    '</div>'+
                                '</div>';

        }else if( record.inputType == "TEXT" ){
            itemDetailInput += '<div class="col-lg-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+ record.title +'</label>'+
                                        '<input type="text" id="inputTxt_'+ record.children[0].key +'" key="'+ record.children[0].key +'" inpTitle="'+ record.children[0].title +'" value="'+ record.title +'" class="form-control" autocomplete="off" placeholder="'+ record.children[0].title +'"/>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label"> Rate </label>'+
                                        '<input type="text" id="inputRate_'+ record.key +'" key="'+ record.key +'" inpTitle="'+ record.title +'" value="'+ record.children[0].price +'" class="onlyNumber form-control" autocomplete="off" placeholder="'+ record.children[0].price +'"/>'+
                                    '</div>'+
                                '</div>';
        }
    });
    $('#itemDetailData').html( itemDetailInput );
    $('#addItemDetailModal select').selectpicker();

    $('#addItemDetailModal').modal('show');
}

var detailJSON = [];
function saveItemDetailing(){

    var itemPrice = parseFloat($('#itemPrice').val());
    var inpPrice = 0;
    detailJSON = [];
    itemDetailData.forEach( function( record , index ){
        tempJson = [];
        if( record.inputType == "LIST" ){
            inpPrice = $("#select_"+ record.key).val();

            var childJson = [{
                inputType: record.inputType,
                key: $("#select_"+ record.key + ' option:selected').attr('key'),
                price: inpPrice,
                title: $("#select_"+ record.key + ' option:selected').attr('inptitle'),
            }]

            tempJson = {
                children : childJson,
                inputType : record.inputType,
                key : record.key,
                title : record.title,
            }

            detailJSON.push( tempJson );

        }else if( record.inputType == "TEXT" ){
            inpPrice = $("#inputRate_"+ record.key).val();
            if( inpPrice == "" || inpPrice == undefined ){
                toastr.warning('Please Enter Valid rate to this Item Option');
                $("#inputRate_"+ record.key).focus();
                setTimeout(function(){
                    $('#addItemDetailModal').modal('show');
                    return false;
                },200);
            }

            var childJson = [{
                inputType: record.inputType,
                key: $("#inputRate_"+ record.key).attr('key'),
                price: inpPrice,
                title: $("#inputTxt_"+ record.children[0].key).val(),
            }]

            tempJson = {
                children : childJson,
                inputType : record.inputType,
                key : record.key,
                title : record.title,
            }

            detailJSON.push( tempJson );
        }
        itemPrice = parseFloat( itemPrice ) + parseFloat( inpPrice ); 
    });
    $('#itemPrice').val(itemPrice);
    $('#addItemDetailModal').modal('hide');
    // console.log(detailJSON);
}

function openVCDialog(context,index){

    var itemId = $(context).attr('item-id');
    // var index = temp_inq_Trans[index];
    // var index = findIndexByKeyValue( temp_inq_Trans , "itemId" , itemId );
    var itemDetailData = [];
    if( index != "-1" ){
        itemDetailData = temp_inq_Trans[index].itemDetail;

        if( itemDetailData != "No record found" && itemDetailData != "" && itemDetailData != null ){
            var startHtml = "<table id='VCItemTable' class='display datatables-alphabet-sorting'>"+
                        "<thead>"+
                            "<tr>"+
                                // "<th>Sr No.</th>"+
                                // "<th>Input Type</th>"+
                                "<th>Property</th>"+
                                "<th>Option</th>"+
                                "<th>Price</th>"+
                            "</tr>"+
                        "</thead>"+
                        "<tbody>";
            var endHtml = "</tbody>";
            var inqRow = '';
            var bomListData = '';
            
            itemDetailData.forEach( function( record , index ){
                bomListData += '<tr>'+
                                // '<td>'+ (index+1) +'</td>'+
                                // '<td>'+ record.inputType +'</td>'+
                                '<td>'+ record.title +'</td>'+
                                '<td>'+ record.children[0].title +'</td>'+
                                '<td>'+ record.children[0].price +'</td>'+
                            '</tr>';  
            });
            $('#showItemDetailData').html( startHtml + bomListData + endHtml);
            $('#VCItemTable').DataTable();

            $('#showItemDetailModal').modal('show');
        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_DETAILING );
        }
    }
}

function getinquiryDetailListing(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : localStorage.POTGclickInqId,
    }

    commonAjax(COMMONURL, postData, inquiryDetailCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryDetailCallback(flag,data){

    if (data.status == "Success" && flag) {
    
        console.log(data);
        inqData = data.data[0];

        // Item Tab
        temp_inq_Trans = [];
        inqData.inqTransDetail.forEach( function( record , index ){
            var  tempData = {
                cancleFlag : "1",
                itemId : record.itemId,
                itemName : record.itemName,
                groupName : record.groupName,
                itemQty : record.itemQty,
                eventStatus : "",
                itemPrice : record.itemPrice,
                itemBasePrice : record.itemBasePrice,
                itemType : record.typeOfItem,
                itemRemark : record.itemRemark,
                itemCat : record.itemCat, // 1 = item 2 - header text
                sequenceId : record.sequenceId,
                itemDetail : record.detailing,
            }
            temp_inq_Trans.push(tempData); 
        });
        addRowTable( temp_inq_Trans );
        
        $('#stateExhibList').val( inqData.exhibState );
        $('#stateExhibList').selectpicker('refresh');

        $('#projectStateForItem').val( inqData.exhibState );
        $('#projectStateForItem').selectpicker('refresh');

        getCity(inqData.exhibState);
        $('#cityExhibList').val( inqData.exhibCity );
        $('#cityExhibList').selectpicker('refresh');

        getProjCityForItem(inqData.exhibState);
        $('#projectCityForItem').val( inqData.exhibCity );
        $('#projectCityForItem').selectpicker('refresh');

        getExhibProject(inqData.exhibCity);
        $('#exhibList').val( inqData.exhibId );
        $('#exhibList').selectpicker('refresh');

        getProjListForItem(inqData.exhibCity);
        $('#projectForItem').val( inqData.exhibId );
        $('#projectForItem').selectpicker('refresh');

        getAllItems();  // CALL API OF ITEM LISTING WHEN CLIENT TYPE IS REAL

        //Customer Tab
        custId = inqData.custDetail.customerId;
        phoneId = inqData.custDetail.phoneId;
        $('#custSearch').val( inqData.custDetail.fName + ' ' + inqData.custDetail.lName );

        // $('#custStateId').val( inqData.custDetail.stateId );
        // $('#custStateId').selectpicker('refresh');
        // populateCity( inqData.custDetail.stateId );
        // $('#custCityId').val( inqData.custDetail.cityId );
        // $('#custCityId').selectpicker('refresh');

        $('#custCountryId').val( inqData.custDetail.countryId ).trigger("change");
        $('#custCountryId').selectpicker('refresh');

//        $('#mobileNumber').val( inqData.custDetail.custMobile );
        
        setNumberInIntl( 'mobileNumber' , inqData.custDetail.custMobile );
        $('#email').val( inqData.custDetail.custEmail );
        $('#custAddress').val( inqData.custDetail.custAddress );
        $('#custCatchArea').val( inqData.custDetail.catchArea );
        $('#contPerson').val( inqData.custDetail.cpName );
        $('#contPersonDesig').val( inqData.custDetail.contPerDesig );
//        $('#ContmobileNumber').val( inqData.custDetail.cpMobile );
        setNumberInIntl( 'ContmobileNumber' , inqData.custDetail.cpMobile );
        $('#contPersonEmail').val( inqData.custDetail.contPerEmail );
        $('#inquiryRateType').val( inqData.custDetail.rateTypeId );
        $('#inquiryRateType').selectpicker('refresh');
        $('#gstinNumber').val( inqData.custDetail.gstTinNo );

        //Inquiry Tab
        $('#projectName').val( inqData.projectName );
        $('#inquiryRemarks').val( inqData.inqRemark );
        $('#inquirytype').val( inqData.inqType );
        $('#inquirytype').selectpicker('refresh');
        $('#inquirysource').val( inqData.inqSource );
        $('#inquirysource').selectpicker('refresh');
        $('#remarks').val( inqData.FollowupRemark );
        $('#followupDate').val( mysqltoDesiredFormat( inqData.FollowupDate , 'dd-MM-yyyy' ) + " | " + inqData.FollowupTime );
        $('#dueTime').val( inqData.FollowupTime );
        $('#potAmt').val( inqData.potAmt );
        $('#closerBy').val( mysqltoDesiredFormat( inqData.closerBy , 'dd-MM-yyyy' ) );
        $('#assUser').val( inqData.inqAssignToId );
        $('#assUser').selectpicker('refresh');

        setCustomFieldData();   // set Custom Field Data when copy inquiry

    } else {
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_NO_DETAILING );
        }
        else{   
            toastr.error(SERVERERROR);
        }
    }
}

function getAllItems(){

    var projectId = ""; 
    if( currClientType == "32" ){
        projectId = $('#projectForItem').val();
    }

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    if( roleAuth.data.clientTypeId != "32" ){
        return false;
    }

    GBLRATETYPEID = (($('#inquiryRateType').val() == null) ? "" : $('#inquiryRateType').val() );
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postdata = {
        requestCase: "getItemListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        orgId: checkAuth(4, 14),
        type: "1",
        projectId: projectId,
        custRateType : GBLRATETYPEID,
    };
    commonAjax(COMMONURL, postdata, callBackgetAllItemsListing, "Please Wait... Getting the group listing");
    function callBackgetAllItemsListing(flag, data)
    {

        if (data.status == "Success" && flag) {

            FINAL_ITEMJSON = data.data;
            renderItemCatalog();
        }
        else {
            FINAL_ITEMJSON = [];
            renderItemCatalog();
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEM_SAMPLE );
            else
                toastr.error(SERVERERROR);
        }

    }
}


function renderItemCatalog(){
    var imageList = "";

    FINAL_ITEMJSON.forEach( function( record , index ){

        // var imgDesc = "<i class='fa fa-cubes'></i> "+ record.groupName;
        // var itemSpan = '<i class="fa fa-cubes"></i>'+ record.groupName +' - <i class="fa fa-hand-peace-o"></i>'+ record.itemQty +' -  <i class="fa fa-rupee"></i>'+ record.itemBasePrice;
        
        // if( (record.itemType).toLowerCase() == "item"){

        //     if( record.bomId != "" && record.bomId != undefined ){
        //         imgDesc = "<i class='fa fa-sitemap'></i> "+ record.itemName;
        //         itemSpan = "<i class='fa fa-sitemap'></i>"+ record.itemName +' - <i class="fa fa-hand-peace-o"></i>'+ record.itemQty +' -  <i class="fa fa-rupee"></i>'+ record.itemBasePrice;
        //     }else{            
        //         imgDesc = "<i class='fa fa-cube'></i> "+ record.itemName;
        //         itemSpan = "<i class='fa fa-cube'></i>"+ record.itemName +' - <i class="fa fa-hand-peace-o"></i>'+ record.itemQty +' -  <i class="fa fa-rupee"></i>'+ record.itemBasePrice;
        //     }
        
        // }else if( (record.itemType).toLowerCase() == "section"){
        //     imgDesc = "<i class='fa fa-file-text-o'></i> "+ record.itemName;
        //     itemSpan = '<i class="fa fa-file-text-o"></i>'+ record.itemName;

        // }else if( (record.itemType).toLowerCase() == "bom"){
        //     imgDesc = "<i class='fa fa-sitemap'></i> "+ record.itemName;
        //     itemSpan = '<i class="fa fa-sitemap"></i>'+ record.itemName;
        // }
        
        var imageLink = httpHead + "//localhost/potg/indeCampusProject/attachment/project-attachment/1511505290_image_V7.png";
        if( record.ITEM_B_URL != "" ){
            imageLink = record.fileLink;
        }

        var index = findIndexByKeyValue( temp_inq_Trans , "itemId" , record.itemId );
        var icon = "plus";
        var qtyInput = '<div class="inputer ItemQtyinput"><div class="input-wrapper"><input type="number" class="form-control" min="1" id="itemQty_'+ record.itemId +'" value="1"></div></div>';
        var priceInput = '<div class="inputer ItemQtyinput"><div class="input-wrapper"><input type="number" class="form-control" min="1" id="itemPrice_'+ record.itemId +'" value="'+ record.itemPrice +'"></div></div>';
        var addToCart = "addToCartCataLog(this)";
        if( index != "-1" ){
            icon = "check";
            addToCart = "";
            qtyInput = "";
            priceInput = "";
        }

        imgDesc = record.itemName;
        itemSpan = "<i class='fa fa-cube'></i>&nbsp;"+ record.itemName +'  <br/> <i class="fa fa-rupee"></i>&nbsp;'+ record.itemPrice;

        imageList += '<div class="col-sm-6 col-xs-12 col-md-3">'+
                        '<a class="thumbnail fancybox " title="'+ imgDesc +'" rel="ligthbox">'+
                            '<div class="blockImage">'+
                                '<img class="img-responsive" alt="" src="'+ imageLink +'" />'+
                            '</div>'+
                            '<div class="text-center properyDetail">'+
                                '<small class="text-muted">'+ itemSpan +'</small>'+
                            '</div>'+
                            '<div class="text-center">'+
                                qtyInput+
                                priceInput+
                                '<small class="btn btn-xs btn-success btn-right-mrg" itemId="'+ record.itemId +'" onclick="'+ addToCart +'"><i class="fa fa-'+ icon +'"></i></small>'+
                            '</div>'+ 
                        '</a>'+
                    '</div>';
    });

    $('#catalogListing').html( imageList );

    // $(".fancybox").fancybox({
    //     openEffect: "none",
    //     closeEffect: "none"
    // });
}

function addToCartCataLog(context){

    var itemId = $(context).attr('itemId');
    var index = findIndexByKeyValue( FINAL_ITEMJSON , "itemId" , itemId )

    if( index != "-1" ){

        var itemQty = $('#itemQty_'+itemId).val();
        var itemPrice = $('#itemPrice_'+itemId).val();
        if( itemQty == ""  || itemQty == "0" ){
            toastr.warning('Please Enter Valid Qty');
            $('#itemQty_'+itemId).focus();
            return false;
        
        }else if( itemPrice == ""  || itemPrice == "0" ){
            toastr.warning('Please Enter Valid Price');
            $('#itemPrice_'+itemId).focus();
            return false;
        }

        var  tempData = {
            cancleFlag : "1",
            itemId : FINAL_ITEMJSON[index].itemId,
            itemName : FINAL_ITEMJSON[index].itemName,
            groupName : FINAL_ITEMJSON[index].itemGroupName,
            itemQty : itemQty,
            eventStatus : "",
            itemPrice : (Number( itemPrice * itemQty)).toFixed(2),
            itemBasePrice : itemPrice,
            itemType : "ITEM",
            itemRemark : "",
            itemCat : "1", // 1 = item 2 - header text
            sequenceId : (temp_inq_Trans.length + 1),
            itemDetail : "",
        }

        var dupInd = findIndexByKeyValue( temp_inq_Trans,'itemId',itemId );
        if( dupInd == "-1" ){
            temp_inq_Trans.push(tempData); 
            addRowTable( temp_inq_Trans );
            renderItemCatalog();

            $('#stateExhibList').val( $('#projectStateForItem').val() );
            $('#stateExhibList').selectpicker('refresh');
            getCity( $('#projectStateForItem').val() );
            $('#cityExhibList').val( $('#projectCityForItem').val() );
            $('#cityExhibList').selectpicker('refresh');
            getExhibProject( $('#projectCityForItem').val() );
            $('#exhibList').val( $('#projectForItem').val() );
            $('#exhibList').selectpicker('refresh');

        }else{
            toastr.warning("This Item Already added in to the Cart");
            return false;
        }
    }
}



var stateItem = "";
function getProjCityForItem(context){

    stateItem = $('#projectStateForItem').val();
    if( stateItem == "-1" ){
        $('#projectCityForItem').val('-1');
        $('#projectCityForItem').selectpicker('refresh');
    }else{
        var index = findIndexByKeyValue( exhibProjectData ,"stateId",stateItem);
        if( index != "-1" ){
            
            cityData = exhibProjectData[index].cityList;
            var option = "<option value='-1'>--- Please Select City ---</option>"
            cityData.forEach(function(record,index) {
                if( record != "No record found" ){
                    option += "<option value='"+ record.cityId +"'>"+ record.cityName +"</option>";
                }
            });
            $('#projectCityForItem').html(option);
            $('#projectCityForItem').selectpicker('refresh');
        }
    }
}

function getProjListForItem(context){

    var city = $('#projectCityForItem').val();
    if( city == "-1" ){
        $('#projectForItem').val('-1');
        $('#projectForItem').selectpicker('refresh');
    }else{
        var index = findIndexByKeyValue( exhibProjectData ,"stateId",stateItem);
        if( index != "-1" ){
            
            var innrIndex = findIndexByKeyValue( exhibProjectData[index].cityList ,"cityId",city);
            if( innrIndex != "-1" ){

                var exhibListData = "";
                var option = "";

                projectListData = exhibProjectData[index].cityList[innrIndex].projectList;
                option = "<option value='-1'>--- Please Select Project ---</option>";
                projectListData.forEach(function(record,index) {
                    if( record != "No record found" ){
                        option += "<option value='"+ record.projectId +"'>"+ record.projectName +"</option>";
                    }
                });
                

                $('#projectForItem').html(option);
                $('#projectForItem').selectpicker('refresh');
            }
        }
    }
}


function showHideTax(){

    var confInqCb = $('#confirmInq').prop('checked');
    if( confInqCb ){
        GBLsetDiscountData();   // TAXATION MODAL
        $('#confInqTaxDiv').show();
        $('.followupCreateDiv').hide();
        $('#remarks').val('');
        $('#followupDate').val('');
        $('#closerBy').val( mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' ) );
    }else{
        $('#confInqTaxDiv').hide();
        $('.followupCreateDiv').show();
        GBLheaderLevelData = [];
    }
}

function setCustomFieldData(){

    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        for(var f=0;f<localJson.data.customFieldDetail.length;f++){
            if(localJson.data.customFieldDetail[f]['tabType'] == "INQMASTER"){
                var customFieldValue = "";
                if(localJson.data.customFieldDetail[f]['fieldType'] == "radio"){

                    var explodeValue = inqData['field'+(localJson.data.customFieldDetail[f].priorityLevel)].split(",");
                    var explodeGetData = inqData['field'+(localJson.data.customFieldDetail[f].priorityLevel)].split(",");

                    for(var g=0;g<explodeValue.length;g++){
                        var itemVal = 'field'+(localJson.data.customFieldDetail[f].priorityLevel) + explodeValue[g];
                        for(var h=0;h<explodeGetData.length;h++){
                            $('input[type="'+ itemVal +'"]').prop('checked',false);
                            if(explodeValue[g] == explodeGetData[h] ){    
                                $('input[id="'+itemVal+'"]').prop('checked',true);
                                break;
                            }
                        }
                    }
                }else if(localJson.data.customFieldDetail[f]['fieldType'] == "checkbox"){
                    
                    var explodeValue = inqData['field'+(localJson.data.customFieldDetail[f].priorityLevel)].split(",");
                    var explodeGetData = inqData['field'+(localJson.data.customFieldDetail[f].priorityLevel)].split(",");
                
                    for(var g=0;g<explodeValue.length;g++){
                        var itemVal = 'field'+(localJson.data.customFieldDetail[f].priorityLevel) + explodeValue[g];
                        for(var h=0;h<explodeGetData.length;h++){
                            $('input[type="'+ itemVal +'"]').prop('checked',false);
                            if(explodeValue[g] == explodeGetData[h] ){    
                                $('input[id="'+itemVal+'"]').prop('checked',true);
                                break;
                            }
                        }
                    }

                } else{
                    var prLevel = (localJson.data.customFieldDetail[f].priorityLevel);
                    $('input[id="field'+(localJson.data.customFieldDetail[f].priorityLevel)+'"]').val(inqData['field'+prLevel]);
                    $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)+'').val(inqData['field'+prLevel]);
                }
            }
        }
        $('.selectPckr').selectpicker('refresh');
    }
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 08-MAY-2018 BY VISHAKHA TAK
function initSelfTour() {

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: ".itemTypeClass",
            title: "Item Type",
            content: "Select item type for your inquiry!!",
            placement: 'top'
        },
        {
            path: "",
            element: "#itemName",
            title: "Search Item/Group",
            content: "Search your item or group and select from given list !!",
            placement: 'top'
        },
        {
            path: "", // REDIRECTS TO THIS PATH
            element: "#addItemRowBtn",
            title: "Add Item",
            content: "Add your item and its price alongwith Quantity !!",
            placement: 'top',
        },
        {
            path: "",
            element: "#tab4_2_CustTab",
            title: "Search Customer",
            content: "Search customer's name or mobile number !!</br> If a new customer, then fill all mandatory details !",
            placement: 'right',
            onShow: function(tour){
                $("#custDetail").click(); // TO DISPLAY NEXT TAB
            },
            onNext:function(tour) {
                $('#InqDetail').click();  // TO DISPLAY NEXT TAB
            },
            onPrev:function(tour) {
                $('#itemSelect').click();  // TO DISPLAY PREV TAB
            }
        },
        {
            path: "",
            element: "#tab4_3_InqTab",
            title: "Inquiry Name",
            content: "Enter your inquiry's name and other mandatory fields!",
            placement: 'top',
        },
        {
            path: "",
            element: "#addUserDiv",
            title: "Assign Inquiry to user",
            content: "Select user to whom you want to assign this inquiry!",
            placement: 'left',
        },
        {
            path: "",
            element: "#createInqBtn",
            title: "Create your inquiry",
            content: "Click this button and successfully create your inquiry !!!",
            placement: 'top',
        },
    ]
    
    // Instance the tour
    instanceTour();

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM ZEROth ELEMENT TO PREVENT FROM DISPLAYING LAST STEP
    $('#itemSelect').click(); // START FROM FIRST TAB
    // Start the tour
    tour.start(true);
}