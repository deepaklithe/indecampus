/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-11-2016.
 * File : configuredDashboardController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_DASHBOARD_DATA = [];

var GBL_TOP5_CUSTOMER = [];
var GBL_BOTTOM5_CUSTOMER = [];
var GBL_TOP5_ITEM = [];
var GBL_BOTTOM5_ITEM = [];
var firstTimeLoadFlag = false;

var GBL_USER_ID = "";
var GBL_USER_NAME = "";

var stDate = new Date().format('yyyy-MM-dd');
var enDate = new Date().format('yyyy-MM-dd');

var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
// var statListInd = findIndexByKeyValue( roleAuth.data.statusListing , "statusCatName" , "INQUIRY" );
// if( statListInd != "-1" ){
//     var thirdPIndex = findIndexByKeyValue( roleAuth.data.statusListing[statListInd].statusListing , "thirdPartyFlag" , "1" );
//     if( thirdPIndex != "-1" ){
//         wonStatId = roleAuth.data.statusListing[statListInd].statusListing[thirdPIndex].ID;
//     }
// }


var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
var allDashCompoArr = loadauth.data.dashbordGridList;

var GBL_TOUR_STEPS = []; // GLOBAL VARIABLE FOR STEPS OF SELF TOUR

function getDashboardData(){

    var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
    var userDashCompoArr = loadauth.data.userActiveGridList;
    var dashbordGridList = loadauth.data.dashbordGridList;

    var gridIdCs = "";

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID != "-1" ){
        userDashCompoArr.forEach( function( record , index ){
            if( gridIdCs ){
                gridIdCs += ','+record.ID
            }else{
                gridIdCs = record.ID;
            }
        });
    }else{
        dashbordGridList.forEach( function( record , index ){
            if( gridIdCs ){
                gridIdCs += ','+record.ID
            }else{
                gridIdCs = record.ID;
            }
        });
    }

    GBL_USER_ID = tmp_json.data[0].PK_USER_ID;
    GBL_USER_NAME = tmp_json.data[0].FULLNAME;
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getDashboardData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1,2),
        gridId: gridIdCs,
        // gridId: userDashCompoArr,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, dashboardDataCallback,"Please Wait... Getting Dashboard Detail");

    function dashboardDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            GBL_DASHBOARD_DATA = data.data;

            if( firstTimeLoadFlag == false ){
                google.charts.load('current', {packages: ['corechart', 'bar' , 'gauge' , 'geochart']});
                firstTimeLoadFlag = true;
            }

            // renderDashboardGridWise();            
            // renderAllCharts(); // RENDER ALL CHARTS AND CALENDAR 
            google.charts.setOnLoadCallback(renderDashboardGridWise); 

            // $('#selfTourBtn').show();
        } else {
            $('#selfTourBtn').hide();
            
            if (flag)
                toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderDashboardGridWise(){

    var contentHtml = ""; 

    for(var i=0;i<GBL_DASHBOARD_DATA.length;i++){

        var key = GBL_DASHBOARD_DATA[i]['keyName'];

        switch (key) {
            case "requestComplaintDashboardData": // IT WILL CALL INQUIRY KPI FUNCTION FOR ITS DATA
                contentHtml += generateComplaintChart( GBL_DASHBOARD_DATA[i].requestComplaintDashboardData, GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "enrollmentDashboardData": // IT WILL CALL INQUIRY KPI FUNCTION FOR ITS DATA
                contentHtml += generateEnrolmentChart( GBL_DASHBOARD_DATA[i].enrollmentDashboardData, GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "eventDashboardData": // IT WILL CALL ACTIVITY KPI FUNCTION FOR ITS DATA
                contentHtml += generateEventChart( GBL_DASHBOARD_DATA[i].eventDashboardData , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "propertyDashboardData": // IT WILL CALL TARGET KPI FUNCTION FOR ITS DATA
                contentHtml += generatePropertyChart( GBL_DASHBOARD_DATA[i].propertyDashboardData , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "revenueDashboardData": // IT WILL CALL TOP BOTTOM KPI FUNCTION FOR ITS DATA
                contentHtml += generateRevenuehart( GBL_DASHBOARD_DATA[i].revenueDashboardData , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "suspenssionDashboardData": // IT WILL CALL DEAL SLEEPING KPI FUNCTION FOR ITS DATA
                contentHtml += generateSuspensionChart( GBL_DASHBOARD_DATA[i].suspenssionDashboardData , GBL_DASHBOARD_DATA[i].keyName );
                break; 
        }
    }

    $('#dashboardCompoGridDiv').html( contentHtml );

    Pleasure.init();
    Layout.init();

    // DRAW ALL CHART ON GOOGLE API LOAD
    
    // google.charts.setOnLoadCallback(renderAllChartData); 
    renderAllChartData();
    //draggableEvent();
    
    function generateEnrolmentChart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6 col-xs-12">'+
                                            '<div id="piechart" style="width: 100%; height: 200px;"></div>'+
                                        '</div>'+
                                        '<div class="col-md-6 col-xs-12">'+
                                            '<div id="piechart1" style="width: 100%; height: 200px;"></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
    
    function generateRevenuehart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12 col-xs-12">'+
                                            '<div id="stackedChart" style="width: 100%; height: 200px;"></div>'+
                                        '</div>'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
    
    function generateComplaintChart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12 col-xs-12">'+
                                            '<div id="chart_div" style="width: 100%; height: 200px;"></div>'+
                                        '</div>'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
    
    function generateSuspensionChart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12 col-xs-12">'+
                                            '<div id="piechart2" style="width: 100%; height: 200px;"></div>'+
                                        '</div>'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
    
    function generateSuspensionChart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12 col-xs-12">'+
                                            '<div id="piechart2" style="width: 100%; height: 200px;"></div>'+
                                        '</div>'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
    
    function generatePropertyChart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12 col-xs-12" id="rentalTable">'+ 
                                        '</div>'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
    
    function generateEventChart( data , keyName ){
        var tempHtml = "";
        // if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-12 col-xs-12" id="calendar">'+ 
                                        '</div>'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        // }
        return tempHtml;
    }
}

function renderAllChartData(){

    GBL_DASHBOARD_DATA.forEach( function( record , index ){

        var enrollmentData = record.enrollmentDashboardData;
        var revenueData = record.revenueDashboardData;
        var complaintData = record.requestComplaintDashboardData;
        var suspensionData = record.suspenssionDashboardData;
        var propertyData = record.propertyDashboardData;
        var eventData = record.eventDashboardData;

        if( enrollmentData != "No record found" && enrollmentData != null && enrollmentData != undefined ){
            
            var chartData = enrollmentData;
            if( chartData == undefined || chartData == "No record found" ){
                return false;
            }

            // setTimeout(function(){
                google.charts.setOnLoadCallback(drawChart);
            // },100);

            function drawChart() {

                var chartArray = [];

                var firstArr = [];
                firstArr.push('Enrollment Type');
                firstArr.push('Count');
                chartArray.push(firstArr);

                var enrollIndex = findIndexByKeyValue( chartData , 'enrollType' , 'NEW' );
                if( enrollIndex != -1 ){
                    if( chartData[enrollIndex].enrollTypeWiseData != "No record found" ){

                        chartData[enrollIndex].enrollTypeWiseData.forEach(function( record , index ){

                            var temp = [];
                            temp.push( record.DISPLAY_VALUE );
                            temp.push( parseInt( record.COUNT ) );
                            chartArray.push(temp);

                        });
                    }
                    
                }
                var data = google.visualization.arrayToDataTable( chartArray );
                var options = {
                  title: 'New Enrollments'
                };
                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                chart.draw(data, options);
            }

          // Total Enrollments PIE Chart2 JS//    
            setTimeout(function(){
                google.charts.setOnLoadCallback(drawChart1);
            },100);

            function drawChart1() {

                var chart2Array = [];

                var firstArray = [];
                firstArray.push('Enrollment Type');
                firstArray.push('Count');
                chart2Array.push(firstArray);

                var enrollReIndex = findIndexByKeyValue( chartData , 'enrollType' , 'RENEW' );
                if( enrollReIndex != -1 ){
                    if( chartData[enrollReIndex].enrollTypeWiseData != "No record found" ){
                        chartData[enrollReIndex].enrollTypeWiseData.forEach(function( record , index ){

                            var temp = [];
                            temp.push( record.DISPLAY_VALUE );
                            temp.push( parseInt( record.COUNT ) );
                            chart2Array.push(temp);

                        });
                    }
                    
                }

                var data = google.visualization.arrayToDataTable( chart2Array );
                // var data = google.visualization.arrayToDataTable([
                //   ['Task', 'Hours per Day'],
                //   ['Total',     parseInt(chartData.totalEnrollmentCount)],
                //   ['General',     parseInt(chartData.renewGeneral)],
                //   ['Handicapped',  parseInt(chartData.renewHandicapped)]
                // ]);
                var options = {
                  title: 'Renewals'        
                };
                var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
                chart.draw(data, options);
            }

        }

        if( revenueData != "No record found" && revenueData != null && revenueData != undefined ){
            
            if( revenueData == undefined || revenueData == "No record found" || revenueData == "" ){
                return false;
            }

            var element = document.getElementById('stackedChart');
            function drawChart () {

                var chartArray = [];

                var tempArr = [];
                tempArr.push( "Total Revenue" );
                revenueData.forEach(function( record , index ){

                    tempArr.push( record.INVOICE_TYPE );

                });

                chartArray.push(tempArr);
                var tempArr = [];
                tempArr.push( "Total Revenue" );
                revenueData.forEach(function( record , index ){

                    tempArr.push( parseInt(record.TOTAL) );

                });
                chartArray.push(tempArr);
                // console.log(chartArray)
                var data = google.visualization.arrayToDataTable( chartArray );
              /*var data = google.visualization.arrayToDataTable([
                ['Total Revenue', 'Total', 'New Enrollments', 'Renewals', 'Rental Space', 'Events', 'Charge Posting', 'General Invoices'],
                ['Total Revenue',  parseInt(revenueData.totalRevenueAmt), parseInt(revenueData.totalEnrollmentAmt), parseInt(revenueData.totalReEnrollmentAmt), parseInt(revenueData.totalPropertyAmt), parseInt(revenueData.totalEventAmt), parseInt(revenueData.totalMiscAmt), parseInt(revenueData.totalGeneralAmt)],
              ]);*/

              var options = {
                isStacked: true,
                // colors: ['#0077c0', '#f00', '#8BC34A', '#fec006', '#8BC34A', '#fec006', '#f00', '#0077c0' ],
              };            
              var chart = new google.visualization.ColumnChart(element);
              chart.draw(data, options);
            }
            setTimeout(function(){
                google.charts.setOnLoadCallback(drawChart);
            },100);

        }

        if( complaintData != "No record found" && complaintData != null && complaintData != undefined ){
            
            if( complaintData == undefined || complaintData == "No record found" ){
                return false;
            }

            setTimeout(function(){
                google.charts.setOnLoadCallback(drawMultSeries);
            },100);    
            function drawMultSeries() {

                var chartArray = [];

                var firstArray = [];
                firstArray.push('Department Name');
                firstArray.push('Total Requests');
                firstArray.push('Total Complaints');
                chartArray.push(firstArray);

                complaintData.forEach(function( record , index ){

                    var tempArr = [];
                    tempArr.push( record.departmentName );
                    tempArr.push( parseInt(record.totalRequest) );
                    tempArr.push( parseInt(record.totalComplaint) );
                    chartArray.push(tempArr);

                });

                console.log(chartArray);
                var data = google.visualization.arrayToDataTable( chartArray );
              // var data = google.visualization.arrayToDataTable([
              //   ['City', 'Total Requests', 'Total Complaints'],
              //   ['Department ', 85000, 50000],
              //   ['Open', 100, 20],
              //   ['Closed', 500, 500]
              // ]);
              var options = {
                chartArea: {width: '50%'},
                hAxis: {
                  title: 'Total Requests & Complaints',
                  minValue: 0
                },
                vAxis: {
                  title: 'Department'
                }
              };
              var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
              chart.draw(data, options);
            }

        }

        if( suspensionData != "No record found" && suspensionData != null && suspensionData != undefined ){

            if( suspensionData == undefined || suspensionData == "No record found" ){
                return false;
            }

            setTimeout(function(){
                google.charts.setOnLoadCallback(drawChart);
            },100); 
              function drawChart() {
                // var data = google.visualization.arrayToDataTable([
                //   ['Task', 'Hours per Day'],
                //   // ['Total', parseInt(suspensionData.total) ],
                //   ['Active', parseInt(suspensionData.totalSuspenssion) ],
                //   ['Revoked', parseInt(suspensionData.totalRevoke) ],
                //   ['Total Terminations',  parseInt(suspensionData.totalTermination) ],
                // ]);
                var chartArr = [];
                var tmpArr = [];
                tmpArr.push( 'Type' );
                tmpArr.push( 'Count' );
                chartArr.push( tmpArr );

                suspensionData.forEach(function( record , index ){

                    var tmpArr = [];
                    tmpArr.push( record.TypeName );
                    tmpArr.push( parseInt(record.totalCount) );
                    chartArr.push( tmpArr );

                });

                var data = google.visualization.arrayToDataTable( chartArr );
                var options = {
                  title: 'Total Suspensions & Terminations'        
                  // title: 'Total Suspensions (Select Acedamic Year)'        
                };
                var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
                chart.draw(data, options);
              } 
            
        }

        if( propertyData != "No record found" && propertyData != null && propertyData != undefined ){

            var tHead = '<table class="display datatables-basic ThCenter dataTable no-footer" id="RentalTbl">'+
                            '<thead>'+
                               '<tr>'+
                                    '<th class="dColor">Property Name</th>'+
                                    '<th class="dColor">Total Inquiries</th>'+
                                    '<th class="dColor">Open Inquries</th>'+
                                    // '<th class="dColor">Closed Inquiries</th>'
                                    '<th class="dColor">Inquiries to Business</th>'+
                                    '<th class="dColor">Dropped Inquiries</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>';
            var tEnd =  '</tbody></table>';

            var tBody = '';

            if( propertyData != "" && propertyData != "No record found" ){

                propertyData.forEach(function( record , index ){

                    tBody += '<tr>'+
                                '<td>'+ record.propertyName +'</td>'+
                                '<td>'+ record.totalInquiry +'</td>'+
                                '<td>'+ record.totalOpenInquiry +'</td>'+
                                // '<td class="yColor">'+ record.totalCancelInquiry +'</td>'+
                                '<td class="gColor">'+ record.totalBookedInquiry +'</td>'+
                                '<td class="rColor">'+ record.totalCancelInquiry +'</td>'+
                            '</tr>';

                });

            }
            $('#rentalTable').html( tHead + tBody + tEnd );
            $('#RentalTbl').dataTable();
            
        }

        if( eventData != "No record found" && eventData != null && eventData != undefined ){

            if( eventData == undefined || eventData == "No record found" || eventData == "" ){
                return false;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth(); 
            var y = date.getFullYear();

            var events = [];
            eventData.forEach(function( value , index ){
                var colors = '';
                if( parseInt(value['maxCapacity']) == parseInt(value['bookedSeats']) ){
                    colors = "#d9534f";
                }
            events.push({
                    title: value['eventName'],
                    description: value['eventName'],
                    start: new Date(value['eventStartDate']+" "+'00:00'), // will be parsed
                    end: new Date(value['eventEndDate']+" "+'23:00'), // will be parsed
                    // start: value['eventDate'] + ' ' + '06:00', // will be parsed
                    // end: value['eventDate'] + ' ' + '23:59' , // will be parsed
                    eventName: value['eventName'],
                    eventStartDate: value['eventStartDate'],
                    eventEndDate: value['eventEndDate'],
                    bookedSeats: value['bookedSeats'],
                    remainingSeats: value['remainingSeats'],
                    maxCapacity: value['maxCapacity'],
                    status: value['status'],
                    color : colors            
                });
            });
            console.log(events);
            var Calendar = {
                createCalendar: function () {
                    var date = new Date(),
                        m = date.getMonth(),
                        d = date.getDate(),
                        y = date.getFullYear();      
                                  
                        $('#calendar').fullCalendar({
                           header: {
                                left: 'prev, next today',
                                center: 'title',
                                right: 'month'
                            },
                            firstDay: 0, // 1 for Monday, 2 for Tuesday
                            //isRTL: Pleasure.settings.rtl,
                            eventLimit: true,
                            editable: false,
                            droppable: false,
                            lazyFetching: false,
                            dayPopoverFormat: 'DD/MM/YYYY',
                            weekMode: 'liquid',
                            events: function(start, end, timezone, callback) {
                                data: events    // PRE INITIALIZATION WITH DATA
                                callback(events);
                            },
                            eventClick:  function(event, jsEvent, view) {
                                //set the values and open the modal
                                console.log(event);
                                $("#actTitle").html(event.eventName);
                                $("#description").html(event.eventName);
                                $("#maxSeats").html(event.maxCapacity);
                                $("#bookedSeats").html(event.bookedSeats);
                                $("#availableSeats").html(event.remainingSeats);
                                $("#eventStartDate").html( mysqltoDesiredFormat( event.eventStartDate , 'dd-MM-yyyy' ) );
                                $("#eventEndDate").html( mysqltoDesiredFormat( event.eventEndDate , 'dd-MM-yyyy' ) );
                                $('#statusIcon').removeClass('fa-phone fa-envelope fa-paper-plane fa-male');
                                $('#statusIcon').addClass('fa-calendar');
                                $("#calendarDetail").modal("show");
                            }

                        });
                        
                    },
                    init: function () {
                        this.createCalendar();
                    }
            }

            Calendar.init();    // Calendar Initialization

        }
    });
} 

function saveDashboardGrid(){

    var dashBoardCompArr = "";

    $('#dashboardCompoGridDiv .draggableBlock.portletslist1').each(function(ind, obj) {
        var currChkId = $(this).attr('id');

        var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
        var allDashCompoArr = loadauth.data.dashbordGridList;

        if( !($('#'+currChkId).hasClass('deleted')) ){
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , currChkId );

            if( indexFound != "-1" ){

                if( dashBoardCompArr ){
                    dashBoardCompArr += "," + allDashCompoArr[ indexFound ].ID;
                }else{
                    dashBoardCompArr = allDashCompoArr[ indexFound ].ID;
                }
            }
        }
    });
    // return false;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'checkUncheckDashbordUserWise',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1,3),
        gridId: dashBoardCompArr,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, saveDashboardCompoCallback,"Please Wait... Setting Dashboard Component");

    function saveDashboardCompoCallback(flag,data){

        if (data.status == "Success" && flag) {

            toastr.success('Dashboard Component Updated Successfully..!');
            
            var tempLoadAuth = JSON.parse(localStorage.indeCampusRoleAuth);
            tempLoadAuth.data.userActiveGridList = data.data.userActiveGridList;

            localStorage.indeCampusRoleAuth = JSON.stringify( tempLoadAuth );

            getDashboardData();

            $('#dashboardCompoGridDiv').removeClass('draggable-portlets');
            $('#dashboardCompoGridDiv').removeClass('ui-sortable');

            $('#dashboardCompoGridDiv .panel').removeClass('ui-sortable-handle');
            $('#dashboardCompoGridDiv .panel').removeClass('portlet-handle');

            $('.deleteIcon').hide('250');

            $('#cancelGrid').hide('250');
            $('#saveGrid').hide('250');
            $('#editGrid').show('250');
            
            Pleasure.init();
            Layout.init();

        } else {
          if (flag)
              toastr.error(data.status);
          else
              toastr.error(SERVERERROR);
        }
    }
  
}

function editDashboardGrid(){

    // toastr.info('Dynamic Dashboard Grid Activated');

    $('#dashboardCompoGridDiv').addClass('draggable-portlets');
    $('#dashboardCompoGridDiv').addClass('ui-sortable');

    $('#dashboardCompoGridDiv .panel').addClass('ui-sortable-handle');
    $('#dashboardCompoGridDiv .panel').addClass('portlet-handle');

    $('.deleteIcon').show();

    $('#cancelGrid').show('250');
    $('#saveGrid').show('250');
    $('#editGrid').hide('250');

    Pleasure.init();
    Layout.init();
}

function cancelDashboardGrid(){

    // toastr.info('Dynamic Dashboard Grid DeActivated');

    $('#dashboardCompoGridDiv').removeClass('draggable-portlets');
    $('#dashboardCompoGridDiv').removeClass('ui-sortable');

    $('#dashboardCompoGridDiv .panel').removeClass('ui-sortable-handle');
    $('#dashboardCompoGridDiv .panel').removeClass('portlet-handle');

    $('.deleteIcon').hide();

    $('#cancelGrid').hide('250');
    $('#saveGrid').hide('250');
    $('#editGrid').show('250');

    Pleasure.init();
    Layout.init();
}

function hideParentDiv( context ){

    var confirmDel = confirm( "Are you sure? Do you want to remove this component" );
    if( !confirmDel ){
        return false;
    }

    var divId = $(context).attr('parentDivId');

    $('#'+divId).addClass('deleted');
    $('#'+divId).hide();
}

// function renderAllCharts(){

//     renderEnrollmentChart( GBL_DASHBOARD_DATA.enrollmentData );
//     renderTotalRevenue( GBL_DASHBOARD_DATA.revenueData );
//     renderRequestComplaint( GBL_DASHBOARD_DATA.complaintData );
//     renderSuspensionTermination( GBL_DASHBOARD_DATA.suspenssionData );
//     renderRentalSpace( GBL_DASHBOARD_DATA.propertyData );
//     renderEventData( GBL_DASHBOARD_DATA.eventData );

// }