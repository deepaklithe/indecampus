/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 09-11-2016.
 * File : dashboardMasterCompoController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var dashboardCompoList = [];
var GBL_GRID_ID = "";
function getDashboardMasterData(){

    $('#compoName').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getDashbordGrid",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1,2),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, getDashboardMasterDataCallback,"Please Wait... Getting Dashboard Components List");

    function getDashboardMasterDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            if(data.data != 'No record found'){

                dashboardCompoList = data.data ;
                renderdashboardCompoList();
            
            }else{
                displayAPIErrorMsg( data.status , GBL_ERR_NO_DASHBOARD_PROP );
            }

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_DASHBOARD_PROP );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderdashboardCompoList(){ 

   var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Grid Name</th>'+
                        '<th>Function Name</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    dashboardCompoList.forEach( function( record , index ){

        newRow += '<tr>'+
                        '<td>'+ (index+1) +'</td>'+
                        '<td>'+ record.gridName +'</td>'+
                        '<td>'+ record.funName +'</td>'+
                        '<td><a href="javascript:void(0)" funcName="'+ record.funName +'" gridName="'+ record.gridName +'" gridId="'+ record.gridId +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" onclick="updateGridData(this)"><i class="fa fa-pencil"></i></a>'+
                        '<a href="javascript:void(0)" gridId="'+ record.gridId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="deleteGridData(this)"><i class="fa fa-trash-o"></i></a></td>'+
                    '</tr>';
    });

    // $('#moduleList').html( newRow );
    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable({ "stateSave": true });
    
    addFocusId( 'compoName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}   

function createUpdateDashboardCompo(){
   
    var gridName = $('#compoName').val();
    var funcName = $('#funcName').val();

    if( gridName == ""){
        toastr.warning('Please Enter Component Name');
        $('#compoName').focus();
        return false;

    }else if( funcName == ""){
        toastr.warning('Please Enter Function Name');
        $('#funcName').focus();
        return false;

    }else{
        var msg = 'Your Dashboard Grid Created Succssfully';
        if( GBL_GRID_ID != "" ){
            msg = 'Your Dashboard Grid Updated Succssfully';
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createUpdateDashbordGrid",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(1,2),
            gridId : GBL_GRID_ID,
            gridName : gridName,
            functionName : funcName,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
        commonAjax(FOLLOWUPURL, postData, createUpdateDashboardCompoCallback,"Please Wait... Creating Dashboard Component");

        function createUpdateDashboardCompoCallback(flag,data){

            if (data.status == "Success" && flag) {

                toastr.success( msg );
                console.log(data); 

                if( GBL_GRID_ID == "" ){
                    dashboardCompoList.push( data.data[0] ); 
                }else{
                    var index = findIndexByKeyValue( dashboardCompoList,'gridId',GBL_GRID_ID );
                    if( index != "-1" ){
                        dashboardCompoList[index] = data.data[0]; 
                    }
                }
                renderdashboardCompoList();

                GBL_GRID_ID = "";
                resetData();

            } else {
                
                GBL_GRID_ID = "";
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_DASHBOARD_GRID );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}


function updateGridData(context){

    GBL_GRID_ID = $(context).attr( 'gridId' );

    $('#compoName').val( $(context).attr( 'gridname' ) );
    $('#funcName').val( $(context).attr( 'funcname' ) );
    addFocusId( 'compoName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var gblContext = "";
function deleteGridData(context){
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){

        var gridId = $(context).attr('gridId');

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "deleteDashbordGrid",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(1,2),
            gridId : gridId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID), 
        }

        commonAjax(FOLLOWUPURL, postData, deleteGridDataCallback,"Please Wait... Getting Deleting Grid Data");
        
        function deleteGridDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                toastr.success('Grid has been Deleted');
                console.log(data); 

                var index = findIndexByKeyValue( dashboardCompoList,'gridId',gridId );

                dashboardCompoList.splice(index,1);

                renderdashboardCompoList();

                resetData();

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DELETE_DASHBOARD_GRID );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}




function resetData(){

    $('#compoName').val('');
    $('#funcName').val('');
    GBL_GRID_ID = "";
}