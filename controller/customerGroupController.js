/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var DataTableInit = 0;
var groupData = [];
var selectGroupIndex = ""; // ADDED BY DEEPAK PATIL ON 15-09-2016
function getUserData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userCustomerGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,66),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        searchType : "CUSTOMER"
    }

    commonAjax(FOLLOWUPURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        $('#updategroupList').modal('hide');

        groupData = data.data;
        renderUserData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function renderUserData(){

    var newRow = '';
    var count = 1 ;

    if( groupData != 'No record found' ){

        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Customer Group Name</th>'+
                            '<th>No.Of.Customer</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';
        
        var custGrpOption = "<option value='-1'> -- Select Customer Group -- </option>";
        for( var i=0; i<groupData.length; i++ ){

            var name = groupData[i].groupName;
            var groupId = groupData[i].groupId;

            var length = '0';
            if( groupData[i].linkedUserDetail != 'No record found' ){
                length = groupData[i].linkedUserDetail.length;
            }

            var totalMembers = '';
            
            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><a href="javascript:void(0)" groupId="'+ groupId +'" class="glist" onclick="groupList(this);">'+ name +'</a></td>'+
                            '<td>'+ length +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" groupName="'+ name +'" groupId="'+ groupId +'" onclick="editGroup(this)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>'+ 
                                '<a href="javascript:void(0)" groupId="'+ groupId +'" onclick="deleteUserGroup(this)" class="btn-right-mrg btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                                '<a href="javascript:void(0)" groupId="'+ groupId +'" onclick="assignUserGroup(this)" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-retweet"></i></a>'+
                            '</td>'+
                        '</tr>';

            custGrpOption += '<option value='+ groupId +' groupId="'+ groupId +'" class="glist">'+ name +'</option>';
        }
        $('#dtTable').html(thead + newRow + tfoot);
        
        $('#toCustGroup').html(custGrpOption);
        $('#fromCustGroup').html(custGrpOption);

        $('#toCustGroup').selectpicker('refresh');
        $('#fromCustGroup').selectpicker('refresh');

    }else{
            displayAPIErrorMsg( "" , GBL_ERR_NO_CUST_GROUP );
    }

    resetActiviytData();

    TablesDataTables.init();
    TablesDataTablesEditor.init();
}

function groupList(context){

    var groupId = $(context).attr('groupId');

    var index = findIndexByKeyValue(groupData,'groupId',groupId);
	
    if( groupData[index].linkedUserDetail != 'No record found' ){
		
    var groupList = '';
	groupList += '<table class="table table-hover" id="userGroupTable">'+
					'<thead>'+
						'<tr>'+
							'<th>#</th>'+
							'<th>Customer Name</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody id="userGroupListTable">';
					
        var count = 1;
        for( var i=0; i<groupData[index].linkedUserDetail.length; i++ ){

            count = i + 1;
            groupList += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ groupData[index].linkedUserDetail[i].userName +'</td>'+
                        '</tr>';
        }
		
		groupList += '</tbody></table>';
        $('#grpTableDiv').html(groupList);
        $('#userGroupTable').dataTable({ "stateSave": true });
        $('#grpHead').html(groupData[index].groupName);
        $('#groupList').modal('show');
    }else{
        toastr.warning(groupData[index].linkedUserDetail);
    }

} 

function getGroupList(context){

    var groupId = $(context).val();
    var groupList = '';
    groupList = '<table class="table table-hover" id="transferTableList">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th>#</th>'+
                                        '<th>customer Name</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody id="toCustGroupList">';

    if( groupId != "-1" ){    
        var index = findIndexByKeyValue(groupData,'groupId',groupId);
        if( groupData[index].linkedUserDetail != 'No record found' ){
            var index = findIndexByKeyValue(groupData,'groupId',groupId);
            var count = 1;
            for( var i=0; i<groupData[index].linkedUserDetail.length; i++ ){

                count = i + 1;
                groupList += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td>'+ groupData[index].linkedUserDetail[i].userName +'</td>'+
                            '</tr>';
            }
        }
    } 

    groupList += '</tbody></table>';

    $('#transferTableListDiv').html(groupList);
    $('#transferTableList').dataTable({ "stateSave": true });
}   

function addGroup(){

    if( checkAuth(17, 65, 1) == -1 ){
                toastr.error('You are not Authorised for this Activity');
                return false;

            }

    $('#saveUserGroup').attr('onclick','createUserGroup()')
    $('#saveUserGroup').attr('onclick','createUserGroup()')

    $('#grpName').val('');

    $('#addGroup .modal-title').html('Add Group');
    $('#addGroup').modal('show');

    addFocusId( 'grpName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function createUserGroup(context){

    if( checkAuth(19, 73, 1) == -1 ){
                toastr.error('You are not Authorised for this Activity');
                return false;

            }
    var grpName = $('#grpName').val().trim();

    if( grpName.length == 0){
        toastr.warning('Please Enter Group Name');
        $('#grpName').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addEditGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(17,65),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupName : grpName,
            groupType : "CUSTOMER",
        }  
        $('#addGroup').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, createUserGroupCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function createUserGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // location.reload();

        var grpName = $('#grpName').val().trim();

        var tempData = {
            groupId: data.data.grpId,
            groupName: grpName,
            linkedUserDetail: "No record found",
        }

        groupData.push( tempData ); 

        renderUserData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ADD_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function editGroup(context){

    if( checkAuth(17, 67, 1) == -1 ){
                toastr.error('You are not Authorised for this Activity');
                return false;

            }
    var groupId = $(context).attr('groupid');
    var groupName = $(context).attr('groupName');

    $('#saveUserGroup').attr('onclick','editUserGroup(this)');
    $('#saveUserGroup').attr('groupId',groupId);

    $('#grpName').val(groupName);

    $('#addGroup .modal-title').html('Update Group');
    $('#addGroup').modal('show'); 
    addFocusId( 'grpName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var grpIdForEdit = '';
function editUserGroup(context){

    if( checkAuth(17, 67, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var grpName = $('#grpName').val().trim();

    grpIdForEdit = $(context).attr('groupid');

    if( grpName.length == 0){
        toastr.warning('Please Enter Group Name');
        $('#grpName').focus();
        return false;

    }else{
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addEditGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(17,65),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupName : grpName,
            groupId : grpIdForEdit,
            groupType : "CUSTOMER"
        }  

        $('#addGroup').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, editUserGroupCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function editUserGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var index = findIndexByKeyValue( groupData,'groupId',grpIdForEdit );

        var grpName = $('#grpName').val();

        var tempArray = {
            groupId: grpIdForEdit,
            groupName: grpName,
            linkedUserDetail: groupData[index].linkedUserDetail
        }

        groupData[index] = tempArray;

        renderUserData();

        $('#saveUserGroup').attr('onclick','createUserGroup()');

    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_EDIT_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteUserGroup(context){

    if( checkAuth(17, 68, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        
        var groupId = $(context).attr('groupid');
        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteUserCustomerGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(17,68),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupId : groupId,
        }  

        $('#addGroup').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, deleteUserGroupCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function deleteUserGroupCallback(flag,data){

    if (data.status == "Success" && flag) {

        var groupId = $(gblContext).attr('groupid');
        
        console.log(data);
        $(gblContext).parents('td').parents('tr').remove();
        // location.reload();
        toastr.success('Group Deleted Successfully');

        var index = findIndexByKeyValue( groupData,'groupId',groupId );

        groupData.splice(index,1);

        renderUserData();

    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_DELETE_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function assignUserGroup(context){

   if( checkAuth(17, 105, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var groupId = $(context).attr('groupId');
    
    var index = findIndexByKeyValue(groupData,'groupId',groupId);
    selectGroupIndex = index;
    $('#userGroupList').html('');
    $('#assignNewUser').val('');
    
    if( groupData[index].linkedUserDetail != 'No record found' ){

        var groupList = '';

        var count = 1;
        for( var i=0; i<groupData[index].linkedUserDetail.length; i++ ){

            count = i + 1;
            var userId = groupData[index].linkedUserDetail[i].userId;
            var userName = groupData[index].linkedUserDetail[i].userName;
            var transId = groupData[index].linkedUserDetail[i].transId;
            groupList += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ userName +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" transId="'+ transId +'" userId="'+ userId +'" onclick="deleteAssignedUser(this)"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'
                        '</tr>';
        }

        $('#userGroupList').html(groupList);
    }

    $('#assignUserBtn').attr('groupId',groupId);

    $('#updategroupList').modal('show');
 
    addFocusId( 'assignNewUser' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}


function assignUserToGroup(context){

    var groupId = $(context).attr('groupId');
    var requestId = $(context).attr('requestid');
    var assignNewUser = $('#assignNewUser').val();

    if( requestId == "" || requestId == undefined){
        toastr.warning('Please search customer');
        $('#assignNewUser').focus();
        return false;

    }else if( assignNewUser == ""){
        toastr.warning('Please search customer');
        $('#assignNewUser').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'assignUserCustomerGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(17,105),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            requestId : requestId,
            groupId : groupId,
        }

        commonAjax(FOLLOWUPURL, postData, assignUserToGroupCallback,"Please Wait... Getting Dashboard Detail");
    }

}


function assignUserToGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
            console.log(data);
            var userName = $('#assignNewUser').val();
            var userId = $('#assignUserBtn').attr('requestId');

            var transId = data.data.transId;
            //var count = (groupData[selectGroupIndex]['linkedUserDetail'] != 'No record found') ? groupData[selectGroupIndex]['linkedUserDetail'].length+1 : 1;
            var count = '';
            var groupListHtml = '';
            groupListHtml += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ userName +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" transId="'+ transId +'" userId="'+ userId +'" onclick="deleteAssignedUser(this)"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'
                        '</tr>';

            $('#userGroupList').append(groupListHtml);

         $('#assignNewUser').val('');
         $('#assignUserBtn').removeAttr('requestId');
    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_ASSIGN_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}


function AutoCompleteForUser(id,callback) {
    
   $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var mobileNumber = item.text.split(",");
            
            $('#assignUserBtn').attr('requestId',item.value);
            
            var index = findIndexByKeyValueAndCond(ITEMS, "id","mobile", item.value,mobileNumber[2]);

            if ( index == -1 ){
                setTimeout(function () {

                    var newCustNm = item.text.split(":")[0];
                    $('#custSearch').val(newCustNm);
                    $('#companyName1').val(newCustNm);
                    $('#custcontmobile1').val("");                
                    $('#companyNum1').val("");              
                    $('#contPerson').val("");
                }, 50);   
            }
            else{
                setTimeout(function () {
                    if($("#custType1").val() == 14 || $("#custType1").val() == 40){
                        $("#ContactNameDiv").hide(250);
                        $("#ContactMoblieDiv").hide(250);
                    }else if($("#custType1").val() == 15){
                        $("#otherCustEmailDiv").hide(250);
                        $("#otherCustEthnicity").hide(250);
                    }
                    $("#" + id).val(ITEMS[index].data);
                    $("#rateType").val(ITEMS[index].rateTypeId);
                    
                    GBL_SELECTEDCUSTID = ITEMS[index].id;
                    GBL_SELECTEDCUSTMOBNO = ITEMS[index].id+"|--|"+mobileNumber[2];
                    localStorage.POTGselectedCustIdInSearch = ITEMS[index].id;
                    //initCommonFunction();
                    callback();
                }, 1);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                GBL_SELECTEDCUSTID = '';
                GBL_SELECTEDCUSTMOBNO = '';
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3,10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {
                        if( data.cust_id != -1 ){
                            
                            var group = {
                                id: data.cust_id,
                                name: data.concatKey,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname,
                                rateTypeId: data.rateTypeId,
                                custTypeId: data.custTypeId
                            };
                            ITEMS.push(group);
                        }
                    });

                    return ITEMS;
                }

            }
        }
    });
}

function saveUserDetail(){

}

var gblContext = '';
function deleteAssignedUser(context){

    gblContext = context;
    
    var transId = $(context).attr('transId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'deleteAssignCustomerUserGroup',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,68),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        transId : transId,
    }

    commonAjax(FOLLOWUPURL, postData, deleteAssignedUserCallback,"Please Wait... Getting Dashboard Detail");
}



function deleteAssignedUserCallback(flag,data){

    if (data.status == "Success" && flag) {
            console.log(data);
            $(gblContext).parent('td').parent('tr').remove();
    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_DELETE_CUST_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function transferCustGroup(){

    var sourceGrp = $('#toCustGroup').val();
    var destGrp = $('#fromCustGroup').val();

    if( sourceGrp == "-1" ){
        toastr.warning('Please Select Source Customer Group');
        return false;
    
    }else if( destGrp == "-1" ){
        toastr.warning('Please Select Destination Customer Group');
        return false;

    } else if( destGrp == sourceGrp ){
        toastr.warning('Source Customer Group and Destination Customer Group are same');
        return false;
    } 

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'transferCustToGroup',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,105),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        sourceGrp : sourceGrp,
        destGrp : destGrp,
    }
    commonAjax(FOLLOWUPURL, postData, transferCustGroupCallback,"Please Wait... Getting Dashboard Detail");

    function transferCustGroupCallback(flag,data){

        if (data.status == "Success" && flag) {
                console.log(data);
                toastr.success('Customers are Transferred Successfully..!');
                $('#tansferGrpModal').modal('hide');
                getUserData();
        } else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_TRANSFER_CUST_GROUP );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function showPages(pageId){
    
    var sourceGrp = $('#toCustGroup').val();
    var destGrp = $('#fromCustGroup').val();

    if( sourceGrp == "-1" ){
        toastr.warning('Please Select Source Customer Group');
        return false;
    
    }else if( destGrp == "-1" ){
        toastr.warning('Please Select Destination Customer Group');
        return false;

    }else if( destGrp == sourceGrp ){
        toastr.warning('Source Customer Group and Destination Customer Group are same');
        return false;

    }else if( ($('#toCustGroupList').html()).length == "" ){
        toastr.error('No Customers are available in this Group');
        return false;
    }

    $('#page1').hide();
    $('#page2').hide();
    $('#page3').hide();
    $('#'+pageId).show();
}

function openTransferPop(){
    $('#page2').hide();
    $('#page3').hide();
    $('#page1').show();
    $('#tansferGrpModal').modal('show');
    addFocusId( 'toCustGroup' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}