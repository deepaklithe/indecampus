    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 05-09-2018.
 * File : roomOccupancyReportController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_ROOM_DATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];
var GBL_ROOM_ITEMS = []; // ROOM TYPE LSITING DATA

// PAGE LOAD EVENTS
function pageInitialEvents(){

    getRoomOccupancyData();
    getRoomType();
    autoCompleteForStudentId('studentName',saveStudent);
}

// RENDER ROOM OCCUPANCY REPORT DATA
function renderRoomOccupancyList(){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="roomOccReportTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Room Type</th>'+
                        '<th>Room Number</th>'+
                        '<th>No. Of Beds</th>'+
                        '<th>Beds Alloted</th>'+
                        '<th>Beds Vacant</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var tempJson = {
        srNo : "#",
        roomTypeName : "Room Type",
        roomName : "Room Number",
        noOfBeds : "No. Of Beds",
        allotedBeds : "Beds Alloted",
        vacantBeds : "Beds Vacant",
    }
    GBL_CSVJSON.push(tempJson);


    var innerRow = '';
    if( GBL_ROOM_DATA.length != 0 && GBL_ROOM_DATA != "No record found" ){

        GBL_ROOM_DATA.forEach(function( record , index ){

            var tempJson = {
                srNo : (index+1),
                roomTypeName : record.roomTypeName,
                roomName : record.roomName,
                noOfBeds : record.totalBedNo,
                allotedBeds : record.totalBedAlloted,
                vacantBeds : record.totalBedVacent,
            }
            GBL_CSVJSON.push(tempJson);
            
            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.roomTypeName +'</td>'+
                            '<td>'+ record.roomName +'</td>'+
                            '<td>'+ record.totalBedNo +'</td>'+
                            '<td>'+ record.totalBedAlloted  +'</td>'+
                            '<td>'+ record.totalBedVacent +'</td>'+
                        '</tr>';
                
        });

    }
    $('#roomListDiv').html( thead + innerRow + tEnd);
    GBL_EXCELJSON = thead + innerRow + tEnd;
    $('#roomOccReportTbl').dataTable();

}

// GET EVENTS ON SELECTED FILTERS
function getRoomOccupancyData(){
    
    var roomType = $('#roomType').val();
    
    if( roomType == "-1" ){
        roomType = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'allotmentReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(26,105),
        roomType : roomType,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getRoomOccupancyDataCallback,"Please Wait... Getting Dashboard Detail");

    function getRoomOccupancyDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
            console.log(data);
            GBL_ROOM_DATA = data.data;
            renderRoomOccupancyList(); // RENDER ROOM DATA

        }else{

            GBL_ROOM_DATA = [];
            GBL_EXCELJSON = [];
            renderRoomOccupancyList(); // RENDER ROOM DATA
                
            if (flag){
                
                displayAPIErrorMsg( data.status , "No allotment data found" );
                // toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }
        }
        $('.Sidenav').removeClass('open');  
    }

}

// RESET FILTER
function resetFilters(){

    $('#roomType').val('');
   
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderRoomOccupancyList(); 

}

function saveStudent(){

}


// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_ROOM_DATA.length < 1 || GBL_ROOM_DATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Room Occupancy Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_ROOM_DATA.length < 1 || GBL_ROOM_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Room_Occupancy_Report.pdf",20);
    }

}

// GET TYPE OF ROOM DATA
function getRoomType(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "RENTAL",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRoomTypeCallback,"Please wait...Adding your item.");

    function getRoomTypeCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_ITEMS = data.data;
            setRoomType();
            
            
        }else{

            GBL_ROOM_ITEMS = [];
            setRoomType();

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOM_TYPE );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// SET ROOM TYPE IN DROPDOWN
function setRoomType(){

    var innerHtml = '<option value="-1">Select Room Type</option>';
    if( GBL_ROOM_ITEMS != "No record found" && GBL_ROOM_ITEMS != "" ){
        GBL_ROOM_ITEMS.forEach(function( record , index ){

            innerHtml += '<option value="'+ record.itemId +'">'+ record.itemName +'</option>';

        });
    }
    $('#roomType').html( innerHtml ).selectpicker('refresh'); // SET ROOM TYPE DATA IN DROPDOWN

}