
 /*
 * Created By : Sachin Jani.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : April 28 2016.
 * File : commonController.
 * File Type : .js.
 * Project : activityMgmt
 *
 * */

/* Page navigation functions START  */


var actInqId = '';
var actProjId = '';
var actCustId = '';
var GBL_ALL_USER_HTML = "";
var GBL_CUST_GROUP = [];

function navigateToActivityAppDashBoard()
{
    clearActivityAppLocalStorage();
    window.location.href = 'Activity-Followup.html';

}
function clearActivityAppLocalStorage()
{
    localStorage.removeItem('POTaGactivityAppActivityId');
}

/* Page navigation functions END  */

/* get roleAuth for Activity App START */
function initCommonFunctionForActivityApp(callBack)
{   
    if( localStorage.indeCampusAccessData == undefined || localStorage.indeCampusAccessData == "" ){
        window.location.href = "index.html";
        return false;
    }

    $('#inqListAct').change(function(){
        if($(this).val() != ""){
            $('.custInfoAct').hide();
        }else{
            $('.custInfoAct').show();
        }
    });

    assignProjectChangeEvent(); // GET THE LISTING OF USER ACCORDING TO PROJECT

    /* init globals constants from gbl_ActivityAppGlobals  START */
    //global var related globals.
    // we're defining the globals used by this function here from "gbl_ActivityAppGlobals" ..
    // .. so that we can keep the track of globals used by this module.
    //Static Globals
    
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;

    /* init globals constants from gbl_ActivityAppGlobals  END */

   /*  colorChange()*/
    
    function initRoleAuthForActivityApp()
    {

        var postData = {
            requestCase: "getUserAuthForActivity",
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,6),
        };



        function callBackGetUserCustomerGroupData(flag, results) {
            //if (results.status == SCS) {


//                if (results.data.AUTH_CREATE == "0")
//                {
//                    gbl_ActivityAppRoleAuth.AUTH_CREATE = false;
//                }
//                if (results.data.AUTH_MODIFY == "0")
//                {
//                    gbl_ActivityAppRoleAuth.AUTH_MODIFY = false;
//                }
//                if (results.data.AUTH_ASSIGN == "0")
//                {
//                    gbl_ActivityAppRoleAuth.AUTH_ASSIGN = false;
//                }
//                if (results.data.AUTH_REASSIGN == "0")
//                {
//                    gbl_ActivityAppRoleAuth.AUTH_REASSIGN = false;
//                }
//                if (results.data.AUTH_CANCEL == "0")
//                {
//                    gbl_ActivityAppRoleAuth.AUTH_CANCEL = false;
//                }
//                if (results.data.AUTH_TRANSFER == "0")
//                {
//                    gbl_ActivityAppRoleAuth.AUTH_TRANSFER = false;
//                }  
            //}
//            else {
//
//                //displayAttendenceListin(results);
//                console.log("====== Role Auth FAIL ======");
//                if (flag) {
//                    console.log(results.status);
//                } else {
//                    // console.log("====== No Records Found ======");
//                }
//            }
                if (checkAuth(2, 5) == -1)
                {
                    gbl_ActivityAppRoleAuth.AUTH_CREATE = false;
                }
                if (checkAuth(2,7) == -1)
                {
                    gbl_ActivityAppRoleAuth.AUTH_MODIFY = false;
                }
                if (checkAuth(2, 107) == -1)
                {
                    gbl_ActivityAppRoleAuth.AUTH_ASSIGN = false;
                }
                if (checkAuth(2, 108) == -1)
                {
                    gbl_ActivityAppRoleAuth.AUTH_REASSIGN = false;
                }
                if (checkAuth(2, 8) == -1)
                {
                    gbl_ActivityAppRoleAuth.AUTH_CANCEL = false;
                }
                if (checkAuth(2, 109) == -1)
                {
                    gbl_ActivityAppRoleAuth.AUTH_TRANSFER = false;
                }

                applyActRoleAuth();

            callBack(); //transfer this line to success callBack after gettin API.


        }

        /* ajax call start  */

        //var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_PRCSREQ;
        //commonAjax(actUrl, postData, callBackGetUserCustomerGroupData, actAjaxMsg);
        var results =[];
        callBackGetUserCustomerGroupData(true, results); //ROLE AUTH FROM LOCAL CHANGE BY VAIBHAV ON 9-Jun-2016
        // commonAjaxActiivity(actUrl, postData, callBackGetUserCustomerGroupData, actAjaxMsg);
        /* ajax call end  */

        /* function to apply the role auth START */
        function applyActRoleAuth()
        {
            /* elements defination START */
            var $btnPlusCreateActivity = $("#btnPlusCreateActivity"); // Btn to open create activity modal
            var $divAssignActInCreateAct = $("#divAssignActInCreateAct"); // 'Assingn To' Div in create activity/create and close activity
            var $btnEditAct = $("#btnEditAct"); // Btn to open edit activity modal
            var $assignbtn = $("#assignbtn"); // Btn to open assign activity popup in activity detail page.
            /* elements defination END */

            /* hide/show the btns/actions based on roleAuth START */

            /* Activity create auth START */
            if (gbl_ActivityAppRoleAuth.AUTH_CREATE)
            {
                $($btnPlusCreateActivity).show();
                $('#btnSmartActivity').show();
            }
            else
            {
                $($btnPlusCreateActivity).hide();
                $('#btnSmartActivity').hide();
            }
            /* Activity create auth END */

            /* Activity Edit auth START */
            if (gbl_ActivityAppRoleAuth.AUTH_MODIFY)
            {
                $($btnEditAct).show();
            }
            else
            {
                $($btnEditAct).hide();
            }
            /* Activity Edit auth END */

            /* Activity Assign in Create/Create and close activity START */
            if (gbl_ActivityAppRoleAuth.AUTH_ASSIGN)
            {
                $($divAssignActInCreateAct).show();
            }
            else
            {
                $($divAssignActInCreateAct).hide();
            }
            /* Activity Assign in Create/Create and close activity END */


            /* Activity Re-Assign START */
            if (gbl_ActivityAppRoleAuth.AUTH_REASSIGN)
            {
                $($assignbtn).show();
            }
            else
            {
                $($assignbtn).hide();
            }
            /* Activity Re-Assign END */


            /* hide/show the btns/actions based on roleAuth END */

        }
        /* function to apply the role auth END */


    }




    initRoleAuthForActivityApp();

}
/* get roleAuth for Activity App END */

/* common module to perform common operation used by many pages in the app START */
function commonModuleForActivityApp()
{
    /* init globals constants from gbl_ActivityAppGlobals  START */
    //global var related globals.
    // we're defining the globals used by this function here from "gbl_ActivityAppGlobals" ..
    // .. so that we can keep the track of globals used by this module.
    //Static Globals
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;

    /* init globals constants from gbl_ActivityAppGlobals  END */

    /* common function to get cust/user group data START */
    var custGrpArr = [];
    var userGrpArr = [];
    var customerListArr = [];
    var userListArr = [];

    /* html variables for user/grp dropdown  */
    var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
    var assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';


    this.getUserCustomerGroupData = function (callBack)
    {

        var postData = {
            requestCase: "getUserCustomerGroup",
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,6),
        };



        function callBackGetUserCustomerGroupData(flag, results) {
            if (results.status == SCS) {

                custGrpArr = results.data.customerGroupList;
                userGrpArr = results.data.userGroupList;
                assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';

                if (custGrpArr)
                {
                    for (var i = 0; i < custGrpArr.length; i++)
                    {
                        custGrpArr[i].label = custGrpArr[i].GROUP_NAME;
                        custGrpArr[i].text = custGrpArr[i].GROUP_NAME;
                        custGrpArr[i].value = custGrpArr[i].PK_GROUP_ID;
                        custGrpArr[i].grpid = custGrpArr[i].PK_GROUP_ID;
                    }


                }

                if (userGrpArr)
                {

                    for (var i = 0; i < userGrpArr.length; i++)
                    {
                        userGrpArr[i].label = userGrpArr[i].GROUP_NAME;
                        userGrpArr[i].text = userGrpArr[i].GROUP_NAME;
                        userGrpArr[i].value = userGrpArr[i].PK_GROUP_ID;
                        userGrpArr[i].grpid = userGrpArr[i].PK_GROUP_ID;

                        var grpid = userGrpArr[i].value;
                        var grpName = userGrpArr[i].label;

                        assignTypeGroupOptionHtml += '<option value="' + grpid + '">' + grpName + '</option>';
                    }


                }



            }
            else {

                //displayAttendenceListin(results);
                console.log("====== FAIL ======");
                if (flag) {
                    // displayAPIErrorMsg( results.status , GBL_ERR_NO_GROUP );
                    console.log(results.status);
                } else {
                    //console.log("====== No Records Found ======");
                }
            }

            /* if success then arrays will data else make callBack with blank data */
            var dataJson = {
                custGrpArr: custGrpArr,
                userGrpArr: userGrpArr,
                assignTypeGroupOptionHtml: assignTypeGroupOptionHtml,
            };

            /* caching/storing the user/cust grp listing array in localStorage for further use START */
            localStorage.POTGactivityAppUserGrpJson = JSON.stringify(userGrpArr);
            localStorage.POTGactivityAppCustGrpJson = JSON.stringify(custGrpArr);
            localStorage.POTGactivityAppAssignTypeGroupOptionHtml = assignTypeGroupOptionHtml;

            /* caching/storing the user/cust grp listing array in localStorage for further use END */


            callBack(dataJson);

        }

        /* ajax call start  */
        //check if we already have the data in localStorage
        if (!localStorage.POTGactivityAppUserGrpJson || !localStorage.POTGactivityAppCustGrpJson)
        {
            var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTLIST;
            commonAjax(actUrl, postData, callBackGetUserCustomerGroupData, actAjaxMsg);
            // commonAjaxActiivity(actUrl, postData, callBackGetUserCustomerGroupData, actAjaxMsg);
        }
        else
        {
            userGrpArr = JSON.parse(localStorage.POTGactivityAppUserGrpJson);
            custGrpArr = JSON.parse(localStorage.POTGactivityAppCustGrpJson);
            assignTypeGroupOptionHtml = localStorage.POTGactivityAppAssignTypeGroupOptionHtml

            var dataJson = {
                custGrpArr: custGrpArr,
                userGrpArr: userGrpArr,
                assignTypeGroupOptionHtml: assignTypeGroupOptionHtml,
            };

            callBack(dataJson);

        }
        /* ajax call end  */


    }

    this.getUserCustomerSingleData = function (callBack)
    {

        assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
        //assignTypeUserOptionHtml += '<option value="5">Temp User1</option>';//temp data
        //assignTypeUserOptionHtml += '<option value="6">Temp User2</option>';//temp data 
        function getUserList()
        {

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            //SET GROUP LOAD
            var postData ={
                requestCase : "userListingActivityModule",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                orgId: checkAuth(2,6),
                userId: tmp_json.data[0].PK_USER_ID,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
            };


            function callBackGetUserSingleData(flag, results) {
                if (results.status == SCS) {

                    
                    userListArr = results.data;

                    if (userListArr)
                    {

                        for (var i = 0; i < userListArr.length; i++)
                        {
                            userListArr[i].label = userListArr[i].fullName;
                            userListArr[i].text = userListArr[i].fullName;
                            userListArr[i].value = userListArr[i].userId;
                            userListArr[i].custId = userListArr[i].userId;

                            var userId = userListArr[i].value;
                            var userName = userListArr[i].label;

                            // if( userId != localStorage.indeCampusActivityAppUserId ){

                                assignTypeUserOptionHtml += '<option value="' + userId + '">' + userName + '</option>';
                            // }
                        }


                    }
                    GBL_ALL_USER_HTML = assignTypeUserOptionHtml;

                }
                else {

                    //displayAttendenceListin(results);
                    console.log("====== FAIL ======");
                    if (flag) {
                        // displayAPIErrorMsg( results.status , GBL_ERR_NO_USER );
                        console.log(results.status);
                    } else {
                        //console.log("====== No Records Found ======");
                    }
                }
                
                /* caching/storing the user/cust grp listing array in localStorage for further use START */
                localStorage.POTGactivityAppUserListJson = JSON.stringify(userListArr);
                localStorage.POTGactivityAppAssignTypeUserOptionHtml = assignTypeUserOptionHtml;

                /* caching/storing the user/cust grp listing array in localStorage for further use END */

                //getCustList();
                /* if success then arrays will have data else make callBack with blank data */
                var dataJson = {
                    userListArr: userListArr,
                    customerListArr: customerListArr,
                    assignTypeUserOptionHtml: assignTypeUserOptionHtml,
                }

                callBack(dataJson);

            }

            /* ajax call start  */

            // var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTLIST;
            // commonAjaxActiivity(actUrl, postData, callBackGetUserSingleData, actAjaxMsg);

            // if( type != "detail" ){ // ONLY FOR DETAILING PAGe
                var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_PRCSREQ;
                commonAjax(URL2, postData, callBackGetUserSingleData, actAjaxMsg);
            // }
            /* ajax call end  */
        }

        function getCustList()
        {

            var postData = {
                requestCase: "custdetails",
                userId: localStorage.indeCampusActivityAppUserId,
                clientId: localStorage.indeCampusActivityAppClientId,
                orgId: checkAuth(2,6),
            };



            function callBackGetCustomerSingleData(flag, results) {
                if (results.status == SCS) {

                    customerListArr = results.data.customerSingleList;



                    if (customerListArr)
                    {
                        for (var i = 0; i < customerListArr.length; i++)
                        {
                            customerListArr[i].label = customerListArr[i].CUST_NAME;
                            customerListArr[i].text = customerListArr[i].CUST_NAME;
                            customerListArr[i].value = customerListArr[i].PK_CUST_ID;
                            customerListArr[i].custId = customerListArr[i].PK_CUST_ID;
                        }


                    }


                }
                else {

                    //displayAttendenceListin(results);
                    console.log("====== FAIL ======");
                    if (flag) {
                        // displayAPIErrorMsg( results.status , GBL_ERR_NO_CUST_DATA );
                        console.log(results.status);
                    } else {
                        //console.log("====== No Records Found ======");
                    }
                }
                
                /* caching/storing the user/cust grp listing array in localStorage for further use START */
                localStorage.POTGactivityAppCustListJson = JSON.stringify(customerListArr);
                /* caching/storing the user/cust grp listing array in localStorage for further use END */

                /* if success then arrays will have data else make callBack with blank data */
                var dataJson = {
                    userListArr: userListArr,
                    customerListArr: customerListArr,
                    assignTypeUserOptionHtml: assignTypeUserOptionHtml,
                }

                callBack(dataJson);

            }

            /* ajax call start  */

            var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTLIST;
            commonAjax(actUrl, postData, callBackGetCustomerSingleData, actAjaxMsg);
            // commonAjaxActiivity(actUrl, postData, callBackGetCustomerSingleData, actAjaxMsg);
            /* ajax call end  */
        }
        
        
        
        //check if we already have the data in localStorage
        //if (!localStorage.POTGactivityAppUserListJson || !localStorage.POTGactivityAppCustListJson)
        if (!localStorage.POTGactivityAppUserListJson)  
        {
            getUserList();
        }
        else
        {
            getUserList();
            // userListArr = JSON.parse(localStorage.POTGactivityAppUserListJson);
            // customerListArr = [];
            // assignTypeUserOptionHtml = localStorage.POTGactivityAppAssignTypeUserOptionHtml

            // var dataJson = {
            //     userListArr: userListArr,
            //     customerListArr: customerListArr,
            //     assignTypeUserOptionHtml: assignTypeUserOptionHtml,
            // };

            // callBack(dataJson);

        }
        


    }

    /* common function to get cust/user group data END */

    /* generate ACT_FREQUENCY dropdown START */
    this.getActFreqDropDown = function ()
    {
        var actFreqOptionHtml = '';

        var actFreqArr = gbl_ActivityAppGlobals.ACT_FREQUENCY;

        for (var i = 0; i < actFreqArr.length; i++)
        {
            var actFreqLabel = actFreqArr[i].label;
            var actFreqValue = actFreqArr[i].value;

            actFreqOptionHtml += '<option value="' + actFreqValue + '">' + actFreqLabel + '</option>';

        }

        return actFreqOptionHtml;
    }

    /* generate ACT_FREQUENCY dropdown END */



}

/* common module to perform common operation used by many pages in the app END */



/* common module functions START */
// used for create activity/close and create activity.
function moduleForCreateActivity()
{

    /* init globals constants from gbl_ActivityAppGlobals  START */
    //global var related globals.
    // we're defining the globals used by this module here from "gbl_ActivityAppGlobals" ..
    // .. so that we can keep the track of globals used by this module.

    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;

    /* init globals constants from gbl_ActivityAppGlobals  END */


    /* this module related globals START */
    var mGbl_customerGrpList = [];
    var mGbl_userGrpList = [];
    var mGbl_customerList = [];
    var mGbl_userList = [];
    var thisModule = this;
    var createActCallBackOnFormSubmit; // callBack function of submit activity

    var mdlCommonModule = new commonModuleForActivityApp(); //create an instance of commonModuleForActivityApp .
    /* this module related globals END */

    /* element selector's defination START */
    var $txtActTitle = $('#txtActTitle');
    var $txtActDesc = $('#txtActDesc');
    var $txtActDueDate = $('#txtActDueDate');
    var $rdoActMode = $('input[type=radio][name="rdoActMode"]');

    var $txtCustGrp = $('#txtCustGrpName');
    var $txtCustGrpLabel = $('#cg-label');
    var $rdoCustType = $('input[type=radio][name="rdoIsCustomerGroup"]');
    var $rdoAssignType = $('input[type=radio][name="rdoIsUserGroup"]');
    var $ddActAssignedTo = $('#ddActAssignedTo');
    var $formCreateUpdateAct = $("#formCreateUpdateAct");


    var $postData = $("#postData"); // to attach json in form

    var $modalCreatactivity = $("#creatactivity"); // create activity modal
    //$($modalCreatactivity).modal('hide');


    /* element selector's defination END */

    /* html variables for user/grp dropdown  */
    var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
    var assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';


    /* public functions of this module START */

    /* common function to register events for create/update activity modal START */
    this.registerEventsForCreateActivity = function ()
    {
        console.log("Registering events for create/create and close activity");


    }
    /* common function to register events for create/update activity modal END */

    this.consoleData = function ()
    {
        console.log(mGbl_customerGrpList);


    }


    this.initCreateActModule = function (paramCreateActCallBackOnFormSubmit , type )
    {
        if (!paramCreateActCallBackOnFormSubmit)
        {
            console.error("callback function as paramter not defined for initCreateActModule");
            return false;
        }
        createActCallBackOnFormSubmit = paramCreateActCallBackOnFormSubmit;

        var callBackGetCustGrpDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {
                mGbl_customerGrpList = dataJson.custGrpArr;
                GBL_CUST_GROUP = dataJson.custGrpArr;
                mGbl_userGrpList = dataJson.userGrpArr;
                assignTypeGroupOptionHtml = dataJson.assignTypeGroupOptionHtml;

                if( type == "detail" ){
                    registerUserGrpEvents();
                }else{
                    mdlCommonModule.getUserCustomerSingleData(callBackGetCustSingleDataForThisModule); 
                }
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }


        }

        var callBackGetCustSingleDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {
                mGbl_customerList = dataJson.customerListArr;
                mGbl_userList = dataJson.userListArr;
                assignTypeUserOptionHtml = dataJson.assignTypeUserOptionHtml;

                registerUserGrpEvents(); // place in the last callBack after getting user/cust group and single data.
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }


        }

        mdlCommonModule.getUserCustomerGroupData(callBackGetCustGrpDataForThisModule);

    }



    // CHANGES FOR BUG ON LIVE SERVER - 1
    // create activity / close and create activity.
    this.submitCreateActivity = function (refId, callBack)
    {
        if( localStorage.indeCampusActivityAppUserId == "-1" ){
            toastr.error('Master User is not Authorised for this Activity');
            return false;
        }

        if (!refId)
        {
            refId = "";
        }

        var contactId = "";
        var actTitle = $($txtActTitle).val();
        var actType = $('#actTypeListSch').val();
        var actDesc = $($txtActDesc).val();
        var actMode = $rdoActMode.filter(":checked").val();
        var actDueDate = $($txtActDueDate).val();
        var isUserGroup = $rdoAssignType.filter(":checked").val();
        var isCustomerGroup = $rdoCustType.filter(":checked").val();
        var assigneTo = $($ddActAssignedTo).val();
        var custName = $('#txtCustGrpName').val();
        var customerId = $('#txtCustGrpName').attr('data-id');
        var customerType = $('#customerType').val();
        var inqIdAct = $('#inqListAct option:selected').val();
        contIdAct = $('#inqListAct option:selected').attr('contactid');
        var actProjId = $('#projectListAct option:selected').val();
        var custIdAct = $('#inqListAct option:selected').attr('custId');
        var actDueTime = $('#dueTime').val();
        var remindFlag = $('#reminderActChk').is(':checked');
        var reminderDate = "";
        var reminderTime = "";
        var schReminderTime = $('#reminderTime').val();

        var GBL_ACT_REF_TYPE = "INQ";

        var unRegCustName = $('#unregCustName').val();
        // var unRegCustMobile = $('#unregCustMobile').val();
        var unRegCustMobile = getIntlMobileNum("unregCustMobile");    // GET MOBILE NUM

        var actNormEndDate = "";
        var actNormEndTime = "";

        actDueDate = "";
        actDueTime = "";

        if( $('#reminderDate').val() != "" ){
            reminderDate = (($('#reminderDate').val() ).split('|')[0]).trim();
            reminderTime = (($('#reminderDate').val() ).split('|')[1]).trim();
        }

        if( $('#txtActNormEndDate').val() != "" ){
            actNormEndDate = (($('#txtActNormEndDate').val() ).split('|')[0]).trim();
            actNormEndTime = (($('#txtActNormEndDate').val() ).split('|')[1]).trim();
        }

        if( $('#txtActDueDate').val() != "" ){
            actDueDate = (($('#txtActDueDate').val() ).split('|')[0]).trim();
            actDueTime = (( $('#txtActDueDate').val() ).split('|')[1]).trim();
        }

        // actCustId = dataArr.

        var schdlAct = $('#scheduleAct').is(':checked');
        if(schdlAct){
            var actFrequency = $('#ddActFrequency').val();

            if( $('#txtActStartDate').val() == "" ){
                toastr.warning("Please Select Start Date/Time");
                $('#txtActStartDate').datetimepicker('show')
                return false;
            
            }else if( $('#txtActEndDate').val() == "" ){
                toastr.warning("Please Select End Date/Time");
                $('#txtActEndDate').datetimepicker('show')
                return false;
            }

            var actStartDate = (($('#txtActStartDate').val() ).split('|')[0]).trim();
            var actEndDate = (($('#txtActEndDate').val() ).split('|')[0]).trim();
            actStartTime = (($('#txtActStartDate').val() ).split('|')[1]).trim();
            actEndTime = (($('#txtActEndDate').val() ).split('|')[1]).trim();

            actStartDateVal = ddmmyyToMysql(actStartDate,'yyyy-MM-dd');
            actEndDateVal = ddmmyyToMysql(actEndDate,'yyyy-MM-dd');

            var dateActStartDate = new Date(actStartDateVal +" "+ actStartTime);
            var dateActEndDate = new Date(actEndDateVal +" "+ actEndTime);
        }
        

        if (!actTitle)
        {
            toastr.warning("Please Enter the Title");
            $($txtActTitle).focus();
            return false;
        }
        else if (!actDesc)
        {
            toastr.warning("Please Enter the Description");
            $($txtActDesc).focus();
            return false;

        }
        else if (!actMode)
        {
            toastr.warning("Please Select Mode");
            $($rdoActMode).focus();
            return false;

        }
        else if (!schdlAct && !actDueDate)
        {
            toastr.warning("Please Select Due Date/Time");
            $($txtActDueDate).focus();
            return false;

        // }
        // else if (!schdlAct && !actDueTime)
        // {
        //     toastr.warning("Please Select the Activity Due Time");
        //     $($dueTime).focus();
        //     return false;

        // }else if(!validateDateTimeFormat(actDueTime) ){
        //     toastr.warning('Please Select Proper Due Time eg:HH:MM (AM/PM) like 10:00 AM');
        //     return false;
        }
        else if (!schdlAct && !actNormEndDate)
        {
            toastr.warning("Please Select End Date/Time");
            $('#txtActNormEndDate').focus();
            return false;

        }
        // else if (!schdlAct && !actNormEndTime)
        // {
        //     toastr.warning("Please Select the Activity End Time");
        //     $('#endNormTime').focus();
        //     return false;

        // }else if(!validateDateTimeFormat(actNormEndTime) ){
        //     toastr.warning('Please Select Proper End Time eg:HH:MM (AM/PM) like 10:00 AM');
        //     return false;
        // }

        else if (remindFlag && !schdlAct && !reminderDate)
        {
            toastr.warning("Please Select Reminder Date/Time");
            $('#reminderDate').focus();
            return false;

        }else if (remindFlag && schdlAct && !schReminderTime)
        {
            toastr.warning("Please Select Reminder Date/Time");
            $('#reminderTime').focus();
            return false;
        }
        else if(schdlAct && !validateDateTimeFormat(schReminderTime) ){
            toastr.warning('Please Select Proper Reminder Time eg:HH:MM (AM/PM) like 10:00 AM');
            return false;

        }else if (!isUserGroup)
        {
            toastr.warning("Please Select Assign Type");
            return false;

        }
        // else if (!isCustomerGroup)
        // {
        //     alert("Please select the activity customer type");
        //     $($rdoCustType).focus();
        //     return false;

        // }
        else if (!assigneTo || assigneTo == "-1")
        {
            if (gbl_ActivityAppRoleAuth.AUTH_ASSIGN)
            {
                toastr.warning("Please Assign Activity");
                $($ddActAssignedTo).focus();
                return false;
            }

        }else if (schdlAct && !actStartDate)
        {
            toastr.warning("Please Select StartDate");
            $('#txtActStartDate').focus();
            return false;

        }
        else if (schdlAct && !actEndDate)
        {
            toastr.warning("Please Select EndDate");
            $('#txtActEndDate').focus();
            return false;

        }
        else if(schdlAct && ( +dateActEndDate < +dateActStartDate )) //dateActEndDate dateActStartDate
        {
            toastr.warning("End Date/Time cannot be less than the Start Date / Time");
            $('#txtActEndDate').datetimepicker('show');
            return false;
            
        }
        else if(customerType == "UNREGISTERED" && unRegCustName == "") //dateActEndDate dateActStartDate
        {
            toastr.warning("please enter customer name");
            $('#unregCustName').focus();
            return false;
            
        }
        else if(customerType == "UNREGISTERED" && !checkValidMobile( "unregCustMobile" )) //dateActEndDate dateActStartDate
        // else if(customerType == "UNREGISTERED" && unRegCustMobile.length != MOBILE_LENGTH) //dateActEndDate dateActStartDate
        {
            toastr.warning("please enter valid mobile number");
            $('#unregCustMobile').focus();
            return false;
            
        }
        // else if (!custName || !customerId)
        // {
        //     alert("Please select the customer/group name for customer type");
        //     $($txtCustGrp).focus();
        //     return false;

        // }

        console.log("Create activity validation passed");

        var conPerName = "";
        var conPerMobile =  "";
        /* generate postData to submit activity START */
        var requestCase = "createActivity";

        // CHANGES FOR BUG ON LIVE SERVER - 6
        contactId = $('#actContPersonSear').attr('contPerId');
        if (refId)
        {
            requestCase = "closeAndCreateNewActivity";
            actInqId = $('#inqListAct').attr('inqId');
            // actInqId = $('#inqListAct').val();
            custIdAct = $('#inqListAct').attr('custid');
            customerId = $('#inqListAct').attr('custid');
            contactId = $('#inqListAct').attr('contactid');

            if( actArr.CUTOMERDETAIL != "--" ){ 
                // actInqId = actArr.C_INQ_ID;
                custIdAct = actArr.FK_CUST_ID;
                customerId = actArr.FK_CUST_ID;
                contactId = actArr.FK_CONTACT_ID;
            }
        }

        if( customerType != "UNREGISTERED" ){
            unRegCustName = "";
            unRegCustMobile = "";
        }

        if( custName == "" || customerId == undefined ){
            if( customerType == "UNREGISTERED" ){
                customerType = "UNREGISTERED"; 
            }else{
                customerType = ""; 
            }
            customerId = ""; 
            isCustomerGroup = "";
            conPerName = "";
            conPerMobile = "";
        }else{
            if( customerType == 'GROUP' ){
                isCustomerGroup = 1;
            }else{
                isCustomerGroup = 0;

                var contPerNameMob = $('#actContPersonSear').val().split(",");
                if( contPerNameMob.length > 1 ){
                    // conPerName = contPerNameMob[0];
                    // conPerMobile = contPerNameMob[1];
                }else{
                    toastr.warning('Please Search Valid Contact Person');
                    return false;
                }
            }
        }

        if( inqIdAct != "" ){
            if( inqIdAct != undefined ){
                actInqId = inqIdAct;
                contactId = $('#inqListAct option:selected').attr('contactid');
                customerId = $('#inqListAct option:selected').attr('custid');
            }
        }else{
            actInqId = "";
            // contactId = "";
        }

        var remindTimeSend = ddmmyyToMysql( reminderDate ) +'|'+reminderTime;
        if( !remindFlag ){
            remindTimeSend = "";
        }

        if( custIdAct == undefined && customerId == undefined ){
            custIdAct = "";
            customerId = "";
        }

        if( localStorage.actCreateRefType != "" && localStorage.actCreateRefType != undefined ){
            GBL_ACT_REF_TYPE = localStorage.actCreateRefType;
            actInqId = GBL_INQ_ID_ACT;

            if( (localStorage.actCreateRefType).toLowerCase() == "service" || (localStorage.actCreateRefType).toLowerCase() == "order" ){
                custIdAct = "";
                customerId = "";
                contactId = "";
            }
            localStorage.removeItem('actCreateRefType'); 
        }

        if( schdlAct ){

            var actDueVar = new Date( actStartDateVal + ' '+ actStartTime );
            var actEndVar = new Date( actEndDateVal + ' '+ actEndTime );

            if( +actEndVar < +actDueVar ){
                toastr.warning("Due Date/Time should not be less than End Date/Time");
                $('#txtActEndDate').datetimepicker('show');
                return false;
            }

            if (remindFlag){ 
                
                var dueStart = new Date( ddmmyyToMysql( actStartDate ) +' '+ actStartTime );
                var currTime = new Date( );
                var remindStart = new Date( ddmmyyToMysql( actStartDate ) +' '+ schReminderTime );

                if( +dueStart < +currTime ){
                    toastr.warning("Due Date/Time should not be less than Current Date/Time");
                    $('#txtActStartDate').datetimepicker('show');
                    return false;

                }else if( +dueStart < +remindStart ){
                    toastr.warning("Reminder Date/Time should not be greater than Activity Due Date/Time");
                    $('#txtActStartDate').datetimepicker('show');
                    return false;

                }else if( +remindStart < +currTime ){ //dateActEndDate dateActStartDate
                    toastr.warning("Reminder Date/Time should not be less Current Date/Time");
                    $('#reminderDate').focus();
                    return false;

                }else if( !validateDateTimeFormat(schReminderTime) ){
                    toastr.warning('Please Select Proper Reminder Time eg:HH:MM (AM/PM) like 10:00 AM');
                    return false; 
                }
            }

            if( !remindFlag ){
               schReminderTime = ""; 
            }

            var postData = {
                requestCase: "createCustomActivity",
                orgId: checkAuth(2,5),
                actTitle: actTitle,
                actDesc: actDesc,
                actStartDate: actStartDateVal,
                actEndDate: actEndDateVal,
                actMode: actMode,
                actFrequency: actFrequency,
                isUserGroup: isUserGroup,
                isCustomerGroup: isCustomerGroup,
                assigneTo: assigneTo,
                customerId: customerId,
                contactId: contactId,
                conPerName: conPerName.trim(),
                conPerMobile: conPerMobile.trim(),
                refId: refId,
                actId: refId,
                clientId: localStorage.indeCampusActivityAppClientId,
                userId: localStorage.indeCampusActivityAppUserId,
                actType : actType, // activty type from role auth
                custType : customerType, // type of customer single or group
                unRegCustName : unRegCustName,
                unRegCustMobile : unRegCustMobile,
                inqId : actInqId,
                projId : actProjId,
                actRefType : ( (actInqId == "" ) ? "" : GBL_ACT_REF_TYPE),
                custId : custIdAct,
                actDueTime : actStartTime,
                reminderTime : '|'+schReminderTime,
                actEndTime : '|'+actEndTime,
            };
            
            console.log( postData );
            // return false;
        }else{

            var dueStart = new Date( ddmmyyToMysql( actDueDate ) +' '+ actDueTime );
            var currTime = new Date( );
            var remindStart = new Date( ddmmyyToMysql( reminderDate ) +' '+ reminderTime );

            var actEnd = new Date( ddmmyyToMysql( actNormEndDate ) +' '+ actNormEndTime );

            if( +dueStart < +currTime ){
                toastr.warning("Due Date/Time should not be less than Current Date/Time");
                $('#txtActDueDate').datetimepicker('show');
                return false;

            }else if( +actEnd < +dueStart ){
                toastr.warning("End Date/Time should not be less than Due Date/Time");
                $('#txtActNormEndDate').datetimepicker('show');
                return false;

            }

            if (remindFlag){ 
                
                if( +dueStart < +currTime ){
                    toastr.warning("Due Date/Time should not be less than Current Date/Time");
                    $('#txtActDueDate').datetimepicker('show');
                    return false;

                }else if( +dueStart < +remindStart ){
                    toastr.warning("Reminder Date/Time should not be greater than Activity Due Date/Time");
                    $('#txtActDueDate').datetimepicker('show');
                    return false;

                }else if( +remindStart < +currTime ){ //dateActEndDate dateActStartDate
                    toastr.warning("Reminder Date/Time should not be less Current Date/Time");
                    $('#reminderDate').datetimepicker('show');
                    return false;

                } 

                // var diffTime = ( (remindStart.getTime() - currTime.getTime())/ 1000 ) / 60;
                // if( diffTime < 30 ){
                //     remindTimeSend = followUpTime;
                // }
            }

            var postData = {
                requestCase: requestCase,
                orgId: checkAuth(2,5),
                actTitle: actTitle,
                actDesc: actDesc,
                actMode: actMode,
                actDueDate: ddmmyyToMysql(actDueDate,'yyyy-MM-dd'),
                isUserGroup: isUserGroup,
                isCustomerGroup: isCustomerGroup,
                assigneTo: assigneTo,
                customerId: customerId,
                contactId: contactId,
                conPerName: conPerName.trim(),
                conPerMobile: conPerMobile.trim(),
                refId: refId,
                actId: refId,
                clientId: localStorage.indeCampusActivityAppClientId,
                userId: localStorage.indeCampusActivityAppUserId,
                actType : actType, // activty type from role auth
                custType : customerType, // type of customer single or group
                unRegCustName : unRegCustName,
                unRegCustMobile : unRegCustMobile,
                inqId : actInqId,
                projId : actProjId,
                actRefType : ( (actInqId == "" ) ? "" : GBL_ACT_REF_TYPE ),
                custId : custIdAct,
                actDueTime : actDueTime,
                reminderTime : remindTimeSend,
                actEndDate : ddmmyyToMysql( actNormEndDate ),
                actEndTime : actNormEndTime,
            };
        }
        /* generate postData to submit activity END */

        if (refId)
        {   
            postData.closeActReasonId = GBL_closeActReasonId;
            postData.closeActReasonText = GBL_closeActReasonText;
        }
  
        $($postData).val(JSON.stringify(postData));
        // console.log(postData); return false;
        $($formCreateUpdateAct).submit();
        //createActCallBackOnFormSubmit();


    }

    /* public functions of this module END */



    function customerSearchCallBack( items ){
        $('#txtCustGrpName').attr("data-id", items.id);
        showCpAndInitAutocomplete( items ); // OPEN CONTACT PERSON BOX
    }

    var registerUserGrpEvents = function ()
    { // init customer search here and render customer assign dd here. 


        commonAutoCompleteForCustomer("txtCustGrpName", customerSearchCallBack);
        /* for customer type START */
        // by default single radio would be selected in customer type search
        // so we are assigning the source array of single radio's customer search. : "mGbl_customerList"
        // $($txtCustGrp).autocomplete({
        //     source: function (request, response)
        //     {
        //         var term = request.term;
        //         //var custData = custTermSearch(term);
        //         custTermSearch(term,response);
        //         //response(custData);
                

        //     },
        //     minChars : 2,
        //     focus: function (event, ui) {
        //         //event.preventDefault();
        //         // $(this).val(ui.item.label);

        //     },
        //     select: function (event, ui) {
        //         event.preventDefault();
        //         $(this).val(ui.item.label);
        //         $(this).attr("data-id", ui.item.value);

        //         showCpAndInitAutocomplete(ui.item); // OPEN CONTACT PERSON BOX

        //     }, change: function (event, ui) {
        //         if (ui.item == null) {
        //             this.value = '';
        //             $(this).val('');
        //             $(this).attr("data-id", "");
        //             toastr.warning('Please Select a value from the List'); // or a custom message
        //         }
        //     }
        // }).autocomplete("widget").addClass("custMenuList");




        $($rdoCustType).off().on('change', (function (e) {
            if (this.value == '0') {
                //user //single

                $($txtCustGrpLabel).html('Customer Name');
                $($txtCustGrp).val('');
                $($txtCustGrp).attr('placeholder', 'Customer (eg. John Smith)');

                var parent = $('#txtCustGrpName').parent();
                var html = parent.html();
                $('#txtCustGrpName').remove();
                parent.html(html);
                
                // $($txtCustGrp).autocomplete("option", {
                //     source: function (request, response)
                //         {
                //             var term = request.term;
                //             //var custData = custTermSearch(term);
                //             custTermSearch(term,response);
                //             //response(custData);
                            

                //         },
                // });

                commonAutoCompleteForCustomer("txtCustGrpName", customerSearchCallBack);

            } else if (this.value == '1') {
                // group
                $($txtCustGrpLabel).html('Group Name');
                $($txtCustGrp).val('');
                $($txtCustGrp).attr('placeholder', 'Group (eg. Company Group)');

                var parent = $('#txtCustGrpName').parent();
                var html = parent.html();
                $('#txtCustGrpName').remove();
                parent.html(html);

                // $($txtCustGrp).autocomplete("option", {
                //     source: mGbl_customerGrpList
                // });
                commonAutoCompleteForGrp("txtCustGrpName", customerSearchCallBack);

            }
        }));
        
        $('#customerType').on('change', (function (e) {

            $('#searchCustName').show();
            $('#unRegCustNameMobile').hide();
            if ((this.value).toLowerCase() == 'single') {
                //user //single
                
                $($txtCustGrpLabel).html('Customer Name');
                $($txtCustGrp).val('');
                // $('#contPersonDivAct').show();
                $('#actContPersonSear').val('');
                $($txtCustGrp).attr('placeholder', 'Customer (eg. John Smith)');
                
                var parent = $('#txtCustGrpName').parent();
                var html = parent.html();
                $('#txtCustGrpName').remove();
                parent.html(html);

                $('#txtCustGrpName').attr('placeholder', 'Customer (eg. John Smith)');
                $('#searchCustLabel').html(' Customer Name ');
                commonAutoCompleteForCustomer("txtCustGrpName", customerSearchCallBack);

                // $($txtCustGrp).autocomplete("option", {
                //     source: function (request, response)
                //     {
                //         var term = request.term;
                //         //var custData = custTermSearch(term);
                //         custTermSearch(term,response);
                //         //response(custData);
                //     },
                // });

            } else if ((this.value).toLowerCase() == 'group') {
                //group

                
                $($txtCustGrpLabel).html('Group Name');
                $($txtCustGrp).val('');
                $('#contPersonDivAct').hide();
                $('#actContPersonSear').val('');
                $($txtCustGrp).attr('placeholder', 'Group (eg. Company Group)');
                
                var parent = $('#txtCustGrpName').parent();
                var html = parent.html();
                $('#txtCustGrpName').remove();
                parent.html(html);

                $('#searchCustLabel').html(' Group Name ');
                $('#txtCustGrpName').attr('placeholder', 'Group (eg. Company Group)');
                commonAutoCompleteForGrp("txtCustGrpName", customerSearchCallBack);
                
                // $($txtCustGrp).autocomplete("option", {
                //     source: mGbl_customerGrpList
                // });

            } else if ((this.value).toLowerCase() == 'unregistered') {
                //group
                var parent = $('#txtCustGrpName').parent();
                var html = parent.html();
                $('#txtCustGrpName').remove();
                parent.html(html);
                $('#searchCustName').hide();
                $('#unRegCustNameMobile').show();
                $('#contPersonDivAct').hide();
                $('#actContPersonSear').val('');
            }
        }));
        /* for customer type END */

        //console.log(assignTypeUserOptionHtml);
        $($ddActAssignedTo).html(assignTypeUserOptionHtml); // default selection of user/single 's html 
        $($ddActAssignedTo).selectpicker('refresh');    
        GBL_ALL_USER_HTML = assignTypeUserOptionHtml;
        $($ddActAssignedTo).selectpicker(); 


        $($rdoAssignType).off().on('change', (function (e) {
            if (this.value == '0') {
                //user //single
                $($ddActAssignedTo).html( GBL_ALL_USER_HTML ); 
                $($ddActAssignedTo).selectpicker('refresh');    

            } else if (this.value == '1') {
                //group
                $($ddActAssignedTo).html(assignTypeGroupOptionHtml); 
                $($ddActAssignedTo).selectpicker('refresh');    
            }

        }));

        /* for assign type START */


        /* register create/update activity form submit event START */
        $($formCreateUpdateAct).off().on('submit', (function (e) {

            addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            e.preventDefault();
            $.ajax({
                url: actUrl, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);

                    addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
                    $("#ajaxloader").removeClass("hideajaxLoader");

                },
                success: function (data)// A function to be called if request succeeds
                {


                    data = JSON.parse(data);
                    console.log(data);

                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    if (data.status == SCS) {
                        //hide the modal here.

                        var msg = 'Your Activity is been Created.';

                        $($modalCreatactivity).modal('hide');
                        if( data.data != "" ){
                            
                            msg = 'Your Activity is been Created';
                        }

                        var currUrl = window.location.href; 
                        var projUrl = httpHead + "//"+FINALPATH+"/projectActivity.html";
                        if( currUrl.search('projectActivity') != "-1" ){
                            getProjectData();   //  FOR PROJECT LISTING                        
                        }

                        toastr.success( msg );
                        setFollowup = "0";
                        createActCallBackOnFormSubmit();

                    } else {
                        toastr.warning(data.status);

                    }

                    $('#actContPersonSear').removeAttr('contperid');
                    $("#ajaxloader").addClass("hideajaxLoader");
                },
                error: function (jqXHR, errdata, errorThrown) {

                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    log("error");
                    $("#ajaxloader").addClass("hideajaxLoader");
                }
            });
        }));
        /* register create/update activity form submit event END */
    }
}


function commonAutoCompleteForGrp( id , callback ){

    var source = [];
    if( GBL_CUST_GROUP.length != 0 && GBL_CUST_GROUP != "No record found" ){
        GBL_CUST_GROUP.forEach( function( record , index ){
            var tempGrp = {id: record.grpid, name: record.GROUP_NAME}
            source.push( tempGrp );
        });
    }
    
    $("#" + id).typeahead({
        onSelect: function (item) {
            // console.log(item);
            setTimeout(function(){
                var grpName = item.text;
                var grpId = item.value;
                $('#'+id).val( grpName );  
                $('#'+id).attr( 'data-id' , grpId );  
            },50);
        },
        source: source,
    });
}

// function dueDateOnCreateAct(){

//     var dueDate = $('#txtActDueDate').val();
//     var dueTime = $('#dueTime').val();
//}

// used for Edit/Assign activity.
function moduleToUpdateActivity()
{

    /* init globals constants from gbl_ActivityAppGlobals  START */
    //global var related globals.
    // we're defining the globals used by this module here from "gbl_ActivityAppGlobals" ..
    // .. so that we can keep the track of globals used by this module.
    //Static globals
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;

    /* init globals constants from gbl_ActivityAppGlobals  END */


    /* this module related globals START */
    var mGbl_userList = [];
    var mGbl_ActDataArr = [];
    var mGbl_actDetActivityId;
    var thisModule = this;
    var updateActCallBackOnFormSubmit, assignActCallBackOnChange;

    var mdlCommonModule = new commonModuleForActivityApp(); //create an instance of commonModuleForActivityApp .
    /* this module related globals END */

    /* element selector's defination START */
    var $txtActTitle = $('#txtActTitleForEditAct');
    var $txtActDesc = $('#txtActDescForEditAct');
    var $txtActDueDate = $('#txtActDueDateForEditAct');
    var $rdoActMode = $('input[type=radio][name="rdoActModeForEditAct"]');

    var $ddActAssignedTo = $('#ddActAssignedToForEditAct');
    var $formCreateUpdateAct = $("#formCreateUpdateActForEditAct");

    var $btnEditAct = $("#btnEditAct");
    var $btnSaveEditedActivity = $("#btnSaveEditedActivity");

    var $assignbox = $('#assignbox');


    var $postData = $("#postDataForEditAct"); // to attach json in form

    var $modalEditactivity = $("#editactivity"); // edit activity modal
    //$($modalEditactivity).modal('hide');


    /* element selector's defination END */

    /* html variables for user/grp dropdown  */
    var assignTypeUserOptionHtml = '<option value="-1">Select User</option>';
    //var assignTypeGroupOptionHtml = '<option value="-1">Select Group</option>';


    /* public functions of this module START */
 
    addFocusId( 'txtActTitleForEditAct' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    this.consoleData = function ()
    {
        console.log(mGbl_userList);


    }


    this.initUpdateActModule = function (paramsJson)
    {
        if (!paramsJson)
        {
            console.error("paramsJson not defined for initUpdateActModule");
            return false;
        }


        if (!paramsJson.paramUpdateActCallBack || !paramsJson.paramAssignActCallBack)
        {
            console.error("callback functions as paramter not defined for initUpdateActModule");
            return false;
        }

        if (!paramsJson.paramActDataArr)
        {
            console.error("paramter paramActDataArr not defined for initUpdateActModule");
            return false;
        }
        updateActCallBackOnFormSubmit = paramsJson.paramUpdateActCallBack;
        assignActCallBackOnChange = paramsJson.paramAssignActCallBack;
        mGbl_ActDataArr = paramsJson.paramActDataArr;
        mGbl_actDetActivityId = mGbl_ActDataArr.PK_ACT_ID;



        var callBackGetCustGrpDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {

                mGbl_userList = dataJson.userListArr;
                assignTypeUserOptionHtml = dataJson.assignTypeUserOptionHtml;

                registerUserGrpEvents();
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }


        }
 
        mdlCommonModule.getUserCustomerSingleData(callBackGetCustGrpDataForThisModule); 

    }



    // function to save edited activity details
    this.submitEditedActivity = function ()
    {
        if( checkAuth(2, 7, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

        var actTitle = $($txtActTitle).val();
        var actDesc = $($txtActDesc).val();
        var actMode = $rdoActMode.filter(":checked").val();
        var actDueDate = (($('#txtActDueDateForEditAct').val() ).split('|')[0]).trim();
        var actDueTime = (($('#txtActDueDateForEditAct').val() ).split('|')[1]).trim();
        //var assigneTo = $($ddActAssignedTo).val();

        var actNormEndDate = (($('#txtActNormEndDateEdit').val() ).split('|')[0]).trim();
        var actNormEndTime = (($('#txtActNormEndDateEdit').val() ).split('|')[1]).trim();


        if (!actTitle)
        {
            toastr.warning("Please Enter the Title");
            $($txtActTitle).focus();
            return false;
        }
        else if (!actDesc)
        {
            toastr.warning("Please Enter the Description");
            $($txtActDesc).focus();
            return false;

        }
        else if (!actMode)
        {
            toastr.warning("Please Select the Mode");
            $($rdoActMode).focus();
            return false;

        }
        else if (!actDueDate)
        {
            toastr.warning("Please Select the Due Date/Time");
            $('$txtActDueDateForEditAct').focus();
            return false;

        // }
        // else if (!actDueTime)
        // {
        //     toastr.warning("Please Select the Activity Due Time");
        //     $('#dueTimeEdit').focus();
        //     return false;

        // }else if(!validateDateTimeFormat(actDueTime) ){
        //     toastr.warning('Please Select Proper Due Time eg:HH:MM (AM/PM) like 10:00 AM');
        //     return false;

        }else if (!actNormEndDate)
        {
            toastr.warning("Please Select the End Date/Time");
            $('#txtActNormEndDateEdit').focus();
            return false;

        }
        // else if (!actNormEndTime)
        // {
        //     toastr.warning("Please Select the Activity End Time");
        //     $('#endNormTimeEdit').focus();
        //     return false;

        // }else if(!validateDateTimeFormat(actNormEndTime) ){
        //     toastr.warning('Please Select Proper End Time eg:HH:MM (AM/PM) like 10:00 AM');
        //     return false;
        // }
//        else if (!assigneTo || assigneTo == "-1")
//        {
//            alert("Please assign the activity");
//            $($ddActAssignedTo).focus();
//            return false;
//
//        }

        console.log("Update activity validation passed");

        /* generate postData to submit activity START */

        var dueStart = new Date( ddmmyyToMysql( actDueDate ) +' '+ actDueTime );
        var currTime = new Date( );
        
        var actEnd = new Date( ddmmyyToMysql( actNormEndDate ) +' '+ actNormEndTime );

        if( +dueStart < +currTime ){
            toastr.warning("Due Date/Time should not be less than Current Date/Time");
            $('#txtActDueDateForEditAct').datetimepicker('show');
            return false;

        }else if( +actEnd < +dueStart ){
            toastr.warning("End Date/Time should not be less than Due Date/Time");
            $('#txtActNormEndDateEdit').datetimepicker('show');
            return false;

        }

        var postData = {
            requestCase: "updateActivity",
            orgId: checkAuth(2,7),
            actId: mGbl_actDetActivityId,
            actTitle: actTitle,
            actDesc: actDesc,
            actMode: actMode,
            actDueTime: actDueTime,
            actDueDate: ddmmyyToMysql(actDueDate, 'yyyy-MM-dd'),
            clientId: localStorage.indeCampusActivityAppClientId,
            userId: localStorage.indeCampusActivityAppUserId,
            actType: localStorage.POTGactType,
            actEndDate : ddmmyyToMysql( actNormEndDate ),
            actEndTime : actNormEndTime,
        };
        /* generate postData to submit activity END */

        $($postData).val(JSON.stringify(postData));
        console.log(postData);
        // return false;
        $($formCreateUpdateAct).submit();


    }

    /* public functions of this module END */

    var registerUserGrpEvents = function ()
    { // init customer search here and render customer assign dd here. 


        $($ddActAssignedTo).html(assignTypeUserOptionHtml); // default selection of user/single 's html
        // $($ddActAssignedTo).selectpicker('refresh'); // default selection of user/single 's html
        var assignedTo = $($ddActAssignedTo).attr('data-assignto');  // activity assigned to id.
        $($ddActAssignedTo).val(assignedTo); // set asiigned selected.
        // $($ddActAssignedTo).val(assignedTo).selectpicker('refresh'); // set asiigned selected.



        // $($ddActAssignedTo).off().on('change', (function (e) {

        //     // $($assignbox).slideUp('fast');

        //     if (this.value && this.value != "-1") {
        //         //make ajax call to Assign activity.
        //         var assigneTo = this.value;
        //         assignActivity(assigneTo); //assigneTo as param
        //     }

        // })); 


        $($btnEditAct).off().on('click', (function (e) {
            renderActivityDataToEdit();

        }));


        $($btnSaveEditedActivity).off().on('click', (function (e) {

            thisModule.submitEditedActivity();

        }));



        /* register create/update activity form submit event START */
        $($formCreateUpdateAct).off().on('submit', (function (e) {

            addRemoveLoader(1)// 1-ADD LOADER || 0-REMOVE LOADER ); // 1-ADD LOADER || 0-REMOVE LOADER 
            e.preventDefault();
            $.ajax({
                url: actUrl, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    //$("#msgDiv").html(bfsendMsg);

                    addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
                    $("#ajaxloader").removeClass("hideajaxLoader");

                },
                success: function (data)// A function to be called if request succeeds
                {


                    data = JSON.parse(data);
                    console.log(data);

                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    if (data.status == SCS) {

                        var msg = 'Your Activity is been Updated.';

                        $($modalEditactivity).modal('hide');
                        if( data.data != "" ){
                            
                            msg = 'Your Activity is been Updated , but attached '+data.data;
                        }

                        toastr.success( msg );
                        updateActCallBackOnFormSubmit();

                    } else {
                        toastr.warning(data.status);

                    }
                    $("#ajaxloader").addClass("hideajaxLoader");
                },
                error: function (jqXHR, errdata, errorThrown) {
                    
                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    log("error");
                    $("#ajaxloader").addClass("hideajaxLoader");
                }
            });
        }));
        /* register create/update activity form submit event END */

    }


    var assignActivity = function (assigneTo)
    {
        console.log("Assign activity change event fired");
        //assignActCallBackOnChange

        if (!mGbl_actDetActivityId)
        {
            console.error("localStorage.POTGactivityAppActivityId / mGbl_actDetActivityId - Not found");
            toastr.error("Required Activity data not found to Assign this activity");
            return false;
        }

        if( checkAuth(2, 107, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

        var postData = {
            requestCase: "transferActivity",
            actId: mGbl_actDetActivityId,
            assigneTo: assigneTo,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,109),
        };



        function callBackAssgnAct(flag, results) {
            if (results.status == SCS) {

                assignActCallBackOnChange();

            } else {

                log("====== FAIL ======");
                if (flag) {
                    displayAPIErrorMsg( results.status , GBL_ERR_TRANSFER_ACT_FAIL );
                    // toastr.error(results.status);
                } else {
                    log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTDATA;
        commonAjax(actUrl, postData, callBackAssgnAct, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackAssgnAct, actAjaxMsg);
        /* ajax call end  */



    }

    var renderActivityDataToEdit = function ()
    {

        console.log("btnedit click event");

        var dataArr = mGbl_ActDataArr;

        var activityTitle = dataArr.ACT_TITLE;
        var activityMode = dataArr.ACT_MODE;
        var activityDueDate = dataArr.DUE_DATE;
        var activityDueTime = dataArr.DUE_TIME;
        var activityEndDate = dataArr.END_DATE;
        var activityEndTime = dataArr.END_TIME;
        var activityAttachment = dataArr.ACT_ATTACHMENT;
        var activityDesc = dataArr.ACT_DESC;


        $($txtActTitle).val(activityTitle);
        $($txtActDesc).val(activityDesc);
        $($rdoActMode).filter('[value=' + activityMode + ']').click(); // set checked
        $($txtActDueDate).val(mysqltoDesiredFormat(activityDueDate,'dd-MM-yyyy') + " | " + activityDueTime );
        // $('#dueTimeEdit').val(activityDueTime);
        // $('#txtActNormEndDate').val(activityEndDate + " | " + activityEndTime );
        // $('#endNormTimeEdit').val(activityEndTime);
		if(dataArr.END_DATE != "0000-00-00"){
			$('#txtActNormEndDateEdit').val(mysqltoDesiredFormat((dataArr.END_DATE),'dd-MM-yyyy') + " | " + activityEndTime);
			return false;
		}else{
			$('#txtActNormEndDateEdit').val('--') ;
		}
        $('#endNormTimeEdit').val(dataArr.END_TIME);
        $($txtActDueDate).datetimepicker("setDate", mysqltoDesiredFormat(activityDueDate,'dd-MM-yyyy'));



        //$("#txtActDueDateForEditAct").datetimepicker("setDate", "2016-04-25");
        //$('input[type=radio][name="rdoActModeForEditAct"][value="1"]');
    }
}




/* common module functions END */
/* common function for cust search using term for ebqms START */
function custTermSearch(term,response)
{
    /* init globals constants from gbl_ActivityAppGlobals  START */
    //global var related globals.
    // we're defining the globals used by this module here from "gbl_ActivityAppGlobals" ..
    // .. so that we can keep the track of globals used by this module.
    //Static globals
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;

    /* init globals constants from gbl_ActivityAppGlobals  END */
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var custData = [];
    /*
    var custData = [
        {
            label:"sachin jani",
            value:"1"
        }
    ]; */

    if( term.length < 2 )
    {
        response(custData);
        return false;
    }

    setTimeout(function () {
        makeAjaxCall();
    },100);
        
    function makeAjaxCall()
    {

        var postData = {
            requestCase: "crmDashboardSearch",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            orgId: checkAuth(3, 10),
            keyWord: term,
            custType: -1
        };


        function custSearchCallBack(flag, result) {
            if (result.status == SCS) {
                
                custData = result.data.searchData;
                
                if ( custData ){
 
                    custData.forEach( function( record , index ){ 
                        if ( index != 0 ){
                            record.label = record.fname + ' ' + record.lname + ' , ' + record.mobile ;
                            record.text = record.fname + ' ' + record.lname + ' , ' + record.mobile ;
                            record.value = record.cust_id;
                        }
                    });
                }

                
            }
            else {
                console.log(result.status);
                // displayAPIErrorMsg( results.status , GBL_ERR_NO_CUST_DATA );
            }
            
            //return custData;
            response(custData);

        }

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_PRCSREQ;
        commonAjax(URL2, postData, custSearchCallBack, actAjaxMsg);
    }
}
/* common function for cust search using term for ebqms END */



function closeActivityPop(){
    if( localStorage.importSalesCustId != "" && localStorage.importSalesCustId != undefined ){
        navigateToImportData();
    }else{
        $('#creatactivity').modal('hide');
    }
}

var GBL_closeActReasonId = "";
var GBL_closeActReasonText = "";
function closeCreateActivity(){

    var closeActReasonId = $('#closeActReason').val();
    var closeActReasonText = $('#closeActReasonTxt').val();
    
    if( closeActReasonId == "-1" ){
        toastr.warning('Please Select Outcome');
        $('#closeActReason').focus();
        return false;

    }else if( closeActReasonText == "" ){
        toastr.warning('Please Enter Activity Close Text');
        $('#closeActReasonTxt').focus();
        return false;
    }

    GBL_closeActReasonId = closeActReasonId;
    GBL_closeActReasonText = closeActReasonText;

    $('#closeActRemarkModal').modal('hide');

    var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
    var userId = userDetail.data[0].PK_USER_ID

    if( assignedToId != userId ){
        toastr.error("You are not Authorised to perform this Activity");
        return false;
    }
    $('#txtActTitle').val('');
    $('#txtActTitle').val('');
    $('#txtActDesc').val('');
    $('#txtActDueDate').val('');
    $('#ddActAssignedTo').val('-1').selectpicker('refresh');
    $('#txtCustGrpName').val('');
    $('#actContPersonSear').val('');
    $(".default-date-picker").datetimepicker('remove');
    $('#creatactivity').modal('show');
    intdefaultdatepicker();
    
    $('#txtActTitle').focus();
}    

function assignProjectChangeEvent(){

    $('#projectListAct').change(function(){
        var currProjID = $('#projectListAct').val();
        
        var options = '<option value="-1">Select User</option>';
        if( currProjID != "" && currProjID != null ){
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            //SET GROUP LOAD
            var postData ={
                requestCase : "userListingActivityModule",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                orgId: checkAuth(2,6),
                userId: tmp_json.data[0].PK_USER_ID,
                projectId: currProjID,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
            };
            commonAjax(URL2, postData, callBackUserListingProjData, "");
            function callBackUserListingProjData(flag, data) {
                if (data.status == "Success" && flag) {

                    if( data.data != "No record found" ){

                        data.data.forEach( function( record , index ){

                            var selectedUser = "";
                            if( tmp_json.data[0].PK_USER_ID == record.userId ){
                                selectedUser = "selected";
                            }
                            options += '<option value="'+ record.userId +'" '+ selectedUser +'>'+ record.fullName +'</option>';
                        });
                    }

                    $('#ddActAssignedTo').html( options );
                    $('#ddActAssignedTo').selectpicker('refresh');

                } else {
                    $('#ddActAssignedTo').html( options );
                    $('#ddActAssignedTo').selectpicker('refresh');
                    if (flag)
                        displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
                        // toastr.error(data.status);
                    else
                        toastr.error(SERVERERROR);
                }
            }
        }else{
            $('#ddActAssignedTo').html( GBL_ALL_USER_HTML );
            $('#ddActAssignedTo').selectpicker('refresh');
        }   

    });
}

function setProjectAndCall(){

    if( currClientType == "32" ){ // REAL ESTATE CLIENT
        var projId = $('#inqListAct option:selected').attr('projid');
        if( projId != "0" && projId != "-1" &&  projId != undefined ){
            $('#projectListAct').val( projId ).trigger('change');
            $('#projectListAct').attr('disabled',true);
            $('#projectListAct').selectpicker('refresh');

        }else{
            $('#projectListAct').val( "-1" ).trigger('change');
            $('#projectListAct').removeAttr('disabled');
            $('#projectListAct').selectpicker('refresh');
        }
    }

}