    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 23-07-2018.
 * File : chargePostListingController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_CHARGE_LIST_DATA = []; // CHARGE POST JSON 

// PAGE LOAD EVENTS
function getChargeData() {

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getChargePostingData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(24,98),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addChargePostCallback,"Please wait...Adding your item.");

    function addChargePostCallback( flag , data ){
        if (data.status == "Success" && flag) {

        	console.log(data);
        	GBL_CHARGE_LIST_DATA = data.data;
			renderChargePost();

		}else{
			GBL_CHARGE_LIST_DATA = [];
			renderChargePost();
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_CHARGE_POST );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RENDER ADDED CHARGE POST ENTRIES
function renderChargePost() {	

	
	var tHead = '<table class="table display table-bordered dataTable" id="chargePostList">'+
			        '<thead>'+
			            '<tr>'+
			                '<th>#</th>'+
			                '<th>Student Membership Id</th>'+
			                '<th>Student Name</th>'+
			                '<th>Charge Date</th>'+
			                '<th>Charge Amount</th>'+
			                // '<th>Action</th>'+
			            '</tr>'+
			        '</thead>'+
			        '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

	if( GBL_CHARGE_LIST_DATA != "No record found" && GBL_CHARGE_LIST_DATA.length != 0 ){

		GBL_CHARGE_LIST_DATA.forEach(function( record , index ) {

			newRow += '<tr>'+
		                '<td>'+ (index+1) +'</td>'+
		                '<td>'+ record.studentName +'</td>'+
		                '<td>'+ record.studentName +'</td>'+
		                '<td>'+ record.chargeDate +'</td>'+
		                '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.itemPrice) +'</td>'+
		                '<td>'+
		                	// '<a class="btn btn-sm btn-danger" arrIndex="'+index+'"  onclick="deleteChargePost(this);"><i class="fa fa-trash" ></i></a>'+
		                '</td>'+
		            '</tr>';
		});

	}
	$('#chargeListingTable').html( tHead + newRow + tEnd );
	$('#chargePostList').dataTable();
}