/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : clientManagementController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
var CLIENTJSON = [];

//START FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO CLIENTJSON
function callBackGetClientListing(flag, data) {
    if (data.status == SCS && flag) {
        if (data.data) {
            CLIENTJSON = data.data;
            for (var i = 0; i < CLIENTJSON.length; i++) {
                var clientId = CLIENTJSON[i].clientId;
                var clientName = CLIENTJSON[i].clientName;
                var clientEmail = CLIENTJSON[i].clientEmail;
                var clientAddress = CLIENTJSON[i].clientAddress;
                var clientContact = CLIENTJSON[i].clientContact;
                var clientType = CLIENTJSON[i].clientType;
                var webAddress = CLIENTJSON[i].webAddress;
                var clientValidity = CLIENTJSON[i].clientValidity;

                var clientTypeTxt = "";

                var data = JSON.parse(localStorage.indeCampusRoleAuth);

                var index = findIndexByKeyValue(data.data.clientType,'ID',clientType);

                CLIENTJSON[i].clientId = clientId;
                CLIENTJSON[i].clientName = clientName;
                CLIENTJSON[i].clientEmail = clientEmail;
                CLIENTJSON[i].clientAddress = clientAddress;
                CLIENTJSON[i].clientContact = clientContact;
                CLIENTJSON[i].clientType = clientType;
                CLIENTJSON[i].webAddress = webAddress;
                CLIENTJSON[i].clientValidity = clientValidity;
                CLIENTJSON[i].clientTypeTxt = ((index != undefined && index != -1) ? data.data.clientType[index].VALUE : 'Master User');
            }
            displayClientData();
        }
    }else {
        if (flag){
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT );
        }else{
            toastr.error(SERVERERROR);
        }
    }
}
//END FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO CLIENTJSON

//START FUNCTION FOR DISPLAY DATA OF CLIENT
function displayClientData() {

    var startHtml = " <table class='display datatables-alphabet-sorting'>" +
            "<thead>" +
            "<tr>" +
            "<th>No</th>" +
            "<th>Type</th>" +
            "<th>Name</th>" +
            "<th>Email</th>" +
            "<th>Address</th>" +
            "<th>Contact No</th>" +
            "<th>Website</th>" +
            "<th>Validity End Date</th>" +
            "<th>Action</th>" +
            "</tr>" +
            "</thead><tbody>";


    var deviceData = CLIENTJSON;
    var html = "";
    for (var i = 0; i < deviceData.length; i++) {
        var arrIndex = i;
        var clientId = deviceData[i]['clientId'];
        var clientName = deviceData[i]['clientName'];
        var clientEmail = deviceData[i]['clientEmail'];
        var clientAddress = deviceData[i]['clientAddress'];
        var clientContact = deviceData[i]['clientContact'];
        var clientTypeTxt = deviceData[i]['clientTypeTxt'];
        var customerLevelTxt = deviceData[i]['customerLevelTxt'];
        var webAddress = deviceData[i]['webAddress'];
        var clientValidity = deviceData[i]['clientValidity'];
        //var deviceStatus = deviceData[i]['deviceStatus'];

        html += "<tr>" +
                "<td>" + (i+1) + "</td>" +
                "<td>" + clientTypeTxt + "</td>" +
                "<td>" + clientName + "</td>" +
                "<td>" + clientEmail + "</td>" +
                "<td>" + clientAddress + "</td>" +
                "<td>" + clientContact + "</td>" +
                "<td>" + webAddress + "</td>" +
                "<td>" + mysqltoDesiredFormat( clientValidity , 'dd-MM-yyyy') + "</td>" +
                "<td><a class='btn btn-xs btn-primary btn-ripple' data-arrIndex='"+arrIndex+"' data-clientId='"+clientId+"' data-clientName='"+clientName+"' data-clientEmail='"+clientEmail+"' data-clientAddress='"+clientAddress+"' data-clientContact='"+clientContact+"' onclick='getClientDetails(this)'><i class='fa fa-pencil'></i></a></td>" +
                "</tr>";
    }
    var endHtml = "</tbody></table>";
    var finalHtml = startHtml + html + endHtml;

    $("#clientList").html(finalHtml);
    TablesDataTables.init();
    TablesDataTablesEditor.init();

}
//END FUNCTION FOR DISPLAY DATA OF CLIENT

//START FUNCTION FOR GET CLIENT DETAILS BY ARRAY INDEX AND ALL DATA
function getClientDetails(clientData){
    if(checkAuth(4,15,1) == -1){
        toastr.error('You are not authorised to perform this activity');
    }else{
        var arrIndex = $(clientData).attr("data-arrIndex");
        var clientId = CLIENTJSON[arrIndex]['clientId'];
        var clientName = CLIENTJSON[arrIndex]['clientName'];
        var clientEmail = CLIENTJSON[arrIndex]['clientEmail'];
        var clientAddress = CLIENTJSON[arrIndex]['clientAddress'];
        var clientContact = CLIENTJSON[arrIndex]['clientContact'];    
        var clientType = CLIENTJSON[arrIndex]['clientType'];    
        var customerLevel = CLIENTJSON[arrIndex]['customerLevel'];  
        var customerAccess = CLIENTJSON[arrIndex]['customerAccess'];  
        var webAddress = CLIENTJSON[arrIndex]['webAddress'];  
        var clientValidity = CLIENTJSON[arrIndex]['clientValidity'];  

        $("#arrIndex").val(arrIndex);
        $("#clientId").val(clientId);
        $("#clientName").val(clientName);
        $("#clientEmail").val(clientEmail);
        $("#clientAddress").val(clientAddress);
        $("#clientContact").val(clientContact);    
        $("#clientType").val(clientType);    
        $("#webAddress").val(webAddress);    
        $("#txtClientValidity").val(clientValidity);    
        $('input:radio[name=rdoInqMode][value='+customerLevel+']').prop('checked', true);   
        $("#clientType").selectpicker('refresh');    

        var updateClientJson = {
            clientId : CLIENTJSON[arrIndex]['clientId'],
            clientName : CLIENTJSON[arrIndex]['clientName'],
            clientEmail : CLIENTJSON[arrIndex]['clientEmail'],
            clientAddress : CLIENTJSON[arrIndex]['clientAddress'],
            clientContact : CLIENTJSON[arrIndex]['clientContact'],
            clientType : CLIENTJSON[arrIndex]['clientType'],
            customerLevel : CLIENTJSON[arrIndex]['customerLevel'],
            customerAccess : CLIENTJSON[arrIndex]['customerAccess'],
            webAddress : CLIENTJSON[arrIndex]['webAddress'],
            clientValidity : CLIENTJSON[arrIndex]['clientValidity'],
        }

        localStorage.indeCampusClientData= JSON.stringify( updateClientJson );
        navigateToclientMgmt("1");

        if( customerLevel == "1" ){
            $('#custAccLab').show();
        }
        if( customerAccess != "0" ){
            $('#customerAccessChk').prop('checked',true);
        }
    }
    
}
//END FUNCTION FOR GET CLIENT DETAILS BY ARRAY INDEX AND ALL DATA

//START FUNCTION FOR CALL API FOR CLIENT LISTING
function getClientData() {
    var postData = {
        requestCase: 'getClientListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, callBackGetClientListing, GETTINGCLIENT);
}
//END FUNCTION FOR CALL API FOR CLIENT LISTING

//START FUNCTION FOR CREATE CLIENT 
function createClient() {

    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(4, 14, 1) == -1){
            toastr.error('You are not authorised to perform this activity');
            return false;
        }
    }
    var arrIndex = $("#arrIndex").val().trim(); //DEVICE NAME
    var clientId = $("#clientId").val().trim(); //DEVICE NAME
    var clientNameadd = $("#clientName").val().trim(); //DEVICE NAME
    var clientEmailadd = $("#clientEmail").val().trim(); //DEVICE TYPE
    var clientAddressadd = $("#clientAddress").val().trim(); //DEVICE TYPE
    var clientContactadd = $("#clientContact").val().trim(); //DEVICE TYPE
    var clientType = $("#clientType").val();
    var webAddress = $("#webAddress").val();
    var clientValidity = $("#txtClientValidity").val();
    var clientTypeTxt = $("#clientType option:selected").text();
    var customerLevel = $('input[type=radio][name="rdoInqMode"]').filter(":checked").val();
    var custAccessVal = "0";

    // CHECK VALIDATIONS
    if (clientType == "-1"){
        toastr.warning("Please Select Client Type");
        $("#clientId").focus();
        return false;

    }else if( clientNameadd.length == 0 ){
        toastr.warning("Please Enter Client Name");
        $("#clientName").focus();
        return false;

    }else if( clientEmailadd.length == 0){
        toastr.warning("Please Enter Email Address");
        $("#clientEmail").focus();
        return false;

    }else if(clientAddressadd.length == 0){
        toastr.warning("Please Enter Address");
        $("#clientAddress").focus();
        return false;

    }else if(clientContactadd.length != MOBILE_LENGTH) {
        toastr.warning("Please Enter Valid contact Number");
        $("#clientContact").focus();
        return false;

    }else if(!validateEmail(clientEmailadd)){
        toastr.warning('Please Enter Valid Email Id');
        return false;
    
    }else if( clientValidity == "" ){
        toastr.warning("Please Select Client Validity Date");
        $('#txtClientValidity').datepicker('show');
        return false;
    
    }else if( customerLevel == "1" ){
        var cstAccChk = $('#customerAccessChk').is(':checked');
        if( cstAccChk ){   
            custAccessVal = "1";
        }
    }

    function addValueToJSON(flag,data){
        if(data.status == SCS && flag){

        // Updated by Nazim starts here

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase : "loadauth",
            userId : tmp_json.data[0].PK_USER_ID,
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            orgId : localStorage.indeCampusBranchIdForChkAuth,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
        commonAjax(COMMONURL,postData, 
            function(a,b){
                //log(b); return false;
                if(b.status==SCS){
                    localStorage.indeCampusRoleAuth = JSON.stringify(b);
                    localStorage.indeCampusUserDetail = JSON.stringify(tmp_json);

                   
                }else{
                    toastr.warning(b.status);
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_LOAD_AUTH );
                    return false;
                }
            }, "Please wait... Retriving Permission Data.");      

        // Updated by Nazim ends here


            var clientIdNew = data.data;
            var newClient = {
                clientId:clientIdNew,
                clientName:clientNameadd,
                clientEmail:clientEmailadd,
                clientAddress:clientAddressadd,
                clientContact:clientContactadd,
                clientType:clientType,
                clientTypeTxt:clientTypeTxt,
                customerLevel:customerLevel,
                customerLevelTxt:(customerLevel == "1") ? "Customer Level" : "User Level",
                customerAccess:custAccessVal,
                webAddress:webAddress,
                clientValidity:ddmmyyToMysql(clientValidity),
            }
            CLIENTJSON.push(newClient);
        }
        toastr.success('New Client has been Created');
        displayClientData();

    }
    function updateValueToJSON(flag,data){
        if(data.status == SCS && flag){
            
            var tempClient = {
                clientId : CLIENTJSON[arrIndex]['clientId'],
                clientName : clientNameadd,
                clientEmail : clientEmailadd,
                clientAddress : clientAddressadd,
                clientContact : clientContactadd,
                clientType : clientType,
                clientTypeTxt:clientTypeTxt,
                customerLevel:customerLevel,
                customerLevelTxt:(customerLevel == "1") ? "Customer Level" : "User Level",
                customerAccess:custAccessVal,
                webAddress:webAddress,
                clientValidity:ddmmyyToMysql(clientValidity),
            }

            CLIENTJSON[ arrIndex ] = tempClient;

            toastr.success('Client has been Updated');
            displayClientData();
        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_CLIENT_FAIL );
        }
    }
    
    if(clientId==""){ // INSERT
        var postData = {
            requestCase: 'createClient',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9, 33),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            clientName: clientNameadd,
            clientEmail: clientEmailadd,
            clientAddress: clientAddressadd,
            clientContact: clientContactadd,
            clientType: clientType,
            customerLevel: customerLevel,
            customerAccess:custAccessVal,
            webAddress:webAddress,
            clientValidity:ddmmyyToMysql(clientValidity),
        }
        // console.log(postData); return false;
        commonAjax(COMMONURL, postData, addValueToJSON, CREATECLIENT);
    }else{ //UPDATE
        var postData = {
            requestCase: 'updateClient',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9, 35),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            clientName: clientNameadd,
            clientEmail: clientEmailadd,
            clientAddress: clientAddressadd,
            clientContact: clientContactadd,
            clientType: clientType,
            customerLevel: customerLevel,
            clientIdUpdate: clientId,
            customerAccess:custAccessVal,
            webAddress:webAddress,
            clientValidity:ddmmyyToMysql(clientValidity),
        }
        // console.log(postData); return false;
        commonAjax(COMMONURL, postData, updateValueToJSON, UPDATECLIENT);
    }

    resetClientData();

}
//END FUNCTION FOR CREATE CLIENT 

//SATRT FUNCTION FOR RESET DATA OF CLIENT AFETR UPDATE/ADD
function resetClientData(){
    $("#clientType").val('-1'); //ARRAY INDEX
    $("#clientType").selectpicker('refresh');
    $("#arrIndex").val(''); //ARRAY INDEX
    $("#clientId").val(''); //CLIENT ID
    $("#clientName").val(''); //CLIENT NAME
    $("#clientEmail").val(''); //CLIENT EMAIL
    $("#clientAddress").val(''); // CLIENT ADDRESS
    $("#clientContact").val(''); //CLIENT CONTACT
    $('#radio-05').prop('checked',true);
    $('#customerAccessChk').prop('checked',false);
    $('#custAccLab').hide();
}
//END FUNCTION FOR RESET DATA OF CLIENT AFETR UPDATE/ADD

getClientData(); //CALL FUNCTION FOR DISPLAY DATA

function pageInitialEvents(){

    $('input[name=rdoInqMode]').change(function() {
        var custConfig =  $('input[name=rdoInqMode]:checked').val()
        if( custConfig == "1" ) {
            $('#custAccLab').show();
        }else{
            $('#customerAccessChk').prop('checked',false);
            $('#custAccLab').hide();
        } 
    });
}