/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : inquiryCreateController.
 * File Type : .js.
 * Project : POTG
 *
 * */

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

var itemJson = [];
var itemGroupJson = [];
var groupJson = [];
var ITEMS = [];
var temp_inq_Trans = [];
var vSnapShot = "";
var custCountryID = "";
var custCityID = "";
var custStateID = "";
var custId = "-2";
var phoneId = "";
var potPrice = 0;

function getEmpListingData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userListingActivityModule',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2,6),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getEmpListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getEmpListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // console.log(empData);return false;
            var brListOpt = '';
            for( var i=0;i<data.data.length; i++ ){
                if( data.data[i].userBranchId.search( localStorage.indeCampusBranchIdForChkAuth ) > "-1" ){
                    if( tmp_json.data[0].PK_USER_ID == data.data[i].userId ){
                        brListOpt += '<option value="'+ data.data[i].userId +'" selected>'+ data.data[i].fullName +'</option>';
                    }else{   
                        brListOpt += '<option value="'+ data.data[i].userId +'">'+ data.data[i].fullName +'</option>';
                    }
                }
            }
            $('#assUser').html( brListOpt );
            $('#assUser').selectpicker();

        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        }

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }
}

function initRoleAuthData(){
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    setOption("0", "inquirytype", data.data.inqType, "---Select Inquiry Type---");
    setOption("0", "inquirysource", data.data.inqSource, "---Select Inquiry Source---");

    $('#inquirytype').selectpicker();
    $('#inquirysource').selectpicker();

    if ($(data.data.inqDefaultSetting).length > 0) {
        $("#inquirytype").val(data.data.inqDefaultSetting.inqTypeId);
        $("#inquirysource").val(data.data.inqDefaultSetting.inqSrcId);
    }

    if( (localStorage.POTGinqTypeForQuick == "") || (localStorage.POTGinqTypeForQuick == undefined) || (localStorage.POTGinqTypeForQuick == null) || (localStorage.POTGinqSourceForQuick == "") || (localStorage.POTGinqSourceForQuick == undefined) || (localStorage.POTGinqSourceForQuick == null) ){

        setTimeout(function() {

            // $('#inquirytype').val( localStorage.POTGinqTypeForQuick );
            // $('#inquirytype').selectpicker('refresh');
            
            // $('#inquirysource').val( localStorage.POTGinqSourceForQuick );
            // $('#inquirysource').selectpicker('refresh');

            $('#inqTypeSource').modal('show');
        },1000);
        
    }
}

function saveInqTypeSource(){

    var inqType = $("#inquirytype").val();
    var inqSource = $("#inquirysource").val();

    if( inqType == "-1" ){
        toastr.warning('Please Select Type');
        $('#inquirytype').focus();
        return false;

    }else if( inqSource == "-1" ){
        toastr.warning('Please Select Source');
        $('#inquirysource').focus();
        return false;
    }else{
        localStorage.POTGinqTypeForQuick = inqType;
        localStorage.POTGinqSourceForQuick = inqSource;
        $('#inqTypeSource').modal('hide');

        addFocusId( 'custSearch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

function saveCustDetail() {
    //log(ITEMS);
    var searchValue = GBL_SELECTEDCUSTMOBNO.split("|--|");
    //var index = findIndexByKeyValue(ITEMS, "id", localStorage.POTGselectedCustIdInSearch);
    var index = findIndexByKeyValueAndCond(ITEMS, "id","mobile", searchValue[0],searchValue[1]);

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    console.log(ITEMS[index]);
    
//    $('#mobileNumber').val(ITEMS[index].mobile);
    setNumberInIntl( 'mobileNumber' , ITEMS[index].mobile );
    
    custId = ITEMS[index].id;
    phoneId = ITEMS[index].phoneId;
    custEmail = ITEMS[index].email;
    custCityID = ITEMS[index].cityId;
    custCountryID = ITEMS[index].countryId;
    custStateID = ITEMS[index].stateId;

    contactPerSonData = ITEMS[index];
    autoCompleteForContactPerson("contPerson", saveContactPerDetail);

    localStorage.removeItem('POTGselectedCustIdInSearch');

}

function autoCompleteForContactPerson(id,callback) {
    
    var source = [];
    if( contactPerSonData.length != 0 ){
        for( var i=0; i<contactPerSonData.cpAllList.length; i++ ){
            source.push( contactPerSonData.cpAllList[i].CONTACTNAME+ ':' + contactPerSonData.cpAllList[i].CONTACT_NUMBER );
        }
    
    }
    $("#" + id).typeahead({
        onSelect: function (item) {
            console.log(item);
            setTimeout(function(){
                var contName = (item.value).split(':')[0];
                var contMobile = (item.value).split(':')[1];
                $('#contPerson').val(contName);  
//                $('#ContmobileNumber').val(contMobile);  
                setNumberInIntl( 'ContmobileNumber' , contMobile);
            },50);
        },
        source: source,
    });
}

function saveContactPerDetail(){

}

function autoCompleteForItemList(id,callback){

    itemJson = JSON.parse(localStorage.POTGquickItemList);
    $("#" + id).typeahead({
        onSelect: function (item) {
            console.log(item);
            
            var itemIndex = findIndexByKeyValue(itemJson,"id",item.value);

            var endDate = mysqltoDesiredFormat( itemJson[itemIndex].priceValidityEndDate , "dd-MM-yyyy" );
            // if( new Date(itemJson[itemIndex].priceValidityEndDate +" 11:59 PM") < new Date()){
            //     toastr.warning(itemJson[itemIndex].itemName+' Price validity is Over on <b>"'+ endDate +'"</b>, So you need to set it from Client Master page');
            //     $('#itemListing').val('');
            //     return false;
            // }
            
            setTimeout(function(){
                console.log(item); 
                $('#itemListing').attr('itemId',item.value); 
            },50);
        },
        source: JSON.parse(localStorage.POTGquickItemList),
    });
}

function saveItemList(){

}

function autoCompleteForGroupList(id,callback){

    itemGroupJson = JSON.parse(localStorage.POTGquickGroupList);
    $("#" + id).typeahead({
        onSelect: function (item) {
            console.log(item);
            setTimeout(function(){
                console.log(item); 
                $('#groupListing').attr('groupId',item.value); 
            },50);
        },
        source: JSON.parse(localStorage.POTGquickGroupList),
    });
}

function saveGroupList(){

}

function getItemList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'getItemListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        keyWord: "",
        type: 1,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, callBackGetItemsListing, "Please Wait... Getting the Item listing");

    function callBackGetItemsListing(flag, data)
    {
       if (data.status == "Success" && flag) {

            var itemData = data.data;
            var finalItemData = [];
            itemData.forEach(function(record , index){

                var tempJson = {
                    clientId: record.clientId,
                    itemGroupId: record.itemGroupId,
                    itemGroupName: record.itemGroupName,
                    itemId: record.itemId,
                    id: record.itemId,
                    itemName: record.itemName,
                    name: record.itemName,
                    data: record.itemName,
                    itemPrice: record.itemPrice,
                    itemSubType: record.itemSubType,
                    itemType: record.itemType,
                    itemUomId: record.itemUomId,
                    itemUomName: record.itemUomName,
                    orgId: record.orgId,
                    priceValidityEndDate: record.priceValidityEndDate,
                    validityEndDate: record.validityEndDate,
                }
                finalItemData.push( tempJson );
            });

            localStorage.POTGquickItemList = JSON.stringify( finalItemData );
            itemJson = finalItemData;
            autoCompleteForItemList("itemListing",saveItemList);
        }
        else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEMS );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function getGroupList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(13, 50),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, callBackGetGroupListing, "Please Wait... Getting the group listing");

    function callBackGetGroupListing(flag, data)
    {
       if (data.status == "Success" && flag) {
           
            var groupData = data.data;
            var finalGroupData = [];
            groupData.forEach(function(record , index){

                var tempJson = {
                    id: record.PK_ITEM_GROUP_ID,
                    name: record.GROUP_NAME,
                    data: record.GROUP_NAME,
                    clientId: record.FK_CLIENT_ID
                }
                finalGroupData.push( tempJson );
            });

            localStorage.POTGquickGroupList = JSON.stringify( finalGroupData );
            itemGroupJson = finalGroupData;
            autoCompleteForGroupList("groupListing",saveGroupList);

        }
        else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );
            else
                toastr.error(SERVERERROR);
        }
    }
}


// function autoCompleteForGroup(id,callback) {
    
//     $("#" + id).typeahead({
        
//         onSelect: function (item) {
           
//             console.log(item);
//             $('#groupListing').attr('groupId',item.value);
//         },
//         ajax: {
//             url: SEARCHURL,
//             timeout: 500,
//             triggerLength: 2,
//             displayField: "name",
//             method: "POST",
            // headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
//             crossDomain: true,
//             preDispatch: function (query) {
                    // addRemoveLoader(1);
//                 var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//                 var postdata = {
//                     requestCase: "getGroupListing",
//                     clientId: tmp_json.data[0].FK_CLIENT_ID,
//                     userId: tmp_json.data[0].PK_USER_ID,
//                     dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//                     orgId: checkAuth(13, 50),
//                     keyWord: query
//                 };
                
                // removeLoaderInTime( REMOVE_LOADER_TIME );
//                 return {postData: postdata};
//             },
//             preProcess: function (data) {
    // addRemoveLoader(0);
//                 if (data.success === false) {
//                     // Hide the list, there was some error
//                     return false;
//                 }
//                 // // We good!
//                 ITEMSGROUP= [];
//                 if (data.status != "No Item Group Found") {
//                     $.map(data.data, function (data) {
                        
//                         var tempJson = '';
//                         tempJson = {
//                             id: data.PK_ITEM_GROUP_ID,
//                             name: data.GROUP_NAME,
//                             data: data.GROUP_NAME,
//                             clientId: data.FK_CLIENT_ID
//                         }
//                         itemGroupJson.push(tempJson);
//                         ITEMSGROUP.push(tempJson);
//                     });

//                     return ITEMSGROUP;
//                 }
//                 console.log(data);
//             }
//         }
//     });
// }

// function saveItemGroupDetail(){
    
// }

// function AutoCompleteForItem(id,callback) {
    
//     $("#" + id).typeahead({
        
//         onSelect: function (item) {
           
//             console.log(item);
//             $('#itemListing').attr('itemId',item.value);
//             //var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
//             //$('#itemGroupText').val(ITEMS[itemIndex].itemGroupName);
//         },
//         ajax: {
//             url: SEARCHURL,
//             timeout: 500,
//             triggerLength: 2,
//             displayField: "name",
//             method: "POST",
            // headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
//             crossDomain: true,
//             preDispatch: function (query) {
    // addRemoveLoader(1);
//                 var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//                 var postdata = {
//                     requestCase: "getItemListing",
//                     clientId: tmp_json.data[0].FK_CLIENT_ID,
//                     userId: tmp_json.data[0].PK_USER_ID,
//                     dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//                     orgId: checkAuth(4, 14),
//                     keyWord: query
//                 };
                
                // removeLoaderInTime( REMOVE_LOADER_TIME );
//                 return {postData: postdata};
//             },
//             preProcess: function (data) {
    // addRemoveLoader(0);
//                 if (data.success === false) {
//                     // Hide the list, there was some error
//                     return false;
//                 }
//                 console.log(data.status);
//                 // // We good!
//                 ITEMS = [];
//                 if (data.status != "No record found") {
//                     $.map(data.data, function (data) {
                        
//                         var tempJson = '';
//                         tempJson = {
//                             id: data.itemId,
//                             name: data.itemName,
//                             data: data.itemName,
//                             clientId: data.clientId,
//                             itemId: data.itemId,
//                             itemName: data.itemName,
//                             itemPrice: data.itemPrice,
//                             itemSubType: data.itemSubType,
//                             itemType: data.itemType,
//                             orgId: data.orgId,
//                             validityEndDate: data.validityEndDate,
//                             itemGroupName: data.itemGroupName
//                         }
//                         itemJson.push(tempJson);
//                         ITEMS.push(tempJson);
//                     });

//                     return ITEMS;
//                 }
//                 console.log(data);
//             }
//         }
//     });
// }

// function saveItemDetail(){

// }

function addItemRow(itemType){
    
    var selectedItemName = $('#itemListing').attr('itemid');
    var selectedGroupName = $('#groupListing').attr('groupid');
    if(itemType == 1){
        
        if((selectedItemName == '' || selectedItemName == undefined)){
            toastr.warning('Please Select Item from Autocomplete');
            $('#itemListing').focus();
            return false;
        }

        var index = findIndexByKeyValue(itemJson,"itemId",selectedItemName);
        var sequenceId = (temp_inq_Trans.length+1);
        var  tempData = {
            cancleFlag : "1",
            itemId : itemJson[index].itemId,
            itemName : itemJson[index].itemName,
            groupName : itemJson[index].itemGroupName,
            itemQty : "1",
            eventStatus : "",
            itemPrice : (Number( itemJson[index].itemPrice * 1)).toFixed(2),
            itemBasePrice : itemJson[index].itemPrice,
            itemType : "ITEM",
            itemRemark : "",
            itemCat : "1", // 1 = item 2 - header text
            sequenceId : sequenceId 
        }
        var dupInd = findIndexByKeyValue( temp_inq_Trans,'itemId',itemJson[index].itemId );
        if( dupInd == "-1" ){
            temp_inq_Trans.push(tempData); 
        }else{
            toastr.warning('This Item already Added in the Cart');
        }

        $('#itemListing').val('');
        $('#itemListing').attr('itemid','');

    }else if(itemType == 2){
        if((selectedGroupName == '' || selectedGroupName == undefined)){
            toastr.warning('Please Select Group from Autocomplete');
            $('#groupListing').focus();
            return false;
        }
        var index = findIndexByKeyValue(itemGroupJson,"id",selectedGroupName);
        var sequenceId = (temp_inq_Trans.length+1);
        var  tempData = {
            cancleFlag : "1",
            itemId : itemGroupJson[index].id,
            itemName : "",
            groupName : itemGroupJson[index].name,
            itemQty : "1",
            eventStatus : "",
            itemPrice : "0.00",
            itemBasePrice : "0.00",
            itemType : "GROUP",
            itemRemark : "",
            itemCat : "1", // 1 = item 2 - header text
            sequenceId : sequenceId 
        }
        var dupInd = findIndexByKeyValue( temp_inq_Trans,'itemId',itemGroupJson[index].id );
        if( dupInd == "-1" ){
            temp_inq_Trans.push(tempData); 
        }else{
            toastr.warning('This group already Added in the Cart');
        }
        $('#groupListing').val('');
        $('#groupListing').attr('groupid','');

    }
    // console.log(temp_inq_Trans);
    // return false;
    addRowTable( temp_inq_Trans );
    
}

function addRowTable(itemJson){

    var thead = '<table id="tableListingDataTab" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>Sr No</th>'+
                            '<th>Item / Group</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    for ( var i=0; i<itemJson.length; i++ ){
        if(itemJson[i].itemType == "ITEM"){
            newRow += '<tr>'+
                        '<td>'+ itemJson[i].sequenceId +'</td>'+
                        '<td>'+ itemJson[i].itemName +'</td>'+
                    '</tr>';
        }else{
            newRow += '<tr class="group">'+
                        '<td>'+ itemJson[i].sequenceId +'</td>'+
                        '<td colspan="6">'+ itemJson[i].groupName +'</td>'+
                    '</tr>';
        }
        potPrice = parseFloat( potPrice ) + parseFloat( itemJson[i].itemPrice );
    }
    
    $('#tableListing').html( thead + newRow + tfoot );
    $("#tableListingDataTab").DataTable({
        bSort : false,
        searching: false,
        paging: false,
        "stateSave": true
    });
    $('#tableListingDataTab_info').hide();
}

function populateCity(stateId){
    
    if(stateId == "-1"){
        setOption("0", "custCityId", "", "--Select City --");
    }else{
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(data.data.cityStateListing,"ID",stateId);
        cityListing = "";
        if(index != -1){
            var cityListing  = data.data.cityStateListing[index]['cityListing'];
        }
        setOption("0", "custCityId", cityListing, "--Select City --");
    }
    
    $("#custCityId").selectpicker("refresh");    
}

function createQuickInquiry(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var POTGroleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }else if( checkAuth(4, 14, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var custName = $('#custSearch').val().trim();
    var contPerson = $('#contPerson').val().trim();
    var mobileNumberVal = getIntlMobileNum("mobileNumber");    // GET MOBILE NUM
    // var mobileNumber = $('#mobileNumber').val();
    // var ContmobileNumber = $('#ContmobileNumber').val();
    var ContmobileNumber = getIntlMobileNum("ContmobileNumber");    // GET MOBILE NUM
    var email = $('#email').val().trim();
    var custCountryId = custCountryID;
    var custStateId = custStateID;
    var custCityId = custCityID;
    var inquirytype = localStorage.POTGinqTypeForQuick;
    var inquirysource = localStorage.POTGinqSourceForQuick;
    var projectName = "Quick Inquiry";
    var inquiryRemarks = "Inquiry Created In Quick Mode";
    var followupDate = $('#followupDate').val();
    var remarks = $('#remarks').val().trim();
    var followUpTime = "";

    var potAmt = potPrice;

    var assUserId = $('#assUser').val();

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    var custSearchFlag = false;

    var stateExhib = "";
    var cityExhib = "";
    var exhibId = "";

    if( custName.length == 0 ){
        toastr.warning('Please Enter Customer Name');
        $('#custSearch').focus();
        return false;
    
    }else if( email != "" && !validateEmail(email) ){
        toastr.warning('Please Enter valid Email');
        $('#email').focus();
        return false;
        
    }else if( !checkValidMobile( "mobileNumber" ) ){
    // }else if( mobileNumber.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Mobile Number');
        $('#mobileNumber').focus();
        return false;
  
    }else if( contPerson.length == 0){
       toastr.warning('Please Enter Contact Name');
       $('#contPerson').focus();
       return false;  
    
    }else if( temp_inq_Trans.length == 0 ){
        toastr.warning('Please Enter Items');
        $('#itemName').focus();
        return false;
    
    }else if( inquirytype == "-1" || inquirytype == undefined ){
        toastr.warning('Please select Inquiry Type');
        $('#inquirytype').focus();
        return false;
    
    }else if( inquirysource == "-1" || inquirysource == undefined ){
        toastr.warning('Please select Inquiry Source');
        $('#inquirytype').focus();
        return false;
    
    }else if( followupDate == "" ){
        toastr.warning('Please Select followup Date / Time');
        $('#followupDate').datetimepicker('show')
        return false;

    // }else if( followUpTime == "" ){
    //     toastr.warning('Please Select followup Time');
    //     $('#dueTime').focus();
    //     return false;
    
    // }else if( !validateDateTimeFormat(followUpTime) ){
    //     toastr.warning('Please Select Proper followup Time eg:HH:MM (AM/PM) like 10:00 AM');
    //     return false;

    }else if( remarks.length == 0 ){
        toastr.warning('Please Enter Remarks');
        $('#remarks').focus();
        return false;

    
    }else{

        followupDate = (( $('#followupDate').val() ).split('|')[0]).trim();
        followUpTime = (( $('#followupDate').val() ).split('|')[1]).trim();
        
        var remindDateTime = ddmmyyToMysql(followupDate) +" "+followUpTime;
        remindTimeSend = minusFromTime( new Date( remindDateTime ) , 30 );

        var dueStart = new Date( ddmmyyToMysql(followupDate) +" "+followUpTime );
        var currTime = new Date( );
        
        if( +dueStart < +currTime ){
            toastr.warning("Followup Date / Time should not be less than Current Date / Time");
            return false;
        }

        var diffTime = ( (dueStart.getTime() - currTime.getTime())/ 1000 ) / 60;
        if( diffTime < 30 ){
            remindTimeSend = followUpTime;
        }

        // Add 30 days plus in closerBy days starts here
        
        var newdate = new Date( ddmmyyToMysql(followupDate) );
        newdate.setDate(newdate.getDate() + 30);
        
        var dd = newdate.getDate();
        var mm = newdate.getMonth() + 1;
        var y = newdate.getFullYear();
        
        var closerBy = y+'-'+mm+'-'+dd;

        // Add 30 days plus in closerBy days ends here

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
       
        var catName = "INQUIRY";
        var statusInd = findIndexByKeyValue(data.data.statusListing,"statusCatName",catName);
       
        var priorityLevel = "1";
       
        var statusPriority = findIndexByKeyValue(data.data.statusListing[statusInd].statusListing,"statusPriority",priorityLevel);
        var inqStatus = data.data.statusListing[statusInd].statusListing[statusPriority].ID;

        var custType = "5"; // 5 - Other Type Customer  ||  4 - Corporate Type Customer
        if( contPerson != "" ){
            custType = "4";
        }

        if( custSearchFlag == true ){
            custCityId = '';
            custStateId = '';
            custCountryID = '';
        }

        if( custId == "-2" ){
            ContmobileNumber = mobileNumberVal;
        }

        var custFName = "";
        var custLName = "";
        if( custName.split(" ").length == 1 ){
            custFName = custName.split(" ")[0];
            custLName = "";

        }else if( custName.split(" ").length == 2 ){
            custFName = custName.split(" ")[0];
            custLName = custName.split(" ")[1];

        }else if( custName.split(" ").length == 3 ){
            custFName = custName.split(" ")[0] + ' ' +custName.split(" ")[1];
            custLName = custName.split(" ")[2];

        }else if( custName.split(" ").length == 4 ){
            custFName = custName.split(" ")[0] + ' ' +custName.split(" ")[1];
            custLName = custName.split(" ")[2] + ' ' +custName.split(" ")[3];

        }else {
            custFName = custName.split(" ")[0] + ' ' +custName.split(" ")[1];
            custLName = custName.split(" ")[2] + ' ' +custName.split(" ")[3] + ' ' +custName.split(" ")[4] + ' ' +custName.split(" ")[5];
        }

        var custMaster = {
            custfName : custFName,
            custLName : custLName,
            custType : custType,
            custMobile : mobileNumberVal,
            custEmail : email,
            custEthnicity : "",
            contactName : contPerson,
            contactNumber : ContmobileNumber,
            countryId : custCountryID,
            cityId : custCityId,
            stateId : custStateId,
            vSnapShot: vSnapShot,
            phoneId: phoneId,
        }
        
        var inqMasterData = {
            inqDate : mysqltoDesiredFormat(new Date(),'yyyy-MM-dd'),
            sourceOf : inquirysource,
            followupFlag : "0",
            orgAssign : localStorage.indeCampusBranchIdForChkAuth,
            companyId : tmp_json.data[0].FK_CLIENT_ID,
            inqType : inquirytype,
            inqMode : "1", // MODE OF GENERATION
            inqModeType : "Normal Customer Inquiry", // INQ MODE WHERE INQ CREATE
            projectName : projectName,
            inquiryRemark : inquiryRemarks,
            inqAssignTo : assUserId,
            contactId : 0,  // NA
            custId : custId,
            inqStatus : inqStatus,
            inq_trans : temp_inq_Trans,
            custMaster : custMaster,
            closerBy : closerBy,
            potAmt : potAmt,
            followUpTime : followUpTime,
            stateExhib : stateExhib,
            cityExhib : cityExhib,
            exhibId : exhibId,
        }

        var postData = {
            requestCase: "createInquiry",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4, 13),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            followupDate: ddmmyyToMysql(followupDate),
            followupRemark: remarks,
            inq_master : inqMasterData,
            reminderTime : ddmmyyToMysql(followupDate)+ '|' +remindTimeSend
        };
        // console.log(postData); return false;
        commonAjax(INQURL, postData, createInquiryTroughPopupCallBack,CREATINGINQDATA);
    }
    
}


function createInquiryTroughPopupCallBack(flag, data){
    if (data.status == "Success" && flag) {
        
        toastr.success("Inquiry Created Successfully");
        resetForm();
        $(".no-display-2").toggle();
        $(".followup").hide();

    }else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_CREATE_INQ );
        else
            toastr.error(SERVERERROR);
    }
}

function assignAttachementEventForCard() {
    // grab your file object from a file input
    $('#visitingCard').change(function () {

        if( $('#visitingCard').val() == "" ){
            vSnapShot = "";
            return false;
        }
        $("#ajaxloader").removeClass("hideajaxLoader");
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase: "actAttachmentUploadOnly",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(21, 71),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };

        $("#postDataVCard").val(JSON.stringify(temp_json));

        setTimeout(function(){
            $("#fileuploadVcard").off().on('submit', (function (e) {
                if( $('#visitingCard').val() == "" ){
                    return false;
                }
                addRemoveLoader(1);
                e.preventDefault();
                $.ajax({
                    url: UPLOADURL,   // Url to which the request is send
                    type: "POST",       // Type of request to be send, called as method
                    data: new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                    contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                    cache: false,// To unable request pages to be cached
                    headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                    processData: false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                    success: function (data)// A function to be called if request succeeds
                    {
                        data = JSON.parse(data);
                        log(data);
                        if (data.status == "Success") {
                            var fileLink = data.data.fileLink;
                            vSnapShot = fileLink;
                        } else {
                            toastr.warning(data.status);
                        }
                        // $("#ajaxloader").addClass("hideajaxLoader");
                        addRemoveLoader(0);
                    },
                    error: function (jqXHR, errdata, errorThrown) {
                        log("error");
                        // $("#ajaxloader").addClass("hideajaxLoader");
                        addRemoveLoader(0);
                    }
                });
            }));
            $("#fileuploadVcard").submit();
        },100);
    });
}

function removeFile(){
    $('#visitingCard').val('');
    $(".fileinput").fileinput("clear");
}

function navigateToFirst(){
    $('.panel').hide();
    removeFile();
    $('.custdetail').show();
}

function resetForm(){

    $('#custSearch').val('');
    $('#contPerson').val('');
    $('#mobileNumber').val('');
    $('#visitingCard').val('');
    $('#dueTime').val('');
    $('#groupListing').val('');
    $('#groupListing').attr('groupid','');
    $('#itemListing').val('');
    $('#itemListing').attr('itemid','');
    $('#followupDate').val('');
    $('#remarks').val('');
    $('#email').val('');
    custEmail = "";
    custCountryID = "";
    custCityID = "";
    custStateId = "";
    if( custId != "-2" ){
        $("#contPerson").data('typeahead').source = [];
    }
    custId = "-2";
    phoneId = "";
    temp_inq_Trans = [];
    addRowTable('');
    removeFile();
}

function pageInitialEvents(){
    
    localStorage.POTGinqTypeForQuick = "";
    localStorage.POTGinqSourceForQuick = "";

    getItemList();
    getGroupList();

    addRowTable('');

    localStorage.POTGselectedCustIdInSearch = "";
    commonAutoCompleteForCustomer("custSearch", saveCustDetail);

    // autoCompleteForGroup("groupListing", saveItemGroupDetail);
    // AutoCompleteForItem("itemListing", saveItemDetail);

    initRoleAuthData();

    assignAttachementEventForCard();
    getEmpListingData();

    $('#dueTime').clockface();

    $('input[type=radio][name=itemgroup]').change(function() {
        if (this.value == 'group') {
            $('#groupDiv').show();
            $('#itemDiv').hide();
            $('#addItemButton').attr('onclick','addItemRow(2)');
        }
        else if (this.value == 'item') {
            $('#groupDiv').hide();
            $('#itemDiv').show();
            $('#addItemButton').attr('onclick','addItemRow(1)');
        }
    });
    

    $("#nextpanel").click(function() {

        var custName = $('#custSearch').val().trim();
        var contPerson = $('#contPerson').val().trim();
        // var mobileNumberVal = $('#mobileNumber').val();
        var mobileNumberVal = getIntlMobileNum("mobileNumber");    // GET MOBILE NUM
        var email = $('#email').val();

        if( custName.length == 0 ){
            toastr.warning('Please Enter Customer Name');
            $('#custSearch').focus();
            return false;
        
        }else if( contPerson.length == 0){
           toastr.warning('Please Enter Contact Name');
           $('#contPerson').focus();
           return false; 

        // }else if( mobileNumberVal.length != MOBILE_LENGTH ){
        }else if( !checkValidMobile( "mobileNumber" ) ){
            toastr.warning('Please Enter Valid Mobile Number');
            $('#mobileNumber').focus();
            return false;
    
        }else if( email != "" && !validateEmail(email) ){
            toastr.warning('Please Enter valid Email');
            $('#email').focus();
            return false;
        
        }else if( localStorage.POTGinqTypeForQuick == "" || localStorage.POTGinqTypeForQuick == undefined || localStorage.POTGinqTypeForQuick == null || localStorage.POTGinqSourceForQuick == "" || localStorage.POTGinqSourceForQuick == undefined || localStorage.POTGinqSourceForQuick == null ){

            $('#inquirytype').val( localStorage.POTGinqTypeForQuick );
            $('#inquirysource').val( localStorage.POTGinqSourceForQuick );

            $('#inquirytype').selectpicker('refresh');
            $('#inquirysource').selectpicker('refresh');

            $('#inqTypeSource').modal('show');
        }else{

            $(".no-display").toggle();
            $(".custdetail").hide();

            addFocusId( 'groupListing' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        }
    });

    $("#nextpanel-1").click(function() {

        if( temp_inq_Trans.length == 0 ){
            toastr.warning('Please Enter Items');
            $('#itemName').focus();
            return false;
        
        }else{

            $(".no-display-1").toggle();
            $(".itemselection").hide();
            addFocusId( 'followupDate' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        }
    });    
    
    $("#previous-2").click(function() {
        $(".itemselection").toggle();
        $(".followup").hide();
    });
    
    $("#previous-1").click(function() {
        $(".custdetail").toggle();
        $(".itemselection").hide();
    });    

    $('#custSearch').keyup(function (e) {
        if (e.which == 8) {
            if( $('#custSearch').val() == "" ){
                $('#custSearch').val('');
                $('#mobileNumber').val('');
                $('#ContmobileNumber').val('');
                $('#contPerson').val('');
                $('#email').val('');
                custId = "-2";
                phoneId = "";
                custEmail = "";
                custCountryID = "";
                custCityID = "";
                custStateId = "";
                $("#contPerson").data('typeahead').source = [];
            }
        }
    });  

    initTelInput( 'mobileNumber' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput( 'ContmobileNumber' );   // INITIALIZE COUNTRY WISE MOBILE NUM
}