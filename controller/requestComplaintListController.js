    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 03-08-2018.
 * File : requestComplaintListController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_REQ_COMP_DATA = [];
var GBL_REQ_COMP_ID = '';
var GBL_REQ_COMP_UPL_TYPE = '';
var GBL_USERS = [];
var GBL_TYPE = "";

// PAGE LOAD EVENTS
function pageInitialEvents() {

    // $('#newReqBtn').click();
    if( localStorage.indeCampusMyNotif != undefined && localStorage.indeCampusMyNotif != "" && localStorage.indeCampusMyNotif != false ){
        $('#newReqBtn').removeClass('disabled');
        $('#myReqBtn').addClass('disabled');
        GBL_TYPE = "2";
    }else{
        GBL_TYPE = "1";
    }
    getUsersList();
    assignAttachmentEvent(); // ATTACHMENT UPLOAD EVENT
    $('#uploadType').selectpicker('refresh');
    localStorage.removeItem('indeCampusUpdateReqId');
    localStorage.removeItem('indeCampusViewReqId');
    
    // getRequestComplaint();
}

// GET REQUEST / COMPLAINT LISTING DATA
function getRequestComplaint(type){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "getComplaintsData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22,90),
        type: type,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestComplaintCallback,"Please wait...Adding rooms to your item.");

    function getRequestComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_REQ_COMP_DATA = data.data;
            renderRequestComplaintList();
            localStorage.removeItem('indeCampusMyNotif');

        }else{

            GBL_REQ_COMP_DATA = [];
            renderRequestComplaintList();

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_REQ_COM );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER REQUEST COMPLAINT LIST
function renderRequestComplaintList(){

    var tableHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="requestComplain">'+
                       '<thead>'+
                          '<tr>'+
                            '<th>#</th>'+
                            '<th>Ticket ID</th>'+
                            '<th>Type</th>'+
                            '<th>Student</th>'+
                            '<th>Department</th>'+                                          
                            '<th>Category</th>' +                                            
                            '<th>Sub Category</th>'+
                            '<th>Description</th>'+
                            '<th>Raised On Date</th>'+
                            '<th>Assigned On Date</th>'+
                            '<th>Closed On Date</th>'+
                            '<th>Assigned To</th>'+
                            '<th>Status</th>'+
                            '<th>Action</th>'+
                          '</tr>'+
                       '</thead>'+
                       '<tbody>';
    var tableFoot = '</tbody></table>';
    var tableBody = '';

    if( GBL_REQ_COMP_DATA != "" && GBL_REQ_COMP_DATA != "No record found" ){

        GBL_REQ_COMP_DATA.forEach(function( record , index ){
            var raisedDate = (record.complaintStartTime != "" ? mysqltoDesiredFormat(record.complaintStartTime,'dd-MM-yyyy | h:mm:s') : "" );
            var assignedDate = (record.complaintAssignStartTime != "" ? mysqltoDesiredFormat(record.complaintAssignStartTime,'dd-MM-yyyy | h:mm:s') : "" );
            var closedDate = (record.complaintEndTime != "" ? mysqltoDesiredFormat(record.complaintEndTime,'dd-MM-yyyy | h:mm:s') : "" );
            var reqCompDesc = ( record.transData != "" && record.transData != "No record found" ? record.transData[0].description : "" );
            var type = "Request";
            var uploadType = "REQUEST";
            if( record.type == "2" ){
                type = "Complaint";
                uploadType = "COMPLAIN";
            }
            var reqCompStatus = ""; // "0"=Pending,"1"=Resolved,"2"=Close,"3"=Reopen,"4"=Canceled"
            if( record.status == "0" ){
                reqCompStatus = "Pending";
            }else if( record.status == "1" ){
                reqCompStatus = "Resolved";
            }else if( record.status == "2" ){
                reqCompStatus = "Closed";
            }else if( record.status == "3" ){
                reqCompStatus = "Reopen";
            }else if( record.status == "4" ){
                reqCompStatus = "Canceled";
            }
            var reOpenClass = 'hide';
            var cancelClass = '';
            if( record.status == "2" ){ // CLOSED
                reOpenClass = '';// SHOW REOPEN OPTION
            }
            if( record.status == "2" || record.status == "4" ){ // WHEN CLOSED / CANCELED
                cancelClass = 'hide'; // CANCEL OPTION HIDE
            }
            var disableEdit = '';
            if( record.status == "2" || record.status == "4" ){ // WHEN CLOSED / CANCELED
                disableEdit = 'disabled';
            }
            var dataAttr = 'data-value="' + record.assignUserId + '"';
            var assignBox = "";
            var assignClass = "";
            var imgPath = "";
            // SET USER ASSIGNBOX
            if( record.assignUserId != "0" && record.assignUserId != "" ){
                if( record.status == "2" || record.status == "4" ){
                    assignClass = '';
                    assignBox = record.assignUserName;
                }else{
                    assignClass = "AssignPopover";
                    assignBox =  '<a href="javascript:void(0)" ' + dataAttr + ' class="assignmentTd" complaintId="'+record.complaintId+'" studentId="'+record.studId+'" onclick="getSelectedDeptUser(this);" assignedId="'+record.assignUserId+'">' + record.assignUserName + '</a>';
                }
                
            }else{
                if( record.status == "2" || record.status == "4" ){
                    assignClass = '';
                    assignBox = ( record.assignUserName != null ? record.assignUserName : "" );
                }else{
                    assignClass = "AssignPopover";
                    assignBox = '<div class="btns">' +
                            '<button ' + dataAttr + ' class="btn btn-primary pulsate-regular btn-sm assignmentTd" complaintId="'+record.complaintId+'" studentId="'+record.studId+'" onclick="getSelectedDeptUser(this);" assignedId="'+record.assignUserId+'"> Assign</button>' +
                            '</div>';
                }
            }
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            var disableActions = '';
            if( (tmp_json.data[0].PK_USER_ID != record.assignUserId) && (record.assignUserId != "0")  ){
                disableActions = 'disabled';
            }

            // DISABLE UPDATE ACTIONS IF NOT OF THAT REQ/COMP CATEGORY DEPT - ADDED ON 28-08-2018
            var disableUpd = 'disabled';
            var allotedDept = ( tmp_json.data[0].DEPARTMENTID != "" && tmp_json.data[0].DEPARTMENTID  != "0" ? tmp_json.data[0].DEPARTMENTID : ""  );
            allotedDept = allotedDept.split(',');
            allotedDept.forEach(function( deptRcd , deptIdx ){
                if( record.departmentId == deptRcd ){
                    disableUpd = "";
                }
            });

            tableBody += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.ticketId +'</td>'+
                            '<td>'+ type +'</td>'+
                            '<td>'+ record.studName +'</td>'+
                            '<td>'+ record.departmentName +'</td>'+
                            '<td>'+ (record.parentCategoryName != null ? record.parentCategoryName : "") +'</td>'+
                            '<td>'+ (record.categoryName != null ? record.categoryName : "") +'</td>'+
                            '<td>'+ reqCompDesc +'</td>'+
                            '<td>'+ raisedDate +'</td>'+
                            '<td>'+ assignedDate +'</td>'+
                            '<td>'+ closedDate +'</td>'+
                            '<td class="'+assignClass+'">'+ assignBox +'</td>'+
                            '<td>'+ reqCompStatus +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-amber btn-ripple"  data-toggle="tooltip" data-original-title="View" complaintId="'+record.complaintId+'" studentId="'+ record.studId +'" onclick="viewRequestComplaint(this)"><i class="fa fa-eye"></i></a>'+
                                '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-primary btn-ripple '+disableEdit+' '+disableActions+' '+disableUpd+'"  data-toggle="tooltip" data-original-title="Update" complaintId="'+record.complaintId+'" studentId="'+ record.studId +'" onclick="updateRequestComplaint(this);"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-ripple btn-danger '+cancelClass+' '+disableActions+' '+disableUpd+'" data-toggle="tooltip" data-original-title="Cancel" complaintId="'+record.complaintId+'" studentId="'+ record.studId +'" type="'+type+'" onclick="cancelComplaintRequest(this);" ><i class="fa fa-close"></i></a>'+
                                '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-teal btn-ripple '+reOpenClass+' '+disableActions+' '+disableUpd+'" data-toggle="tooltip" data-original-title="Re-Open" complaintId="'+record.complaintId+'" studentId="'+ record.studId +'"  onclick="reopenComplaint(this)"><i class="fa fa-undo"></i></a>'+
                                // '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-brown btn-ripple '+disableEdit+'" data-toggle="tooltip" data-original-title="Assign User" complaintId="'+record.complaintId+'" studentId="'+ record.studId +'" type="'+record.type+'" onclick="openAssignModal(this);"><i class="fa fa-user"></i></a>'+
                                '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-gray btn-ripple '+disableEdit+'" data-toggle="tooltip" data-original-title="Attachment" complaintId="'+record.complaintId+'" type="'+uploadType+'" onClick="viewAttachment(this)"><i class="fa fa-paperclip"></i></a>'+
                            '</td>'+
                        '</tr>';

        });

    }

    var usersList = [];
    if( GBL_USERS != "" && GBL_USERS != "No record found" ){
        GBL_USERS.forEach(function( record , index ){
            var temp = {
                label : record.fullName,
                text : record.fullName,
                value : record.userId,
                contactNo : record.contactNo,
                designationId : record.designationId,
                fullName : record.fullName,
                userId : record.userId,
            }
            usersList.push(temp);
        });

    }

    $('#tableDiv').html( tableHead + tableBody + tableFoot );
    $('.assignmentTd').editable({
        type: 'select',
        pk: 1,
        value: '1',
        placement: 'left',
        source: usersList,
        title: 'Assign To',
        validate: function (value) {
            
            if (!value || value == "-1" ) {
                return 'This field is required';
            }  
            var indx = findIndexByKeyValue( usersList , 'value' ,value );
            if( indx != -1 ){
                var complaintId = $(this).attr('complaintId');
                if( complaintId != "" && complaintId != undefined ){
                    var assignUserName = usersList[indx].fullName;
                    assignUser(complaintId,assignUserName,value);
                }
                
            }                    
        }
    });
    $('#requestComplain').dataTable({
        // "fnInitComplete" : function(){

            
        // }

    });

}

// VIEW SELECTED STUDENT'S REQUEST/COMPLAINT ATTACHMENT DATA MODAL
function viewAttachment(context){

    var complaintId = $(context).attr('complaintId');
    var type = $(context).attr('type');
    GBL_REQ_COMP_ID = complaintId;
    GBL_REQ_COMP_UPL_TYPE = type;
    var complaintIndex = findIndexByKeyValue( GBL_REQ_COMP_DATA , 'complaintId' ,GBL_REQ_COMP_ID );
    if( complaintIndex != -1 ){

        renderAttachment( GBL_REQ_COMP_DATA[complaintIndex].attachmentData );
        $('#uploadType').val( type ).selectpicker('refresh');

        $('#attachmentAdd').modal('show');
    }
    
}

// UPLOAD ATTACHMENT SUBMIT CALL
function uploadAttachment(){

    var attachmentName = $('#attachmentName').val();
    var uploadTitle = $('#uploadTitle').val();
    
    if( uploadTitle == "" ){
        toastr.warning('Please enter attachment title');
        addFocusId('uploadTitle');
        return false;

    }else if( attachmentName == "" ){
        toastr.warning('Please Attach File To Submit');
        return false;

    }else{

        $("#fileupload").submit();

    }

}

//  ATTACHMENT UPLOAD EVENT
function assignAttachmentEvent(){
    
    $("#fileupload").off().on('submit', (function (e) {
            
        var complaintIndex = findIndexByKeyValue( GBL_REQ_COMP_DATA , 'complaintId' ,GBL_REQ_COMP_ID );
        if( complaintIndex != -1 ){

            var uploadType = $('#uploadType').val();
            var uploadTitle = $('#uploadTitle').val();

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            var temp_json = {
                requestCase : "attachmentStudentComplainUpload",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(11, 46),
                studentId : GBL_REQ_COMP_DATA[complaintIndex].studId,
                complainId : GBL_REQ_COMP_ID,
                type : "COMPLAIN",
                title : uploadTitle,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                
            };

            $("#postDataAttach").val(JSON.stringify(temp_json));
            // console.log(temp_json);return false;

            var bfsendMsg = "Sending data to server..";
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                beforeSend: function () {
                    //alert(bfsendMsg);
                    $("#msgDiv").html(bfsendMsg);
                    // $("#ajaxloader").removeClass("hideajaxLoader");
                    addRemoveLoader(1);
                },
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    console.log(data);
                    
                    if(data.status == "Success"){
                        
                        console.log(data.data);
                        var attachFile = {
                            attachmentId : data.data.attachmentId,
                            attachmentLink : data.data.fileLink,
                            attachmentName : data.data.fileName,
                            studId : GBL_REQ_COMP_DATA[complaintIndex].studId,
                            attachType : "COMPLAIN",
                            title : uploadTitle,
                            complainId : GBL_REQ_COMP_ID
                        }
                        if( GBL_REQ_COMP_DATA[complaintIndex].attachmentData == undefined || GBL_REQ_COMP_DATA[complaintIndex].attachmentData == "No record found"  ){
                            GBL_REQ_COMP_DATA[complaintIndex].attachmentData = [];
                        }
                        GBL_REQ_COMP_DATA[complaintIndex].attachmentData.push(attachFile);
                        renderAttachment( GBL_REQ_COMP_DATA[complaintIndex].attachmentData ); // RENDER ATTACHMENT DATA
                        
                        refreshFile(); // REFRESH ATTACHMENT FILE
                        toastr.success( "Your attachment submitted successfully" );
                    }else{
                        toastr.warning(data.status);
                        refreshFile(); // REFRESH ATTACHMENT FILE
                        
                    }
                    addRemoveLoader(0); // REMOVE LOADER
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                    if( jqXHR.responseText != "" && jqXHR.responseText != undefined ){
                        if( JSON.parse(jqXHR.responseText).status != "" ){
                            toastr.error(JSON.parse(jqXHR.responseText).status);
                        }else{
                            toastr.error(errdata);
                        }
                        
                    }else{
                        toastr.error(errdata);
                    }
                    
                    refreshFile(); // REFRESH ATTACHMENT FILE
                }
            });
        }
    }));

}

// REFRESH ATTACHMENT FILE
function refreshFile(){

    $('#attachmentName').val('');
    $('#uploadTitle').val('');
    $('#uploadType').val('').selectpicker('refresh');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

// RENDER ATTACHMENT DATA
function renderAttachment(attachmentData){ 
    var photoAttachLi = '';

    // STUDENT'S PHOTO ATTACHMENT DISPLAY
    if( attachmentData != "No record found" && attachmentData != "" ){
        attachmentData.forEach(function( record , index ){

           
            var clsHide = "";
            photoAttachLi += "<div class='card card-image card-light-blue bg-image bg-opaque8 col-md-4' style='width: 30%;height: 156px; margin-right: 10px;'>"+
                                "<img src='"+record.attachmentLink+"' alt='' class='gallery-image'>"+
                                "<div class='context has-action-left has-action-right'>"+
                                    "<div class='tile-content'>"+
                                        "<span class='text-title'>"+record.title+"</span>"+
                                        "<span class='text-subtitle'>png</span>"+
                                    "</div>"+
                                    "<div class='tile-action right'>"+
                                        "<a data-div='imagepopup' data-link='"+record.attachmentLink+"' onClick='viewImageLinkData(this);' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>"+
                                        "<a onClick='deleteAttachment(this)' attachPath='"+record.attachmentLink+"' attachmentId='"+record.attachmentId+"' attachType='"+record.attachType+"' class='btn btn-sm btn-warning'><i class='fa fa-trash'></i></a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
        });
    }else{
        displayAPIErrorMsg( '' , GBL_ERR_NO_ATTACH );
    }
    $('#photoAttachmentListing').html( photoAttachLi );
}

// VIEW REQUEST/COMPLAINT
function viewRequestComplaint(context){

    var studentId = $(context).attr('studentId');
    var complaintId = $(context).attr('complaintId');
    localStorage.indeCampusViewReqId = complaintId;
    window.location.href = "RequestComplainView.html";

}

// VIEW IMAGE LINK IN MODAL
function viewImageLinkData(docData){
    var link = $(docData).attr("data-link");
    $("#imageLink").attr("src",link);
    openModal(docData);   
}

// DELETE SELECTED REQUEST/COMPLAINT'S ATTACHMENT
function deleteAttachment(context){

    var attachmentId = $(context).attr('attachmentId');
    var attachType = $(context).attr('attachType');
    var confmDelete = confirm("Are you sure you want to delete this attachment ?");
    if( !confmDelete ){
        return false;
    }

    var complaintIndex = findIndexByKeyValue( GBL_REQ_COMP_DATA , 'complaintId' ,GBL_REQ_COMP_ID );
    if( complaintIndex != -1 ){

        var attachmentIndex = findIndexByKeyValue( GBL_REQ_COMP_DATA[complaintIndex].attachmentData , 'attachmentId' ,attachmentId );
        if( attachmentIndex != -1 ){
            var attachmentLink = GBL_REQ_COMP_DATA[complaintIndex].attachmentData[attachmentIndex].attachmentLink;
            var attachmentName = ( attachmentLink != "" ? basenameWithExt(attachmentLink) : "" );

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

            var postData = {
                requestCase : "deleteComplaintAttachment",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(11 , 47),
                attachmentId : attachmentId,
                type : attachType,
                attachName : attachmentName,
                complaintId : GBL_REQ_COMP_ID,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            };
            commonAjax(FOLLOWUPURL, postData, deleteAttachmentCallback,"Please wait...Adding rooms to your item.");

            function deleteAttachmentCallback( flag , data ){
                if (data.status == "Success" && flag) {

                    console.log(data.data);
                    var attachmentData = data.data;
                    GBL_REQ_COMP_DATA[complaintIndex].attachmentData = attachmentData;
                    renderAttachment( GBL_REQ_COMP_DATA[complaintIndex].attachmentData );

                }else{


                    if (flag)
                      displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
                    else
                        toastr.error(SERVERERROR);
                }
            }

        }

    }
    
}

// GETS ALL USERS LIST
function getUsersList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "userSearch",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8, 30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getUserCallback,"Please wait...Adding rooms to your item.");

    function getUserCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_USERS = data.data;
            getRequestComplaint(GBL_TYPE);
        }else{

            GBL_USERS = [];
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
            getRequestComplaint(GBL_TYPE);
        }
    }

}

// GET SELECTED DEPT USERS LIST
var GBL_ASS_USER_ID = "";
function openAssignModal(context){

    var studentId = $(context).attr('studentId');
    var complaintId = $(context).attr('complaintId');
    var type = $(context).attr('type');
    GBL_REQ_COMP_ID = complaintId;
    GBL_REQ_COMP_UPL_TYPE = type;
    var index = findIndexByKeyValue( GBL_REQ_COMP_DATA , 'complaintId' ,GBL_REQ_COMP_ID );
    if( index != -1 ){

        var departmentId = GBL_REQ_COMP_DATA[index].departmentId;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : "userSearch",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(8, 30),
            departmentId: departmentId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(FOLLOWUPURL, postData, getUserCallback,"Please wait...Adding rooms to your item.");

        function getUserCallback( flag , data ){
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_USERS = data.data;
                var assignUserId = ( GBL_REQ_COMP_DATA[index].assignUserId != undefined && GBL_REQ_COMP_DATA[index].assignUserId != "" ? GBL_REQ_COMP_DATA[index].assignUserId : "" );
                GBL_ASS_USER_ID = assignUserId;
                renderUserList( assignUserId );

            }else{

                GBL_USERS = [];
                renderUserList( "" );

                if (flag)
                  displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

}

// RENDER USERS LIST IN DROPDOWN OF SELECTED DEPT AND OPEN ASSIGN MODAL
function renderUserList( assignUserId ){

    var options = '<option value="-1">Select User</option>';
    if( GBL_USERS != "" && GBL_USERS != "No record found" ){

        GBL_USERS.forEach(function( record , index ){
            
            options += '<option value="'+ record.userId +'">'+ record.fullName +'</option>';

        });
    }
    $('#assignUser').html( options );
    $('#assignUser').selectpicker('refresh');
    if( assignUserId != "" ){
        $('#assignUser').val( assignUserId ).selectpicker('refresh');
    }
    $('#assignUserModal').modal('show');

}

// ASSIGN USER TO SELECTED REQUEST/COMPLAINT
function assignUser(complaintId,assignUserName,assignUserId){

    var assignUser = assignUserId;
    var index = findIndexByKeyValue( GBL_REQ_COMP_DATA , 'complaintId' ,complaintId );
    var reqCompType = "";
    if( index != -1 ){
        reqCompType = GBL_REQ_COMP_DATA[index].type;
    }
    // if( assignUser == "" || assignUser == "-1" || assignUser == null ){
    //     toastr.warning('Please select user to assign');
    //     addFocusId('assignUser');
    //     return false;

    // }
    // else if( GBL_ASS_USER_ID == assignUser ){
    //     toastr.warning('Please select other user to assign request / complaint');
    //     addFocusId('assignUser');
    //     return false;

    // }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "assignComplain",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22 , 106),
        complainId: complaintId,
        assignUserId: assignUser,
        assignUserName : assignUserName,
        type : reqCompType,
        loginUsername : tmp_json.data[0].FULLNAME,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, assignUserCallback,"Please wait...Adding rooms to your item.");

    function assignUserCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ASS_USER_ID = "";
            GBL_REQ_COMP_ID = "";
            GBL_REQ_COMP_UPL_TYPE = "";
            // $('#assignUser').val('-1').selectpicker('refresh');
            // $('#assignUserModal').modal('hide');
            getRequestComplaint(GBL_TYPE);

        }else{

            getRequestComplaint(GBL_TYPE);

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_ASS_COMP );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// UPDATE SELECTED REQUEST/COMPLAINT
function updateRequestComplaint(context){

    var studentId = $(context).attr('studentId');
    var complaintId = $(context).attr('complaintId');
    localStorage.indeCampusUpdateReqId = complaintId;
    navigateToCreateReqComplaint();

}

// REOPENS SELECTED REQUEST/COMPLAINT
function reopenComplaint(context){
    var complaintId = $(context).attr('complaintId');
    var studentId = $(context).attr('studentId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "reopenComplaint",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22, 89),
        complaintId: complaintId,
        studentId : studentId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, reopenComplaintCallback,"Please wait...Adding rooms to your item.");

    function reopenComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            getRequestComplaint(GBL_TYPE);

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_REOPEN_COMP );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// CANCEL SELECTED REQUEST / COMPLAINT
function cancelComplaintRequest(context){

    var complaintId = $(context).attr('complaintId');
    var studentId = $(context).attr('studentId');
    var type = $(context).attr('type');

    var cnfrmCancel = confirm("Are you sure you want to cancel your "+ type +" ?");
    if( !cnfrmCancel ){
        return false;
    }


    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "cancelComplaint",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22, 92),
        complaintId: complaintId,
        studentId : studentId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, cancelComplaintRequestCallback,"Please wait...Adding rooms to your item.");

    function cancelComplaintRequestCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            getRequestComplaint(GBL_TYPE);

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CAN_COMP );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// GET SELECTED TYPE REQUEST / COMPLAINT DATA 
function getTypeWiseData(type,context) {
    
    // TYPE WHERE 1 = NEW, 2 =MY Complaint And 3 =All
    var typeListing = $('.typeListing a').removeClass('disabled');
    GBL_TYPE = type;
    getRequestComplaint(type);
    $(context).addClass('disabled');


}

// IT WILL GET SELECTED REQUEST/COMPLAINT'S DEPT USERS AND SET INTO ITS EDITABLE SELECTBOX - ADDED ON 23-08-2018 BY VISHAKHA TAK
var GBL_ASS_USER_ID = "";
function getSelectedDeptUser(context){

    var studentId = $(context).attr('studentId');
    var complaintId = $(context).attr('complaintId');
    var assignedId = $(context).attr('assignedId');
    GBL_REQ_COMP_ID = complaintId;
    var index = findIndexByKeyValue( GBL_REQ_COMP_DATA , 'complaintId' ,GBL_REQ_COMP_ID );
    if( index != -1 ){

        var departmentId = GBL_REQ_COMP_DATA[index].departmentId;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : "userSearch",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(8, 30),
            departmentId: departmentId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(FOLLOWUPURL, postData, getUserCallback,"Please wait...Adding rooms to your item.");

        function getUserCallback( flag , data ){
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_USERS = data.data;
                var usersList = [];
                if( data.data != "" && data.data != "No record found" ){
                    data.data.forEach(function( record , index ){
                        var temp = {
                            label : record.fullName,
                            text : record.fullName,
                            value : record.userId,
                            contactNo : record.contactNo,
                            designationId : record.designationId,
                            fullName : record.fullName,
                            userId : record.userId,
                        }
                        usersList.push(temp);
                    });

                }
                var options = '';
                if( data.data != "" && data.data != "No record found" ){
                    data.data.forEach(function( record , index ){
                        var selectClass = '';
                        if( assignedId == record.userId ){ selectClass = "selected";  }
                        options += '<option value="'+record.userId+'" '+selectClass+'>'+record.fullName+'</option>';
                        
                    });

                }else{
                    usersList = [];
                    options = '';
                }
                if( assignedId != "0" && assignedId != "" ){ // SET USER OPTIONS IN EDITABLE SELECT

                    var selectedPopover = $(context).parent('.AssignPopover').find('.editable-container .editable-input select')
                    selectedPopover.html( options );
                }else{
                    // SET USER OPTIONS IN ASSIGN BOX
                    var selectedPopover = $(context).parent('.btns').find('.editable-container').find('.popover-content').find('.form-control.input-sm');
                    selectedPopover.html( options );
                }

                // $(context).editable({
                //     type: 'select',
                //     pk: 1,
                //     value: '1',
                //     placement: 'left',
                //     source: usersList,
                //     title: 'Assign To',
                //     validate: function (value) {
                        
                //         if (!value || value == "-1" ) {
                //             return 'This field is required';
                //         }  
                //         var indx = findIndexByKeyValue( usersList , 'value' ,value );
                //         if( indx != -1 ){
                //             if( complaintId != "" && complaintId != undefined ){
                //                 var assignUserName = usersList[indx].fullName;
                //                 assignUser(complaintId,assignUserName,value);
                //             }
                            
                //         }                    
                //     }
                // });
                
            }else{

                // EMPTY ASSIGN BOX
                GBL_USERS = [];
                var options = '';
                if( assignedId != "0" && assignedId != "" ){ // SET USER OPTIONS IN EDITABLE SELECT
                    var selectedPopover = $(context).parent('.AssignPopover').find('.editable-container .editable-input select')
                    selectedPopover.html( options );
                }else{
                    // SET USER OPTIONS IN ASSIGN BOX
                    var selectedPopover = $(context).parent('.btns').find('.editable-container').find('.popover-content').find('.form-control.input-sm');
                    selectedPopover.html( options );
                }

                if (flag)
                  displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

}