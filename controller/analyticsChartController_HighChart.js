/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var funnelChart = "";
var empBookingChart = "";
var inqColumnChart = "";
var empPendingChart = "";
var dealLostChart = "";
var dealWonChart = "";
var empEffctChart = "";
var monthClosureChart = "";
var InquiryClosureChart = "";
var sourceWiseChart = "";
var typeWiseChart = "";
var predStackChart = "";
var userWiseOpenInquiryChart = "";
var closureSpreadChart = "";
var inquiryConversionChart = "";
var salesSpreadChart = "";

function getAnalyticData(){

    var inqNum = $('#inqNum').val();

    if( inqNum == "-1" ){
        toastr.warning('Please Search Inquiry');
        $('#inqNum').focus();
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getAnalyticChartReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(FOLLOWUPURL, postData, getAnalyticDataCallback,"Please Wait... Getting Dashboard Detail");

    function getAnalyticDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            funnelChart = data.data.funnelChart;          //Inquiry Funnel Chart
            empBookingChart = data.data.empwiseStatusChartBookLostNew;        //Employee wise Pipeline stage status
            // empBookingChart = data.data.empwiseStatusChartBookLost;        //Employee wise Pipeline stage status
            inqColumnChart = data.data.statusChangeAverageAging;       //Inquiry closure time – status change 
            empPendingChart = data.data.empwiseStatusChartPending;        //Employee wise Pipeline stage status
            dealLostChart = data.data.dealLostChart;        //Deal lost reason analysis – Bar chart 
            dealWonChart = data.data.dealWonChart;        //Deal lost reason analysis – Bar chart 
            empEffctChart = data.data.empwiseEffectiveChart;        //Employee Effectiveness – deal closure % - dial chart  
            monthClosureChart = data.data.targetAchieveCurrFiscal;    //Monthly closure – amount bar chart – target vs achievement
            InquiryClosureChart = data.data.dateWiseData;  //target date vs actual – column chart
            sourceWiseChart = data.data.inqSourceChart;      //source wise, type – pie chart     
            typeWiseChart = data.data.inqTypeChart;      //source wise, type – pie chart     
            predStackChart = data.data.dealProbabilityChanceChart;      //source wise, type – pie chart     
            userWiseOpenInquiryChart = data.data.empwiseStatusChartPendingNew;      //source wise, type – pie chart     
            // userWiseOpenInquiryChart = data.data.empwiseStatusChartPending;      //source wise, type – pie chart     
            inquiryConversionChart = data.data.inquiryConversion;      //source wise, type – pie chart     
            salesSpreadChart = data.data.salesSpread.salesSpread;      //source wise, type – pie chart     
            $('#1stLi a').click();
            
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CHART_DATA );
            else
                toastr.error(SERVERERROR);
        }
        
    }
}

//Inquiry Funnel Chart
function funnelChartFun(){
    
    if( funnelChart.length == 0 || funnelChart.length == "" || funnelChart == "No record found" ){
      return false;
    }

    var chartData = [];
    funnelChart.forEach( function( record , index ){
        chartData.push( [ record.statusName , parseFloat( record.potSize ) ] );
    });
    
    Highcharts.chart('inqFunnel', {
        chart: {
            type: 'funnel'
        },
        title: {
            text: ''
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                center: ['40%', '50%'],
                neckWidth: '30%',
                neckHeight: '25%',
                width: '80%'
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Inquiry (in rs.)',
            data: chartData
        }]
    });
}

function inqColumnChartFun(){
    
    if( inqColumnChart.length == 0 || inqColumnChart.length == "" || inqColumnChart == "No record found" ){
      return false;
    }

    var chartData = [];
    inqColumnChart.forEach( function( record , index ){
        var tempJson = [ record.statusName , parseFloat( record.averageDays )];
        chartData.push( tempJson );
    }); 

    Highcharts.chart('inqTime', {
      chart: {
          type: 'column'
      },
      // colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
      title: {
          text: ''
      },
      xAxis: {
          type: 'category',
          labels: {
              rotation: -45,
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'No of Days'
          }
      },
      legend: {
          enabled: false
      },
      tooltip: {
          pointFormat: 'Status Aging'
      },
      series: [{
          name: 'Population',
          data: chartData,
          dataLabels: {
              enabled: true,
              rotation: -90,
              color: '#FFFFFF',
              align: 'right',
              format: '{point.y:.1f}', // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      }]
  });
}

//Deal Lost Reason chart
function dealWonChartFun(){
    
    if( dealWonChart.length == 0 || dealWonChart.length == "" || dealWonChart == "No record found" ){
      return false;
    }

    var categories = [];
    var chartData = [];
    dealWonChart.forEach( function( record , index ){
      categories.push( record.statusName );
      chartData.push( parseFloat( record.inqCount ) );
    });

    Highcharts.chart('dealWon', {
        chart: {
            type: 'bar'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            title: {
                text: 'Won Reason'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Deal Won'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Won Remark',
            data: chartData
        }]
    });
}

//Deal Lost Reason chart
function dealLostChartFun(){

    if( dealLostChart.length == 0 || dealLostChart.length == "" || dealLostChart == "No record found" ){
      return false;
    }
 
    var categories = [];
    var chartData = [];
    dealLostChart.forEach( function( record , index ){
      categories.push( record.statusName );
      chartData.push( parseFloat( record.inqCount ) );
    });

    Highcharts.chart('dealLost', {
        chart: {
            type: 'bar'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,

            title: {
                text: 'Lost Reason'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Deal Lost'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Lost Remark',
            data: chartData
        }]
    });
}

//Inquiry Closure Chart
function empBookingChartFun(){
    
    if( empBookingChart == "No record found" || empBookingChart == "" || empBookingChart.length == 0 ){
      return false;
    }

    var categories = [];
    var chartData = [];
    var tempData = [];

    var categories = [];
    empBookingChart[0].month.forEach( function( record , index ){
        categories.push( record.concatKey );
    });
    
    empBookingChart.forEach( function( record , index ){
        var tempUserData = [];
        record.month.forEach( function( inrRcrd ,innrIndex ){
            tempUserData.push( parseFloat( inrRcrd.statusInqCount ) );
        });
        chartData.push({
            name : record.fullname,
            data : tempUserData
        });       
    });

    Highcharts.chart('bookChrt', {

        chart: {
            type: 'column'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },

        xAxis: {
            categories: categories
        },

        yAxis: {
            // allowDecimals: false,
            min: 0,
            title: {
                text: 'Counts (User Wise)'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'column'
            }
        },
        series: chartData,
    });
}

//Open Inquiry Chart
function empPendingChartFun(){

    if( inquiryConversionChart == "No record found" || inquiryConversionChart == "" || inquiryConversionChart.length == 0 ){
      return false;
    } 

    var chartData = [];
    inquiryConversionChart.forEach( function( record , index ){
        var tempData = {
            name : record.statusName,
            y : parseFloat( record.statusCount ),
        }
        chartData.push( tempData );
    }); 
    Highcharts.chart('empStack', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Count',
            colorByPoint: true,
            data: chartData
        }]
    });
}

//Open Inquiry Chart
function userWiseOpenInquiryFun(){

    if( userWiseOpenInquiryChart.length == 0 || userWiseOpenInquiryChart == "" || userWiseOpenInquiryChart == "No record found" ){
      return false;
    }

    var categories = [];
    userWiseOpenInquiryChart[0].userData.forEach( function( record , index ){
        categories.push( record.fullname );
    });

    var chartData = [];
    userWiseOpenInquiryChart.forEach( function( record , index ){
        var tempUserData = [];
        record.userData.forEach( function( inrRcrd ,innrIndex ){
            tempUserData.push( parseFloat( inrRcrd.statusInqCount ) );
        });
        chartData.push({
            name : record.statusName,
            data : tempUserData,
        });
    });

    Highcharts.chart('userWiseOpenInq', {
        chart: {
            type: 'column'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Amount'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: chartData
    });
}

//Employee Effectiveness chart
function empEffctChartFun(){
    
    if( empEffctChart.length == 0 || empEffctChart == "" || empEffctChart == "No record found" ){
      return false;
    }

    var chartData = [];
    empEffctChart.forEach( function( record , index ){

        var effPerc = parseFloat( (record.effPer).split('%')[0] );
        var tempData = {
            name: record.userName,
            data: [ effPerc ],
            tooltip: {
                valueSuffix: '%'
            },
            showInLegend: true
        }
        chartData.push( tempData );
    }); 

    Highcharts.chart('empEffct', {
    
        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: ''
        },
        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 100,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 1,
                rotation: 'auto',
                enabled : true
            },
            title: {
                text: 'Effectiveness'
            },
            plotBands: [{
                from: 0,
                to: 50,
                color: '#DF5353' // red
            }, {
                from: 50,
                to: 80,
                color: '#DDDF0D' // yellow
            }, {
                from: 80,
                to: 100,
                color: '#55BF3B' // green
            }]
        },
        legend: {
          labelFormatter: function() { 
            return '<span>' + this.name +' - '+ this.data[0].y + '%</span>';
          },
          symbolWidth: 0
        },
        series: chartData,
        color: '#fff'
    });
}

//Monthly Closure Chart
function monthClosureChartFun(){
    
    if( monthClosureChart.length == 0 || monthClosureChart == "" || monthClosureChart == "No record found"){
      return false;
    }

    var tbody = "";
    monthClosureChart.forEach( function( record , index ){
      tbody += '<tr>'+
                    '<th>|'+ record.month +'|</th>'+
                    '<td>'+ parseFloat( record.target ) +'</td>'+
                    '<td>'+ parseFloat( record.achievement ) +'</td>'+
                '</tr>'
    });
    $('#monthClosureTable').html( tbody );

    Highcharts.chart('monthChart', {
        data: {
            table: 'monthClosureDatatable'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Target Amount (in lakhs)'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' lakhs';
            }
        }
    });
}

//Inquiry Closure Date Chart
function InquiryClosureChartFun(){
       
    if( InquiryClosureChart.length == 0 || InquiryClosureChart == "" || InquiryClosureChart == "No record found"){
      return false;
    }
    
    var tbody = "";
    InquiryClosureChart.forEach( function( record , index ){
      tbody += '<tr>'+
                    '<th>'+ record.dayDiff +'</th>'+
                    '<td>'+ parseFloat( record.closureDays ) +'</td>'+
                    '<td>'+ parseFloat( record.actualDays ) +'</td>'+
                '</tr>'
    });
    $('#inqClosureTable').html( tbody );

    Highcharts.chart('inqClosure', {
        data: {
            table: 'inqClosureDatatable'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Target Days'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' days';
            }
        }
    });
}

//Inquiry List Source Wise chart
function sourceWiseChartFun(){
    
    if( sourceWiseChart == "No record found" || sourceWiseChart == "" || sourceWiseChart.length == 0 ){
      return false;
    }

    var chartData = [];
    sourceWiseChart.forEach( function( record , index ){
        var tempData = {
            name : record.statusName,
            y : parseFloat( record.inqCount ),
        }
        chartData.push( tempData );
    });

    Highcharts.chart('sourceWise', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Count',
            colorByPoint: true,
            data: chartData
        }]
    });
}

//Inquiry List Type Wise chart
function typeWiseChartFun(){
    
    if( typeWiseChart == "No record found" || typeWiseChart == "" || typeWiseChart.length == 0 ){
      return false;
    }

    var chartData = [];
    typeWiseChart.forEach( function( record , index ){
        var tempData = {
            name : record.statusName,
            y : parseFloat( record.inqCount ),
        }
        chartData.push( tempData );
    });

    Highcharts.chart('typeWise', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Count',
            colorByPoint: true,
            data: chartData
        }]
    });
}

//Predictive Sales Chart
function predStackChartFun(){

    if( predStackChart.length == 0 || predStackChart.length == "" || predStackChart == "No record found" ){
      return false;
    }

    var chartData = [];
    var categories = [];
    predStackChart.statusWiseData.forEach( function( record , index ){

        var tempData = [];
        categories = []; 
        record.monthWiseData.forEach( function( innrRecrd , innrIndex ){
            tempData.push( parseFloat( innrRecrd.potgSize ) );
            categories.push( innrRecrd.month );
        });

        chartData.push({
            name : record.statusName,
            data : tempData,
        })
    });

    Highcharts.chart('predSales', {
        chart: {
            type: 'column'
        },
        colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
        title: {
            text: ''
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Amount'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'column'
            }
        },
        series: chartData
    });
}

function closureSpreadChartFun(){
  
    var geoData = salesSpreadChart;
    // geoData = "No record found";

    if( geoData == undefined || geoData == "No record found" || geoData == "" ){
        return false;
    }

    var chartData = [];
    geoData.forEach( function( record , index ){
        var tempData = {
            "z": parseFloat( record.totalSales ),
            "lat": parseFloat( record.latitude ),
            "lon": parseFloat( record.longitude )
        }
        chartData.push( tempData );
    })

    var mapData = Highcharts.maps['custom/world'];

    // var data =  [{
        // "z": 10038,
        // "lat": 26.5727,
        // "lon": 73.8390
    // }];

    $('#closureSpread').highcharts('Map', {
        title: {
            text: 'Customer Sales'
        },
        chart : {
            borderWidth : 1
        },
        legend: {
            enabled: true
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        series : [{
            name: 'Countries',
            mapData: mapData,
            color: '#E0E0E0',
            enableMouseTracking: false
        }, {
            type: 'mapbubble',
            mapData: mapData,
            name: 'Total Sales',
            data: chartData,
             joinBy: ['postal-code', 'code'],
            minSize: 4,
            maxSize: '12%',
            tooltip: {
                pointFormat: '{point.code}: {point.z} rs.'
            }
        }]
    });
}
