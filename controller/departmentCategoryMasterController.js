    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 03-08-2018.
 * File : departmentCategoryMasterController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_DEPT_CAT_DATA = [];
var GBL_CATEGORY = [];
var GBL_DEPT_CAT_ID = "";
var GBL_CATEGORY_ID = "";

// PAGE LOAD EVENTS
function pageInitialEvents() {
    
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var departmentData = roleAuth.data.departmentList;
    setOption('0','department',departmentData,'Select Department');
    $('#department').selectpicker('refresh');
   
    $('#departmentTable').dataTable();
    getDepartmentCatList(); // GET DEPARTMENT CATEGORY LISTING DATA
    renderDeptCategoryDropdown();

}

// GET DEPARTMENT CATEGORY LISTING DATA
function getDepartmentCatList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "listDepartmentCategoryData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(15, 62),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getDepartmentCatListCallback,"Please wait...Adding rooms to your item.");

    function getDepartmentCatListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_DEPT_CAT_DATA = data.data;
            renderDeptCategoryList(); // RENDER DEPARTMENT CATEGORY LISTING DATA

        }else{

            GBL_DEPT_CAT_DATA = [];
            renderDeptCategoryList(); // RENDER DEPARTMENT CATEGORY LISTING DATA

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_DEPT_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER DEPARTMENT CATEGORY LISTING DATA
function renderDeptCategoryList(){

    var tableHead = '<table class="display datatables-alphabet-sorting" id="departmentTable">'+
                       '<thead>'+
                          '<tr>'+
                            '<th>#</th>'+
                            '<th>Department</th>'+                                          
                            '<th>Parent Category</th>'+
                            '<th>Category</th>' +                                            
                            '<th>Action</th>'+
                          '</tr>'+
                       '</thead>'+
                       '<tbody>';
    var tableFoot = '</tbody></table>';
    var tableBody = '';

    if( GBL_DEPT_CAT_DATA != "" && GBL_DEPT_CAT_DATA != "No record found" ){
        GBL_DEPT_CAT_DATA.forEach(function( record , index ){

            tableBody += '<tr>'+
                                '<td>'+ (index+1) +'</td>'+
                                '<td>'+ record.departmentName +'</td>'+
                                '<td>'+ record.parentCategoryName +'</td>'+
                                '<td>'+ record.categoryName +'</td>'+
                                '<td>'+
                                    '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple"  data-toggle="tooltip" data-original-title="Edit" departmentCategoryId="'+ record.departmentCategoryId +'" onclick="editDepartment(this);"><i class="fa fa-pencil"></i></a>'+
                                    '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-ripple btn-danger" data-toggle="tooltip" data-original-title="Delete" departmentCategoryId="'+ record.departmentCategoryId +'" onclick="deleteDepartment(this);"><i class="fa fa-trash"></i></a>'+
                                '</td>'+
                            '</tr>';

        });                
    }

    $('#tableDiv').html( tableHead + tableBody + tableFoot );
    $('#departmentTable').dataTable();


}

// ADD DEPARTMENT CATEGORY
function addDeptCategory(){

    var department = $('#department').val();
    var parentCategory = $('#parentCategory').val();
    var categoryName = $('#categoryName').val();

    if( department == "-1" || department == "" ){
        toastr.warning("Please select department");
        addFocusId('department');
        return false;

    }else if( categoryName == "" ){
        toastr.warning("Please enter category name");
        addFocusId('categoryName');
        return false;

    }
    if( parentCategory == "-1" ){
        parentCategory = "0";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "createDepartmentCategory",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11, 46),
        departmentId : department,
        parentCategoryId : parentCategory,
        categoryName : categoryName,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getDepartmentCatCallback,"Please wait...Adding rooms to your item.");

    function getDepartmentCatCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_DEPT_CAT_DATA = data.data;
            renderDeptCategoryList();
            resetFields();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_DEPT_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RESET FIELDS
function resetFields(){

    $('#department').val('-1').selectpicker('refresh');
    $('#parentCategory').val('-1').selectpicker('refresh');
    $('#categoryName').val('');
    $('#addCategoryBtn').html( '<i class="fa fa-floppy-o"></i> Save' );
    $('#addCategoryBtn').attr('onclick','addDeptCategory()');
    GBL_DEPT_CAT_ID = "";
    GBL_CATEGORY_ID = "";

}

// GET CATEGORY ON SELECTED DEPARTMENT
function getDepartmentCategory(context){

    var department = $(context).val();

    if( department != "" && department != "-1" ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : "departmentCategoryList",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(15, 62),
            departmentId : department,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(FOLLOWUPURL, postData, getDepartmentCatCallback,"Please wait...Adding rooms to your item.");

        function getDepartmentCatCallback( flag , data ){
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_CATEGORY = data.data;
                renderDeptCategoryDropdown(); // RENDER CATEGORY DROPDOWN

            }else{

                GBL_CATEGORY = [];
                renderDeptCategoryDropdown(); // RENDER CATEGORY DROPDOWN

                if (flag){
                  // displayAPIErrorMsg( data.status , 'GBL_ERR_NO_DEPT_CAT' );
                }
                else{
                    toastr.error(SERVERERROR);
                }
            }
        }

    }else{
        GBL_CATEGORY = [];
        renderDeptCategoryDropdown(); // RENDER CATEGORY DROPDOWN
    }

}

// RENDER CATEGORY DROPDOWN
function renderDeptCategoryDropdown(){

    var options = '<option value="-1">Select Category</option>';
    if( GBL_CATEGORY != "" && GBL_CATEGORY != "No record found" ){

        GBL_CATEGORY.forEach(function( record , index ){

            options += '<option value="'+ record.departmentCategotyId +'">'+ record.departmentCatName +'</option>';

        });

    }
    $('#parentCategory').html( options );
    $('#parentCategory').selectpicker('refresh');
    if( GBL_CATEGORY_ID != "" && GBL_CATEGORY_ID != "-1" ){
        $('#parentCategory').val( GBL_CATEGORY_ID ).selectpicker('refresh');
    }
}

// GET SELECTED DEPARTMENT CATEGORY DATA FOR UPDATE
function editDepartment(context){

    var departmentCategoryId = $(context).attr('departmentCategoryId');

    if( departmentCategoryId != "" && departmentCategoryId != undefined ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase : "listDepartmentCategoryData",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            departmentCategoryId : departmentCategoryId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(FOLLOWUPURL, postData, getDepartmentCallback,"Please wait...Adding rooms to your item.");

        function getDepartmentCallback( flag , data ){
            if (data.status == "Success" && flag) {

                console.log(data.data);
                var categoryData = data.data;
                renderEditData(categoryData,departmentCategoryId); // RENDER EDIT DATA

            }else{

                if (flag)
                  displayAPIErrorMsg( data.status , GBL_ERR_NO_DEPT_CAT );
                else
                    toastr.error(SERVERERROR);
            }
        }

    }

}

// RENDER EDIT DATA
function renderEditData(categoryData,departmentCategoryId){

    if( categoryData != "No record found" ){

        $('#department').val( categoryData[0].departmentId ).selectpicker('refresh');
        $('#department').trigger('change');
        GBL_CATEGORY_ID = categoryData[0].categoryId;
        $('#parentCategory').val( categoryData[0].categoryId ).selectpicker('refresh');

        $('#categoryName').val( categoryData[0].categoryName );
        $('#addCategoryBtn').html( '<i class="fa fa-floppy-o"></i> Update' );
        $('#addCategoryBtn').attr('onclick','updateDeptCategory()');
        GBL_DEPT_CAT_ID = departmentCategoryId;
    }

}

// UPDATE DEPARTMENT CATEGORY
function updateDeptCategory(){

    var department = $('#department').val();
    var parentCategory = $('#parentCategory').val();
    var categoryName = $('#categoryName').val();

    if( department == "-1" || department == "" ){
        toastr.warning("Please select department");
        addFocusId('department');
        return false;

    }else if( categoryName == "" ){
        toastr.warning("Please enter category name");
        addFocusId('categoryName');
        return false;

    }
    if( parentCategory == "-1" ){
        parentCategory = "0";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "createDepartmentCategory",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(15, 61),
        departmentId : department,
        parentCategoryId : parentCategory,
        categoryName : categoryName,
        departmentCategoryId : GBL_DEPT_CAT_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, updateDeptCategoryCallback,"Please wait...Adding rooms to your item.");

    function updateDeptCategoryCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_DEPT_CAT_DATA = data.data;
            renderDeptCategoryList();
            resetFields();

        }else{


            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_DEPT_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// DELETE SELECTED DEPARTMENT CATEGORY
function deleteDepartment(context){

    var departmentCategoryId = $(context).attr('departmentCategoryId');
    var cnfrmDelete = confirm(CNFDELETE);
    if( !cnfrmDelete ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "deleteDepartmentCategory",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(15,64),
        departmentCategoryId : departmentCategoryId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, deleteDepartmentCallback,"Please wait...Adding rooms to your item.");

    function deleteDepartmentCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_DEPT_CAT_DATA = data.data;
            renderDeptCategoryList(); // RENDER DEPARTMENT CATEGORY LISTING DATA

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_DEL_DEPT_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }

}