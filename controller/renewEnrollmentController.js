    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 01-08-2018.
 * File : renewEnrollmentController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_STUD_ID = ""; // STUDENT ID
var GBL_ROOM_ITEMS = [];
var allotData = [];

function pageInitialEvents(){

	$('#semester').selectpicker('refresh');
    $('#tenancyEndDate').datepicker('remove');
	getRoomType();

    if( localStorage.indeCampusRenewEnroll != undefined ){
        var renewEnrollData = JSON.parse(localStorage.indeCampusRenewEnroll);
        GBL_STUD_ID = renewEnrollData.studId;
        $('#membershipId').val( renewEnrollData.memberShipNumber );
        $('#studentName').val( (renewEnrollData.firstName).trim() + ' ' + (renewEnrollData.lastName).trim());
        $('#membershipId').attr('disabled' , true);
        $('#studentName').attr('disabled' , true);
        $('#semester').val( (renewEnrollData.semester != "" && renewEnrollData.semester != "0" ? renewEnrollData.semester : "" ) ).selectpicker('refresh');
        
        $(".datepicker").datepicker( {
            format: "mm-yyyy",
            startView: "months", 
            minViewMode: "months",
            startDate: new Date(),
            autoclose:true
        });
        $(".datepicker").attr('readonly',true);
        $("#tenancyStartDate").attr('readonly',true);
        $("#tenancyEndDate").attr('readonly',true);
    	$('#academicYearFrom').val( ( renewEnrollData.acdemicStartYear != "" ? (renewEnrollData.acdemicStartYear) : "" ) );
        $('#academicYearTo').val( ( renewEnrollData.acdemicEndYear != "" ? (renewEnrollData.acdemicEndYear) : "" ) );
        getAllotmentDetail(); 
        // localStorage.removeItem('indeCampusRenewEnroll');
    }else{
    	toastr.warning("No data found for renew enrollment");
    	setTimeout(function() {
           navigateToEnrollmentMaster();
        },1000);
    	
    }

}

// GET TYPE OF ROOM DATA
function getRoomType(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "RENTAL",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_ITEMS = data.data;
                
            var innerHtml = '<option value="-1">Select Room</option>';
            if( GBL_ROOM_ITEMS != "No record found" ){
                GBL_ROOM_ITEMS.forEach(function( record , index ){

                    innerHtml += '<option value="'+ record.itemId +'">'+ record.itemName +'</option>';

                });
            }
            $('#roomType').html( innerHtml ).selectpicker('refresh'); // SET ROOM TYPE DATA IN DROPDOWN
            
        }else{

            GBL_ROOM_ITEMS = [];
            $('#roomType').html( '' ).selectpicker('refresh');
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOM_TYPE );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// GET TYPE OF ROOM DATA
function getRoomNumber(context){

    var itemId = $(context).val();
    if( itemId == "-1" || itemId == "" || itemId == undefined ){
        toastr.warning('Please Select Proper Room Type');
        addFocusId('roomType');
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'roomList', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(13 ,54),
        itemId : itemId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRoomDataCallback,"Please wait...Adding your item.");

    function getRoomDataCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_ITEMS = data.data;
            setRoomItems(); // SET ROOM NUMBER DATA
            
        }else{

            GBL_ROOM_ITEMS = [];
            setRoomItems(); // SET ROOM NUMBER DATA
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOMS );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// SET ROOM NUMBER DATA
function setRoomItems() {
   
    var innerHtml = '<option value="-1">Select Room Number</option>';
    if( GBL_ROOM_ITEMS != "No record found" && GBL_ROOM_ITEMS.length != 0 ){
        GBL_ROOM_ITEMS.forEach(function( record , index ){

            innerHtml += '<option value="'+ record.roomId +'">'+ record.roomName +'</option>';

        });
    }
    $('#roomNumber').html( innerHtml ).selectpicker('refresh'); // SET ROOM NUMBER DATA IN DROPDOWN

    if( allotData[0].roomId != "" && allotData[0].roomId != undefined ){
    	$('#roomNumber').val( allotData[0].roomId ).selectpicker('refresh');
    }
    
}

// GET ALLOTMENT DETAIL OF SELECTED STUDENT
function getAllotmentDetail(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getStudentAllotment', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        studentId : GBL_STUD_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getAllotmentDetailCallback,"Please wait...Adding your item.");

    function getAllotmentDetailCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            allotData = data.data;
            renderAllotData();
           
        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ALLOT_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function renderAllotData(){

	if( allotData != "" && allotData != "No record found" ){

		$('#roomType').val( allotData[0].itemId ).selectpicker('refresh');
		$('#roomType').trigger('change');
	    $('#roomNumber').val( allotData[0].roomId ).selectpicker('refresh');
	    $('#bedNumber').val( allotData[0].bedNo ); 
	    $('#tenancyStartDate').val( '' );
        $('#tenancyEndDate').val( '' );
	}

}

function generateInvoice(){

    var studMemId = $('#membershipId').val();
    var studentName = $('#studentName').val();
    var roomType = $('#roomType').val();
    var roomNumber = $('#roomNumber').val();
    var roomName = $('#roomNumber option:selected').text();
    var bedNumber = $('#bedNumber').val();
    var semester = $('#semester').val();
    var academicYearFrom = $('#academicYearFrom').val();
    var academicYearTo = $('#academicYearTo').val();
    var tenancyStartDate = $('#tenancyStartDate').val();
    var tenancyEndDate = $('#tenancyEndDate').val();
    var renewAmount = $('#renewAmount').val();

    if( roomType == "" || roomType == "-1" || roomType == null ){
    	toastr.warning('Please Select Proper Room Type');
        addFocusId('roomType');
        return false;

    }else if( roomNumber == "" || roomNumber == "-1" || roomNumber == null ){
    	toastr.warning('Please Select Room Number');
        addFocusId('roomNumber');
        return false;

    }else if( bedNumber == "" ){
    	toastr.warning('Please Enter Bed Number');
        addFocusId('bedNumber');
        return false;

    }else if( semester == "" || roomNumber == "-1" ){
    	toastr.warning('Please Select Semester');
        addFocusId('semester');
        return false;

    }else if( academicYearFrom == "" ){
    	toastr.warning('Please Select Academic Start Date');
        addFocusId('academicYearFrom');
        return false;

    }else if( academicYearTo == "" ){
    	toastr.warning('Please Select Academic End Date');
        addFocusId('academicYearTo');
        return false;

    }else if( tenancyStartDate == "" ){
    	toastr.warning('Please Select Tenancy Renew Start Date');
        addFocusId('tenancyStartDate');
        return false;

    }else if( tenancyEndDate == "" ){
    	toastr.warning('Please Select Tenancy Renew End Date');
        addFocusId('tenancyEndDate');
        return false;

    }else if( renewAmount == "" ){
        toastr.warning('Please Enter Renew Enrollment Amount');
        addFocusId('renewAmount');
        return false;

    }else if( renewAmount != "" && ( parseFloat(renewAmount) < 1 ) ){
    	toastr.warning('Please Enter Valid Renew Enrollment Amount');
        addFocusId('renewAmount');
        return false;
    }
    var dateStartDate = new Date(ddmmyyToMysql(academicYearFrom));
    var dateEndDate = new Date(ddmmyyToMysql(academicYearTo));

    //START END DATE VALIDATION
    if (+dateEndDate < +dateStartDate) { //dateActEndDate dateActStartDate
        toastr.warning("Academic year to cannot be less than the Academic year from");
        addFocusId('academicYearTo');
        return false;

    }
    var dateRenStartDate = new Date(ddmmyyToMysql(tenancyStartDate));
    var dateRenEndDate = new Date(ddmmyyToMysql(tenancyEndDate));

    //START END DATE VALIDATION
    if (+dateRenEndDate < +dateRenStartDate) { //dateActEndDate dateActStartDate
        toastr.warning("Tenancy end date cannot be less than the Tenancy start date");
        addFocusId('tenancyEndDate');
        return false;

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'renewStudentAllotment', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 44),
        studentId : GBL_STUD_ID,
        itemId : roomType,
        roomId :  roomNumber,
        bedNo :  bedNumber,
        assetIssued :  allotData[0].assetIssued,
        roomName :  roomName,
        tenancyStartDate : tenancyStartDate, 
        tenancyEndDate :  tenancyEndDate,
        foodPreference :  allotData[0].foodPreference,
        tenancyRent :  renewAmount,
        acdemicStartYear :  academicYearFrom,
        acdemicEndYear :  academicYearTo,
        semester : semester,
        facilityAllocation :  allotData[0].facilityAllocation,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(FOLLOWUPURL, postData, generateInvoiceCallback,"Please wait...Your renew enrollment process is going on.");

    function generateInvoiceCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            localStorage.indeCampusRenewEnrollFlag = true;
            localStorage.indeCampusEditStudId = GBL_STUD_ID;
   			window.location.href = "collectpayment.html";
           
        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_RENEW );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// SET END DATE AS PER SELECTED START DATE
function setEndDate(context,refrenceDateEle){
    var startDate = $(context).val();

    $('#'+refrenceDateEle).datepicker('remove');
    $('#'+refrenceDateEle).datepicker({
        format: 'dd-mm-yyyy',
        startDate: startDate,
        // endDate: startDate,
        autoclose: true
    });
}

// SET END DATE AS PER SELECTED START DATE
function setEndYear(context,refrenceDateEle){
    var startDate = $(context).val();

    $('#'+refrenceDateEle).datepicker('remove');
    $('#'+refrenceDateEle).datepicker({
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months",
        startDate: startDate,
        autoclose:true
    });
}