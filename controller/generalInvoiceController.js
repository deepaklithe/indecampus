    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 06-08-2018.
 * File : generalInvoiceController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_CHARGE_LIST_DATA = []; // CHARGE POST JSON 

// PAGE LOAD EVENTS
function getGeneralInvoices() {

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'listInvoiceData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(23, 94),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getGeneralInvoicesCallback,"Please wait...Adding your item.");

    function getGeneralInvoicesCallback( flag , data ){
        if (data.status == "Success" && flag) {

        	console.log(data);
        	GBL_CHARGE_LIST_DATA = data.data;
			    renderChargePost();

		}else{
    			GBL_CHARGE_LIST_DATA = [];
    			renderChargePost();
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_CHARGE_POST );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RENDER ADDED CHARGE POST ENTRIES
function renderChargePost() {	

	
	var tHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="viewChargePostTable">'+
			        '<thead>'+
			            '<tr>'+
			                 '<th>#</th>'+
	                         '<th>Invoice No</th>'+
	                         '<th>Invoice Date</th>'+                                         
	                         '<th>Invoice Name</th>'+
	                         '<th>Student Name</th>'+
                           '<th>Total Charges</th>'+
	                         '<th>Status</th>'+
	                         '<th>Action</th>'+
			            '</tr>'+
			        '</thead>'+
			        '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

	if( GBL_CHARGE_LIST_DATA != "No record found" && GBL_CHARGE_LIST_DATA.length != 0 ){

		GBL_CHARGE_LIST_DATA.forEach(function( record , index ) {

				
			newRow += '<tr>'+
	                       '<td>'+ (index+1) +'</td>' +
	                       '<td>'+ record.clientInvoiceId +'</td>'+
	                       '<td>'+ mysqltoDesiredFormat(record.invoiceDate,'dd-MM-yyyy') +'</td>'+
	                       '<td>'+ record.invoiceName +'</td>'+
	                       '<td>'+ record.studName +'</td>'+
	                       '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(record.totalCharges)) +'</td>'+
                         '<td>'+ record.status +'</td>'+
	                       '<td>'+    
	                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-ripple btn-amber" data-toggle="tooltip" data-original-title="View Invoice" invoiceId="'+ record.clientInvoiceId +'" studId="'+ record.studentId +'" invId="'+ record.invoiceId +'" onclick="viewInvoice(this,0);"><i class="fa fa-eye"></i></a>'+
	                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" data-toggle="tooltip" data-original-title="Transaction History" invoiceId="'+ record.clientInvoiceId +'" studId="'+ record.studentId +'" onClick="viewInvoiceTransaction(this,0);"><i class="fa fa-undo"></i></a> '+
	                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-teal btn-ripple" data-toggle="tooltip" data-original-title="Email" invoiceId="'+ record.clientInvoiceId +'" studId="'+ record.studentId +'" invId="'+ record.invoiceId +'" onclick="sendInvoiceMail(this);"><i class="fa fa-envelope"></i></a>'+
	                            '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-success btn-ripple" data-toggle="tooltip" data-original-title="Print" invoiceId="'+ record.clientInvoiceId +'" studId="'+ record.studentId +'" invId="'+ record.invoiceId +'" onClick="viewInvoice(this,1);"><i class="fa fa-print"></i></a>'+
	                            '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple" data-toggle="tooltip" data-original-title="Edit" invoiceId="'+ record.clientInvoiceId +'" studId="'+ record.studentId +'" onClick="viewInvoiceTransaction(this,1);"><i class="fa fa-pencil"></i></a>'+
	                       '</td>'+
	                   '</tr>';

		});

	}
	$('#nonProcessTable').html( tHead + newRow + tEnd );
	$('#viewChargePostTable').dataTable();
}

function viewInvoice(context,type){

    var invoiceId = $(context).attr('invoiceId'); 
    var invId = $(context).attr('invId');
    var studId = $(context).attr('studId'); 

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var requestCase = 'getStudentInvoiceData';
    var index = findIndexByKeyValue( GBL_CHARGE_LIST_DATA , "invoiceId" , invId );
    if( index != "-1" ){
        if( GBL_CHARGE_LIST_DATA[index].invoiceType == "PROPERTY" ){
          requestCase = 'getCustomerInvoiceData';
        }
    } 
    
    var postData = {
        requestCase: requestCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(16, 66),
        studentId : studId,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, viewInvoiceCallBack,CREATINGINQDATA);

    function viewInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            renderInvoiceDetail(data.data,type,GBL_CHARGE_LIST_DATA[index].invoiceType); //GBL_CHARGE_LIST_DATA[index] Changes KAVITA PATEL ON 23-08-2018
        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INVOICE );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

}

function renderInvoiceDetail(invoiceData,type,invType){

    // type : 0 = SET HTML & MODAL OPEN , 1 = SET HTML & DIRECT PRINT
    if( invoiceData != "" && invoiceData != "No record found" ){ 

        var invoiceDetails = invoiceData[0];
        $('#invoiceNoPrint').html( '# ' + invoiceDetails.cInvoiceId );
        $('#invoiceDatePrint').html( mysqltoDesiredFormat(invoiceDetails.invoiceDate,'dd-MM-yyyy') );
        $('#invPeriodPrint').html(  'Period From: '+ ( invoiceDetails.periodFrom != "" ? mysqltoDesiredFormat(invoiceDetails.periodFrom,'dd-MM-yyyy') : "" ) +' To ' + ( invoiceDetails.periodTo != "" ? mysqltoDesiredFormat(invoiceDetails.periodTo,'dd-MM-yyyy') : "") );
        $('#studInvNamePrint').html( invoiceDetails.fullName );
        $('#address1Print').html( invoiceDetails.address );
        $('#address2Print').html( invoiceDetails.cityName + ' ' + invoiceDetails.stateName + ' ' + invoiceDetails.countryName + ' - ' + invoiceDetails.pinCode );
        $('#studMemIdPrint').html( 'Student Membership No : ' + invoiceDetails.memberShipNo  );

        $('#address1Print').show();
        $('#address2Print').show();
        $('#student-detail').show();
        if( invType == "PROPERTY" ){
          $('#address1Print').hide();
          $('#address2Print').hide();
          $('#student-detail').hide();
        }

        var tBody = '';
        var totalCgstAmount = 0;
        var totalSgstAmount = 0;
        var totalIgstAmount = 0;
        var subTotal = 0;
        var preBalance = 0.00;
        var collection = 0.00;
        var balDue = 0.00;
        if( invoiceDetails.itemData != "" && invoiceDetails.itemData != "No record found" ){

            invoiceDetails.itemData.forEach(function( record , index ){
                var cgstPer = ( record.cgst != "" ? fixedTo2(record.cgst) : 0 );
                var sgstPer = ( record.sgst != "" ? fixedTo2(record.sgst) : 0 );
                var igstPer = ( record.igst != "" && record.igst != undefined ? fixedTo2(record.igst) : 0 );
                var itemTotal = parseFloat(record.itemPrice) * parseFloat(record.qty);
                var cgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(cgstPer)) / 100);
                var sgstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(sgstPer)) / 100);
                var igstAmt = fixedTo2((parseFloat(itemTotal) * parseFloat(igstPer)) / 100);
                totalCgstAmount = fixedTo2(parseFloat(totalCgstAmount) + parseFloat(cgstAmt));
                totalSgstAmount = fixedTo2(parseFloat(totalSgstAmount) + parseFloat(sgstAmt));
                totalIgstAmount = fixedTo2(parseFloat(totalIgstAmount) + parseFloat(igstAmt));
                var itemQty = parseFloat(record.qty);
                var itemSub = parseFloat(itemTotal) + parseFloat(cgstAmt) + parseFloat(sgstAmt) + parseFloat(igstAmt);
                subTotal = fixedTo2(parseFloat(subTotal) + parseFloat(itemTotal));

                tBody += '<tr>'+
                            '<td style="padding: 5px 0 5px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+ (index+1) +'</td>'+
                            '<td style="padding: 5px 0px 5px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+
                                '<div>'+
                                   '<div><span style="word-wrap: break-word;" id="item_name">'+ record.itemName +'</span><br><span class="item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px; " > '+record.hsnCode+'</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ parseFloat(record.itemPrice) +'</div>'+
                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span></span> '+ itemQty +'</div>'+
                                // '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ cgstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ sgstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ sgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ igstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ igstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">'+
                                '<span id="item_amount">'+
                                '<span>₹</span> '+ formatPriceInIndianFormat(fixedTo2(itemTotal)) +'</span>'+
                            '</td>'+
                        '</tr>';
            });        
        }

        var totalPayment = ( invoiceDetails.totalPayment != null && invoiceDetails.totalPayment != "" ? invoiceDetails.totalPayment : 0.00 );
        var finalTotal = fixedTo2(parseFloat(subTotal) + parseFloat(totalCgstAmount) + parseFloat(totalSgstAmount) + parseFloat(totalIgstAmount));
        balDue = (parseFloat(preBalance) + parseFloat(totalPayment)) - parseFloat(finalTotal);
        var balanceDueType = (parseFloat(balDue) > 0) ? 'CR' : 'DR';
        collection = parseFloat(totalPayment);

        tBody += '<tr>'+
                  '<td colspan="8" style="text-align:right;">'+
                     '<table class="totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">'+
                        '<tbody>'+
                           '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>'+
                              '<td colspan="2" id="subtotal" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(subTotal) +'</td>'+
                           '</tr>'+
                           '<tr style="height:10px;">'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>'+
                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(fixedTo2(totalCgstAmount)) +'</td>'+
                           '</tr>'+
                           '<tr style="height:10px;">'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>'+
                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(fixedTo2(totalSgstAmount)) +'</td>'+
                           '</tr>'+
                           '<tr style="height:10px;">'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>'+
                              '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ formatPriceInIndianFormat(fixedTo2(totalIgstAmount)) +'</td>'+
                           '</tr>'+
                           '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>'+
                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ formatPriceInIndianFormat(fixedTo2(finalTotal)) +'</b></td>'+
                           '</tr>'+
                           //  '<tr>'+
                           //    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Pre Balance</b></td>'+
                           //    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                           //    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ preBalance +'</b></td>'+
                           // '</tr>'+
                             '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>'+
                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>CR</b></td>'+
                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span><b> '+ formatPriceInIndianFormat(fixedTo2(collection)) +'</b></td>'+
                           '</tr>'+
                            '<tr>'+
                              '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right background-color: #f5f4f3; height: 40px;font-size: 9pt;" ><b>Balance Due</b></td>'+
                              '<td id="total" style="width:50px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><b>'+ balanceDueType +'</b></td>'+
                              '<td id="total" style="width:120px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><span>₹</span><b> '+ formatPriceInIndianFormat(fixedTo2( Math.abs(balDue) )) +'</b></td>'+
                           '</tr>'+
                            '<tr><td colspan="5"  style="font-size:16px;text-align: left;"><b><u>Terms and Conditions</u></b></td></tr>'+
                            '<tr><td colspan="5" style="padding-top:10px;padding-bottom: 4px;text-align: left;"><span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
                            '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus</span></b>".</span></td></tr>'+
                         '</tbody>'+
                     '</table>'+
                  '</td>'+
                '</tr>';

        $('#printItemBody').html( tBody );
        if( type == 0 ){
            modalShow('viewInvoiceDetail');
        }else{
            printInvoice();
        }

    }

}

function printInvoice(){

    var data = $('#receiptPrint').html();
    var myWindow=window.open('','','height=800,width=1200');
        
    myWindow.document.write(data);
    myWindow.document.close();
    setTimeout(function() {
        myWindow.print();
        myWindow.close();
    }, 250);

}

function viewInvoiceTransaction(context,type){

    var invoiceId = $(context).attr('invoiceId');
    var studId = $(context).attr('studId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'getInvoiceTrans',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        studentId : studId,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, viewInvoiceTransactionCallBack,CREATINGINQDATA);

    function viewInvoiceTransactionCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            var transactionData = data.data;
            renderTransactionData(transactionData,type,studId);

        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INV_TRANS );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderTransactionData(transactionData,type,studId){

    var tHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="transactionTable">'+
                   '<thead>'+
                      '<tr>'+
                         '<th>#</th>'+
                         '<th>Item Name</th>'+
                         '<th>Type</th>'+                                         
                         '<th>Charge Date</th>'+
                         '<th>Charge Qty</th>'+
                         '<th>Total Price</th>'+
                         '<th class="editInvoice">Action</th>'+
                      '</tr>'+
                   '</thead>'+
                   '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

    if( transactionData.length != 0 && transactionData != "No record found" ){

        transactionData.forEach(function( record , index ){

            var canceledInv = '';
            if( record.status == "2" ){ // CANCELED INVOICE
                canceledInv = 'disabled';
            }
            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td>'+ record.type +'</td>'+
                            '<td>'+ mysqltoDesiredFormat(record.chargeDate,'dd-MM-yyyy') +'</td>'+
                            '<td>'+ record.qty +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(record.totalPrice)) +'</td>'+
                            '<td class="editInvoice">'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-ripple" data-toggle="tooltip" data-original-title="Cancel Invoice" studId="'+studId+'" chargeId="'+ record.chargeId +'" invoiceId="'+ record.clientInvoiceId +'" onClick="cancelInvoice(this);" '+canceledInv+'><i class="fa fa-times"></i> Cancel</a>'+
                            '</td>'+
                        '</tr>';
        });

    }

    $('#invoiceTransaction').html( tHead + newRow + tEnd );
    if( type == "0" ){
        $('.editInvoice').hide();
    }else{
        $('#transactionModalHeading').html('Edit Invoice');
    }
    $('#transactionTable').dataTable();
    modalShow('transactionModal');
}

function cancelInvoice(context){

    var invoiceId = $(context).attr('invoiceId');
    var studId = $(context).attr('studId');
    var chargeId = $(context).attr('chargeId');
    var cnfrmCancel = confirm("Are you sure you want to cancel invoice ?");
    if( !cnfrmCancel ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'cancelInvoiceTrans',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 96),
        chargeId : chargeId,
        studentId : studId,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, cancelInvoiceCallBack,CREATINGINQDATA);

    function cancelInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            toastr.success("Invoice cancelled successfully");
            $(context).attr('disabled',true);

        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INV_TRANS_CANCEL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

}

function sendInvoiceMail(context){

    var invoiceId = $(context).attr('invoiceId');
    var invId = $(context).attr('invId');
    var studId = $(context).attr('studId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var requestCase = 'sendStudentInvoiceMail';
    var index = findIndexByKeyValue( GBL_CHARGE_LIST_DATA , "invoiceId" , invId );
    if( index != "-1" ){
        if( GBL_CHARGE_LIST_DATA[index].invoiceType == "PROPERTY" ){
          requestCase = 'sendCustomerInvoiceMail';
        }
    }

    var postData = {
        requestCase: requestCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(16, 66),
        studentId : studId,
        invoiceNo : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(COMMONURL, postData, cancelInvoiceCallBack,CREATINGINQDATA);

    function cancelInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);

        }else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_INV_MAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }

}