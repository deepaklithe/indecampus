/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 02-07-2018.
 * File : commonController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

 var GBL_INDEX = "-1";
 var inqData = [];
 var GBL_CONT_PERSON = [];  // SEARCHED CONTACT PERSON OF CUSTOMER
 
//Followup pop up Starts here
document.write('<div class="modal fade full-height from-right" id="followupModel" tabindex="-1" role="dialog" aria-hidden="true">'+
                '<div class="modal-dialog min-width-modal">'+
                    '<div class="modal-content">'+
                    '<ul class="nav nav-tabs nav-justified" role="tablist">'+
                      '<li class="today active" onclick="getFollowupDetail(1)"><a href="#todayTab" data-toggle="tab">Today<span id="todaySbAct"></span></a></li>'+
                      '<li class="delayed" onclick="getFollowupDetail(2)"><a href="#delayTab" data-toggle="tab">Delay<span id="delaySbAct"></span></a></li>'+
                      '<li class="upcomming" onclick="getFollowupDetail(3)"><a href="#upcommingTab" data-toggle="tab">Planned<span id="planSbAct"></span></a></li>'+
                      '<li class="inprocess" onclick="getFollowupDetail(4)"><a href="#inprocessTab" data-toggle="tab">InProcess<span id="inProcessSbAct"></span></a></li>'+
                      // '<li class="calllog" onclick="getCallLogData();"><a href="#colllogTab" data-toggle="tab">CallLog<span id="callLogSbAct"></span></a></li>'+
                    '</ul>'+            
                    '<div class="modal-body no-pad-lr">'+
                        '<div class="panel no-padding-tab">'+                                
                            '<div class="tab-content">'+
                                '<div class="tab-pane today active" id="todayTab">'+
                                    '<ul id="todayFollowModel" class="list-group list-group-sm list-group-sp list-group-alt auto m-t">'+
                                    '</ul>'+
                                '</div>'+
                                '<div class="tab-pane delayed" id="delayTab">'+
                                    '<ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t" id="delayedFollowModel">'+
                                    '</ul>'+
                                '</div>'+
                                '<div class="tab-pane upcomming" id="upcommingTab">'+
                                    '<ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t" id="upcomingFollowModel">'+
                                    '</ul>'+
                                '</div>'+
                                '<div class="tab-pane inprocess" id="inprocessTab">'+
                                    '<ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t" id="inProcessActivityModel">'+
                                    '</ul>'+
                                '</div>'+
                                '<div class="tab-pane calllog" id="colllogTab">'+
                                    '<ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t" id="callLogModel">'+
                                    '</ul>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                       '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+                            
                    '</div>'+
                '</div>'+
            '</div>'+
            '</div>');

// <!-- Begin Change Branch Modal -->
            

document.write('<div class="modal scale fade" id="changebranch" tabindex="-1" role="dialog" aria-hidden="true">'+
                '<div class="modal-dialog">'+
                    '<div class="modal-content">'+
                        '<div class="modal-body overflowbody">'+
                            '<form action="#">'+
                                '<div class="form-content">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label" style="width: 100%;">Client</label>'+
                                                '<label></label>'+ 
                                                '<select data-live-search="true" id="clientListForChangeBranch" data-width="100%" onChange="onChangeClient();">'+
                                                '</select>'+
                                                '</select>'+
                                            '</div>'+
                                        '</div>'+
                                          '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label" style="width: 100%;">Branch</label>'+
                                                '<select data-live-search="true" id="branchListForChangeBranch" data-width="100%">'+
                                                '</select>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<div class="modal-footer">'+
//                            '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+
                            '<button type="button" class="btn  btn-primary" onClick="navigateToclientMgmt(0);" id="branchCrClientBtn" style="display:none;"><i class="fa fa-plus"></i> Client </button>'+
                            '<button type="button" class="btn  btn-primary" onClick="changebranch();"><i class="fa fa-sitemap"></i> Change Branch</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
        // <!-- End Change Branch Modal -->

// cloud telephony pop for addfollowup
document.write('<div aria-hidden="true" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static" role="dialog" tabindex="-1" id="addfollowup" class="modal fade">'+
                  '<div class="modal-dialog modal-md">'+
                      '<div class="modal-content">'+
                          '<div class="modal-header">'+
                              '<h4 class="modal-title">Add Followup</h4>'+
                          '</div>'+
                          '<div class="modal-body">'+
                              '<div class="row">'+
                                  '<div class="col-lg-12 col-md-12 col-xs-12 tooltips" title="Inquiry Search " data-placement="top" data-toggle="tooltip" data-original-title="Inquiry Search " style="margin-bottom:15px;">'+
                                    '<select data-live-search="true" id="AddfollowUpInqLog" class="form-control">'+
                                    '</select>'+
                                '</div>'+
                                '<div class="col-lg-12 col-md-12 col-xs-12 tooltips" title="Next Followup Date" data-placement="top" data-toggle="tooltip" data-original-title="Next Followup Date" style="margin-bottom:15px;">'+
                                      '<input class="form-control form-control-inline input-medium default-date-picker" size="16" type="text" placeholder=" Next Followup Date" id="AddfollowUpDate">'+
                                  '</div>'+
                                  '<div class="col-lg-12 col-md-12 col-xs-12 tooltips" title="Remark" data-placement="top" data-toggle="tooltip" data-original-title="Remark" style="margin-bottom:15px;">'+
                                        '<label><strong>Remark</strong>'+
                                      '</label>'+
                                      '<textarea class="form-control" size="16" type="text" placeholder="Remark" id="AddfollowUpRemark" ></textarea>'+
                                '</div></div>'+
                          '</div>'+
                          '<div class="modal-footer">'+
                              '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+
                              '<button onclick="addCallFollowup(this);" class="btn btn-flat btn-primary btn-ripple"> Add Followup </button>'+
                          '</div>'+
                      '</div>'+
                  '</div>'+
                '</div>');

        // <!-- Nazim khan cloud Telephone DO NOTHING pop up starts here -->
document.write('<div aria-hidden="true" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static" role="dialog" tabindex="-1" id="addRemark" class="modal fade">'+
                      '<div class="modal-dialog" style="width:30%;">'+
                          '<div class="modal-content">'+
                              '<div class="modal-header">'+
                                  '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                                  '<h4 class="modal-title">Add Remark</h4>'+
                              '</div>'+
                              '<div class="modal-body">'+
                                '<div class="row">'+
                                       '<div class="col-lg-12 col-md-12 col-xs-12 tooltips" title="Remark" data-placement="top" data-toggle="tooltip" data-original-title="Remark" style="margin-bottom:15px;">'+
                                         '<textarea class="form-control" size="16" type="text" placeholder="Remark" id="addRemarkValue" ></textarea>'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                              '<div class="modal-footer">'+
                                  '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+
                                  '<button onclick="addRemark(this);" class="btn btn-flat btn-primary btn-ripple"> Add Remark </a>'+
                              '</div>'+
                          '</div>'+
                      '</div>'+
                '</div>');

      // <!-- ends here -->

       // <!-- Nazim khan cloud Telephone CREATE / UPDATE CUSTOMER pop up starts here -->

document.write('<div aria-hidden="true" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static" role="dialog" tabindex="-1" id="createCust" class="modal fade">'+
                  '<div class="modal-dialog" style="width:30%;">'+
                      '<div class="modal-content">'+
                          '<div class="modal-header">'+
                              '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                              '<h4 class="modal-title">Create / Update Customer</h4>'+
                          '</div>'+
                          '<div class="modal-body modal-overflow">'+
                            '<div class="row">'+
                                  '<div class="col-lg-12 col-md-12 col-xs-12 tooltips" title="Create / Update Customer" data-placement="top" data-toggle="tooltip" data-original-title="Create / Update Customer">'+
                                       '<input id="custSearch1" type="text" class="form-control" size="16" placeholder="Search Customer Here..">'+
                                       '<ul class="typeahead dropdown-menu">'+
                                       '</ul>'+
                                  '</div>'+
                              '</div>'+
                          '</div>'+
                      '</div>'+
                  '</div>'+
              '</div>');
  // <!--pop up  ends -->  


  // <!-- Nazim khan Alert Notification pop up starts here -->

document.write(' <div class="modal scale fade full-height from-right" id="notificationModal" tabindex="-1" role="dialog" aria-hidden="true">'+
                '<div class="modal-dialog min-width-modal">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header bg-primary bot-bor-head">'+
                            '<h4 class="modal-title"><i class="fa fa-bell"></i> Notifications</h4>'+
                        '</div>'+
                        '<div class="modal-body no-pad-lr">'+                            
                            // '<ul class="nav nav-tabs nav-justified" role="tablist">'+
                                    // '<li class="tab-top-color curPoint active" data-toggle="tab" href="#Inq-tab"><a href="javascript:void(0)"> Notifications</a></li>'+
                                    // '<li class="tab-bottom-color curPoint" href="#Proposal-tab" onclick="getProposalChatNotif();" data-toggle="tab"><a href="javascript:void(0)">Proposal Chat</a></li>'+
                            // '</ul>'+
                            '<div class="main-panel">'+
                                '<div class="panel">'+
                                    // '<div class="tab-content">'+
                                    //     '<div class="tab-pane active" id="Inq-tab">'+
                                            '<div class="row" id="clearNotifDiv" style="display:none">'+                           
                                              '<div class="col-md-12">'+     
                                                  '<div class="checkboxer pull-left selAllCheck"><input type="checkbox" id="selDeselAll" class="" onChange="selectDeselectAll()"><label class="control-label" for="selDeselAll"> All </label></div>'+
                                                  '<a class="btn btn-sm btn-primary notifbtn btn-ripple pull-right" onclick="clearNotifList()"><i class="fa fa-window-close"></i> Clear </a>'+                      
                                              '</div>'+                           
                                            '</div>'+
                                            '<ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t" id="notifList">'+
                                            '</ul>'+
                                        '</div>'+
                                        '<div class="tab-pane" id="Proposal-tab">'+
                                            '<ul class="list-group list-group-sm list-group-sp list-group-alt auto m-t" id="propNotifList">'+
                                              '<h4 style="text-align:center; margin-top:20px;">No Notification Found</h4>'+
                                            '</ul>'+
                                        '</div>'+
                                //     '</div>'+
                                // '</div>'+
                            '</div>'+
                        '</div> '+
                        '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+
                            // '<button onclick="addRemark(this);" class="btn btn-flat btn-primary btn-ripple"> Add Remark </a>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
  // <!--pop up  ends -->    

  // <!-- Begin Version Info  Modal -->
  document.write('<div class="modal scale fade" id="versionDetailModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">'+
                  '<div class="modal-dialog">'+
                      '<div class="modal-content">'+
                          '<div class="modal-header">'+
                              '<button type="button" class="btn close-btn btn-flat btn-default btn-ripple" data-dismiss="modal"><i class="fa fa-close"></i></button>'+
                          '</div>'+
                            '<div class="modal-body min-height-modal">'+
                                '<div class="col-md-12">'+
                                  '<div class="form-content">'+
                                      '<div class="row margin-bottom-10" id="newVerContent">'+
                                      '</div>'+
                                  '</div>'+
                                '</div>'+
                            '</div>'+
                          '<div class="modal-footer">'+
                              '<div class="form-group">'+
                                  '<div class="checkboxer">'+
                                      '<input type="checkbox" id="neverShow">'+
                                      '<label for="neverShow"> Never Show me Again </label>'+                                    
                                  '</div>'+
                              '</div>'+
                          '</div>'+
                      '</div>'+
                  '</div>'+
              '</div>');
// <!-- End Version Info Modal -->

// <!-- Begin Creat Customer  Modal -->
  document.write('<div class="modal scale fade" id="createCustomer" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">'+
                '<div class="modal-dialog modal-lg">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<h4 class="modal-title">Create Customer</h4>'+
                        '</div>'+
                        '<div class="modal-body">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 col-sm-12" id="createCustRateTypeDiv" style="display:none;">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Rate Type</label>'+
                                        '<select data-live-search="true" id="createCustRateType" data-width="100%"></select>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-12 col-sm-12">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-address-book"></i> Name <span class="mandatory">*</span></label>'+
                                        '<input type="text" id="createCustFname" class="form-control" autocomplete="off" placeholder="Name (eg. John Smith)">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6 col-sm-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-mobile"></i> Mobile Num <span class="mandatory">*</span></label>'+
                                        '<input type="text" id="createCustMobile" maxlength="13" placeholder="Mobile (eg. +9198xxxxxxxx)" value="+91" class="form-control onlyNumber mobMaxLenCls" autocomplete="off">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6 col-sm-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-envelope"></i> Email</label>'+
                                        '<input type="text" id="createCustEmail" class="form-control" autocomplete="off" placeholder="Email (eg. john.smith@gmail.com)">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-9 col-sm-9">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-address-card-o"></i> Address <span class="mandatory">*</span></label>'+
                                        '<input type="text" id="createCustAddress" class="form-control" autocomplete="off" placeholder="Address (eg. 1750 East 4th Street. Santa Ana)">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-3">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-road"></i> Catchment Area</label>'+
                                        '<input type="text" id="createCustCatchArea" class="form-control" autocomplete="off" placeholder="Catchment Area (eg. Caltrans)">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-4 col-sm-4">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-home"></i> Country <span class="mandatory">*</span></label>'+
                                        '<select id="createCustCountry" data-live-search="true" name="createCustCountry" countryId="createCustCountry" stateId="createCustState" onchange="populateState()" data-width="100%"></select>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-4 col-sm-4">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-home"></i> State <span class="mandatory">*</span></label>'+
                                        '<select id="createCustState" data-live-search="true" name="createCustState" stateId="createCustState" cityId="createCustCity" onchange="populateCityOnState()" data-width="100%"></select>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-4 col-sm-4">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-home"></i> City <span class="mandatory">*</span></label>'+
                                        '<select id="createCustCity" data-live-search="true" data-width="100%"></select>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6 col-sm-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-address-book"></i> Contact Name <span class="mandatory">*</span></label>'+
                                        '<input type="text" id="createContName" class="form-control" autocomplete="off" placeholder="Contact Name (eg. John Smith)">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6 col-sm-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-mobile"></i> Contact Mobile <span class="mandatory">*</span></label>'+
                                        '<input type="text" id="createContMobile" value="+91" maxlength="13" class="form-control onlyNumber mobMaxLenCls" placeholder="Contact Mobile (eg. +9198xxxxxxxx)" autocomplete="off">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6 col-sm-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-envelope"></i> Contact Email <span class="mandatory">*</span></label>'+
                                        '<input type="text" id="createContEmail" placeholder="Contact Email (eg. john.smith@gmail.com)" class="form-control" autocomplete="off">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6 col-sm-6">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+
                                        '<i class="fa fa-id-badge"></i> Contact Designation</label>'+
                                        '<input type="text" id="createContDesig" class="form-control" placeholder="Contact Designation (eg. Manager)" autocomplete="off">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+
                            '<button type="button" onclick="createCustomer()"  class="btn btn-flat btn-primary">Create It</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
            // <!-- End Creat Customer Modal -->

// Activity Listing After Due Date Selection Starts here
document.write('<div class="modal scale fade" id="actTitleShowModal" tabindex="-1" role="dialog" aria-hidden="true">'+
                '<div class="modal-dialog modal-lg">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<h4 class="modal-title">Activity Listing</h4>'+
                        '</div>'+
                        '<div class="modal-body">'+
                            '<form action="#">'+
                                '<div class="form-content">'+  
                                    '<div class="row margin-bottom-10">'+
                                        '<div class="col-md-12">'+
                                            '<div id="actDueListDiv">'+
                                                
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+   
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-flat btn-default btn-ripple" data-dismiss="modal">Close</button>'+
                           
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>' );
// Activity Listing After Due Date Selection Ends here


//CHANGE PASSWORD MODAL STARTS HERE

var show = "show";
var newpasswordManager = "newpasswordManager";
var retypenewpasswordManager = "retypenewpasswordManager";
document.write('<div class="modal scale fade" id="resetPasswordManageModal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">'+
                  '<div class="modal-dialog">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<h4 class="modal-title">Reset Your Password</h4>'+
                        '</div>'+
                        '<div class="modal-body overflowbody">'+
                            '<form action="#" class="margin-bottom-20">'+
                               ' <div class="form-content">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label"><i class="fa fa-eye-slash"></i> New Password '+
                                                  '<span class="mandatory">*</span>'+
                                                  '<a href="javascript:void(0);" data-toggle="popover" title="" data-content="Password Should Contains atleast 8 Chars , Alphanumeric Value with Upper Case letter and a special Character  like.. Abc@1234" data-placement="left" data-original-title="New Password">'+
                                                      '<i class="fa fa-info"></i>'+
                                                  '</a></label>'+
                                                '<div class="inputer">'+
                                                    '<div class="input-wrapper">'+
                                                        '<input type="password" name="newpassword" id="newpasswordManager" class="form-control" autocomplete="off" placeholder="Password">'+
                                                        '<span class="showHidePwd" onclick="showHidePwd(newpasswordManager,show,this);">Show</span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6">'+
                                            '<div class="form-group">'+
                                                '<label class="control-label"><i class="fa fa-eye-slash"></i> Re-type New Password<span class="mandatory">*</span>'+
                                                '<a href="javascript:void(0);" data-toggle="popover" title="" data-content="Password Should Contains atleast 8 Chars , Alphanumeric Value with Upper Case letter and a special Character  like.. Abc@1234" data-placement="left" data-original-title="Re Type New Password">'+
                                                    '<i class="fa fa-info"></i>'+
                                                '</a></label>'+
                                                '<div class="inputer">'+
                                                    '<div class="input-wrapper">'+
                                                        '<input type="password" name="retypenewpassword" id="retypenewpasswordManager" class="form-control" autocomplete="off" placeholder="Confirm Password">'+
                                                        '<span class="showHidePwd" onclick="showHidePwd(retypenewpasswordManager,show,this);">Show</span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                          '<b>Note:</b> <span class="mandatory"> Password Should Contains atleast 8 Chars , Alphanumeric Value with Upper Case letter and a special Character  like.. Abc@1234 </span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            // '<button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>'+
                            '<button type="button" class="btn btn-flat btn-primary" onclick="changePasswordManager()">Change Password</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
              '</div>');
//CHANGE PASSWORD MODAL ENDS HERE

// var callRing = new Audio('img/ringTone.mp3');

var CALLLOGID = "";
var CALLOAD = false; 
var GBL_SELECTEDCUSTID = "";
var GBL_SELECTEDCUSTMOBNO = "";

var eventAlertData = []; //Alert Notification data
var alertCount = 0; //Alert Count for Notification

var maxDate;

if( localStorage.indeCampusRoleAuth != undefined ){
    
    var config = JSON.parse(localStorage.indeCampusRoleAuth);
}

var ACT_MODE_PHONE = "1";
var ACT_MODE_EMAIL = "2";
var ACT_MODE_SMS = "3";
var ACT_MODE_PERSONALMEET = "4";

var TodayFollowup = [];
var UpcomingFollowup = [];
var DelayedFollowup = [];
var callData = [];

var TodayActivity = [];
var UpcomingActivity = []; 
var DelayedActivity = [];
var InprocessActivity = [];

var setFollowup = "0";

var contactCust = false;


var startDate = new Date().format('dd-MM-yyyy');
var endDate = new Date().format('dd-MM-yyyy');


function checkAuth(moduleId, activityId, chk) {
    //log("local load auth");
    //log(localStorage.indeCampusRoleAuth);
    var permission = JSON.parse(localStorage.indeCampusRoleAuth);

    var user = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = user.data[0].PK_USER_ID;

    if( userId == -1 ){
      if( permission != "parsererror" ){

          permission = permission.data.permission;
          //log("permission");
          //log(permission);
          var tmpArr = [];
          
          var branchIdForChkAuth = localStorage.indeCampusBranchIdForChkAuth;
          if((branchIdForChkAuth== undefined || branchIdForChkAuth=="")){
              branchIdForChkAuth="";
          }

          if (activityId == -1) {
              var index = findIndexByKeyValue(permission, "PK_MODULE_ID", moduleId);
              if (index == -1){
                  return index;
              }else{
                  tmpArr = (permission[index].PK_ORG_ID).split(",");
                  // log("org id permission tmp array");
                  // log(tmpArr);
                  var IndexOf = tmpArr.indexOf(branchIdForChkAuth);
                  if(IndexOf != -1){
                      return tmpArr[IndexOf];
                  }else{
                      //return permission[index].PK_ORG_ID;
                      return -1;
                  }
                  
              }
                  
          } else if (chk) {
              var index = findIndexByKeyValueAndCond(permission, "PK_ACTIVITY_ID", "PK_MODULE_ID", activityId, moduleId)
              if (index == -1)
                  return -1;
              else
                  return true;
          } else {
              var index = findIndexByKeyValueAndCond(permission, "PK_ACTIVITY_ID", "PK_MODULE_ID", activityId, moduleId)
              if (index == -1){
                  return index;
              }else{
                  tmpArr = (permission[index].PK_ORG_ID).split(",");
                  // log("org id permission tmp array");
                  // log(tmpArr);
                  // log("branch Id Selected : "+branchIdForChkAuth);
                  var IndexOf = tmpArr.indexOf(branchIdForChkAuth);
                  if(IndexOf != -1){
                      return tmpArr[IndexOf];
                  }else{
                      //return permission[index].PK_ORG_ID;
                      return -1;
                  }
                  
              }
          }
      }else{
          console.log("No Load auth");
      }
    }else{

      if( permission != "parsererror" ){

          permission = permission.data.permission;
          //log("permission");
          //log(permission);
          var tmpArr = [];
          
          var branchIdForChkAuth = localStorage.indeCampusBranchIdForChkAuth;
          if((branchIdForChkAuth== undefined || branchIdForChkAuth=="")){
              branchIdForChkAuth="";
          }

          if (activityId == -1) {
              var index = findIndexByKeyValue(permission, "PK_MODULE_ID", moduleId);
              if (index == -1){
                  return index;
              }else{
                  tmpArr = (permission[index].PK_ORG_ID).split(",");
                  // log("org id permission tmp array");
                  // log(tmpArr);
                  var IndexOf = tmpArr.indexOf(branchIdForChkAuth);
                  if(IndexOf != -1){
                      return tmpArr[IndexOf];
                  }else{
                      //return permission[index].PK_ORG_ID;
                      return -1;
                  }
                  
              }
                  
          } else if (chk) {
              var index = findIndexByKeyValueAndCond(permission, "PK_ACTIVITY_ID", "PK_MODULE_ID", activityId, moduleId)
              if (index == -1)
                  return -1;
              else
                  return true;
          } else {
              var index = findIndexByKeyValueAndCond(permission, "PK_ACTIVITY_ID", "PK_MODULE_ID", activityId, moduleId)
              if (index == -1){
                  return index;
              }else{
                  tmpArr = (permission[index].PK_ORG_ID).split(",");
                  // log("org id permission tmp array");
                  // log(tmpArr);
                  // log("branch Id Selected : "+branchIdForChkAuth);
                  var IndexOf = tmpArr.indexOf(branchIdForChkAuth);
                  if(IndexOf != -1){
                      return tmpArr[IndexOf];
                  }else{
                      //return permission[index].PK_ORG_ID;
                      return -1;
                  }
                  
              }
          }
      }else{
          console.log("No Load auth");
      }
    }
}

function commonCallback(flag, data, callback) {
    if (data.status == "Success" && flag) {
        callback();
    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_LOAD_AUTH );
        else
            toastr.error(SERVERERROR);
    }
}

function loadRoleAndAuth( type ){
    addRemoveLoader(1);
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase : "loadauth",
        userId : tmp_json.data[0].PK_USER_ID,
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        orgId : localStorage.indeCampusBranchIdForChkAuth,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(COMMONURL,postData, 
        function(a,b){
            //log(b); return false;
            
            if(b.status==SCS){
                localStorage.indeCampusRoleAuth = JSON.stringify(b);
                localStorage.indeCampusDateFormat = "dd-MM-yyyy h:mm";

                setTimeout(function(){
                    var indeCampusAccessData = "";   //Module Accessability
                    var accessIds = JSON.parse(localStorage.indeCampusRoleAuth).data.userAccessData;   
                    
                      accessIds.forEach( function( record , index ){
                           
                        indeCampusAccessData += record.MODULE_ID+",";
                          if(index == (accessIds.length-1)){
                              console.log("Access data");
                              console.log(indeCampusAccessData);
                              indeCampusAccessData.slice(0, -1);
                              localStorage.indeCampusAccessData = indeCampusAccessData;
                              if( type != "1" ){
                                  commonCallback(a,b,navigateToDashboard);
                              }
                          }
                      });
                },200);
                addRemoveLoader(0);
            }else{
                addRemoveLoader(0);
                toastr.warning(b.status);
                return false;
            }
        }, "Please wait... Retriving Permission Data.");
}


var searchedCustId = "";
var contactPerSonData = [];
function commonAutoCompleteForCustomer(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var mobileNumber = item.text.split(",");
            
            var index = findIndexByKeyValueAndCond(ITEMS, "id","mobile", item.value,mobileNumber[1]);
            
            searchedCustId = item.value;

            setTimeout(function(){
              if ( index == -1 ){
                      GBL_INDEX = "-1";
                      var newCustNm = item.text.split(":")[0];
                      $('#custSearch').val(newCustNm);
                      $('#companyName1').val(newCustNm);
                      setNumberInIntl( 'mobileNumber' , "" );
                      setNumberInIntl( 'ContmobileNumber' , "" );
                      // $('#mobileNumber').val("");                
                      // $('#ContmobileNumber').val("");                
                      $('#companyNum1').val("");              
                      $('#contPerson').val("");    
                      $('#email').val("");
                      $('#custAddress').val('');
                      $('#contPersonDesig').val('');
                      $('#contPersonEmail').val('');

                      $('#ContactData').show();
              }
              else{
                      GBL_INDEX = index;
                      $('#contPerson').val(ITEMS[index].contactPersonName);
                      $('#email').val(ITEMS[index].email);
                      // $('#ContmobileNumber').val(ITEMS[index].contactPersonNumber);

                      setNumberInIntl( 'ContmobileNumber' , ITEMS[index].contactPersonNumber );

                      // SET COUNTRY , STATE , CITY IN LISTING
                      $('#custCountryId').val( ITEMS[index].countryId ).trigger("change");
                      $('#custCountryId').selectpicker('refresh');

                      $('#custAddress').val(ITEMS[index].custAddress);
                      $('#contPersonDesig').val(ITEMS[index].contPerDesig);
                      $('#contPersonEmail').val(ITEMS[index].contPerEmail);
 
                      $("#" + id).val(ITEMS[index].data);
                      $("#rateType").val(ITEMS[index].rateTypeId);
                      
                      GBL_SELECTEDCUSTID = ITEMS[index].id;
                      GBL_SELECTEDCUSTMOBNO = ITEMS[index].id+"|--|"+mobileNumber[1];
                      localStorage.POTGselectedCustIdInSearch = ITEMS[index].id;
                      //initCommonFunction();
                      callback(ITEMS[index]);
              }
            },1);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                GBL_SELECTEDCUSTID = '';
                GBL_SELECTEDCUSTMOBNO = '';
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3, 10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {

                        if( data.cust_id != "-1" ){

                            var group = {
                                id: data.cust_id,
                                // name: data.concatKey,
                                name: data.fname + " " + data.lname + "," + data.mobile,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname,
                                rateTypeId: data.rateTypeId,
                                custTypeId: data.custTypeId,
                                contactPersonName: data.contactPersonName,
                                email: data.email,
                                contactPersonNumber: data.contactPersonNumber,
                                stateId: data.stateId,
                                cityId: data.cityId,
                                custAddress: data.custAddress,
                                contPerDesig: data.contPerDesig,
                                contPerEmail: data.contPerEmail,
                                cpAllList: data.cpAllList,
                                catchArea : data.catchArea,
                                countryId : data.countryId,
                                phoneId : data.phoneId,
                                gstTinNo : data.gstTinNo
                            };
                            ITEMS.push(group);
                        }
                    });
              
                    return ITEMS;
                }

            }
        }
    });
}

function commonAutoCompleteForCustomerSearch(id,callback) {
    
    var searcCust = "";
    $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var mobileNumber = item.text.split(",");
            var index = findIndexByKeyValueAndCond(ITEMS, "id","mobile", item.value,mobileNumber[2]);
            
            if( item.value == "-2" ){
              
              $('#createCustFname').val('');
              $('#createCustMobile').val('+91');
              $('#createContMobile').val("+91");
              if( isNaN( searcCust ) ){
                $('#createCustFname').val(searcCust);
              }else{
                $('#createCustMobile').val(searcCust);
                $('#createContMobile').val(searcCust);
              }
              $('#createCustEmail').val("");
              $('#createCustAddress').val("");
              $('#createCustCountry').selectpicker('val',"-1");
              $('#createCustState').selectpicker('val',"-1");
              $('#createCustCity').selectpicker('val',"-1");
              $('#createContName').val("");
              $('#createContEmail').val("");
              $('#createContDesig').val("");

              $('#createCustomer').modal('show');
              $('#creatCustSear').val(''); 
              addFocusId( 'createCustFname' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

              initTelInput( 'createCustMobile' );   // INITIALIZE COUNTRY WISE MOBILE NUMD
              initTelInput( 'createContMobile' );   // INITIALIZE COUNTRY WISE MOBILE NUMD

            }else{

              localStorage.POTGCustDetailCustId = item.value;
              window.location.href = 'customerDetail.html';
            }

        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                searcCust = query;
                GBL_SELECTEDCUSTID = '';
                GBL_SELECTEDCUSTMOBNO = '';
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3, 10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                
                ITEMS = [{
                  id: "-2",
                  name: searcCust+" : Create Customer",
                  data: searcCust,
                }];

                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {

                        if( data.cust_id != "-1" ){

                            var group = {
                                id: data.cust_id,
                                // name: data.concatKey,
                                name: data.fname + " " + data.lname + " " + data.mobile,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname,
                                rateTypeId: data.rateTypeId,
                                custTypeId: data.custTypeId,
                                contactPersonName: data.contactPersonName,
                                email: data.email,
                                contactPersonNumber: data.contactPersonNumber,
                                stateId: data.stateId,
                                cityId: data.cityId,
                                custAddress: data.custAddress,
                                contPerDesig: data.contPerDesig,
                                contPerEmail: data.contPerEmail,
                                cpAllList: data.cpAllList,
                                catchArea: data.catchArea,
                            };
                            ITEMS.push(group);
                        }
                    });
                    $('#creatCustSear').removeClass('inputSearc');
                    return ITEMS;
                }

            }
        }
    });
}


// PROVIDE COMMON SEARCH FOR STUDENT MEMBERSHIP ID AND NAME
function autoCompleteForStudentId(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( index != -1 ){
                setTimeout(function() {
                    $('#'+id).val( item.text );
                    $('#'+id).attr( 'studId' , ITEMS[index].id );
                },10);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId:  checkAuth(11,45),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            var tempJson = {
                                id: data.studId,
                                name: data.studName + ' - ' + data.membershipNumber,
                                data: data.studName,
                                studentName : data.studName,
                                membershipNumber : data.membershipNumber,
                            }
                            ITEMS.push(tempJson);

                           
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}


function initCommonFunction(){
    
    if( localStorage.indeCampusAccessData == undefined || localStorage.indeCampusAccessData == "" ){
        window.location.href = "index.html";
        return false;
    }

    // Function created for load new version css and js starts here
    // versionControl();
    // Function created for load new version css and js ends here
    
    //Sets username in the header of the panel
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //CONDITION FOR ACITIVITY USER
    if( tmp_json.data[0].USERLOGINTYPE == "2" ){
        $('#module3').remove();   // FORCE FULLY REMOVED CUSTOMER LIST MODULE
        $('#headCustSearch').remove();
        $('.actUserHide').remove();
        $('#creatCustBtn').show();
    }else{
        $('#creatCustBtn').remove();
        $('.actUserHide').show();
      $('#headCustSearch').show();
    }

    // for Data only show gives check auth not -1 starts here
    $('.access').hide();
    var indeCampusAccessData = localStorage.indeCampusAccessData.split(',');
    for( var i=0; i<indeCampusAccessData.length; i++ ){
        $('#module'+indeCampusAccessData[i]).show();
        $('.module'+indeCampusAccessData[i]).show();

        // REMOVE CLASS
        $('.module'+indeCampusAccessData[i]).removeClass('accessCls');
        $('#module'+indeCampusAccessData[i]).removeClass('accessCls');
    }
    $('.accessCls').remove();
    //ends here

    if( tmp_json.data[0].PROFILE_IMAGE != "" && tmp_json.data[0].PROFILE_IMAGE != httpHead + "//"+FINALPATH+"/indeCampusProject/attachment/" ){
      $('#userProfile').attr('src',tmp_json.data[0].PROFILE_IMAGE);
    }else{
      $('#userProfile').attr('src',httpHead + "//"+FINALPATH+"/assets/globals/img/avtar1.png");
      // $('#profBtn').html('<span class="fa fa-bars text-primary"></span>');
    }

    var uname = ' ' + tmp_json.data[0].USERNAME + ' ';
    var fullUname = ' ' + tmp_json.data[0].FULLNAME + ' ';

    uname = uname.substring(0, 12);
    fullUname = fullUname.substring(0, 18);
    
    $('#fullUname').html(fullUname);
    $('#uname').html(uname +'....');
    //ends here

    $("input[type=text]").attr('autocomplete', 'off'); //Disable Auto Complete in whole system
    
    // it checks if user is developer or not and if developer give all rights to him / her
    var userType = localStorage.indeCampusUserType;
    if ( userType != '-1' ){
        $('.devPanel').remove();
        $('#uploadCustBtn').remove();
        $('#DownloadCustBtn').remove();
        $('#branchCrClientBtn').remove(); // CREATE CLIENT BTN IN CHANGE BRANCH MODAL
    }else{
        $('#branchCrClientBtn').show(); // CREATE CLIENT DISPLAY
        $('.devPanel').show();
        $('#uploadCustBtn').show();
        $('#DownloadCustBtn').show();
    }
    // ends here
    var branchListing = JSON.parse(localStorage.indeCampusRoleAuth);   
    var branchListing = branchListing.data.branchListing;

    if( branchListing.length < 2 ){
      $('.changeBranch').hide();
    }
    
    // var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    // if(  roleAuth.data.versionDetail != "" && roleAuth.data.versionDetail != "No record found"){
    //   $('#newVerContent').html( roleAuth.data.versionDetail );
    // }

   
    $('.mobMaxLenCls').attr('maxlength',MOBILE_LENGTH);   //SET MAX LENGTH INTO MOBILE NUMBER FIELDS

    // setTimeout(function(){
    //     // OPENS MODAL FOR CHANGE PASSWORD FORCE FULLY BY ADMIN / USER
    //     var branchIdForChkAuth = localStorage.indeCampusBranchIdForChkAuth; 
    //     if( roleAuth.data.changePassword == "0" && branchIdForChkAuth != undefined && branchIdForChkAuth != "" ){
    //         // $('#resetPasswordManageModal').modal('show');
    //         $('#resetPasswordManageModal').modal({
    //             backdrop: 'static',
    //             keyboard: false
    //         });
    //     }else{
    //         //Opens Version info pop up starts
    //         var branchIdForChkAuth = localStorage.indeCampusBranchIdForChkAuth;
    //         if((branchIdForChkAuth != undefined && branchIdForChkAuth != "")){ 
    //           if(  roleAuth.data.versionDetail != "" && roleAuth.data.versionDetail != "No record found" && userType != '-1' ){
    //             setTimeout(function(){
    //               if( roleAuth.data.versionViewed != "1" ){
    //                   $('#versionDetailModal').modal('show');
    //               }
    //             },100);
    //           }
    //         }
    //         //Opens Version info pop up ends
    //     }
    // },1000);

    toastr.options = {
      closeButton: true,
      preventDuplicates: true,
      newestOnTop: true,
    }; //Provide close button to toaster

    // it allow only number and plus symbol just place in input fields
    $('.onlyNumberWithDec').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 && e.which != 46 ) {
                   return false;
        }
    });

    // it allow only number and plus symbol just place in input fields
    $('.onlyNumber').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });

    // it allow only number and plus symbol just place in input fields
    $('input[type=number]').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });

    // it allow only text & number just place in input fields
    $('.onlyTextNumber').keypress(function (e) {
         if (e.which != 8 &&  e.which != 32 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 65 || e.which > 90) && (e.which < 97 || e.which > 122) ) {
                   return false;
        }
    });

    // IT WILL ALLOW ONLY NUMBERS WITH ONLY ONE DECIMAL POINT VALUE - ADDED ON 09-JULY-2018 BY VISHAKHA TAK
    $('.onlyNumberWithOneDec').keypress(function (e) {
        if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    // callCustomerAutoComplete('custSearch1', redirectPage);  //Customer search for call log 

    // IT WILL ALLOW ONLY TWO NUM AFTER DECIMAL POINT - ADDED ON 29-AUG-2018 BY VISHAKHA TAK
    // START
    $('.allowTwoValAfterDec').keypress(function(event) {
        var $this = $(this);
        if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
           ((event.which < 48 || event.which > 57) &&
           (event.which != 0 && event.which != 8))) {
               event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which == 46) && (text.indexOf('.') == -1)) {
            setTimeout(function() {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') != -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which != 0 && event.which != 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
                event.preventDefault();
        }      
    });
    // END

    // it allows letters and whitespaces only - ADDED ON 05-SEP-2018 BY VISHAKHA TAK
    $(".onlyText").keypress(function(event){
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
        }
    });

    var currUrl = document.URL;
    if( currUrl.search('proposaledit.html') == "-1" ){
      // $('body').niceScroll();
    }

    // change function of version info modal pop up starts here
    $('#neverShow').change(function(){
        if( $('#neverShow').is(':checked') ){
          var hideConfirm = confirm(fullUname+" are You sure? You dont wanna see this again.");
          if( hideConfirm ){
             hideModalApi();
          }else{
             $('#versionDetailModal').modal('hide'); 
          }  
        }
    });
    // change function of version info modal pop up ends here

    setCountryList( 'createCustCountry' );   // SET COUNTRY LISTING FROM LOAD AUTH SET DATA IN SELECT PICKER USING ID

    // $("#createCustCountry").selectpicker();    
    $("#createCustState").selectpicker();    
    $("#createCustCity").selectpicker(); 
    
    commonAutoCompleteForCustomerSearch("creatCustSear", navigateToCustPage);    

    $('#createCustMobile').blur(function(){
        $('#createContMobile').val( $(this).val() );
    });

    $('#createCustEmail').blur(function(){
        $('#createContEmail').val( $(this).val() );
    });

    $('#creatCustSear').focus(function(){
        $(this).val('');
    });

    $('#creatCustSear').keypress(function (e) {
      if( $('#creatCustSear').val().length >= 1 ){
        $('#creatCustSear').removeClass('inputSearc').addClass('inputSearc');
      }
    });
    // initialization for customer create for each page pop up ends here

    //Change Branch Pop up starts

    var currPage = $(location).attr('href').split('.html')[0].split(httpHead  + "//" + FINALPATH)[1];
    if( currPage != "/devloperclient" ){
      setTimeout(function(){
        changeBranchPopUpRender();
      },100);
    }

    //Change Branch Pop up ends

    // Notification code starts here
    if(typeof(EventSource) !== "undefined") {

        var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
        var userId = btoa( userDetail.data[0].PK_USER_ID );
        var clientId = btoa( userDetail.data[0].FK_CLIENT_ID );

        //WHEN ORG ID NOT AVAILABLE
        if( localStorage.indeCampusBranchIdForChkAuth == undefined || localStorage.indeCampusBranchIdForChkAuth == "" ){
            var roleAuthData = JSON.parse(localStorage.indeCampusRoleAuth);   
            var branchListing = roleAuthData.data.branchListing;
            if( localStorage.indeCampusUserType != "-1" ){
              localStorage.indeCampusBranchIdForChkAuth = branchListing[0]['orgId'];
            }
            
        }

        var orgId = btoa( localStorage.indeCampusBranchIdForChkAuth );

        var source = new EventSource( ALERTPATH + '?userId='+userId+'&&clientId='+ clientId +'&&orgId='+orgId);
        source.onmessage = function(event) {
            var tempEventAlertData = JSON.parse(event.data);
            //console.log(tempEventAlertData);
            eventAlertData = tempEventAlertData['alertData'];

            var totInqCnt = tempEventAlertData['totalInqCount'];
            if( parseInt( totInqCnt ) > 99 ){
                totInqCnt = "99+";
            }
            $('#notifActivityCount').html( totInqCnt );
            if( eventAlertData != 'No record found' ){
                
                //eventAlertData = JSON.parse(event.data);

                alertCount = 0;
                var indeCampusAlertNotifCount = 0; 
                var indeCampusAlerNotifText = '';
                for( var i=0; i<eventAlertData.length; i++ ){

                  // fetchMaxIndexFromNotfication(eventAlertData[i],0,localStorage.firebaseRegisterID); 

                  // if( userId == eventAlertData[i].ALERT_SUB_ID && eventAlertData[i].APP_NAME == localStorage.eposDeliveryAppName && eventAlertData[i].ASSIGNTO != eventAlertData[i].ALERT_SUB_ID ){
                  if( userDetail.data[0].PK_USER_ID != "-1" ){
                      if( eventAlertData[i].ASSIGNBY != eventAlertData[i].ASSIGNTO ){ 
                          if( eventAlertData[i].STATUS == 0 ){
                              alertCount++;
                              indeCampusAlertNotifCount++;
                              indeCampusAlerNotifText = eventAlertData[0].MESSAGE_TEXT;
                              localStorage.indeCampusAlerNotifTxt = indeCampusAlerNotifText;
                          }
                      }
                  }
                  // }
                }

                if( localStorage.indeCampusAlertNotifCnt == undefined ){
                    localStorage.indeCampusAlertNotifCnt = indeCampusAlertNotifCount;
                    if( indeCampusAlertNotifCount != 0 ){ 
                        toastr.success( localStorage.indeCampusAlerNotifTxt );
                    }
                }else if( localStorage.indeCampusAlertNotifCnt < indeCampusAlertNotifCount && localStorage.indeCampusAlertNotifCnt != 0 ){
                    toastr.success( localStorage.indeCampusAlerNotifTxt );
                    localStorage.indeCampusAlertNotifCnt = indeCampusAlertNotifCount;
                }

                if( parseInt( alertCount ) > 99 ){
                    alertCount = "99+";
                }
                $('#notifAlertCount').html( alertCount );
            }
        },
        source.onerror = function(event){
            // console.log(event);
        };
    }
    // Notification code ends here

    if( roleAuth.data.itemRateTypeFlag == "1" ){
        $('#createCustRateTypeDiv').show();
        setOption("0", "createCustRateType", roleAuth.data.itemRateType, "-- Rate Type --");
        $("#createCustRateType").selectpicker();
    }

    initHtmlEditor( 'ticketDesc' , '250' ); // INIT HTML EDITOR
    
    //POP OVER TRIGGER ON HOVER
    $('[data-toggle="popover"]').popover({
        trigger : 'hover'
    });
}

function hideModalApi(){

  var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
  // Followup Listing Todays, Delayed, Upcomming
  var postData = {
      requestCase: "hideVersionModal",
      clientId: tmp_json.data[0].FK_CLIENT_ID,
      userId: tmp_json.data[0].PK_USER_ID,
      dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
  };
  commonAjax(FOLLOWUPURL, postData, hideModalApiCallback,"Please Wait... Getting Dashboard Detail");

  function hideModalApiCallback(flag,data){

      if (data.status == "Success" && flag) {
          
          loadRoleAndAuth( "1" );   // PASS USER TYPE

          var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
          roleAuth.data.versionViewed = "1";

          $('#versionDetailModal').modal('hide'); 
      } else {
          if (flag)
              // toastr.error(data.status);
              displayAPIErrorMsg( data.status , GBL_ERR_HIDE_VERSION_FAILED );
          else
              toastr.error(SERVERERROR);
      }
  }
}

// sort on key values
function keysrt(key,desc) {
  return function(a,b){
   return desc ? ~~(a[key] < b[key]) : ~~(a[key] > b[key]);
  }
}


function getCallLogData(){

  var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
  // Followup Listing Todays, Delayed, Upcomming
  var postData = {
      requestCase: 'followupListing',
      clientId: tmp_json.data[0].FK_CLIENT_ID,
      userId: tmp_json.data[0].PK_USER_ID,
      orgId: checkAuth(26,102),
      dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
  }

  commonAjax(FOLLOWUPURL, postData, setFollowupData,"Please Wait... Getting Followup Detail");

  function setFollowupData(flag,data){

      if (data.status == "Success" && flag) {

          var callData = data.data.data.callLog;
          var callLogData = '';

          var noCallDataFound = '<div class="task *class* last">'+
                          '<div class="desc" style="width:100%;">'+
                          '<h4 style="text-align:center; ">No Call Logs Available</h4>'+
                          '</div><hr>'+
                          '</div></div>';
          if(callData == 'No record found' || callData.length == 0){
              callLogData = noCallDataFound;
          }else{
              for(var l=0;l<callData.length;l++){
                  
                  callLogSbActCnt = callLogSbActCnt + 1;
                  var callStatus = '';
                  var statusCall = '';

                  if( callData[l].callStatus == "MISSCALL" ){

                      callStatus = "missCall";
                      statusCall = "Missed Call";
                  }else{

                      callStatus = "receiveCall";
                      statusCall = "Recived Call";
                  }

                  var custName = '';
                  if( callData[l].customerName == 'No record found' ){
                      custName = 'hide';
                  }

                  callLogData += '<li id="callLogId_'+callData[l].callLogId+'" class="list-group-item"><div class="desc"><div class="title2">'+
                                   '<i class="fa fa-phone '+ callStatus +' tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="'+ statusCall +'"></i>&nbsp;'+
                                    '<span class="tooltips '+ custName +' text-muted" id="Name" title="" data-placement="top" data-toggle="tooltip" data-original-title="Name">'+ callData[l].customerName +'&nbsp; | &nbsp;</span>'+
                                    '<span class="tooltips" id="moNo" title="" data-placement="top" data-toggle="tooltip" data-original-title="Mobile No.">'+ callData[l].custPhoneNumber +'</span>'+
                                  '</div>'+
                                  '<div class="timeCall">'+
                                    '<div class="date tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Call Date/Time"><i class="fa fa-clock-o activitydelayed"></i>&nbsp;&nbsp;'+
                                     callData[l].durationStart + 
                                    '</div>'+
                                  '</div>'+
                                   '<div class="title">'+
                                    '<button type="button" href="javascript:void(0)" custId="'+ callData[l].customerId +'" callLogId="'+ callData[l].callLogId +'" onclick="getInquiryData(this);" data-toggle="modal" class="tooltips btn btn-call Primary-btn" title="" data-placement="top" data-toggle="tooltip" data-original-title="Take Follow Up">'+
                                      '<span class="callActionIcon" id="followUpAction">'+
                                        '<i class="fa fa-comment"></i>'+
                                      '</span>'+
                                      '</button>'+
                                      '<button type="button" href="javascript:void(0)" data-toggle="modal" class="tooltips btn btn-call" custId="'+ callData[l].customerId +'" custName="'+ callData[l].customerName +'" contNum="'+ callData[l].custPhoneNumber +'" callLogId="'+ callData[l].callLogId +'" onclick="createInqThroughLog(this)" title="" data-placement="top" data-toggle="tooltip" data-original-title="Create Inquiry">'+
                                      '<span class="callActionIcon" id="createInqAction">'+
                                        '<i class="fa fa-pencil-square-o"></i>'+
                                      '</span>'+
                                      '</button>'+
                                      '<button type="button" href="javascript:void(0)" data-toggle="modal" class="tooltips btn btn-call" onclick="addRemarkThroughLog(this)" callLogId="'+ callData[l].callLogId +'" title="" data-placement="top" data-toggle="tooltip" data-original-title="Do Nothing">'+
                                        '<span class="callActionIcon" id="remarkAction">'+
                                          '<i class="fa fa-warning"></i>'+
                                        '</span>'+
                                      '</button>'+
                                      '<button type="button" href="javascript:void(0)" data-toggle="modal" class="tooltips btn btn-call"onclick="createCustThroughLog(this)" callLogId="'+ callData[l].callLogId +'" title="" data-placement="top" data-toggle="tooltip" data-original-title="Create / Update User">'+
                                      '<span class="callActionIcon" id="createUser">'+
                                        '<i class="fa fa-plus"></i>'+
                                      '</span>'+
                                      '</button>'+
                                      '<button type="button" href="javascript:void(0)" data-toggle="modal" audioFile="'+ callData[l].recordingFile +'" callLogId="'+ callData[l].callLogId +'" class="tooltips btn btn-call" title="" data-placement="top" data-toggle="tooltip" data-original-title="Play Recording" onclick="playAudio(this)">'+
                                        '<span class="callActionIcon" id="recoding">'+
                                          '<i class="fa fa-volume-up"></i>'+
                                        '</span>'+
                                      '</button>'+
                                    '</div>'+
                                '</div>'+
                            '</li>';
              }
          }

          $("#callLogModel").html(callLogData);

          //static call log for test actions of cloud telephony
          
          // $("#callLogModel").html('<li id="callLogId_" class="list-group-item"><div class="desc"><div class="title2">'+
          //                          '<i class="fa fa-phone tooltips missCall" title="" data-placement="top" data-toggle="tooltip" data-original-title=""></i>&nbsp;'+
          //                           '<span class="tooltips" id="Name" title="" data-placement="top" data-toggle="tooltip" data-original-title="Name">Lithe Support &nbsp; | &nbsp;</span>'+
          //                           '<span class="tooltips" id="moNo" title="" data-placement="top" data-toggle="tooltip" data-original-title="Mobile No."> +919016801230 </span>'+
          //                         '</div>'+
          //                         '<div class="timeCall">'+
          //                           '<div class="date tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Duration"><i class="fa fa-clock-o activitydelayed"></i>&nbsp;&nbsp;'+
          //                            ' 03-05-2016 - 17:03:46 '+
          //                           '</div>'+
          //                         '</div>'+
          //                          '<div class="title">'+
          //                           '<button type="button" href="javascript:void(0)" custId="1" callLogId="" onclick="getInquiryData(this);" data-toggle="modal" class="tooltips btn btn-call Primary-btn" title="" data-placement="top" data-toggle="tooltip" data-original-title="Take Follow Up">'+
          //                             '<span class="callActionIcon" id="followUpAction">'+
          //                               '<i class="fa fa-comment"></i>'+
          //                             '</span>'+
          //                             '</button>'+
          //                             '<button type="button" href="javascript:void(0)" data-toggle="modal" class="tooltips btn btn-call" custId="" custName="" contNum="" callLogId="" onclick="createInqThroughLog(this)" title="" data-placement="top" data-toggle="tooltip" data-original-title="Create Inquiry">'+
          //                             '<span class="callActionIcon" id="createInqAction">'+
          //                               '<i class="fa fa-pencil-square-o"></i>'+
          //                             '</span>'+
          //                             '</button>'+
          //                             '<button type="button" href="javascript:void(0)" data-toggle="modal" class="tooltips btn btn-call" onclick="addRemarkThroughLog(this)" callLogId="" title="" data-placement="top" data-toggle="tooltip" data-original-title="Do Nothing">'+
          //                               '<span class="callActionIcon" id="remarkAction">'+
          //                                 '<i class="fa fa-warning"></i>'+
          //                               '</span>'+
          //                             '</button>'+
          //                             '<button type="button" href="javascript:void(0)" data-toggle="modal" class="tooltips btn btn-call"onclick="createCustThroughLog(this)" callLogId="" title="" data-placement="top" data-toggle="tooltip" data-original-title="Create / Update User">'+
          //                             '<span class="callActionIcon" id="createUser">'+
          //                               '<i class="fa fa-plus"></i>'+
          //                             '</span>'+
          //                             '</button>'+
          //                             '<button type="button" href="javascript:void(0)" data-toggle="modal" audioFile="" callLogId="" class="tooltips btn btn-call" title="" data-placement="top" data-toggle="tooltip" data-original-title="Play Recording" onclick="playAudio(this)">'+
          //                               '<span class="callActionIcon" id="recoding">'+
          //                                 '<i class="fa fa-volume-up"></i>'+
          //                               '</span>'+
          //                             '</button>'+
          //                           '</div>'+
          //                       '</div>'+
          //                   '</li>');

          // $('.modal-body.no-pad-lr').niceScroll();

          intdefaulttooltips();

      } else {
          if (flag)
              console.log(data.status);
          else
              console.log(SERVERERROR);
      }    
  }
}

var GBL_ACT_TYPE_KEY = "";
// getting all the followup data

function followupClicked(context){
    var id = $(context).attr("id");
    localStorage.POTGinqIdForFollowup = id;
    navigateToInquiryDetail(this);
}

function navigateToInquiryEdit(){
    
    if (checkAuth(4, 15, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "inquiryEdit.html";
    }
}

function navigateToProposalEdit(){

    if (checkAuth(15, 59, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "proposaledit.html";
    }
}

function navigateToDashboard(){

    if (checkAuth(1, 2, 1) == -1) {
        toastr.error(NOAUTH);
        navigateToIndex();
    }
    else {
        window.location.href = "home.html";
    }
}

function navigateToeTrack(){
    
    if (checkAuth(5, 18, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "eTrack.html";
    }
}

function navigateToInquiryDetail(context,type){
    
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        var inqId = $(context).attr('inqId');
        var custId = $(context).attr('custId');
        
        localStorage.POTGclickInqId = inqId;
        localStorage.POTGcustId = custId;
        if(type == "2"){ //order

          navigateToOrderDetailOnly();

        }else if( type == "3" ){ //service

           localStorage.POTGclickServiceId = inqId;
           navigateToServiceDetailOnly();

        }else if( type == "4" ){ //PROJECT

           navigateToProjectActivity();

        }else if( type == "5" ){ //CAMPAIGN

          localStorage.selectCampaignFromCampList = inqId;
            navigateToCampaignReportOpen();
       
        }else if( type == "6" ){ //LEAD

          localStorage.POTGclickLeadId = inqId;
           navigateToLeadDetailOnly();

        }else{

          navigateToInquiryDetailOnly();
        }
    }
}

function navigateToInquiryDetailOnly(){
    
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        
        window.location.href = "inquirydetail.html";
    }
}

function navigateToInquiryDetailOpenWin(context , type){
    
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        var inqId = $(context).attr('inqId');
        var custId = $(context).attr('custId');
        
        localStorage.POTGclickInqId = inqId;
        localStorage.POTGcustId = custId;
        
        if( type == "1" ){
          localStorage.POTGredirectProp = "1";
        }
        window.open("inquirydetail.html");
        // window.location.href = "inquirydetail.html";
    }
}


function navigateToProposalList(context){
    
    if (checkAuth(15, 58, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "proposal.html";
    }
}

function navigateToactivity(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "activity.html";
    }
}

function navigateToInquiryCalender(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "inquiryCalender.html";
    }
}

function navigateToactivityOpen(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.open( "activity.html" );
    }
}

function navigateToStudent(){

    if (checkAuth(3, 10, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "student.html";
    }   
}

function navigateToCustomerDetailPage(context){

    if (checkAuth(3, 10, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGCustDetailCustId = $(context).attr('custId');
        window.open('customerDetail.html');
    }   
}

function navigateToInquiry(){

    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "inquiry.html";
        localStorage.removeItem('POTGinqIdForStallBook');
    }     
}

function navigateToQuickInquiry(){

    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "quickInquiry.html";
    }     
}

function navigateToInquiryReport( statuId ){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        if( statuId != "" ){
          localStorage.POTGcurrentStatusId = statuId;
        }
        window.location.href = "inquiryReport.html";
    }     
}

function navigateToInquiryAllocationReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "inquiryAllocationReport.html";
    }     
}

function navigateToActSummaryReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "actSummaryReport.html";
    }     
}

function navigateToSalesTargetReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "salesTargetReport.html";
    }     
}

function navigateToPaymentReport(){

    // if (checkAuth(31, 126, 1) == -1) {
    //     toastr.error(NOAUTH);
    // }
    // else {
        window.location.href = "paymentReport.html";
    // }     
}

function navigateToActivityTargetReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "activityTargetReport.html";
    }     
}

function navigateToActivityMainReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "activityMainReport.html";
    }     
}

function navigateToActivityMainReportOpen(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.open( "activityMainReport.html" );
    }     
}

function navigateToActivityProjectReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "activityProjectReport.html";
    }     
}

function navigateToActivityProjectGraphicalReport(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "activityProjectPivotReport.html";
    }     
}

function navigateToActivityProjectReportOpen(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
         window.open( "activityProjectReport.html" );
    }     
}

function navigateToActivitReportWithFilter(){

    if (checkAuth(31, 126, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        localStorage.loadMoreCompletedAct = "1";
        window.location.href = "activityMainReport.html";
    }     
}

function navigateToTracking(){

    if (checkAuth(5, 18, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "tracking.html";
    }      
}


function navigateToTarget(){

    if (checkAuth(6, 22, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "target.html";
    } 
}

function navigateToScheduleActivity(){

    if (checkAuth(25, 98, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "scheduleActivity.html";
    }      
}

function navigateToSmartActivity(){

    if (checkAuth(25, 98, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "smartActivity.html";
    }      
}

function navigateToSmartActNewTab(){

    if (checkAuth(25, 98, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.open("smartActivity.html");
    }      
}

function navigateToCreateInquiry(){
  
    if (checkAuth(4, 13, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "inquiryCreate.html";
    }        
}

function navigateToCreateServiceInquiry(){
  
    if (checkAuth(49, 194, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "serviceInquiryCreate.html";
    }        
}

function navigateToCreateLeadInquiry(){
  
    if (checkAuth(53, 220, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "leadInquiryCreate.html";
    }        
}

function navigateToType(type){
  
    if (checkAuth(6, 22, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.indeCampusLookupType = type; 
        window.location.href = "devlopertype.html";
    }        
}

function navigateTolookup(type){
  
    if (checkAuth(7, 26, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.indeCampusLookupType = type; 
        window.location.href = "devloperlookup.html";
    }        
}


function navigateToStatusCategory(type){
  
    if (checkAuth(10, 41, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.indeCampusLookupType = type; 
        window.location.href = "statusCategory.html";
    }        
}


function navigateToclientMgmt(type){
  
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.indeCampusClientCreateType = type;
        window.location.href = "devloperclient.html";
    }        
}

function navigateToclientList(){
  
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "devloperclientList.html";
    }        
}

function navigateToBranchMgmt(){
  
    if (checkAuth(5, 18, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "devloperbranch.html";
    }        
}

function navigateToEmailConfig(){
  
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "emailConfig.html";
    }        
}

function navigateToSMSConfig(){
  
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "smsConfig.html";
    }        
}

function navigateToClientConfig(){
  
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clientConfig.html";
    }        
}

function navigateToSmartSchedulerAct(){
  
    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "smartSchedulerActivity.html";
    }        
}

function navigateToUOMMgmt(){
  
    if (checkAuth(9, 34, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "uomManagement.html";
    }        
}

function navigateToDeviceMgmt(){
  
    if (checkAuth(11, 42, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "deviceManagement.html";
    }        
}

function navigateToTeamMgmt(){
  
    if (checkAuth(12, 46, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clientteam.html";
    }        
}

function navigateToLoginHistory(){
  
    // if (checkAuth(12, 46, 1) == -1) {
    //     toastr.error(NOAUTH);
    // }
    // else {
        window.location.href = "loginHistory.html";
    // }        
}

function navigateToItemGrp(){
  
    if (checkAuth(13, 50, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clientiteamgroup.html";
    }        
}

function navigateToItem(type){
  
    if (checkAuth(15, 62, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
      localStorage.indeCampusAddItemType = type;
      window.location.href = "clientiteammas.html";
    }        
}

function navigateToClientProposal(){
  
    if (checkAuth(15, 58, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clientproposal.html";
    }        
}

function navigateToStatus(){
  
    if (checkAuth(10, 41, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clientstatuscat.html";
    }        
}

function navigateToCustGroup(){
  
    if (checkAuth(17, 66, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "customergroup.html";
    }        
}

function navigateToDocMgmt(){
  
    if (checkAuth(18, 70, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "documentManagement.html";
    }        
}

function navigateToWorkFlow(){
  
    if (checkAuth(27, 113, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "workflow.html";
    }        
}

function navigateToSalesBOM(){
  
    if (checkAuth(9, 34, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "salesbom.html";
    }        
}

function navigateToClientTarget(){
  
    if (checkAuth(6, 22, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clienttarget.html";
    }        
}

function navigateToActivityTarget(){
  
    if (checkAuth(6, 22, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "activityTarget.html";
    }        
}

function navigateToUser(){
  
    if (checkAuth(8, 30, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "usermanaguser.html";
    }        
}

function navigateToRole(){
  
    if (checkAuth(9, 37, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "usermanagrole.html";
    }        
}

function navigateToModule(){
  
    if (checkAuth(2, 5, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "usermanagmodule.html";
    }        
}

function navigateToPermission(){
  
    if (checkAuth(3, 10, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "usermanagpermission.html";
    }        
}

function navigateToUserGrp(){
  
    if (checkAuth(23, 90, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "usergroup.html";
    }        
}

function navigateToUserLevel(){
  
    if (checkAuth(8, 30, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "userlevel.html";
    }        
}

function navigateToCreateUser(){
  
    if (checkAuth(8, 29, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "adduser.html";
    }        
}

function navigateToWhy(){
    
    if (checkAuth(28, 116, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "why.html";
    }
}

function navigateToReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "report.html";
    }
}

function navigateToEnrollReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "enrollmentReport.html";
    }
}

function navigateToRevenueReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "revenueReport.html";
    }
}

function navigateToRequestReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "requestReport.html";
    }
}

function navigateToComplaintReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "complaintReport.html";
    }
}

function navigateToSuspensionReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "suspensionReport.html";
    }
}

function navigateToTerminationReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "terminationReport.html";
    }
}

function navigateToRentalSpaceReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "rentalSpaceInquiryReport.html";
    }
}

function navigateToEventMgmtReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "eventManagementReport.html";
    }
}

function navigateToRoomOccuReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "roomOccupancyReport.html";
    }
}

// REDIRECT TO REQUEST & COMPLAINT SUMMARY REPORT
function navigateToReqComSummaryReport(){
    
    if (checkAuth(26, 105, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "requestComplaintSummary.html";
    }
}

function navigateToMasterDeveloper(){
    
    window.location.href = "masterdevloper.html";
}

function navigateToDashboardMasterCompo(){
    
    if (checkAuth(1, 2, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "dashboardMasterCompo.html";
    }
}

function navigateToMasterUpload(){
    
    if (checkAuth(3, 10, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "fileUploadClient.html";
    }
}

function navigateToDashboardPreference(){
    
    if (checkAuth(1, 2, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "settings.html";
    }
}

function navigateToClientMaster(){

    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "clientmaster.html";
    }
}

function navigateToUserManagement(){
    
    if (checkAuth(8, 30, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "usermanagment.html";
    }
}

function navigateToProfile(){
    
    window.location.href = "profile.html";
}

function navigateToAddUser(){
    
    if (checkAuth(8, 29, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "adduser.html";
    }
}

function navigateToProposalPreview(context){
    
    if (checkAuth(15, 58, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGTemplatePropId = $(context).attr('propid');
        window.open("proposalTemplate.html");
        // window.location.href = "proposalTemplate.html";
    }
}

function navigateToVersionInfo(){
    
    if (checkAuth(9, 34, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "versionInfo.html";
    }
}

function navigateToGreeting(){
    // if (checkAuth(21, 82, 1) == -1) {
    //     toastr.error(NOAUTH);
    // }
    // else {
      window.location.href = "greeting.html";
    // }
}

function navigateToProposalPage(){
  if (checkAuth(15, 58, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "proposal.html";
  }
}

function navigateToProposalReport(){
  if (checkAuth(31, 126, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "proposalReport.html";
  }
}

function navigateToDynamicProposalTemplateList(){
  // if (checkAuth(31, 126, 1) == -1) {
  //     toastr.error(NOAUTH);
  // }
  // else { 
    window.location.href = "dynamicProposalTemplate.html";
  // }
}

function navigateToDynamicTemplateCreation( templateId ){
  // if (checkAuth(31, 126, 1) == -1) {
  //     toastr.error(NOAUTH);
  // }
  // else {
    localStorage.selectedDynamicTemplate = templateId; 
    window.location.href = "dynamicTemplateCreate.html";
  // }
}

function navigateToProposal(){
  if (checkAuth(15, 58, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "proposal.html";
  }
}

function navigateToHelp(){
  
  window.open(httpHead + "//help.proposalotg.com");
}



function navigateToEditUser(context){

    if (checkAuth(8, 31, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
      
        var userId = $(context).attr("data-userId");
        localStorage.indeCampusUserIdForEditUser = userId;
        
        window.location.href = "edituser.html";
    }
}

function navigateToActivityDetailsOpen(elem){

    if( checkAuth(2, 6, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    var activityId = $(elem).attr('data-activityId');
    if (!activityId){
        toastr.warning("No Activity data found to proceed");
        return false;
    }
    localStorage.POTGactivityAppActivityId = activityId;
    window.open('activitydetail.html');
}

function navigateToActivityAppDetailsPage(elem){


    if( checkAuth(2, 6, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var activityId = $(elem).attr('data-activityId');
    if (!activityId){
        toastr.warning("No Activity data found to proceed");
        return false;
    }
    localStorage.POTGactivityAppActivityId = activityId;
    window.location.href = 'activitydetail.html';
}

// REDIRECT TO ENROLLMENT MASTER
function navigateToEnrollmentMaster(){
  if (checkAuth(11, 45, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "studentEnrollment.html";
  }
}

// REDIRECT TO CREATE ENROLLMENT
function navigateToCreateEnrollment(type){
  if (checkAuth(11, 44, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    localStorage.indeCampusEnrollType = type;
    window.location.href = "createEnrollment.html";
  }
}

// REDIRECT TO CREATE ENROLLMENT
function navigateToStudentAllotment(){
  if (checkAuth(11, 44, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "studentAllotment.html";
  }
}

// REDIRECT TO CREATE ENROLLMENT
function navigateToPropertyMaster(){
  if (checkAuth(18, 74, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "propertyMaster.html";
  }
}

// REDIRECT TO CREATE ENROLLMENT
function navigateToTimezoneMaster(){
  if (checkAuth(18, 74, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "timezoneMaster.html";
  }
}

function navigateToEnrollmentInvoice(){
  
  if (checkAuth(11, 44, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "createEnrollmentInvoice.html";
  }
  
}

function navigateToChargePosting(){
  
  if (checkAuth(24, 98, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "chargePosting.html";
  }
  
}

function navigateToChargePostListing(){
  
  if (checkAuth(24, 98, 1) == -1) {
      toastr.error(NOAUTH);
  }
  else {
    window.location.href = "chargePostListing.html";
  }
  
}

function navigateToItemMaster(){
  
  if (checkAuth(15, 62, 1) == -1) {
        toastr.error(NOAUTH);
    }
  else {
      window.location.href = "itemmaster.html";
  }
  
}

function navigateToRoomMaster(){
  
  if (checkAuth(15, 62, 1) == -1) {
        toastr.error(NOAUTH);
    }
  else {
      window.location.href = "roommaster.html";

  }
  
}

function navigateToMasterData(){
  
  if (checkAuth(1, 2, 1) == -1) {
        toastr.error(NOAUTH);
    }
  else {
      window.location.href = "masterdata.html";
  }
  
}

function navigateToEventManagementList(){
  
  if (checkAuth(21, 86, 1) == -1) {
        toastr.error(NOAUTH);
    }
  else {
      window.location.href = "eventManagementList.html";
  }
  
}

function navigateToEventView(){
  
  if (checkAuth(21, 86, 1) == -1) {
        toastr.error(NOAUTH);
    }
  else {
      window.location.href = "eventManagementView.html";
  }
  
}

function navigateToCreateReqComplaint(){
  
  if (checkAuth(22, 89, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "RequestComplainCreate.html";
  }
  
}

function navigateToReqComplaintList(){
  
  if (checkAuth(22, 90, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "RequestComplain.html";
  }
  
}

function navigateToDepartmentCategory(){
  
  if (checkAuth(11, 46, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "departmentCategoryMaster.html";
  }
  
}

function navigateToGeneralInvoice(){
  
  if (checkAuth(29, 113, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "generalInvoice.html";
  }
  
}

function navigateToCreateSuspension(){
  
  if (checkAuth(25, 101, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "suspensionTerminationCreate.html";
  }
  
}

function navigateToSuspensionTermination(){
  
  if (checkAuth(25, 102, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "suspensionTermination.html";
  }
  
}

function navigateToPageContentMgmt(){
  
  if (checkAuth(14, 58, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "pageContent.html";
  }
  
}

function navigateToNonAvailServices(){
  
  if (checkAuth(27, 108, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "nonAvailingService.html";
  }
  
}

function navigateToCreateReceipt(){
  
  if (checkAuth(23, 94, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "createreceipt.html";
  }
  
}

function navigateToCreateInvoice(){
  
  if (checkAuth(23, 94, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "createinvoice.html";
  }
  
}

function navigateToNonProcessCharges(){
  
  if (checkAuth(24, 98, 1) == -1) {
        toastr.error(NOAUTH);
  }
  else {
      window.location.href = "NonProcessCharge.html";
  }
  
}

/*Move screen to dashboard*/
function navigateToIndex() {

   var deviceName = localStorage.indeCampusDeviceName;
   var indeCampusDeviceId = localStorage.indeCampusDeviceId;
   var deviceactivateDate = localStorage.indeCampusDeviceactivateDate;
   var deviceType = localStorage.indeCampusDeviceType;
   var alertKey = localStorage.indeCampusAlertKey;
   var indeCampusAppName = localStorage.indeCampusAppName;
   var callAcceptFlag = localStorage.indeCampusCallAcceptFlag;
   var calllogId = localStorage.indeCampusCalllogId;
   var selectedCustIdForCall = localStorage.indeCampusSelectedCustIdForCall;
   var indeCampusAlertNotifCnt = localStorage.indeCampusAlertNotifCnt;
   var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
   
   // localStorage.clear();  

   clearPOTGlocalStorage();
   removeDataTableLogLocalStorage('indecampus');

   if(tmp_json.data[0].USER_TYPE != -1){ //IF USER TYPE NOT -1 THEN ONLY STORE DEVICE ID,NAME AND DATE BY VAIBHAV
        
        if( deviceType == 1 ){
          localStorage.indeCampusDeviceId = indeCampusDeviceId;
          localStorage.indeCampusDeviceName = deviceName;
          localStorage.indeCampusDeviceType = deviceType;
          localStorage.indeCampusDeviceactivateDate = deviceactivateDate;
          localStorage.indeCampusAlertKey = alertKey;
          localStorage.indeCampusAppName = indeCampusAppName;
          
          //COMMENTED BECAUSE USED IN FUTURE FOR CLOUD TELEPHONY USE

          // localStorage.indeCampusCallAcceptFlag = callAcceptFlag;
          // localStorage.indeCampusCalllogId = calllogId;
          // localStorage.indeCampusSelectedCustIdForCall = selectedCustIdForCall;
          localStorage.indeCampusAlertNotifCnt = indeCampusAlertNotifCnt;
        }
   }
   localStorage.indeCampusLoadPage = "true";
   
   window.location.href = 'index.html';
}

function clearPOTGlocalStorage(){

  localStorage.removeItem('indeCampusAlerNotifText');
  localStorage.removeItem('indeCampusLookupType');
  localStorage.removeItem('indeCampusAccessData');
  localStorage.removeItem('loadProposalTab');
  localStorage.removeItem('indeCampusAlertNotifCnt');
  localStorage.removeItem('indeCampusVersion');
  localStorage.removeItem('ProfilePic');
  localStorage.removeItem('POTGattachList');
  localStorage.removeItem('POTGactivityAppActivityId');
  localStorage.removeItem('POTGactivityAppAssignTypeGroupOptionHtml');
  localStorage.removeItem('POTGactivityAppAssignTypeUserOptionHtml');
  localStorage.removeItem('indeCampusActivityAppClientId');
  localStorage.removeItem('POTGactivityAppCustGrpJson');
  localStorage.removeItem('POTGactivityAppUserGrpJson');
  localStorage.removeItem('indeCampusActivityAppUserId'); // REMOVE ACT USER ID
  localStorage.removeItem('POTGactivityAppUserListJson');
  localStorage.removeItem('indeCampusAlertKey');
  localStorage.removeItem('indeCampusAppName');
  localStorage.removeItem('indeCampusBranchIdForChkAuth');
  localStorage.removeItem('indeCampusCallAcceptFlag');
  localStorage.removeItem('indeCampusCalllogId');
  localStorage.removeItem('POTGclickInqId');
  localStorage.removeItem('POTGclickServiceId');
  localStorage.removeItem('POTGcurrentTime');
  localStorage.removeItem('POTGcustId');
  localStorage.removeItem('indeCampusDashVersion');
  localStorage.removeItem('indeCampusDateFormat');
  localStorage.removeItem('indeCampusDeviceId');
  localStorage.removeItem('indeCampusDeviceName');
  localStorage.removeItem('indeCampusDeviceType');
  localStorage.removeItem('indeCampusDeviceactivateDate');
  localStorage.removeItem('POTGinqStatus');
  localStorage.removeItem('indeCampusRoleFlag');
  localStorage.removeItem('POTGinqInfoForProposal');
  localStorage.removeItem('POTGinqSourceForQuick');
  localStorage.removeItem('POTGserviceStatus');
  localStorage.removeItem('POTGinqTypeForQuick');
  localStorage.removeItem('POTGlastProposalId');
  localStorage.removeItem('indeCampusLoadPage');
  localStorage.removeItem('POTGquickGroupList');
  localStorage.removeItem('POTGquickItemList');
  localStorage.removeItem('indeCampusRoleAuth');
  localStorage.removeItem('indeCampusRoleIdForEdit');
  localStorage.removeItem('indeCampusSelectedCustIdForCall');
  localStorage.removeItem('POTGselectedCustIdInSearch');
  localStorage.removeItem('indeCampusUserDetail');
  localStorage.removeItem('indeCampusUserIdForEditUser');
  localStorage.removeItem('POTGuserTargetDetail');
  localStorage.removeItem('POTGuserType');
  localStorage.removeItem('POTGTotalInqCount');
  localStorage.removeItem('POTGinqIdOpenPop');
  localStorage.removeItem('POTGInqToWhyInqStat');
  localStorage.removeItem('POTGInqToWhyActMiss');
  localStorage.removeItem('POTGInqToWhyActstDate');
  localStorage.removeItem('POTGInqToWhyActenDate');
  localStorage.removeItem('POTGInqToWhyActUserTxt');
  localStorage.removeItem('POTGInqToWhyActUser');
  localStorage.removeItem('POTGInqToWhyActstatType');
  localStorage.removeItem('POTGgetItem');
  localStorage.removeItem('POTGtempActData');
  localStorage.removeItem('POTGactType');
  localStorage.removeItem('POTGactivityAppIP');
  localStorage.removeItem('POTGopenTblMenu');
  localStorage.removeItem('POTGalterB');
  localStorage.removeItem('POTGpopupflag');
  localStorage.removeItem('POTGsideMenu');
  localStorage.removeItem('POTGorderMenuPopupflag');
  localStorage.removeItem('POTGorderIdForEdit');
  localStorage.removeItem('POTGitemsPanel');
  localStorage.removeItem('POTGcustPanel');
  localStorage.removeItem('POTGbottomMenuflag');
  localStorage.removeItem('POTGupdateTargetId');
  localStorage.removeItem('POTGClientImage');
  localStorage.removeItem('POTGinqIdForStallBook');
  localStorage.removeItem('POTGserviceIdForStallBook');
  localStorage.removeItem('exhibitionId');
  localStorage.removeItem('POTGcallLogSeacrhCustName');
  localStorage.removeItem('POTGcallLogAction');
  localStorage.removeItem('POTGbranchIdForUpdate');
  localStorage.removeItem('POTGeTRACKDate');
  localStorage.removeItem('POTGeTRACKUname');
  localStorage.removeItem('POTGeTRACKUId');
  localStorage.removeItem('POTGtaxDiscDetail');
  localStorage.removeItem('POTGdocumentList');
  localStorage.removeItem('POTGitemdocumentList');
  localStorage.removeItem('POTGtemplateId');
  localStorage.removeItem('POTGitemList');
  localStorage.removeItem('POTGdocumentListHtml');
  localStorage.removeItem('POTGitemdocumentListHtml');
  localStorage.removeItem('POTGtemplateListHtml');
  localStorage.removeItem('POTGtaxDiscDetail');
  localStorage.removeItem('POTGitemListHtml');
  localStorage.removeItem('POTGlastProposalArrIndex');
  localStorage.removeItem('POTGstatusIdForUpdate');
  localStorage.removeItem('POTGInqToWhyUnameId');
  localStorage.removeItem('POTGInqToWhyUname');
  localStorage.removeItem('POTGInqToWhyStatus');
  localStorage.removeItem('POTGInqToWhyActstatType');
  localStorage.removeItem('POTGTemplatePropId');
  localStorage.removeItem('indeCampusStudentView');
  localStorage.removeItem('projectId');
  localStorage.removeItem('projectInqId');
  localStorage.removeItem('lastSelectedInqFields');
  localStorage.removeItem('serviceLastSelectedInqFields');
  localStorage.removeItem('loadMoreCompletedAct');
  localStorage.removeItem('POTGclickLeadId');
  localStorage.removeItem('selectedDynamicReport');
  localStorage.removeItem('selectedDynamicRefReport');
  localStorage.removeItem('selectedDynamicReportMode');
  localStorage.removeItem('selectedDynamicReportType');
  localStorage.removeItem('displayTeamView');
  localStorage.removeItem('selectedDynamicTemplate');
  localStorage.removeItem('selectedTemplateData');
  localStorage.removeItem('proposalTemplateDispTrans');
  localStorage.removeItem('inqDetGetData');
  localStorage.removeItem('POTGclientData');
  localStorage.removeItem('actCreateRefType');
  localStorage.removeItem('actReqInqId');
  localStorage.removeItem('indeCampusAddItemType'); // REMOVE ITEM TYPE
  localStorage.removeItem('indeCampusRoomDetail'); // REMOVE ITEM ROOM DETAIL
  localStorage.removeItem('indeCampusEnrollType'); // REMOVE CREATE/UPDATE ENROLL TYPE
  localStorage.removeItem('indeCampusMyNotif');
}

//START FUNCTION FOR OPEN CHANGE BRANCH POP UP ON CLICK OF CHANGE BRANCH IN HEADER BY VAIBHAV
function openChangeBranchPopUp(){
    var filter = 1;
    changeBranchPopUpRender(filter);
}
//END FUNCTION FOR OPEN CHANGE BRANCH POP UP ON CLICK OF CHANGE BRANCH IN HEADER BY VAIBHAV

//START FUNCTION FOR GENERATE CHANGE BRANCH POP UP DATA AND SELECTED VALUE SET BY VAIBHAV
function changeBranchPopUpRender(filter) {
    var branchListing1 = JSON.parse(localStorage.indeCampusRoleAuth);   
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var branchListing = branchListing1.data.branchListing;
    var clientListing = branchListing1.data.clientListing;
    var branchIdForChkAuth = localStorage.indeCampusBranchIdForChkAuth;
    if((branchIdForChkAuth== undefined || branchIdForChkAuth=="")){
        branchIdForChkAuth="";
    }
    

    var clientIdSelected = tmp_json.data[0].FK_CLIENT_ID;
    // log("branch id in chengebranch pop up : "+branchIdForChkAuth);

    
    if(branchListing.length>1 && (filter ==1 || branchIdForChkAuth=="")){
        var branchOption = "<option value=''>Select Branch</option>";
        var clientOption = "";
        if(branchListing != "No record found"){
            for(var i=0;i<branchListing.length;i++){
                var branchId = branchListing[i]['orgId'];
                var branchclientId = branchListing[i]['clientId']; 
                // log(clientIdSelected+"==="+branchclientId);
                if(clientIdSelected == branchclientId){            
                    var branchName = branchListing[i]['orgName'];
                    branchOption +="<option value='"+branchId+"'>"+branchName+"</option>";
                }
            }
        }
        var clientOption = "<option value=''>Select Client</option>";
        if(clientListing != "No record found"){
            for(var j=0;j<clientListing.length;j++){
                var clientId = clientListing[j]['clientId'];
                var clientName = clientListing[j]['clientName'];
                clientOption +="<option value='"+clientId+"'>"+clientName+"</option>";
            }
        }
        // console.log(branchOption);
        $("#branchListForChangeBranch").html(branchOption);
        $("#branchListForChangeBranch").selectpicker();
        $("#branchListForChangeBranch").val(branchIdForChkAuth).selectpicker("refresh");
        
        $("#clientListForChangeBranch").html(clientOption);
        $("#clientListForChangeBranch").val(clientIdSelected);
        $("#clientListForChangeBranch").selectpicker();
        //$("#clientNameForChangeBranch").html(clientNameForChangeBranch);
        
        var openHtml = "<input type='button' data-div='changebranch'></input>";
        //openModal(openHtml);
        $('#changebranch').modal({
          backdrop: 'static'
        });

        addFocusId( 'clientListForChangeBranch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }else{
        if(branchIdForChkAuth==""){
            localStorage.indeCampusBranchIdForChkAuth = branchListing[0]['orgId'];
            getDashboardData();
        }        
    }

    $('#changebranch').addClass('overflow-hidden');
    
    $('.overlay').removeClass('active');
    $('.nav-menu').removeClass('active');
    $('.menu-layer').removeClass('active');
}
//END FUNCTION FOR GENERATE CHANGE BRANCH POP UP DATA AND SELECTED VALUE SET BY VAIBHAV

//START FUNCTION FOR GENERATE BRANCH LISTING IN CHANGE BRANCH POP UP ON CHANGE OF CLIENT BY VAIBHAV
function onChangeClient(){
    
    var branchListing1 = JSON.parse(localStorage.indeCampusRoleAuth);      
    
    $("#branchListForChangeBranch").selectpicker("refresh");    
    var branchListing = branchListing1.data.branchListing;    
    var eID = document.getElementById("clientListForChangeBranch"); 
    //var clientId = eID.options[eID.selectedIndex].value;  
    //$("#clientListForChangeBranch");
    var clientId = $("#clientListForChangeBranch").val();    
    var branchOption = "<option value=''>Select Branch</option>";
    log("CLient ID:"+clientId);
    log(branchListing);
    if(branchListing != "No record found"){
        for(var i=0;i<branchListing.length;i++){
            var branchId = branchListing[i]['orgId'];
            var branchclientId = branchListing[i]['clientId'];
            
            if(clientId == branchclientId){            
                var branchName = branchListing[i]['orgName'];
                branchOption +="<option value='"+branchId+"'>"+branchName+"</option>";
            }
        }
    }
    $("#branchListForChangeBranch").html(branchOption);
    $("#branchListForChangeBranch").selectpicker("refresh");
}
//END FUNCTION FOR GENERATE BRANCH LISTING IN CHANGE BRANCH POP UP ON CHANGE OF CLIENT  


//START FUCNTION FOR CHANGE BRANCH ON CLICK OF SAVE CHANGES OF CHANGE BRANCH POP UP BY VAIBHAV
function changebranch(){
    var branchId = $("#branchListForChangeBranch").val();
    if(branchId==""){
        toastr.warning("Please Select Atleast One Branch.");
        return false;
    }else{
        //var clientId = $("#clientListForChangeBranch").val();
        var eID = document.getElementById("clientListForChangeBranch"); 
        var clientId = eID.options[eID.selectedIndex].value; 
        var clientName = eID.options[eID.selectedIndex].text;
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        tmp_json.data[0].FK_CLIENT_ID = clientId;
        roleAuth.data.FK_CLIENT_ID = clientId;
        roleAuth.data.clientName = clientName;


        var clientIdBranch = $('#clientListForChangeBranch').val();

        // Updated by Nazim starts here

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase : "loadauth",
            userId : tmp_json.data[0].PK_USER_ID,
            clientId : clientIdBranch,
            orgId : localStorage.indeCampusBranchIdForChkAuth,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
        commonAjax(COMMONURL,postData, 
            function(a,b){
                //log(b); return false;
                if(b.status==SCS){
                    localStorage.indeCampusRoleAuth = JSON.stringify(b);
                    
                    tmp_json.data[0].FK_CLIENT_ID = clientId;
                    
                    localStorage.indeCampusUserDetail = JSON.stringify(tmp_json);
                    // localStorage.indeCampusRoleAuth = JSON.stringify(roleAuth);
                    log("branch id selected : "+branchId);
                    localStorage.indeCampusBranchIdForChkAuth = branchId;
                    if(tmp_json.data[0].USER_TYPE == -1){ //IF USER TYPE NOT -1 THEN ONLY STORE DEVICE ID,NAME AND DATE BY VAIBHAV
                        loadRoleAndAuth();  // USER TYPE EMPTY
                        setTimeout(function() {
                            reloadWin();
                        },5000);
                    }else{
                        setTimeout(function(){
                            var POTGaccessData = "";   //Module Accessability
                            // var accessIds = [[1,2],[2,6],[3,10],[4,14],[5,18],[6,22],[7,26],[8,30],[9,34],[10,38],[11,42],[12,46],[13,50],[14,54],[15,58],[16,62],[17,66],[18,70],[19,74],[20,78],[21,82],[22,86],[23,90],[24,94],[25,98],[26,102],[27,113],[28,116],[29,122],[30,123],[31,126]];
                            var accessIds = JSON.parse(localStorage.indeCampusRoleAuth).data.userAccessData;   

                            // for( var i=0; i< accessIds.length; i++ ){
                              accessIds.forEach( function( record , index ){
                               
                               POTGaccessData += record.MODULE_ID+",";

                                if(index == (accessIds.length-1)){
                                    console.log("Access data");
                                    console.log(POTGaccessData);
                                    POTGaccessData.slice(0, -1);
                                    localStorage.indeCampusAccessData = POTGaccessData;
                                }
                              });
                            // }
                            reloadWin();
                        },200);
                    }
                }else{
                    toastr.warning(b.status);
                    return false;
                }
            }, "Please wait... Retriving Permission Data.");      

        // Updated by Nazim ends here
                
    }    
}
//END FUCNTION FOR CHANGE BRANCH ON CLICK OF SAVE CHANGES OF CHANGE BRANCH POP UP

function findIndexByKeyValueSubstr(obj, key, value){ 
    var j = 0;
    for (var i = 0; i < obj.length; i++) {
        var data = obj[i][key].search(value);
        if ( data != -1) {
            return data;
        }
    }
    return -1;
}

function resetActiviytData(){
    
    localStorage.removeItem( "POTGactivityAppAssignTypeGroupOptionHtml" );
    localStorage.removeItem( "POTGactivityAppAssignTypeUserOptionHtml" );
    localStorage.removeItem( "POTGactivityAppUserListJson" );
    localStorage.removeItem( "POTGactivityAppUserGrpJson" );
    localStorage.removeItem( "POTGactivityAppCustGrpJson" );
}


// Call Log functions starts here

function playAudio(context){

    if (checkAuth(1, -1, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        CALLLOGID = $(context).attr('calllogid');
        var audioFile = $(context).attr('audioFile');
        var audio = new Audio(audioFile);
        audio.play();
    }
}

function callCustomerAutoComplete(id, redirectPage) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var index = findIndexByKeyValue(ITEMS, "id", item.value);
            setTimeout(function () {
                $("#" + id).val(ITEMS[index].data);
                $("#rateType").val(ITEMS[index].rateTypeId);
                localStorage.POTGselectedCustIdInSearch = ITEMS[index].id;
                redirectPage(item,index);
            }, 1);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3,10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {
                        var group = {
                            id: data.cust_id,
                            name: data.concatKey,
                            data: data.fname + " " + data.lname,
                            mobile: data.mobile,
                            fname: data.fname,
                            lname: data.lname,
                            rateTypeId: data.rateTypeId,
                            catchArea: data.catchArea,
                        };
                        ITEMS.push(group);
                    });
                    // ITEMS.shift(); //It  create new customer li from type ahead
                    return ITEMS;
                }

            }
        }
    });
}

// Code for Version Control Starts here
function versionControl(){

    // $( "link" ).each(function() {
        
    //     var currHref = $(this).attr('href');

    //     if( $(this) != undefined ){

    //         var newHref = currHref +'?v='+VERSION;
    //         $(this).attr('href',newHref);
    //     }
    // });

    $( "script" ).each(function() {

        var currSrc = $(this).attr('src');
        var newSrc = currSrc +'?v='+VERSION;

        if( currSrc != undefined ){
            var newSrc = currSrc +'?v='+VERSION;
            $(this).attr('src',newSrc);
        }

    });
}
// Code for Version Control Ends here

function getActSideBar(type){

    // if( type == "today" ){
    //     getFollowupDetail(1);
    //     $('[href="#todayTab"]').tab('show');
    // }else if( type == "upcomming" ){
    //     getFollowupDetail(3);
    //     $('[href="#upcommingTab"]').tab('show');
    // }else {
    //     getFollowupDetail(2);
    //     $('[href="#delayTab"]').tab('show');
    // }
}

function createCustomer(){

    var createCustFname = $('#createCustFname').val().trim();
    var createCustMobile = getIntlMobileNum("createCustMobile");    // GET MOBILE NUM
    // var createCustMobile = $('#createCustMobile').val().trim();
    var createCustEmail = $('#createCustEmail').val();
    var createCustAddress = $('#createCustAddress').val().trim();
    var createCustCountry = $('#createCustCountry').val();
    var createCustState = $('#createCustState').val();
    var createCustCity = $('#createCustCity').val();
    var createContName = $('#createContName').val();
    var createContMobile = getIntlMobileNum("createContMobile");    // GET MOBILE NUM
    // var createContMobile = $('#createContMobile').val().trim();
    var createContEmail = $('#createContEmail').val();
    var createContDesig = $('#createContDesig').val();
    var createCustRateType = $('#createCustRateType').val();
    var createCustCatchArea = $('#createCustCatchArea').val();

    if( createCustFname == "" ){
        toastr.warning('Please Enter Name');
        $('#createCustFname').focus();
        return false;
    
    }else if( !checkValidMobile( "createCustMobile" ) ){
    // }else if( createCustMobile.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Mobile Number');
        $('#createCustMobile').focus();
        return false;
    
    }else if( createCustEmail != "" && !validateEmail(createCustEmail) ){
       toastr.warning('Please Enter Valid Customer Email');
       $('#createCustEmail').focus();
       return false;
   
    }else if( createCustAddress.length == 0){
       toastr.warning('Please Enter Address');
       $('#createCustAddress').focus();
       return false; 

    }else if( createCustCountry == "-1"){
       toastr.warning('Please Select Country'); 
       addFocusId( 'createCustCountry' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
       return false; 

    }else if( createCustState == "-1"){
       toastr.warning('Please Select State'); 
       addFocusId( 'createCustState' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
       return false; 

    }else if( createCustState == null){
       toastr.warning('Please Select State');
       addFocusId( 'createCustState' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
       return false; 

    }else if( createCustCity == "-1"){
       toastr.warning('Please Select City'); 
       addFocusId( 'createCustCity' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
       return false; 

    }else if( createContName == ""){
       toastr.warning('Please Enter Contact Name');
       $('#createContName').focus();
       return false;  

    }else if( !checkValidMobile( "createContMobile" ) ){
    // }else if( createContMobile.length != MOBILE_LENGTH ){
       toastr.warning('Please Enter Valid Contact Number');
       $('#createContMobile').focus();
       return false;

    }else if( !validateEmail(createContEmail) ){
       toastr.warning('Please Enter Valid Contact Email');
       $('#createContEmail').focus();
       return false;
    
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
            requestCase: "createCustomer",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 9),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            fname: createCustFname,
            mob: createCustMobile,
            email: createCustEmail,
            customerAddress: createCustAddress,
            countryId: createCustCountry,
            stateId: createCustState,
            cityId: createCustCity,
            contactName: createContName,
            contactMobile: createContMobile,
            contactEmail: createContEmail,
            contactDesignation: createContDesig,
            createCustRateType: createCustRateType,
            catchArea: createCustCatchArea,
        };
        // console.log(postData); return false;
        commonAjax(INQURL, postData, createCustomerTroughPopupCallBack,CREATINGINQDATA);
}

function createCustomerTroughPopupCallBack(flag, data){
    if (data.status == "Success" && flag) {

        toastr.success("Customer Created Successfully")

        localStorage.POTGCustDetailCustId = data.data.custId;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        if( tmp_json.data[0].USERLOGINTYPE == "2" ){
            var createCustFname = $('#createCustFname').val().trim();
            var createCustMobile = $('#createCustMobile').val().trim();

            $('#txtCustGrpName').val( createCustFname +' , '+ createCustMobile );
            $('#txtCustGrpName').attr('data-id',data.data.custId);
            modalHide('createCustomer');
            resetCreateCustModal();
        }else{
            window.location.href = 'customerDetail.html';
        }
    }else {
        if (flag)
            // toastr.error(data.status);
          displayAPIErrorMsg( data.status , GBL_ERR_CREATE_CUSTOMER_FAIL );
        else
            toastr.error(SERVERERROR);
    }
}

function resetCreateCustModal(){
    $('#createCustFname').val("");
    $('#createCustMobile').val("");
    $('#createCustEmail').val("");
    $('#createCustAddress').val("");
    $('#createCustState').val("-1").selectpicker('refresh');
    $('#createCustCity').val("-1").selectpicker('refresh');
    $('#createContName').val("");
    $('#createContMobile').val("");
    $('#createContEmail').val("");
    $('#createContDesig').val("");
    $('#createCustRateType').val("-1").selectpicker('refresh');
    $('#createCustCatchArea').val("");
}

function populateCustCity(stateId){
    
    if(stateId == "-1"){
        setOption("0", "createCustCity", "", "--Select City --");
    }else{
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(data.data.cityStateListing,"ID",stateId);
        cityListing = "";
        if(index != -1){
            var cityListing  = data.data.cityStateListing[index]['cityListing'];
        }
        setOption("0", "createCustCity", cityListing, "--Select City --");
    }
    
    $("#createCustCity").selectpicker("refresh");    
}

function navigateToCustPage(){

}

function getProposalChatNotif(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "getProposalChatNotif",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(15, 58),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, getProposalChatNotifCallBack,CREATINGINQDATA);
    
    function getProposalChatNotifCallBack(flag, data){
        if (data.status == "Success" && flag) {

            // console.log(data.data);
            renderPropNotifList(data.data);
        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_PROP_NOTIF );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderPropNotifList(propChat){

    if (checkAuth(15, 58, 1) == -1) {
        toastr.error(NOAUTH);
        return false;
    }

    var tempLi = '';
    var readCount = '';
    if( propChat != 'No record found' ){

      $('#propNotifList').html( '' );
      propChat.forEach( function( record , index ){
        tempLi += '<li class="list-group-item notRead curPoint" inqId="'+ record.inqId +'" title="'+ record.message +'" onclick="navigateToInquiryDetailOpenWin(this , 1)">'+
                        '<div class="notificationlist"><div class="titlediv-1">'+
                            '<span>'+
                            '<i class="actionicon notRead f-gray fa fa-comments"></i>&nbsp;&nbsp;&nbsp;'+
                            '<span data-toggle="tooltip" data-placement="right" title="" class="tooltips" data-original-title="'+ record.message +'">'+ record.message +'</span></span>'+
                        '</div>'+
                        '<div class="gray-textNotif"><span class="small tooltips" title="" data-placement="right" data-toggle="tooltip" data-original-title="Messaged By">Messaged By :<strong>'+ record.name +'</strong></span></div></div>'+
                        '<div class="datedivNotif"><span data-toggle="tooltip" data-placement="left" title="" class="tooltips text-muted" data-original-title="Time">'+ record.date +'</span></div>'+                                    
                     '</li>';
      });
      $('#propNotifList').html( tempLi );
      // $('.modal-body.no-pad-lr').niceScroll();
    }else{
      toastr.warning( 'No New Proposal Notification Available' );
    }

}

function renderError(GBL_ERROR){ //added on 07-07-2017
    
    var errorList = "";
    if( GBL_ERROR != undefined ){    
        GBL_ERROR.forEach(function(record,index){
            errorList += '<span class="text-red">'+record+'</span><br/>';
        });
        $('#errorHideDiv').show();
        $('#errorDiv').html(errorList);
    }    
}

function showHidePwd(id , type , context) {

    if( type == "show" ){
        $('#'+id).attr('type','text');
        $(context).attr('onclick','showHidePwd("'+id+'","hide",this)');
        $(context).html('HIDE');
    }else{
        $('#'+id).attr('type','password');
        $(context).attr('onclick','showHidePwd("'+id+'","show",this)');
        $(context).html('SHOW');
    }
}

function checkActList( typeTxt , idTxt ){

    var dueDate = "";
    var dueTime = "";

    if( idTxt != undefined ){
        dueDate = (($('#'+idTxt).val() ).split('|')[0]).trim();
        dueTime = (( $('#'+idTxt).val() ).split('|')[1]).trim();
    }else{
        dueDate = (($('#txtActDueDate').val() ).split('|')[0]).trim();
        dueTime = (( $('#txtActDueDate').val() ).split('|')[1]).trim();
    }

    if(  dueDate == "" ){
        $('#txtActDueDate').datepicker('show');
        toastr.warning("Please enter due date");
        return false;
    // }
    // else if( dueTime == "" ){
    //     $('#dueTime').focus();
    //     toastr.warning("Please enter due time");
    //     return false;
    }else{

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase : 'getActDetailOnDueDateTime',
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID, 
        dueTime : dueTime,
        dueDate : ddmmyyToMysql(dueDate),
        orgId: checkAuth(2,6),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(ACTURL, postData, checkActListCallback, "");
    }

    function checkActListCallback(flag,data){

       if(data.status == "Success" && flag){

        console.log(data.data);

        var thead = '<table class="display datatables-alphabet-sorting" id="actDueListTable">'+
                    '<thead>'+
                    '<th>Title</th>'+
                    '<th>Customer Name</th>'+
                    '<th>customer No</th>'+
                    '<th>Due Date / Time</th>'+
                    '</thead>'+
                    '<tbody>';

        var newRow = '';
        var checkActData = data.data;
        if(checkActData != "No record found"){

            for(var i=0 ; i<checkActData.length ; i++){

                newRow += '<tr>'+
                            '<td>'+ checkActData[i].actTitle +'</td>'+
                            '<td>'+ checkActData[i].custName +'</td>'+
                            '<td>'+ checkActData[i].custNo +'</td>'+
                            '<td>'+ mysqltoDesiredFormat( checkActData[i].dueDate , 'dd-MM-yyyy' ) +' '+ checkActData[i].dueTime +'</td>'+
                          '</tr>';

            }
        }

        newRow += '</tbody></table>';

        $('#actDueListDiv').html( thead + newRow );
        $('#actDueListTable').dataTable({ "stateSave": true });
        $('#dueTime').clockface('hide');
        $('#actTitleShowModal').modal('show');

        }else{
            if (flag){
               //toastr.error(data.status);
             }
            else{

            toastr.error(SERVERERROR);
            }
            $('#actTitleShowModal').modal('hide');
        }
    }
}

function addRemoveLoader(type){

  if( type == "1" ){ // 1 for add
      $("#loader-wrapper").show();
      $("body").removeClass("loaded");
  }else{  // remove 
      $("#loader-wrapper").hide();
      $("body").addClass("loaded");
  } 

}

//START FUNCTION FOR CHANGE PASSWORD
function changePasswordManager() {

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var newPassword = $("#newpasswordManager").val(); // NEW PASSWORD 
    var retypenewpassword = $("#retypenewpasswordManager").val(); //RE TYPE PASSWORD
    
    if( newPassword == "" ) { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Enter New Password");
        $("#newpasswordManager").focus();
        return false;

    }else if( retypenewpassword == "" ) { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Enter New Re Type New Password");
        $("#retypenewpasswordManager").focus();
        return false;

    }else if( !validatePassword( newPassword ) ){
        toastr.warning('Please Enter Strong Password');
        $('#newpasswordManager').focus();
        return false;
    
    }else if( !validatePassword( retypenewpassword ) ){
        toastr.warning('Please Enter Strong Confirm Password');
        $('#retypenewpasswordManager').focus();
        return false;

    }else if (newPassword != retypenewpassword) { //CHECK IF PASSWORD AND CONFIRM PASSWORD NOT MATCH
        toastr.warning("Password and Confirm Password doesnt Match");
        return false;
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'changePassword',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        reqUserId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(19,205),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        newpassword: md5(newPassword),
        forcePwdFlag: "1",
    }
    // console.log(postData); return false;
    commonAjax(COMMONURL, postData, changePasswordManagerCallback, CHANGEPASSWORDMSG);

    function changePasswordManagerCallback(flag, data) {
        console.log(data);
        if (data.status == "Success" && flag) {
            toastr.success("Password Successfully Updated");
            navigateToIndex();
        } else {
            if (flag){
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_PASSWORD_CHANGE_FAILED );
            }else{
                toastr.error(SERVERERROR);
            }                
            return false;
        }
    }
}


// THIS FUNCTION HIDES THE ACTIVITY MODE WHICHEVER IS NOT RIGHTS TO HIM
function actModeHideShow( type ){

    var returnFlag = "-1";

    // var callActMode = checkAuth(2, 206);
    // var emailActMode = checkAuth(2, 207);
    // var smsActMode = checkAuth(2, 208);
    // var meetingActMode = checkAuth(2, 209);
    // TEMPORARILY CHANGED TO STATIC
    var emailActMode = -1;
    var callActMode = 1;
    var smsActMode = 1;
    var meetingActMode = -1;

    if( type == "1" ){    // 1 - SMART ACTIVITY || 2 - CAMPAING ACTIVITY || 3 - NORMAL ACTIVITY
        // for Smart Activity 
        ( (callActMode == -1) ? $('#phoneradioSmart').parents('div.radioer.manual-radio-width').hide() : activeMode('phoneradioSmart') );  // CALL ACT MODE
        ( (emailActMode == -1) ? $('#emailradioSmart').parents('div.radioer.manual-radio-width').hide() : activeMode('emailradioSmart') );  // EMAIL ACT MODE
        ( (smsActMode == -1) ? $('#smsradioSmart').parents('div.radioer.manual-radio-width').hide() : activeMode('smsradioSmart') );  // SMS ACT MODE
        ( (meetingActMode == -1) ? $('#personalradioSmart').parents('div.radioer.manual-radio-width').hide() : activeMode('personalradioSmart') );  // MEETING ACT MODE
    
    }else if( type == "2" ){
        // for CAMPAING
        ( (callActMode == -1) ? $('#campaignPhoneMode').parents('div.radioer.manual-radio-width').hide() : activeMode('campaignPhoneMode') );  // CALL ACT MODE
        ( (emailActMode == -1) ? $('#campaignEmailMode').parents('div.radioer.manual-radio-width').hide() : activeMode('campaignEmailMode') );  // EMAIL ACT MODE
        ( (smsActMode == -1) ? $('#campaignSMSMode').parents('div.radioer.manual-radio-width').hide() : activeMode('campaignSMSMode') );  // SMS ACT MODE
        ( (meetingActMode == -1) ? $('#campaignMeetMode').parents('div.radioer.manual-radio-width').hide() : activeMode('campaignMeetMode') );  // MEETING ACT MODE
    }else{
        // for Normal Activity
        ( (callActMode == -1) ? $('#radio-05').parents('div.radioer.manual-radio-width').hide() : activeMode('radio-05') );  // CALL ACT MODE
        ( (emailActMode == -1) ? $('#radio-06').parents('div.radioer.manual-radio-width').hide() : activeMode('radio-06') );  // EMAIL ACT MODE
        ( (smsActMode == -1) ? $('#radio-07').parents('div.radioer.manual-radio-width').hide() : activeMode('radio-07') );  // SMS ACT MODE
        ( (meetingActMode == -1) ? $('#radio-08').parents('div.radioer.manual-radio-width').hide() : activeMode('radio-08') );  // MEETING ACT MODE
    }

    function activeMode( radioId ){
        
        if( returnFlag != "1" ){
            setTimeout(function(){
              $('#'+radioId).prop('checked',true);
            },100);
            returnFlag = "1";
        }
    }

    // returnFlag = "-1";
    return returnFlag;
}

// THIS FUNCTION HIDES THE ACTIVITY MODE WHICHEVER IS NOT RIGHTS TO HIM

// OPEN CREATE CUSTOMER MODAL ON ACTIVITY FOR ACTIVITY TYPE USER
function openCreatCust(){
    modalShow('createCustomer');
}


function callActListFun( id ){
    setTimeout(function(){
        checkActList("1", id );

        var radioName = "";
        var setId = "";
        var getId = "";

        if( id == "txtActDueDateForEditAct" ){
          radioName = "rdoActModeForEditAct";
          setId = "txtActNormEndDateEdit";
          getId = "txtActDueDateForEditAct";
        }

        if( $('input[type=radio][name="'+ radioName +'"]').filter(":checked").val() == "4" ){    // ADD 60 MIN
            addTimeInMinutes( setId , $('#'+getId).val() , 60 )
        }else{    // ADD 15 MIN
            addTimeInMinutes( setId , $('#'+getId).val() , 15 )
        }

    },10);
}

function removeLoaderInTime( miliSec ){
  setTimeout(function(){
      addRemoveLoader( miliSec );
  },miliSec);
}

// SET COUNTRY LISTING IN TO THE SELECTPICKER FROM LOAD AUTH
function setCountryList( id ){

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var countryList = roleAuth.data.countryListing;

    setOptionWithRef("0", id, countryList, "---Select Country---", 'countryId', 'countryName');
    $('#'+id).selectpicker(); 
    $('#'+id).selectpicker('refresh'); 
}

var GBL_COUNTRY_CITY_STATE = [];
// SET STATE LISTING IN TO THE SELECTPICKER FROM API CALL
function populateState( countryIdCntx , stateIdCntx ){

    if( countryIdCntx == undefined ){
      countryIdCntx = $('#createCustCountry').attr('countryId');
      stateIdCntx = $('#createCustCountry').attr('stateId');
    }

    var countryId = $('#'+countryIdCntx).val();
    var stateId = $('#'+countryIdCntx).val();

    if( countryId == "-1" || countryId == "0" ){
      return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStateCity',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1,2),
        countryId: countryId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, populateStateListingDataCallback,"Please Wait... Getting Dashboard Detail");

    function populateStateListingDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            GBL_COUNTRY_CITY_STATE = data.data;
            setOptionWithRef("0", stateIdCntx, GBL_COUNTRY_CITY_STATE, "---Select State---", 'stateId', 'stateName');
            $('#'+stateIdCntx).selectpicker('refresh'); 

            if( currPage == "/createEnrollment" || currPage == "/studentEnrollmentDetail" ){

              setOptionWithRef("0", 'collegeState', GBL_COUNTRY_CITY_STATE, "---Select State---", 'stateId', 'stateName');
              $('#collegeState').selectpicker('refresh');
              setOptionWithRef("0", 'guarantorState', GBL_COUNTRY_CITY_STATE, "---Select State---", 'stateId', 'stateName');
              $('#guarantorState').selectpicker('refresh');
              setOptionWithRef("0", 'parentState', GBL_COUNTRY_CITY_STATE, "---Select State---", 'stateId', 'stateName');
              $('#parentState').selectpicker('refresh');
              
            }

            if( GBL_INDEX != "-1" ){
                
                $('#'+stateIdCntx).val( ITEMS[GBL_INDEX].stateId ).trigger("change");
                $('#'+stateIdCntx).selectpicker('refresh');

                $('#custCityId').val( ITEMS[GBL_INDEX].cityId );
                $('#custCityId').selectpicker('refresh');
                
                $('#city').val( ITEMS[GBL_INDEX].cityId );
                $('#city').selectpicker('refresh');

                $('#parentState').val( ITEMS[GBL_INDEX].parentStateId ).trigger("change");
                $('#parentState').selectpicker('refresh');
                $('#parentCity').val( ITEMS[GBL_INDEX].parentCityId ).trigger("change");
                $('#parentCity').selectpicker('refresh');
                
                $('#guarantorState').val( ITEMS[GBL_INDEX].guarantorStateId ).trigger("change");
                $('#guarantorState').selectpicker('refresh');
                $('#guarantorCity').val( ITEMS[GBL_INDEX].guarantorCityId ).trigger("change");
                $('#guarantorCity').selectpicker('refresh');
                $('#collegeState').val( ITEMS[GBL_INDEX].collegeStateId ).trigger("change");
                $('#collegeState').selectpicker('refresh');
                $('#collegeCity').val( ITEMS[GBL_INDEX].collegeCityId );
                $('#collegeCity').selectpicker('refresh');


            }else{
                
                if( inqData != "" && inqData != undefined && inqData != "No record found" ){
                    $('#'+stateIdCntx).val( inqData.custDetail.stateId ).trigger("change");
                    $('#'+stateIdCntx).selectpicker('refresh');

                    $('#custCityId').val( inqData.custDetail.cityId );
                    $('#custCityId').selectpicker('refresh');
                }
            }

        } else {
            GBL_COUNTRY_CITY_STATE = [];
            setOptionWithRef("0", stateIdCntx, GBL_COUNTRY_CITY_STATE, "---Select State---", 'stateId', 'stateName');
            $('#'+stateIdCntx).selectpicker('refresh'); 

            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CITY_STATE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// IT WILL POPULATE THE CITY ON STATE
function populateCityOnState( stateIdCntx , cityIdCntx ){

    if( stateIdCntx == undefined ){
      stateIdCntx = $('#createCustState').attr('stateId');
      cityIdCntx = $('#createCustState').attr('cityId');
    }

    var stateId = $('#'+stateIdCntx).val();
    
    if( stateId == "-1" ){
      return false;
    }
      
    var cityList = [];
    var index = findIndexByKeyValue( GBL_COUNTRY_CITY_STATE , "stateId" , stateId );
    if( index != "-1" ){
      if( GBL_COUNTRY_CITY_STATE[index].cityData != "No record found" ){ 
          cityList = GBL_COUNTRY_CITY_STATE[index].cityData;
      }
    } 

    setOptionWithRef("0", cityIdCntx, cityList, "---Select City---", 'cityId', 'cityName');
    $('#'+cityIdCntx).selectpicker('refresh');

}

$(function(){
    $('#navlink_switch').on("click", function () {
    $('body').toggleClass('bodyBig');
    $('.nav-bar-container').toggleClass('nav-bar-containerBig');
    $('.nav-bar-border').toggleClass('nav-bar-containerBig');
    $('.actUserHide span').toggleClass("hideMenu");
    });
    
}); 


function selectDeselectAll(){

    if( $('#selDeselAll').prop('checked') ){
        $('.notifCbClass').prop('checked',true);
    }else{ 
        $('.notifCbClass').prop('checked',false);
    }
}

function commonAutoCompleteForCustInqSearch(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) { 
            
            callback( item ); //CALLBACK FUNCTION
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "customerSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(19,78),
                    keyword: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data, function (data) {

                        if( data.customerId != "-1" ){

                            var group = {
                                id: data.customerId,
                                custId: data.customerId, 
                                name: data.firstName + " " + data.lastName,
                                data: data.firstName + " " + data.lastName,
                                mobile: data.mobileNo,
                                email: data.emailId,
                                clientCustomerId: data.clientCustomerId
                            };
                            ITEMS.push(group);
                        }
                    });
              
                    return ITEMS;
                }

            }
        }
    });
} 

function renderNotifList(){

    if (checkAuth(1, 2, 1) == -1) {
        toastr.error(NOAUTH);
        return false;
    }

    $('#clearNotifDiv').hide();
    var tempLi = '';
    alertCount = 0;
    var readCount = '';
    if( eventAlertData != 'No record found' ){

      var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
      var userId = userDetail.data[0].PK_USER_ID;
      for( var i=0; i<eventAlertData.length; i++ ){
        var readStatus = 'f-gray';
        var readStatusFaClass = 'fa fa-envelope';
        var readBgStatus = 'notRead';
        var messg = 'Unread';
        var curPoint = 'curPoint';
        if( userId == eventAlertData[i].ALERT_SUB_ID && eventAlertData[i].ASSIGNTO != eventAlertData[i].ALERT_SUB_ID ){
            
            if( eventAlertData[i].ASSIGNBY != eventAlertData[i].ASSIGNTO ){     
                  if( eventAlertData[i].STATUS == 0 ){
                      alertCount++;
                  }
                  var updateNofiStat = 'onclick="updateAlert(this)"';
                  if( eventAlertData[i].STATUS == 1 ){
                    readStatus = 'f-green';
                    readStatusFaClass = "fa fa-envelope-open";
                    readBgStatus = 'Read';
                    updateNofiStat = '';
                    messg = 'Read';
                    curPoint = '';
                    readCount++;
                  }

                  if( readCount < 10  ){
                      var alertStagingId = eventAlertData[i].PK_STAGING_ID;

                      tempLi += '<li class="list-group-item '+ readBgStatus +' '+ curPoint +'">'+
                                    '<div class="checkboxer">'+
                                        '<input type="checkbox" id="notifCb_'+ alertStagingId +'" class="notifCbClass">'+ 
                                        '<label class="control-label" for="notifCb_'+ alertStagingId +'"></label>'+
                                    '</div>'+
                                    '<div class="notifList" alertId="'+ alertStagingId +'" '+ updateNofiStat +' data-activityid="'+ eventAlertData[i].REF_ID +'" inqid="'+ eventAlertData[i].REF_ID +'" title="'+ eventAlertData[i].MESSAGE_TEXT +'">'+
                                      '<div class="notificationlist">'+
                                          '<div class="titlediv-1">'+
                                            '<span>'+
                                            '<i class="actionicon '+ readStatus +' '+ readStatusFaClass +'"></i>&nbsp;&nbsp;&nbsp;'+
                                            '<span data-toggle="tooltip" data-placement="right" title="" class="tooltips" data-original-title="'+ messg +'">'+ eventAlertData[i].MESSAGE_TEXT +'</span></span>'+
                                          '</div>'+
                                          // '<div class="gray-textNotif"><span class="small tooltips" title="" data-placement="right" data-toggle="tooltip" data-original-title="Assigned By">Assigned By :<strong>'+ eventAlertData[i].ASSIGNBY +'</strong></span></div>'+
                                      '</div>'+
                                      '<div class="datedivNotif"><span data-toggle="tooltip" data-placement="left" title="" class="tooltips text-muted" data-original-title="Time">'+ eventAlertData[i].ASSIGNON +'</span></div>'+                                    
                                    '</div>'+
                                '</li>';
                  }
                }
            }
        $('#clearNotifDiv').show();
      }
      $('#notifList').html( tempLi );
      if( parseInt( alertCount ) > 99 ){
          alertCount = "99+";
      }
      $('#notifAlertCount').html( alertCount );
      // $('.modal-body.no-pad-lr').niceScroll();
      $('#notificationModal').modal('show');
      $('#propNotifList').html( '' );
    }else{
      toastr.warning( 'No New Notification Available' );
      $('#notificationModal').modal('show');
    }
    $('#notificationModal .active').click();

    $('#selDeselAll').prop('checked',false);
}

function updateAlert(context){
    
    var alertId = $(context).attr('alertId');
    var title = $(context).attr('title');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'updateStatusOnClick',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1, 2),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        alertId: alertId,
    }

    commonAjax(COMMONURL,postData,updateAlertCallBack,RETRIEVINGMODULEDETAIL);

    function updateAlertCallBack(flag,data){
          if (data.status == "Success" && flag) {
            // $(context).addClass('Read').removeClass('notRead');
            // $(context).removeAttr('onclick');
            // $(context).find('i').addClass('f-green').removeClass('f-gray');
            alertCount = alertCount - 1;
            if( parseInt( alertCount ) > 99 ){
                alertCount = "99+";
            }
            $('#notifAlertCount').html( alertCount );

            var index = findIndexByKeyValue( eventAlertData , 'PK_STAGING_ID' , alertId );
            eventAlertData[index].STATUS = 1;
            $('#clearNotifDiv').hide();
            renderNotifList();

            var reqFlag = (title).toLowerCase().search("req");
            var compFlag = (title).toLowerCase().search("comp"); 
            var supFlag = (title).toLowerCase().search("sup"); 
            var terFlag = (title).toLowerCase().search("ter"); 

            if( reqFlag != '-1' || compFlag != '-1' ){
              // custid   // inqid
              localStorage.indeCampusMyNotif = true;
              navigateToReqComplaintList();
            }else if( supFlag != '-1' || terFlag != '-1' ){
              // custid   // inqid
              navigateToSuspensionTermination();
            } 

          } else {
              if (flag)
                  // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NOTI_STATUS_UPDATE_FAILED );
              else
                  toastr.error(SERVERERROR);
          }
    }
}

// ADDED BY KAUSHA SHAH ON 16-08-2018
function navigateToCentraliseInvoice(){
    
    if (checkAuth(28, 111, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "centralisedinvoice.html";
    }
}


function navigateToChargePostMaster(){
    
    if (checkAuth(24, 98, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        window.location.href = "chargePostingMaster.html";
    }
}

 

function clearNotifList(){
    
    var alertIdCs = "";
    if( eventAlertData == "" || eventAlertData == "No record found" ) {
      toastr.warning('No Notification Available');
      return false;
    }

    eventAlertData.forEach( function( record , index ){

        if( $('#notifCb_'+record.PK_STAGING_ID).prop('checked') ){
          if( alertIdCs ){
              alertIdCs += ','+record.PK_STAGING_ID;
          }else{
              alertIdCs = record.PK_STAGING_ID;
          }
        }
    });

    if( alertIdCs == "" ) {
      toastr.warning('Please Select Notification to clear');
      return false;
    }
 
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'updateStatusOnClick',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1, 2),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        alertId: alertIdCs,
    }

    commonAjax(COMMONURL,postData,updateAlertCallBack,RETRIEVINGMODULEDETAIL);

    function updateAlertCallBack(flag,data){
          if (data.status == "Success" && flag) {
            
            $('#clearNotifDiv').hide();
            alertIdCs.split(",").forEach( function( record , index ){
              alertCount = alertCount - 1;
              var index = findIndexByKeyValue( eventAlertData , 'PK_STAGING_ID' , record );
              // eventAlertData[index].STATUS = 1;
              eventAlertData.splice( index , 1 );
            });

            if( parseInt( alertCount ) > 99 ){
                alertCount = "99+";
            }
            $('#notifAlertCount').html( alertCount );
            renderNotifList();

          } else {
              if (flag)
                  // toastr.error(data.status);
                  displayAPIErrorMsg( data.status , GBL_ERR_NOTI_STATUS_UPDATE_FAILED );
              else
                  toastr.error(SERVERERROR);
          }
    }
}