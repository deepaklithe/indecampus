/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : userManageRoleController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */


var selectedActivityIds = [];
var editSelectedActivityIds = [];
var roleData = [];
var mainHtml = '';
// GET ROLE LISTING DATA
function getRoleData(){

    $('#rolename').focus();
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getrole",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9,37),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getRoleDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET ROLE LISTING DATA CALLBACK 
function getRoleDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        roleData = data.data;

        renderRoleData();

        getRoleListing();

    } else {
        if (flag){
            getRoleListing();
            displayAPIErrorMsg( data.status , GBL_ERR_ROLE_LIST );
        }else{
            toastr.error(SERVERERROR);
        }
            
    }
}

// RENDER ROLE LISTING DATA
function renderRoleData(){

    var newRow = '';
    var count = 1 ;

    addFocusId( 'rolename' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    if(roleData != 'No record found'){
        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Role Name</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';
        for( var i=0; i<roleData.length; i++ ){

            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><a href="javascript:void(0)" onclick="roleTrans(this)" id="'+ roleData[i].roleId +'">'+ roleData[i].roleName +'</a></td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" id="'+ roleData[i].roleId +'" onclick="editRoleDetail(this)"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" id="'+ roleData[i].roleId +'" onclick="deleteRole(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
        }

        $('#dtTable').html(thead + newRow + tfoot);

    }

    TablesDataTables.init();
    TablesDataTablesEditor.init();

}

// GET ROLE PERMISSION LISTING
function getRoleListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getPermissionListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,10),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL,postData,displayRoleListing,RETRIEVINGMODULEDETAIL);
}

// GET ROLE PERMISSION LISTING CALLBACK - DISPLAY ROLE PERMISSION
function displayRoleListing(flag,data){
    if (data.status == "Success" && flag) {

        data = data.data;
        //MODULE_NAME ACTIVITY_NAME PK_MODULE_ID PK_ACTIVITY_ID

        var start = '<optgroup label="**">';
        var end = '</optgroup>';
        var option = '';
        var innerHTML = ' ';
        mainHtml = '';
        for(var i=0;i<data.length;i++){

            if(localStorage.indeCampusRoleFlag == "true"){


                var tmp_start = start;
                tmp_start = tmp_start.replace("**",data[i].moduleName);
                innerHTML = tmp_start;

                var tmp_option = data[i].activityName.split(",");
                var tmp_option1 = data[i].activityId.split(",");

                for(var j=0;j<tmp_option.length;j++){
                    if(editSelectedActivityIds.indexOf(tmp_option1[j]) == -1){
                        innerHTML += '<option value="'+data[i].moduleId+'|'+tmp_option1[j]+'" id="'+data[i].moduleId+'|'+tmp_option1[j]+'">'+tmp_option[j]+'</option>';
                    }else{
                        selectedActivityIds.push(data[i].moduleId+'|'+tmp_option1[j]);
                        innerHTML += '<option selected value="'+data[i].moduleId+'|'+tmp_option1[j]+'" id="'+data[i].moduleId+'|'+tmp_option1[j]+'">'+tmp_option[j]+'</option>';
                    }

                }
                innerHTML += end;
                mainHtml +=innerHTML;

            }else{
                var tmp_start = start;
                tmp_start = tmp_start.replace("**",data[i].moduleName);
                innerHTML = tmp_start;

                var tmp_option = data[i].activityName.split(",");
                var tmp_option1 = data[i].activityId.split(",");
                for(var j=0;j<tmp_option.length;j++){
                    innerHTML += '<option value="'+data[i].moduleId+'|'+tmp_option1[j]+'" id="'+data[i].moduleId+'|'+tmp_option1[j]+'">'+tmp_option[j]+'</option>';
                }
                innerHTML += end;
                mainHtml +=innerHTML;
            }

        }

        $(".my_multi_select").html('<select multiple="multiple" class="multi-select" id="my_multi_select2" name="my_multi_select2[]">'+mainHtml+'</select>');
        
        multiSelectFunction();
        assignMultiselectEvent();


    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_PERMISSION_LIST );
        else
            toastr.error(SERVERERROR);
    }
}

// MULTI-SELECT ELEMENT CHANGE EVENT 
function assignMultiselectEvent(){

    $('#my_multi_select2').change(function () {
        selectedActivityIds = [];
        $('#my_multi_select2 option:selected').each(function () {
            selectedActivityIds.push($(this).attr("id"));
        });
        var uniqueNames = [];
        $.each(selectedActivityIds, function (i, el) {
            if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });
        selectedActivityIds = uniqueNames;

        log(selectedActivityIds);
    });
}

// CREATE / UPDATE ROLE
function saveRoleDetail(){

    if(checkAuth(9,36,1) == -1){
         toastr.error( NOAUTH );
    }else{
        var rolename = $("#rolename").val().trim();
        if(rolename.length <= 0 ){
            toastr.warning(ROLENAMEBLANK);
            $('#rolename').focus();
        } else if(selectedActivityIds.length <= 0){
            toastr.warning(ACTIVITYBLANK);
        } else {
            var role = [];
            var mainarray = [];
            for(var i=0;i<selectedActivityIds.length;i++){
                var tmp_data = [];
                var tmp = selectedActivityIds[i].split("|");

                tmp_data = {
                    rolename : rolename,
                    moduleid : tmp[0],
                    activityid : tmp[1]
                }

                role.push(tmp_data);
            }
            mainarray = role;
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            var reqCase = "";
            var tmp_org="";
            if(localStorage.indeCampusRoleFlag == "true"){
                reqCase = "editrole";
                tmp_org=checkAuth(9,38);
            }else{

                var index = findIndexByKeyValue( roleData , "roleName" , rolename );
                if( index != "-1" ){
                    toastr.warning('This Role Name is already Used');
                    return false;
                } 

                reqCase = "createrole";
                tmp_org=checkAuth(9,36);
            }
            var postData = {
                requestCase : reqCase,
                clientId : tmp_json.data[0].FK_CLIENT_ID,
                userId : tmp_json.data[0].PK_USER_ID,
                orgId: tmp_org,
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                role : mainarray
            };
            if(localStorage.indeCampusRoleFlag == "true")
                postData.roleId = localStorage.indeCampusRoleIdForEdit;

            // console.log(postData);
            // return false;
            commonAjax(COMMONURL,postData,callbackOfSaveRoleDetail,SENDINGROLEDETAILTOCREATE);

        }
    }
}

// CREATE / UPDATE ROLE CALLBACK
function callbackOfSaveRoleDetail(flag,data){

    if (data.status == "Success" && flag) {
        //success
        console.log(data);
        
        var rolename = $("#rolename").val().trim();

        if( localStorage.indeCampusRoleFlag == "false" ){

            var tempData = {
                roleId: data.data.PK_ROLE_ID,
                roleName: rolename,
                roleTrans: "No record found",
            }

            roleData.push( tempData ); 

            // renderRoleData();
            getRoleData();

        }else{

            var index = findIndexByKeyValue( roleData,'roleId',localStorage.indeCampusRoleIdForEdit );

            var tempArray = {
                roleId: localStorage.indeCampusRoleIdForEdit,
                roleName: rolename,
                roleTrans: roleData[index].roleTrans,
            }

            roleData[index] = tempArray;

            // renderRoleData();
            getRoleData();
        }

        localStorage.indeCampusRoleFlag = "false";

        resetPopUp();

    } else {
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_ROLE_LIST );
        }
        else{
            toastr.error(SERVERERROR);
        }
    }

}

// GET SELECTED ROLE'S DATA FOR UPDATE
function editRole(){
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase : "getroledetails",
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9,37),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        roleId : localStorage.indeCampusRoleIdForEdit
    };
    commonAjax(COMMONURL,postData,editRoleCallback,RETRIEVINGROLEDETAIL);
}

function editRoleCallback(flag,data){
    if (data.status == "Success" && flag) {
        //success
        log(data);
        editSelectedActivityIds = [];
        for(var i=0;i<data.data.length;i++){
            var tmp = data.data[i].ACTIVITY.split(",");
            $("#rolename").val(data.data[i].ROLE_NAME);
            for(var j = 0; j<tmp.length;j++){
                editSelectedActivityIds.push(tmp[j]);
            }
        }
        setTimeout(function(){getRoleListing();},500);
     addFocusId( 'rolename' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_ROLE_LIST );
        else
            toastr.error(SERVERERROR);
    }
}

// GET SELECTED ROLE ID FOR GETTING ROLE DETAIL FOR EDIT
function editRoleDetail(context){
    if(checkAuth(9,38,1) == -1){
         toastr.error('You are not authorised for this Activitiy');
    }else{
        var roleid = $(context).attr("id");
        localStorage.indeCampusRoleIdForEdit = roleid;
        localStorage.indeCampusRoleFlag = true;
        editRole();
    }
}

var roleIdForDelete = '';
// DELETE ROLE
function deleteRole(context){

    if(checkAuth(9,39,1) == -1){
         toastr.error( NOAUTH );
    }else{
        var flag = confirm(CNFDELETE);
        if(flag){
            roleIdForDelete = $(context).attr("id");
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            //delete role
            var postData = {
                requestCase : "deleterole",
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(9,39),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                roleid : roleIdForDelete
            };

            commonAjax(COMMONURL,postData,deleteRoleCallback,DELETEROLE);
        }
    }
}

// DELETE ROLE CALLBACK
function deleteRoleCallback(flag,data){
    
    if (data.status == "Success" && flag) {
        //success
        console.log(data);

        var index = findIndexByKeyValue( roleData,'roleId',roleIdForDelete );

        roleData.splice(index,1);

        renderRoleData();

        resetPopUp();
        

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_ROLE );
        else
            toastr.error(SERVERERROR);
    }
}

// DISPLAY ROLE PERMISSION LISTING IN MODAL
function roleTrans(context){

    var roleId = $(context).attr('id');

    var index = findIndexByKeyValue(roleData,'roleId',roleId);

    var newRow = '';
    var count = 1 ;
    if( roleData[index].roleTrans != 'No record found' ){

        for( var i=0; i<roleData[index].roleTrans.length; i++ ){
            count = i + 1;

            newRow += '<tr>'+
                            '<td>' + count + '</td>'+
                            '<td>' + roleData[index].roleTrans[i].ACTIVITYNAME + '</td>'+
                            '<td>' + roleData[index].roleTrans[i].MODULE_NAME + '</td>'+
                       '</tr>';
        }

        $('#roleListData').html(newRow);
        $('#roleList').modal('show');
        
    }
}

// RESET ROLE DATA FIELDS
function resetPopUp(){

    $('#rolename').val('');
        
    location.reload();

    localStorage.removeItem('indeCampusRoleIdForEdit');
    localStorage.indeCampusRoleFlag = false;
}