/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 03-Jun-2016.
 * File : userProfileController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data

//START FUNCTION FOR DISPLAY DATA OF USER
function displayUserData() {
    var userData = tmp_json.data[0];
    var userId = userData['PK_USER_ID'];
    var userName = userData['USERNAME'];
    var userAddress = userData['ADDRESS'];
    var userMobile = userData['CONTACT_NO'];
    var userEmail = userData['EMAIL'];
    var userFullName = userData['FULLNAME'];
    var userProfileImg = (userData['PROFILE_IMAGE']) == "" ? httpHead + "//"+FINALPATH+"/assets/globals/img/avtar1.png" : userData['PROFILE_IMAGE'];
    $("#userId").html(userId);
    $("#userName").html(userName);
    $("#userEmail").html(userEmail);
    $("#userMobile").html(userMobile);
    $("#userAddress").html(userAddress);
    $("#userFullName").html(userFullName);
    $('#userImage').attr('src',userProfileImg);
}
//END FUNCTION FOR DISPLAY DATA OF USER


//START FUNCTION FOR CHANGE PASSWORD
function changePassword() {
    var userId = $("#userId").html(); //USER ID 
    var newPassword = $("#newpassword").val(); // NEW PASSWORD 
    var retypenewpassword = $("#retypenewpassword").val(); //RE TYPE PASSWORD
    var oldpassword = $("#oldpassword").val(); // OLD PASSWORD

    if (newPassword == "" || retypenewpassword == "" || oldpassword == "") { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Fill all details");
        return false;

    }else if( !validatePassword( newPassword ) ){
        toastr.warning('Please Enter Strong Password');
        $('#newpassword').focus();
        return false;
    
    }else if( !validatePassword( retypenewpassword ) ){
        toastr.warning('Please Enter Strong Confirm Password');
        $('#retypenewpassword').focus();
        return false;

    }else if (newPassword != retypenewpassword) { //CHECK IF PASSWORD AND CONFIRM PASSWORD NOT MATCH
        toastr.warning("Password and confirm password mismatch");
        return false;
    }
    
    function afterChangedPassword(flag, data) {
        console.log(data);
        if (data.status == "Success" && flag) {
            toastr.success("Password Successfully updated");
            navigateToIndex();
        } else {
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_PASSWORD_CHANGE_FAILED );
            }else{
                toastr.error(SERVERERROR);
            }                
            return false;
        }
    }

    var postData = {
        requestCase: 'changePasswordUser',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8, 35),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        oldpassword :md5(oldpassword),
        newpassword: md5(newPassword),
        passwordFlag : "1"
    }
    // var postData = {
    //     requestCase: 'changePassword',
    //     clientId: tmp_json.data[0].FK_CLIENT_ID,
    //     userId: tmp_json.data[0].PK_USER_ID,
    //     reqUserId: tmp_json.data[0].PK_USER_ID,
    //     orgId: checkAuth(19,205),
    //     dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    //     newpassword: md5(newPassword),
    //     oldpassword :md5(oldpassword),
    //     userProfileFlag : "1"
    // }
    commonAjax(COMMONURL, postData, afterChangedPassword, CHANGEPASSWORDMSG);
}
//END FUNCTION FOR CHANGE PASSWORD

displayUserData(); //CALL FUNCTION FOR DISPLAY DATA

//START FUNCTION FOR ADD DOCUMENT
function addDodument(){

    registerCanvasEvent();         
    var temp_json = {
        requestCase: "userProfileImageUpload",
        // requestCase: "profileImageUpload",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: localStorage.indeCampusBranchIdForChkAuth,
    };
    $("#postData").val(JSON.stringify(temp_json));
    $("#addDocument").submit();
}
//END FUNCTION FOR ADD DOCUMENT

//START FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA
function registerCanvasEvent(){

    var bfsendMsg = "Sending data to server..";
    var profileImg = $('#docFile').val();
    $("#addDocument").off().on('submit', (function (e) {

        if( checkAuth(8, 31, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }else if( profileImg == "" ){
            toastr.warning('Please select image');
            return false;
        }
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                addRemoveLoader(1);
                // $("#ajaxloader").removeClass("hideajaxLoader");
            },
            success: function (data)// A function to be called if request succeeds
            {
                data = JSON.parse(data);
                console.log(data);

                if (data.status == "Success") {
                    var userData = JSON.parse(localStorage.indeCampusUserDetail);
                    userData.data[0].PROFILE_IMAGE = data.data.fileLink;
                    localStorage.indeCampusUserDetail = JSON.stringify(userData);
                    $('#userImage').attr('src',userData.data[0].PROFILE_IMAGE);
                    $('#userProfile').attr('src',userData.data[0].PROFILE_IMAGE);
                
                } else {
                    toastr.success(data.status);
                }
                // $("#ajaxloader").addClass("hideajaxLoader");
                addRemoveLoader(0);
                $('#uploadProfilePop').modal('hide');
                
                $(".fileinput-exists").trigger("click");
                $('#postData').val("");
                $('#docName').val("");
                $('#docFile').val("");
                $('#docTypeAdd').val("1");
                $("#docType").val("0").selectpicker('refresh');
                VIEWJSONDATALENGTH = 0;
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                // $("#ajaxloader").addClass("hideajaxLoader");
            }
        });
    }));
}
//END FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA

$('#reset-password').on('show.bs.modal',function(){
    $('#oldpassword').val('');
    $('#newpassword').val('');
    $('#retypenewpassword').val('');
});

$('#uploadProfilePop').on('show.bs.modal',function(){
    $('#postData').val("");
    $(".fileinput-exists").trigger("click");
});