/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 09-02-2016.
 * File : activityAnlyticsController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_TYPE_WISE = [];
var GBL_MODE_WISE = [];
var GBL_CONVERT_WISE = [];
var GBL_TARGET_ACTUAL_WISE = [];
function getAnalyticData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getAnalyticActivityChartReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(FOLLOWUPURL, postData, getAnalyticDataCallback,"Please Wait... Getting Dashboard Detail");

    function getAnalyticDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log( data.data );

            google.charts.load('current', {packages: ['corechart', 'bar' , 'gauge' , 'geochart']});

            GBL_TYPE_WISE = data.data.actTypeChart;
            GBL_MODE_WISE = data.data.actModeChart;
            GBL_CONVERT_WISE = data.data.actConversion;
            GBL_TARGET_ACTUAL_WISE = data.data.actComparison;
            $('#1stLi a').click();
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CHART_DATA );
            else
                toastr.error(SERVERERROR);
        }
        
    }
}

function typeWiseChart(){

    if( GBL_TYPE_WISE == "No record found" || GBL_TYPE_WISE == "" ){
      return false;
      
    }

    var chartData = GBL_TYPE_WISE;
    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawChart);
    },100);
    function drawChart() {

      var chartArr = [];
      var firstArr = [];

      firstArr.push( 'Status' );
      firstArr.push( 'Count' );

      chartArr.push( firstArr );
      chartData.forEach(function(record,index) {
            
          var temp = [];
          temp.push(record.statusName);
          temp.push(parseFloat(record.actCount));
          chartArr.push( temp );
      });

      var data = google.visualization.arrayToDataTable( chartArr );

      var options = {
        sliceVisibilityThreshold: .0,
        pieHole: 0.3,
        legend: 'none',
        pieSliceText: 'label',
        height:500,
        width : "100%",
        // is3D: true,
        animation: {
                  duration: 1000,
                  easing: 'linear',
                  startup: true
              }
      };

      var chart = new google.visualization.PieChart(document.getElementById('typeWiseChrt'));

      chart.draw(data, options);
    }
}

function modeWiseChart(){

    if( GBL_MODE_WISE == "No record found" || GBL_MODE_WISE == "" ){
      return false;
    }

    var chartData = GBL_MODE_WISE;
    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawChart);
    },100);
    function drawChart() {

      var chartArr = [];
      var firstArr = [];

      firstArr.push( 'Status' );
      firstArr.push( 'Count' );

      chartArr.push( firstArr );
      chartData.forEach(function(record,index) {
            
          var temp = [];
          temp.push(record.statusName);
          temp.push(parseFloat(record.actCount));
          chartArr.push( temp );
      });

      var data = google.visualization.arrayToDataTable( chartArr );

      var options = {
        sliceVisibilityThreshold: .0,
        pieHole: 0.3,
        legend: 'none',
        pieSliceText: 'label',
        height:500,
        width : "100%",
        // is3D: true,
        animation: {
                  duration: 1000,
                  easing: 'linear',
                  startup: true
              }
      };

      var chart = new google.visualization.PieChart(document.getElementById('modeWiseChrt'));

      chart.draw(data, options);
    }
}

function statConvertChart(){

    if( GBL_CONVERT_WISE == "No record found" || GBL_CONVERT_WISE == "" ){
      return false;
    }

    var chartData = GBL_CONVERT_WISE;
    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawChart);
    },100);
    function drawChart() {

      var chartArr = [];
      var firstArr = [];

      firstArr.push( 'Status' );
      firstArr.push( 'Count' );

      chartArr.push( firstArr );
      chartData.forEach(function(record,index) {
            
          var temp = [];
          temp.push(record.statusName);
          temp.push(parseFloat(record.statusCount));
          chartArr.push( temp );
      });

      var data = google.visualization.arrayToDataTable( chartArr );

      var options = {
        sliceVisibilityThreshold: .0,
        pieHole: 0.3,
        legend: 'none',
        pieSliceText: 'label',
        height:500,
        width : "100%",
        // is3D: true,
        animation: {
                  duration: 1000,
                  easing: 'linear',
                  startup: true
              }
      };

      var chart = new google.visualization.PieChart(document.getElementById('convertChrt'));
      chart.draw(data, options);
    }
}

function targetVsActChart(){

    if( GBL_TARGET_ACTUAL_WISE == "No record found" || GBL_TARGET_ACTUAL_WISE == "" ){
      return false;
    }

    chartData = GBL_TARGET_ACTUAL_WISE;

    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {
        var chartArr = [];
        var firstArr = [];

        // chartData.sort(function(a, b) {
        //   return a.month - b.month;
        // });

        firstArr.push( 'Year' );
        firstArr.push( 'Target' );
        firstArr.push( 'Achievement' );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
              
            var temp = [];
            temp.push(record.actType);
            record.typeData.forEach(function(record,index) {
              temp.push(parseFloat(record.targetAct));
              temp.push(parseFloat(record.actCount));
            });
            chartArr.push( temp );
        }); 
         // var data = google.visualization.arrayToDataTable([
         //      ['Year', 'Target', 'Achievement'],
         //      ['Jan', 1000, 400],
         //      ['Feb', 1170, 460],
         //      ['Mar', 660, 1120],
         //      ['Apr', 1030, 540],
         //    ]);
           var data = google.visualization.arrayToDataTable( chartArr );

            var options = {
                chart: {
                  title: '',
                  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
                        animation: {
                    duration: 1000,
                    easing: 'out',
                    startup: true
                        },
                hAxis: {
                    title: 'Target vs Actual',
                  },
                vAxis: {
                  title: 'No Of Activity'
                },
                height : "500",
                width : "100%",
            };

          var chart = new google.visualization.ColumnChart(
            document.getElementById('targetVsActChrt'));

          chart.draw(data, options);
    }
}