/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 18-07-2018.
 * File : studentController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var custGroupFlag = '';

var studentData = []; // STUDENT JSON DATA
var custInfoData = [];
var currStudId = '';

var contactpersonData = [];

var multiPhoneData = [];
var multiEmailData = [];
var multiAddressData = [];
var multiGroupData = [];

var GBL_CUSTOMER_TOTAL_COUNT = 0;
var GBL_CUSTOMER_FETCH_FROM = 0;

var custCnt = 12;
var prvCnt = 0;
var gblStudId = '';
var gblStudName = '';
var GBL_USERLISTARR = [];

var GBL_Add = '';
var searchArr =[];
var renderStudData = [];

var GBL_SEARCHED_DATA = [];

var addCnt = 1;
// RENDER NEXT PAGE STUDENTS
function nextpageStudent(){
    // CHANGE MADE BY DEEPAK ON 01-07-2016
    if(renderStudData.length > custCnt){
        prvCnt = custCnt;
        custCnt = custCnt + 12;
        //$('#customerSearch').val('');
        renderDataStudent(renderStudData , '1');
    }else{
        if(renderStudData.length == undefined){
            prvCnt = custCnt;
            custCnt = custCnt + 12;
            renderDataStudent(studentData , '1');
        }else{
            toastr.warning("No more records available");
        }
    }
    
}

// RENDER PREV PAGE STUDENTS
function prevpageStudent(){

    if( custCnt != 12 ){
        custCnt = custCnt - 12;
        prvCnt = custCnt - 12;
        //$('#customerSearch').val('');
        if(renderStudData.length == undefined){
            //console.log("CUstomer Data ");
            renderDataStudent(studentData , '1');
        }else{
            //console.log("render Data ");
            renderDataStudent(renderStudData , '1');
        }
    }else{
        toastr.warning('No Previous Customer is available');
    }
}

// GET STUDENTS LISTING DATA
function getStudentData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStudentDetailData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11,45),
        limitFrom : GBL_CUSTOMER_FETCH_FROM,
        // customerId : 10,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getStudentDataCallback,"Please Wait... Getting Dashboard Detail");

}

// GET STUDENTS LISTING DATA CALLBACK
function getStudentDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        studentData = data.data;
        $("#lodingDivCustomer").show();

        GBL_CUSTOMER_FETCH_FROM  = GBL_CUSTOMER_FETCH_FROM + 1000;

        GBL_SEARCHED_DATA = data.data;

        GBL_CUSTOMER_TOTAL_COUNT = studentData[0].totalcustomerCount;
        
        searchCustomer();
        renderDataStudent(studentData , '1');
        // if(GBL_CUSTOMER_TOTAL_COUNT > 1000){
        //     callFunctionLoadMore(GBL_CUSTOMER_FETCH_FROM);
            // $("#lodedcount").html("Loaded - "+(GBL_SEARCHED_DATA.length)+" Records.");
        // }else{
            $("#lodingDivCustomer").hide();
        // }
        $("#searchresult").html("(Search Result : "+GBL_SEARCHED_DATA.length+")");
        
        // if( custGroupFlag != "1" ){
        //     gerCustGroup();
        // }

        if( localStorage.indeCampusStudentView == "List" ){
            showList();
        }

    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_STUD_LIST );
        else
            toastr.error(SERVERERROR);
    }
}

function callFunctionLoadMore(from){
    var limitFrom = from;
    
    if(limitFrom != GBL_CUSTOMER_TOTAL_COUNT){
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        GBL_CUSTOMER_FETCH_FROM = limitFrom;
        //GBL_CUSTOMER_FETCH_TO = limitTo;
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getCustomerDetailBySearch',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3,10),
            limitFrom : GBL_CUSTOMER_FETCH_FROM,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }

        commonAjax(FOLLOWUPURL, postData, callFunctionLoadMoreCallBack,"Please Wait... Getting Dashboard Detail");
    }
}

function callFunctionLoadMoreCallBack(flag,data){
    if (data.status == "Success" && flag) {
            //console.log(data.data);
            $.merge( studentData , data.data );
            //studentData.concat(data.data);
            if(GBL_CUSTOMER_FETCH_FROM >= GBL_CUSTOMER_TOTAL_COUNT){
                $("#lodingDivCustomer").hide();
                $("#lodedcount").html("Loaded - "+GBL_CUSTOMER_FETCH_FROM+" Records.");
            }else{
                $("#lodingDivCustomer").show();
                GBL_CUSTOMER_FETCH_FROM = GBL_CUSTOMER_FETCH_FROM + 1000;
                $("#lodedcount").html("Loaded - "+GBL_CUSTOMER_FETCH_FROM+" Records.");
                callFunctionLoadMore(GBL_CUSTOMER_FETCH_FROM);
            }
            
    } else {
        if (flag){
            //toastr.error(data.status);
            $("#lodingDivCustomer").hide();
            $("#lodedcount").html("Loaded - "+GBL_CUSTOMER_FETCH_FROM+" Records.");
        }
        else
            toastr.error(SERVERERROR);
    }
}

// RENDER STUDENT DATA GRID
function renderDataStudent(studentData , type){

    var newRow = '';
    
    renderStudData  = studentData;
    GBL_SEARCHED_DATA  = studentData;
    if ( studentData != "No record found" ){
        // START - CHANGE MADE BY DEEPAK ON 01-07-2016
        var countCustomer = 0;
        if(studentData.length > custCnt){
            countCustomer = custCnt;
        }else{
            countCustomer = studentData.length;
        }
        // END - CHANGE MADE BY DEEPAK ON 01-07-2016
        for( var i=prvCnt; i<countCustomer; i++ ){
            
            var studId = studentData[i].studId;
            var customerName = studentData[i].firstName + ' ' + studentData[i].lastName;
            var custName = customerName.trim();

            var Alias1 = '';
            var Alias2 = '';
            if( custName.length != 0 ){
                Alias1 = (custName.split(" ")[0][0]).toUpperCase();
                Alias2 = custName.split(" ")[1][0];

                var custNameAlias = '';
                if( Alias2 != undefined ){
                    custNameAlias = Alias1 + (Alias2).toUpperCase();
                }else{
                    custNameAlias = Alias1;
                }
            }

            if( custNameAlias == undefined ){
                custNameAlias = "";
            }

            // var companyName = studentData[i].CustomerInfo.CompanyName;
            var Name = custName;
            var Address = studentData[i].address;
            var cityName = (studentData[i].cityName != null ? studentData[i].cityName : "" );
            var stateName = (studentData[i].stateName != null ? studentData[i].stateName : "");
            var countryName = (studentData[i].countryName != null ? studentData[i].countryName : "");
            var pincode = ( studentData[i].pincode != "0" ? studentData[i].pincode : "");
            var address = studentData[i].address;
            var fullAddress = address + ' - ' + cityName + ' - '+ stateName + ' - ' + countryName + ' - ' + pincode;
            var contactPerson = "";
            var mobileNumber = studentData[i].mobileNo;
            if( studentData[i].multiPhoneData != "No record found" ){
                var mobileIndex = findIndexByKeyValue( studentData[i].multiPhoneData , 'defaultFlag' , 1 );
                if( mobileIndex != "-1" ){
                    mobileNumber = studentData[i].multiPhoneData[mobileIndex].phoneNo;
                }
            }
            // var OutStanding = studentData[i].CustomerInfo.OutStanding;
            // var TurnOver = studentData[i].CustomerInfo.TurnOver;
            // var TagLine = studentData[i].CustomerInfo.custType;

            // if( studentData[i].CustomerInfo.Members != "No record found"){
                
            //     for ( var j=0; j<studentData[i].CustomerInfo.Members.length; j++ ){
            //         var txtClr = "";
            //         var txtIcon = "";
            //         if( j==0 ){
            //             txtClr = "text-blue";
            //             txtIcon = "fa fa-handshake-o";
            //         }else if( j==1 ){
            //             txtClr = "text-light-green";
            //             txtIcon = "fa fa-child";
            //         }else{
            //             txtClr = "text-red";
            //             txtIcon = "fa fa-child";
            //         }
            //         var userName = studentData[i].CustomerInfo.Members[j].userName;
            //         contactPerson += '<span class="margin-bottom-5"><b><i class="'+txtIcon+' '+txtClr+'"></i></b> '+ userName +'</span>';
            //     }
            // }

            newRow += '<div class="col-md-4 col-sm-6 col-xs-12">'+
                        '<div class="card card-event card-clickable clickable-pad card-user material-animate">'+
                            '<div class="card-heading card-heading-changes">'+
                                '<div class="col-md-3 col-sm-3  col-xs-3"><a href="javascript:void(0)" class="" id="" title=""><span class="user-image user-text" custId="'+ studId +'" onclick="navigateToCustomerDetailPage(this)"><div class="customer-img"><img src="http://'+ FINALPATH +'/assets/globals/img/avtar1.png" alt="" class="user-image"></div><b>'+ custNameAlias +'</b></span></a></div>'+
                                '<div class="col-md-9 col-sm-9 col-xs-9 customer-block">'+
                                    // '<div class="customer-tag">'+
                                    //     '<div class="tagline">'+
                                    //         // '<span>'+ TagLine +'</span>'+
                                    //     '</div>'+
                                    // '</div>'+
                                    '<h3 class="card-title font-changes">'+ Name +'</h3>'+
                                    '<div class="subhead font_titles_size font-left"><span class="margin-bottom-10" id="outAdd_'+ studId +'" data-toggle="popover" data-placement="bottom" title="Address" data-content="'+ fullAddress +'">'+ cityName +' - '+ stateName +' - '+ countryName +'</span></div>'+
                                    '<div class="subhead font-left" style="color: #008ab1;"><span class="margin-bottom-10" data-placement="bottom" title="Mobile Number" id="defaultMobile">'+ mobileNumber +'</span></div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="card-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-6">'+ contactPerson +
                                    '</div>'+
                                    '<div class="col-md-6">'+
                                        '<div class="row">'+
                                            '<div class="col-md-12 col-sm-9 col-xs-11 margin-bottom-10">'+
                                                '<p class="p-turn-stock">'+
                                                    '<span class="label label-success" data-toggle="popover" data-placement="bottom" title="Invoices" data-content="'+ 0 +'"><i class="fa fa-rupee"></i> '+ 0 +'</span>'+
                                                    '<span class="label label-danger" data-toggle="popover" data-placement="bottom" title="Ledgers" data-content="'+ 0 +'"> <i class="fa fa-rupee"></i> '+ 0 +'</span></p>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="clickable-button">'+
                                '<div class="layer bg-amber"></div>'+
                                '<a class="btn btn-floating btn-amber initial-position floating-open" custName="'+ Name +'" studId="'+ studId +'" onclick="getStudentInfoData(this);"><i class="ion-android-more-horizontal"></i></a>'+
                            '</div>'+
                            '<div class="layered-content bg-amber" id="card_'+ studId +'">'+
                            '</div>'+
                        '</div>'+
                    '</div>';
            }
        }
        $('#customerList').html(newRow);

        Pleasure.handleMaterialAnimation();
        Pleasure.listenClickableCards();
        Pleasure.handleTooltipsAndPopovers();
        Layout.init();    
}

// GET DETAIL DATA OF SELECTED STUDENT
function getStudentInfoData(context){                                       

    closeCard();

    multiPhoneData = [];
    multiEmailData = [];
    multiAddressData = [];

    var studId = $(context).attr('studId');

    gblStudName = $(context).attr('custName');
    gblStudId = studId;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStudentDetailData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11,45),
        studentId: studId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    commonAjax(FOLLOWUPURL, postData, getStudentInfoDataCallback,"Please Wait... Getting Dashboard Detail");

    function getStudentInfoDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            // console.log(data);

            custInfoData = data.data;
            // contactpersonData = data.data[0].CustomerInfo.multiContactData;
            renderStudentInfoData();
            // getLastLevelUserListing();
            
           
        } else {
            closeCard();
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_CUST_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RENDER STUDENT DATA
function renderStudentInfoData(){

    var studId = gblStudId;
    var newRow = '';

    newRow += '<h4 class="text-white text-name-cust">'+ gblStudName +'</h4>'+
                '<div class="overflow-content">'+
                    '<div class="row">'+
                        '<div class="col-md-6">'+
                            '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="studentDetails(this);">Details</button>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="paymentDetails(this);">Payment</button>'+
                        '</div>'+
                        // '<div class="col-md-6">'+
                        //    '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="productDetails(this);">Products</button>'+
                        // '</div>'+
                    '</div>'+
                    '<div class="row">'+  
                        '<div class="col-md-6">'+
                            '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="invoiceDetails(this);">Invoices </button>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="unprocessInvoiceDetails(this);">Unprocessed Inv. </button>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row">'+
                       
                        '<div class="col-md-6">'+
                            '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="ledgerDetails(this)">Ledger</button>'+
                        '</div>'+
                    '</div>'+
//                    '<div class="row">'+
//                        '<div class="col-md-6">'+
//                            '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="salesDetails(this)">Sales Order</button>'+
//                        '</div>'+
                    // '<div class="row">'+
                    //     '<div class="col-md-6">'+
                    //         '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="inqDetail(this)">Inquiries</button>'+
                    //     '</div>'+
                    //     '<div class="col-md-6">'+
                    //         '<button type="button" class="btn btn-default btn-white btn-ripple" style="font-size:15px; width: 100%;" studId="'+ studId +'" onclick="inventoryDetails(this)">Activities</button>'+
                    //     '</div>'+
                    // '</div>'+
                '</div>'+
                '<div class="clickable-close-button">'+
                    '<a class="btn btn-floating initial-position floating-close" onclick="closeCard();"><i class="ion-android-close"></i></a>'+
                '</div>';

        $('#card_'+studId).html( newRow );
}

// function gerCustGroup(){

//     var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//     // Followup Listing Todays, Delayed, Upcomming
//     var postData = {
//         requestCase: 'userCustomerGroupListing',
//         clientId: tmp_json.data[0].FK_CLIENT_ID,
//         userId: tmp_json.data[0].PK_USER_ID,
//         user_Id: tmp_json.data[0].PK_USER_ID,
//         orgId: checkAuth(17,66),
//         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//         searchType : "CUSTOMER"
//     }

//     commonAjax(FOLLOWUPURL, postData, gerCustGroupCallback,"Please Wait... Getting Dashboard Detail");
// }


// function gerCustGroupCallback(flag,data){

//     if (data.status == "Success" && flag) {
        
//         var newRow = '';
//         for( var i=0; i<data.data.length; i++ ){

//             var name = data.data[i].groupName;
//             var groupId = data.data[i].groupId;
            
//             newRow += '<option value="'+ groupId +'|--|'+ name +'"> '+ name +' </option>';
//         }

//         $('#custEditGroup').html(newRow);
//         $('#custEditGroup').selectpicker();

//         custGroupFlag = '1';

//     } else {
//         if (flag) {
//             //toastr.error(data.status);
//         } else {
//             toastr.error(SERVERERROR);
//         }
//     }
// }

// DISPLAY SELECTED STUDENT'S DETAILS ON CARD
function studentDetails(context){

    var studId = $(context).attr('studId'); 

    currStudId = studId;

    var custData = custInfoData[0];
    var contactData = custInfoData[0].multiPhoneData;
    var emailData = custInfoData[0].multiEmailData;
    var addressData = custInfoData[0].multiAddressData;
    var groupData = custInfoData[0].groupData;

    multiGroupData = [];

    if( custData != "No record found" ){

        var landlineIndex = findIndexByKeyValue( custInfoData[0].multiPhoneData,'defaultFlag','1' )
        var mailIndex = findIndexByKeyValue( custInfoData[0].multiEmailData,'defaultFlag','1' )
        var addressIndex = findIndexByKeyValue( custInfoData[0].multiAddressData,'defaultFlag','1' )

        var defLandline = "";
        if( landlineIndex != -1 ){
            defLandline = custInfoData[0].multiPhoneData[landlineIndex].phoneNo;
        }

        var defEmail = "";
        if( mailIndex != -1 ){
            defEmail = custInfoData[0].multiEmailData[mailIndex].emailAddress;
        }

        var defAddress = "";
        var defCA = "";
        if( addressIndex != -1 ){
            defAddress = custInfoData[0].multiAddressData[addressIndex].address;
            // defCA = custInfoData[0].multiAddressData[addressIndex].catchArea;
        }

        $('#custName').html(gblStudName);
        // $('#custName').html(custData.Name +' <a href="javascript:void(0)" style="font-size:20px;" onclick="groupEdit()"><i class="fa fa-pencil"></i></a>');
        // $('#custLandline').html(defLandline);
        $('#custMobile').html(defLandline);
        $('#custEmail').html(defEmail);
        $('#custAddress').html(defAddress);
        // $('#catchAr').html(defCA);
        $('#aadharNo').html(custData.aadharNo);
        $('#academicYear').html(custData.acdemicYear);
        var birthdate = ( custData.birthDate != "0000-00-00" && custData.birthDate != "" && custData.birthDate != "1970-01-01" ? mysqltoDesiredFormat(custData.birthDate,'dd-MM-yyyy') : "" );
        var anniversarydate = ( custData.aniversaryDate != "0000-00-00" && custData.aniversaryDate != ""  && custData.aniversaryDate != "1970-01-01" ? mysqltoDesiredFormat(custData.aniversaryDate,'dd-MM-yyyy') : "" );

        $('#birthDate').html( birthdate );
        $('#annivDate').html( anniversarydate );
        $('#selectedCourse').html(custData.course);
        $('#custCity').html(custData.cityName);
        $('#custState').html(custData.stateName);
        $('#custCountry').html(custData.countryName);
        $('#pincode').html(custData.pincode);
        // var contactPerson = "";
        // if( custInfoData[0].CustomerInfo.Members != "No record found"){
            
        //     for ( var j=0; j<custInfoData[0].CustomerInfo.Members.length; j++ ){

        //         var userName = custInfoData[0].CustomerInfo.Members[j].userName;
        //         contactPerson += '<span class="margin-bottom-5"><b><i class="fa fa-child"></i></b> '+ userName +'</span><br />';
        //     }
        //     $("#customerUserHerierchy").html(contactPerson);
        // }
        
        // var custGroupData = custInfoData[0].CustomerInfo.groupData;

        // if( custGroupData != 'No record found' ){

        //     var grp = '';
        //     for( var i=0; i<custGroupData.length; i++ ){
        //         // ADDED BY DEEPAK PATIL ON 01-07-2016
        //         var seperator = "";
        //         if(i!=0){
        //             seperator = ",";
        //         }
        //         grp += seperator+custGroupData[i].groupName;
        //     }
        //     $('#custGroup').html(grp);
        // } else { $('#custGroup').html(''); }

        // var DOB = '';
        // var DOA = '';
        // if( custData.DOB != "1970-01-01" && custData.DOB != "" && custData.DOB != "0000-00-00"){
        //     DOB = mysqltoDesiredFormat(custData.DOB,'dd-MM-yyyy');
        // }
        // if( custData.DOA != "1970-01-01" && custData.DOA != "" && custData.DOA != "0000-00-00"){
        //     DOA = mysqltoDesiredFormat(custData.DOA,'dd-MM-yyyy');
        // }
        
  //       $('#custType').html(custData.CustomerTypeName);
  //       $('#custDesignation').html(custData.Designation);
  //       $('#custPan').html(custData.PanNo);
  //       $('#custGstTinNo').html(custData.gstTinNo);//ADD BY DARSHAN PATEL ON 23-3-2018
  //       $('#custEthinicity').html(custData.Ethinicity);
  //       // $('#custOccupation').html(custData.OccupationName);
  //       $('#indTypeTxt').html(custData.indTypeTxt);
  //       $('#rateTypeTxt').html(custData.rateTypeText);
  //       // $('#custDOB').html(DOB);
  //       // $('#custDOA').html(DOA);
  //       $('#custWebAdd').html('<a href="'+ httpHead +'//'+ custData.custWebsite +'">'+ custData.custWebsite +'</a>');
        
		// $('#inqCount').html(custData.totalInqCount);
		// $('#orderCount').html(custData.totalOrderCount);
		
  //       if( custData.Members != 'No record found' ){
  //           var newRow = "";
  //           for( var j=0; j<custData.Members.length; j++ ){
                    
  //               newRow += '<span style="width:100%; float:left"><i class="fa fa-child icon-pad"></i><span id="member'+ i +'"> '+ custData.Members[j].name +' </span>&nbsp;<a class="btn btn-sm btn-flat cust-a-pad" onclick="custEdit()"><i class="fa fa-pencil"></i></a></span>'
  //           }
  //           $('#memberListEdit').html( newRow );
  //       }

        if ( contactData != 'No record found' ){
            multiPhoneData = contactData;
        }

        if ( emailData != 'No record found' ){
            multiEmailData = emailData;
        }
        
        if ( addressData != 'No record found' ){
            multiAddressData = addressData;
        }

        // if ( groupData != 'No record found' ){
        //     multiGroupData = groupData;
        // }

        $('#cust-details-1').modal('show');


        // custom customFieldsArr

        // var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
        // if(localJson.data.customFieldDetail != "No record found"){
        //     var divStruct = '<div class="col-md-4 no-pad-lr" style="font-size:14px;">';
        //     for(var j=0;j<localJson.data.customFieldDetail.length;j++){
        //         if(localJson.data.customFieldDetail[j]['tabType'] == "CUSTOMER"){
        //             // console.log(localJson.data.customFieldDetail[j]);
        //             divStruct += '<div>';
        //             if(localJson.data.customFieldDetail[j].fieldType == "text"){
        //                 divStruct += '<div class="col-md-12">';
        //                 divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span >'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
        //                 divStruct += '<div class="col-md-12">';
        //                 divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
        //                 divStruct += '<div class="col-md-12">';
        //                 divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
        //                 divStruct += '<div class="col-md-12">';
        //                 divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
        //                 divStruct += '<div class="col-md-12">';
        //                 divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
        //                 divStruct += '<div class="col-md-12">';
        //                  divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
        //                 divStruct += '<div class="col-md-12">';
        //                 divStruct += '<p class="margin-bottom-05" style="font-size:14px;"><span class="bold">'+localJson.data.customFieldDetail[j]['fieldName']+' : </span>';
        //                 divStruct += '<span>'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</span></p>';
        //             }
        //             divStruct += '</div>'+
        //             '</div>';
        //             if(((j+1)%3) == 0){
        //                 divStruct += '</div><div class="col-md-4 no-pad-lr">';

        //             }   
        //         }
                
        //     }
        //     $("#customFieldDataCust").html(divStruct);
        // }
        // $('.selectPckr').selectpicker('refresh');

        // var indexCustom = findIndexByKeyValue( localJson.data.customFieldDetail , "tabType" , "CUSTOMER" );

        // if( indexCustom == "-1" ){
            $('#moreDet').hide();
        // }
    
    }else{
        // toastr.warning('No record found');
        displayAPIErrorMsg( "" , GBL_ERR_NO_CUST_DATA );
    }

}


// function productDetails(context){
    
//     var custId = $(context).attr('custId'); 

//     currStudId = custId;

//     var productData = custInfoData[0].ProductDetail;
//     var newRow = '';
//     if( productData != "No record found" ){

//         for( var i=0; i<productData.length; i++ ){

//             newRow += '<tr>'+
//                         '<td>'+ productData[i].ProductName +'</td>'+
//                         //'<td>'+ productData[i].Price +'</td>'+
//                         '<td>'+ productData[i].Quantity +'</td>'+
//                         '<td>'+ productData[i].UOM +'</td>'+
//                         '<td class="amtDisp">'+ numberFormat( productData[i].Value ) +'</td>'+
//                    ' </tr>';
//         }

//         $('#productData').html(newRow);
//         $('#productDT').DataTable();
//         $('#product-details').modal('show');
//     }else{
//         // toastr.warning('No record found');
//         displayAPIErrorMsg( "" , GBL_ERR_NO_ITEMS );
//     }
// }

// GET LEDGER DETAILS OF STUDENT
function ledgerDetails(context){
    
    var custId = $(context).attr('custId'); 
    currStudId = custId;

    // var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // // Ledger Details
    // var postData = {
    //     requestCase: 'listLedgerData',
    //     clientId: tmp_json.data[0].FK_CLIENT_ID,
    //     userId: tmp_json.data[0].PK_USER_ID,
    //     orgId: checkAuth(4, 16),
    //     dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    //     studentId: studId,
    // };
    // commonAjax(FOLLOWUPURL, postData,getLedgerDetailsCallBack,CREATINGINQDATA);

    // function getLedgerDetailsCallBack(flag, data){
    //     if (data.status == "Success" && flag) {
            
    //         console.log(data.data);
    //         var ledgerData = data.data;
    //         renderLedger( ledgerData );
            
    //     }else {
    //         if (flag)
                // toastr.error(data.status);
                // displayAPIErrorMsg( data.status , GBL_ERR_NO_LEDGER );
                displayAPIErrorMsg( "" , GBL_ERR_NO_LEDGER );
    //         else
    //             toastr.error(SERVERERROR);
    //     }
    // }
}

// RENDER LEDGER DETAILS OF STUDENT
function renderLedger( ledgerData ) {

    // var newRow = '';
    // var creditBal = '';
    // var debitBal = '';
    // var thead = '<table class="display datatables-alphabet-sorting" id="ledgerTable">'+
    //                 '<thead>'+
    //                     '<tr>'+
    //                         '<th>Date</th>'+
    //                         '<th>Voucher Type</th>'+
    //                         '<th>Voucher Number</th>'+
    //                         '<th>Credit</th>'+
    //                         '<th>Debit</th>'+
    //                         '<th>Closing Balance</th>'+
    //                     '</tr>'+
    //                 '</thead>'+
    //                 '<tbody id="ledgerData">'+
    // var tEnd = '</tbody></table>';

    // if( ledgerData != 'No record found' ){
        
    //     ledgerData.forEach(function( record , index ){
    //         if( record.type == 'DR' ){
    //             debitBal = ( record.Amount != "" ? '&#8377; ' + numberFormat(record.Amount) : "--" );
    //             creditBal = '--';
    //         }else{
    //             creditBal = ( record.Amount != "" ? '&#8377; ' + numberFormat(record.Amount) : "--" );
    //             debitBal = '--';
    //         }
    //         newRow += '<tr>'+
    //                     '<td>'+ record.entryDate +'</td>'+
    //                     '<td>'+ record.vchType +'</td>'+
    //                     '<td>'+ record.vchNo +'</td>'+
    //                     '<td class="amtDisp">'+  creditBal  +'</td>'+
    //                     '<td class="amtDisp">'+  debitBal +'</td>'+
    //                     '<td class="amtDisp">&#8377; '+ record.closeAmt +'</td>'+
    //                '</tr>';
    //     });

    //     
    // }
    // $('#ledgerDataDiv').html( thead + newRow + tEnd );
    // $('#leger-details').modal('show');
    // $('#ledgerTable').DataTable()

}

// GET AND DISPLAY LEDGER DETAILS
function paymentDetails(context){
    
    var custId = $(context).attr('custId'); 
    currStudId = custId;

    // var paymentData = custInfoData[0].paymentDetail;
    // var thead = ' <table class="table table-bordered table-striped table-condensed cf" id="paymentTable">'+
    //                 '<thead>'+
    //                     '<tr>'+
    //                         '<th>Receipt Id</th>'+
    //                         '<th>Invoice Number</th>'+
    //                         '<th>Total Amount</th>'+
    //                         '<th>Narration</th>'+
    //                         '<th>Paid Date</th>'+
    //                     '</tr>'+
    //                 '</thead>'+
    //                 '<tbody>';
                        
    // var tfoot = '</tbody></table>';
    // var newRow = '';
    // if( paymentData != 'No record found' ){

    //     paymentData.forEach(function( record , index ){
            
    //         newRow += '<tr>'+
    //                         '<td>'+ record.receiptId +'</td>'+
    //                         '<td>'+ record.invoiceNum +'</td>'+
    //                         '<td class="amtDisp">&#8377; '+ numberFormat(record.totalAmt) +'</td>'+
    //                         '<td>'+ record.narration +'</td>'+
    //                         '<td>'+ mysqltoDesiredFormat( record.voucherDate , 'dd-MM-yyyy' ) +'</td>'+
    //                   '</tr>';
    //     });

    //     $('#payHistory').html(thead + newRow + tfoot);
    //     $('#paymentTable').DataTable();
    //     $('#payment-details').modal('show');
    
    // }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_PAYMENT );
    // }
}

// GET INVOICE DATA
function invoiceDetails(context){

    var studId = $(context).attr('studId'); 
    currStudId = studId;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'listInvoiceData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23, 94),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        studentId: studId,
    };
    commonAjax(FOLLOWUPURL, postData,getInvoiceDataCallBack,CREATINGINQDATA);

    function getInvoiceDataCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            console.log(data.data);
            var invoiceData = data.data;
            renderInvoice( invoiceData );
            
        }else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_INVOICE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RENDER INVOICE DATA
function renderInvoice(invoiceData) {
    
    var thead = '<table id="invoiceTable" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>Sr. No.</th>'+
                            '<th>Invoice Name</th>'+
                            '<th>Start Date</th>'+
                            '<th>End Date</th>'+
                            '<th>Total Charges</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';                        
    var tEnd = '</tbody></table>';
    var newRow = "";
    if( invoiceData != "No record found" ){

        invoiceData.forEach( function( record , index ){

            newRow += '<tr>'+
                        '<td>'+ (index+1) +'</td>'+
                        '<td>'+ record.invoiceName +'</td>'+
                        '<td>'+ ( record.invoiceStartDate != "0000-00-00" && record.invoiceStartDate != "" ? mysqltoDesiredFormat(record.invoiceStartDate,'dd-MM-yyyy') : "" ) +'</td>'+
                        '<td>'+ ( record.invoiceEndDate != "0000-00-00" && record.invoiceEndDate != "" ? mysqltoDesiredFormat(record.invoiceEndDate,'dd-MM-yyyy') : "" ) +'</td>'+
                        '<td><i class="fa fa-rupee"></i> '+ record.totalCharges +'</td>'+
                    '</tr>';
           
        });
        $('#dtTableInvoice').html( thead + newRow + tEnd );
        $('#invoiceTable').DataTable();
        modalShow('invoiceModal');
        // $('#invoice-details').modal('show');    
       
    }
}

// function getInvoiceData( invId ){

//     var invoiceId = invId;
//     var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//     // Inquiry Details
//     var postData = {
//         requestCase: 'getInvoiceDetail',
//         clientId: tmp_json.data[0].FK_CLIENT_ID,
//         userId: tmp_json.data[0].PK_USER_ID,
//         orgId: checkAuth(4, 16),
//         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//         invoiceId: invoiceId,
//     };
//     commonAjax(INQURL1, postData, getInvoiceDataCallBack,CREATINGINQDATA);

//     function getInvoiceDataCallBack(flag, data){
//         if (data.status == "Success" && flag) {
            
//             renderInvoiceDataInTable( data.data , invoiceId );

//         }else {
//             if (flag)
//                 // toastr.error(data.status);
//                 displayAPIErrorMsg( data.status , GBL_ERR_NO_INVOICE_DATA );
//             else
//                 toastr.error(SERVERERROR);
//         }
//     }
// }

// function renderInvoiceDataInTable( invoiceData , invoiceId ){


//     var tableRow = '<table class="display datatables-alphabet-sorting" id="invoiceDataTbl_'+ invoiceId +'">'+
//                         '<thead>'+
//                             '<tr>'+
//                                 '<th>Sr No</th>'+
//                                 '<th>Item</th>'+
//                                 '<th>Qty</th>'+
//                                 '<th>Discount</th>'+
//                                 '<th>Tax</th>'+
//                                 '<th>Price</th>'+
//                             '</tr>'+
//                         '</thead>'+
//                         '<tbody>';
    
//     if( invoiceData.itemDetail != "No record found" ){

//         invoiceData.itemDetail.forEach( function( record , index ){

//                 var amtTypeDis = ( (record.disType == "percentage") ? record.disAmount+"%" : "<i class='fa fa-rupee'></i>"+record.disAmount );
//                 var amtTypeAmt = ( (record.disType == "percentage") ? record.taxAmount+"%" : "<i class='fa fa-rupee'></i>"+record.taxAmount );

//                 tableRow += '<tr>'+
//                             '<td>'+ record.srNo +'</td>'+
//                             '<td>'+ record.itemName +'</td>'+
//                             '<td>'+ ( (record.itemQty == 0) ? "--" : record.itemQty ) +'</td>'+
//                             '<td>'+ amtTypeDis +'</td>'+
//                             '<td>'+ amtTypeAmt +'</td>'+
//                             '<td><i class="fa fa-rupee"></i>'+ record.itemRate +'</td>'+
//                         '</tr>';
//         });      
//     }

//     if( invoiceData.taxDetail != "No record found" ){
        
//         invoiceData.taxDetail.forEach( function( record , index ){

//                 tableRow += '<tr>'+
//                             '<td><span style="display:none">'+ parseInt( invoiceData.itemDetail.srNo ) + index +'</span></td>'+
//                             '<td></td>'+ 
//                             '<td></td>'+
//                             '<td></td>'+
//                             '<td>'+ record.taxName +'</td>'+
//                             '<td><i class="fa fa-rupee"></i>'+ numberFormat( parseFloat( record.taxRate ).toFixed(2) ) +'</td>'+
//                         '</tr>';
//         });      
//     }

//     tableRow += '</tbody>'+
//                     '</table>';  

//     $('#Inv_'+invoiceId).html(tableRow);

//     $('#invoiceDataTbl_'+ invoiceId ).DataTable({ "stateSave": true });
// }

// function agingDetails(context){
    
//     var custId = $(context).attr('custId'); 
//     currStudId = custId;
      
//     var AgingDetail = custInfoData[0].AgingDetail;

//     if( AgingDetail != "No record found" && AgingDetail != null){

//         setTimeout(function(){
//           google.charts.setOnLoadCallback(drawBarColors);
//         },100);

//         function drawBarColors() {

//               var chartArr = [];
//               var firstArr = [];

//               // firstArr.push( 'Element' );
//               // firstArr.push( 'Aging' );
//               // firstArr.push( { role: 'style' } );
//               // firstArr.push( { role: 'annotation' } );

//               // chartArr.push( firstArr );
//               //   AgingDetail.forEach(function(record,index) {
                    
//               //     var clrCode = "";
//               //     if( index == 0 ){
//               //         clrCode = "#b87333"
//               //     }else if( index == 1 ){
//               //         clrCode = "silver"
//               //     }else if( index == 2 ){
//               //         clrCode = "gold"
//               //     }else if( index == 3 ){
//               //         clrCode = "#e5e4e2"
//               //     }else if( index == 4 ){
//               //         clrCode = "#b87333"
//               //     }else{
//               //         clrCode = "silver"
//               //     }
//               //     var temp = [];
//               //     temp.push(record);
//               //     temp.push(clrCode);
//               //     chartArr.push( temp );
//               // });
//               var data = google.visualization.arrayToDataTable([
//                  ['Element', 'Aging', { role: 'style' } , { role: 'annotation' }],
//                  ['30' ,AgingDetail.thirty,  '#b87333',AgingDetail.thirty],            // RGB value
//                  ['60' ,AgingDetail.sixtty,  'silver',AgingDetail.sixtty],            // English color name
//                  ['120' ,AgingDetail.oneTwenty, 'gold',AgingDetail.oneTwenty],
//                  ['180' ,AgingDetail.oneEighty,  'color: #e5e4e2' ,AgingDetail.oneEighty], // CSS-style declaration
//                  ['180+' ,AgingDetail.oneEightyPlus,  'color: #e5e4e2' ,AgingDetail.oneEightyPlus], // CSS-style declaration
//               ]);
              
//               // var data = google.visualization.arrayToDataTable(chartArr);
//               var options = {
//                 title: '',
//                 height : "500",
//                 width : "100%",
//                 colors: ['#b87333', '#ffab91'],
//                 hAxis: {
//                   title: 'Total Aging',
//                   // minValue: 0
//                 },
//                 vAxis: {
//                   title: 'Aging',
//                   // minValue: 0
//                 },
//                 animation: {
//                     duration: 1000,
//                     easing: 'out',
//                     startup: true
//                 }
//               };
//               var chart = new google.visualization.BarChart(document.getElementById('barChartContainer'));
//               chart.draw(data, options);
//         }

//         $('#aging-modal').modal('show')
//     }else{
//         // toastr.warning('No record found');
//         displayAPIErrorMsg( "" , GBL_ERR_NO_AGING );
//     }
// }

// function salesDetails(context){
    
//     var custId = $(context).attr('custId'); 
//     currStudId = custId;
    
//     var salesData = custInfoData[0].SalesDetail;
    
//     if( salesData != "No record found" ){

//         var newRow = '';

//         for( var i=0; i<salesData.length; i++ ){

//             newRow += '<div class="panel">'+
//                         '<div class="panel-heading">'+
//                             '<a class="panel-title panel-width" data-toggle="collapse" data-parent="#accordion" href="#collapse'+ i +'Order"><span style="font-size:14px; text-align:left"><span class="open-sales-status">'+ salesData[i].Status +'</span></span> Order # : '+ salesData[i].OrderNo +' <span class="pull-right"> <i class="fa fa-rupee"></i> '+ salesData[i].Amount +'</span></a>'+
//                         '</div>'+
//                         '<div id="collapse'+ i +'Order" class="panel-collapse collapse">'+
//                             '<div class="panel-body">'+
//                                 '<div class="overflow-table">'+
//                                     '<table class="display datatables-alphabet-sorting" id="mtdCollection">'+
//                                         '<thead>'+
//                                             '<tr>'+
//                                                 '<th>Item</th>'+
//                                                 '<th>Quantity</th>'+
//                                                 '<th>Rate</th>'+
//                                                 '<th>value</th>'+
//                                             '</tr>'+
//                                         '</thead>'+
//                                         '<tbody id="'+ salesData[i].OrderNo +'">'+
//                                         '</tbody>'+
//                                     '</table>'+
//                                 '</div>'+
//                             '</div>'+
//                         '</div>'+
//                     '</div>';
//         }

//         $('#accordion1.Sales').html(newRow);

//         for( var i=0; i<salesData.length; i++ ){

//             var id = salesData[i].OrderNo;

//             var tableRow = '';
//             for( var j=0; j<salesData[i].InvoiceData.length; j++ ){

//                 tableRow += '<tr>'+
//                             '<td>'+ salesData[i].InvoiceData[j].Item +'</td>'+
//                             '<td>'+ salesData[i].InvoiceData[j].Quantity +'</td>'+
//                             '<td>'+ salesData[i].InvoiceData[j].Rate +'</td>'+
//                             '<td class="amtDisp">'+ numberFormat( salesData[i].InvoiceData[j].Value ) +'</td>'+
//                         '</tr>';
//             }

//             $('#'+id).html(tableRow);
//         }

//         TablesDataTables.init();
//         TablesDataTablesEditor.init();

//         $('#sales-details').modal('show');
//     }
//     else{
//         // toastr.warning('No record found');
//         displayAPIErrorMsg( "" , GBL_ERR_NO_SALES );
//     }
// }

// function inqDetail(context){
    
//     var custId = $(context).attr('custId'); 
//     currStudId = custId;
    
//     var inqData = custInfoData[0].inqListing;
    
//     if( inqData != "No record found" ){
//         var tableHeader = '<table class="table table-bordered table-striped table-condensed cf" id="inqDataTable">'+
//             '<thead>'+
//                 '<tr>'+
//                     '<th>Inquiry No</th>'+
//                     '<th>Inquiry Name</th>'+
//                     '<th>Inquiry Remark</th>'+
//                     '<th>Status</th>'+
//                     '<th>Owner</th>'+
//                 '</tr>'+
//             '</thead>'+
//             '<tbody id="inqDetailDataBody">';
//         var tableFooter = '</tbody>'+
//         '</table>';
//         var newRow = '';

//         for( var i=0; i<inqData.length; i++ ){

//             newRow += '<tr>'+
//                             '<td><a href="javascript:void(0)" onclick="navigateToInquiryDetail(this)" custid="'+currStudId+'" inqid="'+inqData[i]['inqNo']+'">'+inqData[i]['clientLevelInqNo']+'</a></td>'+
//                             '<td>'+inqData[i]['inqName']+'</td>'+
//                             '<td>'+inqData[i]['inqRemark']+'</td>'+
//                             '<td>'+inqData[i]['inqStatus']+'</td>'+
//                             '<td>'+inqData[i]['inqOwner']+'</td>'+
//                         '</tr>';
//         }

//         $('#inqDetailData').html(tableHeader + newRow + tableFooter);
        
//         $("#inqDataTable").DataTable({ "stateSave": true });
//         $('#inq-details').modal('show');
//     }
//     else{
//         // toastr.warning('No record found');
//         displayAPIErrorMsg( "" , GBL_ERR_NO_INQ );
//     }
// }

// function inventoryDetails(context){

//     var custId = $(context).attr('custId'); 
//     currStudId = custId;
    
//     var ActivityData = custInfoData[0].ActivityDetail;

//     if( ActivityData != "No record found" ){

//         var newRow = '';
//         var count = 1 ;
//         var thead = '<table id="tableListingAct" class="table table-bordered table-striped table-condensed cf">'+
//                         '<thead>'+
//                             '<tr>'+
//                                 '<th>Sr. No</th>'+
//                                 '<th>'+
//                                     'Activity No'+
//                                 '</th>'+
//                                 '<th>Activity Title</th>'+
//                                 '<th>Mode</th>'+
//                                 '<th>Due Date</th>'+
//                                 '<th>Assigned To</th>'+
//                                 '<th>Status</th>'+
//                             '</tr>'+
//                         '</thead>'+
//                         '<tbody id="inventoryData">';

//         var tfoot = '</tbody></table>';

//         for( var i=0; i<ActivityData.length; i++ ){

//             count = i + 1;
//             newRow += '<tr>'+
//                         '<td>'+ count +'</td>'+
//                         '<td data-activityid="'+ ActivityData[i].activityId +'" onclick="navigateToActivityAppDetailsPage(this)" style="cursor:pointer;"><a href="javascript:void(0)">'+ ActivityData[i].clientLevelActId +'</a></td>'+
//                         '<td>'+ ActivityData[i].activityTitle +'</td>'+
//                         '<td>'+ ActivityData[i].activityMode +'</td>'+
//                         '<td>'+ ( (ActivityData[i].activityDueDate == "0000-00-00") ? "--" : mysqltoDesiredFormat( ActivityData[i].activityDueDate , 'dd-MM-yyyy') ) +'</td>'+
//                         '<td>'+ ActivityData[i].activityAssignTo +'</td>'+
//                         '<td>'+ ActivityData[i].activityStatus +'</td>'+
//                     ' </tr>';
//         }

//         $('#dtTableActivity').html(thead + newRow + tfoot);
//         $("#tableListingAct").DataTable({ "stateSave": true });

//         $('#inventory-details').modal('show');
//     }else{
//         displayAPIErrorMsg( "" , GBL_ERR_NO_ACT );
//     }
// }

// PROVIDE SEARCH FUNCTIONALITY FOR STUDENTS
function searchCust(){

    var custIndex = findIndexByKeyValue(studentData,"CustId",searchedCustId);
    if( custIndex != -1 ){
        
        var searchCustData = [];
        searchCustData.push( studentData[custIndex] );
        custCnt = 1;
        prvCnt = 0;
        renderDataStudent( searchCustData , '0' );
        localStorage.removeItem('POTGselectedCustIdInSearch');

    }else{
        toastr.warning('Customer Not Found');
        $('#customerSearch').val('');
        renderDataStudent( studentData , '1' );
    }
}

// OPEN CONTACT DATA MODAL AND DISPLAY DATA AVAILABLE, ALLOW TO ADD/DELETE THEM
function custEdit(){

    var MembersData = multiPhoneData;
    var newRow = '';

    $('#newPhoneNum').val('');
    $('#phoneList').html('');

    if( MembersData != 'No record found' ){

        for( var i=0; i<MembersData.length; i++ ){
            var defaultFlag = '';
            if ( MembersData[i].defaultFlag == "1" ){
                defaultFlag = 'checked';
            }

            newRow += '<tr>'+
                        '<td><div class="radioer form-inline">'+                                                    
                                '<input id="radio-'+ MembersData[i].phoneId +'" type="radio" '+ defaultFlag +' name="isDefaultContact">'+
                                '<label for="radio-'+ MembersData[i].phoneId +'"></label>'+
                            '</div></td>'+ 
                        '<td>'+ MembersData[i].phoneNo +'</td>'+
                        '<td><a href="javascript:void(0)" id="'+ MembersData[i].phoneId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="phone" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                    ' </tr>';
        }
        $('#phoneList').html( newRow );
        
    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_MOBILE );
    }
    
    $('#cust-edit-phone').modal('show');
 
    addFocusId( 'newPhoneNum' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    changePhone(); // Initialize for Default change
}

// OPEN EMAIL DATA MODAL AND DISPLAY DATA AVAILABLE, ALLOW TO ADD/DELETE THEM
function custEmailEdit(){

    var MembersData = multiEmailData;
    var newRow = '';

    $('#newEmail').val('');
    $('#emailList').html('');

    if( MembersData != 'No record found' ){

        for( var i=0; i<MembersData.length; i++ ){
            
            var defaultFlag = '';
            if ( MembersData[i].defaultFlag == "1" ){
                defaultFlag = 'checked';
            }

            newRow += '<tr>'+
                        '<td><div class="radioer form-inline">'+                                                    
                                '<input id="radio-'+ MembersData[i].pkEmailId +'" type="radio" '+ defaultFlag +' name="isDefaultMail">'+
                                '<label for="radio-'+ MembersData[i].pkEmailId +'"></label>'+
                            '</div></td>'+
                        '<td>'+ MembersData[i].emailAddress +'</td>'+
                        '<td><a href="javascript:void(0)" id="'+ MembersData[i].pkEmailId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="email" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                    ' </tr>';
        }

        $('#emailList').html( newRow );

    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_EMAIL );
    }
    
    $('#cust-edit-email').modal('show');
    addFocusId( 'newEmail' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    
    changeEmail();
}

// OPEN ADDRESS DATA MODAL AND DISPLAY DATA AVAILABLE, ALLOW TO ADD/DELETE THEM
function addressEdit(){

    var MembersData = multiAddressData;
    var newRow = '';

    $('#addressList').html('');
    resetPopUp();
    
    if( MembersData != 'No record found' ){

        for( var i=0; i<MembersData.length; i++ ){
            
            var defaultFlag = '';
            if ( MembersData[i].defaultFlag == "1" ){
                defaultFlag = 'checked';
            }

            var stateId = $('#addStateId option[value='+ MembersData[i].stateid +']').text();
            populateCity(MembersData[i].statename);
            var cityId = $('#addCityId option[value='+ MembersData[i].cityid +']').text();

            newRow += '<tr>'+
                         '<td><div class="radioer form-inline">'+                                                    
                            '<input id="radio-'+ MembersData[i].addressId +'" type="radio" '+ defaultFlag +' name="isDefaultAddress">'+
                            '<label for="radio-'+ MembersData[i].addressId +'"></label>'+
                        '</div></td>'+
                        '<td>'+ MembersData[i].address +'</td>'+
                        '<td>'+ MembersData[i].catchArea +'</td>'+
                        '<td>'+ MembersData[i].cityname +'</td>'+
                        '<td>'+ MembersData[i].statename +'</td>'+
                        '<td>'+ MembersData[i].countryname +'</td>'+
                        '<td><a href="javascript:void(0)" id="'+ MembersData[i].addressId +'" type="address" class="btn btn-xs btn-primary btn-ripple btn-danger" type="address" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                    ' </tr>';
        }

        $('#addressList').html( newRow );
        
    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_ADDRESS );
    }
    
    $('#address-edit').modal('show'); 
    addFocusId( 'newaddress' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

//    var state = $('#custState').html();
//    $("#custStateId option").filter(function() {
//        return $(this).text() == state; 
//    }).prop('selected', true);
//    $("#custStateId").selectpicker('refresh');
//
//    populateCity( $('#custStateId').val() );
//
//    var city = $('#custCity').html();
//    $("#custCityId option").filter(function() {
//        return $(this).text() == city; 
//    }).prop('selected', true);
//    $("#custCityId").selectpicker('refresh');

    changeAddress();
}


function groupEdit(){
    
    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    
    resetAttach();
    openAttachment();

    $('.nav-tabs a[href="#tab4_1"]').tab('show');

    initCustomField();  //init Custom field
    renderContactPerson();  //null init dataTable for contact customer
    
    /* element selector defination START */
    var $ddEmpList = $('#userData');
    /* element selector defination END */

    var userListArr = GBL_USERLISTARR;
    var optionHtml = '<option value="-1">Select Employee</option>';

    for (var i = 0; i < userListArr.length; i++)
    {
        var userName = userListArr[i].fullName;
        var userId = userListArr[i].userId;
        optionHtml += '<option value="' + userId + '">' + userName + '</option>';
    }

    
    $($ddEmpList).html(optionHtml);
    $($ddEmpList).selectpicker('refresh');
    
    // $('#custEditDOB').val('');
    // $('#custEditDOA').val('');
    
    var custData = custInfoData[0].CustomerInfo;
    var groupData = custInfoData[0].CustomerInfo.groupData;

    $('#custNameEdit').html( custData.Name );
    $('#custEditGroup').val( custData.CustomerGroup );

    $('#custEditEthinicity').val( custData.EthinicityId );
    $('#custEditEthinicity').selectpicker('refresh'); 

    $('#indType').val( custData.indTypeId );
    $('#indType').selectpicker('refresh');

    $('#rateType').val( custData.rateTypeId );
    $('#rateType').selectpicker('refresh');
    
    $('#custStateId').val( custData.stateId );
    $('#custStateId').selectpicker('refresh');
    
    populateCity(custData.stateId);
    
    $('#custCityId').val( custData.cityId );
    $('#custCityId').selectpicker('refresh');

    $('#custEditType').val( custData.CustomerType );
    $('#custEditType').selectpicker('refresh');

    // $('#custEditOccupation').val( custData.Occupation );
    // $('#custEditOccupation').selectpicker('refresh');

    $('#custEditDesignation').val( custData.Designation );
    $('#custEditPan').val( custData.PanNo );
    $('#custEditGst').val( custData.gstTinNo );

    // if( custData.DOB != "0000-00-00" && custData.DOB != "" && custData.DOB != "1970-01-01"){
    //     $('#custEditDOB').val( mysqltoDesiredFormat(custData.DOB,'dd-MM-yyyy') );
    // }

    // if( custData.DOA != "0000-00-00" && custData.DOA != "" && custData.DOA != "1970-01-01"){
    //     $('#custEditDOA').val( mysqltoDesiredFormat(custData.DOA,'dd-MM-yyyy') );
    // }

    for( var i=0; i<groupData.length; i++ ){
        $("#custEditGroup option[value='" + groupData[i].groupId + "|--|"+ groupData[i].groupName +"']").prop("selected", true);
    }
    $("#custEditGroup").selectpicker('refresh');
    
    $($ddEmpList).val( custData.selectedUserId );
    $($ddEmpList).selectpicker('refresh');
    
    $('#group-edit').modal('show');

    addFocusId( 'indType' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

// UPDATE MULTI CONTACT, ADDRESS AND EMAIL DATA
function updateCustContactDetail(type){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }else if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    multiPhone = [];
    multiEmail = [];
    multiAddress = [];
    
    if( multiPhoneData.length != 0 ){
        multiPhoneData.forEach(function( record , index ){
            var deleteFlag = ( record.deleteFlag == undefined ? 0 : record.deleteFlag );
            var comment = ( record.comment == undefined ? "" : record.comment );
            var defaultFlag = 0;
            if( record.defaultFlag == undefined ){
                if( index == 0 ){
                    defaultFlag = 1;
                }else{
                    defaultFlag = 0;
                }
            }else{
                defaultFlag = record.defaultFlag;
            }
            var phoneId = "";
            if( record.phoneId == record.phoneNo ){
                phoneId = "";
            }else{
                phoneId = record.phoneId;
            }

            var temp = {
                mobileNo : record.phoneNo,
                defaultFlag : record.defaultFlag,
                deleteFlag : deleteFlag,
                phoneId : phoneId,
                countryCode : record.countryCode,
                comment : comment,
            }
            multiPhone.push( temp  );
        });
    }
    if( multiEmailData.length != 0 ){
        multiEmailData.forEach(function( record , index ){
            var deleteFlag = ( record.deleteFlag == undefined ? 0 : record.deleteFlag );
            var defaultFlag = 0;
            if( record.defaultFlag == undefined ){
                if( index == 0 ){
                    defaultFlag = 1;
                }else{
                    defaultFlag = 0;
                }
            }else{
                defaultFlag = record.defaultFlag;
            }
            var pkEmailId = "";
            if( record.pkEmailId == record.emailAddress ){
                pkEmailId = "";
            }else{
                pkEmailId = record.pkEmailId;
            }

            var temp = {
                emailAddressId : pkEmailId,
                emailAddress : record.emailAddress,
                comment : "",
                defaultFlag : defaultFlag,
                deleteFlag : deleteFlag,
            }
            multiEmail.push( temp  );
        });
    }

    if( multiAddressData.length != 0 ){
        multiAddressData.forEach(function( record , index ){
            var deleteFlag = ( record.deleteFlag == undefined ? 0 : record.deleteFlag );
            var defaultFlag = 0;
            if( record.defaultFlag == undefined ){
                if( index == 0 ){
                    defaultFlag = 1;
                }else{
                    defaultFlag = 0;
                }
            }else{
                defaultFlag = record.defaultFlag;
            }
            var addressId = "";
            if( record.addressId.indexOf('add_') >= 0 ){
                addressId = "";
            }else{
                addressId = record.addressId;
            }

            var temp = {
                addressId : addressId,
                address : record.address,
                pinCode : record.pincode,
                catchArea : record.catchArea,
                comment : "",
                cityId : record.cityid,
                stateId : record.stateid,
                countryId : record.countryid,
                defaultFlag : defaultFlag,
                deleteFlag : deleteFlag,
            }
            multiAddress.push( temp  );
        });
    }

    // multiPhone = multiPhoneData;
    // multiEmail = multiEmailData;
    // multiAddress = multiAddressData;

    if( type == "phone" ){

        var index = findIndexByKeyValue( multiPhone , 'defaultFlag' , 1 );
        if( index == "-1" ){
            toastr.warning('Please Select Default Mobile Number');
            return false;
        }
        
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateInsertMultiEntryPhone',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11,45),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            studentId: currStudId,
            multiPhone : multiPhone
        }
        commonAjax(FOLLOWUPURL, postData, getcustContactUpdateCallback,"Please Wait... Getting Dashboard Detail");

    }else if( type == 'mail' ){
        var index = findIndexByKeyValue( multiEmail , 'defaultFlag' , 1 );
        if( index == "-1" ){
            toastr.warning('Please Select Default Email Address');
            return false;
        }
        
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateInsertMultiEntryEmail',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11,45),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            studentId: currStudId,
            multiemailAddress : multiEmail
        }
        commonAjax(FOLLOWUPURL, postData, getcustContactUpdateCallback,"Please Wait... Getting Dashboard Detail");
    
    }else{
        var index = findIndexByKeyValue( multiAddress , 'defaultFlag' , 1 );
        if( index == "-1" ){
            toastr.warning('Please Select Default Address');
            return false;
        }

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateInsertMultiEntryAddress',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11,45),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            studentId: currStudId,
            multiAddress : multiAddress
        }
        commonAjax(FOLLOWUPURL, postData, getcustContactUpdateCallback,"Please Wait... Getting Dashboard Detail");
    }

    // console.log(postData); return false;
}

// UPDATE MULTI CONTACT, ADDRESS AND EMAIL DATA CALLBACK
function getcustContactUpdateCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $('#cust-edit-email').modal('hide');
        $('#cust-details-1').modal('hide');
        $('#address-edit').modal('hide');
        $('#cust-edit-phone').modal('hide');

        multiPhoneData = [];
        multiEmailData = [];
        multiAddressData = [];

        closeCard();
        
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_STUDENT_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

function UpdateCustomer(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }else if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    var indType = $('#indType').val();
    var rateType = $('#rateType').val();
    var custGroup = $('#custEditGroup').val();
    var ethinicity = $('#custEditEthinicity').val();
    var custType = $('#custEditType').val();
    // var occupation = $('#custEditOccupation').val();
    var designation = "";
    // var designation = $('#custEditDesignation').val();
    var panNo = $('#custEditPan').val();
    var gstNo = $('#custEditGst').val();
    // var DOB = $('#custEditDOB').val();
    // var DOA = $('#custEditDOA').val();
    var custWebsite = $('#custWebsite').val();
    // var cityId = $('#custCityId option:selected').val();
    // var stateId = $('#custStateId option:selected').val();
    var selectedUserId = $('#userData option:selected').val();

    // if( indType == "-1" ){
    //     toastr.warning('Please Select Industry Type Of Customer');
    //     $('#indType').focus();
    //     return false;
    
    // }else if( custGroup == null ){
    //     toastr.warning('Please Select Customer Group');
    //     $('#custEditGroup').focus();
    //     return false;
    
    // }else if( ethinicity == "-1" || ethinicity == null ){
    //     toastr.warning('Please Select Customer Ethnicity');
    //     $('#custEditEthinicity').focus();
    //     return false;
    
    // }else if( custType == "-1" || custType == null ){
    //     toastr.warning('Please Select Customer Type');
    //     $('#custEditType').focus();
    //     return false;
    
    // }else if( occupation == "-1" || occupation == null ){
    //     toastr.warning('Please Select Customer Occupation');
    //     $('#custEditOccupation').focus();
    //     return false;
    
    // }else if( designation == "" ){
    //     toastr.warning('Please Enter Customer Designation');
    //     $('#custEditDesignation').focus();
    //     return false;
    
    // }else if( panNo == "" ){
    //     toastr.warning('Please Enter Customer Pan No');
    //     $('#custEditPan').focus();
    //     return false;
    
    // }else if( DOB == "" ){
    //     toastr.warning('Please Enter Customer Date of Birth');
    //     $('#custEditDOB').focus();
    //     return false;
    
    // }else if( DOA == "" ){
    //     toastr.warning('Please Enter Customer Date of Anniversary');
    //     $('#custEditDOA').focus();
    //     return false;
    // }
    if( selectedUserId == "-1" ){
        toastr.warning('Please Select Responsible Manager');
        $('#userData').focus();
        return false;
    
    }else{
        
        multiGroupData = []; // ADDED BY DEEPAK ON 01-07-2016 ONLY SEND SELECTED.
        if( custGroup != null ){
            for( var j=0; j<custGroup.length; j++ ){
                var tempGroup = {
                    groupId : custGroup[j].split('|--|')[0],
                    groupName : custGroup[j].split('|--|')[1]
                }
                multiGroupData.push( tempGroup );
            }
        }

        var custData = custInfoData[0].CustomerInfo;

        var custMobile = custData.MobileNo;
        var FullAddress = custData.FullAddress;
        var custTypeId = custData.custTypeId;
        var orderCount = custData.totalOrderCount;
        var inqCount = custData.totalInqCount;

        var custExpName = custData.Name.split(' ');

        if( custExpName.length == 2 ){
            first_name  = custExpName[0];
            last_name = custExpName[1];
        
        }else if( custExpName.length == 3 ){
            first_name  = custExpName[0] +' '+custExpName[1];
            last_name = custExpName[2];

        }else if( custExpName.length == 4 ){
            first_name  = custExpName[0] +' '+custExpName[1];
            last_name = custExpName[2] +' '+custExpName[3];

        }else if( custExpName.length > 4 ){
            first_name  = custExpName[0] +' '+custExpName[1];
            last_name = custExpName[2] +' '+custExpName[3] +' '+custExpName[4];
        
        }else{
            first_name  = custData.Name;
        }

        // if( DOB != "" ){
        //     DOB = ddmmyyToMysql(DOB);
        // }

        // if( DOA != "" ){
        //     DOA = ddmmyyToMysql(DOA);
        // }

        var cust_data = {
            indusTypeId: indType,
            address: FullAddress,
            contact_name: "",
            contact_number: "",
            cust_mobile: custMobile,
            first_name: first_name,
            last_name: last_name,
            rateTypeId: rateType,
            remark: "",
            special_feedback: "",
            ethnicity : ethinicity,
            custType : custType,
            // occupation : occupation,
            designation : designation,
            panNo : panNo,
            gstNo : gstNo,
            // birthdate : DOB,
            // anniversary_date : DOA,
            selectedUserId : selectedUserId,
            custWebsite : custWebsite,
            custTypeId : custTypeId,
            orderCount : orderCount,
            inqCount : inqCount
        }

        console.log('Validation Passed');

        // START GETTING CUSTOM FILED AND IT DATA IF SET FOR INQUIRY
        var customFieldsArr = [];
        var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
        if(localJson.data.customFieldDetail != "No record found"){
            var customFieldValue = {};
            for(var f=0;f<localJson.data.customFieldDetail.length;f++){
                if(localJson.data.customFieldDetail[f]['tabType'] == "CUSTOMER"){

                    if(localJson.data.customFieldDetail[f]['fieldType'] == "radio"){
                        //var customFieldValueTemp = 
                        customFieldValue['field'+(localJson.data.customFieldDetail[f].priorityLevel)] =  $('input[name=field'+(localJson.data.customFieldDetail[f].priorityLevel)+']:checked').val();
                    }else if(localJson.data.customFieldDetail[f]['fieldType'] == "checkbox"){
                        var tempChck = '';
                        $('input[name="field'+(localJson.data.customFieldDetail[f].priorityLevel)+'"]:checked').each(function() {
                           // console.log(this.value);
                           if( tempChck.length == 0 ){
                                tempChck += $(this).val();
                           }else{
                                tempChck += ',' + $(this).val();
                           } 
                        });
                        if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && tempChck == "" ){
                            toastr.warning("Please Select "+ localJson.data.customFieldDetail[f].fieldName);
                            return false;
                        }else{
                            customFieldValue['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = tempChck; 
                        }
                    } else{
                        if( localJson.data.customFieldDetail[f].ismendortyFlag == "1" && ( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "" || $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() == "-1" ) ){
                            
                            var msg = "Please Enter "+ localJson.data.customFieldDetail[f].fieldName;
                            if( localJson.data.customFieldDetail[f].fieldType == "dropdown" || localJson.data.customFieldDetail[f].fieldType == "date"){
                                msg = "Please Select "+ localJson.data.customFieldDetail[f].fieldName;
                            }

                            toastr.warning( msg );
                            $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                            return false;
                        }else{

                            if( localJson.data.customFieldDetail[f]['fieldType'] == "email" && $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() != "" && !validateEmail( $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val() ) ){
                                toastr.warning( "Please Enter Valid Email Id in "+ localJson.data.customFieldDetail[f].fieldName );
                                $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).focus();
                                return false;
                            }
                            
                            customFieldValue['field'+(localJson.data.customFieldDetail[f].priorityLevel)] = $('#field'+(localJson.data.customFieldDetail[f].priorityLevel)).val();
                        }
                    }
                }
            }
            customFieldsArr.push(customFieldValue);
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'editcust',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3,11),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            custId: currStudId,
            cust_data : cust_data,
            groupData : multiGroupData,
            multiAddress: multiAddressData,
            multiEmail: multiEmailData,
            multiPhone: multiPhoneData,
            customField: customFieldsArr,
        }

        // console.log(postData); return false;
        commonAjax(FOLLOWUPURL, postData, getcustGroupUpdateCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function getcustGroupUpdateCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $('#group-edit').modal('hide');
        $('#cust-details-1').modal('hide');
        
        closeCard();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_STUDENT_FAILED );
        else
            toastr.warning(SERVERERROR);
    }
}

// ADD CONTACT NUMBER DATA
function addContactNumber(){

    var moNo = getIntlMobileNum("newPhoneNum");    // GET MOBILE NUM
    var countryCode = getCountryDialCode("newPhoneNum"); // GET COUNTRY CODE
    // var moNo = $('#newPhoneNum').val().trim();
    var defaultPhone = "0";

    if( !checkValidMobile( "newPhoneNum" ) ){
    // if( moNo == "" || moNo.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter valid Mobile Number');
        $('#newPhoneNum').focus();
        return false;
    }else{

        resetPopUp();

        var custData = custInfoData[0].CustomerInfo;

        if( multiPhoneData.length == 0 ){
            defaultPhone = "1";
            $('#defaultMobile').html( moNo );
        }

        var index = findIndexByKeyValue( multiPhoneData , "phone" , moNo );
        if( index != "-1" ){
            toastr.warning('This Mobile Number is already exist');
            return false;
        }

        var tempPhone = {
            comment: "",
            defaultFlag: defaultPhone,
            deleteFlag: 0,
            phoneNo: moNo,
            phoneId: moNo,
            countryCode : countryCode
        }
        multiPhoneData.push( tempPhone );

        console.log(multiPhoneData);

        var chk = '';
        if( defaultPhone == "1" ){
            chk = "checked";
        }

        $('#phoneList .dataTables_empty').parents('tr').remove();
        $('#phoneList').append('<tr><td><div class="radioer form-inline">'+                                                    
                                        '<input '+ chk +' id="radio-'+ moNo +'" type="radio" name="isDefaultContact">'+
                                        '<label for="radio-'+ moNo +'"></label>'+
                                    '</div></td>'+
                                '<td>'+ moNo +'</td>'+
                                '<td><a href="javascript:void(0)" id="'+ moNo +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="phone" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>')
    }
    changePhone();
}

// ADD EMAIL DATA
function addEmail(){

    var email = $('#newEmail').val().trim();
    var defaultMail = "0";

    if( email.length == 0 ){
        toastr.warning('Please Enter Email Number');
        $('#newEmail').focus();
        return false;
    }else if( !validateEmail(email) ){
        toastr.warning('Please Enter valid E-Mail');
        $('#newEmail').focus();
        return false;
    }else{

        resetPopUp();
        var custData = custInfoData[0].CustomerInfo;

        if( multiEmailData.length == 0 ){
            defaultMail = "1";
        }

        var tempEmail = {
            // comment: "",
            defaultFlag: defaultMail,
            deleteFlag: 0,
            emailAddress: email,
            pkEmailId: email,
        }
        multiEmailData.push( tempEmail );

        console.log(multiEmailData);

        var chk = '';
        if( defaultMail == "1" ){
            chk = "checked";
        }

        $('#emailList .dataTables_empty').parents('tr').remove();
        $('#emailList').append('<tr><td><div class="radioer form-inline">'+                                                    
                                        '<input '+ chk +' id="radio-'+ email +'" type="radio" name="isDefaultMail">'+
                                        '<label for="radio-'+ email +'"></label>'+
                                    '</div></td>'+
                                '<td>'+ email +'</td>'+
                                '<td><a href="javascript:void(0)" id="'+ email +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="email" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>');
    }
    changeEmail();
}

// ADD ADDRESS DATA
function addAddress(){

    var address = $('#newaddress').val().trim();
    var catArea = $('#catArea').val().trim();
    var custCountryId = $('#custCountryId').val();
    var custStateId = $('#custStateId').val();
    var custCityId = $('#custCityId').val();
    var custCountryId = $('#custCountryId').val();
    var custCountryTxt = $('#custCountryId option:selected').text();
    var custStateTxt = $('#custStateId option:selected').text();
    var custCityTxt = $('#custCityId option:selected').text();
    var defaultAddress = "0";

    if( address.length == 0 ){
        toastr.warning('Please Enter address');
        $('#newaddress').focus();
        return false;

    }else if( custCountryId == "-1" ){
        toastr.warning('Please Select Country');
        addFocusId( 'custCountryId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;

    }else if( custStateId == "-1" ){
        toastr.warning('Please Select State'); 
        addFocusId( 'custStateId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        return false;

    }else if( custCityId == "-1" ){
        toastr.warning('Please Select City');
        addFocusId( 'custCityId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT 
        return false;

    }else{

        resetPopUp();

        var custData = custInfoData[0].CustomerInfo;

        if( multiAddressData.length == 0 ){
            defaultAddress = "1";
            $('#outAdd_'+currStudId).html( $("#custCityId option[value='"+ custCityId +"']").text() +' - '+ $("#custStateId option[value='"+ custStateId +"']").text() +' - '+ $("#custCountryId option[value='"+ custCountryId +"']").text() );
        }

        var tempaddress = {
            comment: "",
            defaultFlag: defaultAddress,
            deleteFlag: 0,
            address: address,
            catchArea: catArea,
            countryid: custCountryId,
            cityid: custCityId,
            stateid: custStateId,
            // addressId: "",
            addressId: "add_"+addCnt,
        }
        multiAddressData.push( tempaddress );

        console.log(multiAddressData);

        var chk = '';
        if( defaultAddress == "1" ){
            chk = "checked";
        }

        $('#addressList .dataTables_empty').parents('tr').remove();
        $('#addressList').append('<tr><td><div class="radioer form-inline">'+                                                    
                                        '<input '+ chk +' id="radio-add_'+ addCnt +'" type="radio" name="isDefaultAddress">'+
                                        '<label for="radio-add_'+ addCnt +'"></label>'+
                                    '</div></td>'+
                                '<td>'+ address +'</td>'+
                                '<td>'+ catArea +'</td>'+
                                '<td>'+ custCityTxt +'</td>'+
                                '<td>'+ custStateTxt +'</td>'+
                                '<td>'+ custCountryTxt +'</td>'+
                                '<td><a href="javascript:void(0)" id="add_'+ addCnt +'" class="btn btn-xs btn-primary btn-ripple btn-danger" type="address" onclick="deleteMultiData( this )"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>');
        addCnt = addCnt + 1;
    }
    $('#custCountryId').val('-1');
    $('#custCountryId').selectpicker('refresh');
    $('#custStateId').val('-1');
    $('#custStateId').selectpicker('refresh');
    $('#custCityId').val('-1');
    $('#custCityId').selectpicker('refresh');
    changeAddress();
}

// var searchedCustId = "";
// function commonAutoCompleteForCustomerPage(id,callback) {
    
//     $("#" + id).typeahead({
//         grepper : function (item){
//             console.log("Grepper");
//             console.log(item);
//             var newTempCustomerArr = [];
//             if(item.length != 0){
//                 for(var g=0;g<item.length;g++){
//                     //console.log(item[g]['id']);
//                     var customerIndex = findIndexByKeyValue(studentData,"CustId",item[g]['id']);
//                     //console.log(customerIndex);
//                     if(customerIndex != "-1"){
//                         if(newTempCustomerArr.length != 0){
//                             var innerIndex = findIndexByKeyValue(newTempCustomerArr,"CustId",item[g]['id']);
//                             if(innerIndex == -1){
//                                 var customerDetail = {
//                                     CustId:studentData[customerIndex]['CustId'],
//                                     CustomerInfo : studentData[customerIndex]['CustomerInfo'],
//                                 }
//                                 newTempCustomerArr.push(customerDetail);
//                             }
//                         }else{
//                             var customerDetail = {
//                                     CustId:studentData[customerIndex]['CustId'],
//                                     CustomerInfo : studentData[customerIndex]['CustomerInfo'],
//                                 }
//                             newTempCustomerArr.push(customerDetail);
//                         }
//                     }else{
//                        toastr.warning('No customer found for your criteria. '); 
//                     }
//                 }
//                 renderDataStudent(newTempCustomerArr, 0);
//             }else{
//                 renderDataStudent("No record found", 0);
//                 toastr.warning('No customer found for your criteria. ');
//             }
//         },
// //        onSelect: function (item) {
// //            //alert(item);
// //            console.log(item);
// //            var mobileNumber = item.text.split(",");
// //            var index = findIndexByKeyValueAndCond(ITEMS, "id","mobile", item.value,mobileNumber[2]);
// //            
// //            searchedCustId = item.value;
// //
// //            if ( index == -1 ){
// //                    var newCustNm = item.text.split(":")[0];
// //                    $('#custSearch').val(newCustNm);
// //            }
// //            else{
// //                setTimeout(function () {
// //                    
// //                    GBL_SELECTEDCUSTID = ITEMS[index].id;
// //                    GBL_SELECTEDCUSTMOBNO = ITEMS[index].id+"|--|"+mobileNumber[2];
// //                    localStorage.POTGselectedCustIdInSearch = ITEMS[index].id;
// //                    //initCommonFunction();
// //                    callback();
// //
// //                    $('#ContactData').hide();
// //                }, 1);
// //            }
// //        },
//         ajax: {
//             url: SEARCHURL,
//             timeout: 500,
//             triggerLength: 2,
//             displayField: "name",
//             method: "POST",
            // headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
//             crossDomain: true,
//             preDispatch: function (query) {

//                 GBL_SELECTEDCUSTID = '';
//                 GBL_SELECTEDCUSTMOBNO = '';
                // addRemoveLoader(1);
//                 var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//                 var postdata = {
//                     requestCase: "crmDashboardSearch",
//                     clientId: tmp_json.data[0].FK_CLIENT_ID,
//                     userId: tmp_json.data[0].PK_USER_ID,
//                     dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//                     orgId: checkAuth(3, 10),
//                     keyWord: query
//                 };

                
                // removeLoaderInTime( REMOVE_LOADER_TIME );
//                 return {postData: postdata};
//             },
//             preProcess: function (data) {
    //           addRemoveLoader(0);
//                 if (data.success === false) {
//                     // Hide the list, there was some error
//                     return false;
//                 }
//                 // We good!
//                 ITEMS = [];
//                 if (data.data != null) {
//                     $.map(data.data.searchData, function (data) {

//                         if( data.cust_id != "-1" ){

//                             var group = {
//                                 id: data.cust_id,
//                                 name: data.concatKey,
//                                 data: data.fname + " " + data.lname,
//                                 mobile: data.mobile,
//                                 fname: data.fname,
//                                 lname: data.lname,
//                                 rateTypeId: data.rateTypeId,
//                                 custTypeId: data.custTypeId
//                             };
//                             ITEMS.push(group);
//                         }
//                     });

//                     return ITEMS;
//                 }

//             }
//         }
//     });
// }
 
//POPULATE THE SELECTED CITY ON STATE
function populateCity(stateId){
    
    if(stateId == "-1"){
        setOption("0", "custCityId", "", "--Select City --");
    }else{
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        data.data.cityStateListing = [];
        var index = findIndexByKeyValue(data.data.cityStateListing,"ID",stateId);
       
        var cityListing = "";
        if(index != -1){
            cityListing  = (data.data.cityStateListing[index]['cityListing'] != "No record found") ? data.data.cityStateListing[index]['cityListing'] : "";
        }else{
            cityListing = "";
        }
        setOption("0", "custCityId", cityListing, "--Select City --");
    }
    
    $("#custCityId").selectpicker("refresh");    
}

// CLOSE ACTIVE CARD OF STUDENT DETAIL
function closeCard(){

    $('.clickable-button.clicked').removeClass('clicked');
    $('.layered-content.active').removeClass('active'); 
}

// RESET MODAL FIELDS
function resetPopUp(){

    $('#newaddress').val('');
    $('#newPhoneNum').val('');
    $('#catArea').val('');
    $('#custStateId').val('-1');
    $('#custCityId').val('-1');
    $('#custStateId').selectpicker('refresh');
    $('#custCityId').selectpicker('refresh');
    $('#newEmail').val('');
}

// CHANGE PHONE NUMBER DEFAULT SELECTION
function changePhone(){
    $('input[type=radio][name=isDefaultContact]').change(function() {
        var id = $(this).attr('id').split('-')[1];

        for( var i=0; i<multiPhoneData.length; i++ ){

            if( id !=  multiPhoneData[i].phoneId ){
                multiPhoneData[i].defaultFlag = 0;
            }else{
                multiPhoneData[i].defaultFlag = 1;
            }
        }
        console.log( multiPhoneData );
    });
}

// CHANGE EMAIL DEFAULT SELECTION
function changeEmail(){
    $('input[type=radio][name=isDefaultMail]').change(function() {
        var id = $(this).attr('id').split('-')[1];

        for( var i=0; i<multiEmailData.length; i++ ){

            if( id !=  multiEmailData[i].pkEmailId ){
                multiEmailData[i].defaultFlag = 0;
            }else{
                multiEmailData[i].defaultFlag = 1;
            }
        }
        console.log( multiEmailData );
    });
}

// CHANGE ADDRESS DEFAULT SELECTION
function changeAddress(){
    $('input[type=radio][name=isDefaultAddress]').change(function() {
        var id = $(this).attr('id').split('-')[1];

        for( var i=0; i<multiAddressData.length; i++ ){

            if( id !=  multiAddressData[i].addressId ){
                multiAddressData[i].defaultFlag = 0;
            }else{
                multiAddressData[i].defaultFlag = 1;
            }
        }
        console.log( multiAddressData );
    });
}

// ALLOW DELETION OF MULTI EMAIL, PHONE AND ADDRESS DATA
function deleteMultiData(context){

    var id =  $(context).attr('id');
    var type =  $(context).attr('type');
    console.log(id);

    if( type == 'phone' ){
        //Delete Contact Data
        var index = findIndexByKeyValue( multiPhoneData , "phoneId" , id );

        if( index != "-1" ){
            if( multiPhoneData[index].defaultFlag != "1" ){

                if( multiPhoneData[index].deleteFlag == 0 ){
                    multiPhoneData[index].deleteFlag = -1;
                }else{
                    multiPhoneData.splice( index , 1 );
                }
            }else{
                toastr.error( "You can't Delete Default Phone number" );
                return false;
            }
        }else{
            toastr.warning('Deletion is not Possible');
        }
        console.log( multiPhoneData );

    }else if( type == 'email' ){
        //Delete mail Data
        var index = findIndexByKeyValue( multiEmailData , "pkEmailId" , id );

        if( index != "-1" ){

            if( multiEmailData[index].defaultFlag != "1" ){
                if( multiEmailData[index].deleteFlag == 0 ){
                    multiEmailData[index].deleteFlag = -1;
                }else{
                    multiPhoneData.splice( index , 1 );
                }
            }else{
                toastr.error( "You can't Delete Default Email Address" );
                return false;
            }
        }else{
            toastr.warning('Deletion is not Possible');
        }
        console.log( multiEmailData );

    }else{
        //Delete address Data
        var index = findIndexByKeyValue( multiAddressData , "addressId" , id );

        if( index != "-1" ){

            if( multiAddressData[index].defaultFlag != "1" ){
                if( multiAddressData[index].deleteFlag == 0 ){
                    multiAddressData[index].deleteFlag = -1;
                }else{
                    multiAddressData.splice( index , 1 );
                }
            }else{
                toastr.error( "You can't Delete Default Address" );
                return false;
            }
        }else{
            toastr.warning('Deletion is not Possible');
        }
        console.log( multiAddressData );
    }
    $(context).parents('td').parent('tr').remove();
}

function getLastLevelUserListing()
{
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
    var postData = {
        requestCase: 'userListingActivityModule',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2, 6),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(COMMONURL, postData, callBackGetLevel10UserList, "Please Wait... Getting the level 10 user listing");
    
    function callBackGetLevel10UserList(flag, data) {
        if (data.status == SCS && flag) {

            if (data.data)
            {
                GBL_USERLISTARR = data.data;
            }
        }
        else {
            
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
        }

    }

}

function initCustomField(){
    
    var custData = custInfoData[0].CustomerInfo;
    var readOnlyClass = '';
    var disableClass = '';
    var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    if(localJson.data.customFieldDetail != "No record found"){
        var divStruct = '<div class="row">';
        for(var j=0;j<localJson.data.customFieldDetail.length;j++){
            if(localJson.data.customFieldDetail[j]['tabType'] == "CUSTOMER"){
                divStruct += '<div class="col-lg-12">';
                if(localJson.data.customFieldDetail[j].fieldType == "text"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'" '+readOnlyClass+' />';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "number"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="number" min="0" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'"/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "email"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="email" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+localJson.data.customFieldDetail[j]['fieldValue']+'" class="form-control" value="'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'"/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<textarea id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" maxlength="'+ localJson.data.customFieldDetail[j].charLen +'" class="form-control" '+readOnlyClass+' placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'">'+ custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)] +'</textarea>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
                    divStruct += '<div class="form-group radioer">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+' '+disableClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
                    }
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectPckr" '+disableClass+'>';
                    divStruct += '<option value="-1">-- '+localJson.data.customFieldDetail[j]['fieldName']+' --</option>';
                    for(var g=0;g<explodeValue.length;g++){
                        var classSeleted = "";
                        if(explodeValue[g] == custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            classSeleted = "selected";
                        }
                        divStruct += '<option value="'+explodeValue[g]+'" '+classSeleted+'>'+explodeValue[g]+'</option>';
                    }
                    divStruct += "</select>";
                    $("#field"+(j+1)).selectpicker();
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += '<div class="form-group">';
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    divStruct += requiredClass;
                    divStruct += '<input type="text" class="form-control normal-date-picker" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="'+ localJson.data.customFieldDetail[j].fieldValue +'" class="form-control" value="'+custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]+'" '+readOnlyClass+' readOnly/>';
                
                }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += requiredClass;
                    divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    for(var g=0;g<explodeValue.length;g++){
                        var selectedClass = "";
                        if(explodeValue[g] == custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
                            selectedClass = "checked";
                        }
                        divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+' '+disableClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
                    }
               
                }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
                    divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
                    var requiredClass = "";
                    if(localJson.data.customFieldDetail[j].ismendortyFlag == 1){
                        requiredClass = " <span class='mand'>*</span></br>";
                    }else{
                        requiredClass = "";
                    }
                    divStruct += requiredClass;
                    divStruct += '<div class="form-group checkboxer">';
                    var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
                    var explodeGetData = custData['field'+(localJson.data.customFieldDetail[j].priorityLevel)].split(",");
                    
                    for(var g=0;g<explodeValue.length;g++){
                        for(var h=0;h<explodeGetData.length;h++){
                            var selectedClass = "";
                            if(explodeValue[g] == explodeGetData[h] ){
                                selectedClass = "checked";
                                break;
                            }
                        }
                        divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ '" value="'+explodeValue[g]+'" '+selectedClass+' '+ disableClass +' /> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g]+'" '+readOnlyClass+'>'+explodeValue[g]+'</label>';
                    }
                }
                
                divStruct += '</div>'+
                '</div>';
                if(((j+1)%2) == 0){
                    divStruct += '</div><div class="row">';

                }
                $("#customFieldDiv").show();   
            }
            
        }
        $("#customFieldDiv").html(divStruct);
        $('.selectPckr').selectpicker('refresh');
        
        $('.normal-date-picker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });
    }

    // var disableClass = "";
    // var readOnlyClass = "";
    // var localJson = JSON.parse(localStorage.indeCampusRoleAuth);
    // if(localJson.data.customFieldDetail != "No record found"){
    //     var divStruct = '<div class="row">';
    //     for(var j=0;j<localJson.data.customFieldDetail.length;j++){
    //         if(localJson.data.customFieldDetail[j]['tabType'] == "CUSTOMER"){
    //             console.log(localJson.data.customFieldDetail[j]);
    //             divStruct += '<div class="col-lg-6">';
    //             if(localJson.data.customFieldDetail[j].fieldType == "text"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<input type="text" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="" class="form-control" value="'+ localJson.data.customFieldDetail[j].fieldValue +'" '+readOnlyClass+' />';
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "textarea"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<textarea id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control" '+readOnlyClass+'>'+ localJson.data.customFieldDetail[j].fieldValue +'</textarea>';
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "radio"){
    //                 divStruct += '<div class="form-group radioer" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label><br>';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var selectedClass = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         selectedClass = "checked";
    //                     }
    //                     divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+' '+disableClass+'> <label for="field'+(localJson.data.customFieldDetail[j].priorityLevel)+ explodeValue[g] +'">'+explodeValue[g]+'</label>';
    //                 }
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "dropdown"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<select id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" class="form-control selectpicker" '+disableClass+'>';
    //                 divStruct += '<option value="-1">---'+localJson.data.customFieldDetail[j]['fieldName']+'---</option>';
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var classSeleted = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         classSeleted = "selected";
    //                     }
    //                     divStruct += '<option value="'+explodeValue[g]+'" classSeleted>'+explodeValue[g]+'</option>';
    //                 }
    //                 divStruct += "</select>";
    //                 $("#field"+(j+1)).selectpicker();
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "date"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 divStruct += '<label class="control-label">'+localJson.data.customFieldDetail[j]['fieldName']+'</label>';
    //                 divStruct += '<input type="text" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" placeholder="" class="form-control" value="'+getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]+'" '+readOnlyClass+' />';
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "rating"){
    //                 divStruct += '<div class="form-group" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var selectedClass = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         selectedClass = "checked";
    //                     }
    //                     divStruct += '<input type="radio" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'"  '+selectedClass+' '+disableClass+'> <label for="'+explodeValue[g]+'">'+explodeValue[g]+'</label>';
    //                 }
    //             }else if(localJson.data.customFieldDetail[j].fieldType == "checkbox"){
    //                 divStruct += '<div class="form-group checkboxer" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'">';
    //                 var explodeValue = localJson.data.customFieldDetail[j].fieldValue.split(",");
    //                 for(var g=0;g<explodeValue.length;g++){
    //                     var selectedClass = "";
    //                     if(explodeValue[g] == getData[0]['field'+(localJson.data.customFieldDetail[j].priorityLevel)]){
    //                         selectedClass = "checked";
    //                     }
    //                     divStruct += '<input type="checkbox" id="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" name="field'+(localJson.data.customFieldDetail[j].priorityLevel)+'" value="'+explodeValue[g]+'" '+selectedClass+'> <label for="'+explodeValue[g]+'" '+readOnlyClass+'>'+explodeValue[g]+'</label>';
    //                 }
    //             }
    //             divStruct += '</div>'+
    //             '</div>';
    //             if(((j+1)%2) == 0){
    //                 divStruct += '</div><div class="row">';

    //             }   
    //         }
            
    //     }
    //     $("#customFieldDiv").html(divStruct);
    // }
}

function addContactPerson(){

    if( checkAuth(3, 11, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
// contactpersonData
    var contName = $('#conpersonname').val();
    var contNum = getIntlMobileNum("conpersonNum");    // GET MOBILE NUM
    // var contNum = $('#conpersonNum').val();
    var contEmail = $('#conpersonEmail').val();
    var contDesig = $('#conpersonDesig').val();
    var conpersonDob = $('#conpersonDob').val();
    var conpersonDoa = $('#conpersonDoa').val();

    if( contName.length == '0' ){
        toastr.warning('Please Enter Contact Name');
        $('#conpersonname').focus();
        return false;

    }else if( !checkValidMobile( "conpersonNum" ) ){
    // }else if( contNum == '' || contNum.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Contact Number');
        $('#conpersonNum').focus();
        return false;

    }else if( !validateEmail(contEmail) ){
        toastr.warning('Please Enter Valid Contact Email');
        $('#conpersonEmail').focus();
        return false;

    }else{

        var tempContData = {
            contactCustFName : contName,
            contactCustLName : "",
            contactCustMobile : contNum,
            custId : currStudId,
            contactCustEmail : contEmail,
            contactCustDesignation : contDesig,
            conpersonDob : conpersonDob,
            conpersonDoa : conpersonDoa,
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Inquiry Details
        var postData = {
            requestCase: 'addCreateContactPerson',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 9),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            conactPersonDetail : tempContData,
        };
        commonAjax(INQURL1, postData, addContactCallBack,CREATINGINQDATA);

        function addContactCallBack(flag, data){
            if (data.status == "Success" && flag) {
                
                var tempContact = {
                    contactId : data.data.contactId,
                    fname : contName,
                    lname : "",
                    contactNumber : contNum,
                    contactEmail : contEmail,
                    contactDesig : contDesig,
                    custId : currStudId,
                    contactDob : conpersonDob,
                    contactDoa : conpersonDoa,
                    deleteFlag : currStudId,
                }

                if( contactpersonData != 'No record found' ){

                    contactpersonData.push( tempContact );
                }else{
                    contactpersonData = [];
                    contactpersonData.push( tempContact );
                }

                $('#conpersonname').val('');
                $('#conpersonNum').val('');
                $('#conpersonEmail').val('');
                $('#conpersonDesig').val('');
                $('#contactDob').val('');
                $('#contactDoa').val('');

                renderContactPerson();

            }else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_CREATE_CONTACT_PERSON );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

function renderContactPerson(){

    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting" style="border:0px;">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>Name</th>'+
                            '<th>MobileNo</th>'+
                            '<th>Email</th>'+
                            '<th>Designation</th>'+
                            '<th>DOB</th>'+
                            '<th>DOA</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="inquiryDetailTable">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    if( contactpersonData != "No record found" ){

        for ( var i=0; i<contactpersonData.length; i++ ){

            newRow += '<tr>'+
                        '<td>'+ contactpersonData[i].fname +'</td>'+
                        '<td>'+ contactpersonData[i].contactNumber +'</td>'+
                        '<td>'+ contactpersonData[i].contactEmail +'</td>'+
                        '<td>'+ contactpersonData[i].contactDesig +'</td>'+
                        '<td>'+ contactpersonData[i].contactDob +'</td>'+
                        '<td>'+ contactpersonData[i].contactDoa +'</td>'+
                       '</tr>';
        }
    }

    $('#contTable').html(thead + newRow + tfoot);

    $("#tableListing").DataTable({ "stateSave": true });
}

// function changeUrl(){
//     var locationHref = (window.location.href).split("#");
//     window.location.href = locationHref[0];
// }

function hideShow(){
    $('#customFieldDataCust').toggle('150');
    $('.moredetails').find('i').toggleClass('fa-sort-asc fa-sort-desc');
}

function TagSearch(){
	 
    var searchField = (($('#customerSearch').val()).trim()).toLowerCase();
	var regex = new RegExp(searchField, "i");
	searchArr = [];
	$.each(GBL_SEARCHED_DATA, function (key, val) {
	    var customerName = val.firstName + ' ' + val.lastName;
        var cityName = ( val.cityName != null ? val.cityName : "" );
        var stateName = ( val.stateName != null ? val.stateName : "" );

        if ( ( ( customerName).trim()).toLowerCase().includes( searchField ) || ( ( val.mobileNo).trim()).toLowerCase().includes( searchField ) || ((cityName).trim()).toLowerCase().includes( searchField ) || ((stateName).trim()).toLowerCase().includes( searchField ) ) {
                searchArr.push(val); 
        } 
	});
	$("#searchresult").html("(Search Result : "+searchArr.length+")");
	// console.log(searchArr)
	renderDataStudent(searchArr);
}
function searchCustomer() {
    $('#customerSearch').keyup(function () {
        TagSearch();
    });
}

// function searchCustTag(tag,context){
// 	// $('#customerSearch').val(tag);
//  //    TagSearch();
//     var tempCust = [];
//     var GBL_SEARCHED_DATA = [];
//     studentData.forEach( function( record , index ){
//         if( (record.CustomerInfo.custType).toLowerCase() == (tag).toLowerCase() ){
//             GBL_SEARCHED_DATA.push( record );
//         }
//     });
//     $('.custType').removeClass('active');
//     $(context).addClass('active');
//     renderDataStudent(GBL_SEARCHED_DATA , "1");
//     $('#customerSearch').val('');
//     $("#searchresult").html("(Search Result : "+GBL_SEARCHED_DATA.length+")");
// }

function pageInitialEvents(){

    $('#customerSearch').keypress(function (e) {
        if (e.which == 13) {
            if( $('#customerSearch').val() == "" ){

                custCnt = 12;
                prvCnt = 0;
                renderDataStudent( GBL_SEARCHED_DATA , '1' );
                loadAllGridStudents();
                $('.custType').removeClass('active'); 
            }
        }
    });
    
    $('#customerSearch').keyup(function (e) {
        if($('#customerSearch').val().length < 2 && $('#customerSearch').val().length > 0 || $('#customerSearch').val().length == 0 ){

            prvCnt = 0;
            if( $('#customerSearch').val() == "" ){
                GBL_SEARCHED_DATA = studentData;
                renderDataStudent( studentData , '1' );
                $('.custType').removeClass('active');
            }else{
                renderDataStudent( GBL_SEARCHED_DATA , '1' );
            }
        }
    });

    //BACK SPACE AND DELETE KEY DOWN
    $('#customerSearch').on('keydown', function(e) {
        if( e.which == 8 || e.which == 46 ){
            $('#customerSearch').val('')
            custCnt = 12;
            prvCnt = 0;
            renderDataStudent( GBL_SEARCHED_DATA , '1' );
            loadAllGridStudents();
            $('.custType').removeClass('active'); 
        }
    });


    var data = JSON.parse(localStorage.indeCampusRoleAuth);

    // setOption("0", "custEditEthinicity", data.data.ethinicity, "--Ethnicity--");
    // setOption("0", "custEditOccupation", data.data.occupation, "--Occupation--");
    // setOption("0", "custEditType", data.data.customertype, "--Select Customer Type--");
    // setOption("0", "custStateId", data.data.cityStateListing, "--Select State --");
    // setOption("0", "indType", data.data.industryType, "--Select Customer Industry --");
    // $('#indType').selectpicker();
    // setOption("0", "rateType", data.data.itemRateType, "--Select Customer Rate Type --");
    // $('#rateType').selectpicker();

    var GBL_RATETYPEACTIVATE = data.data.itemRateTypeFlag;
    if( GBL_RATETYPEACTIVATE == "1" ){
        $('.rateTypeFlag').show();
    }
    
    $("#custStateId").selectpicker();    
    $("#custCityId").selectpicker(); 
    $('#inventoryData').dataTable({ "stateSave": true });

    // uploadCustomer();

    addFocusId( 'customerSearch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    setCountryList( 'custCountryId' );   // SET COUNTRY LISTING FROM LOAD AUTH SET DATA IN SELECT PICKER USING ID
    
    initTelInput( 'newPhoneNum' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput( 'conpersonNum' );   // INITIALIZE COUNTRY WISE MOBILE NUM
    google.charts.load('current', {packages: ['corechart', 'bar' , 'gauge' , 'geochart']});
}

// DISPLAY STUDENTS IN GRID VIEW
function showGrid(){

   $('#Grid').show(); 
   $('#List').hide(); 
   $('#custGridMode').show('300');
   $('#custSearchInp').show('300');
   $('#custListMode').hide('300');

   localStorage.indeCampusStudentView = "Grid";
}

// DISPLAY STUDENTS IN LIST VIEW
function showList(){

   $('#List').show(); 
   $('#Grid').hide(); 
   $('#custListMode').show('300');
   $('#custGridMode').hide('300');
   $('#custSearchInp').hide('300');

   if( $('#customerListing').html().length == "66" ){
        renderCustomerList(studentData);
   }
   localStorage.indeCampusStudentView = "List";
}

// RENDER STUDENT IN DATATABLE LISTING
function renderCustomerList(data){
	var tempCustData = data;
    var thead = '<table id="custTableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th width="400px">Name</th>'+
                        '<th>Email</th>'+
                        '<th>Mobile</th>'+
                        '<th>City | State | Country</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    if(tempCustData != 'No record found'){
        
        for( var i=0; i<tempCustData.length; i++ ){

            var custId = tempCustData[i].customerId;
            var Name = tempCustData[i].firstName + ' ' + tempCustData[i].lastName ;
            var Email = ((tempCustData[i].email) == "" || (tempCustData[i].email) == "0" ? "--" : tempCustData[i].email);
            var MobileNo = tempCustData[i].mobileNo;
            var cityName = (tempCustData[i].cityName) == "" || tempCustData[i].cityName == null ? "--" :  tempCustData[i].cityName;
            var stateName = (tempCustData[i].stateName) == "" || tempCustData[i].stateName == null ? "--" :  tempCustData[i].stateName;
            var countryName = (tempCustData[i].countryName) == "" || tempCustData[i].countryName == null ? "--" :  tempCustData[i].countryName;
            var profileImg = httpHead + "//"+ FINALPATH +"/assets/globals/img/avtar1.png";
			count = i + 1;

            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td class="tagCust"><a href="javascript:void(0)" custId="'+ custId +'" ><span class="useraImageBody"><img class="userProfile" id="" src="'+ profileImg +'"></span>'+ Name +'</a></td>'+
                            // '<td class="tagCust"><a href="javascript:void(0)" custId="'+ custId +'"  onclick="navigateToCustomerDetailPage(this)"><span class="useraImageBody"><img class="userProfile" id="" src="'+ profileImg +'"></span>'+ Name +'</a><span class="taglineCust"><span>'+ custTag +'</span></span></td>'+
                            '<td>'+ Email +'</td>'+
                            '<td>'+ MobileNo +'</td>'+
                            '<td>'+ cityName +' - '+  stateName +'<br>'+ countryName +'</td>'+
                        '</tr>';
        }

        
        $('#customerListing').html(thead + newRow + tfoot);

    }else{
        displayAPIErrorMsg( data.status , GBL_ERR_NO_STUD_LIST );
    }

     $('#custTableListing').DataTable({
        "pageLength": 50,
        "stateSave": true
    });
	
}

var GBL_custData = studentData;
// function searchList(tag)
// {
// 		var regex = new RegExp(tag, "i");
// 		var localCust = [];
// 		$.each(studentData, function (key, val) {
// 		if(val.CustomerInfo.Members != "No record found"){
// 			// console.log(val.CustomerInfo.FullAddress);
// 			if( val.CustomerInfo.Members[2] != undefined ){
// 				if (val.CustomerInfo.Name.search(regex) != -1 || val.CustomerInfo.cityName.search(regex) != -1 || val.CustomerInfo.stateName.search(regex) != -1 || val.CustomerInfo.Members[0].userName.search(regex) != -1 || val.CustomerInfo.Members[1].userName.search(regex) != -1 || val.CustomerInfo.Members[2].userName.search(regex) != -1 || ( val.CustomerInfo.FullAddress != "" && val.CustomerInfo.FullAddress.search(regex) != -1 ) || val.CustomerInfo.custType.search(regex) != -1 ) {
// 					localCust.push(val);
// 				}                
// 			}else if( val.CustomerInfo.Members[1] != undefined ){
// 				if (val.CustomerInfo.Name.search(regex) != -1 || val.CustomerInfo.cityName.search(regex) != -1 || val.CustomerInfo.stateName.search(regex) != -1 || val.CustomerInfo.Members[0].userName.search(regex) != -1 || val.CustomerInfo.Members[1].userName.search(regex) != -1 || ( val.CustomerInfo.FullAddress != "" && val.CustomerInfo.FullAddress.search(regex) != -1 ) || val.CustomerInfo.custType.search(regex) != -1 ) {
// 					localCust.push(val);
// 				}                
// 			}
// 			else if(  val.CustomerInfo.Members[1] != undefined && val.CustomerInfo.Members[2] != undefined ){
// 				if (val.CustomerInfo.Name.search(regex) != -1 || val.CustomerInfo.cityName.search(regex) != -1 || val.CustomerInfo.stateName.search(regex) != -1 || val.CustomerInfo.Members[0].userName.search(regex) != -1 || val.CustomerInfo.Members[1].userName.search(regex) || val.CustomerInfo.Members[2].userName.search(regex) || ( val.CustomerInfo.FullAddress != "" && val.CustomerInfo.FullAddress.search(regex) != -1 ) || val.CustomerInfo.custType.search(regex) != -1  ) {
// 					localCust.push(val);
// 				}                
// 			}else{
// 				if (val.CustomerInfo.Name.search(regex) != -1 || val.CustomerInfo.cityName.search(regex) != -1 || val.CustomerInfo.stateName.search(regex) != -1 || val.CustomerInfo.Members[0].userName.search(regex) != -1 || ( val.CustomerInfo.FullAddress != "" && val.CustomerInfo.FullAddress.search(regex) != -1 ) || val.CustomerInfo.custType.search(regex) != -1  ) {
// 					localCust.push(val);
// 				}
// 			}
// 		}
// 	});
// 	renderCustomerList(localCust);
// 	showList();
// }

// function loadAllCustomers(){
//     renderCustomerList(studentData);
// }

// REFRESH AND DISPLAY ALL GRID VIEW STUDENTS
function loadAllGridStudents(){
	
    renderDataStudent(studentData , "1");
    $('#customerSearch').val('');
    $("#searchresult").html("(Search Result : "+studentData.length+")");
    $('.custType').removeClass('active');
}

function navigateToCustomerDetailPage(context){

    localStorage.POTGCustDetailCustId = $(context).attr('custId');
    // window.open('customerDetail.html');
}


// function openUploadDialog(){
//     $('#uploadCustModal').modal('show');
// }

// function uploadCust(){

//     if( checkAuth(3, 9, 1) == -1 ){
//         toastr.error('You are not Authorised for this Activity');
//         return false;

//     }
    
//     var uploadFileName = $("#attachmentName").val();
//     if(!uploadFileName){
//         toastr.warning('Please select file for upload Customer.');
//         return false;
//     }
//     $("#addDocument").submit();
// }

// function uploadCustomer(){

//     addRemoveLoader(1);
//     $("#addDocument").off().on('submit', (function (e) {
//         //alert("asdasd");

//             var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//             var temp_json = {
//                 requestCase : "uploadMasterData",
//                 clientId: tmp_json.data[0].FK_CLIENT_ID,
//                 userId: tmp_json.data[0].PK_USER_ID,
//                 orgId: checkAuth(3, 9),
//                 dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//                 uploadType : "customerUpload",
//             };

//             $("#postDataActUpload").val(JSON.stringify(temp_json));
        
//             e.preventDefault();
//             $.ajax({
//                 url: UPLOADURL, // Url to which the request is send
//                 type: "POST", // Type of request to be send, called as method
//                 data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
//                 contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
//                 cache: false, // To unable request pages to be cached
//                 processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
//                 headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
//                 beforeSend: function () {
//                     //alert(bfsendMsg);
//                     //$("#msgDiv").html(bfsendMsg);

//                     addRemoveLoader(1);
//                     $("#ajaxloader").removeClass("hideajaxLoader");

//                 },
//                 success: function (data)// A function to be called if request succeeds
//                 {

//                     data = JSON.parse(data);
//                     console.log(data);
                    
//                     if(data.status == "Success"){
                        
//                         refreshFile();
//                         $('#errorHideDiv').hide();
//                         $('#errorDiv').html("");
//                         toastr.warning( data.data );
//                     }else{
//                         toastr.warning(data.status);
//                         refreshFile();
//                         GBL_ERROR = data.data;
//                         renderError(GBL_ERROR);
//                     }
//                     addRemoveLoader(0);
//                     $('#uploadCustModal').modal('hide');
//                 },
//                 error: function (jqXHR, errdata, errorThrown) {
//                     log("error");
//                     addRemoveLoader(0);
//                     toastr.error(errdata);
//                     if( jqXHR.responseText != "" ){
//                         GBL_ERROR = JSON.parse(jqXHR.responseText).data;
//                         renderError(GBL_ERROR);
//                     }
//                     refreshFile();
//                 }
//             });
//         }));
// }

//END FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA
function refreshFile(){

    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}


// Download Customer CSV Sample file starts here

var GBL_USERLIST = [];
// function downloadCustomerCSV(){

//     var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//     // Followup Listing Todays, Delayed, Upcomming
//     var postData = {
//         requestCase: 'getUserListing',
//         clientId: tmp_json.data[0].FK_CLIENT_ID,
//         userId: tmp_json.data[0].PK_USER_ID,
//         user_Id: tmp_json.data[0].PK_USER_ID,
//         orgId: checkAuth(19,74),
//         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//     }
//     // console.log(postData);return false;
//     commonAjax(FOLLOWUPURL, postData, downloadCustomerCSVCallback,"Please Wait... Getting Dashboard Detail");

//     function downloadCustomerCSVCallback(flag,data){

//         if (data.status == "Success" && flag) {
            
//             console.log(data);
//             GBL_USERLIST = data.data;

//             getGroupListing();
            
//         } else {

//             if (flag)
//                 displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
//             else
//                 toastr.error(SERVERERROR);
//         }
//     }
            
// }

// function getGroupListing(){

//     var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
//     // Followup Listing Todays, Delayed, Upcomming
//     var postData = {
//         requestCase: 'userCustomerGroupListing',
//         clientId: tmp_json.data[0].FK_CLIENT_ID,
//         userId: tmp_json.data[0].PK_USER_ID,
//         user_Id: tmp_json.data[0].PK_USER_ID,
//         orgId: checkAuth(17,66),
//         dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
//         searchType : "CUSTOMER"
//     }
//     // console.log(postData);return false;
//     commonAjax(FOLLOWUPURL, postData, getGroupListingCallback,"Please Wait... Getting Dashboard Detail");

//     function getGroupListingCallback(flag,data){

//         if (data.status == "Success" && flag) {

//             downloadCSVCustData( data );

//         } else {
//             downloadCSVCustData( 'No record found' );
//             if (flag)
//                 displayAPIErrorMsg( data.status , GBL_ERR_NO_CUST_GROUP );
//             else
//                 toastr.error(SERVERERROR);
//         }
//     }           
// }
// Download Customer CSV Sample file ends here

// function downloadCSVCustData( data ){

//     var groupListData = data.data;

//     var GBL_SAMPLEJSON3 = [];
//     var tempEXL = { // HEADER FOR PDF AND CSV
//         name : "Name",
//         mobile : "Mobile",
//         email : "Email",
//         address : "Address",
//         country : "Country",
//         state : "State",
//         city : "City",
//         contPerName : "ContactName",
//         contPerMob : "ContactMobile",
//         contEmail : "ContactEmail",
//         contDesig : "ContactDesignation",
//         assUser : "assign User Name",
//         industryType : "industryType",
//         rateType : "rateType",
//         catchmentArea : "catchmentArea",
//         custGroupName : "custGroupName",
//     };
//     GBL_SAMPLEJSON3.push(tempEXL);

//     GBL_SAMPLEJSON4 = [];

//     userData = GBL_USERLIST;
//     // console.log(data.data);return false;
//     var tempEXLUser = { // HEADER FOR PDF AND CSV
//         userId : "User Id",
//         userName : "User Full Name",
//     };
//     GBL_SAMPLEJSON4.push(tempEXLUser);

//     for( var i=0; i<userData.length; i++ ){
    
//         var tempEXLUser = {
//             userId: userData[i].userId,
//             userName: userData[i].userFullName,
//         };
//         GBL_SAMPLEJSON4.push(tempEXLUser);
//     }

//     var tempEXLUser = { // HEADER FOR PDF AND CSV
//         userId : "",
//         userName : "",
//     };
//     GBL_SAMPLEJSON4.push(tempEXLUser);
    
//     var tempEXLUser = { // HEADER FOR PDF AND CSV
//         userId : "Group Id",
//         userName : "Group Name",
//     };
//     GBL_SAMPLEJSON4.push(tempEXLUser);

//     if( groupListData != "No record found" ){
        
//         groupListData.forEach(function( record , index ){
//             var tempEXLUser = {
//                 userId: record.groupId,
//                 userName: record.groupName,
//             };
//             GBL_SAMPLEJSON4.push(tempEXLUser);
//         });
//     }
    
//     csvSave(GBL_SAMPLEJSON4, "user&GroupListing.csv");
//     csvSave(GBL_SAMPLEJSON3, "customerSample.csv");
// }

function downloadCustomerList(){
        
    // console.log(studentData);return false;

    var tableStartTodayHtml = '<table>';
    var tableEndHtml = '</table>';
    var threadHtml = '<thead>'+
                        '<tr>'+
                        '<th>No</th>'+
                        '<th>Name</th>'+
                        '<th>Email</th>'+
                        '<th>Mobile</th>'+
                        '<th>City</th>'+
                        '<th>State</th>'+
                        '<th>Country</th>'+
                        '</tr>'+
                    '</thead>';
    var innerHtml = "";

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    studentData.forEach(function( record , index ){

        innerHtml += '<tr>' +
                        '<td>' + (index+1) + '</td>' +
                        '<td>' + record.firstName + ' ' + record.firstName + '</td>' +
                        '<td>' + (record.email != "0" ? record.email : "" ) + '</td>' +
                        '<td>' + record.mobileNo + '</td>' +
                        '<td>' + record.cityName + '</td>' +
                        '<td>' + record.stateName + '</td>' +
                        '<td>' + record.countryName + '</td>' +
                    '</tr>';
        
    });
    var tbodyStart = '<tbody>';
    var tbodyEnd = '</tbody>';

    customCsvFormatSave(tableStartTodayHtml + threadHtml + tbodyStart + innerHtml + tbodyEnd + tableEndHtml, "CustomerList.xls");

}

var attachmetData = [];

function openAttachment(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "attachmentListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        custId: currStudId,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, sendAttachmentCallBack,CREATINGINQDATA);

    function sendAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            attachmetData = data.data;

            renderAttachment();

        }else {
            if (flag){
                // toastr.error(data.status);
            }
            else{
                // toastr.error(SERVERERROR);
            }
        }
    }
    $('#attachmentAdd').modal('show');
    // assignAttachementEvent();
}

function renderAttachment(){
    var attachLi = '<div class="row">';
    
    for( var i=0; i<attachmetData.length; i++ ){
        var iconNm = '';
        if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "pdf" ){
            iconNm = 'file-pdf-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "doc" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "docx"  ){
            iconNm = 'file-word-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "txt" ){
            iconNm = 'file-text-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "csv" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "xls" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "xlsx" ){
            iconNm = 'file-excel-o';
        }else if( (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "jpg" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "jpeg" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "png" || (attachmetData[i].fileName.split('.')[1]).toLowerCase() == "gif" ){
            iconNm = 'file-image-o';
        }
        attachLi += '<div class="col-md-3 col-sm-6 col-xs-6">'+
                        '<div class="list-content">'+
                            '<a style="text-align:center" href="javascript:void(0)" data-link="'+ attachmetData[i].fileLink +'" onclick="openLinkInNewTab(this);">'+
                                '<i class="fa fa-'+ iconNm +' iconStyle"></i>'+
                                '<a href="javascript:void(0)" attchId="'+ attachmetData[i].fileId +'" onclick="deleteAttachment(this)">'+
                                    '<i style="margin-top: 5px;" class="fa fa-trash-o pull-right"></i>'+
                                    '<span>'+ attachmetData[i].attachTitle +'</span>'+
                                '</a>'+
                            '</a>'+                            
                        '</div>'+
                    '</div>';
    }
    attachLi += '</div>';

    $('#AttachmentListing').html( attachLi );
}

function resetAttach(){
    attachmetData = [];
    $('#AttachmentListing').html('');
    $('#attachmentName').val('');
    $('#attachmentTitle').val('');
}

function submitInqAttach(){
    // grab your file object from a file input
    // $('#attachmentName').change(function () {

        var attachTitle = $('#attachmentTitle').val();
        var attachFile = $('#attachmentNameInq').val()
        if( attachTitle == "" ){
            toastr.warning('Please Enter Title');
            $('#attachmentTitle').focus();
            return false;

        }else if( attachFile == "" ){
            toastr.warning('Please Select File');
            $('#attachmentNameInq').click();
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "attachmentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7, 23),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            custId : currStudId,
            attachTitle :attachTitle,
            emailAdress : ""
        };
        $("#postData").val( JSON.stringify(temp_json));

        $("#fileupload").off().on('submit',(function(e) {
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",       // Type of request to be send, called as method
                data:  new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData:false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function(data)// A function to be called if request succeeds
                {
                    data = JSON.parse(data);
                    console.log(data);
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                            attachTitle: data.data.attachTitle,
                        }

                        attachmetData.push( tempData );
                        renderAttachment();
                    }else{
                        toastr.warning(data.status);
                    }
                    addRemoveLoader(0);

                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                }
            });
        }));
        $("#fileupload").submit();
        $('#attachmentNameInq').val('');
        $('#attachmentTitle').val('');
    // });
}

function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attchId');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'attachmentListingDelete',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 16),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        custId: currStudId,
        attachId : attachmentId
    };
    commonAjax(INQURL1, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var index = findIndexByKeyValue( attachmetData , "fileId" , attachmentId );
            
            attachmetData.splice( index , 1 );

            renderAttachment();

        }else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

function openLinkInNewTab(linkData){
    var link = $(linkData).attr("data-link");
    if(link==""){
        toastr.warning("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}


function navigateToInquiryDetailOpen(context){
    
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        var inqId = $(context).attr('inqId');
        var custId = $(context).attr('custId');
        
        localStorage.POTGclickInqId = inqId;
        localStorage.POTGcustId = custId;
        
        window.open("inquirydetail.html");
    }
}

// GET UPROCESSED INVOICES
function unprocessInvoiceDetails(context){

    var studId = $(context).attr('studId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'listChargePosting',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(24 , 98),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        studentId: studId,
    };
    commonAjax(FOLLOWUPURL, postData, unprocessInvoiceCallBack,CREATINGINQDATA);

    function unprocessInvoiceCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var unprocessInvoiceData = data.data;
            renderUnprocessInvoice( unprocessInvoiceData );
            
        }else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_UNPROC_INV );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER UPROCESSED INVOICES
function renderUnprocessInvoice( invoiceData ){

    console.log(invoiceData);
    var thead = '<table id="unProcInvTable" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>Sr. No.</th>'+
                            '<th>Item Name</th>'+
                            '<th>Item Price</th>'+
                            '<th>Cgst</th>'+
                            '<th>Sgst</th>'+
                            '<th>Igst</th>'+
                            '<th>Total Price</th>'+
                            '<th>Charge Date</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';                        
    var tEnd = '</tbody></table>';
    var newRow = "";

    if( invoiceData != "No record found" && invoiceData != "" ){

        invoiceData.forEach(function( record , index ){
            var cgstAmt = 0 , sgstAmt = 0 , igstAmt = 0;
            if( record.taxCgst != "0" ){
                cgstAmt = fixedTo2(parseFloat(record.itemBasicPrice) * parseFloat(record.taxCgst) / 100);
            }
            if( record.taxSgst != "0" ){
                sgstAmt = fixedTo2(parseFloat(record.itemBasicPrice) * parseFloat(record.taxSgst) / 100);
            }
            if( record.taxIgst != "0" ){
                igstAmt = fixedTo2(parseFloat(record.itemBasicPrice) * parseFloat(record.taxIgst) / 100);
            }
            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.itemName +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ record.itemBasicPrice +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ cgstAmt +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ sgstAmt +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ igstAmt +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ record.itemTotalPrice +'</td>'+
                            '<td>'+ ( record.chargeDate != "0000-00-00" && record.chargeDate != "" ? mysqltoDesiredFormat(record.chargeDate,'dd-MM-yyyy') : "" ) +'</td>'+
                        '</tr>';

        });

    }
    $('#dtTableUnprocInvoice').html( thead + newRow + tEnd );
    $('#unProcInvTable').DataTable();
    modalShow('unprocInvoiceModal');
}