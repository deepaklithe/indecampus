/*
 *
 * Created By : Vrushank Rindani.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 10-02-2015.
 * File : greetingControllera.
 * File Type : .js.
 * Project : eBqMs
 *
 * */

jQuery(document).ready(function (){
    
    $('#smsAnni').focus();

    $("input[type='checkbox']").on('change', function(){
        
        var anniSms = $('#smsFlag').prop('checked');
        var anniEmail = $('#emaiFlag').prop('checked');
        var bdaySms = $('#smsFlagbday').prop('checked');
        var bdayEmail = $('#emaiFlagbday').prop('checked');

        if( anniSms ){
            $('#smsAnni').removeAttr('disabled');
            $('#smsAnni').focus();
            addFocusId( 'smsAnni' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        }else{
            $('#smsAnni').attr('disabled',true);
            $('#smsBirthday').focus();
            addFocusId( 'smsBirthday' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        }

        if( anniEmail ){
            $('#emailIdAnni').removeAttr('disabled');
            $('#emailSubjectAnni').removeAttr('disabled');
            $('#emailMessageAnni').removeAttr('disabled');
        }else{
            $('#emailIdAnni').attr('disabled',true);
            $('#emailSubjectAnni').attr('disabled',true);
            $('#emailMessageAnni').attr('disabled',true);
        }

        if( bdaySms ){
            $('#smsBirthday').removeAttr('disabled');
        }else{
            $('#smsBirthday').attr('disabled',true);
        }

        if( bdayEmail ){
            $('#emailIdBirth').removeAttr('disabled');
            $('#emailsubjectBirthday').removeAttr('disabled');
            $('#emailMessageBirthday').removeAttr('disabled');
        }else{
            $('#emailIdBirth').attr('disabled',true);
            $('#emailsubjectBirthday').attr('disabled',true);
            $('#emailMessageBirthday').attr('disabled',true);
        }

    });
});

var imagePathHBD = "";
var imagePathAnni = "";
var editableFlag = "0";
var greetingId = "";

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.warning(NOTAUTH);
            navigateToIndex();
        }
    }
}
function getGreetingList() {
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "listingGreeting",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(21,82),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(URL2, postData, getGreetingListCallBack,RETRIEVINGGREETDETAIL);
}

function getGreetingListCallBack(flag, data) {
    if (data.status == "Success" && flag) {
        console.log(data);
        //success
        if(data.data.greetingDetail.editableFlag == 1){
            
            $("#smsAnni").val(data.data.greetingDetail.smsAnni);
            $("#smsBirthday").val(data.data.greetingDetail.smsBirthday);
            $("#emailIdAnni").val(data.data.greetingDetail.emailIdAnni);
            $("#emailSubjectAnni").val(data.data.greetingDetail.emailSubjectAnni);

            setTimeout( function(){
                tinyMCE.get('emailMessageAnni').setContent( data.data.greetingDetail.emailMessageAnni );
            },100);
            // $("#emailMessageAnni").val();
            
            $("#emailIdBirth").val(data.data.greetingDetail.emailIdBirth);
            $("#emailsubjectBirthday").val(data.data.greetingDetail.emailsubjectBirthday);
            
            setTimeout( function(){
                tinyMCE.get('emailMessageBirthday').setContent( data.data.greetingDetail.emailMessageBirthday );
            },100);

            $("#BirthdayLink").html(data.data.greetingDetail.emailImageBirth);
            $("#AnniLink").html(data.data.greetingDetail.emailImageAnni);

            $("#BirthdayFileName").html( (basenameWithExt(data.data.greetingDetail.emailImageBirth)).split('_')[1] );
            $("#AnniFileName").html( (basenameWithExt(data.data.greetingDetail.emailImageAnni)).split('_')[1] );

            imagePathHBD = data.data.greetingDetail.emailImageBirth;
            imagePathAnni = data.data.greetingDetail.emailImageAnni;
            editableFlag = data.data.greetingDetail.editableFlag;
            greetingId = data.data.greetingDetail.greetingId;

            if( data.data.greetingDetail.smsAnni != "" ){
                $('#smsFlag').prop('checked',true);
                $('#smsAnni').attr('disabled',false);
            }
            if( data.data.greetingDetail.emailIdAnni != "" ){
                $('#emaiFlag').prop('checked',true);
                $('#emailIdAnni').attr('disabled',false);
                $('#emailSubjectAnni').attr('disabled',false);
                $('#emailMessageAnni').attr('disabled',false);
            }
            if( data.data.greetingDetail.smsBirthday != "" ){
                $('#smsFlagbday').prop('checked',true);
                $('#smsBirthday').attr('disabled',false);
            }
            if( data.data.greetingDetail.emailIdBirth != "" ){
                $('#emaiFlagbday').prop('checked',true);
                $('#emailIdBirth').attr('disabled',false);
                $('#emailsubjectBirthday').attr('disabled',false);
                $('#emailMessageBirthday').attr('disabled',false);
            }
            
        }else{
            // $('#anniFlag').trigger('click').trigger('click');
            // $('#birthFlag').trigger('click').trigger('click');
            // $('#smsFlag').trigger('click').trigger('click');
            // $('#emaiFlag').trigger('click').trigger('click');

            $('#smsFlag').prop('checked',false);
            $('#emaiFlag').prop('checked',false);
            $('#smsFlagbday').prop('checked',false);
            $('#emaiFlagbday').prop('checked',false);

        }
    }
    else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_GREETINGS );
        else
            toastr.error(SERVERERROR);
    }
}

function saveGreeting() {

    var selected = $( ".nav.nav-tabs.tabs-left .active" ).find('a').attr('href');

    var typeOfWish = '';
    if( selected == '#tab9_1' ){
        typeOfWish = "anniversory";
        anniFlag = true;
        birthFlag = false;
        var emailFlag = $("#emaiFlag").prop("checked");
        var smsFlag = $("#smsFlag").prop("checked");
    }else{
        typeOfWish = "birthday";
        anniFlag = false;
        birthFlag = true;
        var emailFlag = $("#emaiFlagbday").prop("checked");
        var smsFlag = $("#smsFlagbday").prop("checked");
    }

    // var anniFlag = $("#anniFlag").prop("checked");
    // var birthFlag = $("#birthFlag").prop("checked");
    
    var smsAnni = $("#smsAnni").val().trim();
    var smsBirthday = $("#smsBirthday").val().trim();
    var emailIdAnni = $("#emailIdAnni").val().trim();
    var emailSubjectAnni = $("#emailSubjectAnni").val().trim();

    // var emailMessageAnni = $("#emailMessageAnni").val().trim();
    var emailMessageAnni = tinyMCE.get('emailMessageAnni').getContent();

    var emailIdBirth = $("#emailIdBirth").val().trim();
    var emailsubjectBirthday = $("#emailsubjectBirthday").val().trim();
    
    // var emailMessageBirthday = $("#emailMessageBirthday").val().trim();
    var emailMessageBirthday = tinyMCE.get('emailMessageBirthday').getContent();
    
    var chckAnnSms = $('#smsFlag').prop('checked');
    var chckAnnMail = $('#emaiFlag').prop('checked');
    var chckBdaySms = $('#smsFlagbday').prop('checked');
    var chckBdayMail = $('#emaiFlagbday').prop('checked');

  
    if (!((anniFlag || birthFlag) && (emailFlag || smsFlag))) {
        toastr.warning(GRECHKBOX);
        return false;
    }  
    if (anniFlag && smsFlag && smsAnni.length < 1) {
        toastr.warning(ANNISMS);
        return false;
    }
    if(!chckAnnMail){
        emailIdAnni = "";
        emailSubjectAnni = "";
        emailMessageAnni = "";
    }
    if(!chckAnnSms){
        smsAnni = "";
    }
    if(!chckBdayMail){
        emailIdBirth = "";
        emailsubjectBirthday = "";
        emailMessageBirthday = "";
    }
    if(!chckBdaySms){
        smsBirthday = "";
    }
    if (anniFlag && emailFlag && emailMessageAnni.length < 1) {
        toastr.warning(ANNIEMAILTXT);
        return false;
    } 
    if (anniFlag && emailFlag && emailSubjectAnni.length < 1) {
        toastr.warning(ANNIEMAILSUB);
        return false;
    } 
    if (anniFlag && emailFlag && emailIdAnni.length < 1) {
        toastr.warning(ANNIEMAIL);
        return false;
    } 
    if (anniFlag && emailFlag && emailIdAnni.length > 1) {
        if(!validateEmail(emailIdAnni)){
            toastr.warning(EMAILVALID);
            return false;
        }
    }
    if (birthFlag && smsFlag && smsBirthday.length < 1) {
        toastr.warning(HBDSMS);
        return false;
    }
    if (birthFlag && emailFlag && emailMessageBirthday.length < 1) {
        toastr.warning(HBDEMAILTXT);
        return false;
    }
    if (birthFlag && emailFlag && emailsubjectBirthday.length < 1) {
        toastr.warning(HBDEMAILSUB);
        return false;
    }
    if (birthFlag && emailFlag && emailIdBirth.length < 1) {
        toastr.warning(HBDEMAIL);
        return false;
    }
    if (birthFlag && emailFlag && emailIdBirth.length > 1) {
        if(!validateEmail(emailIdBirth)){
            //toastr.warning("daeepak");
            toastr.warning(EMAILVALID);
            return false;
        }
    }
        //// UPDATING GREETING
        //toastr.warning("i ma here"); return true;
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var orgId = checkAuth(21, 82);
        if(editableFlag == 0){
            orgId = checkAuth(21, 82);
        }
        var postData = {
            requestCase: "updateGreeting",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId:orgId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            anniFlag : anniFlag ? 1 : 0,
            birthdayFalg : birthFlag ? 1 : 0,
            smsFlag :smsFlag ? 1 : 0,
            emailFlag :emailFlag ? 1 : 0,
            smsTextBirthday : smsBirthday,
            smsTextAnni : smsAnni,
            emailIdAnni : emailIdAnni,
            emailSubjectAnni : emailSubjectAnni,
            emailMessageAnni : emailMessageAnni,
            emailImageAnni : $("#AnniLink").html(),
            emailIdBithday : emailIdBirth,
            emailSubjectBithday : emailsubjectBirthday,
            emailMessageBithday : emailMessageBirthday,
            emailImageBithday : $("#BirthdayLink").html()
        }
        if(editableFlag == 1){
            postData.greetingId = greetingId;
        }
        // log(postData); return false;
        commonAjax(URL2, postData, saveGreetingCallBack,SENDINGGREETCREATE);
}

function saveGreetingCallBack(flag,data) {
    if (data.status == "Success" && flag) {
        log(data);
        //success
        reloadWin();
        
    }
    else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_GREETINGS );
        else
            toastr.warning(SERVERERROR);
    }
}


function assignAttachementEventForAnni() {
    // grab your file object from a file input
    $('#attachmentNameAnn').change(function () {
        // $("#ajaxloader").removeClass("hideajaxLoader");
        addRemoveLoader(1);
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase: "actAttachmentUploadOnly",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(21, 71),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };

        $("#postDataAnn").val(JSON.stringify(temp_json));

        $("#fileuploadAnn").off().on('submit', (function (e) {
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",      	// Type of request to be send, called as method
                data: new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData: false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function (data)// A function to be called if request succeeds
                {

                    var data = JSON.parse(data);
                    //log(data);
                    
                    if (data.status == "Success") {
                        imagePathAnni = data.data.fileName;
                        $("#AnniLink").html(data.data.fileLink);
                    } else {
                        toastr.warning(data.status);

                    }
                    addRemoveLoader(0);
                    // $("#ajaxloader").addClass("hideajaxLoader");
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                    // $("#ajaxloader").addClass("hideajaxLoader");
                }
            });
        }));
        $("#fileuploadAnn").submit();
    });
}

function assignAttachementEventForHBD() {
    // grab your file object from a file input
    $('#attachmentNameHBD').change(function () {
        // $("#ajaxloader").removeClass("hideajaxLoader");
        addRemoveLoader(1);
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase: "actAttachmentUploadOnly",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(21, 71),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };

        $("#postDataHBD").val(JSON.stringify(temp_json));

        $("#fileuploadHBD").off().on('submit', (function (e) {
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",      	// Type of request to be send, called as method
                data: new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData: false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function (data)// A function to be called if request succeeds
                {

                    data = JSON.parse(data);
                    log(data);
                    if (data.status == "Success") {
                        imagePathHBD = data.data.fileName;
                        $("#BirthdayLink").html(data.data.fileLink);
                    } else {
                        toastr.warning(data.status);

                    }
                    addRemoveLoader(0);
                    // $("#ajaxloader").addClass("hideajaxLoader");
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                    // $("#ajaxloader").addClass("hideajaxLoader");
                }
            });
        }));
        $("#fileuploadHBD").submit();
    });
}


function autoCompleteSearchBarCustomer(id, callback) {   
    
    
    $("#" + id).typeahead({        
        onSelect: function (item) {
            //log(item);

            localStorage.indeCampusCallAcceptFlag = '0';
            $(".custInfo").hide();
            if(item.value == "-1"){
                var tempData = item.text.split(":");
                //log(tempData);
                showCustPopup();
                if(!isNaN(tempData[0])){
                    $("#custmobile").val("+91"+tempData[0]);
                    $("#custcontmobile").val("+91"+tempData[0]);
                }else{
                    $("#custfnm").val(tempData[0]);
                    $("#custmobile").val("+91");
                    $("#companyName").val(tempData[0]);
                    $("#custcontmobile").val("+91");
                }
                var data = JSON.parse(localStorage.indeCampusRoleAuth);
                $("#Ethnicity").val(data.data.inqDefaultSetting.ethnicityId);
                $("#autocomplete").val('');
            }else{
                var index = findIndexByKeyValue(ITEMS, "id", item.value);
                setTimeout(function () {
                    $("#" + id).val(ITEMS[index].data);
                    localStorage.POTGselectedCustIdInSearch = ITEMS[index].custId;
                    localStorage.POTGselectedInquiryIdInSearch = ITEMS[index].name;
                    // function we want to run
                    var fnstring = localStorage.POTGcalltype;

                    // find object
                    var fn = window[fnstring];

                    // is object a function?
                    if (typeof fn === "function") 
                    fn();
                }, 1);
            }
            
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 1,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) { 
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                addRemoveLoader(1);
                var postData = {
                    requestCase: localStorage.POTGsearchType,
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    keyWord : query,
                    orgId: localStorage.POTGauth
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postData};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                
                if (data.data != null) {
                    //log(data.data);
                    if(data.data.searchType == "event"){
                        ITEMS = [];
                        $.map(data.data.searchData, function (data) {
                            log(data);
                            var group = {
                                id: data.inqId,
                                custId: data.custId,
                                name: data.inqId,
                                data: data.inqId,
                            };
                            ITEMS.push(group);
                        });
                    }else if(data.data.searchType == "inquiry"){
                        ITEMS = [];
                        $.map(data.data.searchData, function (data) {
                            log(data);
                            var group = {
                                id: data.inqId,
                                custId: data.custId,
                                name: data.inqId,
                                data: data.inqId,
                            };
                            ITEMS.push(group);
                        });
                    }else{
                        $('#autocomplete').removeClass('loading'); //Added by Nazim khan on 13/07/2015
                        ITEMS = [];
                        $.map(data.data.searchData, function (data) {                            
                            var group = {
                                id: data.cust_id,
                                custId: data.cust_id,
                                name: data.concatKey,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname
                            };
                            ITEMS.push(group);
                        });
                    }
                    return ITEMS;
                }

            }
        }
    });
}

function pageInitialEvents(){
    
    setTimeout(function() {
        $("#crmDashboardSearch").trigger('click');
        autoCompleteSearchBarCustomer("autocomplete", navigateToDashboard);
    },100);
    //commonAutoComplete("autocomplete", navigateToCustDashboard);
    assignAttachementEventForAnni();
    assignAttachementEventForHBD();
    
    
    getGreetingList();
    $(".selectpicker").css({'min-height': "114px"});
    // $(".addscroll2").niceScroll({
    //     styler: "fb",
    //     cursorcolor: "#6dbb4a",
    //     cursorwidth: '6',
    //     cursorborderradius: '10px',
    //     background: '#404040',
    //     spacebarenabled: false,
    //     cursorborder: '',
    //     zindex: '1000'
    // });

    addFocusId( 'smsAnni' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    addFocusId( 'smsBirthday' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}