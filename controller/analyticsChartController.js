/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var funnelChart = "";
var empBookingChart = "";
var inqColumnChart = "";
var empPendingChart = "";
var dealLostChart = "";
var dealWonChart = "";
var empEffctChart = "";
var monthClosureChart = "";
var InquiryClosureChart = "";
var sourceWiseChart = "";
var typeWiseChart = "";
var predStackChart = "";
var userWiseOpenInquiryChart = "";
var closureSpreadChart = "";
var inquiryConversionChart = "";
var salesSpreadChart = "";

function getAnalyticData( stDate , enDate ){

    var inqNum = $('#inqNum').val();

    if( inqNum == "-1" ){
        toastr.warning('Please Search Inquiry');
        $('#inqNum').focus();
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getAnalyticChartReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        startDate : stDate,   //START DATE
        endDate : enDate,   //END DATE
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(FOLLOWUPURL, postData, getAnalyticDataCallback,"Please Wait... Getting Dashboard Detail");

    function getAnalyticDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log( data.data );

            funnelChart = data.data.funnelChart;          //Inquiry Funnel Chart
            empBookingChart = data.data.empwiseStatusChartBookLost;        //Employee wise Pipeline stage status
            inqColumnChart = data.data.statusChangeAverageAging;       //Inquiry closure time – status change 
            empPendingChart = data.data.empwiseStatusChartPending;        //Employee wise Pipeline stage status
            dealLostChart = data.data.dealLostChart;        //Deal lost reason analysis – Bar chart 
            dealWonChart = data.data.dealWonChart;        //Deal lost reason analysis – Bar chart 
            empEffctChart = data.data.empwiseEffectiveChart;        //Employee Effectiveness – deal closure % - dial chart  
            monthClosureChart = data.data.targetAchieveCurrFiscal;    //Monthly closure – amount bar chart – target vs achievement
            InquiryClosureChart = data.data.dateWiseData;  //target date vs actual – column chart
            sourceWiseChart = data.data.inqSourceChart;      //source wise, type – pie chart     
            typeWiseChart = data.data.inqTypeChart;      //source wise, type – pie chart     
            predStackChart = data.data.dealProbabilityChanceChart;      //source wise, type – pie chart     
            userWiseOpenInquiryChart = data.data.empwiseStatusChartPending;      //source wise, type – pie chart     
            inquiryConversionChart = data.data.inquiryConversion;      //source wise, type – pie chart     
            salesSpreadChart = data.data.salesSpread.salesSpread;      //source wise, type – pie chart     
            $('#1stLi a').click();
            
        } else {
            if (flag)
                toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
        
    }
}

//Inquiry Funnel Chart
function funnelChartFun(){
    
    chartData = funnelChart;

    if( chartData.length == 0 ){
      return false;
    }
    
    var chartArr = [];
    chartData.forEach(function(record,index) {
          
        var clrCode = "#F00";
        if( index == 0 ){
            clrCode = "#a8c81f"
        }else if( index == 1 ){
            clrCode = "#118acb"
        }else if( index == 2 ){
            clrCode = "#40b4a6"
        }else if( index == 3 ){
            clrCode = "#008d36"
        }else if( index == 4 ){
            clrCode = "#F00"
        }else{
            clrCode = "#F00"
        }

        var potSize =(record.potSize) ? parseFloat(record.potSize) : "0.00";
        var potWithNF = 'Rs. ' + potSize.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        
        var temp = {
            "category": record.statusName +' <br> ' + potWithNF,
            "value": parseFloat( record.potSize ),
            "value2": -Math.abs(parseFloat( record.potSize )),
            "color": clrCode
        }
        chartArr.push( temp );
    });
    
    var lastEle = {
        "category": 'ooooo',
        "value": 0,
        "value2": -0,
        "color": '#F00'
    }
    chartArr.push( lastEle );

    var chart = AmCharts.makeChart("inqFunnel", {
        "type": "serial",
        "addClassNames": true,
        "showBalloon" : true,
        "categoryField": "category",
        "categoryAxis": {
            "startOnAxis": true,
            "axisAlpha": 0.1,
            "gridPosition": "left",
            "gridAlpha": 0.1,
            "tickLength": 20,
            "tickPosition": "start",
            "showLastLabel": true
        },
        "valueAxes": [{
            "axisAlpha": 0,
            "labelsEnabled": false,
            "gridAlpha": 0
        }],
        // "dataProvider": [
        //       {
        //         "category": "Open <br> 896079",
        //         "value": 896079,
        //         "value2": -896079,
        //         "color": "#a8c81f"
        //       },{
        //         "category": "Inprocess <br> 670080",
        //         "value": 670080,
        //         "value2": -670080,
        //         "color": "#118acb"
        //       }, {
        //         "category": "confirm <br> 566712",
        //         "value": 566712,
        //         "value2": -566712,
        //         "color": "#40b4a6"
        //       }, {
        //         "category": "Out For Delievery <br> 436498",
        //         "value": 436498,
        //         "value2": -436498,
        //         "color": "#008d36"
        //       },{
        //         "category": "Delievered <br> 170000",
        //         "value": 170000,
        //         "value2": -170000,
        //         "color": "#F00"
        //       },{
        //         "category": "oooo <br> 436498",
        //         "value": 0,
        //         "value2": -0,
        //         "color": "#F00"
        //       }
        // ],
        "dataProvider": chartArr,
        "graphs": [
            {
                "id": "fromGraph",
                "lineAlpha": 0,
                "showBalloon": true,    
                "valueField": "value2",
                "fillAlphas": 0
            }, {
                "fillAlphas": 1,
                "fillToGraph": "fromGraph",
                "lineAlpha": 0,
                "lineColorField": "color",
                "fillColorsField": "color",
                "showBalloon": true,
                "valueField": "value"
            }
        ],
          
    });
}

function inqColumnChartFun(){
    
    chartData = inqColumnChart;

    if( chartData.length == 0 ){
      return false;
    }
    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {

        var chartArr = [];
        var firstArr = [];

        firstArr.push( 'Open' );
        firstArr.push( 'Aging' );
        firstArr.push( { role: 'style' } );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
              
            var clrCode = "";
            if( index == 0 ){
                clrCode = "#b87333"
            }else if( index == 1 ){
                clrCode = "silver"
            }else if( index == 2 ){
                clrCode = "gold"
            }else if( index == 3 ){
                clrCode = "#e5e4e2"
            }else if( index == 4 ){
                clrCode = "#b87333"
            }else{
                clrCode = "silver"
            }
            var temp = [];
            temp.push(record.statusName);
            temp.push(parseFloat(record.averageDays));
            temp.push(clrCode);
            chartArr.push( temp );
        });

         var data = google.visualization.arrayToDataTable( chartArr );
         
          // var data = google.visualization.arrayToDataTable([
          //    ['Open', 'Aging', { role: 'style' }],
          //    ['InProcess', 8.94, '#b87333'],            // RGB value
          //    ['Confirm', 10.49, 'silver'],            // English color name
          //    ['Closed', 19.30, 'gold'],

          //  ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
          // ]);
          var options = {
            hAxis: {
              title: 'Status Aging',
            },
            vAxis: {
              title: 'No Of Days (In Days)'
            },
            height : "500",
            width : "100%",
                animation: {
                      duration: 1000,
                      easing: 'linear',
                      startup: true
                }
          };

        var chart = new google.visualization.ColumnChart(
        document.getElementById('inqTime'));
        chart.draw(data, options);
    }
}

//Deal Lost Reason chart
function dealWonChartFun(){
    
    chartData = dealWonChart;

    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawBarColors);
    },100);
    console.log(chartData)
    function drawBarColors() {

          var chartArr = [];
          var firstArr = [];

          firstArr.push( 'Element' );
          firstArr.push( 'Won' );
          firstArr.push( { role: 'style' } );
          firstArr.push( { role: 'annotation' } );

          chartArr.push( firstArr );
          chartData.forEach(function(record,index) {
                
              var clrCode = "";
              if( index == 0 ){
                  clrCode = "#b87333"
              }else if( index == 1 ){
                  clrCode = "silver"
              }else if( index == 2 ){
                  clrCode = "gold"
              }else if( index == 3 ){
                  clrCode = "#e5e4e2"
              }else if( index == 4 ){
                  clrCode = "#b87333"
              }else{
                  clrCode = "silver"
              }
              var temp = [];
              temp.push(record.statusName);
              temp.push(parseFloat(record.inqCount));
              temp.push(clrCode);
              temp.push(record.statusName);
              chartArr.push( temp );
          });
          // var data = google.visualization.arrayToDataTable([
          //    ['Element', 'Lost', { role: 'style' }],
          //    ['Open', 8.94, '#b87333'],            // RGB value
          //    ['Inprocess', 10.49, 'silver'],            // English color name
          //    ['Confirm', 19.30, 'gold'],
          //    ['Closed', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
          // ]);
          
          var data = google.visualization.arrayToDataTable(chartArr);
          var options = {
            title: '',
            height : "500",
            width : "100%",
            colors: ['#b0120a', '#ffab91'],
            hAxis: {
              title: 'Total Deal Won',
              minValue: 0
            },
            vAxis: {
              title: 'Won Reason'
            },
                animation: {
                duration: 1000,
                easing: 'linear',
                startup: true
                }
          };
          var chart = new google.visualization.BarChart(document.getElementById('dealWon'));
          chart.draw(data, options);
    }
}

//Deal Lost Reason chart
function dealLostChartFun(){
    
    chartData = dealLostChart;

    if( chartData.length == 0 ){
      return false;
    }
    console.log(chartData)

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawBarColors);
    },100);

    function drawBarColors() {

          var chartArr = [];
          var firstArr = [];

          firstArr.push( 'Element' );
          firstArr.push( 'Lost' );
          firstArr.push( { role: 'style' } );
          firstArr.push( { role: 'annotation' } );

          chartArr.push( firstArr );
          chartData.forEach(function(record,index) {
                
              var clrCode = "";
              if( index == 0 ){
                  clrCode = "#b87333"
              }else if( index == 1 ){
                  clrCode = "silver"
              }else if( index == 2 ){
                  clrCode = "gold"
              }else if( index == 3 ){
                  clrCode = "#e5e4e2"
              }else if( index == 4 ){
                  clrCode = "#b87333"
              }else{
                  clrCode = "silver"
              }
              var temp = [];
              temp.push(record.statusName);
              temp.push(parseFloat(record.inqCount));
              temp.push(clrCode);
              temp.push(record.statusName);
              chartArr.push( temp );
          });
          // var data = google.visualization.arrayToDataTable([
          //    ['Element', 'Lost', { role: 'style' }],
          //    ['Open', 8.94, '#b87333'],            // RGB value
          //    ['Inprocess', 10.49, 'silver'],            // English color name
          //    ['Confirm', 19.30, 'gold'],
          //    ['Closed', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
          // ]);
          
          var data = google.visualization.arrayToDataTable(chartArr);
          var options = {
            title: '',
            height : "500",
            width : "100%",
            colors: ['#b0120a', '#ffab91'],
            hAxis: {
              title: 'Total Deal Lost',
              minValue: 0
            },
            vAxis: {
              title: 'Lost Reason'
            },
                animation: {
              duration: 1000,
              easing: 'out',
              startup: true
                }
          };
          var chart = new google.visualization.BarChart(document.getElementById('dealLost'));
          chart.draw(data, options);
    }
}

//Inquiry Closure Chart
function empBookingChartFun(){
    
    chartData = empBookingChart;

    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {

        var chartArr = [];
        var temp = [];

        temp.push('Genre');
        chartData.userWiseChartData[0].userWiseData.forEach(function(record,index) {
            temp.push(record.fullname);
        });
        temp.push( { role: 'annotation' } );
        chartArr.push( temp );  

        chartData.userWiseChartData.forEach(function(record,index) {          
            var temp = [];
            temp.push( record.concatKey );  
            record.userWiseData.forEach(function(innrRec,innrIndex) {      
                temp.push( parseFloat(innrRec.statusInqCount) );                     
            });
            temp.push('');
            chartArr.push( temp );
        });
        console.log(chartArr);
        // var data = google.visualization.arrayToDataTable([
        //     ['Genre', 'Nazim', 'Rajesh', 'ajay', 'vijay',
        //      'Nazim', 'vijay', { role: 'annotation' } ],
        //     ['4 / 2016', 10, 24, 20, 32, 18, 5, ''],
        //     ['4 / 2017', 10, 24, 20, 32, 18, 5, ''],
        //     ['4 / 2018', 10, 24, 20, 32, 18, 5, ''],
        //   ]);
        
        var data = google.visualization.arrayToDataTable( chartArr );

          var options = {
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '100' },
            isStacked: true,
            height : "500",
            width : "100%",
            hAxis: {
              title: 'Month',
            },
            vAxis: {
              title: 'Counts (User Wise)'
            },
                animation: {
              duration: 1000,
              easing: 'linear',
              startup: true
                }
          };
            
            var chart = new google.visualization.ColumnChart(
            document.getElementById('bookChrt'));

          chart.draw(data, options);
    }
}

//Open Inquiry Chart
function empPendingChartFun(){

    chartData = inquiryConversionChart;

    if( chartData.length == 0 ){
      return false;
    }

      setTimeout(function(){
        google.charts.setOnLoadCallback(drawChart);
      },100);
      function drawChart() {

        var chartArr = [];
        var firstArr = [];

        firstArr.push( 'Status' );
        firstArr.push( 'Count' );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
              
            var temp = [];
            temp.push(record.statusName);
            temp.push(parseFloat(record.statusCount));
            chartArr.push( temp );
        });

        var data = google.visualization.arrayToDataTable( chartArr );

        var options = {
          sliceVisibilityThreshold: .0,
          pieHole: 0.3,
          legend: 'none',
          pieSliceText: 'label',
          height:500,
          width : "100%",
          // is3D: true,
              animation: {
              duration: 1000,
              easing: 'linear',
              startup: true
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('empStack'));

        chart.draw(data, options);
      }
}

//Open Inquiry Chart
function userWiseOpenInquiryFun(){

    chartData = userWiseOpenInquiryChart;

    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {

        var chartArr = [];
        var temp = [];

        temp.push('Genre');
        chartData.allStatus.forEach(function(record,index) {
            temp.push(record.STATUS_NAME);
        });
        temp.push( { role: 'annotation' } );
        chartArr.push( temp );  

        chartData.userWiseChartData.forEach(function(record,index) {
          
          var temp = [];
          
          temp.push( record.fullname ); 
          record.statusWiseData.forEach(function(innrRec,innrIndex) {
              temp.push( parseFloat(innrRec.statusInqCount) );
          });
          temp.push('');
          chartArr.push( temp );
        });

        // var data = google.visualization.arrayToDataTable([
        //     ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General',
        //      'Western', 'Literature', { role: 'annotation' } ],
        //     ['Nazim', 10, 24, 20, 32, 18, 5, ''],
        //   ]);
        
        var data = google.visualization.arrayToDataTable( chartArr );

          var options = {
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '100' },
            isStacked: true,
            height : "500",
            width : "100%",
            hAxis: {
              title: 'Users',
            },
            vAxis: {
              title: 'Counts'
            },
                animation: {
              duration: 1000,
              easing: 'linear',
              startup: true
            }
          };
            
            var chart = new google.visualization.ColumnChart(
            document.getElementById('userWiseOpenInq'));

          chart.draw(data, options);
    }
}

//Employee Effectiveness chart
function empEffctChartFun(){
    
    chartData = empEffctChart;

    if( chartData.length == 0 ){
      return false;
    }
    
    setTimeout(function(){
      google.charts.setOnLoadCallback(drawChart);
    },100);
    
    function drawChart() {

        var chartArr = [];
        var firstArr = [];

        firstArr.push( 'Label' );
        firstArr.push( 'Value' );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
            
          var temp = [];
          temp.push(record.userName);
          temp.push(parseFloat(record.effPer));
          chartArr.push( temp );
        });

        // var data = google.visualization.arrayToDataTable([
        //   ['Label', 'Value'],
        //   ['Nazim khan', 80],
        //   ['CPU', 55],
        //   ['Network', 68],
        //   ['Memory', 80]
        // ]);
        var data = google.visualization.arrayToDataTable( chartArr );

        var options = {
          
          redFrom: 0, redTo: 40,
          yellowFrom:40, yellowTo: 70,
          greenFrom:70, greenTo:100,
          minorTicks: 5,
          
        legend: { position: true },
          height : "500",
          width : "100%",
              animation: {
            duration: 1000,
            easing: 'inAndOut',
            startup: true
          }
        };

        var chart = new google.visualization.Gauge(document.getElementById('empEffct'));

        chart.draw(data, options);
        
    }
}

//Monthly Closure Chart
function monthClosureChartFun(){
    
    chartData = monthClosureChart;

    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {
        var chartArr = [];
        var firstArr = [];

        // chartData.sort(function(a, b) {
        //   return a.month - b.month;
        // });

        firstArr.push( 'Year' );
        firstArr.push( 'Target' );
        firstArr.push( 'Achievement' );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
              
            var temp = [];
            temp.push(record.month);
            temp.push(parseFloat(record.target));
            temp.push(parseFloat(record.achievement));
            chartArr.push( temp );
        });

         // var data = google.visualization.arrayToDataTable([
         //      ['Year', 'Target', 'Achievement'],
         //      ['Jan', 1000, 400],
         //      ['Feb', 1170, 460],
         //      ['Mar', 660, 1120],
         //      ['Apr', 1030, 540],
         //    ]);
           var data = google.visualization.arrayToDataTable( chartArr );

            var options = {
                chart: {
                  title: '',
                  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
                        animation: {
                    duration: 1000,
                    easing: 'out',
                    startup: true
                        },
                hAxis: {
                    title: 'Month',
                  },
                vAxis: {
                  title: 'Target Amount (in Lakhs)'
                },
                height : "500",
                width : "100%",
            };

          var chart = new google.visualization.ColumnChart(
            document.getElementById('monthChart'));

          chart.draw(data, options);
    }
}

//Inquiry Closure Date Chart
function InquiryClosureChartFun(){
    
    chartData = InquiryClosureChart;
    //console.log(chartData);
    
        
    if( chartData.length == 0 ){
      return false;
    }
    
    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {
        var chartArr = [];
        var temp = [];
        var firstArr = [];
 
        
        firstArr.push( 'Year' );
        firstArr.push( 'Closure' );
        firstArr.push( 'Actual' );
        
        chartArr.push( firstArr );
        
        chartData.forEach(function(record,index) {
              //console.log(record.dayDiff);return false;
            var temp = [];
            temp.push(record.dayDiff);
            temp.push(record.closureDays);
            temp.push(  record.actualDays);
            chartArr.push( temp );
        });
        
        var data = google.visualization.arrayToDataTable( chartArr );
            var options = {
                chart: {
                    title: '',
                    subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
                hAxis: {
                  title: 'Days'
                },
                vAxis: {
                  title: 'Target Days',
                },
                height : "500",
                width : "100%",
                        animation: {
                  duration: 1000,
                  easing: 'out',
                  startup: true
                        }
            };

          var chart = new google.visualization.ColumnChart(
            document.getElementById('inqClosure'));

          chart.draw(data, options);
    }
}

//Inquiry List Source Wise chart
function sourceWiseChartFun(){
    
    chartData = sourceWiseChart;

    if( chartData.length == 0 ){
      return false;
    }

      setTimeout(function(){
        google.charts.setOnLoadCallback(drawChart);
      },100);
      function drawChart() {

        var chartArr = [];
        var firstArr = [];

        firstArr.push( 'Inquiry' );
        firstArr.push( 'Count' );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
              
            var temp = [];
            temp.push(record.statusName);
            temp.push(parseFloat(record.inqCount));
            chartArr.push( temp );
        });

        /* var data = google.visualization.arrayToDataTable([
              ['Inquiry', 'Count'],
              ['Email',     11],
              ['Message',      2],
              ['Hordings',  2],
              ['Word of Mouth', 2],
              ['News',    7]
        ]);*/
        var data = google.visualization.arrayToDataTable( chartArr );

        var options = {
          title: '',
          legend: 'none',
          is3D: true,
          pieSliceText: 'label',
          height : "500",
          width : "100%",
              animation: {
            duration: 1000,
            easing: 'linear',
                    startup: true
                  }
        };

        var chart = new google.visualization.PieChart(document.getElementById('sourceWise'));

        chart.draw(data, options);
      }
}

//Inquiry List Type Wise chart
function typeWiseChartFun(){
    
    chartData = typeWiseChart;

    if( chartData.length == 0 ){
      return false;
    }

      setTimeout(function(){
        google.charts.setOnLoadCallback(drawChart);
      },100);
      function drawChart() {

        var chartArr = [];
        var firstArr = [];

        firstArr.push( 'Inquiry' );
        firstArr.push( 'Count' );

        chartArr.push( firstArr );
        chartData.forEach(function(record,index) {
              
            var temp = [];
            temp.push(record.statusName);
            temp.push(parseFloat(record.inqCount));
            chartArr.push( temp );
        });

        //  var data = google.visualization.arrayToDataTable([
        //       ['Inquiry', 'Count'],
        //       ['Email',     25],
        //       ['SMS',      9],
        //       ['WalkIn',  4]
        // ]);

        var data = google.visualization.arrayToDataTable( chartArr );

        var options = {
          title: '',
          legend: 'none',
          is3D: true,
          pieSliceText: 'label',
          height : "500",
          width : "100%",
              animation: {
                duration: 1000,
                easing: 'out',
                startup: true
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('typeWise'));

        chart.draw(data, options);
      }
}

//Predictive Sales Chart
function predStackChartFun(){

    chartData = predStackChart;

    if( chartData.length == 0 ){
      return false;
    }

    setTimeout(function(){
      google.charts.setOnLoadCallback(drawMultSeries);
    },100);
    
    function drawMultSeries() {

        var chartArr = [];
        var temp = [];

        temp.push('Genre');
        chartData.allstatus.forEach(function(record,index) {
            if( record != "No record found" ){
              temp.push(record.STATUS_NAME);
            }
        });
        temp.push( { role: 'annotation' } );
        chartArr.push( temp );

        chartData.statusWiseChartData.forEach(function(record,index) {
          
          var temp = [];
          if( record != "No record found" ){
                temp.push( record.month ); 
                record.statusWiseData.forEach(function(inrRcrd,index) {
                    temp.push( parseFloat(inrRcrd.potgSize) );
                })
                temp.push( '' );
                chartArr.push( temp );
          }
        });

        // var data = google.visualization.arrayToDataTable([
        //     ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General',
        //      'Western', 'Literature', { role: 'annotation' } ],
        //     ['Nazim', 10, 24, 20, 32, 18, 5, ''],
        // ]);
        
        var data = google.visualization.arrayToDataTable( chartArr );

          var options = {
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '100%' },
            isStacked: true,
            hAxis: {
              title: 'Status',
            },
            vAxis: {
              title: 'Amounts (in Rs.)'
            },
            height : "500",
            width : "100%",
                animation: {
                    duration: 1000,
                    easing: 'linear',
                    startup: true
                }
          };
            
            var chart = new google.visualization.ColumnChart(
            document.getElementById('predSales'));

          chart.draw(data, options);
    }
}

function closureSpreadChartFun(){
  
  var geoData = salesSpreadChart;
  // geoData = "No record found";
  
  var geoDataArr = [];
  if( geoData != "No record found" ){
      var geoHeader = ['City', 'Sales', 'Customers'];
      geoDataArr.push( geoHeader );
      for(var g=0;g<geoData.length;g++){
          var tempArr = [geoData[g]['cityName'],parseFloat(geoData[g]['totalSales']),parseFloat(geoData[g]['customerCount'])];
          geoDataArr.push( tempArr );
      }
  }else{
      var tempArr = [
        ['', { role: 'annotation' }],
        ['', '']
      ];
      geoDataArr.push( tempArr );
  }

  google.charts.setOnLoadCallback(drawMarkersMap);
  function drawMarkersMap() {
    
      var data = google.visualization.arrayToDataTable( geoDataArr );

      var options = {
        // region: 'IN',
        displayMode: 'markers',
        colorAxis: {colors: ['red', 'green']},
        height:500
      };

      var chart = new google.visualization.GeoChart(document.getElementById('closureSpread'));
      chart.draw(data, options);
  };
}

//PAGE INITIAL FUNCTION
function pageInitialEvents(){
                    
    //DATE RANGE PICKER ADDED
    var start = moment();
    var end = moment();

    stDate = moment().subtract(1, 'day').format('YYYY-MM-DD');
    enDate = moment().subtract(1, 'day').format('YYYY-MM-DD');
    
    stDate = getCurrentFiscalYear().split('-')[0] +'-04-01';
    enDate = getCurrentFiscalYear().split('-')[1] +'-03-31'; 

    start = moment(stDate);
    end = moment(enDate);
    
    $('#dateRangePicker span').html( moment(stDate).format('DD, MMMM, YYYY') + ' - ' + moment(enDate).format('DD, MMMM, YYYY') );
    $('#dateRangePicker').daterangepicker(
    {   
        maxDate: end,
        startDate: start,
        endDate: end,
        opens:'left',
        locale: {
                format: 'YYYY-MM-DD'
            },
        }, function(start, end, label) {

             stDate = start.format('YYYY-MM-DD');
             enDate = end.format('YYYY-MM-DD');

            $('#dateRangePicker span').html(start.format('DD, MMMM, YYYY') + ' - ' + end.format('DD, MMMM, YYYY'));
            console.log(stDate);
            console.log(enDate);

            getAnalyticData( stDate , enDate );  //GET ANALYTIC DATA
    });

    getAnalyticData( stDate , enDate );  //GET ANALYTIC DATA

    funnelChartFun();

    //LOAD GOOGLE CHART
    google.charts.load('current', {packages: ['corechart', 'bar' , 'gauge' , 'geochart']});

    var myURL = (window.location.href).split('#')[0]; 
    document.location = myURL + "#tab_chart1";

    setTimeout(function(){
        $('li').removeClass('active');
        $('#1stLi').addClass('active');
        $('#1stLi a').click();
        $('.tab-content .tab-pane').removeClass('active');
        $('#chart1').addClass('active');
    },500);
}