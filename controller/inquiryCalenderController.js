var GBL_PROPERTY_DATA = [];
var GBL_TIMEZONE_DATA = [];
var GBL_BOOKING_DATA = [];
// var gbl_property_id = "-1";

var dayEnd = lastday(getTodaysDate().split('-')[0], (getTodaysDate().split('-')[1] - 1));

var startDate = getTodaysDate().split('-')[0] + '-' + getTodaysDate().split('-')[1] + '-01';
var endDate = getTodaysDate().split('-')[0] + '-' + getTodaysDate().split('-')[1] + dayEnd;

var GBL_PENDING_STAT = "";
var GBL_BOOKED_STAT = "";
var GBL_CANCEL_STAT = "";

if (localStorage.indeCampusRoleAuth != "" && localStorage.indeCampusRoleAuth != undefined) {
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);

    var index = findIndexByKeyValue(roleAuth.data.statusListing, "statusCatName", "BOOKING");
    if (index != "-1") {

        //BOOKED STATUS
        var bookIndex = findIndexByKeyValue(roleAuth.data.statusListing[index].statusListing, "thirdPartyFlag", "1");
        if (bookIndex != "-1") {
            GBL_BOOKED_STAT = roleAuth.data.statusListing[index].statusListing[bookIndex].ID
        }

        //PENDING STATUS
        var pendingIndex = findIndexByKeyValue(roleAuth.data.statusListing[index].statusListing, "statusPriority", "1");
        if (pendingIndex != "-1") {
            GBL_PENDING_STAT = roleAuth.data.statusListing[index].statusListing[pendingIndex].ID
        }

        //PENDING STATUS
        var cancelIndex = findIndexByKeyValue(roleAuth.data.statusListing[index].statusListing, "remarkOnStatusFlag", "1");
        if (cancelIndex != "-1") {
            GBL_CANCEL_STAT = roleAuth.data.statusListing[index].statusListing[cancelIndex].ID
        }
    }
}

var Calendar = {
    createCalendar: function () {
        var date = new Date(),
                m = date.getMonth(),
                d = date.getDate(),
                y = date.getFullYear();
        var gbl_property_id = $('#propertyList').val();
        if (gbl_property_id == "-1" || gbl_property_id == "" || gbl_property_id == undefined) { // IF NOT SELECTED ANY PROPERTY, DO NOT RENDER CALENDAR
            $('#calendar').fullCalendar('destroy');
            return false;
        }

        // addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "getInquiryDataByProperty",
            userId: tmp_json.data[0].PK_USER_ID,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            orgId: checkAuth(20, 82),
            propertyId: gbl_property_id,
            startDate: startDate,
            endDate: endDate,
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month'
                        // right: 'month,agendaWeek,agendaDay'
            },
            firstDay: 0, // 1 for Monday, 2 for Tuesday
            //isRTL: Pleasure.settings.rtl,
            eventLimit: 5, // CHANGE MADE BY DEEPAK FOR LIMITION OF EVENT LISTING INTO EACH CAL DAY
            editable: false,
            droppable: false,
            lazyFetching: false,
            dayPopoverFormat: 'DD/MM/YYYY',
            weekMode: 'liquid',
            events: function (start, end, timezone, callback) {
                addRemoveLoader(1); //ADD LOADER
                $.ajax({
                    type: 'POST',
                    url: SEARCHURL,
                    dataType: 'json',
                    headers: {Authorization: 'Bearer ' + ((localStorage.POTGSignatureToken != "") ? localStorage.POTGSignatureToken : "")},
                    data: {postData: postData},
                    success: function (results) {
                        if (results.status == "Success") {
                            addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                            var events = [];
                            var todayDate = new Date();

                            GBL_BOOKING_DATA = results.data.bookingData;

                            results.data.bookingData.forEach(function (record, index) {
                                // var record = results.data[g];
                                var color = "";
                                var title = "";
                                var textColor = "";
                                if (record['status'] == GBL_BOOKED_STAT) { // BOOKED 
                                    title = "BOOKED";
                                    color = "#0077c0";
                                    textColor = "#ffffff";
                                } else if (record['status'] == GBL_PENDING_STAT) { // PENDING
                                    color = "orange";
                                    textColor = "#ffffff";
                                    title = "PENDING";
                                } else if (record['status'] == GBL_CANCEL_STAT) { // CANCEL
                                    color = "#dddddd";
                                    textColor = "#000";
                                    title = "CANCEL";
                                }
                                // else if(record['status'] == "14"){ // CONFIRM
                                //     color = "#f39c12";
                                //     textColor = "#ffffff";
                                //     title = "CONFIRM";
                                // } 

                                //console.log()
                                events.push({
                                    title: title,
                                    start: new Date(record.bookingStartDate + " " + '00:00'), // will be parsed
                                    end: new Date(record.bookingEndDate + " " + '23:00'), // will be parsed
                                    inqId: record.inqId,
                                    inqStatus: record.status,
                                    color: color,
                                    backColorCell: color,
                                });
                            });
                            callback(events);
                            // renderCalendarTable();
                            // getAllInqList();
                            // if( localStorage.indeCampusCalendarView == "List" ){
                            showList();
                            // }else{
                            //     showCalendar();
                            // }
                        } else {
                            addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                        }
                    },
                    error: function (jqXHR, errdata, errorThrown) {
                        toastr.error('No inquiry available');
                        addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER  
                    }
                });
            },
            eventClick: function (event, jsEvent, view) {
                // //set the values and open the modal 
                // console.log(event.start);
                // console.log(jsEvent);
                // console.log(view);
                getInquiryData(event.inqId);   //GET INQUIRY DATA
            },
            dayRender: function (date, element, view) {
                addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 

                //UPDATE POST DATA JSON

                var currDate = $('#calendar').fullCalendar('getDate').format();
                // console.log(date._d.format('yyyy-MM-dd'))
                var dayEnd = lastday(currDate.split('-')[0], (currDate.split('-')[1] - 1));
                postData.startDate = currDate.split('-')[0] + '-' + currDate.split('-')[1] + '-01';
                postData.endDate = currDate.split('-')[0] + '-' + currDate.split('-')[1] + '-' + dayEnd;

                //set the values and open the modal 
                setTimeout(function () {

                    renderAddBtn(date, element, view);
                    var calendarDate = new Date((date._d).format('yyyy-MM-dd'));
                    if (GBL_BOOKING_DATA != "" && GBL_BOOKING_DATA != "No record found") {

                        GBL_BOOKING_DATA.forEach(function (record, index) {

                            var eventStartDate = new Date(record.bookingStartDate);
                            var eventEndDate = new Date(record.bookingEndDate);

                            if (((+calendarDate >= +eventStartDate) && (+calendarDate <= +eventEndDate) && (record.status == GBL_BOOKED_STAT))) {
                                $(element[0]).css('background', 'red');
                                $(element[0]).html('');
                                // $(element[0]).addClass('currDate_'+date._d.format('yyyy-MM-dd'));
                            }
                        })
                    }
                }, 100);
            },
            dayClick: function (date, element, view) {
                console.log((date._d).format('yyyy-MM-dd'))
                //GET DATA USING A DATE 
            },
            eventRender: function (event, element, view) {
                // renderAddBtn();
                if (event.inqStatus == GBL_BOOKED_STAT) {
                    // $(element).css("background-color","red");
                    $(element).addClass('bookedDate');
                } else if (event.inqStatus == GBL_PENDING_STAT) {
                    // $(element).css("color","#000");
                } else if (event.inqStatus == GBL_CANCEL_STAT) {
                    $(element).css("color", "#000");
                }
                initNxtPrevBtn(event, element, view);   // NXT PREV BTN INITIALIZATION
                renderAddBtn(event, element, view);
            },
        });

    },
    init: function () {
        this.createCalendar();
        addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
    }
}

// OPEN CREATE INQ MODAL FROM CALENDAR
function openActModal(context) {

    if (checkAuth(20, 81, 1) == -1) {
        toastr.error(NOAUTH);
        return false;
    }

    var dataDate = new Date($(context).parent('.fc-day').attr('data-date')).format('yyyy-MM-dd');
    var currDate = new Date( ).format('yyyy-MM-dd');

    if (new Date(currDate) > new Date(dataDate)) {
        toastr.warning("Due Date should not be less than Current Date to create an event");
        return false;

    } else {

        $('.dueDatedp-dateTime').datepicker('remove');
        $('.dueDatedp-dateTime').datepicker({
            format: "dd-mm-yyyy",
            showMeridian: true,
            autoclose: true,
            startDate: new Date(dataDate),
            endDate: new Date(dataDate),
            autoclose: true,
                    minuteStep: 15,
        });

        setTimeout(function () {
            $('#txtActDueDate').val(mysqltoDesiredFormat(dataDate, 'dd-MM-yyyy'));
        }, 10);

        var date = dataDate;

        $(".default-EndDateTime-picker").datepicker('remove');
        $('.default-EndDateTime-picker').datepicker({
            format: "dd-mm-yyyy",
            showMeridian: true,
            autoclose: true,
            startDate: new Date(date),
            autoclose: true,
        });
        $('.default-EndDateTime-picker').prop("readonly", true);

        resetPopups();
        var propertyList = $('#propertyList').val();
        $('#propertyListVenue').val(propertyList).selectpicker('refresh');

        $('#timeZoneList').selectpicker();
        commonAutoCompleteForCustInqSearch('searchCustForInq', saveCustData);
        modalShow('addVenueInquiry');
        addFocusId('propertyListVenue');   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

// RENDER ADD INQ BUTTON IN CALENDAR
function renderAddBtn(event, element, view) {

    var addBtn = "<a href='javascript:void(0)' style='position: absolute;bottom: 0;width: 11.8%;background:#ddd;color:#000;' class='btn btn-sm creatActBtn' onclick='openActModal( this )'><i class='fa fa-plus'></i></a>";
    var addBtnWidgets = $('.fc-month-view .fc-day.fc-widget-content');

    for (var i = 0; i < addBtnWidgets.length; i++) {

        if ($(addBtnWidgets[i]).hasClass('fc-past')) {
            $(addBtnWidgets[i]).html('');
        } else {

            var calendarDate = new Date(addBtnWidgets[i].dataset.date);
            if (GBL_BOOKING_DATA != "" && GBL_BOOKING_DATA != "No record found") {

                GBL_BOOKING_DATA.forEach(function (record, index) {

                    var eventStartDate = new Date(record.bookingStartDate);
                    var eventEndDate = new Date(record.bookingEndDate);

                    if (((+calendarDate < +eventStartDate) || (+calendarDate > +eventEndDate)) || (record.status == GBL_PENDING_STAT || record.status == GBL_CANCEL_STAT)) {
                        if (!($(addBtnWidgets[i]).hasClass('bookedRow'))) {   //ALREADY BOOKED
                            $(addBtnWidgets[i]).html(addBtn);
                        }
                    } else {
                        $(addBtnWidgets[i]).html('');
                        $(addBtnWidgets[i]).addClass('bookedRow');
                    }
                })
            } else {
                $(addBtnWidgets[i]).html(addBtn);
            }
        }
    }

    $('.fc-other-month').html('');  //HIDE OTHER MONTH DATA
}

// INITIALIZE BUTTONS ON NEXT / PREV BUTTON OF CALENDAR
function initNxtPrevBtn(event) {

    $(".fc-next-button").on("click", function () {
        renderAddBtn(event);
    });

    $(".fc-prev-button").on("click", function () {
        renderAddBtn(event);
    });

    $(".fc-agendaWeek-button").on("click", function () {
        setTimeout(function () {
            $('.creatActBtn').hide();
        }, 200);
    });

    $(".fc-agendaDay-button").on("click", function () {
        setTimeout(function () {
            $('.creatActBtn').hide();
        }, 200);
    });
    $(".fc-month-button").on("click", function () {
        renderAddBtn(event);
    });

}

// PAGE LOAD EVENTS
function pageInitialEvents() {

    $('#propertyList').selectpicker();
    $('.dueDatedp').datepicker({
        format: 'dd-mm-yyyy',
        startDate: 'today',
        autoclose: true,
    });

    getPropertyList();
    initTelInput('custMobileNumber');   // INITIALIZE COUNTRY WISE MOBILE NUM
    $('#custMobileNumber').attr('maxlength', MOBILE_LENGTH);

    $('#searchCustForInq').on('keydown', function (e) {
        if (e.which == 8 || e.which == 46) {

            if ($('#searchCustForInq').val() == "" || $('#searchCustForInq').val().length <= 2) {

                $('#searchCustForInq').attr("custId", "-2");
                $('#searchCustForInq').attr("clientCustomerId", "");
                // $('#custMobileNumber').val('');
                setNumberInIntl('custMobileNumber', '');
                $('#custEmail').val('');
                $('#emailDiv').show(200);

            }

        }
    });

    $('#inqStatus').selectpicker('refresh');
    localStorage.removeItem('indeCampusPropCustId');
    localStorage.removeItem('indeCampusEditStudId');
    localStorage.indeCampusCalendarView == "List";

}

// RESET POPUP OF GENERATE INQ MODAL
function resetPopups() {

    $('#txtActDueDate').val('');
    $('#txtActNormEndDate').val('');
    $('#searchCustForInq').val('');
    $('#searchCustForInq').attr('custId', '-2')
    // $('#custMobileNumber').val('');
    setNumberInIntl('custMobileNumber', '');
    $('#custEmail').val('');
    $('#emailDiv').show();

}

// CREATE INQUIRY 
function createCalendarInq() {

    if (localStorage.indeCampusActivityAppUserId == "-1") {
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }

    var contactId = "";
    var propertyListVenue = $('#propertyListVenue').val();
    var propertyName = $('#propertyListVenue option:selected').text();
    var timeZoneList = $('#timeZoneList').val();
    var timeZoneName = $('#timeZoneList option:selected').text();
    var startDate = $('#txtActDueDate').val();
    var endDate = $('#txtActNormEndDate').val();
    var custId = $('#searchCustForInq').attr('custid');
    var clientCustomerId = $('#searchCustForInq').attr('clientCustomerId');
    var custName = $('#searchCustForInq').val();
    // var custMobileNumber = $('#custMobileNumber').val();
    var custMobileNumber = getIntlMobileNum("custMobileNumber");    // GET MOBILE NUM
    var custEmail = $('#custEmail').val();

    if (propertyListVenue == "-1") {
        toastr.warning("Please Select property venue");
        addFocusId('propertyListVenue');
        return false;

    } else if (timeZoneList == "-1") {
        toastr.warning("Please Select timezone");
        addFocusId('timeZoneList');
        return false;

    } else if (startDate == "") {
        toastr.warning("Please Select start date");
        $('#txtActDueDate').datepicker('show');
        return false;

    } else if (endDate == "") {
        toastr.warning("Please Select end date");
        $('#txtActNormEndDate').datepicker('show');
        return false;

    } else if (custName == "") {
        toastr.warning("Please Enter customer name");
        addFocusId('searchCustForInq');
        return false;

    } else if (!checkValidMobile("custMobileNumber")) {
        // }else if ( custMobileNumber == "" || custMobileNumber.length != MOBILE_LENGTH ){ 
        toastr.warning("Please Enter valid mobile number");
        addFocusId('custMobileNumber');
        return false;

    } else if (custEmail != "" && !validateEmail(custEmail)) {
        toastr.warning("Please Enter valid email");
        addFocusId('custEmail');
        return false;

    }

    var dateStartDate = new Date(ddmmyyToMysql(startDate));
    var dateEndDate = new Date(ddmmyyToMysql(endDate));

    //START END DATE VALIDATION
    if (+dateEndDate < +dateStartDate) { //dateActEndDate dateActStartDate
        toastr.warning("End Date cannot be less than the Start Date");
        $('#txtActDueDate').datepicker('show');
        return false;

    }

    //CUSTOMER JSON
    var custArrData = {
        custId: custId,
        firstName: custName,
        lastName: '',
        mobileNo: custMobileNumber,
        emailId: custEmail,
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "addInquiry",
        orgId: checkAuth(20, 81),
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        custId: custId,
        propertyId: propertyListVenue,
        propertyName: propertyName,
        timezoneId: timeZoneList,
        timezoneName: timeZoneName,
        startDate: startDate,
        endDate: endDate,
        status: GBL_PENDING_STAT, //PENDING STATUS   
        custData: custArrData,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, createInquiryCallback, "Please wait...Creating Inquiry.");

    function createInquiryCallback(flag, data) {
        if (data.status == "Success" && flag) {

            var myCalendar = $('#calendar');
            myCalendar.fullCalendar();

            if( data.data != "No record found" ){
                if (data.data.bookingData != "" && data.data.bookingData != "No record found") {
                    data.data.bookingData.forEach(function (record, index) {

                        var color = "";
                        var title = "";
                        var textColor = "";
                        if (record['status'] == GBL_BOOKED_STAT) { // BOOKED 
                            title = "BOOKED";
                            color = "#0077c0";
                            textColor = "#ffffff";
                        } else if (record['status'] == GBL_PENDING_STAT) { // PENDING
                            color = "orange";
                            textColor = "#ffffff";
                            title = "PENDING";
                        } else if (record['status'] == GBL_CANCEL_STAT) { // CANCEL
                            color = "#dddddd";
                            textColor = "#000";
                            title = "CANCEL";
                        }
                        // else if(record['status'] == "14"){ // CONFIRM
                        //     color = "#f39c12";
                        //     textColor = "#ffffff";
                        //     title = "CONFIRM";
                        // }  

                        var source = {
                            title: title,
                            start: new Date(record.bookingStartDate + " " + '00:00'), // will be parsed
                            end: new Date(record.bookingEndDate + " " + '23:00'), // will be parsed
                            inqId: record.inqId,
                            inqStatus: record.status,
                            color: color,
                            backColorCell: color,
                        };
                        myCalendar.fullCalendar('renderEvent', source);
                    });
                }
            }
            modalHide('addVenueInquiry');
            getAllInqList();

        } else {

            if (flag)
                displayAPIErrorMsg(data.status, GBL_ERR_ADD_INQ);
            else
                toastr.error(SERVERERROR);
        }
    }
}

// GET PROPERTY LISTING DATA
function getPropertyList() {

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getPropertyData",
        userId: tmp_json.data[0].PK_USER_ID,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(2, 6),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    commonAjax(FOLLOWUPURL, postData, getPropertyDataCallback, "Please wait...Adding your item.");

    function getPropertyDataCallback(flag, data) {
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_PROPERTY_DATA = data.data;
            renderPropertyList();

        } else {

            GBL_PROPERTY_DATA = [];
            renderPropertyList();

            if (flag)
                displayAPIErrorMsg(data.status, GBL_ERR_NO_PROP);
            else
                toastr.error(SERVERERROR);
        }
        getTimezoneDataList('propertyList');
        $('#calendar').fullCalendar('destroy');
        // Calendar.init();
        showList();
        // getAllInqList();
    }

}

// RENDER PROPERTY LISTING DATA 
function renderPropertyList() {

    var innerHtml = '';
    if (GBL_PROPERTY_DATA != "No record found" && GBL_PROPERTY_DATA != "") {
        GBL_PROPERTY_DATA.forEach(function (record, index) {

            innerHtml += '<option value="' + record.propertyId + '">' + record.propertyName + '</option>';

        });
    }
    $('#propertyList').html(innerHtml); // SET LIST IN SELECTPICKER
    $('#propertyList').selectpicker('refresh');
    $('#propertyListVenue').html(innerHtml).selectpicker('refresh');

}

// ON CHANGE OF PROPERTY LIST, GET CALENDAR DATA AND RENDER IT
function changeCalData(context) {

    var propertyId = $(context).val();

    // gbl_property_id = propertyId;
    $('#calendar').fullCalendar('destroy');
    Calendar.init();
    // getAllInqList();
    getTimezoneDataList('propertyList');
}

// GET TIMEZONE OF SELECTED PROPERTY
function getTimezoneDataList(eleId) {

    var propertyList = $('#'+eleId).val();

    if( propertyList != "" && propertyList != "-1" && propertyList != null ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "getPropertyTimeZone",
            userId: tmp_json.data[0].PK_USER_ID,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            orgId: checkAuth(18, 74),
            itemId: propertyList,
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }

        commonAjax(FOLLOWUPURL, postData, getPropertyDataCallback, "Please wait...Adding your item.");

        function getPropertyDataCallback(flag, data) {
            if (data.status == "Success" && flag) {

                console.log(data.data);
                GBL_TIMEZONE_DATA = data.data;
                renderTimezoneData();
                if( eleId == "propertyList" ){
                    $('#propertyListVenue').val(propertyList).selectpicker('refresh');
                }else{
                    $('#propertyList').val(propertyList).selectpicker('refresh');
                    $('#propertyList').trigger('change');
                }
                getAllInqList()

            } else {

                GBL_TIMEZONE_DATA = [];
                renderTimezoneData();

                if (flag)
                    displayAPIErrorMsg(data.status, "No property timezone available");
                else
                    toastr.error(SERVERERROR);
            }
        }
    }else{
        GBL_TIMEZONE_DATA = [];
        renderTimezoneData();
    }
}

// RENDER TIMEZONE IN DROPDOWN
function renderTimezoneData(){

    var options = '';
    if( GBL_TIMEZONE_DATA != "" && GBL_TIMEZONE_DATA != "No record found" ){
        GBL_TIMEZONE_DATA.forEach(function( record , index ) {
            options += '<option value="'+ record.timezoneId +'">'+ record.propTimezoneName +'</option>';
        });
    }
    $('#timeZoneList').html( options );
    $('#timeZoneList').selectpicker('refresh');
}

//GET DATA OF INQUIRY USING INQUIRY ID
function getInquiryData(inqId) {

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getInquiryData",
        userId: tmp_json.data[0].PK_USER_ID,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(20, 82),
        inqId: inqId,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(FOLLOWUPURL, postData, getInquiryDataCallback, "Please wait...getting your inquiry data");

    //GET DATA OF INQUIRY USING INQUIRY CALLBACK FUNCTION
    function getInquiryDataCallback(flag, data) {
        if (data.status == "Success" && flag) {

            console.log(data.data[0]);

            // console.log(event);  
            // if( data.data[0].customerData != "" && data.data[0].customerData != "No record found" ){
            //     $("#customerName").html(data.data[0].customerData[0].firstName+' '+data.data[0].customerData[0].lastName);
            //     $("#customerNo").html(data.data[0].customerData[0].mobileNo);
            //     $("#customerAddress").html(data.data[0].customerData[0].address);
            // }
            // $("#startDateTxt").html( mysqltoDesiredFormat( data.data[0].bookingStartDate , 'dd-MM-yyyy' ) );
            // $("#endDateTxt").html( mysqltoDesiredFormat( data.data[0].bookingEndDate , 'dd-MM-yyyy' ) );
            // $("#propertyNameTxt").html(data.data[0].propertyName); 

            // $('#statusIcon').removeClass('fa-check fa-hourglass-1'); 

            // $('#activityDialog').removeClass('pendingActClass completedActClass');
            // if( data.data[0].status == GBL_PENDING_STAT ){
            //     $('#activityDialog').addClass('pendingActClass');
            //     $('#actTitle').html('INQUIRY - '+ data.data[0].inqId);
            //     $('#statusIcon').addClass('fa-hourglass-1');
            // }else if( data.data[0].status == GBL_BOOKED_STAT ){
            //     $('#activityDialog').addClass('completedActClass');
            //     $('#actTitle').html('BOOKING - '+ data.data[0].bookingNo);
            //     $('#statusIcon').addClass('fa-check');
            // }else if( data.data[0].status == GBL_CANCEL_STAT ){
            //     $('#activityDialog').addClass('cancelActClass');
            //     $('#actTitle').html('CANCEL - '+ data.data[0].bookingNo);
            //     $('#statusIcon').addClass('fa-times');
            // }
            // // else if( data.data[0].status == "14" ){
            // //     $('#activityDialog').addClass('confirmActClass');
            // //     $('#actTitle').html('CONFIRM - '+ data.data[0].bookingNo);
            // //     $('#statusIcon').addClass('fa-check');
            // // }
            // $('#inqStatus').attr('inqId',data.data[0].inqId);

            var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
            var bookingStatIndex = findIndexByKeyValue(roleAuth.data.statusListing, "statusCatName", "BOOKING");
            var statusListing = "";

            if (bookingStatIndex != "-1") {
                statusListing = roleAuth.data.statusListing[bookingStatIndex].statusListing;
            }


            var curStatus = data.data[0].status;
            var curStatusIndex = findIndexByKeyValue(statusListing, 'ID', curStatus);
            var optList = '';
            if (curStatusIndex != -1) {

                var currStatPriority = statusListing[curStatusIndex].statusPriority;
                statusListing.forEach(function (statRecord, statIndex) {

                    if (parseInt(statRecord.statusPriority) >= currStatPriority) {
                        optList += '<option value="' + statRecord.ID + '">' + statRecord.VALUE + '</option>';
                    }

                });
            }
            var newRow = '';
            var actTitle = '';
            data.data.forEach(function (record, index) {
                if (record.status == GBL_PENDING_STAT) {
                    actTitle = 'PENDING - ' + record.inqId;
                } else if (record.status == GBL_BOOKED_STAT) {
                    actTitle = 'BOOKING - ' + record.inqId;
                } else if (record.status == GBL_CANCEL_STAT) {
                    actTitle = 'CANCEL - ' + record.inqId;
                }

                newRow += '<tr>' +
                        '<td>' + (index + 1) + '</td>' +
                        '<td>' + record.customerData[0].firstName + ' ' + record.customerData[0].lastName + '</td>' +
                        '<td>' + record.customerData[0].mobileNo + '</td>' +
                        '<td>' + record.customerData[0].address + '</td>' +
                        '<td>' + mysqltoDesiredFormat(record.bookingStartDate, 'dd-MM-yyyy') + '</td>' +
                        '<td>' + mysqltoDesiredFormat(record.bookingEndDate, 'dd-MM-yyyy') + '</td>' +
                        '<td>' + record.propertyName + '</td>' +
                        '<td><select class="selectpicker" data-width="100%" id="inqStatus" onchange="changeInqStatusTable(this);" inqId="' + record.inqId + '">' + optList + '</select></td>' +
                        '</tr>';
            });

            // setOption('0','inqStatus',statusListing,'');
            // $('#inqStatus').html( optList );
            $('#inqTableBody').html(newRow);
            $('#inqDetailTable').dataTable();
            // $('#actTitle').html( actTitle );
            modalShow('calendarDetail');
            $('.selectpicker').selectpicker('refresh');

        } else {
            if (flag)
                displayAPIErrorMsg(data.status, 'No Inquiry data available');
            else
                toastr.error(SERVERERROR);
        }
    }
}

var searchedCustId = "";
function saveCustData(item) {

    var index = findIndexByKeyValue(ITEMS, "id", item.value);
    searchedCustId = item.value;
    setTimeout(function () {
        if (index == -1) {
            $('#searchCustForInq').attr("custId", "-2");
            $('#searchCustForInq').attr("clientCustomerId", "");
            // $('#custMobileNumber').val('');

            setNumberInIntl('custMobileNumber', '');
            $('#custEmail').val('');
            $('#emailDiv').show(200);

        } else {
            $('#searchCustForInq').val(ITEMS[index].name);
            $('#searchCustForInq').attr("custId", ITEMS[index].custId);
            $('#searchCustForInq').attr("clientCustomerId", ITEMS[index].clientCustomerId);
            // $('#custMobileNumber').val( ITEMS[index].mobile );
            setNumberInIntl('custMobileNumber', ITEMS[index].mobile);
            $('#custEmail').val(ITEMS[index].email);
            $('#emailDiv').hide(200);
        }
    }, 1);
}

// CHANGE INQUIRY STATUS
function changeInqStatus() {

    var inqStatusVal = $('#inqStatus').val();
    var inqId = $('#inqStatus').attr('inqId');
    var propertyId = $('#propertyList').val();

    if (inqStatusVal == "" || inqStatusVal == null) {
        toastr.warning("Please select proper inquiry status");
        addFocusId('inqStatusVal');
        return false;
    }

    if (inqId != "" && inqId != undefined) {

        var inqIndex = findIndexByKeyValue(GBL_BOOKING_DATA, 'inqId', inqId);

        if (inqIndex == -1) {
            return false;
        } else {
            if (inqStatusVal == GBL_BOOKED_STAT) {
                var oldStatus = GBL_BOOKING_DATA[inqIndex].status;
                var selectedStartDate = new Date(GBL_BOOKING_DATA[inqIndex].bookingStartDate);
                var selectedEndDate = new Date(GBL_BOOKING_DATA[inqIndex].bookingEndDate);
                var changeFlag = true;
                GBL_BOOKING_DATA.forEach(function (inqRecord, inqIdx) {

                    var oldInqStartDate = new Date(inqRecord.bookingStartDate);
                    var oldInqEndDate = new Date(inqRecord.bookingEndDate);
                    if (((+selectedStartDate <= +oldInqStartDate) && (inqRecord.status == GBL_BOOKED_STAT) && (inqRecord.inqId != inqId)) || ((+selectedStartDate <= +oldInqEndDate) && (inqRecord.status == GBL_BOOKED_STAT) && (inqRecord.inqId != inqId))) {
                        changeFlag = false;
                    }

                });
            }
            if (changeFlag == false) {
                toastr.warning("You cannot book this event");
                $('#inqStatus').val(oldStatus).selectpicker('refresh');
                return false;
            } else {
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

                // Followup Listing Todays, Delayed, Upcomming
                var postData = {
                    requestCase: "updateInqStatus",
                    userId: tmp_json.data[0].PK_USER_ID,
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    orgId: checkAuth(20, 83),
                    inqId: inqId,
                    status: inqStatusVal,
                    propertyId: propertyId,
                    customerId: GBL_BOOKING_DATA[inqIndex].custId,
                    customerName: GBL_BOOKING_DATA[inqIndex].custName,
                    propertyName: GBL_BOOKING_DATA[inqIndex].propertyName,
                    // propertyId: GBL_BOOKING_DATA[inqIndex].propertyId,
                    bookingStartDate: GBL_BOOKING_DATA[inqIndex].bookingStartDate,
                    bookingEndDate: GBL_BOOKING_DATA[inqIndex].bookingEndDate,
                    dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                }
                // console.log(postData); return false;
                commonAjax(FOLLOWUPURL, postData, changeInqStatusCallback, "Please wait...Adding your item.");

                function changeInqStatusCallback(flag, data) {
                    if (data.status == "Success" && flag) {

                        console.log(data);

                        addRemoveLoader(1);
                        if (inqStatusVal == GBL_BOOKED_STAT) {
                            if (data.inseterdInvoiceId != undefined && data.inseterdInvoiceId != "" && data.inseterdInvoiceId != "No record found") {
                                var temp = {
                                    inqId: inqId,
                                    custId: GBL_BOOKING_DATA[inqIndex].custId,
                                    clientInvoiceId: data.inseterdInvoiceId
                                }
                                localStorage.indeCampusBookPropPayment.push(JSON.stringify(temp));
                                navigateToCollectPayment(GBL_BOOKING_DATA[inqIndex].custId);
                            } else {
                                modalHide('calendarDetail');
                                getAllInqList();
                            }
                        } else {
                            modalHide('calendarDetail');
                            getAllInqList();
                        }
                        // modalHide('calendarDetail');
                        addRemoveLoader(0);
                        $('#calendar').fullCalendar('destroy');
                        setTimeout(function () {
                            Calendar.init();
                        }, 10);

                        // var myCalendar = $('#calendar'); 
                        // myCalendar.fullCalendar();

                        // var color = "";
                        // var title = "";
                        // var textColor = "";
                        // if( inqStatusVal == GBL_BOOKED_STAT ){ // BOOKED 
                        //     title = "BOOKED";
                        // }else if(inqStatusVal == GBL_PENDING_STAT){ // PENDING
                        //     color = "#e6b4b4";
                        //     textColor = "#ffffff";
                        //     title = "PENDING INQUIRY";
                        // }else if(inqStatusVal == GBL_CANCEL_STAT){ // CANCEL
                        //     color = "#785447";
                        //     textColor = "#ffffff";
                        //     title = "CANCEL";
                        // }else if(inqStatusVal == "14"){ // CONFIRM
                        //     color = "#f39c12";
                        //     textColor = "#ffffff";
                        //     title = "CONFIRM";
                        // }  

                        // var source = {
                        //     title: title,
                        //     start: new Date( GBL_BOOKING_DATA[inqIndex].bookingStartDate+" "+'00:00'), // will be parsed
                        //     end: new Date(GBL_BOOKING_DATA[inqIndex].bookingStartDate+" "+'23:00'), // will be parsed
                        //     inqId : inqId,
                        //     inqStatus : inqStatusVal,
                        //     color  : color, 
                        //     backColorCell  : color,
                        // };
                        // myCalendar.fullCalendar( 'renderEvent', source );
                        // myCalendar.fullCalendar( 'dayRender', source );



                    } else {

                        if (flag)
                            displayAPIErrorMsg(data.status, GBL_ERR_NO_PROP);
                        else
                            toastr.error(SERVERERROR);
                    }
                }
            }


        }
    }

}


// CHANGE INQUIRY STATUS FROM DATATABLE
function changeInqStatusTable(context) {

    var inqStatusVal = $(context).val();
    var inqId = $(context).attr('inqId');
    var propertyId = $('#propertyList').val();

    if (inqStatusVal == "" || inqStatusVal == null) {
        toastr.warning("Please select proper inquiry status");
        addFocusId('inqStatusVal');
        return false;
    }

    if (inqId != "" && inqId != undefined) {

        var inqIndex = findIndexByKeyValue(GBL_BOOKING_DATA, 'inqId', inqId);

        if (inqIndex == -1) {
            return false;
        } else {
            if (inqStatusVal == GBL_BOOKED_STAT) {
                var oldStatus = GBL_BOOKING_DATA[inqIndex].status;
                var selectedStartDate = new Date(GBL_BOOKING_DATA[inqIndex].bookingStartDate);
                var selectedEndDate = new Date(GBL_BOOKING_DATA[inqIndex].bookingEndDate);
                var changeFlag = true;
                GBL_BOOKING_DATA.forEach(function (inqRecord, inqIdx) {

                    var oldInqStartDate = new Date(inqRecord.bookingStartDate);
                    var oldInqEndDate = new Date(inqRecord.bookingEndDate);
                    if (((+selectedStartDate <= +oldInqStartDate) && (inqRecord.status == GBL_BOOKED_STAT) && (inqRecord.inqId != inqId)) || ((+selectedStartDate <= +oldInqEndDate) && (inqRecord.status == GBL_BOOKED_STAT) && (inqRecord.inqId != inqId))) {
                        changeFlag = false;
                    }

                });
            }
            if (changeFlag == false) {
                toastr.warning("You cannot book this property");
                $(context).val(oldStatus).selectpicker('refresh');
                return false;
            } else {
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

                // Followup Listing Todays, Delayed, Upcomming
                var postData = {
                    requestCase: "updateInqStatus",
                    userId: tmp_json.data[0].PK_USER_ID,
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    orgId: checkAuth(20, 83),
                    inqId: inqId,
                    status: inqStatusVal,
                    propertyId: propertyId,
                    customerId: GBL_BOOKING_DATA[inqIndex].custId,
                    customerName: GBL_BOOKING_DATA[inqIndex].custName,
                    propertyName: GBL_BOOKING_DATA[inqIndex].propertyName,
                    // propertyId: GBL_BOOKING_DATA[inqIndex].propertyId,
                    bookingStartDate: GBL_BOOKING_DATA[inqIndex].bookingStartDate,
                    bookingEndDate: GBL_BOOKING_DATA[inqIndex].bookingEndDate,
                    dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                }
                // console.log(postData); return false;
                commonAjax(FOLLOWUPURL, postData, changeInqStatusCallback, "Please wait...Adding your item.");

                function changeInqStatusCallback(flag, data) {
                    if (data.status == "Success" && flag) {

                        console.log(data);
                        modalHide('calendarDetail');
                        if (inqStatusVal == GBL_BOOKED_STAT) {
                            if (data.inseterdInvoiceId != undefined && data.inseterdInvoiceId != "" && data.inseterdInvoiceId != "No record found") {
                                var temp = {
                                    inqId: inqId,
                                    custId: GBL_BOOKING_DATA[inqIndex].custId,
                                    clientInvoiceId: data.inseterdInvoiceId
                                }
                                localStorage.indeCampusBookPropPayment = JSON.stringify(temp);
                                navigateToCollectPayment(GBL_BOOKING_DATA[inqIndex].custId);
                            }
                        } else {
                            getAllInqList();
                        }
                        $('#calendar').fullCalendar('destroy');
                        setTimeout(function () {
                            Calendar.init();
                        }, 10);

                        // var myCalendar = $('#calendar'); 
                        // myCalendar.fullCalendar();

                        // var color = "";
                        // var title = "";
                        // var textColor = "";
                        // if( inqStatusVal == GBL_BOOKED_STAT ){ // BOOKED 
                        //     title = "BOOKED";
                        // }else if(inqStatusVal == GBL_PENDING_STAT){ // PENDING
                        //     color = "#e6b4b4";
                        //     textColor = "#ffffff";
                        //     title = "PENDING INQUIRY";
                        // }else if(inqStatusVal == GBL_CANCEL_STAT){ // CANCEL
                        //     color = "#785447";
                        //     textColor = "#ffffff";
                        //     title = "CANCEL";
                        // }else if(inqStatusVal == "14"){ // CONFIRM
                        //     color = "#f39c12";
                        //     textColor = "#ffffff";
                        //     title = "CONFIRM";
                        // }  

                        // var source = {
                        //     title: title,
                        //     start: new Date( GBL_BOOKING_DATA[inqIndex].bookingStartDate+" "+'00:00'), // will be parsed
                        //     end: new Date(GBL_BOOKING_DATA[inqIndex].bookingStartDate+" "+'23:00'), // will be parsed
                        //     inqId : inqId,
                        //     inqStatus : inqStatusVal,
                        //     color  : color, 
                        //     backColorCell  : color,
                        // };
                        // myCalendar.fullCalendar( 'renderEvent', source );
                        // myCalendar.fullCalendar( 'dayRender', source );



                    } else {

                        if (flag)
                            displayAPIErrorMsg(data.status, GBL_ERR_NO_PROP);
                        else
                            toastr.error(SERVERERROR);
                    }
                }
            }


        }
    }

}

// SHOW CALENDAR
function showCalendar() {

    $('#CalendarDisplay').show();
    $('#List').hide();
    $('#calendarViewDiv').show();
    $('#listViewDiv').hide();
    if ($('.fc-view.fc-month-view').html() == "") {
        $('#calendar').fullCalendar('destroy');
        Calendar.init();
    }

    localStorage.indeCampusCalendarView = "Calendar";
}

// DISPLAY STUDENTS IN LIST VIEW
function showList() {

    $('#List').show();
    $('#CalendarDisplay').hide();
    $('#listViewDiv').show();
    $('#calendarViewDiv').hide();
    renderCalendarTable();
    localStorage.indeCampusCalendarView = "List";
}

// RENDER CALENDAR BOOKING DATA IN DATATABLE
function renderCalendarTable() {

    var tHead = '<table class="overFlowTbl display datatables-basic" id="calendarTable">' +
            '<thead>' +
            '<tr>' +
            '<th>#</th>' +
            '<th>Inquiry Id</th>' +
            '<th>Property</th>' +
            '<th>Customer Name</th>' +
            '<th>Booking Start Date</th>' +
            '<th>Booking End Date</th>' +
            '<th>Inquiry Status</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="calendarTableBody">';

    var tEnd = '</tbody></table>';

    var newRow = '';
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var statIdex = findIndexByKeyValue(roleAuth.data.statusListing, "statusCatName", "BOOKING");
    var statusListing = "";
    if (statIdex != -1) {
        statusListing = roleAuth.data.statusListing[statIdex].statusListing;
    }
    var propertyList = $('#propertyList option:selected').text();

    if (GBL_BOOKING_DATA != "" && GBL_BOOKING_DATA.length > 0) {
        GBL_BOOKING_DATA.forEach(function (record, index) {

            var curStatus = record.status;
            var curStatusIndex = findIndexByKeyValue(statusListing, 'ID', curStatus);
            var optList = '';
            if (curStatusIndex != -1) {

                var currStatPriority = statusListing[curStatusIndex].statusPriority;
                statusListing.forEach(function (statRecord, statIndex) {

                    if (parseInt(statRecord.statusPriority) >= currStatPriority) {
                        optList += '<option value="' + statRecord.ID + '">' + statRecord.VALUE + '</option>';
                    }

                });
            }
            var inqColor = '';
            if (record.status == GBL_BOOKED_STAT) {
                inqColor = 'color:red';
            }

            newRow += '<tr>' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td><a href="javascript:void(0);" inqId="' + record.inqId + '" onclick="getInquiryData( ' + record.inqId + ' )" style="' + inqColor + '">' + record.inqId + '</a></td>' +
                    '<td>' + record.propertyName + '</td>' +
                    '<td>' + record.custName + '</td>' +
                    '<td>' + mysqltoDesiredFormat(record.bookingStartDate, 'dd-MM-yyyy') + '</td>' +
                    '<td>' + mysqltoDesiredFormat(record.bookingEndDate, 'dd-MM-yyyy') + '</td>' +
                    '<td><select class="selectpicker" data-width="70%" id="inqStatus" onchange="changeInqStatusTable(this);" inqId="' + record.inqId + '">' + optList + '</select></td>' +
                    '</tr>';
        });
    }
    $('#tableDiv').html(tHead + newRow + tEnd);
    $('.selectpicker').selectpicker('refresh');
    $('#calendarTable').dataTable();
}

// GET ALL INQUIRY LISTING FOR DISPLAY IN DATATABLE
function getAllInqList() {

    var gbl_property_id = $('#propertyList').val();
    if (gbl_property_id == "-1" || gbl_property_id == "" || gbl_property_id == undefined) { // IF NOT SELECTED ANY PROPERTY, DO NOT RENDER CALENDAR
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getInquiryDataByProperty",
        userId: tmp_json.data[0].PK_USER_ID,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(20, 82),
        propertyId: gbl_property_id,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    commonAjax(FOLLOWUPURL, postData, getAllInqListCallback, "Please wait...Creating Inquiry.");

    function getAllInqListCallback(flag, data) {
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_BOOKING_DATA = data.data.bookingData;
            renderCalendarTable();

        } else {


            GBL_BOOKING_DATA = [];
            renderCalendarTable();
            if (flag)
                displayAPIErrorMsg(data.status, "No inquiries available");
            else
                toastr.error(SERVERERROR);
        }
    }

}

// OPEN CREATE INQ MODAL 
function openAddInq() {

    if (checkAuth(20, 81, 1) == -1) {
        toastr.error(NOAUTH);
        return false;
    }

    $('.dueDatedp-dateTime').datepicker('remove');
    $('.dueDatedp-dateTime').datepicker({
        format: "dd-mm-yyyy",
        showMeridian: true,
        autoclose: true,
        startDate: new Date( ),
        // endDate: new Date( ),
        autoclose: true,
                minuteStep: 15,
    });

    setTimeout(function () {
        $('#txtActDueDate').val(mysqltoDesiredFormat(new Date(), 'dd-MM-yyyy'));
        $('#txtActDueDate').datepicker('update',mysqltoDesiredFormat(new Date(), 'dd-MM-yyyy'));
    }, 10);

    var date = new Date();

    $(".default-EndDateTime-picker").datepicker('remove');
    $('.default-EndDateTime-picker').datepicker({
        format: "dd-mm-yyyy",
        showMeridian: true,
        autoclose: true,
        startDate: new Date( ),
    });
    $('.default-EndDateTime-picker').prop("readonly", true);

    resetPopups();
    var propertyList = $('#propertyList').val();
    $('#propertyListVenue').val(propertyList).selectpicker('refresh');

    $('#timeZoneList').selectpicker();
    commonAutoCompleteForCustInqSearch('searchCustForInq', saveCustData);
    modalShow('addVenueInquiry');
    addFocusId('propertyListVenue');   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

}

function setEndDate(context) {

    var startDate = $(context).val();

    $("#txtActNormEndDate").datepicker('remove');
    $("#txtActNormEndDate").datepicker({
        format: "dd-mm-yyyy",
        showMeridian: true,
        autoclose: true,
        startDate: startDate,
    });
    $('#txtActNormEndDate').prop("readonly", true);
}


function navigateToCollectPayment(custId) {

    localStorage.indeCampusPropCustId = custId;
    window.location.href = "collectpayment.html";

}