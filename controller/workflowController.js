/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : workflowController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var statusData = [];


function getWorkflowListingData(){

    $('#status').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getWorkFlowListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(27,113),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getWorkflowListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getWorkflowListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        console.log(data);
        
        statusData = data.data;
        renderWorkflowData();

    } else {
        statusData = "No record found";
        renderWorkflowData();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_WORKFLOW );
        else
            toastr.error(SERVERERROR);
    }
}

function renderWorkflowData(){

    var newRow = '';
    var count = 1 ;

    addFocusId( 'status' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>WorkFlow Name</th>'+
                        '<th>Priority</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    if(statusData != 'No record found'){

        for( var i=0; i<statusData.length; i++ ){

            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ statusData[i].workFlowName +'</td>'+
                            '<td>'+ statusData[i].workFlowPriorityLevel +'</td>'+
                            '<td><a href="javascript:void(0)" statusId="'+ statusData[i].workFlowId +'" statusName="'+ statusData[i].workFlowName +'" priorityLevel="'+ statusData[i].workFlowPriorityLevel +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" onclick="updatewrkflow(this)"><i class="fa fa-pencil"></i></a>'+
                            '<a href="javascript:void(0)" statusId="'+ statusData[i].workFlowId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="deleteWorkflow(this)"><i class="fa fa-trash-o"></i></a></td>'+
                        '</tr>';
        } 
    }
    $('#dtTable').html(thead + newRow + tfoot);

    TablesDataTables.init();
    TablesDataTablesEditor.init();

}

var gblContext = '';
function createWorkflow(){

    if( checkAuth(27, 112, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var status = $('#status').val();
    var priority = $('#priority').val();

    if( status == ""){
        toastr.warning('Please Enter Status');
        $('#status').focus();
        return false;

    }else if( priority == ""){
        toastr.warning('Please Enter Priority');
        $('#priority').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'createWorkFlow',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(27,112),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            workFlowPrioriy : priority,
            workFlowName : status
        }

        commonAjax(FOLLOWUPURL, postData, createWorkflowCallback,"Please Wait... Getting Module Detail");
    }
}


function createWorkflowCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('New Status has been Created');
        console.log(data); 
        // location.reload(); 
        
        var status = $('#status').val();
        var priority = $('#priority').val();

        var tempData = {
            workFlowId: data.data.workFlowId,
            workFlowName: status,
            workFlowPriorityLevel : priority
        }

        statusData.push( tempData ); 

        renderWorkflowData(); 

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_WORKFLOW );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteWorkflow(context){
    
    if( checkAuth(27, 115, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        var statusId = $(context).attr('statusId');
        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteWorkFlow',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(27,115),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID), 
            workFlowId : statusId
        }

        commonAjax(FOLLOWUPURL, postData, deleteworkflowCallback,"Please Wait... Getting Module Detail");
    }
}


function deleteworkflowCallback(flag,data){

    if (data.status == "Success" && flag) {

        $(gblContext).parents('td').parents('tr').remove();
        toastr.success('WorkFlow has been Deleted');
        console.log(data); 

        var statusId = $(gblContext).attr('statusId');

        var index = findIndexByKeyValue( statusData,'statusId',statusId );

        statusData.splice(index,1);

        renderWorkflowData();

        resetData();
        // location.reload();  

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_WORKFLOW );
        else
            toastr.error(SERVERERROR);
    }
}


function updatewrkflow(context){

    if( checkAuth(27, 114, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    localStorage.POTGstatusIdForUpdate = "";

    var statusid = $(context).attr('statusid');
    var prioritylevel = $(context).attr('prioritylevel');
    var statusname = $(context).attr('statusname');

    localStorage.POTGstatusIdForUpdate = statusid;

    $('#createStatusBtn').attr('onclick','updateworkflow()');

    $('#priority').val(prioritylevel);
    $('#status').val(statusname);
    
    addFocusId( 'status' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function updateworkflow(context){

    var status = $('#status').val();
    var priority = $('#priority').val();
    var statusId = localStorage.POTGstatusIdForUpdate;

    if( status == ""){
        toastr.warning('Please Enter WorkFlow Name');
        $('#status').focus();
        return false;

    }else if( priority == ""){
        toastr.warning('Please Enter Priority');
        $('#priority').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateWorkFlow',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(27,114),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            workFlowPrioriy : priority,
            workFlowName : status,
            workFlowId : statusId
        }
        // console.log(postData); return false;
        commonAjax(FOLLOWUPURL, postData, updateworkflowCallback,"Please Wait... Getting Module Detail");
    }
}


function updateworkflowCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('WorkFlow has been Updated');
        console.log(data); 
        // location.reload();  
        var status = $('#status').val();
        var priority = $('#priority').val();
        var statusId = localStorage.POTGstatusIdForUpdate;

        var index = findIndexByKeyValue( statusData,'workFlowId',statusId );

        var tempArray = {
            workFlowId: statusData[index].workFlowId,
            workFlowName: status,
            workFlowPriorityLevel : priority
        }

        statusData[index] = tempArray;

        renderWorkflowData();

        $('#createStatusBtn').attr('onclick','createWorkflow()');

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_WORKFLOW );
        else
            toastr.error(SERVERERROR);
    }
}


function resetData(){

    $('#status').val('');
    $('#priority').val('');
    $('#createStatusBtn').attr('onclick','createWorkflow()');
}