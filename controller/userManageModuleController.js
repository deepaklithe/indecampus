/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : userManageModuleController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var moduleData = [];
function getModuleData(){

    $('#moduleName').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getModuleListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(21,82),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getModuleDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getModuleDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        moduleData = data.data;

        renderModuleData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_MODULE );
        else
            toastr.error(SERVERERROR);
    }
}

function renderModuleData(){

    
    addFocusId( 'moduleName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Name</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    if(moduleData != 'No record found'){
        for( var i=0; i<moduleData.length; i++ ){

            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ moduleData[i].moduleName +'</td>'+
                            '<td><a href="javascript:void(0)" module-id="'+ moduleData[i].moduleId +'" class="btn btn-xs btn-danger btn-ripple" onclick="deleteModule(this)"><i class="fa fa-trash-o"></i></a></td>'+
                        '</tr>';
        }

        $('#dtTable').html(thead + newRow + tfoot);
    
    }

    TablesDataTables.init();
    TablesDataTablesEditor.init();
}
      
function createModule(){
    
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(21,81,1) == -1){
            toastr.error( NOAUTH );
            return false;
        }
    }
    
    var levelName = $('#moduleName').val().trim();

    if( levelName.length == 0 ){
        toastr.warning('Please Enter Module Name');
        $('#moduleName').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'createModule',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(2,5),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
           moduleName : $('#moduleName').val()
        }

        commonAjax(FOLLOWUPURL, postData, createModuleCallback,"Please Wait... Getting Module Detail");
    }
}


function createModuleCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('New Module has been Created');
        console.log(data); 
        // location.reload(); 

        var levelName = $('#moduleName').val().trim();

        var tempData = {
            moduleId: data.data.moduleId,
            moduleName : levelName
        }

        moduleData.push( tempData ); 

        renderModuleData();  
        reset(); 

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_MODULE );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteModule(context){
    
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(21,84,1) == -1){
            toastr.error( NOAUTH );
            return false;
        }
    }
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        var moduleId = $(context).attr('module-id');
        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteModule',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(2,8),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            moduleId : moduleId
        }

        commonAjax(FOLLOWUPURL, postData, deleteModuleCallback,"Please Wait... Getting Module Detail");
    }
}


function deleteModuleCallback(flag,data){

    if (data.status == "Success" && flag) {

        $(gblContext).parents('td').parents('tr').remove(); 
        toastr.success('Module has been Deleted');
        console.log(data); 

        var moduleId = $(gblContext).attr('module-id');

        var index = findIndexByKeyValue( moduleData,'moduleId',moduleId );

        moduleData.splice(index,1);

        renderModuleData();
        // location.reload();  
        reset();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_MODULE );
        else    
            toastr.error(SERVERERROR);
    }
}

function reset(){
    $('#moduleName').val('');
}