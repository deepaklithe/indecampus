/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : clientConfigController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var clientData = [];

function getClientData(){

    $('#typeOfConfig').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "clientListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9,34),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getClientDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getClientDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var newRow = '';
        if(tmp_json.data[0].FK_CLIENT_ID == -1){
            for( var i=0; i<data.data.length; i++ ){
                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
            }
        }else{
            for( var i=0; i<data.data.length; i++ ){
                if(tmp_json.data[0].FK_CLIENT_ID ==data.data[i].clientId ){

                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
                }
            }
        }
        

        $('#clientList').html(newRow);
        //$('#clientList').val(tmp_json.data[0].FK_CLIENT_ID);
        $('#clientList').selectpicker();


        getClientConfigListing();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT );
        else
            toastr.error(SERVERERROR);
    }
}


function getClientConfigListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "listingClientConfiguration",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9,34),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getClientConfigListingDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getClientConfigListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        clientData = data.data;

        clientConfigDataRender();

    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT_CONFIG );
        else
            toastr.error(SERVERERROR);
    }
}

function clientConfigDataRender(){


    addFocusId( 'typeOfConfig' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Client Name</th>'+
                        '<th>Type of Configuration</th>'+
                        '<th>Display Name</th>'+
                        '<th>Status</th>'+
                        '<th>Max Value</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    if(clientData != 'No record found'){
        for( var i=0; i<clientData.length; i++ ){

            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ clientData[i].clientName +'</td>'+
                            '<td>'+ clientData[i].typeOfConfig +'</td>'+
                            '<td>'+ clientData[i].taxName +'</td>'+
                            '<td>'+ ((clientData[i].flag == 1) ? "On" : "Off") +'</td>'+
                            '<td>'+ ((clientData[i].tranType === "ADD" || clientData[i].tranType === "") ? "+" : "-") +" "+ clientData[i].maxValue +" "+ ((clientData[i].discType === "perc") ? "%" : "") +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" config-id="'+ clientData[i].id +'" onclick="updateClient(this)"><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" config-id="'+ clientData[i].id +'" onclick="deleteClient(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
        }

        $('#moduleList').html(newRow);
        
    }else{
        displayAPIErrorMsg( "" , GBL_ERR_NO_CLIENT_CONFIG );
    }

    $('#dtTable').html(thead + newRow + tfoot);

    TablesDataTables.init();
    TablesDataTablesEditor.init();

}

function createClient(){

    var requestClientId = $('#clientList').val();
    var requestClientName = $('#clientList option:selected').text();
    var typeOfConfig = $('#typeOfConfig').val();
    var maxValue = $('#maxVal').val().trim();
    var chkVal = $('#MF').is(':checked');
    var taxName = $("#taxName").val();
    var typeOfTrans = $("#typeOfTransId").val();
    var discTypeHeader = $("#discTypeHeader").val();
    
    var sendFlag = 0;
    if( chkVal == true ){
        sendFlag = 1;
    }
    var flag = 0;
    if(typeOfConfig == "HEADERTAX"){
        
        var index = findIndexByKeyValueAndCond( clientData , "typeOfConfig" , "taxName" , "HEADERTAX" , taxName );
        if(taxName.length == 0){
            toastr.warning("Please Enter Tax name.");
            $("#taxName").focus();
            return false;
            flag = 1;
        }else if(index != "-1"){
            toastr.warning("This Tax Name is already exist");
            $("#taxName").focus();
            return false;
            flag = 1;
        }else if(maxValue.length == 0){
            toastr.warning("Please Enter Max Value");
            $('#maxVal').focus();
            return false;
            flag = 1;
        }else if(maxValue > 100  && discTypeHeader == "perc"){
            toastr.warning("Max 100% Discount allowed.");
            $('#maxVal').focus();
            return false;
            flag = 1;
        }
        
        
    }else {
        if( maxValue.length == 0){
            toastr.warning("Please Enter Max Value");
            $('#maxVal').focus();
            return false;
            flag = 1;
        }
        
    }
    
    if(flag == 0){
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createClientConfiguration",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9,33),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            typeOfConfig : typeOfConfig,
            maxValue : maxValue,
            taxName : taxName,
            typeOfTrans : typeOfTrans,
            discTypeHeader : discTypeHeader,
            requestClientId : requestClientId,
            flag : sendFlag,
        };

        commonAjax(FOLLOWUPURL, postData, createClientCallback,"Please Wait... Getting Dashboard Detail");
        function createClientCallback(flag,data){
            if (data.status == "Success" && flag) {
                console.log(data);

                var tempArray = {
                    clientName: requestClientName,
                    flag: sendFlag,
                    id: data.data.configId,
                    maxValue: maxValue,
                    discType: discTypeHeader,
                    taxName: taxName,
                    tranType: typeOfTrans,
                    typeOfConfig: typeOfConfig
                }

                clientData.push(tempArray);

                clientConfigDataRender();

                resetData();

            } else {
                if (flag)
                    // toastr.error(data.status);
                    displayAPIErrorMsg( "" , GBL_ERR_CREATE_CLIENT_FAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

var gblContext = "";
var clientIdForDelete = '';
function deleteClient(context){

    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        clientIdForDelete = $(context).attr('config-id');
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        gblContext = context;

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "deleteClientConfiguration",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9,36),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            configId : clientIdForDelete
        };

        commonAjax(FOLLOWUPURL, postData, deleteClientCallback,"Please Wait... Getting Dashboard Detail");
    }
}

function deleteClientCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $(gblContext).parents('td').parents('tr').remove(); 
        // location.reload();

        var index = findIndexByKeyValue( clientData,'id',clientIdForDelete );

        clientData.splice(index,1);

        clientConfigDataRender();

        resetData();
        
    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_CLIENT_CONFIG );
        else
            toastr.error(SERVERERROR);
    }
}

var configId = '';
function updateClient(context){

    localStorage.POTGclientIdForUpdate = "";

    configId = $(context).attr('config-id');

    localStorage.POTGbranchIdForUpdate = configId;

    $('#createBranchBtn').attr('onclick','editclient()');

    var index = findIndexByKeyValue( clientData , 'id' , configId )
    if(clientData[index]['typeOfConfig'] == "HEADERTAX"){
        $("#taxName").val(clientData[index].taxName);
        $("#typeOfTransId").val(clientData[index].tranType);
        $("#discTypeHeader").val(clientData[index].discType);
        $("#typeOfConfig").val("HEADERTAX");
        $("#typeOfConfig").selectpicker("refresh");
        $("#discTypeHeader").selectpicker("refresh");
        $("#typeOfTransId").selectpicker("refresh");
        $("#divforDisplayVal").show();
        $("#typeOfTrans").show();
        $("#typeOfDisc").show();
        $("#typeOfDisc").show();
    }else{
        
        $("#typeOfConfig").val("INQGROUP");
        $("#typeOfConfig").selectpicker("refresh");
        $("#divforDisplayVal").hide();
        $("#typeOfTrans").hide();
        $("#typeOfDisc").hide();
        $("#discTypeHeader").hide();
    }
    $('#maxVal').val(clientData[index].maxValue);
    $('#typeOfConfig').val(clientData[index].typeOfConfig);
    if(clientData[index].flag == 1){
        $("#MF").prop("checked", true);
    }else{
        $("#MF").prop("checked", false);
    }
    // $('#clientList').val(clientData[index].clientName);
}

function editclient(){

    var requestClientId = $('#clientList').val();
    var requestClientName = $('#clientList option:selected').text();
    var typeOfConfig = $('#typeOfConfig').val();
    var maxValue = $('#maxVal').val().trim();
    var chkVal = $('#MF').is(':checked');
    var taxName = $("#taxName").val();
    var typeOfTrans = $("#typeOfTransId").val();
    var discTypeHeader = $("#discTypeHeader").val();
    
    var checkflag = 0;
    if( chkVal == true ){
        checkflag = 1;
    }
    
    var submitflag = 0;
    if(typeOfConfig == "HEADERTAX"){
        
        if(taxName.length == 0){
            toastr.warning("Please Enter Tax name.");
            $("#taxName").focus();
            return false;
            submitflag = 1;
        }else if(maxValue.length == 0){
            toastr.warning("Please Enter Max Value");
            $('#maxVal').focus();
            return false;
            submitflag = 1;
        }else if(maxValue > 100  && discTypeHeader == "perc"){
            toastr.warning("Max 100% Discount allowed.");
            $('#maxVal').focus();
            return false;
            submitflag = 1;
        }
    }else {
        if( maxValue.length == 0){
            toastr.warning("Please Enter Max Value");
            $('#maxVal').focus();
            return false;
            submitflag = 1;
        }
    }
    
    if( submitflag == 0 ){
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createClientConfiguration",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9,33),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            typeOfConfig : typeOfConfig,
            maxValue : maxValue,
            configId : configId,
            taxName : taxName,
            typeOfTrans : typeOfTrans,
            discTypeHeader : discTypeHeader,
            requestClientId : requestClientId,
            flag : checkflag
        };
        commonAjax(FOLLOWUPURL, postData, editclientCallback,"Please Wait... Getting Dashboard Detail");
        
        function editclientCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data);

                var index = findIndexByKeyValue( clientData,'id',configId );
                
                var tempArray = {
                    clientName: requestClientName,
                    flag: checkflag,
                    id: clientData[index].id,
                    maxValue: maxValue,
                    discType: discTypeHeader,
                    taxName: taxName,
                    tranType: typeOfTrans,
                    typeOfConfig: typeOfConfig
                }
                
//                var tempArray = {
//                    clientName: requestClientName,
//                    flag: flag,
//                    id: clientData[index].id,
//                    maxValue: maxValue,
//                    typeOfConfig: typeOfConfig
//                }

                clientData[index] = tempArray;

                $('#createBranchBtn').attr('onclick','createClient()');

                clientConfigDataRender();

                resetData();

            } else {
                if (flag)
                    // toastr.error(data.status);
                    displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_CLIENT_CONFIG );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

    
}

function resetData(){

    $('#maxVal').val('');
    $('#taxName').val('');
    $('#createBranchBtn').attr('onclick','createClient()');
    $("#typeOfConfig").val("INQGROUP");
    $("#MF").prop("checked",false);
    $("#typeOfConfig").selectpicker("refresh");
    showHideFileds();
}

function showHideFileds(value){
    if(value == "HEADERTAX"){
        $("#divforDisplayVal").show();
        $("#typeOfDisc").show();
        $("#typeOfTrans").show();
    }else{
        $("#divforDisplayVal").hide();
        $("#typeOfDisc").hide();
        $("#typeOfTrans").hide();
    }
}