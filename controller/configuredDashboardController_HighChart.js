/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-11-2016.
 * File : configuredDashboardController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_DASHBOARD_DATA = [];
var GBL_DASHBOARD_DATA = [];

var GBL_TOP5_CUSTOMER = [];
var GBL_BOTTOM5_CUSTOMER = [];
var GBL_TOP5_ITEM = [];
var GBL_BOTTOM5_ITEM = [];
var firstTimeLoadFlag = false;

var GBL_USER_ID = "";
var GBL_USER_NAME = "";

var stDate = new Date().format('yyyy-MM-dd');
var enDate = new Date().format('yyyy-MM-dd');

var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
var statListInd = findIndexByKeyValue( roleAuth.data.statusListing , "statusCatName" , "INQUIRY" );
if( statListInd != "-1" ){
    var thirdPIndex = findIndexByKeyValue( roleAuth.data.statusListing[statListInd].statusListing , "thirdPartyFlag" , "1" );
    if( thirdPIndex != "-1" ){
        wonStatId = roleAuth.data.statusListing[statListInd].statusListing[thirdPIndex].ID;
    }
}


var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
var allDashCompoArr = loadauth.data.dashbordGridList;

function getDashboardData(){

    var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
    var userDashCompoArr = loadauth.data.userActiveGridList;
    var dashbordGridList = loadauth.data.dashbordGridList;

    var gridIdCs = "";

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID != "-1" ){
        userDashCompoArr.forEach( function( record , index ){
            if( gridIdCs ){
                gridIdCs += ','+record.ID
            }else{
                gridIdCs = record.ID;
            }
        });
    }else{
        dashbordGridList.forEach( function( record , index ){
            if( gridIdCs ){
                gridIdCs += ','+record.ID
            }else{
                gridIdCs = record.ID;
            }
        });
    }

    GBL_USER_ID = tmp_json.data[0].PK_USER_ID;
    GBL_USER_NAME = tmp_json.data[0].FULLNAME;
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'customDashboard',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1,2),
        gridId: gridIdCs,
        // gridId: userDashCompoArr,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, dashboardDataCallback,"Please Wait... Getting Dashboard Detail");

    function dashboardDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            GBL_DASHBOARD_DATA = data.data;
            renderDashboardGridWise();

        } else {
            if (flag)
                // toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_DASHBOARD_PROP );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderDashboardGridWise(){

    var contentHtml = ""; 

    for(var i=0;i<GBL_DASHBOARD_DATA.length;i++){

        var key = GBL_DASHBOARD_DATA[i]['keyName'];

        switch (key) {
            case "DashbordKpi": // IT WILL CALL INQUIRY KPI FUNCTION FOR ITS DATA
                contentHtml += generateDashboardKpi( GBL_DASHBOARD_DATA[i].DashbordKpi , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "activityKpi": // IT WILL CALL ACTIVITY KPI FUNCTION FOR ITS DATA
                contentHtml += generateActivityKpi( GBL_DASHBOARD_DATA[i].activityKpi , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "targetKpi": // IT WILL CALL TARGET KPI FUNCTION FOR ITS DATA
                contentHtml += generateTargetKpi( GBL_DASHBOARD_DATA[i].targetKpi , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "topButtomCustItem": // IT WILL CALL TOP BOTTOM KPI FUNCTION FOR ITS DATA
                contentHtml += generatetopButtomCustItem( GBL_DASHBOARD_DATA[i].topButtomCustItem , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "dealSleepingAway": // IT WILL CALL DEAL SLEEPING KPI FUNCTION FOR ITS DATA
                contentHtml += generatedealSleepingAway( GBL_DASHBOARD_DATA[i].dealSleepingAway , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getFunnelChart": // IT WILL CALL FUNNEL CHART FUNCTION FOR ITS DATA
                contentHtml += generatefunnelChart( GBL_DASHBOARD_DATA[i].getFunnelChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getInqSourceChart": // IT WILL CALL INQUIRY SOURCE FUNCTION FOR ITS DATA
                contentHtml += generateinqSourceChart( GBL_DASHBOARD_DATA[i].getInqSourceChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getInqTypeChart": // IT WILL CALL INQUIRY TYPE FUNCTION FOR ITS DATA
                contentHtml += generateinqTypeChart( GBL_DASHBOARD_DATA[i].getInqTypeChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getDealLostChart": // IT WILL CALL DEAL LOST CHART FUNCTION FOR ITS DATA
                contentHtml += generatedealLostChart( GBL_DASHBOARD_DATA[i].getDealLostChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getCurrentFiscalTragetAndAchieve": // IT WILL CALL TARGET ACHIEVE CURR FISCAL FUNCTION FOR ITS DATA
                contentHtml += generatetargetAchieveCurrFiscal( GBL_DASHBOARD_DATA[i].getCurrentFiscalTragetAndAchieve , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getEmpWiseStatusChartPending": // IT WILL CALL EMPLOYEE WISE CHART PENDING FUNCTION FOR ITS DATA
                contentHtml += generateempwiseStatusChartPending( GBL_DASHBOARD_DATA[i].getEmpWiseStatusChartPending , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getEmpWiseStatusChartBookLost": // IT WILL CALL STATUS CHART BOOK LOST FUNCTION FOR ITS DATA
                contentHtml += generateempwiseStatusChartBookLost( GBL_DASHBOARD_DATA[i].getEmpWiseStatusChartBookLost , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getEmpWiseEffectiveNessChart": // IT WILL CALL EMPLOYEE EFFECTIVENESS FUNCTION FOR ITS DATA
                contentHtml += generateempwiseEffectiveChart( GBL_DASHBOARD_DATA[i].getEmpWiseEffectiveNessChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "statusChangeAverageAging": // IT WILL CALL STATUS CHANGE AVERAGE AGING FUNCTION FOR ITS DATA
                contentHtml += generatestatusChangeAverageAging( GBL_DASHBOARD_DATA[i].statusChangeAverageAging , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getDealProbabilityChanceChart": // IT WILL CALL DEAL PROBABILITY FUNCTION FOR ITS DATA
                contentHtml += generatedealProbabilityChanceChart( GBL_DASHBOARD_DATA[i].getDealProbabilityChanceChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getInquiryDateWiseChart": // IT WILL CALL DATE WISE DATA FUNCTION FOR ITS DATA
                contentHtml += generatedateWiseData( GBL_DASHBOARD_DATA[i].getInquiryDateWiseChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getConvertionRation": // IT WILL CALL INQUIRY CONVERSION KPI FUNCTION FOR ITS DATA
                contentHtml += generateinquiryConversion( GBL_DASHBOARD_DATA[i].getConvertionRation , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getSalesSpreadChart": // IT WILL CALL SALES SPREAD FUNCTION FOR ITS DATA
                contentHtml += generatesalesSpread( GBL_DASHBOARD_DATA[i].getSalesSpreadChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getDealWonChart": // IT WILL CALL DEAL WON CHART FUNCTION FOR ITS DATA
                contentHtml += generatedealWonChart( GBL_DASHBOARD_DATA[i].getDealWonChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "activityCalenderData": // IT WILL CALL DEAL WON CHART FUNCTION FOR ITS DATA
                contentHtml += generateActivityCalenderData( GBL_DASHBOARD_DATA[i].activityCalenderData , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getActTypeChart": // IT WILL CALL DEAL WON CHART FUNCTION FOR ITS DATA
                contentHtml += generateActivityTypeChart( GBL_DASHBOARD_DATA[i].getActTypeChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getActModeChart": // IT WILL CALL DEAL WON CHART FUNCTION FOR ITS DATA
                contentHtml += generateActivityModeChart( GBL_DASHBOARD_DATA[i].getActModeChart , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getActConversion": // IT WILL CALL DEAL WON CHART FUNCTION FOR ITS DATA
                contentHtml += generateActivityConversionChart( GBL_DASHBOARD_DATA[i].getActConversion , GBL_DASHBOARD_DATA[i].keyName );
                break;
            case "getActComparison": // IT WILL CALL DEAL WON CHART FUNCTION FOR ITS DATA
                contentHtml += generateActivityComparison( GBL_DASHBOARD_DATA[i].getActComparison , GBL_DASHBOARD_DATA[i].keyName );
                break;
        }
    }

    $('#dashboardCompoGridDiv').html( contentHtml );

    Pleasure.init();
    Layout.init();

    // DRAW ALL CHART ON GOOGLE API LOAD
    
    // google.charts.setOnLoadCallback(renderAllChartData); 
    renderAllChartData();
    //draggableEvent();

    function generateDashboardKpi( data , keyName ){
        
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // INQUIRY KPI

            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4><i class="fa fa-key"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-blue curPoint" id="statusId" onclick="clientopenInq(this)">'+
                                                '<i class="fa fa-folder-open-o dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.open_inq_count +'</p>'+
                                                    '<small><i class="fa fa-arrow-circle-right"></i> Active Inquiry </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-amber curPoint" onclick="navigateToStudent()">'+
                                                '<i class="fa fa-users dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.customerCount +'</p>'+
                                                    '<small> Total Customer </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-pink">'+
                                                '<i class="fa fa-thumbs-down dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.dealCancelledByUser +'</p>'+
                                                    '<small> Deal Cancelled (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate curPoint"  onclick="navigateToInqReportWithDealClosed(this)">'+
                                            '<div class="card card-dashboard-info card-light-green">'+
                                                '<i class="fa fa-thumbs-up dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.dealClosedByUser +'</p>'+
                                                    '<small> Deal Closed (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate curPoint" type="created" onclick="navigateToInqReportWithFilter(this)">'+
                                            '<div class="card card-dashboard-info card-cyan">'+
                                                '<i class="fa fa-question dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.totalCreatedInqCount +' / '+ data.totalCreatedInqPotAmount +'</p>'+
                                                    '<small> Generated (No/In Lakhs) (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate curPoint" type="assigned" onclick="navigateToInqReportWithFilter(this)">'+
                                            '<div class="card card-dashboard-info card-orange">'+
                                                '<i class="fa fa-hand-rock-o dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.totalAssignInqCount +' / '+ data.totalAssignInqPotAmountPasson +'</p>'+
                                                    '<small>On-Hand (No/In Lakhs) (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate curPoint" onclick="navigateToPropReportWithFilter(this)">'+
                                            '<div class="card card-dashboard-info card-red">'+
                                                '<i class="fa fa-list-ol dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.totalProposalCount +'</p>'+
                                                    '<small> Proposal (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateActivityKpi( data , keyName ){
        
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // ACTIVITY KPI

            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4><i class="fa fa-key"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-amber curPoint" type="today" onclick="getActSideBar(this)">'+
                                                '<i class="fa fa-calendar-check-o dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id=""> '+ data.pendingActivities +' </p>'+
                                                    '<small> Today </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-red curPoint" type="delayed" onclick="getActSideBar(this)">'+
                                                '<i class="fa fa-exclamation-triangle dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.DelayedActivities +'</p>'+
                                                    '<small> Delayed </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-light-green curPoint" type="upcomming" onclick="getActSideBar(this)">'+
                                                '<i class="fa fa-calendar-minus-o dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.upcomingActivities +'</p>'+
                                                    '<small> Planned </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-cyan curPoint" type="inprocess" onclick="getActSideBar(this)">'+
                                                '<i class="fa fa-random dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.inprocessActivities +'</p>'+
                                                    '<small> InProcecss </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate curPoint" onclick="navigateToactivityWithFilter(this)">'+
                                            '<div class="card card-dashboard-info card-teal">'+
                                                '<i class="fa fa-calendar-check-o dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.performedActivity +'</p>'+
                                                    '<small> Performed (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate curPoint" onclick="navigateToactivityWithFilterMiss()">'+
                                            '<div class="card card-dashboard-info card-grey" type="missed">'+
                                                '<i class="fa fa-calendar-minus-o dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.missedActivity +'</p>'+
                                                    '<small> Missed (Yesterday) </small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateTargetKpi( data , keyName ){
        
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // ACTIVITY KPI

            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4><i class="fa fa-key"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-amber">'+
                                                '<i class="fa fa-bullseye dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.target +'</p>'+
                                                    '<small><i class="fa fa-rupee"></i> Target (In Lakhs)</small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-teal">'+
                                                '<i class="fa fa-line-chart dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.achievement +' </p>'+
                                                    '<small><i class="fa fa-rupee"></i> Achievement (In Lakhs)</small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-pink">'+
                                                '<i class="fa fa-trophy dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.achievementLstYear +'</p>'+
                                                    '<small><i class="fa fa-rupee"></i> Last Yr. Achieved (In Lakhs)</small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-4 material-animate">'+
                                            '<div class="card card-dashboard-info card-blue-grey">'+
                                                '<i class="fa fa-diamond dashboard-info-icon"></i>'+
                                                '<div class="card-body">'+
                                                    '<p class="result" id="">'+ data.pipelineSize +'</p>'+
                                                    '<small><i class="fa fa-rupee"></i> Pipeline Size (In Lakhs)</small>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }
    
    function generatetopButtomCustItem( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // TOP BOTTOM CUSTOMER

            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-sort-numeric-asc"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<ul class="nav nav-tabs nav-justified" role="tablist">'+
                                        '<li class="active tab-top-color curPoint" data-toggle="tab" href="#topCust" onclick="topCustomer()"><a href="javascript:void(0)">Top 5 Customer</a></li>'+
                                        '<li class="tab-bottom-color curPoint" href="#bottomCust" data-toggle="tab" onclick="bottomCustomer()"><a href="javascript:void(0)">Bottom 5 Customer</a></li>'+
                                        '<li class="tab-top-color curPoint" href="#topItem" data-toggle="tab" onclick="topItem()"><a href="javascript:void(0)">Top 5 Items</a></li>'+
                                        '<li class="tab-bottom-color curPoint" href="#bottomItem" data-toggle="tab" onclick="bottomItem()"><a href="javascript:void(0)">Bottom 5 Items</a></li>'+
                                    '</ul>'+
                                    '<div class="tab-content">'+
                                        '<div class="tab-pane active" id="topCust">'+
                                            '<div class="overflow-table" id="topCustDT">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="tab-pane" id="bottomCust">'+
                                            '<div class="overflow-table" id="bottomCustDT">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="tab-pane" id="topItem">'+
                                            '<div class="overflow-table" id="topItemDT">'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="tab-pane" id="bottomItem">'+
                                            '<div class="overflow-table" id="bottomItemDT">'+
                                            '</div>'+
                                        '</div>'+
                                        '<span>*( in Lakhs )</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }
    
    function generatedealSleepingAway( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // DEAL SLEEPING AWAY
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-frown-o"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="sleepingDealDiv"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }
    
    function generatefunnelChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // FUNNEL CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-lg-6  col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-line-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div id="inqFunnel" style="min-width: 800px; max-width: 800px; height: 500px; margin: 0 auto"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateinqSourceChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
            // INQUIRY SOURCE CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-pie-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="sourceWise"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateinqTypeChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // INQUIRY TYPE CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-pie-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="typeWise"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generatedealLostChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // DEAL LOST CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-thumbs-down"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div id="dealLost" class="map-chart-size"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generatetargetAchieveCurrFiscal( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // TARGET ACHIVE CURR FISCAL
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-calendar"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="monthChart"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateempwiseStatusChartPending( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // EMP WISE STATUS CHART PENDING
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-folder-open-o"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="userWiseOpenInq"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateempwiseStatusChartBookLost( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // EMP WISE STATUS CHART BOOK LOST
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-check-square-o"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="bookChrt"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateempwiseEffectiveChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // EMPLOYEE EFFECTIVENESS CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-user-o"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="empEffct"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generatestatusChangeAverageAging( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // STATUS CHANGE AVERAGE AGING
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-bar-chart-o"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="inqTime"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generatedealProbabilityChanceChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // DEAL PROBABILITY CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-balance-scale"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div id="predSales" class="map-chart-size"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generatedateWiseData( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // CLOSURE REPORT
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-calendar-check-o"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div id="inqClosure" class="map-chart-size"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateinquiryConversion( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
            // INQUIRY CONVERSION
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-exchange"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="empStack"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generatesalesSpread( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // SALES SPREAD CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-map-marker"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="closureSpread"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }


    function generatedealWonChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // DEAL WON CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-thumbs-up"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="dealWon"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateActivityCalenderData( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // DEAL WON CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-calendar"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="dashboardCalendar"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateActivityTypeChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // SALES SPREAD CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-pie-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="typeWiseChrt"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }
    
    function generateActivityModeChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // SALES SPREAD CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-pie-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="modeWiseChrt"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }
    
    function generateActivityConversionChart( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // SALES SPREAD CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-pie-chart"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="convertChrt"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }

    function generateActivityComparison( data , keyName ){
        var tempHtml = "";
        if( data != "No record found" && data != null && data != undefined ){
        // SALES SPREAD CHART
            
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , keyName );
            var compoName = ( (indexFound != "-1") ? allDashCompoArr[indexFound].VALUE : "" );

            tempHtml += '<div class="col-md-6 col-sm-12 col-xs-12 draggableBlock portletslist1" id="'+ keyName +'">'+
                            '<div class="panel curPoint">'+
                                '<div class="panel-heading">'+
                                    '<div class="panel-title"><h4> <i class="fa fa-bullseye"></i> '+ compoName +' </h4><i class="fa fa-times pull-right deleteIcon" parentDivId="'+ keyName +'" onclick="hideParentDiv(this)" style="display:none"></i></div>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="" id="targetVsActChrt"></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        }
        return tempHtml;
    }
}

function renderAllChartData(){

    GBL_DASHBOARD_DATA.forEach( function( record , index ){

        if( record.getInquiryDateWiseChart != "No record found" && record.getInquiryDateWiseChart != null && record.getInquiryDateWiseChart != undefined ){
            // CLOSURE REPORT
            
            var InquiryClosureChart = record.getInquiryDateWiseChart;
            if( InquiryClosureChart.length == 0 && InquiryClosureChart == "No record found"){
              return false;
            }
            
            var tbody = "";
            InquiryClosureChart.forEach( function( record , index ){
              tbody += '<tr>'+
                            '<th>'+ record.dayDiff +'</th>'+
                            '<td>'+ parseFloat( record.closureDays ) +'</td>'+
                            '<td>'+ parseFloat( record.actualDays ) +'</td>'+
                        '</tr>'
            });
            $('#inqClosureTable').html( tbody );

            Highcharts.chart('inqClosure', {
                data: {
                    table: 'inqClosureDatatable'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Target Days'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            this.point.y + ' days';
                    }
                }
            });
        }

        if( record.getDealLostChart != "No record found" && record.getDealLostChart != null && record.getDealLostChart != undefined ){
            // DEAL LOST CHART

            var dealLostChart = record.getDealLostChart;
            if( dealLostChart.length == 0 || dealLostChart == "No record found" ){
              return false;
            }

            var categories = [];
            var chartData = [];
            dealLostChart.forEach( function( record , index ){
              categories.push( record.statusName );
              chartData.push( parseFloat( record.inqCount ) );
            });

            Highcharts.chart('dealLost', {
                chart: {
                    type: 'bar'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categories,

                    title: {
                        text: 'Lost Reason'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Deal Lost'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: [{
                    name: 'Lost Remark',
                    data: chartData
                }]
            });
        }

        if( record.getDealProbabilityChanceChart != "No record found" && record.getDealProbabilityChanceChart != null && record.getDealProbabilityChanceChart != undefined ){
            // DEAL PROBABILITY CHART

            var predStackChart = record.getDealProbabilityChanceChart;
            if( predStackChart.length == 0 || predStackChart == "No record found" ){
              return false;
            }

            var chartData = [];
            var categories = [];
            predStackChart.statusWiseData.forEach( function( record , index ){

                var tempData = [];
                categories = []; 
                record.monthWiseData.forEach( function( innrRecrd , innrIndex ){
                    tempData.push( parseFloat( innrRecrd.potgSize ) );
                    categories.push( innrRecrd.month );
                });

                chartData.push({
                    name : record.statusName,
                    data : tempData,
                })
            });

            Highcharts.chart('predSales', {
                chart: {
                    type: 'column'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Amount'
                    }
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                    shared: true
                },
                plotOptions: {
                    column: {
                        stacking: 'column'
                    }
                },
                series: chartData
            });
        }

        if( record.dealSleepingAway != "No record found" && record.dealSleepingAway != null && record.dealSleepingAway != undefined ){
            // DEAL SLEEPING AWAY
            var newRow = '';
            var count = 0;
            
            var thead = '<table id="SleepingTable" class="table table-bordered table-striped table-condensed cf">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th>No</th>'+
                                    '<th>Inq No</th>'+
                                    '<th>Name</th>'+
                                    '<th>Contact Number</th>'+
                                    '<th>Inq Name</th>'+
                                    '<th>Status</th>'+
                                    '<th>Amount</th>'+
                            '</thead>'+
                            '<tbody id="sleepingDealBody">';

            var tfoot = '</tbody></table>'; 

            data = record.dealSleepingAway;
            if( data != "No record found" ){

                data.forEach( function( record , index ){

                    count = count + 1;
                    newRow += '<tr>'+
                             '<td>'+ count +'</td>'+
                             '<td><a href="javascript:void(0)" onclick="navigateToInquiryDetail(this)" id="'+record.inqId+'" custid="'+record.custId+'" inqid="'+record.inqId+'">'+ record.clientLevelInqId +'</a></td>'+
                             '<td>'+ record.fName+' '+ record.lName +'</td>'+
                             '<td>'+ record.custMobile +'</td>'+
                             '<td>'+ record.projectName +'</td>'+
                             '<td>'+ record.statusName +'</td>'+
                             '<td class="amtDisp">&#8377; '+ numberFormat(record.potAmt) +'</td>'+
                          '</tr>'; 
                });

            }else{

            }
            $('#sleepingDealDiv').html(thead + newRow + tfoot);
            $('#SleepingTable').DataTable({ "stateSave": true });
        }

        if( record.getDealWonChart != "No record found" && record.getDealWonChart != null && record.getDealWonChart != undefined ){
            // DEAL WON CHART

            dealWonChart = record.getDealWonChart;
            if( dealWonChart.length == 0 || dealWonChart == "No record found" ){
              return false;
            }

            var categories = [];
            var chartData = [];
            dealWonChart.forEach( function( record , index ){
              categories.push( record.statusName );
              chartData.push( parseFloat( record.inqCount ) );
            });

            Highcharts.chart('dealWon', {
                chart: {
                    type: 'bar'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categories,
                    title: {
                        text: 'Won Reason'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Deal Won'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: [{
                    name: 'Won Remark',
                    data: chartData
                }]
            });
        }

        if( record.getEmpWiseEffectiveNessChart != "No record found" && record.getEmpWiseEffectiveNessChart != null && record.getEmpWiseEffectiveNessChart != undefined ){
            // EMPLOYEE EFFECTIVENESS CHART
            chartDataEmpwiseEffectiveChart = record.getEmpWiseEffectiveNessChart;

            if( chartDataEmpwiseEffectiveChart.length == 0 || chartDataEmpwiseEffectiveChart == "No record found" ){
              return false;
            }

            var chartData = [];
            chartDataEmpwiseEffectiveChart.forEach( function( record , index ){

                var effPerc = parseFloat( (record.effPer).split('%')[0] );
                var tempData = {
                    name: record.userName,
                    data: [ effPerc ],
                    tooltip: {
                        valueSuffix: '%'
                    },
                    showInLegend: true
                }
                chartData.push( tempData );
            }); 
            
            Highcharts.chart('empEffct', {
            
                chart: {
                    type: 'gauge',
                    plotBackgroundColor: null,
                    plotBackgroundImage: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                title: {
                    text: ''
                },
                pane: {
                    startAngle: -150,
                    endAngle: 150,
                    background: [{
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
                },

                // the value axis
                yAxis: {
                    min: 0,
                    max: 100,

                    minorTickInterval: 'auto',
                    minorTickWidth: 1,
                    minorTickLength: 10,
                    minorTickPosition: 'inside',
                    minorTickColor: '#666',

                    tickPixelInterval: 30,
                    tickWidth: 2,
                    tickPosition: 'inside',
                    tickLength: 10,
                    tickColor: '#666',
                    labels: {
                        step: 1,
                        rotation: 'auto',
                        enabled : true
                    },
                    title: {
                        text: 'Effectiveness'
                    },
                    plotBands: [{
                        from: 0,
                        to: 50,
                        color: '#DF5353' // red
                    }, {
                        from: 50,
                        to: 80,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 80,
                        to: 100,
                        color: '#55BF3B' // green
                    }]
                },
                legend: {
                  labelFormatter: function() {
                    return '<span>' + this.name +' - '+ this.data[0].y + '%</span>';
                  },
                  symbolWidth: 0
                },
                series: chartData

            });
        }

        if( record.getEmpWiseStatusChartBookLost != "No record found" && record.getEmpWiseStatusChartBookLost != null && record.getEmpWiseStatusChartBookLost != undefined ){
            // EMP WISE STATUS CHART BOOK LOST

            var empBookingChart = record.getEmpWiseStatusChartBookLost;
            if( empBookingChart == "No record found" || empBookingChart.length == 0 ){
              return false;
            }

            var categories = [];
            var chartData = [];
            var tempData = [];

            var categories = [];
            empBookingChart[0].month.forEach( function( record , index ){
                categories.push( record.concatKey );
            });
            
            empBookingChart.forEach( function( record , index ){
                var tempUserData = [];
                record.month.forEach( function( inrRcrd ,innrIndex ){
                    tempUserData.push( parseFloat( inrRcrd.statusInqCount ) );
                });
                chartData.push({
                    name : record.fullname,
                    data : tempUserData
                });       
            });

            Highcharts.chart('bookChrt', {

                chart: {
                    type: 'column'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },

                xAxis: {
                    categories: categories
                },

                yAxis: {
                    // allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Counts (User Wise)'
                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' +
                            'Total: ' + this.point.stackTotal;
                    }
                },

                plotOptions: {
                    column: {
                        stacking: 'column'
                    }
                },
                series: chartData,
            });
            
        }

        if( record.getEmpWiseStatusChartPending != "No record found" && record.getEmpWiseStatusChartPending != null && record.getEmpWiseStatusChartPending != undefined ){
            // EMP WISE STATUS CHART PENDING

            var userWiseOpenInquiryChart = record.getEmpWiseStatusChartPending;
            if( userWiseOpenInquiryChart.length == 0 || userWiseOpenInquiryChart == "No record found" ){
              return false;
            }

            var categories = [];
            userWiseOpenInquiryChart[0].userData.forEach( function( record , index ){
                categories.push( record.fullname );
            });

            var chartData = [];
            userWiseOpenInquiryChart.forEach( function( record , index ){
                var tempUserData = [];
                record.userData.forEach( function( inrRcrd ,innrIndex ){
                    tempUserData.push( parseFloat( inrRcrd.statusInqCount ) );
                });
                chartData.push({
                    name : record.statusName,
                    data : tempUserData,
                });
            });

            Highcharts.chart('userWiseOpenInq', {
                chart: {
                    type: 'column'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Amount'
                    }
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                    shared: true
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
                series: chartData
            });
        }

        if( record.getFunnelChart != "No record found" && record.getFunnelChart != null && record.getFunnelChart != undefined ){
            // FUNNEL CHART
            chartDataFunnelChart = record.getFunnelChart;

            if( chartDataFunnelChart.length == 0 || chartDataFunnelChart == "No record found" ){
              return false;
            }
            
            var chartData = [];
            chartDataFunnelChart.forEach( function( record , index ){
                chartData.push( [ record.statusName , parseFloat( record.potSize ) ] );
            });
            
            Highcharts.chart('inqFunnel', {
                chart: {
                    type: 'funnel'
                },
                title: {
                    text: ''
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b> ({point.y:,.0f})',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            softConnector: true
                        },
                        center: ['40%', '50%'],
                        neckWidth: '30%',
                        neckHeight: '25%',
                        width: '80%'
                    }
                },
                legend: {
                    enabled: false
                },
                series: [{
                    name: 'Inquiry (in rs.)',
                    data: chartData
                }]
            });
        }

        if( record.getInqSourceChart != "No record found" && record.getInqSourceChart != null && record.getInqSourceChart != undefined ){
            // INQUIRY SOURCE CHART

            var sourceWiseChart = record.getInqSourceChart;
            if( sourceWiseChart == "No record found" || sourceWiseChart.length == 0 ){
              return false;
            }

            var chartData = [];
            sourceWiseChart.forEach( function( record , index ){
                var tempData = {
                    name : record.statusName,
                    y : parseFloat( record.inqCount ),
                }
                chartData.push( tempData );
            });

            Highcharts.chart('sourceWise', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Count',
                    colorByPoint: true,
                    data: chartData
                }]
            });
        }

        if( record.getInqTypeChart != "No record found" && record.getInqTypeChart != null && record.getInqTypeChart != undefined ){
            // INQUIRY TYPE CHART

            var typeWiseChart = record.getInqTypeChart;
            if( typeWiseChart == "No record found" || typeWiseChart.length == 0 ){
              return false;
            }

            var chartData = [];
            typeWiseChart.forEach( function( record , index ){
                var tempData = {
                    name : record.statusName,
                    y : parseFloat( record.inqCount ),
                }
                chartData.push( tempData );
            });

            Highcharts.chart('typeWise', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Count',
                    colorByPoint: true,
                    data: chartData
                }]
            });
        }

        if( record.getConvertionRation != "No record found" && record.getConvertionRation != null && record.getConvertionRation != undefined ){
            // INQUIRY CONVERSION

            var inquiryConversionChart = record.getConvertionRation;
            if( inquiryConversionChart == "No record found" || inquiryConversionChart.length == 0 ){
              return false;
            } 

            var chartData = [];
            inquiryConversionChart.forEach( function( record , index ){
                var tempData = {
                    name : record.statusName,
                    y : parseFloat( record.statusCount ),
                }
                chartData.push( tempData );
            }); 
            Highcharts.chart('empStack', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Count',
                    colorByPoint: true,
                    data: chartData
                }]
            });
        }

        if( record.getSalesSpreadChart != "No record found" && record.getSalesSpreadChart != null && record.getSalesSpreadChart != undefined ){
            // SALES SPREAD CHART
            var geoData = record.getSalesSpreadChart.salesSpread;

            if( geoData == undefined || geoData == "No record found" ){
                return false;
            }

            var chartData = [];
            geoData.forEach( function( record , index ){
                var tempData = {
                    "z": parseFloat( record.totalSales ),
                    "lat": parseFloat( record.latitude ),
                    "lon": parseFloat( record.longitude )
                }
                chartData.push( tempData );
            })

            var mapData = Highcharts.maps['custom/world'];

            // var data =  [{
                // "z": 10038,
                // "lat": 26.5727,
                // "lon": 73.8390
            // }];

            $('#closureSpread').highcharts('Map', {
                title: {
                    text: 'Customer Sales'
                },
                chart : {
                    borderWidth : 1
                },
                legend: {
                    enabled: true
                },

                mapNavigation: {
                    enabled: true,
                    buttonOptions: {
                        verticalAlign: 'bottom'
                    }
                },

                series : [{
                    name: 'Countries',
                    mapData: mapData,
                    color: '#E0E0E0',
                    enableMouseTracking: false
                }, {
                    type: 'mapbubble',
                    mapData: mapData,
                    name: 'Total Sales',
                    data: chartData,
                     joinBy: ['postal-code', 'code'],
                    minSize: 4,
                    maxSize: '12%',
                    tooltip: {
                        pointFormat: '{point.code}: {point.z} rs.'
                    }
                }]
            });

        }

        if( record.statusChangeAverageAging != "No record found" && record.statusChangeAverageAging != null && record.statusChangeAverageAging != undefined ){
            // STATUS CHANGE AVERAGE AGING

            var inqColumnChart = record.statusChangeAverageAging;
            if( inqColumnChart.length == 0 || inqColumnChart == "No record found" ){
              return false;
            }

            var chartData = [];
            inqColumnChart.forEach( function( record , index ){
                var tempJson = [ record.statusName , parseFloat( record.averageDays )];
                chartData.push( tempJson );
            }); 
            Highcharts.chart('inqTime', {
              chart: {
                  type: 'column'
              },
              // colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
              title: {
                  text: ''
              },
              xAxis: {
                  type: 'category',
                  labels: {
                      rotation: -45,
                      style: {
                          fontSize: '13px',
                          fontFamily: 'Verdana, sans-serif'
                      }
                  }
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'No of Days'
                  }
              },
              legend: {
                  enabled: false
              },
              tooltip: {
                  pointFormat: 'Status Aging'
              },
              series: [{
                  name: 'Population',
                  data: chartData,
                  dataLabels: {
                      enabled: true,
                      rotation: -90,
                      color: '#FFFFFF',
                      align: 'right',
                      format: '{point.y:.1f}', // one decimal
                      y: 10, // 10 pixels down from the top
                      style: {
                          fontSize: '13px',
                          fontFamily: 'Verdana, sans-serif'
                      }
                  }
              }]
          });
        }

        if( record.getCurrentFiscalTragetAndAchieve != "No record found" && record.getCurrentFiscalTragetAndAchieve != null && record.getCurrentFiscalTragetAndAchieve != undefined ){
            // TARGET ACHIVE CURR FISCAL

            var monthClosureChart = record.getCurrentFiscalTragetAndAchieve;
            if( monthClosureChart.length == 0 || monthClosureChart == "No record found"){
              return false;
            }

            var tbody = "";
            monthClosureChart.forEach( function( record , index ){
              tbody += '<tr>'+
                            '<th>|'+ record.month +'|</th>'+
                            '<td>'+ parseFloat( record.target ) +'</td>'+
                            '<td>'+ parseFloat( record.achievement ) +'</td>'+
                        '</tr>'
            });
            $('#monthClosureTable').html( tbody );

            Highcharts.chart('monthChart', {
                data: {
                    table: 'monthClosureDatatable'
                },
                colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Target Amount (in lakhs)'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            this.point.y + ' lakhs';
                    }
                }
            });
        }

        if( record.topButtomCustItem != "No record found" && record.topButtomCustItem != null && record.topButtomCustItem != undefined ){
            // TOP BOTTOM CUSTOMER
            GBL_TOP5_CUSTOMER = record.topButtomCustItem.topFiveCustomer;
            GBL_BOTTOM5_CUSTOMER = record.topButtomCustItem.bottomFiveCustomer;
            GBL_TOP5_ITEM = record.topButtomCustItem.topFiveItem;
            GBL_BOTTOM5_ITEM = record.topButtomCustItem.bottomFiveItem;

            topCustomer(); // INITTIALIZE THE TOP CUSTOMER
        }

        if( record.activityCalenderData != "No record found" && record.activityCalenderData != null && record.activityCalenderData != undefined ){
            // ACTIVITY CALENDAR

            var events = [];
            var todayDate = new Date();
            for(var g=0;g<record.activityCalenderData.length;g++){
                var value = record.activityCalenderData[g];
                var color = "";
                if( value['ACT_STATUS']== 0 ){ // PENDING
                    color = "#FEC006"; 
                }else if(value['ACT_STATUS'] == 1){ // IN PROCESSS
                    color = "#5CB85C";
                }else if(value['ACT_STATUS'] == 2){ // COMPLETED
                    color = "#008AB1";
                }

                var actEndTime = new Date(value['DUE_DATE']+" "+value['DUE_TIME']);
                if( value['ACTMODE'] == "Phone" ){
                    actEndTime = new Date(value['DUE_DATE']+" "+ PlusFromTime( actEndTime , 20 ) );
                
                }else if( value['ACTMODE'] == "Personal Meeting" ){
                    actEndTime = new Date(value['DUE_DATE']+" "+ PlusFromTime( actEndTime , 60 ) );
                
                }else{
                    actEndTime = new Date(value['DUE_DATE']+" "+value['DUE_TIME']);
                }
                
                
                var remindOn = value['REMINDER_ON'];
                if( remindOn ){
                    remindOn = remindOn.split('|');
                    var remindDate = remindOn[0];
                    var remindTime = remindOn[1];
                    remindOn = ( mysqltoDesiredFormat( remindDate , 'dd-MM-yyyy' ) + " " + remindTime );
                }

                events.push({
                    title: value['ACT_TITLE'],
                    description: value['ACT_DESC'],
                    customerName: value['CUTOMERDETAIL'],
                    customerNo: value['PHONE_NO'],
                    start: new Date(value['DUE_DATE']+" "+value['DUE_TIME']), // will be parsed
                    end: actEndTime, // will be parsed
                    dueAt: mysqltoDesiredFormat( value['DUE_DATE'] , 'dd-MM-yyyy' ) +" "+value['DUE_TIME'], // will be parsed
                    actStatus:value['ACT_STATUS_NAME'],
                    actStatusId:value['ACT_STATUS'],
                    actAssignTo : value['ASSIGNEDTO'],
                    actId : value['PK_ACT_ID'],
                    reminderOn : remindOn,
                    lastActDate : value['LASTACTIVTYDATE'],
                    custId : value['FK_CUST_ID'],
                    createdBy : value['CREATEDBY'],
                    createdOn : value['CREATEDTIME'],
                    actMode : value['ACTMODE'],
                    actModeId : value['ACT_MODE'],
                    color  : color,
                });
            }
            var DashboardCalendar = {
                createCalendar: function () {
                    var date = new Date(),
                        m = date.getMonth(),
                        d = date.getDate(),
                        y = date.getFullYear();      
                                  
                        $('#dashboardCalendar').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,agendaWeek,agendaDay'
                            },
                            firstDay: 0, // 1 for Monday, 2 for Tuesday
                            //isRTL: Pleasure.settings.rtl,
                            eventLimit: true,
                            editable: false,
                            droppable: false,
                                        lazyFetching: false,
                                        dayPopoverFormat: 'DD/MM/YYYY',
                                        weekMode: 'liquid',
                            events: function(start, end, timezone, callback) {
                                data: events    // PRE INITIALIZATION WITH DATA
                                callback(events);
                            },
                            eventClick:  function(event, jsEvent, view) {
                                //set the values and open the modal
                                console.log(event);
                                $("#actId").html(event.actId);
                                $("#moreDetailBtn").attr("data-activityid",event.actId);
                                $("#actTitle").html(event.title);
                                $("#description").html(event.description);
                                $("#actMode").html(event.actMode);
                                $("#actState").html(event.actStatus);
                                $("#actState").css('color',event.color);
                                $("#assignedTo").html(event.actAssignTo);
                                $("#createdBy").html(event.createdBy);
                                $("#createdOn").html(event.createdOn);
                                $("#customerName").html(event.customerName);
                                $("#customerNo").html(event.customerNo);
                                $("#lastActDate").html(mysqltoDesiredFormat( event.lastActDate , 'dd-MM-yyyy' ));
                                $("#RemindOn").html(event.reminderOn);
                                $("#dueAt").html(event.dueAt);
                                
                                $('#statusIcon').removeClass('fa-phone fa-envelope fa-paper-plane fa-male');
                                if( event.actModeId == "1"){
                                    $('#statusIcon').addClass('fa-phone');
                                }else if( event.actModeId == "2"){
                                    $('#statusIcon').addClass('fa-envelope');
                                }else if( event.actModeId == "3"){
                                    $('#statusIcon').addClass('fa-paper-plane');
                                }else if( event.actModeId == "4"){
                                    $('#statusIcon').addClass('fa-male');
                                }

                                $('#activityDialog').removeClass('pendingActClass');
                                $('#activityDialog').removeClass('inprocessActClass');
                                $('#activityDialog').removeClass('completedActClass');
                                if( event.actStatusId == 0 ){
                                    $('#activityDialog').addClass('pendingActClass');
                                }else if( event.actStatusId == 1 ){
                                    $('#activityDialog').addClass('inprocessActClass');
                                }else  if( event.actStatusId == 2 ){
                                    $('#activityDialog').addClass('completedActClass');
                                } 
                                $("#calendarDetail").modal("show");
                            }

                        });
                        
                    },
                    init: function () {
                        this.createCalendar();
                    }
            }

            DashboardCalendar.init();    // Calendar Initialization

        }
    
        if( record.getActTypeChart != "No record found" && record.getActTypeChart != null && record.getActTypeChart != undefined ){
            // STATUS CHANGE AVERAGE AGING

            GBL_TYPE_WISE = record.getActTypeChart.actType;
            var chartData = [];
              GBL_TYPE_WISE.forEach( function( record , index ){
                  var tempData = {
                      name : record.statusName,
                      y : parseFloat( record.actCount ),
                  }
                  chartData.push( tempData );
              });
              Highcharts.chart('typeWiseChrt', {
                  chart: {
                      plotBackgroundColor: null,
                      plotBorderWidth: null,
                      plotShadow: false,
                      type: 'pie'
                  },
                  title: {
                      text: ''
                  },
                  colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                  tooltip: {
                      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                      pie: {
                          allowPointSelect: true,
                          cursor: 'pointer',
                          dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                              style: {
                                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                              }
                          }
                      }
                  },
                  series: [{
                      name: 'Count',
                      colorByPoint: true,
                      data: chartData
                  }]
              });

        }

        if( record.getActModeChart != "No record found" && record.getActModeChart != null && record.getActModeChart != undefined ){
            // STATUS CHANGE AVERAGE AGING

            GBL_MODE_WISE = record.getActModeChart.actMode;
             var chartData = [];
            GBL_MODE_WISE.forEach( function( record , index ){
                  var tempData = {
                      name : record.statusName,
                      y : parseFloat( record.actCount ),
                  }
                  chartData.push( tempData );
              });
              Highcharts.chart('modeWiseChrt', {
                  chart: {
                      plotBackgroundColor: null,
                      plotBorderWidth: null,
                      plotShadow: false,
                      type: 'pie'
                  },
                  colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                  title: {
                      text: 'Activity Mode Wise Chart'
                  },
                  tooltip: {
                      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                      pie: {
                          allowPointSelect: true,
                          cursor: 'pointer',
                          dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                              style: {
                                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                              }
                          }
                      }
                  },
                  series: [{
                      name: 'Count',
                      colorByPoint: true,
                      data: chartData
                  }]
              });
        }

        if( record.getActConversion != "No record found" && record.getActConversion != null && record.getActConversion != undefined ){
            // STATUS CHANGE AVERAGE AGING
            
            var GBL_CONVERT_WISE = record.getActConversion.actConversion;
            var chartData = [];
              GBL_CONVERT_WISE.forEach( function( record , index ){
                  var tempData = {
                      name : record.statusName,
                      y : parseFloat( record.statusCount ),
                  }
                  chartData.push( tempData );
              });
              Highcharts.chart('convertChrt', {
                  chart: {
                      plotBackgroundColor: null,
                      plotBorderWidth: null,
                      plotShadow: false,
                      type: 'pie'
                  },
                  colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE','#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
                  title: {
                      text: ''
                  },
                  tooltip: {
                      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                      pie: {
                          allowPointSelect: true,
                          cursor: 'pointer',
                          dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                              style: {
                                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                              }
                          }
                      }
                  },
                  series: [{
                      name: 'Count',
                      colorByPoint: true,
                      data: chartData
                  }]
              });
        }

        if( record.getActComparison != "No record found" && record.getActComparison != null && record.getActComparison != undefined ){
            // STATUS CHANGE AVERAGE AGING

            GBL_TARGET_ACTUAL_WISE = record.getActComparison;
            var chartCategory = [];
              var chartTargetData = [];
              var chartActualData = [];

              GBL_TARGET_ACTUAL_WISE.forEach( function( record , index ){
                    
                    if( record.typeData != undefined ){
                        
                      chartTargetData.push( parseFloat( record.typeData[0].targetAct ) );
                      chartActualData.push( parseFloat( record.typeData[0].actCount ) );

                      chartCategory.push( record.actType );
                    }
              });

              var chartData = [];
              chartData = [
                {
                  name : "Target",
                  data : chartTargetData
                },
                {
                  name : "Actual",
                  data : chartActualData
                }
              ]

              Highcharts.chart('targetVsActChrt', {
                  chart: {
                      type: 'column',
                      options3d: {
                          enabled: true,
                          alpha: 15,
                          beta: 15,
                          viewDistance: 25,
                          depth: 40
                      }
                  },
                  colors: ['#4572A7', '#AA4643'],
                  title: {
                      text: ''
                  },

                  xAxis: {
                      categories: chartCategory,
                      labels: {
                          skew3d: true,
                          style: {
                              fontSize: '16px'
                          }
                      }
                  },

                  yAxis: {
                      allowDecimals: false,
                      min: 0,
                      title: {
                          text: 'Number of Activity',
                          skew3d: true
                      }
                  },

                  tooltip: {
                      headerFormat: '<b>{point.key}</b><br>',
                      pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
                  },

                  plotOptions: {
                      column: {
                          // stacking: 'normal',
                          depth: 40
                      }
                  },

                  series: chartData
              });
        }
    });
}

function topCustomer(){

    var topFiveCustomer = GBL_TOP5_CUSTOMER;

    if( topFiveCustomer == undefined || topFiveCustomer == "No record found" ){
        return false;
    }

    var categories = [];
    var chartData = [];

    topFiveCustomer.forEach( function( record , index ){
        categories.push( record.custName );
        chartData.push( parseFloat( record.totalInq ) );
    });

    // Top Five customer Listing starts here
    
    Highcharts.chart('topCustDT', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            // labels: {
            //     overflow: 'justify'
            // }
        },
        tooltip: {
            valueSuffix: ' lakhs'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total Amount',
            data: chartData
        }]
    });
}

function bottomCustomer(){

    var bottomFiveCustomer = GBL_BOTTOM5_CUSTOMER;

    if( bottomFiveCustomer == undefined || bottomFiveCustomer == "No record found" ){
        return false;
    } 
    var categories = [];
    var chartData = [];

    bottomFiveCustomer.forEach( function( record , index ){
        categories.push( record.custName );
        chartData.push( parseFloat( record.totalInq ) );
    });

    // Top Five customer Listing starts here
    
    Highcharts.chart('bottomCustDT', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
        },
        tooltip: {
            valueSuffix: ' lakhs'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total Amount',
            data: chartData
        }]
    });
}

function topItem(){

    var topFiveItem = GBL_TOP5_ITEM;

    if( topFiveItem == undefined || topFiveItem == "No record found" ){
        return false;
    }

    var categories = [];
    var chartData = [];

    topFiveItem.forEach( function( record , index ){
        categories.push( record.itemName );
        chartData.push( parseFloat( record.totalInq ) );
    });

    // Top Five customer Listing starts here
    
    Highcharts.chart('topItemDT', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
        },
        tooltip: {
            valueSuffix: ' lakhs'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total Amount',
            data: chartData
        }]
    });
}

function bottomItem(){

    var bottomFiveItem = GBL_BOTTOM5_ITEM;

    if( bottomFiveItem == undefined  || bottomFiveItem == "No record found" ){
        return false;
    } 
    var categories = [];
    var chartData = [];

    bottomFiveItem.forEach( function( record , index ){
        categories.push( record.itemName );
        chartData.push( parseFloat( record.totalInq ) );
    });

    // Top Five customer Listing starts here
    
    Highcharts.chart('bottomItemDT', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
        },
        tooltip: {
            valueSuffix: ' lakhs'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total Amount',
            data: chartData
        }]
    });
}


function getActSideBar(context){

    var type = $(context).attr('type');
    if( type == "today" ){
        getFollowupDetail(1);
        $('[href="#todayTab"]').tab('show');

    }else if( type == "upcomming" ){
        getFollowupDetail(3);
        $('[href="#upcommingTab"]').tab('show');

    }else if( type == "delayed" ){
        getFollowupDetail(2);
        $('[href="#delayTab"]').tab('show');

    }else{
        getFollowupDetail(4);
        $('[href="#inprocessTab"]').tab('show');
    }
}

function saveDashboardGrid(){

    var dashBoardCompArr = "";

    $('#dashboardCompoGridDiv .draggableBlock.portletslist1').each(function(ind, obj) {
        var currChkId = $(this).attr('id');

        var loadauth = JSON.parse(localStorage.indeCampusRoleAuth);
        var allDashCompoArr = loadauth.data.dashbordGridList;

        if( !($('#'+currChkId).hasClass('deleted')) ){
            var indexFound = findIndexByKeyValue( allDashCompoArr , "FUN_NAME" , currChkId );

            if( indexFound != "-1" ){

                if( dashBoardCompArr ){
                    dashBoardCompArr += "," + allDashCompoArr[ indexFound ].ID;
                }else{
                    dashBoardCompArr = allDashCompoArr[ indexFound ].ID;
                }
            }
        }
    });
    // return false;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'checkUncheckDashbordUserWise',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(1,3),
        gridId: dashBoardCompArr,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, saveDashboardCompoCallback,"Please Wait... Setting Dashboard Component");

    function saveDashboardCompoCallback(flag,data){

        if (data.status == "Success" && flag) {

            toastr.success('Dashboard Component Updated Successfully..!');
            
            var tempLoadAuth = JSON.parse(localStorage.indeCampusRoleAuth);
            tempLoadAuth.data.userActiveGridList = data.data.userActiveGridList;

            localStorage.indeCampusRoleAuth = JSON.stringify( tempLoadAuth );

            getDashboardData();

            $('#dashboardCompoGridDiv').removeClass('draggable-portlets');
            $('#dashboardCompoGridDiv').removeClass('ui-sortable');

            $('#dashboardCompoGridDiv .panel').removeClass('ui-sortable-handle');
            $('#dashboardCompoGridDiv .panel').removeClass('portlet-handle');

            $('.deleteIcon').hide('250');

            $('#cancelGrid').hide('250');
            $('#saveGrid').hide('250');
            $('#editGrid').show('250');
            
            Pleasure.init();
            Layout.init();

        } else {
          if (flag)
              toastr.error(data.status);
          else
              toastr.error(SERVERERROR);
        }
    }
  
}

function editDashboardGrid(){

    // toastr.info('Dynamic Dashboard Grid Activated');

    $('#dashboardCompoGridDiv').addClass('draggable-portlets');
    $('#dashboardCompoGridDiv').addClass('ui-sortable');

    $('#dashboardCompoGridDiv .panel').addClass('ui-sortable-handle');
    $('#dashboardCompoGridDiv .panel').addClass('portlet-handle');

    $('.deleteIcon').show();

    $('#cancelGrid').show('250');
    $('#saveGrid').show('250');
    $('#editGrid').hide('250');

    Pleasure.init();
    Layout.init();
}

function cancelDashboardGrid(){

    // toastr.info('Dynamic Dashboard Grid DeActivated');

    $('#dashboardCompoGridDiv').removeClass('draggable-portlets');
    $('#dashboardCompoGridDiv').removeClass('ui-sortable');

    $('#dashboardCompoGridDiv .panel').removeClass('ui-sortable-handle');
    $('#dashboardCompoGridDiv .panel').removeClass('portlet-handle');

    $('.deleteIcon').hide();

    $('#cancelGrid').hide('250');
    $('#saveGrid').hide('250');
    $('#editGrid').show('250');

    Pleasure.init();
    Layout.init();
}

function hideParentDiv( context ){

    var confirmDel = confirm( "Are you sure? Do you want to remove this component" );
    if( !confirmDel ){
        return false;
    }

    var divId = $(context).attr('parentDivId');

    $('#'+divId).addClass('deleted');
    $('#'+divId).hide();
}

function clientopenInq(context){
    var statusId = $(context).attr("statusId");
    localStorage.POTGcurrentStatusId = statusId;
    navigateToInquiry();

}


function navigateToInqReportWithFilter( context ){
    
    if (checkAuth(28, 116, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        var uname = GBL_USER_ID;
        var unameTxt = GBL_USER_NAME;
        var type = $(context).attr('type');

        localStorage.POTGInqToWhyType = type;
        localStorage.POTGInqToWhyUnameId = uname;
        localStorage.POTGInqToWhyUname = unameTxt;
        localStorage.POTGInqToWhyStDate = stDate;
        localStorage.POTGInqToWhyEnDate = enDate;

        window.open("inqReport.html");
        // window.location.href = "inqReport.html";
    }
}

function navigateToInqReportWithDealClosed(){
    
    if (checkAuth(28, 116, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {

        var uname = GBL_USER_ID;
        var unameTxt = GBL_USER_NAME;
        localStorage.POTGInqToWhyUnameId = uname;
        localStorage.POTGInqToWhyUname = unameTxt;
        localStorage.POTGInqToWhyStDate = stDate;
        localStorage.POTGInqToWhyEnDate = enDate;
        localStorage.POTGInqToWhyStatus= wonStatId;

        window.open("inqWonReport.html");
        // window.location.href = "inqWonReport.html";
    }
}

function navigateToactivityWithFilter(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGInqToWhyActUser = GBL_USER_ID;
        localStorage.POTGInqToWhyActUserTxt = GBL_USER_NAME;
        localStorage.POTGInqToWhyActstDate = stDate;
        localStorage.POTGInqToWhyActenDate = enDate;
        localStorage.POTGInqToWhyActstatType = wonStatId;

       window.open("activityMainReport.html");
        // window.location.href = "activityMainReport.html";
    }
}

function navigateToactivityWithFilterMiss(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGInqToWhyActUser = GBL_USER_ID;
        localStorage.POTGInqToWhyActUserTxt = GBL_USER_NAME;
        localStorage.POTGInqToWhyActstDate = stDate;
        localStorage.POTGInqToWhyActenDate = enDate;
        localStorage.POTGInqToWhyActDelayType = "1";
        localStorage.POTGInqToWhyActMiss = true;

        window.open("activityMainReport.html");
        // window.location.href = "activityMainReport.html";
    }
}

function navigateToPropReportWithFilter(){

    if (checkAuth(2, 6, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        localStorage.POTGPropToWhyUser = GBL_USER_ID;
        localStorage.POTGPropToWhyUserTxt = GBL_USER_NAME;
        localStorage.POTGPropToWhystDate = stDate;
        localStorage.POTGPropToWhyenDate = enDate;

        window.open("proposalReport.html");
        // window.location.href = "proposalReport.html";
    }
}