/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportData = [];
var GBL_EXCELJSON = [];

function getReportData(){

    reportData = [];
    GBL_EXCELJSON = [];

    var stDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var status = $('#typeStatusListing').val();
    var actTypeList = $('#actTypeList').val();
    var userId = $('#user').attr('userId');
    var taskId = $('#projectList').val();

    if( stDate != "-1" && stDate != "" ){
        stDate = ddmmyyToMysql(stDate);
    }else{
        stDate = "-1";
    }
    if( endDate != "-1" && endDate != "" ){
        endDate = ddmmyyToMysql(endDate);
    }else{
        endDate = "-1";
    }
    
    if($('#user').val().length <= 0){
        userId = "";
    }

    var dateActStartDate = new Date(stDate);
    var dateActEndDate = new Date(endDate);

    if( +dateActEndDate < +dateActStartDate ) //dateActEndDate dateActStartDate
    {
        toastr.warning("End Date cannot be less than the StartDate");
        $('#endDate').datepicker('show');
        return false;
    }

    var actTypes = "";
    if( actTypeList ){ 

        actTypeList.forEach( function( record , index ){ 
            if( actTypes ){
                actTypes += ','+record;
            }else{
                actTypes = record;
            }
        });
    }

    var taskIds = "";
    if( taskId ){

        taskId.forEach( function( record , index ){  
            if( taskIds ){
                taskIds += ','+record;
            }else{
                taskIds = record;
            }
        });
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getTaskCampaignPivotReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        startDate: stDate,
        endDate: endDate,
        actType: actTypes,
        selectedUserId: userId,
        reportType: "1", // 1= TASK , 2= CAMPAIGN
        selectedTaskId: taskIds,    //COMMA SEPERATED
        selectedCampaignId: "",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getReportDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        GBL_EXCELJSON = [];
        reportData = data.data;
        console.log(reportData);
        GBL_EXCELJSON = "Success";
        if( reportData ){
            GeneratePivotTable(reportData);
        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TASK_REPORT );
        }


    } else {

        reportData = [];
        GeneratePivotTable(reportData);
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TASK_REPORT );
        else
            toastr.error(SERVERERROR);
    }
}

function GeneratePivotTable(mps) {
    console.log(mps);
    var Sum = $.pivotUtilities.aggregators["Sum"];

    $("#inqReportList").pivot(mps, {
        aggregator: Sum(["count"]),
        rows: ["Task Name","User Name"],
        cols: ["Activity Type" , "Activity Status"]
    });

    $('.sidemenu').addClass('hiden', 300);
}


function AutoCompleteForCustomer(id,callback) {
    
   $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var mobileNumber = item.text.split(",");
            $('#customer').attr('custId',item.value);

        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                GBL_SELECTEDCUSTID = '';
                GBL_SELECTEDCUSTMOBNO = '';
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3,10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {
                        if( data.cust_id != -1 ){
                            
                            var group = {
                                id: data.cust_id,
                                name: data.concatKey,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname,
                                rateTypeId: data.rateTypeId,
                                custTypeId: data.custTypeId
                            };
                            ITEMS.push(group);
                        }
                    });

                    return ITEMS;
                }

            }
        }
    });
}

function saveCustDetail(){

}

function AutoCompleteForUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#user').attr('userid',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveUserDetail(){

}


function downLoadExcel(){
    if( GBL_EXCELJSON.length < 2 ){
        toastr.warning('Please Search Report Before Downloading...');
    }else{
        customCsvFormatSave($('#inqReportList').html(), "Activity_Task_Report.xls");
    }
}

function resetReportFilter(){
    $('#startDate').val('');
    $('#endDate').val('');
    $('#actTypeList').val('-1');
    $('#actTypeList').selectpicker('refresh');
    $('#projectList').val('-1');
    $('#projectList').selectpicker('refresh');
    $('#customer').val('');
    $('#user').val('');
    $('#customer').attr('custId','-1');
    $('#user').attr('userId','-1');
}


function pageInitialEvents(){
    
    $('.fab-dial-handle').click(function(){
        $('.fabdail-buttons').toggleClass('active');
    });

    reportData = [];
    GeneratePivotTable(reportData);
    
    $(".bmd-fab-speed-dialer").on('click', function(){$(this).toggleClass("press")});

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var actType = roleAuth.data.actType;

    setOption("0", "actTypeList", actType, "");
    $('#actTypeList').selectpicker( 'refresh' );

    $('.sidemenu').removeClass('hiden', 300);

    var date = new Date();
    var day = "01";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if( month < 10 ){
        month = "0" + month;
    }

    var monStDate = day + '-' + month + '-' + year;
    var today = new Date().format('dd-MM-yyyy')
    
    $('#startDate').val( monStDate );
    $('#endDate').val( today );

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getActivityProjectListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(50,200),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getProjectDataCallback,"Please Wait... Getting Dashboard Detail");
    function getProjectDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            var options = '<option value="-1" disabled> -- Please Select Project -- </option>';
            if( data.data != "No record found" ){

                data.data.forEach( function( record , index ){
                    options += '<option value="'+ record.actProjectId +'">'+ record.name +'</option>';
                });
            }
            $('#projectList').html( options );
            $('#projectList').selectpicker();
            getReportData();

        } else {

            getReportData();

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_PROJECT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function menu() {
    if ($('.sidemenu').hasClass('hiden', 300)) {
        $('.sidemenu').removeClass('hiden', 300);
        setTimeout(function(){
            $('#startDate').focus();
            $('#startDate').datepicker('show');
        },500);
    } else {
        $('.sidemenu').addClass('hiden', 300);
    }
}