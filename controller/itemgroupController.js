/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 31-05-2016.
 * File : itemgroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var ITEMGRPJSON = [];
var GBL_GRPLISTARR = [];
var treeInit = "0";
var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data

//START FUNCTION FOR LISTING GROUPS
function itemGroupListing() {
    
    addFocusId( 'groupname' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    
    var startHtml ="<table class='display datatables-alphabet-sorting'>"+
                        "<thead>"+
                            "<tr>"+
                                "<th>No</th>"+
                                "<th>Item Group</th>"+
                                "<th>Parent Group</th>"+
                                "<th>Action</th>"+
                            "</tr>"+
                        "</thead><tbody>";

    var html = "";
    if (ITEMGRPJSON.length > 0) {
        for (var i = 0; i < ITEMGRPJSON.length; i++) {

            var groupId = ITEMGRPJSON[i]['GROUPID'];

            var parentName = "No Parent Group";

            if( ITEMGRPJSON[i]['PARENTGROUPNAME'] != "" ){
                parentName = ITEMGRPJSON[i]['PARENTGROUPNAME'];
            }

            var detailFlag = "";
            if( ITEMGRPJSON[i]['GROUP_DETAIL_FLAG'] == "0" ){
                detailFlag = 'disabled';
            }

            html += "<tr>" +
                    "<td>" + (i + 1) + "</td>" +
                    "<td>" + ITEMGRPJSON[i]['GROUPNAME'] + "</td>" +
                    "<td>" + parentName + "</td>" +
                    "<td><a data-placement='top' data-toggle='tooltip' id='varConfigBtn' "+ detailFlag +" href='javascript:void(0)' grpId='" + ITEMGRPJSON[i]['GROUPID'] + "'  onclick='openVCDialog(this)' data-original-title='Item Detailing' title='' class='tooltips btn btn-blue-grey btn-xs btn-ripple e2b btn-right-mrg' style='display:none;''><i class='fa fa-sitemap'></i></a>"+
                    "<a href='javascript:void(0)' class='btn btn-xs btn-primary btn-right-mrg' id='" + ITEMGRPJSON[i]['GROUPID'] + "' data-indexid='" + i + "' data-parentgrpid='" + ITEMGRPJSON[i]['PARENTGROUPID'] + "' onclick='groupDetails(this)'><i class='fa fa-pencil'></i></a>" +
                    "<a href='javascript:void(0)' class='btn btn-xs btn-danger btn-ripple' data-indexid='" + i + "' data-groupid='" + groupId + "' onclick='deleteGroup(this);'><i class='fa fa-trash-o'></i></a></td>" +
                    "</tr>";
            
        }
        
        var endHtml = "</tbody></table>"; 
        var finalHtml = startHtml + html + endHtml;
        $("#grouplisting").html(finalHtml);
        
        var data = JSON.parse(localStorage.indeCampusRoleAuth);
        var currClientType = data.data.clientTypeId;

        console.log(currClientType);
        if( currClientType == 35 ){
            $('.e2b').show();
            initializeTreeView();
        }else{
            $('.e2b').hide();
        }

        TablesDataTables.init();
        TablesDataTablesEditor.init();

    }
}
//END FUNCTION FOR LISTING GROUPS

//START FUNCTION FOR GET GROUP DETAILS
function groupDetails(grpdata) {

    if( checkAuth(13, 51, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }

    //var groupId = $(grpdata).attr("id");
    var arrIndex = $(grpdata).attr("data-indexid");
    //var parentGroupId = $(grpdata).attr("data-parentgrpid");
    console.log(arrIndex + "--->>>ARRAY INDEX ");
    //console.log(arrIndex+"--->>>ARRAY INDEX "); 
    var groupName = ITEMGRPJSON[arrIndex]['GROUPNAME'];
    var groupId = ITEMGRPJSON[arrIndex]['GROUPID'];
    var parentGroupId = ITEMGRPJSON[arrIndex]['PARENTGROUPID'];
    var grpDetailFlag = ITEMGRPJSON[arrIndex]['GROUP_DETAIL_FLAG'];

    $("#groupname").val(groupName);// GROUP NAME
    $("#groupId").val(groupId); // GROUP ID
    //$("#parentGroupId").val(parentGroupId); //PARENT GROUP ID
    
    $("#arrIndex").val(arrIndex); // INDEX OF AN ARRAY
    
    generateGroupDropDown(ITEMGRPJSON, groupId);
    //$("#parentGroupId").val(parentGroupId); //PARENT GROUP ID
    $("#parentGroupId").val(parentGroupId).selectpicker('refresh');

    $('#allowDetail').prop('checked',false);
    if( grpDetailFlag == "1" ){
        $('#allowDetail').prop('checked',true);
    }

    addFocusId( 'groupname' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

}
//START FUNCTION FOR GET GROUP DETAILS


//START FUNCTION FOR UPDATE GROUPS DATA
function updateGroupData() {
    var groupName = $("#groupname").val();// GROUP NAME
    var groupId = $("#groupId").val(); // GROUP ID
    var parentGroupId = $("#parentGroupId").val(); //PARENT GROUP ID
    var PARENTGROUPNAME = $("#parentGroupId option:selected").text(); //PARENT GROUP ID
    var arrIndex = $("#arrIndex").val(); // INDEX OF AN ARRAY
    var detailFlag = "0";
    if( $('#allowDetail').prop('checked') == true ){
        detailFlag = "1";
    }

    ITEMGRPJSON[arrIndex]['GROUPNAME'] = groupName;
    ITEMGRPJSON[arrIndex]['GROUPID'] = groupId;
    ITEMGRPJSON[arrIndex]['PARENTGROUPID'] = parentGroupId;
    ITEMGRPJSON[arrIndex]['PARENTGROUPNAME'] = PARENTGROUPNAME;
    ITEMGRPJSON[arrIndex]['GROUP_DETAIL_FLAG'] = detailFlag;

    //CALL AJAX HERE FOR UPDATE GROUP

    itemGroupListing(); //CALL FUNCTION FOR ITEM GROUP LISTING
    clearGroupData(); // CLEAR DATA OF ITEM GROUP AFTER UPDATE
    
    addFocusId( 'groupname' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}
//END FUNCTION FOR UPDATE GROUPS DATA

//START FUNCTION FOR CLEAR DATA AFTER UPDATE
function clearGroupData() {
    $("#groupname").val('');// GROUP NAME
    $("#groupId").val(''); // GROUP ID
    //$("#parentGroupId").val(''); //PARENT GROUP ID
    $("#arrIndex").val(''); // INDEX OF AN ARRAY
    $("#parentGroupId").val("").selectpicker('refresh');
    $('#allowDetail').prop('checked',false);
    generateGroupDropDown(ITEMGRPJSON);
}
//END FUNCTION FOR CLEAR DATA AFTER UPDATE



function addUpdateItemGroup() {

    function submitAddUpdateAjax()
    {
        function callBackGetGroupListing(flag, data)
        {
            if (data.status == "Success" && flag) {

                var parentGroupName = $('#parentGroupId option:selected').text();
                if (arrIndex == "" || arrIndex == undefined) { // INSERT        

                    var newGroupId = data.groupId;

                    var itemGrpJSON = {
                        "GROUPNAME": groupName,
                        "GROUPID": newGroupId,
                        "PARENTGROUPID": parentGroupId,
                        "PARENTGROUPNAME": parentGroupName,
                        "GROUP_DETAIL_FLAG": detailFlag
                    };
                    ITEMGRPJSON.push(itemGrpJSON);
                    //addUpdateDeleteParentGrpList(itemGrpJSON);

                } else { // UPDATE
                    ITEMGRPJSON[arrIndex]['GROUPNAME'] = groupName;
                    ITEMGRPJSON[arrIndex]['GROUPID'] = groupId;
                    ITEMGRPJSON[arrIndex]['PARENTGROUPID'] = parentGroupId;
                    ITEMGRPJSON[arrIndex]['PARENTGROUPNAME'] = parentGroupName;
                    ITEMGRPJSON[arrIndex]['GROUP_DETAIL_FLAG'] = detailFlag;
                    var itemGrpJSON = {
                        "GROUPNAME": groupName,
                        "GROUPID": groupId,
                        "PARENTGROUPID": parentGroupId,
                        "PARENTGROUPNAME": parentGroupName,
                        "GROUP_DETAIL_FLAG": detailFlag
                    };

                    //addUpdateDeleteParentGrpList(itemGrpJSON,"update");
                }
                
                var data = JSON.parse(localStorage.indeCampusRoleAuth);
                var currClientType = data.data.clientTypeId;
 
                if( currClientType == 35 ){ // VC CLIENT
                    getGroupListing();
                }else{
                    itemGroupListing(); //CALL FUNCTION FOR ITEM GROUP LISTING
                }
                clearGroupData(); // CLEAR DATA OF ITEM GROUP AFTER UPDATE

            }
            else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );
                else
                    toastr.error(SERVERERROR);
            }

        }

        //var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
        if(groupId == ""){ //CREATE GROUP
            if( checkAuth(13, 49, 1) == -1 ){
                toastr.error( NOAUTH );
                return false;

            }
            var orgId = checkAuth(13, 49);
        }else{ //UPDATE GROUP
            if( checkAuth(13, 51, 1) == -1 ){
                toastr.error( NOAUTH );
                return false;

            }
            var orgId = checkAuth(13, 49);
        }
        var postData = {
            requestCase: 'addEditItemGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: orgId,
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupName: groupName,
            groupId: groupId,
            parentGroupId: parentGroupId,
            detailFlag: detailFlag,
        }
        commonAjax(COMMONURL, postData, callBackGetGroupListing, "Please Wait... Getting the group listing");

    }




    var groupName = $("#groupname").val().trim();// GROUP NAME
    var groupId = $("#groupId").val(); // GROUP ID
    var parentGroupId = $("#parentGroupId").val(); //PARENT GROUP ID
    var arrIndex = $("#arrIndex").val(); // INDEX OF AN ARRAY
    var detailFlag = "0";
    if( $('#allowDetail').prop('checked') == true ){
        detailFlag = "1";
    }
    //alert(arrIndex);
    if (groupName.length == 0 ){
        toastr.warning("Please Enter Group Name");
        $('#groupname').focus();
        return false;
    }
    // else if (!parentGroupId || parentGroupId == "-1"){
    //     alert("Please select the parent group");
    //     return false;
    // }
//    else if (groupId == parentGroupId){
//        alert("Please select other parent group id..group and parent cant be same");
//        return false;
//    }

    submitAddUpdateAjax();



}

//START FUNCTION FOR GENERATE PARENT GROUP ID DROPDOWN
function generateGroupDropDown(parentGroupArr, groupIdToIgnore) {
    if(!parentGroupArr)
    {
       parentGroupArr = ITEMGRPJSON;
    }

    var parentGrphtml = "<option value=''> No Parent Group </option>";
    for (var j = 0; j < parentGroupArr.length; j++) {

        var groupId = parentGroupArr[j]['GROUPID'];
        if (groupId != groupIdToIgnore)
        {
            parentGrphtml += "<option value='" + parentGroupArr[j]['GROUPID'] + "'>" + parentGroupArr[j]['GROUPNAME'] + "</option>";
        }
    }
    //console.log(parentGrphtml);
    $("#parentGroupId").html(parentGrphtml); // GENERATE DROPDOWN OF PARENT GROUP ID
    $("#parentGroupId").selectpicker("refresh"); 
    
}
//END FUNCTION FOR GENERATE PARENT GROUP ID DROPDOWN

//START FUNCTION FOR DELETE GROUP FROM AN ARRAY
function deleteGroup(elem) {

    if( checkAuth(13, 52, 1) == -1 ){
            toastr.error( NOAUTH );
            return false;

        }
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        //ITEMGRPJSON.splice(arrIndex, 1);
        var groupId = $(elem).attr('data-groupid');
        var indexid = $(elem).attr('data-indexid');

        if (!groupId)
        {
            console.error("groupId not found to perform delete operation");
            return false;
        }

        function callBackDelgrp(flag, data)
        {
            if (data.status == "Success" && flag) {

                //var index = findIndexByKeyValue(ITEMGRPJSON, "GROUPID", groupId);
                ITEMGRPJSON.splice(indexid, 1);

                itemGroupListing(); //CALL FUNCTION FOR ITEM GROUP LISTING
                clearGroupData(); // CLEAR DATA OF ITEM GROUP AFTER UPDATE

            }
            else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DELETE_GROUP );
                else
                    toastr.warning(SERVERERROR);
            }
        }


        var postData = {
            requestCase: 'deleteGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(13, 52),
            dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupId: groupId,
        }

        commonAjax(COMMONURL, postData, callBackDelgrp, "Please Wait... Deleting the group");
    }
}
//END FUNCTION FOR DELETE GROUP FROM AN ARRAY

function getGroupListing() {

    $('#groupname').focus();

    function callBackGetGroupListing(flag, data)
    {
        if (data.status == "Success" && flag) {

            if (data.data)
            {
                ITEMGRPJSON = data.data;

                for (var i = 0; i < ITEMGRPJSON.length; i++)
                {

                    var groupName = ITEMGRPJSON[i].GROUP_NAME;
                    var groupId = ITEMGRPJSON[i].PK_ITEM_GROUP_ID;
                    var parentGroupId = ITEMGRPJSON[i].FK_PARENT_GRP_ID;
                    var parentGroupName = ITEMGRPJSON[i].PARENTGROUPNAME;

                    ITEMGRPJSON[i].GROUPNAME = groupName;
                    ITEMGRPJSON[i].GROUPID = groupId;
                    ITEMGRPJSON[i].PARENTGROUPID = parentGroupId;
                    ITEMGRPJSON[i].PARENTGROUPNAME = parentGroupName;


                }

                generateGroupDropDown(ITEMGRPJSON);
                itemGroupListing();
            }

        }
        else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );
            else
                toastr.warning(SERVERERROR);
        }

    }

    //var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data

    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(13, 50),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, callBackGetGroupListing, "Please Wait... Getting the group listing");


}

//NOT IN USE
function addUpdateDeleteParentGrpList(itemGrpJSON, action)
{
    var groupName = itemGrpJSON.GROUPNAME;
    var groupId = itemGrpJSON.GROUPID;
    var parentGroupId = itemGrpJSON.PARENTGROUPID;
    var PARENTNAME = itemGrpJSON.PARENTGROUPNAME;



    var tmpData = {
        "GROUPNAME": groupName,
        "GROUPID": groupId,
        "PARENTGROUPID": parentGroupId,
        "PARENTGROUPNAME": PARENTNAME
    };

    if (action == "update")
    {

        /*
         var index = findIndexByKeyValue(GBL_GRPLISTARR, "GROUPID", groupId);
         
         GBL_GRPLISTARR[index].GROUP_NAME = groupName;
         GBL_GRPLISTARR[index].PK_ITEM_GROUP_ID = groupId;
         GBL_GRPLISTARR[index].FK_PARENT_GRP_ID = parentGroupId;
         */




    }
    else if (action == "delete")
    {
        var index = findIndexByKeyValue(GBL_GRPLISTARR, "GROUPID", groupId);

    }
    else
    { // add
        GBL_GRPLISTARR.push(tmpData);
    }


}


function initializeTreeView(){

    $('#sampleButtons').html('');
    $(function(){
        addSampleButton({
            label: "Add Properties",
            newline: false,
            code: function(){
                var node = $("#tree").fancytree("getActiveNode");
                if( !node ) {
                    alert("Please activate a parent node.");
                    return;
                }
                node.editCreateNode("child", "Node title");
            }
        });
    });
}

var treeNode = []; 
var treeEvent = []; 
var treeData = []; 
var GBL_GRP_ID = "";
var updateFlag = false;
var oldTreeNode = [];
function openVCDialog(context){

    var grpId = $(context).attr('grpId');
    GBL_GRP_ID = $(context).attr('grpId');

    oldTreeNode = [];
    
    // var treeNode = [
    //             {"key": "1", "title": "ba", "folder": true, "children": [
    //                 {"key": "1_1", "title": "Red", "children": [
    //                     {"key": "1_1_1", "title": "Front Red"},
    //                     {"key": "1_1_2", "title": "Rear Red"}
    //                 ]},
    //                 {"key": "1_2", "title": "Black", "children": [
    //                     {"key": "1_2_1", "title": "Front Black"},
    //                     {"key": "1_2_2", "title": "Rear Black"}
    //                 ]}
    //             ]},
    //             {"key": "2", "title": "Nitrogen Cylinder", "extraClasses": "Nitrogen Gas kit" },
    //         ];

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getGrpDetailing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14,54),
        groupId: GBL_GRP_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, openVCDialogDataCallback,"Please Wait... Getting Dashboard Detail");

    function openVCDialogDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            tempTreeNode = data.data;
            oldTreeNode = JSON.parse(JSON.stringify(tempTreeNode));
            var index = findIndexByKeyValue( ITEMGRPJSON , "GROUPID" , grpId );
            var grpTitle = "";
            if( index != "-1" ){

                grpTitle = ITEMGRPJSON[index].GROUPNAME;
                treeNode = [
                    {"key": "seq_1", "title": grpTitle, "expanded": true, "type" : "none", inputType : "none" , "folder": true , "children" : tempTreeNode}
                ];
            } 

            updateFlag = true;
            renderGrpTree();
        } else {
            if (flag){
                var index = findIndexByKeyValue( ITEMGRPJSON , "GROUPID" , grpId );
                var grpTitle = "";
                if( index != "-1" ){

                    grpTitle = ITEMGRPJSON[index].GROUPNAME;
                    treeNode = [
                        {"key": "seq_1", "title": grpTitle, "expanded": true, "type" : "none", inputType : "none" , "folder": true , "children" : ""}
                    ];
                } 

                renderGrpTree();   
                displayAPIErrorMsg( data.status , GBL_ERR_NO_DETAILING );
            }
            else{   
                toastr.error(SERVERERROR);
            }
        }
    }

    function renderGrpTree(){

        $(":ui-fancytree").fancytree("destroy");
        
        $("#tree").fancytree({
            extensions: ["edit"],
            source: treeNode,
            lazyLoad: function(event, data) {
                data.result = { url: "ajax-sub2.json", debugDelay: 1000 };
            },
            renderNode:function(event, data){ 
                //it call when render
            },
            edit: {
                triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"],
                beforeEdit: function(event, data){
                    
                    if( data.node.parent.InputType == "text" && data.node.parent.children.length == "2" ){
                        data.node.remove();
                        toastr.error('You can not add more options in input type text..!');
                        return false; 
                    }
                    if( data.node.getLevel() > 3 ){
                        data.node.remove();
                        // toastr.error('You can not add more options..!');
                        return false;   
                    }
                    // Return false to prevent edit mode
                },
                edit: function(event, data){
                    // Editor was opened (available as data.input)
                },
                beforeClose: function(event, data){
                    // Return false to prevent cancel/save (data.input is available)
                    if( data.originalEvent.type === "mousedown" ) {
                        
                    }
                },
                save: function(event, data){
                    
                    setTimeout(function(){
                        
                        treeEvent = event;
                        treeData = data;

                        console.log(data)
                        if( data.node.getLevel() == "2" ){
                            
                            $('#creatVcModal').modal('hide');
                            $('#propertyTypeTxt').html( data.node.title );     //SET POP UP TITLE ON INPUT
                            $('#typePropModal').modal({
                              backdrop: 'static'
                            });

                        }else{
                            
                            $('#creatVcModal').modal('hide');
                            $('#propertyTypePriceTxt').html( data.node.title );     //SET POP UP TITLE ON INPUT
                            $('#priceOptModal').modal({
                              backdrop: 'static'
                            });
                            $('#optPrice').val( data.node.OptionPrice );
                            $('#optPrice').focus();

                        }
                        return false;
                    }, 100);
                    
                    return true;
                },
                close: function(event, data){
                    // Editor was removed
                    if( data.save ) {
                        // Since we started an async request, mark the node as preliminary
                        $(data.node.span).addClass("pending");
                    }
                }
            }
        });
    }

     setTimeout(function(){
         $('.fancytree-title:last-child').click();   // Focusing Main Group on 1st time 
     },500);

    $('#creatVcModal').modal('show');
}

function setTypeOfProp(){

    event = treeEvent;
    data = treeData;

    generateTreeJson( treeData , treeEvent );

    $('#typePropModal').modal('hide');
    
     setTimeout(function(){
         $('.fancytree-title:last-child').click();   // Focusing Main Group on 1st time 
     },500);

    $('#creatVcModal').modal({
      backdrop: 'static'
    });
}

function setPriceOfProp(){

    event = treeEvent;
    data = treeData;

    var optPrice = $('#optPrice').val();

    if( optPrice == "" ){
        toastr.warning('Please Enter Option Price..!');
        $('#optPrice').val('');
        return false;
    }
    data.node.OptionPrice = optPrice;

    generateTreeJson( treeData , treeEvent );

    $('#priceOptModal').modal('hide');
    
     setTimeout(function(){
         $('.fancytree-title:last-child').click();   // Focusing Main Group on 1st time 
     },500);

    $('#creatVcModal').modal({backdrop: 'static', keyboard: false});
}

function generateTreeJson( data , event ){
    
    var inputType = "text";
    if( $('#list1').is(':checked') ){
        inputType = "list";
    }

    $(data.node.span).removeClass("pending");
    // Let's pretend the server returned a slightly modified
    // title:
    data.node.setTitle(data.node.title);

    // console.log(data);
    var parentNode = data.node.parent.children;
    data.node.InputType = inputType;
    var tempNode = [];
    
    var parentKey = data.node.parent.key;

    var firstIndex = findIndexByKeyValue( treeNode[0].children , "key" , parentKey );
     parentNode.forEach( function( record , index ){
        if( record.children != null ){
            var innrTempNode = [];
            record.children.forEach( function( innRecord , innrIndex ){
                innrTempNode.push({"key": innRecord.key, "inputType": innRecord.InputType , "price": innRecord.OptionPrice , "title": innRecord .title, "children" : "" })
            });
            tempNode.push( {"key": record.key, "inputType": record.InputType , "price": record.OptionPrice , "title": record.title, "children" : innrTempNode } );
        }else{
            tempNode.push( {"key": record.key, "inputType": record.InputType , "price": record.OptionPrice , "title": record.title, "children" : ""} );
        }
    });
     
    if( firstIndex != "-1" ){
        treeNode[0].children[firstIndex].children = tempNode;
    }else{
        treeNode[0].children = tempNode
    }

    oldTreeNode.forEach( function( record , index ){
        if( treeNode[0].children[index].inputType == undefined ){
            treeNode[0].children[index].inputType = record.inputType;
        }
        record.children.forEach( function( innRecord , innrIndex ){
            var keyIndex = findIndexByKeyValue( treeNode[0].children[index].children , "key" , innRecord.key );
            if( keyIndex != "-1" ){
                if( treeNode[0].children[index].children[keyIndex].price == undefined ){
                    treeNode[0].children[index].children[keyIndex].price = innRecord.price;
                    treeNode[0].children[index].children[keyIndex].inputType = innRecord.inputType;
                }
            }
        });
    });

}

function saveDetailing(){

    var allow = true;
    treeNode[0].children.forEach( function( record , index ){
        // record.children.forEach( function( innRecord , innrIndex ){
            if( record == undefined || record == "" || record == null || record.children == "" || record.children == undefined || record.children == null ){
                allow = false;
            }
        // })
    });

    if( !allow ){
        toastr.error('Please Enter atleast one option of all Properties');
        return false;
    }

    var reqCase = "saveDetailing";
    var chckauth = checkAuth(4,13);
    if( updateFlag ){
        reqCase = "updateDetailing";
        chckauth = checkAuth(4,15);
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: reqCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: chckauth,
        groupId: GBL_GRP_ID,
        detailing: treeNode,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, saveDetailingDataCallback,"Please Wait... Getting Dashboard Detail");

    function saveDetailingDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
            $('#creatVcModal').modal('hide');
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_SAVE_UPDATE_DETAILING );
            else
                toastr.error(SERVERERROR);
        }

    }
}

function setKeyEventForVc(){
    
    $(document).keyup(function (e) {
        if (e.which == 13) {
//            if( $('#creatVcModal').hasClass('in') ){
//                $('.sampleButtonContainer button').click();
//            }else if( $('#typePropModal').hasClass('in') ){
//                setTypeOfProp();
//            }else if( $('#priceOptModal').hasClass('in') ){
//                setPriceOfProp();
//            }
        }else if(e.which == 9 && $('#typePropModal').hasClass('in')){
//            if( $('#list1').prop('checked') ){
//                $('#list1').prop('checked',false);
//                $('#text2').prop('checked',true);
//            }else{
//                $('#list1').prop('checked',true);
//                $('#text2').prop('checked',false);
//            }
        }
    });
}