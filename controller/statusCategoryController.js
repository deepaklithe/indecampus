/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 11-08-2016.
 * File : statusCategoryController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var statusData = [];
var catVal = '';
var GBL_STAT_ID = "";

function getStatusCategoryData(){

    $('#status').focus();

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getCategoryForStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(10,41),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(FOLLOWUPURL, postData, getStatusCategoryDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getStatusCategoryDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        statusData = data.data;
        renderStatusData();

    } else {
        statusData = [];
        renderStatusData();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_STATUS_CAT );
        else
            toastr.error(SERVERERROR);
    }
}

function renderStatusData(){

    var newRow = '';
    var count = 1 ;

    if(statusData != 'No record found'){

        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Status Name</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        statusData.forEach( function( record , index ){

            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.lookupValue +'</td>'+
                            '<td><a href="javascript:void(0)" statId="'+ record.lookupId +'" statusname="'+ record.lookupValue +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" onclick="updateStatus(this)"><i class="fa fa-pencil"></i></a></td>'+
                            // '<a href="javascript:void(0)" statId="'+ record.lookupId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="deleteStatusType(this)"><i class="fa fa-trash-o"></i></a></td>'+
                        '</tr>';
        });

        $('#dtTable').html(thead + newRow + tfoot);
     
     }else{
        toastr.warning('No record found');
    }

    TablesDataTables.init();
    TablesDataTablesEditor.init();
    
    addFocusId( 'status' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

}

var gblContext = '';
function createStatusType(){

    if( checkAuth(10,40, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var status = $('#status').val();

    if( status == ""){
        toastr.warning('Please Enter Status');
        $('#status').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        if( GBL_STAT_ID != "" ){
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'updateStatusCategory',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(10,42),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                statusName : status,
                statusId : GBL_STAT_ID
            }
            commonAjax(FOLLOWUPURL, postData, updateStatusCategoryCallback,"");

        }else{
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'createCategoryForStatus',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(10,40),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                statusName : status
            }
            commonAjax(FOLLOWUPURL, postData, createStatusCategoryCallback,"");
        }
    }

    function createStatusCategoryCallback(flag,data){

        if (data.status == "Success" && flag) {

            toastr.success('New Status Category has been Created');
            statusData.push(data.data);

            renderStatusData();       
            resetData();

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ADD_STATUS_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }

    function updateStatusCategoryCallback(flag,data){

        if (data.status == "Success" && flag) {

            toastr.success('Your Status Category has been Updated');
            
            var index = findIndexByKeyValue( statusData , "lookupId" , GBL_STAT_ID );
            if( index != "-1" ){
                statusData = data.data;
                renderStatusData();     
                resetData();
            }

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_STATUS_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

var gblContext = "";
function deleteStatusType(context){
    
    if( checkAuth(10,42, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        var statusId = $(context).attr('statusId');
        GBL_STAT_ID = statusId;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteWorkFlow',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(27,115),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID), 
            workFlowId : statusId
        }

        commonAjax(FOLLOWUPURL, postData, deletestatusCallback,"Please Wait... Getting Module Detail");
    }
}


function deletestatusCallback(flag,data){

    if (data.status == "Success" && flag) {

        var index = findIndexByKeyValue( statusData , "lookupId" , GBL_STAT_ID );
        if( index != "-1" ){
            statusData.splice( index , 1 );
            renderStatusData();
        }

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_WORKFLOW );
        else
            toastr.error(SERVERERROR);
    }
}


function updateStatus(context){

    if( checkAuth(10,42, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    GBL_STAT_ID = $(context).attr('statId');
    var statusname = $(context).attr('statusname');
    $('#status').val(statusname);
    addFocusId( 'status' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function resetData(){
    $('#status').val("");
    GBL_STAT_ID = "";
}