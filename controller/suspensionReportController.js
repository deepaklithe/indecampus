    /*
 *
 * Created By :  Kavita Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 08-08-2018.
 * File : suspensionReportController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';
var GBL_REPORT_DATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    autoCompleteForStudentId('studentName',saveStudent);
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
    getSuspensionReportList(); // GET EVENTS LISTING DATA
   
}
// GET EVENTS LISTING DATA
function getSuspensionReportList(){

    var studentId = $('#studentName').attr('studId');
    var monthFrom = $('#monthFrom').val();
    var monthTo = $('#monthTo').val();
    
    if( monthFrom != "" && monthTo != "" ){

        var stMonth = monthFrom.split('-').reverse().join('-');
        var enMonth = monthTo.split('-').reverse().join('-');
        var dateStartDate = new Date(ddmmyyToMysql(stMonth));
        var dateEndDate = new Date(ddmmyyToMysql(enMonth));

        //START END DATE VALIDATION
        if (+dateEndDate < +dateStartDate) { 
            toastr.warning("End month cannot be less than the Start month");
            addFocusId('monthTo');
            return false;

        }
    }
    if( $('#studentName').val() == "" ){
        studentId = "";
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'suspensionReport', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(26,105),
    	studentId : studentId,
    	fromMonth : monthFrom,
    	toMonth : monthTo,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestListCallback,"Please wait...Adding your item.");

    function getRequestListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_REPORT_DATA = data.data;
            renderSuspensionReportList(GBL_REPORT_DATA); 
            $('.Sidenav').removeClass('open');          

        }else{

            GBL_REPORT_DATA = [];
            GBL_EXCELJSON = [];
            renderSuspensionReportList(GBL_REPORT_DATA); 
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_FILTER );
            else
                toastr.error(SERVERERROR);
        }
        // resetFilters();
    }

}

// RENDER ITEM LISTING DATA
function renderSuspensionReportList(suspensionData){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="enrollmentViewRepoTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Student ID</th>'+
                        '<th>Student Name</th>'+
                        '<th>Academic Year</th>'+
                        '<th>From Date</th>'+
                        '<th>To Date</th>'+
                        '<th>No Of Days</th>'+
            			'<th>Reason</th>'+
            			'<th>Revocation Date</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var tempJson = {
        srNo : "#",
        studMemId : "Student ID",
        studName : "Student Name",
        academicYear : "Academic Year",
        fromDate : "From Date",
        toDate : "To Date",
        noOfDays : "No Of Days",
        reason : "Reason",
        revocationDate : "Revocation Date",
    }
    GBL_CSVJSON.push(tempJson);

    var innerRow = '';
    if( suspensionData.length != 0 && suspensionData != "No record found" ){

        suspensionData.forEach(function( record , index ){
            
            var tempJson = {
                srNo : (index+1),
                studMemId : record.studMembershipNo,
                studName : record.studentName,
                academicYear : record.acdemicYear,
                fromDate : (record.fromDate != "0000-00-00" && record.fromDate != "1970-01-01" && record.fromDate != "" ? mysqltoDesiredFormat(record.fromDate,'dd-MM-yyyy') : "" ),
                toDate : (record.toDate != "0000-00-00" && record.toDate != "1970-01-01" && record.toDate != "" ? mysqltoDesiredFormat(record.toDate,'dd-MM-yyyy') : "" ),
                noOfDays : record.noOfDays,
                reason : record.reason,
                revocationDate : (record.revocationDate != "0000-00-00" && record.revocationDate != "1970-01-01" && record.revocationDate != "" ? mysqltoDesiredFormat(record.revocationDate,'dd-MM-yyyy') : "" ),
            }
            GBL_CSVJSON.push(tempJson);

            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.studMembershipNo +'</td>'+
                            '<td>'+ record.studentName +'</td>'+
                            '<td>'+ record.acdemicYear +'</td>'+
                            '<td>'+ (record.fromDate != "0000-00-00" && record.fromDate != "1970-01-01" && record.fromDate != "" ? mysqltoDesiredFormat(record.fromDate,'dd-MM-yyyy') : "" ) +'</td>'+
			                '<td>'+ (record.toDate != "0000-00-00" && record.toDate != "1970-01-01" && record.toDate != "" ? mysqltoDesiredFormat(record.toDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ record.noOfDays +'</td>'+
                            '<td>'+ record.reason +'</td>'+
			                '<td>'+ (record.revocationDate != "0000-00-00" && record.revocationDate != "1970-01-01" && record.revocationDate != "" ? mysqltoDesiredFormat(record.revocationDate,'dd-MM-yyyy') : "" ) +'</td>'+
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    GBL_EXCELJSON =  thead + innerRow + tEnd;
    $('#enrollmentViewRepoTbl').DataTable();

}

// RESET FILTER
function resetFilters(){

    $('#studentName').attr('studId','');
    $('#studentName').val('');
    $('#monthFrom').val('');
    $('#monthTo').val('');
    $("#monthFrom,#monthTo").datepicker('remove');
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
   
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderSuspensionReportList(GBL_REPORT_DATA); // RENDER ORIGINAL WITHOUT FILTER DATA

}


function saveStudent(){

}

function setMonthTo(context){

    var monthFrom = $(context).val();

    $('#monthTo').datepicker('remove');
    $('#monthTo').datepicker({
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        startDate: monthFrom,
        autoclose:true
    });

}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Suspension Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Suspension_Report.pdf",20);
    }

}