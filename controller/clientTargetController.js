/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : developerTypeController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var targetData = [];
var itemGroupJson = [];
var ITEMGRPJSON = [];

function getBranchListing(){

    $('#storeName').focus();

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var branchData = roleAuth.data.branchListing;

    var brListOpt = '<option value="-1"> Select Branch </option>';
    for( var i=0;i<branchData.length; i++ ){

        if( localStorage.indeCampusBranchIdForChkAuth == branchData[i].orgId ){
            brListOpt += '<option value="'+ branchData[i].orgId +'">'+ branchData[i].orgName +'</option>'
        }
    }
    $('#storeName').html( brListOpt );
    $('#storeName').val( localStorage.indeCampusBranchIdForChkAuth );
    $('#storeName').selectpicker();

    getGroupList();
    setNext10Years(); //
}

function setNext10Years(){
    var date = new Date();
    var currYear = date.getFullYear();

    var next10Years = [currYear - 2 , currYear - 1 ];
    for( var i=0; i<10; i++ ){
        next10Years.push( currYear + i );
    }
    // console.log(next10Years);
    
    var yearList = '';
    for( var j=0; j<next10Years.length; j++ ){
        if( currYear == next10Years[j] ){
            yearList += '<option value="'+ next10Years[j] +'" selected> '+ next10Years[j] +' </option>';
        }else{ 
            yearList += '<option value="'+ next10Years[j] +'"> '+ next10Years[j] +' </option>';
        }
    }
    $('#tarYear').html( yearList );
    $('#tarYear').selectpicker();

    var date = new Date();
    var currMon = date.getMonth() + 1;

    $('#tarMonth').val( currMon );
    $('#tarMonth').selectpicker( 'refresh' );
}

function getListingData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getClientTraget',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(6,22),
        type: "1",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){

            targetData = data.data ;
            renderTargetListing();
        
        }

    } else {
        renderTargetListing();
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
        else
            toastr.error(SERVERERROR);
    }
}

function renderTargetListing(){

    addFocusId( 'storeName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    var startHtml = "<table id='targetListTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th>Branch Name.</th>"+
                            "<th>Year</th>"+
                            "<th>Month</th>"+
                            "<th>Target Type</th>"+
                            "<th>Group</th>"+
                            "<th>Amount</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";
    var inqRow = '';
    for( var i=0; i<targetData.length; i++ ){

        var branchName = $('#storeName option[value="'+ targetData[i].branchId +'"]').text();
        var monthName = $('#tarMonth option[value="'+ targetData[i].monthId +'"]').text();
        var targetType = "";

        var btnColor = 'btn-green'
        if( targetData[i].setButtonColor == "Y" ){
            btnColor = 'btn-amber'
        }else if(targetData[i].setButtonColor == "GR"){
            btnColor = 'btn-gray'
        }

        if( targetData[i].monthId == "" ){
            monthName = "";
        }

        if( targetData[i].salesTargetType != undefined ){
            if( targetData[i].salesTargetType == "1" ){
                targetType = "Amount";
            }else if( targetData[i].salesTargetType == "2" ) {
                targetType = "Qty";
            }
        }
        var firstTarget = "";
        if( i == 0 ){ firstTarget = "firstTarget"; }

        inqRow += '<tr>'+
                    '<td>'+ (i+1) +'</td>'+
                    '<td>'+ branchName +'</td>'+
                    '<td>'+ targetData[i].year +'</td>'+
                    '<td>'+ monthName +'</td>'+
                    '<td>'+ targetType +'</td>'+
                    '<td>'+ targetData[i].itemGroupName +'</td>'+
                    '<td class="amtDisp">'+ numberFormat( targetData[i].targetAmt ) +'</td>'+
                    '<td>'+
                        '<a href="javascript:void(0)" class="btn btn-right-mrg btn-sm '+ btnColor +'" targetId="'+ targetData[i].targetId +'" id="'+ firstTarget +'" onclick="navigateToSetTarget(this)">Set</a>'+
                        '<a href="javascript:void(0)" class="btn btn-right-mrg btn-primary btn-sm" targetId="'+ targetData[i].targetId +'" onclick="editTarget(this)"><i class="fa fa-pencil"></i></a>'+
                        '<a href="javascript:void(0)" class="btn btn-danger btn-sm" targetId="'+ targetData[i].targetId +'" onclick="deleteTarget(this)"><i class="fa fa-trash"></i></a>'+
                    '</td>'+
                '</tr>';
                                                
    }
    var finalHtml = startHtml + inqRow + endHtml;
    $('#TargetTable').html(finalHtml);
    $('#targetListTable').DataTable({ "stateSave": true });
    
}

function createTarget(){


    var storeName = $('#storeName').val();
    var tarYear = $('#tarYear').val();
    var tarMonth = $('#tarMonth').val();
    var groupId = $("#groupListing").val();
    // var groupName = $('#groupName').val();
    var groupName = $("#groupListing option:selected").text();
    var Amount = $('#Amount').val();
    var salesTargetType = $('#salesTargetType').val();

    var updateTargetId = localStorage.POTGupdateTargetId;

    var groupIdCs = "";
    if( groupId ){
        for( var j=0; j<groupId.length; j++ ){
            if( groupIdCs ){
                groupIdCs += ','+groupId[j];
            }else{
                groupIdCs = groupId[j];
            }
        }
    }

    if( storeName == "-1"){
        toastr.warning('Please Select Branch');
        $('#storeName').focus();
        return false;

    }else if( Amount == ""){
        toastr.warning('Please Enter Amount');
        $('#Amount').focus();
        return false;

    }else {
        
        if( localStorage.POTGupdateTargetId != "" && localStorage.POTGupdateTargetId != undefined ){
           if( checkAuth(6, 23, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'updateClientTraget',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(6,23),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                selectedBranchId: storeName,
                year: tarYear,
                month: tarMonth,
                groupId: groupIdCs,
                // groupId: groupId,
                amt: Amount,
                targetId: localStorage.POTGupdateTargetId,
                type: "1",
            }
            localStorage.removeItem( 'POTGupdateTargetId' );
            commonAjax(FOLLOWUPURL, postData, updateTargetDataCallback,"Please Wait... Getting Dashboard Detail");

        }else{
          if( checkAuth(6, 21, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = { 
                requestCase: 'createClientTraget',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(6,21),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                selectedBranchId: storeName,
                year: tarYear,
                month: tarMonth,
                groupId: groupIdCs,
                // groupId: groupId,
                amt: Amount,
                type: "1",
                salesTargetType: salesTargetType, // 1 = amount 2 = qty
            }

            commonAjax(FOLLOWUPURL, postData, sendTargetDataCallback,"Please Wait... Getting Dashboard Detail");
        }


        function sendTargetDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                console.log(data);

                if(data.data != 'No record found'){

                    console.log(data.data);

                    targetData.push( data.data[0] );
                    resetData();    
                    renderTargetListing();

                    toastr.success('Your Target is Created');

                }else{
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
                }

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_INSERT_TARGET_FAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }

        function updateTargetDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                console.log(data);

                if(data.data != 'No record found'){

                    console.log(data.data);

                    var index = findIndexByKeyValue( targetData , 'targetId', updateTargetId );

                    var tempJson = {
                        branchId: storeName,
                        clientId: targetData[index].clientId,
                        itemGroupId: groupIdCs,
                        itemGroupName: groupName,
                        monthId: tarMonth,
                        targetAmt: Amount,
                        targetId: targetData[index].targetId,
                        setButtonColor: targetData[index].setButtonColor,
                        year: tarYear,
                        salesTargetType: salesTargetType,
                        transDetail: targetData[index].transDetail,
                        type: "1",
                    }

                    targetData[index] = tempJson;
                    // targetData[index] = data.data[0] ;
                    resetData();
                    renderTargetListing();

                    toastr.success('Your Target is Updated');

                }else{
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
                }

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_TARGET_FAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }
    } 
}


function editTarget(context){

    if( checkAuth(6, 23, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    var targetId = $(context).attr('targetId');
    localStorage.POTGupdateTargetId = targetId;

    var index = findIndexByKeyValue( targetData, 'targetId' , targetId );

    if( targetData[index].itemGroupId != undefined ){
        $.each(targetData[index].itemGroupId.split(","), function(i,e){
            $("#groupListing option[value='" + e + "']").prop("selected", true);
        });
    }
    $("#groupListing").selectpicker('refresh');

    // $('#groupName').val(targetData[index].itemGroupName);
    // $('#groupName').attr('groupId',targetData[index].itemGroupId);
    $('#Amount').val(targetData[index].targetAmt);
    $('#storeName').val(targetData[index].branchId);
    $('#tarYear').val(targetData[index].year);
    $('#tarMonth').val(targetData[index].monthId);
    $('#salesTargetType').val(targetData[index].salesTargetType);
    $('#storeName').selectpicker('refresh');
    $('#tarYear').selectpicker('refresh');
    $('#tarMonth').selectpicker('refresh');   
    $('#salesTargetType').selectpicker('refresh');   

    $('.editMode').attr('disabled',true).selectpicker('refresh');
    addFocusId( 'storeName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function deleteTarget(context){

    if( checkAuth(6, 24, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var targetId = $(context).attr('targetId');
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteClientTraget',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(6,24),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            targetId: targetId,
            type: "1",
        }

        commonAjax(FOLLOWUPURL, postData, deleteTargetDataCallback,"Please Wait... Getting Dashboard Detail");

        function deleteTargetDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                var index = findIndexByKeyValue( targetData , 'targetId', targetId );

                targetData.splice(index,1);
                renderTargetListing();

                toastr.success('Your Target is Deleted');

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DELETE_TARGET_FAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

function resetData(){

    $('#groupName').val('');
    $('#Amount').val('');
    // $('#tarMonth').val('');
    $('#groupListing').val('').selectpicker('refresh');
    // $('#storeName').val('-1');
    // $('#storeName').selectpicker('refresh');
    $('#errorHideDiv').hide();
    localStorage.removeItem('POTGupdateTargetId');
    $('.editMode').removeAttr('disabled',true).selectpicker('refresh');
}

function autoCompleteForGroup(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#groupName').attr('groupId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getGroupListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(13, 50),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMSGROUP= [];
                if (data.status != "No Item Group Found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.PK_ITEM_GROUP_ID,
                            name: data.GROUP_NAME,
                            data: data.GROUP_NAME,
                            clientId: data.FK_CLIENT_ID
                        }
                        itemGroupJson.push(tempJson);
                        ITEMSGROUP.push(tempJson);
                    });

                    return ITEMSGROUP;
                }
                console.log(data);
            }
        }
    });
}

function saveGroupDetail(){

}

function navigateToSetTarget(context){

     if( checkAuth(5, 19, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
        
    var targetId = $(context).attr('targetId');
    var index = findIndexByKeyValue( targetData , 'targetId' , targetId );

    if( index != '-1' ){

        var branchName = $('#storeName option[value="'+ targetData[index].branchId +'"]').text();
        var monthName = $('#tarMonth option[value="'+ targetData[index].monthId +'"]').text();

        var targetDetails = targetData[index];

        targetDetails.branchName = branchName;
        targetDetails.monthName = monthName;

        localStorage.POTGuserTargetDetail = JSON.stringify( targetDetails );

        window.location.href = "targetDist.html";
    }

}

function openUploadDialog() {

   $('#uploadSalesTargetModal').modal('show');
}

function uploadSalesTarget() {

    var uploadFileName = $("#attachmentName").val();
    if(!uploadFileName){
        toastr.warning('Please select file for upload sales target.');
        $('#attachmentName').click();
        return false;
    }
    $("#addDocument").submit();
}

function assignAttachmentEvent(){
    
    $("#addDocument").off().on('submit', (function (e) {
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(6,21),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            uploadType : "salesTarget",
        };
        $("#postDataActUpload").val(JSON.stringify(temp_json));
        var bfsendMsg = "Sending data to server..";

        addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                
                if(data.status == "Success"){
                    
                    console.log(data.data);

                    refreshFile();
                    $('#errorHideDiv').hide();
                    $('#errorDiv').html("");
                    toastr.warning( data.data );
                }else{
                    toastr.warning(data.status);
                    refreshFile();
                    GBL_ERROR = data.data;
                    renderError(GBL_ERROR);
                    
                }
                addRemoveLoader(0);
                $('#uploadSalesTargetModal').modal('hide');
                // $("body").addClass("loaded");
                // $("#loader-wrapper").hide();
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                // $("body").addClass("loaded");
                toastr.error(errdata);
                if( jqXHR.responseText != "" ){
                    GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                    renderError(GBL_ERROR);
                }
                refreshFile();
            }
        });
    }));
}

function refreshFile(){

    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

function downloadSalesSample() {
    

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(13, 50),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, downloadSampleDataCallback,"Please Wait... Getting Dashboard Detail");

    function downloadSampleDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            ITEMGRPJSON = data.data;

            var GBL_GROUPJSON = [];
            var tempGrpJson = {
                groupId : "Group Id",
                groupName : "Group Name",
                parentGroupId : "Parent Group Id",
                parentGroupName : "Parent Group Name",
            }
            GBL_GROUPJSON.push( tempGrpJson );

            ITEMGRPJSON.forEach(function( record , index ){
                var tempGrpJson = {
                    groupId : record.PK_ITEM_GROUP_ID,
                    groupName : record.GROUP_NAME,
                    parentGroupId : record.FK_PARENT_GRP_ID,
                    parentGroupName : record.PARENTGROUPNAME,
                }
                GBL_GROUPJSON.push( tempGrpJson );
            });

            csvSave(GBL_GROUPJSON, "groupSample.csv");


            var GBL_SAMPLEJSON = [];
            var tempSampleJson = {
                year : "Year",
                month : "Month",
                group : "GroupId",
                amount : "Amount",
                userName : "UserName",
            }
            GBL_SAMPLEJSON.push( tempSampleJson );

            csvSave(GBL_SAMPLEJSON, "salesTargetSample.csv");

        } else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function getGroupList(){

    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(13,50),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(COMMONURL, postData, callBackGetGroupList, "Please Wait... Getting the group listing");

    function callBackGetGroupList(flag, data) {
         
        getListingData() //Getting Render List of target data
        if (data.status == SCS && flag) {
            
            var groupList = '';
            for( var i=0; i<data.data.length; i++ ){
                groupList += '<option value="'+ data.data[i].PK_ITEM_GROUP_ID +'">'+ data.data[i].GROUP_NAME +'</option>' 
            }

            $('#groupListing').html(groupList);
            $('#groupListing').selectpicker('refresh');

        }else {
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NO_GROUP );            
            }else{
                toastr.error(SERVERERROR);
            }    
        }
    }

}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 09-MAY-2018 BY VISHAKHA TAK
function initSelfTour(){

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: "#TargetTable",
            title: "<b>Sales Target </b>",
            content: "Available Sales Target listing and actions performed on it !!",
            placement: 'top'
        },
        {
            path: "",
            element: ".storeNameDiv",
            title: "<b>Branch Name</b>",
            content: "Select branch name and other details to set sales target!!",
            placement: 'right'
        },
        {
            path: "",
            element: '#Amount',
            title: "<b>Target amount</b>",
            content: "Add amount of target for selected branch !!",
            placement: 'left',
        },
        {
            path: "",
            element: "#createTargetBtn",
            title: "<b>Save target details</b>",
            content: "Save target details",
            placement: 'right',
        },
        {
            path: "",
            element: "#downloadTargetBtn",
            title: "<b>Download target sample </b>",
            content: "You can also download target sample files",
            placement: 'left',     
        },
        {
            path: "",
            element: "#uploadActBtn",
            title: "<b>Upload target </b>",
            content: "You can also upload target csv file",
            placement: 'left',
            onNext:function (tour){
                openUploadDialog();
                var deferred = $.Deferred();
                $('#uploadSalesTargetModal').on('shown.bs.modal', function () {
                    deferred.resolve();
                });
                return deferred.promise();
            }     
        },
        {
            path: "",
            element: '#addDocument',
            title: "<b>Upload target file</b>",
            content: "Upload sales target file from here!",
            placement: 'top', 
            reflex: true,
            onPrev: function(tour){
                $('#uploadSalesTargetModal').modal('hide');
            }, 
            onNext: function(tour){
                $('#uploadSalesTargetModal').modal('hide');
            },
        },
        {
            path: "",
            element: "#TargetTable",
            title: "<b>Target Set </b>",
            content: "Set Target Details Data !!",
            placement: 'top',
            onPrev: function(tour){
                $('#uploadSalesTargetModal').modal('show');
            }, 
            onNext: function(tour){
                if( targetData == "" ){
                    tour.end();
                }else{
                    $('#firstTarget').click();
                }
            },
        },
        {
            path: "",
            element: '#targetListTable',
            title: "<b>Target Distribution</b>",
            content: "Listing of target distributed according to employee <br/> Which you can add or edit!",
            placement: 'top',
            onNext: function(tour){
                setInterval(function () {
                    // $('#createTargetOnUserBtn').click();
                    window.location.href = "clienttarget.html";
                    tour.end();
                },1);
            }
        },
        {
            path: "",
            element: '#createTargetOnUserBtn',
            title: "<b>Save Target</b>",
            content: "Sales Target!",
            placement: 'right',   
            onShown: function(tour){
                // setInterval(function () {
                    $('#createTargetOnUserBtn').click();
                    // tour.end();
                // },3000);
            }
        },
    ];
   
    // Instance the tour
    instanceTour();
    tour._options.name = "Sales Target Tour";
    // tour._options.debug = true;

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM THIS STEP TO PREVENT FROM DISPLAYING LAST STORED STEP

    // Start the tour
    tour.start(true);
    localStorage.POTGtargetTour = 1;
    localStorage.GBL_TOUR_STEPS = JSON.stringify(GBL_TOUR_STEPS);
}