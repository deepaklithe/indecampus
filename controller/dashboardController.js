/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : dashboardController.
 * File Type : .js.
 * Project : POTG
 *
 * */



var startDate = new Date().format('dd-MM-yyyy');
var endDate = new Date().format('dd-MM-yyyy');

var dashboardData = [];
var inqData = [];

function getDashboardData(){

    // if (checkAuth(1, 2) == -1) {
    //     alert(NOAUTH);
    //     navigateToIndex();
    // }
    // else {

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'followupDashboard',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            requestedUserId : tmp_json.data[0].PK_USER_ID,
            startDate : new Date().format('yyyy-MM-dd'),
            endDate : new Date().format('yyyy-MM-dd'),
            orgId: checkAuth(1,2),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }

        commonAjax(FOLLOWUPURL, postData, dashboardDataCallback,"Please Wait... Getting Dashboard Detail");
    // }
}


function dashboardDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        dashboardData = data.data;
        console.log(data.data);

        // return false;
//        $('#todayFollowDashboard').html(data.data.TodaysFollowup);
//      $('#delayedFollowDashboard').html(data.data.DelayedFollowup);
        $('#todaysActivities').html(data.data.pendingActivities);
        $('#upcomingActivities').html(data.data.upcomingActivities);
        $('#delayedAct').html(data.data.DelayedActivities);
        // $('#inprocessActivities').html(data.data.inprocessActivities);
        $('#openInqDashboard').html(data.data.open_inq_count);
        $('#target').html(data.data.target);
        $('#achievement').html(data.data.achievement);
        $('#pipelineSize').html(data.data.pipelineSize);
        $('#totalCustomer').html(data.data.customerCount);
        $('#achieveLastYear').html(data.data.achievementLstYear);

        localStorage.setItem('POTGTotalInqCount',data.data.totalInqCount);
        //console.log(localStorage.POTGTotalInqCount);
        $('#notifActivityCount').html(data.data.totalInqCount);

        var localStatus = JSON.parse(localStorage.indeCampusRoleAuth);
        var index = findIndexByKeyValue(localStatus['data']['statusListing'],'statusCatName','INQUIRY');
        if( index != "-1" ){

            var innerIndex = findIndexByKeyValue(localStatus['data']['statusListing'][index]['statusListing'],'statusPriority','1');
            if( innerIndex != "-1" ){
                $('#statusId').attr("statusId",localStatus['data']['statusListing'][index]['statusListing'][innerIndex]['ID']);
            }
        }

        // chartInit(data.data.weeklyReportbarChart);

        var totalBook = data.data.inq_ConvertRatioUser_Donet.totalBooking;
        var totalInq = data.data.inq_ConvertRatioUser_Donet.totalInq;

        var bookPerc = ( parseInt( totalBook ) / parseInt( totalInq ) ) * 100;

        bookPerc = bookPerc.toFixed(2);

        $('.knob').val(bookPerc).trigger('change');

        eventListDashboard(data);
        renderCustStats();

        // DONUT CHART DATA CREATION
        var donData = data.data.inq_ConvertRatioUser_Donet;
        var donutData = [];

        for( var d=0; d<donData.length; d++ ){
            var temp = [];
                temp.push(donData[d].statusName);
                temp.push( parseInt(donData[d].statusCount) );
                donutData.push( temp );
        }

        var geoData = data.data.salesSpread;
        var geoDataArr = [];
        // geoData = "No record found";
        if( geoData != "No record found" ){
            var geoHeader = ['City', 'Sales', 'Customers'];
            geoDataArr.push( geoHeader );
            for(var g=0;g<geoData.length;g++){
                var tempArr = [geoData[g]['cityName'],parseInt(geoData[g]['totalSales']),parseInt(geoData[g]['customerCount'])];
                geoDataArr.push( tempArr );
            }
        }else{
            var tempArr = [
                            ['', { role: 'annotation' }],
                            ['', '']
                          ];
            geoDataArr.push( tempArr );
        }
        
        donutChart( donutData );
        geoChart(geoDataArr); //Get Geo chart
        // getinquiryListing();
    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_DASHBOARD_PROP );
        else
            toastr.error(SERVERERROR);
    }
}

function getinquiryListing(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryListCallback(flag,data) {
    
    if( data.status == "Success" && flag ){

    inqData = data.data;
    // console.log(inqData);
    renderSleepingData( inqData );

    }else {   

        if (flag){
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ );
        }else{
            toastr.error(SERVERERROR);
        }            
    }
}

function renderSleepingData( data ) {
    
    var newRow = '';
    var count = 0;
    var roleAuthData = JSON.parse(localStorage.indeCampusRoleAuth);
    var statList = roleAuthData.data.statusListing;
    var index = findIndexByKeyValue( statList,'statusCatName','INQUIRY');
    var confirmId = "";
    var cancelId = "";
    if( index != "-1" ){
        var inqStat = statList[index].statusListing;
        var thrdPrtyInd = findIndexByKeyValue(inqStat,'thirdPartyFlag','1');
        if( thrdPrtyInd != "-1" ){
            confirmId = statList[index].statusListing[thrdPrtyInd].ID;
        }

        var cnclRemarkInd = findIndexByKeyValue(inqStat,'remarkOnStatusFlag','1');
        if( cnclRemarkInd != "-1" ){
            cancelId = statList[index].statusListing[cnclRemarkInd].ID;
        }
        
    }

    var thead = '<table id="SleepingTable" class="table table-bordered table-striped table-condensed cf">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Inq No</th>'+
                            '<th>Name</th>'+
                            '<th>Contact Number</th>'+
                            '<th>Inq Name</th>'+
                            '<th>Status</th>'+
                            '<th>Amount</th>'+
                    '</thead>'+
                    '<tbody id="sleepingDealBody">';

    var tfoot = '</tbody></table>'; 

    if( data != "No record found" ){

        data.forEach( function( record , index ){
            
            if(record.actDetail ==  "No record found" || record.actDetail.COLORCODE == "RED"){

                if( (record.statusId != confirmId) && (record.statusId != cancelId) ){

                    count = count + 1;
                    newRow += '<tr>'+
                             '<td>'+ count +'</td>'+
                             '<td><a href="javascript:void(0)" onclick="navigateToInquiryDetail(this)" id="'+record.inqId+'" custid="'+record.custId+'" inqid="'+record.inqId+'">'+ record.clientLevelInqId +'</a></td>'+
                             '<td>'+ record.custDetail.fName+' '+ record.custDetail.lName +'</td>'+
                             '<td>'+ record.custDetail.custMobile +'</td>'+
                             '<td>'+ record.projectName +'</td>'+
                             '<td>'+ record.statusName +'</td>'+
                             '<td class="amtDisp">&#8377; '+ numberFormat(record.potAmt) +'</td>'+
                          '</tr>'; 
                }
            }
        });

    }else{

    }
    $('#sleepingDealDiv').html(thead + newRow + tfoot);
    $('#SleepingTable').DataTable({ "stateSave": true });
}

function eventListDashboard(data){

    var noDataFound = '<div class="task *class* last">'+
        '<div class="desc" style="width:100%;">'+
        '<h4 style="text-align:center;" id="NoFlwp">No Followup Available</h4>'+
        '</div>'+
        '</div></div>';

        var eventListing = data.data.eventListing;
        var todayFlwp = ''
        if(eventListing == "No record found" ){
            if( $('#NoFlwp').html() == ''  ){

                todayFlwp = noDataFound;
            }
        }else{

            if( eventListing != "No record found" ){
                var headerUL = "<ul id='eventListDashUL'>";
                var middelLi = "";
                for(var i=0;i<eventListing.length;i++){

                    var sdate = new Date(eventListing[i].EVENT_START_DATE).format("dd-MM-yyyy");
                    var edate = new Date(eventListing[i].EVENT_END_DATE).format("dd-MM-yyyy");
                    var tmp_date1 = new Date(eventListing[i].INQ_DATE);


                    var month1 = months[ tmp_date1.getMonth() ];
                    var date1 = tmp_date1.getDate();
                    var year1 = tmp_date1.getFullYear();

                    var statusColor = '';
                    if( eventListing[i].inqStatus == "BOOKED"){
                        statusColor = "bookStatus";
                    }

                    middelLi += '<li>'+
                                '<div class="task low">'+
                                    '<a href="javascript:void(0)" id="'+ eventListing[i].PK_INQ_ID +'" custid="'+eventListing[i].FK_CUST_ID+'" inqid="'+eventListing[i].PK_INQ_ID+'" onclick="navigateToInquiryDetail(this)" >'+
                                        '<div class="desc">'+
                                           // '<div class="title2"><span class="tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Occasion Date">'+ eventListing[i].OCANAME +'</span></div>'+
                                           // '<div class="title2"><span class="tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Occasion Date">'+ mysqltoDesiredFormat(eventListing[i].EVENT_START_DATE,'dd-MM-yyyy') +' TO ' + mysqltoDesiredFormat(eventListing[i].EVENT_END_DATE,'dd-MM-yyyy') + '</span></div>'+
                                            '<div class="title2"><span class="tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Customer Name">'+ eventListing[i].CUSTNAME +'</span>&nbsp;|&nbsp;<span class="tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Number">'+ eventListing[i].CUSTMOBILE +'</span></div>'+
                                            '<div class="date tooltips" title="" data-placement="top" data-toggle="tooltip" data-original-title="Followup date ">'+ month1 +' '+ date1 +', '+ year1 +'</div>'+
                                        '</div>'+
                                        '<div class="time">'+
                                            '<div class="tooltips badge '+ statusColor +'" title="" data-placement="top" data-toggle="tooltip" data-original-title="Event Status">'+ eventListing[i].inqStatus +'</div>'+
                                        '</div>'+
                                    '</a>'+
                                '</div>'+
                            '</li>';
                }
                var footerUL = "</ul>";
                todayFlwp = headerUL+ middelLi + footerUL;
            }else{
                // toastr.warning("No record found");
                displayAPIErrorMsg( "" , GBL_ERR_NO_FLWP );
            }
        }

        $('#eventListDash').append(todayFlwp);

        // $('#eventListDashUL').niceScroll();

        intdefaulttooltips();
}

function loadMoreEvents(context){

    if( $(context).attr('initialized') == '0' ){

        startDate = ddmmyyToMysql(startDate);
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate() + 1);
        startDate = mysqltoDesiredFormat(startDate,'dd-MM-yyyy');

        endDate = ddmmyyToMysql(endDate);
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate() + 7);
        endDate = endDate.format('dd-MM-yyyy');
        console.log(endDate);
        $(context).attr('initialized','1');

    }else{

        startDate = ddmmyyToMysql(startDate);
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate() + 7);
        startDate = mysqltoDesiredFormat(startDate,'dd-MM-yyyy');

        endDate = ddmmyyToMysql(endDate);
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate() + 7);
        endDate = mysqltoDesiredFormat(endDate,'dd-MM-yyyy');

        console.log('st Date '+startDate);
        console.log('en Date '+endDate);
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
       requestCase: 'followupDashboardEventListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        requestedUserId : tmp_json.data[0].PK_USER_ID,
        startDate : startDate,
        endDate : endDate,
        orgId: checkAuth(1,2),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
     }
     commonAjax(FOLLOWUPURL, postData, loadMoreCallback,"Please Wait... Getting more Events Detail");
}


function loadMoreCallback(flag,data){

    if (data.status == "Success" && flag) {

        console.log(data.data);

        eventListDashboard(data)

    } else {
        if (flag)
             displayAPIErrorMsg( data.status , GBL_ERR_NO_DASHBOARD_PROP );
        else
            toastr.error(SERVERERROR);
    }
}

function chartInit(charData){

    var week = [];
    var bookingCount = [];

    for( var i=0; i<charData.length; i++ ){

        week.push(charData[i].week);
        bookingCount.push(parseInt(charData[i].bookingCount));
    }

    console.log(charData);

    var barChartData = {
        labels: week,
        datasets: [{
            label: 'Conversion ratio of booking',
            backgroundColor: "rgba(220,220,220,0.5)",
            data: bookingCount
        }]

    };

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each bar to be 2px wide and green
            elements: {
                rectangle: {
                    borderWidth: 2,
                    borderColor: 'rgb(0, 138, 177)',
                    borderSkipped: 'bottom'
                }
            },
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Weekly Converstation Ratio - Bar Chart'
            }
        }
    });
}

function clientopenInq(context){
    var statusId = $(context).attr("statusId");
    localStorage.POTGcurrentStatusId = statusId;
    navigateToInquiry();
}

function renderCustStats(){

    google.charts.load('current', {packages: ['corechart']});
    topCustomer();
}


function topCustomer(){

    var topFiveCustomer = dashboardData.topFiveCustomer;

    if( topFiveCustomer == undefined ){
        return false;
    }
// Top Five customer Listing starts here

    chartData1 = topFiveCustomer;

    if( chartData1.length == 0 ){
      return false;
    }

    setTimeout(function(){
        google.charts.setOnLoadCallback(drawBarColors1);
    },100);

    function drawBarColors1() {

          var chartArr = [];
          var firstArr = [];

          firstArr.push( 'Element' );
          firstArr.push( 'Total Amount' );
          firstArr.push( { role: 'style' } );
          firstArr.push( { role: 'annotation' } );

          chartArr.push( firstArr );
          if(topFiveCustomer != 'No record found'){
            chartData1.forEach(function(record,index) {

                var clrCode = "";
                if( index == 0 ){
                    clrCode = "#008ab1"
                }else if( index == 1 ){
                    clrCode = "#fdb928"
                }else if( index == 2 ){
                    clrCode = "#008ab1"
                }else if( index == 3 ){
                    clrCode = "#fdb928"
                }else if( index == 4 ){
                    clrCode = "#008ab1"
                }else{
                    clrCode = "#fdb928"
                }

                var temp = [];
                temp.push(record.custName);
                temp.push(parseFloat(record.totalInq));
                temp.push(clrCode);
                temp.push(record.custName+' ('+record.totalInq+')');
                chartArr.push( temp );
            });

            var data1 = google.visualization.arrayToDataTable(chartArr);
            var options1 = {
              title: '',
              chartArea: {
                  width: '100%',
                  height: '100%'
              },
              //colors: ['#008ab1', '#fdb928'],
              hAxis: {
                title: 'Top 5 Customer',
                minValue: 0
              },
			  animation: {
                   duration: 1000,
                   easing: 'linear',
                   startup: true
               },
              vAxis : {textColor: '#ffffff'},
              legend: { position: "none" },
            }
			 
            var chart1 = new google.visualization.BarChart(document.getElementById('topCustDT'));
            chart1.draw(data1, options1);
        }
    }
}

function bottomCustomer(){

    var bottomFiveCustomer = dashboardData.bottomFiveCustomer;

    if( bottomFiveCustomer == undefined ){
        return false;
    }

    // Bottom 5 Customer Listing starts here
    chartData2 = bottomFiveCustomer;

    if( chartData2.length == 0 ){
      return false;
    }

    setTimeout(function(){
        google.charts.setOnLoadCallback(drawBarColors2);
    },100);

    function drawBarColors2() {

          var chartArr = [];
          var firstArr = [];

          firstArr.push( 'Element' );
          firstArr.push( 'Total Amount' );
          firstArr.push( { role: 'style' } );
          firstArr.push( { role: 'annotation' } );

          chartArr.push( firstArr );
          if(bottomFiveCustomer != 'No record found') {
            chartData2.forEach(function(record,index) {

                var clrCode = "";
                if( index == 0 ){
                    clrCode = "#008ab1"
                }else if( index == 1 ){
                    clrCode = "#fdb928"
                }else if( index == 2 ){
                    clrCode = "#008ab1"
                }else if( index == 3 ){
                    clrCode = "#fdb928"
                }else if( index == 4 ){
                    clrCode = "#008ab1"
                }else{
                    clrCode = "#fdb928"
                }
                //console.log(clrCode);
                var temp = [];
                temp.push(record.custName);
                temp.push(parseFloat(record.totalInq));
                temp.push(clrCode);
                temp.push(record.custName+' ('+record.totalInq+')');
                chartArr.push( temp );
            });

            var data2 = google.visualization.arrayToDataTable(chartArr);
            var options2 = {
              title: '',
              chartArea: {
                  width: '100%',
                  height: '100%'
              },
              //colors: ['#b0120a', '#ffab91'],
              hAxis: {
                title: 'Bottom 5 Customer',
                minValue: 0
              },
			  animation: {
                   duration: 1000,
                   easing: 'linear',
                   startup: true
               },
              vAxis : {textColor: '#ffffff'},
              legend: { position: "none" },
            }
            var chart2 = new google.visualization.BarChart(document.getElementById('bottomCustDT'));
            chart2.draw(data2, options2);
        }
    }
}

function topItem(){

    var topFiveItem = dashboardData.topFiveItem;

    if( topFiveItem == undefined ){
        return false;
    }

    //Top 5 items Listing Starts here
    chartData3 = topFiveItem;

    if( chartData3.length == 0 ){
      return false;
    }

    setTimeout(function(){
        google.charts.setOnLoadCallback(drawBarColors3);
    },100);

    function drawBarColors3() {

          var chartArr = [];
          var firstArr = [];

          firstArr.push( 'Element' );
          firstArr.push( 'Total Amount' );
          firstArr.push( { role: 'style' } );
          firstArr.push( { role: 'annotation' } );

          chartArr.push( firstArr );
          if(topFiveItem != 'No record found'){
            chartData3.forEach(function(record,index) {

                var clrCode = "";
                if( index == 0 ){
                    clrCode = "#008ab1"
                }else if( index == 1 ){
                    clrCode = "#fdb928"
                }else if( index == 2 ){
                    clrCode = "#008ab1"
                }else if( index == 3 ){
                    clrCode = "#fdb928"
                }else if( index == 4 ){
                    clrCode = "#008ab1"
                }else{
                    clrCode = "#fdb928"
                }

                //console.log(clrCode);
                var temp = [];
                temp.push(record.itemName);
                temp.push(parseFloat(record.totalInq));
                temp.push(clrCode);
                temp.push(record.itemName+' ('+record.totalInq+')');
                chartArr.push( temp );
            });

            var data3 = google.visualization.arrayToDataTable(chartArr);
            var options3 = {
              title: '',
              chartArea: {
                  width: '100%',
                  height: '100%'
              },
              //colors: ['#b0120a', '#ffab91'],
              hAxis: {
                title: 'Top 5 Items',
                minValue: 0
              },
			  animation: {
                   duration: 1000,
                   easing: 'linear',
                   startup: true
              },
              vAxis : {textColor: '#ffffff'},
              legend: { position: "none" },
            };
            var chart3 = new google.visualization.BarChart(document.getElementById('topItemDT'));
            chart3.draw(data3, options3);
        }
    }

}

function bottomItem(){

    var bottomFiveItem = dashboardData.bottomFiveItem;

    if( bottomFiveItem == undefined ){
        return false;
    }

    //Bottom 5 Items Listing starts here
    chartData4 = bottomFiveItem;

    if( chartData4.length == 0 ){
      return false;
    }

    setTimeout(function(){
        google.charts.setOnLoadCallback(drawBarColors4);
    },100);

    function drawBarColors4() {

          var chartArr = [];
          var firstArr = [];

          firstArr.push( 'Element' );
          firstArr.push( 'Total Amount' );
          firstArr.push( { role: 'style' } );
          firstArr.push( { role: 'annotation' } );

          chartArr.push( firstArr );
          if(bottomFiveItem != 'No record found') {
            chartData4.forEach(function(record,index) {

                var clrCode = "";
                if( index == 0 ){
                    clrCode = "#008ab1"
                }else if( index == 1 ){
                    clrCode = "#fdb928"
                }else if( index == 2 ){
                    clrCode = "#008ab1"
                }else if( index == 3 ){
                    clrCode = "#fdb928"
                }else if( index == 4 ){
                    clrCode = "#008ab1"
                }else{
                    clrCode = "#fdb928"
                }

                //console.log(clrCode);
                var temp = [];
                temp.push(record.itemName);
                temp.push(parseFloat(record.totalInq));
                temp.push(clrCode);
                temp.push(record.itemName+' ('+record.totalInq+')');
                chartArr.push( temp );
            });

            var data4 = google.visualization.arrayToDataTable(chartArr);
            var options4 = {
              title: '',
              chartArea: {
                  width: '100%',
                  height: '100%'
              },
              //colors: ['#008ab1', '#fdb928'],
              hAxis: {
                title: 'Bottom 5 Items',
                minValue: 0
              },
				animation: {
                   duration: 1000,
                   easing: 'linear',
                   startup: true
               },
              vAxis : {textColor: '#ffffff'},
              legend: { position: "none" },
            };
            var chart4 = new google.visualization.BarChart(document.getElementById('bottomItemDT'));
            chart4.draw(data4, options4);
        }
    }
}


function getActSideBar(type){

    if( type == "today" ){
        getFollowupDetail(1);
        $('[href="#todayTab"]').tab('show');
    }else if( type == "upcomming" ){
        getFollowupDetail(3);
        $('[href="#upcommingTab"]').tab('show');
    }else {
        getFollowupDetail(2);
        $('[href="#delayTab"]').tab('show');
    }
}


function geoChart(geoDataArr){

    // google.charts.load('current', {'packages': ['geochart']});
    google.charts.setOnLoadCallback(drawMarkersMap);

    function drawMarkersMap() {
          var data = google.visualization.arrayToDataTable(geoDataArr);

          var options = {
            region: 'IN',
            displayMode: 'markers',
            colorAxis: {colors: ['red', 'green']},
            height:300,
			animation: {
                duration: 1000,
                easing: 'linear',
                startup: true
			}
          };

          var chart = new google.visualization.GeoChart(document.getElementById('custChart'));
          chart.draw(data, options);
    };
}

function donutChart( donutData ){

    //google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Status');
      data.addColumn('number', 'Status Count');
      data.addRows( donutData );

      var options = {
        //title: 'Inquiry Conversion',
        sliceVisibilityThreshold: .0,
        pieHole: 0.3,
        legend: 'none',
        pieSliceText: 'label',
        height:400,
		animation: {
                duration: 1000,
                easing: 'linear',
                startup: true
        }
      };

      var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
      chart.draw(data, options);
    }
}