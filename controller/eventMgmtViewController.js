    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 02-08-2018.
 * File : eventMgmtViewController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_DETAIL = [];

// PAGE LOAD EVENTS
function pageInitialEvents() {

    if( localStorage.indeCampusViewEventId != undefined && localStorage.indeCampusViewEventId != "" ){
        getEventDetails(); // GET EVENT DETAILS TO DISPLAY
    }else{
        // navigateToEventManagementList();
    }
}

// GET EVENT DETAILS TO DISPLAY
function getEventDetails(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getEventDetails',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(21,86),
        itemId : localStorage.indeCampusViewEventId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getFilteredStudentsDataCallback,"Please Wait... Getting Dashboard Detail");

    function getFilteredStudentsDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
            console.log(data);
            GBL_EVENT_DETAIL = data.data;
            renderEventDetail(); // RENDER EVENT DETAILS

        }else{

            GBL_EVENT_DETAIL = [];
            renderEventDetail(); // RENDER EVENT DETAILS
            if (flag){
                
                displayAPIErrorMsg( data.status , GBL_ERR_NO_EVENT_DETAIL );
                // toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }
        }
    }

}

// RENDER EVENT DETAILS
function renderEventDetail(){

    var eventDetails = GBL_EVENT_DETAIL[0];
    $('#eventNameId').html( eventDetails.itemName + ' - ' + eventDetails.eventNo );
    $('#eventVenue').html( 'Address : ' + eventDetails.eventVenue );
           
    $('#eventStartDateTime').html( eventDetails.eventStartDate );
    $('#eventEndDateTime').html( eventDetails.eventEndDate );

    var tHead = '<table class="display datatables-basic" id="eventBookingTable">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Membership ID</th>'+
                            '<th>Student Name</th>'+
                            '<th>No. Of Seats Booked</th>'+
                            // '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '' ;
    if( eventDetails.studentData != "No record found" && eventDetails.studentData != "" ){

        eventDetails.studentData.forEach(function( record , index ){

            newRow +=   '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.membershipNo +'</td>'+
                            '<td>'+ record.fullName +'</td>'+
                            '<td>'+ record.qty +'</td>'+
                            // '<td>'+
                            //     '<a href="javascript:void(0)"  class="btn-right-mrg btn btn-xs btn-amber btn-ripple" data-toggle="tooltip" data-original-title="View" data-placement="top" fullName="'+ record.fullName +'" invoiceId="'+ record.invoiceId +'" onclick="getBookingInvData(this);"><i class="fa fa-eye"></i></a>'+ 
                            // '</td>'+
                        '</tr>';

        });

    }

    $('#eventBookinTblDiv').html( tHead + newRow + tEnd );
    $('#eventBookingTable').dataTable();
    // $('#diveventMfgListing').dataTable();
}

function getBookingInvData(context){

    var invoiceId = $(context).attr('invoiceId');
    var fullName = $(context).attr('fullName');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getEventInvoiceData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(21,86),
        itemId : localStorage.indeCampusViewEventId,
        invoiceId : invoiceId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getFilteredStudentsDataCallback,"Please Wait... Getting Dashboard Detail");

    function getFilteredStudentsDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
            console.log(data);
            var invoiceData = data.data;
            renderInvoice( invoiceData , fullName ); // RENDER INVOICE IN MODAL

        }else{

            if (flag){
                
                displayAPIErrorMsg( data.status , GBL_ERR_NO_EVENT_BOOK_INV );
                // toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }
        }
    }

}

// RENDER INVOICE IN MODAL
function renderInvoice(invoiceData,fullName){

    if( invoiceData != "" && invoiceData != "No record found" ){

        invoiceData = invoiceData[0];
        $('#invoiceNoPrint').html( invoiceData.cInvoiceId );
        $('#invoiceDatePrint').html( 'Invoice Date: ' +  mysqltoDesiredFormat(invoiceData.invoiceDate,'dd-MM-yyyy') );
        $('#eventNamePrint').html( invoiceData.itemName );
        $('#startDateTimePrint').html( invoiceData.eventStartDate );
        $('#endDateTimePrint').html( invoiceData.eventEndDate );
        $('#eventVenuePrint').html( invoiceData.eventVenue );
        
        $('#studentNamePrint').html( fullName );
        $('#studentAdd1Print').html( invoiceData.address );
        $('#studentAdd2Print').html( invoiceData.cityName + ' ' + invoiceData.stateName + ' ' + invoiceData.countryName + ' - ' + invoiceData.pinCode );
        $('#studMemIdPrint').html( 'Student Membership No : ' + invoiceData.memberShipNo );

        var tBody = '';
        var totalCgstAmount = 0;
        var totalSgstAmount = 0;
        var subTotal = 0;
        var preBalance = 0.00;
        var collection = 0.00;
        var balDue = 0.00;
        var showModalFlag = false;

        if( invoiceData.itemData != "" && invoiceData.itemData != "No record found"  ){

            invoiceData.itemData.forEach(function( record , index ){

                showModalFlag = true;

                var cgstPer = ( record.cgst != "" ? fixedTo2(record.cgst) : 0 );
                var sgstPer = ( record.sgst != "" ? fixedTo2(record.sgst) : 0 );
                var cgstAmt = fixedTo2(parseFloat(invoiceData.itemPrice) * parseFloat(cgstPer) / 100);
                var sgstAmt = fixedTo2(parseFloat(invoiceData.itemPrice) * parseFloat(sgstPer) / 100);
                totalCgstAmount = fixedTo2(parseFloat(totalCgstAmount) + parseFloat(cgstAmt));
                totalSgstAmount = fixedTo2(parseFloat(totalSgstAmount) + parseFloat(sgstAmt));
                var totalAmount = parseFloat(record.qty) * parseFloat(invoiceData.itemPrice);
                subTotal = fixedTo2(parseFloat(subTotal) + parseFloat(totalAmount));

                tBody += '<tr>'+
                            '<td style="padding: 5px 0 5px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+ (index+1) +'</td>'+
                            '<td style="padding: 5px 0px 5px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">'+
                                '<div>'+
                                    '<div><span style="word-wrap: break-word;" id="item_name">'+ record.itemName +'</span><br><span class="item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">SAC: '+ record.hsnCode +'</span></div>'+
                                '</div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<span id="item_amount">'+
                                '<span>₹</span> '+ fixedTo2(invoiceData.itemPrice) +'</span>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<span id="item_amount">'+
                                '<span></span> '+ record.qty +'</span>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ cgstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ cgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">'+
                                '<div id="item_tax_amount"><span>₹</span> '+ sgstAmt +'</div>'+
                                '<div class="item-desc" style="color: #727272; font-size: 8pt;">'+ sgstPer +'% </div>'+
                            '</td>'+
                            '<td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">'+
                                '<span id="item_amount">'+
                                '<span>₹</span> '+ fixedTo2(totalAmount) +'</span>'+
                            '</td>'+
                        '</tr>';


            });
            
        }
        var finalTotal = fixedTo2(parseFloat(subTotal) + parseFloat(totalCgstAmount) + parseFloat(totalSgstAmount));    

        tBody += '<tr>'+
                   '<td colspan="7" style="text-align:right;">'+
                        '<table class="totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">'+
                            '<tbody>'+
                                '<tr>'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>'+
                                    '<td colspan="2" id="subtotal" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ subTotal +'</td>'+
                                '</tr>'+
                                '<tr style="height:10px;">'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>'+
                                    '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ totalCgstAmount +'</td>'+
                                '</tr>'+
                                '<tr style="height:10px;">'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>'+
                                    '<td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span>₹</span> '+ totalSgstAmount +'</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>'+
                                    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                                    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b><span>₹</span> '+ finalTotal +'</b></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Pre Balance</b></td>'+
                                    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>'+
                                    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b><span>₹</span> '+ preBalance +'</b></td>'+
                                '</tr>'+
                                 '<tr>'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>'+
                                    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>CR</b></td>'+
                                    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b><span>₹</span> '+ collection +'</b></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right background-color: #f5f4f3; height: 40px;font-size: 9pt;" ><b>Balance Due</b></td>'+
                                    '<td id="total" style="width:50px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><b>CR</b></td>'+
                                    '<td id="total" style="width:120px;padding: 5px 10px 5px 5px; background-color: #f5f4f3; height: 40px;font-size: 9pt;" valign="middle" align="right"><b><span>₹</span> '+ balDue +'</b></td>'+
                                '</tr>'+
                                '<tr><td colspan="5"  style="font-size:16px;text-align: left;"><b><u>Terms and Conditions</u></b></td></tr>'+
                                '<tr><td colspan="5" style="padding-top:10px;padding-bottom: 4px;text-align: left;"><span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATE, FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>'+
                                '<tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus</span></b>".</span></td></tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</td>'+
                '</tr>';

        $('#printItemBody1').html( tBody );
        if( showModalFlag == true ){
            modalShow('eventViewInvoiceDetail');
        }else{
            displayAPIErrorMsg( "" , "No booking invoice item available" );
        }

    }
}

// PRINT INVOICE 
function printInvoice(){

    var data = $('#receiptPrint').html();
    var myWindow=window.open('','','height=800,width=1200');
        
    myWindow.document.write(data);
    myWindow.document.close();
    setTimeout(function() {
        myWindow.print();
        myWindow.close();
    }, 250);

}