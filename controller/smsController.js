/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : smsController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var branchData = [];
var GBL_TEMPLATE_LIST = [];
var GBL_SEND_TEMPLATE_DATA = [];
// GET CLIENT LISTING DATA
function getClientData(){

    $('#clientList').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getClientListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getClientDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET CLIENT LISTING DATA CALLBACK
function getClientDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var newRow = '';
        if(tmp_json.data[0].FK_CLIENT_ID == -1){
            for( var i=0; i<data.data.length; i++ ){
                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
            }
        }else{
            for( var i=0; i<data.data.length; i++ ){
                if(tmp_json.data[0].FK_CLIENT_ID ==data.data[i].clientId ){

                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
                }
            }
        }
        

        $('#clientList').html(newRow);
        //$('#clientList').val(tmp_json.data[0].FK_CLIENT_ID);
        $('#clientList').selectpicker();

        getSMSdata();

    } else {
        getSMSdata();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT );
        else
            toastr.error(SERVERERROR);
    }
}

// GET SMS CONFIG DATA
function getSMSdata(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "SMSData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getSMSdataCallback,"Please Wait... Getting Dashboard Detail");

    function getSMSdataCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
            
            if( data.data != 'No record found' ){

                $('#clientList').val(data.data[0].client);
                $('#uName').val(data.data[0].uname);
                $('#password').val(data.data[0].password);
                $('#sid').val(data.data[0].sid);
                $('#apis').val(data.data[0].apis);

                $('#apiConstUrl').val(data.data[0].smsUrl);
                $('#apiConstSchUrl').val(data.data[0].smsUrlSchedule);
                $('#smsScheduleTime').val(data.data[0].smsScheduleTime).selectpicker('refresh');

                $('#clientList').selectpicker('refresh');
                
            }
            addFocusId( 'clientList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_SMS_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// CREATE / UPDATE SMS CONFIG DATA
function sendSMSdata(){

    var clientName = $('#clientList').val();
    var uname = $('#uName').val().trim();
    var password = $('#password').val().trim();
    var sid = $('#sid').val().trim();
    var api = $('#apis').val().trim();
    var smsScheduleTime = $('#smsScheduleTime').val().trim();

    var smsUrl = $('#apiConstUrl').val().trim();
    var smsUrlSchedule = $('#apiConstSchUrl').val().trim();

    if( uname.length == 0 ){
        toastr.warning("Please Enter User Name");
        $('#uName').focus();
        return false;

    }else if( password.length == 0 ){
        toastr.warning("Please Enter password");
        $('#password').focus();
        return false;
    
    }else if( sid.length == 0 ){
        toastr.warning("Please Enter SID");
        $('#sid').focus();
        return false;

    }else if( api.length == 0 ){
        toastr.warning("Please Enter API");
        $('#api').focus();
        return false;
    
    }else if( smsUrl.length == 0 ){
        toastr.warning("Please Enter API URL With Constants");
        $('#apiConstUrl').focus();
        return false;

    }else if( smsUrlSchedule.length == 0 ){
        toastr.warning("Please Enter API URL Scheduled With Constants");
        $('#apiConstSchUrl').focus();
        return false;
    
    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createUpdateSms",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,13),
            reqClientId: tmp_json.data[0].FK_CLIENT_ID,
            clientName : clientName,
            uname : uname,
            password : password,
            sid : sid,
            api : api,
            smsUrl : smsUrl,
            smsUrlSchedule : smsUrlSchedule,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, sendSMSdataCallback,"Please Wait... Getting Dashboard Detail");

        function sendSMSdataCallback(flag,data){

            if (data.status == "Success" && flag) {
                console.log(data);
                
                toastr.success('SMS data Sent Successfully');
                getSMSdata();

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_SEND_SMS_DATA );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

// RESET FIELDS
function resetData(){

    $('#uName').val('');
    $('#password').val('');
    $('#sid').val('');
    $('#apis').val('');
}