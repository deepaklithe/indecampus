/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 11-Jan-2018.
 * File : dynamicProposalTemplateController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
var GBL_TEMPLATE_JSON=[]; 

function pageInitialEvents(){
    getTemplateData();
}

//START FUNCTION FOR CALL API FOR CLIENT LISTING
function getTemplateData() {

    var postData = {
        requestCase: 'getSystemTemplate',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        
    }
    commonAjax(COMMONURL, postData, getTemplateDataListingCallBack, "");
    //END FUNCTION FOR CALL API FOR CLIENT LISTING

    //START FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO GBL_TEMPLATE_JSON
    function getTemplateDataListingCallBack(flag, data) {

        if (data.status == "Success" && flag) { 
            GBL_TEMPLATE_JSON = data.data; 
            renderTemplateData();

        } else { 
            GBL_TEMPLATE_JSON = []; 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_TEMPLATE );
            else
                toastr.error(SERVERERROR);
        }
    }
}
 
//START FUNCTION FOR DISPLAY DATA OF DOCUMENT
function renderTemplateData() {  

    // GROUP FIELD JSON
    console.log(GBL_TEMPLATE_JSON);
    var groupFieldData = [];
    if( GBL_TEMPLATE_JSON != "No record found" ){
        
        groupFieldData = GBL_TEMPLATE_JSON.reduce(function(result, current) {
            result[current.templateTypeName] = result[current.templateTypeName] || [];
            result[current.templateTypeName].push(current);
            return result;
        }, {});
    } 
    
    if( groupFieldData.System != "" && groupFieldData.System != undefined && groupFieldData.System != null ){
        // SYSTEM TEMPLATE
        
        var clientTemplateHtml = "";
        groupFieldData.System.forEach( function( record , index ){
            var rbChecked = "";
            if( index == "0" ){
                rbChecked = "checked";
            }
            clientTemplateHtml += "<div class='col-lg-4' id='template_"+rbChecked+"'>"+
                                "<div class='card card-image card-light-blue bg-image bg-opaque8'>"+ 
                                    "<div class='context has-action-left has-action-right'>"+
                                        "<div class='tile-content'>"+
                                            "<span class='text-title'> "+ record.templateName +" </span>"+
                                            "<span class='text-subtitle'> "+ record.templateTypeName +" Generated  </span>"+
                                        "</div>"+
                                        "<div class='tile-action right'>"+ 
                                            "<a href='javascript:void(0)' data-placement='top' data-toggle='tooltip' data-original-title='View Template' title='' class='tooltips btn btn-xs btn-warning btn-right-mrg' onclick='viewTemplate("+ record.templateId +")'><i class='fa fa-eye'></i></a>"+
                                            "<a href='javascript:void(0)' data-placement='top' data-toggle='tooltip' data-original-title='Create Template' title='' class='tooltips btn btn-xs btn-success btn-right-mrg' onclick='proceedToDynamicTemplate("+ record.templateId +")'><i class='fa fa-plus'></i></a>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
        });
        $("#sysGenTemplates").html( clientTemplateHtml );
    }

    if( groupFieldData.Client != "" && groupFieldData.Client != undefined && groupFieldData.Client != null ){
        // USER TEMPLATE
        var userTemplateHtml = ""
        groupFieldData.Client.forEach( function( record , index ){ 
            var rbChecked = "";
            if( index == "0" ){
                rbChecked = "checked";
            }
            userTemplateHtml += "<div class='col-lg-4' id='userTemplate_"+rbChecked+"'>"+
                                "<div class='card card-image card-light-blue bg-image bg-opaque8'>"+ 
                                    "<div class='context has-action-left has-action-right'>"+
                                        "<div class='tile-content'>"+
                                            "<span class='text-title'> "+ record.templateName +" </span>"+
                                            "<span class='text-subtitle'> "+ record.templateTypeName +" Generated  </span>"+
                                        "</div>"+
                                        "<div class='tile-action right'>"+ 
                                            "<a href='javascript:void(0)' data-placement='top' data-toggle='tooltip' data-original-title='View Template' title='' class='tooltips btn btn-xs btn-warning btn-right-mrg' onclick='viewTemplate("+ record.templateId +")'><i class='fa fa-eye'></i></a>"+
                                            "<a href='javascript:void(0)' data-placement='top' data-toggle='tooltip' data-original-title='Edit Template' title='' class='tooltips btn btn-xs btn-success btn-right-mrg' onclick='proceedToDynamicTemplate("+ record.templateId +")'><i class='fa fa-edit'></i></a>"+
                                            "<a href='javascript:void(0)' data-placement='top' data-toggle='tooltip' data-original-title='Delete Template' title='' class='tooltips btn btn-xs btn-danger btn-right-mrg' onclick='deleteTemplate("+ record.templateId +" , "+ record.defaultTemplateId +")'><i class='fa fa-trash-o'></i></a>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
        });
        $("#userGenTemplates").html( userTemplateHtml );
    }else{
        noUserTemplate = 1;
    }

}

function proceedToDynamicTemplate( selectedTemplate ){ 
    var index = findIndexByKeyValue( GBL_TEMPLATE_JSON , "templateId" , selectedTemplate );
    if( index != "-1" ){
        localStorage.selectedTemplateData = JSON.stringify( GBL_TEMPLATE_JSON[index] );
        navigateToDynamicTemplateCreation( selectedTemplate );
    }
}

//START FUNCTION FOR CALL API FOR CLIENT LISTING
function deleteTemplate( templateId , defaultTemplateId ) {

    var confirmBox = confirm("Are you sure? You want to delete this template");
    if( !confirmBox ){
        return false;
    }

    var postData = {
        requestCase: 'deleteClientTemplate',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4, 14),
        defaultTemplateId : defaultTemplateId,
        clientTemplateId : templateId,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    commonAjax(COMMONURL, postData, deleteTemplateDataCallBack, "");
    //END FUNCTION FOR CALL API FOR CLIENT LISTING

    //START FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO GBL_TEMPLATE_JSON
    function deleteTemplateDataCallBack(flag, data) {

        if (data.status == "Success" && flag) {  

            var index = findIndexByKeyValue( GBL_TEMPLATE_JSON , "templateId" , templateId );
            if( index != "-1" ){

                GBL_TEMPLATE_JSON.splice( index , 1 );
                renderTemplateData();
            } 
        } else { 
            GBL_TEMPLATE_JSON = []; 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_DELETE_TEMPLATE );
            else
                toastr.error(SERVERERROR);
        }
    }
}
//END FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO GBL_TEMPLATE_JSON

function viewTemplate( templateId ){
    var index = findIndexByKeyValue( GBL_TEMPLATE_JSON , "templateId" , templateId );
    if( index != "-1" ){

        // ISSUE WHILE SETTING THE HTML IN CONTENT - BUG REF - #6441
        // $('#proposalViewHtml').html( GBL_TEMPLATE_JSON[index].templateHtml );
        $( window.open().document.body ).html( GBL_TEMPLATE_JSON[index].templateHtml );
    } 
}

var GBL_TOUR_STEPS = [];
var noUserTemplate = 0;
// INITIALIZE TOUR - ADDED ON 09-MAY-2018 BY VISHAKHA TAK
function initSelfTour(){

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: ".sysGenTemplatesCls",
            title: "<b>System Generated Templates</b>",
            content: "These are the system generated proposal templates !!",
            placement: 'top'
        },
        {
            path: "",
            element: '.userGenTemplatesCls',
            title: "<b>User Generated Templates</b>",
            content: "These are the user generated proposal templates !!",
            placement: 'top',
        },
        {
            path: "",
            element: "#template_checked",
            title: "<b>System Generated Templates Action</b>",
            content: "You can view and add these templates",
            placement: 'top',
            onNext:function (tour){
                if( noUserTemplate == 1 ){  // SKIP NEXT STEP AND PERFORM NEXT ACTIVITIES
                    $( window.open().document.body ).html( GBL_TEMPLATE_JSON[0].templateHtml );
                    proceedToDynamicTemplate(GBL_TEMPLATE_JSON[0].templateId);
                }
            } 
        },
        {
            path: "",
            element: "#userTemplate_checked",
            title: "<b>User Generated Templates Action</b>",
            content: "You can view , edit and delete these templates",
            placement: 'top',
            onNext:function (tour){
                $( window.open().document.body ).html( GBL_TEMPLATE_JSON[0].templateHtml );
            }     
        },
        {
            path: "",
            element: '#template_checked',
            title: "<b>Template View</b>",
            content: "View the system and user generated templates!",
            placement: 'top', 
            onNext: function (tour){
                
            }
        },
        {
            path: "",
            element: '#template_checked',
            title: "<b>Create/Update Template</b>",
            content: "Create/Update the system and user generated templates!",
            placement: 'top',
            onShown: function (tour){
                proceedToDynamicTemplate(GBL_TEMPLATE_JSON[0].templateId);
            }
        },
        {
            path: "",
            element: '#templateNameDiv',
            title: "<b>Template Name</b>",
            content: "Add or update your template's name!",
            placement: 'bottom',   
        },
        {
            path: "",
            element: '#templateDataDiv',
            title: "<b>Edit Template</b>",
            content: "Create your template or edit it!",
            placement: 'left',   
        },
        {
            path: "",
            element: '#creatUpdTemBtn',
            title: "<b>Save/Update Template details</b>",
            content: "Click and save your template!",
            placement: 'left', 
            onNext: function(tour){
                 window.location.href = "dynamicProposalTemplate.html";
            }
        },
        {
            path: "dynamicProposalTemplate.html",
            element: '#creatUpdTemBtn',
            title: "<b>See your template</b>",
            content: "Click and save your template!",
            placement: 'left',   
        }
    ];
   
    // Instance the tour
    instanceTour();
    tour._options.name = "Dynamic Template Tour";
    // tour._options.debug = true;

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM THIS STEP TO PREVENT FROM DISPLAYING LAST STORED STEP

    // Start the tour
    tour.start(true);
    localStorage.POTGtemplateTour = 1;
    localStorage.GBL_TOUR_STEPS_USER = JSON.stringify(GBL_TOUR_STEPS);
}