/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : teamController.
 * File Type : .js.
 * Project : POTG
 *
 * */
var teamData = [];
var organoGramData = [];
function getTeamListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getTeamListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(19,74),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getTeamListingCallback,"Please Wait... Getting Dashboard Detail");
}


function getTeamListingCallback(flag,data){

    if (data.status == "Success" && flag) {
        // console.log(data);

        var newRow = '';
        var count = 1 ;

        if( localStorage.displayTeamView == "List" ){
            displayList();
        }else{
            displayChart();
        }
        if(data.data != 'No record found'){
            teamData = data.data.tableData;
            organoGramData = data.data.organogramData;
            console.log(teamData);

            for( var i=0; i<data.data.tableData.length; i++ ){

                var name = data.data.tableData[i].userName;
                var mobile = data.data.tableData[i].userContact;
                var department = data.data.tableData[i].userDesignation;
                var supervisor = data.data.tableData[i].userSupervisor;
                
                count = i + 1;
                newRow += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td>'+ name +'</td>'+
                                '<td>'+ mobile +'</td>'+
                                '<td>'+ department +'</td>'+
                                '<td>'+ supervisor +'</td>'+
                            '</tr>';
            }

            
            $('#moduleList').html(newRow);

        }
        TablesDataTables.init();
        TablesDataTablesEditor.init();
        
        var data = organoGramData;
        
        var orgEditorConfig = new primitives.orgdiagram.Config();
        //console.log(orgEditorConfig); return false;
        orgEditorConfig.editMode = false;
        orgEditorConfig.items = data;
        orgEditorConfig.hasSelectorCheckbox = 2;
        orgEditorConfig.pageFitMode = 1;
        orgEditorConfig.orientationType = 0;
        if( data != "No record found" ){
            //console.log(orgEditorConfig);
            orgEditorConfig.items.forEach(function( itemRecord , itemIndex ){
                //console.log(itemRecord[]);
                itemRecord.image = (itemRecord.profileImage !== "") ? "assets/globals/img/avtar1.png" : itemRecord.profileImage;
            });
        }
        $("#centerpanel").orgDiagram(orgEditorConfig);

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );  
        else
            toastr.error(SERVERERROR);
    }
}

function displayChart() {
    
   $('#Chart').show(); 
   $('#List').hide(); 
   $('#organoGramDiv').show('300');
   $('#ListDiv').hide('300');

    
    localStorage.displayTeamView = "Chart";
}

function displayList() {
    
   $('#List').show(); 
   $('#Chart').hide(); 
   $('#ListDiv').show('300');
   $('#organoGramDiv').hide('300');

   localStorage.displayTeamView = "List";
}
