/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : editUserController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var selectedRoleIds = [];
var userRoleData = [];
var GBL_USERID = '';
// GET USER BRANCH, USER LEVEL AND USER ROLE DATA
function getUserData(){

    $('#fullname').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getUserLevelForUserManagement",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET USER DATA CALLBACK
function getUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        var branchListing = '';
        
        for( var i=0; i<data.data.branchListing.length; i++ ){

            branchListing += '<option value="'+ data.data.branchListing[i].orgId +'">'+ data.data.branchListing[i].orgName +'</option>';
        }
        $('#branchLevel').html(branchListing);
        $('#branchLevel').selectpicker();


        var userLevel = '<option value="-1">--- Please select Level ---</option>';
        if( data.data.userLevel != "" && data.data.userLevel != "No record found" ){
            
            
            for( var i=0; i<data.data.userLevel.length; i++ ){
                
                var level = '';
                
                //Level defined for only 3 level
                if( i == 0 ){
                    level = 1; // Last Level
                }else if( i == data.data.userLevel.length - 1 ){
                    level = 0;
                }else{
                    level = 2; // Higher Level
                }

                userLevel += '<option level="'+ level +'" value="'+ data.data.userLevel[i].levelId +'">'+ data.data.userLevel[i].levelName +'</option>';
            }
        }
        $('#userLevel').html(userLevel);
        $('#userLevel').selectpicker();
        $('#userLevel').val('-1');

        userRoleData = data.data.userRole;

        var userRole = '';
        for( var i=0; i<data.data.userRole.length; i++ ){

            var roleId = data.data.userRole[i].roleId;
            var roleName = data.data.userRole[i].roleName;
            // var orgType = data.data.userRole[i].ORG_TYPE;
            // var activity = data.data.userRole[i].ACTIVITY;
            // var mouleId = data.data.userRole[i].FK_MODULE_ID;

            userRole += '<option value="'+ roleId +'">'+ roleName +'</option>';
        }

        $('#roleLevel').html(userRole);
        $('#roleLevel').selectpicker();

        $('#userLevel').change(function(){

            var level =  $('option:selected', this).attr('level');
            //0 - higher , 1 -lower

            if( level == "9" ){

                $('.lowerLevel').show();
                $('#supervisorLevel').hide();
                $('#supName').val('');
                $('#supName').attr('itemid','');

            }else if( level == "1" ){

                $('.lowerLevel').show();
                $('#supervisorLevel').show();
                $('#supName').val('');
                $('#supName').attr('itemid','');
            
            }else{

                $('#supervisorLevel').show();
                $('.lowerLevel').show();
                $('#supName').val('');
                $('#supName').attr('itemid','');
            }
        })
        
        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        var designationList = roleAuth.data.designation;
        
        setOption("0", "desLevel", designationList, "--- Select Designation ---");

        $('#desLevel').selectpicker();
        
        addFocusId( 'fullname' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_LEVEL );
        else
            toastr.error(SERVERERROR);
    }

    editUserListing();
}

// CREATE USER JSON TO UPDATE SELECTED USER
function createUser(){

    var userLoginType = $('#userLoginType').val();
    var fullname = $('#fullname').val().trim();
    var email = $('#email').val().trim();
    // var mobile = $('#mobile').val().trim();
    var mobile = getIntlMobileNum("mobile");    // GET MOBILE NUM
    var username = $('#username').val().trim();
    var pwd = $('#pwd').val();
    var cnfpwd = $('#cnfpwd').val();
    var supName = $('#supName').attr('itemid');
    var address = $('#address').val().trim();
    var uLevel = $('#userLevel option:selected').attr('level');
    var uLevelVal = $('#userLevel').val();
    var desLevel = $('#desLevel').val();
    var googleCalKey = $('#googleCalKey').val();
	var bDate = $('#birthdate').val();
	var annDate= $('#anniversaryDate').val();
    var uGender = $('input[type=radio][name="userGender"]').filter(":checked").val();
	var bDt = new Date(ddmmyyToMysql(bDate));
    var annivDt = new Date(ddmmyyToMysql(annDate));
    var department = $('#department').val();
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    // CHECK VALIDATIONS
    if( fullname.length == 0 ){
        toastr.warning('Please Enter Full Name');
        $('#fullname').focus();
        return false;
    
    }else  if( email.length == 0 ){
        toastr.warning('Please Enter E-Mail');
        $('#email').focus();
        return false;
    
    }else if( !validateEmail(email) ){
        toastr.warning('Please Enter valid email');
        $('#email').focus();
        return false;
    
    }else if( !checkValidMobile( "mobile" ) ){
    // }else if( mobile.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Mobile Number');
        $('#mobile').focus();
        return false;
    
    }else if( username.length == 0 ){
        toastr.warning('Please Enter user Name');
        $('#username').focus();
        return false;
    
    }else if( !validateEmail(username)){
        toastr.warning('Please Enter valid user name (Ex:abc@hostname.com)');
        $('#username').focus();
        return false;
    
    }else if( bDate == "" ){
        toastr.warning('Please Enter Birth Date');
        $('#birthdate').focus();
        return false;
    
    }else if( (annDate != "") && ( +annivDt < +bDt ) ){
        toastr.warning('Please Enter Proper Anniversary Date');
        $('#anniversaryDate').focus();
        return false;
    
    }else if( address.length == 0 ){
        toastr.warning('Please Enter address');
        $('#address').focus();
        return false;

    }else if( uLevelVal == -1 ){
        toastr.warning('Please Select User Level');
        $('#userLevel').focus();
        return false;
    
    }else if( desLevel == -1 ){
        toastr.warning('Please Select Designation');
        $('#desLevel').focus();
        return false;
    
    }else if( uLevel == "1" && (supName == "" || supName == undefined )){
        toastr.warning('Please Enter Supervisor Name for this Level of User');
        $('#supName').focus();
        return false;

    }else if( uLevel == "1" && $('#roleLevel').val() == null ){ 
        toastr.warning('Please select Roles for this Level of User');
        $('#roleLevel').focus();
        return false;

    }else if($('#branchLevel').val() == null ){ 
        toastr.warning('Please select Branches for this Level of User');
        $('#branchLevel').focus();
        return false;
    
    }else if( uLevel == "2" &&  (supName == "" || supName == undefined )){

        toastr.warning('Please Enter Supervisor Name for this Level of User');
        $('#supName').focus();
        return false;
    
    }else if( googleCalKey != "" && !validateEmail(googleCalKey)){
        toastr.warning('Please Enter valid calendar Email (Ex:abc@hostname.com)');
        $('#googleCalKey').focus();
        return false;
    
    }else if ( (department == null || department == undefined || department == "") ){
        toastr.warning("Please select department");
        addFocusId('department');
        return false;
    }else {
            
        // DEPARTMENT JSON - COMMA SEPERATED
        var departmentList = "";
        if( department != null && department != "" ){
            department.forEach(function(record,index) {
                if( departmentList == "" ){
                    departmentList += record;
                }else{
                    departmentList += ","+record;
                }
            });
        }
        
        // CREATE USER JSON DATA
        var userdata = {
            userName : username,
            fullName : fullname,
            userType : 1,
            // USERLOGINTYPE : userLoginType,
            address : address,
            contactNo : mobile,
            email : email,
            dashboard : 'V1.0',
            levelId : uLevelVal,
            // empCode : empCode,
            passWord : pwd,
            supervisorId : supName,
            designationId : desLevel,
            birthdate : ( (bDate!="")? ddmmyyToMysql(bDate) : "" ),
            anniversarydate : ( (annDate!="")? ddmmyyToMysql(annDate) : "" ),
			gender : uGender,
            googleCalKey: googleCalKey,
            department : departmentList
        }

        var role = "";
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "useredit",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(8,31),
            requestUserId: localStorage.indeCampusUserIdForEditUser,
            userdata: userdata,
            role: role,
            roleId: $('#roleLevel').val(),
            branch: $('#branchLevel').val(),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };

        console.log(postData);
        // return false;
        commonAjax(FOLLOWUPURL, postData, createUserDataCallback,"Please Wait... Getting Dashboard Detail");
    }
}

function createUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        toastr.success('User Updated Successfully');
        resetActiviytData();
        window.location.href = 'usermanaguser.html';

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_FAIL_EDIT_USER );
        else
            toastr.error(SERVERERROR);
    }
}

// GET USER DATA FOR UPDATE
function editUserListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "userdetails",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        requestedUserId : localStorage.indeCampusUserIdForEditUser
    };

    commonAjax(FOLLOWUPURL, postData, editUserListingCallback,"Please Wait... Getting Dashboard Detail");
}

// GET USER DATA FOR UPDATE CALLBACK
function editUserListingCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var userType = data.data[0].UERLOGINTYPE;
        var fullname = data.data[0].FULLNAME;
        var email = data.data[0].EMAIL;
        var mobile = data.data[0].CONTACT_NO;
        var username = data.data[0].USERNAME;
        //var pwd = data.data[0].;
        var supNameId = data.data[0].FK_SUPERVISOR_ID;
        var supName = data.data[0].SUPERVISORNAME;
        var address = data.data[0].ADDRESS;
		var bDate = data.data[0].BIRTHDATE;
		var anniDate = data.data[0].ANNIVERSARYDATE;
		var gender = data.data[0].GENDER;
        //var remarks = data.data[0].;
        var userLevelId = data.data[0].USER_LEVELID;
        var userDesignationId = data.data[0].FK_DESIGNATION_ID;
        var roleIds = data.data['permission'].roleIds;
        var branchIds = data.data['permission'].orgIds;
        var departmentIds = data.data[0].FK_DEPARTMENT_ID;

        var USER_G_CAL_ID = data.data[0].USER_G_CAL_ID;

        GBL_USERID = data.data[0].PK_USER_ID;

         $('#userLoginType').val( userType ).selectpicker('refresh')
         $('#fullname').val( fullname );
         $('#email').val( email );
//         $('#mobile').val( mobile );
        setNumberInIntl( 'mobile' , mobile );
         $('#username').val( username );
         $('#supName').val(supName);
         $('#supName').attr('itemid', supNameId );
         //$('#supName').val( supName );
         $('#address').val( address );
        
		 if( data.data[0].BIRTHDATE != "0000-00-00" ){
			$('#birthdate').val( mysqltoDesiredFormat ( (data.data[0].BIRTHDATE) , 'dd-MM-yyyy') );
		 }else{
			 $('#birthdate').val('') ;
		 }
		 
		 if( data.data[0].ANNIVERSARYDATE != "0000-00-00" ){
			$('#anniversaryDate').val( mysqltoDesiredFormat ( (data.data[0].ANNIVERSARYDATE) , 'dd-MM-yyyy') );
		 }else{
			 $('#anniversaryDate').val('') ;
		 }
		 
         if( data.data[0].GENDER != "" ){
            var $gen = $('input[type=radio][name="userGender"]');
            $($gen).filter('[value=' + gender + ']').click();
         }
		 

        $('#userLevel[name=levelSelect]').val(userLevelId);
        $('#userLevel').selectpicker('refresh');
        $("#desLevel").val(userDesignationId).selectpicker('refresh');

        $('#googleCalKey').val( USER_G_CAL_ID );
        
        if( data.data['permission'] != 'No record found' ){
            
            var wards = roleIds.split(",");
            var strWard = '[';
            for (i = 0; i < wards.length; i++) {
                strWard += '"' + wards[i] + '",';
            }
            strWard = strWard.slice(0, -1); //remove last comma
            strWard += ']';

            $("#roleLevel").selectpicker('val', JSON.parse(strWard));
            // $("#roleLevel").val([roleIds]).selectpicker('refresh');

            var wards = branchIds.split(",");
            var strWard = '[';
            for (i = 0; i < wards.length; i++) {
                strWard += '"' + wards[i] + '",';
            }
            strWard = strWard.slice(0, -1); //remove last comma
            strWard += ']';

            $('#branchLevel').selectpicker('val', JSON.parse(strWard));

            $("#roleLevel").val(data.data.permission.roleIds);
            $("#branchLevel").selectpicker('refresh');
            
        }
        // SET DEPARTMENT LIST
        if( departmentIds != "" && departmentIds != undefined ){
            var departList = departmentIds.split(",");
            var dpt = '[';
            for (i = 0; i < departList.length; i++) {
                dpt += '"' + departList[i] + '",';
            }
            dpt = dpt.slice(0, -1); //remove last comma
            dpt += ']';

            $("#department").selectpicker('val', JSON.parse(dpt));
        }

        var level =  $('#userLevel option:selected').attr('level');

        if( level == "0" ){

            $('.lowerLevel').show();
            $('#supervisorLevel').hide();
            
        }else if( level == "1" ){

            $('.lowerLevel').show();
            $('#supervisorLevel').show();
        
        }else{

            $('#supervisorLevel').show();
            $('.lowerLevel').show();
        }

    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }

}

// PROVIDE SEARCH FOR SUPERVISOR
function autoCompleteForSupervisor(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#supName').attr('itemId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                var levelVal = $('#userLevel').val();
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getSuperVisorListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8,30),
                    keyWord: query,
                    selectedLevelId: levelVal
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.userId,
                            name: data.fullName,
                            data: data.fullNames
                        }
                        ITEMS.push(tempJson);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveSuperWiserData(){

}

//START FUNCTION FOR CHANGE PASSWORD
function changePassword() {

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = GBL_USERID; //USER ID 
    var newPassword = $("#newpassword").val(); // NEW PASSWORD 
    var retypenewpassword = $("#retypenewpassword").val(); //RE TYPE PASSWORD
  
    if( newPassword == "" ) { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Enter New Password");
        $("#newpassword").focus();
        return false;

    }else if( retypenewpassword == "" ) { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Enter Re Type New Password");
        $("#retypenewpassword").focus();
        return false;

    }else if( !validatePassword( newPassword ) ){
        toastr.warning('Please Enter Strong Password');
        $('#newpassword').focus();
        return false;
    
    }else if( !validatePassword( retypenewpassword ) ){
        toastr.warning('Please Enter Strong Confirm Password');
        $('#retypenewpassword').focus();
        return false;

    }else if (newPassword != retypenewpassword) { //CHECK IF PASSWORD AND CONFIRM PASSWORD NOT MATCH
        toastr.warning("Password and Confirm Password doesn't Match");
        return false;
    }
    
    function afterChangedPassword(flag, data) {
        console.log(data);
        if (data.status == "Success" && flag) {
            toastr.success("Password Successfully Updated");
            window.location.href = 'usermanaguser.html';
        } else {
            if (flag){
                toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }                
            return false;
        }
    }

    var postData = {
        requestCase: 'resetPasswordUser',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        reqUserId: GBL_USERID,
        orgId: checkAuth(8,35),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        newpassword: md5(newPassword),
        confirmpassword :md5(retypenewpassword)
    }
    console.log(postData);
    // return false;
    commonAjax(COMMONURL, postData, afterChangedPassword, CHANGEPASSWORDMSG);
}

$('#reset-password').on('show.bs.modal',function(){
    $('#newpassword').val('');
    $('#retypenewpassword').val('');
});