/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 02-07-2018.
 * File : globalvar.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

// LIST OF LOCAL URLs

var localUrls = ["192.168.129.108","192.168.129.205","192.168.129.215", "192.168.129.108","localhost"];
var httpHead = returnHttps( localUrls );

var HOST = "localhost"; 
var PATH = "indecampus";

var FINALPATH = (PATH != "") ? HOST+"/"+PATH : HOST;
var COMMONURL = httpHead + "//"+FINALPATH+"/indeCampusProject/getData.php"; // Added // Added By Deepak patil on 30/03/2015
var ACTURL = httpHead + "//"+FINALPATH+"/indeCampusProject/external-API/eTASK/getData.php"; 
var CUSTURL = httpHead + "//"+FINALPATH+"/indeCampusProject/getCustomerLogin.php";   //  ADDED BY NAZIM KHAN FOR CUSTOMER DATA - 24th NOV 2017 
var DEVICERGURL = httpHead + "//"+FINALPATH+"/indeCampusProject/getDatadeviceRegistation.php"; 
var UPLOADURL = httpHead + "//"+FINALPATH+"/indeCampusProject/getDataUpload.php";

var FORGETPASSURL = httpHead + "//"+FINALPATH+"/indeCampusProject/getDataForgetPass.php"; // Added // Added By Deepak patil on 30/03/2015
var SEARCHURL = COMMONURL;
var FOLLOWUPURL = COMMONURL;
var URL2 = COMMONURL;

var MAP_API_KEY = "";
// var MAP_API_KEY = "AIzaSyDqkiJJ_IwNXSXITEzMWvGd8-gcZVacLtI";
if( localStorage.indeCampusRoleAuth != "" && localStorage.indeCampusRoleAuth != undefined ){
    roleAuth = JSON.parse(localStorage.indeCampusRoleAuth); 
    MAP_API_KEY = roleAuth.data.mapApiKey;
}

// ETRACK LOCAL & GLOBAL URL WITH KEY & WITHOUT KEY
// var eTrackApiUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyB4i9HGFWCoXAdEWR51Riy2lk8sVwFRvlw&signed_in=true&callback=initMap";
var eTrackApiUrl = "https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap";

var MOBILE_LENGTH = "10";   // SET MAX LENGTH OF MOBILE NUM ACCORDING TO COUNTRY CODE
var VERSION = 'V1.0';		// Version of Software for CSS and JS Auto Load
 

var CLOUDEVENTSOURCEPATH = httpHead + "//"+FINALPATH+"/indeCampusProject/cloudEventSource/serverTelephony.php";
var ALERTPATH = httpHead + "//"+FINALPATH+"/indeCampusProject/cloudEventSource/broadCastingAlert.php"; 

var TIMEOUT = 500000;	//Time out 500 seconds

var pageLoadChk = false;

var REMOVE_LOADER_TIME = 5000;  // IT WILL REMOVE THE LOADER FROM THE TYPE AHEAD SEARCH

if(localStorage.indeCampusRoleAuth != undefined){ 
    var config = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = config.data.clientTypeId;
} 

localStorage.indeCampusVersion = VERSION;

var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var monthsList = ['January','February','March','April','May','June','July','August','September','October','November','December'];

// RETURN FUNCTION HTTP HEADER
function returnHttps( urls ){
    
    var currUrl = document.URL

    var httpHead = "https:";
    urls.forEach( function( record , index ){
        if( currUrl.search( record ) != "-1" ){
            httpHead = "http:";
        }
    });
    
    return httpHead;
}

// /CONSTANT MESSAGES FOR AJAX CALL
var NOAUTH = "You are not Authorized for this activity";
var GBL_ERR_NO_CLIENT = "No Client are available";
var CREATINGINQDATA = "Super! Lemme create this inquiry for you!";
var RETRIEVINGROLEDETAIL = "Lemme check the permissions first.";
var RETRIEVINGMODULEDETAIL = "Lemme check the access for this user";
var SENDINGROLEDETAILTOCREATE = "Let me create this role for you";
var SENDINGGREETCREATE = "And this is how we should greet.";
var RETRIEVINGGREETDETAIL = "This is how we will greet.";
var CHECKDEVICE = "Please wait... Checking Your Device configuration and registering device.";
var CHANGEPASSWORDMSG = "Please wait... Resetting your Password.";
var CREATEDEVICE = "Please Wait, Creating Device";
var REVALIDATEDEVICE = "Please Wait, Device is revalidating";
var GETTINGDEVICE = "Please Wait, getting data of devices";
var GETTINGLOOKUP = "Please Wait, getting data of lookup";
var DELETELOOKUP = "Please Wait, Deleting lookup";
var CREATELOOKUP = "Please Wait, Creating lookup";
var UPDATELOOKUP = "Please Wait, Updating lookup";
var GETTINGTEMPLATES = "Please Wait, Getting Teplates";
var GETTINGPROPOSAL = "Please Wait, Getting Proposals";
var UPDATECLIENT = "Please Wait, Updating Client";
var CREATECLIENT = "Please Wait, Creating Client";
var GETTINGCLIENT = "Please Wait, Getting Clients";
var NOACTMODERIGHTS = "You dont have Rights of any activity mode";
var FORGTPASSWORD = "Sending detail to generate forgot password mail.";
var INQUIRYDETAILGET = "Please Wait... Getting Inquiry Detail";
var DELETEROLE = "Sure. Let me delete this role for you!!";
var CONTACTPERSONMAILID = "You haven't entered contact Person's Email ID ";
var NOPDF = "No data available to create pdf.";
var NOCSV = "No data available to create csv.";
var SCS = "Success";
var ROLENAMEBLANK = "Please Enter Rolename";

// CONSTANT ERROR MESSAGE OF API
var CNFDELETE = "Are you sure you want to delete this record ?";
var GBL_ERR_NO_LOOKUP = "No lookup available";
var GBL_ERR_NO_TYPE = "No developer type available";
var GBL_ERR_NO_STATUS_CAT = "No Status Category available";
var GBL_ERR_NO_USER_GROUP = "No Status Category available";
var GBL_ERR_PASSWORD_CHANGE_FAILED = "Unable to change password";
var GBL_ERR_CREATE_USER_FAIL = "Unable to create user";
var GBL_ERR_FAIL_EDIT_USER = "Unable to edit user";
var GBL_ERR_NO_USER = "No Users available";
var GBL_ERR_DELETE_USER = "Unable to delete user";
var GBL_ERR_ACT_INACT_USER = "Unable to active / deactive user";
var GBL_ERR_ACT_INACT_EVENT = "Unable to active / deactive event";
var GBL_ERR_TRANSFER_USER = "Unable to transfer user";
var GBL_ERR_ROLE_LIST = "No Role available";
var GBL_ERR_PERMISSION_LIST = "No permissions available";
var GBL_ERR_ADD_UPDATE_ROLE_LIST = "Unable to add / update Role";
var GBL_ERR_DELETE_ROLE = "Not able to delete role";
var GBL_ERR_NO_USER_LEVEL = "No User Levels available";
var GBL_ERR_FAIL_CREATE_EDIT_USERLEVEL = "Not able to add / update user level";
var GBL_ERR_UPDATE_BRANCH = "Not able to update branch";
var GBL_ERR_NO_CUST_DATA = "No customers available";
var GBL_ERR_NO_TRACKING = "No Tracking available";
var GBL_ERR_CREATE_UPDATE_CLIENT_FAIL = "Unable to create / update client";
var GBL_ERR_NO_ITEMS = "No items available";
var GBL_ERR_NO_SERVICE = "No service list available";
var GBL_ERR_CRE_UPD_ITEM = "Unable to create / update item";
var GBL_ERR_CRE_UPD_ROOM = "Unable to create / update room";
var GBL_ERR_CRE_UPD_EVENT = "Unable to create / update event";
var GBL_ERR_DELETE_ROOM = "Unable to delete room";
var GBL_ERR_NO_STUDENT = "No student enrollment available";
var GBL_ERR_NO_STUDENT_DET = "No student enrollment detail available";
var GBL_ERR_NO_STUDENT_FILTER = "No student found in this search criteria";
var GBL_ERR_NO_EVENT_FILTER = "No event found in this search criteria";
var GBL_ERR_ADD_ENROLL = "Unable to create / update enrollment";
var GBL_ERR_DELETE_ENROLL = "Unable to delete enrollment";
var GBL_ERR_NO_CITY_STATE = "No City / State available of this country";
var GBL_ERR_NO_ATTACH = "No Attachment Available";
var GBL_ERR_ATTACH_DELETE_FAIL = "Unable to delete attachment";
var GBL_ERR_NO_ROOMS = "No room numbers available in this room";
var GBL_ERR_ASSIGN_ALLOT = "Unable to add / update allotment details";
var GBL_ERR_NO_ALLOT_DATA = "No allotment assigned to this student";
var GBL_ERR_NO_PROP = "No property list available";
var GBL_ERR_ADD_INQ = "Unable to create inquiry on this day for the venue";
var GBL_ERR_NO_STUD_LIST =  "No students available";
var GBL_ERR_NO_EMAIL = "No Email available";
var GBL_ERR_NO_ADDRESS = "No Email available";
var GBL_ERR_UPDATE_STUDENT_FAILED = "Not able to update Student";
var GBL_ERR_UNPROC_INV = "No unprocessed invoices available";
var GBL_ERR_NO_INVOICE = "No invoices available";
var GBL_ERR_NO_PAYMENT = "No Payment available";
var GBL_ERR_NO_LEDGER = "No Ledger available";
var GBL_ERR_NO_ROOM_TYPE = "No room type list available";
var GBL_ERR_NO_FACILITY_TYPE = "No faclitiy type list available";
var GBL_ERR_NO_ROOM_CAT = "No room type category available";
var GBL_ERR_ADD_CHARGE_POST = "Unable to add charge post";
var GBL_ERR_NO_CHARGE_POST = "No charge posting data available";
var GBL_ERR_STU_STATUS = "Unable to change student's status";
var GBL_ERR_PSWD_CHNG_ENR = "Unable to change password for student / parent";
var GBL_ERR_NO_PAYMENT = "No Payments available";
var GBL_ERR_NO_PAY_INVOICE = "No Payment invoices available";
var GBL_ERR_ADD_PAY = "Unable to collect payment";
var GBL_ERR_INV_TRANS = "No invoice transaction history available";
var GBL_ERR_INV_TRANS_CANCEL = "Unable to cancel invoice";
var GBL_ERR_INVOICE = "No invoice details available";
var GBL_ERR_INV_MAIL = "Unable to send invoice mail";
var GBL_ERR_NO_EVENTS = "No events available";
var GBL_ERR_NO_EVENT_DETAIL = "No event details available";
var GBL_ERR_NO_EVENT_BOOK_INV = "No booking invoice available";
var GBL_ERR_NO_REQ_COM = "No Request / Complaint available";
var GBL_ERR_CRE_UPD_REQ_COMP = "Unable to create / update request or complaint";
var GBL_ERR_REOPEN_COMP = "Unable to reopen request or complaint";
var GBL_ERR_CAN_COMP = "Unable to cancel request or complaint";
var GBL_ERR_CRE_UPD_DEPT_CAT = "Unable to create / update department category";
var GBL_ERR_NO_DEPT_CAT = "No Departmental category available";
var GBL_ERR_NO_DEPT_SUB_CAT = "No Departmental sub category available";
var GBL_ERR_DEL_DEPT_CAT = "Unable to delete department category";
var GBL_ERR_ASS_COMP = "Unable to assign user for request / complaint";
var GBL_ERR_NO_REQ_COM_DET = "No Request / Complaint detail available";
var GBL_ERR_NO_CLOSE_REQ = "Unable to close request / complaint";
var GBL_ERR_DEL_CHARGE_POST = "Unable to delete charge post";
var GBL_ERR_UPD_CHARGE_QTY = "Unable to update charge post quantity";
var GBL_ERR_NO_NON_PR_CHG = "No non-process charges available";
var GBL_ERR_GEN_INVOICE = "No invoices available to generate";
var GBL_ERR_RENEW = "Unable to renew enrollment";
var GBL_ERR_NO_SUS_DATA = "No suspension / termination data available";
var GBL_ERR_ADD_SUS_TER = "Unable to create / update student's suspension / termination";
var GBL_ERR_REVOKE_SUS_TER = "Unable to revoke student's suspension / termination";
var GBL_ERR_DELETE_SUS = "Unable to delete suspension";
var GBL_ERR_BOOK_EVENT = "Unable to book this event";
var GBL_ERR_TERMINATE = "Unable to proceed termination of this student";
var GBL_ERR_NO_FILTER = "No records found in this search criteria";
var GBL_ERR_NO_PAGE_CONTENT = "No Page Content List Available";
var GBL_ERR_Update_PAGE_FAILED = "Unable to create page content";
var GBL_ERR_UPDATE_SECTION_FAILED = "Unable to update page content";
var GBL_ERR_Delete_PAGE_FAILED = "Unable to delete page content";
var EMPTYUSERNAME = "User Name Can't Be Blank..!!";
var EMPTYPWD = "Password Can't Be Blank..!!";
var GBL_ERR_NON_AVAIL_DATA = "No non availing services data found";
var GBL_ERR_NON_AVAIL_DEL = "Unable to delete non availing services";
var GBL_ERR_NO_STATUS = "No Status available";
var EMAILVALID = "Enter Valid Email Address";
var GBL_ERR_MAIL_SEND = "Not able to send mail";
var GBL_ERR_LOGIN = "Not able to login";
var GBL_ERR_OTP = "OTP is not valid";
var GBL_ERR_REG_ALERT = "Not able to register alert";
var GBL_ERR_ADD_NON_AVAIL = "Unable to add non availing service";
var GBL_ERR_DEL_EVENTS = "Unable to delete this event";
var GBL_ERR_CREATE_STATUS = "Not able to create status"; // ADDED BY DEEPAK
var GBL_ERR_CRE_RECEIPT = "Unable to create receipt"; // ADDED BY KAUSHA SHAH ON16-08-2018 
var GBL_ERR_TALLY_INTEGRATION_NOT_ALLOW = "Tally integration is not allowed"; // ADDED BY KAUSHA SHAH ON 24-08-2018 

var GBL_ERR_NOTI_STATUS_UPDATE_FAILED = "Not able to Update Notification status";
var GBL_ERR_USAGE_DATA = "No Usage history available";

// STUDENT STATUS
var STUDENT_STATUS = [{
            ID: 0,
            VALUE: "Inactive"
        },
        {
            ID: 1,
            VALUE: "Active"
        },
        {
            ID: 2,
            VALUE: "Termination"
        },
        {
            ID: 3,
            VALUE: "Suspension"
        }
    ];

// GENDER
var ENROLL_GENDER = [{
            ID: "Male",
            VALUE: "Male"
        },
        {
            ID: "Female",
            VALUE: "Female"
        },
        {
            ID: "Transgender",
            VALUE: "Transgender"
        },
        {
            ID: "Other",
            VALUE: "Other"
        }
    ];

// REQUEST / COMPLAINT STATUS
var REQUEST_STATUS = [{
            ID: 0,
            VALUE: "Pending"
        },
        {
            ID: 1,
            VALUE: "Resolved"
        },
        {
            ID: 2,
            VALUE: "Close"
        },
        {
            ID: 3,
            VALUE: "Reopen"
        },
        {
            ID: 4,
            VALUE: "Canceled"
        }
    ];

var gbl_ActivityAppGlobals; // we'll access all the static global vars from this variable.
var gbl_ActivityAppRoleAuth; // we'll access all the dynamic role auth variables from this.

var initActivityAppGlobals = function () {

    var URLV = ACTURL;
    var SCS = "Success";
    var FAIL = "Fail";

    var ACT_MODE_PHONE = "1";
    var ACT_MODE_EMAIL = "2";
    var ACT_MODE_SMS = "3";
    var ACT_MODE_PERSONALMEET = "4";
    var ACT_STATUS_PENDING = "0";
    var ACT_STATUS_INPROCESS = "1";
    var ACT_STATUS_CLOSED = "2";
    // ACT_STATUS : 0=Pending,1=In process,2=Closed

    var ACT_FREQUENCY = [{
            label: "Daily",
            value: "1"
        },
        {
            label: "Weekly",
            value: "7"
        },
        {
            label: "Monthly",
            value: "30"
        },
        {
            label: "Quarterly",
            value: "90"
        },
        {
            label: "Yearly",
            value: "365"
        }];

    var AUTH_CREATE = true;
    var AUTH_MODIFY = true;
    var AUTH_ASSIGN = true;
    var AUTH_REASSIGN = true;
    var AUTH_CANCEL = true;
    var AUTH_TRANSFER = true;

    var AJAXMSG_GTACTLIST = "Lemme grab your activities..sit tight..";
    var AJAXMSG_GTACTDATA = "Lemme get the details of this activity..";
    var AJAXMSG_GTADDRMRK = "Lemme add this remark..";
    var AJAXMSG_PRCSREQ = "Processing your request..";

    var MSG_NOAUTH = "You are not authorized to use this feature..";
    var globals = {
        URLV: URLV,
        SCS: SCS,
        FAIL: FAIL,
        ACT_MODE_PHONE: ACT_MODE_PHONE,
        ACT_MODE_EMAIL: ACT_MODE_EMAIL,
        ACT_MODE_SMS: ACT_MODE_SMS,
        ACT_MODE_PERSONALMEET: ACT_MODE_PERSONALMEET,
        ACT_STATUS_PENDING: ACT_STATUS_PENDING,
        ACT_STATUS_INPROCESS: ACT_STATUS_INPROCESS,
        ACT_STATUS_CLOSED: ACT_STATUS_CLOSED,
        ACT_FREQUENCY: ACT_FREQUENCY,
        AJAXMSG_GTACTLIST: AJAXMSG_GTACTLIST,
        AJAXMSG_GTACTDATA: AJAXMSG_GTACTDATA,
        AJAXMSG_GTADDRMRK: AJAXMSG_GTADDRMRK,
        AJAXMSG_PRCSREQ: AJAXMSG_PRCSREQ,
        MSG_NOAUTH: MSG_NOAUTH,
    };


    var roleAuth = {
        AUTH_CREATE: AUTH_CREATE,
        AUTH_MODIFY: AUTH_MODIFY,
        AUTH_ASSIGN: AUTH_ASSIGN,
        AUTH_REASSIGN: AUTH_REASSIGN,
        AUTH_CANCEL: AUTH_CANCEL,
        AUTH_TRANSFER: AUTH_TRANSFER,
    };

    gbl_ActivityAppGlobals = globals;
    gbl_ActivityAppRoleAuth = roleAuth;
}

initActivityAppGlobals();