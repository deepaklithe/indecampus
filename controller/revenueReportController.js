    /*
 *
 * Created By : Kavita Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 08-08-2018.
 * File : revenueReportController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';
var GBL_REVENUEREPORTDATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    getRevenueReportList(); // GET EVENTS LISTING DATA
    autoCompleteForStudentId('studentName',saveStudent);
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
    $("#monthFrom").attr('readonly',true);
    $("#monthTo").attr('readonly',true);
}

// GET EVENTS LISTING DATA
function getRevenueReportList(){

    var studentId = $('#studentName').attr('studId');
    var monthFrom = $('#monthFrom').val();
    var monthTo = $('#monthTo').val();
    var invoiceType = $('#invoiceType').val();

    if( monthFrom != "" && monthTo != "" ){

        var stMonth = monthFrom.split('-').reverse().join('-');
        var enMonth = monthTo.split('-').reverse().join('-');
        var dateStartDate = new Date(ddmmyyToMysql(stMonth));
        var dateEndDate = new Date(ddmmyyToMysql(enMonth));

        //START END DATE VALIDATION
        if (+dateEndDate < +dateStartDate) { 
            toastr.warning("End month cannot be less than the Start month");
            addFocusId('monthTo');
            return false;

        }
    }

    if( $('#studentName').val() == "" ){
        studentId = "";
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'revenueReport', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(26,105),
    	studentId : studentId,
    	fromMonth : monthFrom,
    	toMonth : monthTo,
    	invoiceType : invoiceType,
    	invoiceNumber : "",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRevenueListCallback,"Please wait...Adding your item.");

    function getRevenueListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_REVENUEREPORTDATA = data.data;
            renderRevenueReportList(GBL_REVENUEREPORTDATA); // RENDER ENROLLMENT REPORT DATA
            $('.Sidenav').removeClass('open');  
            
        }else{

            GBL_REVENUEREPORTDATA = [];
            GBL_EXCELJSON = [];
            renderRevenueReportList(GBL_REVENUEREPORTDATA); // RENDER EVENTS
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_FILTER );
            else
                toastr.error(SERVERERROR);
        }
        // resetFilters();
    }

}

// RENDER ITEM LISTING DATA
function renderRevenueReportList(eventData){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="enrollmentViewRepoTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Membership ID</th>'+
                        '<th>Student Name</th>'+
                        '<th>Acedamic Year</th>'+
                        '<th>Invoice Date</th>'+
                        '<th>Invoice Number</th>'+
                        '<th>Invoice Type</th>'+
			            '<th>Invoice Amount</th>'+
                        '<th>Status</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var tempJson = {
        srNo : "#",
        studMemNo: "Membership ID",
        studName : "Student Name",
        academicYear : "Academic Year",
        invoiceDate : "Invoice Date",
        invoiceNumber : "Invoice Number",
        invoiceType : "Invoice Type",
        invoiceAmount : "Invoice Amount",
        status : "Status",
    }
    GBL_CSVJSON.push(tempJson);

    var innerRow = '';
    if( eventData.length != 0 && eventData != "No record found" ){

        eventData.forEach(function( record , index ){
            
            var invoiceDate = (record.invoiceDate != "0000-00-00" && record.invoiceDate != "1970-01-01" && record.invoiceDate != "" ? mysqltoDesiredFormat(record.invoiceDate,'dd-MM-yyyy') : "" );

            var tempJson = {
                srNo : (index+1),
                studMemNo: record.memberShipId,
                studName : record.studentName,
                academicYear : record.acdemicYear,
                invoiceDate : invoiceDate,
                invoiceNumber : record.invoiceNumber,
                invoiceType : record.invoiceType,
                invoiceAmount : record.invoiceAmount,
                status : record.status,
            }
            GBL_CSVJSON.push(tempJson);


            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.memberShipId +'</td>'+
                            '<td>'+ record.studentName +'</td>'+
                            '<td>'+ record.acdemicYear +'</td>'+
                            '<td>'+ (record.invoiceDate != "0000-00-00" && record.invoiceDate != "1970-01-01" && record.invoiceDate != "" ? mysqltoDesiredFormat(record.invoiceDate,'dd-MM-yyyy') : "" ) +'</td>'+ // Date format Change BY KAVITA PATEL ON 22-08-2018
                            '<td>'+ record.invoiceNumber +'</td>'+
                            '<td>'+ record.invoiceType +'</td>'+
                            '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.invoiceAmount) +'</td>'+
                            '<td>'+ record.status +'</td>'+
                            
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    GBL_EXCELJSON =  thead + innerRow + tEnd;
    $('#enrollmentViewRepoTbl').DataTable();

}

// RESET FILTER
function resetFilters(){

    $('#studentName').attr('studId','');
    $('#studentName').val('')
    $('#monthFrom').val('');
    $('#monthTo').val('');
    $('#invoiceType').val('').selectpicker('refresh');
    $("#monthFrom,#monthTo").datepicker('remove');
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderEventList(GBL_REVENUEREPORTDATA); // RENDER ORIGINAL WITHOUT FILTER DATA

}

function saveStudent(){

}

function setMonthTo(context){

    var monthFrom = $(context).val();

    $('#monthTo').datepicker('remove');
    $('#monthTo').datepicker({
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        startDate: monthFrom,
        autoclose:true
    });

}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_REVENUEREPORTDATA.length < 1 || GBL_REVENUEREPORTDATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Revenue Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_REVENUEREPORTDATA.length < 1 || GBL_REVENUEREPORTDATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Revenue_Report.pdf",20);
    }

}