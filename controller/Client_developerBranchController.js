/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : Client_developerBranchController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var branchData = [];

// GET CLIENT LISTING DATA
function getClientData(){

    $('#clientList').focus();

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getClientListing",
        clientId: newClientId,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getClientDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getClientDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var newRow = '';
        if(newClientId == -1){
            for( var i=0; i<data.data.length; i++ ){
                newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
            }
        }else{
            for( var i=0; i<data.data.length; i++ ){
                if(newClientId ==data.data[i].clientId ){
                    newRow += '<option value="'+ data.data[i].clientId +'">'+ data.data[i].clientName +'</option>';
                }
            }
        }
        

        $('#clientList').html(newRow);
        
        $('#clientList').val( newClientId ).attr('disabled',true).selectpicker('refresh'); // SET CLIENT LSITING IN DROPDOWN

        initTelInput( 'branchMobile' );   // INITIALIZE COUNTRY WISE MOBILE NUM

        getBranchListing();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_CLIENT );
        else
            toastr.error(SERVERERROR);
    }
}

// GET BRANCH LISTING DATA
function getBranchListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "branchListing",
        clientId: newClientId,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(5,18),
        dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getBranchListingDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getBranchListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        branchData = data.data;

        branchDataRender();

        addFocusId( 'clientList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_BRANCH );
        else
            toastr.error(SERVERERROR);
    }
}

// RENDER BRANCH DATA
function branchDataRender(){

    GBL_BRANCH_LIST = [];
   var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Client</th>'+
                        '<th>Name</th>'+
                        '<th>Address</th>'+
                        '<th>Email</th>'+
                        '<th>Mobile</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    if(branchData != 'No record found'){
        // for( var i=0; i<branchData.length; i++ ){

        var count = 0;
        branchData.forEach( function( record , index ){

            if( record.clientId == newClientId ){

                GBL_BRANCH_LIST.push( record );
                count = count + 1;
                newRow += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td>'+ record.clientName +'</td>'+
                                '<td>'+ record.orgName +'</td>'+
                                '<td>'+ record.orgAddress +'</td>'+
                                '<td>'+ record.orgEmail +'</td>'+
                                '<td>'+ record.orgMobile +'</td>'+
                                '<td>'+
                                    '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" org-id="'+ record.orgId +'" name="'+ record.orgName +'" address="'+ record.orgAddress +'" email="'+ record.orgEmail +'" mobile="'+ record.orgMobile +'" onclick="updateBranch(this)"><i class="fa fa-pencil"></i></a>'+
                                    '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" org-id="'+ record.orgId +'" onclick="deleteBranch(this);"><i class="fa fa-trash-o"></i></a>'+
                                '</td>'+
                            '</tr>';
            }
        })
        // }

        $('#moduleList').html(newRow);
        
    }else{
            displayAPIErrorMsg( "" , GBL_ERR_NO_BRANCH );
    }

    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable({ "stateSave": true });
}

// CREATE A NEW BRANCH
function createBranch(){
    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(5,17,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){
        var requestClientId = $('#clientList').val();
        var branchName = $('#branchName').val().trim();
        var branchAddress = $('#branchAddress').val().trim();
        var branchEmail = $('#branchEmail').val().trim();
        var branchMobile = getIntlMobileNum("branchMobile");    // GET MOBILE NUM
        // var branchMobile = $('#branchMobile').val().trim();

        if( branchName.length == 0 ){
            toastr.warning("Please Enter Branch Name");
            $('#branchName').focus();
            return false;

        }else if( branchAddress.length == 0 ){
            toastr.warning("Please Enter Branch Address");
            $('#branchAddress').focus();
            return false;
        
        }else if( branchEmail.length == 0 ){
            toastr.warning("Please Enter Branch Email Address");
            $('#branchEmail').focus();
            return false;

        }else if( !validateEmail(branchEmail) ){
            toastr.warning('Please Enter valid Branch EMail Address');
            $('#branchEmail').focus();
            return false;
        
        }else if( branchMobile.length == 0 ){
            toastr.warning("Please Enter Branch Mobile No");
            $('#branchMobile').focus();
            return false;

        }else if( !checkValidMobile( "branchMobile" ) ){
        // }else if( branchMobile.length != MOBILE_LENGTH ){
            toastr.warning('Please Enter Valid Branch Mobile No');
            $('#branchMobile').focus();
            return false;

        }else{

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "createEditBranch",
                clientId: newClientId,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(5,17),
                dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID),
                requestClientId : requestClientId,
                branchName : branchName,
                branchAddress : branchAddress,
                branchEmail : branchEmail,
                branchMobile : branchMobile
            };

            commonAjax(FOLLOWUPURL, postData, createBranchCallback,"Please Wait... Getting Dashboard Detail");
        }
    }
}

function createBranchCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var requestClientId = $('#clientList').val();
        var branchName = $('#branchName').val().trim();
        var branchAddress = $('#branchAddress').val().trim();
        var branchEmail = $('#branchEmail').val().trim();
        var branchMobile = getIntlMobileNum("branchMobile");    // GET MOBILE NUM

        var tempArray = {
            clientName: $('#clientList option:selected').text(),
            orgAddress: branchAddress,
            clientId: requestClientId,
            orgId: data.data[0].orgId,
            orgName: branchName,
            orgEmail : branchEmail,
            orgMobile : branchMobile
        }
        
        branchData.push(tempArray);

        toastr.success('New Branch has been created');

        branchDataRender();

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_BRANCH );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
var branchIdForDelete = '';
// DELETE SELECTED BRANCH
function deleteBranch(context){

    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(5,20,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){
        var deleteRow = confirm('Are You sure ? You want to delete this Record');

        if( deleteRow ){

            branchIdForDelete = $(context).attr('org-id');
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

            gblContext = context;

            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: "deleteBranch",
                clientId: newClientId,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(5,20),
                dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID),
                branchId : branchIdForDelete
            };

            commonAjax(FOLLOWUPURL, postData, deleteBranchCallback,"Please Wait... Getting Dashboard Detail");
        }
    }
}

function deleteBranchCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $(gblContext).parents('td').parents('tr').remove(); 
        
        var index = findIndexByKeyValue( branchData,'orgId',branchIdForDelete );

        branchData.splice(index,1);

        toastr.success('Branch Deleted Successfully');
        branchDataRender();

        resetData();
        
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_BRANCH );
        else
            toastr.error(SERVERERROR);
    }
}

// SET SELECTED BRANCH DATA TO UPDATE
function updateBranch(context){
    
    var flag = true;
    if(localStorage.indeCampusUserType != "-1"){
        if(checkAuth(5,19,1) == -1){
            toastr.error('You are not authorised to perform this activity');
            flag = false;
            return false;
        }
    }
    
    if(flag){
        localStorage.indeCampusBranchIdForUpdate = "";

        var orgId = $(context).attr('org-id');
        var name = $(context).attr('name');
        var address = $(context).attr('address');
        var mobile = $(context).attr('mobile');
        var email = $(context).attr('email');

        localStorage.indeCampusBranchIdForUpdate = orgId;

        $('#createBranchBtn').attr('onclick','editBranch()');
        // $('#addViewLogoBtn').show();

        $('#branchName').val(name);
        $('#branchAddress').val(address);
        $('#branchEmail').val(email);
//        $('#branchMobile').val(mobile);
        setNumberInIntl( 'branchMobile' , mobile );
    }
    addFocusId( 'clientList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

// UPDATE SELECTED BRANCH DATA
function editBranch(){

    var branchName = $('#branchName').val().trim();
    var branchAddress = $('#branchAddress').val().trim();
    var branchId = localStorage.indeCampusBranchIdForUpdate;
    var branchEmail = $('#branchEmail').val().trim();
    // var branchMobile = $('#branchMobile').val().trim();
    var branchMobile = getIntlMobileNum("branchMobile");    // GET MOBILE NUM

    if( branchName.length == 0 ){

        toastr.warning("Please Enter Bracnch Name");
        $('#branchName').focus();
        return false;
        
    }else if( branchEmail.length == 0 ){
        toastr.warning("Please Enter Branch Email Address");
        $('#branchEmail').focus();
        return false;

    }else if( !validateEmail(branchEmail) ){
        toastr.warning('Please Enter valid Branch EMail Address');
        $('#branchEmail').focus();
        return false;
    
    }else if( branchMobile.length == 0 ){
        toastr.warning("Please Enter Branch Mobile No");
        $('#branchMobile').focus();
        return false;

    // }else if( branchMobile.length != MOBILE_LENGTH ){
    }else if( !checkValidMobile( "branchMobile" ) ){
        toastr.warning('Please Enter Valid Branch Mobile No');
        $('#branchMobile').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createEditBranch",
            clientId: newClientId,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(5,19),
            dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID),
            branchName : branchName,
            branchAddress : branchAddress,
            branchId : branchId,
            branchMobile : branchMobile,
            branchEmail : branchEmail
        };

        commonAjax(FOLLOWUPURL, postData, editBranchCallback,"Please Wait... Getting Dashboard Detail");
    }
}

function editBranchCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var index = findIndexByKeyValue( branchData,'orgId',localStorage.indeCampusBranchIdForUpdate )

        var requestClientId = $('#clientList').val();
        var branchName = $('#branchName').val().trim();
        var branchAddress = $('#branchAddress').val().trim();
        var branchEmail = $('#branchEmail').val().trim();
        var branchMobile = getIntlMobileNum("branchMobile");    // GET MOBILE NUM

        var tempArray = {
            clientName: $('#clientList').text(),
            orgAddress: branchAddress,
            orgId: localStorage.indeCampusBranchIdForUpdate,
            orgName: branchName,
            orgMobile: branchMobile,
            orgEmail: branchEmail
        }
        
        branchData[index] = tempArray;

        toastr.success('Branch has been Updated');
        $('#createBranchBtn').attr('onclick','createBranch()');

        $('#addViewLogoBtn').hide();
        branchDataRender();

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_BRANCH );
        else
            toastr.error(SERVERERROR);
    }
}

// RESET FIELDS
function resetData(){

    $('#branchName').val('');
    $('#branchAddress').val('');
    $('#branchEmail').val('');
    $('#branchMobile').val('');
    $('#createBranchBtn').attr('onclick','createBranch()');
    $('#addViewLogoBtn').hide();
}

function openAttachment(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "logoAttachmentListing",
        clientId: newClientId,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: localStorage.indeCampusBranchIdForUpdate,
        dataValue: generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, sendAttachmentCallBack,CREATINGINQDATA);

    function sendAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            attachmetData = data.data;
            renderLogoAttachment();

        }else {
            attachmetData = [];
            renderLogoAttachment();
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ATTACH );
            else
                toastr.error(SERVERERROR);
        }
    }
    $('#attachmentAddLogo').modal('show');
}

function renderLogoAttachment(){
    var attachLi = ''
    for( var i=0; i<attachmetData.length; i++ ){
        attachLi += '<li class="">'+
                        '<div class="col-lg-12">'+
                            '<div class="card card-image card-light-blue bg-image bg-opaque8">'+
                                '<img src="'+ attachmetData[i].fileLink +'" alt="" class="gallery-image">'+
                                '<div class="context has-action-left has-action-right">'+
                                    '<div class="tile-content">'+
                                        '<span class="text-title">'+ attachmetData[i].fileName +'</span>'+
                                        '<span class="text-subtitle">'+ (attachmetData[i].fileName).split('.')[1] +'</span>'+
                                    '</div>'+
                                    '<div class="tile-action right">'+
                                        '<a href="javascript:void(0)" attchId="'+ attachmetData[i].fileId +'" onclick="deleteAttachment(this)" class="btn btn-sm btn-warning">'+
                                            '<i class="fa fa-trash"></i>'+
                                        '</a>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</li>';
    }

    $('#AttachmentListing').html( attachLi );
}

function assignLogoAttachementEvent(){
    // grab your file object from a file input
    $('#attachmentLogo').change(function () {
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "logoAttachmentUpload",
            clientId: newClientId,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: localStorage.indeCampusBranchIdForUpdate,
            dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID),
        };
        $("#postDataAttachLogo").val( JSON.stringify(temp_json));

        $("#fileuploadLogo").off().on('submit',(function(e) {
            addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",       // Type of request to be send, called as method
                data:  new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                processData:false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function(data)// A function to be called if request succeeds
                {
                    data = JSON.parse(data);
                    console.log(data);
                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    if(data.status == "Success"){
                        
                        console.log(data.data);
                        attachmetData = [];
                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            clientId: data.data.clientId,
                        }

                        attachmetData.push( tempData );

                        renderLogoAttachment();
                    }else{
                        toastr.warning(data.status);

                    }
                },
                error: function (jqXHR, errdata, errorThrown) {
                    addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                    log("error");
                }
            });
        }));
        $("#fileuploadLogo").submit();
        $('#attachmentLogo').val('');
    });
}

function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attchId');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'logoAttachmentListingDelete',
        clientId: newClientId,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: localStorage.indeCampusBranchIdForUpdate,
        dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID),
        attachId : attachmentId
    };
    commonAjax(INQURL1, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var index = findIndexByKeyValue( attachmetData , "fileId" , attachmentId );
            
            attachmetData.splice( index , 1 );

            renderLogoAttachment();

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
            else
                toastr.error(SERVERERROR);
        }
    }
    
}