    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 31-07-2018.
 * File : eventMgmtListController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';
var GBL_EVENT_DATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    setOption("0",'gender',ENROLL_GENDER,'Select Gender'); // SET GENDER FROM GLOBAL VAR
    $('#gender').selectpicker('refresh');
    getFilteredEnrollementReportdata();
    autoCompleteForStudentId('studentName',saveStudent);
}


// RENDER ITEM LISTING DATA
function renderEnrollmentReportList(eventData){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="enrollmentViewRepoTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Student Name</th>'+
                        '<th>Membership ID</th>'+
                        '<th>Enrollment Date</th>'+
                        '<th>Academic Year</th>'+
                        '<th>Enrollment Type</th>'+
                        '<th>Gender</th>'+
                        '<th>Mobile Number</th>'+
                        '<th>Status</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var innerRow = '';

    var temp = {
        srNo : "#",
        studName : "Student Name",
        studMemNo: "Membership ID",
        enrollDate : "Enrollment Date",
        academicYear : "Academic Year",
        enrollType : "Enrollment Type",
        gender : "Gender",
        mobileNo : "Mobile Number",
        status : "Status",
    }
    GBL_CSVJSON.push(temp);

    var count = 0;
    if( eventData.length != 0 && eventData != "No record found" ){

        eventData.forEach(function( record , index ){
            var eventStatus = 'Active';
            var disabledClass = '';
            var btnClass = 'btn-success';
            if( record.status == "0" ){
                eventStatus = 'Inactive';
                disabledClass = 'disabled';
                btnClass = 'btn-danger';
            }
            var studStatus ='';
            var studStatusIndex = findIndexByKeyValue( STUDENT_STATUS , 'ID' , record.status );
            if( studStatusIndex != -1 ){
                studStatus = STUDENT_STATUS[studStatusIndex].VALUE;
            }
            count = count + 1;

            // PDF JSON
            var temp = {
                srNo : count,
                studName : record.studentName,
                studMemNo: record.memberShipId,
                enrollDate : record.enrollmentDate,
                academicYear : record.acdemicYear,
                enrollType : record.enrollmentType,
                gender : record.gender,
                mobileNo : record.mobileNo,
                status : studStatus,
            }
            GBL_CSVJSON.push(temp);

            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.studentName +'</td>'+
                            '<td>'+ record.memberShipId +'</td>'+
                            '<td>'+ record.enrollmentDate +'</td>'+
                            '<td>'+ record.acdemicYear +'</td>'+
                            '<td>'+ record.enrollmentType +'</td>'+
                            '<td>'+ record.gender +'</td>'+
                            '<td>'+ record.mobileNo +'</td>'+
                            '<td>'+ studStatus +'</td>'+
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    GBL_EXCELJSON = thead + innerRow + tEnd;
    $('#enrollmentViewRepoTbl').dataTable();

}

// GET EVENTS ON SELECTED FILTERS
function getFilteredEnrollementReportdata(){
    
    var enrollmentType = $('#enrollmentType').val();
    var gender = $('#gender').val();
    var studentName = $('#studentName').attr('studId');
    if( gender == "-1" ){
        gender = "";
    }
    if( enrollmentType == "-1" ){
        enrollmentType = "";
    }
    if( $('#studentName').val() == "" ){
        studentName = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'studentEnrollmentReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(26,105),
        studentId : studentName,
        enrollmentType : enrollmentType,
        gender : gender,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getFilteredStudentsDataCallback,"Please Wait... Getting Dashboard Detail");

    function getFilteredStudentsDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
            console.log(data);
            GBL_EVENT_DATA = data.data;
            renderEnrollmentReportList(data.data); // RENDER FILTERED STUDENT LIST
            $('.Sidenav').removeClass('open');  
            // resetFilters(); // RESET FILTER VALUES

        }else{
            GBL_EVENT_DATA = [];
            GBL_EXCELJSON = [];
            if (flag){
                if( data.status == "No record found" ){
                    renderEnrollmentReportList([]); // RENDER FILTERED STUDENT LIST
                    $('.Sidenav').removeClass('open');  
                    // resetFilters(); // RESET FILTER VALUES
                }
                displayAPIErrorMsg( data.status , "No enrollment found on this search criteria" );
                // toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }
        }
    }

}

// RESET FILTER
function resetFilters(){

    $('#enrollmentType').val('').selectpicker('refresh');
    $('#gender').val('').selectpicker('refresh');
    $('#studentName').val('');
    $('#studentName').attr('studId','');
   
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderEnrollmentReportList(GBL_ENROLLMENTREPORTDATA); // RENDER ORIGINAL WITHOUT FILTER DATA

}

function saveStudent(){

}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_EVENT_DATA.length < 1 || GBL_EVENT_DATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Enrollment Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_EVENT_DATA.length < 1 || GBL_EVENT_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Enrollment_Report.pdf",20);
    }

}