/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportData = [];
var GBL_EXCELJSON = [];

function getReportData(){

    reportData = [];
    GBL_EXCELJSON = [];

    var stDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var assTo = $('#assTo').attr('assid');
    var creatTo = $('#creatTo').attr('createdid');
    var actStatusListing = $('#actStatusListing').val();
    var delayType = $('#delayType').val();
    // var projectList = $('#projectList').val();

    if( stDate != "-1" && stDate != "" ){
        stDate = ddmmyyToMysql(stDate);
    }else{
        stDate = "-1";
    }
    if( endDate != "-1" && endDate != "" ){
        endDate = ddmmyyToMysql(endDate);
    }else{
        endDate = "-1";
    }
    
    if($('#assTo').val().length <= 0){
        assTo = "";
    }

    if($('#creatTo').val().length <= 0){
        creatTo = "";
    }
    

    var actStatus = "";
    if( actStatusListing ){
        actStatusListing.forEach( function( record , index ){  
            if( actStatus ){
                actStatus += ','+record;
            }else{
                actStatus = record;
            }
        });
    }

    var delayTy = "";
    if( delayType ){ 
        delayType.forEach( function( record , index ){  
            if( delayTy ){
                delayTy += ','+record;
            }else{
                delayTy = record;
            }
        });
    }

    var dateActStartDate = new Date(stDate);
    var dateActEndDate = new Date(endDate);

    if( +dateActEndDate < +dateActStartDate ) //dateActEndDate dateActStartDate
    {
        toastr.warning("End Date cannot be less than the StartDate");
        $('#endDate').datepicker('show');
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getActivityDetailReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        startDate: stDate,
        endDate: endDate,
        selectedUserId: assTo,
        createById: creatTo,
        statusId: actStatus,
        typeId: delayTy,
        // projectId: projectList,
        reportType : "NORMAL",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getReportDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        GBL_EXCELJSON = [];
        reportData = data.data;
        renderReportData();

    } else {

        reportData = [];
        renderReportData();

        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_ACT_REPORT );
            // toastr.error(data.status);
        else
            toastr.error(SERVERERROR);
    }
}

function renderReportData(){

    var newRow = '';
    var count = 1 ;

    if( reportData != 'No record found' ){

        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Act Id</th>'+
                            '<th>Title</th>'+
                            '<th>Mode</th>'+
                            '<th>Due Date / Time</th>'+
                            '<th>Customer Detail</th>'+
                            '<th>Contact Detail</th>'+
                            '<th>Type</th>'+
                            '<th>Status</th>'+
                            '<th>Assigned To</th>'+
                            '<th>Created By</th>'+
                            '<th>Start Time</th>'+
                            '<th>End Time</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        var tempEXL = { // HEADER FOR PDF AND CSV
                No : "#.",
                actId : "Activity Id",
                title : "Title",
                mode : "Mode",
                dueDate : "Due Date / Time",
                custName : "Customer Name",
                moNo : "Customer Mobile",
                contName : "Contact Name",
                contmoNo : "Contact Mobile",
                type : "Type",
                status : "Status",
                assignId : "Assigned To",
                createBy : "Created By",
                startTime : "Start Time",
                endTime : "End Time",
            };
            GBL_EXCELJSON.push(tempEXL);
     
        reportData.forEach( function( record , index ){  
            
            count = index + 1;
            var startTm = ((record.ACTSTARTTIME) == " " ? "--" : record.ACTSTARTTIME);
            var endTm = ((record.ACTENDTIME) == " " ? "--" : record.ACTENDTIME);

            var tempEXL = {
                No : count,
                actId : record.C_ACT_ID,
                title : record.ACT_TITLE,
                mode : record.VALUE,
                dueDate : mysqltoDesiredFormat(record.DUE_DATE , 'dd-MM-yyyy') + ' '+ record.DUE_TIME,
                custName : record.CUTOMERDETAIL,
                moNo : record.PHONE_NO,
                contName : record.CONTACTPERNAME,
                contmoNo : record.CONTACTPERNO,
                type : record.ACTTYPE,
                status : record.ACT_STATUS,
                assignId : record.ASSIGNTO,
                createBy : record.CREATEBY,
                startTime : record.ACTSTARTTIME,
                endTime : record.ACTENDTIME,
            };
            GBL_EXCELJSON.push(tempEXL);
                
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><a href="javascript:void(0)" data-activityId="'+ record.PK_ACT_ID +'" onclick="navigateToActivityDetailsOpen(this)">'+ record.C_ACT_ID +'</a></td>'+
                            // '<td><a href="javascript:void(0)" inqid="'+ record.inqId +'" onclick="navigateToInquiryDetail(this)">'+ record.inqId +'</a></td>'+
                            '<td>'+ record.ACT_TITLE +'</td>'+
                            '<td>'+ record.VALUE +'</td>'+
                            '<td><span class="hide">'+ ( record.DUE_DATE ).replace(/-/g, '/') +'</span>'+ mysqltoDesiredFormat(record.DUE_DATE , 'dd-MM-yyyy') + ' '+ record.DUE_TIME +'</td>'+
                            '<td>'+ record.CUTOMERDETAIL +'<br>'+ record.PHONE_NO +'</td>'+
                            '<td>'+ record.CONTACTPERNAME +'<br>'+ record.CONTACTPERNO +'</td>'+
                            '<td>'+ record.ACTTYPE +'</td>'+
                            '<td>'+ record.ACT_STATUS +'</td>'+
                            '<td>'+ record.ASSIGNTO +'</td>'+
                            '<td>'+ record.CREATEBY +'</td>'+
                            '<td>'+ startTm +'</td>'+
                            '<td>'+ endTm +'</td>'+
                        '</tr>';
        });
        
        $('#inqReportList').html(thead + newRow + tfoot);

    }else{
        $('#inqReportList').html(thead + newRow + tfoot);
        toastr.warning('No record found');
    }

    // resetReportFilter();
    $('.sidemenu').addClass('hiden', 300);

    // TablesDataTables.init();
    // TablesDataTablesEditor.init();
    $('#tableListing').dataTable({
        "aoColumns": [
            null,
            null,
            null,
            null,
            {"sType": "date-uk"},
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ],
        "pageLength": 50,
        "stateSave": true
    });
}

function AutoCompleteForAssignedUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#assTo').attr('assid',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveAssUsrDetail(){

}

function AutoCompleteForCreatedUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#creatTo').attr('createdid',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveCreatUsrDetail(){

}

function downLoadExcel(){
    if( GBL_EXCELJSON.length < 2 ){
        toastr.warning('Please Search Report Before Downloading...');
    }else{
        if(checkAuth(31,127,1) == -1){  // NOT AUTH            
            toastr.error('You are not authorised for this Activitiy');
        }else{
            csvSave(GBL_EXCELJSON, "ActivityMainReport.csv");
        }
    }
}

function resetReportFilter(){

    $('#startDate').val('');
    $('#endDate').val('');
    $('#actStatusListing').val('-1');
    $('#actStatusListing').selectpicker('refresh');
    $('#delayType').val('-1');
    $('#delayType').selectpicker('refresh');
    $('#assTo').val('');
    $('#assTo').attr('assid','-1');
    $('#creatTo').val('');
    $('#creatTo').attr('createdid','-1');
}

function pageInitialEvents(){
    $('.sidemenu').removeClass('hiden', 300);

    var date = new Date();
    var day = "01";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if( month < 10 ){
        month = "0" + month;
    }

    var monStDate = day + '-' + month + '-' + year;
    var today = new Date().format('dd-MM-yyyy')
    
    $('#startDate').val( today );
    $('#endDate').val( today );


    $('.fab-dial-handle').click(function(){
        $('.fabdail-buttons').toggleClass('active');
    });

    if( localStorage.POTGInqToWhyActMiss != "" && localStorage.POTGInqToWhyActMiss != undefined ){

        $('#startDate').val( mysqltoDesiredFormat( localStorage.POTGInqToWhyActstDate , 'dd-MM-yyyy' ) );
        $('#endDate').val( mysqltoDesiredFormat( localStorage.POTGInqToWhyActenDate , 'dd-MM-yyyy' ) );
        $('#assTo').val( localStorage.POTGInqToWhyActUserTxt  );
        $('#assTo').attr( 'assid',localStorage.POTGInqToWhyActUser  );
        $('#delayType').val( "1" );
        $('#delayType').selectpicker('refresh');
        // $('#actStatusListing').val( "0" );
        // $('#actStatusListing').selectpicker('refresh');

        // return false;
        localStorage.removeItem('POTGInqToWhyActMiss');
        localStorage.removeItem('POTGInqToWhyActDelayType');
        localStorage.removeItem('POTGInqToWhyActUser');
        localStorage.removeItem('POTGInqToWhyActstDate');
        localStorage.removeItem('POTGInqToWhyActenDate');
        localStorage.removeItem('POTGInqToWhyActUserTxt');

        //getReportData();
    
    }else if( localStorage.POTGInqToWhyActstDate != "" && localStorage.POTGInqToWhyActstDate != undefined ){

        $('#startDate').val( mysqltoDesiredFormat( localStorage.POTGInqToWhyActstDate , 'dd-MM-yyyy' ) );
        $('#endDate').val( mysqltoDesiredFormat( localStorage.POTGInqToWhyActenDate , 'dd-MM-yyyy' ) );
        $('#assTo').val( localStorage.POTGInqToWhyActUserTxt  );
        $('#assTo').attr( 'assid',localStorage.POTGInqToWhyActUser  );
        $('#actStatusListing').val( "2" );
        $('#actStatusListing').selectpicker('refresh');

        // return false;
        localStorage.removeItem('POTGInqToWhyActstatType');
        localStorage.removeItem('POTGInqToWhyActUser');
        localStorage.removeItem('POTGInqToWhyActstDate');
        localStorage.removeItem('POTGInqToWhyActenDate');
        localStorage.removeItem('POTGInqToWhyActUserTxt');

        //getReportData();
    }

    if( localStorage.loadMoreCompletedAct == "1" ){
        $('#startDate').val( '01-01-'+ year );
        $('#actStatusListing').val('2').selectpicker('refresh');  
        localStorage.removeItem('loadMoreCompletedAct');          
    }
    
    getReportData();
}

function menu() {
    if ($('.sidemenu').hasClass('hiden', 300)) {
        $('.sidemenu').removeClass('hiden', 300);
        setTimeout(function(){
            $('#startDate').focus();
            $('#startDate').datepicker('show');
        },500);
    } else {
        $('.sidemenu').addClass('hiden', 300);
    }
}