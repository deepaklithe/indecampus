    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : userManageController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

// GLOBAL VARIABLE DEFINED
var GBL_REQ_USER_ID = "";
var GBL_USER_LISTING = [];;

//API FOR USER LISTING CALL 
function getUserListingData(){ 

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getUserListing',
        clientId: newClientId,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(newClientId, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getUserListingDataCallback,"Please Wait... Getting Dashboard Detail");
}

//USER LISTING CALLBACK FUNCTION
function getUserListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // return false;
        GBL_USER_LISTING = data.data;
        renderUsersList(); 

    } else {
        renderUsersList();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }
}

//RENDER USERS FROM JSON
function renderUsersList(){

    var thead = '<table id="tableListingUsers" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Full Name</th>'+
                        '<th>User Name</th>'+
                        '<th>Level</th>'+
                        '<th>Designation</th>'+
                        '<th>Branch</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;
    
    if(GBL_USER_LISTING != 'No record found'){
        
        for( var i=0; i<GBL_USER_LISTING.length; i++ ){

            var fullName = GBL_USER_LISTING[i].userFullName;
            var name = GBL_USER_LISTING[i].userName;
            var userLevel = GBL_USER_LISTING[i].userLevelName;
            var userDesignation = GBL_USER_LISTING[i].userDesignationName;
            var userId = GBL_USER_LISTING[i].userId;
            var profileImg = GBL_USER_LISTING[i].userProfileImage;
            var branch = GBL_USER_LISTING[i].userBranchName;
            
            if( profileImg == "" ){
                profileImg = "http://"+FINALPATH+"/assets/globals/img/avtar1.png";
            }

            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><span class="useraImageBody"><img class="userProfile" id="" src="'+ profileImg +'"></span>&nbsp;&nbsp;'+ fullName +'</td>'+
                            '<td>'+ name +'</td>'+
                            '<td>'+ userLevel +'</td>'+
                            '<td>'+ userDesignation +'</td>'+
                            '<td>'+ branch +'</td>'+
                        '</tr>';
        }      

    }
    $('#userTable').html(thead + newRow + tfoot);
    $("#tableListingUsers").DataTable({ "stateSave": true });
}
