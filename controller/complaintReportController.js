    /*
 *
 * Created By :  Kavita Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 08-08-2018.
 * File : requestReportController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';
var GBL_REPORT_DATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    autoCompleteForStudentId('studentName',saveStudent);
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
    $("#monthFrom,#monthTo").attr('readonly',true);
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var departmentData = roleAuth.data.departmentList;
    setOption('0','department',departmentData,'Select Department');
    $('#department').selectpicker('refresh');

    // setOption('0','requestStatus',REQUEST_STATUS,'Select Complaint Status');
    $('#requestStatus').selectpicker('refresh');

    getComplaintReportList(); // GET EVENTS LISTING DATA
} 

// GET EVENTS LISTING DATA
function getComplaintReportList(){

    var studentId = $('#studentName').attr('studId');
    var monthFrom = $("#monthFrom").val();
    var monthTo = $("#monthTo").val();
    var department = $("#department").val();
    var requestStatus = $("#requestStatus").val();
    if( department == "-1" ){
        department = "";
    }
    if( requestStatus == "-1" ){
        requestStatus = "";
    }
    if( $('#studentName').val() == "" ){
        studentId = "";
    }
    if( monthFrom != "" && monthTo != "" ){

        var stMonth = monthFrom.split('-').reverse().join('-');
        var enMonth = monthTo.split('-').reverse().join('-');
        var dateStartDate = new Date(ddmmyyToMysql(stMonth));
        var dateEndDate = new Date(ddmmyyToMysql(enMonth));

        //START END DATE VALIDATION
        if (+dateEndDate < +dateStartDate) { 
            toastr.warning("End month cannot be less than the Start month");
            addFocusId('monthTo');
            return false;

        }
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'complaintReport', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(26,105),
    	studentId : studentId,
    	fromMonth : monthFrom,
        toMonth : monthTo,
    	departmet : department,
    	openCloseStatus : requestStatus,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestListCallback,"Please wait...Adding your item.");

    function getRequestListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_REPORT_DATA = data.data;
            renderComplaintReportList(GBL_REPORT_DATA); 
            $('.Sidenav').removeClass('open');  
            // resetFilters(); // RESET FILTER VALUES

        }else{

            GBL_REPORT_DATA = [];
            GBL_EXCELJSON = [];
            renderComplaintReportList(GBL_REPORT_DATA); 
            // resetFilters(); // RESET FILTERS
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_FILTER );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER ITEM LISTING DATA
function renderComplaintReportList(eventData){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="enrollmentViewRepoTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Student ID</th>'+
                        '<th>Student Name</th>'+
                        '<th>Department</th>'+
			            '<th>Ticket ID</th>'+
                        '<th>Category</th>'+
                        '<th>Sub-Category</th>'+
            			'<th>Ticket Date</th>'+
            			'<th>Issue</th>'+
                        ' <th>Status</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var tempJson = {
        srNo : "#",
        studMemId : "Student ID",
        studName : "Student Name",
        department : "Department",
        ticketId : "Ticket ID",
        category : "Category",
        subCategory : "Sub-Category",
        ticketDate : "Ticket Date",
        issue : "Issue",
        status : "Status",
    }
    GBL_CSVJSON.push(tempJson);

    var innerRow = '';
    if( eventData.length != 0 && eventData != "No record found" ){

        eventData.forEach(function( record , index ){

            var ticketDate = (record.ticketDate != "0000-00-00" && record.ticketDate != "1970-01-01" && record.ticketDate != "" ? mysqltoDesiredFormat(record.ticketDate,'dd-MM-yyyy') : "" );

            var tempJson = {
                srNo : (index+1),
                studMemId: record.studMembershipNo,
                studName : record.studentName,
                department : record.department,
                ticketId : record.ticketId,
                category : record.category,
                subCategory : record.subCategory,
                ticketDate : ticketDate,
                issue : record.issue,
                status : record.status,
            }
            GBL_CSVJSON.push(tempJson);
            
            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.studMembershipNo +'</td>'+
                            '<td>'+ record.studentName +'</td>'+
			                '<td>'+ record.department +'</td>'+
                            '<td>'+ record.ticketId +'</td>'+
                            '<td>'+ record.category +'</td>'+
                            '<td>'+ record.subCategory +'</td>'+
                            '<td>'+ (record.ticketDate != "0000-00-00" && record.ticketDate != "1970-01-01" && record.ticketDate != "" ? mysqltoDesiredFormat(record.ticketDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ record.issue +'</td>'+
			                '<td>'+ record.status +'</td>'+
                            
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    GBL_EXCELJSON =  thead + innerRow + tEnd;
    $('#enrollmentViewRepoTbl').DataTable();

}

// RESET FILTER
function resetFilters(){

    $('#studentName').attr('studId');
    $('#studentName').val('');
    $("#monthFrom").val('');
    $("#monthTo").val('');
    $("#department").val('').selectpicker('refresh');
    $("#requestStatus").val('').selectpicker('refresh');
    $("#monthFrom,#monthTo").datepicker('remove');
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
   
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderComplaintReportList(GBL_REPORT_DATA); // RENDER ORIGINAL WITHOUT FILTER DATA

}


function saveStudent(){

}
function setMonthTo(context){

    var monthFrom = $(context).val();

    $('#monthTo').datepicker('remove');
    $('#monthTo').datepicker({
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        startDate: monthFrom,
        autoclose:true
    });
    // $("#monthTo").attr('readonly',true);
}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Complaint Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Complaint_Report.pdf",20);
    }

}