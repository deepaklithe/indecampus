    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-08-2018.
 * File : suspensionTerminationController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_SUS_TERM_DATA = []; // SUSPENSION  / TERMINATION DATA
var GBL_SUS_ID = '';

// PAGE LOAD EVENTS  
function pageInitialEvents(){ 

    $('#terminationDate').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        autoclose: true,
    });
    localStorage.removeItem('indeCampusEditSuspensionId');
    localStorage.removeItem('indeCampusRevokeSusFlag');
    getSuspensionTermination();  // GET SUSPENSION / TERMINATION DATA
    assignAttachmentEvent();
    
}

// GET SUSPENSION / TERMINATION DATA
function getSuspensionTermination(){

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'listStudentSuspensionData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getSuspensionTerminationCallback,"Please wait...Adding your item.");

    function getSuspensionTerminationCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_SUS_TERM_DATA = data.data;
            renderSuspensionTermination(); // RENDER SUSPENSION DATA

        }else{

           	GBL_SUS_TERM_DATA = [];
           	renderSuspensionTermination(); // RENDER SUSPENSION DATA

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_SUS_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER SUSPENSION DATA
function renderSuspensionTermination(){

    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="suspensionTable">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Student No.</th>'+
                        '<th>Student Name</th>'+
                        '<th>Room No.</th>'+
                        '<th>Status</th>'+
                        '<th>Suspension Start</th>'+
                        '<th>Suspension End</th>'+
                        '<th>Revoke W.E.F</th>'+
                       '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var innerRow = '';
    if( GBL_SUS_TERM_DATA.length != 0 && GBL_SUS_TERM_DATA != "No record found" ){

        GBL_SUS_TERM_DATA.forEach(function( record , index ){
            var studStatus = 'Suspension';
            var studStatusIndex = findIndexByKeyValue( STUDENT_STATUS , 'ID' , record.status );
            if( studStatusIndex != -1 ){
                studStatus = STUDENT_STATUS[studStatusIndex].VALUE;
            }
            var disabledClass = '';
            var revokeClass = '';
            var terminateClass = '';
            var terminateVisibleClass = '';
            if( (record.revokeDate != "" && record.revokeDate != "0000-00-00") || studStatus.toLowerCase() == "active" ){
                revokeClass = 'hide';
                disabledClass = 'disabled';
            }
            if( record.type == "2" ){
                terminateClass = 'hide';
            }
            if( record.status == "1" ){
                terminateVisibleClass = 'hide';
            }
            var status = "Suspension";
            if( record.type == "2" ){
                status = "Termination";
            }

            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.studentMembershipNo +'</td>'+
                            '<td>'+ record.studentName +'</td>'+
                            '<td>'+ (record.roomName != null ? record.roomName : "") +'</td>'+
                            '<td>'+ studStatus +'</td>'+
                            '<td>'+ ( record.suspensionStartDate != "" && record.suspensionStartDate != "0000-00-00" ? mysqltoDesiredFormat(record.suspensionStartDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ ( record.suspensionEndDate != "" && record.suspensionEndDate != "0000-00-00" ? mysqltoDesiredFormat(record.suspensionEndDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ ( record.revokeDate != "" && record.revokeDate != "0000-00-00" ? mysqltoDesiredFormat(record.revokeDate,'dd-MM-yyyy') : "" ) +'</td>'+
                            '<td>'+ 
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple '+disabledClass+' '+terminateClass+'"  data-toggle="tooltip" data-original-title="Edit" suspensionId="'+record.suspensionId+'" onclick="editSuspension(this);" ><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-ripple btn-amber " data-toggle="tooltip" data-original-title="Upload Report" suspensionId="'+record.suspensionId+'" onclick="openUploadModal(this)"><i class="fa fa-paperclip"></i></a>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-ripple btn-danger '+disabledClass+'" data-toggle="tooltip" data-original-title="Delete" suspensionId="'+record.suspensionId+'" onclick="deleteSuspension(this)"><i class="fa fa-trash"></i></a>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-success btn-ripple '+revokeClass+' '+terminateClass+'" data-toggle="tooltip" data-original-title="Revoke" suspensionId="'+record.suspensionId+'" onclick="revokeSuspension(this)"><i class="fa fa-undo"></i></a>'+
                                '<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-success btn-blue-grey '+terminateClass+' '+terminateVisibleClass+'" data-toggle="tooltip" data-original-title="Termination" suspensionId="'+record.suspensionId+'" onclick="openTerminateModal(this)"><i class="fa fa-times"></i></a>'+
                            '</td>'+
                        '</tr>';
                
        });

    }
    $('#suspensionTerminationDiv').html( thead + innerRow + tEnd);
    $('#suspensionTable').DataTable();
	
}


// EDIT SUSUPENSION / TERMINATION
function editSuspension(context){

    var suspensionId = $(context).attr('suspensionId');
    localStorage.indeCampusEditSuspensionId = suspensionId;
    navigateToCreateSuspension();

}

// OPEN UPLOAD REPORT MODAL
function openUploadModal(context){

    var suspensionId = $(context).attr('suspensionId');
    var index = findIndexByKeyValue( GBL_SUS_TERM_DATA , 'suspensionId' , suspensionId );
    if( index != -1 ){
        GBL_SUS_ID = suspensionId;
        renderAttachment( GBL_SUS_TERM_DATA[index].attachmentData );
        refreshFile();
        modalShow('uploadReportModal');
    }

}

// RENDER ATTACHMENT DATA
function renderAttachment(attachmentData){  

    var attachLi = '';

    // STUDENT'S SUSPENSION REPORT ATTACHMENT
    if( attachmentData != "No record found" && attachmentData != "" && attachmentData != undefined  ){
        attachmentData.forEach(function( record , index ){
            
            // attachLi += '<li class="">'+
            //                 '<div class="list-content">'+
            //                     '<div class="col-lg-8 col-md-4">'+
            //                         '<a href="javascript:void(0)" data-link="'+ record.attachmentLink +'" onclick="openLinkInNewTab(this);"><span class="title">'+ record.title +'</span></a>'+
            //                     '</div>'+
            //                     '<div class="col-lg-4 col-md-2">'+
            //                         '<a href="javascript:void(0)" attachType="'+ record.attachType +'" attachPath="'+ record.attachmentLink +'" attachmentId="'+ record.attachmentId +'" onclick="deleteAttachment(this)"><i class="fa fa-trash-o pull-right"></i></a>'+
            //                     '</div>'+
            //                 '</div>'+
            //                 '<hr/>'+
            //             '</li>';
            var file = record.attachmentLink;
            var fileExt = file.substr( (file.lastIndexOf('.') +1) ); // FILE EXTENSION

            attachLi += "<div class='card card-image card-light-blue bg-image bg-opaque8 col-md-4' style='width: 30%;height: 156px; margin-right: 10px;'>"+
                                "<img src='"+record.attachmentLink+"' alt='' class='gallery-image'>"+
                                "<div class='context has-action-left has-action-right'>"+
                                    "<div class='tile-content'>"+
                                        "<span class='text-title'>"+record.title+"</span>"+
                                        "<span class='text-subtitle'>"+fileExt+"</span>"+
                                    "</div>"+
                                    "<div class='tile-action right'>"+
                                        "<a data-div='imagepopup' data-link='"+record.attachmentLink+"' onClick='openLinkInNewTab(this);' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>"+
                                        "<a onClick='deleteAttachment(this)' attachPath='"+record.attachmentLink+"' attachType='"+record.attachType+"' attachmentId='"+record.attachmentId+"' class='btn btn-sm btn-warning'><i class='fa fa-trash'></i></a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
        });
    }

    $('#attachmentListing').html( attachLi );
}

// UPLOAD ATTACHMENT ON SUBMIT CALL
function uploadAttachment(){

    var attachmentTitle = $('#uploadTitle').val();
    var attachmentName = $('#attachmentName').val();
    
    if( attachmentTitle == "" ){
        toastr.warning('Please Enter Report Title');
        addFocusId('uploadTitle');
        return false;

    }else if( attachmentName == "" ){
        toastr.warning('Please Attach File To Submit');
        return false;

    }else{

        $("#fileupload").submit();

    }

}

//  ATTACHMENT UPLOAD EVENT
function assignAttachmentEvent(){
    
    $("#fileupload").off().on('submit', (function (e) {
            
        var stdIndex = findIndexByKeyValue( GBL_SUS_TERM_DATA , 'suspensionId' , GBL_SUS_ID );

        var uploadTitle = $('#uploadTitle').val();

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "attachmentStudentSuspensionUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            suspensionId : GBL_SUS_ID,
            title : uploadTitle,
            type : "SUSPENSED"
        };

        $("#postDataAttach").val(JSON.stringify(temp_json));
        // console.log(temp_json);return false;

        var bfsendMsg = "Sending data to server..";
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1);
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                
                if(data.status == "Success"){
                    
                    console.log(data.data);
                    // ADD ARRAY TO ATTACHMENT JSON
                    var attachFile = {
                        attachmentId : data.data.attachmentId,
                        attachmentLink : data.data.fileLink,
                        attachmentName : data.data.fileName,
                        suspensionId : GBL_SUS_ID,
                        title : data.data.title,
                        attachType : "SUSPENSED"
                    }

                    if( stdIndex != -1 ){
                        // ADD ARRAY TO ATTACHMENT JSON ACCORDING TO UPLOAD TYPE
                        if( GBL_SUS_TERM_DATA[stdIndex].attachmentData == "No record found" || GBL_SUS_TERM_DATA[stdIndex].attachmentData == undefined ){
                            GBL_SUS_TERM_DATA[stdIndex].attachmentData = [];
                        }
                        GBL_SUS_TERM_DATA[stdIndex].attachmentData.push( attachFile );                            
                       
                        renderAttachment( GBL_SUS_TERM_DATA[stdIndex].attachmentData ); // RENDER ATTACHMENT DATA
                    }

                    refreshFile(); // REFRESH ATTACHMENT FILE
                    toastr.success( "Your attachment report uploaded successfully" );
                }else if( data.status == "FILEFORMTERROR" ){
                    toastr.warning("Not a valid file format");

                }else{
                    toastr.warning(data.status);
                    refreshFile(); // REFRESH ATTACHMENT FILE
                    
                }
                addRemoveLoader(0); // REMOVE LOADER
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                
                if( jqXHR.responseText != "" && jqXHR.responseText != undefined ){
                    if( JSON.parse(jqXHR.responseText).status != "" ){
                        toastr.error(JSON.parse(jqXHR.responseText).status);
                    }else{
                        toastr.error(errdata);
                    }
                    
                }else{
                    toastr.error(errdata);
                }
                
                refreshFile(); // REFRESH ATTACHMENT FILE
            }
        });
    }));
}

// DELETE SELECTED ATTACHMENT
function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attachmentId');
    var attachType = $(context).attr('attachType');
    var attachPath = $(context).attr('attachPath');
    var attachmentName = ( attachPath != "" ? basenameWithExt(attachPath) : "" );

    var confmDelete = confirm("Are you sure you want to delete this attachment ?");
    if( !confmDelete ){
        return false;
    }

    if( attachType == undefined || attachType == "undefined" ){
        attachType = "SUSPENSED";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'deleteSuppesionAttachment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(25, 104),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        attachmentId: attachmentId,
        suspensionId : GBL_SUS_ID,
        type : attachType,
        attachPath : attachPath,
        attachName : attachmentName
    };
    commonAjax(COMMONURL, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var studIndex = findIndexByKeyValue( GBL_SUS_TERM_DATA , 'suspensionId' , GBL_SUS_ID );

            // REMOVE ATTACHMENT FROM JSON 
            if( studIndex != -1 ){
                var index = findIndexByKeyValue( GBL_SUS_TERM_DATA[studIndex].attachmentData , "attachmentId" , attachmentId );
            
                GBL_SUS_TERM_DATA[studIndex].attachmentData.splice( index , 1 );

                renderAttachment(GBL_SUS_TERM_DATA[studIndex].attachmentData); // RENDER ATTACHMENT DATA
            }

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

// REFRESH ATTACHMENT FILE
function refreshFile(){

    $('#uploadTitle').val('');
    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

// OPEN SELECTED ATTACHMENT LINK IN NEW TAB
function openLinkInNewTab(context){
    var link = $(context).attr("data-link");
    if(link==""){
        toastr.warnings("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}

// DELETE STUDENT SUSPENSION
function deleteSuspension(context){

    var suspensionId = $(context).attr('suspensionId');
    var index = findIndexByKeyValue( GBL_SUS_TERM_DATA , 'suspensionId' , suspensionId );
    if( index != -1 ){
        var type = "suspension";
        if( GBL_SUS_TERM_DATA[index].type == "2" ){
            type = "termination";
        }

        var cnfmDelete = confirm("Are you sure you want to remove this "+type+" ?");
        if( !cnfmDelete ){
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
        var postData = {
            requestCase: 'deleteStudentSuspension',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(25, 104),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            suspensionId : suspensionId,
            studentId : GBL_SUS_TERM_DATA[index].studentId
        };
        commonAjax(COMMONURL, postData, deleteSuspensionCallBack,CREATINGINQDATA);

        function deleteSuspensionCallBack(flag, data){
            if (data.status == "Success" && flag) {
                
                GBL_SUS_TERM_DATA.splice(index,1);
                renderSuspensionTermination();

            }else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DELETE_SUS );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }

    }

}

// REVOKE STUDENT SUSPENSION
function revokeSuspension(context){

    var suspensionId = $(context).attr('suspensionId');
    localStorage.indeCampusEditSuspensionId = suspensionId;
    localStorage.indeCampusRevokeSusFlag = true;
    navigateToCreateSuspension();

}

// OPEN TERMINATE STUDENT MODAL
function openTerminateModal(context){

    var suspensionId = $(context).attr('suspensionId');
    var index = findIndexByKeyValue( GBL_SUS_TERM_DATA , 'suspensionId' , suspensionId );
    if( index != -1 ){
        GBL_SUS_ID = suspensionId;
        $('#terminationDate').val('');
        modalShow('terminateModal');
    }

}

// TERMINATION OF STUDENT AFTER SUSPENSION
function terminateStudent(){

    var index = findIndexByKeyValue( GBL_SUS_TERM_DATA , 'suspensionId' , GBL_SUS_ID );

    if( index != -1 ){

        var terminationDate = $('#terminationDate').val();
        if( terminationDate == "" ){
            toastr.warning('Please Select Termination Date');
            addFocusId('uploadTitle');
            return false;
        }

        var cnfmTerminate = confirm("Are you sure you want to terminate this student ?");
        if( !cnfmTerminate ){
            return false;
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

        var postData = {
            requestCase: 'studentTermination',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 44),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            suspensionId : GBL_SUS_ID,
            terminationDate : terminationDate,
            type : "2", // TERMINATION
            studentId : GBL_SUS_TERM_DATA[index].studentId
        };
        commonAjax(COMMONURL, postData, terminateStudentCallBack,CREATINGINQDATA);

        function terminateStudentCallBack(flag, data){
            if (data.status == "Success" && flag) {
                
                GBL_SUS_ID = "";
                $('#terminationDate').val('');
                modalHide('terminateModal');
                getSuspensionTermination();

            }else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_TERMINATE );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

}