/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 16-08-2016.
 * File : addNonAvailingController.
 * File Type : .js.
 * Project : indecampus
 *
 * */

// PAGE INITIAL EVENTS
function pageInitialEvents(){

	$('.bootstrap-daterangepicker-basic').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date(),
        todayHighlight: true,
        autoclose: true
    });
    // PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID / STUDENT NAME IN FILTERS
    autoCompleteForStudentFilter('studentMemId',saveStudentNameData);
    autoCompleteForStudentFilter('studentName',saveStudentNameData);

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);    
    var nonAvailingService = roleAuth.data.NonAvailingService;
    setOptionWithRef("0", 'availService', nonAvailingService, "---Select Service---", 'PK_ITEM_ID', 'ITEM_NAME');
    $('#availService').selectpicker('refresh');
   
    // if( localStorage.indeCampusStudentEditAvail != undefined && localStorage.indeCampusStudentEditAvail != "" ){
    // 	getAvailServiceData();
    // }

}

// ADD NON AVAIL SERVICE
function addNonAvailService(){

    var studentName = $('#studentName').val();
    var studentMemId = $('#studentMemId').val();
    var studId = $('#studentMemId').attr( 'studId' );
	var availStartDate = $('#availStartDate').val();
	var availEndDate = $('#availEndDate').val();
	var availService = $('#availService').val();
	var availDescription = $('#availDescription').val();

	// CHECK VALIDATIONS
	if( studentName == "" || studentMemId == "" || studId == "" || studId == undefined ){
        toastr.warning("Please Search Student For Non Availing of Service");
        addFocusId('studentName');
        return false;

    }else if( availStartDate == "" ){
		toastr.warning("Please Select Non Avail Service Start Date");
		addFocusId('availStartDate');
		return false;

	}else if( availEndDate == "" ){
		toastr.warning("Please Select Non Avail Service End Date");
		addFocusId('availEndDate');
		return false;

	}else if( availService == "-1" || availService == "" || availService == null ){
		toastr.warning("Please Select Non Avail Service Facility");
		addFocusId('availService');
		return false;

	}else if( availDescription == "" ){
		toastr.warning("Please Enter Non Avail Service Description");
		addFocusId('availDescription');
		return false;

	}
    var dateStartDate = new Date(ddmmyyToMysql(availStartDate));
    var dateEndDate = new Date(ddmmyyToMysql(availEndDate));

    //START END DATE VALIDATION
    if (+dateEndDate < +dateStartDate) { 
        toastr.warning("End date cannot be less than the Start date");
        addFocusId('availStartDate');
        return false;

    }

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    // POST DATA
    var postData = {
        requestCase : 'addEditNonAvailingService',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(27, 107),
        studentId : studId,
        serviceStartDate : ddmmyyToMysql(availStartDate),
        serviceEndDate : ddmmyyToMysql(availEndDate),
        itemId : availService,
        description : availDescription,
        dataValue : generateSecurityCode(tmp_json.data[0].clientId, tmp_json.data[0].studentId)
    }
    // console.log(postData); return false;
    commonAjax(FOLLOWUPURL, postData, addNonAvailServiceCallback,"Please Wait... Getting Dashboard Detail");
    
    function addNonAvailServiceCallback(flag , data) {
      
        if (data.status == "Success" && flag) {

        	// navigateToNonAvailServices(); // REDIRECT TO LISTING PAGE
            //ADDED BY NAZIM KHAN
            toastr.success('Non availing service added successfully');
            resetNonAvailService();
            
        } else {
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_ADD_NON_AVAIL );
            }
            else{
                toastr.error(SERVERERROR);
            }
        }
        
    }
	
}

// SET AVAIL END DATE-PICKER
function setEndDate(context) {
	
	var startDate = $(context).val();
	$("#availEndDate").datepicker('removeLoaderInTime');
	$("#availEndDate").datepicker({ 
        format: 'dd-mm-yyyy',
        autoclose: true, 
        todayHighlight: true,
        startDate: startDate
    });
    $('#availEndDate').attr('readonly',true);

}

// RESET FIELDS
function resetNonAvailService(){
    
    $('#studentName').val('');
    $('#studentMemId').val('');
    $('#roomNumber').val('');
    $('#studentMemId').attr( 'studId' ,'');
    $('#availStartDate').val('');
    $('#availEndDate').val('');
    $('#availService').val('-1').selectpicker('refresh');
    $('#availDescription').val('');
    $('#availStartDate,#availEndDate').datepicker('remove');
    $('#availStartDate,#availEndDate').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true, 
        todayHighlight: true,
        startDate: new Date(),
    });
}


// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID / STUDENT NAME IN FILTERS
function autoCompleteForStudentFilter(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( id == "studentMemId" ){
                setTimeout(function() {
                    $('#'+id).val( item.text );
                    $('#'+id).attr( 'studId' , item.value );
                    $('#studentName').val( ITEMS[index].studentName );
                    $('#roomNumber').val( ITEMS[index].roomName );
                },10);
            }else{
                setTimeout(function() {
                    $('#'+id).val( item.text );
                    $('#studentMemId').attr( 'studId' , item.value );
                    $('#studentMemId').val( ITEMS[index].membershipNumber );
                    $('#roomNumber').val( ITEMS[index].roomName );
                },10);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(1,2),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            if( id == "studentMemId" ){

                                var tempJson = {
                                    id: data.studId,
                                    name: data.membershipNumber,
                                    data: data.membershipNumber,
                                    studentName : data.studName,
                                    membershipNumber : data.membershipNumber,
                                    roomName : data.roomName
                                }
                                ITEMS.push(tempJson);

                            }else{

                                var tempJson = {
                                    id: data.studId,
                                    name: data.studName,
                                    data: data.studName,
                                    studentName : data.studName,
                                    membershipNumber : data.membershipNumber,
                                    roomName : data.roomName
                                }
                                ITEMS.push(tempJson);

                            }
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}