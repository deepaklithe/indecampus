/*
 *
 * Created By : Sachin Jani.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : April 2016.
 * File : Schedule Activity.
 * File Type : .js.
 * Project : activityMgmt
 *
 * */


function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}


function activitySchedulePage() {

    /* init globals constants from gbl_ActivityAppGlobals  START */
    //Static Globals
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;
    var ACT_MODE_PHONE = gbl_ActivityAppGlobals.ACT_MODE_PHONE;
    var ACT_MODE_EMAIL = gbl_ActivityAppGlobals.ACT_MODE_EMAIL;
    var ACT_MODE_SMS = gbl_ActivityAppGlobals.ACT_MODE_SMS;
    var ACT_MODE_PERSONALMEET = gbl_ActivityAppGlobals.ACT_MODE_PERSONALMEET;
    var ACT_STATUS_PENDING = gbl_ActivityAppGlobals.ACT_STATUS_PENDING;
    var ACT_STATUS_INPROCESS = gbl_ActivityAppGlobals.ACT_STATUS_INPROCESS;
    var ACT_STATUS_CLOSED = gbl_ActivityAppGlobals.ACT_STATUS_CLOSED;


    /* init globals constants from gbl_ActivityAppGlobals  END */

    /* this module related globals START */
    var mGbl_customerGrpList = [];
    var mGbl_userGrpList = [];
    var mGbl_customerList = [];
    var mGbl_userList = [];
    var thisModule = this;
    var mdlCommonModule = new commonModuleForActivityApp(); //create an instance of commonModuleForActivityApp .
    /* this module related globals END */
    
        /* element selector's defination START */
    var $txtActTitle = $('#txtActTitle');
    var $txtActDesc = $('#txtActDesc');
    var $txtActStartDate = $('#txtActStartDate');
    var $txtActEndDate = $('#txtActEndDate');
    var $rdoActMode = $('input[type=radio][data-name="rdoActMode"]');

    var $txtCustGrp = $('#txtCustGrpName');
    var $txtCustGrpLabel = $('#cg-label');
    //var $rdoCustType = $('input[type=radio][name="rdoIsCustomerGroup"]');
    var $rdoCustType = $('#customerType');
    var $rdoAssignType = $('input[type=radio][data-name="rdoIsUserGroup"]');
    var $ddActAssignedTo = $('#ddActAssignedTo');
    var $ddActFrequency = $('#ddActFrequency');
    var $formCreateUpdateAct = $("#formCreateCustomAct");


    
    /* public functions START */
    this.initSchPage = function()
    {
        initCreateCustomActModule();
        
    }
    
    /* public functions END */




    var initCreateCustomActModule = function ()
    {
        /* render ACT_FREQUENCY dropdown START */
        var actFreqOptionHtml = mdlCommonModule.getActFreqDropDown();
        $($ddActFrequency).html(actFreqOptionHtml);
        $($ddActFrequency).selectpicker();
        /* render ACT_FREQUENCY dropdown END */
 

        var callBackGetCustGrpDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {
                mGbl_customerGrpList = dataJson.custGrpArr;
                mGbl_userGrpList = dataJson.userGrpArr;
                assignTypeGroupOptionHtml = dataJson.assignTypeGroupOptionHtml;


                mdlCommonModule.getUserCustomerSingleData(callBackGetCustSingleDataForThisModule);
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }


        }

        var callBackGetCustSingleDataForThisModule = function (dataJson)
        {
            if (dataJson)
            {
                mGbl_customerList = dataJson.customerListArr;
                mGbl_userList = dataJson.userListArr;
                assignTypeUserOptionHtml = dataJson.assignTypeUserOptionHtml;

                getSchActListin();
            }
            else
            {
                console.error("There's some problem in getting the data of cust/user grp in moduleForCreateActivity module");
            }


        }

        mdlCommonModule.getUserCustomerGroupData(callBackGetCustGrpDataForThisModule);

    }


    /* function to make ajax call and get activity data START */
    var getSchActListin = function ()
    {
        var postData = {
            requestCase: "getCustomActListing",
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,6),
        };



        function callBackGetSchActListin(flag, results) {
            if (results.status == SCS) {
                //log(msg);
                //localStorage.POTGtempActData = JSON.stringify(results);
                var dataArr = results.data;

                // params : array
                renderSchActivityData(dataArr);

            } else {
                var tmpDataArr = {
                    data:[{}]
                };
                renderSchActivityData(tmpDataArr);

                //displayAttendenceListin(results);
                log("====== FAIL ======");
                if (flag) {
                    displayAPIErrorMsg( results.status , GBL_ERR_NO_ACT );
                    //alert(results.status);
                } else {
                    log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTLIST;
        commonAjax(actUrl, postData, callBackGetSchActListin, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackGetSchActListin, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to make ajax call and get activity data END */
    
    /* function to render sch act data START */
    function renderSchActivityData(dataArr)
    {
        var dataArrLen = dataArr.length;

        var innerHtml = '';
        var tableStartHtml = '<table id="actSchListinDataTable" class="display datatables-alphabet-sorting" >';
        var tableEndHtml = '</table>';
        var threadHtml = '<thead>' +
                '<tr>' +
                // '<th>Activity Type.</th>' +
                '<th>Activity Title</th>' +
                '<th>Mode</th>' +
                '<th>Start Date</th>' +
                '<th>End Date</th>' +
                '<th>Duration</th>' +
                '<th>Assigned To</th>' +
                '</tr>' +
                '</thead>';
        var tbodyStart = '<tbody id="tbodySchActListin">';
        var tbodyEnd = '</tbody>';

        if (dataArrLen <= 0)
        {
            innerHtml = ' '; //<li class="nodata">No Data Found ..!</li> 
        }

        for (var i = 0; i < dataArrLen; i++)
        {
            /* data init START */
            var activityType = "Schedule";
            var activityId = dataArr[i].PK_ACT_CUSTOM_ID;
            var activityTitle = dataArr[i].ACT_CUSTOM_TITLE;
            var activityMode = dataArr[i].ACT_CUSTOM_MODE;
            var activityModeName = '';
            var activityStartDate = dataArr[i].START_DATE;
            var activityEndDate = dataArr[i].END_DATE;
            var activityFreqValue = dataArr[i].FREQUENCY;
            var activityStatus = dataArr[i].ACT_STATUS;
            var activityAssignedTo = dataArr[i].ASSIGN_TO; // get user name from id : pending
            var activityAssignedToLabel = '';
            var isUserGroup = dataArr[i].IS_USER_GROUP;
            /* data init END */
            
            /* get assignedTo name from id START */
            if (isUserGroup == '0') {
                //user //single
                //mGbl_userList;
                var idx = findIndexByKeyValue(mGbl_userList, "value", activityAssignedTo);
                if (idx == -1)
                {   
                    console.log("label for ASSIGN_TO :" + activityAssignedTo + " Not found in user list");
                }
                else
                {
                    activityAssignedToLabel = mGbl_userList[idx].fullName;
                }
                
                
            }
            else if (isUserGroup == '1')
            { // group
                //mGbl_userGrpList;
                var idx = findIndexByKeyValue(mGbl_userGrpList, "value", activityAssignedTo);
                if (idx == -1)
                {
                    console.log("label for ASSIGN_TO :" + activityAssignedTo + " Not found in user group list");
                }
                else
                {
                    activityAssignedToLabel = mGbl_userList[idx].fullName;
                }
                
                
            }
            
            /* get assignedTo name from id END */

            /* activity mode icon init START */
            var activityModeIcon;

            if (activityMode == ACT_MODE_PHONE)
            {
                activityModeIcon = 'fa-phone';
                activityModeName = 'Phone';
            }
            else if (activityMode == ACT_MODE_EMAIL)
            {
                activityModeIcon = 'fa-envelope';
                activityModeName = 'E-Mail';
            }
            else if (activityMode == ACT_MODE_SMS)
            {
                activityModeIcon = 'fa-paper-plane';
                activityModeName = 'SMS';
            }
            else if (activityMode == ACT_MODE_PERSONALMEET)
            {
                activityModeIcon = 'fa-user';
                activityModeName = 'Personal Meeting';
            }
            /* activity mode icon init END */


            if (activityStartDate && activityStartDate != "null" && activityStartDate != "undefined")
            {
                activityStartDate = new Date(activityStartDate).format('dd-MM-yyyy');
            }
            
            if (activityEndDate && activityEndDate != "null" && activityEndDate != "undefined")
            {
                activityEndDate = new Date(activityEndDate).format('dd-MM-yyyy');
            }

            var dataAttr = 'data-activityId="' + activityId + '" data-isUserGroup="' + isUserGroup + '"';

            var actAssignToTDHtml = '';

            if (activityAssignedTo)
            {
                actAssignToTDHtml = '<span ' + dataAttr + ' class="assignmentTd">' + activityAssignedToLabel + '</span>';
            }
            else
            {
                actAssignToTDHtml = '<div class="btns">' +
                        '<button ' + dataAttr + ' class="btn btn-primary pulsate-regular btn-sm assignmentTd"> Assign</button>' +
                        '</div>';
            }

            /* activity status init START */

            var actStatusName = '';
            var actStatusClass = 'btn-success';

            if (activityStatus == ACT_STATUS_PENDING)
            {
                actStatusName = 'Pending';
                actStatusClass = 'btn-warning';

            }
            else if (activityStatus == ACT_STATUS_INPROCESS)
            {
                actStatusName = 'In-Process';
                actStatusClass = 'btn-success';

            }
            else if (activityStatus == ACT_STATUS_CLOSED)
            {

                actStatusName = 'Closed';
                actStatusClass = 'btn-danger';

            }
            /* activity status init END */
            
            /* get act freq label START */
            var actFreqArr = gbl_ActivityAppGlobals.ACT_FREQUENCY;
            var actFreqLabel = '';
            var idx = findIndexByKeyValue(actFreqArr,"value",activityFreqValue);
            if(idx == -1)
            {
                console.log("label for ACT_FREQUENCY :" + activityFreqValue + " Not found");
            }
            else
            {
                actFreqLabel = actFreqArr[idx].label;
            }
            /* get act freq label END */
            

            innerHtml += '<tr class="gradeX">' +
                    // '<td>' + activityType + '</td>' +
                    '<td>' + activityTitle + '</td>' +
                    '<td>' + activityModeName + '</td>' +
                    '<td>' + activityStartDate + '</td>' +
                    '<td>' + activityEndDate + '</td>' +
                    '<td>' + actFreqLabel + '</td>' +
                    '<td>' + actAssignToTDHtml + '</td>' +
                    '</tr>'
        }

        var finalTableHtml = tableStartHtml + threadHtml + tbodyStart + innerHtml + tbodyEnd + tableEndHtml;

        $('#divSchActListing').html('');
        $('#divSchActListing').html(finalTableHtml);
        //registerAssignToPopUpEvent(); // uncomment this line to activate "Assign schedule activity".
        // $('#actSchListinDataTable').dataTable();
        generateDatatable("actSchListinDataTable");


    }
    /* function to render sch act data END */


}

var actSchPage;
function initSchActPage()
{

    actSchPage = new activitySchedulePage();
    actSchPage.initSchPage();
    
}