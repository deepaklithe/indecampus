/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : teamController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_CS_USER_ID = "";
var GBL_CS_LOGIN_MONTH = "";
var GBL_LOGIN_HISTORY = [];
var GBL_LOGIN_LIST_HTML = "";
function getLoginHistory(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userLoginHistory',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(19,74),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getLoginHistoryCallback,"Please Wait... Getting Dashboard Detail");

    function getLoginHistoryCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            var newRow = '';

            var tHead = '<table class="display datatables-alphabet-sorting" id="loginTabReg" width="100%">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th>No</th>'+
                                    '<th>Name</th>'+
                                    '<th>Month</th>'+
                                    '<th>Login Count</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody id="loginHistoryList">';
            var tEnd = '</tbody></table>';

            if(data.data != 'No record found'){

                GBL_LOGIN_HISTORY = data.data;
                data.data.forEach( function( record , index ){
                    newRow += '<tr>'+
                                    '<td>'+ (index+1) +'</td>'+
                                    '<td>'+ record.userName +'</td>'+
                                    '<td>'+ monthsList[ record.loginMonth - 1 ] +'</td>'+
                                    '<td>'+ record.loginCount +'</td>'+
                                '</tr>';

                    if( GBL_CS_USER_ID ){
                        GBL_CS_USER_ID += ','+record.userId;
                        GBL_CS_LOGIN_MONTH += ','+record.loginMonth;
                    }else{
                        GBL_CS_USER_ID = record.userId;
                        GBL_CS_LOGIN_MONTH = record.loginMonth;
                    }
                });

                
                $('#divHeadTable').html( tHead + newRow + tEnd );
                GBL_LOGIN_LIST_HTML = tHead + newRow + tEnd;

            }else{
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_HISTORY );
            }
            
            $('#loginTabReg').dataTable({ "stateSave": true });
            
        } else {
            
            $('#loginTabReg').dataTable();
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_HISTORY );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function openLHModal(){
    // var data = 
    if( GBL_LOGIN_HISTORY.length == "0" ){
        toastr.warning('Login History is Not Available');
        return false;
    }

    tinyMCE.get('loginHistoryData').setContent( GBL_LOGIN_LIST_HTML );
    $('#loginHistoryModal').modal('show');
     
    addFocusId( 'toName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function sendLoginHistory(context){

    var loginHistoryContent = tinyMCE.get('loginHistoryData').getContent();
    
    var toEmailId = $('#toEmailId').val();
    var toName = $('#toName').val();

    if( toName == "" ){
        toastr.warning('Please Enter Name');
        $('#toName').focus();
        return false;
    
    }else if( toEmailId == "" ){
        toastr.warning('Please Enter To-Email Id');
        $('#toEmailId').focus();
        return false;
    
    }else if( !validateEmail(toEmailId) ){
        toastr.warning('Please Enter Valid To-Email Id');
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'sendLoginHistory',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        loginHistoryContent : loginHistoryContent,
        toName : toName,
        toEmailId : toEmailId,
        userIdList : GBL_CS_USER_ID,
        monthList : GBL_CS_LOGIN_MONTH,
    }  
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, sendLoginHistoryCallback,"");
    
    function sendLoginHistoryCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            navigateToMasterDeveloper();
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_SEND_HISTORY );
            else
                toastr.error(SERVERERROR);
        }
    }
}
