/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-Jun-2016.
 * File : documentManagementController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
var DOCJSON=[];
var VIEWJSONDATALENGTH=0;
var searchTagArr=[];
var searchTagArrCopy=[];
var DOCJSONCOPY=[];

//START FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO DOCJSON
function callBackGetDocListing(flag, data) {
    if (data.status == SCS && flag) {

        addFocusId( 'docName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        if (data.data) {
            VIEWJSONDATALENGTH=0;
            DOCJSON = data.data.data; 
            DOCJSONCOPY = data.data.data;
            addSearchDataArr(data.data.tagList);
            displayDocData(1,DOCJSON);
        }
    }else {
        VIEWJSONDATALENGTH=0;
        DOCJSON = []; 
        DOCJSONCOPY = [];
        addSearchDataArr('');
        displayDocData(1,DOCJSON);
        if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NO_DOC );
        }else{
            toastr.error(SERVERERROR);
        }
    }
}
//END FUNCTION FOR CALL BACK AFETR LISTING API CALL AND SET IT INTO DOCJSON

//START FUNCTION FOR VIEW IMAGE IN POP UP 
function viewImageLinkData(docData){
    var link = $(docData).attr("data-link");
    $("#imageLink").attr("src",link);
    openModal(docData);   
}
//END FUNCTION FOR VIEW IMAGE IN POP UP 

//START FUNCTION FOR VIEW VIDEO IN POP UP 
function viewVideoLinkData(docData){
    var link = $(docData).attr("data-link");
    $(".videoLink").attr("src",link);    
    var player = $("#videoplayer");
    player.load();
    openModal(docData); 
}
//END FUNCTION FOR VIEW VIDEO IN POP UP

//START FUNCTION FOR DISPLAY DOCUMENT BY FILTER DOCUMENT TYPE (STANDARD/ITEM/ALL)
function displayDocDataByFilter(){
    VIEWJSONDATALENGTH = 0;
    var filterVal = $("#docType").val(); //SEARCH DOC TYPE FILTER
    var docData = [];
    searchTagArr = [];
    
    if(filterVal !=0){ // IF SEARCH DOC TYPE IS NOT ALL
        var filterFlag = false;        
        for(var l=0;l<searchTagArrCopy.length;l++){
            if(searchTagArrCopy[l]['type'] == filterVal){
                searchTagArr.push(searchTagArrCopy[l]);  //PUSH VALUE FOR SEARCH DOCUMENT   
            }
        }
    }else{ //IF SEARCH DOC TYPE IS ALL
        var filterFlag = true;
        searchTagArr = searchTagArrCopy;
    }
    for(var i=0;i<DOCJSON.length;i++){
        var arrIndex = i;
        var docId = DOCJSON[i]['PK_DOC_ID'];
        var docName = DOCJSON[i]['DOC_NAME'];
        var docType = parseInt(DOCJSON[i]['DOC_TYPE']);
        var docFileType = DOCJSON[i]['FILE_TYPE'];
        var doclink = DOCJSON[i]['doclink'];
        
        if(filterFlag==true || (filterFlag==false && filterVal==docType)){          
            //var tmpdata = {label: docName , value: arrIndex.toString()};
            //searchTagArr.push(tmpdata);            
            docData.push(DOCJSON[i]);
        }
        if(i==(DOCJSON.length-1)){ // IF JSON LENGTH IS SAME AS INDEX
            displayDocData(filterVal,docData); //CALL FUNCTION FOR DISPLAY DOCUMENT DATA
        }
    }    
}
//END FUNCTION FOR DISPLAY DOCUMENT BY FILTER DOCUMENT TYPE (STANDARD/ITEM/ALL)

function addSearchDataArr(searchData){
    searchTagArr=[];
    searchTagArrCopy=[];
    log(searchData);
    for(var i = 0; i < searchData.length; i++){
        var arrIndex = i;        
        var docName = searchData[i]['lable'];
        var docType = searchData[i]['type'];
        var tmpdata = {label: docName , value: docName,type:docType};
        searchTagArr.push(tmpdata);
        searchTagArrCopy.push(tmpdata);
        
    }
}

//START FUCNTION FOR INIT ON PAGE LOAD
function initDocFunction(){

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var branchList = roleAuth.data.branchListing;
    var brLst = '<option value="-1">Select Branch</option>';
    for( var i=0; i<branchList.length; i++ ){
        brLst += '<option value="'+ branchList[i].orgId +'">'+ branchList[i].orgName +'</option>';
    }
    $('#branchListSelect').html( brLst );
    $('#branchListSelect').selectpicker( 'refresh' );
    // setOption("0", "branchListSelect", roleAuth.data.branchListing, "---Select Branch---");
    // $('#branchListSelect').selectpicker( 'refresh' );

    $('#docTypeAdd').change(function(){
        if( $('#docTypeAdd').val() == "1" ){
                $('#inqAttDiv').show();
        }else{
                $('#inqAttDiv').hide();
        }
    });

    $('#inqAtt').change(function(){
        if( $('#inqAtt').prop('checked') == true  ){
                $('#branchDiv').show();
        }else{
                $('#branchDiv').hide();
        }
    });
    getDocData(); //CALL FUNCTION FOR DISPLAY DATA    
    registerCanvasEvent();
}
//END FUCNTION FOR INIT ON PAGE LOAD

//START FUNCTION FOR RESET SEARCH DATA
function resetSearchdata(){
    $("#docType").val("0").selectpicker('refresh');
    VIEWJSONDATALENGTH = 0;
    displayDocData(0,DOCJSON);
}
//END FUNCTION FOR RESET SEARCH DATA

//START FUNCTION FOR PAGE NAVIGATION ON NEXT BUTTON
function addNextValue(){
    if((VIEWJSONDATALENGTH + 9) > DOCJSONCOPY.length){        
    }else{
        VIEWJSONDATALENGTH=VIEWJSONDATALENGTH+9;
    }
}
//END FUNCTION FOR PAGE NAVIGATION ON NEXT BUTTON

//START FUNCTION FOR PAGE NAVIGATION ON PREVIOUS BUTTON
function prevNextValue(){
    if(VIEWJSONDATALENGTH !=0){
        VIEWJSONDATALENGTH=VIEWJSONDATALENGTH-9;
    }    
}
//END FUNCTION FOR PAGE NAVIGATION ON PREVIOUS BUTTON

//START FUNCTION FOR GET ACTION OF BUTTON IS NEXT OR PREVIOUS AND CALL RESPECTIVE FUNCTION
function setValue(ele){
    var action = $(ele).attr("data-action");
    log(action);
    if(action=="prev"){
        prevNextValue();
    }else{
        addNextValue();
    }
    displayDocData(0,DOCJSONCOPY);
}
//END FUNCTION FOR GET ACTION OF BUTTON IS NEXT OR PREVIOUS AND CALL RESPECTIVE FUNCTION

//START FUNCTION FOR ENABLE/DISABLE NEXT AND PREV BUTTON
function enableDisablePrevNext(docData){
    if(docData.length<9){
        $('.prevNext').attr("disabled","disabled");
    }else{
        $('.prevNext').attr("disabled",false);
    }    
}
//START FUNCTION FOR ENABLE/DISABLE NEXT AND PREV BUTTON

//START FUNCTION FOR DISPLAY DATA OF DOCUMENT
function displayDocData(filter,docDataParam) { //filter 0=ALL,1=STANDARD,2=ITEM
    log(docDataParam.length);
    log(VIEWJSONDATALENGTH);
    if(docDataParam.length>0){
        var docData = docDataParam;
        DOCJSONCOPY = docDataParam;
    }else{
        var docData = DOCJSONCOPY;
    }    
    var html = "";
    if(filter !=0){
        var filterFlag = false;
    }else{
        var filterFlag = true;
    }
    var k=0;
    
    for (var i = VIEWJSONDATALENGTH; i < docData.length; i++) {
        
        var arrIndex = i;
        var docId = docData[i]['PK_DOC_ID'];
        var docName = docData[i]['DOC_NAME'];
        var docType = parseInt(docData[i]['DOC_TYPE']);
        var docFileType = docData[i]['FILE_TYPE'];
        var docFileExt = docData[i]['FILE_EXTENSION'];
        var doclink = docData[i]['doclink'];            
            var aTag ="";
            var imgTag ="";
            if(docFileType == "image"){
                aTag = "<a data-div='imagepopup' data-link='"+doclink+"' onClick='viewImageLinkData(this);' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>";
                imgTag = "<img src='"+doclink+"' alt='' class='gallery-image'>";
            }else if(docFileType == "video"){
                aTag = "<a data-div='videopopup' data-link='"+doclink+"' onClick='viewVideoLinkData(this);' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>";
                imgTag = "<img src='assets/globals/img/play_btn.gif' alt='' class='gallery-image'>";
            }else{
                aTag = "<a href='"+doclink+"' target='_blank' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>";
                imgTag = "<img src='' alt='' class='gallery-image'>";
            }
            
            if(k < 9){
                html += "<div class='col-lg-4'>"+
                            "<div class='card card-image card-light-blue bg-image bg-opaque8'>"+
                                imgTag+
                                "<div class='context has-action-left has-action-right'>"+
                                    "<div class='tile-content'>"+
                                        "<span class='text-title'>"+docName+"</span>"+
                                        "<span class='text-subtitle'>"+docFileExt+"</span>"+
                                    "</div>"+
                                    "<div class='tile-action right'>"+
                                        aTag+
                                        "<a onClick='deleteDocument(this)' data-docId='"+docId+"' class='btn btn-sm btn-warning'><i class='fa fa-trash'></i></a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
            }
       
        k++;
    }
    registerTagsSearch();
    enableDisablePrevNext(docData);   
    var finalHtml =  html;

    $("#docList").html(finalHtml);
    TablesDataTables.init();
    TablesDataTablesEditor.init();

}
//END FUNCTION FOR DISPLAY DATA OF DOCUMENT
function renderSelectedTagTiles(searchValue,searchdocType){
    VIEWJSONDATALENGTH =0;   
    var newArr =[];
    var filterVal = $("#docType").val();
    for(var k=0;k<DOCJSON.length;k++){
        var arrIndex = k;
        var docId = DOCJSON[k]['PK_DOC_ID'];
        var docName = DOCJSON[k]['DOC_NAME'];
        var docType = parseInt(DOCJSON[k]['DOC_TYPE']);
        var docFileType = DOCJSON[k]['FILE_TYPE'];
        var doclink = DOCJSON[k]['doclink'];
        if((filterVal==0 || docType == filterVal) && docName==searchValue){
            newArr.push(DOCJSON[k]);            
        }
        if(k== (DOCJSON.length-1)){
            displayDocData(0,newArr);
        }
    } 
}

//START FUNCTION FOR REGISTER SEARCH TEXT BOX AUTOCOMPLETE
function registerTagsSearch(){
    $("#searchGalleryTag").autocomplete({
        source: searchTagArr,
        focus: function (event, ui) {
            event.preventDefault();
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            console.log(ui.item.label);
            console.log(ui.item.value);
            renderSelectedTagTiles(String(ui.item.value),ui.item.type);
            $(this).val('');
        },
        change: function (event, ui) {
            if (ui.item == null) {
                this.value = '';
                toastr.warning('Please select a value from the list'); // or a custom message
            }
        }
    });
}
//END FUNCTION FOR REGISTER SEARCH TEXT BOX AUTOCOMPLETE

//START FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA
function registerCanvasEvent(){
    var bfsendMsg = "Sending data to server..";

    $("#addDocument").off().on('submit', (function (e) {

        if( checkAuth(18, 69, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1);

            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                if (data.status == "Success") {
                    $(".fileinput-exists").trigger("click");
                    $('#docName').val("");
                    $('#docFile').val("");
                    $('#docTypeAdd').val("1").selectpicker('refresh');
                    $("#docType").val("0").selectpicker('refresh');
                    VIEWJSONDATALENGTH = 0;
                    getDocData();

                } else {
                    toastr.success(data.status);
                }
                // $("#ajaxloader").addClass("hideajaxLoader");
                addRemoveLoader(0);
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                // $("#ajaxloader").addClass("hideajaxLoader");
            }
        });
    }));
}
//END FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA

//START FUNCTION FOR ADD DOCUMENT
function addDodument(){
            
    var docName = $('#docName').val();
    var docFile = $('#docFile').val();
    var docTypeAdd = $('#docTypeAdd').val();
    var inqAttachment = ($('#inqAtt').prop('checked') == true ) ? "1" : "0";
    var branch = $('#branchListSelect').val();

    if(docFile==""){
        toastr.warning("Please Attach File Before Saving Doc");
        $('#docFile').click();
        return false;
    }else if(docName=="" || docTypeAdd==""){
        toastr.warning("Please fill all details");
        $('#docName').focus();
        return false;
    }

    branch = ( (inqAttachment == "1") ? branch : "-1" );
    branch = ( (branch == "-1") ? "" : branch ); 

    var temp_json = {
        requestCase: "addDocument",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(18, 69),
        docName: docName,
        docType : docTypeAdd,
        inqAttach : inqAttachment,
        orgType : branch
    };
    $("#postData").val(JSON.stringify(temp_json));
    // console.log(temp_json);
    $("#addDocument").submit();

}
//END FUNCTION FOR ADD DOCUMENT

//START FUNCTION FOR DELETE DOCUMENT
function deleteDocument(docEle){

    if( checkAuth(18, 72, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

    var docId = $(docEle).attr("data-docId");
    var postData = {
        requestCase: 'deleteDocument',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(18, 72),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        docId : docId
    }
    commonAjax(COMMONURL, postData, getDocData, GETTINGCLIENT);
}
//END FUNCTION FOR DELETE DOCUMENT
//
//START FUNCTION FOR CALL API FOR CLIENT LISTING
function getDocData() {

    
    var postData = {
        requestCase: 'getDocumentListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(18, 70),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        
    }
    commonAjax(COMMONURL, postData, callBackGetDocListing, GETTINGCLIENT);
}
//END FUNCTION FOR CALL API FOR CLIENT LISTING

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 14-MAY-2018 BY VISHAKHA TAK
function initDocMgmtTour(){

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: "#docFile",
            title: "<b>Select document</b>",
            content: "Select a document to add!!",
            placement: 'bottom',
        },
        {
            path: "",
            element: '#docName',
            title: "<b>Document Name</b>",
            content: "Add a name for the attached document",
            placement: 'bottom',
        },
        {
            path: "",
            element: ".docTypeAddDiv",
            title: "<b>Category</b>",
            content: "Select category of document!<br/> <b>Note:</b> Set profile option is available when standard category is selected.",
            placement: 'bottom',
            onNext: function (tour) {
                if( $('#docTypeAdd').val() != 1 ){

                    setTimeout(function() {
                       
                        tour.setCurrentStep(5); // JUMP TO STEP 5
                        // Start the tour
                        tour.start(true);

                    },1);
                }
            }
        },
        {
            path: "",
            element: "#inqAttDiv",
            title: "<b>Set Profile</b>",
            content: "If you select standard category, then you can check profile checkbox and also select a branch for the same ",
            placement: 'bottom',
            onNext: function (tour){
                $('#inqAtt').click();
            }
        },
        {
            path: "",
            element: '#branchDiv',
            title: "<b>Branch</b>",
            content: "Select branch for the document!",
            placement: 'bottom', 
        },
        {
            path: "",
            element: '.saveDoc',
            title: "<b>Save</b>",
            content: "Click and save your document details!",
            placement: 'left', 
        },
        {
            path: "",
            element: '#docList',
            title: "<b>Documents</b>",
            content: "Available documents list .<br/> You can view and delete a document!",
            placement: 'top',
        },
        {
            path: "",
            element: '#searchGalleryTag',
            title: "<b>Search Gallery</b>",
            content: "Search your document from here!",
            placement: 'top',
        },
        {
            path: "",
            element: '.docTypeCatDiv',
            title: "<b>Category</b>",
            content: "View your document according to category!",
            placement: 'top',
        },
        {
            path: "",
            element: '#selfTourBtn',
            title: "<b>Tour end</b>",
            content: "Tour ends here...!!!",
            placement: 'left', 
            onShow: function (tour) {
                tour.end();
            }
        },
    ];
   
    // Instance the tour
    instanceTour();
    // tour._options.redirect = false;
    tour._options.name = "Document Mgmt Tour";
    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM ZEROth ELEMENT TO PREVENT FROM DISPLAYING LAST STEP

    // Start the tour
    tour.start(true);
}