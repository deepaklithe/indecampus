    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 23-07-2018.
 * File : chargePostingController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_CHARGE_POST_DATA = []; // CHARGE POST JSON 
var GBL_CHARGE_ITEM_JSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents() {
	
	$('#chargeDate').datepicker( 'update' , mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' ));
	autoCompleteForItem('itemName',saveItemData);
	autoCompleteForStudent('studentName',saveStudentNameData);
	renderChargePost();
}

// ADD CHARGE POST ENTRY
function addChargePost() { 
		
	var itemName = $('#itemName').val();
	var itemId = $('#itemName').attr('itemId');
	var chargeDate = $('#chargeDate').val();
	var studentName = $('#studentName').val();
	var studentId = $('#studentName').attr('studentId');
	var membershipNumber = $('#studentName').attr('membershipNumber');
	var chargeQty = $('#chargeQty').val();
	var grossAmount = $('#grossAmount').val();

	if( itemName == "" || itemId == "" || itemId == undefined ){
		toastr.warning("Please search item first");
		addFocusId('itemName');
		return false;
	}else if( studentName == "" || studentId == "" || studentId == undefined ){
		toastr.warning("Please search student name or student membership Id");
		addFocusId('studentName');
		return false;
	}else if( chargeQty == "" || chargeQty < 0 ){
		toastr.warning("Please enter atleast 1 quantity");
		addFocusId('chargeQty');
		return false;
	}
	
	var itemType = "";
	var cgstAmt = 0;
	var cgstPer = 0;
	var sgstAmt = 0;
	var igstAmt = 0;
	var sgstPer = 0;
	var igstPer = 0;
	var itemPrice = 0;
	var itemPriceAftrQty = 0;
	var hsnCode = "";
	var itemFinalPrice = "";
	if( GBL_CHARGE_ITEM_JSON != "" ){
        itemPrice = GBL_CHARGE_ITEM_JSON.itemPrice;
        itemPriceAftrQty = parseFloat(itemPrice) * parseFloat(chargeQty);
		if( GBL_CHARGE_ITEM_JSON.cgst != "" && GBL_CHARGE_ITEM_JSON.cgst != "0" ){
			cgstAmt = fixedTo2(parseFloat(itemPriceAftrQty) * parseFloat(GBL_CHARGE_ITEM_JSON.cgst) / 100);
			cgstPer = fixedTo2(GBL_CHARGE_ITEM_JSON.cgst);
		}
		if( GBL_CHARGE_ITEM_JSON.sgst != "" && GBL_CHARGE_ITEM_JSON.sgst != "0" ){
			sgstAmt = fixedTo2(parseFloat(itemPriceAftrQty) * parseFloat(GBL_CHARGE_ITEM_JSON.sgst) / 100);
			sgstPer = fixedTo2(GBL_CHARGE_ITEM_JSON.sgst);
		}
		if( GBL_CHARGE_ITEM_JSON.igst != "" && GBL_CHARGE_ITEM_JSON.igst != "0" && GBL_CHARGE_ITEM_JSON.igst != undefined  ){
			igstAmt = fixedTo2(parseFloat(itemPriceAftrQty) * parseFloat(GBL_CHARGE_ITEM_JSON.igst) / 100);
			igstPer = fixedTo2(GBL_CHARGE_ITEM_JSON.igst);
		}
		// if( chargeQty != "" && chargeQty != "0" ){
		// 	itemFinalPrice = parseFloat(GBL_CHARGE_ITEM_JSON.itemPrice) + parseFloat(cgstAmt) + parseFloat(sgstAmt);
		// }else{
		itemFinalPrice = parseFloat(itemPriceAftrQty) + parseFloat(cgstAmt) + parseFloat(sgstAmt);
		// }
        hsnCode = GBL_CHARGE_ITEM_JSON.hsnCode;
        itemType = GBL_CHARGE_ITEM_JSON.itemType;

	}
	var tempJson = {
		chargeDate : chargeDate,
		itemName : itemName,
		itemId : itemId,
		studentId : studentId,
		studentName : studentName,
		membershipNumber : membershipNumber,
		chargeQty : chargeQty,
		itemType : itemType,
		cgstAmt : cgstAmt,
		cgstPer : cgstPer,
		sgstAmt : sgstAmt,
		sgstPer : sgstPer,
		igstPer : igstPer,
		itemPrice : fixedTo2(itemPrice),
		hsnCode : hsnCode,
		itemFinalPrice : fixedTo2(grossAmount)	
	}
	GBL_CHARGE_POST_DATA.push(tempJson);
	renderChargePost(); // RENDER CHARGE POST ENTRIES
	resetChargeFields(); // RESET CHARGE FIELDS
}

// RENDER ADDED CHARGE POST ENTRIES
function renderChargePost() {	

	
	var tHead = '<table class="table display table-bordered dataTable" id="chargePostList">'+
			        '<thead>'+
			            '<tr>'+
			                '<th>#</th>'+ 
				            '<th>Student ID</th>'+  
				            '<th>Student Name</th>'+                                         
				            '<th>Charge Date</th>'+                                         
				            '<th>Item</th>'+
				            '<th>Item Type </th>'+
				            '<th>HSN Code</th>'+
				            '<th>Qty</th>'+
				            '<th>Rate</th>'+
				            '<th>CGST</th>'+
				            '<th>SGST</th>'+
				            '<th>IGST</th>'+
				            '<th>Net Amt</th>'+
				            '<th>Action</th>'+
			            '</tr>'+
			        '</thead>'+
			        '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

	if( GBL_CHARGE_POST_DATA != "No record found" && GBL_CHARGE_POST_DATA.length != 0 ){

		GBL_CHARGE_POST_DATA.forEach(function( record , index ) {

			newRow += '<tr>'+
		                '<td>'+ (index+1) +'</td>'+
		                '<td>'+ record.membershipNumber +'</td>'+
		                '<td>'+ record.studentName +'</td>'+
		                '<td>'+ record.chargeDate +'</td>'+
		                '<td>'+ record.itemName +'</td>'+
		                '<td>'+ record.itemType +'</td>'+
		                '<td>'+ record.hsnCode +'</td>'+
		                '<td>'+ record.chargeQty +'</td>'+
		                '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.itemPrice) +'</td>'+
		                '<td>'+ record.cgstPer +'</td>'+
		                '<td>'+ record.sgstPer +'</td>'+
		                '<td>'+ record.igstPer +'</td>'+
		                '<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.itemFinalPrice) +'</td>'+
		                '<td>'+
		                	'<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple" data-toggle="tooltip" data-original-title="Edit" arrIndex="'+index+'"  onclick="editChargePost(this);"><i class="fa fa-pencil"></i></a>'+
		                	'<a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple btn-danger" data-toggle="tooltip" data-original-title="Delete" arrIndex="'+index+'"  onclick="deleteChargePost(this);" style="margin-left: 3px;"><i class="fa fa-trash-o"></i></a>'+
		                	// '<a class="btn btn-sm btn-danger" arrIndex="'+index+'"  onclick="deleteChargePost(this);"><i class="fa fa-trash" ></i></a>'+
		                '</td>'+
		            '</tr>';
		});

	}
	$('#chargeTable').html( tHead + newRow + tEnd );
	$('#chargePostList').dataTable();
}

// DELETE ADDED ENTRIES
function deleteChargePost(context){

	var currIndex = $(context).attr('arrIndex');
	if( currIndex != "" && currIndex != "-1" ){
		var confrm = confirm(CNFDELETE);
		if( !confrm ){
			return false;
		}else{
			GBL_CHARGE_POST_DATA.splice(currIndex,1);
			renderChargePost();
		}
	}

}

// SAVE CHARGE POST
function saveChargePost() {
	
	var chargeArray = [];
	if( GBL_CHARGE_POST_DATA.length == 0 || GBL_CHARGE_POST_DATA == "" ){
		toastr.warning("Please add some entries to create charge posting.");
		return false;
	}else{

		GBL_CHARGE_POST_DATA.forEach(function( record , index ) {
			var tempArr = {
				studentId : record.studentId,
				studentName : record.studentName,
				qty : record.chargeQty,
				chargeDate : record.chargeDate,
				itemId : record.itemId,
				itemName : record.itemName
			}
			chargeArray.push(tempArr);
		});

		var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

	    var postData = {
	        requestCase : 'addChargePosting', 
	        clientId : tmp_json.data[0].FK_CLIENT_ID,
	        userId : tmp_json.data[0].PK_USER_ID,
	        orgId : checkAuth(24 , 97),
	        studentArr : chargeArray,
	        typeOfCust : "STUDENT",
	        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
	    };
	    commonAjax(FOLLOWUPURL, postData, addChargePostCallback,"Please wait...Adding your item.");

	    function addChargePostCallback( flag , data ){
	        if (data.status == "Success" && flag) {

	        	console.log(data);
	        	resetChargePost();

			}else{

	            if (flag)
	              displayAPIErrorMsg( data.status , GBL_ERR_ADD_CHARGE_POST );
	            else
	                toastr.error(SERVERERROR);
	        }
	    }

	}

	
}

// RESET CHARGE POST DATA
function resetChargePost(){
		
	resetChargeFields(); // RESET CHARGE FIELDS
	$('#itemName').val('');
	$('#itemName').attr('itemId','');
	$('#itemType').val('');
	$('#hsnCode').val('');
	$('#itemPrice').val('');
	$('#itemCgst').val('');
	$('#itemSgst').val('');
	$('#itemIgst').val('');
	$('#grossAmount').val('');
	GBL_CHARGE_POST_DATA = [];
	renderChargePost();

}

// PROVIDE SEARCH FOR ITEM
function autoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);
            if( index != -1 ){
            	var cgstAmt = 0;
				var sgstAmt = 0;
				var igstAmt = 0;
				if( ITEMS[index].cgst != "" ){
					cgstAmt = fixedTo2(parseFloat(ITEMS[index].itemPrice) * parseFloat(ITEMS[index].cgst) / 100);
				}
				if( ITEMS[index].sgst != "" ){
					sgstAmt = fixedTo2(parseFloat(ITEMS[index].itemPrice) * parseFloat(ITEMS[index].sgst) / 100);
				}
				if( ITEMS[index].igst != "" && ITEMS[index].igst != undefined ){
					igstAmt = fixedTo2(parseFloat(ITEMS[index].itemPrice) * parseFloat(ITEMS[index].igst) / 100);
				}
				itemFinalPrice = parseFloat(ITEMS[index].itemPrice) + parseFloat(cgstAmt) + parseFloat(sgstAmt) + parseFloat(igstAmt);
        
            	setTimeout(function() {
            		$('#itemName').val( item.text );
            		$('#itemName').attr('itemId',item.value);
            		$('#itemType').val( ITEMS[index].itemType );
            		$('#hsnCode').val( ITEMS[index].hsnCode );
            		$('#itemPrice').val( ITEMS[index].itemPrice );
            		$('#itemCgst').val( ITEMS[index].cgst );
            		$('#itemSgst').val( ITEMS[index].sgst );
            		$('#itemIgst').val( ( ITEMS[index].igst != undefined ? ITEMS[index].igst : "0" ) );
            		$('#grossAmount').val( itemFinalPrice );
            		$('#chargeQty').val( 1 );
            	},50);
    			GBL_CHARGE_ITEM_JSON = ITEMS[index];

            }else{

            	setTimeout(function() {
            		$('#itemName').val( '' );
            		$('#itemName').attr('itemId','');
            		$('#itemType').val( '' );
            		$('#hsnCode').val( '' );
            		$('#itemPrice').val( '' );
            		$('#itemCgst').val( '' );
            		$('#itemSgst').val( '' );
            		$('#itemIgst').val( '' );
            		$('#grossAmount').val( '' );
            		$('#chargeQty').val( 1 );
            	},1);

            }
            
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                var levelVal = $('#userLevel').val();
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "itemSearchForChargePosting",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(15,63),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                        	
                    	if( data.itemId != "-1" ){

	                        var tempJson = {
	                            id: data.itemId,
	                            name: data.itemName,
	                            data: data.itemId,
	                            itemName : data.itemName,
	                            cgst : data.cgst,
	                            hsnCode : data.hsnCode,
	                            itemPrice : data.itemPrice,
	                            itemType : data.itemType,
	                            sgst : data.sgst,
	                            igst : data.igst
	                        }
	                        ITEMS.push(tempJson);
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveItemData(){
}

// PROVIDE SEARCH FOR STUDENT
function autoCompleteForStudent(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( index != -1 ){

            	setTimeout(function() {
            		$('#studentName').attr('studentId',item.value);
            		$('#studentName').attr('membershipNumber',ITEMS[index].membershipNumber);
            		$('#studentName').val( ITEMS[index].studentName );
            	},1);

            }else{

            	setTimeout(function() {
            		$('#studentName').attr('studentId','');
            		$('#studentName').attr('membershipNumber','');
	            	$('#studentName').val( '' );
            	},1);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId:  checkAuth(11,45),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                        	
                    	if( data.studId != "-1" ){

	                        var tempJson = {
	                            id: data.studId,
	                            name: data.studName + ' - ' + data.membershipNumber,
	                            data: data.studName,
	                            studentName : data.studName,
	                            membershipNumber : data.membershipNumber,
	                        }
	                        ITEMS.push(tempJson);

                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}

// RESET CHARGE FIELDS
function resetChargeFields(){

	$('#itemName').val('');
	$('#itemName').attr('itemId','');
	$('#chargeDate').datepicker( 'update' , mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' ) );
	$('#studentName').val('');
	$('#studentName').attr('studentId','');
	$('#studentName').attr('membershipNumber','');
	$('#chargeQty').val("1");
	$('#grossAmount').val( $('#itemPrice').val() );
	$('#itemType').val('');
	$('#hsnCode').val('');
	$('#itemPrice').val('');
	$('#itemCgst').val('');
	$('#itemSgst').val('');
	$('#itemIgst').val('');
	$('#grossAmount').val('');

}

// CALCULATE GROSS AMOUNT ON CHANGE OF QTY
function calculateGrossAmt(context){

	var qty = $(context).val();
	var cgstPer = $('#itemCgst').val();
	var sgstPer = $('#itemSgst').val();
	var igstPer = $('#itemIgst').val();
	var grossAmount = $('#grossAmount').val();
	var itemPrice = $('#itemPrice').val();
	var cgstAmount = 0 , sgstAmount = 0, igstAmount = 0;
	var finalItemPrice = 0;
	if( qty != "" && qty != "0" ){ 

		finalItemPrice = parseFloat(itemPrice) * parseFloat(qty);

		if( cgstPer != "" && cgstPer != "0" ){
			cgstAmount = fixedTo2(parseFloat(finalItemPrice) * parseFloat(cgstPer) / 100);
		}
		if( sgstPer != "" && sgstPer != "0" ){
			sgstAmount = fixedTo2(parseFloat(finalItemPrice) * parseFloat(sgstPer) / 100);
		}
		if( igstPer != "" && igstPer != "0" && igstPer != undefined ){
			igstAmount = fixedTo2(parseFloat(finalItemPrice) * parseFloat(igstPer) / 100);
		}
		var finalPrice = fixedTo2(parseFloat(finalItemPrice) + parseFloat(cgstAmount) + parseFloat(sgstAmount) + parseFloat(igstAmount) )
		$('#grossAmount').val( finalPrice );
	}else{
		toastr.warning("Please enter atleast 1 quantity");
		$(context).val('1');
		addFocusId('chargeQty');
	}
	
}

function editChargePost(context){

	var currIndex = $(context).attr('arrIndex');
	if( currIndex != "" && currIndex != "-1" && currIndex != undefined ){
		$('#itemName').val( GBL_CHARGE_POST_DATA[currIndex].itemName );
		$('#itemName').attr('itemId', GBL_CHARGE_POST_DATA[currIndex].itemId );
		$('#studentName').val( GBL_CHARGE_POST_DATA[currIndex].studentName );
		$('#studentName').attr( 'studentId' , GBL_CHARGE_POST_DATA[currIndex].studentId );
		$('#studentName').attr( 'membershipNumber' , GBL_CHARGE_POST_DATA[currIndex].membershipNumber );
		$('#chargeDate').datepicker( 'update' , GBL_CHARGE_POST_DATA[currIndex].chargeDate );
		$('#chargeQty').val( GBL_CHARGE_POST_DATA[currIndex].chargeQty );
		$('#grossAmount').val( GBL_CHARGE_POST_DATA[currIndex].itemFinalPrice );
		$('#itemType').val( GBL_CHARGE_POST_DATA[currIndex].itemType );
		$('#hsnCode').val( GBL_CHARGE_POST_DATA[currIndex].hsnCode );
		$('#itemPrice').val( GBL_CHARGE_POST_DATA[currIndex].itemPrice );
		$('#itemCgst').val( GBL_CHARGE_POST_DATA[currIndex].cgstPer );
		$('#itemSgst').val( GBL_CHARGE_POST_DATA[currIndex].sgstPer );
		$('#itemIgst').val( GBL_CHARGE_POST_DATA[currIndex].igstPer );
		$('#grossAmount').val( GBL_CHARGE_POST_DATA[currIndex].itemFinalPrice );
		GBL_CHARGE_POST_DATA.splice(currIndex,1);
		renderChargePost();
	}

}