/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : statusMgmtController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var statusData = [];
var catVal = '';
var GBL_USER_OPTIONS = ""; 

function getStatusCategoryData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getCategoryForStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(10,41),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getStatusCategoryDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getStatusCategoryDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        console.log(data);
        var newRow = '';
        for( var i=0; i<data.data.length; i++ ){

            newRow += '<option value="'+ data.data[i].lookupId +'">'+ data.data[i].lookupValue +'</option>';
        }

        $('#category').html( newRow );
        $('#category').val( catVal );
        $('#category').selectpicker('refresh');
        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_STATUS_CAT );
        else
            toastr.error(SERVERERROR);
    }

    getStatusListingData();
}

function getStatusListingData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStatusListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(10,41),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getStatusListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getStatusListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        console.log(data);
        
        statusData = data.data;

        renderStatusData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_STATUS );
        else
            toastr.error(SERVERERROR);
    }
}

function renderStatusData(){

    var newRow = '';
    var count = 1 ;
    
    addFocusId( 'category' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    if(statusData != 'No record found'){

        var thead = '<table id="statusListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>Status Category</th>'+
                            '<th>Status</th>'+
                            '<th>Priority</th>'+
                            '<th>Action</th>'+
                            '<th>Confirm</th>'+
                            // '<th>Approval Status</th>'+
                            '<th>Cancel</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        for( var i=0; i<statusData.length; i++ ){

            count = i + 1;

            var priorityData = '<td></td>'; 
            var statChkDivHideShow = "none";
            if( statusData[i].statusPriorityLevel > 1 || (statusData[i].statusCatName).toUpperCase() == "ORDER" ){

                var checkStatus = "";
                var checkStatusRemark = "";
                var checkStatusApproval = "";
                if( statusData[i].statusThirdPartyFlag == 1 ){
                    checkStatus = "checked";
                }
                if(statusData[i].cancelRemarkFlag == 1){
                    checkStatusRemark = "checked";
                }
                if(statusData[i].approvalFlag == 1){
                    checkStatusApproval = "checked";
                }
                
                if( (statusData[i].statusCatName).toUpperCase() == "BOOKING" || (statusData[i].statusCatName).toUpperCase() == "PROPORTY" || (statusData[i].statusCatName).toUpperCase() == "STUDENT" ){

                    priorityData = '<td class="sm-switch">'+
                                        '<input type="checkbox" categoryId="'+ statusData[i].statusCatId +'" statusId="'+ statusData[i].statusId +'" id="thirdPartyswitch'+ i +'" flagType = "1" class="switch" data-size="large" '+ checkStatus +' style="font-size:14px" />'+
                                    '</td>'+
                                    // '<td></td>'+
                                    '<td class="sm-switch">'+
                                        '<input type="checkbox" categoryId="'+ statusData[i].statusCatId +'" statusId="'+ statusData[i].statusId +'" id="remarkonStatusSwitch'+ i +'" flagType = "2" class="switch-remark" data-size="large" '+ checkStatusRemark +' style="font-size:14px" />'+
                                    '</td>';
                }else{
                    priorityData = '<td class="sm-switch">'+
                                        '<input type="checkbox" categoryId="'+ statusData[i].statusCatId +'" statusId="'+ statusData[i].statusId +'" id="thirdPartyswitch'+ i +'" flagType = "1" class="switch" data-size="large" '+ checkStatus +' style="font-size:14px" />'+
                                    '</td>'+
                                    // '<td class="sm-switch">'+
                                    //     '<input type="checkbox" categoryId="'+ statusData[i].statusCatId +'" statusId="'+ statusData[i].statusId +'" id="approvalonStatusSwitch'+ i +'" flagType = "3" class="switch-remark" data-size="large" '+ checkStatusApproval +' style="font-size:14px" />'+
                                    // '</td>'+
                                    '<td class="sm-switch">'+
                                        '<input type="checkbox" categoryId="'+ statusData[i].statusCatId +'" statusId="'+ statusData[i].statusId +'" id="remarkonStatusSwitch'+ i +'" flagType = "2" class="switch-remark" data-size="large" '+ checkStatusRemark +' style="font-size:14px" />'+
                                    '</td>';
                }
            }else{
                priorityData = '<td></td><td></td>';
            }    

            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ statusData[i].statusCatName +'</td>'+
                            '<td>'+ statusData[i].statusName +'</td>'+
                            '<td>'+ statusData[i].statusPriorityLevel +'</td>'+
                            '<td><a href="javascript:void(0)" statusCatId="'+ statusData[i].statusCatId +'" statusId="'+ statusData[i].statusId +'" statusName="'+ statusData[i].statusName +'" priorityLevel="'+ statusData[i].statusPriorityLevel +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" onclick="updateStatus(this)"><i class="fa fa-pencil"></i></a>'+
                            '<a href="javascript:void(0)" statusId="'+ statusData[i].statusId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="deleteStatusType(this)"><i class="fa fa-trash-o"></i></a></td>'+
                            priorityData +
                        '</tr>'; 
            
        }

        $('#dtTable').html(thead + newRow + tfoot);

        statusData.forEach( function( record , index ){
            if( record.userId != "0" ){
                $('#statId_'+record.statusId).val( record.userId );
            }else{
                $('#statId_'+record.statusId).val( "-1" );
            }
        });

        $('.statUser').selectpicker('refresh');

        $('input.switch').bootstrapSwitch();
        $('input.switch-remark').bootstrapSwitch();
        $('input.switch').on('switchChange.bootstrapSwitch', function(event, state) {
            event.stopPropagation();
            console.log(event);
            console.log(state);
            var inputId = $( this ).attr("id");
            var flag = changeParty(this);
            console.log( flag );

            // setTimeout(function(){
            //     if( returnFlag ){ 
            //         $('#'+inputId).bootstrapSwitch('state', false,true );
            //     }else{
            //         $('#'+inputId).bootstrapSwitch('toggleState', true );
            //     }
            // },500);

            getStatusCategoryData();
            
        });
        
        $('input.switch-remark').on('switchChange.bootstrapSwitch', function(event, state) {

            event.stopPropagation();
            console.log(event);
            console.log(state);
            var inputId = $( this ).attr("id");
            var flag = changeParty(this);
            console.log( flag );

            // setTimeout(function(){
            //     if( returnFlag ){ 
            //         $('#'+inputId).bootstrapSwitch('state', false,true );
            //     }else{
            //         $('#'+inputId).bootstrapSwitch('toggleState', true );
            //     }
            // },500);
            
            getStatusCategoryData();
            $('#category').selectpicker('refresh')
        });
     
     }else{
        toastr.warning('No record found');
    }
    
    $('#statusListing').DataTable({"aaSorting": [],"pageLength": 100,stateSave: true});
//    TablesDataTables.init();
//    TablesDataTablesEditor.init();

}


function assigStatusUser(context){

    var statId = $(context).attr('statid');
    var userId = $('#statId_'+statId).val();
    var smsStat = ($('#statIdChkSMS_'+statId).is(':checked')) == true ? "1" : "0";
    var mailStat = ($('#statIdChkMAIL_'+statId).is(':checked')) == true ? "1" : "0";

    if( userId == "-1" ){
        toastr.warning('Please Select User for assign Status');
        return false;
    }

    $('#chkBoxDiv_'+statId).show();

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'assignUserToOrderStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(16,63),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        statusId: statId,
        assignUserId: userId,
        smsStat: smsStat,
        mailStat: mailStat,
    }
    commonAjax(FOLLOWUPURL, postData, assigStatusUserCallback,"");
    function assigStatusUserCallback(flag,data){

        if (data.status == "Success" && flag) {
            toastr.success('Assigned Status Successfully');

        } else {
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_ASSIGN_FAIL );
            }
            else{
                toastr.warning(SERVERERROR);
            }
        }
    }
}

var gblContext = '';
var returnFlag = '';
var flagType = "";
function changeParty(context){

    if( checkAuth(10,42) == -1 ){
        toastr.error( NOAUTH );
        return false;
    }

    gblContext = context;
    var flag = $( context ).prop("checked");
    var categoryId = $( context ).attr("categoryId");
    var statusId = $( context ).attr("statusId");
    var inputId = $( context ).attr("id");
    flagType = $( context ).attr("flagType");
    
    if( flag == true ){
        flag = 1;
    }else{
        flag = 0;
    }    
    
                
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'changeThirdPartyFlag',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(10,42),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        categoryId: categoryId,
        statusId: statusId,
        thirdPartyFlag: flag,
        flagType: flagType
    }

    commonAjax(FOLLOWUPURL, postData, changePartyDataCallback,"Please Wait... Getting Dashboard Detail");
    return returnFlag;
}
    
function changePartyDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        if(flagType == 1){
            toastr.success('Third Party Submission Changed');
        }else{
            toastr.success('Status on Remark Changed');
        }
        
        returnFlag = true;

    } else {
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_NO_STATUS_CHANGE );
        }
        else{
            toastr.warning(SERVERERROR);
        }
        returnFlag = false;
    }
    return returnFlag;
}




function createStatusType(){

    if( checkAuth(10, 40) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    var status = $('#status').val();
    var priority = $('#priority').val();
    var category = $('#category').val();

    if( status == ""){
        toastr.warning('Please Enter Status');
        $('#status').focus();
        return false;

    }else if( priority == ""){
        toastr.warning('Please Enter Priority');
        $('#priority').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'createStatus',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(10,40),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            priorityLevel : priority,
            categoryId : category,
            statusName : status
        }

        commonAjax(FOLLOWUPURL, postData, createStatusTypeCallback,"Please Wait... Getting Module Detail");
    }
}


function createStatusTypeCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('New Status has been Created');
        console.log(data); 
        // location.reload(); 
        
        var status = $('#status').val();
        var priority = $('#priority').val();
        var category = $('#category').val();
        var categoryName = $('#category option:selected').text();

        var tempData = {
            statusCatId: category,
            statusCatName: categoryName,
            statusId: data.data.statusid,
            statusName: status,
            statusPriorityLevel: priority,
            statusThirdPartyFlag: "0"
        }

        statusData.push( tempData ); 

        renderStatusData(); 
        catVal = category;

        resetData();
        getStatusCategoryData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_STATUS );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteStatusType(context){

    if( checkAuth(10, 43) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        
        var statusId = $(context).attr('statusId');
        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteStatus',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(10,43),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID), 
            statusId : statusId
        }

        commonAjax(FOLLOWUPURL, postData, deletestatusCallback,"Please Wait... Getting Module Detail");
    }
}


function deletestatusCallback(flag,data){

    if (data.status == "Success" && flag) {

        $(gblContext).parents('td').parents('tr').remove();
        toastr.success('Status has been Deleted');
        console.log(data); 

        var statusId = $(gblContext).attr('statusId');

        var index = findIndexByKeyValue( statusData,'statusId',statusId );

        statusData.splice(index,1);

        renderStatusData();

        resetData();
        // location.reload();  

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_STATUS );
        else
            toastr.error(SERVERERROR);
    }
}


function updateStatus(context){

    if( checkAuth(10,42) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    addFocusId( 'category' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    localStorage.POTGstatusIdForUpdate = "";

    var statusid = $(context).attr('statusid');
    var prioritylevel = $(context).attr('prioritylevel');
    var statusname = $(context).attr('statusname');
    var statusCatId = $(context).attr('statusCatId');

    localStorage.POTGstatusIdForUpdate = statusid;

    $('#createStatusBtn').attr('onclick','updateStatusType()');

    $('#priority').val(prioritylevel);
    $('#status').val(statusname);
    $('#category').val(statusCatId);
    $('#category').selectpicker('refresh');
}

function updateStatusType(context){

    var status = $('#status').val();
    var category = $('#category').val();
    var priority = $('#priority').val();
    var statusId = localStorage.POTGstatusIdForUpdate;

    if( status == ""){
        toastr.warning('Please Enter status');
        $('#status').focus();
        return false;

    }else if( priority == ""){
        toastr.warning('Please Enter priority');
        $('#priority').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateStatus',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(10,42),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            priorityLevel : priority,
            categoryId : category,
            statusName : status,
            statusId : statusId
        }
        // console.log(postData); return false;
        commonAjax(FOLLOWUPURL, postData, updateStatusTypeCallback,"Please Wait... Getting Module Detail");
    }
}


function updateStatusTypeCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('Status has been Updated');
        console.log(data); 
        // location.reload();  
        var status = $('#status').val();
        var category = $('#category').val();
        var categoryName = $('#category option:selected').text();
        var priority = $('#priority').val();
        var statusId = localStorage.POTGstatusIdForUpdate;

        var index = findIndexByKeyValue( statusData,'statusId',statusId );

        var tempArray = {
            statusCatId: category,
            statusCatName: categoryName,
            statusId: statusId,
            statusName: status,
            statusPriorityLevel: priority,
            statusThirdPartyFlag: "0"
        }

        statusData[index] = tempArray;

        renderStatusData();

        $('#createStatusBtn').attr('onclick','createStatusType()');

        catVal = category;
        resetData();
        getStatusCategoryData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_STATUS );
        else
            toastr.error(SERVERERROR);
    }
}


function resetData(){

    $('#status').val('');
    $('#priority').val('');
    $('#createStatusBtn').attr('onclick','createStatusType()');
}

function hideShowTr(statId){

    if( $('#row_'+statId).hasClass('hideTr') ){
        $('#row_'+statId).removeClass('hideTr').addClass('showTr');
        $('#icon_'+statId).removeClass('fa-plus-circle').addClass('fa-minus-circle');
    }else{
        $('#row_'+statId).removeClass('showTr').addClass('hideTr');
        $('#icon_'+statId).removeClass('fa-minus-circle').addClass('fa-plus-circle');
    }
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 11-MAY-2018 BY VISHAKHA TAK
function initStatusSelfTour(){

    GBL_TOUR_STEPS = [
        {
            element: ".categoryDiv",
            title: "<b>Status Category</b>",
            content: "Specifies status category on which status has to be created !",
            placement: 'bottom',
            
        },
        {
            element: '.statusDiv',
            title: "<b>Status</b>",
            content: "Specifies status of select status category !",
            placement: 'bottom', 
        },
        {
            element: '.priorityDiv',
            title: "<b>Status priority</b>",
            content: "Specifies status category on which status has to be created !!",
            placement: 'bottom',
        },
        {
            element: '#createStatusTypeBtn',
            title: "<b>Save status</b>",
            content: "Create or update status type!",
            placement: 'right',
            onNext: function (tour) {
                if( statusData.length == 0 || statusData == "" ){
                    tour.end();       // SKIP STEPS AND END TOUR
                }
            } 
        },
        {
            element: '#dtTable',
            title: "<b> Status Listing</b>",
            content: "Status categories and their status according to the priority . <br/> Delete or edit status  . <br/> Change flags for status at third party submission , appproval and remark on status stage.",
            placement: 'top',  
            onShown: function (tour){
                $('.sm-switch:eq(0)').addClass('activeSwitch');
            },
        },
        {
            element: '.activeSwitch',  // FLAGS
            title: "<big><b> Status Flags </b></big>",
            content: "Flag to activate on different priority level and different status.<br/> <u>These status are for :-</u> <ol type='i'><li>- Third Party Submission</li><li>- Approval Status</li><li>- Remark on Status</li></ol> <b> NOTE: These flags can be activated only once per status category !</b>",
            placement: 'right', 
            onShown: function (tour){
                $('.hideRow:eq(0)').addClass('activeHideRow');
            },
            onNext: function (tour) {
                $('.hideRow')[0].click(); // OPEN DIV
            }
        },
        {
            element: '.activeHideRow',  // ORDER 
            title: "<b> Order Status</b>",
            content: "In order status, you can assign the categories to particular users .<br/> Can also activate SMS and mail for the same.",
            placement: 'right', 
            onNext: function (tour) {
                $('.hideRow')[0].click(); // SHOW DIV
            }
        },
        {
            element: '#createStatusTypeBtn',
            title: "<b>Save status</b>",
            content: "Create or update status type!",
            placement: 'right', 
            onShow: function (tour) {
                $('.sm-switch').removeClass('activeSwitch');
                $('.hideRow').removeClass('activeHideRow');
               tour.end();
            },
        },
    ]
    instanceTour();
    tour._options.name = "Client Status Tour";
    tour._options.reflex = true;
    // tour._options.debug = true;
    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM THIS STEP TO PREVENT FROM DISPLAYING LAST STORED STEP

    // Start the tour
    tour.start(true);
    
}