/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportData = [];
var GBL_EXCELJSON = [];
var GBLcustId = "";
var GBLinqId = "";
var GBLOutStand = "";
var GBLexhibProjId = "";

var GBL_PAY_MILEID = "";
var GBL_PAY_FLATID = "";
var GBL_PAY_MILEINQID = "";

var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
var currClientType = roleAuth.data.clientTypeId;
// currClientType = 33;

var ERROR_MSG = "";
function getReportData(){

    reportData = [];
    GBL_EXCELJSON = [];

    $('#exhibtionList').focus();

    var exhibtionId = $('#exhibtionList').val();

    if( exhibtionId == "-1" ){
        toastr.warning('Please Select Exhibition to get Report');
        return false;
    }

    GBLexhibProjId = exhibtionId;
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming

    if( currClientType == "32" ){
        
        ERROR_MSG = GBL_ERR_NO_PROJECT_PAYMENT;
        var postData = {
            requestCase: 'getPaymentReportRealEstate',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(50,200),
            projectId:exhibtionId,
            // inqId:getData[0].inqId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
    }else{
        ERROR_MSG = GBL_ERR_NO_EXHIBITION_PAYMENT;
        var postData = {
            requestCase: 'getpaymentReportExhibition',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(31,126),
            exhibitionId: exhibtionId,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getReportDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        reportData = data.data;
        renderReportData();

    } else {

        reportData = 'No record found';
        renderReportData();

        if (flag)
            displayAPIErrorMsg( data.status , ERROR_MSG );
        else
            toastr.error(SERVERERROR);
    }
}

function renderReportData(){

    var exhibProjId = GBLexhibProjId;

    addFocusId( 'exhibtionList' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    // currClientType = "33"
    if( currClientType == "32" ){

        var GBL_MILESTONE_ARR = [];
        GBL_MILESTONE_ARR = reportData;
        var newRow = '';

        var thead = '<table id="mileStoneListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Unit Name</th>'+
                            '<th>Cust Name - Id</th>'+
                            '<th>Title</th>'+
                            // '<th>Description</th>'+
                            '<th>Due Date</th>'+
                            '<th>Payment Date</th>'+
                            '<th>Amount</th>'+
                            '<th>Outstanding</th>'+
                            '<th>Status</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        $('#mileStoneTable').html(thead + newRow + tfoot);
        $('#tableListing').dataTable();

        GBL_MILESTONE_AMT = 0;
        if( GBL_MILESTONE_ARR != 'No record found' && GBL_MILESTONE_ARR != "" ){
            // console.log(GBL_MILESTONE_ARR);return false;
            var count = 0;
            GBL_MILESTONE_ARR.forEach( function( outerRecrd , outerIndx ){

                var collPayBtn = '<button onclick="navigateToRealEstateBookInq( '+ exhibProjId +' , '+ outerRecrd.inqId +' , '+ outerRecrd.cinqId +' );" type="button" class="btn btn-primary btn-ripple btn-xs"><i class="fa fa-bank"> Project </i></button>';
                if( outerRecrd.flatTrans == "No record found" ){
                    return false;
                }

                outerRecrd.flatTrans.forEach( function( record , index ){

                    if( record.flatDetail == "No record found" ){
                        return false;
                    }

                    count = count + 1;
                    newRow += '<tr>'+
                                '<td>'+ count +'</a></td>'+
                                '<td>'+ record.flatName +'</td>'+                
                                '<td>'+ outerRecrd.custName +' - Inq:'+ outerRecrd.cinqId +'</td>'+                               
                                // '<td></td>'+                               
                                '<td></td>'+                               
                                '<td></td>'+                
                                '<td></td>'+                               
                                '<td></td>'+                               
                                '<td></td>'+           
                                '<td></td>'+           
                                '<td>'+ collPayBtn +'</td>'+           
                            '</tr>';

                    record.flatDetail.forEach( function( innrRecord , innrIndex ){

                        var disabledBtn = "";
                        if( innrRecord.mileStoneoutStanding == "0" ){
                            disabledBtn = "disabled";
                        }

                        // innrRecord.mileStatus = "due";
                        GBL_MILESTONE_AMT = parseFloat( GBL_MILESTONE_AMT ) + parseFloat( innrRecord.mileStonePerc );
                        newRow += '<tr>'+
                                    '<td><span style="display:none;">'+ count +'</span></td>'+
                                    // '<td></td>'+                               
                                    '<td></td>'+                               
                                    '<td></td>'+                               
                                    '<td>'+ innrRecord.mileStoneTitle +'</td>'+                               
                                    // '<td>'+ innrRecord.mileStoneDesc +'</td>'+                
                                    '<td>'+ mysqltoDesiredFormat( innrRecord.mileStoneDueDate , "dd-MM-yyyy" ) +'</td>'+           
                                    '<td>'+ innrRecord.mileStonePaymentDate +'</td>'+   
                                    '<td class="amtDisp" id="amt_'+ innrRecord.mileStoneInqId +'">'+ innrRecord.mileStonePerc +'</td>'+                               
                                    '<td class="amtDisp" id="outStand_'+ innrRecord.mileStoneInqId +'">'+ innrRecord.mileStoneoutStanding +'</td>'+                               
                                    '<td id="payStat_'+ innrRecord.mileStoneInqId +'"><button class="btn btn-sm btn-'+ ( (innrRecord.status).toLowerCase() == "due" ? "danger" : "success" ) +'">'+ innrRecord.status +'</button></td>'+             
                                    '<td><a href="javascript:void(0)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" mileStoneId="'+ innrRecord.mileStoneId +'" id="payBtn_'+ innrRecord.mileStoneInqId +'" outStandAmt="'+ innrRecord.mileStoneoutStanding +'" cust-id="'+ outerRecrd.custId +'" inq-id="'+ outerRecrd.inqId +'" outStandAmt="'+ outerRecrd.mileStoneoutStanding +'" flatId="'+ record.flatId +'" mileStoneInqId="'+ innrRecord.mileStoneInqId +'" onclick="collectPayModal(this)" data-toggle="tooltip" data-placement="top" data-original-title="Collect Payment" '+ disabledBtn +'><i class="fa fa-rupee"></i></a></td>'+             
                                    // '<td>'+ payDateBtn +'</td>'+           
                                '</tr>';
                        });
                });

            });

        }
        $('#inqReportList').html(thead + newRow + tfoot);
        $('#mileStoneListing').dataTable({

            "aoColumns": [
                null,
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                // { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
            ],
            "bPaginate": false,
            "bInfo": false,
            "stateSave": true
        });

    }else{
    
        var count = 1 ;
        var newRowBody = "";
        var newRowHeader = "";
        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Inq No</th>'+
                            '<th>Customer Name</th>'+
                            '<th>Stall No</th>'+
                            '<th>Total Amount</th>'+
                            '<th>Collect Amount</th>'+
                            '<th>Outstanding Amount</th>'+
                            '<th>Inquiry Status</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        if( reportData != 'No record found' ){

            var count = 0;
            reportData.forEach(function( record , index ){
                var stallData = "";
                if( record.stallList != "No record found" ){
                    record.stallList.forEach(function( stallRecord , index ){
                        if( stallData ){
                            stallData += ','+stallRecord.stallName;
                        }else{
                            stallData = stallRecord.stallName;
                        }
                    });
                }else{
                    stallData = '';
                }

                
                if( record.paymentListing != "No record found" ){
                    
                    var collectPay = "";
                    count = count + 1;
                    
                    record.paymentListing.forEach( function( innerRecord , innerIndex ){

                        if( innerRecord.outStandingAmount <= "0" || record.confirmInqFlag == "0" ){ 
                            collectPay = "disabled";
                        }
                        newRowBody += '<tr>'+
                                '<td><span style="display:none;">'+ count +'</span></td>'+
                                '<td><span style="display:none;">'+ record.inqCId +'</span></td>'+
                                '<td><span style="display:none;">'+ record.customerName +'</span></td>'+
                                '<td><span style="display:none;">'+ stallData +'</span></td>'+
                                '<td class="amtDisp">'+  numberFormat( innerRecord.totalAmount ) +'</td>'+
                                '<td class="amtDisp">'+  numberFormat( innerRecord.amount ) +'</td>'+
                                '<td class="amtDisp">'+  numberFormat( innerRecord.outStandingAmount ) +'</td>'+
                                '<td><span style="display:none;">'+ record.statusName +'</span></td>'+
                            '</tr>';

                    });
                    var collPayBtn = '<a href="javascript:void(0)"><button outStandAmt="'+ record.paymentListing[0].outStandingAmount +'" cust-id="'+ record.custId +'" inq-id="'+ record.inqId +'" type="button" '+ collectPay +' class="btn btn-primary btn-ripple btn-xs btn-danger" onclick="collectPayModal(this)"><i class="fa fa-rupee"> Collect Pay</i></button>';
                    newRowHeader += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td><a href="javascript:void(0)" inqid="'+ record.inqId +'" onclick="navigateToInquiryDetailOpenWin(this)">'+ record.inqCId +'</a></td>'+
                                '<td>'+ record.customerName +'</td>'+
                                '<td>'+ stallData +'</td>'+
                                '<td class="amtDisp"> </td>'+
                                '<td class="amtDisp"> </td>'+
                                '<td>'+ collPayBtn +'</td>'+
                                '<td>'+ record.statusName +'</td>'+
                            '</tr>';
                }
            });
        }
        $('#inqReportList').html(thead + newRowHeader + newRowBody + tfoot);

        $('#tableListing').dataTable({
            "aoColumns": [
                null,
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false },
            ],
            "pageLength": 50,
            "bPaginate": false,
            "bInfo": false,
            "stateSave": true
        });
        
    }
}

function collectPayModal(context){

    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    
    setOption("0", "payType", roleAuth.data.paymentMode, "---Select Payment Mode---");
    $('#payType').selectpicker('refresh');

    setOption("0", "payMode", roleAuth.data.paymentType, "---Select  Payment Type---");
    $('#payMode').selectpicker('refresh');

    GBLcustId = $(context).attr('cust-id');
    GBLinqId = $(context).attr('inq-id');
    GBLOutStand = $(context).attr('outStandAmt');
    GBL_PAY_MILEINQID = $(context).attr('mileStoneInqId');
    
    GBL_PAY_MILEID = $(context).attr('mileStoneId');
    GBL_PAY_FLATID = $(context).attr('flatid');

    $('#creditCardPaymentCollect').hide();
    $('#ddPaymentCollect').hide();
    $('#cashPaymentCollect').hide();
    
    $('#paymentModal').modal({
      backdrop: 'static'
    });   
}

function sendPay(){

    //var payMode = $('#payMode').val();
    var payType = $('#payType').val();
    
    // CASH & OTHER PAYMENT TYPE FIELDS
    var amount = "";
    var chequeChargeNo = "";
    var bankName = "";
    var dateOrLast4Digit = "";
    var paymentRemark = "";
    var flag = 0;
    
    if( payType == "-1" ){
        toastr.warning('Please Select Payment Mode');
        $('#payMode').focus();
        return false;

    }
    
    if(payType == "36" ){ // CASH
        
        amount = $("#payAmount").val();
        paymentRemark = $("#payRemark").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#payAmount').focus();
            flag = 1;
        }else{
            flag = 0;            
        }
        
        if(flag){
            return false;
        }
    }
    
    if(payType == "37" ){ // DD
       
        amount = $("#ddpayAmount").val();
        chequeChargeNo = $("#ddcheckNo").val();
        bankName = $("#BankNamedd").val();
        dateOrLast4Digit = $("#chequeDatedd").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#ddpayAmount').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(chequeChargeNo.length <= 0){
            toastr.warning('Please Enter Valid DD / Cheque Number');            
            $('#ddcheckNo').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(bankName.length <= 0){
            toastr.warning('Please Enter Valid Bank Name');            
            $('#BankNamedd').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(dateOrLast4Digit.length <= 0){
            toastr.warning('Please Enter Valid date');            
            $('#chequeDatedd').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(flag){
            return false;
        }
    }
    
    if(payType == "38" ){ // CC
        
        amount = $("#ccpayAmount").val();
        chequeChargeNo = $("#ccChargeSlipNo").val();
        bankName = $("#BankName").val();
        dateOrLast4Digit = $("#CardL4digit").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount.');           
            $('#ccpayAmount').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(chequeChargeNo.length <= 0){
            toastr.warning('Please Enter Valid Charge Slip Number.');            
            $('#ccChargeSlipNo').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(bankName.length <= 0){
            toastr.warning('Please Enter Valid Bank Name.');             
            $('#BankName').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        
        if(dateOrLast4Digit.length <= 0){
            toastr.warning('Please Enter Valid last 4 digit of credit card.');            
            $('#CardL4digit').focus();
            flag = 1;
            return false;
        }else{
            flag = 0;
        }
        
        if(flag){
            return false;
        }
        
    }
    
    if(payType == "39" ){ // OTHER
        amount = $("#payAmount").val();
        paymentRemark = $("#payRemark").val();
        
        if(amount == 0 || amount < 0){
            toastr.warning('Please Enter Valid Amount');            
            $('#payAmount').focus();
            flag = 1;
        }else{
            flag = 0;
        }
        
        if(flag){
            return false;
        }
    }
     
    var remainToPay = GBLOutStand;

    if( parseFloat(amount) > parseFloat(remainToPay) ){
        toastr.error('You cannot Pay More than '+remainToPay+'rs.');
        $('#payAmount').focus();
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    
    var postData = {
        requestCase: 'collectPayment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        inqId : GBLinqId,
        custId : GBLcustId,
        paymentMode : "-1",
        paymentType : payType,
        paymentRemark : paymentRemark,
        paymentAmount : amount,
        paymentfiled1 : chequeChargeNo,
        paymentfiled2 : bankName,
        paymentfiled3 : dateOrLast4Digit,
    }

    if( currClientType == "32" ){
        postData.flatId = GBL_PAY_FLATID;
        postData.mileStoneId = GBL_PAY_MILEID;
        postData.mileStoneInqId = GBL_PAY_MILEINQID;
    }
    // console.log(postData);return false;
    commonAjax(COMMONURL, postData, sendPayCallback,"Please Wait... Getting Inquiry Data");

        function sendPayCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
            // location.reload();
            $('#paymentModal').modal('hide');

            // if( currClientType == "32" ){
            //     $('#payBtn_'+GBL_PAY_MILEINQID).attr('disabled',true);
            // }else{
            // }
            getReportData();
            resetPayModal();
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_COLLECT_PAYMENT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function resetPayModal(){

    $('#payMode').val('-1');
    $('#payMode').selectpicker('refresh');
    $('#payType').val('-1');
    $('#payType').selectpicker('refresh');
    $('#payRemark').val('');
    $('#payAmount').val('');
    $('#ddcheckNo').val('');
    $('#BankNamedd').val('');
    $('#chequeDatedd').val('');
    $('#ccChargeSlipNo').val('');
    $('#BankName').val('');
    $('#CardL4digit').val('');
}

function getExhibitionData(type){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming

    var ERROR_MSG_LIST = "";
    if( type == "1" ){
        ERROR_MSG_LIST = GBL_ERR_NO_EXHIBITION;
        var postData = {
            requestCase: 'getExhibitionMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,13),
            stateId: "",
            cityId: "",
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
    }else{
        ERROR_MSG_LIST = GBL_ERR_NO_PAYMENT;
        var postData = {
            requestCase: 'getRealEstateMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(50,200),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
    }

    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getExhibitionDataCallback,"Please Wait... Getting Dashboard Detail");

    function getExhibitionDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            exhibitionData = data.data;

            var options = "<option value='-1'> Please Select Exhibition </option>";
            exhibitionData.forEach(function( record , index ){

                var selectedOpt = "";
                if( index == "0" ){
                    selectedOpt = "selected";
                }
                if( type == "1" ){
                    options += '<option '+ selectedOpt +' value="'+ record.exibitionId +'"> '+ record.exibitionName +' </option>';
                }else{
                    options += '<option '+ selectedOpt +' value="'+ record.projectId +'"> '+ record.projectName +' </option>';
                }
            });

            $('#exhibtionList').html( options );
            $('#exhibtionList').selectpicker( 'refresh' );

            getReportData();

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , ERROR_MSG_LIST );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function changeFiled(paymentType){
    //console.log(outStandingTotal);
    if(paymentType == "36"){ // CASH
       $("#cashPaymentCollect").show();
       $("#ddPaymentCollect").hide();
       $("#creditCardPaymentCollect").hide();
       $("#payAmount").val(GBLOutStand);
    }else if(paymentType == "37"){ // DD 
       $("#ddPaymentCollect").show();
       $("#cashPaymentCollect").hide();
       $("#creditCardPaymentCollect").hide();
       $("#ddpayAmount").val(GBLOutStand);
    }else if(paymentType == "38"){ // CREDIT CARD
        $("#creditCardPaymentCollect").show();
        $("#ddPaymentCollect").hide();
        $("#cashPaymentCollect").hide();
        $("#ccpayAmount").val(GBLOutStand);
    }else if(paymentType == "39"){ // OTHER
        $("#cashPaymentCollect").show();
        $("#ddPaymentCollect").hide();
        $("#creditCardPaymentCollect").hide();
        $("#payAmount").val(GBLOutStand);
    }

    if( currClientType == "32" ){
        $("#payAmount").attr('disabled',true);
        $("#ddpayAmount").attr('disabled',true);
        $("#ccpayAmount").attr('disabled',true);
    }
}

