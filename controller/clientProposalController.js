/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : clientProposalController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var proposalGetData = [];
function getProposalData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getSampleSectionListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(15,58),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getProposalDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getProposalDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        proposalGetData = data.data;

        renderProposalData();
        

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_SECTION );
        else
            toastr.error(SERVERERROR);
    }
}
 
function renderProposalData(){

    var newRow = '';
    var count = 1 ;

    if( proposalGetData != 'No record found' ){

        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
            '<thead>'+
                '<tr>'+
                    '<th>No</th>'+
                    '<th>Name</th>'+
                    '<th>Action</th>'+
                '</tr>'+
            '</thead>'+
            '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        for( var i=0; i<proposalGetData.length; i++ ){

            var name = proposalGetData[i].sectionName;
            var sectionId = proposalGetData[i].sectionId;
            
            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ name +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" sectionName="'+ name +'" sectionId="'+ sectionId +'" onclick="editProposal(this)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>'+ 
                                '<a href="javascript:void(0)" sectionId="'+ sectionId +'" onclick="deleteProposal(this)" class="btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';
        }

        
        // $('#moduleList').html(newRow);
        $('#dtTable').html(thead + newRow + tfoot);
    
    }else{
         displayAPIErrorMsg("" , GBL_ERR_NO_SECTION );
    }
    

    TablesDataTables.init();
    TablesDataTablesEditor.init();
    
}


function addProposal(){

    if( checkAuth(15, 57, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    $('#saveUserGroup').attr('onclick','createSection()');

    $('#secName').val('');

    tinyMCE.get('proposalData').setContent('');

    $('#proposal').modal('show');
    $('#secName').focus();
    addFocusId( 'secName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function createSection(context){

    var secName = $('#secName').val().trim();
    var sectionDetail = tinyMCE.get('proposalData').getContent();

    if( secName.length == 0 ){
        toastr.warning('Please Enter section Name');
        $('#secName').focus();
        return false;

    }else if( sectionDetail == ""){
        toastr.warning('Please Enter data');
        $('#sectionDetail').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'createSampleSection',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(15,57),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            sectionName : secName,
            sectionDetail : sectionDetail,
        }  
        $('#proposal').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, createSectionCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function createSectionCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // location.reload();

        var secName = $('#secName').val().trim();
        var sectionDetail = tinyMCE.get('proposalData').getContent();

        var tempData = {
            sectionId: data.data.sectionId,
            sectionName: secName,
            sectionDetail: sectionDetail,
        }

        proposalGetData.push( tempData ); 

        renderProposalData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_SECTION_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

function editProposal(context){

    if( checkAuth(15, 59, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }
    var sectionId = $(context).attr('sectionId');
    var sectionName = $(context).attr('sectionName');

    $('#saveUserGroup').attr('onclick','editSaveProposal(this)');
    $('#saveUserGroup').attr('sectionId',sectionId);

    var index = findIndexByKeyValue(proposalGetData,'sectionId',sectionId);
    
    var proposalContent = proposalGetData[index].sectionDetail;

    $('#secName').val(sectionName);

    // $('.ng-pristine.ng-valid.ta-bind.ng-not-empty').html( proposalContent );
    console.log(proposalContent);
    tinyMCE.get('proposalData').setContent(proposalContent);

    $('#proposal').modal('show'); 
    addFocusId( 'secName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var secIdforEdit = '';
function editSaveProposal(context){

    var secName = $('#secName').val();
    secIdforEdit = $(context).attr('sectionId');
    var sectionDetail = tinyMCE.get('proposalData').getContent();

    if( secName == ""){
        toastr.warning('Please Enter group Name');
        $('#grpName').focus();
        return false;

    }else{
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'updateSampleSection',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(15,59),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            sectionName : secName,
            sectionDetail : sectionDetail,
            sectionId : secIdforEdit,
        }  

        $('#proposal').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, editProposalCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function editProposalCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // location.reload();

        var index = findIndexByKeyValue( proposalGetData,'sectionId',secIdforEdit );

        var secName = $('#secName').val();
        var sectionDetail = tinyMCE.get('proposalData').getContent();

        var tempArray = {
            sectionId: secIdforEdit,
            sectionDetail: sectionDetail,
            sectionName: secName
        }

        proposalGetData[index] = tempArray;

        renderProposalData();

        $('#saveUserGroup').attr('onclick','createSection()');
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_SECTION_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteProposal(context){
 if( checkAuth(15, 60, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
       
        var sectionId = $(context).attr('sectionId');
        gblContext = context;

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteSampleSection',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(15,60),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            sectionId : sectionId,
        }  

        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, deleteProposalCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function deleteProposalCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        // location.reload();
        $(gblContext).parents('td').parents('tr').remove(); 
        var sectionId = $(gblContext).attr('sectionId');
        toastr.success('Proposal Deleted Successfully.');

        var index = findIndexByKeyValue( proposalGetData,'sectionId',sectionId );

        proposalGetData.splice(index,1);

        renderProposalData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_SECTION_FAILED );
        else
            toastr.error(SERVERERROR);
    }
}

