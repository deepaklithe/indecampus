/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 01-06-2016.
 * File : inquiryListController.
 * File Type : .js.
 * Project : POTG
 *
 * */

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";        
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

function getinquiryListing(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryListCallback(flag,data){

    var customFieldTh = "";
    var customFieldNull = "";
    var customFieldCnt = 0;
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    if(roleAuth.data.customFieldDetail != "No record found"){
        roleAuth.data.customFieldDetail.forEach(function( record , index ){
            if(record.tabType == "INQMASTER"){
                customFieldTh += '<th>'+ record.fieldName +'</th>';
                customFieldCnt = customFieldCnt + 1;
                customFieldNull += null+','
            }
        });
    }

    var startHtml = "<table id='tableListing' class='display datatables-alphabet-sorting'>"+
                        "<thead>"+
                            "<tr>"+
                                "<th>#</th>"+
                                "<th>Inquiry No.</th>"+
                                "<th>Inquiry Date</th>"+
                                "<th>Customer Name</th>"+
                                "<th>Inquiry Name</th>"+
                                "<th>Inquiry Remark</th>"+
                                "<th>Assigned Person</th>"+
                                "<th>Status</th>"+
                                customFieldTh +
                            "</tr>"+
                        "</thead>"+
                        "<tbody>";
    var endHtml = "</tbody>";

    if (data.status == "Success" && flag) {
    
        var inqRow = '';
        var inqData = data.data;

        if( inqData != 'No record found' ){
            
            for( var i=0; i<inqData.length; i++ ){
                
                var status = inqData[i].statusName;

                if( status == "" ){
                    status = "open";
                } 
                
                var customFieldTd = "";
                for( var j=0; j<customFieldCnt; j++ ){
                    customFieldTd += '<td>'+ inqData[i]['field'+( j+1 )] +'</td>';
                }

                inqRow += '<tr>'+
                            '<td>'+ (i+1) +'</td>'+
                            '<td><a href="javascript:void(0)" custId="'+ inqData[i].custId +'" inqId="'+ inqData[i].inqId +'" onclick="navigateToInquiryDetailOpenWin(this)">'+ inqData[i].clientLevelInqId +'</a></td>'+
                            '<td><span class="hide">'+ ( inqData[i].inqDate ).replace(/-/g, '/') +'</span>'+ mysqltoDesiredFormat( inqData[i].inqDate , 'dd-MM-yyyy' ) +'</td>'+
                            '<td>'+ inqData[i].custDetail.fName + ' ' + inqData[i].custDetail.lName + '</td>'+
                            '<td>'+ inqData[i].projectName +'</td>'+
                            '<td>'+ inqData[i].inqRemark +'</td>'+
                            '<td>'+ inqData[i].inqAssignToName +'</td>'+
                            '<td>'+
                                '<button type="button" class="btn btn-amber btn-ripple btn-sm">'+ status +'</button>'+
                            '</td>'+
                            customFieldTd +
                        '</tr>';
                                                        
            }
        }
        
        var finalHtml = startHtml + inqRow + endHtml;
        $('#inquiryTable').html(finalHtml);
        
        // TablesDataTables.init();
        // TablesDataTablesEditor.init();
        $('#tableListing').dataTable({
            // "aoColumns": [
            //     null,
            //     null,
            //     {"sType": "date-uk"},
            //     null,
            //     null,
            //     null,
            //     null,
            //     null,
            // ],
            "pageLength": 50,
            "stateSave": true
        });

        addFocusId( 'typeStatusListing' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        
    } else {        
        addFocusId( 'typeStatusListing' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
        if (flag){
            var finalHtml = startHtml + endHtml;
            $('#inquiryTable').html(finalHtml);
            TablesDataTables.init();
            TablesDataTablesEditor.init();
            
        }else{
            toastr.error(SERVERERROR);
        }            
    }
}

function getInqListingByStatus(id){
    if( id == "undefined" ){
        id = "-1";
    }
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetailByStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        statusId : id
    }

    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}


function pageInitialEvents(){
    var localStatus = JSON.parse(localStorage.indeCampusRoleAuth);
    var index = findIndexByKeyValue(localStatus['data']['statusListing'],'statusCatName','INQUIRY');

    var statusList = localStatus['data']['statusListing'][index]['statusListing'];

    statusList.sort(function(a, b) {
        return parseFloat(a.statusPriority) - parseFloat(b.statusPriority);
    });

    setOption("0", "typeStatusListing", statusList, "All");
    $('#typeStatusListing').selectpicker();
    
    
    if(localStorage.POTGcurrentStatusId != undefined){
        getInqListingByStatus(localStorage.POTGcurrentStatusId);
        $('#typeStatusListing').val(localStorage.POTGcurrentStatusId);
        localStorage.removeItem('POTGcurrentStatusId');
        $('#typeStatusListing').selectpicker("refresh");
    }else{
        getInqListingByStatus("-1");
    }
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);

    setOption("0", "custEditEthinicity", data.data.ethinicity, "--Ethnicity--");
    setOption("0", "custEditOccupation", data.data.occupation, "--Occupation--");
    setOption("0", "custEditType", data.data.customertype, "--Select Customer Type--");
    setOption("0", "custStateId", data.data.cityStateListing, "--Select State --");
    $("#custStateId").selectpicker();    
    $("#custCityId").selectpicker(); 

    $('#typeStatusListing').focus();
}