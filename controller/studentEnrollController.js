    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 10-07-2018.
 * File : studentEnrollController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_ENROLL_DATA = [];
var GBL_STD_ID = "";
var GBL_ATTACH_TYPE = "";

// GET ENROLLMENT LISTING DATA
function getEnrollmentData() {

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStudentData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11,45),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getEnrollmentDataCallback,"Please Wait... Getting Dashboard Detail");

}

// GET ENROLLMENT LISTING DATA CALLBACK
function getEnrollmentDataCallback( flag , data ){

	if (data.status == "Success" && flag) {
        console.log(data);
        GBL_ENROLL_DATA = data.data;
        renderStudentList(GBL_ENROLL_DATA);

    } else {

    	GBL_ENROLL_DATA = [];
    	renderStudentList(GBL_ENROLL_DATA);
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_STUDENT );
        else
            toastr.error(SERVERERROR);
    }

}

// RENDER STUDENT ENROLLMENT LISTING
function renderStudentList(GBL_ENROLL_DATA){

	var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+    
                        '<th>Student ID</th>'+
                        '<th>Student Name</th>'+ 
                        '<th>Enrollment Date</th>'+
                        '<th>Room Number</th>'+
                        '<th>Mobile Number</th>'+
                        '<th>Email</th>'+                        
                        '<th>Status</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';
    var newRow = '';
    if( GBL_ENROLL_DATA.length != 0 && GBL_ENROLL_DATA != "No record found" ){

        GBL_ENROLL_DATA.forEach(function( record , index ){
            var renewClass = '';
            var studStatus = "Active";
            var btnClass = "btn-success";
            var disabledClass = '';
            var disabledStatus = '';
            var studStatusIndex = findIndexByKeyValue( STUDENT_STATUS , 'ID' , record.status );
            if( studStatusIndex != -1 ){
                studStatus = STUDENT_STATUS[studStatusIndex].VALUE;
            }
            if( studStatus.toLowerCase() != "active" ){
                btnClass = "btn-danger";
                disabledClass = "disabled";
            }
            if( studStatus.toLowerCase() == "termination" || studStatus.toLowerCase() == "suspension" ){
                disabledStatus = "disabled";
            }
            var hideClass = '';
            if( record.renewStatus != "1" || record.tenacyEndDate == null  ){
                renewClass = 'hide';
            }

            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td><a href="javascript:void(0);" studId="'+ record.studId +'" onclick="studentDetail(this);">'+ record.memberShipNumber +'</a></td>'+
                            '<td>'+ record.firstName + ' ' + record.lastName +'</td>'+ 
                            '<td>'+ mysqltoDesiredFormat(record.enrollmentDate,'dd-MM-yyyy') +'</td>'+ 
                            '<td>'+ (record.roomNo != null ? record.roomNo : "") +'</td>'+
                            '<td>'+ record.mobileNo +'</td>'+
                            '<td>'+ record.email +'</td>'+                            
                            '<td>'+ 
                                '<a href="javascript:void(0)" studId="'+ record.studId +'" studStatusId="'+ record.status +'" onclick="changeStudentStatus(this);" class="btn-right-mrg btn btn-xs '+btnClass+' btn-ripple '+disabledStatus+'">'+ studStatus +'</a>' +
                            '</td>'+ 
                            '<td>'+

                                '<a href="javascript:void(0);" onclick="viewAttachment(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-amber btn-ripple" data-toggle="tooltip" data-placement="top" data-original-title="View Attachment" '+disabledClass+'><i class="fa fa-eye"></i></a>'+
                                '<a href="javascript:void(0);" onclick="editStudent(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" data-placement="top" data-toggle="tooltip" data-original-title="Edit Enrollment" '+disabledClass+'><i class="fa fa-pencil"></i></a>'+
                                '<a href="javascript:void(0);" onclick="navigateToRenewEnrollment(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-light-green btn-ripple '+hideClass+' '+renewClass+'" data-toggle="tooltip" data-placement="top" data-original-title="Renew Enrollment" '+disabledClass+'><i class="fa fa-graduation-cap"></i></a>'+
                                '<a href="javascript:void(0);" class="btn-right-mrg btn btn-xs btn-teal btn-ripple" studId="'+ record.studId +'" data-toggle="tooltip" data-placement="top" data-original-title="Collect Payment" onclick="navigateToCollectPayment(this);" '+disabledClass+'><i class="fa fa-credit-card"></i></a>'+
                                // '<a href="javascript:void(0);" onclick="navigateToGenerateInvoice(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-brown btn-ripple" data-toggle="tooltip" data-placement="top" data-original-title="View Invoice"><i class="fa fa-file-text-o "></i></a>'+
                                '<a href="javascript:void(0);" onclick="navigateToViewInvoice(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-brown btn-ripple" data-toggle="tooltip" data-placement="top" data-original-title="View Invoice" '+disabledClass+'><i class="fa fa-file-text-o "></i></a>'+
                                '<a href="javascript:void(0);" onclick="navigateToViewInvoice(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-blue-grey btn-ripple" data-toggle="tooltip" data-placement="top" data-original-title="Send Email" '+disabledClass+'><i class="fa fa-paper-plane"></i></a>'+
                                '<a href="javascript:void(0);" onclick="navigateToViewLedger(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-orange btn-ripple" data-toggle="tooltip" data-placement="top" data-original-title="View Ledger" '+disabledClass+'><i class="fa fa-address-book"></i></a>'+
                                '<a href="javascript:void(0);" onclick="openChangePswdModal(this);" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-cyan btn-ripple" data-toggle="tooltip" data-placement="top" data-original-title="Change Password" '+disabledClass+'><i class="fa fa-eye-slash"></i></a>'+
//                                '<a href="javascript:void(0)" onclick="deleteEnrollment(this)" studId="'+ record.studId +'" class="btn-right-mrg btn btn-xs btn-danger btn-ripple" data-toggle="tooltip" data-original-title="Delete Enrollment"><i class="fa fa-trash"></i></a>'+
                            '</td>'+
                      '</tr>';

        });

    }

    $('#dtTable').html( thead + newRow + tfoot );
    TablesDataTables.init();
    TablesDataTablesEditor.init();
}

// EDIT STUDENT ENROLLMENT 
function editStudent(context){

    var studId = $(context).attr('studId');
    localStorage.indeCampusEditStudId = studId;
    navigateToCreateEnrollment('edit'); // NAVIGATE TO ENROLLMENT CREATE PAGE FOR UPDATE

}

// DELETE STUDENT ENROLLMENT 
function deleteEnrollment(context){

    var studId = $(context).attr('studId');

    var confmDelete = confirm(CNFDELETE);
    if( !confmDelete ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'deleteStudentData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11, 47),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        studentId : studId
    };
    commonAjax(COMMONURL, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {

            var index = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , studId );
            if( index != -1 ){
                GBL_ENROLL_DATA.splice( index , 1 );
                renderStudentList(GBL_ENROLL_DATA);
            }

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_DELETE_ENROLL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
}

// VIEW SELECTED STUDENT'S ATTACHMENT DATA MODAL
function viewAttachment(context){

    var studId = $(context).attr('studId');
    GBL_STD_ID = studId;
    var index = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , studId );
    if( index != -1 ){
        if( GBL_ENROLL_DATA[index].attachmentData.length != 0 && GBL_ENROLL_DATA[index].attachmentData != "No record found" ){
            renderAttachment(GBL_ENROLL_DATA[index].attachmentData);
        }else{
            toastr.error( GBL_ERR_NO_ATTACH );
        }
    }
    $('#uploadMode').val( $("#uploadMode option:first").val() ).selectpicker('refresh');
    $('#uploadMode').trigger('change');
    refreshFile();
    $('#attachmentAdd').modal('show');

}

// RENDER ATTACHMENT DATA
function renderAttachment(attachmentData){ 
    var photoAttachLi = '';
    var medicalAttachLi = '' ;

    // STUDENT'S PHOTO ATTACHMENT DISPLAY
    if( attachmentData.photosArr != "No record found" && attachmentData.photosArr != "" ){
        attachmentData.photosArr.forEach(function( photoRecord , photoIndex ){

            // photoAttachLi += '<li class="">'+
            //             '<div class="list-content">'+
            //                 '<div class="col-lg-8 col-md-4">'+
            //                     '<a href="javascript:void(0)" data-link="'+ photoRecord.attachmentLink +'" onclick="openLinkInNewTab(this);"><span class="title">'+ photoRecord.title +'</span></a></div><div class="col-lg-4 col-md-2"><a data-div="imagepopup" data-link="'+ photoRecord.attachmentLink +'" onClick="viewImageLinkData(this);" style="cursor:pointer;"><i class="fa fa-eye"></i></a><a href="javascript:void(0)" attachType="PHOTO" attachPath="'+ photoRecord.attachmentLink +'" attachmentId="'+ photoRecord.attachmentId +'" onclick="deleteAttachment(this)"><i class="fa fa-trash-o pull-right"></i></a>'+
            //                 '</div>'+
            //             '</div>'+
            //         '</li>';
            var clsHide = "";
            photoAttachLi += "<div class='card card-image card-light-blue bg-image bg-opaque8 col-md-4' style='width: 30%;height: 156px; margin-right: 10px;'>"+
                                "<img src='"+photoRecord.attachmentLink+"' alt='' class='gallery-image'>"+
                                "<div class='context has-action-left has-action-right'>"+
                                    "<div class='tile-content'>"+
                                        "<span class='text-title'>"+photoRecord.title+"</span>"+
                                        "<span class='text-subtitle'>png</span>"+
                                    "</div>"+
                                    "<div class='tile-action right'>"+
                                        "<a data-div='imagepopup' data-link='"+photoRecord.attachmentLink+"' onClick='viewImageLinkData(this);' class='btn btn-sm btn-warning'><i class='fa fa-eye'></i></a>"+
                                        "<a onClick='deleteAttachment(this)' attachPath='"+photoRecord.attachmentLink+"' attachType='PHOTO' attachmentId='"+photoRecord.attachmentId+"' class='btn btn-sm btn-warning'><i class='fa fa-trash'></i></a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
        });
    }else{
        photoAttachLi = '<ul class="list-material">'+
                            '<li class="">'+
                                '<a href="javascript:void(0)" class="visible">'+
                                    '<div class="list-content">'+
                                        '<span class="title"> No Attachment Available</span>'+
                                    '</div>'+
                                '</a>'+
                            '</li>'+
                        '</ul>';
    }

    // STUDENT'S MEDICAL CERTIFICATE ATTACHMENT DISPLAY
    if( attachmentData.certificateArr != "No record found" && attachmentData.certificateArr != ""  ){
        attachmentData.certificateArr.forEach(function( medicalRecord , medicalIndex ){
            
            medicalAttachLi += '<li class="">'+
                                    '<div class="list-content">'+
                                        '<div class="col-lg-8 col-md-4">'+
                                            '<a href="javascript:void(0)" data-link="'+ medicalRecord.attachmentLink +'" onclick="openLinkInNewTab(this);"><span class="title">'+ medicalRecord.title +'</span></a></div><div class="col-lg-4 col-md-2"><a href="javascript:void(0)" attachPath="'+ medicalRecord.attachmentLink +'" attachType="MEDICAL" attachmentId="'+ medicalRecord.attachmentId +'" onclick="deleteAttachment(this)"><i class="fa fa-trash-o pull-right"></i></a>'+
                                        '</div>'+
                                    '</div>'+
                                '</li>';
        });
    }else{
        medicalAttachLi = '<li class="">'+
                            '<a href="javascript:void(0)" class="visible">'+
                                '<div class="list-content">'+
                                    '<span class="title"> No Attachment Available</span>'+
                                '</div>'+
                            '</a>'+
                        '</li>';
    }

    $('#photoAttachmentListing').html( photoAttachLi );
    $('#medicalAttachmentListing').html( medicalAttachLi );

    if( GBL_ATTACH_TYPE == "medicalCerti" ){
        $('#certiAttachDiv').show(200);
        $('#photoAttachDiv').hide(200);
    }else if( GBL_ATTACH_TYPE == "photo" ){
        $('#photoAttachDiv').show(200);
        $('#certiAttachDiv').hide(200);
    }
}

// DISPLAY ATTACHMENT ACC TO SELECTED UPLOAD TYPE - ADDED ON 11-09-2018 BY VISHAKHA TAK
function renderAttachmentType(context){

    var attachmentType = $(context).val();
    if( attachmentType == "photo" ){
        GBL_ATTACH_TYPE = "photo";
        $('#photoAttachDiv').show(200);
        $('#certiAttachDiv').hide(200);
    }else if( attachmentType == "medicalCerti" ){
        GBL_ATTACH_TYPE = "medicalCerti";
        $('#certiAttachDiv').show(200);
        $('#photoAttachDiv').hide(200);
    }

}

// UPLOAD ATTACHMENT ON SUBMIT CALL
function uploadAttachment(){

    var attachmentTitle = $('#uploadTitle').val();
    var attachmentName = $('#attachmentName').val();
    
    if( attachmentTitle == "" ){
        toastr.warning('Please Enter Attachment Title');
        return false;

    }else if( attachmentName == "" ){
        toastr.warning('Please Attach File To Submit');
        return false;

    }else{

        $("#fileupload").submit();

    }

}

//  ATTACHMENT UPLOAD EVENT
function assignAttachmentEvent(){
    
    $("#fileupload").off().on('submit', (function (e) {
            
        var stdIndex = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , GBL_STD_ID );

        var uploadTitle = $('#uploadTitle').val();
        var uploadType = $('#uploadMode').val();
        var uploadTypeVal = "";

        if( uploadType == "photo" ){
            uploadTypeVal = "PHOTO";

        }else{
            uploadTypeVal = "MEDICAL";
            
        }

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "attachmentStudentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            type : uploadTypeVal,
            studentId : GBL_STD_ID,
            title : uploadTitle
        };

        $("#postDataAttach").val(JSON.stringify(temp_json));
        // console.log(temp_json);return false;

        var bfsendMsg = "Sending data to server..";
        addRemoveLoader(1);
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1);
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                
                if(data.status == "Success"){
                    
                    console.log(data.data);
                    // ADD ARRAY TO ATTACHMENT JSON
                    var attachFile = {
                        attachmentId : data.data.attachmentId,
                        attachmentLink : data.data.fileLink,
                        attachmentName : data.data.fileName,
                        studId : GBL_STD_ID,
                        attachType : uploadTypeVal,
                        title : data.data.title
                    }

                    if( stdIndex != -1 ){
                        // ADD ARRAY TO ATTACHMENT JSON ACCORDING TO UPLOAD TYPE
                        if( uploadType == "photo" ){
                            if( GBL_ENROLL_DATA[stdIndex].attachmentData['photosArr'] == "No record found" ){
                                GBL_ENROLL_DATA[stdIndex].attachmentData['photosArr'] = [];
                            }
                            GBL_ENROLL_DATA[stdIndex].attachmentData['photosArr'].push( attachFile );                            
                        }else{
                            if( GBL_ENROLL_DATA[stdIndex].attachmentData['certificateArr'] == "No record found" ){
                                GBL_ENROLL_DATA[stdIndex].attachmentData['certificateArr'] = [];
                            }
                            GBL_ENROLL_DATA[stdIndex].attachmentData['certificateArr'].push( attachFile );
                        }
                        renderAttachment( GBL_ENROLL_DATA[stdIndex].attachmentData ); // RENDER ATTACHMENT DATA
                    }

                    refreshFile(); // REFRESH ATTACHMENT FILE
                    toastr.success( "Your attachment uploaded successfully" );
                }else{
                    toastr.warning(data.status);
                    refreshFile(); // REFRESH ATTACHMENT FILE
                    
                }
                addRemoveLoader(0); // REMOVE LOADER
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                if( jqXHR.responseText != "" && jqXHR.responseText != undefined ){
                        if( JSON.parse(jqXHR.responseText).status != "" ){
                            toastr.error(JSON.parse(jqXHR.responseText).status);
                        }else{
                            toastr.error(errdata);
                        }
                        
                    }else{
                        toastr.error(errdata);
                    }
                // toastr.error(errdata);
                refreshFile(); // REFRESH ATTACHMENT FILE
            }
        });
    }));
}

// DELETE SELECTED ATTACHMENT
function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attachmentId');
    var attachType = $(context).attr('attachType');
    var attachPath = $(context).attr('attachPath');
    var attachmentName = ( attachPath != "" ? basenameWithExt(attachPath) : "" );

    var confmDelete = confirm("Are you sure you want to delete this attachment ?");
    if( !confmDelete ){
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    var postData = {
        requestCase: 'deleteStudentAttachment',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11, 47),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        attachmentId: attachmentId,
        studentId : GBL_STD_ID,
        type : attachType,
        attachPath : attachPath,
        attachName : attachmentName
    };
    commonAjax(COMMONURL, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var studIndex = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , GBL_STD_ID );

            // REMOVE ATTACHMENT FROM JSON ACCORDING TO UPLOAD TYPE
            if( studIndex != -1 ){
                if( attachType == "PHOTO" ){

                    var index = findIndexByKeyValue( GBL_ENROLL_DATA[studIndex].attachmentData['photosArr'] , "attachmentId" , attachmentId );
            
                    GBL_ENROLL_DATA[studIndex].attachmentData['photosArr'].splice( index , 1 );

                }else{

                    var index = findIndexByKeyValue( GBL_ENROLL_DATA[studIndex].attachmentData['certificateArr'] , "attachmentId" , attachmentId );
            
                    GBL_ENROLL_DATA[studIndex].attachmentData['certificateArr'].splice( index , 1 );
                }

                renderAttachment(GBL_ENROLL_DATA[studIndex].attachmentData); // RENDER ATTACHMENT DATA
            }

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

// REFRESH ATTACHMENT FILE
function refreshFile(){

    $('#uploadTitle').val('');
    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

// OPEN SELECTED ATTACHMENT LINK IN NEW TAB
function openLinkInNewTab(context){
    var link = $(context).attr("data-link");
    if(link==""){
        toastr.warnings("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}

// REDIRECT TO ALLOTMENT PAGE OF CURRENT STUDENT
function navigateToAllotment(context){

    var studId = $(context).attr('studId');
    var index = findIndexByKeyValue( GBL_ENROLL_DATA, 'studId' , studId );
    if( index != -1 ){
        localStorage.indeCampusAllotStudId = studId;
        localStorage.indeCampusAllotItemId = GBL_ENROLL_DATA[index].typeOfRoomRequired;
        localStorage.indeCampusAllotRoomType = GBL_ENROLL_DATA[index].roomType;
        navigateToStudentAllotment();
    }
}

// VIEW IMAGE LINK IN MODAL
function viewImageLinkData(docData){
    var link = $(docData).attr("data-link");
    $("#imageLink").attr("src",link);
    openModal(docData);   
}

// OPEN CHANGE STUDENT STATUS MODAL
function changeStudentStatus(context){

    var studId = $(context).attr('studId');
    var stuIndex = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , studId );

    if( stuIndex != -1 ){
        var currStatus = GBL_ENROLL_DATA[stuIndex].status;
        GBL_STD_ID = studId;
    
        // RENDER STATUS LIST FROM GLOBAL VAR
        var innerhtml = "";
        if( STUDENT_STATUS.length != 0 ){
            STUDENT_STATUS.forEach(function( record , index ){
                if( record.VALUE != "Termination" && record.VALUE != "Suspension" ){
                    if( record.ID != currStatus ){
                        innerhtml += '<option value="'+record.ID+'">'+record.VALUE+'</option>' ;
                    }
                }
            });
        }
        $('#studentStatus').html( innerhtml ).selectpicker('refresh');
        modalShow('studentStatusPopup'); // OPEN CHANGE STATUS MODAL
    }
    
}

// CHANGE STUDENT STATUS
function changeStatus(){

    var newStatus = $('#studentStatus').val();

    var index = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , GBL_STD_ID );

    if( index != -1 ){
    
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        
        var postData = {
            requestCase: 'updateStudentStatus',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(11, 46),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            studentId : GBL_STD_ID,
            status : newStatus,
        };
        commonAjax(COMMONURL, postData, changeStudentStatusCallBack,CREATINGINQDATA);

        function changeStudentStatusCallBack(flag, data){
            if (data.status == "Success" && flag) {
                
                console.log(data);
                GBL_ENROLL_DATA[index].status = newStatus;         // UPDATE DATA
                modalHide('studentStatusPopup');    // CLOSE CHANGE STATUS MODAL
                renderStudentList(GBL_ENROLL_DATA); // RENDER STUDENT LIST

            }else{
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_STU_STATUS );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

// CLEAR SELECTED FILTERS
function resetFilters(){

    $('#filterMembershipId').val('');
    $('#filterStudentName').val('');
    $('#filterEnrollmentDate').val('');
    $('#filterStatus').val('1').selectpicker('refresh');

}
// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderStudentList(GBL_ENROLL_DATA); // RENDER ORIGINAL WITHOUT FILTER DATA

}

// PAGE LOAD EVENTS
function pageInitialEvents(){

    if( currPage == "/studentEnrollment" ){
        localStorage.removeItem('indeCampusEnrollTab1Data');
        localStorage.removeItem('indeCampusEnrollTab2Data');
        localStorage.removeItem('indeCampusEnrollDetailStdId');
        localStorage.removeItem('indeCampusEditFromDetail');
        localStorage.removeItem('indeCampusEditStudId');
        getEnrollmentData(); // GETS ENROLLMENT DATA
        $('#uploadMode').selectpicker('refresh');
        $('#changePswdType').selectpicker('refresh');
        $('#filterStatus').selectpicker('refresh');
        assignAttachmentEvent(); // ATTACHMENT UPLOAD EVENT
        autoCompleteForStudentFilter('filterMembershipId',saveStudentNameData); // SEARCH FUNCTIONALITY FOR FILTERS
        autoCompleteForStudentFilter('filterStudentName',saveStudentNameData); // SEARCH FUNCTIONALITY FOR FILTERS
    }else{
        var paymentMode = roleAuth.data.paymentMode;
        setOption("0",'paymentMode',paymentMode,''); // SET PAYMENT MODE
        $('#paymentMode').selectpicker('refresh');
    }
}

// REDIRECT TO COLLECT PAYMENT PAGE
function navigateToCollectPayment(context){

    var studId = $(context).attr('studId');
    localStorage.indeCampusEditStudId = studId;
    window.location.href = "collectpayment.html";

}

// REDIRECT TO RENEW ENROLLMENT PAGE
function navigateToRenewEnrollment(context){

    var studId = $(context).attr('studId');
    var index = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , studId );
    if( index != -1 ){
        var studentJson = {
            studId : studId,
            memberShipNumber : GBL_ENROLL_DATA[index].memberShipNumber,
            firstName : GBL_ENROLL_DATA[index].firstName,
            lastName : GBL_ENROLL_DATA[index].lastName,
            acdemicStartYear : GBL_ENROLL_DATA[index].acdemicStartYear,
            acdemicEndYear : GBL_ENROLL_DATA[index].acdemicEndYear,
            semester : GBL_ENROLL_DATA[index].semester,
        }
        localStorage.indeCampusRenewEnroll = JSON.stringify(studentJson);
        window.location.href = "renewEnrollment.html";
    }

}

// REDIRECT TO GENERATE INVOICE PAGE
function navigateToGenerateInvoice(context){

    var studId = $(context).attr('studId');

    window.location.href = "generateInvoice.html";

}

// GET STUDENT ENROLLMENT DATA ON SELECTED FILTER
function getFilteredStudents(){

    var filterMembershipId = $('#filterMembershipId').val();
    var filterStudentName = $('#filterStudentName').val();
    var filterEnrollmentDate = $('#filterEnrollmentDate').val();
    var filterStatus = $('#filterStatus').val();

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStudentData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11,45),
        membershipId : filterMembershipId,
        studentName : filterStudentName,
        enrollmentDate : filterEnrollmentDate,
        status : filterStatus,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getFilteredStudentsDataCallback,"Please Wait... Getting Dashboard Detail");

    function getFilteredStudentsDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
                console.log(data);
                renderStudentList(data.data); // RENDER FILTERED STUDENT LIST
                $('.Sidenav').removeClass('open');  
                resetFilters(); // RESET FILTER VALUES

            }else{

                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_STUDENT_FILTER );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
    }
}

// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID / STUDENT NAME IN FILTERS
function autoCompleteForStudentFilter(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            setTimeout(function() {
                $('#'+id).val( item.text );
            },10);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(11,45),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            if( id == "filterMembershipId" ){

                                var tempJson = {
                                    id: data.studId,
                                    name: data.membershipNumber,
                                    data: data.membershipNumber,
                                    studentName : data.studName,
                                    membershipNumber : data.membershipNumber,
                                }
                                ITEMS.push(tempJson);

                            }else{

                                var tempJson = {
                                    id: data.studId,
                                    name: data.studName,
                                    data: data.studName,
                                    studentName : data.studName,
                                    membershipNumber : data.membershipNumber,
                                }
                                ITEMS.push(tempJson);

                            }
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}

// OPENS CHANGE PASSWORD MODAL FOR STUDENT / PARENT
function openChangePswdModal(context){

    var studId = $(context).attr('studId');
    var index = findIndexByKeyValue( GBL_ENROLL_DATA , 'studId' , studId );
    if( index != -1 ){
        GBL_STD_ID = studId;
        $('#newPasswordStud').val('');
        $('#retypeNewpasswordStud').val('');
        modalShow('resetChildParentPswd');
    }
}

// CHANGE PASSWORD FOR STUDENT / PARENT
function changeStudParPswd(){

    var passwordChangeType = $('#changePswdType').val();
    var newPasswordStud = $('#newPasswordStud').val();
    var retypeNewpasswordStud = $('#retypeNewpasswordStud').val();


    if( newPasswordStud == "" ) { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Enter New Password");
        $("#newPasswordStud").focus();
        return false;

    }else if( retypeNewpasswordStud == "" ) { // CHECK BLANK VALUE OF ANY FIELD
        toastr.warning("Please Re Type New Password");
        $("#retypeNewpasswordStud").focus();
        return false;

    }else if( !validatePassword( newPasswordStud ) ){ // CHECK VALID PASSWORD ENTERED 
        toastr.warning('Please Enter Strong Password');
        $('#newPasswordStud').focus();
        return false;
    
    }else if( !validatePassword( retypeNewpasswordStud ) ){ // CHECK VALID CONFIRM PASSWORD ENTERED 
        toastr.warning('Please Enter Strong Confirm Password');
        $('#retypeNewpasswordStud').focus();
        return false;

    }else if (newPasswordStud != retypeNewpasswordStud) { //CHECK IF PASSWORD AND CONFIRM PASSWORD NOT MATCH
        toastr.warning("Password and Confirm Password doesn't Match");
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase : 'studentResetPassword',
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11,44),
        studentId : GBL_STD_ID,
        type : passwordChangeType,
        password : newPasswordStud,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getFilteredStudentsDataCallback,"Please Wait... Getting Dashboard Detail");

    function getFilteredStudentsDataCallback(flag,data){

        if (data.status == "Success" && flag) {
                
                console.log(data);
                toastr.success("Changed Password Successfully");
                $('#newPasswordStud').val('');
                $('#retypeNewpasswordStud').val('');
                modalHide('resetChildParentPswd'); // HIDE MODAL 
                getEnrollmentData(); // GET STUDENT LISTING

        }else{

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_PSWD_CHNG_ENR );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
        
    }

}

function studentDetail(context){

    var studId = $(context).attr('studId');
    localStorage.indeCampusEnrollDetailStdId = studId;
    location.href = "studentEnrollmentDetail.html";

}

function navigateToViewInvoice(context){

    var studId = $(context).attr('studId');
    localStorage.indeCampusEnrollDetailStdId = studId;
    localStorage.indeCampusEnrollInvoiceView = true;
    location.href = "studentEnrollmentDetail.html";

}

function navigateToViewLedger(context){

    var studId = $(context).attr('studId');
    localStorage.indeCampusEnrollDetailStdId = studId;
    localStorage.indeCampusEnrollLedgerView = true;
    location.href = "studentEnrollmentDetail.html";

}