/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : developerTypeController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var developerType = [];
var getListingTypeReqCase = "getListingType";
var createTypeReqCase = "createType";
var deleteTypeReqCase = "deleteType";
var editTypeReqCase = "editType";

function getDeveloperData(){

    addFocusId( 'devType' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    $('#scoreType').selectpicker('refresh');

    if( localStorage.indeCampusLookupType == 'lithe' ){
        getListingTypeReqCase = "getLitheListingType";
        createTypeReqCase = "createLitheType";
        deleteTypeReqCase = "deleteLitheType";
        editTypeReqCase = "editLitheType";
        $('#pageTitle').html( 'IndeCampus Type' );
        $('.scoreWeightageCls').hide();
        $('.panel-heading').hide();
        $('#backBtn').attr('onclick','navigateToMasterDeveloper()');
    }else{
        $('#pageTitle').html( 'Type' );
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: getListingTypeReqCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(6 , 22),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getDeveloperDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getDeveloperDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        console.log(data);

        if(data.data != 'No record found'){

            developerType = data.data ;
            renderDeveloperType();
        
        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TYPE );
        }

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TYPE );
        else
            toastr.error(SERVERERROR);
    }
}

function renderDeveloperType(){


   var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Name</th>'+
                        '<th>Shortcode</th>'+
                        // '<th class="scoreWeightageCls">Score Flag</th>'+
                        // '<th class="scoreWeightageCls">Score Type</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    for( var i=0; i<developerType.length; i++ ){

        count = i + 1;
        var scoreTypeNm = "--";
        if( developerType[i].scoreType == "inq" ){
            scoreTypeNm = "Inquiry";
        }else if( developerType[i].scoreType == "act" ){
            scoreTypeNm = "Activity";
        }

        newRow += '<tr>'+
                        '<td>'+ count +'</td>'+
                        '<td>'+ developerType[i].lookupValue +'</td>'+
                        '<td>'+ developerType[i].lookupShortCode +'</td>'+
                        // '<td class="scoreWeightageCls">'+ ((developerType[i].scoreFlag == "1") ? "On" : "Off" ) +'</td>'+
                        // '<td class="scoreWeightageCls">'+ scoreTypeNm +'</td>'+
                        '<td><a href="javascript:void(0)" scoreFlag="'+ developerType[i].scoreFlag +'" scoreType="'+ developerType[i].scoreType +'" lookupCode="'+ developerType[i].lookupShortCode +'" lookupName="'+ developerType[i].lookupValue +'" lookupId="'+ developerType[i].lookupId +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple" onclick="updatePermission(this)"><i class="fa fa-pencil"></i></a>'+
                        '<a href="javascript:void(0)" lookupId="'+ developerType[i].lookupId +'" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="deleteModulePermission(this)"><i class="fa fa-trash-o"></i></a></td>'+
                    '</tr>';
    }

    // $('#moduleList').html( newRow );
    $('#dtTable').html(thead + newRow + tfoot);

    if( localStorage.indeCampusLookupType == 'lithe' ){
        $('.scoreWeightageCls').hide();
    }
    $("#tableListing").DataTable({ "stateSave": true });
}   

function createDeveloperType(){
    if(checkAuth(7,25,1) == -1){
        toastr.error('You are not authorised to perform this activity');
    }else{
        var devType = $('#devType').val();
        var devCode = $('#devCode').val();
        var devScore = (($('#scoreFlag').prop('checked') ? "1" : "0" ));
        var devScoreType = $('#scoreType').val();

        if( devType == ""){
            toastr.warning('Please Enter Developer Type');
            $('#devType').focus();
            return false;

        }else if( devCode == ""){
            toastr.warning('Please Enter Developer Code');
            $('#devCode').focus();
            return false;

        }else{

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: createTypeReqCase,
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(7,25),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                typeName : devType,
                typeShortCode : devCode,
                scoreFlag : devScore,
                scoreType : devScoreType
            }

            commonAjax(FOLLOWUPURL, postData, createDeveloperTypeCallback,"Please Wait... Getting Module Detail");
        }
    }
}


function createDeveloperTypeCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('New Developer Type has been Created');
        console.log(data); 
        // location.reload(); 
        var devType = $('#devType').val();
        var devCode = $('#devCode').val();

        var tempData = {
            lookupId: data.data.lookupId,
            lookupShortCode: devCode,
            lookupValue: devType,
        }

        developerType.push( tempData ); 
        renderDeveloperType();

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_TYPE );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteModulePermission(context){
    
    if(checkAuth(7,28,1) == -1){
        toastr.error('You are not authorised to perform this activity');
    }else{
        var deleteRow = confirm('Are You sure ? You want to delete this Record');

        if( deleteRow ){

            var lookupid = $(context).attr('lookupid');
            gblContext = context;

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: deleteTypeReqCase,
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(6,24),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID), 
                typeId : lookupid
            }

            commonAjax(FOLLOWUPURL, postData, deleteModulePermissionCallback,"Please Wait... Getting Module Detail");
        }
    }
}


function deleteModulePermissionCallback(flag,data){

    if (data.status == "Success" && flag) {

        $(gblContext).parents('td').parents('tr').remove(); 
        toastr.success('Module Permission has been Deleted');
        console.log(data); 

        var lookupid = data.data.lookupId;

        var index = findIndexByKeyValue( developerType,'lookupId',lookupid );

        developerType.splice(index,1);

       renderDeveloperType();

       resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_TYPE );
        else
            toastr.error(SERVERERROR);
    }
}


function updatePermission(context){

    if(checkAuth(7,27,1) == -1){
        toastr.error('You are not authorised to perform this activity');
    }else{
        localStorage.POTGActivityIdForUpdate = "";

        var lookupid = $(context).attr('lookupid');
        var lookupCode = $(context).attr('lookupCode');
        var lookupName = $(context).attr('lookupName');
        var scoreFlag = $(context).attr('scoreFlag');
        var scoreType = $(context).attr('scoreType');

        localStorage.POTGlookupIdForUpdate = lookupid;

        $('#developerBtn').attr('onclick','updateDeveloperType()');

        $('#devType').val(lookupName);
        $('#devCode').val(lookupCode);
        // $('#scoreFlag').prop('checked', ( (scoreFlag == "1") ? true : false ) );
        // $('#scoreTypeDiv').hide();
        // if( scoreFlag == "1" ){
        //     $('#scoreTypeDiv').show();
        //     $('#scoreType').val(scoreType).selectpicker('refresh');
        // }
        addFocusId( 'devType' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}

function updateDeveloperType(context){

    var devType = $('#devType').val();
    var devCode = $('#devCode').val();
    // var devScore = (($('#scoreFlag').prop('checked') ? "1" : "0" ));
    var devScoreType = $('#scoreType').val();
    var lookupid = localStorage.POTGlookupIdForUpdate;

    if( devType == ""){
        toastr.warning('Please Enter Developer Type');
        $('#devType').focus();
        return false;

    }else if( devCode == ""){
        toastr.warning('Please Enter Developer Code');
        $('#devCode').focus();
        return false;

    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: editTypeReqCase,
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(7,27),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            typeId : lookupid,
            typeName : devType,
            typeShortCode : devCode,
            // scoreFlag : devScore,
            scoreType : devScoreType
        }
        // console.log(postData); return false;
        commonAjax(FOLLOWUPURL, postData, updateDeveloperTypeCallback,"Please Wait... Getting Module Detail");
    }
}


function updateDeveloperTypeCallback(flag,data){

    if (data.status == "Success" && flag) {

        toastr.success('Developer Type has been Updated');
        console.log(data); 
        
        var index = findIndexByKeyValue( developerType,'lookupId',localStorage.POTGlookupIdForUpdate );

        var devType = $('#devType').val();
        var devCode = $('#devCode').val();
        // var devScore = (($('#scoreFlag').prop('checked') ? "1" : "0" ));
        var devScoreType = $('#scoreType').val();
        var lookupid = localStorage.POTGlookupIdForUpdate;

        var tempArray = {
            lookupId: lookupid,
            lookupShortCode: devCode,
            lookupValue: devType,
            // scoreFlag : devScore,
            scoreType : devScoreType,
        }

        developerType[index] = tempArray;

        renderDeveloperType();

        $('#developerBtn').attr('onclick','createDeveloperType()');

        resetData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_TYPE );
        else
            toastr.error(SERVERERROR);
    }
}

function scoreTypeHideShow(){

    var scoreFlag = $('#scoreFlag').prop('checked');
    if( scoreFlag ){
        $('#scoreTypeDiv').show(300);
    }else{
        $('#scoreTypeDiv').hide(300);
    }
}

function openScoreDistModal(){

    $('#scoreTypeDist').val('-1').selectpicker('refresh');
    scoreDistTypeChange();
    
    modalShow('scoreDistModal');
}

function scoreDistTypeChange(){

    var scoreTypeDist = $('#scoreTypeDist').val();
    var scoreTypeDistNm = $('#scoreTypeDist option:selected').text();
    var scoreDistHtml = "";
    if( scoreTypeDist != "-1" ){
        developerType.forEach( function( record , index ){
            if( record.scoreFlag == "1" && record.scoreType == scoreTypeDist ){
                scoreDistHtml += '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">'+ record.lookupValue +'</label>'+
                                        '<input type="text" id="scoreDistInp_'+ record.lookupId +'" class="form-control onlyNumber" value="'+ record.lookDistAmt +'" placeholder="Enter '+ scoreTypeDistNm +' Distribution">'+
                                    '</div>'+
                                '</div>';
            }
        });
    }
    $('#scoreDistList').html( scoreDistHtml );
}

function addDistribution(){

    var scoreTypeDist = $('#scoreTypeDist').val();
    var scoreDistArr = [];
    if( scoreTypeDist == "-1" ){
        toastr.warning('Please Select Score Type');
        return false;
    }

    var distScoreTotal = 0;

    developerType.forEach( function( record , index ){
        if( record.scoreFlag == "1" && record.scoreType == scoreTypeDist ){

            var distScore = (($('#scoreDistInp_'+record.lookupId).val() == "") ? 0 :$('#scoreDistInp_'+record.lookupId).val());
            var tempArray = {
                lookupid : record.lookupId,
                lookupShortCode : record.lookupShortCode,
                lookupValue : record.lookupValue,
                distScore : distScore,
            }
            scoreDistArr.push( tempArray );
            distScoreTotal = parseInt( distScoreTotal ) + parseInt( distScore );
        }
    });

    if( distScoreTotal > 100 ){
        toastr.warning('You cannot Distribute More than 100');
        return false;
    }else if( distScoreTotal == 0 ){
        toastr.warning('Please Distribute Before Saving');
        return false;
    }else if( distScoreTotal < 100 ){
        toastr.info('You have Distributed only '+ distScoreTotal);
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'addTypeDistribution',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(7,25),
        scoreDistArr: scoreDistArr,
        distScoreType: scoreTypeDist,
        distScoreTotal: distScoreTotal,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, addDistributionCallback,"");
    function addDistributionCallback(flag,data){

        if (data.status == "Success" && flag) {
            developerType = data.data;
            modalHide('scoreDistModal');
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_TYPE_WEIGHTAGE );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function resetData(){

    $('#devType').val('');
    $('#devCode').val('');
    $('#scoreFlag').prop('checked',false);
    $('#scoreTypeDiv').hide();
    $('#developerBtn').attr('onclick','createDeveloperType()');
}