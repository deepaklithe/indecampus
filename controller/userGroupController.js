/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : userGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var DataTableInit = 0;
var groupData = [];

function getUserData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userCustomerGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        user_Id: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(23,90),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        searchType : "USER"
    }

    commonAjax(FOLLOWUPURL, postData, getUserDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        $('#updategroupList').modal('hide');
        
        groupData = data.data;

        renderUserData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function renderUserData(){

    var newRow = '';
    var count = 1 ;

    addFocusId( 'grpName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    if( groupData != 'No record found' ){

        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>No</th>'+
                            '<th>User Group Name</th>'+
                            '<th>No.Of.User</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';
    
        for( var i=0; i<groupData.length; i++ ){

            var name = groupData[i].groupName;
            var groupId = groupData[i].groupId;

            var length = '0';
            if( groupData[i].linkedUserDetail != 'No record found' ){
                length = groupData[i].linkedUserDetail.length;
            }

            var totalMembers = '';
            
            count = i + 1;
            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><a href="javascript:void(0)" groupId="'+ groupId +'" class="glist" onclick="groupList(this);">'+ name +'</a></td>'+
                            '<td>'+ length +'</td>'+
                            '<td>'+
                                '<a href="javascript:void(0)" groupName="'+ name +'" groupId="'+ groupId +'" onclick="editGroup(this)" class="btn-right-mrg btn btn-xs btn-primary btn-ripple"><i class="fa fa-pencil"></i></a>'+ 
                                '<a href="javascript:void(0)" groupId="'+ groupId +'" onclick="deleteUserGroup(this)" class="btn-right-mrg btn btn-xs btn-danger btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                                '<a href="javascript:void(0)" groupId="'+ groupId +'" onclick="assignUserGroup(this)" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-retweet"></i></a>'+
                            '</td>'+
                        '</tr>';
        }

        
        $('#dtTable').html(thead + newRow + tfoot);

    }

    resetActiviytData();

    TablesDataTables.init();
    TablesDataTablesEditor.init();
}


function groupList(context){

    var groupId = $(context).attr('groupId');

    var index = findIndexByKeyValue(groupData,'groupId',groupId);

    if( groupData[index].linkedUserDetail != 'No record found' ){
		 var groupList = '';
			groupList += '<table class="table table-hover" id="userGrpHeadTbl">'+
							 '<thead>'+
								 '<tr>'+
								 	  '<th>#</th>'+
								   	  '<th>User Name</th>'+
								 '</tr>'+
							 '</thead>'+
							 '<tbody id="userGroupListTable">';
						
        var count = 1;
        for( var i=0; i<groupData[index].linkedUserDetail.length; i++ ){

            count = i + 1;
            groupList += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td>'+ groupData[index].linkedUserDetail[i].userFullName +'</td>'+
                        '</tr>';
        }
		groupList += '</tbody></table>';
        $('#userGrpHeadDiv').html(groupList);
        $('#userGrpHeadTbl').dataTable({ "stateSave": true });
		$('#grpHead').html(groupData[index].groupName);
        $('#groupList').modal('show');
    }else{
        toastr.warning(groupData[index].linkedUserDetail);
    }

}   

function addGroup(){
    
    if(checkAuth(23,89,1) == -1){
         toastr.error( NOAUTH );
    }else{
        $('#saveUserGroup').attr('onclick','createUserGroup()')
        $('#saveUserGroup').attr('onclick','createUserGroup()')

        $('#grpName').val('');

        $('#addGroup .modal-title').html('Add Group');
        $('#addGroup').modal('show');

        $('#grpName').focus();
        addFocusId( 'grpName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
    
}

function createUserGroup(context){

    var grpName = $('#grpName').val().trim();

    if( grpName.length == 0){
        toastr.warning('Please Enter Group Name');
        $('#grpName').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addEditGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(23,89),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupName : grpName,
            groupType : "USER",
        }  
        $('#addGroup').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, createUserGroupCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function createUserGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);// location.reload();

        var grpName = $('#grpName').val().trim();

        var tempData = {
            groupId: data.data.grpId,
            groupName: grpName,
            linkedUserDetail: "No record found",
        }

        groupData.push( tempData ); 

        renderUserData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function editGroup(context){
    
    if(checkAuth(23,91,1) == -1){
         toastr.error('You are not authorised for this Activitiy');
    }else{
        var groupId = $(context).attr('groupid');
        var groupName = $(context).attr('groupName');

        $('#saveUserGroup').attr('onclick','editUserGroup(this)');
        $('#saveUserGroup').attr('groupId',groupId);

        $('#grpName').val(groupName);

        $('#addGroup .modal-title').html('Update Group');
        $('#addGroup').modal('show');

        $('#grpName').focus();
        addFocusId( 'grpName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
    
}

var grpIdForEdit = '';
function editUserGroup(context){

    grpIdForEdit = $(context).attr('groupid');
    var grpName = $('#grpName').val().trim();

    if( grpName.length == 0){
        toastr.warning('Please Enter Group Name');
        $('#grpName').focus();
        return false;

    }else{
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'addEditGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(23,89),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            groupName : grpName,
            groupId : grpIdForEdit,
            groupType : "USER"
        }  

        $('#addGroup').modal('hide');
        // console.log(postData);return false;
        commonAjax(FOLLOWUPURL, postData, editUserGroupCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function editUserGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var index = findIndexByKeyValue( groupData,'groupId',grpIdForEdit );

        var grpName = $('#grpName').val();

        var tempArray = {
            groupId: grpIdForEdit,
            groupName: grpName,
            linkedUserDetail: groupData[index].linkedUserDetail
        }

        groupData[index] = tempArray;

        renderUserData();

        $('#saveUserGroup').attr('onclick','createUserGroup()');

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

var gblContext = "";
function deleteUserGroup(context){
    
    
    if(checkAuth(23,92,1) == -1){
         toastr.error('You are not authorised for this Activitiy');
    }else{
        var deleteRow = confirm('Are You sure ? You want to delete this Record');

        if( deleteRow ){

            var groupId = $(context).attr('groupid');
            gblContext = context;

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'deleteUserCustomerGroup',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                user_Id: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(17,68),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                groupId : groupId,
            }  

            $('#addGroup').modal('hide');
            // console.log(postData);return false;
            commonAjax(FOLLOWUPURL, postData, deleteUserGroupCallback,"Please Wait... Getting Dashboard Detail");
        }
    }
}


function deleteUserGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        $(gblContext).parents('td').parents('tr').remove(); 
        // location.reload();
        toastr.success('User Group Deleted Successfully');

        var groupId = $(gblContext).attr('groupid');
        
        var index = findIndexByKeyValue( groupData,'groupId',groupId );

        groupData.splice(index,1);

        renderUserData();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

function assignUserGroup(context){

    if(checkAuth(23,106,1) == -1){
         toastr.error('You are not authorised for this Activitiy');
    }else{
        var groupId = $(context).attr('groupId');

        var index = findIndexByKeyValue(groupData,'groupId',groupId);

        $('#userGroupList').html('');
        $('#assignNewUser').val('');
        
        if( groupData[index].linkedUserDetail != 'No record found' ){

            var groupList = '';

            var count = 1;
            for( var i=0; i<groupData[index].linkedUserDetail.length; i++ ){

                count = i + 1;
                var userId = groupData[index].linkedUserDetail[i].userId;
                var userName = groupData[index].linkedUserDetail[i].userFullName;
                var transId = groupData[index].linkedUserDetail[i].transId;

                groupList += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td>'+ userName +'</td>'+
                                '<td>'+
                                    '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" transId="'+ transId +'" userId="'+ userId +'" onclick="deleteAssignedUser(this)"><i class="fa fa-trash-o"></i></a>'+
                                '</td>'
                            '</tr>';
            }

            $('#userGroupList').html(groupList);
        }

        $('#assignUserBtn').attr('groupId',groupId);

        $('#updategroupList').modal('show'); 
        addFocusId( 'assignNewUser' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    }
}


function assignUserToGroup(context){

    var groupId = $(context).attr('groupId');
    var requestId = $(context).attr('requestid');
    var assignNewUser = $('#assignNewUser').val();

    if( requestId == "" || requestId == undefined){
        toastr.warning('please Search User');
        $('#assignNewUser').focus();
        return false;

    }else if( assignNewUser == ""){
        toastr.warning('please Search User');
        $('#assignNewUser').focus();
        return false;

    }else{
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'assignUserCustomerGroup',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            user_Id: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(17,105),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            requestId : requestId,
            groupId : groupId,
        }

        commonAjax(FOLLOWUPURL, postData, assignUserToGroupCallback,"Please Wait... Getting Dashboard Detail");
    }

}


function assignUserToGroupCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        var userName = $('#assignNewUser').val();
        var userId = $('#assignUserBtn').attr('requestId');
        var transId = data.data.transId;
        var count = '';

        var groupList = '';
        groupList += '<tr>'+
                        '<td>'+ count +'</td>'+
                        '<td>'+ userName +'</td>'+
                        '<td>'+
                            '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" userId="'+ userId +'"  transId="'+ transId +'" onclick="deleteAssignedUser(this)"><i class="fa fa-trash-o"></i></a>'+
                        '</td>'
                    '</tr>';

        $('#userGroupList').append(groupList);

        $('#assignNewUser').val('');
        $('#assignUserBtn').removeAttr('requestId');

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ASSIGN_FAIL );
        else
            toastr.error(SERVERERROR);
    }
}


function AutoCompleteForUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#assignUserBtn').attr('requestId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveUserDetail(){

}


var gblContext = '';
function deleteAssignedUser(context){

    gblContext = context;
    
    var transId = $(context).attr('transId');

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'deleteAssignCustomerUserGroup',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(17,68),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        transId : transId,
    }

    commonAjax(FOLLOWUPURL, postData, deleteAssignedUserCallback,"Please Wait... Getting Dashboard Detail");
}



function deleteAssignedUserCallback(flag,data){

    if (data.status == "Success" && flag) {
            console.log(data);
            $(gblContext).parent('td').parent('tr').remove();
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_DELETE_ASS_GROUP );
        else
            toastr.error(SERVERERROR);
    }
}

