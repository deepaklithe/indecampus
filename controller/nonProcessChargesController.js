    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 01-08-2018.
 * File : nonProcessChargesController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_NON_PR_CHRG_DATA = []; // CHARGE POST JSON 
var todayDate = mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' );

// PAGE LOAD EVENTS
function pageInitialEvents(){

	$('#invoiceTillDate').val( mysqltoDesiredFormat( getTodaysDate() , 'dd-MM-yyyy' ) );
	$('#invoiceGenerateBtn').click(function(){
        $('#invoiceGenerateBlock').toggleClass('active');
        if( $('#invoiceGenerateBlock').hasClass('active') ){
	    	$('#invoiceName').val('');
	    }
    });   

	getNonProcessCharge(); // GET NON PROCESS CHARGES

}

// GET NON PROCESS CHARGES
function getNonProcessCharge() {

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'listChargePosting', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(24 , 98),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addChargePostCallback,"Please wait...Adding your item.");

    function addChargePostCallback( flag , data ){ 
        if (data.status == "Success" && flag) {

        	console.log(data);
        	GBL_NON_PR_CHRG_DATA = data.data;
			renderNonProcessCharges();

		}else{
			GBL_NON_PR_CHRG_DATA = [];
			renderNonProcessCharges();
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_NON_PR_CHG );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// RENDER ADDED CHARGE POST ENTRIES
function renderNonProcessCharges() {	

	
	var tHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="viewChargePostTable">'+
			        '<thead>'+
			            '<tr>'+
			                '<th>#</th>'+
                            '<th>Charge Date</th>'+  
                            '<th>Student ID</th>'+  
                            '<th>Student Name</th>'+                                             
                            '<th>Item</th>'+
                            '<th>Charge Type </th>'+
                            '<th>HSN Code</th>'+
                            '<th>Rate</th>'+
                            '<th>CGST Amount</th>'+
                            '<th>SGST Amount</th>'+
                            '<th>IGST Amount</th>'+
                            '<th>Net Amt</th>'+
                            '<th style="width: 50px;">Qty</th>'+
                            '<th>Action</th>'+
			            '</tr>'+
			        '</thead>'+
			        '<tbody>';
    var tEnd = '</tbody></table>';
    var newRow = '';

	if( GBL_NON_PR_CHRG_DATA != "No record found" && GBL_NON_PR_CHRG_DATA.length != 0 ){

		GBL_NON_PR_CHRG_DATA.forEach(function( record , index ) {
			var cgstAmt = 0 , sgstAmt = 0 , igstAmt = 0;
			var finalAmt = parseFloat(record.itemBasicPrice) * parseFloat(record.qtyOfSession);
			if( record.taxCgst != "" ){
				cgstAmt = fixedTo2((parseFloat(finalAmt) * parseFloat(record.taxCgst)) / 100);
			}
			if( record.taxSgst != "" ){
				sgstAmt = fixedTo2((parseFloat(finalAmt) * parseFloat(record.taxSgst)) / 100);
			}
			if( record.taxIgst != "" & record.taxIgst != undefined ){
				igstAmt = fixedTo2((parseFloat(finalAmt) * parseFloat(record.taxIgst)) / 100);
			}
			var grossAmount = fixedTo2( parseFloat(finalAmt) + parseFloat(cgstAmt) + parseFloat(sgstAmt) + parseFloat(igstAmt) );
						
			newRow += '<tr>'+
		                '<td>'+ (index+1) +'</td>'+
		                '<td>'+ mysqltoDesiredFormat(record.chargeDate,'dd-MM-yyyy') +'</td>'+
		                '<td>'+ record.studMembershipNo +'</td>'+
		                '<td>'+ record.studName +'</td>'+
		                '<td>'+ record.itemName +'</td>'+
		                '<td>'+ record.itemType +'</td>'+
		                '<td>'+ record.hsnCode +'</td>'+
		                '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(record.itemBasicPrice)) +'</td>'+
		                '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(cgstAmt)) +'</td>'+
		                '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(sgstAmt)) +'</td>'+
		                '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(igstAmt)) +'</td>'+
		                '<td class="amtDisp"><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(fixedTo2(grossAmount)) +'</td>'+
		                '<td style="width: 50px;">'+ 
		                	'<span class="viewField_'+ index +'">' + record.qtyOfSession + '</span>' +
		                	'<input type="text" class="onlyNumber editField_'+ index +'" style="display:none;" id="qtyOfSession_'+ index +'" value="'+ record.qtyOfSession +'" />' +
		                '</td>'+
		                '<td>'+
		                	'<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple editBtn_'+index+'" data-toggle="tooltip" data-original-title="Edit" arrIndex="'+ index +'" onclick="editChargeQty(this);"> <i class="fa fa-pencil"></i> </a>&nbsp;'+
		                	'<a href="javascript:void(0)" class="btn btn-xs btn-amber btn-ripple updateBtn_'+index+'" data-toggle="tooltip" data-original-title="Update" arrIndex="'+ index +'" onclick="updateChargeQty(this);" style="display:none;"> <i class="fa fa-floppy-o"></i> </a>&nbsp;'+
		                	'<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-ripple cancelBtn_'+index+'" data-toggle="tooltip" data-original-title="Cancel" arrIndex="'+ index +'" onclick="cancelChargeQty(this);" style="display:none;"> <i class="fa fa-times"></i> </a>'+
                        '</td>'+
		            '</tr>';
		});

	}
	$('#nonProcessTable').html( tHead + newRow + tEnd );
	$('#viewChargePostTable').DataTable();

	 // it allow only number and plus symbol just place in input fields
    $('.onlyNumber').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });

}

// SHOW EDITABLE QTY FIELD FOR UPDATE
function editChargeQty(context){

	var arrIndex = $(context).attr('arrIndex');

	if( arrIndex != "" && arrIndex != undefined ){

		$('.viewField_'+arrIndex).hide();
		$('.editField_'+arrIndex).show();
		$('.editBtn_'+arrIndex).hide();
		$('.updateBtn_'+arrIndex).show();
		$('.cancelBtn_'+arrIndex).show();

	}

}

// SHOW NON EDITABLE QTY FIELD 
function cancelChargeQty(context){

	var arrIndex = $(context).attr('arrIndex');

	if( arrIndex != "" && arrIndex != undefined ){

		$('.viewField_'+arrIndex).show();
		$('.editField_'+arrIndex).hide();
		$('.editBtn_'+arrIndex).show();
		$('.updateBtn_'+arrIndex).hide();
		$('.cancelBtn_'+arrIndex).hide();

	}

}

// UPDATE CHARGE POST QTY
function updateChargeQty(context){

	var arrIndex = $(context).attr('arrIndex');

	if( arrIndex != "" && arrIndex != undefined ){

		var qtyOfSession = $('#qtyOfSession_'+arrIndex).val();

		if( qtyOfSession == "" || qtyOfSession < 1 ){
			toastr.warning("Please enter valid quantity");
			$('#qtyOfSession_'+arrIndex).focus();
			return false;

		}

		var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

	    var postData = {
	        requestCase : 'updateChargePosting', 
	        clientId : tmp_json.data[0].FK_CLIENT_ID,
	        userId : tmp_json.data[0].PK_USER_ID,
	        orgId : checkAuth(24, 99),
	        chargePostingId : GBL_NON_PR_CHRG_DATA[arrIndex].chargeId,
	        itemId : GBL_NON_PR_CHRG_DATA[arrIndex].itemId,
	        qty : qtyOfSession,
	        price : GBL_NON_PR_CHRG_DATA[arrIndex].itemBasicPrice,
	        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
	    };
	    commonAjax(FOLLOWUPURL, postData, updateChargePostingCallback,"Please wait...Adding your item.");

	    function updateChargePostingCallback( flag , data ){
	        if (data.status == "Success" && flag) {

	        	console.log(data);
	        	getNonProcessCharge(); // GET NON PROCESS CHARGES

			}else{

	            if (flag)
	              displayAPIErrorMsg( data.status , GBL_ERR_UPD_CHARGE_QTY );
	            else
	                toastr.error(SERVERERROR);
	        }
	    }

	}

}

// GENERATE INVOICE
function generateInvoice(){

	var invoiceName = $('#invoiceName').val();
	var invoiceTillDate = $('#invoiceTillDate').val();

	if( invoiceName == "" ){
		toastr.warning("Please enter invoice name");
		addFocusId('invoiceName');
		return false;

	}else if( invoiceTillDate == "" ){
		toastr.warning("Please select invoice till date");
		addFocusId('invoiceTillDate');
		return false;

	}

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'createInvoice', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(23, 93),
        chargeEndDate : invoiceTillDate,
        invoiceStartDate : invoiceTillDate,
        invoiceEndDate : invoiceTillDate,
        invoiceName : invoiceName,
        invoiceType : "MISC",
        mailSend : "1",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, generateInvoiceCallback,"Please wait...Adding your item.");

    function generateInvoiceCallback( flag , data ){ 
        if (data.status == "Success" && flag) {

        	console.log(data);
        	$('#invoiceGenerateBlock').removeClass('active');
        	getNonProcessCharge(); // GET NON PROCESS CHARGES

		}else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_GEN_INVOICE );
            else
                toastr.error(SERVERERROR);
        }
    }

}