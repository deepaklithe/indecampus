    /*
 *
 * Created By : Kausha Shah
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 16-08-2018.
 * File : createInvoiceController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_CENTRALIZED_DATA = [];
var GBL_ITEMS = [];
var itemCount = '' ;
var GBL_TEMP_ITEM = [];
var GBL_ITEM_INV_DATA = [];

function pageInitialEvents() {
    
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    autoCompleteForStudentId('studentName',saveStudentNameData);
        
    $('#receiptDate').datepicker('update',mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy'));
    getItemData() ;
    GBL_TEMP_ITEM = [];
    renderItemList();
}

// PROVIDE SEARCH FOR STUDENT MEMBERSHIP ID 
function autoCompleteForStudentId(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( index != -1 ){
                setTimeout(function() {
                    $('#'+id).val( ITEMS[index].studentName + ' - ' + ITEMS[index].membershipNumber );
                    $('#'+id).attr( 'studId' , ITEMS[index].id ); 
                },1);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "studentSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId:  checkAuth(11,45),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            var tempJson = {
                                id: data.studId,
                                name: data.studName + ' - ' + data.membershipNumber,
                                data: data.studName,
                                studentName : data.studName,
                                membershipNumber : data.membershipNumber,
                            }
                            ITEMS.push(tempJson);

                           
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveStudentNameData(){
}

function getItemData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "MISC",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ITEMS = data.data;
            renderItem('itemName_1');
                
           
            
        }else{

            GBL_ITEMS = [];
            renderItem('itemName_1');
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOM_TYPE );
            else
                toastr.error(SERVERERROR);
        }
        // if( localStorage.indeCampusEnrollTab2Data != undefined ){
        //     renderTab2(); // RENDER TAB2 DATA   
        // }
    }

}

function renderItem(eleId) {
    
    var innerHtml = '<option value="-1" data-itemPrice="0">Select Item</option>';
    if( GBL_ITEMS != "No record found" ){
        GBL_ITEMS.forEach(function( record , index ){

            innerHtml += '<option data-itemPrice="'+ record.itemPrice +'" value="'+ record.itemId +'" >'+ record.itemName +'</option>';

        });
    }
    innerHtml += '<option data-itemPrice="" value="Other" >Other</option>';
    $('#'+eleId).html( innerHtml ).selectpicker('refresh'); // SET ROOM TYPE DATA IN DROPDOWN
    $('#'+eleId).val( 'Other' ).selectpicker('refresh'); // SET OTHER TYPE ITEM OPTION ONLY - REMOVED SELECTING ITEM OPTIONS - ON 06-09-2018
    $('#'+eleId).trigger('change');
    $('#'+eleId).attr('disabled',true); // SET ROOM TYPE DATA IN DROPDOWN

}

function addItemHtml(){
    
    var itemHtmlLen = $('.itemWrapper').length ;
    var lastId = $( ".itemWrapper" ).last().attr('id') ;
    var lastIdVal = lastId.replace('itemWrapper','') ;
    itemHtmlLen = parseInt(lastIdVal) + 1 ;
    
    
    var html = '' ;
    html += '<div class="itemWrapper" id="itemWrapper'+itemHtmlLen+'">' ;
    html += '<div class="col-md-4" style="margin-bottom:10px;">'+
                                   '<div class="form-group">'+
                                      '<label class="control-label flot-lable-left" style="width: 100%;"><i class="fa fa-list-ol"></i> Item Name<span class="mand">*</span></label>'+
                                      '<select onchange="getItemPrice(this);" class="selectpicker itemName" name="itemName[]" id="itemName_'+itemHtmlLen+'">'+
                                            
                                  '</select>'+
                                 '</div>'+
                                '</div>'+
                                '<div class="col-md-3" style="margin-bottom:10px;">'+
                                    '<div class="form-group">'+
                                         '<label class="control-label flot-lable-left" style="width: 100%;"><i class="fa fa-money"></i>  Amount.<span class="mand">*</span></label>'+
                                         '<input type="text" class="form-control itemAmount onlyNumberWithOneDec" name="itemAmount[]" id="itemAmount'+itemHtmlLen+'">'+
                                                    
                                     '</div>'+
                                '</div>'+
                                 '<div class="col-lg-4 " style="padding : 12px;">'+
                                     '<label class="control-label flot-lable-left" style="width: 100%;"> </label>'+
                                     '<a title="Add New Price" onclick="removeItemHtml(this)" data-id="itemWrapper'+itemHtmlLen+'"><i class="fa fa-minus btn btn-danger buttontable"></i>'+
                                            '</a> '+
                                  '</div> </div>' ;
    // getItemData() ;
    $('#itemHtmlWrapper').append(html);
    renderItem('itemName_'+itemHtmlLen);
    
    
}

function removeItemHtml(context) {
    
    var dataId = $(context).attr('data-id') ;
    $('#'+dataId).remove() ;
    
}

function resetFields(){
    
    $('#studentName').val('');
    $('#studentName').attr('studId','');
    // $('#receiptDate').val('') ;
    $('#receiptDate').datepicker('update',mysqltoDesiredFormat(getTodaysDate(),'dd-MM-yyyy'));
    $('#narration').val('') ;
    $('.itemName').val('').selectpicker('refresh') ;
    $('.itemAmount').val('') ;
    resetItemFields();
    GBL_TEMP_ITEM = [];
    renderItemList();
    
}

function createInvoice() {
    
    // var Xlength = $('.itemWrapper').length ;
    var studentName = $('#studentName').attr('studId');
    var studName = $('#studentName').val();
    
    var receiptDate = $('#receiptDate').val() ;
    var narration = $('#narration').val() ;
    var itemCount = $('.itemName').length ;
    console.log('item count : '+itemCount) ;

    if( studentName == "" || studentName == undefined || studName == ""){
        toastr.warning("Please Search student name");
        addFocusId('studentName');
        return false;

    } else if( receiptDate == "" ){
        toastr.warning("Please select receipt date");
        addFocusId('receiptDate');
        return false;

    } else if( GBL_TEMP_ITEM.length == 0 ){
        toastr.warning("Please add atleast one item to create invoice");
        addFocusId('itemName_1');
        return false;

    }
    
    // for(var j=1; j<=Xlength; j++){
        
    //     var attrId = $(".itemWrapper:nth-child("+j+")").attr('id') ;
    //     var attrIdVal = attrId.replace('itemWrapper','') ;
        
    //     var itemName = $('#itemName_'+attrIdVal).val() ;

    //     if(itemName == -1) {
    //        alert('Please select item name.') ; 
    //        $('#itemName_'+attrIdVal).focus();
    //        return false;
    //     }

    //     var itemAmount = $('#itemAmount'+attrIdVal).val() ;
    //     if(itemAmount == '') {
    //        alert('Please enter item amount.') ; 
    //        $('#itemAmount'+attrIdVal).focus();
    //        return false;
    //     }
        
    // }
    
    var itemData = [];
    GBL_TEMP_ITEM.forEach(function( record , index ){
        if( record.itemId != "Other" ){
            var temp = {
                itemId : record.itemId,
                itemName : record.itemName,
                itemAmount : record.itemAmount,
                cgst : fixedTo2(record.cgst),
                sgst : fixedTo2(record.sgst),
                igst : fixedTo2(record.igst),
                hsnCode : record.hsnCode
            }
            itemData.push(temp);
        }else{
            var temp = {
                itemId : "",
                itemName : record.itemName,
                itemAmount : fixedTo2(record.itemAmount),
                cgst : fixedTo2(record.cgst),
                sgst : fixedTo2(record.sgst),
                igst : fixedTo2(record.igst),
                hsnCode : record.hsnCode
            }
            itemData.push(temp);
        }
    });
       
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    
    // $('select[name^="itemName"]').each(function(index) {
    //     console.log(index) ;
    //     var custCount = index + 1 ;
    //     var thisId = $(this).attr('id');
    //     var thisIdVal = thisId.replace('itemName_','') ;
    //     var itemAmount = $('#itemAmount'+thisIdVal).val() ;
    //     var itemName = $(this).data('selectpicker').$button.text();
    //     itemData.push({itemName:itemName,itemAmount:itemAmount});
    // });
    
    console.log(itemData) ;
    
    var postData = {
        requestCase : 'createGeneralInvoice' ,
        userId : tmp_json.data[0].PK_USER_ID,
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        orgId : checkAuth(23, 94),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        studentId : studentName,
        invDate : receiptDate,
        narration : narration,
        itemData : itemData
    }
    
    console.log(postData) ; 
    // return false;
    commonAjax(FOLLOWUPURL, postData, createInvoiceCallback,"Please wait...Adding your item.");

    function createInvoiceCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            // navigateToChargePostMaster();
            toastr.success("Your invoice created successfully");
            resetFields();
                
        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOM_TYPE );
            else
                toastr.error(SERVERERROR);
        }
        
    }
}

function getItemPrice(context) {
    
    var thisId = $(context).attr('id') ;
    //alert(thisId) ;
    //var text = $('#'+thisId).data('selectpicker').$button.text();
    //alert(text) ;

    // CHANGED BY VISHAKHA TAK FOR SET SELECTED ITEM TAXATION ON 24-08-2018
    if( $(context).val() != "" && $(context).val() != "-1" ){
        if( $(context).val() == "Other" ){
            $('.otherTypeFields').show(); // SHOW DROPDOWN OF OTHER TYPE ITEM
            $('.miscItemAmount').hide(); // HIDE AMOUNT FIELD
            $('#cgstPer').val( '' );
            $('#igstPer').val( '' );
            $('#sgstPer').val( '' );
            $('#otherItemName').val( '' );
            $('#otherItemPrice').val( '' );
        }else{
            var itemId = $(context).val();
            var index = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );
            
            $('.otherTypeFields').hide(); // HIDE DROPDOWN OF OTHER TYPE ITEM
            $('.miscItemAmount').show(); // SHOW AMOUNT FIELD
            $('#otherItemPrice').val('');
        }
    }else{

        $('.otherTypeFields').hide(); // HIDE DROPDOWN OF OTHER TYPE ITEM
        $('.miscItemAmount').show(); // SHOW AMOUNT FIELD
        $('#otherItemPrice').val('');
        $('#cgstPer').val( '' );
        $('#igstPer').val( '' );
        $('#sgstPer').val( '' );

    }
    
    var attrVal = $('#'+thisId+' :selected').attr('data-itemprice');
    //alert(attrVal) ;
    var thisIdVal = thisId.replace('itemName_','') ;
    $('#itemAmount'+thisIdVal).val(attrVal) ;
    //alert(test) ;
}

// ADD ITEM INTO INVOICE ITEM JSON - ADDED BY VISHAKHA TAK ON 24-08-2018
function addItemList(){

    var itemId = $('#itemName_1').val();
    var itemName = $('#itemName_1 option:selected').text();
    var itemAmount = $('#itemAmount1').val();
    var cgst = $('#cgstPer').val();
    var sgst = $('#sgstPer').val();
    var igst = $('#igstPer').val();
    var otherItemPrice = $('#otherItemPrice').val();
    var otherItemName = $('#otherItemName').val();

    if( itemId == "" || itemId == "-1" ){
        toastr.warning("Please select item");
        addFocusId('itemName_1');
        return false;

    }else if( itemId != "Other" && itemAmount == "" ){
        toastr.warning("Please enter item amount");
        addFocusId('itemAmount1');
        return false;
        
    }else if( itemId == "Other" && otherItemName == "" ){
        toastr.warning("Please enter item name");
        addFocusId('otherItemName');
        return false;
        
    }else if( itemId == "Other" && otherItemPrice == "" ){
        toastr.warning("Please enter item amount");
        addFocusId('otherItemPrice');
        return false;
        
    }else if( itemId == "Other" && cgst == "" && sgst == "" && igst == "" ){
        toastr.warning("Please enter atleast one taxation percentage");
        addFocusId('cgstPer');
        return false;
        
    }else if( itemId == "Other" && cgst != "" && (parseFloat(cgst) > 100) ){
        toastr.warning("Please enter valid item cgst percentage");
        addFocusId('cgstPer');
        return false;
        
    }else if( itemId == "Other" && sgst != "" && (parseFloat(sgst) > 100) ){
        toastr.warning("Please enter valid item sgst percentage");
        addFocusId('sgstPer');
        return false;
        
    }else if( itemId == "Other" && igst != "" && (parseFloat(igst) > 100) ){
        toastr.warning("Please enter valid item igst percentage");
        addFocusId('igstPer');
        return false;
        
    }else if( (itemId == "Other") && ( cgst != "" && parseFloat(cgst) != "0" || sgst != "" && parseFloat(sgst) != "0"  ) && (igst != "" && parseFloat(igst) != "0") ){
        toastr.warning("You cannot enter Igst alongwith Cgst or Sgst");
        return false;

    }else {


        if( itemId != "Other" ){

            var index = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );

            if( index != -1 ){
                var temp = {
                    itemId : itemId,
                    itemName : itemName,
                    cgst : GBL_ITEMS[index].cgst,
                    sgst : GBL_ITEMS[index].sgst,
                    igst : GBL_ITEMS[index].igst,
                    hsnCode : GBL_ITEMS[index].hsnCode,
                    itemAmount : itemAmount
                }
                GBL_TEMP_ITEM.push(temp);
            }

        }else{
            
            var temp = {
                itemId : itemId,
                itemName : otherItemName,
                cgst : (cgst != "" ? cgst : "0"),
                sgst : (sgst != "" ? sgst : "0"),
                igst : (igst != "" ? igst : "0"),
                hsnCode : "",
                itemAmount : otherItemPrice
            }
            GBL_TEMP_ITEM.push(temp);
        }
        
        renderItemList();
        resetItemFields();
    
    }

}

// RENDER ADDED ITEMS IN DATATABLE - ADDED BY VISHAKHA TAK ON 24-08-2018
function renderItemList() {

    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Item Desc.</th>'+
                        '<th>Item Amount</th>'+
                        '<th>Cgst %</th>'+
                        '<th>Sgst %</th>'+
                        '<th>Igst %</th>'+
                        '<th>Action</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    if( GBL_TEMP_ITEM != "" && GBL_TEMP_ITEM.length > 0 ){

        GBL_TEMP_ITEM.forEach(function( record , index ){

            var roomId = "";
            if( record.roomId != "" && record.roomId != undefined && record.roomId != "undefined" ){
                roomId = record.roomId;
            }

            newRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ record.itemName +'</td>'+ 
                            '<td>'+ fixedTo2(record.itemAmount) +'</td>'+ 
                            '<td>'+ fixedTo2(record.cgst) +'</td>'+ 
                            '<td>'+ fixedTo2(record.sgst) +'</td>'+ 
                            '<td>'+ fixedTo2(record.igst) +'</td>'+ 
                            '<td>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" arrIndex="'+ index +'" onclick="removeItemRow(this);"><i class="fa fa-trash-o"></i></a>'+
                            '</td>'+
                        '</tr>';

        });

    }
    $('#dtTable').html(thead + newRow + tfoot);
    $("#tableListing").DataTable();

}

// RESET ITEM FIELDS AFTER ADDING INTO ITEM JSON - ADDED BY VISHAKHA TAK ON 24-08-2018
function resetItemFields(){

    $('#itemName_1').val('-1').selectpicker('refresh') ;
    $('#itemName_1').val('') ;
    $('#itemAmount1').val('');
    $('#itemAmount1').removeAttr('readonly');
    $('#cgstPer').val('');
    $('#sgstPer').val('');
    $('#igstPer').val('');
    $('#itemName_1').val( 'Other' ).selectpicker('refresh');
    $('#itemName_1').trigger('change');
    $('#otherItemPrice').val('');
    $('#otherItemPrice').removeAttr('onkeyup');

}

// DELETE SELECTED ITEM - ADDED BY VISHAKHA TAK ON 24-08-2018
function removeItemRow(context){

    var cnDelete = confirm("Do you want to delete this item ?");
    if( !cnDelete ){
        return false;
    }

    var arrIndex = $(context).attr('arrIndex');
    if( arrIndex != "" && arrIndex != undefined ){
        GBL_TEMP_ITEM.splice(arrIndex,1);
        renderItemList();
    }
 
}