/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 05-01-2018.
 * File : dynamicReportListController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_REPORT_DATA = [];   // ALL REPORT DATA
var GBL_USER_LIST = []; // ALL USERS
var GBL_USERS = []; // ALL USERS

// INITIALIZE THE FUNCTION AT THE PAGE LOAD TIME
function pageInitialEvents(){

    initCommonFunction();   // INITIALIZE THE COMMON FUNCTIONS
    getReportList();      // GET THE LIST OF THEAD OF ACTIVITY
}

// REPORT LISTING API CALL
function getReportList(){
 
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // API CALL FOR DYNAMIC REPORT
    var postData = { 
        requestCase: 'getAllReportListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(54,227), 
        // reportId: 1, 
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportListCallback,"Please Wait... Getting Dashboard Detail");
    //CALL BACK OF REPORT LISTING
    function getReportListCallback(flag,data){

        if (data.status == "Success" && flag) {
            console.log(data);
 
            GBL_REPORT_DATA = data.data; 
            renderReportSideBar();
            renderSelectedReport( "0" );

        } else {

            GBL_REPORT_DATA = [];  
            renderReportSideBar();

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_DYNAMIC_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// SIDE BAR FIELD LISTING OF REPORT
function renderReportSideBar(){
 
    var sideBarHtml = "";
    if(  GBL_REPORT_DATA != "" && GBL_REPORT_DATA != "No record found" ){

        // NORMAL FIELD RENDERING OF SIDE BAR 
        GBL_REPORT_DATA.forEach( function( record , index ){
            //LISTING GENERATION   
            var funName = "renderSelectedReport("+ index +");";
            sideBarHtml += '<li id="reportLi_'+ index +'"><a id="reportA_'+ record.reportId +'" href="javascript:void(0)" onclick="'+ funName +'">'+ record.reportName +'</a></li>';
        }); 
    } 
    //FINAL LISTING HTML GENERATION
    $('#actHeadUlList').html( sideBarHtml );

    setTimeout( function(){
        if( localStorage.selectedDynamicReport != undefined && localStorage.selectedDynamicReport != "" ){
            $('#reportA_'+localStorage.selectedDynamicReport).click();
        }else{
            $('#reportLi_0 a').click();
        }
    });
}

function renderSelectedReport( reportIndex ){

    var reportData = "";
    if( reportIndex != "-1" ){
        reportData = GBL_REPORT_DATA[reportIndex];
    }
    var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th> Report Name </th>'+
                        '<th> Created On </th>'+
                        '<th> Edited On </th>'+
                        '<th> Created By </th>'+
                        // '<th> Scheduled </th>'+
                        '<th> Action </th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';
    var newRow = "";

    if( reportData.reportData != 'No record found' && reportData.reportData != "" && reportData.reportData != undefined ){
        // localStorage.selectedDynamicReport = reportData.reportId;
        $('#actHeadUlList li').removeClass('active');
        $('#reportLi_'+reportIndex).addClass('active');

        
        reportData.reportData.forEach( function( record , index ){

            var iconCls = "pencil";
            var toolTipLabel = "Edit";
            if( record.reportType == "1" ){
             // SYSTEM GENERATED
                iconCls = "plus";
                toolTipLabel = "Create";
            } 
            
            var reportActions = '<a href="javascript:void(0)" class="btn btn-xs btn-cyan btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Schedule Report" onclick="hideShowTr('+ record.reportId +')"><i class="fa fa-clock-o"></i></a>';            
            if( record.shareReport != "1" ){    // NOT SHARED REPORT
                reportActions = '<a href="javascript:void(0)" class="btn btn-xs btn-cyan btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Schedule Report" onclick="hideShowTr('+ record.reportId +')"><i class="fa fa-clock-o"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-warning btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Report Share" onclick="openShareModal('+ reportData.reportId +' , '+ record.reportId +')"><i class="fa fa-share-alt"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="'+ toolTipLabel +' Report" onclick="navigateToDynamicReport('+ reportData.reportId +' , '+ record.reportId +' , 2 , '+ record.reportType +')"><i class="fa fa-'+ iconCls +'"></i></a>'+
                                '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Delete Report" onclick="deleteReport('+ reportData.reportId +' , '+ record.reportId +')"><i class="fa fa-trash-o"></i></a>';
            }
             
            if( record.reportType == "1" ){    // SYSTEM GENERATED REPORT
                reportActions = '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="'+ toolTipLabel +' Report" onclick="navigateToDynamicReport('+ reportData.reportId +' , '+ record.reportId +' , 2 , '+ record.reportType +')"><i class="fa fa-'+ iconCls +'"></i></a>';
            }

            // TEST DATA
            //REMOVE WHEN ACTUAL DATA GET
            // reportActions = '<a href="javascript:void(0)" class="btn btn-xs btn-cyan btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Schedule Report" onclick="hideShowTr('+ record.reportId +')"><i class="fa fa-clock-o"></i></a>'+
            //                 '<a href="javascript:void(0)" class="btn btn-xs btn-warning btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Report Share" onclick="openShareModal('+ reportData.reportId +' , '+ record.reportId +')"><i class="fa fa-share-alt"></i></a>'+
            //                 '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="'+ toolTipLabel +' Report" onclick="navigateToDynamicReport('+ reportData.reportId +' , '+ record.reportId +' , 2 , '+ record.reportType +')"><i class="fa fa-'+ iconCls +'"></i></a>';


            newRow += '<tr>'+
                        '<td>'+ (index+1) +'</td>'+
                        '<td><a href="javascript:void(0)" onclick="navigateToDynamicReport('+ reportData.reportId +' , '+ record.reportId +' , 1 , '+ record.reportType +')">'+ record.reportName +'</a></td>'+
                        '<td>'+ record.reportCreatedOn +'</td>'+
                        '<td>'+ record.reportEditOn +'</td>'+
                        '<td>'+ record.reportGeneratedBy +'</td>'+
                        // '<td> </td>'+
                        '<td>'+
                            '<a href="javascript:void(0)" class="btn btn-xs btn-success btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="View Report" onclick="navigateToDynamicReport('+ reportData.reportId +' , '+ record.reportId +' , 1 , '+ record.reportType +')"><i class="fa fa-eye"></i></a>'+
                            reportActions +
                        '</td>'+
                    '</tr>';
 
            var reportScheduleId = ""; 

            var schStartDateVal = "";
            var schEndDateVal = "";
            var schFreqVal = "";

            var schRepBtn = '<a href="javascript:void(0)" class="btn btn-xs btn-success btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Save Schedule" onclick="saveUpdateSchedule('+ record.reportId +' , '+ reportData.reportId +' )"><i class="fa fa-save"></i></a>';
            if( record.scheduledReport != "No record found" ){
                reportScheduleId = record.scheduledReport.reportScheduleId;
                schStartDateVal = record.scheduledReport.startDate;
                schEndDateVal = record.scheduledReport.endDate;
                schFreqVal = record.scheduledReport.freqVal;
                schRepBtn = '<a href="javascript:void(0)" class="btn btn-xs btn-warning btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Edit Schedule" onclick="saveUpdateSchedule('+ record.reportId +' , '+ reportData.reportId +' , '+ reportScheduleId +' )"><i class="fa fa-pencil"></i></a>'+
                            '<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-right-mrg" data-toggle="tooltip" data-placement="left" data-original-title="Delete Schedule" onclick="deleteScheduleReport('+ reportScheduleId +' , '+ record.reportId +' , '+ record.reportRefId +' )"><i class="fa fa-trash-o"></i></a>';
            }

            // FREQUENCY OPTION LIST
            var scheduleFreqOpt = "";
            SCHEDULER_FREQUENCY.forEach( function( record , index ){
                if( record.value == schFreqVal ){
                    scheduleFreqOpt += '<option value="'+ record.value +'" selected>'+ record.label +'</option>';
                }else{
                    scheduleFreqOpt += '<option value="'+ record.value +'">'+ record.label +'</option>';
                }
            });

            newRow += '<tr id="row_'+ record.reportId +'" class="hideTr userSelectListHead scheduleRow_'+ reportScheduleId +'" >'+
                        '<td><span style="display:none;">'+ (index+1) +'</span></td>'+
                        '<td><span style="display:none;">'+ record.reportName +'</span><input type="text" id="startDateScheduled_'+ record.reportId +'" value="'+ schStartDateVal +'" class="form-control default-dateTime-picker" placeholder="Select Start Date / Time" readonly="" autocomplete="off"></td>'+
                        '<td><span style="display:none;">'+ record.reportCreatedOn +'</span><input type="text" id="endDateScheduled_'+ record.reportId +'" value="'+ schEndDateVal +'" class="form-control default-dateTime-picker" placeholder="Select End Date / Time" readonly="" autocomplete="off"></td>'+
                        '<td><span style="display:none;">'+ record.reportEditOn +'</span></td>'+
                        '<td class="userSelectList"><span style="display:none;">'+ record.reportGeneratedBy +'</span>'+
                            '<select data-live-search="true" id="scheduleFreq_'+ record.reportId +'" class="scheduleFreq" data-width="100%" >'+
                                scheduleFreqOpt +
                            '</select></td>'+
                        // '<td> </td>'+
                        '<td>'+ schRepBtn +'</td>'+
                    '</tr>';
        });
    }
    $('#dynamicReportList').html(thead + newRow + tfoot);
    initDefaultDateTimePicker(); // DATE PICKER INIT
    $('#tableListing').dataTable({"stateSave": true,"info":false});

    // setOptionWithRef( "1" , "scheduleFreq" , SCHEDULER_FREQUENCY , "" , "value" , "label" );
    $('.scheduleFreq').selectpicker('refresh');
}

function hideShowTr(statId,schedule){

    if( $('#row_'+statId).hasClass('hideTr') ){
        $('#row_'+statId).removeClass('hideTr').addClass('showTr'); 
    }else{
        $('#row_'+statId).removeClass('showTr').addClass('hideTr'); 
    }
}

function openShareModal( reportId , reportRefId ){ 
    
    $('#shareUser').val(''); 
    $('#shareUser').attr('userId','');
    
    GBL_SHARE_REPORT_ID = reportId;
    GBL_SHARE_REPORT_REF_ID = reportRefId;

    var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
    if( index != "-1" ){
        var innrIndx = findIndexByKeyValue( GBL_REPORT_DATA[index].reportData , "reportId" , reportRefId );
        if( innrIndx != "-1" ){
            if( GBL_REPORT_DATA[index].reportData[innrIndx].userShareData != "No record found" ){
                GBL_USERS = GBL_REPORT_DATA[index].reportData[innrIndx].userShareData;
            }else{
                GBL_USERS = [];
            }
            renderUsersList( GBL_USERS );
        }
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD
    var postData ={
        requestCase : "userListingActivityModule",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(2,6),
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(FOLLOWUPURL, postData, getUserListCallback,"");
    //CALL BACK OF REPORT LISTING
    function getUserListCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            if( data.data != "No record found" ){ 
                GBL_USER_LIST = [];
                data.data.forEach( function( record , index){

                    if( tmp_json.data[0].PK_USER_ID != record.userId ){
                        var tempData = {
                            id : record.userId,
                            name : record.fullName,
                            data : record.fullName,
                        }
                        GBL_USER_LIST.push( tempData );
                    }
                });
                autoCompleteForUser("shareUser", saveUserDetail , GBL_USER_LIST);
            }
            modalShow('shareModal');

        } else {
 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
            else
                toastr.error(SERVERERROR);
        }
    } 
}

function autoCompleteForUser(id,callback,userData) {
     
     $("#" + id).typeahead({
        onSelect: function (item) {
            console.log(item);
            setTimeout(function(){

                var userIndex = findIndexByKeyValue(GBL_USER_LIST,"id",item.value);

                var userName = GBL_USER_LIST[userIndex].name;
                var userId = GBL_USER_LIST[userIndex].id;
                $('#shareUser').val( userName );  
                $('#shareUser').attr('userId',userId);  
            },50);
        },
        source: GBL_USER_LIST,
    });
}

function saveUserDetail(){

}

function renderUsersList( userData ){

    var userListHead = '<thead>'+
                            '<tr>'+
                                '<th>#</th>'+
                                '<th>User Name</th>'+
                                '<th>Action</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody id="userGroupList">';
    var userListEnd = '</tbody>';
    var userListBody = "";
    if( userData != "" ){
        userData.forEach( function( record , index ){

            var disBtn = "";
            if( record.shareId == undefined ){
                disBtn = "disabled";
            }

            userListBody += '<tr>'+
                                '<td>'+ (index+1) +'</td>'+
                                '<td>'+ record.userName +'</td>'+
                                '<td><a '+ disBtn +' href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" onclick="removeUser('+ record.shareId +')"><i class="fa fa-trash-o"></i></a></td>'+
                            '</tr>';
        });

    }                       
    $('#userListHtml').html( userListHead + userListBody + userListEnd);
}

function assignUserToShare(){

    var userName = $('#shareUser').val();  
    var userId = $('#shareUser').attr('userId'); 
    
    if( userId == "" || userId == undefined ){
        toastr.warning('Please Search Users');
        $('#shareUser').focus();  
        return false;
    }

    var index = findIndexByKeyValue( GBL_USERS , "userId" , userId );
    if( GBL_USERS == "" ){
        index = "-1";
    }
    if( index == "-1" ){

        var tempUser = {
            userId : userId,
            userName : userName,
        }
        GBL_USERS.push( tempUser );
    }else{
        toastr.warning('Already Added this user');
        return false;
    }
    renderUsersList( GBL_USERS );

    $('#shareUser').val(''); 
    $('#shareUser').attr('userId','');  
}

function shareReportData(){

    if( GBL_USERS == "" ){
        toastr.warning('Please Select Users');
        addFocusId('shareUser');
        return false;
    } 

    var users = "";
    GBL_USERS.forEach( function( record , index ){
        if( users ){
            users += ','+record.userId;
        }else{
            users = record.userId;
        }
    });

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD
    var postData ={
        requestCase : "shareDynamicReport",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(54,229),
        userId: tmp_json.data[0].PK_USER_ID,
        reportId: GBL_SHARE_REPORT_ID,
        reportRefId: GBL_SHARE_REPORT_REF_ID,
        shareUserId: users,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(FOLLOWUPURL, postData, shareReportDataCallback,"");
    //CALL BACK OF REPORT LISTING
    function shareReportDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            toastr.success('Report Shared Successfully');
            modalHide('shareModal');
            getReportList();
        } else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_SHARE_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function removeUser( shareId ){ 
     
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD
    var postData ={
        requestCase : "removeSharedUser",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(54 ,229),
        shareId: shareId,
        reportId: GBL_SHARE_REPORT_ID,
        reportRefId: GBL_SHARE_REPORT_REF_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(FOLLOWUPURL, postData, removeSharedReportCallback,"");
    //CALL BACK OF REPORT LISTING
    function removeSharedReportCallback(flag,data){

        if (data.status == "Success" && flag) {
            
             var index = findIndexByKeyValue( GBL_USERS , "shareId" , shareId );
             if( index != "-1" ){
                GBL_USERS.splice( index , 1 );
                renderUsersList( GBL_USERS );
             }
        } else {
 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_REMOVE_SHARE_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    } 
}

function saveUpdateSchedule( reportId , reportRefId , scheduleId ){ 
     
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD

    // scheduleId -- "" -- CREATE ---- "schedule ID available then" -- EDIT 
    if( scheduleId == undefined ){
        scheduleId = "";
    }

    var startDate = $('#startDateScheduled_'+reportId).val();
    var endDate = $('#endDateScheduled_'+reportId).val();
    var frequency = $('#scheduleFreq_'+reportId).val();

    if( startDate == "" ){
        toastr.warning('Please Select Start Date');
        $('#startDateScheduled_'+reportId).datetimepicker('show');
        return false;
    }

    var schStartDate = (($('#startDateScheduled_'+reportId).val() ).split('|')[0]).trim();
    var schStartTime = (($('#startDateScheduled_'+reportId).val() ).split('|')[1]).trim();
    var startDateVal = ddmmyyToMysql(schStartDate,'yyyy-MM-dd');
    
    var schEndDate = "";
    var schEndTime = "";
    var endDateVal = "";

    if( endDate != "" ){
        schEndDate = (($('#endDateScheduled_'+reportId).val() ).split('|')[0]).trim();
        schEndTime = (($('#endDateScheduled_'+reportId).val() ).split('|')[1]).trim();
        endDateVal = ddmmyyToMysql(schEndDate,'yyyy-MM-dd');

        var startVar = new Date( startDateVal + ' '+ schStartTime );
        var endVar = new Date( endDateVal + ' '+ schEndTime );

        if( +startVar > +endVar ){
            toastr.warning("Start Date/Time should not be less than End Date/Time");
            $('#startDateScheduled_'+reportId).datetimepicker('show');
            return false;
        }
    }

    if( scheduleId == "" ){

        var postData ={
            requestCase : "createScheduleReport",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            orgId: checkAuth(54,230),
            userId: tmp_json.data[0].PK_USER_ID,
            startDate: startDate,
            endDate: endDate,
            frequency: frequency,
            reportId: reportId,
            reportRefId: reportRefId, 
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
    }else{
        var postData ={
            requestCase : "updateScheduleReport",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            orgId: checkAuth(54,230),
            userId: tmp_json.data[0].PK_USER_ID,
            startDate: startDate,
            endDate: endDate,
            frequency: frequency,
            reportId: reportId,
            reportRefId: reportRefId,
            scheduleId: scheduleId, // "" -- CREATE - "ID" -- UPDATE
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };
    }
    commonAjax(FOLLOWUPURL, postData, saveUpdateScheduleCallback,"");
    //CALL BACK OF REPORT LISTING
    function saveUpdateScheduleCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            if( scheduleId == "" ){
                toastr.success('Scheduled Report Added Successfully'); 
            }else{
                toastr.success('Scheduled Report Updated Successfully'); 
            }
            // getReportList();

            var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportRefId );
            if( index != "-1" ){
                var refRepIdIndex = findIndexByKeyValue( GBL_REPORT_DATA[index].reportData , "reportId" , reportId );
                if( refRepIdIndex != "-1" ){ 
                    GBL_REPORT_DATA[index].reportData[ refRepIdIndex ].scheduledReport = {};
                    GBL_REPORT_DATA[index].reportData[ refRepIdIndex ].scheduledReport.reportScheduleId = data.data;
                    GBL_REPORT_DATA[index].reportData[ refRepIdIndex ].scheduledReport.freqVal = frequency;
                    GBL_REPORT_DATA[index].reportData[ refRepIdIndex ].scheduledReport.startDate = startDate;
                    GBL_REPORT_DATA[index].reportData[ refRepIdIndex ].scheduledReport.endDate = endDate;
                    renderSelectedReport('0');
                }
            }
                hideShowTr( reportId );
        } else {
 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ADD_UPDATE_SCHEDULE_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    } 
}

function deleteScheduleReport( scheduleId , reportId , reportRefId ){ 
     
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD
    var postData ={
        requestCase : "deleteScheduleReport",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(54,230),
        userId: tmp_json.data[0].PK_USER_ID,
        scheduleId: scheduleId, 
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(FOLLOWUPURL, postData, deleteScheduleReportCallback,"");
    //CALL BACK OF REPORT LISTING
    function deleteScheduleReportCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            toastr.success('Scheduled Report Removed Successfully'); 
            // getReportList();

            var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportRefId );
            if( index != "-1" ){
                var refRepIdIndex = findIndexByKeyValue( GBL_REPORT_DATA[index].reportData , "reportId" , reportId );
                if( refRepIdIndex != "-1" ){
                    GBL_REPORT_DATA[index].reportData[ refRepIdIndex ].scheduledReport = "No record found";
                    renderSelectedReport('0');
                }
            }
            hideShowTr( reportId );
        } else {
 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_DELETE_SCHEDULE_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    } 
}

function deleteReport( reportId , reportRefId ){ 

    var confDel = confirm('Are you sure? You wants to delete this report');
    if( !confDel ){
        return false;
    }
     
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    //SET GROUP LOAD
    var postData ={
        requestCase : "deleteDynamicReport",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        orgId: checkAuth(54,231),
        userId: tmp_json.data[0].PK_USER_ID,
        reportId: reportId, 
        reportRefId: reportRefId, 
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };
    commonAjax(FOLLOWUPURL, postData, deleteReportCallback,"");
    //CALL BACK OF REPORT LISTING
    function deleteReportCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            toastr.success('Report Removed Successfully'); 
            // getReportList();

            var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
            if( index != "-1" ){
                var refRepIdIndex = findIndexByKeyValue( GBL_REPORT_DATA[index].reportData , "reportId" , reportRefId );
                if( refRepIdIndex != "-1" ){
                    GBL_REPORT_DATA[index].reportData.splice( refRepIdIndex , 1 );
                    renderSelectedReport('0');
                }
            } 
        } else {
 
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_DELETE_DYNAMIC_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    } 
}