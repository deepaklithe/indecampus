/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 16-08-2016.
 * File : nonAvailingViewController.
 * File Type : .js.
 * Project : indecampus
 *
 * */

var GBL_NON_AVAIL_TRANS_DATA = []; // GLOBAL DATA JSON 

// INITIAL PAGE EVENTS
function pageInitialEvents(){

    if( localStorage.indeCampusEditAvail != undefined && localStorage.indeCampusEditAvail != "" ){
        getNonAvailingServices(); //GET NON AVAILING SERVICES TRANS DATA
    }else{
        navigateToNonAvailServices(); // REDIRECT BACK
    }
    
}
  
//GET NON AVAILING SERVICES LIST DATA
function getNonAvailingServices(){  
    
    var tmp_json = JSON.parse( localStorage.indeCampusUserDetail );
    var postdata= {
        requestCase : 'listStudentNonAvailingService',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(27, 108),
        serviceId : localStorage.indeCampusEditAvail,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }; 

    commonAjax(FOLLOWUPURL, postdata, getNonAvailingServicesCallBack,"Please wait.....Changing Student password");
    // console.log(postdata);return false; 
    function getNonAvailingServicesCallBack(flag,data){
        
        if(data.status == "Success" && flag){
            
            console.log(data);
            GBL_NON_AVAIL_TRANS_DATA = data.data;
            renderNonAvailTransData(); // RENDER NON AVAILING SERVICES LIST DATA
            $('#serviceReqId').html( GBL_NON_AVAIL_TRANS_DATA[0].cServiceId );
            $('#serviceReqDate').html( mysqltoDesiredFormat(GBL_NON_AVAIL_TRANS_DATA[0].serviceStartDate,'dd-MM-yyyy') );

        }else{

            if(flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NON_AVAIL_DATA );
                //location.reload();
            }else{
                toastr.warning(SERVERERROR); 
            }
        }
    }
} 

// RENDER NON AVAILING SERVICES LIST DATA
function renderNonAvailTransData(){

	var tHead = '<table class="table table-striped display" id="ledgerTbl">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Service Date</th>'+
                            '<th>Status</th>'+
                            '<th>Action</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';

    var tEnd = '</tbody></table>';

    var newRow = '';

    if( GBL_NON_AVAIL_TRANS_DATA != "" && GBL_NON_AVAIL_TRANS_DATA != "No record found" ){

    	GBL_NON_AVAIL_TRANS_DATA[0].servicetransData.forEach(function( record , index ){

            var status = ""; // 1=CONFIRM,2=CANCEL
            var disabledClass = ""; // 1=CONFIRM,2=CANCEL
            if( record.status == "1" ){
                status = "Confirm";
            }else{
                status = "Cancel";
                disabledClass = "disabled";
            }
            if( record.hide == "1" ){
                disabledClass = "disabled";
            }

	    	newRow +=  '<tr>'+
	                        '<td>'+ (index+1) +'</td>'+
	                        '<td>'+ ( record.serviceDate != "" && record.serviceDate != "0000-00-00" ? mysqltoDesiredFormat(record.serviceDate,'dd-MM-yyyy') : "" ) +'</td>'+
	                        '<td>'+ status +'</td>'+
	                        '<td>'+
	                            '<a href="javascript:void(0);" class="btn btn-sm btn-danger '+disabledClass+'" data-toggle="tooltip" data-original-title="Cancel" serviceId="'+GBL_NON_AVAIL_TRANS_DATA[0].serviceId+'" transId="'+record.transId+'" onclick="deleteNonAvailServiceTrans(this);"><i class="fa fa-trash"></i></a>'+
	                        '</td>'+
	                    '</tr>';       
                                                
    	});

    }   
    $('#nonAvailDiv').html( tHead + newRow + tEnd );
    $('#ledgerTbl').dataTable();

}

// DELETE SELECTED NON AVAIL SERVICE TRANS
function deleteNonAvailServiceTrans(context){

    var serviceId = $(context).attr('serviceId');
    var transId = $(context).attr('transId');
    var cnfmDelete = confirm("Are you sure you want to cancel request for this date ?");
    if( !cnfmDelete ){
        return false;
    }

    var tmp_json = JSON.parse( localStorage.indeCampusUserDetail );
    var postdata= {
        requestCase : 'updateServiceStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(27, 109),
        servicetransId : transId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }; 

    commonAjax(FOLLOWUPURL, postdata, deleteNonAvailServiceCallBack,"Please wait.....Changing Student password");
    // console.log(postdata);return false; 
    function deleteNonAvailServiceCallBack(flag,data){
        
        if(data.status == "Success" && flag){
            
            console.log(data);
            toastr.success("Non availing for this service canceled successfully !");
            var index = findIndexByKeyValue(GBL_NON_AVAIL_TRANS_DATA[0].servicetransData,'transId',transId);
            if( index != -1 ){
                GBL_NON_AVAIL_TRANS_DATA[0].servicetransData[index].status = "2";
                renderNonAvailTransData();
            }

        }else{

            if(flag){
                displayAPIErrorMsg( data.status , GBL_ERR_NON_AVAIL_DEL );
                //location.reload();
            }else{
                toastr.warning(SERVERERROR); 
            }
        }
    }

}

