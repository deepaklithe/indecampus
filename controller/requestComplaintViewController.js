    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 04-08-2018.
 * File : requestComplaintViewController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_VIEW_DATA = [];
var GBL_ATTACH_IMG_DATA = [];
var GBL_COMMENTS = [];
var GBL_ITEMS = [];
var GBL_CHARGEABLE_ITEMS = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){ 

	if( localStorage.indeCampusViewReqId != "" && localStorage.indeCampusViewReqId != undefined ){
        getRequestComplaint();
    }else{
    	displayAPIErrorMsg( '' , 'No request / complaint detail found' );
    	navigateToReqComplaintList();
    }

	$('input[type=radio][name="closeRequestOption"]').off().on('change', (function (e) {
        if (this.value == 'yes') {
           $('#chargeableDiv').show(300);

        } else if (this.value == 'no') {
           $('#chargeableDiv').hide(300);

        }

    }));
    $('#reqChargeTbl').dataTable();
    getChargeableItems();

}

// GET REQUEST / COMPLAINT LISTING DATA
function getRequestComplaint(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "getComplaintsData",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22,90),
        complaintId : localStorage.indeCampusViewReqId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestComplaintCallback,"Please wait...Adding rooms to your item.");

    function getRequestComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_VIEW_DATA = data.data;
            renderViewData();

        }else{


            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_REQ_COM_DET );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function renderViewData(){

	if( GBL_VIEW_DATA != "" && GBL_VIEW_DATA != "No record found" ){

		var viewData = GBL_VIEW_DATA[0];

		$('#studentName').html( viewData.studName );
		$('#studentMemId').html( viewData.membershipNumber );
		$('#ticketId').html( viewData.ticketId );
		$('#departmentName').html( viewData.departmentName );
		$('#categoryName').html( viewData.categoryName );
		$('#subCategoryName').html( viewData.parentCategoryName );
		
		var raisedDate = (viewData.complaintStartTime != "" ? mysqltoDesiredFormat(viewData.complaintStartTime.split(" ")[0],'dd-MM-yyyy') : "" );
		var reqCompDesc = ( viewData.transData != "" && viewData.transData != "No record found" ? viewData.transData[0].description : "" );
		$('#raisedDate').html( raisedDate );
		$('#reqComplaintDescription').html( reqCompDesc );
		if( viewData.attachmentData != "" && viewData.attachmentData != "No record found" ){
			GBL_ATTACH_IMG_DATA = viewData.attachmentData;
			renderAttachmentSlider();
		}else{
			$('#myCarousel').html('');
		}
		if( viewData.transData != "" && viewData.transData != "No record found" ){
			GBL_COMMENTS = viewData.transData;
		}
		renderCommentTimeline();	
		if( viewData.status == "2" || viewData.status == "4" ){ // WHEN CLOSED / CANCELED
	        $('#reqCloseBtn').hide();
	        $('#addCommentBtn').hide();
	    }else{
	    	if( viewData.assignUserId == "0" || viewData.assignUserId == "" ){
	    		$('#addCommentBtn').hide();
	    	}else{
	    		$('#addCommentBtn').show();
	    	}
	        $('#reqCloseBtn').show();
	    }

	    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
	    if( tmp_json.data[0].PK_USER_ID != viewData.assignUserId && viewData.assignUserId != "0" ){
            $('#reqCloseBtn').attr('disabled',true);
	        $('#addCommentBtn').attr('disabled',true);
        }

        // DISABLE UPDATE ACTIONS IF NOT OF THAT REQ/COMP CATEGORY DEPT - ADDED ON 28-08-2018
        var disableUpd = false;
        var allotedDept = ( tmp_json.data[0].DEPARTMENTID != "" && tmp_json.data[0].DEPARTMENTID  != "0" ? tmp_json.data[0].DEPARTMENTID : ""  );
        allotedDept = allotedDept.split(',');
        allotedDept.forEach(function( deptRcd , deptIdx ){
            if( viewData.departmentId == deptRcd ){
                disableUpd = true;
            }
        });
        if( !disableUpd ){
        	$('#reqCloseBtn').attr('disabled',true);
        	$('#addCommentBtn').attr('disabled',true);
        }
	}

}

function renderAttachmentSlider(){

	var sliderListHtml = "";
	if( GBL_ATTACH_IMG_DATA != "" && GBL_ATTACH_IMG_DATA != "No record found" ){
		GBL_ATTACH_IMG_DATA.forEach( function( record , index ){ 

		    var imageSrc = "";
		    if( record.attachmentLink != "" ){
		        imageSrc = record.attachmentLink;
		    }
		    var activeClass = ( index == 0 ? 'active' : "" );

		    sliderListHtml += '<div class="item '+activeClass+'">'+
		                          '<img src="'+imageSrc+'" alt="'+record.title+'">'+
		                        '</div>';
		});                 
	}
	$('#attachmentSlider').html( sliderListHtml );
	
    $('#myCarousel').carousel({
    	// interval: 20000
    });
    // if( GBL_ATTACH_IMG_DATA.length <= 1 ){
    // 	$('.carousel-control').hide();
    // }
}

function playPrevNext(type){

	if( type == 'prev' ){
		 
		$('#myCarousel').carousel('prev');
	}else{
		$('#myCarousel').carousel('next');
		
	}
	
}

function renderCommentTimeline(){

	var timeineInnerHtml = '';
	if( GBL_COMMENTS != "" && GBL_COMMENTS != "No record found" ){
		GBL_COMMENTS.forEach( function( record , index ){ 

			timeineInnerHtml += '<li class="li complete">'+
                                    '<div class="timestamp">'+
                                      '<div><i class="fa fa fa-comments"></i> '+ record.description +'</div>'+
                                     '<div class="customInqActListing"><i class="fa fa fa-user"></i> '+ ( record.username != null ? ('By '+ record.username) : "") +'</div>'+
                                     '<div class="customInqActListing"><i class="fa fa-clock-o"></i> '+ record.commentDateTime +'</div>'+
                                    '</div>'+
                                    '<div class="status">'+
                                    '</div>'+
                                  '</li>';

		   
		});                 
	}
	$('#timeline').html( timeineInnerHtml );

}

function addComment(){

	var commentDesc = $('#commentDesc').val();
	if( commentDesc == "" ){
		toastr.warning("Please enter comment description");
		addFocusId('commentDesc');
		return false;

	}

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : "addComplaintComment",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(22, 89),
        complaintId : localStorage.indeCampusViewReqId,
        comment : commentDesc,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, createRequestComplaintCallback,"Please wait...Adding rooms to your item.");

    function createRequestComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            modalHide('AddCommentModal');
            $('#commentDesc').val('');
            getRequestComplaint();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_CRE_UPD_REQ_COMP );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function getChargeableItems(){

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "MISC",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ITEMS = data.data;
            renderItemList(); // RENDER ITEMS

        }else{

            GBL_ITEMS = [];
            renderItemList(); // RENDER ITEMS
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEMS );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function renderItemList(){

	var optionList = '<option value="-1">Select Item</option>';
	if( GBL_ITEMS != "" && GBL_ITEMS != "No record found" ){

		GBL_ITEMS.forEach(function( record , index ){

			optionList += '<option value="'+ record.itemId +'">'+ record.itemName +'</option>';

		});

	}
	$('#chargeItem').html( optionList );
	$('#chargeItem').selectpicker('refresh');

}

function getItemQtyPrice(context){

	var itemId = $(context).val();
	if( itemId != "-1" && itemId != "" ){

		var itemPrice = 0;
		var itemIndex = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );
		if( itemIndex != -1 ){
			itemPrice = parseFloat(GBL_ITEMS[itemIndex].itemPrice);
			$('#chargeQty').val( "1" );
			$('#chargePrice').val( fixedTo2(itemPrice) );
		}else{
			$('#chargeQty').val( "" );
			$('#chargePrice').val( "" );
		}
		

	}else{

		$('#chargeQty').val( "" );
		$('#chargePrice').val( "" );
	}
	$('#chargePrice').attr('disabled',true);

}

function calculatePrice(context) {

	var itemQty = $(context).val();
	var itemId = $('#chargeItem').val();
	var itemChargePrice = $('#chargePrice').val();
	
	if( itemQty != "" && itemQty != "0" ){
		var itemIndex = findIndexByKeyValue( GBL_ITEMS , 'itemId' , itemId );
		if( itemIndex != -1 ){

			var itemPrice = parseFloat(itemQty) * parseFloat(GBL_ITEMS[itemIndex].itemPrice);
			$('#chargePrice').val( fixedTo2(itemPrice) );

		}
	}else{
		$('#chargeQty').val( "" );
		$('#chargePrice').val( "" );
	}


}

function addItemRow(){

	var itemName = $('#chargeItem option:selected').text();
	var itemId = $('#chargeItem').val();
	var itemQty = $('#chargeQty').val();
	var itemPrice = $('#chargePrice').val();

	if( itemId == "-1" || itemId == "" ){
		toastr.warning("Please select item to add to charge");
		addFocusId('chargeItem');
		return false;

	}else if( itemQty == "" || itemQty == "0" ){
		toastr.warning("Please enter atleast one item quantity");
		addFocusId('chargeQty');
		return false;

	}else if( itemPrice == "" || itemPrice == "0" ){
		toastr.warning("Item price not found");
		addFocusId('chargePrice');
		return false;

	}

	var tempItem = {
		itemName : itemName,
		itemId : itemId,
		itemQty : itemQty,
		itemPrice : itemPrice
	}
	GBL_CHARGEABLE_ITEMS.push(tempItem);
	renderItemRow();
	resetItemFields();
}


function renderItemRow(){

	var tHead = '<table class="table charge-posting-table display table-bordered  dataTable" id="reqChargeTbl">'+
                    '<thead>'+
                        '<tr>'+
                        '<th>#</th>'+
                        '<th>Item</th>'+
                        '<th>Qty</th>'+
                        '<th>Price</th>'+
                    '</tr>'+
                    '</thead>'+
                    '<tbody id="reqChargeTblItems">';

    var tEnd = '</tbody></table>';
	var newRow = '';

	if( GBL_CHARGEABLE_ITEMS != "" && GBL_CHARGEABLE_ITEMS != "No record found" ){

		GBL_CHARGEABLE_ITEMS.forEach(function( record , index ){

			newRow += '<tr>'+
							'<td>'+ (index+1) +'</td>'+
							'<td>'+ record.itemName +'</td>'+
							'<td>'+ record.itemQty +'</td>'+
							'<td><i class="fa fa-rupee"></i> '+ formatPriceInIndianFormat(record.itemPrice) +'</td>'+
						'</tr>';

		});

	}
	$('#reqChargeTblDiv').html( tHead + newRow + tEnd );
	$('#reqChargeTbl').dataTable();

}

function resetItemFields(){

	$('#chargeItem').val( "-1" ).selectpicker('refresh');
	$('#chargeQty').val( "" );
	$('#chargePrice').val( "" );
	$('#chargePrice').attr('disabled',false);

}

function closeReqComplaint(){

	var closeReqCompType = $('input[type=radio][name="closeRequestOption"]:checked').val()

	if( closeReqCompType == "yes" ){

		if( GBL_CHARGEABLE_ITEMS == "" || GBL_CHARGEABLE_ITEMS == "No record found" ){
			toastr.warning("Please add atleast one item to charge");
			return false;
		}

	}
	var itemArray = [];
	if( GBL_CHARGEABLE_ITEMS != "" && GBL_CHARGEABLE_ITEMS != "No record found" ){

		GBL_CHARGEABLE_ITEMS.forEach(function( record , index ){

			var temp = {
				itemId : record.itemId,
				qty : record.itemQty,
				itemName : record.itemName,
			}
			itemArray.push( temp );
		});

	}
	var chargeable = "1";
	if( closeReqCompType == "yes" ){
		chargeable = "1";
	}else{
		chargeable = "0";
		itemArray = "";
	}

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'closeComplaintData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(22, 92),
        studentId : GBL_VIEW_DATA[0].studId,
        studentName : GBL_VIEW_DATA[0].studName,
        complaintId : localStorage.indeCampusViewReqId,
        isCharge: chargeable,   // 0 -NON CHARGE , 1=CHARGE.
        itemArr : itemArray,
        loginUsername : tmp_json.data[0].FULLNAME,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, closeReqComplaintCallback,"Please wait...Adding your item.");

    function closeReqComplaintCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            navigateToReqComplaintList();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_CLOSE_REQ );
            else
                toastr.error(SERVERERROR);
        }
    }

}


function openCloseReq(){
	
	GBL_CHARGEABLE_ITEMS = [];
	resetItemFields();
	renderItemRow();
	modalShow('ReqModal');
}