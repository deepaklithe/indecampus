/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : developerTypeController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var BOMData = [];
var BOMListingData = [];
var ITEMJSON = [];
var UOMJSONARR = [];
var ITEMDOCJSONARR = [];

function getBOMListingData(){

    $('#searchitem').focus();
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getBomListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14,54),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getBOMListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getBOMListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){

            BOMListingData = data.data ;
            renderBOMListing();
        
        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_BOM );
        }

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_BOM );
        else
            toastr.error(SERVERERROR);
    }
}

function renderBOMListing(){

    addFocusId( 'searchitem' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    var startHtml = "<table id='bomListTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th>BOM Name.</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";
    var inqRow = '';
    for( var i=0; i<BOMListingData.length; i++ ){

        inqRow += '<tr>'+
                    '<td>'+ (i+1) +'</td>'+
                    '<td><a href="javascript:void(0)" id="'+ BOMListingData[i].itemId +'" onclick="openModal(this)">'+ BOMListingData[i].itemName +'</a></td>'+
                    '<td>'+
                        '<a href="javascript:void(0)" onclick="editBom(this)" item-Id="'+ BOMListingData[i].itemId +'" class="btn-right-mrg btn btn-xs btn-primary btn-ripple btn-danger"><i class="fa fa-pencil"></i></a>'+
                        '<a href="javascript:void(0)" onclick="DeleteBom(this)" item-Id="'+ BOMListingData[i].itemId +'" class="btn btn-xs btn-primary btn-ripple"><i class="fa fa-trash-o"></i></a>'+
                    '</td>'+
                '</tr>';
                                                
    }
    var finalHtml = startHtml + inqRow + endHtml;
    $('#BOMTable').html(finalHtml);
    $('#bomListTable').DataTable({ "stateSave": true });
    
    // TablesDataTables.init();
    // TablesDataTablesEditor.init();

}

function openModal(context){

    var itemId = $(context).attr('id');
    var index = findIndexByKeyValue( BOMListingData , 'itemId' , itemId );

    if( index != '-1' ){

        var startHtml = "<table id='bomPopList' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th>BOM Name.</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
        var endHtml = "</tbody>";

        var bomItemList = '';
        var bomInnerItem = BOMListingData[index].bomInnerItem;
        if( bomInnerItem != 'No record found' ){

            for( var i=0; i<bomInnerItem.length; i++ ){
                bomItemList += '<tr>'+
                                    '<td>'+ (i+1) +'</td>'+
                                    '<td>'+ bomInnerItem[i].itemName +'</td>'+
                                '</tr>';
            }
        }

        $('#bomItemListPop').html( startHtml + bomItemList + endHtml );
        $('#bomPopList').DataTable({ "stateSave": true });

    }else{
        toastr.warning('Item Listing is not available');
    }

    $('#bomsales').modal('show');
}

function getUomDataApi() {

    function callBackGetUomListing(flag, data)
    {

        if (data.status == "Success" && flag) {

            if (data.data)
            {
                UOMJSONARR = data.data;

                generateUomDropDown();
                getGroupListing();
            }

        }
        else {
           // if (flag)
           //     toastr.error(data.status);
           // else
           //     toastr.error(SERVERERROR);
        }

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
    var postData = {
        requestCase: 'getUOMListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14, 54),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, callBackGetUomListing, "Please Wait... Getting the group listing");


}

//END FUNCTION FOR DELETE GROUP FROM AN ARRAY

function getGroupListing() {


    function callBackGetGroupListing(flag, data)
    {

        if (data.status == "Success" && flag) {

            if (data.data)
            {
                ITEMGRPJSON = data.data;

                for (var i = 0; i < ITEMGRPJSON.length; i++)
                {

                    var groupName = ITEMGRPJSON[i].GROUP_NAME;
                    var groupId = ITEMGRPJSON[i].PK_ITEM_GROUP_ID;
                    var parentGroupId = ITEMGRPJSON[i].FK_PARENT_GRP_ID;

                    ITEMGRPJSON[i].GROUPNAME = groupName;
                    ITEMGRPJSON[i].GROUPID = groupId;
                    ITEMGRPJSON[i].PARENTGROUPID = parentGroupId;


                }
                generateGroupDropDown(ITEMGRPJSON);
                getItemDocDataApi();
            }

        }
        else {
           // if (flag)
           //     toastr.error(data.status);
           // else
           //     toastr.error(SERVERERROR);
        }

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
    var postData = {
        requestCase: 'getGroupListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(13, 50),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL, postData, callBackGetGroupListing, "Please Wait... Getting the group listing");


}

function getItemDocDataApi() {


    function callBackGetItemDocListing(flag, data)
    {
        if (data.status == SCS && flag) {

            if (data.data)
            {
                var itemDocData = data.data;
                for(var i=0;i<itemDocData.length;i++){
                    var tmp_json ={
                        label : itemDocData[i]['DOC_NAME'],
                        value : itemDocData[i]['PK_DOC_ID']
                    }
                    ITEMDOCJSONARR.push(tmp_json);
                }
                
                registerTagsSearch();
            }

        }
        else {
           // if (flag)
           //     toastr.error(data.status);
           // else
           //     toastr.error(SERVERERROR);
        }

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail); //ebqms user data
    var postData = {
        requestCase: 'getDocumentListingByDocType',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(18, 70),
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        docType:2 //1 = STANDARD,2= ITEM
    }

    commonAjax(COMMONURL, postData, callBackGetItemDocListing, "Please Wait... Getting the group listing");


}

//START FUNCTION FOR GENERATE UOM DROPDOWN
function generateUomDropDown() {


    var parentGrphtml = "<option value='-1'>Select UOM</option>";
    for (var j = 0; j < UOMJSONARR.length; j++) {

        var uomId = UOMJSONARR[j]['PK_UOM_ID'];
        var uomName = UOMJSONARR[j]['UOM_NAME'];
        parentGrphtml += "<option value='" + uomId + "'>" + uomName + "</option>";

    }
    //console.log(parentGrphtml);
    $("#ddItemCategory").html(parentGrphtml); // GENERATE DROPDOWN OF PARENT GROUP ID
    $("#ddItemCategory").selectpicker(); // GENERATE DROPDOWN OF PARENT GROUP ID
}
//END FUNCTION FOR GENERATE UOM DROPDOWN

//START FUNCTION FOR GENERATE PARENT GROUP ID DROPDOWN
function generateGroupDropDown(parentGroupArr, groupIdToIgnore) {
    if (!parentGroupArr)
    {
        parentGroupArr = ITEMGRPJSON;
    }

    var parentGrphtml = "<option value='-1'>Select Group</option>";
    for (var j = 0; j < parentGroupArr.length; j++) {

        var groupId = parentGroupArr[j]['GROUPID'];
        if (groupId != groupIdToIgnore)
        {
            parentGrphtml += "<option value='" + parentGroupArr[j]['GROUPID'] + "'>" + parentGroupArr[j]['GROUPNAME'] + "</option>";
        }
    }
    //console.log(parentGrphtml);
    $("#ddItemGroup").html(parentGrphtml); // GENERATE DROPDOWN OF PARENT GROUP ID
    $("#ddItemGroup").selectpicker(); // GENERATE DROPDOWN OF PARENT GROUP ID
}
//END FUNCTION FOR GENERATE PARENT GROUP ID DROPDOWN

function createItem(){
    
    if( checkAuth(14, 53, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var searchitem = $('#searchitem').attr('itemid');
    var searchitemVal = $('#searchitem').val();

    if( searchitem == "" || searchitem == undefined ){
        toastr.warning('Please Select Item from List');
        $('#searchitem').focus();
        return false;

    }else{

        var index = findIndexByKeyValue( BOMData , 'itemId' , searchitem );

        if( index == '-1' ){

            var tempJson = {
                itemId : searchitem,
                itemName : searchitemVal,
            }

            BOMData.push( tempJson );
        }else{
            toastr.warning('This Item is already Added');
        }
        
        $('#searchitem').val('');
        $('#searchitem').attr('itemid','');

        renderBOM();
    }
}


function renderBOM(){

    var newRow = '';

    for( var i=0; i<BOMData.length; i++ ){
        newRow += '<li class="has-action-left">'+
                        '<a href="javascript:void(0)" class="hidden" itemId="'+ BOMData[i].itemId +'" onclick="removeItem(this)"><i class="ion-android-delete"></i></a>'+
                        '<a href="javascript:void(0)" class="visible visible-2">'+
                            '<div class="list-content margin-l-0">'+
                                '<span class="title">'+ BOMData[i].itemName +'</span>'+
                            '</div>'+
                        '</a>'+
                    '</li>';
    }   
    $('#BOMList').html( newRow );
    // $('#BOMList').niceScroll();
}

function removeItem(context){

    var itemId = $(context).attr('itemId');

    var index = findIndexByKeyValue( BOMData , 'itemId' , itemId );

    if( index != '-1' ){
        BOMData.splice(index,1);
        renderBOM();
    }
}

function createBOM(){

    if( checkAuth(14, 53, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    var bomName = $('#bomName').val();
    var searchitem = $('#searchitem').attr('itemid');
    var searchitemVal = $('#searchitem').val();
    var txtItemValidity = $('#txtItemValidity').val();
    var txtItemDocument = $('#txtItemDocument').val();
    var ddItemCategory = $('#ddItemCategory').val();
    var ddItemGroup = $('#ddItemGroup').val();
    var ddItemGroupName = $('#ddItemGroup option:selected').text();
    var termsCond = $('#termsCond').val();
    var txtItemDocumentId = $('#txtItemDocumentId').val();

    var updateBomId = localStorage.POTGupdateBomId;
    var index = findIndexByKeyValue( BOMListingData , "itemName" , bomName );

    if( bomName == ""){
        toastr.warning('Please Enter BOM Name');
        $('#bomName').focus();
        return false;

    }else if( BOMData.length == 0){
        toastr.warning('Please Enter Item List');
        $('#searchitem').focus();
        return false;

    }else if( txtItemValidity == ""){
        toastr.warning('Please Enter Item Validity');
        $('#txtItemValidity').focus();
        return false;

    // }else if( txtItemDocumentId == ""){
    //     toastr.warning('Please Select Document from List');
    //     $('#txtItemDocument').focus();
    //     return false;

    }else if( ddItemCategory == ""){
        toastr.warning('Please Select UOM');
        $('#ddItemCategory').focus();
        return false;

    }else if( ddItemGroup == "-1"){
        ddItemGroup = "";

    // }else if( termsCond == ""){
    //     toastr.warning('Please Enter Terms & condition');
    //     $('#termsCond').focus();
    //     return false;

    }else {
        
        if( localStorage.POTGupdateBomId != "" && localStorage.POTGupdateBomId != undefined ){
           if( checkAuth(14, 55, 1) == -1 ){
                toastr.error('You are not Authorised for this Activity');
                return false;

            }
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'createUpdateBom',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(14,53),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                bomName: bomName,
                bomGroupId: ddItemGroup,
                itemValidity: ddmmyyToMysql(txtItemValidity),
                itemType: "38",
                uomId: ddItemCategory,
                procurement: "",
                docId: txtItemDocumentId,
                termsCondition: termsCond,
                bomItemList: BOMData,
                itemId: localStorage.POTGupdateBomId,
            }
            localStorage.removeItem( 'updateBomId' );
            commonAjax(FOLLOWUPURL, postData, updateBOMDataCallback,"Please Wait... Getting Dashboard Detail");

        }else{
            if( checkAuth(14, 53, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

            if( index != "-1"){
                toastr.warning('This BOM Name is already exist');
                $('#bomName').focus();
                return false;
            }
            
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'createUpdateBom',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(14,53),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                bomName: bomName,
                bomGroupId: ddItemGroup,
                itemValidity: ddmmyyToMysql(txtItemValidity),
                itemType: "38",
                uomId: ddItemCategory,
                procurement: "",
                docId: txtItemDocumentId,
                termsCondition: termsCond,
                bomItemList: BOMData,
            }

            commonAjax(FOLLOWUPURL, postData, sendBOMDataCallback,"Please Wait... Getting Dashboard Detail");
        }


        function sendBOMDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                console.log(data);

                if(data.data != 'No record found'){

                    console.log(data.data);
                    
                    // var tempJson = {
                    //     bomInnerItem: BOMData,
                    //     itemDocId: txtItemDocumentId,
                    //     itemDocName: txtItemDocumentId,
                    //     itemGroupId: ddItemGroup,
                    //     itemGroupName: ddItemGroupName,
                    //     itemId: data.data.itemId,
                    //     itemName: bomName,
                    //     itemPrice: txtItemPrice,
                    //     itemPriceId: txtItemPrice,
                    //     itemPriceValidity: ddmmyyToMysql(txtItemPriceValidity),
                    //     itemTM: termsCond,
                    //     itemUomId: ddItemCategory,
                    //     itemValidity: ddmmyyToMysql(txtItemValidity),
                    // }

                    BOMListingData.push( data.data[0] );
                    resetData();    
                    renderBOMListing();

                    toastr.success('Your BOM is Created');

                }else{
                    toastr.warning('No record found');
                }

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_ADD_BOM );
                else
                    toastr.error(SERVERERROR);
            }
        }

        function updateBOMDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                console.log(data);

                if(data.data != 'No record found'){

                    console.log(data.data);

                    var index = findIndexByKeyValue( BOMListingData , 'itemId', updateBomId );

                    var tempJson = {
                        bomInnerItem: BOMData,
                        itemDocId: txtItemDocumentId,
                        itemDocName: txtItemDocumentId,
                        itemGroupId: ddItemGroup,
                        itemGroupName: ddItemGroupName,
                        itemId: updateBomId,
                        itemName: bomName,
                        itemTM: termsCond,
                        itemUomId: ddItemCategory,
                        itemValidity: ddmmyyToMysql(txtItemValidity),
                    }

                    BOMListingData[index] = tempJson ;
                    resetData();
                    renderBOMListing();

                    toastr.success('Your BOM is Updated');

                }else{
                    toastr.warning('No record found');
                }

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_BOM );
                else
                    toastr.error(SERVERERROR);
            }
        }
    } 
}


function editBom(context){

    if( checkAuth(14, 55, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var bomId = $(context).attr('item-Id');
    localStorage.POTGupdateBomId = bomId;

    var index = findIndexByKeyValue( BOMListingData, 'itemId' , bomId );

    $('#bomName').val(BOMListingData[index].itemName);
    $('#txtItemDocument').val(BOMListingData[index].itemDocName);
    $('#txtItemDocumentId').val(BOMListingData[index].itemDocId);
    $('#txtItemValidity').val(mysqltoDesiredFormat( BOMListingData[index].itemValidity,'dd-MM-yyyy'));
    $('#ddItemCategory').val(BOMListingData[index].itemUomId);
    $('#ddItemGroup').val(BOMListingData[index].itemGroupId);
    $('#termsCond').val(BOMListingData[index].itemTM);
    $('#ddItemCategory').selectpicker('refresh');
    $('#ddItemGroup').selectpicker('refresh');

    BOMData = BOMListingData[index].bomInnerItem;
    renderBOM();    
    addFocusId( 'searchitem' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function DeleteBom(context){

    if( checkAuth(14, 56, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var bomId = $(context).attr('item-Id');
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        
        
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteBom',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(14,56),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            itemId: bomId,
        }

        commonAjax(FOLLOWUPURL, postData, deleteBomDataCallback,"Please Wait... Getting Dashboard Detail");

        function deleteBomDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                var index = findIndexByKeyValue( BOMListingData , 'itemId', bomId );

                BOMListingData.splice(index,1);
                renderBOMListing();

                toastr.success('Your BOM is Deleted');

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DELETE_BOM );
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

function resetData(){

    $('#bomName').val('');
    $('#searchitem').val('');
    $('#searchitem').attr('itemid','');
    $('#txtItemDocument').val('');
    $('#txtItemValidity').val('');
    $('#termsCond').val('');
    $('#ddItemCategory').val('-1');
    $('#ddItemGroup').val('-1');
    $('#ddItemCategory').selectpicker('refresh');
    $('#ddItemGroup').selectpicker('refresh');
    $('#errorHideDiv').hide();
    $('#errorDiv').html("");
    localStorage.removeItem('POTGupdateBomId');

    BOMData = [];
    renderBOM();
}

function AutoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
            var endDate = mysqltoDesiredFormat( ITEMS[itemIndex].priceValidityEndDate , "dd-MM-yyyy" );
            // if( new Date(ITEMS[itemIndex].priceValidityEndDate +" 11:59 PM") < new Date() ){
            //     toastr.warning(ITEMS[itemIndex].itemName+' Price validity is Over on <b>"'+ endDate +'"</b>, So you need to set it from Client Master page');
            //     $('#searchitem').val('');
            //     return false;
            // }
            
            $('#searchitem').attr('itemId',item.value);
            $('#itemGroupText').val(ITEMS[itemIndex].itemGroupName);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getItemListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(4, 14),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                console.log(data.status);
                // // We good!
                ITEMS = [];
                if (data.status != "No record found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.itemId,
                            name: data.itemName,
                            data: data.itemName,
                            clientId: data.clientId,
                            itemId: data.itemId,
                            itemName: data.itemName,
                            itemPrice: data.itemPrice,
                            itemSubType: data.itemSubType,
                            itemType: data.itemType,
                            orgId: data.orgId,
                            validityEndDate: data.validityEndDate,
                            priceValidityEndDate: data.priceValidityEndDate,
                            itemGroupName: data.itemGroupName
                        }
                        // itemJson.push(tempJson);
                        ITEMS.push(tempJson);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveItemDetail(){

}

function registerTagsSearch(){
    $("#txtItemDocument").autocomplete({
        source: ITEMDOCJSONARR,
        focus: function (event, ui) {
            event.preventDefault();
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            console.log(ui.item.label);
            console.log(ui.item.value);
            
            $("#txtItemDocumentId").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                this.value = '';
                toastr.error('Please select a value from the list'); // or a custom message
            }
        }
    });
}


function openUploadDialog(){
    $('#uploadSalesBomModal').modal('show');
}

//START FUNCTION FOR ADD Activity
function uploadSalesBOM(){
    
    var uploadFileName = $("#attachmentName").val();
    if(!uploadFileName){
        toastr.warning('Please select file for upload activity.');
        $('#attachmentName').click();
        return false;
    }
    $("#addDocument").submit();
}

function assignAttachementEvent(){
    
    $("#addDocument").off().on('submit', (function (e) {
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(3, 10),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            uploadType : "salesBomUpload",
        };
        $("#postDataActUpload").val(JSON.stringify(temp_json));
        var bfsendMsg = "Sending data to server..";

        addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                
                if(data.status == "Success"){
                    
                    console.log(data.data);

                    refreshFile();
                    $('#errorHideDiv').hide();
                    $('#errorDiv').html("");
                    toastr.warning( data.data );
                }else{
//                    toastr.warning(data.status);
//                    refreshFile();
                    toastr.warning(data.status);
                    refreshFile();
                    GBL_ERROR = data.data;
                    renderError(GBL_ERROR);
                    
                }
                addRemoveLoader(0);
                $('#uploadSalesBomModal').modal('hide');
                // $("body").addClass("loaded");
                // $("#loader-wrapper").hide();
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                // $("body").addClass("loaded");
                addRemoveLoader(0); // 1-ADD LOADER || 0-REMOVE LOADER 
                toastr.error(errdata);
                if( jqXHR.responseText != "" ){
                    GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                    renderError(GBL_ERROR);
                }
                refreshFile();
            }
        });
    }));
}

function downloadSalesBom(){
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getItemSample',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(14,53),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(UPLOADURL, postData, downloadCSVDataCallback,"Please Wait... Getting Dashboard Detail");

    function downloadCSVDataCallback(flag,data){

        if (data.status == "Success" && flag) {
            
            GBL_SAMPLEJSON1 = [];

            itemData = data.data.itemMaster;
            var GBL_GROUPJSON = data.data.groupUomMaster;
            
            for( var i=0; i<itemData.length; i++ ){
            
                var tempEXL = {
                    // itemId: itemData[i].ITEMID,
                    itemName: itemData[i].ITEMNAME,
                    // groupId: itemData[i].GROUPID,
                    groupName: itemData[i].GROUPNAME,
                    itemPrice: itemData[i].ITEMPRICE,
                    // uomId: itemData[i].UOMID,
                    uomName: itemData[i].UOMNAME,
                    itemTm: itemData[i].ITEMTM,
                    itemValidityEndDate: itemData[i].ITEMVALIDITYENDDATE,
                    priceValidityEndDate: itemData[i].PRICEVALIDITYENDDATE,
                };
                GBL_SAMPLEJSON1.push(tempEXL);
            }

            csvSave(GBL_SAMPLEJSON1, "ItemSample.csv");
            
            // Sales BOM Samples starts
            var GBL_SAMPLEJSON3 = [];

            var tempEXL = { // HEADER FOR PDF AND CSV
                name : "BomName",
                mobile : "ItemValidityDate",
                email : "Term & Condition",
                address : "ItemIds (Comma Seperated)",
            };
            GBL_SAMPLEJSON3.push(tempEXL);

            csvSave(GBL_SAMPLEJSON3, "salesBOMSample.csv");

            // var GBL_GROUPJSON = [];
            // var tempGrpJson = {
            //     groupId : "Group Id",
            //     groupName : "Group Name",
            //     parentGroupId : "Parent Group Id",
            //     parentGroupName : "Parent Group Name",
            // }
            // GBL_GROUPJSON.push( tempGrpJson );

            // ITEMGRPJSON.forEach(function( record , index ){
            //     var tempGrpJson = {
            //         groupId : record.GROUPID,
            //         groupName : record.GROUPNAME,
            //         parentGroupId : record.PARENTGROUPID,
            //         parentGroupName : record.PARENTGROUPNAME,
            //     }
            //     GBL_GROUPJSON.push( tempGrpJson );
            // });
            csvSave(GBL_GROUPJSON, "groupSample.csv");

        } else {

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ITEM_SAMPLE );
            else
                toastr.error(SERVERERROR);
        }
    }
}

//END FUNCTION FOR REGISTER SAVE BUTTON CLICK FOR SUBMIT FORM DATA
function refreshFile(){

    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 09-MAY-2018 BY VISHAKHA TAK
function initSelfTour(){

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: "#searchitem",
            title: "<b>Search Item</b>",
            content: "Search the item to be added into the BOM!!",
            placement: 'right'
        },
        {
            path: "",
            element: '#createItemBtn',
            title: "<b>Add Item</b>",
            content: "Click and add the item into BOM listing",
            placement: 'right',
        },
        {
            path: "",
            element: "#bomName",
            title: "<b>Enter details</b>",
            content: "Enter BOM name and other mandatory fields",
            placement: 'right',
        },
        {
            path: "",
            element: "#createBOMBtn",
            title: "<b>Add Sales BOM</b>",
            content: "Click and add BOM into the list",
            placement: 'right'
        },
        {
            path: "",
            element: '#uploadBomBtn',
            title: "<b>Upload BOM</b>",
            content: "Instead of creating BOM, you can also file to add new BOM!",
            placement: 'top',   
            onNext: function(tour){
                openUploadDialog();
            }
        },
        {
            path: "",
            element: '#addDocument',
            title: "<b>Add BOM Document</b>",
            content: "Upload Sales BOM document from here !",
            placement: 'right',   
            onShown: function (tour){
                openUploadDialog();
            },
            onPrev: function(tour){
                $('#uploadSalesBomModal').modal('hide');
            },
            onNext: function(tour){
                $('#uploadSalesBomModal').modal('hide');
            }
        },
        {
            path: "",
            element: '#downloadBomBtn',
            title: "<b>Download BOM</b>",
            content: "You can download sample BOM files from here !",
            placement: 'right',
            onPrev: function(tour){
                openUploadDialog();
            },
            onNext:function (tour){
                if( BOMListingData == "" ){
                    tour.end();
                }
            }
        },
        {
            path: "",
            element: '#BOMTable',
            title: "<b>BOM Listing</b>",
            content: "There it is the listing of BOM you added!",
            placement: 'top',            
        },
        {
            path: "",
            element: '#bomListTable',
            title: "<b>BOM Items</b>",
            content: "Click on BOM Name and view list of items in your BOM!",
            placement: 'top',            
        },
    ];
   
    // Instance the tour
    instanceTour();
    tour._options.name = "Sales BOM Tour";
    // tour._options.debug = true;

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM ZEROth ELEMENT TO PREVENT FROM DISPLAYING LAST STEP

    // Start the tour
    tour.start(true);
}