/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : addUserController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var selectedRoleIds = [];
var userRoleData = [];

// GET USER BRANCH, USER LEVEL AND USER ROLE DATA
function getUserData(){

    $('#fullname').focus();

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getUserLevelForUserManagement",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(8,30),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(COMMONURL, postData, getUserAddDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getUserAddDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        
        var branchListing = '';
        
        data.data.branchListing.forEach( function( record , index ){ // SET BRANCH LISTING DATA
            branchListing += '<option value="'+ record.orgId +'">'+ record.orgName +'</option>';
        });

        $('#branchLevel').html(branchListing);
        $('#branchLevel').selectpicker();


        var userLevel = '<option value="-1">--- Please select Level ---</option>';   // SET USER LEVEL DATA
        if( data.data.userLevel != "" && data.data.userLevel != "No record found" ){
            
            data.data.userLevel.forEach( function( record , index ){

                var level = '';
                
                //Level defined for only 3 level
                if( index == 0 ){
                    level = 1; // Last Level
                }else if( index == data.data.userLevel.length - 1 ){
                    level = 0;
                }else{
                    level = 2; // Higher Level
                }

                userLevel += '<option level="'+ level +'" value="'+ record.levelId +'">'+ record.levelName +'</option>';
            });
        }
        $('#userLevel').html(userLevel);
        $('#userLevel').selectpicker();
        $('#userLevel').val('-1');

        userRoleData = data.data.userRole;

        var userRole = '';
        if( userRoleData != 'No record found' ){
 
            data.data.userRole.forEach( function( record , index ){

                var roleId = record.roleId;
                var roleName = record.roleName;

                userRole += '<option value="'+ roleId +'">'+ roleName +'</option>'; // SET USER ROLE DATA
            });

        }else{
            userRole = '';
        }

        $('#roleLevel').html(userRole);
        $('#roleLevel').selectpicker();

        $('#userLevel').change(function(){ // DISPLAY SUPERVISOR FIELD ACCORDING TO USER LEVEL

            var level =  $('option:selected', this).attr('level');
            //0 - higher , 1 -lower

            if( level == "0" ){

                $('.lowerLevel').show();
                $('#supervisorLevel').hide();
                $('#supName').val('');
                $('#supName').attr('itemid','');
                
            }else if( level == "1" ){

                $('.lowerLevel').show();
                $('#supervisorLevel').show();
                $('#supName').val('');
                $('#supName').attr('itemid','');
            
            }else{

                $('#supervisorLevel').show();
                $('.lowerLevel').show();
                $('#supName').val('');
                $('#supName').attr('itemid','');
            }
        })
        
        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        var designationList = roleAuth.data.designation; 
        
        setOption("0", "desLevel", designationList, "--- Select Designation ---"); // SET DESIGNATION DATA
        $('#desLevel').selectpicker();
        
        addFocusId( 'fullname' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER_LEVEL );
            // toastr.error(data.status);
        else
            toastr.error(SERVERERROR);
    }
}


function createUser(){

    var userLoginType = $('#userLoginType').val();
    var fullname = $('#fullname').val().trim();
    var email = $('#email').val().trim(); 
    // var mobile = $('#mobile').val().trim();
    var mobile = getIntlMobileNum("mobile");    // GET MOBILE NUM
    var username = $('#username').val().trim();
    var pwd = $('#pwd').val();
    var cnfpwd = $('#cnfpwd').val();
    var supName = $('#supName').attr('itemid');
    var address = $('#address').val().trim();
    var uLevel = $('#userLevel option:selected').attr('level');
    var uLevelVal = $('#userLevel').val();
    var desLevel = $('#desLevel').val();
    var googleCalKey = $('#googleCalKey').val();
    var bDate = $('#birthdate').val();
    var annDate= $('#anniversaryDate').val();
    var uGender = $('input[type=radio][name="userGender"]').filter(":checked").val();
    var bDt = new Date(ddmmyyToMysql(bDate));
    var annivDt = new Date(ddmmyyToMysql(annDate));
    var department = $('#department').val();
    
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    // CHECK VALIDATIONS
    if( fullname.length == 0 ){
        toastr.warning('Please Enter Full Name');
        $('#fullname').focus();
        return false;
    
    }else if( email.length == 0 ){
        toastr.warning('Please Enter E-Mail');
        $('#email').focus();
        return false;
    
    }else if( !validateEmail(email) ){
        toastr.warning('Please Enter valid E-Mail');
        $('#email').focus();
        return false;
    
    }else if( !checkValidMobile( "mobile" ) ){
    // }else if( mobile.length != MOBILE_LENGTH ){
        toastr.warning('Please Enter Valid Mobile Number');
        $('#mobile').focus();
        return false;
    
    }else if( username.length == 0 ){
        toastr.warning('Please Enter user name (Ex:abc@hostname.com)');
        $('#username').focus();
        return false;
    
    }else if( !validateEmail(username)){
        toastr.warning('Please Enter valid user name (Ex:abc@hostname.com)');
        $('#username').focus();
        return false;
    
    }else if( pwd == "" ){
        toastr.warning('Please Enter Password');
        $('#pwd').focus();
        return false;
    
    }else if( cnfpwd == "" ){
        toastr.warning('Please Enter Confirm Password');
        $('#cnfpwd').focus();
        return false;

    }else if( !validatePassword( pwd ) ){
        toastr.warning('Please Enter Strong Password');
        $('#pwd').focus();
        return false;
    
    }else if( !validatePassword( cnfpwd ) ){
        toastr.warning('Please Enter Strong Confirm Password');
        $('#cnfpwd').focus();
        return false;

    }else if( pwd != cnfpwd ){
        toastr.warning("Password & Confirm Password doesn't Matched");
        $('#pwd').focus();
        return false;
    
    }else if( bDate == "" ){
        toastr.warning('Please Select Birth Date');
        $('#birthdate').focus();
        return false;
    
    }else if( (annDate != "") && ( +annivDt < +bDt ) ){
        toastr.warning('Please Select Proper Anniversary Date');
        $('#anniversaryDate').focus();
        return false;
    
    }else if( address.length == 0 ){
        toastr.warning('Please Enter address');
        $('#address').focus();
        return false;

    }else if( uLevelVal == -1 ){
        toastr.warning('Please Select User Level');
        $('#userLevel').focus();
        return false;
    
    }else if( desLevel == -1 ){
        toastr.warning('Please Select Designation');
        $('#desLevel').focus();
        return false;
    
    }else if( uLevel == "1" && (supName == "" || supName == undefined )){
        toastr.warning('Please Enter Supervisor Name for this Level of User');
        $('#supName').focus();
        return false;

    }else if( uLevel == "1" && $('#roleLevel').val() == null ){ 
        toastr.warning('Please select Roles for this Level of User');
        $('#roleLevel').focus();
        return false;

    }else if( $('#branchLevel').val() == null ){ 
        toastr.warning('Please select Branches for this Level of User');
        $('#branchLevel').focus();
        return false;
    
    }else if( uLevel == "2" &&  (supName == "" || supName == undefined )){

        toastr.warning('Please Enter Supervisor Name for this Level of User');
        $('#supName').focus();
        return false;

    }else if( googleCalKey != "" && !validateEmail(googleCalKey)){
        toastr.warning('Please Enter valid calendar Email (Ex:abc@hostname.com)');
        $('#googleCalKey').focus();
        return false;
        
    }else if( (department == null || department == undefined || department == "") ){
        toastr.warning("Please select department");
        addFocusId('department');
        return false;
        
    }else {

        // DEPARTMENT JSON - COMMA SEPERATED
        var departmentList = "";
        if( department != null && department != "" ){
            department.forEach(function(record,index) {
                if( departmentList == "" ){
                    departmentList += record;
                }else{
                    departmentList += ","+record;
                }
            });
        }
        
        // CREATE USER DATA JSON
        var userdata = {
            userName : username,
            fullName : fullname,
            userType : 1,
            // USERLOGINTYPE : userLoginType,
            address : address,
            contactNo : mobile,
            email : email,
            dashboard : 'V1.0',
            levelId : uLevelVal,
            // empCode : empCode,
            passWord : pwd,
            supervisorId : supName,
			birthdate : ( (bDate!="")? ddmmyyToMysql(bDate) : "" ),
            designationId : desLevel,
			anniversarydate : ( (annDate!="")? ddmmyyToMysql(annDate) : "" ),
            gender : uGender,
            googleCalKey : googleCalKey,
            department : departmentList
        }

        var role = "";

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: "createUser",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(8,29),
            userdata: userdata,
            role: role,
            branch: $('#branchLevel').val(),
            roleId: $('#roleLevel').val(),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        };

        console.log(postData);
        // return false;
        commonAjax(COMMONURL, postData, createUserDataCallback,"Please Wait... Getting Dashboard Detail");
    }
}


function createUserDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
        toastr.success('User created Successfully');
        resetActiviytData();
        window.location.href = 'usermanaguser.html';

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_USER_FAIL );
        else
            toastr.error(SERVERERROR);
    }
}

// PROVIDE SEARCH FOR SUPERVISOR
function autoCompleteForSupervisor(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#supName').attr('itemId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                var levelVal = $('#userLevel').val();
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getSuperVisorListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8,30),
                    keyWord: query,
                    selectedLevelId: levelVal
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.userId,
                            name: data.fullName,
                            data: data.fullNames
                        }
                        ITEMS.push(tempJson);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveSuperWiserData(){

}
