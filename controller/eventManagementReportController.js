    /*
 *
 * Created By :  Kavita Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 08-08-2018.
 * File : terminationReportController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_EVENT_ID = '';
var GBL_REPORT_DATA = [];
var GBL_EXCELJSON = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    autoCompleteForStudentId('studentName',saveStudent);
    autoCompleteForEvent('eventName',saveStudent);
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
    getEventManagementReportList(); // GET EVENTS LISTING DATA
}

// GET EVENTS LISTING DATA
function getEventManagementReportList(){

    var studentId = $('#studentName').attr('studId');
    var monthFrom = $('#monthFrom').val();
    var monthTo = $('#monthTo').val();
    var status = $('#status').val();
    var eventName = $('#eventName').attr('itemId');
    
    if( monthFrom != "" && monthTo != "" ){

        var stMonth = monthFrom.split('-').reverse().join('-');
        var enMonth = monthTo.split('-').reverse().join('-');
        var dateStartDate = new Date(ddmmyyToMysql(stMonth));
        var dateEndDate = new Date(ddmmyyToMysql(enMonth));

        //START END DATE VALIDATION
        if (+dateEndDate < +dateStartDate) { 
            toastr.warning("End month cannot be less than the Start month");
            addFocusId('monthTo');
            return false;

        }
    }
    if( $('#eventName').val() == "" ){
        eventName = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'eventMangementReport', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(26,105),
    	//studentId : studentId,
    	fromMonth : monthFrom,
    	toMonth : monthTo,
    	eventName : eventName,
    	status : status,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRequestListCallback,"Please wait...Adding your item.");

    function getRequestListCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_REPORT_DATA = data.data;
            renderEventManagementReportList(GBL_REPORT_DATA); 
            $('.Sidenav').removeClass('open');          
            // resetFilters();

        }else{

            GBL_REPORT_DATA = [];
            GBL_EXCELJSON = [];
            renderEventManagementReportList(GBL_REPORT_DATA); 
            // resetFilters();
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_FILTER );
            else
                toastr.error(SERVERERROR);
        }
        $('.Sidenav').removeClass('open');    
    }

}

// RENDER ITEM LISTING DATA
function renderEventManagementReportList(eventData){
    
    
    var thead = '<table class="table charge-posting-table display table-bordered  dataTable" id="enrollmentViewRepoTbl">'+
                '<thead>'+
                    '<tr>'+
                        '<th>#</th>'+
                        '<th>Start Date</th>'+
                        '<th>End Date</th>'+
                        '<th>Event Name</th>'+
                        '<th>Total Capacity</th>'+
                        '<th>Booked Seats</th>'+
                        '<th>Unbooked Seats</th>'+
        			    '<th>Status</th>'+
        		    '</tr>'+
                '</thead>'+
                '<tbody id="tbodyItemListin">';

    var tEnd = '</tbody></table>';

    var tempJson = {
        srNo : "#",
        startDate : "Start Date",
        endDate : "End Date",
        eventName : "Event Name",
        totalCapacity : "Total Capacity",
        bookedCapacity : "Booked Seats",
        unbookedCapacity : "Unbooked Seats",
        status : "Status",
    }
    GBL_CSVJSON.push(tempJson);

    var innerRow = '';
    if( eventData.length != 0 && eventData != "No record found" ){

        eventData.forEach(function( record , index ){
            
            var tempJson = {
                srNo : (index+1),
                startDate : (record.startDate != "0000-00-00" && record.startDate != "1970-01-01" && record.startDate != "" ? record.startDate : "" ),
                endDate : (record.endDate != "0000-00-00" && record.endDate != "1970-01-01" && record.endDate != "" ? record.endDate : "" ),
                eventName : record.eventName,
                totalCapacity : record.totalCapacity,
                bookedCapacity : record.bookedCapacity,
                unbookedCapacity : record.unbookedCapacity,
                status : record.status,
            }
            GBL_CSVJSON.push(tempJson);

            innerRow += '<tr>'+
                            '<td>'+ (index+1) +'</td>'+
                            '<td>'+ (record.startDate != "0000-00-00" && record.startDate != "1970-01-01" && record.startDate != "" ? record.startDate : "" ) +'</td>'+
                            '<td>'+ (record.endDate != "0000-00-00" && record.endDate != "1970-01-01" && record.endDate != "" ? record.endDate : "" ) +'</td>'+
                            '<td>'+ record.eventName +'</td>'+
                            '<td>'+ record.totalCapacity +'</td>'+
                            '<td>'+ record.bookedCapacity +'</td>'+
                            '<td>'+ record.unbookedCapacity +'</td>'+
			                '<td>'+ record.status +'</td>'+
                        '</tr>';
                
        });

    }
    $('#divItemListing').html( thead + innerRow + tEnd);
    GBL_EXCELJSON = thead + innerRow + tEnd;
    $('#enrollmentViewRepoTbl').DataTable();

}

// RESET FILTER
function resetFilters(){

    $('#monthFrom').val('');
    $('#monthTo').val('');
    $('#status').val('').selectpicker('refresh');
    $('#eventName').val('');
    $('#eventName').attr('itemId','');
    $("#monthFrom,#monthTo").datepicker('remove');
    $("#monthFrom,#monthTo").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        autoclose:true
    });
   
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    renderEventManagementReportList(GBL_REPORT_DATA); // RENDER ORIGINAL WITHOUT FILTER DATA

}


function saveStudent(){

}

function setMonthTo(context){

    var monthFrom = $(context).val();
    
    $('#monthTo').datepicker('remove');
    $('#monthTo').datepicker({
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months",
        startDate: monthFrom,
        autoclose:true
    });

}

// PROVIDE SEARCH FOR EVENT NAME
function autoCompleteForEvent(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            var index = findIndexByKeyValue( ITEMS , 'id' , item.value);

            if( index != -1 ){
                setTimeout(function() {
                    $('#'+id).val( ITEMS[index].itemName );
                    $('#'+id).attr( 'itemId' , ITEMS[index].id );
                },10);
            }
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "eventSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(21,86),
                    keyword: query,
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                $('.typeahead.dropdown-menu').hide();
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (data) {
                            
                        if( data.studId != "-1" ){

                            var tempJson = {
                                id: data.itemId,
                                name: data.itemName + ' - ' + data.eventNo,
                                data: data.itemName,
                                itemName : data.itemName,
                                eventNo : data.eventNo,
                            }
                            ITEMS.push(tempJson);

                           
                        }
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave(GBL_EXCELJSON, "Event Report.xls");        
    }

}

// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Event_Report.pdf",20);
    }

}
