/*
 *
 * Created By : Sachin Jani.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : April 28 2016.
 * File : ActivityDashboard.
 * File Type : .js.
 * Project : activityMgmt
 *
 * */

/* we are using OO-javaScript for this page  
 * our main object will be ""activityDetailsPage" 
 * all the functions will declared inside it.
 * 
 * */

var assignedToId = '';
var actStatus = '';
var pendingStatus = '';
var InProcessStatus = '';
var closedStatus = '';
var actArr = [];
var actProjId = "";
var inqProjId = "";
var actClientInqName = "";
var GBL_ACT_DATA = [];

function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}


function activityDetailsPage() {


    /* init globals constants from gbl_ActivityAppGlobals  START */
    //global var related globals.
    // we're defining the globals used by this module here from "gbl_ActivityAppGlobals" ..
    // .. so that we can keep the track of globals used by this module.
    //Static Globals
    var SCS = gbl_ActivityAppGlobals.SCS;
    var FAIL = gbl_ActivityAppGlobals.FAIL;
    var actUrl = gbl_ActivityAppGlobals.URLV;
    var ACT_MODE_PHONE = gbl_ActivityAppGlobals.ACT_MODE_PHONE;
    var ACT_MODE_EMAIL = gbl_ActivityAppGlobals.ACT_MODE_EMAIL;
    var ACT_MODE_SMS = gbl_ActivityAppGlobals.ACT_MODE_SMS;
    var ACT_MODE_PERSONALMEET = gbl_ActivityAppGlobals.ACT_MODE_PERSONALMEET;
    var ACT_STATUS_PENDING = gbl_ActivityAppGlobals.ACT_STATUS_PENDING;
    var ACT_STATUS_INPROCESS = gbl_ActivityAppGlobals.ACT_STATUS_INPROCESS;
    var ACT_STATUS_CLOSED = gbl_ActivityAppGlobals.ACT_STATUS_CLOSED;

    /* init globals constants from gbl_ActivityAppGlobals  END */

    /* this module related globals START */
    var mGbl_actDetDataArr = [];
    var mGbl_actDetActivityId = localStorage.POTGactivityAppActivityId;
    var thisModule = this;
    /* this module related globals END */


    /* use the functions of moduleForCreateActivity by creating an instance of it. START */
    //i've used this here to encapsulate this in Activity details module..
    var mdlCreateAct = new moduleForCreateActivity();
    var mdlUpdateAct = new moduleToUpdateActivity();
    
    /* use the functions of moduleForCreateActivity by creating an instance of it. END */
    
    

    /* public functions ----------------START--------------- */

    /* register all needed events for activity details page(this page) START */
    this.registerActDetailsPageEvents = function()
    {
        mdlCreateAct.initCreateActModule(callBackCloseAndCreateActivity , "detail");
        
        var paramActDataArr = mGbl_actDetDataArr.data.actData[0];
        var paramsJson ={
            paramActDataArr:paramActDataArr,
            paramUpdateActCallBack:callBackUpdateActivity,
            paramAssignActCallBack:callBackAssignActivity,
        };

        mdlUpdateAct.initUpdateActModule(paramsJson);
        
        
    }
    /* register all needed events for activity details page(this page) END */
    
    /* make 'create activity' submit call from moduleForCreateActivity START */
    this.closeAndCreateActivity = function ()
    {
        mdlCreateAct.submitCreateActivity(mGbl_actDetActivityId); //params : refId,callBack

    }

    /* make 'create activity' submit call from moduleForCreateActivity END */


    /* function to console this module related globals START */
    this.consoleActDetMglobals = function ()
    {
        console.log(mGbl_actDetDataArr);
        console.log("Activity Id:" + mGbl_actDetActivityId);
        console.log("FAIL:" + FAIL);
        console.log("gbl_ActivityAppGlobals.FAIL:" + gbl_ActivityAppGlobals.FAIL);
        //console.log("abc of commonModule " + commonControllerModule.abc );
    }
    /* function to console this module related globals END */

    /* function to make ajax call and get activity data START */
    this.getActDetailsData = function ()
    {
        if(!mGbl_actDetActivityId)
        {
            console.error("localStorage.POTGactivityAppActivityId - Not found");
            toastr.error("Required Activity data not found");
            // alert("Required Activity data not found");
            navigateToActivityAppDashBoard();
            return false;
        }
        
        
        
        var postData = {
            requestCase: "getActivityDetails",
            actId: mGbl_actDetActivityId,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,6),
        };



        function callBackGetActDetails(flag, results) {
            if (results.status == SCS) {
                //log(msg);
                //localStorage.POTGtempActData = JSON.stringify(results);
                GBL_ACT_DATA = results.data.actData[0];
                mGbl_actDetDataArr = results;
                actInqId = results.data.actData[0].FK_INQ_ID;
                actClientInqId = results.data.actData[0].C_INQ_ID;
                actClientInqName = results.data.actData[0].INQ_NAME;
                actCustId = results.data.actData[0].FK_CUST_ID;
                actContId = results.data.actData[0].FK_CONTACT_ID;
                inqProjId = results.data.actData[0].ACT_PROJ_ID;
                var dataArr = mGbl_actDetDataArr.data.actData[0];
                var remarkDataArr = mGbl_actDetDataArr.data.remark;
                //mGbl_actDetActivityId = dataArr.PK_ACT_ID;
                //return false;
                renderActivityDetailsData(dataArr);
                if (remarkDataArr) {
                    renderRemarks(remarkDataArr);
                }
                
                thisModule.registerActDetailsPageEvents();

                renderAllocationList( results.data['allocationHistory'] );

            } else {

                console.log("====== FAIL ======");
                if (flag) {
                    // toastr.error( GBL_ERR_NO_ACT_DATA );
                    displayAPIErrorMsg( results.status , GBL_ERR_NO_ACT_DATA );
                    // toastr.error(results.status);
                } else {
                    toastr.error("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTACTDATA;
        commonAjax(actUrl, postData, callBackGetActDetails, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackGetActDetails, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to make ajax call and get activity data END */

    /* function to insert new remark START */
    this.insertCommentInAct = function (remark)
    {   
        var postData = {
            requestCase: "insertCommentInAct",
            actId: mGbl_actDetActivityId,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            remark: remark,
            remarkIndexId: "0",
            orgId: checkAuth(2,7),
        };



        function callBackInsertCommentInAct(flag, results) {
            if (results.status == SCS) {

                thisModule.getActDetailsData();


            } else {

                console.log("====== FAIL ======");
                if (flag) {
                    console.log(results.status);
                    displayAPIErrorMsg( results.status , GBL_ERR_REMARK_INSERT_ACT_FAIL );
                    // toastr.error(results.status);
                } else {
                    console.log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTADDRMRK;
        commonAjax(actUrl, postData, callBackInsertCommentInAct, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackInsertCommentInAct, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to insert new remark END */



    /* show add remark modal START */
    this.showAddRemarkModal = function ()
    {   
        $('#rmbox').slideToggle('fast');
        $('#remarkbox').focus();
        $('#remarkbox').val('');
        event.stopPropagation();
    }

    /* show add remark modal END */


    /* save new remark function START  */
    this.saveNewRemark = function ()
    {
        var remarkVal = $('#remarkbox').val();
        if (!remarkVal)
        {
            toastr.error("Please Insert the Remark");
            $('#remarkbox').focus();
            return false;
        
        }
        
        thisModule.insertCommentInAct(remarkVal);
        $('#remarkbox').val("");
        $('#remarkfollowup').modal('hide');
    }

    /* save new remark function END  */


    /* function to start pending activity START */
    this.startActivity = function ()
    {
        if( checkAuth(2, 7) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;
        }
        var dataArr = mGbl_actDetDataArr.data.actData[0];
        var activityStatus = dataArr.ACT_STATUS;
        if (activityStatus != ACT_STATUS_PENDING)
        {
            toastr.error("Something is wrong. This Activity is In-Process or Closed");
            reloadWin();
            return false;
        }


        var postData = {
            requestCase: "startActivity",
            actId: mGbl_actDetActivityId,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            orgId: checkAuth(2,7),
        };



        function callBackStartActivity(flag, results) {
            if (results.status == SCS) {

                thisModule.getActDetailsData();


            } else {

                console.log("====== FAIL ======");
                displayAPIErrorMsg( results.status , GBL_ERR_START_ACT_FAIL );
                if (flag) {
                    console.log(results.status);
                } else {
                    console.log("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTADDRMRK;
        commonAjax(actUrl, postData, callBackStartActivity, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackStartActivity, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to start pending activity END */
    
    /* function to start pending activity START */
    this.closeActivity = function ()
    {
        var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
        var userId = userDetail.data[0].PK_USER_ID

        if( assignedToId != userId ){
            toastr.error("You are not Authorised to perform this Activity");
            return false;
        }

        var dataArr = mGbl_actDetDataArr.data.actData[0];
        var activityStatus = dataArr.ACT_STATUS;
        if (activityStatus != ACT_STATUS_INPROCESS)
        {
            toastr.error("Something is wrong. This Activity is either Pending or Closed");
            reloadWin();
            return false;
        }

        var closeActReasonId = $('#closeActReason').val();
        var closeActReasonText = $('#closeActReasonTxt').val();
        
        if( closeActReasonId == "-1" ){
            toastr.warning('Please Select Outcome');
            $('#closeActReason').focus();
            return false;

        }else if( closeActReasonText == "" ){
            toastr.warning('Please Enter Activity Close Text');
            $('#closeActReasonTxt').focus();
            return false;
        }

        var postData = {
            requestCase: "closeActivity",
            actId: mGbl_actDetActivityId,
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            closeActReasonId: closeActReasonId,
            closeActReasonText: closeActReasonText,
            orgId: checkAuth(2,7),
        };



        function callBackCloseActivity(flag, results) {
            if (results.status == SCS) {

                thisModule.getActDetailsData();
                $('#closeActRemarkModal').modal('hide');


            } else {

                console.log("====== FAIL ======");
                displayAPIErrorMsg( results.status , GBL_ERR_CLOSE_ACT_FAIL );
                if (flag) {
                    // toastr.error(results.status);
                } else {
                    toastr.error("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTADDRMRK;
        commonAjax(actUrl, postData, callBackCloseActivity, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackCloseActivity, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to start pending activity END */


    /* public functions ----------------END--------------- */



    /* function to update  remark START */
    var updateCommentInAct = function (remarkIndexId, remark)
    {
        var postData = {
            requestCase: "updateCommentInAct",
            userId: localStorage.indeCampusActivityAppUserId,
            clientId: localStorage.indeCampusActivityAppClientId,
            actId: mGbl_actDetActivityId,
            remark: remark,
            remarkIndexId: remarkIndexId,
            orgId: checkAuth(2,7),
        };



        function callBackUpdateCommentInAct(flag, results) {
            if (results.status == SCS) {
                thisModule.getActDetailsData();

            } else {

                console.log("====== FAIL ======");
                displayAPIErrorMsg( results.status , GBL_ERR_REMARK_UPDATE_ACT_FAIL );
                if (flag) {
                    // toastr.error(results.status);
                } else {
                    toastr.error("====== No Records Found ======");
                }
            }
        }

        /* ajax call start  */

        var actAjaxMsg = gbl_ActivityAppGlobals.AJAXMSG_GTADDRMRK;
        commonAjax(actUrl, postData, callBackUpdateCommentInAct, actAjaxMsg);
        // commonAjaxActiivity(actUrl, postData, callBackUpdateCommentInAct, actAjaxMsg);
        /* ajax call end  */
    }
    /* function to update remark END */


    /* callBack function of closeAndCreateActivity START */
    function callBackCloseAndCreateActivity()
    {
        console.log("callBackCloseAndCreateActivity function fired");
        thisModule.getActDetailsData();
        
    }
    /* callBack function of closeAndCreateActivity END */
    
    /* callBack function of submitEditedActivity START */
    function callBackUpdateActivity()
    {
        console.log("callBackUpdateActivity function fired");
        thisModule.getActDetailsData();
        
    }
    /* callBack function of submitEditedActivity END */
    
    /* callBack function of assignActivity START */
    function callBackAssignActivity()
    {
        console.log("callBackAssignActivity function fired");
        thisModule.getActDetailsData();
        
    }
    /* callBack function of assignActivity END */


    /* function to render the activity detailds data START */
    function renderActivityDetailsData(dataArr)
    {
        /* data init START */
        var activityId = dataArr.PK_ACT_ID;
        var activityTitle = dataArr.ACT_TITLE;
        var activityMode = dataArr.ACT_MODE;
        var activityModeName = dataArr.ACTMODE;
        var activityAssignedDate = dataArr.CREATEDTIME;
        var activityDueDate = dataArr.DUE_DATE;
        var activityCreatedBy = dataArr.CREATEDBY;
        var activityAssignedBy = dataArr.ASSIGNBY;
        var activityAssignedTo = dataArr.ASSIGNEDTO;
        var activityAssignedToId = dataArr.ASSIGN_TO;
        var activityStartTime = dataArr.STARTTIME;
        var activityEndTime = dataArr.ENDTTIME;
        var activityLastUpdate = dataArr.CHANGEDTIME;
        var activityAttachment = dataArr.ACT_ATTACHMENT;
        var activityDesc = dataArr.ACT_DESC;
        var activityStatus = dataArr.ACT_STATUS;
        var activityDelayedFlag = dataArr.DELAYED_FLAG;
        var activityCustomerName = dataArr.CUTOMERDETAIL;
        var activityCustomerMob = dataArr.PHONE_NO;
        var activityContCustomerName = dataArr.CONTACTPERNAME;
        var activityContCustomerMob = dataArr.CONTACTPERNO;
        var activityInqId = dataArr.FK_INQ_ID;
        var activityClientInqId = dataArr.C_INQ_ID;
        var activityCustId = dataArr.FK_CUST_ID;
        var lastActDateHtml = ( dataArr.LASTACTIVTYDATE != "" && dataArr.LASTACTIVTYDATE != "--" ) ? mysqltoDesiredFormat(dataArr.LASTACTIVTYDATE,'dd-MM-yyyy') : "--";
        var actDueTime = dataArr.DUE_TIME;
        var activityTypeName = dataArr.ACT_TYPE_NAME;
        localStorage.POTGactType= dataArr.ACT_TYPE;
        
        var activityEndDate = dataArr.END_DATE;
        var actEndTime = dataArr.END_TIME;
        
        actArr = dataArr;

        /* data init END */
        
        /*
        if (activityAssignedDate && activityAssignedDate != "null" && activityAssignedDate != "undefined")
        {
            activityAssignedDate = new Date(activityAssignedDate).format('dd-MM-yyyy');
        }
        */
       
        if (activityDueDate && activityDueDate != "null" && activityDueDate != "undefined" && activityDueDate != "0000-00-00")
        {
            activityDueDate = new Date(activityDueDate).format('dd-MM-yyyy');
        }

        if (activityEndDate && activityEndDate != "null" && activityEndDate != "undefined" && activityEndDate != "0000-00-00")
        {
            activityEndDate = new Date(activityEndDate).format('dd-MM-yyyy');
        }

        /* activity mode icon init START */
        var activityModeIcon;

        if (activityMode == ACT_MODE_PHONE)
        {
            activityModeIcon = 'fa fa-phone';
        }
        else if (activityMode == ACT_MODE_EMAIL)
        {
            activityModeIcon = 'fa fa-envelope';
        }
        else if (activityMode == ACT_MODE_SMS)
        {
            activityModeIcon = 'glyphicon glyphicon-send';
        }
        else if (activityMode == ACT_MODE_PERSONALMEET)
        {
            activityModeIcon = 'fa fa-user';
        }
        /* activity mode icon init END */

        var actModeHtml = '<i class="' + activityModeIcon + ' green-text"></i> ' + activityModeName + '';

        var imgAssign = ( dataArr.ASSIGNBYIMAGE == "" || dataArr.ASSIGNBYIMAGE == httpHead + "//"+FINALPATH+"/indeCampusProject/attachment/" ) ? "http://"+FINALPATH+"/assets/globals/img/avtar1.png" : dataArr.ASSIGNBYIMAGE ;
        var imgCreated = ( dataArr.CRAATEDBYIMAGE == "" || dataArr.CRAATEDBYIMAGE == httpHead + "//"+FINALPATH+"/indeCampusProject/attachment/" ) ? "http://"+FINALPATH+"/assets/globals/img/avtar1.png" : dataArr.CRAATEDBYIMAGE ;
        var imgAssignTo = ( dataArr.ASSIGNTOIMAGE == "" || dataArr.ASSIGNTOIMAGE == httpHead + "//"+FINALPATH+"/indeCampusProject/attachment/" ) ? "http://"+FINALPATH+"/assets/globals/img/avtar1.png" : dataArr.ASSIGNTOIMAGE ;
        
        /* data render part START */
        $('#actDetTitle').text(activityTitle);
        $('#actDetTitle').text(activityTitle);
        $('#actDetAssignedBy').html('<span class="useraImageBody"><img class="userProfile" id="" src="'+ imgAssign +'"></span>&nbsp;&nbsp;'+ activityAssignedBy);
        $('#actDetCreatedBy').html('<span class="useraImageBody"><img class="userProfile" id="" src="'+ imgCreated +'"></span>&nbsp;&nbsp;'+activityCreatedBy);
        $('#actDetAssignedToUser').html('<span class="useraImageBody"><img class="userProfile" id="" src="'+ imgAssignTo +'"></span>&nbsp;&nbsp;'+activityAssignedTo);
        $('#actDueDtTm').html('<i class="fa fa-clock-o green-text"></i>  '+ activityDueDate + ' ' + actDueTime+' <b>To</b> <i class="fa fa-clock-o green-text"></i> '+activityEndDate + ' ' + actEndTime);
        $('#actDetAssignedTo').html('<a href="javascript:void(0)" custid="'+ dataArr.FK_CUST_ID +'" onclick="navigateToCustomerDetailPage(this)"><i class="fa fa-user"></i> '+activityCustomerName+' - '+'<i class="fa fa-mobile"></i> '+activityCustomerMob);
        $('#actDetAssignedToCont').html('<i class="fa fa-briefcase"></i> '+activityContCustomerName+' - '+'<i class="fa fa-phone"></i> '+activityContCustomerMob+'</a>');

        if( activityCustomerName != "--" ){
            $('#custDetDiv').show();
        }

        if( activityAssignedBy != "" ){
            $('#assignByTr').show();
        }else{
            $('#createByTr').show();
        }

        // START - CHANGE MADE BY DEEPAK PATIL ON 07-07-2016
        var inqDetailHtml = "";
        var inqDetailHtmlTemp = $('#inqDetail').html();
        var inqTypeName = dataArr.ACT_REFERENCE_TYPE;
        
        var type = "";
        var inqTypeNameTxt = "INQUIRY :";
        if( (inqTypeName).toLowerCase() == "ord" ){
            type = "2";
            inqTypeNameTxt = "ORDER :";
        }else if( (inqTypeName).toLowerCase() == "service" ){
            type = "3";
            inqTypeNameTxt = "SERVICE :";
        }else if( (inqTypeName).toLowerCase() == "project" ){
            type = "4";
            inqTypeNameTxt = "TASK :"
            activityClientInqId = dataArr.PROJECT_TITLE;

        }else if( (inqTypeName).toLowerCase() == "campaign" ){
            type = "5";
            inqTypeNameTxt = "CAMPAIGN :"
            activityClientInqId = dataArr.CAMPAIGN_TITLE;

        }else if( (inqTypeName).toLowerCase() == "leadinq" ){
            type = "6";
            inqTypeNameTxt = "Lead :"
        }

        if(activityInqId != 0){
            inqDetailHtml += inqDetailHtmlTemp.replace("*|--|*","("+ inqTypeNameTxt +" <a href='javascript:void(0)' custid='"+activityCustId+"' inqid='"+activityInqId+"' onclick='navigateToInquiryDetail(this , "+ type +")'>"+activityClientInqId+"</a>)");
        }else{
            inqDetailHtml += inqDetailHtmlTemp.replace("*|--|*","");
        }
        
        $("#inqDetail").html(inqDetailHtml);
        // END - CHANGE MADE BY DEEPAK PATIL ON 07-07-2016
        $('#actDetAssignedOn').text(activityAssignedDate);
        $('#spanHeaderActivityDate').text(activityDueDate);

        var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
        var userId = userDetail.data[0].PK_USER_ID
        assignedToId = activityAssignedToId;
        actStatus = activityStatus;
        pendingStatus = ACT_STATUS_PENDING;
        InProcessStatus = ACT_STATUS_INPROCESS;
        closedStatus = ACT_STATUS_CLOSED;

        $("#actType").html(activityTypeName);
        
        var attchFName = [];
        if( activityAttachment.length != 0 ){
            attchFName = activityAttachment.split(',');
        }

        var attchFiles = '<ul>'; 
        attchFName.forEach( function( record , index ){ 
            var fileNm = basenameWithExt( record );
            attchFiles += '<li><a href="javascript:void(0)" class="attachList" data-link="'+ record +'" onclick="openLinkInNewTab(this);"><span class=""> '+ fileNm +'</span></a></li>';
        });
        attchFiles += '</ul>';

        $('#actDetAttachment').html( attchFiles );
        $('#actDetStartTime').html('<i class="fa fa-clock-o green-text"></i> '+activityDueDate + ' ' + ((actDueTime != '0' || actDueTime != '' ) ? actDueTime : "" ) );
        $('#actDetEndTime').html('<i class="fa fa-clock-o green-text"></i> '+ ((activityEndDate == "0000-00-00" ) ? "--" : activityEndDate) + ' ' + actEndTime);
        $('#actDetLastUpdate').html(activityLastUpdate);
        $('#pActivityDesc').html(activityDesc);

        $('#actDetMode').html(actModeHtml);
        $('#lastActDate').html(lastActDateHtml);
        $('#actDetAssignedToTxt').html('<span class="useraImageBody"><img class="userProfile" id="" src="'+ imgAssignTo +'"></span>&nbsp;&nbsp;'+activityAssignedTo);
        $('#ddActAssignedToForEditAct').val(activityAssignedToId);
        // $('#ddActAssignedToForEditAct').val(activityAssignedToId).selectpicker('refresh');
        $('#ddActAssignedToForEditAct').attr('data-Assignto',activityAssignedToId);
        // $('#ddActAssignedToForEditAct').attr('data-Assignto',activityAssignedToId).selectpicker('refresh');
        /* data render part END */

        $('#unRegCustNameTxt').html( dataArr.UNREG_CUST_NAME );
        $('#unRegCustMobileTxt').html( dataArr.UNREG_CUST_MOBILE );

        if( dataArr.UNREG_CUST_NAME == "" || dataArr.UNREG_CUST_MOBILE == "" ){
            $('.unRegCustType').remove();
        }

        if( dataArr.PROJECTNAME != undefined && dataArr.PROJECTNAME != "" ){
            $('.projDiv').show();
            $('#projNameTxt').html( dataArr.PROJECTNAME );
        }

        /* activity status init START */

        if (activityStatus == ACT_STATUS_PENDING)
        {
            $('#btnStartActivity').show();
            $('#btnCloseActivity').hide();
            $('#btnAddRemak').hide();
            $('#btnCloseAndCreateActivity').hide();

            $('#activityProgressStatusBar').css('width', '0%');
            $('#activityProgressStatusBar .sr-onlyInq').html( '0%' );
            $('#activityProgressStatusName').text('Pending');


        }
        else if (activityStatus == ACT_STATUS_INPROCESS)
        {
            $('#btnStartActivity').hide();
            $('#btnCloseActivity').show();
            $('#btnAddRemak').show();
            $('#btnCloseAndCreateActivity').show();

            $('#assignbtn').attr('disabled',true);

            $('#activityProgressStatusBar').css('width', '50%');
            $('#activityProgressStatusBar .sr-onlyInq').html( '50%' );
            $('#activityProgressStatusName').text('In-Process');

            /* DELAYED_FLAG = 1 then apply red color to progressBar No matter what the status is  START */
            if (activityDelayedFlag == "1")
            {
                $('#activityProgressStatusBar').addClass('progress-bar-deadline');
            }
            else {

                $('#activityProgressStatusBar').addClass('progress-bar-success');
            }
            /* DELAYED_FLAG = 1 then apply red color to progressBar No matter what the status is  END */

        }
        else if (activityStatus == ACT_STATUS_CLOSED)
        {
            $('#btnStartActivity').hide();
            $('#btnCloseActivity').hide();
            $('#actAttach').attr('disabled',true);
            $('#btnCloseAndCreateActivity').hide();
            $('#btnAddRemak').hide();

            $('#assignbtn').attr('disabled',true);
            $('#btnEditAct').attr('disabled',true);
            $('#actAttach').attr('disabled',true);
            
            $('#activityProgressStatusBar').css('width', '100%');
            $('#activityProgressStatusBar .sr-onlyInq').html( '100%' );
            $('#activityProgressStatusName').text('Closed');

            /* DELAYED_FLAG = 1 then apply red color to progressBar No matter what the status is  START */
            if (activityDelayedFlag == "1")
            {
                $('#activityProgressStatusBar').addClass('progress-bar-deadline');
            }
            else {

                $('#activityProgressStatusBar').addClass('progress-bar-deadline');
            }
            /* DELAYED_FLAG = 1 then apply red color to progressBar No matter what the status is  END */

        }

         if( activityAssignedToId != userId ){
            $('#btnStartActivity').attr('disabled',true);
            $('#btnEditAct').attr('disabled',true);
            $('#assignbtn').attr('disabled',true);
            $('#actAttach').attr('disabled',true);
            
            $('#btnCloseAndCreateActivity').hide();
            $('#btnCloseActivity').hide();
        }


        
        /* activity status init END */

    }

    /* function to render the activity detailds data END */


    /* funtion to render remarks START */
    function renderRemarks(dataArr)
    {
        var innerHtml = '';

        for (var i = 0; i < dataArr.length; i++)
        {
            var remarkCreatedBy = dataArr[i].createdByName;
            var editRemarkFlag = dataArr[i].editFlag;
            var remarkCreatedByDate_Date = dataArr[i].createdByDate_Date;
            var remarkCreatedByDate_Time = dataArr[i].createdByDate_Time;
            var remarkVal = dataArr[i].remark;

            if (remarkCreatedByDate_Date && remarkCreatedByDate_Date != "null" && remarkCreatedByDate_Date != "undefined")
            {
                // remarkCreatedByDate = new Date(remarkCreatedByDate).format('dd-MM-yyyy');
                remarkCreatedByDate_Date = remarkCreatedByDate_Date;
                remarkCreatedByDate_Time = remarkCreatedByDate_Time;
            }
            var editRemark = '<span>'+ remarkVal +'</span>';

            if( editRemarkFlag != 'NO' ){
                editRemark = '<a href="javascript:void(0)" class="editRemark" data-remarkIndexId="' + dataArr[i].index + '">' + remarkVal + '</a>';
            }

            innerHtml += '<div class="room-box">' +
                    '<span class="pull-right text-warning room-detail" data-toggle="tooltip" data-placement="top" title="Added On"><i class="fa fa-calendar "></i> &nbsp;' + remarkCreatedByDate_Date + '<br /><i class="fa fa-clock-o"></i> &nbsp;'+remarkCreatedByDate_Time+'<br/><i class="fa fa-user"></i> &nbsp;' + remarkCreatedBy + '</span>' +
                    '<p class="remark-body" title="' + remarkVal + '">' +
                    editRemark +
                    '</p></div>';
                    
        }

        $('#room-desk1').html(innerHtml);

        registerRemarksEvents();
        registerToolTip();

    }
    /* funtion to render remarks END */


    /* funtion to bind remarks related events START */
    function registerRemarksEvents()
    {
        $('.editRemark').editable({
            placement: 'bottom',
            type: 'textarea',
            pk: 1,
            name: 'comments',
            title: 'Edit Remark',
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
                var remark = value;
                var remarkIndexId = $(this).attr('data-remarkIndexId');

                if( actArr.ACT_STATUS == "2" ){
                    toastr.warning('Activity is in closed state, so you cannot update any remarks');
                    thisModule.getActDetailsData();
                    return false;
                }
                updateCommentInAct(remarkIndexId, remark);
            }
        });
        
        $('.editRemark').click(function(){
            $('textarea.input-large').keypress(function(e){
                if( e.which == 92 ){
                  return false;
                }
            });
        });

    }



    /* funtion to bind remarks related events END */

    /* register tooltip START */
    function registerToolTip()
    {
        //$('.tooltip').hide(); // to hide the tooltips.
        // $('.tooltip').remove(); // to remove the tooltips.

        // $('[title]').tooltip({
        //     container: 'body'
        // });



    }
    /* register tooltip END */





}

var actPage;
function initActivityDetailsPage()
{

    /* create an instance of this page's object to use all its public functions START */
    actPage = new activityDetailsPage();

    /* create an instance of this page's object to use all its public functions END */

    // get activity data from localStorage.POTGactivityAppActivityId
    actPage.getActDetailsData();
    
    //init/register events for this page's elements.
    //actPage.registerActDetailsPageEvents();

    //saveNewRemark actPage.saveNewRemark();
}


function openLinkInNewTab(linkData){
    var link = $(linkData).attr("data-link");
    if(link==""){
        toastr.warnings("There are no attachment linked");
        return false;
    }else{
        window.open(link);
    }    
}

function openModal(dataElement){

    if( checkAuth(2, 7, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }

    var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
    var userId = userDetail.data[0].PK_USER_ID

    if( assignedToId == userId && actStatus == InProcessStatus ){
        //var modaldivId = $(dataElement).attr("data-div");
        var modaldivId = $(dataElement).data("div");
        $("#"+modaldivId).modal("show");
    
    }else if( actStatus == pendingStatus){
        toastr.warning('You have to Start Activity to Add Remark');
        return false;

    }else if( actStatus == closedStatus ){
        toastr.warning("Activity is Closed, Now You can't Add Remark");
        return false;

    }else if( actArr.ACT_STATUS == "2" ){
        toastr.warning('Activity is in closed state, so you cannot update any remarks');
        return false;

    }else{
        toastr.warning('You are not Authorised to perform this Activity');
        return false;
    }
}


function openModalAct(){
    
    $('#txtActTitle').val('Call');

    var closeActReasonId = $('#closeActReason').val();
    var closeActReasonText = $('#closeActReasonTxt').val();
    
    if( closeActReasonId == "-1" ){
        $('#closeActReason').focus();
        return false;

    }else if( closeActReasonText == "" ){
        $('#closeActReasonTxt').focus();
        return false;
    }

    // resetPopups();
    $('#ddActAssignedTo').html(GBL_ALL_USER_HTML).selectpicker('refresh');
    
    var currTime = formatAMPMRoundOff( new Date() );
    // var currTime = formatAMPMRoundOff( new Date('Thu Nov 24 2016 11:55:01 GMT+0530 (IST)') );
    var d = new Date();
    var remindTime = d.setMinutes(d.getMinutes() - 20);
    remindTime = formatAMPMRoundOff( new Date(remindTime) );

    $('#dueTime').val( currTime );
    $('#endNormTime').val( currTime );
    $('#reminderTime').val( remindTime );

    $('#dueTime').on('pick.clockface', function() 
    {
        var d = new Date('01-01-1970 '+$('#dueTime').val());
        var remindTime = d.setMinutes(d.getMinutes() - 20);
        remindTime = formatAMPMRoundOff( new Date(remindTime) );
        $('#reminderTime').val( remindTime );
    });

    $('#ddActAssignedTo').val(localStorage.indeCampusActivityAppUserId).selectpicker('refresh');    

    $('#txtActTitle').val( actArr.ACT_TITLE );
    $('#txtActDesc').val( actArr.ACT_DESC );


    $('#contPersonDivAct').hide();
    $('#txtCustGrpName').attr('disabled',false);
    $('#actContPersonSear').attr('disabled',false);

    if( actArr.ACT_MODE == "1" ){
        $('#radio-05').prop('checked',true);
    }else if( actArr.ACT_MODE == "2" ){
        $('#radio-06').prop('checked',true);
    }else if( actArr.ACT_MODE == "3" ){
        $('#radio-07').prop('checked',true);
    }else if( actArr.ACT_MODE == "4" ){
        $('#radio-08').prop('checked',true);
    }

    $('#actTypeList').val( actArr.ACT_TYPE );
    $('#actTypeList').selectpicker('refresh'); 

    $('#customerType').val('SINGLE').trigger('change');
    $('#customerType').selectpicker('refresh');

    $('#searchCustName').show();
    $('#unRegCustNameMobile').hide();

    $('#unregCustName').val("");
    $('#unregCustMobile').val("");

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    if( tmp_json.data[0].USERLOGINTYPE == "2" ){
        return false;
    }
    
    if( checkAuth(4,14) != "-1" ){

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getInqListingByUser',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,14),
            custId: tmp_json.data[0].PK_USER_ID,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }

        commonAjax(COMMONURL, postData, inquiryDataCallBack,INQUIRYDETAILGET);

        function inquiryDataCallBack(flag,data) {

            if(data.status == "Success" && flag){

                var inqName = "No Inquiry Name";

                var inqLogOptions = '<option value=""> --Please Select Inquiry -- </option>';
                data.data.forEach( function( record , index ){
                    if( record.inqName != "" ){
                        inqName = data.data[index].inqName;
                    }
                    inqLogOptions += '<option projId="'+ record.projectId +'" contactId="'+ record.contactId +'" custId="'+ record.custId +'" value="'+ record.inqId +'">'+ record.ClientLevelInqId +' - ' + inqName + '</option>';
                });

                $('#inqListAct').html( inqLogOptions );
                $('#inqListAct').selectpicker('refresh');

                // CHANGES FOR BUG ON LIVE SERVER - 7
                if( actInqId != "0" ){
                    $('#InquiryCompo').html( '' );
                    $('#InquiryCompo').html( '<input type="text" id="inqListAct" class="form-control"></input>' );
                    $('#inqListAct').val( actClientInqId +" - "+ actClientInqName );
                    $('#inqListAct').attr( "custId" , actCustId );
                    $('#inqListAct').attr( "contactId" , actContId );
                    $('#inqListAct').attr( "inqId" , actInqId );
                    $('#inqListAct').attr( "projId" , inqProjId );
                    $('#inqListAct').attr('disabled',true);

                    $('.custInfoAct').hide();
                }

                $('#inqOnActDetDiv').show();
                if( actInqId == 0 && actArr.CUTOMERDETAIL != "--" ){
                    $('#inqOnActDetDiv').hide();
                }


            }else{
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

    var currClientType = JSON.parse( localStorage.indeCampusRoleAuth ).data.clientTypeId;
     
    if( currClientType == "32" ){
        $('#unRegOpt').show();
        $('#customerType').selectpicker('refresh');
    }

    if( currClientType == "32" && checkAuth(50,200) != "-1" ){

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'getRealEstateMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(50,200),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }

        commonAjax(COMMONURL, postData, projectDataCallBack,INQUIRYDETAILGET);

        function projectDataCallBack(flag,data) {

            if(data.status == "Success" && flag){

                $('#projListActDiv').show();
                
                var options = '<option value=""> --Please Select Project -- </option>';

                if( data.data != "No record found" ){
                    data.data.forEach(function( record , index ){

                        var selectedOpts = "";
                        if( record.projectId == actProjId ){
                            selectedOpts = "selected";
                        }
                        options += '<option '+ selectedOpts +' value="'+ record.projectId +'"> '+ record.projectName +' </option>';
                    });
                }

                $('#projectListAct').html( options );
                $('#projectListAct').selectpicker('refresh');
                
                if( actInqId != "0" ){
                    if( inqProjId != "0" && inqProjId != "-1" &&  inqProjId != undefined ){
                        $('#projectListAct').val( inqProjId ).trigger('change');
                        $('#projectListAct').attr('disabled',true);
                        $('#projectListAct').selectpicker('refresh');

                    }else{
                        $('#projectListAct').val( "-1" ).trigger('change');
                        $('#projectListAct').removeAttr('disabled');
                        $('#projectListAct').selectpicker('refresh');
                    }
                }
                 

            }else{
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_REAL_ESTATE );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }
    }

    $('#projectListAct').removeAttr('disabled');

    // CHANGES FOR BUG ON LIVE SERVER - 2
    $('#inqOnActDetDiv').show();
    if( actArr.CUTOMERDETAIL != "--" ){

        // CUSTOMER DATA SET
        $('#txtCustGrpName').val( actArr.CUTOMERDETAIL );
        $('#txtCustGrpName').attr( 'data-id' , actArr.FK_CUST_ID );
        
        // CONTACT DATA SET
        $('#actContPersonSear').val( actArr.CONTACTPERNAME + " , " + actArr.CONTACTPERNO );
        $('#actContPersonSear').attr( 'contperid' , actArr.FK_CONTACT_ID );
        $('#contPersonDivAct').show();

        $('#txtCustGrpName').attr('disabled',true);
        $('#actContPersonSear').attr('disabled',true);

        setTimeout( function(){
            $('#customerType').attr('disabled',true).selectpicker('refresh');
            // $('#inqOnActDetDiv').hide();
        },500);
    }

    addFocusId( 'txtActTitle' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

var attachmetData = [];
function openAttachment(){

    if( checkAuth(2, 6) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    } 

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: "actAttachmentListing",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2, 6),
        actId: localStorage.POTGactivityAppActivityId,
        dataValue: generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // console.log(postData); return false;
    commonAjax(INQURL, postData, sendAttachmentCallBack,CREATINGINQDATA);

    function sendAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            attachmetData = data.data;

            renderAttachment();

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ATTACH );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    $('#attachmentAdd').modal('show');
}

function renderAttachment(){
    var attachLi = '' 
    attachmetData.forEach( function( record , index ){

        attachLi += '<li class="">'+
                        '<div class="list-content">'+
                            '<div class="col-lg-11 col-md-11"><a href="javascript:void(0)" data-link="'+ record.fileLink +'" onclick="openLinkInNewTab(this);"><span class="title"> '+ record.fileName +'</span></a></div><div class="col-lg-1 col-md-1"><a href="javascript:void(0)" attchId="'+ record.fileId +'" onclick="deleteAttachment(this)"><i class="fa fa-trash-o pull-right"></i></a></div>'+
                        '</div>'+
                    '</li>';
    });

    $('#AttachmentListing').html( attachLi );
}

function assignAttachementEvent(){
    // grab your file object from a file input
    $('#attachmentName').change(function () {

        if( checkAuth(2, 7) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;
        } 

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "actAttachmentUpload",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(2, 5),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            actId : localStorage.POTGactivityAppActivityId,
        };
        $("#postDataAttach").val( JSON.stringify(temp_json));

        $("#fileupload").off().on('submit',(function(e) {
            addRemoveLoader(1);
            e.preventDefault();
            $.ajax({
                url: UPLOADURL,   // Url to which the request is send
                type: "POST",       // Type of request to be send, called as method
                data:  new FormData(this),// Data sent to server, a set of key/value pairs representing form fields and values
                contentType: false,// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false,// To unable request pages to be cached
                processData:false,// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)           
                headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
                success: function(data)// A function to be called if request succeeds
                {
                    data = JSON.parse(data);
                    console.log(data);
                    if(data.status == "Success"){
                        
                        console.log(data.data);

                        var tempData = {
                            fileId: data.data.attachmentId,
                            fileLink: data.data.fileLink,
                            fileName: data.data.fileName,
                            inqId: data.data.inqId,
                        }

                        attachmetData.push( tempData );
                        addRemoveLoader(0);
                        renderAttachment();
                    }else{
                        toastr.warning(data.status);
                        addRemoveLoader(0);

                    }
                },
                error: function (jqXHR, errdata, errorThrown) {
                    log("error");
                    addRemoveLoader(0);
                }
            });
        }));
        $("#fileupload").submit();
        $('#attachmentName').val('');
    });
}

function deleteAttachment(context){
    
    var attachmentId = $(context).attr('attchId');
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Inquiry Details
    var postData = {
        requestCase: 'actAttachmentListingDelete',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2, 8),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        actId: localStorage.POTGactivityAppActivityId,
        attachId : attachmentId
    };
    commonAjax(INQURL1, postData, deleteAttachmentCallBack,CREATINGINQDATA);

    function deleteAttachmentCallBack(flag, data){
        if (data.status == "Success" && flag) {
            
            var index = findIndexByKeyValue( attachmetData , "fileId" , attachmentId );
            
            attachmetData.splice( index , 1 );

            renderAttachment();

        }else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_ATTACH_DELETE_FAIL );
                // toastr.error(data.status);
            else
                toastr.error(SERVERERROR);
        }
    }
    
}

function openRemark(context){

    if( checkAuth(2, 7, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    var userDetail = JSON.parse( localStorage.indeCampusUserDetail );
    var userId = userDetail.data[0].PK_USER_ID

    if( assignedToId == userId && actStatus == InProcessStatus ){
        $('#remarkfollowup').modal('show');
    }else{
        toastr.warning('Activity should be in InProcess mode to add Remark');
        return false;
    }

    $('#remarkbox').show();
    addFocusId( 'remarkbox' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function setRemindDate(actDate){
    
    $('#reminderDate').val( actDate );
    $(".remindDate").datetimepicker('remove');

    var actTrimDate = (( actDate ).split('|')[0]).trim();
    var actTime = (( actDate ).split('|')[1]).trim();

    $('.remindDate').datetimepicker({
        // startDate: 'today',
        // endDate: actDate,
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        endDate: new Date( ddmmyyToMysql( actTrimDate ) +" "+ actTime ),
        autoclose: true,
        minuteStep: 15,
    });
    $('.remindDate').prop("readonly",true);

    var date = (($('#txtActDueDate').val() ).split('|')[0]).trim();
    var time = (( $('#txtActDueDate').val() ).split('|')[1]).trim();

    $(".default-EndDateTime-picker").datetimepicker('remove');
    $('.default-EndDateTime-picker').datetimepicker({
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date( ddmmyyToMysql( date ) +" "+ time ),
        autoclose: true,
        minuteStep: 15,
    })
    $('.default-EndDateTime-picker').prop("readonly",true);

    if( $('input[type=radio][name="rdoActMode"]').filter(":checked").val() == "4" ){    // ADD 60 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#txtActDueDate').val() , 60 )
    }else{    // ADD 15 MIN
        addTimeInMinutes( "txtActNormEndDate" , $('#txtActDueDate').val() , 15 )
    }
}

function pageInitialEvents(){

    $('#reminderActChk').change(function() {
        if($(this).is(":checked")) {
            $('.reminderActChkCls').show();
        }else{
            $('.reminderActChkCls').hide();
        } 
    });

    $('input[name=rdoActMode]').change(function(){

        if($(this).val() == "1"){
            $('#txtActTitle').val('Call');

        }else if($(this).val() == "2"){
            $('#txtActTitle').val('Send Email');

        }else if($(this).val() == "3"){
            $('#txtActTitle').val('Send SMS');

        }else if($(this).val() == "4"){
            $('#txtActTitle').val('Meet');

        }else {
            $('#txtActTitle').val('');
        }
    });

    // $('.dueDatedpEdit').datetimepicker({
    //     format: 'dd-mm-yyyy',
    //     startDate: 'today',
    //     autoclose: true,
    // }) 
    // .on('changeDate', checkActDetList);

    $('.dueDatedp-dateTime').datetimepicker({
        format: "dd-mm-yyyy | HH:ii P",
        showMeridian: true,
        autoclose: true,
        startDate: new Date(),
        autoclose: true,
        minuteStep: 15,
    }).on('changeDate', checkActDetList);

    // $('#dueTimeEdit').on('pick.clockface', checkActDetList);

    setTimeout(function () {
        $("#crmDashboardSearch").trigger('click');
        //autoCompleteSearchBarCustomer("autocomplete", navigateToCustDashboard);
    }, 100);

    $('#assignbtn').click(function () {
        if( checkAuth(2, 107, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;
        }
        $('#assignbox').slideToggle('fast');
        event.stopPropagation();
    });

    $('body').click(function () {
        $('#assignbox').hide('250');
        $('#rmbox').hide('250');
    });

    $('#rmbox , #assignbox').click(function () {
        event.stopPropagation();
    });

    $('#remarkbox').keypress(function(e){
        if( e.which == 92 ){
          return false;
        }
    }); 

    var roleAuth = JSON.parse( localStorage.indeCampusRoleAuth );
    var actType = roleAuth.data.actType;

    if( roleAuth.data.clientTypeId == "32" ){
        $('.unRegCustType').show();
    }else{
        $('.unRegCustType').remove();
    }

    // var actList = '';
    // for( var i=0; i<actType.length; i++ ){
    //     actList += '<option value="'+ actType[i].ID +'">'+ actType[i].VALUE +'</option>';
    // }
    // $('#actTypeListSch').html( actList );
    setOption("0", "actTypeListSch", actType, "");
    $('#actTypeListSch').selectpicker( 'refresh' );

    setOption("0", "closeActReason", roleAuth.data.actCloseType, "---Select Activity Outcome---");
    $('#closeActReason').selectpicker( 'refresh' );
   
    initBootStrapDatePicker();

    // $('[title]').tooltip({
    //     container: 'body'
    // });

    //ACTIVITY CODE END
    assignAttachementEvent();

    initTelInput( 'unregCustMobile' );   // INITIALIZE COUNTRY WISE MOBILE NUM
}

function createActivity() {

    var userDetail = JSON.parse(localStorage.indeCampusUserDetail);
    var userId = userDetail.data[0].PK_USER_ID

    if (assignedToId != userId) {
        toastr.error("You are not Authorised to perform this Activity");
        return false;
    }
    $('#txtActTitle').val('');
    $('#txtActDesc').val('');
    $('#txtActDueDate').val('');
    $('#ddActAssignedTo').val('-1').selectpicker('refresh');
    $('#txtCustGrpName').val('');
    $(".default-date-picker").datetimepicker('remove');
    $('#creatactivity').modal('show');
    intdefaultdatepicker();

    var currTime = formatAMPMRoundOff( new Date() );

    var d = new Date();
    var remindTime = d.setMinutes(d.getMinutes() - 20);
    remindTime = formatAMPMRoundOff( new Date(remindTime) );

    $('#dueTime').val( currTime );
    $('#endNormTime').val( currTime );
    $('#reminderTime').val( remindTime );

    $('#ddActAssignedTo').val(localStorage.indeCampusActivityAppUserId).selectpicker('refresh');

    $('#dueTime').on('pick.clockface', function() 
    {
        var d = new Date('01-01-1970 '+$('#dueTime').val());
        var remindTime = d.setMinutes(d.getMinutes() - 20);
        remindTime = formatAMPMRoundOff( new Date(remindTime) );
        $('#reminderTime').val( remindTime );
    });
}

function openCloseActPop(type){

    if( checkAuth(2, 7) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }

    if( type == "close" ){
        $('#closeActBtn').attr("onclick","actPage.closeActivity();" );
    }else{
        if( actModeHideShow() == "-1" ){  //  HIDE SHOW ACTIVITY MODE ACCORDING TO ROLE AUTH RIGHTS
            toastr.warning( NOACTMODERIGHTS );
            return false;
        }
        $('#closeActBtn').attr("onclick","closeCreateActivity();openModalAct();" ); 
    }

    $('#closeActRemarkModal').modal('show'); 
    addFocusId( 'closeActReasonTxt' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function checkActDetList(){

    var dueDate = (($('#txtActDueDate').val() ).split('|')[0]).trim();
    var dueTime = (($('#txtActDueDate').val() ).split('|')[1]).trim();

    if(  dueDate == "" ){
        $('#txtActDueDate').datetimepicker('show');
        toastr.warning("Please enter due date/time");
        return false;
    // }else if( dueTime == "" ){
    //     $('#dueTimeEdit').focus();
    //     toastr.warning("Please enter due time");
    //     return false;
    }else{

        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var postData = {
            requestCase : 'getActDetailOnDueDateTime',
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID, 
            dueTime : dueTime,
            dueDate : ddmmyyToMysql(dueDate),
            orgId: checkAuth(2,6),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        };
        commonAjax(ACTURL, postData, checkActListCallback, "");
    }

    function checkActListCallback(flag,data){

       if(data.status == "Success" && flag){

        console.log(data.data);

        var thead = '<table class="display datatables-alphabet-sorting" id="actDueListTable">'+
                    '<thead>'+
                    '<th>Activity Title</th>'+
                    '<th>Customer Name</th>'+
                    '<th>customer No</th>'+
                    '</thead>'+
                    '<tbody>';

        var newRow = '';
        var checkActData = data.data;
        if(checkActData != "No record found"){
 
            checkActData.forEach( function( record , index ){ 
                newRow += '<tr>'+
                            '<td>'+ record.actTitle +'</td>'+
                            '<td>'+ record.custName +'</td>'+
                            '<td>'+ record.custNo +'</td>'+
                          '</tr>';

            });
        }

        newRow += '</tbody></table>';

        $('#actDueListDiv').html( thead + newRow );
        $('#actDueListTable').dataTable({ "stateSave": true });
        $('#dueTimeEdit').clockface('hide');

        $('#actTitleShowModal').modal('show');

        }else{
            if (flag){
                //toastr.error(data.status);
                displayAPIErrorMsg( data.status , GBL_ERR_NO_ACT );
            }
            else{
                toastr.error(SERVERERROR);
            }
            $('#actTitleShowModal').modal('hide');
        }
    }
}


// ASSIGN USER NEW CODE STARTED
function assignUser(){
    var assigneTo = $('#ddActAssignedToForEditAct').val();
    if( assigneTo && assigneTo != "-1" ){
        assignUserActivity(assigneTo);
    }
}

function assignUserActivity (assigneTo){
    console.log("Assign activity change event fired");
    //assignActCallBackOnChange

    if( checkAuth(2, 107, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;

    }

    var postData = {
        requestCase: "transferActivity",
        userId: localStorage.indeCampusActivityAppUserId,
        clientId: localStorage.indeCampusActivityAppClientId,
        actId: GBL_ACT_DATA.PK_ACT_ID,
        assigneTo: assigneTo,
        orgId: checkAuth(2,109),
    };



    function callBackAssgnAct(flag, results) {
        if (results.status == SCS) {

            actPage.getActDetailsData();
            $('#assignbox').slideUp('fast');

        } else {

            log("====== FAIL ======");
            if (flag) {
                displayAPIErrorMsg( results.status , GBL_ERR_TRANSFER_ACT_FAIL );
                // toastr.error(results.status);
            } else {
                log("====== No Records Found ======");
            }
        }
    }

    /* ajax call start  */  
    commonAjax(ACTURL, postData, callBackAssgnAct, "");
    // commonAjaxActiivity(actUrl, postData, callBackAssgnAct, actAjaxMsg);
    /* ajax call end  */
}

function renderAllocationList( allocationData ){

    if( allocationData != "No record found" ){

        var allocationHtml = "";

        allocationData.forEach( function( record , index ){

            var icon = "fa fa-share";
            if( record.currentOwner == "1" ){       //CHANGE BY AS PER REQUEST
            // if( index == ( allocationData.length - 1 ) ){
                icon = "fa fa-handshake-o";
            }
            allocationHtml += '<li class="tl-item">'+
                                 '<div class="tl-wrap  transfer">'+
                                    '<div class="tl-content panel-1 padder b-a" style="margin-top: 23px;">'+
                                       '<span class="arrow left pull-up"></span>'+
                                       '<div><i class="'+ icon +'" aria-hidden="true"></i>&nbsp;'+ record.inqAssignTo +' </div>'+
                                       '<div class="customInqActListing">Assign On : '+ record.inqAssignToOn +'</div>'+
                                       '<div class="customInqActListing">Assigned By : '+ record.inqAssignBY +'</div>'+
                                    '</div>'+
                                '</div>'+
                            '</li>'; 
        });
        $('#allocationTimeline').html( allocationHtml );
    }
}