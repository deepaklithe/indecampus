/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : developerTypeController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var targetData = [];
var itemGroupJson = [];
var TodayDate = getTodaysTimeStamp().split(" ")[0];
var GblTodayDate = mysqltoDesiredFormat(TodayDate,'dd-MM-yyyy');

function getBranchListing(){

    addFocusId( 'storeName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
    
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var branchData = roleAuth.data.branchListing;

    var brListOpt = '<option value="-1"> Select Branch </option>'; 
    branchData.forEach( function( record , index ){
        if( localStorage.indeCampusBranchIdForChkAuth == record.orgId ){
            brListOpt += '<option value="'+ record.orgId +'">'+ record.orgName +'</option>'
        }
    });

    $('#storeName').html( brListOpt );
    $('#storeName').val( localStorage.indeCampusBranchIdForChkAuth );
    $('#storeName').selectpicker();

    getListingData() //Getting Render List of target data
    setNext10Years(); //

    $('#chckBox').change(function(){
        if( $('#chckBox').prop('checked') == true  ){
                $('#datediv').show();
        }else{
                $('#datediv').hide();
        }
    });
}

function setNext10Years(){
    var date = new Date();
    var currYear = date.getFullYear();

    var next10Years = [currYear - 2 , currYear - 1 ];
    for( var i=0; i<10; i++ ){
        next10Years.push( currYear + i );
    }
    // console.log(next10Years);
    
    var yearList = ''; 
    next10Years.forEach( function( record , index ){
        if( currYear == record ){
            yearList += '<option value="'+ record +'" selected> '+ record +' </option>';
        }else{ 
            yearList += '<option value="'+ record +'"> '+ record +' </option>';
        }
    });

    $('#tarYear').html( yearList );
    $('#tarYear').selectpicker();

    var date = new Date();
    var currMon = date.getMonth() + 1;

    $('#tarMonth').val( currMon );
    $('#tarMonth').selectpicker( 'refresh' );
}

function getListingData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getClientTraget',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(6,22),
        type: "2",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){

            targetData = data.data ;
            renderTargetListing();
        
        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
        }

    } else {
        renderTargetListing();
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
            // toastr.error(data.status);
        else
            toastr.error(SERVERERROR);
    }
}

function renderTargetListing(){

    var startHtml = "<table id='targetListTable' class='display datatables-alphabet-sorting'>"+
                    "<thead>"+
                        "<tr>"+
                            "<th>#</th>"+
                            "<th>Branch Name.</th>"+
                            "<th>Year</th>"+
                            "<th>Month</th>"+
                            "<th>Activity Type</th>"+
                            "<th>Date Wise</th>"+
                            "<th>Date</th>"+
                            "<th>Qty</th>"+
                            "<th>Action</th>"+
                        "</tr>"+
                    "</thead>"+
                    "<tbody>";
    var endHtml = "</tbody>";
    var inqRow = '';
 
    targetData.forEach( function( record , index ){ 

        var branchName = $('#storeName option[value="'+ record.branchId +'"]').text();
        var monthName = $('#tarMonth option[value="'+ record.monthId +'"]').text();
        var date = ( (record.date == "0000-00-00") ? "--" : mysqltoDesiredFormat(record.date,'dd-MM-yyyy') );
        var actFlag =( (record.actDateFlag == "1") ? "Yes" : "No");
        var btnColor = 'btn-green'
        if( record.setButtonColor == "Y" ){
            btnColor = 'btn-amber'
        }else if(record.setButtonColor == "GR"){
            btnColor = 'btn-gray'
        }

        if( record.monthId == "" ){
            monthName = "";
        }

        var firstTarget = "";
        if( index == 0 ){ firstTarget = "firstTarget"; }

        inqRow += '<tr>'+
                    '<td>'+ (index+1) +'</td>'+
                    '<td>'+ branchName +'</td>'+
                    '<td>'+ record.year +'</td>'+
                    '<td>'+ monthName +'</td>'+
                    '<td>'+ record.itemGroupName +'</td>'+
                    '<td>'+ actFlag +'</td>'+
                    '<td>'+ date +'</td>'+
                    '<td class="amtDisp">'+ numberFormat( record.targetAmt ) +'</td>'+
                    '<td>'+
                        '<a href="javascript:void(0)" class="btn btn-right-mrg btn-sm '+ btnColor +'" targetId="'+ record.targetId +'" id="'+firstTarget+'" onclick="navigateToSetTarget(this)">Set</a>'+
                        '<a href="javascript:void(0)" class="btn btn-right-mrg btn-primary btn-sm" targetId="'+ record.targetId +'" onclick="editTarget(this)"><i class="fa fa-pencil"></i></a>'+
                        '<a href="javascript:void(0)" class="btn btn-danger btn-sm" targetId="'+ record.targetId +'" onclick="deleteTarget(this)"><i class="fa fa-trash"></i></a>'+
                    '</td>'+
                '</tr>';
                                                
    });

    var finalHtml = startHtml + inqRow + endHtml;
    $('#TargetTable').html(finalHtml);
    $('#targetListTable').DataTable({ "stateSave": true });
    
}

function createTarget(){


    var storeName = $('#storeName').val();
    var tarYear = $('#tarYear').val();
    var tarMonth = $('#tarMonth').val();
    var groupId = $('#groupName').val();
    var groupName = $('#groupName option:selected').text();
    var Amount = $('#Amount').val();
    var chckTick = ($('#chckBox').prop('checked') == true ) ? "1" : "0";
    var targetDate = $('#startDate').val();

    var updateTargetId = localStorage.POTGupdateTargetId;

    if( storeName == "-1"){
        toastr.warning('Please Select Branch');
        $('#storeName').focus();
        return false;

    }else if( Amount == ""){
        toastr.warning('Please Enter Amount');
        $('#Amount').focus();
        return false;

    }else if( chckTick == "1" && targetDate == ""){
        toastr.warning('Please Enter Date');
        $('#startDate').focus();
        return false;

    }else {
        
        targetDate = ( (chckTick == "1") ? targetDate : "" );
        targetDate = ( (targetDate != "") ? ddmmyyToMysql(targetDate) : "0000-00-00" );

        if( localStorage.POTGupdateTargetId != "" && localStorage.POTGupdateTargetId != undefined ){
           if( checkAuth(6, 23, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'updateClientTraget',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(6,23),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                selectedBranchId: storeName,
                year: tarYear,
                month: tarMonth,
                groupId: groupId,
                amt: Amount,
                targetId: localStorage.POTGupdateTargetId,
                type: "2",
                actDateFlag : chckTick,
                date : targetDate, 

            }
            localStorage.removeItem( 'POTGupdateTargetId' );
            commonAjax(FOLLOWUPURL, postData, updateTargetDataCallback,"Please Wait... Getting Dashboard Detail");

        }else{
          if( checkAuth(6, 21, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }

            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // Followup Listing Todays, Delayed, Upcomming
            var postData = {
                requestCase: 'createClientTraget',
                clientId: tmp_json.data[0].FK_CLIENT_ID,
                userId: tmp_json.data[0].PK_USER_ID,
                orgId: checkAuth(6,21),
                dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                selectedBranchId: storeName,
                year: tarYear,
                month: tarMonth,
                groupId: groupId,
                amt: Amount,
                type: "2",
                actDateFlag : chckTick,
                date : targetDate,
            }

            commonAjax(FOLLOWUPURL, postData, sendTargetDataCallback,"Please Wait... Getting Dashboard Detail");
        }


        function sendTargetDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                console.log(data);

                if(data.data != 'No record found'){

                    console.log(data.data);

                    targetData.push( data.data[0] );
                    resetData();    
                    renderTargetListing();

                    toastr.success('Your Target is Created');

                }else{
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
                }

            } else {
                if (flag)
                    // toastr.error(data.status);
                    displayAPIErrorMsg( data.status , GBL_ERR_INSERT_TARGET_FAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }

        function updateTargetDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                console.log(data);

                if(data.data != 'No record found'){

                    console.log(data.data);

                    var index = findIndexByKeyValue( targetData , 'targetId', updateTargetId );

                    var tempJson = {
                        branchId: storeName,
                        clientId: targetData[index].clientId,
                        itemGroupId: groupId,
                        itemGroupName: groupName,
                        monthId: tarMonth,
                        targetAmt: Amount,
                        targetId: targetData[index].targetId,
                        setButtonColor: targetData[index].setButtonColor,
                        year: tarYear,
                        transDetail: targetData[index].transDetail,
                        type: "2",
                        actDateFlag : targetData[index].actDateFlag,
                        date : targetDate,
                    }

                    targetData[index] = tempJson ;
                    resetData();
                    renderTargetListing();

                    toastr.success('Your Target is Updated');

                }else{
                    displayAPIErrorMsg( data.status , GBL_ERR_NO_TARGET );
                }

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_UPDATE_TARGET_FAIL );
                else
                    toastr.error(SERVERERROR);
            }
        }
    } 
}


function editTarget(context){

    if( checkAuth(6, 23, 1) == -1 ){
        toastr.error('You are not Authorised for this Activity');
        return false;
    }
    
    var targetId = $(context).attr('targetId');
    localStorage.POTGupdateTargetId = targetId;

    var index = findIndexByKeyValue( targetData, 'targetId' , targetId );
    var dateChck = targetData[index].actDateFlag;

    $('#groupName').val(targetData[index].itemGroupId);
    $('#groupName').selectpicker('refresh');
    $('#Amount').val(targetData[index].targetAmt);
    $('#storeName').val(targetData[index].branchId);
    $('#tarYear').val(targetData[index].year);
    $('#tarMonth').val(targetData[index].monthId);
    $('#storeName').selectpicker('refresh');
    $('#tarYear').selectpicker('refresh');
    $('#tarMonth').selectpicker('refresh');   
    if(dateChck == "1"){
        $('#chckBox').prop('checked',true);
        $('#datediv').show();
        $('#startDate').val(targetData[index].date);
    }else{
        $('#chckBox').prop('checked',false);
    }
    
    $('.editMode').attr('disabled',true).selectpicker('refresh');
    addFocusId( 'storeName' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

}

function deleteTarget(context){

    if( checkAuth(6, 24, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
    var targetId = $(context).attr('targetId');
    
    var deleteRow = confirm('Are You sure ? You want to delete this Record');

    if( deleteRow ){
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase: 'deleteClientTraget',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(6,24),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            targetId: targetId,
            type: "2",
        }

        commonAjax(FOLLOWUPURL, postData, deleteTargetDataCallback,"Please Wait... Getting Dashboard Detail");

        function deleteTargetDataCallback(flag,data){

            if (data.status == "Success" && flag) {

                var index = findIndexByKeyValue( targetData , 'targetId', targetId );

                targetData.splice(index,1);
                renderTargetListing();

                toastr.success('Your Target is Deleted');

            } else {
                if (flag)
                    displayAPIErrorMsg( data.status , GBL_ERR_DELETE_TARGET_FAIL );
                    // toastr.error(data.status);
                else
                    toastr.error(SERVERERROR);
            }
        }
    }
}

function resetData(){

    $('#groupName').val('');
    $('#Amount').val('');
    // $('#tarMonth').val('');
    // $('#tarMonth').selectpicker('refresh');
    $('#groupName').selectpicker('refresh');
    // $('#storeName').val('-1');
    // $('#storeName').selectpicker('refresh');
    $('#startDate').val('');
    $('#chckBox').prop('checked',false); 
    $('#datediv').hide();
    $('#errorHideDiv').hide();
    
    localStorage.removeItem('POTGupdateTargetId');
    $('.editMode').removeAttr('disabled',true).selectpicker('refresh');
}

function navigateToSetTarget(context){

     if( checkAuth(5, 19, 1) == -1 ){
            toastr.error('You are not Authorised for this Activity');
            return false;

        }
        
    var targetId = $(context).attr('targetId');
    var index = findIndexByKeyValue( targetData , 'targetId' , targetId );

    if( index != '-1' ){

        var branchName = $('#storeName option[value="'+ targetData[index].branchId +'"]').text();
        var monthName = $('#tarMonth option[value="'+ targetData[index].monthId +'"]').text();

        var targetDetails = targetData[index];

        targetDetails.branchName = branchName;
        targetDetails.monthName = monthName;

        localStorage.POTGuserTargetDetail = JSON.stringify( targetDetails );

        window.location.href = "actTargetDist.html";
    }

}

function openUploadDialog() {

   $('#uploadActivityTargetModal').modal('show');
}

function uploadActivityTarget() {

    var uploadFileName = $("#attachmentName").val();
    if(!uploadFileName){
        toastr.warning('Please select file for upload activity target.');
        $('#attachmentName').click();
        return false;
    }
    $("#addDocument").submit();
}

function assignAttachmentEvent(){
    
    $("#addDocument").off().on('submit', (function (e) {
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var temp_json = {
            requestCase : "uploadMasterData",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(6,21),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            uploadType : "activityTarget",
        };
        $("#postDataActUpload").val(JSON.stringify(temp_json));
        var bfsendMsg = "Sending data to server..";

        addRemoveLoader(1); // 1-ADD LOADER || 0-REMOVE LOADER 
        e.preventDefault();
        $.ajax({
            url: UPLOADURL, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values
            contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            beforeSend: function () {
                //alert(bfsendMsg);
                // $("#msgDiv").html(bfsendMsg);
                // $("#ajaxloader").removeClass("hideajaxLoader");
                addRemoveLoader(1);
            },
            success: function (data)// A function to be called if request succeeds
            {

                data = JSON.parse(data);
                console.log(data);
                
                if(data.status == "Success"){
                    
                    console.log(data.data);

                    refreshFile();
                    $('#errorHideDiv').hide();
                    $('#errorDiv').html("");
                    toastr.warning( data.data );
                }else{
                    toastr.warning(data.status);
                    refreshFile();
                    GBL_ERROR = data.data;
                    renderError(GBL_ERROR);
                    
                }
                addRemoveLoader(0);
                $('#uploadActivityTargetModal').modal('hide');
                // $("body").addClass("loaded");
                // $("#loader-wrapper").hide();
            },
            error: function (jqXHR, errdata, errorThrown) {
                log("error");
                addRemoveLoader(0);
                // $("body").addClass("loaded");
                toastr.error(errdata);
                if( jqXHR.responseText != "" ){
                    GBL_ERROR = JSON.parse(jqXHR.responseText).data;
                    renderError(GBL_ERROR);
                }
                refreshFile();
            }
        });
    }));
}

function refreshFile(){

    $('#attachmentName').val('');
    $("#postDataAttach").val('');
    $(".fileinput-filename").html('');
    $(".close.fileinput-exists").hide();
    $("span.fileinput-exists").html('Select file');
}

function downloadTargetSample() { //download activity type and activity target sample files
    

    var tmp_json = JSON.parse( localStorage.indeCampusRoleAuth );
    var actTypeData = tmp_json.data.actType;
    
    var tableStartTodayHtml = '<table>';
    var tableEndHtml = '</table>';
    var threadHtml = '<thead>'+
                        '<tr>'+
                        '<th>Activity Type Id</th>'+
                        '<th>Activity Type Value</th>'+
                        '</tr>'+
                    '</thead>';
    var innerHtml = "";
    actTypeData.forEach(function( record , index ){
           
        innerHtml += '<tr>' +
                        '<td>' + record.ID + '</td>' +
                        '<td>' + record.VALUE + '</td>' +
                     '</tr>';
        
    });
    var tbodyStart = '<tbody>';
    var tbodyEnd = '</tbody>';
    if(innerHtml != ""){
        customCsvFormatSave(tableStartTodayHtml + threadHtml + tbodyStart + innerHtml + tbodyEnd + tableEndHtml, "Activity Type " + GblTodayDate + ".xls");
    }else{
        toastr.warning("No record");
    }

    var GBL_SAMPLEJSON = [];

    var tempEXL = { // HEADER FOR PDF AND CSV
                year : "Year",
                month : "Month",
                actType : "ActivityType",
                qty : "Qty",
                userName : "UserName",
    };
    GBL_SAMPLEJSON.push(tempEXL);

    csvSave(GBL_SAMPLEJSON, "activityTargetSample.csv");
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 09-MAY-2018 BY VISHAKHA TAK
function initSelfTour(){

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: "#TargetTable",
            title: "<b>Activity Target </b>",
            content: "Available Activity Target listing and actions performed on it !!",
            placement: 'top'
        },
        {
            path: "",
            element: ".storeNameDiv",
            title: "<b>Branch Name</b>",
            content: "Select branch name and other details to set sales target!!",
            placement: 'right'
        },
        {
            path: "",
            element: '#Amount',
            title: "<b>Target amount</b>",
            content: "Add amount of target for selected branch !!",
            placement: 'left',
        },
        {
            path: "",
            element: "#createTargetBtn",
            title: "<b>Save target details</b>",
            content: "Save target details",
            placement: 'right',
        },
        {
            path: "",
            element: "#downloadTargetBtn",
            title: "<b>Download target sample </b>",
            content: "You can also download target sample files",
            placement: 'left',     
        },
        {
            path: "",
            element: "#uploadActBtn",
            title: "<b>Upload target </b>",
            content: "You can also upload target csv file",
            placement: 'left',
            onNext:function (tour){
                openUploadDialog();
            }     
        },
        {
            path: "",
            element: '#addDocument',
            title: "<b>Upload target file</b>",
            content: "Upload activity target file from here!",
            placement: 'top', 
            onPrev: function(tour){
                $('#uploadActivityTargetModal').modal('hide');
            }, 
            onNext: function(tour){
                $('#uploadActivityTargetModal').modal('hide');
            },
        },
        {
            path: "",
            element: "#TargetTable",
            title: "<b>Target Set </b>",
            content: "Set Target Details Data !!",
            placement: 'top',
            onPrev: function(tour){
                $('#uploadActivityTargetModal').modal('show');
            }, 
            onNext: function(tour){
                if( targetData == "" ){
                    tour.end();
                }else{
                    $('#firstTarget').click();
                }
            },
        },
        {
            path: "",
            element: '#targetListTable',
            title: "<b>Target Distribution</b>",
            content: "Listing of target distributed according to employee <br/> Which you can add or edit!",
            placement: 'top',
            onNext: function(tour){
                setInterval(function () {
                    // $('#createTargetOnUserBtn').click();
                    window.location.href = "clienttarget.html";
                    tour.end();
                },1);
            }
        },
        {
            path: "",
            element: '#createTargetOnUserBtn',
            title: "<b>Save Target</b>",
            content: "Activity Target!",
            placement: 'right',   
            onShown: function(tour){
                // setInterval(function () {
                    $('#createTargetOnUserBtn').click();
                    // tour.end();
                // },3000);
            }
        },
    ];
   
    // Instance the tour
    instanceTour();
    tour._options.name = "Activity Target Tour";
    // tour._options.debug = true;

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM THIS STEP TO PREVENT FROM DISPLAYING LAST STORED STEP

    // Start the tour
    tour.start(true);
    localStorage.POTGtargetTour = 1;
    localStorage.GBL_TOUR_STEPS = JSON.stringify(GBL_TOUR_STEPS);
}