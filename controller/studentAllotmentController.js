    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 11-07-2018.
 * File : studentAllotmentController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_ROOM_ITEMS = []; // ROOM NUMBER JSON
var GBL_ALLOTMENT_DATA = []; // ROOM NUMBER JSON

// PAGE INITIAL EVENTS
function pageInitialEvents(){

    $('#roomNumber').selectpicker();
    $('#assetsIssued').selectpicker();
    $('#roomSelected').html( localStorage.indeCampusAllotRoomType );
    $('#tenancyStDate').attr('readonly',true);
    $('#tenancyEndDate').attr('readonly',true);
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    var assetTypeData = roleAuth.data.assetType;
    setOption("0",'assetsIssued',assetTypeData,'Select Asset');
    $('#assetsIssued').selectpicker('refresh');
    if( localStorage.indeCampusAllotStudId != undefined && localStorage.indeCampusAllotItemId != undefined ){
        getRoomList();
    }
}

// GET TYPE OF ROOM DATA
function getRoomList(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'roomList', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(13 ,54),
        itemId : localStorage.indeCampusAllotItemId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRoomDataCallback,"Please wait...Adding your item.");

    function getRoomDataCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_ITEMS = data.data;
                
            var innerHtml = '<option value="-1">Select Room Number</option>';
            if( GBL_ROOM_ITEMS != "No record found" ){
                GBL_ROOM_ITEMS.forEach(function( record , index ){

                    innerHtml += '<option value="'+ record.roomId +'">'+ record.roomName +'</option>';

                });
            }
            $('#roomNumber').html( innerHtml ).selectpicker('refresh'); // SET ROOM NUMBER DATA IN DROPDOWN
            
        }else{

            GBL_ROOM_ITEMS = [];
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOMS );
            else
                toastr.error(SERVERERROR);
        }
        getAllotmentDetail();
    }

}

// ASSIGN ALLOTMENT TO STUDENT
function assignAllotment() {

    var roomNumber = $('#roomNumber').val();
    var bedNumber = $('#bedNumber').val();
    var tenancyStDate = $('#tenancyStDate').val();
    var tenancyEndDate = $('#tenancyEndDate').val();
    var assetsIssued = $('#assetsIssued').val();
    var startDate = new Date(ddmmyyToMysql(tenancyStDate));
    var endDate = new Date(ddmmyyToMysql(tenancyEndDate));
    var roomName = $('#roomNumber option:selected').text();

    // CREATE COMMA SEPERATED LIST OF ASSETS
    var assetsList = "";
    if( assetsIssued != "-1" && assetsIssued != null ){
        assetsIssued.forEach(function( assetRcd , assetIndex ){
            if( assetRcd != "-1" ){
                if( assetsList == "" ){
                    assetsList += assetRcd;
                }else{
                    assetsList += "," + assetRcd;
                }
            }
        });
    }
    console.log(assetsList);

    // CHECK VALIDATIONS
    if( roomNumber == "-1" ){
        toastr.warning("Please select room number");
        addFocusId('roomNumber');
        return false;

    }else if( bedNumber == "" ){
        toastr.warning("Please enter bed number");
        addFocusId('bedNumber');
        return false;

    }else if( tenancyStDate == "" ){
        toastr.warning("Please select tenancy start date");
        addFocusId('tenancyStDate');
        return false;

    }else if( tenancyEndDate == "" ){
        toastr.warning("Please select tenancy end date");
        addFocusId('tenancyEndDate');
        return false;

    }else if( tenancyEndDate != "" && ( +endDate < +startDate ) ){
        toastr.warning("Please tenancy end date cannot be less than tenancy start date");
        addFocusId('tenancyEndDate');
        return false;

    }else if( assetsIssued == null || assetsList == "" ){
        toastr.warning("Please select assets to allot");
        addFocusId('assetsIssued');
        return false;

    }

    if( roomNumber == "-1" || roomNumber == "" ){
        roomName = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'addStudentAllotment', // ADD ALLOTMENT DATA
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 44),
        studentId : localStorage.indeCampusAllotStudId,
        itemId : localStorage.indeCampusAllotItemId,
        roomId : roomNumber,
        roomName : roomName,
        bedNo : bedNumber,
        assetIssued : assetsList,
        tenancyStartDate : tenancyStDate,
        tenancyEndDate : tenancyEndDate,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRoomDataCallback,"Please wait...Adding your item.");

    function getRoomDataCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            // REMOVE USED LOCALSTORAGE VARIABLE
            localStorage.removeItem('indeCampusAllotStudId');
            localStorage.removeItem('indeCampusAllotItemId');
            localStorage.removeItem('indeCampusAllotRoomType');
            navigateToEnrollmentMaster(); // REDIRECT TO LISTING PAGE

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_ASSIGN_ALLOT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// GET ALLOTMENT DETAIL OF SELECTED STUDENT
function getAllotmentDetail(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getStudentAllotment', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        studentId : localStorage.indeCampusAllotStudId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getAllotmentDetailCallback,"Please wait...Adding your item.");

    function getAllotmentDetailCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ALLOTMENT_DATA = data.data;
            // IF HAVE DATA , RENDER AVAILABLE DATA FOR UPDATE
            if( GBL_ALLOTMENT_DATA != "No record found" && GBL_ALLOTMENT_DATA != "" ){
                renderAllotmentData();  // RENDER AVAILABLE DATA FOR UPDATE  
            }
           
        }else{

            GBL_ALLOTMENT_DATA = [];
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ALLOT_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER AVAILABLE DATA FOR UPDATE
function renderAllotmentData(){

    $('#roomNumber').val( GBL_ALLOTMENT_DATA[0].roomId ).selectpicker('refresh');
    $('#bedNumber').val( GBL_ALLOTMENT_DATA[0].bedNo );
    $('#tenancyStDate').datepicker( 'update' , mysqltoDesiredFormat(GBL_ALLOTMENT_DATA[0].tenancyStartDate,'dd-MM-yyyy') );
    $('#tenancyEndDate').datepicker( 'update' , mysqltoDesiredFormat(GBL_ALLOTMENT_DATA[0].tenancyEndDate,'dd-MM-yyyy') );
    
    var assetIssuedList = GBL_ALLOTMENT_DATA[0].assetIssued.split(',');
    $('#assetsIssued').val( assetIssuedList ).selectpicker('refresh');

    $('#createUserBtn').html('<i class="fa fa-floppy-o"></i> Update');
    $('#createUserBtn').attr('onclick', 'updateAllotment()' );
}

// UPDATE ASSIGNED ALLOTMENT TO STUDENT
function updateAllotment(){

    var roomNumber = $('#roomNumber').val();
    var bedNumber = $('#bedNumber').val();
    var tenancyStDate = $('#tenancyStDate').val();
    var tenancyEndDate = $('#tenancyEndDate').val();
    var assetsIssued = $('#assetsIssued').val();
    var startDate = new Date(ddmmyyToMysql(tenancyStDate));
    var endDate = new Date(ddmmyyToMysql(tenancyEndDate));
    var roomName = $('#roomNumber option:selected').text();

    // CREATE COMMA SEPERATED LIST OF ASSETS
    var assetsList = "";
    if( assetsIssued != "-1" && assetsIssued != null ){
        assetsIssued.forEach(function( assetRcd , assetIndex ){
            if( assetRcd != "-1" ){
                if( assetsList == "" ){
                    assetsList += assetRcd;
                }else{
                    assetsList += "," + assetRcd;
                }
            }
        });
    }
    console.log(assetsList);

    // CHECK VALIDATIONS
    if( roomNumber == "-1" ){
        toastr.warning("Please select room number");
        addFocusId('roomNumber');
        return false;

    }else if( bedNumber == "" ){
        toastr.warning("Please enter bed number");
        addFocusId('bedNumber');
        return false;

    }else if( tenancyStDate == "" ){
        toastr.warning("Please select tenancy start date");
        addFocusId('tenancyStDate');
        return false;

    }else if( tenancyEndDate == "" ){
        toastr.warning("Please select tenancy end date");
        addFocusId('tenancyEndDate');
        return false;

    }else if( tenancyEndDate != "" && ( +endDate < +startDate ) ){
        toastr.warning("Please tenancy end date cannot be less than tenancy start date");
        addFocusId('tenancyEndDate');
        return false;

    }else if( assetsIssued == null || assetsList == ""){
        toastr.warning("Please select assets to allot");
        addFocusId('assetsIssued');
        return false;

    }
    
    if( roomNumber == "-1" || roomNumber == "" ){
        roomName = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'editStudentAllotment',  // UPDATE ALLOTMENT DATA
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 46),
        studentId : localStorage.indeCampusAllotStudId,
        itemId : localStorage.indeCampusAllotItemId,
        allotmentId : GBL_ALLOTMENT_DATA[0].allotmentId,
        roomId : roomNumber,
        roomName : roomName,
        bedNo : bedNumber,
        assetIssued : assetsList,
        tenancyStartDate : tenancyStDate,
        tenancyEndDate : tenancyEndDate,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, updateAllotmentDataCallback,"Please wait...Adding your item.");

    function updateAllotmentDataCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            // REMOVE USED LOCALSTORAGE VARIABLE
            localStorage.removeItem('indeCampusAllotStudId');
            localStorage.removeItem('indeCampusAllotItemId');
            localStorage.removeItem('indeCampusAllotRoomType');
            navigateToEnrollmentMaster(); // REDIRECT TO LISTING PAGE

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_ASSIGN_ALLOT );
            else
                toastr.error(SERVERERROR);
        }
    }

}