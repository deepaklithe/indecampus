/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 01-06-2016.
 * File : inquiryListController.
 * File Type : .js.
 * Project : POTG
 *
 * */
 
var userWiseInqData = [];
var pendingStatus = [];
var wonHtml = "";
var lostHtml = "";
var html = "";
var cartTotalAmt = 0;
var newCartData = [];
var getData = [];
var cancelRemarkData = [];
var inqData = [];
function chkValidPageLoad() {
    
    if (localStorage.indeCampusLoadPage == "true") {
        localStorage.indeCampusLoadPage = "false";        
    }
    else {
        if (pageLoadChk) {
            toastr.error(NOTAUTH);
            navigateToIndex();
        }
    }
}

function getEmpListingData(){

    // setTimeout(function(){
    //     $("#loader-wrapper").show();
    //     $("body").removeClass("loaded");
    // },100);
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'userListingActivityModule',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(2,6),
        supervisorFlag: "1",
        activeInactiveFlag : "1",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getEmpListingDataCallback,"Please Wait... Getting Dashboard Detail");
}


function getEmpListingDataCallback(flag,data){

    if (data.status == "Success" && flag) {

        if(data.data != 'No record found'){

            if(data.data.length > 1){
                $('#transferBtn').show();
            }
            var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
            // console.log(empData);return false;
            var brListOpt = '';
            var userListOpt = '<option value="-1" selected> Select User </option>';
            if( data.data.length > 1 ){
                brListOpt += '<option value="-1"> All </option>';
            }
            for( var i=0;i<data.data.length; i++ ){
                if( tmp_json.data[0].PK_USER_ID == data.data[i].userId ){
                    brListOpt += '<option value="'+ data.data[i].userId +'" selected>'+ data.data[i].fullName +'</option>';
                    userListOpt += '<option value="'+ data.data[i].userId +'">'+ data.data[i].fullName +'</option>';
                }else{   
                    brListOpt += '<option value="'+ data.data[i].userId +'">'+ data.data[i].fullName +'</option>';
                    userListOpt += '<option value="'+ data.data[i].userId +'">'+ data.data[i].fullName +'</option>';
                }
            }
            $('#userList').html( brListOpt );
            $('#userList').selectpicker();

            $('#toUser').html(userListOpt);
            $('#toUser').selectpicker('refresh');
            $('#fromUser').html(userListOpt);
            $('#fromUser').selectpicker('refresh');

            getinquiryListing();

        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        }

        $("#loader-wrapper").hide();
        $("body").addClass("loaded");
    } else {
        $("#loader-wrapper").hide();
        $("body").addClass("loaded");
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_USER );
        else
            toastr.error(SERVERERROR);
    }
}

function getinquiryListing(){
    
    setTimeout(function(){
        $("#loader-wrapper").show();
        $("body").removeClass("loaded");
    },100);
    var requesteduserId = $("#userList").val();
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetail',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        orderFlag: '2',//FOR INQ LISTING
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        requestedUserId : requesteduserId
    }

    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}

function inquiryListCallback(flag,data){
            
    //if (data.status == "Success" && flag) {
        
    var localStatus = JSON.parse(localStorage.indeCampusRoleAuth);
    var index = findIndexByKeyValue(localStatus['data']['statusListing'],'statusCatName','INQUIRY');
    if( index == "-1" ){
        return false;
    }

    var statusList = localStatus['data']['statusListing'][index]['statusListing'];

    statusList.sort(function(a, b) {
        return parseFloat(a.statusPriority) - parseFloat(b.statusPriority);
    });
    
    var thrdPartyStat = "";
    var indPriority = findIndexByKeyValue( statusList , "thirdPartyFlag" , "1" );
    if( indPriority != "-1" ){
        thrdPartyStat = statusList[indPriority].statusPriority
    }

    console.log(statusList);
    html = "";
    wonHtml = "";
    lostHtml = "";
    pendingStatus = [];
    $.each(statusList, function(k,v){
       if(v.thirdPartyFlag != "1" && v.remarkOnStatusFlag != "1" && parseInt( v.statusPriority ) < parseInt( thrdPartyStat ) ){
           html += '<div class="fbox1" statPriority="'+ v.statusPriority +'">'+
                            '<div class="draggable-title">'+							
                                '<div class="panel-title"><h4>'+v.VALUE+'</h4></div>'+
                                '<div class="draggable-value">'+
                                '<span class="value" id="totalValue_'+v.ID+'">&#8377; 00</span><span id="totalDeals_'+v.ID+'">0 deals</span>'+
                            '</div>'+
                            '</div>'+
                        '</div>';
                var temp = {
                    id : v.ID,
                    priority : v.statusPriority,
                }
                pendingStatus.push(temp);
       }else if(v.thirdPartyFlag == "1"){
           wonHtml = '<div id="won" class="fbox" statId="'+ v.ID +'" statPriority="'+ v.statusPriority +'"><button onclick="navigateToInquiryReport('+ v.ID +')" type="button" class="btn-success btn btn-sm margin-top-5"><span>'+v.VALUE+'</span></button></div>';
       }else if(v.remarkOnStatusFlag == "1"){
           lostHtml = '<div id="lost" class="fbox" statId="'+ v.ID +'" statPriority="'+ v.statusPriority +'"><button onclick="navigateToInquiryReport('+ v.ID +')" type="button" class="btn-danger btn btn-sm margin-top-5"><span>'+v.VALUE+'</span></button></div>';
       }
    });

    if (data.status == "Success" && flag) {  

        userWiseInqData = data.data;
        getUserInq(userWiseInqData , 1);
        searchInquiry();

        $('#transferInqBtn').removeAttr('disabled');
        
    } else {   

        getUserInq("No record found");     
        
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_DATA );
        }else{
            toastr.error(SERVERERROR);
        }            
    }
}

function getUserInq( data , type ){
 
    var currUser = $('#userList').val();

    var updatedInqList = [];
    
    if( !data ){
        data = userWiseInqData;
    }
    
    if( currUser == "-1" && type == "1" ){
        updatedInqList = userWiseInqData;
    }else{
        if( data && data != "No record found" ){
            if( currUser != "-1" ){
                data.forEach( function(record , index ){
                    if( currUser == record.inqAssignToId ){
                        updatedInqList.push( record );
                    }
                });    
            }else{
                updatedInqList = data;
            }
        }
    }

    if( data == "No record found" ){
        updatedInqList = data;
    }

    renderUserWiseInqData( updatedInqList );
}

var context = [];
var inqId = "";
var newStatId = "";
var oldStatId = "";
var event = [];
var ui = [];
function renderUserWiseInqData(data){

    $("#statusListing").html(html);
    $("#bottom-row").html(wonHtml + lostHtml);
    var inqRow = '';
    inqData = data;
    var finalInqListingHtml = "";
    if( inqData != 'No record found' ){
        $.each(pendingStatus, function(keyStatus,valueStatus){ // STATUS EACH
           var potAmt = 0; 
           var totalDeals = 0; 
           var statusWiseHtmlHeader = '<div id="status_'+valueStatus.id+'" statId="'+ valueStatus.id +'" statPriority="'+ valueStatus.priority +'" class="fbox">';
           var statusWiseHtmlMid = '';
           var statusWiseHtmlFooter = '</div>';
           $.each(inqData, function(keyInq,valueInq){ // INQ EACH

               if(valueInq.statusId == valueStatus.id){
                    var color = valueInq.actDetail.COLORCODE;
                    var sign = "";
                    var modalOpen = "actDetails(this)";
                    var activityMode = valueInq.actDetail.ACT_MODE;
                    
                    var actIdDt = "";
                    if( valueInq.actDetail.PK_ACT_ID != undefined ){
                        actIdDt ='<b>('+ valueInq.actDetail.PK_ACT_ID +')</b>';
                    }    

                    if( valueInq.actDetail == 'No record found' ){
                        sign = "fa fa-exclamation-triangle";
                        color = "red";
                    
                    }else if (activityMode == ACT_MODE_PHONE){
                    
                        sign = 'fa fa-phone';
                    }else if (activityMode == ACT_MODE_EMAIL){
                    
                        sign = 'fa fa-envelope';
                    }else if (activityMode == ACT_MODE_SMS){
                    
                        sign = 'fa fa-paper-plane';
                    }else{
                    
                        sign = 'fa fa-male';
                    }

                    var bgColor = "";
                    if( valueInq.potgScore.score <= 30  ){
                        bgColor = "#ea3e3e";
                    }else if( valueInq.potgScore.score <= 60  ){
                        bgColor = "#F97600";
                    }else if( valueInq.potgScore.score <= 90  ){
                        bgColor = "#6787ea";
                    }else{
                        bgColor = "#5cb85c";
                    }

//                    var renderScoreCalc = "<table>";
//                    if( valueInq.potgScore != "" && valueInq.potgScore != "No record found" ){
//                        valueInq.potgScore.data.forEach( function( record , index ){
//                            renderScoreCalc += '<tr><td>'+record.lookupTypeName +'</td><td>'+ record.lookupTypeWaitage + ' |  </td><td>'+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].displayValue + ' - ' : "" ) +' '+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].lookWaitage +"" : " " ) +'</td></tr>'; 
//                        });
//                    }
//                    renderScoreCalc += "</table>";
                   
//                   var renderScoreCalc = "<div class='innerPopoverContent'><table border = '1' class='potgScoreTable display datatables-alphabet-sorting'><thead><tr><th>Type</th><th>Weightage</th><th>Type </th><th>Weightage</th></tr></thead>";
//                    if( valueInq.potgScore != "" && valueInq.potgScore != "No record found" ){
//                        valueInq.potgScore.data.forEach( function( record , index ){
//                            renderScoreCalc += '<tbody><tr><td>'+record.lookupTypeName +'</td><td>'+ record.lookupTypeWaitage + ' </td><td>'+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].displayValue + ' - ' : " - " ) +'</td><td> '+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].lookWaitage : " - " ) +'</td></tbody></tr>'; 
//                        });
//                    }
//                    renderScoreCalc += "</table></div>";
//                    
                    var renderScoreCalc = "<div class='innerPopoverContent'>";
                    if( valueInq.potgScore != "" && valueInq.potgScore != "No record found" ){
                        valueInq.potgScore.data.forEach( function( record , index ){
                            renderScoreCalc += '<div><div>'+record.lookupTypeName +'</div><div>'+ record.lookupTypeWaitage + ' |  </div><div>'+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].displayValue + ' - ' : "" ) +' '+ ( ( record.lookupTypeData != "" ) ? record.lookupTypeData[0].lookWaitage +"" : " " ) +'</div></div>'; 
                        });
                    }
                    renderScoreCalc += "</div>";
                    //console.log(renderScoreCalc);
                    var firstStepClass = (keyInq == 0 ? "firstStepClass" : "");
                    statusWiseHtmlMid += '<div class="dragg-com">'+
                                            '<div class="col-lg-12 col-md-12 col-sm-12 draggable-source curPoint '+firstStepClass+'" inqId="'+ valueInq.inqId +'" custId="'+ valueInq.custId +'" statId="'+ valueInq.statusId +'"><div class="row" inqId="'+ valueInq.inqId +'" custId="'+ valueInq.custId +'" statId="'+ valueInq.statusId +'" onclick="navigateToInquiryDetailOpen(this)"><div id="draggable" class="draggable">'+
                                                '<div class="col-md-12 col-sm-12"><div class="row"><p><i class="fa fa-user-circle" aria-hidden="true"></i> '+valueInq.custDetail.fName+' '+valueInq.custDetail.lName+'<a class="draggableMeter id="potgScoreCont" data-html="true" href="javascript:void(0);" data-toggle="popover" title="POTG Score" data-content="'+ renderScoreCalc +'" data-placement="left" style="color: #fff;background:'+ bgColor +'"><i class="fa fa-tachometer"></i>&nbsp;'+valueInq.potgScore.score+'</a></p></div></div>'+
                                                // '<div class="col-md-12 col-sm-12"><div class="row"><p><i class="fa fa-user-circle" aria-hidden="true"></i> '+valueInq.custDetail.fName+' '+valueInq.custDetail.lName+'<span class="draggableMeter" title="POTG Score" style="background:'+ bgColor +'"><i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;'+valueInq.potgScore.score+'</span></p></div></div>'+
                                                // '<div class="col-md-12 col-sm-12"><div class="row"><p><i class="fa fa-user-circle" aria-hidden="true"></i> '+valueInq.custDetail.fName+' '+valueInq.custDetail.lName+'  <i class="fa fa-mobile" aria-hidden="true"></i>&nbsp;'+valueInq.custDetail.custMobile+'</p></div></div>'+
                                                '<div class="col-md-12 col-sm-12"><div class="row"><p style="color: #6A6868; font-size:12px"><i class="fa fa-user-plus" aria-hidden="true"></i> '+valueInq.custDetail.cpName + ' '+(valueInq.custDetail.cpMobile ? '-'+valueInq.custDetail.cpMobile+'' : 'NA')+' </p></div></div>'+
                                                '<div class="col-md-12 col-sm-12"><div class="row"><p style="color: #6A6868; font-size:12px"><span class="inqInfo"><i class="fa fa-hashtag" aria-hidden="true"></i>&nbsp;'+valueInq.clientLevelInqId+'</span> <span class="inqInfo pull-right"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;'+mysqltoDesiredFormat(valueInq.inqDate,"dd-MM-yyyy")+'</span></p></div></div>'+//ADD BY DARSHAN ON 23-03-2018
                                                '<span style="color: #6A6868; font-size:12px">&#8377; '+numberFormat( valueInq.potAmt )+' &nbsp; <i class="fa fa-comment" aria-hidden="true"></i>&nbsp;'+valueInq.projectName+' </span>'+                                                                      
                                            '</div>'+                                            
                                            '</div>'+
                                            '<div class="notification-icon curPoint">'+
                                                    '<a title="Activity - '+( (valueInq.actDetail.ACT_TITLE ==  undefined) ? "No Activity" : valueInq.actDetail.ACT_TITLE )+'" id="actDet'+ valueInq.inqId +'" data-toggle="modal" data-original-title="Activity Details" actDue="'+ valueInq.actDetail.DUE_DATE +'" actTime="'+ valueInq.actDetail.DUE_TIME +'" actTitle="'+ valueInq.actDetail.ACT_TITLE +'" modalId="actDetailModal'+ valueInq.inqId +'" actId ="'+ valueInq.actDetail.PK_ACT_ID +'" cActId ="'+ valueInq.actDetail.C_ACT_ID +'" inqId="'+ valueInq.inqId +'"  onclick="'+ modalOpen +'">'+
                                                    // '<i class="'+ sign +'" style="color:'+ color +'; font-size: 24px"></i>'+
                                                        '<span class="fa-stack fa-lg text-white" style="font-size:18px;">'+
                                                          '<i class="fa fa-circle fa-stack-2x" style="color:'+ color +';"></i>'+
                                                          '<i class="fa '+ sign +' fa-stack-1x"></i>'+
                                                        '</span>'+
                                                    '</a>'+
                                                '</div>'+
                                            '<div id="actDetailModal'+ valueInq.inqId +'" class="triangle-isosceles top inqList">'+
                                                //'<div class="rmbox-header"><i class="fa fa-check-square-o"></i> Activity Details '+ actIdDt +'</div>'+
                                                '<div class="rmbox-body">'+
                                                    '<a href="javascript:void(0)" class="curPoint" onclick=""><label id="actDetailModal'+ valueInq.inqId +'Body" class="curPoint"></label></a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div></div>';
                    potAmt = potAmt+parseFloat(valueInq.potAmt);  
                    totalDeals = totalDeals+1;  
               }
           });
           finalInqListingHtml += statusWiseHtmlHeader + statusWiseHtmlMid + statusWiseHtmlFooter;
           $("#totalValue_"+valueStatus.id).html("&#8377; " + numberFormat( potAmt ));
           $("#totalDeals_"+valueStatus.id).html(totalDeals + " Deals");
        });
        $("#draggable-wrapper").html(finalInqListingHtml);
        
        var overStat = "";
        var dropStat = "";
         inqId = "";
         oldStatId = "";
         newStatId = "";

        $(".draggable-source").draggable({ 
            cursor: "crosshair", 
            revert: "invalid",
            zIndex: 9999,
            helper:'clone',
            scroll: true,
            refreshPositions: true,
            containment : "#dragnDropArea",
            revert: function(dropped) {
                var dropId = dropped && dropped[0].id;
                dropStat = $('#'+dropId).attr('statPriority');
                var dropped = dropped && dropped[0].id == "status_"+overStat;
                // if( dropStat != undefined ){
                //     if( !dropped && dropStat < overStat ) {
                //         $(this).find("draggable").originalPosition = {top:0, left:0}
                //         $(this).appendTo($(this).data('originalParent'));
                //         toastr.warning('You cannot set previous status');
                //     }
                // }
                // return !dropped;
            } 
        });
        
        $(".fbox").droppable({ 
            accept: ".draggable-source", 
            tolerance:"pointer",
            drop: function(event, ui) {
                // toastr.success('Dropped'); return false;

                if( checkAuth(4,15) == -1 ){
                    toastr.error(NOAUTH);
                    return false;
                }

                $(this).removeClass("border").removeClass("over");
                context = $(this);
                
                var statRet = false;

                event = event;
                ui = ui;

                dropStat = $(this).attr('statpriority');
                newStatId = $(this).attr('statid');
                if( overStat == ""  || oldStatId == newStatId ){
                // if( dropStat < overStat || overStat == ""  || oldStatId == newStatId ){
                    return false;
                }else{
                    
                    var data = JSON.parse(localStorage.indeCampusRoleAuth);
                    var statusListing = data.data.statusListing;
                    var catName = "INQUIRY";
                    var statusInd = findIndexByKeyValue(data.data.statusListing,"statusCatName",catName);

                    var statusInnerInq = findIndexByKeyValue(data.data.statusListing[statusInd].statusListing,"thirdPartyFlag","1");
                    
                    var finalStat = data.data.statusListing[statusInd].statusListing[statusInnerInq].ID;
                    if( finalStat == newStatId ){
                        
                        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                        // Followup Listing Todays, Delayed, Upcomming
                        var postData = {
                            requestCase: 'getInqDetail',
                            clientId: tmp_json.data[0].FK_CLIENT_ID,
                            userId: tmp_json.data[0].PK_USER_ID,
                            orgId: checkAuth(4,14),
                            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                            inqId : inqId
                        }
                        commonAjax(COMMONURL, postData, inquiryDetailCallback,"Please Wait... Getting Inquiry Data");
                        function inquiryDetailCallback(flag,data){

                            if (data.status == "Success" && flag) {
                                var roleAuthData = JSON.parse(localStorage.indeCampusRoleAuth);
                                    
                                getData = data.data;
                                if( roleAuthData.data.inquiryBookingFlag == "1" && getData[0].inqBookFlag == "0"){
                                    toastr.warning("Please approve proposal before confirming inquiry");
                                    return false;
                                }

                                var typeItem = findIndexByKeyValue( getData[0].inqTransDetail , "typeOfItem" , "ITEM" );
                            
                                if( typeItem == "-1" ){
                                    toastr.error("Please add Items before moving Inquiry to final status");
                                    $('#updatePotAmt').modal('hide');
                                    return false;
                                }else{
                                    var currClientType = roleAuthData.data.clientTypeId;

                                    console.log(currClientType);
                                    if( currClientType == 34 ){
                                        if( data.data[0].pendingStallListing != "No record found" ){
                                            renderCartTransData(data.data[0].inqTransDetail);
                                        }else{
                                            toastr.error("Sorry..! No Pendings stalls are available of this Inquiry's Exhibition");
                                        }
                                    }else{
                                        renderCartTransData(data.data[0].inqTransDetail);
                                    }
                                }
                                localStorage.POTGSelectedHall = getData[0].hallId;
                            
                            } else {
                                if (flag)
                                    displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ_DATA );
                                else
                                    toastr.error(SERVERERROR);
                            }
                        }
                    }else{

                        var innerIndex = findIndexByKeyValue(data.data.statusListing[statusInd]['statusListing'],"remarkOnStatusFlag","1");
                    
                        if(innerIndex != "-1"){
                        
                            var statusCat = data.data.statusListing[statusInd].statusListing[innerIndex].ID;
                            var statustext = data.data.statusListing[statusInd].statusListing[innerIndex].VALUE;
                            if( newStatId == statusCat ){
                                showCancelRemark(statusCat,statustext);
                                return false;
                            }
                        }

                        callAjax(1); 
                    }
                }
                return true;
            }, 
            over: function(event, elem) {
                $(this).addClass("over");
                // console.log("over");
            },
            out: function(event, elem) {
                $(this).removeClass("out");
                // console.log("out");
                overStat = $(this).attr('statPriority');
                inqId = $(elem.draggable[0]).attr('inqid');
                oldStatId = $(elem.draggable[0]).attr('statid');
            },   
        }).sortable({
            disabled: true
        });
    } else { 
        $("#statusListing").html(html); 
        $("#draggable-wrapper").html("");
        $("#bottom-row").html(wonHtml + lostHtml); 
    }

    $('[data-toggle="popover"]').popover({
        trigger : 'hover',
        html:true
    });

    $("#loader-wrapper").hide();
    $("body").addClass("loaded");
}

function renderCartTransData( inqTransData ){

    var itemList = "";
    newCartData = inqTransData;
    
    inqTransData.forEach( function( record , index ){
        
        if( record.typeOfItem == "ITEM" ){

            itemList += '<tr>' +
                            '<td>' + (index+1) + '</td>' +
                            '<td> <div class="checkboxer"><input type="checkbox" index="'+ index +'" class="cartChk" itemId="'+ record.itemId +'" id="'+ record.itemId +'_'+index+'"><label for="'+ record.itemId +'_'+index+'">'+ record.itemName +'</label></div> </td>' +
                            '<td><input type="text" value=' + record.itemQty + ' onchange="calCartItem('+ index +','+ record.itemId +',this)" itemIdKey="'+ record.itemId +'_'+index+'" id="qty_'+ record.itemId +'_'+index+'" class="form-control onlyNumber" disabled></td>' +
                            '<td><input type="text" value=' + record.itemBasePrice + ' onchange="calCartItem('+ index +','+ record.itemId +',this)" itemIdKey="'+ record.itemId +'_'+index+'" id="price_'+ record.itemId +'_'+index+'" class="form-control onlyNumber" disabled></td>' + 
                            '<td><input type="text" value=' + record.itemPrice + ' inpType="total" onchange="calCartItem('+ index +','+ record.itemId +',this)" itemIdKey="'+ record.itemId +'_'+index+'" id="totalPrice_'+ record.itemId +'_'+index+'" class="form-control onlyNumber" disabled></td>' +
                        '<tr>';
        }else if( record.typeOfItem == "GROUP" ){
            itemList += '<tr>' +
                            '<td>' + (index+1) + '</td>' +
                            '<td>' + record.groupName + '</td>' +
                            '<td></td>' +
                            '<td></td>' + 
                            '<td></td>' +
                        '<tr>';
        }else if( record.typeOfItem == "SECTION" || record.typeOfItem == "BOM" ){
            itemList += '<tr>' +
                            '<td>' + (index+1) + '</td>' +
                            '<td>' + record.itemName + '</td>' +
                            '<td></td>' +
                            '<td></td>' + 
                            '<td></td>' +
                        '<tr>';
        }
    });

    $('#itemListBodyCart').html(itemList);
    $('#cartTotal').html( 0 );

    $('#dealWonRemarkId').selectpicker('refresh');
    GBLheaderLevelData = [];
    $('#dtTableHeader').html('');
    $('#dealWonRemark').val('');
    $('#cartDataModal').modal('show');

    GBLsetDiscountData();   // TAXATION MODAL

    addFocusId( 'dealWonRemarkId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT

    $('.cartChk').change(function(){

        cartTotalAmt = 0;
        var check = $(this).is(':checked');
        var newItemId = $(this).attr('id');
        var actItemId = $(this).attr('itemId');
        var index = $(this).attr('index');

        if( check ){

            $('#qty_'+newItemId).attr('disabled',false);
            $('#price_'+newItemId).attr('disabled',false);

            newCartData[index].deleteFlag = "0";

        }else{

            $('#qty_'+newItemId).attr('disabled',true);
            $('#price_'+newItemId).attr('disabled',true);
            $('#totalPrice_'+newItemId).attr('disabled',true);

            if( newCartData.length != 0 ){
                if( index != "-1" ){
                    // newCartData.splice( index , 1 );
                    newCartData[index].deleteFlag = "1";
                }
            }
        }

        newCartData.forEach(function( record , index ){
            if( record.deleteFlag != "1" ){
                cartTotalAmt = parseFloat( cartTotalAmt ) + parseFloat( record.itemPrice );
            }
        });

        $('#cartTotal').html( numberFormat( cartTotalAmt ) );
    });

    $('.cartChk').click();

    $('.onlyNumber').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 43 ) {
                   return false;
        }
    });
}


function calCartItem(index,itemId,context){

    var inpType = $(context).attr('inpType');
    // var index = findIndexByKeyValue( newCartData , "itemId" , itemId );

    if( inpType == "total" ){
        var totPrice = $('#totalPrice_'+itemId).val();
        newCartData[index].itemPrice = parseFloat( totPrice );
    }else{

        var itemIdKey = $(context).attr('itemIdKey');
        var qty = $('#qty_'+itemIdKey).val();
        var price = $('#price_'+itemIdKey).val();

        if( qty == 0 || qty == "" ){
            toastr.warning('Please Enter Valid Qty');
            $('#qty_'+itemIdKey).focus();
            return false;
            
        }else if( price == 0 || price == "" ){
            toastr.warning('Please Enter Valid Price');
            $('#price_'+itemIdKey).focus();
            return false;
        
        }else if( index == "-1" ){
            toastr.warning('Item is not found in cart');
            $('#qty_'+itemIdKey).focus();
            return false;
        }

        var totPrice = parseFloat( qty ) * parseFloat( price );

        $('#totalPrice_'+itemIdKey).val( parseFloat( totPrice ) );
        
        newCartData[index].itemQty = parseFloat( qty );
        newCartData[index].itemBasePrice = parseFloat( price );
        newCartData[index].itemPrice = parseFloat( totPrice );

        var cartTotalAmt = 0;
        newCartData.forEach(function( record , index ){
            if( record.deleteFlag != "1" ){
                cartTotalAmt = parseFloat( cartTotalAmt ) + parseFloat( record.itemPrice );
            }
        });
        $('#cartTotal').html( numberFormat( cartTotalAmt ) );
    }
}


function callAjax(type,statType){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'updateInqStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(20,83),
        inqId : inqId,
        newStatusId : newStatId,
        currentStatusId : oldStatId,
        //statusRemark : "",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }

    if( type == "0" ){

        var tempOrderCart = [];
        newCartData.forEach( function( record , index ){
            if( record.deleteFlag == "0" ){
                tempOrderCart.push( record );
            }
        });

        postData.orderCart = tempOrderCart;
        postData.custId = getData[0].custDetail.customerId;

        var dealWonRemarkId = $('#dealWonRemarkId').val();
        var dealWonRemarkText = $('#dealWonRemark').val();
        
        if( tempOrderCart.length == "0" ){
            toastr.warning('Please Select atleast one item to confirm Inquiry');
            return false;

        }else if( dealWonRemarkId == "-1" ){
            toastr.warning('Please Select Deal Won Reason');
            $('#dealWonRemarkId').focus();
            return false;
            
        }else if( dealWonRemarkText == "" ){
            toastr.warning('Please Enter Deal Won Remark');
            $('#dealWonRemark').focus();
            return false;
        }

        postData.taxDiscDetail = GBLheaderLevelData;
        postData.dealWonRemarkId = dealWonRemarkId;
        postData.dealWonRemarkText = dealWonRemarkText;
    }

    if(  statType == "cancel" ){

        postData.cancelRemarkId = cancelRemarkData[0].cancelRemarkId;
        postData.cancelRemarkText = cancelRemarkData[0].cancelText;
    }   

    console.log(postData);
    // return false;
    commonAjax(COMMONURL, postData, function(flag,data){

        if(data.status == "Success"){
            
            toastr.success('Your Inquiry Status Updated Successfully');
            $('#cartDataModal').modal('hide');
            
            var dropped = ui.draggable;
            var droppedOn = context;
            //$(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn).removeClass('draggable');  
            $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 

            var respomseData = data;
            
            var data = JSON.parse(localStorage.indeCampusRoleAuth);
            var currClientType = data.data.clientTypeId;

            console.log(currClientType);
            
            var statData = [];
            var statusListing = data.data.statusListing;
            var catName = "INQUIRY";
            var statusInd = findIndexByKeyValue(data.data.statusListing,"statusCatName",catName);
            statData = data.data.statusListing[statusInd].statusListing;
            if( currClientType == 34 || currClientType == 32 ){
                var statusInnerInq = findIndexByKeyValue(data.data.statusListing[statusInd].statusListing,"thirdPartyFlag","1");
                
                var finalStat = data.data.statusListing[statusInd].statusListing[statusInnerInq].ID;
                if( finalStat == newStatId ){
                    if( respomseData.data.exhibId != "No record found" ){

                        if( currClientType == 32 ){
                            navigateToRealEstateBookInq( respomseData.data.exhibId , inqId , respomseData.data.clientLevelInqId );
                        }else{
                            localStorage.exhibitionId = respomseData.data.exhibId;
                            localStorage.POTGinqIdForStallBook = inqId;
                            navigateToCreateStallsWithExhbId();
                        }

                    }else{
                        
                        if( currClientType == 32 ){
                            toastr.warning("This is not a Real Estate Inquiry");
                        }else{
                            toastr.warning("This is not an Exhibition Inquiry");
                        }
                        
                    }
                }
            }

            // getinquiryListing();

            var index = findIndexByKeyValue( userWiseInqData , "inqId" , inqId );

            if( index != "-1" ){
                userWiseInqData[index].statusId = newStatId;

                var statIndex = findIndexByKeyValue( statData , "ID" , newStatId );
                if( statIndex != "-1" ){
                    userWiseInqData[index].statusPriority = statData[statIndex].statusPriority;
                }

                getUserInq(userWiseInqData);
            }


        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_FAILED_UPDATE_INQ );
        }
    },"Please Wait... Changing the Status Of Inquiry");
}

function actDetails(context){

    var modal = $(context).attr('modalId');  
    var actId = $(context).attr('actId');  
    var cActId = $(context).attr('cActId');  
    var inqId = $(context).attr('inqid');  
    var actTitle = $(context).attr('actTitle');  
    var actTime = $(context).attr('actTime');  
    var actDue = $(context).attr('actDue');  

    $('.active').hide('250');
    $('.inqList').removeClass('active');
    $('#actDetailModal'+inqId).addClass('active');

    if( actId != "undefined" ){//content available
        var html="<div class='inq-remark'><span class='remark-title pull-left text-left'>"+ "<span class='actid'>"+ cActId +" </span> "+ actTitle+"<hr></span><small class='text-right pull-right text-muted remark-date'>"+actDue+"<span class='date-info'>"+actTime+" </span></small></div>";
        $('#'+ modal).slideToggle('fast');
        $('#'+ modal + 'Body').attr('data-activityid',actId);
        $('#'+ modal + 'Body').attr('data-inqid',inqId);
        $('#'+ modal + 'Body').attr('onclick','navigateToActivityDetailsOpen(this)');
        $('#'+ modal + 'Body').html(html);
    }else{//content available  Create Activity
        var html="<h4 class='panel-title'><i class='fa fa-plus'></i> Activity </h4><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'><small class='text-right'>  </small></div></div>";
        $('#'+ modal).slideToggle('fast');
        $('#'+ modal + 'Body').attr('data-activityid',actId);
        $('#'+ modal + 'Body').attr('data-inqid',inqId);
        $('#'+ modal + 'Body').attr('onclick','navigateAndOpenActivity(this)');
        $('#'+ modal + 'Body').html( html );
    }

    // event.stopPropagation();
}

function getInqListingByStatus(id){
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqDetailByStatus',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,14),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        statusId : id
    }

    commonAjax(COMMONURL, postData, inquiryListCallback,"Please Wait... Getting Inquiry Data");
}

function navigateAndOpenActivity(context){
    if (checkAuth(2, 5) == -1) {
        toastr.error(NOAUTH);
        return false;
    }
    else {
        localStorage.POTGinqIdOpenPop = $(context).attr('data-inqid');
        localStorage.actCreateRefType = "INQ";

        navigateToactivityOpen();
        // window.open("activity.html");
        // window.location.href = "activity.html";
    }
}


function searchInquiry() {

    var inquiryData = [];
    inquiryData = [{
        inqList : userWiseInqData
    }];

    $('#inquirySearch').keyup(function () {
        var searchField = (($('#inquirySearch').val()).trim()).toLowerCase();
        if( searchField != "" ){
            
            var regex = new RegExp(searchField, "i");
            searchArr = [];
            $.each(userWiseInqData, function (key, val) {
                // if (val.inqAssignToName.search(regex) != -1 || val.CustomerInfo.cityName.search(regex) != -1 || val.CustomerInfo.stateName.search(regex) != -1 || val.CustomerInfo.catchmentArea.search(regex) != -1 || val.CustomerInfo.Members[0].userName.search(regex) != -1 || val.CustomerInfo.mobNum.search(regex) != -1) {
                if( val.custDetail.custMobile != null ){
                    // if ( val.inqAssignToName.search(regex) != -1 || val.projectName.search(regex) != -1 || val.custDetail.fName.search(regex) != -1 || val.custDetail.lName.search(regex) != -1 || val.custDetail.custMobile.search(regex) != -1 ) {
                    //     searchArr.push(val);
                    // }
                    if ( ((val.inqAssignToName).trim()).toLowerCase().includes( searchField ) || ((val.projectName).trim()).toLowerCase().includes( searchField ) || ((val.custDetail.fName).trim() +' '+(val.custDetail.lName).trim()).toLowerCase().includes( searchField ) || ((val.custDetail.custMobile).trim()).includes( searchField ) ) {
                        searchArr.push(val);
                    }
                }
            });
            getUserInq(searchArr);
        }else{
            getUserInq(userWiseInqData);
        }

    });
}

function showCancelRemark(statusCat,statustext){

    var statusArr = JSON.parse(localStorage.indeCampusRoleAuth);
    //var remarksArr = statusArr.data.cancelRemark;
    $("#cancelRemark").attr("statusId",statusCat);
    $("#cancelRemark").attr("statusName",statustext);
    setOption("0", "cancelRemarkId", statusArr.data.cancelRemark, "-- Select Cancel Remark --");
    $("#cancelRemarkId").selectpicker("refresh");

    $('#cancelRemark').val('');
    $("#remarkOnstatus").modal("show");
    addFocusId( 'cancelRemarkId' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}


function saveCancelRemarkAndStatus(){

    var statusId = $("#cancelRemark").attr("statusId");
    var statusName = $("#cancelRemark").attr("statusName");
    var cancelRemarkId  = $("#cancelRemarkId").val();
    var cancelText = $('#cancelRemark').val();

    if( cancelRemarkId == "-1" ){
        toastr.warning('Please Select Cancel Remark Id');
        $("#cancelRemarkId").focus();
        return false;

    }else if( cancelText == "" ){
        toastr.warning('Please Enter Cancel Remark');
        $("#cancelRemark").focus();
        return false;
    }

    $('#statNameList').html(statusName);
    localStorage.POTGinqStatus = statusId;
    var tempData = {
        cancelRemarkId : cancelRemarkId,
        cancelText : cancelText,
        statusId : statusId,
        statusName : statusName
    };
    cancelRemarkData.push(tempData);
    callAjax( "1" , "cancel" );
    $("#remarkOnstatus").modal("hide");
}

function pageInitialEvents(){
    
    $('.fab-dial-handle').click(function () {
        $('.fabdail-buttons').toggleClass('active');
    });

    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    console.log(currClientType);
    if( currClientType == 34 ){
        $('#inquiryRocket').hide();
    }else if( currClientType == 35 ){
        $('#inquiryRocket').hide();
    }else if( currClientType == 32 ){
        $('#inquiryRocket').hide();
    }

    var statusArr = JSON.parse(localStorage.indeCampusRoleAuth);
    setOption("0", "dealWonRemarkId", statusArr.data.dealWonReason, "-- Select Deal Won Remark --");
    $('#dealWonRemarkId').selectpicker('refresh');
    
    addFocusId( 'inquirySearch' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
}

function navigateToInquiryDetailOpen(context){
    
    if (checkAuth(4, 14, 1) == -1) {
        toastr.error(NOAUTH);
    }
    else {
        var inqId = $(context).attr('inqId');
        var custId = $(context).attr('custId');
        
        localStorage.POTGclickInqId = inqId;
        localStorage.POTGcustId = custId;
        
        window.open("inquirydetail.html");
    }
}

function selectAllFun(){

    var allPages = oTable.fnGetNodes();
    $('.icheck', allPages).prop('checked',true);
}

function deselectAllFun(){
    var allPages = oTable.fnGetNodes();
    $('.icheck', allPages).prop('checked',false);
}

function openTransferPop(){

    $('#page2').hide();
    $('#page3').hide();
    $('#page1').show();
    $('#transferUserModal').modal('show');
    resetTransferInq();
}

function showPages(pageId,type){

    var sourceUser = $('#toUser').val();
    var destGrp = $('#fromUser').val();
    var groupList = '';

    if( sourceUser == "-1" ){
        toastr.warning('Please Select Source User');
        return false;

    }else if( destGrp == "-1" ){
        toastr.warning('Please Select Destination User');
        return false;

    }else if( destGrp == sourceUser ){
        toastr.warning('Source User and Destination User are same');
        return false;
    }
    var currCheck = false;
    $('.icheck').each(function(){
        if($(this).prop('checked')){
            currCheck = true; 
        }
    });

    if(pageId == "page3" && !currCheck){
        toastr.warning('Please Select Inquiry');
        return false;
    }

    if(pageId == "page2"){  

      var sourceUserId = $('#toUser').val();
        
        if( sourceUserId != "-1"){

            if(type == 'next'){

                var count = 0;
                //console.log(updatedInqList);
                if( userWiseInqData != "No record found" ){

                    groupList +='<table class="table table-hover" id="transferTableList">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th>#</th>'+
                                                '<th>Inquiry Id</th>'+
                                                '<th>Inquiry Name</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody id="toUserList">';

                    for( var i=0; i<userWiseInqData.length; i++ ){

                        if( userWiseInqData[i].inqAssignToId == sourceUserId ){

                            count = count + 1;
                            groupList += '<tr>'+
                                            '<td>'+ count +'</td>'+
                                            '<td><div class="checkboxer">'+ 
                                            '<input type="checkbox" class="checkbox icheck" id="'+userWiseInqData[i].inqId+'"><label for="'+ userWiseInqData[i].inqId +'">' + userWiseInqData[i].clientLevelInqId + 
                                            '</label></div>'+
                                            '</td>'+
                                            '<td>'+ userWiseInqData[i].projectName +'</td>'+
                                        '</tr>';
                        }
                    }
                            
                    groupList +='</tbody></table>';
                    $('#InqDivTbl').html(groupList);

                    oTable = $('#transferTableList').dataTable({
                        stateSave: true,
                        "stateSave": true
                    });
                }else{
                    toastr.warning("No inquiry available");
                    return false;
                }

            }else{
                $('#page1').hide();
                $('#page2').hide();
                $('#page3').hide();
                $('#'+pageId).show();
            }
        }
    }
    $('#page1').hide();
    $('#page2').hide();
    $('#page3').hide();
    $('#'+pageId).show();
}

function transferUserInquiry() {

    var sourceUser = $('#toUser').val();
    var destUser = $('#fromUser').val();
    var userIds="";
    var allPages = oTable.fnGetNodes();
    
    $('.icheck', allPages).each(function(){
       var currCheck = $(this).prop('checked');
       var checkId = $(this).attr('id');

       if( currCheck ){
            if( userIds == ""){
               userIds += checkId;
            }else{
               userIds += ','+checkId;
            }
       } 
    });
    console.log(userIds);

    if( sourceUser == "-1" ){
        toastr.warning('Please Select Source User');
        return false;
    
    }else if( destUser == "-1" ){
        toastr.warning('Please Select Destination User');
        return false;

    } else if( destUser == sourceUser ){
        toastr.warning('Source User and Destination User are same');
        return false;
    }
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase: 'transferInquiry',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(4,13),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        oldUserId : sourceUser,
        newUserId : destUser,
        InqIds : userIds

    }
    commonAjax(FOLLOWUPURL, postData, transferCustGroupCallback,"Please Wait... Getting Dashboard Detail");

    function transferCustGroupCallback(flag,data){

        if (data.status == "Success" && flag) {
                console.log(data);
                toastr.success('Inquiries are Transferred Successfully..!');
                $('#transferUserModal').modal('hide');
                getinquiryListing();
        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_FAILED_TRANSFER_INQ );
            else
                toastr.error(SERVERERROR);
        }
    }
}

function resetTransferInq(){

    $('#toUser').selectpicker('val' , '-1');
    $('#fromUser').selectpicker('val' , '-1');
}

var GBL_TOUR_STEPS = [];
// INITIALIZE TOUR - ADDED ON 08-MAY-2018 BY VISHAKHA TAK
function initSelfTour() {

    GBL_TOUR_STEPS = [
        {
            path: "",
            element: ".draggable-detail",
            title: "Inquiries",
            content: "List of inquiries according to the status priority!!",
            placement: 'top'
        },
        {
            path: "",
            element: ".firstStepClass",
            title: "Inquiry",
            content: "Details regaring the inquiry !!",
            placement: 'right',
            onNext:function(tour){
                $('.firstStepClass div')[0].click(); // OPEN INQUIRY DETAIL
            }
        },
        {
            path: "", // REDIRECTS TO THIS PATH
            element: ".firstStepClass",
            title: "This page tour end",
            content: "This page tour end",
            placement: 'top',
            onShown:function(tour){
                tour.end();
            }
        }
    ];
   
    // Instance the tour
    instanceTour();
    // tour._options.backdrop = false

    // Initialize the tour
    tour.init();

    tour.setCurrentStep(0); // START FROM ZEROth ELEMENT TO PREVENT FROM DISPLAYING LAST STEP

    // Start the tour
    tour.start(true);
    localStorage.POTGinqTourFlag = 1;
}

// FOR INQUIRY CREATE TOUR
function initCreateInqTour(){
    localStorage.POTGinqCreateTour = 1;
    navigateToCreateInquiry();
}