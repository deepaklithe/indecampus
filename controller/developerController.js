/*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-07-2018.
 * File : developerController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */


var selectedActivityIds = [];
var editSelectedActivityIds = [];
// GET ROLE LISTING DATA
function getRoleData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: "getrole",
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9,37),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    };

    commonAjax(FOLLOWUPURL, postData, getRoleDataCallback,"Please Wait... Getting Dashboard Detail");
}

// GET ROLE LISTING DATA CALLBACK 
function getRoleDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);
     
        var newRow = '';
        var count = 1 ;
        if(data.data != 'No record found'){
            for( var i=0; i<data.data.length; i++ ){

                count = i + 1;
                newRow += '<tr>'+
                                '<td>'+ count +'</td>'+
                                '<td>'+ data.data[i].ROLE_NAME +'</td>'+
                                '<td>'+ data.data[i].MODULE_NAME +'</td>'+
                                '<td>'+ data.data[i].ACTIVITYNAME +'</td>'+
                                '<td>'+
                                    '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple" id="'+ data.data[i].FK_ROLE_ID +'" onclick="editRoleDetail(this)"><i class="fa fa-pencil"></i></a>'+
                                    '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-ripple btn-danger" id="'+ data.data[i].FK_ROLE_ID +'" onclick="deleteRole(this);"><i class="fa fa-trash-o"></i></a>'+
                                '</td>'+
                            '</tr>';
            }

            $('#moduleList').html(newRow);

        }else{
            displayAPIErrorMsg( data.status , GBL_ERR_ROLE_LIST );
        }
        
        TablesDataTables.init();
        TablesDataTablesEditor.init();

        getRoleListing();

    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ROLE_LIST );
        else
            toastr.error(SERVERERROR);
    }
}

// GET ROLE PERMISSION LISTING
function getRoleListing(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getPermissionListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(3,10),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(COMMONURL,postData,displayRoleListing,RETRIEVINGMODULEDETAIL);
}

// GET ROLE PERMISSION LISTING CALLBACK - DISPLAY ROLE PERMISSION
function displayRoleListing(flag,data){
    if (data.status == "Success" && flag) {

        data = data.data;
        //MODULE_NAME ACTIVITY_NAME PK_MODULE_ID PK_ACTIVITY_ID

        var start = '<optgroup label="**">';
        var end = '</optgroup>';
        var option = '';
        var innerHTML = ' ';
        var mainHtml = '';
        for(var i=0;i<data.length;i++){

            if(localStorage.indeCampusRoleFlag == "true"){


                var tmp_start = start;
                tmp_start = tmp_start.replace("**",data[i].moduleName);
                innerHTML = tmp_start;

                var tmp_option = data[i].activityName.split(",");
                var tmp_option1 = data[i].activityId.split(",");

                for(var j=0;j<tmp_option.length;j++){
                    if(editSelectedActivityIds.indexOf(tmp_option1[j]) == -1){
                        innerHTML += '<option value="'+data[i].moduleId+'|'+tmp_option1[j]+'" id="'+data[i].moduleId+'|'+tmp_option1[j]+'">'+tmp_option[j]+'</option>';
                    }else{
                        selectedActivityIds.push(data[i].moduleId+'|'+tmp_option1[j]);
                        innerHTML += '<option selected value="'+data[i].moduleId+'|'+tmp_option1[j]+'" id="'+data[i].moduleId+'|'+tmp_option1[j]+'">'+tmp_option[j]+'</option>';
                    }

                }
                innerHTML += end;
                mainHtml +=innerHTML;

            }else{
                var tmp_start = start;
                tmp_start = tmp_start.replace("**",data[i].moduleName);
                innerHTML = tmp_start;

                var tmp_option = data[i].activityName.split(",");
                var tmp_option1 = data[i].activityId.split(",");
                for(var j=0;j<tmp_option.length;j++){
                    innerHTML += '<option value="'+data[i].moduleId+'|'+tmp_option1[j]+'" id="'+data[i].moduleId+'|'+tmp_option1[j]+'">'+tmp_option[j]+'</option>';
                }
                innerHTML += end;
                mainHtml +=innerHTML;
            }

        }

        $(".my_multi_select").html('<select multiple="multiple" class="multi-select" id="my_multi_select2" name="my_multi_select2[]">'+mainHtml+'</select>');
        
        multiSelectFunction();
        assignMultiselectEvent();


    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_PERMISSION_LIST );
        else
            toastr.error(SERVERERROR);
    }
}

// MULTI-SELECT ELEMENT CHANGE EVENT 
function assignMultiselectEvent(){

    $('#my_multi_select2').change(function () {
        selectedActivityIds = [];
        $('#my_multi_select2 option:selected').each(function () {
            selectedActivityIds.push($(this).attr("id"));
        });
        var uniqueNames = [];
        $.each(selectedActivityIds, function (i, el) {
            if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
        });
        selectedActivityIds = uniqueNames;

        log(selectedActivityIds);
    });
}

// CREATE / UPDATE ROLE
function saveRoleDetail(){

    var rolename = $("#rolename").val().trim();
    if(rolename.length <= 0 ){
        toastr.warning(ROLENAMEBLANK);
    } else if(selectedActivityIds.length <= 0){
        toastr.warning(ACTIVITYBLANK);
    } else {
        var role = [];
        var mainarray = [];
        for(var i=0;i<selectedActivityIds.length;i++){
            var tmp_data = [];
            var tmp = selectedActivityIds[i].split("|");

            tmp_data = {
                rolename : rolename,
                moduleid : tmp[0],
                activityid : tmp[1]
            }

            role.push(tmp_data);
        }
        mainarray = role;
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        var reqCase = "";
        var tmp_org="";
        if(localStorage.indeCampusRoleFlag == "true"){
            reqCase = "editrole";
            tmp_org=checkAuth(9,38);
        }else{
            reqCase = "createrole";
            tmp_org=checkAuth(9,36);
        }
        var postData = {
            requestCase : reqCase,
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID,
            orgId: tmp_org,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            role : mainarray
        };
        if(localStorage.indeCampusRoleFlag == "true")
            postData.roleId = localStorage.indeCampusRoleIdForEdit;

        // console.log(postData);
        // return false;
        commonAjax(COMMONURL,postData,callbackOfSaveRoleDetail,SENDINGROLEDETAILTOCREATE);

    }


}

// CREATE / UPDATE ROLE CALLBACK
function callbackOfSaveRoleDetail(flag,data){

    localStorage.indeCampusRoleFlag = "false";
    if (data.status == "Success" && flag) {
        //success
        console.log(data);
        location.reload()
    } else {
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_CREATE_UPDATE_PERMISSION );
        }
        else{
            toastr.error(SERVERERROR);
        }
    }

}

// GET SELECTED ROLE'S DATA FOR UPDATE
function editRole(){
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    var postData = {
        requestCase : "getroledetails",
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(9,37),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        roleId : localStorage.indeCampusRoleIdForEdit
    };
    commonAjax(COMMONURL,postData,editRoleCallback,RETRIEVINGROLEDETAIL);
}

function editRoleCallback(flag,data){
    if (data.status == "Success" && flag) {
        //success
        log(data);
        editSelectedActivityIds = [];
        for(var i=0;i<data.data.length;i++){
            var tmp = data.data[i].ACTIVITY.split(",");
            $("#rolename").val(data.data[i].ROLE_NAME);
            for(var j = 0; j<tmp.length;j++){
                editSelectedActivityIds.push(tmp[j]);
            }
        }
        setTimeout(function(){getRoleListing();},500);
    } else {
        if (flag)
            // toastr.error(data.status);
            displayAPIErrorMsg( data.status , GBL_ERR_ROLE_DETAIL );
        else
            toastr.error(SERVERERROR);
    }
}

// GET SELECTED ROLE ID FOR GETTING ROLE DETAIL FOR EDIT
function editRoleDetail(context){
    var roleid = $(context).attr("id");
    localStorage.indeCampusRoleIdForEdit = roleid;
    localStorage.indeCampusRoleFlag = true;
    editRole();
}

// DELETE ROLE
function deleteRole(context){

    var flag = confirm(CNFDELETE);
    if(flag){
        var roleid = $(context).attr("id");
        var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
        //delete role
        var postData = {
            requestCase : "deleterole",
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(9,39),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
            roleid : roleid
        };

        commonAjax(COMMONURL,postData,deleteRoleCallback,DELETEROLE);
    }
}

// DELETE ROLE CALLBACK
function deleteRoleCallback(flag,data){
    if (data.status == "Success" && flag) {
        //success
        console.log(data);
        location.reload();
    } else {
        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_ROLE_DELETE );
        else
            toastr.error(SERVERERROR);
    }
}