/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 03-01-2018.
 * File : dynamicReportController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var GBL_REPORT_DATA = [];   // ALL REPORT DATA
var GBL_EXCELJSON = [];    // JSON FOR EXCEL DOWNLOAD
var GBL_DYNAMIC_JSON = [];   // JSON WHEN SENDING / RUN REPORT
var GBL_REPORT_FILTER = [];   // JSON RUN REPORT FILTER 
var GBL_SUMMARY_FILTER = [];   // JSON RUN REPORT FILTER OF SUMMARY FIELDS
var GBL_RUN_REPORT = [];    // ALREADY RUN REPORT DATA
var GBL_SAVE_FLAG = "1";    // CHECK REPORT IS SAVED OR NOT
var SUMMARY_IN_EXCEL = "";    // TEMPRORY STORE SUMMARY DATA FOR EXCEL DOWNLOAD

// INITIALIZE THE FUNCTION AT THE PAGE LOAD TIME
function pageInitialEvents(){

    initCommonFunction();   // INITIALIZE THE COMMON FUNCTIONS
    getReportList();      // GET THE LIST OF THEAD OF ACTIVITY
}

// REPORT LISTING API CALL
function getReportList(){
 
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // API CALL FOR DYNAMIC REPORT
    var reportId = "";
    var reportRefId = "";
    if( localStorage.selectedDynamicReport != "" && localStorage.selectedDynamicReport != undefined && localStorage.selectedDynamicRefReport != "" && localStorage.selectedDynamicRefReport != undefined && localStorage.selectedDynamicReportMode != "" && localStorage.selectedDynamicReportMode != undefined ){
        reportId = localStorage.selectedDynamicReport;
        reportRefId = localStorage.selectedDynamicRefReport;
        if( localStorage.selectedDynamicReportType == "1" ){    // SYSTEM REPORT ON
            reportRefId = "";
        }
    }
    var postData = { 
        requestCase: 'getDynamicReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(54,227), 
        reportId: reportId, 
        reportRefId: reportRefId, 
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportListCallback,"Please Wait... Getting Dashboard Detail");
    //CALL BACK OF REPORT LISTING
    function getReportListCallback(flag,data){

        if (data.status == "Success" && flag) { 
 
            GBL_REPORT_DATA = data.data; 
            // REPORT SELECTPICKER INITIALIZATION
            setOptionWithRef("0", "reportList", GBL_REPORT_DATA, "---Select Report---", 'reportId', 'reportName');
            $('#reportList').selectpicker('refresh');
 
            if( localStorage.selectedDynamicReport != "" && localStorage.selectedDynamicReport != undefined && localStorage.selectedDynamicRefReport != "" && localStorage.selectedDynamicRefReport != undefined && localStorage.selectedDynamicReportMode != "" && localStorage.selectedDynamicReportMode != undefined ){
                if( localStorage.selectedDynamicReportMode == "1" ){    // DISPLAY
                    $('#mainTableHead').removeClass('col-md-9 col-lg-9 col-xs-9').addClass('col-md-12 col-lg-12 col-xs-12');
                    $('.editMode').hide();
                    $('.dispMode').show();
                    $('#reportList').val( localStorage.selectedDynamicReport ).selectpicker('refresh');
                    renderReportThead();    // RENDER SIDE BAR
                    saveAndRunDynamicReport('2');    // RUN REPORT DATA

                }else{  // SAVE REPORT
                    $('.editMode').show();
                    $('.dispMode').hide();
                    $('#reportList').val( localStorage.selectedDynamicReport ).selectpicker('refresh');
                    renderReportThead();    // RENDER SIDE BAR
                    saveAndRunDynamicReport('2');    // RUN REPORT DATA
                }
                if( localStorage.selectedDynamicReportMode == "1" && localStorage.selectedDynamicReportType != "1" ){
                    $('#summaryFilterBtn').show();
                }else{
                    $('#summaryFilterBtn').hide();
                }
            }else{
                    $('.editMode').show();
                    $('.dispMode').hide();
            }

        } else {

            GBL_REPORT_DATA = []; 
            setOptionWithRef("0", "reportList", GBL_REPORT_DATA, "---Select Report---", 'reportId', 'reportName');
            $('#reportList').selectpicker('refresh');

            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_DYNAMIC_REPORT );
            else
                toastr.error(SERVERERROR);
        }
    }
}

// SIDE BAR FIELD LISTING OF REPORT
function renderReportThead(){

    var reportId = $('#reportList').val();
    var reportName = $('#reportList option:selected').text();
    var sideBarHtml = "";
    if( reportId != "-1" && GBL_REPORT_DATA != "" && GBL_REPORT_DATA != "No record found" ){

        // GROUP FIELD JSON
        var groupFieldData = GBL_REPORT_DATA[0].field.reduce(function(result, current) {
            result[current.displayTableName] = result[current.displayTableName] || [];
            result[current.displayTableName].push(current);
            return result;
        }, {});

        sideBarHtml += "<div id='dynamicRecAccord'><h3> "+ reportName +" </h3><div>";
        // FIND THE REPORT INDEX FROM REPORT DATA
        var repIndex = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
        if( repIndex != "-1" ){ 
            
            // SET SAVE REPORT NAME FIELD DATA
            $('#reportNameAdd').val( GBL_REPORT_DATA[repIndex].userReportName );
            $('#repNameHtm').html( GBL_REPORT_DATA[repIndex].userReportName );

            // NORMAL FIELD RENDERING OF SIDE BAR
            if( GBL_REPORT_DATA[repIndex].field != "No record found" ){

                $.each(groupFieldData,function(key,val){  
                    sideBarHtml += '<b>'+ val[0].displayTableName +'</b><hr>';
                    val.forEach( function( record , index ){

                        var funName = "setThAndRendor( this , "+reportId+" , 1 )";

                        var iconCls = "";
                        if( (record.dbDataType).toLowerCase() == "text" || (record.dbDataType).toLowerCase() == "textarea" ){
                            iconCls = "text-width";
                        
                        }else if( (record.dbDataType).toLowerCase() == "date" ){
                            iconCls = "calendar";
                            
                        }else if( (record.dbDataType).toLowerCase() == "search" ){
                            iconCls = "search";
                            
                        }else if( (record.dbDataType).toLowerCase() == "dropdown" ){
                            iconCls = "caret-down";
                            
                        }else if( (record.dbDataType).toLowerCase() == "email" ){
                            iconCls = "envelope";
                            
                        }else if( (record.dbDataType).toLowerCase() == "rating" ){
                            iconCls = "star-o";
                            
                        }else if( (record.dbDataType).toLowerCase() == "int" || (record.dbDataType).toLowerCase() == "number" ){
                            iconCls = "sort-numeric-asc";

                        }else if( (record.dbDataType).toLowerCase() == "boolean" || (record.dbDataType).toLowerCase() == "checkbox" || (record.dbDataType).toLowerCase() == "radio" ){
                            iconCls = "toggle-on";
                        }

                        //LISTING GENERATION
                        sideBarHtml += '<div class="checkboxer">'+
                                            '<input type="checkbox" matchKey="'+ record.matchKey +'"  id="reportLi_'+ reportId +'_'+ record.matchKey +'" onchange="'+ funName +'"/>'+
                                            '<label for="reportLi_'+ reportId +'_'+ record.matchKey +'"><i class="fa fa-'+ iconCls +'"></i> '+ record.displayName +'</label>'+
                                        '</div>';
                        // sideBarHtml += '<li id="reportLi_'+ reportId +'_'+ record.keyId +'"><a href="javascript:void(0)" fieldName="'+ record.dbFieldName +'" tableName="'+ record.dbTableName +'" onclick="'+ funName +'">'+ record.keyName +'</a></li>';
                    });
                });

            }
            sideBarHtml += "</div>";

            // CUSTOM FIELD RENDERING OF SIDE BAR
            if( GBL_REPORT_DATA[repIndex].customTableFields != "No record found" ){
                GBL_REPORT_DATA[repIndex].customTableFields.forEach( function( record , index ){

                    sideBarHtml += "<h3>"+ record.displayName +"</h3><div>"; 
                    if(record.fieldsValues != "No record found"){ // START ADDED BY DARSHAN ON 28-02-2018
                        record.fieldsValues.forEach( function( innrRecord , innrIndex ){
                            var funName = "setThAndRendor( this , "+reportId+" , 2  )";

                            var iconCls = "";
                            if( (innrRecord.dbDataType).toLowerCase() == "text" || (innrRecord.dbDataType).toLowerCase() == "textarea" ){
                                iconCls = "text-width";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "date" ){
                                iconCls = "calendar";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "search" ){
                                iconCls = "search";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "dropdown" ){
                                iconCls = "caret-down";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "email" ){
                                iconCls = "envelope";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "rating" ){
                                iconCls = "star-o";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "int" || (innrRecord.dbDataType).toLowerCase() == "number" ){
                                iconCls = "sort-numeric-asc";

                            }else if( (innrRecord.dbDataType).toLowerCase() == "boolean" || (innrRecord.dbDataType).toLowerCase() == "checkbox" || (innrRecord.dbDataType).toLowerCase() == "radio" ){
                                iconCls = "toggle-on";
                            }

                            //LISTING GENERATION
                            sideBarHtml += '<div class="checkboxer">'+
                                                '<input type="checkbox" matchKey="'+ innrRecord.matchKey +'" dbTableName="'+ record.dbTableName +'"  id="reportLi_'+ reportId +'_'+ innrRecord.matchKey +'" onchange="'+ funName +'"/>'+
                                                '<label for="reportLi_'+ reportId +'_'+ innrRecord.matchKey +'"><i class="fa fa-'+ iconCls +'"></i> '+ innrRecord.displayName +'</label>'+
                                            '</div>';
                            // sideBarHtml += '<li id="reportLi_'+ reportId +'_'+ record.keyId +'"><a href="javascript:void(0)" fieldName="'+ record.dbFieldName +'" tableName="'+ record.dbTableName +'" onclick="'+ funName +'">'+ record.keyName +'</a></li>';
                        });
                        sideBarHtml += "</div>";
                    }else{
                        sideBarHtml += "</div>";
                    }// END ADDED BY DARSHAN ON 28-02-2018
                });
            }

            // ASSIGN DATA IN ONE OBJ TO ANOTHER BECAUSE OF DEEP / SHALLOW COPY ISSUE
            GBL_DYNAMIC_JSON = JSON.parse(JSON.stringify(GBL_REPORT_DATA[repIndex].field));

            renderTableTrans( GBL_REPORT_DATA[repIndex].userField , GBL_REPORT_DATA[repIndex].field );   // RENDER TRANS DATA FOR DEFAULT DATA
            resetFilters();
            
            if( GBL_REPORT_DATA[repIndex].userReportFilter != "No record found" ){
                 GBL_REPORT_FILTER = GBL_REPORT_DATA[repIndex].userReportFilter;
                 renderFilterList();
            }else{
                $('#filterContentDiv').html( "" );
                GBL_REPORT_FILTER = [];
            }
            GBL_SUMMARY_FILTER = [];
            GBL_RUN_REPORT = [];
        }   
    }
    if( reportId == "-1"){
        $('#dynamicReportList').html("");   // REMOVED THE HTML WHEN -1
        GBL_EXCELJSON = [];
    }

    //FINAL LISTING HTML GENERATION
    $('#actHeadUlList').html( sideBarHtml );

    $( "#dynamicRecAccord" ).accordion({collapsible: true});
}

// ADD / REMOVE SELECTED FIELD IN DYNAMIC LISTING 
function setThAndRendor( context , reportId , type ){   // TYPE - 1 -- NORMAL -- 2 -CUSTOME FIELDS

    var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
    var matchKey = $(context).attr('matchKey'); // GET THE MATCH KEY
    if( index != "-1" ){

        // FOR NORMAL FIELDS
        if( type == "1" ){
            // FIND THE MATCH KEY FROM REPORT FIEL DATA
            var innIndex = findIndexByKeyValue( GBL_REPORT_DATA[index].field , "matchKey" , matchKey );
            if( innIndex != "-1" ){
                var availIndx = findIndexByKeyValue( GBL_DYNAMIC_JSON , "matchKey" , matchKey );
                if( availIndx == "-1" ){ 
                    // toastr.success( '<b>'+ GBL_REPORT_DATA[index].keyMaster[innIndex].keyName +'</b> added in Table');
                    GBL_DYNAMIC_JSON.push( GBL_REPORT_DATA[index].field[innIndex] );
                }else{
                    // toastr.warning( '<b>'+ GBL_REPORT_DATA[index].keyMaster[innIndex].keyName +'</b> removed from Table');
                    GBL_DYNAMIC_JSON.splice( availIndx , 1 );
                }
                renderTableBody( reportId );
            }
        // FOR CUSTOME FIELDS
        }else{

            var dbTableName = $(context).attr('dbtablename');
            var innIndex = findIndexByKeyValue( GBL_REPORT_DATA[index].customTableFields , "dbTableName" , dbTableName );
            if( innIndex != "-1" ){
                var customIndx = findIndexByKeyValue( GBL_REPORT_DATA[index].customTableFields[innIndex].fieldsValues , "matchKey" , matchKey );
                if( customIndx != "-1" ){
                    var availIndx = findIndexByKeyValue( GBL_DYNAMIC_JSON , "matchKey" , matchKey );
                    if( availIndx == "-1" ){ 
                        // toastr.success( '<b>'+ GBL_REPORT_DATA[index].keyMaster[innIndex].keyName +'</b> added in Table');
                        GBL_DYNAMIC_JSON.push( GBL_REPORT_DATA[index].customTableFields[innIndex].fieldsValues[customIndx] );
                    }else{
                        // toastr.warning( '<b>'+ GBL_REPORT_DATA[index].keyMaster[innIndex].keyName +'</b> removed from Table');
                        GBL_DYNAMIC_JSON.splice( availIndx , 1 );
                    }
                    renderTableBody( reportId );
                }
            }
        }
    }
}

// RENDER THE TABLE BODY LISTING USING ID
function renderTableBody( reportId ){

    if( GBL_DYNAMIC_JSON != "" && reportId != "-1" ){
        
        // THEAD - TFOOT
        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>';

        var tbody = '<tbody id="itemList"><tr>';
        var tfoot = '</tbody></table>';

        
        GBL_DYNAMIC_JSON.forEach( function( record , index ){

            thead += '<th>'+ record.displayName +'</th>';
            tbody += '<td></td>';
            $('#reportLi_'+reportId+'_'+record.matchKey).prop('checked',false);   // REMOVE ACTIVE CLASS IN LI
        });

        thead += '</tr>';
        tbody += '</tr>';

        GBL_EXCELJSON = [];
        $('#dynamicReportList').html( thead  );

        if( localStorage.selectedDynamicReport != "" && localStorage.selectedDynamicReport != undefined && localStorage.selectedDynamicRefReport != "" && localStorage.selectedDynamicRefReport != undefined && localStorage.selectedDynamicReportMode != "" && localStorage.selectedDynamicReportMode != undefined ){
            if( localStorage.selectedDynamicReportMode == "1" ){    // DISPLAY
                $('#tableListing').dataTable({ 
                    "pageLength": 50, 
                });
            }else{    // EDIT
                $('#tableListing').dataTable({ 
                    "pageLength": 50,
                    "stateSave": true,
                    // "bPaginate": false,
                });
            }
        }else{
            $('#tableListing').dataTable({ 
                "pageLength": 50,
                "stateSave": true,
                // "bPaginate": false,
            });
        }

        // SHOW CHECKED REPORT FIELDS
        setTimeout( function(){
            GBL_DYNAMIC_JSON.forEach( function( record , index ){
                $('#reportLi_'+reportId+'_'+record.matchKey).prop('checked',true);   // ADD ACTIVE CLASS IN LI
            });
        },10);
    }else{
        $('#dynamicReportList').html( "" );
    }
}

// OPEN MODAL FOR SAVE REPORT NAME
function saveReportNameModal(){

    var reportId = $('#reportList').val();
    
    // VALIDATION STARTED
    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( GBL_DYNAMIC_JSON == "" ){
        toastr.warning('Please Generate Report');
        addFocusId( 'reportList' );
        return false;
    }

    modalShow('reportNameModal');
}

// CALL AN API OF SAVE AND RUN REPORT 
function saveAndRunDynamicReport( type ){   // 1 - SAVE - 2 - RUN REPORT

    var reportId = $('#reportList').val();
    var reportName = $('#reportList option:selected').text();

    // VALIDATION STARTED
    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( GBL_DYNAMIC_JSON == "" ){
        toastr.warning('Please Generate Report');
        addFocusId( 'reportList' );
        return false;
    } 

    var reportNameAdd = "";
    if( type == "1" ){
        reportNameAdd = $('#reportNameAdd').val();
        if( reportNameAdd == "" ){
            toastr.warning('Please Enter Report Name');
            $('#reportNameAdd').focus();
            return false;
        }
    }

    var refId = "";
    var reportTable = "";
    var aliasName = "";
    
    var keyMaster = [];
    var jointTable = [];
    var linkCustomTable = [];
    var tempReportFilter = [];
    var repIndex = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );

    // NORMAL USER SEND JSON LOOP ITERATED FOR GET THE JOINT FIELD INDEX
    GBL_DYNAMIC_JSON.forEach( function( record , index ){

        if( repIndex != "-1" ){

            refId = GBL_REPORT_DATA[repIndex].reportId;
            reportTable = GBL_REPORT_DATA[repIndex].reportTable;
            aliasName = GBL_REPORT_DATA[repIndex].reportAliasName;
            linkCustomTable = GBL_REPORT_DATA[repIndex].linkCustomTable;
            tempReportFilter = GBL_REPORT_DATA[repIndex].reportFilter;
 
            var jointFieldIndex = findIndexByKeyValue( GBL_REPORT_DATA[repIndex].jointField , "matchKey" , record.matchKey );

            var tempJField = [];
            if( jointFieldIndex != "-1" ){
                tempJField = GBL_REPORT_DATA[repIndex].jointField[jointFieldIndex];
            }
            var tempData = {
                jointField : tempJField,
                field : record
            }
            keyMaster.push( tempData );
        }
    }); 
    // console.log(keyMaster);return false;
    // ADD PERMANENT JOINT FIELD IN SEND / RUN JSON
    var tempFilterJoint = [];
    GBL_REPORT_DATA[repIndex].jointField.forEach( function( record , index ){
        var keyMasIndx = findIndexByKeyValue( record.jointTable , "jointType" , "1" ); 
        if( keyMasIndx != "-1" ){ 
            keyMaster.forEach( function( KMRecord , KMIndex ){
                if( KMRecord.jointField != "" ){
                    var KMInnrIndex = findIndexByKeyValue( KMRecord.jointField.jointTable , "jointFieldId" , record.jointTable[keyMasIndx].jointFieldId );
                    if( KMInnrIndex == "-1" ){

                        if( tempFilterJoint != "" ){

                            tempFilterJoint.forEach( function( tempFilRec , tempFilInd ){
                                if( findIndexByKeyValue( tempFilRec.jointField.jointTable , "jointFieldId" , record.jointTable[keyMasIndx].jointFieldId ) == "-1" ){
                                // if( tempFilRec.jointField.jointKeyId != record.jointKeyId ){
                                    var tempData = {
                                        jointField : record, 
                                    }
                                    tempFilterJoint.push( tempData );
                                }
                            });
                        }else{
                            var tempData = {
                                jointField : record, 
                            }
                            tempFilterJoint.push( tempData );
                        }
                    }
                }
            });
        }
    });

    // PERMANENT FILTER IN KEY MASTER JSON
    if( tempFilterJoint != "" ){
        tempFilterJoint.forEach( function( record , index ){
            var temp = {
                jointField : record.jointField,
                field : {},
            }
            keyMaster.push( temp );
        })
    }
    // console.log(keyMaster);return false;
    var reqCase = "";
    var checkAuthKey = "";
    if( type == "1" ){  // 1- CREATE UPDATE
        reqCase = "createUpdateDynamicReport";
        checkAuthKey = checkAuth(54,226);
    }else{
        reqCase = "runDynamicReport";
        checkAuthKey = checkAuth(54,227);
    }



    var GBL_FINAL_FILTER = [];
    GBL_FINAL_FILTER = JSON.parse(JSON.stringify( GBL_REPORT_FILTER ));
    // if( GBL_REPORT_DATE_FILTER != "" && GBL_REPORT_DATE_FILTER != "No record found" ){
    //     GBL_FINAL_FILTER[ GBL_FINAL_FILTER.length ] = JSON.parse(JSON.stringify( GBL_REPORT_DATE_FILTER ));   // ADDED REPORT DATE FILTER IN ACTUAL FILTER JSON
    // }

    var createReportFlag = "0";
    if( localStorage.selectedDynamicReport != "" && localStorage.selectedDynamicReport != undefined && localStorage.selectedDynamicRefReport != "" && localStorage.selectedDynamicRefReport != undefined && localStorage.selectedDynamicReportMode != "" && localStorage.selectedDynamicReportMode != undefined ){
        if( localStorage.selectedDynamicReportType == "1" ){    // SYSTEM REPORT ON
            if( tempReportFilter != "No record found" ){
                tempReportFilter.forEach( function( record , index ){
                    var indexFilter = findIndexByKeyValue( GBL_FINAL_FILTER , "filterId" , record.filterId );
                    if( record.filterType == "1" && indexFilter == "-1" ){
                        GBL_FINAL_FILTER.push( record );
                    }
                });
            }

            createReportFlag = "1";
        }

        if( localStorage.selectedDynamicReportMode == "2" ){    // EDIT MODE
            createReportFlag = "1";
        }
    }

    GBL_RUN_REPORT = [];
    setTimeout(function(){
        addRemoveLoader(1); // LOADER ADDED
    },10);
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = { 
        requestCase: reqCase,
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuthKey, 
        reportName : reportName, 
        saveReportName : reportNameAdd, 
        createReportFlag : createReportFlag, 
        // reportId : reportId, 
        refId : refId, 
        reportRefId : localStorage.selectedDynamicRefReport, 
        reportTable : reportTable, 
        aliasName : aliasName, 
        reportType : "2",   // 1 - DEFAULT - 2 - DYNAMIC REPORT 
        keyMaster : keyMaster,
        reportFilter : GBL_FINAL_FILTER,
        linkCustomTable : linkCustomTable,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, saveAndRunDynamicReportCallback,"Please Wait... Getting Dashboard Detail");
    function saveAndRunDynamicReportCallback(flag,data){

        if (data.status == "Success" && flag) { 

            modalHide('reportNameModal');
            if( type == "1" ){
                toastr.success( reportName +' Report Generated Successfully');
                GBL_RUN_REPORT = data.data;
                renderTableBodyWithData( data.data[0].userSaveReport );
                GBL_SAVE_FLAG = "1";
            }else{
                toastr.success( reportName +' Report Run Successfully');
                GBL_RUN_REPORT = data.data;
                renderTableBodyWithData( data.data );
                GBL_SAVE_FLAG = "0";
            }
            addRemoveLoader(0); // LOADER REMOVED

        } else {
            addRemoveLoader(0); // LOADER REMOVED
            renderTableBodyWithData( [] );
            var errMsg = GBL_ERR_NO_REPORT_DATA_DYNAMIC;
            if( type == "1" ){  // 1- CREATE UPDATE
                errMsg = GBL_ERR_CREATE_REPORT_DATA_DYNAMIC;
            }else{
                errMsg = GBL_ERR_NO_REPORT_DATA_DYNAMIC;
            }
            if (flag)
                displayAPIErrorMsg( data.status , errMsg );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// COPY THE ALREADY SELECTED USER'S FIELD FOR PARTICULAR TABLE
function renderTableTrans( userFieldData , masterData ){

    var reportId = $('#reportList').val();
    if( userFieldData != "No record found"){
        
        // ASSIGN DATA IN ONE OBJ TO ANOTHER BECAUSE OF DEEP / SHALLOW COPY ISSUE
        GBL_DYNAMIC_JSON = JSON.parse(JSON.stringify( userFieldData ));
        // GBL_DYNAMIC_JSON = userFieldData;
    }else{
        // ASSIGN DATA IN ONE OBJ TO ANOTHER BECAUSE OF DEEP / SHALLOW COPY ISSUE
        GBL_DYNAMIC_JSON = JSON.parse(JSON.stringify( masterData ));
        // GBL_DYNAMIC_JSON = masterData;
    }
    
    renderTableBody( reportId );
}

// RENDER THE TABLE WITH DATA AFTER RUN REPORT
function renderTableBodyWithData( reportData ){

    addRemoveLoader(1);
    if( reportData != "" && reportData != "No record found" ){

        var reportName = $('#reportList option:selected').text();
        
        // THEAD - TFOOT
        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>';
        var tbody = '<tbody id="itemList">';
        var tfoot = '</tbody></table>';

        GBL_DYNAMIC_JSON.forEach( function( record , index ){
            thead += '<th>'+ record.displayName +'</th>';
        });

        thead += '</tr>';

        // TBODY
        reportData.forEach( function( record , index ){  

            tbody += '<tr>';
            GBL_DYNAMIC_JSON.forEach( function( innrRecrd , innrIndx ){
                var statInd = findIndexByKeyValueAndCond( STATUS_DYNAMIC_TYPE , "fieldName" , "key" , (innrRecrd.displayName).toLowerCase() , (reportName).toLowerCase() );
                tbody += '<td>'+ ( (statInd != "-1") ? STATUS_DYNAMIC_TYPE[statInd].lookup[ record[ innrRecrd.displayName ] ].VALUE : record[ innrRecrd.displayName ] ) +'</td>';
            });
            tbody += '</tr>';
        }); 

        $('#dynamicReportList').html( thead + tbody );
        GBL_EXCELJSON = thead + tbody;
        
        if( localStorage.selectedDynamicReport != "" && localStorage.selectedDynamicReport != undefined && localStorage.selectedDynamicRefReport != "" && localStorage.selectedDynamicRefReport != undefined && localStorage.selectedDynamicReportMode != "" && localStorage.selectedDynamicReportMode != undefined ){
            if( localStorage.selectedDynamicReportMode == "1" ){    // DISPLAY
                $('#tableListing').dataTable({ 
                    "pageLength": 50, 
                    "stateSave": true,
                });
            }else{    // EDIT
                $('#tableListing').dataTable({ 
                    "pageLength": 50,
                    "stateSave": true,
                    // "bPaginate": false,
                });
            }
        }else{
            $('#tableListing').dataTable({ 
                "pageLength": 50,
                "stateSave": true,
                // "bPaginate": false,
            });
        }

        addRemoveLoader(0); // REMOVE LOADER
 
    }else{
        GBL_EXCELJSON = [];
        $('#dynamicReportList').html( "" );
        addRemoveLoader(0);
    }
}

// GENERATE THE HTML FILTER IN DISPLAY TO ADD MULTIPLE FILTERS
function addFilterHtml( type ){ // 1 - NORMAL FILTER - 2 DATE FILTER - 3 SUMMARY FIELD

    var reportId = $('#reportList').val();
    var reportName = $('#reportList option:selected').text();

    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( GBL_DYNAMIC_JSON == "" ){
        toastr.warning('Please Generate Report');
        addFocusId( 'reportList' );
        return false;
    } 

    var dynamicFilter = "";
    if( type == "1" ){

        var conditionCls = "none";
        if( GBL_REPORT_FILTER != "" ){
            conditionCls = "block";
        }
        dynamicFilter += '<div class="col-lg-2 col-md-2 col-sm-2" id="conditionDiv" style="display: '+ conditionCls +';">'+
                            '<div class="form-group">'+
                                '<label class="control-label" style="width: 100%;">'+
                                'Condition '+
                                '<select id="condition" class="" data-width="100%">'+
                                    '<option value="and"> AND </option>'+
                                    // '<option value="or"> OR </option>'+
                                '</select>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-3 col-md-3 col-sm-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Filter By '+
                                '<select data-live-search="true" id="filterBy" class="selecter" data-width="100%" onchange="setFilterType()"></select>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-3 col-md-3 col-sm-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Operator '+
                                '<select data-live-search="true" id="operator" class="selecter" data-width="100%" onchange="setOperatorType()"></select>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-3 col-md-3 col-sm-6" id="inpDateFieldDiv">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Value '+
                                '<input type="text" id="inpField" class="form-control" placeholder="Enter Value">'+
                                '<input type="text" id="dateField" class="form-control normal-date-picker" placeholder="Select Date" style="display:none;">'+
                                '<span id="searchFieldDiv"><input type="text" id="searchField" class="form-control" placeholder="Search User" style="display:none;" /></span>'+
                                '<div id="filterBoolSelectDiv"><select id="filterBoolSelect" class="selecter" data-width="100%" style="display:none;"></select></div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-1 col-md-1 col-sm-6">'+
                            '<a href="javascript:void(0)" onclick="resetFilters()" class="btn-right-mrg btn btn-danger btn-xs pull-right btn-ripple" style="margin-right:0px;"><i class="fa fa-times"></i> </a>'+
                            '<a href="javascript:void(0)" onclick="addFilter()" class="btn-right-mrg btn btn-success btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-save"></i> </a>'+  
                        '</div>';
        $('#filterListDiv').html( dynamicFilter );

        initNoramlDatePicker();     // INITIALIZE A NORMAL DATE PICKER

        // autoCompleteForUser("searchField", saveUsrDetail);

        var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
        if( index != "-1" ){
            // setOptionWithRef("0", "filterBy", GBL_REPORT_DATA[index].reportFilter, "---Select Filter By---", 'filterId', 'filterDisplayName');
                
            var optionList = "<option value='-1'>---Select Filter By---</option>";
            if( GBL_REPORT_DATA[index].reportFilter != "No record found" ){
                GBL_REPORT_DATA[index].reportFilter.forEach( function( record , index ){
                    if( record.filterType != "1" ){
                        optionList += '<option value="'+ record.filterId +'">'+ record.filterDisplayName +'</option>';
                    }
                });
            }
            $('#filterBy').html( optionList );

            $('#filterBy').selectpicker();
            setOption("0", "operator", FIELD_OPERATOR_LIST, "--- Select Filter ---");
            $('#operator').selectpicker();
            $('#condition').selectpicker();

            $('#filterListDiv').show();
        }
    }else if( type == "2" ){
        var conditionCls = "none";
        if( GBL_REPORT_FILTER != "" ){
            conditionCls = "block";
        }
        dynamicFilter +='<div class="col-lg-3 col-md-3 col-sm-6" id="conditionDiv" style="display: '+ conditionCls +';">'+
                            '<div class="form-group">'+
                                '<label class="control-label" style="width: 100%;">'+
                                'Condition '+
                                '<select id="dateCondition" class="" data-width="100%">'+
                                    '<option value="and"> AND </option>'+
                                    '<option value="or"> OR </option>'+
                                '</select>'+
                            '</div>'+
                        '</div>'+ 
                        '<div class="col-lg-3 col-md-3 col-sm-3">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Filter Date '+
                                '<select data-live-search="true" id="dateFilter" class="selecter" data-width="100%"></select>'+
                            '</div>'+
                        '</div>'+ 
                        '<div class="col-lg-2 col-md-2 col-sm-2">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Start Date '+
                                '<input type="text" id="dateStDate" class="form-control normal-date-picker">'+ 
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-2 col-md-2 col-sm-2">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'End Date '+
                                '<input type="text" id="dateEnDate" class="form-control normal-date-picker">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-2 col-md-2 col-sm-2">'+
                            '<a href="javascript:void(0)" onclick="resetDateFilters()" class="btn-right-mrg btn btn-danger btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-times"></i> </a>'+
                            '<a href="javascript:void(0)" id="editDateFilterBtn" onclick="editDateFilter()" class="btn-right-mrg btn btn-warning btn-xs pull-right btn-ripple" style="margin-right:5px;display:none;"><i class="fa fa-pencil"></i> </a>'+  
                            '<a href="javascript:void(0)" id="addDateFilterBtn" onclick="addDateFilter()" class="btn-right-mrg btn btn-success btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-save"></i> </a>'+  
                        '</div>';
        $('#filterListDiv').html( dynamicFilter );

        initNoramlDatePicker();     // INITIALIZE A NORMAL DATE PICKER

        var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
        if( index != "-1" ){
            setOptionWithRef("0", "dateFilter", GBL_REPORT_DATA[index].dateFilter, "---Select Date Filter---", 'filterId', 'filterDisplayName');
            // var optionList = "<option value='-1'>---Select Date Filter---</option>";
            // if( GBL_REPORT_DATA[index].dateFilter != "No record found" ){
            //     GBL_REPORT_DATA[index].reportFilter.forEach( function( record , index ){
            //         if( record.filterType != "1" ){
            //             optionList += '<option value="'+ record.filterId +'">'+ record.filterDisplayName +'</option>';
            //         }
            //     });
            // }
            // $('#dateFilter').html( optionList );

            $('#dateFilter').selectpicker();
            $('#dateCondition').selectpicker();

            $('#filterListDiv').show();
        }

    }else if( type == "3" ){

        if( GBL_RUN_REPORT == "" ){
            toastr.warning('Please Generate / Run Report');
            addFocusId( 'reportList' );
            return false;
        }

        dynamicFilter += '<div class="col-lg-3 col-md-3 col-sm-3">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Summary Field '+
                                '<select data-live-search="true" id="summaryFilter" class="selecter" data-width="100%"></select>'+
                            '</div>'+
                        '</div>'+ 
                        '<div class="col-lg-3 col-md-3 col-sm-3">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Summary Type '+
                                '<select data-live-search="true" id="summaryType" class="selecter" data-width="100%"></select>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-3 col-md-3 col-sm-3">'+
                            '<div class="form-group">'+
                                '<label class="control-label">'+
                                'Field Label '+
                                '<input type="text" id="summaryFieldLabel" class="form-control">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-3 col-md-3 col-sm-3">'+
                            '<a href="javascript:void(0)" onclick="resetSummaryFilters()" class="btn-right-mrg btn btn-danger btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-times"></i> </a>'+
                            '<a href="javascript:void(0)" id="addSummaryFilterBtn" onclick="addSummaryFilter()" class="btn-right-mrg btn btn-success btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-save"></i> </a>'+  
                        '</div>';
        $('#filterListDiv').html( dynamicFilter );

        initNoramlDatePicker();     // INITIALIZE A NORMAL DATE PICKER

        var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
        if( index != "-1" ){
            // setOptionWithRef("0", "summaryFilter", GBL_REPORT_DATA[index].summaryFilter, "---Select Summary Filter---", 'reportFieldId', 'displayName');
            
            var optionList = "<option value='-1'>---Select Summary Filter---</option>"; 
            if( GBL_REPORT_DATA[index].userField != "No record found" ){
                GBL_REPORT_DATA[index].userField.forEach( function( record , index ){
                    if( ( (record.dbDataType).toLowerCase() == "int" || (record.dbDataType).toLowerCase() == "number" ) ){
                        optionList += '<option value="'+ record.reportFieldId +'">'+ record.displayName +'</option>';
                    }
                });
            }
            $('#summaryFilter').html( optionList );

            $('#summaryFilter').selectpicker();
            setOption("0", "summaryType", SUMMARY_TYPE, "---Select Summary Type---");
            $('#summaryType').selectpicker();

            $('#filterListDiv').show();
        }
    }
    // $('#dateFilterBtn').attr('disabled',true);
}

// CREATE JSON WHEN ADDING A FILTER IN LOCAL JSON FOR SENDING IN SAVE / RUN REPORT API
function setFilterType(){

    var filterBy = $('#filterBy').val();
    var reportId = $('#reportList').val(); 

    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( filterBy == "-1" ){
        toastr.warning('Please Select Filter');
        addFocusId( 'filterBy' );
        return false;

    }

    var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
    if( index != "-1" ){
        var filterDataTypeInd =  findIndexByKeyValue( GBL_REPORT_DATA[index].reportFilter , "filterId" , filterBy );
        if( filterDataTypeInd != "-1" ){
            // filterDataType = "int";
            // filterDataType = "date"; 
            // filterDataType = "search"; 
            filterDataType = GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterDataType;
            filterSearchType = GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterSearchType;
            
            if( (filterDataType).toLowerCase() == "date" ){ 
                $('#dateField').show();
                $('#inpField').hide();
                $('#searchField').hide();
                $('#filterBoolSelectDiv').hide();
                $('#filterBoolSelect').selectpicker('refresh');
            }else if( (filterDataType).toLowerCase() == "int" || (filterDataType).toLowerCase() == "number" ){
                $('#inpField').show();
                $('#dateField').hide();
                $('#searchField').hide();
                $('#filterBoolSelectDiv').hide();
                $('#filterBoolSelect').selectpicker('refresh');
            }else if( (filterDataType).toLowerCase() == "search" ){
                $('#searchField').show();
                $('#inpField').hide();
                $('#dateField').hide();
                $('#filterBoolSelectDiv').hide();
                $('#filterBoolSelect').selectpicker('refresh');

                $('#searchFieldDiv').html('<input type="text" id="searchField" class="form-control" placeholder="Search User"" />')
                if( filterSearchType == "customer" ){
                    autoCompleteForCustomer("searchField", saveCustDetail);
                    $('#searchField').attr('placeholder','Search Customer');
                }else if( filterSearchType == "item" ){
                    autoCompleteForItem("searchField", saveCustDetail);
                    $('#searchField').attr('placeholder','Search Item');
                }else{
                    autoCompleteForUser("searchField", saveUsrDetail);
                    $('#searchField').attr('placeholder','Search User');
                }
                

            }else if( (filterDataType).toLowerCase() == "boolean" ){

                $('#filterBoolSelectDiv').show(); 
                if( GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].selectFilterValue != "No record found" ){
                    setOption("0", "filterBoolSelect", GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].selectFilterValue, "---Select "+ GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterDisplayName +"---");
                    $('#filterBoolSelect').selectpicker('refresh');
                }
                $('#inpField').hide();
                $('#dateField').hide();
                $('#searchField').hide();
            }
        }
    }
}

// THIS FUNCTION REMOVE THE VALUE FIELD FROM DISPLAY WHEN OPERATOR IS ORDER BY AND GROUP BY
function setOperatorType(){

    var operator = $('#operator').val();
    var reportId = $('#reportList').val(); 

    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( operator == "-1" ){
        toastr.warning('Please Select Operator');
        addFocusId( 'operator' );
        return false;

    }

    if( operator == "10" || operator == "11" ){     // ORDER BY - GROUP BY
        $('#inpDateFieldDiv').hide();
    }else{
        $('#inpDateFieldDiv').show();
        setFilterType();
    }
}

// ADD FILTER WILL ADD FILTER JSON 
function addFilter(){

    var reportId = $('#reportList').val();
    var filterBy = $('#filterBy').val();
    var selectedOperId = $('#operator').val();
    var selectedOper = $('#operator option:selected').text();

    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( filterBy == "-1" ){
        toastr.warning('Please Select Filter');
        addFocusId( 'filterBy' );
        return false;

    }else if( selectedOperId == "-1" ){
        toastr.warning('Please Select Operator');
        addFocusId( 'operator' );
        return false;

    }else if( selectedOperId == "-1" ){
        toastr.warning('Please Select Operator');
        addFocusId( 'operator' );
        return false;

    }

    var filterOperationSymbol = FIELD_OPERATOR_LIST[ selectedOperId-1 ].INPUT_SYMBOL;
    
    var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
    if( index != "-1" ){

        var filterDataType = "";
        var filterValue = "";
        var filterDataTypeInd =  findIndexByKeyValue( GBL_REPORT_DATA[index].reportFilter , "filterId" , filterBy );
        if( filterDataTypeInd != "-1" ){
            // filterDataType = "int";
            // filterDataType = "date";
            // filterDataType = "search";
            filterDataType = GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterDataType;
            
            if( selectedOperId == "10" || selectedOperId == "11" ){     // ORDER BY - GROUP BY
                filterValue = "";

            }else if( (filterDataType).toLowerCase() == "date" ){
                filterValue = $('#dateField').val();
                if( filterValue == "" ){
                    toastr.warning('Please Select Filter Date');
                    $('#dateField').datepicker('show');
                    return false;
                }
            }else if( (filterDataType).toLowerCase() == "int" || (filterDataType).toLowerCase() == "number" ){
                filterValue = $('#inpField').val();
                if( filterValue == "" ){
                    toastr.warning('Please Enter Filter Value');
                    $('#inpField').focus();
                    return false;
                }
            }else if( (filterDataType).toLowerCase() == "boolean" ){
                filterValue = $('#filterBoolSelect option:selected').text() + '|--|' +$('#filterBoolSelect').val();
                if( $('#filterBoolSelect').val() == "-1" || $('#filterBoolSelect').val() == null ){
                    toastr.warning('Please Select Value');
                    addFocusId('filterBoolSelect');
                    return false;
                }
            }else if( (filterDataType).toLowerCase() == "search" ){
                filterValue = $('#searchField').val() + '|--|' +$('#searchField').attr('userId');
                if( $('#searchField').attr('userId') == "" || $('#searchField').attr('userId') == undefined ){
                    toastr.warning('Please Search Value');
                    $('#searchField').focus();
                    return false;
                }
            }

            var dupFilterInd = findIndexByKeyValueAndCond( GBL_REPORT_FILTER , "filterFieldName" , "filterValue" , GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterFieldName , filterValue );

            if( dupFilterInd == "-1" ){

                var filterCondition = "";
                if( GBL_REPORT_FILTER != "" ){
                    filterCondition = $('#condition').val();
                }

                var tempFilter = {
                    filterSearchType : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterSearchType,
                    filterFieldName : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterFieldName,
                    filterDisplayName : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterDisplayName,
                    filterOperationSymbol : filterOperationSymbol,
                    filterAlias : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterAlias,
                    filterId : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterId,
                    filterOperation : selectedOper,
                    filterOperationId : selectedOperId,
                    filterValue : filterValue,
                    filterCondition : filterCondition,
                    filterType : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterType,
                    filterDataType : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterDataType,
                }
                GBL_REPORT_FILTER.push( tempFilter );
                console.log(GBL_REPORT_FILTER);

                renderFilterList();
                resetFilters();
            }else{
                toastr.warning('This Filter is already added');
                return false;
            }
        }
    }
}

// ADD DATE FILTER WILL ADD DATE FILTER JSON 
function addDateFilter(){

    var reportId = $('#reportList').val();
    var dateFilter = $('#dateFilter').val();

    var dateStDate = $('#dateStDate').val();
    var dateEnDate = $('#dateEnDate').val();

    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( dateFilter == "-1" ){
        toastr.warning('Please Select Date Filter');
        addFocusId( 'dateFilter' );
        return false;

    }else if( dateStDate == "" ){
        toastr.warning('Please Select Start Date');
        $('#dateStDate').datepicker('show');
        return false;

    }else if( dateEnDate == "" ){
        toastr.warning('Please Select End Date');
        $('#dateEnDate').datepicker('show');
        return false;

    }
    var dateStDatePlus = new Date( ddmmyyToMysql( dateStDate ) );
    var dateEnDatePlus = new Date( ddmmyyToMysql( dateEnDate ) );

    if( +dateEnDatePlus < +dateStDatePlus ){
        toastr.warning("Start should not be less than End Date");
        $('#dateStDate').datepicker('show');
        return false;
    }
    
    var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
    if( index != "-1" ){
 
        var filterDataTypeInd =  findIndexByKeyValue( GBL_REPORT_DATA[index].reportFilter , "filterId" , dateFilter );
        if( filterDataTypeInd != "-1" ){
            var dupFilterInd = findIndexByKeyValueAndCond( GBL_REPORT_FILTER , "filterFieldName" , "filterValue" , GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterFieldName , dateStDate +','+ dateEnDate );

            if( dupFilterInd == "-1" ){
                
                var filterCondition = "";
                if( GBL_REPORT_FILTER != "" ){
                    filterCondition = $('#dateCondition').val();
                }

                var tempFilter = {
                    filterFieldName : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterFieldName,
                    filterDisplayName : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterDisplayName,
                    filterOperationSymbol : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterOperationSymbol,
                    filterAlias : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterAlias,
                    filterId : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterId,
                    filterOperation : "BETWEEN",
                    filterOperationId : dateFilter,
                    filterValue : dateStDate +','+ dateEnDate,
                    filterCondition : filterCondition,
                    filterType : GBL_REPORT_DATA[index].reportFilter[filterDataTypeInd].filterType,
                };
                // GBL_REPORT_DATE_FILTER = tempFilter;
                GBL_REPORT_FILTER.push( tempFilter );
                console.log(GBL_REPORT_FILTER);
 
                $('#filterListDiv').hide(); 

                renderFilterList();
                resetDateFilters();
            }else{
                toastr.warning('This Filter is already added');
                return false;
            }
        }
    }
}

// ADD SUMMARY FIELD FILTER WILL ADD DATE FILTER JSON 
function addSummaryFilter(){

    var reportId = $('#reportList').val(); 

    var summaryFilterId = $('#summaryFilter').val();
    var summaryFilter = $('#summaryFilter option:selected').text();
    var summaryTypeId = $('#summaryType').val();
    var summaryType = $('#summaryType option:selected').text();
    var summaryFieldLabel = $('#summaryFieldLabel').val();

    if( reportId == "-1" ){
        toastr.warning('Please Select Report');
        addFocusId( 'reportList' );
        return false;

    }else if( GBL_RUN_REPORT == "" ){
        toastr.warning('No Data available for Summary'); 
        return false;

    }else if( summaryFilterId == "-1" ){
        toastr.warning('Please Select Summary Filter');
        addFocusId( 'summaryFilter' );
        return false;

    }else if( summaryType == "-1" ){
        toastr.warning('Please Select Summary Type');
        addFocusId( 'summaryType' );
        return false;

    }else if( summaryFieldLabel == "" ){
        toastr.warning('Please Enter Field Label');
        $('#summaryFieldLabel').focus();
        return false;

    }

    var index = findIndexByKeyValue( GBL_REPORT_DATA , "reportId" , reportId );
    if( index != "-1" ){
 
        var dupFilterInd = findIndexByKeyValueAndCond( GBL_SUMMARY_FILTER , "summaryFilterId" , "summaryTypeId" , summaryFilterId , summaryTypeId );

        if( dupFilterInd == "-1" ){

            var tempFilter = {
                summaryFilter : summaryFilter,
                summaryFilterId : summaryFilterId,
                summaryType : summaryType,
                summaryTypeId : summaryTypeId,
                summaryFieldLabel : summaryFieldLabel,
            };
            GBL_SUMMARY_FILTER.push( tempFilter ); 

            resetSummaryFilters();
            renderSummaryFilterList();
        }else{
            toastr.warning('This Summary is already added');
            return false;
        }
    }
}

// THIS FUNCTION WILL EDIT THE ALREADY ADDED DATE FILTER FOR UPDATE
function editDateFilter(){
    $('#dateFilter').attr('disabled',false).selectpicker('refresh');
    $('#dateStDate').attr('disabled',false);
    $('#dateEnDate').attr('disabled',false);
    $('#addDateFilterBtn').show();
    $('#editDateFilterBtn').hide();

}

// THIS FUNCTION RENDER THE ALREADY ADDED FILTER LIST
function renderFilterList(){

    var filterListHtm = "";
    if( GBL_REPORT_FILTER != "" ){   
        GBL_REPORT_FILTER.forEach( function( record , index ){

            if( record.filterType == "1" ){
                return false;
            }

            var conditionCls = "block";
            if( index == "0" ){
                conditionCls = "none";
            }

            var operatorCls = "block";
            if( record.filterOperationId == "10" || record.filterOperationId == "11" ){ // ORDER BY - GROUP BY
                operatorCls = "none";
            }

            var filterValue = record.filterValue;
            if( filterValue.split('|--|').length == "2" ){
                filterValue = filterValue.split('|--|')[0];
            }

            filterListHtm += '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div class="col-lg-12 col-md-12 col-sm-6" style="display: '+ conditionCls +';">'+
                                        '<div class="form-group condition">'+
                                            '<label class="control-label">'+
                                            'Condition : '+ ( (record.filterCondition == "") ? "" : ( record.filterCondition ).toUpperCase() ) +
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">'+
                                            'Filter '+ (index+1) +': '+ record.filterDisplayName +
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-3 col-md-3 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">'+
                                            'Operator : '+ record.filterOperation +
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-6" style="display: '+ operatorCls +';">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">'+
                                            'Value : '+ filterValue +
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-1 col-md-1 col-sm-6 pull-right">'+
                                        '<a href="javascript:void(0)" onclick="removeAddedFilter('+ index +')" class="btn-right-mrg btn btn-danger btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-times"></i> </a>'+
                                        // '<a href="javascript:void(0)" onclick="addFilter()" class="btn-right-mrg btn btn-success btn-xs pull-right btn-ripple" style="margin-right:5px;"><i class="fa fa-save"></i> </a>'+  
                                    '</div>'+
                                '</div>'+
                            '</div>';
        });
    }
    $('#filterContentDiv').html( filterListHtm );
}

// THIS FUNCTION RENDER THE SUMMARY FILTER LIST
function renderSummaryFilterList(){

    var filterListHtm = "";
    SUMMARY_IN_EXCEL = '<tr>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                    '</tr>';

    if( GBL_SUMMARY_FILTER != "" ){   
        GBL_SUMMARY_FILTER.forEach( function( record , index ){

            var summaryValue = 0;
            if( record.summaryTypeId == "1" ){ // COUNT
                GBL_RUN_REPORT.forEach( function( innrRecord , innrIndex ){
                    if( innrRecord[ record.summaryFilter ] != "" ){
                        summaryValue = summaryValue + 1;
                    }
                });
            }else if( record.summaryTypeId == "2" ){ // SUM
                GBL_RUN_REPORT.forEach( function( innrRecord , innrIndex ){
                    if( innrRecord[ record.summaryFilter ] != "" ){
                        summaryValue = parseFloat( innrRecord[ record.summaryFilter ] ) + summaryValue;
                    }
                });
            }else if( record.summaryTypeId == "3" ){ // AVERAGE
                var tempTotCnt = 0;
                var tempTot = 0;
                GBL_RUN_REPORT.forEach( function( innrRecord , innrIndex ){
                    if( innrRecord[ record.summaryFilter ] != "" ){
                        tempTotCnt = tempTotCnt + 1;
                        tempTot = parseFloat( innrRecord[ record.summaryFilter ] ) + tempTot;
                    }
                }); 
                summaryValue = ( tempTot / tempTotCnt );
            }else if( record.summaryTypeId == "4" ){ // MIN
                GBL_RUN_REPORT.forEach( function( innrRecord , innrIndex ){
                    if( innrRecord[ record.summaryFilter ] != "" ){
                        if( innrIndex == "0" ){
                            summaryValue = parseFloat( innrRecord[ record.summaryFilter ] );
                        }else{
                            if( summaryValue > parseFloat( innrRecord[ record.summaryFilter ] ) ){
                                summaryValue = parseFloat( innrRecord[ record.summaryFilter ] );
                            }
                        }
                    }
                });  
            }else if( record.summaryTypeId == "5" ){ // MAX
                GBL_RUN_REPORT.forEach( function( innrRecord , innrIndex ){
                    if( innrRecord[ record.summaryFilter ] != "" ){
                        if( innrIndex == "0" ){
                            summaryValue = parseFloat( innrRecord[ record.summaryFilter ] );
                        }else{
                            if( summaryValue < parseFloat( innrRecord[ record.summaryFilter ] ) ){
                                summaryValue = parseFloat( innrRecord[ record.summaryFilter ] );
                            }
                        }
                    }
                });  
            }

            filterListHtm += '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div class="col-lg-6 col-md-6 col-sm-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">'+
                                                'Summary '+ (index+1) +
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-3 col-md-3 col-sm-3">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">'+
                                                record.summaryFieldLabel +" - ("+ record.summaryFilter +") :"+summaryValue +
                                            '</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'; 

            SUMMARY_IN_EXCEL += '<tr>'+
                                    '<td>Summary '+ (index+1) +'</td>'+
                                    '<td> </td>'+
                                    '<td>'+ record.summaryFieldLabel +' - ('+ record.summaryFilter +') :</td>'+
                                    '<td>'+ summaryValue +'</td>'+
                                '</tr>';
        });
    }
    $('#filterSummaryDiv').html( filterListHtm );
}

// THIS FUNCTION REMOVE THE ALREADY ADDED DATE FILTER FROM LISTING
function removeAddedFilter( index ){

    GBL_REPORT_FILTER.splice( index , 1 );
    renderFilterList();
    toastr.success('Filter Removed Successfully');
}

// USER SEARCH FUNCTION
function autoCompleteForUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {    
            $('#searchField').attr('userId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

// USER SEARCH FUNCTION CALLBACK
function saveUsrDetail(){

}

// DOWNLOAD REPORT IN EXCEL FORMAT
function downLoadExcel(){
    if( GBL_EXCELJSON.length == 0 ){
        toastr.warning('Please Search Report Before Downloading...');
    }else{
        if(checkAuth(31,127,1) == -1){  // NOT AUTH            
            toastr.error('You are not authorised for this Activitiy');
        }else{

            var reportName = 'Dynamic '+$('#reportList option:selected').text();
            customCsvFormatSave(GBL_EXCELJSON + SUMMARY_IN_EXCEL, reportName+".xls");
        }
    }
}

function confirmAndNavigate(){
    if( GBL_SAVE_FLAG == "0" ){
        var confirmUnSaved = confirm('Are you sure? Your Unsaved data will be lost');
        if( confirmUnSaved ){
            navigateToDynamicReportList();
        }
    }else{
        navigateToDynamicReportList();
    }
}



// THIS FUNCTION WILL RESET THE FILTERS
function resetFilters(){

    $('#filterBy').val('-1').selectpicker('refresh');
    $('#operator').val('-1').selectpicker('refresh');
    $('#dateField').val('');
    $('#inpField').val('');
    $('#searchField').val('');
    $('#searchField').attr('userId','');
    $('#conditionDiv').hide();
    $('#filterListDiv').hide();
    $('#dateField').hide();
    $('#searchField').hide();
    $('#inpField').show();
    $('#addFilterBtn').attr('disabled',false);
    $('#dateFilterBtn').attr('disabled',false);
    $('#summayFieldBtn').attr('disabled',false);
}

// THIS FUNCTION WILL RESET THE DATE FILTERS
function resetDateFilters(){

    $('#dateFilter').val('-1').selectpicker('refresh');
    $('#dateStDate').val('');
    $('#dateEnDate').val(''); 
    $('#filterListDiv').hide(); 
    $('#addFilterBtn').attr('disabled',false);
    $('#dateFilterBtn').attr('disabled',false);
    $('#summayFieldBtn').attr('disabled',false);
}

// THIS FUNCTION WILL RESET THE SUMMARY FIELD FILTERS
function resetSummaryFilters(){

    $('#summaryFilter').val('-1').selectpicker('refresh');
    $('#summaryType').val('-1').selectpicker('refresh'); 
    $('#summaryFieldLabel').val(''); 
    $('#filterListDiv').hide(); 
    $('#addFilterBtn').attr('disabled',false);
    $('#dateFilterBtn').attr('disabled',false);
    $('#summayFieldBtn').attr('disabled',false);
}


function autoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
           $('#searchField').attr('userId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {

                var type = "1";
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getItemListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(4, 14),
                    keyWord: query,
                    type: type
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                console.log(data.status);
                // // We good!
                ITEMS = [];
                if (data.status != "No record found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.itemId,
                            name: data.itemName,
                            data: data.itemName,
                            clientId: data.clientId,
                            itemId: data.itemId,
                            itemName: data.itemName,
                            itemPrice: data.itemPrice,
                            itemSubType: data.itemSubType,
                            itemType: data.itemType,
                            orgId: data.orgId,
                            validityEndDate: data.validityEndDate,
                            itemGroupName: data.itemGroupName,
                            bomInnerItem: data.bomInnerItem
                        }
                        // if( data.bomInnerItem != undefined ){
                        //     bomJson.push(tempJson)
                        // }else{
                            // itemJson.push(tempJson);
                        // }
                        ITEMS.push(tempJson);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveItemDetail(){
}

function autoCompleteForCustomer(id,callback) {
    
   $("#" + id).typeahead({
        
        onSelect: function (item) {
            $('#searchField').attr('userId',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                GBL_SELECTEDCUSTID = '';
                GBL_SELECTEDCUSTMOBNO = '';
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3,10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {
                        if( data.cust_id != -1 ){
                            
                            var group = {
                                id: data.cust_id,
                                name: data.concatKey,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname,
                                rateTypeId: data.rateTypeId,
                                custTypeId: data.custTypeId
                            };
                            ITEMS.push(group);
                        }
                    });

                    return ITEMS;
                }

            }
        }
    });
}
function saveCustDetail(){

}