/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : customerGroupController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var reportData = [];
var GBL_EXCELJSON = [];
var itemJson = [];
var itemList = [];
var data = JSON.parse(localStorage.indeCampusRoleAuth);
var currClientType = data.data.clientTypeId;

function getReportData(){

    reportData = [];
    GBL_EXCELJSON = [];

    var stDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var exhibtionId = $('#exhibtionList').val();
    var status = $('#typeStatusListing').val();
    var custId = $('#customer').attr('custId');
    var userId = $('#user').attr('userId');
    var creatTo = $('#creatTo').attr('createdid');
    var itemId = $('#itemName').attr('itemId');

    if( stDate != "-1" && stDate != "" ){
        stDate = ddmmyyToMysql(stDate);
    }else{
        stDate = "-1";
    }
    if( endDate != "-1" && endDate != "" ){
        endDate = ddmmyyToMysql(endDate);
    }else{
        endDate = "-1";
    }
    
    if($('#customer').val().length <= 0){
        custId = "";
    }
    if($('#user').val().length <= 0){
        userId = "";
    }
    
    if($('#creatTo').val().length <= 0){
        creatTo = "-1";
    }
    

    var dateActStartDate = new Date(stDate);
    var dateActEndDate = new Date(endDate);

    if( +dateActEndDate < +dateActStartDate ) //dateActEndDate dateActStartDate
    {
        toastr.warning("End Date cannot be less than the StartDate");
        $('#endDate').datepicker('show');
        return false;
    }


    if( currClientType == 33 || currClientType == 35 ){
        exhibtionId = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getInqReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(31,126),
        filterStartDate: stDate,
        filterEndDate: endDate,
        filterStatus: status,
        filterCustomerId: custId,
        filterUserId: userId,
        filterCreatedBy: creatTo,
        exhibtionId: exhibtionId,
        itemId: itemList,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getReportDataCallback,"Please Wait... Getting Dashboard Detail");
}

function getReportDataCallback(flag,data){

    if (data.status == "Success" && flag) {
        console.log(data);

        GBL_EXCELJSON = [];
        reportData = data.data;
        renderReportData();

    } else {

        reportData = [];
        renderReportData();

        if (flag)
            displayAPIErrorMsg( data.status , GBL_ERR_NO_INQ );
        else
            toastr.error(SERVERERROR);
    }
}

function renderReportData(){

    var newRow = '';
    var count = 1 ;

    var stDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    var status = $('#typeStatusListing option:selected').text();
    var cust = $('#customer').val();
    var user = $('#user').val();

    $('#stDateLbl').html(stDate);
    $('#enDateLbl').html(endDate);
    $('#inqStatLbl').html(status);
    $('#custNameLbl').html(cust);
    $('#uNameLbl').html(user);

    exhibtionShow = "hide";
    if( currClientType == 34 || currClientType == 32 ){
        exhibtionShow = "";
    }

    if( reportData != 'No record found' ){

        var clientTypeName = "Exhibition";
        if( currClientType == "32" ){
            clientTypeName = "Project";
        }

        var customFieldTh = "";
        var customFieldNull = "";
        var customFieldCnt = 0;

        var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
        if(roleAuth.data.customFieldDetail != "No record found"){
            roleAuth.data.customFieldDetail.forEach(function( record , index ){
                if(record.tabType == "INQMASTER"){
                    customFieldTh += '<th>'+ record.fieldName +'</th>';
                    customFieldCnt = customFieldCnt + 1;
                    customFieldNull += null+','
                }
            });
        }

        var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>#</th>'+
                            '<th>Inq No</th>'+
                            '<th>Inquiry Date</th>'+
                            '<th>Inquiry Name</th>'+
                            '<th class="'+ exhibtionShow +'">'+ clientTypeName +' Name</th>'+
                            '<th>Item Name</th>'+
                            '<th>Item Qty</th>'+//ADD BY DARSHAN ON 23-03-2018
                            '<th>Item Status</th>'+//ADD BY DARSHAN ON 23-03-2018
                            '<th>Customer Name</th>'+
                            '<th>Responsible Person</th>'+
                            '<th>Created By</th>'+
                            '<th>Remark</th>'+
                            '<th>Inq Status</th>'+
                            customFieldTh +
                        '</tr>'+
                    '</thead>'+
                    '<tbody id="itemList">';

        var tfoot = '</tbody></table>';

        if( currClientType == 34 ){
            var tempEXL = { // HEADER FOR PDF AND CSV
                No : "#.",
                inqId : "Inq No",
                inqDate : "Inquiry Date",
                inqName : "Inquiry Name",
                exhibtionName : clientTypeName,
                itemName : "Item Name",
                itemQty : "Item Qty",
                itemStatus : "Item Status",
                custName : "Customer Name",
                assUser : "Responsible Person",
                createBy : "Created By",
                inqRemark : "Remark.",
                inqStatus : "Status",
            };
        }else{

            var tempEXL = { // HEADER FOR PDF AND CSV
                No : "#.",
                inqId : "Inq No",
                inqDate : "Inquiry Date",
                inqName : "Inquiry Name",
                itemName : "Item Name",
                itemQty : "Item Qty",
                itemStatus : "Item Status",
                custName : "Customer Name",
                assUser : "Responsible Person",
                createBy : "Created By",
                inqRemark : "Remark.",
                inqStatus : "Status",
            };
        }
        GBL_EXCELJSON.push(tempEXL);
    
        for( var i=0; i<reportData.length; i++ ){
            
            count = i + 1;
                
            var uname = reportData[i].AssignUserName;
            if( reportData[i].AssignUserName == null ){
                uname = ""
            }
            var createBy = reportData[i].createBy;
            if( reportData[i].createBy == null ){
                createBy = ""
            }

            if( currClientType == 34 ){
                var tempEXL = {
                    No : count,
                    inqId : reportData[i].clientLevelInqId,
                    inqDate : reportData[i].inqDate,
                    inqName : reportData[i].inqName,
                    exhibtionName : reportData[i].exhibtionName,
                    itemName : reportData[i].itemName,
                    itemQty : reportData[i].itemQty,
                    itemStatus : reportData[i].itemStatus,
                    custName : reportData[i].customerName,
                    assUser : uname,
                    createBy : createBy,
                    inqRemark : reportData[i].inqRemark,
                    inqStatus : reportData[i].inqStatus,
                };
            }else{
                var tempEXL = {
                    No : count,
                    inqId : reportData[i].clientLevelInqId,
                    inqDate : reportData[i].inqDate,
                    inqName : reportData[i].inqName,
                    itemName : reportData[i].itemName,
                    itemQty : reportData[i].itemQty,
                    itemStatus : reportData[i].itemStatus,
                    custName : reportData[i].customerName,
                    assUser : uname,
                    createBy : createBy,
                    inqRemark : reportData[i].inqRemark,
                    inqStatus : reportData[i].inqStatus,
                };
            }
            
            GBL_EXCELJSON.push(tempEXL);
            
            var customFieldTd = "";
            for( var j=0; j<customFieldCnt; j++ ){
                customFieldTd += '<td>'+ reportData[i]['field'+( j+1 )] +'</td>';
            }

            var vcDetail = "";
            if( reportData[i].inqTransDetail[0].detailing != "No record found" ){
                reportData[i].inqTransDetail[0].detailing.forEach( function( detRecrd , detIndx ){
                    vcDetail += ( detIndx + 1 ) +'. <span>'+ detRecrd.title +' : '+ detRecrd.children[0].title +'</span><br>';
                });
            }

            newRow += '<tr>'+
                            '<td>'+ count +'</td>'+
                            '<td><a href="javascript:void(0)" inqid="'+ reportData[i].inqId +'" onclick="navigateToInquiryDetailOpenWin(this)">'+ reportData[i].clientLevelInqId +'</a></td>'+
                            '<td>'+ reportData[i].inqDate +'</td>'+
                            '<td>'+ reportData[i].inqName +'</td>'+
                            '<td class="'+ exhibtionShow +'">'+ reportData[i].exhibtionName +'</td>'+
                            '<td><b>'+ reportData[i].itemName +'</b><br>'+ vcDetail +'</td>'+
                            '<td>'+ reportData[i].itemQty +'</td>'+//ADD BY DARSHAN ON 23-03-2018
                            '<td>'+ reportData[i].itemStatus +'</td>'+//ADD BY DARSHAN ON 23-03-2018
                            '<td>'+ reportData[i].customerName +'</td>'+
                            '<td>'+ uname +'</td>'+
                            '<td>'+ createBy +'</td>'+
                            '<td>'+ reportData[i].inqRemark +'</td>'+
                            '<td>'+ reportData[i].inqStatus +'</td>'+
                            customFieldTd +
                        '</tr>';
        }

        
        $('#inqReportList').html(thead + newRow + tfoot);
        GBL_EXCELJSON = thead + newRow + tfoot;
    }else{
        $('#inqReportList').html(thead + newRow + tfoot);
        displayAPIErrorMsg( "" , GBL_ERR_NO_INQ );
    }

    // resetReportFilter();
    $('.sidemenu').addClass('hiden', 300);

    // TablesDataTables.init();
    // TablesDataTablesEditor.init();

    $('#tableListing').dataTable({
        // "aoColumns": [
        //     null,
        //     null,
        //     {"sType": "date-uk"},
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     null,
        //     customFieldNull
        // ],
        "pageLength": 50,
        "stateSave": true
    });
}

function AutoCompleteForCustomer(id,callback) {
    
   $("#" + id).typeahead({
        
        onSelect: function (item) {
            //alert(item);
            var mobileNumber = item.text.split(",");
            $('#customer').attr('custId',item.value);

        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                GBL_SELECTEDCUSTID = '';
                GBL_SELECTEDCUSTMOBNO = '';
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "crmDashboardSearch",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(3,10),
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // We good!
                ITEMS = [];
                if (data.data != null) {
                    $.map(data.data.searchData, function (data) {
                        if( data.cust_id != -1 ){
                            
                            var group = {
                                id: data.cust_id,
                                name: data.fname + " " + data.lname + " , " + data.mobile,
                                // name: data.concatKey,
                                data: data.fname + " " + data.lname,
                                mobile: data.mobile,
                                fname: data.fname,
                                lname: data.lname,
                                rateTypeId: data.rateTypeId,
                                custTypeId: data.custTypeId
                            };
                            ITEMS.push(group);
                        }
                    });

                    return ITEMS;
                }

            }
        }
    });
}
function saveCustDetail(){

}

function AutoCompleteForUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#user').attr('userid',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}
function saveUserDetail(){

}

function AutoCompleteForCreatedUser(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            console.log(item);
            $('#creatTo').attr('createdid',item.value);
        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getUserListingByName",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(8 , 30),
                    activeInactiveFlag : "1",
                    keyWord: query
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                // // We good!
                ITEMS = [];
                if (data != null) {
                    $.map(data.data, function (userData) {
                        
                        var group = {
                            id: userData.userId,
                            name: userData.fullName,
                            data: userData.fullName,
                            clientId: userData.clientId,
                            userName: userData.fullName
                        };
                        ITEMS.push(group);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveCreatUsrDetail(){

}

function downLoadExcel(){
    if( reportData.length == 0 ){
        toastr.warning('Please Search Report Before Downloading...');
    }else{
        if(checkAuth(31,127,1) == -1){  // NOT AUTH            
            toastr.error('You are not authorised for this Activitiy');
        }else{
            customCsvFormatSave(GBL_EXCELJSON, "InquiryReport.xls");
        }
    }
}

function resetReportFilter(){
    $('#startDate').val('');
    $('#endDate').val('');
    $('#status').val('-1');
    $('#status').selectpicker('refresh');
    $('#customer').val('');
    $('#user').val('');
    $('#customer').attr('custId','-1');
    $('#user').attr('userId','-1');
}

function menu() {
    if ($('.sidemenu').hasClass('hiden', 300)) {
        $('.sidemenu').removeClass('hiden', 300);
        setTimeout(function(){
            $('#startDate').focus();
            $('#startDate').datepicker('show');
        },500);
    } else {
        $('.sidemenu').addClass('hiden', 300);
    }
}

function getExhibitionData(type){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming

    var NO_DATA = "";
    if( type == "1" ){

        NO_DATA = GBL_ERR_NO_EXHIBITION;
        var postData = {
            requestCase: 'getExhibitionMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(4,13),
            stateId: "",
            cityId: "",
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
    }else{
        NO_DATA = GBL_ERR_NO_PROJECT;
        var postData = {
            requestCase: 'getRealEstateMaster',
            clientId: tmp_json.data[0].FK_CLIENT_ID,
            userId: tmp_json.data[0].PK_USER_ID,
            orgId: checkAuth(50,200),
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
        }
    }
    // console.log(postData);return false;
    commonAjax(FOLLOWUPURL, postData, getExhibitionDataCallback,"Please Wait... Getting Dashboard Detail");

    function getExhibitionDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            exhibitionData = data.data;

            var options = "<option value='-1'> All </option>";
            exhibitionData.forEach(function( record , index ){
                if( type == "1" ){
                    options += '<option value="'+ record.exibitionId +'"> '+ record.exibitionName +' </option>';
                }else{
                    options += '<option value="'+ record.projectId +'"> '+ record.projectName +' </option>';
                }
            });

            $('#exhibtionList').html( options );
            $('#exhibtionList').selectpicker( 'refresh' );

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , NO_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }
}


function AutoCompleteForItem(id,callback) {
    
    $("#" + id).typeahead({
        
        onSelect: function (item) {
           
            var itemIndex = findIndexByKeyValue(ITEMS,"id",item.value);
            if( jQuery.inArray( item.value, itemList ) == "-1" ){

                $('#itemName').attr('itemId',item.value);
                
                if( itemList != "" ){
                    itemList += ','+item.value;
                }else{
                    itemList = item.value;
                }

                if( itemList == "" ){
                    $('#itemListDiv').hide();
                }else{
                    $('#itemListDiv').show();
                }

                $('#itemListHtml').append('<br>'+ITEMS[itemIndex].itemName+'');
                callback(ITEMS[itemIndex].bomInnerItem);
            }else{
                toastr.warning(ITEMS[itemIndex].itemName +' is already selected..!');
                setTimeout(function(){
                    $('#itemName').val('');
                },100)
            }

        },
        ajax: {
            url: SEARCHURL,
            timeout: 500,
            triggerLength: 2,
            displayField: "name",
            method: "POST",
            headers: { Authorization: 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : "") },
            crossDomain: true,
            preDispatch: function (query) {

                var type = "1";
                addRemoveLoader(1);
                var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
                var postdata = {
                    requestCase: "getItemListing",
                    clientId: tmp_json.data[0].FK_CLIENT_ID,
                    userId: tmp_json.data[0].PK_USER_ID,
                    dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
                    orgId: checkAuth(4, 14),
                    keyWord: query,
                    type: type
                };
                
                removeLoaderInTime( REMOVE_LOADER_TIME );
                return {postData: postdata};
            },
            preProcess: function (data) {
                addRemoveLoader(0);
                if (data.success === false) {
                    // Hide the list, there was some error
                    return false;
                }
                console.log(data.status);
                // // We good!
                ITEMS = [];
                if (data.status != "No record found") {
                    $.map(data.data, function (data) {
                        
                        var tempJson = '';
                        tempJson = {
                            id: data.itemId,
                            name: data.itemName,
                            data: data.itemName,
                            clientId: data.clientId,
                            itemId: data.itemId,
                            itemName: data.itemName,
                            itemPrice: data.itemPrice,
                            itemSubType: data.itemSubType,
                            itemType: data.itemType,
                            orgId: data.orgId,
                            validityEndDate: data.validityEndDate,
                            itemGroupName: data.itemGroupName,
                            bomInnerItem: data.bomInnerItem
                        }
                        // if( data.bomInnerItem != undefined ){
                        //     bomJson.push(tempJson)
                        // }else{
                            itemJson.push(tempJson);
                        // }
                        ITEMS.push(tempJson);
                    });

                    return ITEMS;
                }
                console.log(data);
            }
        }
    });
}

function saveItemDetail(){
    setTimeout(function(){
        $('#itemName').val('');
    },100);
}

function resetItems(){
    $('#itemListHtml').html('');
    itemList = [];
    $('#itemListDiv').hide();
}

function pageInitialEvents(){

    AutoCompleteForCustomer("customer", saveCustDetail);
    AutoCompleteForUser("user", saveCustDetail);
    AutoCompleteForCreatedUser("creatTo", saveCreatUsrDetail);
    AutoCompleteForItem("itemName", saveItemDetail);

    initCommonFunction();
    $(".bmd-fab-speed-dialer").on('click', function(){$(this).toggleClass("press")});

    var localStatus = JSON.parse(localStorage.indeCampusRoleAuth);
    var index = findIndexByKeyValue(localStatus['data']['statusListing'],'statusCatName','INQUIRY');
    var statusList = localStatus['data']['statusListing'][index]['statusListing'];
    statusList.sort(function(a, b) {
        return parseFloat(a.statusPriority) - parseFloat(b.statusPriority);
    });

    setOption("0", "typeStatusListing", statusList, "All");
    $('#typeStatusListing').selectpicker();

    renderReportData();
    $('.sidemenu').removeClass('hiden', 300);

    var date = new Date();
    var day = "01";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if( month < 10 ){
        month = "0" + month;
    }

    var monStDate = day + '-' + month + '-' + year;
    var today = new Date().format('dd-MM-yyyy')
    
    $('#startDate').val( today );
    $('#endDate').val( today );

    $('.fab-dial-handle').click(function(){
        $('.fabdail-buttons').toggleClass('active');
    });

    if( localStorage.POTGInqToWhyUname != "" && localStorage.POTGInqToWhyUname != undefined ){

        if( localStorage.POTGInqToWhyType == "created" ){
            $('#creatTo').val( localStorage.POTGInqToWhyUname  );
            $('#creatTo').attr( 'createdid' , localStorage.POTGInqToWhyUnameId );
        }else{
            $('#user').val( localStorage.POTGInqToWhyUname  );
            $('#user').attr( 'userid' , localStorage.POTGInqToWhyUnameId );
        }
        $('#typeStatusListing').val( localStorage.POTGInqToWhyStatus  );
        $('#typeStatusListing').selectpicker('refresh');
        $('#startDate').val( mysqltoDesiredFormat( localStorage.POTGInqToWhyStDate , 'dd-MM-yyyy' ) );
        $('#endDate').val( mysqltoDesiredFormat( localStorage.POTGInqToWhyEnDate , 'dd-MM-yyyy' ) );
        
        // return false;
        localStorage.removeItem('POTGInqToWhyType');
        localStorage.removeItem('POTGInqToWhyUnameId');
        localStorage.removeItem('POTGInqToWhyUname');
        localStorage.removeItem('POTGInqToWhyStDate');
        localStorage.removeItem('POTGInqToWhyEnDate');
        localStorage.removeItem('POTGInqToWhyEnDate');
        localStorage.removeItem('POTGInqToWhyStatus');

        //getReportData();
    }
    getReportData();
    var data = JSON.parse(localStorage.indeCampusRoleAuth);
    var currClientType = data.data.clientTypeId;

    console.log(currClientType);
    if( currClientType == 34 ){
        $('.exhibition').show();
        $('#exhibProjectFilterNm').html('Exhibition Name');
        getExhibitionData("1");
    }else if( currClientType == 32 ){
        $('#exhibProjectFilterNm').html('Project Name');
        getExhibitionData("2");
        $('.exhibition').show();
    }else{
        $('.exhibition').hide();
    }
}