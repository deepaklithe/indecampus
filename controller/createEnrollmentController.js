    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 10-07-2018.
 * File : createEnrollmentController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */

var GBL_ROOM_ITEMS = []; // ROOM TYPE JSON
var GBL_ROOM_CATEGORY = []; // ROOM CATEORY TYPE JSON
var studentData = [];
var ITEMS = [];
var GBL_INVOICE_DATA = [];
var GBL_STD_ID = "";
var GBL_FACILITY = [];
var GBL_ALLOT_DATA_EDIT = [];
var GBL_ALLOT_EDIT_FLAG = 0;
var GBL_PARENT_ID = "";
var GBL_GUARANTOR_ID = "";

// PAGE INITIAL EVENTS
function pageInitialEvents(){

    $('#studentDetailTab').click();
    $('#gender').selectpicker('refresh');
    var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    setCountryList( 'country' ); // SET COUNTRY LISTING DATA
    $('#state').selectpicker('refresh');
    $('#city').selectpicker('refresh');
    $('#collegeCity').selectpicker('refresh');
    $('#collegeState').selectpicker('refresh');
    $('#guarantorState').selectpicker('refresh');
    $('#guarantorCity').selectpicker('refresh');
    $('#parentState').selectpicker('refresh');
    $('#parentCity').selectpicker('refresh');
    $('#semester').selectpicker('refresh');
        
    $("#academicYearFrom").datepicker( {
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months",
        startDate: new Date(),
        autoclose:true
    });
    $("#academicYearFrom").attr('readonly',true);
    
    initTelInput('mobileNumber');     // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput('guarantorMobile1'); // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput('guarantorMobile2'); // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput('emergencyPersonMobile'); // INITIALIZE COUNTRY WISE MOBILE NUM
    initTelInput('parentMobile'); // INITIALIZE COUNTRY WISE MOBILE NUM
    $('#roomType').selectpicker('refresh');
    $('#facilityAllocation').selectpicker('refresh');
    $('#bloodGroup').selectpicker('refresh');
    // getRoomType()
    var assetTypeData = roleAuth.data.assetType;
    setOption("0",'assetsIssued',assetTypeData,''); // SET ASSETS TYPE
    $('#assetsIssued').selectpicker('refresh');
    $('#assetsIssued').selectpicker('selectAll');

    setOption("0",'gender',ENROLL_GENDER,''); // SET GENDER FROM GLOBAL VAR
    $('#gender').selectpicker('refresh');

    $('#roomCategory').selectpicker('refresh');
    var categoryType = roleAuth.data.categoryType;
    setOption("0",'roomCategory',categoryType,''); // SET ROOM CATEGORY DATA
    $('#roomCategory').selectpicker('refresh');
    // getRoomCategoryData(); // GET ROOM CATEGORY DATA
    getRoomType();// GET ROOM TYPE DATA

    getFacilityData(); // GET FACILITY LIST DATA

    var paymentMode = roleAuth.data.paymentMode;
    setOption("0",'paymentMode',paymentMode,''); // SET PAYMENT MODE
    $('#paymentMode').selectpicker('refresh');

    $('#invoiceTable').DataTable();

    var countryList = roleAuth.data.countryListing;

    setOptionWithRef("0", 'nationality', countryList, "---Select Nationality---", 'countryId', 'countryName');
    $('#nationality').selectpicker('refresh'); 
    var studentRelation = roleAuth.data.studentRelation;
    setOption("0",'parentRelation',studentRelation,'');
    $('#parentRelation').selectpicker('refresh'); 
    setOption("0",'guarantorRelation',studentRelation,'');
    $('#guarantorRelation').selectpicker('refresh'); 
    var medicalCondition = roleAuth.data.medicalCondition;
    setOption("0",'medicalCondition',medicalCondition,'');
    $('#medicalCondition').selectpicker('refresh');
    

    if( localStorage.indeCampusEnrollType == 'edit' ){
        getStudentEnrollData(); // GET STUDENT EDIT DATA
    }else{
        localStorage.removeItem('indeCampusEditStudId');
        // IF AVAILABLE TAB1 , TAB2 AND INVOICE TAB DATA BEFORE COMPLETION OF PROCESS, RENDER THAT TABS DATA
        // RENDER TAB1 DATA
        if( localStorage.indeCampusEnrollTab1Data != undefined ){
            disableRefresh(); // DISABLE USER TO CLICK ANY BUTTONS OR MENU
            $('#saveTab1').attr('disabled',true);
            renderTab1(); // RENDER TAB1 DATA
        }
        $('#tenancyEndDate').datepicker('remove');
        $('#tenancyStartDate').attr('readonly',true);
        $('#tenancyEndDate').attr('readonly',true);
        $('#academicYearTo').datepicker('remove');
        // RENDER TAB2 DATA        
        // if( localStorage.indeCampusEnrollTab2Data != undefined ){
        //     disableRefresh(); // DISABLE USER TO CLICK ANY BUTTONS OR MENU
        //     $('#studentAllotBtn').attr('disabled',true);
        // }
        // // RENDER INVOICE TAB DATA      
        // if( localStorage.indeCampusEnrollInvoiceData != undefined ){
        //     var invoiceData = JSON.parse(localStorage.indeCampusEnrollInvoiceData);
        //     renderInvoice(invoiceData);
        // }
    }
}

// GET ROOM CATEGORY DATA
function getRoomCategoryData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getRoomCategory', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(13,54),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_CATEGORY = data.data;
            
            setOptionWithRef("0",'roomCategory',GBL_ROOM_CATEGORY,'','categoryId','categoryName');
            $('#roomCategory').selectpicker('refresh');
            getRoomType();
        }else{

            getRoomType();
            GBL_ROOM_CATEGORY = [];
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOM_CAT );
            else
                toastr.error(SERVERERROR);
        }
    }

    // var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
    // var statListIndex = findIndexByKeyValue( roleAuth.data.statusListing , "" ) 

}

// GET TYPE OF ROOM DATA
function getRoomType(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "RENTAL",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_ITEMS = data.data;
                
            var innerHtml = '<option value="-1">Select Room</option>';
            if( GBL_ROOM_ITEMS != "No record found" ){
                GBL_ROOM_ITEMS.forEach(function( record , index ){

                    innerHtml += '<option value="'+ record.itemId +'">'+ record.itemName +'</option>';

                });
            }
            $('#roomType').html( innerHtml ).selectpicker('refresh'); // SET ROOM TYPE DATA IN DROPDOWN
            
        }else{

            GBL_ROOM_ITEMS = [];
            $('#roomType').html( '' ).selectpicker('refresh');
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOM_TYPE );
            else
                toastr.error(SERVERERROR);
        }
        // if( localStorage.indeCampusEnrollTab2Data != undefined ){
        //     renderTab2(); // RENDER TAB2 DATA   
        // }
    }

}

// CREATE ENROLLMENT LISTING DATA
function createEnrollment() {

    // STUDENT PERSONAL DETAIL TAB
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var birthdate = $('#birthdate').val();
    var gender =  $('#gender').val();
    var bloodGroup = $('#bloodGroup').val();
    var address1 = $('#address1').val();
    var address2 = $('#address2').val();
    var address3 = $('#address3').val();
    var city = $('#city').val();
    var state = $('#state').val();
    var pincode = $('#pincode').val();
    var country = $('#country').val();
    var mobileNumber = getIntlMobileNum("mobileNumber");
    var email = $('#email').val();
    var nationality = $('#nationality').val();
    var passportNumber = $('#passportNumber').val();
    var aadharNumber = $('#aadharNumber').val();
    var medicalCondition = $('#medicalCondition').val();
    var emergencyPersonName = $('#emergencyPersonName').val();
    var emergencyPersonMobile = getIntlMobileNum('emergencyPersonMobile');
    var username = $('#username').val();
    var password = $('#password').val();
    var cnfpwd = $('#cnfpwd').val();

    // STUDENT PARENT DETAIL TAB
    var parentRelation = $('#parentRelation').val();
    var parentName = $('#parentName').val();
    var parentMobile = getIntlMobileNum('parentMobile');
    var parentEmail = $('#parentEmail').val();
    var parentAddress1 = $('#parentAddress1').val();
    var parentAddress2 = $('#parentAddress2').val();
    var parentAddress3 = $('#parentAddress3').val();
    var parentState = $('#parentState').val();
    var parentCity = $('#parentCity').val();
    var parentPincode = $('#parentPincode').val();
    var parentUsername = $('#parentUsername').val();
    var parentPassword = $('#parentPassword').val();
    var parentCnfPassword = $('#parentCnfPassword').val();
    
    // STUDENT COLLEGE DETAIL TAB
    var collegeName = $('#collegeName').val();
    var courseName = $('#courseName').val();
    var academicYearFrom = $('#academicYearFrom').val();
    var academicYearTo = $('#academicYearTo').val();
    var semester = $('#semester').val();
    var collegeAddressLine1 = $('#collegeAddressLine1').val();
    var collegeAddressLine2 = $('#collegeAddressLine2').val();
    var collegeAddressLine3 = $('#collegeAddressLine3').val();
    var collegeCity = $('#collegeCity').val();
    var collegeState = $('#collegeState').val();
    var collegePincode = $('#collegePincode').val();

    // STUDENT GUARANTOR DETAIL TAB
    var guarantorRelation = $('#guarantorRelation').val();
    var guarantorName = $('#guarantorName').val();
    var guarantorMobile1 = getIntlMobileNum('guarantorMobile1');
    var guarantorMobile2 = getIntlMobileNum('guarantorMobile2');
    var guarantorEmail = $('#guarantorEmail').val();
    var guarantorAddress1 = $('#guarantorAddress1').val();
    var guarantorAddress2 = $('#guarantorAddress2').val();
    var guarantorAddress3 = $('#guarantorAddress3').val();
    var guarantorCity = $('#guarantorCity').val();
    var guarantorState = $('#guarantorState').val();
    var guarantorPincode = $('#guarantorPincode').val();

    // STUDENT ROOM PREFERENCE TAB
    // var roomType = $('#roomType').val();
    var roomCategory = $('#roomCategory').val();

    // CHECK VALIDATIONS
    if( firstName == "" ){
        $('.colorTab li a[href="#tab4_1"] ').click();
        if( $('#StudentcollapseOneAccord').hasClass('collapsed') ){
            $('#StudentcollapseOneAccord').click();
        }
        toastr.warning("Please enter first name");
        addFocusId('firstName');
        return false;

    }else if( lastName == "" ){
        toastr.warning("Please enter last name");
        addFocusId('lastName');
        return false;

    }else if( birthdate == "" ){
        toastr.warning("Please enter birthdate");
        addFocusId('birthdate');
        return false;
    
    }else if( bloodGroup == "" ){
        toastr.warning('Please Select Blood Group');
        addFocusId('bloodGroup');
        return false;
    
    }else if( address1 == "" ){
        toastr.warning('Please Enter Address Line 1');
        addFocusId('address1');
        return false;
    
    // }else if( address2 == "" ){
    //     toastr.warning('Please Enter Address Line 2');
    //     addFocusId('address2');
    //     return false;
    
    // }else if( address3 == "" ){
    //     toastr.warning('Please Enter Address Line 3');
    //     addFocusId('address3');
    //     return false;
    
    }else if( country == "-1" ){
        toastr.warning('Please Select Country');
        addFocusId('country');
        return false;
    
    }else if( state == "-1" || state == null ){
        toastr.warning('Please Select State');
        addFocusId('state');
        return false;
    
    }else if( city == "-1" || city == null ){
        toastr.warning('Please Select City');
        addFocusId('city');
        return false;
    
    }else if( pincode == "" ){
        toastr.warning('Please Enter pincode');
        addFocusId('pincode');
        return false;
    
    }else if( pincode != "" && pincode < 6 ){
        toastr.warning('Please Enter valid pincode');
        addFocusId('pincode');
        return false;
    
    }else if( !checkValidMobile( 'mobileNumber' ) ){
        toastr.warning('Please Enter Proper Mobile Number');
        addFocusId('mobileNumber');
        return false;
    
    }else if( !validateEmail( email ) ){
        toastr.warning('Please Enter Proper Email');
        addFocusId('email');
        return false;
    
    }else if( passportNumber != "" &&  (nationality == "-1" || nationality == null) ){
        toastr.warning('Please Select Nationality');
        addFocusId('nationality');
        return false;
    
    }else if( aadharNumber == "" ){
        toastr.warning('Please Enter Aadhar Number');
        addFocusId('aadharNumber');
        return false;
    
    }else if( aadharNumber != "" && aadharNumber.length < 12  ){
        toastr.warning('Please Enter Proper Aadhar Number');
        addFocusId('aadharNumber');
        return false;
    
    }else if( emergencyPersonName == "" ){
        toastr.warning("Please enter emergency contact person name");
        addFocusId('emergencyPersonName');
        return false;

    }else if( !checkValidMobile( 'emergencyPersonMobile' ) ){
        toastr.warning("Please enter valid emergency contact person mobile");
        addFocusId('emergencyPersonMobile');
        return false;

    }else if( username == "" ){
        toastr.warning("Please enter user name");
        addFocusId('username');
        return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && ( password == "" ) ){
    //     toastr.warning("Please enter password");
    //     addFocusId('password');
    //     return false;

    // }else if(  localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && !validatePassword( password ) ){
    //     toastr.warning("Please enter strong password");
    //     addFocusId('password');
    //     return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && cnfpwd == "" ){
    //     toastr.warning("Please enter confirm password");
    //     addFocusId('cnfpwd');
    //     return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && !validatePassword( cnfpwd ) ){
    //     toastr.warning("Please enter strong confirm password");
    //     addFocusId('cnfpwd');
    //     return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && (password != cnfpwd) ){
    //     toastr.warning("Password and confirm password not matched!");
    //     addFocusId('cnfpwd');
    //     return false;

    }else if( roomCategory == "-1" || roomCategory == null ){
        toastr.warning('Please Select Room Category');
        addFocusId('roomCategory');
        return false;
    
    }else if( parentRelation == "-1" || parentRelation == null ){
        toastr.warning('Please Select Parent Relation');
        addFocusId('parentRelation');
        return false;
    
    }else if( parentName == "" ){
        toastr.warning('Please Enter Parent Name');
        addFocusId('parentName');
        return false;
    
    }else if( !checkValidMobile( 'parentMobile' ) ){
        toastr.warning("Please enter valid parent mobile number");
        addFocusId('parentMobile');
        return false;

    }else if( !validateEmail( parentEmail ) ){
        toastr.warning('Please Enter Proper Parent Email');
        addFocusId('parentEmail');
        return false;
    
    }else if( parentAddress1 == "" ){
        toastr.warning('Please Enter Parent Address1');
        addFocusId('parentAddress1');
        return false;
    
    // }else if( parentAddress2 == "" ){
    //     toastr.warning('Please Enter Parent Address2');
    //     addFocusId('parentAddress2');
    //     return false;
    
    // }else if( parentAddress3 == "" ){
    //     toastr.warning('Please Enter Parent Address3');
    //     addFocusId('parentAddress3');
    //     return false;
    
    }else if( parentState == "-1" || parentState == null ){
        toastr.warning('Please Select State');
        addFocusId('parentState');
        return false;
    
    }else if( parentCity == "-1" || parentCity == null ){
        toastr.warning('Please Select City');
        addFocusId('parentCity');
        return false;
    
    }else if( parentPincode == "" ){
        toastr.warning('Please Enter pincode');
        addFocusId('parentPincode');
        return false;
    
    }else if( parentPincode != "" && parentPincode < 6 ){
        toastr.warning('Please Enter valid pincode');
        addFocusId('parentPincode');
        return false;
    
    }else if( parentUsername == "" ){
        toastr.warning('Please Enter Parent Username');
        addFocusId('parentUsername');
        return false;
    
    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && (parentPassword == "") ){
    //     toastr.warning('Please Enter Parent Password');
    //     addFocusId('parentPassword');
    //     return false;
    
    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && (!validatePassword( parentPassword )) ){
    //     toastr.warning("Please enter strong password for parent detail");
    //     addFocusId('parentPassword');
    //     return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && (parentCnfPassword == "") ){
    //     toastr.warning("Please enter confirm password for parent detail");
    //     addFocusId('cnfpwd');
    //     return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") &&  (!validatePassword( parentCnfPassword )) ){
    //     toastr.warning("Please enter strong confirm password for parent detail");
    //     addFocusId('parentCnfPassword');
    //     return false;

    // }else if( localStorage.indeCampusEnrollType != 'edit' && (localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "") && (parentPassword != parentCnfPassword) ){
    //     toastr.warning("Password and confirm password not matched for parent detail!");
    //     addFocusId('parentCnfPassword');
    //     return false;

    }else if( collegeName == "" ){
        toastr.warning('Please Enter College Name');
        addFocusId('collegeName');
        return false;
    
    }else if( courseName == "" ){
        toastr.warning('Please Enter Course Name');
        addFocusId('courseName');
        return false;
    
    }else if( academicYearFrom == "" ){
        toastr.warning('Please Select Academic Year Start Date');
        addFocusId('academicYear');
        return false;
    
    }else if( academicYearTo == "" ){
        toastr.warning('Please Select Academic Year End Date');
        addFocusId('academicYearTo');
        return false;
    
    }else if( semester == "-1" || semester == "" ){
        toastr.warning('Please Select Academic Semester');
        addFocusId('semester');
        return false;
    
    }else if( collegeAddressLine1 == "" ){
        toastr.warning('Please Enter College Address Line 1');
        addFocusId('collegeAddressLine1');
        return false;
    
    // }else if( collegeAddressLine2 == "" ){
    //     toastr.warning('Please Enter College Address Line 2');
    //     addFocusId('collegeAddressLine2');
    //     return false;
    
    // }else if( collegeAddressLine3 == "" ){
    //     toastr.warning('Please Enter College Address Line 3');
    //     addFocusId('collegeAddressLine3');
    //     return false;
    
    }else if( collegePincode == "" ){
        toastr.warning('Please Enter College Pincode');
        addFocusId('collegePincode');
        return false;
    
    }else if( collegePincode != "" && collegePincode < 6  ){
        toastr.warning('Please Enter Valid College Pincode');
        addFocusId('collegePincode');
        return false;
    
    }else if( collegeState == "-1" || collegeState == null || collegeState == "" ){
        toastr.warning('Please Select College State');
        addFocusId('collegeState');
        return false;
    
    }else if( collegeCity == "-1" || collegeCity == null || collegeCity == "" ){
        toastr.warning('Please Select College City');
        addFocusId('collegeCity');
        return false;
    
    }else if( guarantorRelation == "-1" || guarantorRelation == null ){
        toastr.warning('Please Select Guarantor Relation');
        addFocusId('guarantorRelation');
        return false;
    
    }else if( guarantorName == "" ){
        toastr.warning('Please Enter Guarantor Name');
        addFocusId('guarantorName');
        return false;
    
    }else if( guarantorMobile1 == "" ){
        toastr.warning('Please Enter Guarantor&#39;s First Mobile Detail');
        addFocusId('guarantorMobile1');
        return false;
    
    }else if( guarantorMobile1 != "" && !checkValidMobile('guarantorMobile1') ){
        toastr.warning('Please Enter Proper Guarantor Mobile Detail');
        addFocusId('guarantorMobile1');
        return false;
    
    // }else if( guarantorMobile2 == "" ){
    //     toastr.warning('Please Enter Guarantor&#39;s Second Mobile Detail');
    //     addFocusId('guarantorMobile2');
    //     return false;
    
    }else if( guarantorMobile2 != "" && guarantorMobile2 != "+91" && !checkValidMobile('guarantorMobile2')  ){
        toastr.warning('Please Enter Proper Guarantor Mobile Detail');
        addFocusId('guarantorMobile2');
        return false;
    
    }else if( guarantorEmail == "" ){
        toastr.warning('Please Enter Guarantor Email');
        addFocusId('guarantorEmail');
        return false;
    
    }else if( guarantorEmail != "" && !validateEmail(guarantorEmail) ){
        toastr.warning('Please Enter Proper Guarantor Email');
        addFocusId('guarantorEmail');
        return false;
    
    }else if( guarantorAddress1 == "" ){
        toastr.warning('Please Enter Guarantor Address 1');
        addFocusId('guarantorAddress1');
        return false;
    
    // }else if( guarantorAddress2 == "" ){
    //     toastr.warning('Please Enter Guarantor Address 2');
    //     addFocusId('guarantorAddress2');
    //     return false;
    
    // }else if( guarantorAddress3 == "" ){
    //     toastr.warning('Please Enter Guarantor Address 3');
    //     addFocusId('guarantorAddress3');
    //     return false;
    
    }else if( guarantorPincode == "" ){
        toastr.warning('Please Enter Guarantor &#39; Pincode');
        addFocusId('guarantorPincode');
        return false;
    
    }else if(  guarantorPincode != "" && guarantorPincode < 6  ){
        toastr.warning('Please Enter Valid Guarantor &#39; Pincode');
        addFocusId('guarantorPincode');
        return false;
    
    }else if( guarantorState == "-1" || guarantorState == null || guarantorState == "" ){
        toastr.warning('Please Select Guarantor &#39; s State');
        addFocusId('guarantorState');
        return false;
    
    }else if( guarantorCity == "-1" || guarantorCity == null || guarantorCity == "" ){
        toastr.warning('Please Select Guarantor &#39; s City');
        addFocusId('guarantorCity');
        return false;
    
    }

    var dateStartDate = new Date(ddmmyyToMysql(academicYearFrom));
    var dateEndDate = new Date(ddmmyyToMysql(academicYearTo));

    //START END DATE VALIDATION
    if (+dateEndDate < +dateStartDate) { //dateActEndDate dateActStartDate
        toastr.warning("Academic year to cannot be less than the Academic year from");
        addFocusId('academicYearTo');
        return false;

    }
    // var phoneData = [];
    // var tempPhone = {
    //     mobileNo : mobileNumber,
    //     countryCode : getCountryDialCode('mobileNumber'),
    //     defaultFlag : 1
    // }
    // phoneData.push(tempPhone);

    // var emailData = [];
    // var tempEmail = {
    //     emailId : email,
    //     defaultFlag : 1
    // }
    // emailData.push(tempEmail);

    // var addressData = [];
    // var tempAdd = {
    //     address : address,
    //     pincode : pincode,
    //     defaultFlag : 1
    // }
    // addressData.push(tempAdd);

    if( guarantorMobile2 == "+91" ){
        guarantorMobile2 = "";
    }

	var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    if( localStorage.indeCampusEnrollType != 'edit' && localStorage.indeCampusEditStudId == undefined || localStorage.indeCampusEditStudId == "" ){
  
        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase : 'addStudent', // ADD STUDENT ENROLLMENT
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID,
            orgId : checkAuth(11,44),
            firstName : firstName,
            lastName : lastName,
            userName : username,
            password : "1234",
            mobileNo : mobileNumber,
            email : email,
            collegeName : collegeName,
            collegeAddressLine1 : collegeAddressLine1,
            collegeAddressLine2 : collegeAddressLine2,
            collegeAddressLine3 : collegeAddressLine3,
            collegeStateId : collegeState,
            collegeCityId : collegeCity,
            collegePincode : collegePincode,
            gender : gender,
            bloodGroup : bloodGroup,
            course : courseName,
            acdemicStartYear : academicYearFrom,
            acdemicEndYear : academicYearTo,
            semester : semester,
            countryId : country,
            addressLine1 : address1,
            addressLine2 : address2,
            addressLine3 : address3,
            pinCode : pincode,
            stateId : state,
            cityId : city,
            birthDate : birthdate,
            // aniversaryDate : anniversaryDate,
            parentRelation : parentRelation,
            parentName : parentName,
            parentMobileNo : parentMobile,
            parentEmailId : parentEmail,
            parentAddreLine1 : parentAddress1,
            parentAddreLine2 : parentAddress2,
            parentAddreLine3 : parentAddress3,
            parentStateId : parentState,
            parentCityId : parentCity,
            parentCountryId : country,
            parentPincode : parentPincode,
            parentUserName : parentUsername,
            parentPasword : "1234",
            guarantorRelation : guarantorRelation,
            guarantorName : guarantorName,
            guarantorAddressLine1 : guarantorAddress1,
            guarantorAddressLine2 : guarantorAddress2,
            guarantorAddressLine3 : guarantorAddress3,
            guarantorMobile1 : guarantorMobile1,
            guarantorMobile2 : guarantorMobile2,
            guarantorEmail : guarantorEmail,
            guarantorCityId : guarantorCity,
            guarantorStateId : guarantorState,
            guarantorCountryId : country,
            guarantorPincode : guarantorPincode,
            emergencyContactNo : emergencyPersonMobile,
            emergencyContactName : emergencyPersonName,
            aadharNo : aadharNumber,
            passportNo : passportNumber,
            nationality : nationality,
            medicalCondition : medicalCondition,
            // phone : phoneData,
            // email : emailData,
            // address : addressData,
            typeOfRoomRequired : roomCategory,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }
        // console.log( postData ); return false;
        commonAjax(COMMONURL, postData, createUpdateEnrollmentCallback,"Please Wait... Getting Dashboard Detail");

    }else{

        // Followup Listing Todays, Delayed, Upcomming
        var postData = {
            requestCase : 'editStudent', // UPDATE STUDENT ENROLLMENT
            clientId : tmp_json.data[0].FK_CLIENT_ID,
            userId : tmp_json.data[0].PK_USER_ID,
            orgId : checkAuth(11,46),
            fkStudentId : GBL_STD_ID,
            firstName : firstName,
            lastName : lastName,
            userName : username,
            // password : password,
            mobileNo : mobileNumber,
            email : email,
            collegeName : collegeName,
            collegeAddressLine1 : collegeAddressLine1,
            collegeAddressLine2 : collegeAddressLine2,
            collegeAddressLine3 : collegeAddressLine3,
            collegeStateId : collegeState,
            collegeCityId : collegeCity,
            collegePincode : collegePincode,
            gender : gender,
            bloodGroup : bloodGroup,
            course : courseName,
            acdemicStartYear : academicYearFrom,
            acdemicEndYear : academicYearTo,
            semester : semester,
            countryId : country,
            addressLine1 : address1,
            addressLine2 : address2,
            addressLine3 : address3,
            pinCode : pincode,
            stateId : state,
            cityId : city,
            birthDate : birthdate,
            // aniversaryDate : anniversaryDate,
            parentId : GBL_PARENT_ID,
            guarantorId : GBL_GUARANTOR_ID,
            parentRelation : parentRelation,
            parentName : parentName,
            parentMobileNo : parentMobile,
            parentEmailId : parentEmail,
            parentAddreLine1 : parentAddress1,
            parentAddreLine2 : parentAddress2,
            parentAddreLine3 : parentAddress3,
            parentStateId : parentState,
            parentCityId : parentCity,
            parentCountryId : country,
            parentPincode : parentPincode,
            parentUserName : parentUsername,
            // parentPasword : parentPassword,
            guarantorRelation : guarantorRelation,
            guarantorName : guarantorName,
            guarantorAddressLine1 : guarantorAddress1,
            guarantorAddressLine2 : guarantorAddress2,
            guarantorAddressLine3 : guarantorAddress3,
            guarantorMobile1 : guarantorMobile1,
            guarantorMobile2 : guarantorMobile2,
            guarantorEmail : guarantorEmail,
            guarantorCityId : guarantorCity,
            guarantorStateId : guarantorState,
            guarantorCountryId : country,
            guarantorPincode : guarantorPincode,
            emergencyContactNo : emergencyPersonMobile,
            emergencyContactName : emergencyPersonName,
            aadharNo : aadharNumber,
            passportNo : passportNumber,
            nationality : nationality,
            medicalCondition : medicalCondition,
            // phone : phoneData,
            // email : emailData,
            // address : addressData,
            typeOfRoomRequired : roomCategory,
            dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
        }
        // console.log( postData ); return false;
        commonAjax(COMMONURL, postData, createUpdateEnrollmentCallback,"Please Wait... Getting Dashboard Detail");

    }
}

// GET ENROLLMENT LISTING DATA CALLBACK
function createUpdateEnrollmentCallback( flag , data ){

	if (data.status == "Success" && flag) {
        console.log(data);
        $('#errorHideDiv').hide();
        $('#errorDiv').html('');
        localStorage.removeItem('indeCampusEditStudId');
        localStorage.indeCampusEnrollTab1Data = JSON.stringify(data.data[0]);
        GBL_STD_ID = data.data[0].studId;
        GBL_PARENT_ID = data.data[0].parentId;
        GBL_GUARANTOR_ID = data.data[0].guarantorId;
        if( localStorage.indeCampusEnrollType != 'edit' ){
            toastr.success("Your enrollment process was successfull");
            nextBtnClick('student');
            $('#saveTab1').attr('disabled',true);
            disableRefresh();  // DISABLE USER TO CLICK ANY BUTTONS OR MENU
        }else{
            if( localStorage.indeCampusEditFromDetail != undefined && localStorage.indeCampusEditFromDetail != false && localStorage.indeCampusEditFromDetail != "" ){
                window.history.back();
            }else{
                toastr.success("Your update enrollment process was successfull");
                navigateToEnrollmentMaster();
            }
        }

    } else {
    	
        $('#errorHideDiv').hide();
        $('#errorDiv').html('');
        if (flag){
            displayAPIErrorMsg( data.status , GBL_ERR_ADD_ENROLL );
            renderError(data.data);
            addFocusId('username');
        }
        else{
            toastr.error(SERVERERROR);
        }
    }

}

// RESET FIELDS DATA
function resetFields(type){

    if( type == "student" ){
        $('.passwordBlock').show();
        $('#firstName').val( '' );
        $('#lastName').val( '' );
        $('#birthdate').val( '' );
        $('#gender').val('').selectpicker('refresh');
        $('#bloodGroup').val( '' ).selectpicker('refresh');
        $('#address1').val( '' );
        $('#address2').val( '' );
        $('#address3').val( '' );
        $('#pincode').val('');
        
        $('#country').val( '' ).selectpicker('refresh');
        $('#state').val( '' ).selectpicker('refresh');
        $('#city').val( '' ).selectpicker('refresh');
        $('#mobileNumber').val( '' );

        $('#email').val( '' );
        $('#passportNumber').val( "" );
        $('#aadharNumber').val( '' );
        $('#medicalCondition').val( '' ).selectpicker('refresh');
        $('#emergencyPersonName').val( '' );
        $('#emergencyPersonMobile').val('');
        $('#username').val( '' );
        $('#password').val( '' );
        $('#cnfpwd').val( '');

        // STUDENT PARENT DETAIL TAB
        $('#parentRelation').val( '-1' ).selectpicker('refresh');
        $('#parentName').val( '' );
        $('#parentMobile').val('');
        $('#parentEmail').val( '' );
        $('#parentAddress1').val( '' );
        $('#parentAddress2').val( '' );
        $('#parentAddress3').val( '' );
        $('#parentState').val( '-1' ).selectpicker('refresh');
        $('#parentCity').val( '-1' ).selectpicker('refresh');
        $('#parentPincode').val( '' );
        $('#parentUsername').val( '' );
        $('#parentPassword').val( '' );
        $('#parentCnfPassword').val('');
        
        // STUDENT COLLEGE DETAIL TAB
        $('#collegeName').val('' );
        $('#courseName').val( '' );
        $('#academicYearFrom').val( '' );
        $('#academicYearTo').val( '' );
        $('#semester').val( '' ).selectpicker('refresh');
        $('#collegeAddressLine1').val( '' );
        $('#collegeAddressLine2').val( '' );
        $('#collegeAddressLine3').val( '' );
        $('#collegeCity').val( '' ).selectpicker('refresh');
        $('#collegeState').val('').selectpicker('refresh');
        $('#collegePincode').val( '' );

        // STUDENT GUARANTOR DETAIL TAB
        $('#guarantorRelation').val( '-1' ).selectpicker('refresh');
        $('#guarantorName').val( '' );
        $('#guarantorMobile1').val('');
        $('#guarantorMobile2').val('');
        $('#guarantorEmail').val( '' );
        $('#guarantorAddress1').val( '' );
        $('#guarantorAddress2').val( '' );
        $('#guarantorAddress3').val( '' );
        $('#guarantorCity').val( '' ).selectpicker('refresh');
        $('#guarantorState').val('' ).selectpicker('refresh');
        $('#guarantorPincode').val( '' );

        // STUDENT ROOM PREFERENCE TAB
        $('#roomCategory').val( '' ).selectpicker('refresh');
        $('#saveTab1').html( '<i class="fa fa-floppy-o"></i> Save Student Form' );

    }else if( type == "room" ){

        $('#roomType').val( '' ).selectpicker('refresh');
        $('#roomNumber').val( '' ).selectpicker('refresh');
        $('#bedNumber').val( '' );  
        $('#optionsRadios1').click();
        $('#assetsIssued').val('').selectpicker('refresh');

        // TENANCY DETAILS TAB
        $('#tenancyStartDate').val('');
        $('#tenancyEndDate').val('');
        $('#tenancyDeposit').val( '' );
        $('#totalRent').val( '' );

        // FACILITY ALLOCATION TAB
        $('#facilityAllocation').val( '' ).selectpicker('refresh');

    }else if( type == "invoicePayment" ){

        resetInvoice();
        renderInvoice([]);
        $('#invoiceListingDiv').hide();
        $('#paymentMode').val('').selectpicker('refresh');
        $('#bankName').val('');
        $('#branchName').val('');
        $('#chequeDDNeftNo').val('');
        $('#chequeDDNeftDate').val('');
        $('#paymentAmount').val('');
        $('#referenceInvoiceNo').val('').selectpicker('refresh');
        $('#paymentRemark').val('');
    }
}

// GET STUDENT ENROLLMENT DATA FOR GETTING DETAILS
function getStudentEnrollData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getStudentData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(11,45),
        studentId : localStorage.indeCampusEditStudId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getEnrollmentDataCallback,"Please Wait... Getting Dashboard Detail");

    function getEnrollmentDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            console.log(data.data);
            studentData = data.data[0];
            renderDataForEdit( studentData );
            // getAllotmentDetail();

        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_STUDENT_DET );
            else
                toastr.error(SERVERERROR);
        }

    }
}

// SET ENROLLMENT DETAIL FOR UPDATE
function renderDataForEdit( studentData ){

    ITEMS.push(studentData);
    GBL_INDEX = 0;
    GBL_STD_ID = studentData.studId;
    GBL_PARENT_ID = studentData.parentId;   
    GBL_GUARANTOR_ID = studentData.guarantorId;   
    $('.passwordBlock').hide();
    $('#firstName').val( studentData.firstName );
    $('#lastName').val( studentData.lastName );
    $('#birthdate').val( (studentData.birthDate != "0000-00-00" && studentData.birthDate != "1970-01-01" && studentData.birthDate != "" ? mysqltoDesiredFormat(studentData.birthDate,'dd-MM-yyyy') : "" ) );
    $('#gender').val( studentData.gender ).selectpicker('refresh');
    $('#bloodGroup').val( studentData.bloodGroup ).selectpicker('refresh');
    $('#address1').val( studentData.addressLine1 );
    $('#address2').val( studentData.addressLine2 );
    $('#address3').val( studentData.addressLine3 );
    $('#pincode').val( studentData.pincode );
    
    $('#country').val( studentData.countryId ).selectpicker('refresh');
    populateState('country','state');
    $('#state').val( studentData.stateId ).selectpicker('refresh');
    populateCityOnState('state','city');
    $('#city').val( studentData.cityId ).selectpicker('refresh');

    setNumberInIntl("mobileNumber",studentData.mobileNo);
    $('#email').val( studentData.email );
    $('#nationality').val( studentData.nationality ).selectpicker('refresh');
    $('#passportNumber').val( studentData.passportNo );
    $('#aadharNumber').val( studentData.aadharNo );
    $('#medicalCondition').val( studentData.medicalCondition ).selectpicker('refresh');
    $('#emergencyPersonName').val( studentData.emeregencyContactPersonName );
    setNumberInIntl('emergencyPersonMobile',studentData.emeregencyContactPersonNo);
    $('#username').val( studentData.userName );
    // $('#password').val( studentData.password );
    // $('#cnfpwd').val( studentData.password );

    // STUDENT PARENT DETAIL TAB
    $('#parentRelation').val( studentData.parentRelation ).selectpicker('refresh');
    $('#parentName').val( studentData.parentName );
    setNumberInIntl('parentMobile',studentData.parentMobileNo);
    $('#parentEmail').val( studentData.parentEmailId );
    $('#parentAddress1').val( studentData.parentAddressLine1 );
    $('#parentAddress2').val( studentData.parentAddressLine2 );
    $('#parentAddress3').val( studentData.parentAddressLine3 );
    $('#parentPincode').val( studentData.parentPincode );
    $('#parentUsername').val( studentData.parentUserName );
    // $('#parentPassword').val( studentData.parentPassword );
    // $('#parentCnfPassword').val( studentData.parentPassword );
    
    // STUDENT COLLEGE DETAIL TAB
    $('#collegeName').val( studentData.collegeName );
    $('#courseName').val( studentData.course );
    $('#academicYearFrom').val( (studentData.acdemicStartYear != "0000-00-00" ?  (studentData.acdemicStartYear) : "" )  );
    $('#academicYearTo').val( (studentData.acdemicEndYear != "0000-00-00" ?  (studentData.acdemicEndYear) : "" )  );
    $('#semester').val( studentData.semester  ).selectpicker('refresh');
    $('#collegeAddressLine1').val( studentData.collegeAddressLine1 );
    $('#collegeAddressLine2').val( studentData.collegeAddressLine2 );
    $('#collegeAddressLine3').val( studentData.collegeAddressLine3 );
    $('#collegeCity').val( studentData.collegeCityId ).selectpicker('refresh');
    $('#collegeState').val( studentData.collegeStateId ).selectpicker('refresh');
    $('#collegePincode').val( studentData.collegePinCode );

    // STUDENT GUARANTOR DETAIL TAB
    $('#guarantorRelation').val( studentData.guarantorRelation ).selectpicker('refresh');
    $('#guarantorName').val( studentData.guarantorName );
    setNumberInIntl('guarantorMobile1',studentData.guarantorMobileNo1);
    setNumberInIntl('guarantorMobile2',studentData.guarantorMobileNo2);
    $('#guarantorEmail').val( studentData.guarantorEmail );
    $('#guarantorAddress1').val( studentData.guarantorAddress1 );
    $('#guarantorAddress2').val( studentData.guarantorAddress2 );
    $('#guarantorAddress3').val( studentData.guarantorAddress3 );
    $('#guarantorCity').val( studentData.guarantorCityId ).selectpicker('refresh').trigger('change');
    $('#guarantorState').val( studentData.guarantorStateId ).selectpicker('refresh');
    $('#guarantorPincode').val( studentData.guarantorPinCode );

    // STUDENT ROOM PREFERENCE TAB
    $('#roomCategory').val( studentData.typeOfRoomRequired ).selectpicker('refresh');

    $('#saveTab1').html( '<i class="fa fa-floppy-o"></i> Update Student Form' );
    $('#tab1ResetBtn').hide();

    $('#roomFacilityDetailTab').hide();
    $('#roomFacilityDetailTab').parent('li').remove();
    $('#nextBtn').hide();
    // if( invoiceData != "" ){
    //     renderInvoice(invoiceData);
    //     $('#generateInvoiceBtn').attr('disabled',true);
    // }
    // disableRefresh();  // DISABLE USER TO CLICK ANY BUTTONS OR MENU
}


// GET TYPE OF ROOM DATA
function getRoomNumber(context){

    var itemId = $(context).val();
    if( itemId == "-1" || itemId == "" || itemId == undefined ){
        toastr.warning('Please Select Proper Room Type');
        $('#roomNumber').html( '' ).selectpicker('refresh');
        addFocusId('roomType');
        return false;
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'roomList', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(13 ,54),
        itemId : itemId,
        flag : 1,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getRoomDataCallback,"Please wait...Adding your item.");

    function getRoomDataCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ROOM_ITEMS = data.data;
            setRoomItems(); // SET ROOM NUMBER DATA
            
        }else{

            GBL_ROOM_ITEMS = [];
            setRoomItems(); // SET ROOM NUMBER DATA
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ROOMS );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// SET ROOM NUMBER DATA
function setRoomItems() {
   
    var innerHtml = '<option value="-1">Select Room Number</option>';
    if( GBL_ROOM_ITEMS != "No record found" && GBL_ROOM_ITEMS.length != 0 ){
        GBL_ROOM_ITEMS.forEach(function( record , index ){

            innerHtml += '<option value="'+ record.roomId +'">'+ record.roomName +'</option>';

        });
    }
    $('#roomNumber').html( innerHtml ).selectpicker('refresh'); // SET ROOM NUMBER DATA IN DROPDOWN
        
    // if( localStorage.indeCampusEnrollTab2Data != undefined && localStorage.indeCampusEnrollTab2Data != "" ){
    //     var tab2Json = JSON.parse(localStorage.indeCampusEnrollTab2Data);
    //     $('#roomNumber').val( tab2Json.roomId ).selectpicker('refresh');
    // }
    if( GBL_ALLOT_DATA_EDIT != "" && GBL_ALLOT_DATA_EDIT.length != 0 ){
        $('#roomNumber').val( GBL_ALLOT_DATA_EDIT.roomId ).selectpicker('refresh');
    }

}

// NEXT BUTTON CLICK EVENT
function nextBtnClick(type){

    // var curTab = $('.colorTab li.active a').attr('href');
    
    if( type == "student" ){
        $('#roomFacilityDetailTab').trigger('click');
    }else if( type == "room" ){
        $('#invoicePaymentDetailTab').trigger('click');
    }

}

// PREV BUTTON CLICK EVENT
function prevBtnClick(type) {

    if( type == "room" ){
        $('#studentDetailTab').trigger('click');
    }else if( type == "invoicePayment" ){
        $('#roomFacilityDetailTab').trigger('click');
    }

}

// CREATE ALLOTMENT PROCESS
function createAllotment() {
  
    /* ROOM & FACILITY ALLOCATION TAB */
    // ROOM DETAILS TAB
    var roomType = $('#roomType').val();
    // var roomCategory = $('#roomCategory').val();
    var roomNumber = $('#roomNumber').val();
    var roomName = $('#roomNumber option:selected').text();
    var bedNumber = $('#bedNumber').val();
    var foodPreference = $("input[name=optionsRadioFoodPref]:checked").val();
    var assetsIssued = $('#assetsIssued').val();

    // TENANCY DETAILS TAB
    var tenancyStDate = $('#tenancyStartDate').val();
    var tenancyEndDate = $('#tenancyEndDate').val();
    var tenancyDeposit = $('#tenancyDeposit').val();
    var totalRent = $('#totalRent').val();
    var startDate = new Date(ddmmyyToMysql(tenancyStDate));
    var endDate = new Date(ddmmyyToMysql(tenancyEndDate));

    // FACILITY ALLOCATION TAB
    var facilityAllocation = $('#facilityAllocation').val();
    var facilityList = "";
    facilityList = $("#facilityAllocation option:selected").map(function () {
        return $(this).val() + ":" + $(this).text();
    }).get().join(',');
    console.log(facilityList);
     // ASSETS LIST COMMA SEPERATED
    var assetsList = "";
    if( assetsIssued != "-1" && assetsIssued != null ){
        assetsIssued.forEach(function( assetRcd , assetIndex ){
            if( assetRcd != "-1" ){
                if( assetsList == "" ){
                    assetsList += assetRcd;
                }else{
                    assetsList += "," + assetRcd;
                }
            }
        });
    }
    console.log(assetsList);

    // FACILITY LIST COMMA SEPERATED
    // var facilityList = "";
    // if( facilityAllocation != "-1" && facilityAllocation != null ){
    //     facilityAllocation.forEach(function( facilityRcd , facilityIndex ){
    //         if( facilityRcd != "-1" ){
    //             if( facilityList == "" ){
    //                 facilityList += facilityRcd;
    //             }else{
    //                 facilityList += "," + facilityRcd;
    //             }
    //         }
    //     });
    // }
    // console.log(facilityList);

    // CHECK VALIDATIONS
    var enrollTabData = localStorage.indeCampusEnrollTab1Data;

    if( enrollTabData == undefined && studentData == "" ){
        toastr.warning("Please fill and submit student details");
        $('#studentDetailTab').click();
        addFocusId('firstName');
        return false;

    }else if( roomType == "-1" || roomType == null ){
        toastr.warning("Please select room type");
        addFocusId('roomType');
        return false;

    }else if( roomNumber == "-1" || roomNumber == null ){
        toastr.warning("Please select room number");
        addFocusId('roomNumber');
        return false;

    }else if( bedNumber == "" ){
        toastr.warning("Please enter bed number");
        addFocusId('bedNumber');
        return false;

    }else if( assetsIssued == null || assetsList == "" ){
        toastr.warning("Please select assets to allot");
        addFocusId('assetsIssued');
        return false;

    }else if( facilityList == "" ){
        toastr.warning("Please select atleast one facility");
        addFocusId('totalRent');
        return false;

    }else if( tenancyStDate == "" ){
        toastr.warning("Please select tenancy start date");
        addFocusId('tenancyStDate');
        return false;

    }else if( tenancyEndDate == "" ){
        toastr.warning("Please select tenancy end date");
        addFocusId('tenancyEndDate');
        return false;

    }else if( tenancyEndDate != "" && ( +endDate < +startDate ) ){
        toastr.warning("Please tenancy end date cannot be less than tenancy start date");
        addFocusId('tenancyEndDate');
        return false;

    }else if( tenancyDeposit == "" ){
        toastr.warning("Please enter tenancy deposit amount");
        addFocusId('tenancyDeposit');
        return false;

    }else if( tenancyDeposit != "" && ( parseFloat(tenancyDeposit) < 1 ) ){
        toastr.warning("Please enter valid tenancy deposit amount");
        addFocusId('tenancyDeposit');
        return false;

    }else if( totalRent == "" ){
        toastr.warning("Please enter tenancy total rent");
        addFocusId('totalRent');
        return false;

    }else if( totalRent != "" && ( parseFloat(totalRent) < 1 ) ){
        toastr.warning("Please enter valid tenancy total rent");
        addFocusId('totalRent');
        return false;

    }

    if( roomNumber == "-1" || roomNumber == "" ){
        roomName = "";
    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var requestCase = "addStudentAllotment";

    if( GBL_ALLOT_EDIT_FLAG == 1  ){
        requestCase = "editStudentAllotment";
    }


    var postData = {
        requestCase : requestCase, // ADD / EDIT ALLOTMENT DATA
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        studentId : GBL_STD_ID,
        itemId : roomType,
        roomId : roomNumber,
        roomName : roomName,
        bedNo : bedNumber,
        foodPreference : foodPreference,
        assetIssued : assetsList,
        tenancyStartDate : tenancyStDate,
        tenancyEndDate : tenancyEndDate,
        tenancyDeposit : tenancyDeposit,
        tenancyRent : totalRent,
        facilityAllocation : facilityList,
        studentPassword : "1234",
        parentsPasword : "1234",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // return false; console.log(postData);
    commonAjax(COMMONURL, postData, createUpdateEnrollmentCallback,"Please Wait... Getting Dashboard Detail");

   
    function createUpdateEnrollmentCallback( flag , data ){

        if (data.status == "Success" && flag) {
            console.log(data);

            localStorage.indeCampusEnrollTab2Data = JSON.stringify(data.data[0]);
            GBL_ALLOT_DATA_EDIT = [];
            // $('#studentAllotBtn').attr('disabled',true);
            // nextBtnClick('room');

            localStorage.removeItem('indeCampusEnrollTab1Data');
            localStorage.removeItem('indeCampusEnrollTab2Data');
            localStorage.removeItem('indeCampusEnrollInvoiceData');
            navigateToEnrollmentMaster();
        } else {
            
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_ASSIGN_ALLOT );
            }
            else{
                toastr.error(SERVERERROR);
            }
        }

    }
}

// GENERATE INVOICE 
function generateInvoice() {    

    /* INVOICE GENERATION & PAYMENT COLLECTION TAB */
    // INVOICE GENERATION DETAILS TAB
    var invoiceNumber = $('#invoiceNumber').val();
    var invoiceDate = $('#invoiceDate').val();
    var hsnCode = $('#hsnCode').val();
    var invoiceParticulars = $('#invoiceParticulars').val();
    var invoiceRate = $('#invoiceRate').val();
    var invoiceAmount = $('#invoiceAmount').val();
    var invoiceRemarks = $('#invoiceRemarks').val();

    // if( invoiceNumber == "" ){
    //     toastr.warning("Please enter invoice number");
    //     addFocusId('invoiceNumber');
    //     return false;

    // } 
    if( invoiceDate == "" ){
        toastr.warning("Please enter invoice date");
        addFocusId('invoiceDate');
        return false;

    }else if( hsnCode == "" ){
        toastr.warning("Please enter invoice hsn");
        addFocusId('hsnCode');
        return false;

    }else if( invoiceParticulars == "" ){
        toastr.warning("Please enter invoice particulars");
        addFocusId('invoiceParticulars');
        return false;

    }else if( invoiceAmount == "" ){
        toastr.warning("Please enter invoice amount");
        addFocusId('invoiceAmount');
        return false;

    }

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'createInvoiceStudentNew', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(16,65),
        studentId : GBL_STD_ID,
        invoiceDate : invoiceDate,
        // invoiceNumber : invoiceNumber,
        invoiceName : invoiceParticulars,
        hsnCode : hsnCode,
        itemName : invoiceParticulars,
        itemRate : invoiceRate,
        itemAmount : invoiceAmount,
        // invoiceRemarks : invoiceRemarks,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    // return false; console.log(postData);
    commonAjax(COMMONURL, postData, createUpdateEnrollmentCallback,"Please Wait... Getting Dashboard Detail");

    function createUpdateEnrollmentCallback( flag , data ){

        if (data.status == "Success" && flag) {
            console.log(data);
            GBL_INVOICE_DATA = data.data;
            localStorage.indeCampusEnrollInvoiceData = JSON.stringify(GBL_INVOICE_DATA);
            renderInvoice(GBL_INVOICE_DATA);
            resetInvoice();
            $('#generateInvoiceBtn').attr('disabled',true);

        } else {
            
            if (flag){
                displayAPIErrorMsg( data.status , GBL_ERR_ASSIGN_ALLOT );
            }
            else{
                toastr.error(SERVERERROR);
            }
        }

    }

}

// RENDER INVOICE DATA
function renderInvoice(invoiceData) {
    
    var tHead = '<table id="invoiceTable" class="display datatables-alphabet-sorting">'+
                    '<thead>'+
                        '<tr>'+
                            '<th>Student Name</th>'+
                            '<th>Invoice Name</th>'+
                            '<th>Invoice Date</th>'+
                            '<th>Total Charges</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>';                      
    var tEnd = '</tbody></table>';
    var newRow = '';
    var options = "";
    if( invoiceData != "No record found" && invoiceData.length != 0 ){

        invoiceData.forEach(function( record , index ) {

            newRow += '<tr>'+
                        '<td>'+ record.studName +'</td>'+
                        '<td>'+ record.invoiceName +'</td>'+
                        '<td>'+ ( record.invoiceDate != "0000-00-00" && record.invoiceDate != "" ? record.invoiceDate : "" ) +'</td>'+
                        '<td><i class="fa fa-rupee"></i> '+ record.totalCharges +'</td>'+
                    '</tr>';

            options += '<option value="'+ record.clientInvoiceId +'">'+ record.clientInvoiceId +'</option>';
        });

    }
    $('#invoiceTableDiv').html( tHead + newRow + tEnd );
    $('#invoiceTable').DataTable();
    $('#referenceInvoiceNo').html('');
    $('#referenceInvoiceNo').html( options );
    $('#referenceInvoiceNo').selectpicker('refresh');
    $('#invoiceListingDiv').show();
}

// RESET INVOICE FIELDS
function resetInvoice() {
    
    $('#invoiceNumber').val('');
    $('#invoiceDate').val('');
    $('#hsnCode').val('');
    $('#invoiceParticulars').val('');
    $('#invoiceRate').val('');
    $('#invoiceAmount').val('');
    $('#invoiceRemarks').val('');

}

// RENDER TAB1 DATA
function renderTab1(){

    if( localStorage.indeCampusEnrollTab1Data != undefined && localStorage.indeCampusEnrollTab1Data != "" ){

    var tab1Data = JSON.parse(localStorage.indeCampusEnrollTab1Data);
    ITEMS = [];
    ITEMS.push(tab1Data);
    GBL_INDEX = 0;
    GBL_STD_ID = tab1Data.studId;
    GBL_PARENT_ID = tab1Data.parentId;
    GBL_GUARANTOR_ID = tab1Data.guarantorId;
    $('#firstName').val( tab1Data.firstName );
    $('#lastName').val( tab1Data.lastName );
    $('#birthdate').val( (tab1Data.birthDate != "0000-00-00" && tab1Data.birthDate != "1970-01-01" && tab1Data.birthDate != "" ? mysqltoDesiredFormat(tab1Data.birthDate,'dd-MM-yyyy') : "" ) );
    $('#gender').val( tab1Data.gender ).selectpicker('refresh');
    $('#bloodGroup').val( tab1Data.bloodGroup ).selectpicker('refresh');
    $('#address1').val( tab1Data.addressLine1 );
    $('#address2').val( tab1Data.addressLine2 );
    $('#address3').val( tab1Data.addressLine3 );
    $('#pincode').val( tab1Data.lastName );
    
    $('#country').val( tab1Data.countryId ).selectpicker('refresh');
    $('#country').trigger('change')
    populateState('country','state');
    $('#state').val( tab1Data.stateId ).selectpicker('refresh');
    populateCityOnState('state','city');
    $('#city').val( tab1Data.cityId ).selectpicker('refresh');

    setNumberInIntl("mobileNumber",tab1Data.mobileNo);
    $('#email').val( tab1Data.email );
    $('#nationality').val( tab1Data.nationality ).selectpicker('refresh');
    $('#passportNumber').val( tab1Data.passportNo );
    $('#aadharNumber').val( tab1Data.aadharNo );
    $('#medicalCondition').val( tab1Data.medicalCondition ).selectpicker('refresh');
    $('#emergencyPersonName').val( tab1Data.emeregencyContactPersonName );
    setNumberInIntl('emergencyPersonMobile',tab1Data.emeregencyContactPersonNo);
    $('#username').val( tab1Data.userName );
    $('.passwordBlock').hide();
    // $('#password').val( tab1Data.password );
    // $('#cnfpwd').val( tab1Data.password );

    // STUDENT PARENT DETAIL TAB
    $('#parentRelation').val( tab1Data.parentRelation ).selectpicker('refresh')
    $('#parentName').val( tab1Data.parentName );
    setNumberInIntl('parentMobile',tab1Data.parentMobileNo);
    $('#parentEmail').val( tab1Data.parentEmailId );
    $('#parentAddress1').val( tab1Data.parentAddressLine1 );
    $('#parentAddress2').val( tab1Data.parentAddressLine2 );
    $('#parentAddress3').val( tab1Data.parentAddressLine3 );
    $('#parentPincode').val( tab1Data.parentPincode );
    $('#parentUsername').val( tab1Data.parentUserName );
    // $('#parentPassword').val( tab1Data.parentPassword );
    // $('#parentCnfPassword').val( tab1Data.parentPassword );
    
    // STUDENT COLLEGE DETAIL TAB
    $('#collegeName').val( tab1Data.collegeName );
    $('#courseName').val( tab1Data.course );
    $('#academicYearFrom').val( (tab1Data.acdemicStartYear != "0000-00-00" ?  (tab1Data.acdemicStartYear) : "" )  );
    $('#academicYearTo').val( (tab1Data.acdemicEndYear != "0000-00-00" ?  (tab1Data.acdemicEndYear) : "" )  );
    $('#semester').val( tab1Data.semester  ).selectpicker('refresh');
    $('#collegeAddressLine1').val( tab1Data.collegeAddressLine1 );
    $('#collegeAddressLine2').val( tab1Data.collegeAddressLine2 );
    $('#collegeAddressLine3').val( tab1Data.collegeAddressLine3 );
    $('#collegeCity').val( tab1Data.collegeCityId ).selectpicker('refresh');
    $('#collegeState').val( tab1Data.collegeStateId ).selectpicker('refresh');
    $('#collegePincode').val( tab1Data.collegePinCode );

    // STUDENT GUARANTOR DETAIL TAB
    $('#guarantorRelation').val( tab1Data.guarantorRelation );
    $('#guarantorName').val( tab1Data.guarantorName );
    setNumberInIntl('guarantorMobile1',tab1Data.guarantorMobileNo1);
    setNumberInIntl('guarantorMobile2',tab1Data.guarantorMobileNo2);
    $('#guarantorEmail').val( tab1Data.guarantorEmail );
    $('#guarantorAddress1').val( tab1Data.guarantorAddress1 );
    $('#guarantorAddress2').val( tab1Data.guarantorAddress2 );
    $('#guarantorAddress3').val( tab1Data.guarantorAddress3 );
    $('#guarantorCity').val( tab1Data.guarantorCityId ).selectpicker('refresh').trigger('change');
    $('#guarantorState').val( tab1Data.guarantorStateId ).selectpicker('refresh');
    $('#guarantorPincode').val( tab1Data.guarantorPinCode );

    // STUDENT ROOM PREFERENCE TAB
    $('#roomCategory').val( tab1Data.typeOfRoomRequired );

    }

}

// RENDER TAB2 DATA   
function renderTab2(){

    if( localStorage.indeCampusEnrollTab2Data != undefined && localStorage.indeCampusEnrollTab2Data != "" ){

        var tab2Data = JSON.parse(localStorage.indeCampusEnrollTab2Data);
        GBL_STD_ID = tab2Data.studId;
        $('#roomType').val( tab2Data.itemId ).selectpicker('refresh');
        $('#roomType').trigger('change')
        $('#roomNumber').val( tab2Data.roomId ).selectpicker('refresh');
        $('#bedNumber').val(  tab2Data.bedNo  );
        if( tab2Data.foodPreference == "Non Vegetarian" ){
            $('#optionsRadios2').click();
        }else{
            $('#optionsRadios1').click();
        }
        var assetIssuedList = tab2Data.assetIssued.split(',');
        $('#assetsIssued').val( assetIssuedList ).selectpicker('refresh');

        // TENANCY DETAILS TAB
        if( tab2Data.tenancyStartDate != "" && tab2Data.tenancyStartDate != "0000-00-00" ){
            $('#tenancyStartDate').datepicker( 'update' , mysqltoDesiredFormat( tab2Data.tenancyStartDate , 'dd-MM-yyyy' ) );
        }else{
            $('#tenancyStartDate').val('');
        }
        if( tab2Data.tenancyEndDate != "" && tab2Data.tenancyEndDate != "0000-00-00" ){
            $('#tenancyEndDate').datepicker( 'update' , mysqltoDesiredFormat( tab2Data.tenancyEndDate , 'dd-MM-yyyy' ) );
        }else{
            $('#tenancyEndDate').val('');
        }
        $('#tenancyDeposit').val( tab2Data.tenancyDeposit );
        $('#totalRent').val( tab2Data.tenancyRent );

        // FACILITY ALLOCATION TAB
        var facilityAllocationList = tab2Data.facilityAllocation.split(',');
        $('#facilityAllocation').val( facilityAllocationList ).selectpicker('refresh');
    }
}

// DISABLE USER TO CLICK ANY BUTTONS OR SIDEBAR MENU
function disableRefresh() {    

    $('#tab1ResetBtn').attr('disabled',true);
    $('#tab2ResetBtn').attr('disabled',true);
    $('#tab3ResetBtn').attr('disabled',true);
    $('.SideLeftMenu .actUserHide a').removeAttr('href');
    $('.SideLeftMenu .actUserHide a').removeAttr('onclick');
    $('.SideLeftMenu .actUserHide').removeAttr('href');
    $('.SideLeftMenu .actUserHide').removeAttr('onclick');
    $('.SideLeftMenu .actUserHide').addClass('disableNav');
    // $('.nav-logo a').removeAttr('onclick');
    $('#backBtn1').attr('disabled',true);
    $('#backBtn2').attr('disabled',true);

}

// GET FACILITY LIST DATA
function getFacilityData() {
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getItemsData', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(15,62),
        itemType : "SUBSC",
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, addItemCallback,"Please wait...Adding your item.");

    function addItemCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_FACILITY = data.data;
            setOptionWithRef("0",'facilityAllocation',GBL_FACILITY,'','itemId','itemName');
            $('#facilityAllocation').selectpicker('refresh');
            $('#facilityAllocation').selectpicker('selectAll');
            
        }else{

            GBL_FACILITY = [];
            setOptionWithRef("0",'facilityAllocation',GBL_FACILITY,'','itemId','itemName');
            $('#facilityAllocation').selectpicker('refresh');
            $('#facilityAllocation').selectpicker('selectAll');
            
            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_FACILITY_TYPE );
            else
                toastr.error(SERVERERROR);
        }
    }
}


// GET ALLOTMENT DETAIL OF SELECTED STUDENT
function getAllotmentDetail(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);

    var postData = {
        requestCase : 'getStudentAllotment', 
        clientId : tmp_json.data[0].FK_CLIENT_ID,
        userId : tmp_json.data[0].PK_USER_ID,
        orgId : checkAuth(11, 45),
        studentId : localStorage.indeCampusEditStudId,
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID),
    };
    commonAjax(FOLLOWUPURL, postData, getAllotmentDetailCallback,"Please wait...Adding your item.");

    function getAllotmentDetailCallback( flag , data ){
        if (data.status == "Success" && flag) {

            console.log(data.data);
            GBL_ALLOT_DATA_EDIT = data.data[0];
            // IF HAVE DATA , RENDER AVAILABLE DATA FOR UPDATE
            if( GBL_ALLOT_DATA_EDIT != "No record found" && GBL_ALLOT_DATA_EDIT != "" ){
                renderAllotmentDataForEdit(GBL_ALLOT_DATA_EDIT);  // RENDER AVAILABLE DATA FOR UPDATE  
                $('#roomFacilityDetailTab').click();
                GBL_ALLOT_EDIT_FLAG = 1;
            }
           
        }else{

            if (flag)
              displayAPIErrorMsg( data.status , GBL_ERR_NO_ALLOT_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

// RENDER ALLOTMENT DATA FOR UPDATE
function renderAllotmentDataForEdit(GBL_ALLOT_DATA_EDIT) {
    
    var allotmentData = GBL_ALLOT_DATA_EDIT;
    $('#roomType').val( allotmentData.itemId ).selectpicker('refresh');
    $('#roomType').trigger('change');
    $('#roomNumber').val( allotmentData.roomId ).selectpicker('refresh');
    $('#bedNumber').val(  allotmentData.bedNo  );
    if( allotmentData.foodPreference == "Non Vegetarian" ){
        $('#optionsRadios2').click();
    }else{
        $('#optionsRadios1').click();
    }
    var assetIssuedList = allotmentData.assetIssued.split(',');
    $('#assetsIssued').val( assetIssuedList ).selectpicker('refresh');

    // TENANCY DETAILS TAB
    if( allotmentData.tenancyStartDate != "" && allotmentData.tenancyStartDate != "0000-00-00" ){
        $('#tenancyStartDate').datepicker( 'update' , mysqltoDesiredFormat( allotmentData.tenancyStartDate , 'dd-MM-yyyy' ) );
    }else{
        $('#tenancyStartDate').val('');
    }
    if( allotmentData.tenancyEndDate != "" && allotmentData.tenancyEndDate != "0000-00-00" ){
        $('#tenancyEndDate').datepicker( 'update' , mysqltoDesiredFormat( allotmentData.tenancyEndDate , 'dd-MM-yyyy' ) );
    }else{
        $('#tenancyEndDate').val('');
    }
    $('#tenancyDeposit').val( allotmentData.tenancyDeposit );
    $('#totalRent').val( allotmentData.tenancyRent );

    // FACILITY ALLOCATION TAB
    var facilityAllocationList = allotmentData.facilityAllocation.split(',');
    $('#facilityAllocation').val( facilityAllocationList ).selectpicker('refresh');
    
}

// FILLS SAME DATA AS FILLED IN PARENT'S FIELDS IF CHECKED THE CHECKBOX
function fillSameAsParent(context){

    var ifSameAsAbove = $(context).prop('checked');

    if( ifSameAsAbove == true ){

        $('#guarantorRelation').val( $('#parentRelation').val() ).selectpicker('refresh');
        $('#guarantorName').val( $('#parentName').val() );
        setNumberInIntl('guarantorMobile1',$('#parentMobile').val());
        $('#guarantorEmail').val( $('#parentEmail').val() );
        $('#guarantorAddress1').val( $('#parentAddress1').val() );
        $('#guarantorAddress2').val( $('#parentAddress2').val() );
        $('#guarantorAddress3').val( $('#parentAddress3').val() );
        $('#guarantorState').val( $('#parentState').val() ).selectpicker('refresh');
        $('#guarantorState').trigger('change');
        $('#guarantorCity').val( $('#parentCity').val() ).selectpicker('refresh');
        $('#guarantorPincode').val( $('#parentPincode').val() );

    }else{
        
        $('#guarantorRelation').val('-1').selectpicker('refresh');
        $('#guarantorName').val('');
        setNumberInIntl('guarantorMobile1','');
        $('#guarantorEmail').val('');
        $('#guarantorAddress1').val('');
        $('#guarantorAddress2').val('');
        $('#guarantorAddress3').val('');
        $('#guarantorState').val('-1').selectpicker('refresh');
        $('#guarantorCity').val('-1').selectpicker('refresh');
        $('#guarantorPincode').val('');

    }

}

// SET TENANCY END DATE AS PER SELECTED TENANCY START DATE
function setEndDate(context){
    var startDate = $(context).val();

    $('#tenancyEndDate').datepicker('remove');
    $('#tenancyEndDate').datepicker({
        format: 'dd-mm-yyyy',
        startDate: startDate,
        // endDate: startDate,
        autoclose: true
    });
    $('#tenancyEndDate').attr('readonly',true);
}

// SET ACADEMIC END DATE AS PER SELECTED TENANCY START DATE
function setAcademicYear(context){
    var startDate = $(context).val();

    $('#academicYearTo').datepicker('remove');
    $("#academicYearTo").datepicker( {
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months",
        startDate: startDate,
        autoclose:true
    });
    $('#academicYearTo').attr('readonly',true);
}