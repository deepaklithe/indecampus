/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 08-01-2015.
 * File : eTrackController.
 * File Type : .js.
 * Project : Google Map Api
 *
 * */

  var GBL_CNT = 0;
  var GBL_INDEX_VAR = [];
  var map = [];
  var markers = [];
  var directionsService = [];
  var directionsDisplay = [];
  var waypts_Cnt = 10;      //By Default 10 because google allow only 8 waypoints 1st & 10th initial and end point
  var optimizedRoute = false;
  var trackerData = [];
  var srNoWp = 0;

  var mapData = [];

function getTrackData(){

    $('#eTrackDate').html( mysqltoDesiredFormat(localStorage.POTGeTRACKDate,'dd-MM-yyyy') );
    $('#eTrackUname').html( localStorage.POTGeTRACKUname );

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'eTrackingData',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        requestUserId: localStorage.POTGeTRACKUId,
        cdate: localStorage.POTGeTRACKDate,
        userName: localStorage.POTGeTRACKUname,
        orgId: checkAuth(5,18),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getTrackDataCallback,"Please Wait... Getting Dashboard Detail");

    function getTrackDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            if(data.data != 'No record found'){

                loadJS('indeCampusJs/markerwithlabel.js' , 'markerLabelId');

                trackerData = data.data;

                setTimeout(function(){
                  
                  calculateAndDisplayRoute(directionsService, directionsDisplay);
                  
                  $(".loader").fadeIn('fast').delay(2000).fadeOut('slow');
                  $('#submit').attr('value','Find Next Route');
                  
                },1000);
            
            }else{
                alert('No record found');
                navigateToTracking();
            }

        } else {
            if (flag){ 
                alert(data.status);
                navigateToTracking();
            }
            else{ 
                alert(SERVERERROR);
                navigateToTracking();
            }
        }
    }

}

function currLoc(){

  // REINTIALIZE THE MAP AFTER CURRENT LOCATION SET START HERE 
  directionsDisplay.setMap(null);
  mapData = [];
  GBL_CNT = 0;
  GBL_INDEX_VAR = [];
  srNoWp = 0;

  // REINTIALIZE THE MAP AFTER CURRENT LOCATION SET END HERE

  var currLoc = trackerData.length;
  
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  mapData.push( trackerData[ currLoc - 1 ] );

  calculateAndDisplayLastRoute(directionsService, directionsDisplay);
}

//IT WILL INITIALIZE THE GOOGLE MAP WITH LAT 0 AND LONG 0
function initMap() {
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat: 0, lng: 0},
  });

  directionsDisplay.setMap(map);

//  console.log(trackerData);
  $('#submit').click(function(){
      // directionsDisplay.setMap(null);
      // directionsDisplay.setDirections({routes: []});
      calculateAndDisplayRoute(directionsService, directionsDisplay);
  });

  getTrackData(); // GET DATA FOR API CALL
}

//IT WILL CALL THE FUNCTION TO CLEAR ALL THE MARKERS
  function clearMarkers() {
    setMapOnAll(null);
  }


//IT WILL INITIALIZE THE FUNCTION TO CLEAR ALL THE MARKERS
  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  } 

//IT WILL CALCULATE THE ROUTE AND DISPLAY THE ROUTE USING DIRECTION DISPLAY SERVICES
function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        
   var waypoints = [];
   
   for( var i=0; i<($(trackerData).length); i++ ){
       
       var latitude = trackerData[i].latitude;
       var logitude = trackerData[i].longitude;
       
       var temp = {
           location : new google.maps.LatLng( latitude , logitude ),
           stopover : true,
       }
       
       waypoints.push(temp);
   }
    
    
  clearMarkers(); //call the funtion to remove all the markers
  markers = []; //nullify the array which are created to store markers

    
  GBL_INDEX_VAR = [];
  for ( var i=GBL_CNT; i<(GBL_CNT + waypts_Cnt); i++ ){ // it will give the chunks of data to render in google api
      if( waypoints[i] !== undefined ){
        
        srNoWp = srNoWp + 1;
        
        GBL_INDEX_VAR.push(waypoints[i]);

        var marker = new MarkerWithLabel({
            position: waypoints[i].location,
            map: map,
            labelContent: srNoWp +" - "+trackerData[i].CTIME,
            labelClass: "labels",
            labelAnchor: new google.maps.Point(15, 65),
        });

        markers.push(marker);
      }
  }

  if ( GBL_INDEX_VAR != "" ){

    GBL_CNT = GBL_CNT + waypts_Cnt;
      
   var startPoint = GBL_INDEX_VAR[0].location;  //starting point of the route
   var endPoint = GBL_INDEX_VAR[GBL_INDEX_VAR.length-1].location;  //ending point of the route

   GBL_INDEX_VAR.splice(0,1);
   GBL_INDEX_VAR.splice(GBL_INDEX_VAR.length-1,1);
   
   //ACTUAL INITIALIZATION OF MAP WITH ALL THE DATA STPOINT, ENDPOINT, & WAYPOINTS
    directionsService.route({
      origin: startPoint,
      destination: endPoint,
      waypoints: GBL_INDEX_VAR,
      optimizeWaypoints: optimizedRoute,
      travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      } else {
        toastr.error('Directions request failed due to ' + status);
      }
    });
  
  }else{
    //WHEN NEXT ROUTE IS NOT FOUND THAN THIS BLOCK WILL BE CALLED AND THIS BLOCK CAN SENT TO THE INITIAL SEARCHED ROUTE
    srNoWp = 0;
    toastr.warning('No more Routes available , Now you Redirected to Initial Route');
    GBL_CNT = 0;  //reintialize the count
    calculateAndDisplayRoute(directionsService, directionsDisplay);  //recall the function for calculate and display route
  } 

  $(".loader").fadeOut("slow");
}


//IT WILL CALCULATE THE ROUTE AND DISPLAY THE ROUTE USING DIRECTION DISPLAY SERVICES
function calculateAndDisplayLastRoute(directionsService, directionsDisplay) {
        
   var waypoints = [];
          
   var latitude = mapData[0].latitude;
   var logitude = mapData[0].longitude;
   
   var temp = {
       location : new google.maps.LatLng( latitude , logitude ),
       stopover : true,
   }
   
   waypoints.push(temp);
    
  clearMarkers(); //call the funtion to remove all the markers
  markers = []; //nullify the array which are created to store markers
    
  GBL_INDEX_VAR = [];
  GBL_INDEX_VAR.push(waypoints[0]);

  var marker = new MarkerWithLabel({
      position: waypoints[0].location,
      map: map,
      labelContent: mapData[0].CTIME,
      labelClass: "labels",
      labelAnchor: new google.maps.Point(15, 65),
  });


  if ( GBL_INDEX_VAR != "" ){
    
   var startPoint = GBL_INDEX_VAR[0].location;  //starting point of the route
   var endPoint = GBL_INDEX_VAR[0].location;  //ending point of the route
   
   //ACTUAL INITIALIZATION OF MAP WITH ALL THE DATA STPOINT, ENDPOINT, & WAYPOINTS
    directionsService.route({
      origin: startPoint,
      destination: endPoint,
      waypoints: GBL_INDEX_VAR,
      optimizeWaypoints: optimizedRoute,
      travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      } else {
        toastr.error('Directions request failed due to ' + status);
      }
    });
  
  }else{
    calculateAndDisplayRoute(directionsService, directionsDisplay);  //recall the function for calculate and display route
  } 
}