    /*
 *
 * Created By : Vishakha Tak.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 07-09-2018.
 * File : requestComplaintSummaryController.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
var GBL_REPORT_DATA = [];
var GBL_CSVJSON = [];

// PAGE LOAD EVENTS
function pageInitialEvents(){

    getRequestComplaintSummary();
}

// GET REQUEST COMPLAINT SUMMARY ON DEPARTMENT AND USER
function getRequestComplaintSummary(){
    
    
    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);    
    
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'studentDepartmentwiseComplaintReport',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(26,105),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }
    commonAjax(COMMONURL, postData, getRequestComplaintSummaryCallback,"Please Wait... Getting Dashboard Detail");

    function getRequestComplaintSummaryCallback(flag,data){

        if (data.status == "Success" && flag) {
                
            console.log(data);
            GBL_REPORT_DATA = data.data;
            if( GBL_REPORT_DATA && GBL_REPORT_DATA != "No record found"){
                GeneratePivotTable(GBL_REPORT_DATA);
            }else{
                displayAPIErrorMsg( "" , "No request / complaint summary data found");
            }

        }else{

            GBL_REPORT_DATA = [];
            GeneratePivotTable([]); 
                
            if (flag){
                
                displayAPIErrorMsg( data.status , "No request / complaint summary data found" );
                // toastr.error(data.status);
            }else{
                toastr.error(SERVERERROR);
            }
        }
        // $('.Sidenav').removeClass('open');  
    }

}


// GENERATE REPORT PIVOT TABLE
function GeneratePivotTable(mps) {

    console.log(mps);
    var Sum = $.pivotUtilities.aggregators["Sum"];
    $("#reqCompSummaryDiv").pivot(mps, {
        aggregator: Sum(["Status Count"]),
        rows: ["Type" ,"Student Name" ,  "Department Name"],
        cols: ["Status Name"]
    });

    var tempJson = {
        type : "Type",
        studName : "Student Name",
        deptName : "Department Name",
        statusName : "Student Name",
        count : "Count",
    }
    GBL_CSVJSON.push(tempJson);

    if( GBL_REPORT_DATA && GBL_REPORT_DATA != "No record found"){

        for(var i=0; i<GBL_REPORT_DATA.length; i++){

            var tempJson = {
                type : GBL_REPORT_DATA[i]['Type'],
                studName : GBL_REPORT_DATA[i]['Student Name'],
                deptName : GBL_REPORT_DATA[i]['Department Name'],
                statusName : GBL_REPORT_DATA[i]['Student Name'],
                count : GBL_REPORT_DATA[i]['Status Count'],
            }
            GBL_CSVJSON.push(tempJson);

        }
        // console.log(GBL_CSVJSON);
    }
}

// RESET FILTER
function resetFilters(){

    $('#studentName').val('');
    $('#studentName').attr('studId','');
   
}

// CLEAR SELECTED FILTERS
function clearFilters(){

    resetFilters();
    GeneratePivotTable(GBL_REPORT_DATA); 

}

// DOWNLOAD REPORT IN EXCEL FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadExcel(){
    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No record found" ){
        toastr.warning('You dont have any data to download excel');
    }else{
        customCsvFormatSave($('#reqCompSummaryDiv').html(), "Request & Complaint Summary Report.xls");
    }
}


// DOWNLOAD REPORT IN PDF FORMAT - ADDED BY VISHAKHA TAK ON 07-09-2018
function downloadPdfFile(){

    if( GBL_REPORT_DATA.length < 1 || GBL_REPORT_DATA == "No Record Found" ){

        toastr.warning('You dont have any data to download pdf');
        return false;
    }else{

        var arr = [];
        pdfSave(GBL_CSVJSON,arr, "Request_&_Complaint_Summary_Report.pdf",20);
    }

}