/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 02-07-2018.
 * File : lib.
 * File Type : .js.
 * Project : indeCampus
 *
 * */
 
$(document).ready(function(){ 
//    $("input[type='text'], textarea, input[type='password']").bind("cut copy paste",function(e) { e.preventDefault(); }); 
//    var ua = navigator.userAgent;
//    var event = ua.match(/(iphone|ipod|ipad)/) ? 'touchstart' : 'click';
});

var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

 //removes \ from given string
function stripslashes(value){
    return value.replace(/\\/g, '');
}

//disable right click on page
function disableContextMenu() {
    //alert("dsfasdf");
    $(document).bind("contextmenu", function (e) {
        e.preventDefault();
    });
    window.addEventListener('keydown', function (event) {
        // if the keyCode is 123 ( f12 key was pressed )
        //16 17 18 cltr+shift+alter
        if((event.ctrlKey && event.keyCode == 77)){
            window.print();
        }else if((event.ctrlKey && event.keyCode == 66)){
            reloadWin();
        }
        if ( (event.ctrlKey && event.keyCode == 80) || (event.ctrlKey && event.keyCode == 82) || (event.ctrlKey && event.keyCode == 67) ||(event.ctrlKey && event.keyCode == 86) || (event.keyCode >= 112 &&event.keyCode <= 123) ||  ( event.ctrlKey && event.shiftKey && event.keyCode == 73)) {

            // prevent default behaviour
            event.preventDefault();
            toastr.warning(DISABLEKEY);
            return false;
        }

    });
}

//10 digit time stamp like php
function getPhpTimeStamp() {
    return parseInt((new Date().getTime()) / 1000);
}

//chk compilable browser for sqlite db
function browserChk() {
    if (navigator.appVersion.indexOf("Chrome") != -1) {
    } else if (navigator.userAgent.indexOf("Safari") != -1) {
    } else {
        toastr.warning(WRONGBROWSER);
    }
}

//minified version of console.log
function log(msg) {
    console.log(msg);
}

// return date in YYYYMONDDMMSS format
function getDateValue() {
    var date = new Date();
    var datevalue = date.getFullYear().toString() + (date.getMonth() + 1).toString() + date.getDate().toString() + date.getMinutes() + date.getSeconds();
    return datevalue;
}

/* return date in YYYYMONDD format
 function getDateValue() {
 var date = new Date();
 var datevalue = date.getFullYear().toString() + (date.getMonth() + 1).toString() + date.getDate().toString();
 return datevalue;
 }*/

//return timestamp in YYYY-Mon-dd hh:mm:ss
function getTodaysTimeStamp(timestamp) {
    var today;
    if(timestamp == undefined){
        today = new Date();
    }else{
        today = new Date(parseInt(timestamp)*1000);
    }

    var dd = today.getDate();
    var mon = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var mm = today.getMinutes();
    var ss = today.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mon < 10) {
        mon = '0' + mon
    }
    if (hh < 10) {
        hh = '0' + hh
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    if (ss < 10) {
        ss = '0' + ss
    }

    today = yyyy + '-' + mon + '-' + dd + " " + hh + ":" + mm + ":" + ss;
    return today;
}

//return date in php format
function getTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' + dd;
    return today;
}


//return string in Title Case
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

//fix precision to 2 decimal point
function fixedTo2(value){
    return parseFloat(value).toFixed(2);
}

//gives upper rounded value for eg 10.5 = 10 and 10.6 = 11
function ceil(value){
    return Math.round(value);
}

//get basename (php) of url - rpatel - 04/22/14 -- It gives without extension file name
function baseName(str)
{
    var base = new String(str).substring(str.lastIndexOf('/') + 1);
    if(base.lastIndexOf(".") != -1)
        base = base.substring(0, base.lastIndexOf("."));
    return base;
}

// it gives with extension of file name
function basenameWithExt(path) {
   return path.split('/').reverse()[0];
}

//check user session - rpatel - 04/22/14
function chkusr_session(){
    var cururl = location.href;
    var login_Username = localStorage.indeCampusGetItem('username');
    var login_Session = localStorage.indeCampusGetItem('loginSession');
    var login_Status = localStorage.indeCampusGetItem('loginStatus');
    var last_Url = localStorage.indeCampusGetItem('lastUrl');
    if (login_Username && last_Url && login_Session && login_Status && login_Status == CryptoJS.AES.decrypt(login_Session, login_Username) && (cururl.indexOf('index.html') > -1 || baseName(cururl).length == 0)) {
        window.location.href = last_Url;
    } else if ((baseName(cururl).length > 0 && baseName(cururl) != 'index') && (!login_Username || !login_Session || !login_Status || login_Status != CryptoJS.AES.decrypt(login_Session, login_Username))) {
        logout();
    }
}

// this function remove place holder value and return blank value
$.fn.valPlaceHolder = function(){
    var $this = $(this),
        val = $this.eq(0).val();
    if(val == $this.attr('placeholder'))
        return '';
    else
        return val;
}

//Returns index or -1 if the key doesn't exist
function findIndexByKeyValue(obj, key, value)
{
    //log(obj+"----"+key+"-----"+value);
    //log("obj length"+obj.length);
    for (var i = 0; i < obj.length; i++) {
        //log(obj[i][key]+"=="+value);
        if (obj[i][key] == value) {
            return i;
        }
    }
    return -1;
}

function findIndexByKeyValueClosure(obj, key, value)
{
        //log(obj[key]+"---"+key+"---"+ value);
        if (obj[key] == value) {
            return 3;
        }else{
            return -1;
        }
}
//fix range in inputbox
function limitNumberInRang(input) {
    if (parseInt(input.value) < parseInt(MINNUMFORRANGE)){
        input.value = MINNUMFORRANGE;
    }
    if (parseInt(input.value) > parseInt(MAXNUMFORRANGE)){
        input.value = MAXNUMFORRANGE;
    }
}

// //pdf save file data  Created by Mishil Patel - 16-6-2014
// function pdfSave(pdfdata,fieldwidth,filename,pedding){
//     //log(pdfdata); return false;
//     if(pdfdata.length > 0){
//         pedding = pedding || 0;
//         var doc = new jsPDF();
// //        var specialElementHandlers = {
// //            '#editor': function (element, renderer) {
// //                return true;
// //            }
// //        };

//         var index = 0;
//         var doc = new jsPDF('l','pt', 'a4', true);
//         doc.setFontSize(10)
//         //.splitTextToSize('loremipsum', 7.5)
//         doc.cellInitialize();
//         //log(pdfdata); return false;
//         $.each(pdfdata, function (i, row){
//             //console.log(i);
//             //console.log(row);
//             index = 0;
//             $.each(row, function (j, cell){
                
//                 console.log(cell);
// //                var data = cell.split(" ");
// //                //log(data);
// //                var finalData = "";
// //                var rowHeight = 0;
// //                if(index != 0){
// //                    for(var k=0;k<data.length;k++){
// //                        finalData += data[k]+'<br>';
// //                    }
// //                }else{
// //                    finalData = cell;
// //                }
                
//                 var rowHeight = 20;
//                 //log(finalData);
//                 doc.cell(pedding,0,fieldwidth[index],rowHeight,cell,i);  // 2nd parameter=top margin,1st=left margin 3rd=row cell width 4th=Row height
//                 //doc.text(rowHeight,fieldwidth[index] , finalData);
                
//                 index++;
//             })
//         });


//         doc.save(filename);
//     }else{
//         alert("No data available to create pdf.");
//     }
    
// }

//pdf save file data  Created by Mishil Patel - 16-6-2014
function pdfSave(pdfdata,fieldwidth,filename,pedding){
    //log(pdfdata); return false;
    if(pdfdata.length > 0){
        
        var columns = [];
        var data = [];
        var doc = new jsPDF('l', 'pt','a4', true);
        index = 0;
        $.each(pdfdata, function (i, row){
            
            
            var mainArray = [];
            $.each(row, function (j, cell){

                mainArray.push(cell);
            })
            console.log(index);
            if( index == 0 ){
                columns = mainArray;
            }else{
                data.push(mainArray);
            }
            index++;
        });

        var header = filename.split(".")[0];
        doc.text(header, 30, 30);
        doc.autoTable(columns, data, {
            // startY: doc.autoTableEndPosY() + 45,
            startY: 45,
            margin: {horizontal: 10},
            styles: {overflow: 'linebreak'},
            bodyStyles: {valign: 'top'},
            columnStyles: {email: {columnWidth: 'wrap'}},
        });

        doc.save(filename);
    }else{
        toastr.warning(NOPDF);
    }
    
}
//end pdf save file data

//convert json data to csv string  Created by Mishil Patel - 16-6-2014
function JSON2CSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    
        var str = '';
        var line = '';

        if ($("#labels").is(':checked')) {
            var head = array[0];
            if ($("#quote").is(':checked')) {
                for (var index in array[0]) {
                    var value = index + "";
                    line += '"' + value.replace(/"/g, '""') + '",';
                }
            } else {
                for (var index in array[0]) {
                    line += index + ',';
                }
            }

            line = line.slice(0, -1);
            str += line + '\r\n';
        }

        for (var i = 0; i < array.length; i++) {
            var line = '';

            if ($("#quote").is(':checked')) {
                for (var index in array[i]) {
                    var value = array[i][index] + "";
                    line += '"' + value.replace(/"/g, '""') + '",';
                }
            } else {
                for (var index in array[i]) {
                    line += array[i][index] + ',';
                }
            }

            line = line.slice(0, -1);
            str += line + '\r\n';
        }
        return str;
    
}
//convert json data to csv string

//csv save file data  Created by Mishil Patel - 16-6-2014
function csvSave(csvdata,filename){
    var csv =JSON2CSV(csvdata);
        //csv = "<table border=2><tr><td>Mishil Patel</td><td> Pending</td></tr><tr><td>Deepak Patil</td><td> DONE</td></tr></table>"
    if(csv.length > 0){
        //log(window.URL); return false;
        window.URL = window.URL || window.webkiURL;
        //window.URL = httpHead + "//192.168.129.205/development/eBqMs/eBqMs/reports-inquiry.html";
        log(window.URL);
        //log(csv); 
        var blob = new Blob([csv]);
        var blobURL = window.URL.createObjectURL(blob);
        //to use this function you need to add this html in your page in footer. so it will append download link in the div and click it
        //<div id="downloadLink"></div>
        $("#downloadLink").html("");
        $("<a id='nazim'></a>").
            attr("href", blobURL).
            attr("download", filename).
            //attr("download", "deepak.xls").
            text("Download Data").
            appendTo('#downloadLink');
        //alert("hii");
        //setTimeout(function(){
        $('#nazim')[0].click();
        //},10);
    }else{
       toastr.warning(NOCSV); 
    }    
    
}
//csv save file data

function customCsvFormatSave(csvdata,filename){
    var csv =csvdata;
        //csv = "<table border=2><tr><td>Mishil Patel</td><td> Pending</td></tr><tr><td>Deepak Patil</td><td> DONE</td></tr></table>"
    if(csv.length > 0){
        //log(window.URL); return false;
        window.URL = window.URL || window.webkiURL;
        //window.URL = httpHead + "//192.168.129.205/development/eBqMs/eBqMs/reports-inquiry.html";
        log(window.URL);
        //log(csv); 
        var blob = new Blob([csv]);
        var blobURL = window.URL.createObjectURL(blob);
        //to use this function you need to add this html in your page in footer. so it will append download link in the div and click it
        //<div id="downloadLink"></div>
        $("#downloadLink").html("");
        $("<a id='mishil'></a>").
            attr("href", blobURL).
            attr("download", filename).
            //attr("download", "deepak.xls").
            text("Download Data").
            appendTo('#downloadLink');
        //alert("hii");
        //setTimeout(function(){
        $('#mishil')[0].click();
        //},10);
    }else{
       toastr.warning(NOCSV); 
    }    
    
}

//print report data  Created by Mishil Patel - 16-6-2014
function printReport(){

    var display_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    display_setting += "scrollbars=yes,width=280px, height=400px, left=100, top=25";
    var document_print = window.open("printreport.html", "Print", display_setting);
    var timer = setInterval(checkChild, 500);

    function checkChild() {
        if (document_print.closed) {
            clearInterval(timer);
            navigateToDashboard();
        }
    }
}

//give time stamp of php of give date from 00:00 to 23:59 hrs. you need to split using , Created by Vrushank Rindani - 21-6-2014
function startToEndTimestampOfGivanDate(date1,date2){
    var tmp_sdate = ddmmyyToMysql(date1);
    var tmp_edate = ddmmyyToMysql(date2);
    //log(tmp_sdate);
    var myStartDate= tmp_sdate.split("-");
    var newStartDate=myStartDate[0]+"/"+myStartDate[1]+"/"+myStartDate[2];
    var myEndDate= tmp_edate.split("-");
    var newEndDate=myEndDate[0]+"/"+myEndDate[1]+"/"+myEndDate[2];
    //log(newDate);
    var sdate = parseInt((new Date(newStartDate+ " 00:00" ).getTime()) / 1000);
    var edate = parseInt((new Date(newEndDate+ " 23:59" ).getTime()) / 1000);
    return sdate + "," + edate;
}


function startToEndTimestampOfGivanDateWithTime(date1,date2){
    var tmp_sdate = date1.split(" ");
    var tmp_edate = date2.split(" ");
    //log(tmp_sdate[0]);
    //log(ddmmyyToMysql(tmp_sdate[0]));
    //log(tmp_edate);
    
    //log(convertAMPMto24Hrformat(tmp_sdate[1]+" "+tmp_sdate[2])+"---"+convertAMPMto24Hrformat(tmp_edate[1]+" "+tmp_edate[2])); return false;
    
    var sdate = parseInt((new Date(ddmmyyToMysql(tmp_sdate[0])+" "+convertAMPMto24Hrformat(tmp_sdate[1]+" "+tmp_sdate[2])).getTime()) / 1000);
    var edate = parseInt((new Date(ddmmyyToMysql(tmp_edate[0])+" "+convertAMPMto24Hrformat(tmp_edate[1]+" "+tmp_edate[2])).getTime()) / 1000);
    
    //log(sdate +"***"+edate); return false;
    return sdate + "," + edate;
}

/* This function is convert timestamp to date in dd-mm-yyyy format START*/
function getDateFromPhpTimeStamp(timestamp){
    return new Date(parseInt(timestamp)*1000).format(localStorage.indeCampusDateFormat);
}
/* This function is convert timestamp to date in dd-mm-yyyy format END*/



//give time stamp of php of give date from 00:00 hrs. Created by Vrushank Rindani - 21-6-2014
function dateTOPhpTimeStamp(date){
    return parseInt((new Date(date + " 00:00").getTime()) / 1000);
}

/*this function gives date in desired format START*/
function mysqltoDesiredFormat(date,format){
    if(format == undefined){ 
        format = localStorage.indeCampusDateFormat;
    }else if(date === "0000-00-00" || date === "00-00-0000"){
        return date;
    }
    return new Date(date).format(format);
}


/*this function gives date in desired format END*/


/*this function gives date in desired format START*/
function ddmmyyToMysql(date,format){
    date = date.split("-").reverse().join("-");
    if(format == undefined) format = "yyyy-MM-dd";
    return new Date(date).format(format);

}
/*this function gives date in desired format END*/



/*SQL demp Insert Support START*/
function insertSQLDump(insertValue){
    var tmp_start_value = insertValue.split("VALUES");
    var tmp_value = tmp_start_value[1].split("),");

    var query = "";
    for (var i=0;i<tmp_value.length;i++) {
        if(i == tmp_value.length-1){
            query += tmp_start_value[0]+" VALUES "+ tmp_value[i];
        }else{
            query += tmp_start_value[0]+" VALUES "+ tmp_value[i]+"),"+"|";
        }
    }

    var q = query.split(",|");


    ePOSDB.transaction(function(tx){
        for(var a=0;a< q.length;a++){
            log(q[a] + " ====== "+a);
            tx.executeSql(q[a], [], function (tx, result) {}, dbErrorHandler);
        }
    }, dbErrorHandler);


}

/*SQL demp Insert Support END*/


/* + - * / on array start*/
function aggregationOnArray(arr,field,valType){
//alert(arr[0][field]);
    // valType == 1 INT
    // valType == 0 FLOAT
    if (valType == undefined) valType = 1;
    //valType = valType || 1;

    var tmp_ans= 0;
    for(var i = 0 ; i < arr.length ; i++){
        if(valType == 1){
            tmp_ans += parseInt(arr[i][field]);
        }else{
            tmp_ans += parseFloat(arr[i][field]);
        }
    }

    return tmp_ans;
}



/* + - * / on array end*/

//replace all occurrence from given string
function replaceAll(str,search,replace){

    var reg = new RegExp(search, 'g');
    return str.replace(reg,replace);

}

//calculate difference between to date in days
function findDateDifferenceInDays(sdate,edate){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(sdate);
    var secondDate = new Date(edate);

    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
}


//fix range in inputbox by Given Value
function limitNumberInRangByValue(input,maxval) {
    //var MAX = $("#"+id).val();
    //log(parseInt(input.value));
    //log(maxval);
    if (parseInt(input.value) > parseInt(maxval)){
        //log("1---");
        input.value = parseInt(maxval);
    }else if(input.value  == " " || input.value  == ""){
        //log("2---");
        input.value = parseInt(0);
    }else{
        //log("3---");
        input.value = parseInt(input.value);
    }
}

/*this function reload page created by,vrushank START*/
function reloadWin(){
    window.location.reload();
}
/*this function reload page created by,vrushank END*/


/*give 12hr time from given date 12:30 PM */
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
/*give 12hr time from given date 12:30 PM */

/*give 12hr time from given date 12:30 PM */
function formatAMPMRoundOff(date) {
    var hours = date.getHours();    //gives hour
    var minutes = date.getMinutes();    //gives minutes
    var ampm = hours >= 12 ? 'PM' : 'AM';   //gives AM PM format according to hours
    minutes = minutes < 10 ? '0'+minutes : minutes;    //add 0 if minutes is less than 10
    minutes = (Math.ceil(minutes / 10) * 10);    //round off to 10 minutes
    hours = (minutes == 60) ? hours + 1 : hours;    //plus 1 add in hour if minute is 60.
    minutes = (minutes == 60) ? '00' : minutes;    //set 00 is minute if minute is 60.
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
/*give 12hr time from given date 12:30 PM */

/**/
function commonAjax(url,data,callback,bfsendMsg){
    
    // console.log(data);
    $.ajax({
        type: 'POST',
        url: url,
        data: {postData: data},
        dataType: 'json',
        timeout: TIMEOUT,
        crossDomain: true,
        headers: { 
            'Authorization': 'Bearer ' + ((localStorage.indeCampusSignatureToken != "") ? localStorage.indeCampusSignatureToken : ""),
        },
        beforeSend: function() {
            $("#msgDiv").html(bfsendMsg);
            $("#loader-wrapper").show();
            $("body").removeClass("loaded");
            var date = new Date();
            localStorage.indeCampusCurrentTime = date.getTime();
        },
        success: function (data, textStatus, jqXHR) { 
            // callback(true,data);
            
            if( jqXHR.getResponseHeader('Authorization') != null ){
                localStorage.indeCampusSignatureToken = jqXHR.getResponseHeader('Authorization').split(' ')[1];
            }else{ 
                localStorage.indeCampusSignatureToken = "";
            }
            callback(jqXHR.status,data);

            $("#loader-wrapper").hide();
            $("body").addClass("loaded");
        },
        error: function (jqXHR, errdata, errorThrown) {
            $("#loader-wrapper").hide();
            $("body").addClass("loaded");
            // callback(false,errdata,errorThrown);

            if(jqXHR.responseJSON.status == "Expired token" && jqXHR.status == "401"){
                localStorage.indeCampusSignatureToken = jqXHR.getResponseHeader('Authorization').split(' ')[1];
                alert('Your session has been expired! Please login again.');
                navigateToIndex();
            }

            callback(jqXHR.status,jqXHR.responseJSON,errorThrown);
        }
    });
}

/**/

/**/
function generateDatatable(tableid,columnNum){

    var a = $('#' + tableid).dataTable({
        "oLanguage": {
            "sSearch": "",
            "sLengthMenu": " _MENU_ "
        },
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        // "aaSorting": [
        //     [1, 'asc']
        // ],
        "order": [
            [ 1, "asc" ]
        ],
        "bRetrieve": true,
        "bDestroy": true,
        "bDrawing": false,
        "bPaginate":true,
        "bFilter": true,
        "stateSave": true
    });
    //alert(tableid); return false;
    if(!INQUIRYPAGEURL){
        $('#' + tableid + ' tbody tr td img').on('click', function () { // start function when click on + for first column detail with item name ,qty and uom.
            var nTr = $(this).closest('tr')[0];
            //console.log(nTr);
            //console.log(a);
            var aData = a.fnGetData(nTr);
            //console.log(aData);
            var abc = aData[columnNum];

            if (a.fnIsOpen(nTr))
            {
                /* This row is already open - close it */
                this.src = "./img/table-data-open.png";
                a.fnClose(nTr);

            }

            else
            {
                //console.log(abc);
                /* Open this row */
                this.src = "./img/table-data-close.png";
                a.fnOpen(nTr, abc, 'details');
            }


        });
    }

    return a;
}
/**/

/**/

function setOption(idclass,id,data,defaultMsg){
    //idclass  = 0 (id)
    //idclass  = 1 (class)
    //log("caller is " + arguments.callee.caller.name);
    //log(data); return false;
    var innerhtml = "";
    if(defaultMsg.trim().length <= 0)
        innerhtml = "";
    else
        innerhtml += '<option value="-1">'+defaultMsg+'</option>' ;
        
    if( data != "No record found" ){
        for(var i=0;i<data.length;i++){
            
            var selected = "";
            if( data[i].ISDEFAULTFLAG != undefined ){
                if( data[i].ISDEFAULTFLAG == "1" ){
                    selected = "selected";
                }
            }
            innerhtml += '<option '+selected+' value="'+data[i].ID+'">'+data[i].VALUE+'</option>' ;
        }
    }
    //log("inner html"+innerhtml)
    if(idclass == 0){
        $("#"+id).html(innerhtml);
    }else{
        $("."+id).html(innerhtml);
    }
}

function setOptionWithDefaultChecked(idclass,id,data,defaultMsg){
    //idclass  = 0 (id)
    //idclass  = 1 (class)
    //log("caller is " + arguments.callee.caller.name);
    
    var innerhtml = "";
    if(defaultMsg.trim().length <= 0)
        innerhtml = "";
    else
        innerhtml += '<option value="-1">'+defaultMsg+'</option>' ;
        
      
    for(var i=0;i<data.length;i++){
        var selected = "";
        if(i==0){
            selected = "selected";
        }
        
        innerhtml += '<option '+selected+' value="'+data[i].ID+'">'+data[i].VALUE+'</option>' ;
    }
    //log("inner html"+innerhtml)
    if(idclass == 0){
        $("#"+id).html(innerhtml);
    }else{
        $("."+id).html(innerhtml);
    }
}

function setOptionWithRef(idclass,id,data,defaultMsg,keyRef,valueRef){
    //idclass  = 0 (id)
    //idclass  = 1 (class)
    //log("caller is " + arguments.callee.caller.name);
    //log(data); return false;
    var innerhtml = "";
    if(defaultMsg.trim().length <= 0)
        innerhtml = "";
    else
        innerhtml += '<option value="-1">'+defaultMsg+'</option>' ;
        
    if( data != "No record found" ){
        for(var i=0;i<data.length;i++){
            
            var selected = "";
            innerhtml += '<option '+selected+' value="'+data[i][keyRef]+'">'+data[i][valueRef]+'</option>' ;
        }
    }
    //log("inner html"+innerhtml);return false;
    if(idclass == 0){
        $("#"+id).html(innerhtml);
    }else{
        $("."+id).html(innerhtml);
    }
}


function setOptionCustomDataType(idclass,id,data,defaultMsg){
    //idclass  = 0 (id)
    //idclass  = 1 (class)
    //log("caller is " + arguments.callee.caller.name);
    
    var innerhtml = "";
    if(defaultMsg.trim().length <= 0)
        innerhtml = "";
    else
        innerhtml += '<option value="-1">'+defaultMsg+'</option>' ;
        
      
    for(var i=0;i<data.length;i++){
        var selected = "";
        if(i==0){
            selected = "";
        }
        
        innerhtml += '<option '+selected+' value="'+data[i].ID+'" data-itemSub-Type = '+data[i].SUB_TYPE_ID+'>'+data[i].VALUE+'</option>' ;
    }
    //log("inner html"+innerhtml)
    if(idclass == 0){
        $("#"+id).html(innerhtml);
    }else{
        $("."+id).html(innerhtml);
    }
}
/**/


/**/
function getSelectedOptionById(id){
    return $("#"+id+" option:selected").text();
}
/**/

/**/
function getSelectedOptionByClass(id){
    return $("."+id+" option:selected").text();
}
/**/


//Returns index or -1 if the key doesn't exist
function findIndexByKeyValueAndCond(obj, key1,key2, value1,value2)
{
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key1] == value1 && obj[i][key2] == value2 ) {
            return i;
        }
    }
    return -1;
}

if (typeof String.prototype.startsWith != 'function') {
    // see below for better implementation!
    String.prototype.startsWith = function (str){
        return this.indexOf(str) == 0;
    };
}

//this will chk email id format
function validateEmail(email) {

    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var res = pattern.test(email);

    if (res) {
        return true;
    } else {
        return false;
    }
}

//this will chk password format
function validatePassword(password) {

    var pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    var res = pattern.test(password);

    if (res) {
        return true;
    } else {
        return false;
    }
}

function validateOnlyLetterSpace(event) {
    //log(char);
    var inputValue = (event.which) ? event.which : event.keyCode;
    // allow letters and whitespaces only.
    if((inputValue > 47 && inputValue < 58) && (inputValue != 32)){
        event.preventDefault();
    }
}

function validateOnlyNumber(event) {
    
    var inputValue = event.which;
    if (inputValue!= 8 && inputValue != 0 && (inputValue < 48 || inputValue > 57)){
        event.preventDefault();
    }
}

function validateAlphaNumeric(event){ // Alphanumeric only
    if (event.shiftKey || event.ctrlKey || event.altKey) {
        event.preventDefault();
    } else {
        var key = (event.which) ? event.which : event.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) || key == 32) {
            event.preventDefault();
        }
    }
 }
 
function validatePhoneNumberWithCountryCode(event){ // Alphanumeric only
    var key = (event.which) ? event.which : event.keyCode;
    if((key >=48 && key <=57) || key == 43){
    }else{
        event.preventDefault();
    }
 }
 
function validateOnlyStringWithSpecialChar(event){ // Alphanumeric only
    var key = (event.which) ? event.which : event.keyCode;
    if((key >= 65 && key <= 90) || (key >= 96 && key <= 122) || (key == 46) || key == 32){
    }else{
        event.preventDefault();
    }
 }

//this will chk Date id format
function validateDateTimeFormat(dateTime) {

    var pattern = /\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))/;
    // var pattern = /^\d{1,2}:\d{2}([AP]M)?$/;
    var res = pattern.test(dateTime);

    if (res) {
        return true;
    } else {
        return false;
    }
}
/**/
function getFirstAndLastDateOfGivenDate(date){
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth()+1, 0);
    var monthLength = (lastDay - firstDay) / (1000 * 60 * 60 * 24)
    return (firstDay.format('yyyy-MM-dd')+"||"+lastDay.format('yyyy-MM-dd')+"||"+(++monthLength));
}
/**/


/**/
Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    return Date.isLeapYear(this.getFullYear());
};

Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
Date.prototype.subMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() - value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + days);
    return this;
};

// date formater Vrushank 28-04-2014
//Eg. new Date().format("MM/dd/yy");
Date.prototype.format = function(format)
{
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
        "S" : this.getMilliseconds() //millisecond
    }

    if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
        (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)if(new RegExp("("+ k +")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length==1 ? o[k] :
                ("00"+ o[k]).substr((""+ o[k]).length));
    return format;
}
/**/


/**/
function findMatchingPair(data,key,value){
    var tmp_Arr =[];
    for(var i=0;i<data.length;i++){
        if(data[i][key] == value){
            tmp_Arr.push(data[i]);
        }
    }
    if(tmp_Arr.length<=0){
        return -1;
    }else{
        return tmp_Arr;
    }
}

/**/

/**/
function sendFile(file,url){
    log("file obj");
    log(file.type);
    log(file.totalSize);

    $.ajax({
        type: 'POST',
        url: url,
        data: file,
        success: function () {
            // do something
        },
        xhrFields: {
            // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
            onprogress: function (progress) {
                // calculate upload progress
                var percentage = Math.floor((progress.total / progress.totalSize) * 100);
                // log upload progress to console
                console.log('progress', percentage);
                if (percentage === 100) {
                    console.log('DONE!');
                }
            }
        },
        processData: false,
        contentType: file.type
    });
}
/**/

/**/
function generateSecurityCode(cId, uId){
    
    return btoa(randomString(10)+ md5(cId+'|'+uId)+randomString(10)+"|--|"+btoa(localStorage.indeCampusDeviceId+"|--|"+localStorage.indeCampusDeviceName+"|--|"+localStorage.indeCampusDeviceactivateDate));
    //return btoa(randomString(10)+ md5(cId|uId)+randomString(10));
}

function generateDeviceToken(){
    return btoa(localStorage.indeCampusDeviceId+"|--|"+localStorage.indeCampusDeviceName+"|--|"+localStorage.indeCampusDeviceactivateDate);
}
/**/

/**/
function randomString(length) {
    return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}
/**/

function formatPriceInIndianFormat(price){
    price=price.toString();
    var afterPoint = '';
    if(price.indexOf('.') > 0)
        afterPoint = price.substring(price.indexOf('.'),price.length);
    price = Math.floor(price);
    price=price.toString();
    var lastThree = price.substring(price.length-3);
    var otherNumbers = price.substring(0,price.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;
}

function in_array(needle, haystack, argStrict) {
  //  discuss at: http://phpjs.org/functions/in_array/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: vlado houba
  // improved by: Jonas Sciangula Street (Joni2Back)
  //    input by: Billy
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
  //   returns 1: true
  //   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
  //   returns 2: false
  //   example 3: in_array(1, ['1', '2', '3']);
  //   example 3: in_array(1, ['1', '2', '3'], false);
  //   returns 3: true
  //   returns 3: true
  //   example 4: in_array(1, ['1', '2', '3'], true);
  //   returns 4: false

  var key = '',
    strict = !! argStrict;

  //we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] == ndl)
  //in just one for, in order to improve the performance 
  //deciding wich type of comparation will do before walk array
  if (strict) {
    for (key in haystack) {
      if (haystack[key] === needle) {
        return true;
      }
    }
  } else {
    for (key in haystack) {
      if (haystack[key] == needle) {
        return true;
      }
    }
  }

  return false;
}

function convertAMPMto24Hrformat(time) {
//log(time);
var hrs = Number(time.match(/^(\d+)/)[1]);
var mnts = Number(time.match(/:(\d+)/)[1]);
var format = time.match(/\s(.*)$/)[1].trim();
//log(hrs+"--"+mnts+"---"+format);
//log(format == "PM" );
if (format == "PM" && hrs <= 12) hrs = hrs + 12;
if (format == "AM" && hrs == 12) hrs = hrs - 12;
var hours = hrs.toString();
var minutes = mnts.toString();
if (hrs < 10) hours = "0" + hours;
if (mnts < 10) minutes = "0" + minutes;
return hours + ":" + minutes;

}

function convert24HrformattoAMPM(time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}

function getAMPMFromGivenDateTimeStdFormat(dateTime) {
    
    var currentTime = new Date(dateTime);
    //log(currentTime);
    var date = currentTime.getDate();
    var month = ("0" + (currentTime.getMonth() + 1)).slice(-2);
    var year = currentTime.getFullYear();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();

    if (minutes < 10)
        minutes = "0" + minutes;

    var suffix = "AM";
    if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
    }
    if (hours == 0) {
        hours = 12;
    }
    var current_time = hours + ":" + minutes + " " + suffix;
    return date+"-"+month+"-"+year+" "+current_time;
}


function uniqueArrayList(list) {
    var result = [];
    $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
    });
    return result;
}

function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),   // Convert the passed timestamp to milliseconds
        yyyy = d.getFullYear(),
        mm = ('0' + (d.getMonth() + 1)).slice(-2),  // Months are zero based. Add leading 0.
        dd = ('0' + d.getDate()).slice(-2),         // Add leading 0.
        hh = d.getHours(),
        h = hh,
        min = ('0' + d.getMinutes()).slice(-2),     // Add leading 0.
        ampm = 'AM',
        time;
            
    if (hh > 12) {
        h = hh - 12;
        ampm = 'PM';
    } else if (hh === 12) {
        h = 12;
        ampm = 'PM';
    } else if (hh == 0) {
        h = 12;
    }
    
    // ie: 2013-02-18, 8:35 AM  
    time = dd + '-' + mm + '-' + yyyy + ' ' + h + ':' + min + ' ' + ampm;
        
    return time;
}

function sortDates(a, b)
{
   return a.getTime() - b.getTime();
}

//function convertStrToNumber(str) {
//    return(str.replace(/\,/g,''));
//}



function trimLength(text, maxLength)
{
    var ellipsis = "...";
    text = $.trim(text);

    if (text.length > maxLength)
    {
        text = text.substring(0, maxLength - ellipsis.length);
        return text.substring(0, text.lastIndexOf(" ")) + ellipsis;
    }
    else
        return text;
}

function stripslashes(str) {
 
  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
}

function getMonthAndDateFromDate(date){
    var newDate = date.split("-");
    var month = newDate[2]+'-'+months[(parseInt(newDate[1])-1)];
    return (months[(parseInt(newDate[1])-1)] == undefined) ? '' : month;
}

function openclose1(el,columnNum,tableId){
    
    var nTr = $(el).closest('tr')[0];
    var aData = tableId.fnGetData(nTr);
    var abc = aData[columnNum];
    if (tableId.fnIsOpen(nTr)){
        /* This row is already open - close it */
        el.src = "./img/table-data-open.png";
        tableId.fnClose(nTr);

    }else
    {
        /* Open this row */
        el.src = "./img/table-data-close.png";
        tableId.fnOpen(nTr, abc, 'details');
    }
}
// THIS FUNCTION WILL USED FOR 2 DATA TABLE INITILIZED IN SINGLE PAGE
function openclose(el,columnNum){
    
    var nTr = $(el).closest('tr')[0];
    var aData = gTable.fnGetData(nTr);
    var abc = aData[columnNum];
    if (gTable.fnIsOpen(nTr)){
        /* This row is already open - close it */
        el.src = "./img/table-data-open.png";
        gTable.fnClose(nTr);

    }else
    {
        /* Open this row */
        el.src = "./img/table-data-close.png";
        gTable.fnOpen(nTr, abc, 'details');
        $(".chkLbl").prop("checked", false);
        initCheckBox();
    }
    checkEvent();
}

//START FUNCTION FOR OPEN MODAL BY THIS ELEMENT BY VAIBHAV ON 01-Jun-2016
function openModal(dataElement){
    //var modaldivId = $(dataElement).attr("data-div");
    var modaldivId = $(dataElement).data("div");
    $("#"+modaldivId).modal("show");
}
//END FUNCTION FOR OPEN MODAL BY THIS ELEMENT

//It minus mintues from given time starts here

function minusFromTime( date , minTime ){

    var date=new Date( date );
    var minutes=date.getMinutes();
    var seconds=date.getSeconds();
    var milliseconds=date.getMilliseconds();
    date.setMinutes(minutes-minTime,seconds,milliseconds);
    var HH=date.getHours();
    var MM=date.getMinutes();
    HH<10?HH='0'+HH:null;
    MM<10?MM='0'+MM:null;
    var ampm = HH >= 12 ? 'PM' : 'AM';
    var hours = HH > 12 ? (HH - 12) : HH;
    return hours+':'+MM + ' '+ ampm ;
}

//It minus mintues from given time ends here


//It minus mintues from given time starts here

function PlusFromTime( date , plusTime ){

    var date=new Date( date );
    var minutes=date.getMinutes();
    var seconds=date.getSeconds();
    var milliseconds=date.getMilliseconds();
    date.setMinutes(minutes+plusTime,seconds,milliseconds);
    var HH=date.getHours();
    var MM=date.getMinutes();
    HH<10?HH='0'+HH:null;
    MM<10?MM='0'+MM:null;
    var ampm = HH >= 12 ? 'PM' : 'AM';
    var hours = HH > 12 ? (HH - 12) : HH;
    return hours+':'+MM + ' '+ ampm ;
}

//It minus mintues from given time ends here

//Remove value of give input
function resetDate(idName,relatedId){
    if(idName){
        $("#"+idName).val('');
        $("."+idName).val('');
    }
    if(relatedId){
        $("#"+relatedId).val('');
        $("."+idName).val('');
    }
}

function numberFormat(x) {
    var amt = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if( amt.indexOf(".") == "-1" ){
        return amt + '.00';
    }else{
        return amt;
    }
}


 function htmlspecialchars_decode (string, quoteStyle) { 

  var optTemp = 0
  var i = 0
  var noquotes = false

  if (typeof quoteStyle === 'undefined') {
    quoteStyle = 2
  }
  string = string.toString()
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  }
  if (quoteStyle === 0) {
    noquotes = true
  }
  if (typeof quoteStyle !== 'number') {
    // Allow for a single string or an array of string flags
    quoteStyle = [].concat(quoteStyle)
    for (i = 0; i < quoteStyle.length; i++) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if (OPTS[quoteStyle[i]] === 0) {
        noquotes = true
      } else if (OPTS[quoteStyle[i]]) {
        optTemp = optTemp | OPTS[quoteStyle[i]]
      }
    }
    quoteStyle = optTemp
  }
  if (quoteStyle & OPTS.ENT_HTML_QUOTE_SINGLE) {
    // PHP doesn't currently escape if more than one 0, but it should:
    string = string.replace(/&#0*39;/g, "'")
    // This would also be useful here, but not a part of PHP:
    // string = string.replace(/&apos;|&#x0*27;/g, "'");
  }
  if (!noquotes) {
    string = string.replace(/&quot;/g, '"')
  }
  // Put this in last place to avoid escape being double-decoded
  string = string.replace(/&amp;/g, '&')

  return string
}

//Added By Nazim khan checks image is available or not
function imageExists(image_url){

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;

}


// IT WILL ADDS THE DYNAMIC SCRIPT TAG IN CURRENT PAGE
function loadJS(file,id) {
    // DOM: Create the script element
    var jsElm = document.createElement("script");
    // set the type attribute
    jsElm.type = "application/javascript";
    // make the script element load file
    jsElm.src = file;
    jsElm.id = id;
    // finally insert the element to the body element in order to load the script
    document.body.appendChild(jsElm);
}

// IT WILL REMOVE ALL THE LOCAL STORAGE WHICH WILL PRODUCE FROM THE DATA TABLE STATE SAVE
function removeDataTableLogLocalStorage( projectName ){

    if( projectName == undefined ){
        return false;
    }
    for (var key in localStorage){
        if( key.search( 'DataTables_' ) != "-1" && key.search( projectName ) != "-1" ){
            console.log("Removed Local Storage - "+ projectName +" "+ key );
            localStorage.removeItem( key );
        }
    }
}

//IT COMPARES TWO JSON WITH ITS INNER DATA
function jsonEqual(a,b) {
    return JSON.stringify(a) === JSON.stringify(b);
}

function modalHide( modalId ){
    $('#'+modalId).modal('hide');
}

function modalShow( modalId ){
    $('#'+modalId).modal('show');
}

// ADD TIME IN GIVEN MINUTES
function addTimeInMinutes( id , time , addMin ){

    if( time != undefined ){
        
        time = new Date( ddmmyyToMysql(time.split(" | ")[0]) + " " + time.split(" | ")[1]);

        var actDateTime = new Date( time.setMinutes(time.getMinutes() + addMin) );
        var onlyDate = mysqltoDesiredFormat( actDateTime , 'dd-MM-yyyy' );
        var onlyTime = formatAMPM( actDateTime );

        $('#'+id).val( onlyDate + " | " + onlyTime );
    }
}

// PROVIDE FOCUS TO INPUT ELEMENT USING AN ID
function addFocusId( id ){ 
    setTimeout(function(){ 
        if( !$('#'+id).is("select") ){
            $('#'+id).focus();
        }else{
            $('#'+id).data('selectpicker').$button.focus();
            $('#'+id).selectpicker('refresh');
        }
    },500);
}

// RETURNS CURRENT TIME
function currTime(){
    d = new Date();
    return d.toLocaleTimeString();  // -> "7:38:05 AM"
}

// DISABLE SELECTPICKER USING ID

function enableDisableSelectpicker( id , type ){
    $('#'+id).attr('disabled',type).selectpicker('refresh');
}

// INTL INPUT SET INITIALIZE
function initTelInput( id ){

    var telInput = $("#"+id);
    
//    telInput.intlTelInput({
//        autoFormat: true,
//        // placeholderNumberType: "MOBILE",
//        autoHideDialCode: false,
//        autoPlaceholder: false,
//        // hiddenInput: "full_number",
//        initialCountry: "in",
//        utilsScript: "assets/intl/js/utils.js" 
//    });

     telInput.intlTelInput({
      allowDropdown: true,
      autoHideDialCode: true,
      autoPlaceholder: "off",
      dropdownContainer: "body",
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      hiddenInput: "full_number",
      initialCountry: "in",
      //nationalMode: true,
      onlyCountries: ['in'],
      placeholderNumberType: "MOBILE",
      //preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.13/js/utils.js",
      // utilsScript: "assets/intl/js/utils.js"
    });
}

// GET MOBILE NUMBER FROM INTL
function getIntlMobileNum( id ){
    
    var telInput = $("#"+id);
    return  telInput.intlTelInput("getNumber");
}

// GET MOBILE NUMBER FROM INTL
function checkValidMobile( id ){
    var telInput = $("#"+id);
    if ($.trim(telInput.val())) {
        
        if ( telInput.intlTelInput("isValidNumber") ) { // VALID NUM FOR ALL MOBILE / LANDLINE
        // if ( ( telInput.intlTelInput("isValidNumber") ) && ( (telInput.intlTelInput("getNumberType") == "1") || (telInput.intlTelInput("getNumberType") == "2") ) ) {
            return true;
        } else {
            return false;
        }
    }
}

//SET MOBILE NUM USING INTL
function setNumberInIntl( id , value ){
    $("#"+id).intlTelInput("setNumber", value);
}

//INITIALIZE HTML EDITOR LOCALLY FOR ALL
function initHtmlEditor( id , size , type ){
    // NOT WORKING PLUGINS LIST
    // powerpaste , advcode , tinymcespellchecker , linkchecker , mediaembed
    tinymce.init({
        selector: '#'+id,
        height: size,
        theme: 'modern',
        plugins: 'fullpage save searchreplace autolink directionality visualblocks visualchars fullscreen image media table template charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help emoticons template paste codesample code,noneditable',
        toolbar1: 'insertfile undo redo | insert | styleselect | formatselect | bold italic strikethrough forecolor backcolor | link | sizeselect | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
//        toolbar2: 'print preview media | forecolor backcolor',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        image_advtab: true,
        readonly: type,
        relative_urls : false,
        remove_script_host : false,
     });
}

// ERROR DISPLAY OF API
function displayAPIErrorMsg( apiError , staticError ){

    if( apiError == "" || apiError == "No record found" ){
        toastr.error( staticError );
    }else{
        toastr.error( apiError );
    }
}

//GET CURRNET TIME IN AM PM
function displayCurrentTime() {
    var date = new Date();
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12 ? "PM" : "AM";
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    time = hours + ":" + minutes +" "+ am_pm;
  
    return time;
};

// REMOVE LOCAL STORAGE PROJECT NAME WISE
function removeLocalStorageProjWise( projectName ){

    if( projectName == undefined ){
        return false;
    }
    for (var key in localStorage){
        if( key.search( projectName ) != "-1" ){
            console.log("Removed Local Storage - "+ projectName +" "+ key );
            localStorage.removeItem( key );
        }
    }
}

// GET SELECTED COUNTRY'S DIAL CODE FROM INTL - ADDED BY VISHAKHA TAK 
function getCountryDialCode( id ) {

    var countryData = $('#'+id).intlTelInput('getSelectedCountryData');
    if( countryData != undefined && countryData != "" ){
        return  "+" + countryData.dialCode;
    }
}


//GET TRAVEL TIME USING GOOGLE MAPS
function getTravelTime( originLat , originLng , destinationLat , destinationLng ){  //ORIGIN LAT LONG , DESTINATION LAT LONG

    var origin = new google.maps.LatLng( originLat, originLng ); // using google.maps.LatLng class
    var destination = destinationLat + ', ' + destinationLng; // using string

    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: origin, // LatLng|string
        destination: destination, // LatLng|string
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };

    var routeInfo = "";
    directionsService.route( request, function( response, status ) {

        if ( status === 'OK' ) {
            var point = response.routes[ 0 ].legs[ 0 ];
            // console.log(point)
            routeInfo += 'Start Point :'+ point.start_address +
                        '<br>End Point :'+ point.end_address +
                        '<br>Estimated Travel time is :' + point.duration.text +
                        '<br>Distance is ' + point.distance.text;

            
        }
    });
    return routeInfo;
}

//STATE CITY ON ZIP CODE
function getCityStateOnZipCode( zipCode ){
 
    var lat;
    var lng;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': zipCode  }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            geocoder.geocode({'latLng': results[0].geometry.location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    console.log( getCityState(results) );
                }
            }
        });
        }
    }); 

function getCityState(results)
    {
        var a = results[0].address_components;
        var city, state;
        for(i = 0; i <  a.length; ++i)
        {
           var t = a[i].types;
           if(compIsType(t, 'administrative_area_level_1'))
              state = a[i].long_name; //store the state
           else if(compIsType(t, 'locality'))
              city = a[i].long_name; //store the city
        }
        return (city + ', ' + state)
    }

function compIsType(t, s) { 
       for(z = 0; z < t.length; ++z) 
          if(t[z] == s)
             return true;
       return false;
    }
}

//ADD LOADER
function addLoader(){
    setTimeout( function(){
        $("#loader-wrapper").show();
        $("body").removeClass("loaded");
    },1);
}

//remove LOADER
function removeLoader(){
    $("#loader-wrapper").hide();
    $("body").addClass("loaded");
}

//FIND ADDRESS ON LATLONG
function findAddressOnLatLong( lat , lng ) { 
    var latlng = new google.maps.LatLng(lat, lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0].address_components != "" && results[0].address_components != false) {
                console.log( results[0].address_components );

                var addressLen = results[0].address_components.length;

                $('#pincode').val( results[0].address_components[ addressLen - 1 ].long_name );

                //SET COUNTRY USING TEXT 
                $("#custCreateCountryId option").filter(function() {
                    return this.text == results[0].address_components[ addressLen - 2 ].long_name; 
                }).attr('selected', true).trigger('change'); 

                //SET STATE USING TEXT 
                setTimeout( function(){
                    $("#custCreateStateId option").filter(function() {
                        return this.text == results[0].address_components[ addressLen - 3 ].long_name; 
                    }).attr('selected', true).trigger('change'); 
                    
                },500); 
                
                //SET CITY
                setTimeout( function(){
                    $("#custCreateCityId option").filter(function() {
                        return this.text == results[0].address_components[ addressLen - 4 ].long_name; 
                    }).attr('selected', true);
                    $("#custCreateCityId").selectpicker('refresh'); 
                },750); 


                if( addressLen >= 6 ){
                    $('#bulidingNum').val( results[0].address_components[ 0 ].long_name );
                    $('#areaName').val( results[0].address_components[ 1 ].long_name );
                }else{
                    $('#bulidingNum').val('')
                    $('#societyName').val( ( (results[0].address_components[ addressLen - 5 ] != undefined) ? results[0].address_components[ addressLen - 5 ].long_name : "" ) );
                    $('#areaName').val( ( (results[0].address_components[ addressLen - 5 ] != undefined) ? results[0].address_components[ addressLen - 5 ].long_name : "" ) );
                }
                
            }
        }
    });
}


//DETECT CURRENT LOCATION
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((loc) => {
            //SET LAT LONG IN ADDRESS
           setAddressLatLong(loc.coords.latitude , loc.coords.longitude);
        });
    } else { 
        console.log("Geolocation is not supported by this browser.");
    }
}
 
//SET ADDRESS LAT LONG
function setAddressLatLong( lat , long ){
 
    //PLOT MAP FOR ADD NEW ADDRESS
    addMarker(new google.maps.LatLng( lat , long ) , addressMap);
}

//LAST DAY OF PASSED OF REQUESETED MONTH 
function lastday(y,m){
    return  new Date(y, m +1, 0).getDate();
}

// CHECK GIVEN DATE IS VALID OR NOT - ADDED ON 21-08-2018 - BY VISHAKHA TAK
function isValidDate(d) {

  var d = new Date(d);
  var checkFlag = true;
  if (Object.prototype.toString.call(d) === "[object Date]") {
      // it is a date
      if (isNaN(d.getTime())) {  // d.valueOf() could also work
        checkFlag =  false; // date is not valid
      } else {
        checkFlag =  true;  // date is valid
      }
    } else {
      checkFlag =  false; // not a date
    }
    return checkFlag;
}