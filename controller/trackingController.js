/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-05-2016.
 * File : trackingController.
 * File Type : .js.
 * Project : POTG
 *
 * */

var trackData = [];

function getTrackData(){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'trackingClientListing',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        orgId: checkAuth(5,18),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, getTrackDataCallback,"Please Wait... Getting Dashboard Detail");

    function getTrackDataCallback(flag,data){

        if (data.status == "Success" && flag) {

            console.log(data);

            if(data.data != 'No record found'){

                trackData = data.data ;
                renderTrack();
            
            }

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_TRACKING ); 
            else
                toastr.error(SERVERERROR);
        }
    }
}

function renderTrack(){

   var thead = '<table id="tableListing" class="display datatables-alphabet-sorting">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Full Name</th>'+
                        '<th>User Name</th>'+
                        '<th>Last Login</th>'+
                        // '<th>Notification</th>'+
                        '<th>Tracking</th>'+
                        '<th>Old Tracking</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="itemList">';

    var tfoot = '</tbody></table>';

    var newRow = '';
    var count = 1 ;

    for( var i=0; i<trackData.length; i++ ){

        var notif = trackData[i].ENABLE_NOTIFICATION;
        var uid = trackData[i].PK_USER_ID;
        var devType = trackData[i].DEVICE_TYPE;
        var devId = trackData[i].DEVICE_ID;
        var uname = trackData[i].USERNAME;
        var trDate = trackData[i].LAST_LOGIN;
        var fullName = trackData[i].FULLNAME;

        var trackBtn = '';
        if( notif == 1 ){
            trackBtn = '<td><a class="btn btn-success btn-ripple btn-sm" trDate="'+ trDate +'" enableNotif="'+ notif +'" fullName="'+ fullName +'" userName="'+ uname +'" userId="'+ uid +'" onclick="tracking(this);"><i class="fa fa-location-arrow"></i></a></td>';
        }else{
            trackBtn = '<td><a class="btn btn-primary btn-ripple btn-sm" enableNotif="'+ notif +'" onclick="tracking(this);"><i class="fa fa-location-arrow"></i></a></td>';
        }

        var lastLogin = "";
        if( trackData[i].LAST_LOGIN != "" ){
            var lastLogDate = (trackData[i].LAST_LOGIN).split(" ")[0];
            var lastLogTime = (trackData[i].LAST_LOGIN).split(" ")[1];
            lastLogin = mysqltoDesiredFormat( lastLogDate , "dd-MM-yyyy" ) + " " + lastLogTime;
        }

        count = i + 1;
        newRow += '<tr>'+
                        '<td>'+ count +'</td>'+
                        '<td>'+ trackData[i].FULLNAME +'</td>'+
                        '<td>'+ trackData[i].USERNAME +'</td>'+
                        '<td>'+ lastLogin +'</td>'+
                        // '<td><a href="javascript:void(0)" enableNotif="'+ notif +'" userId="'+ uid +'" device="'+ devType +'" onclick="sendNotif(this)" data-toggle="modal" class="btn btn-primary btn-ripple btn-sm"><i class="fa fa-rss"></i></a></td>'+
                        trackBtn +
                        '<td><a href="javascript:void(0)" deviceId="'+ devId +'" userId="'+ uid +'" userName="'+ uname +'" fullName="'+ fullName +'" onclick="oldTrack(this)" data-toggle="modal" class="btn btn-primary btn-ripple btn-sm"><i class="fa fa-location-arrow"></i></a></td>'+
                    '</tr>';
    }

    // $('#moduleList').html( newRow );
    $('#dtTable').html(thead + newRow + tfoot);

    $("#tableListing").DataTable({ "stateSave": true });
}   

function sendNotification(context){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }

    var devType = $('#devType').val();
    var devId = $('#devId').val();
    var message = $('#message').val().trim();

    var mapUrl='';
    
    if(devType == "android"){
        mapUrl = httpHead + '//117.247.81.154/development/eTRACK/gcm/send_push_notification_message.php';
    }
    else if(devType == "ios"){
        mapUrl = httpHead + '//117.247.81.154/development/eTRACK/gcm/newspush.php'; //IOS
    }

    if ( message.length == 0){
        toastr.warning("Please Enter Message");
        return false;

    }else if (devId == ''){

        toastr.warning("Device Id not found");
        return false;
    }
    else
    {
        $.ajax({
            type: 'get',
            url: mapUrl,
            data: {
                message: message,
                regId: devId,
            },
            success: function (data) {
                toastr.success("Notification send successfully");
                $("#sendNotification").modal('hide');
                resetnotificationdata();
            },
            error: function (e) {
                toastr.error('error');
            }
        });
    }

}

function tracking(context){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }

    var notif = $(context).attr('enablenotif');

    if( notif != 0 ){

        var uId = $(context).attr('userid');
        var uname = $(context).attr('username');
        var fullName = $(context).attr('fullName');
        var trdate = $(context).attr('trdate').split(' ')[0];

        localStorage.POTGeTRACKUname = fullName;
        localStorage.POTGeTRACKDate = trdate;
        localStorage.POTGeTRACKUId = uId;

        navigateToeTrack();

    }else{
        toastr.warning('User is Offline Today');
        return false;
    }
}

function sendNotif(context){

    var notif = $(context).attr('enablenotif');

    if( notif != 0 ){

        var deviceType = $(context).attr('device');

        $('#devType').val( deviceType );

        $('#sendNotification').modal('show');

    }else{
        toastr.warning('User is Offline Today');
        return false;
    }
}

function oldTrack(context){

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    if( tmp_json.data[0].PK_USER_ID == "-1" ){
        toastr.error('Master User is not Authorised for this Activity');
        return false;
    }
    
    var userId = $(context).attr('userid');

    var uname = $(context).attr('username');
    var fullName = $(context).attr('fullName');
    var uId = $(context).attr('userid');
    localStorage.POTGeTRACKUname = fullName;
    localStorage.POTGeTRACKUId = uId;

    var tmp_json = JSON.parse(localStorage.indeCampusUserDetail);
    // Followup Listing Todays, Delayed, Upcomming
    var postData = {
        requestCase: 'getLoginDate',
        clientId: tmp_json.data[0].FK_CLIENT_ID,
        userId: tmp_json.data[0].PK_USER_ID,
        requestUserId: userId,
        orgId: checkAuth(5,18),
        dataValue : generateSecurityCode(tmp_json.data[0].FK_CLIENT_ID, tmp_json.data[0].PK_USER_ID)
    }

    commonAjax(FOLLOWUPURL, postData, oldTrackCallback,"Please Wait... Getting Dashboard Detail");

    function oldTrackCallback(flag,data){

        if (data.status == "Success" && flag) {

            var options = '';

            if( data.data != 'No record found' ){

                for( var i=0; i<data.data.length; i++){

                    var trackDates = mysqltoDesiredFormat( data.data[i],'dd-MM-yyyy' );
                    options += '<option value="'+ data.data[i] +'">'+ trackDates +'</option>'
                }

                $('#trackDate').html(options);
                $('#trackDate').selectpicker();

                $('#trackDate').focus();
            
                $('#oldTracking').modal('show');
                addFocusId( 'trackDate' );   // PROVIDE THE FOCUS TO REQUESTED ELEMENT
            
            }

        } else {
            if (flag)
                displayAPIErrorMsg( data.status , GBL_ERR_NO_TRACKING_DATA );
            else
                toastr.error(SERVERERROR);
        }
    }

}

function navigateToeTrackOld(){

    var trdate = $('#trackDate').val();
    localStorage.POTGeTRACKDate = trdate;

    window.location.href = "eTrack.html";
}

function resetnotificationdata(){

    $('#devType').val('');
    $('#message').val('');
}