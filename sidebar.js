var roleAuth = JSON.parse(localStorage.indeCampusRoleAuth);
var orderFlag = (roleAuth.data.orderFLag == "1" ?  "" : "hide");
var serviceFlag = (roleAuth.data.serviceManegementFlag == "1" ?  "" : "hide");
var expenseFlag = (roleAuth.data.expenseFlag == "1" ?  "" : "hide");
var leadFlag = (roleAuth.data.leadFlag == "1" ?  "" : "hide");


document.write('<div class="nav-logo" id="module1"><a href="javascript:void(0)" onclick="navigateToDashboard()"><div class="logo-div">'+
                        '<img src="assets/admin1/img/logo-web.png" alt="">'+
                '</div><div class="cross"><span class="line"></span><span class="line"></span>'+
                '</div>'+
                '</a></div>'+
                // '<div class="home hide-xs" id="module1">'+
                //     '<a href="javascript:void(0)" onclick="navigateToDashboard()" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard"><i class="ion-home"></i></a>'+
                // '</div>'+
                '<div class="SideLeftMenu">'+
                '<div class="inquiry hide-xs actUserHide accessCls" onclick="navigateToEnrollmentMaster()" id="module11" title="Student Enrollment" style="display:none;">'+
                    '<a href="javascript:void(0)"><span class="hideMenu">Student Enrollment</span><i class="fa fa-graduation-cap text-blue"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" onclick="navigateToEventManagementList()" id="module21" title="Event Management" style="display:none;">'+
                    '<a href="javascript:void(0)"><span class="hideMenu">Events</span><i class="fa fa-calendar text-light-green"></i></a>'+
                '</div>'+
                 '<div class="inquiry hide-xs actUserHide accessCls" onclick="navigateToReqComplaintList();" id="module22" title="Request and Complaints" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Request and Complaints</span><i class="fa fa-commenting-o text-amber"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" onclick="navigateToGeneralInvoice();" id="module23" title="Invoices" style="display:none;">'+
                    '<a href="javascript:void(0)"><span class="hideMenu">Invoices</span><i class="fa fa-file-text-o text-red"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" id="module24" onclick="navigateToChargePostMaster()" title="Charge Posting" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Charge Posting</span><i class="fa fa-rupee text-light-blue"></i></a>'+
                '</div>'+ 
                '<div class="inquiry hide-xs actUserHide accessCls" id="module28" onclick="navigateToCentraliseInvoice()" title="Centralized Invoicing" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Centralized Invoicing</span><i class="fa fa-bank text-light-green"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" id="module18" onclick="navigateToInquiryCalender();" title="Rental Space Management" style="display:none;">'+
                    '<a href="javascript:void(0)"><span class="hideMenu">Rental Space</span><i class="fa fa-building text-amber"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" id="module25" onclick="navigateToSuspensionTermination()" title="Student Suspension and Termination" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Suspension and Termination</span><i class="fa fa-legal text-red"></i></a>'+
                '</div>'+ 
                '<div class="inquiry hide-xs actUserHide accessCls" id="module27" onclick="navigateToNonAvailServices()" title="Non Availing Services" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Non Availing Services</span><i class="fa fa-qrcode text-light-blue"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" id="module26" onclick="navigateToReport()" title="Report" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Report</span><i class="fa fa-pie-chart text-light-green"></i></a>'+
                '</div>'+
                '<div class="inquiry hide-xs actUserHide accessCls" id="module2" onclick="navigateToMasterData()" title="Master Data" style="display:none;">'+
                    '<a href="javascript:void(0);"><span class="hideMenu">Master Data</span><i class="fa fa-database text-amber"></i></a>'+
                '</div>'+ 
                '<div class="inquiry hide-xs actUserHide devPanel" onclick="navigateToMasterDeveloper()" id="module3" title="Developer Masters"  style="display:none;">'+
                   '<a href="javascript:void(0)"><span class="hideMenu"> Developer Masters</span><i class="fa fa-user-secret text-grey"></i></a>'+
                '</div>'+              
                '<div class="navlink_switch hide-xs actUserHide" id="navlink_switch" style=""><a href="javascript:void(0)"><i class="switchClose text-black "></i></a></div>'+
//                '<div class="activity hide-xs actUserHide accessCls" onclick="navigateToactivity()" id="module200" title="Activity" style="display:none;">'+
//                    '<a href="javascript:void(0)"><span class="hideMenu">Activity</span><i class="fa fa-calendar-o text-blue"></i></a>'+
//                '</div>'+
//                '<div class="users hide-xs actUserHide accessCls" onclick="navigateToStudent()" id="module3" title="Student" style="display:none;">'+
//                    '<a href="javascript:void(0)"><span class="hideMenu">Student</span><i class="fa fa-user text-light-green"></i></a>'+
//                '</div>'+                
//                '<div class="inquiry hide-xs actUserHide" onclick="navigateToMasterDeveloper()" id="module44" title="Developer Masters"  style="display:none;">'+
//                    '<a href="javascript:void(0)"><span class="hideMenu"> Developer Masters</span><i class="fa fa-user-secret text-amber"></i></a>'+
//               '</div>'+
//               '<div class="inquiry hide-xs actUserHide" onclick="navigateToClientMaster()" id="module66" title=" Client Master"  style="display:none;">'+
//                    '<a href="javascript:void(0)"><span class="hideMenu"> Client Master</span><i class="fa fa-male text-red"></i></a>'+
//               '</div>'+
//               '<div class="inquiry hide-xs actUserHide" onclick="navigateToUserManagement()" id="module77" title=" User Management"  style="display:none;">'+
//                    '<a href="javascript:void(0)"><span class="hideMenu"> User Management</span><i class="fa fa-child text-light-red"></i></a>'+
//               '</div>'+
                // '<div class="inquiry hide-xs actUserHide accessCls" onclick="navigateToInquiry()" id="module4" title="Inquiry" style="display:none;">'+
                //     '<a href="javascript:void(0)"><span class="hideMenu">Inquiry</span><i class="fa fa-question-circle text-red"></i></a>'+
                // '</div>'+
                // '<div class="inquiry hide-xs '+ orderFlag +' actUserHide accessCls" onclick="navigateToOrder()" id="module47" title="Order" style="display:none;">'+
                //     '<a href="javascript:void(0)"><span class="hideMenu">Order</span><i class="fa fa-check-square-o text-amber"></i></a>'+
                // '</div>'+
                // '<div class="inquiry hide-xs '+ serviceFlag +' actUserHide accessCls" onclick="navigateToService()" id="module49" title="Service" style="display:none;">'+
                //     '<a href="javascript:void(0)"><span class="hideMenu">Service</span><i class="fa fa-wrench text-blue"></i></a>'+
                // '</div>'+
                // '<div class="inquiry hide-xs '+ leadFlag +' actUserHide accessCls" onclick="navigateToLead()" id="module53" title="Lead" style="display:none;">'+
                //     '<a href="javascript:void(0)"><span class="hideMenu">Lead</span><i class="fa fa-check text-green"></i></a>'+
                // '</div>'+
                // '<div class="tracking hide-xs actUserHide accessCls" onclick="navigateToTracking()" id="module5" title="Tracking" style="display:none;">'+
                //     '<a href="javascript:void(0)"><span class="hideMenu">Tracking</span><i class="fa fa-map-marker text-light-red"></i></a>'+
                // '</div>'+
               //  '<div class="target hide-xs actUserHide accessCls" onclick="navigateToTarget()" id="module6" title="Target" style="display:none;">'+
               //      '<a href="javascript:void(0)"><span class="hideMenu">Target</span><i class="fa fa-bullseye text-amber"></i></a>'+
               //  '</div>'+
               //  '<div class="target hide-xs '+ expenseFlag +' actUserHide accessCls" onclick="navigateToExpense()" id="module55" title="Expense" style="display:none;">'+
               //      '<a href="javascript:void(0)"><span class="hideMenu">Expense</span><i class="fa fa-rupee text-blue"></i></a>'+
               //  '</div>'+
               //  '<div class="why hide-xs actUserHide accessCls" onclick="navigateToWhy()" id="module28" title="Why Report" style="display:none;">'+
               //      '<a href="javascript:void(0)"><span class="hideMenu">Why Report</span><i class="fa fa-area-chart text-green"></i></a>'+
               //  '</div>'+ 
               //  '<div class="report hide-xs actUserHide accessCls" onclick="navigateToReport()" id="module31" title="Report" style="display:none;">'+
               //      '<a href="javascript:void(0)"><span class="hideMenu">Report</span><i class="fa fa-pie-chart text-blue"></i></a>'+
               //  '</div>'+
               //  '<div class="navlink_switch hide-xs actUserHide" id="navlink_switch" style="">'+
               //  '<a href="javascript:void(0)"><i class="switchClose text-black "></i></a>'+
               // '</div>'+
                '</div>');

