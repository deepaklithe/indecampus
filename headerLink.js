/*
 *
 * Created By : Nazim khan.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 05-09-2017.
 * File : headerLink.js.
 * File Type : .js.
 * Project : POTG
 *
 * */

var currPage = window.location.href.split('.html')[0].split(httpHead  + "//" + FINALPATH)[1];

var dynamicCssLinks = '<link rel="stylesheet" href="assets/admin1/css/admin1.css?v='+VERSION +'">'+     // COMMON
                        '<link rel="stylesheet" href="assets/admin1/flipClock/css/flipclock.css?v='+VERSION +'">'+     // COMMON
                        '<script src="assets/globals/scripts/jquery.min.js?v='+VERSION +'"></script>'+       // COMMON
                        '<link rel="stylesheet" href="assets/globals/css/elements.css?v='+VERSION +'">'+     // COMMON
                        '<link rel="stylesheet" href="assets/globals/plugins/datatables/media/css/jquery.dataTables.min.css?v='+VERSION +'">'+    // COMMON
                        '<link rel="stylesheet" href="assets/globals/css/plugins.css?v='+VERSION +'">'+     // COMMON
                        '<link rel="stylesheet" href="indeCampusCss/style-responsive.css?v='+VERSION +'">'+     // COMMON
                        '<script src="assets/globals/plugins/modernizr/modernizr.min.js?v='+VERSION +'"></script>'+     // COMMON
                        '<link rel="stylesheet" href="assets/globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.css?v='+VERSION +'">'+     // COMMON
                        '<script src="assets/admin1/tinymce/tinymce.min.js?v='+VERSION +'"></script>'+     // COMMON
                        '<link rel="stylesheet" href="assets/intl/css/intlTelInput.css?v='+VERSION +'"/>'+     // COMMON
                        '<script src="assets/admin1/flipClock/js/flipclock.js?v='+VERSION+'"></script>'+     // COMMON
                        '<link rel="stylesheet" href="assets/admin1/css/bootstrap-select.min.css?v='+VERSION +'">'+     // COMMON
                        '<link rel="stylesheet" href="indeCampusCss/jasny-bootstrap.min.css?v='+VERSION +'">'+
                        '<link rel="stylesheet" href="assets/intl/js/intlTelInput.js?v='+VERSION +'"/>'+     // COMMON
                        '<link rel="stylesheet" href="assets/globals/plugins/bootstrap-tour/css/bootstrap-tour.min.css?v='+VERSION +'">'+
                        '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'; // TOUR CSS


    switch (currPage) {
        case "/inquiryCalender": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="assets/globals/plugins/clockface/css/clockface.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/fullcalendar/dist/fullcalendar.min.css?v='+VERSION +'">';
            break;

        case "/actSummaryReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/pivot.css?v='+VERSION +'">';
            break;

        case "/actTargetDist": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/activity": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="assets/globals/plugins/clockface/css/clockface.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/activityAnalyticsChart": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/globals/plugins/c3js-chart/c3.min.css?v='+VERSION +'">';
            break;

        case "/activityMainReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/activityProjectPivotReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/pivot.css?v='+VERSION +'">';
            break;

        case "/activityProjectReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/activityTarget": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/activityTargetReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/pivot.css?v='+VERSION +'">';
            break;

        case "/activitydetail": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/globals/plugins/clockface/css/clockface.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />';
            break;

        case "/adduser": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/analyticsChart": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/admin1/css/daterangepicker-bs3.css?v='+VERSION +'">';
            break;

        case "/clientConfig": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/clientiteamgroup": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link href="indeCampusCss/treeCss/skin-win8/ui.fancytree.css?v='+VERSION +'" rel="stylesheet">';
            break;

        case "/clientiteammas": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" href="indeCampusCss/treeCss/jquery.treetable.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="indeCampusCss/treeCss/jquery.treetable.theme.default.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />';
            break;

        case "/createRoom": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" href="indeCampusCss/treeCss/jquery.treetable.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="indeCampusCss/treeCss/jquery.treetable.theme.default.css?v='+VERSION +'" />';
            break;

        case "/clientmaster": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/clientproposal": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/clientstatuscat": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/globals/plugins/switchery/dist/switchery.min.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css?v='+VERSION +'">';
            break;

        case "/storeMigration": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/globals/plugins/switchery/dist/switchery.min.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css?v='+VERSION +'">';
            break;

        case "/clienttarget": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/clientteam": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;


        case "/student": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/c3js-chart/c3.min.css?v='+VERSION +'">';
            break;

        case "/customerDetail": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/customergroup": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/dashboardMasterCompo": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/devloperbranch": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/propertyMaster": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/devloperclient": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/devloperclientList": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/devloperlookup": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
        
        case "/devlopertype": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
        
        case "/documentManagement": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;
        
        case "/draggableInfo": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
        
        case "/dynamicProposalTemplate": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/dynamicReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/jquery-ui.css?v='+VERSION +'"/>';
            break;

        case "/dynamicReportList": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/dynamicTemplateCreate": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/eTrack": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/gMap.css?v='+VERSION +'"/>';
            break; 

        case "/tracking": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break; 

        case "/edituser": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/emailConfig": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/expenseReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;
            
        case "/fileUploadClient": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/greeting": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/home": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/globals/plugins/c3js-chart/c3.min.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/fullcalendar/dist/fullcalendar.min.css?v='+VERSION +'">';
            break;

        case "/index": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
       
        case "/inqReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;
       
        case "/inqWonReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/inquiry": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/admin1/css/custom.css?v='+VERSION +'">';
            break;

        case "/inquiryAllocationReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/inquiryCreate": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />';
            break;

        case "/loginHistory": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/masterdevloper": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
            
        case "/paymentReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;


        case "/profile": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/proposalReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;
            
        case "/proposaledit": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/quickInquiry": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />';
            break;

        case "/report": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/realEstateProject": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/inquiryReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/inquirydetail": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/c3js-chart/c3.min.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="indeCampusCss/aeroplane.css?v='+VERSION +'">';
            break;

        case "/salesTargetReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/pivot.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/salesbom": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" href="assets/globals/plugins/switchery/dist/switchery.min.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/globals/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css?v='+VERSION +'">';
            break;

        case "/scheduleActivity": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/smartActivity": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />'+
                                '<link rel="stylesheet" href="assets/globals/plugins/select2/select2.min.css?v='+VERSION +'">';
            break;

        case "/serviceReport": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/settings": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/globals/plugins/bootstrap-tour/css/bootstrap-tour.min.css?v='+VERSION +'">'; // TOUR CSS
            break;

        case "/smartSchedulerActivity": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/smsConfig": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/studentEnrollment": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks +=  '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/studentEnrollmentDetail": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/admin1/css/daterangepicker-bs3.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

         case "/statusCategory": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/target": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/targetDist": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/uomManagement": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/usergroup": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/userlevel": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/usermanagment": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/usermanagpermission": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/usermanagrole": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/admin1/css/bootstrap-select.min.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" type="text/css" href="indeCampusCss/multi-select.css?v='+VERSION +'"/>';
            break;

        case "/usermanaguser": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/usermanagmodule": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/versionInfo": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/why": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/pivot.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="assets/admin1/css/daterangepicker-bs3.css?v='+VERSION +'">';
            break;

        case "/workflow": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/createEnrollment": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/studentAllotment": // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/createEnrollmentInvoice" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;
    
        case "/renewEnrollment" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/itemMasterList" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/itemMasterListDetail" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;
    
        case "/chargePosting" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

    
        case "/chargePostListing" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
                                
            break;
    
        case "/eventManagementDetail" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                // '<link rel="stylesheet" href="assets/globals/plugins/clockface/css/clockface.css?v='+VERSION +'">';
                                '<link rel="stylesheet" type="text/css" href="assets/globals/plugins/bootstrap-datetimepicker/css/datetimepicker.css?v='+VERSION +'" />';
            break;

        case "/eventManagementList" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>';
            break;

        case "/collectpayment" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">'+
                                '<script src="assets/admin1/js/jasny-bootstrap.min.js?v='+VERSION +'"></script>'+
                                '<link rel="stylesheet" href="assets/globals/plugins/clockface/css/clockface.css?v='+VERSION +'">';
            break;

        case "/roommaster" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break; 
            
        case "/NonProcessCharge" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/centralisedinvoice" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
            
        case "/eventManagementView" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;
            
        case "/RequestComplainCreate" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/RequestComplain" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/RequestComplainView" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/generalInvoice" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '';
            break;

        case "/suspensionTermination" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;
            
        case "/suspensionTerminationCreate" : // IT WILL APPEND ALL THE CSS DYNAMICALLY
            dynamicCssLinks += '<link rel="stylesheet" href="assets/admin1/css/daterangepicker-bs3.css?v='+VERSION +'">'+
                                '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/rentalSpaceInquiryReport": // ADDED BY KAVITA PATEL ON 08-08-2018
                dynamicCssLinks +=  '<link rel="stylesheet" href="indeCampusCss/datepicker.css?v='+VERSION +'">';
            break;

        case "/nonAvailingService":
                dynamicCssLinks +=  '';
            break;
            
        case "/addNonAvailService":
                dynamicCssLinks +=  '';
            break;

        case "/createreceipt" : // ADDED BY KAUSHA SHAH ON 16-08-2018
            dynamicCssLinks += '' ;
            break; 

        case "/nonAvailingView" : 
            dynamicCssLinks += '' ;
            break;

        case "/timezoneMaster" : 
            dynamicCssLinks += '' ;
            break;
            
        case "/roomOccupancyReport" : 
            dynamicCssLinks += '' ;
            break;
            
        case "/requestComplaintSummary" : 
            dynamicCssLinks += '<link rel="stylesheet" href="indeCampusCss/pivot.css?v='+VERSION +'">';
            break;
    }   

document.write( dynamicCssLinks ); // ASSIGN ALL THE LINKS TO ONE PAGE
    
