document.write('<div class="layer-container">'+
        '<div class="menu-layer">'+
            '<ul>'+
                '<li data-open-after="true" class="module1 access">'+
                    '<a href="javascript:void(0)" onclick="navigateToDashboard()"><i class="ion-home"></i> Dashboard</a>'+
                '</li>'+
                '<li class="module11 access">'+
                    '<a href="javascript:void(0)" onclick="navigateToEnrollmentMaster()"><i class="fa fa-graduation-cap"></i>  Student Enrollment</a>'+
                '</li>'+
                '<li class="module21 access">'+
                    '<a href="javascript:void(0)" onclick="navigateToEventManagementList()"><i class="fa fa-calendar"></i>  Events</a>'+
                '</li>'+
                '<li class="module22 access">'+
                    '<a href="javascript:void(0)" onclick="navigateToReqComplaintList();" ><i class="fa fa-commenting-o"></i>  Request and Complaints</a>'+
                '</li>'+
                '<li class="module23 access">'+
                    '<a href="javascript:void(0)" onclick="navigateToGeneralInvoice();"><i class="fa fa-file-text-o"></i> Invoices</a>'+
                '</li>'+
                '<li class="module24 access"><a href="javascript:void(0)" onclick="navigateToChargePostMaster()"><span class="fa fa fa-rupee"></span> Charge Posting</a></li>'+
                '<li class="module28 access"><a href="javascript:void(0)" onclick="navigateToCentraliseInvoice()"><span class="fa fa-bank"></span> Centralized Invoicing</a></li>'+
               '<li class="module18 access"><a href="javascript:void(0)" onclick="navigateToInquiryCalender();"><span class="fa fa-building"></span> Rental Space</a></li>'+
               '<li class="module25 access"><a href="javascript:void(0)" onclick="navigateToSuspensionTermination()" class=""><span class="fa fa-legal"></span> Suspension and Termination</a></li>'+
               '<li class="module27 access"><a href="javascript:void(0)" onclick="navigateToNonAvailServices()" class=""><span class="fa fa-qrcode"></span> Non Availing Services</a></li>'+
               '<li class="module26 access"><a href="javascript:void(0)" onclick="navigateToReport()" class=""><span class="fa fa-pie-chart"></span> Report</a></li>'+
               '<li class="module2 access"><a href="javascript:void(0)" onclick="navigateToMasterData()" class=""><span class="fa fa-database"></span> Master Data</a></li>'+
               '<li class="devPanel"><a href="javascript:void(0);" onclick="navigateToMasterDeveloper()" class=""><span class="fa fa-user-secret"></span> Developer Masters</a></li>'+
                '<li>'+
                    '<a href="javascript:;"><i class="fa fa-cogs"></i> Settings</a>'+
                    '<ul class="child-menu">'+                        
                        '<li class="changeBranch"><a href="javascript:void(0)" data-div="changeBranchPopUpRender();" onClick="openChangeBranchPopUp(this)"><span class="fa fa-exchange"></span> Change Branch</a></li>'+  
                       '<li class=""><a href="javascript:void(0)" onclick="loadRoleAndAuth(1);" class=""><span class="fa fa-refresh"></span> Refresh </a></li>'+
                       '<li class="module1"><a href="javascript:void(0)" onclick="navigateToDashboardPreference();" class=""><span class="fa fa-gears"></span> Dashboard Preferences </a></li>'+
                        
                        '<li><a href="javascript:void(0)" class="" onclick="navigateToProfile()"><span class="fa fa-user"></span> Profile</a></li>'+
                    '</ul>'+
                '</li>'+
                '<li>'+
                    '<a href="javascript:void(0)" onclick="navigateToIndex();"><span class="fa fa-sign-out"></span> Log Out</a>'+
                '</li>'+
            '</ul>'+
        '</div>'+
    '</div>');
