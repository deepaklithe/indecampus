<?php
define('INDECAMPUSFOLDEARNAME','testing/indecampus') ;
require_once ($_SERVER['DOCUMENT_ROOT'].'/'.INDECAMPUSFOLDEARNAME.'/indeCampusProject/include/globals.php');
?>
<html>
<head>
	<title>Forget Password Link</title>
</head>
<body>
    <?php
	if(isset($_GET) && !empty($_GET['data'])) {
	    
	    $PK_STUD_ID = decstr($_GET['data']) ;
	    
	    
	    $updateArr = array() ;
	    
	    $updateArr['IS_ACTIVE'] = 1;
	    $updateArr['CHANGED_BY_DATE'] = time() ;
	    
	    $updateUserMaster = update_rec(STUDENTMASTER, $updateArr, 'PK_STUD_ID = "'.$PK_STUD_ID.'"');
    ?>
    <span>Your account has been activated. Please login using new password.</span>
    <?php
	}
    ?>

</body>
</html>
