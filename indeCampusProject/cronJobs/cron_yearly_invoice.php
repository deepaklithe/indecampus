<?php

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

set_time_limit(0);
require_once '../include/globals.php';
require_once('../include/function_DL.php');
require_once '../include/sendgrid-php.php';

/*$orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
$clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
$userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
$chargeStartDate = !empty($postData['postData']['chargeStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeStartDate'])))) : "";
$chargeEndDate = !empty($postData['postData']['chargeEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeEndDate'])))) : "";
$invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
$studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";*/

$orgId =1;
$clientId=1;

$chargeStartDate='2018-05-01';
$chargeEndDate='2019-05-01';
$invoiceName='yearly';
$studentId='';
$userId=1;


if (!empty($chargeStartDate) && !empty($chargeEndDate) && !empty($invoiceName)) {

    if (strtotime($chargeStartDate) <= strtotime($chargeEndDate)) {
        $whereStudentCond = "";
        if (!empty($studentId)) {
            $whereStudentCond = " AND STUDENT_ID='" . $studentId . "'";
        }
     $sqlChargeStudentData = "SELECT STUDENT_FULL_NAME,STUDENT_ID FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND IS_TRANSFER ='0' AND INVOICE_FLAG ='0' AND CHARGE_DATE BETWEEN '" . $chargeStartDate . "' AND '" . $chargeEndDate . "' " . $whereStudentCond . " GROUP BY STUDENT_ID"; // QUERY    FOR CHARGE MASTER DATA WITHOUT INVOICE
    
      
        $resChargeStudentData = fetch_Rec_query($sqlChargeStudentData);
         
        if (count($resChargeStudentData) > 0) {

            foreach ($resChargeStudentData as $keyStudent => $valueStudent) {// STUDENT DAT FOR EACH LOOP.
                if (!empty($valueStudent['STUDENT_ID'])) {
                    $whereStudentCond = " AND STUDENT_ID='" . $valueStudent['STUDENT_ID'] . "'";
                }

// CHARGE_DATE
               $sqlChargeMasterDataWithOutInvoice = "SELECT FK_ITEM_ID,STUDENT_ID,GROUP_CONCAT(PK_CHARGE_ID SEPARATOR ',')AS CHARGE_IDS,ITEM_BASE_PRICE,SUM(QTY_OF_SESSION)AS TOTAL_QTY,SUM(TOTAL_TAX)AS TOTAL_TAX,SUM(ITEM_TOTAL_PRICE)AS ITEM_TOTAL_PRICE,ITEM_NAME,TAX_CGST,TAX_SGST FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND IS_TRANSFER ='0' AND INVOICE_FLAG ='0' AND CHARGE_DATE BETWEEN '" . $chargeStartDate . "' AND '" . $chargeEndDate . "' " . $whereStudentCond . " GROUP BY FK_ITEM_ID"; // QUERY FOR CHARGE MASTER DATA WITHOUT INVOICE 
              
                $resChargeMasterDataWithOutInvoice = fetch_rec_query($sqlChargeMasterDataWithOutInvoice); // RESULT FOR CHARGE MASTER DATA WITHOUT INVOICE
              // echo "<pre>"; print_r($resChargeMasterDataWithOutInvoice); exit;
                
                $chargeMasterArr = $tempArr = Array();
               if (count($resChargeMasterDataWithOutInvoice) > 0) {
                    foreach ($resChargeMasterDataWithOutInvoice as $keyChargeMaster => $valueChargeMaster) { // CHARGE MASTER DATA WITHOUT INVOICE DATA FOREACH LOOP.
                        $chargeMasterArr[$keyChargeMaster]['studentId'] = $valueChargeMaster['STUDENT_ID'];
                        $chargeMasterArr[$keyChargeMaster]['itemName'] = $valueChargeMaster['ITEM_NAME'];
                        $chargeMasterArr[$keyChargeMaster]['itemId'] = $valueChargeMaster['FK_ITEM_ID'];
                        $chargeMasterArr[$keyChargeMaster]['chargeIds'] = $valueChargeMaster['CHARGE_IDS'];
                        $chargeMasterArr[$keyChargeMaster]['totalQty'] = $valueChargeMaster['TOTAL_QTY'];
                        $chargeMasterArr[$keyChargeMaster]['itemBasicPrice'] = $valueChargeMaster['ITEM_BASE_PRICE'];
                        $chargeMasterArr[$keyChargeMaster]['totalTax'] = $valueChargeMaster['TOTAL_TAX'];
                        $chargeMasterArr[$keyChargeMaster]['totalItemPrice'] = $valueChargeMaster['ITEM_TOTAL_PRICE'];
                        $chargeMasterArr[$keyChargeMaster]['cgst'] = $valueChargeMaster['TAX_CGST'];
                        $chargeMasterArr[$keyChargeMaster]['sgst'] = $valueChargeMaster['TAX_SGST'];
                    }
                }

                $chargeMasterData = array_values($chargeMasterArr); // GET ALL CHARGE MASTER DATA.
              // echo "<pre>"; print_r($chargeMasterData); exit;
                if (!empty($chargeMasterData)) {

                    $chargeMasterArray = $chargeMasterData;
                    $insertInvoiceMaster = Array();
                    $totalPrice = 0;
                    $insertInvoiceMaster['FK_CLIENT_ID'] = $clientId;
                    $insertInvoiceMaster['FK_STUDENT_ID'] = $valueStudent['STUDENT_ID'];
                    $insertInvoiceMaster['INVOICE_DATE'] = date("Y-m-d");
                    $insertInvoiceMaster['INVOICE_NAME'] = $invoiceName;
                    $insertInvoiceMaster['TOTAL_CHARGES'] = '';
                    $insertInvoiceMaster['PERIOD_FROM'] = $chargeStartDate;
                    $insertInvoiceMaster['PERIOD_TO'] = $chargeEndDate;
                    $insertInvoiceMaster['CREATED_BY'] = $userId;
                    $insertInvoiceMaster['CREATED_BY_DATE'] = time();
                    $insertInvoiceMasterData = insert_rec(INVOICEMASTER, $insertInvoiceMaster);
                  
                    if (isset($insertInvoiceMasterData['lastInsertedId']) && $insertInvoiceMasterData['lastInsertedId'] > 0) {
                        $invoiceNo = $insertInvoiceMasterData['lastInsertedId'];
                        foreach ($chargeMasterArray as $keyCharge => $valueCharge) { // CHARGE MASTER TRANS ARRAY LOOP.
                          
                            if ($valueCharge['totalQty'] > 0 && $valueCharge['itemName'] != "") {
// INVOICE TRANS ARRAY.
                                $insertInvoiceTrans = array();
                                $insertInvoiceTrans['FK_INVOICE_ID'] = $insertInvoiceMasterData['lastInsertedId'];
                                $insertInvoiceTrans['CHARGE_TYPE'] = 'STUDENT';
                                $insertInvoiceTrans['TOTAL_STUDENT'] = '1';
                                $insertInvoiceTrans['FK_ITEM_ID'] = $valueCharge['itemId'];
                                $insertInvoiceTrans['REF_ITEM_ID'] = '';
                                $insertInvoiceTrans['ITEM_NAME'] = $valueCharge['itemName'];
                                $insertInvoiceTrans['SGST'] = $valueCharge['sgst'];
                                $insertInvoiceTrans['CGST'] = $valueCharge['cgst'];
                                $insertInvoiceTrans['TOTAL_CHARGES'] = ($valueCharge['itemBasicPrice'] * $valueCharge['totalQty']);
                                $insertInvoiceTrans['TOTAL_TAX'] = $valueCharge['totalTax'];
                                $insertInvoiceTrans['PAYABLE_AMOUNT'] = $valueCharge['totalItemPrice'];
                                $insertInvoiceTrans['CREATED_BY'] = $userId;
                                $insertInvoiceTrans['CREATED_BY_DATE'] = time();
                                $insertInvoiceTransData = insert_rec(INVOICETRANSMASTER, $insertInvoiceTrans);

                                $totalPrice = $totalPrice + $valueCharge['totalItemPrice'];

                                $chargeIdsArr = ($valueCharge['chargeIds'] != "") ? explode(",", $valueCharge['chargeIds']) : Array();
                                if (count($chargeIdsArr) > 0) { // UPDATE  INVOICE_FLAG =1 AND INVOICE ID IN CHARGE MASTER DATA 
                                    foreach ($chargeIdsArr as $keyCharge => $valueChargeIds) { // CHARGE IDS ARRAY LOOP.    
// UPDATE CHARGE ARRAY.
                                        $updateChargeArr = Array();
                                        $updateChargeArr['INVOICE_FLAG'] = 1;
                                        $updateChargeArr['INVOICE_ID'] = $invoiceNo;

                                        $whereChargeCond = "PK_CHARGE_ID='" . $valueChargeIds . "'"; // WHERE CHARGE ID COND.
                                        $updateInvoiceMaster = update_rec(CHARGEMASTER, $updateChargeArr, $whereChargeCond); // UPDATE IN CHARGE_MASTER TABLE.
                                    }
                                }
                            }
                        }
                        $updateInvoiceArr = Array();
                        $updateInvoiceArr['TOTAL_CHARGES'] = $totalPrice;
                        $whereInvoiceCond = "PK_INVOICE_ID='" . $invoiceNo . "'";
                        $updateInvoiceMaster = update_rec(INVOICEMASTER, $updateInvoiceArr, $whereInvoiceCond); // UPDATE INVOICE TOTAL IN INVOICE MASTER TABLE.
                      $sqlStudentData = "SELECT SM.PK_STUD_ID,SM.EMAIL,SM.ADDRESS,SM.PINCODE,CO.COUNTRY_NAME,CT.STATE_NAME,CT.CITY_NAME,SM.MEMBERSHIP_NUMBER FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  WHERE SM.DELETE_FLAG ='0' AND SM.PK_STUD_ID='" . $valueStudent['STUDENT_ID'] . "'";// QUERY FOR STUDENT DATA
                        $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA
                        
                       
                        $sqlInvoiceTrans = "SELECT ITEM_NAME,TOTAL_CHARGES,TOTAL_TAX,PAYABLE_AMOUNT,SGST,CGST FROM " . INVOICETRANSMASTER . " WHERE FK_INVOICE_ID='" . $invoiceNo . "' AND DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA

                        $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA

                        $sqlPaymentInvoice = "SELECT SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $invoiceNo . "' AND FK_STUDENT_ID ='" . $valueStudent['STUDENT_ID'] . "'"; //QUERY FOR INVOICE PAYMENT DATA 
                        $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);
                        $totalPayment = 0;
                        if (count($resPaymentInvoice) > 0) {
                            $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
                        }
                        
                        if (!empty($resStudentData[0]['EMAIL'])) {

                            //echo "total payment1234:::".$totalPayment; exit;
                            
                            /* Mail sent code start */
                           $sendgrid_username = "arunnagpal";
                            $sendgrid_password = "devil1502";

                            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


                            $totalPrevBalance = "";
                            $totalPrevBalanceType = "";
                            $htmlInvioiceTemplateHeader = '<!DOCTYPE html>' .
                                    '<html lang="en">' .
                                    '<head>' .
                                    '<meta charset="utf-8">' .
                                    '<meta name="viewport" content="width=device-width, initial-scale=1.0">' .
                                    '<meta name="description" content="">' .
                                    '<meta name="author" content="Mosaddek">' .
                                    '<link rel="shortcut icon" href="img/favicon.ico">' .
                                    '<title></title>' .
                                    '</head>' .
                                    '<body class="invoice-page" data-gr-c-s-loaded="true"> <div style="background: #ffffff none repeat scroll 0 0; color: #333333; font-family: DejaVu Sans; sans-serif;font-size: 9pt;"><div class="pcs-template-header pcs-header-content" id="header" style="height: 0.1in; padding: 0 0.4in 0 0.55in; background-color: #ffffff; color: #333333; font-size: 9pt;"></div><div class="pcs-template-body" style="padding: 0 0.4in 0 0.55in;">';

                            $htmlTableLogo = '<table style="width:100%;table-layout: fixed;">' .
                                    '<tbody>' .
                                    '<tr>' .
                                    '<td colspan = 2 style="vertical-align: top; width:100%; text-align: center;">' .
                                    '<span class="pcs-entity-title" style="color: #000000; font-size: 28pt;">Tax Invoice</span><br>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="vertical-align: top; width:50%;">' .
                                    '<span class="pcs-entity-title" style="color: #000000; font-size: 28pt;">Bill No</span><br>' .
                                    '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b># ' . $invoiceNo . '</b></span><br>' .
                                    '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice Date : ' . date("d-m-Y") . '</b></span><br>' .
                                    // '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Period From : ' . $getInvoiceDetail['data'][0]['invoicePeroidFrom'] . '</b></span>' .
                                    '</td>' .
                                    '<td style="vertical-align: top; text-align:right;width:50%;">' .
                                    '<div>' .
                                    // '<img src="' . APP_SERVER_ROOT_FRONTEND . '/img/gulmoharInvLogo.png" style="width:160.00px;height:129.00px;" id="logo_content">' .
                                    '</div>' .
                                    '</td>' .
                                    '</tr>' .
                                    '</tbody>' .
                                    '</table>';

                            $htmlMemberDetail = '<table style="clear:both;width:100%;margin-top:30px;table-layout:fixed;">' .
                                    '<tbody><tr><td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                                    '</td>' .
                                    '<td style="vertical-align:bottom;width: 40%;" align="right"></td>' .
                                    '</tr><tr>' .
                                    '<td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                                    '<div><label style="font-size: 14pt; color:#817d7d" class="pcs-label" id="tmp_billing_address_label">To,</label><br>' .
                                    '<span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 12pt;">' . $valueStudent['STUDENT_FULL_NAME'] . '</span><br>' .
                                    '<span style="white-space: pre-wrap; font-size: 9pt;">' . wordwrap($resStudentData[0]['ADDRESS'], 70) . '</span><br>' .
                                    '<span style="white-space: pre-wrap; font-size: 9pt;">' . strtoupper(strtolower($resStudentData[0]['CITY_NAME'])) . ' ' . strtoupper(strtolower($resStudentData[0]['STATE_NAME'])) . ' ' . strtoupper(strtolower($resStudentData[0]['COUNTRY_NAME'])) . ' - ' . $resStudentData[0]['PINCODE'] . '</span><br>' .
                                    '</div>' .
                                    '</td>' .
                                    '<td style="vertical-align:bottom;width: 40%;" align="right">' .
                                    '</td>' .
                                    '</tr>' .
                                    '</tbody></table>';

                            //  $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;">' . $gstinNo . '<br><b>Membership No</b> : ' . $getInvoiceDetail['data'][0]['memId'] . '</span>' . '<table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
                            $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;"><br><b>Membership No</b> : </span>' . $resStudentData[0]['MEMBERSHIP_NUMBER'] . '<table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
                                    '<thead>' .
                                    '<tr style="height:32px;">' .
                                    '<td style="padding:5px 0 5px 5px;text-align: center;word-wrap: break-word;width: 5%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header">#</td>' .
                                    '<td style="padding:5px 10px 5px 20px;word-wrap: break-word; background-color: #3c3d3a;color: #ffffff; font-size: 9pt; " class="pcs-itemtable-header pcs-itemtable-description">Particular</td>' .
                                    //'<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 11%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>'.
                                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">CGST</td>' .
                                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">SGST</td>' .
                                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width:22%; background-color: #3c3d3a; color: #ffffff;font-size: 9pt;background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>' .
                                    '</tr>' .
                                    '</thead>' .
                                    '<tbody class="itemBody">';
                            $invoiceItemTransMid = "";
                            if (!empty($resInvoiceTrans)) {
                                $totalSgstAmt = $totalCgstAmt = $totalCharges = $totalPayable = 0;
                                foreach ($resInvoiceTrans as $keyTrans => $valueTrans) {

                                    if (!empty($valueTrans['PAYABLE_AMOUNT'])) {
                                        $sgstAmt = $cgstAmt = 0;
                                        $sgstAmt = ($valueTrans['SGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['SGST'] / 100 : 0;
                                        $cgstAmt = ($valueTrans['CGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['CGST'] / 100 : 0;
                                        $totalSgstAmt = $totalSgstAmt + $sgstAmt;
                                        $totalCgstAmt = $totalCgstAmt + $cgstAmt;

                                        $invoiceItemTransMid .= '<tr>' .
                                                '<td style="padding: 10px 0 10px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">' . ($keyTrans + 1) . '</td>' .
                                                '<td style="padding: 10px 0px 10px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" ><div><div><span style="word-wrap: break-word;" id="tmp_item_name">' . $valueTrans['ITEM_NAME'] . '</span><br><span class="pcs-item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">' . (!empty($valueTrans['itemCode']) ? 'SAC: ' . $valueTrans['itemCode'] : '') . '</span></div></div></td>' .
                                                //'<td <td style="padding: 10px 10px 5px 10px;text-align:right;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><span id="tmp_item_qty"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> '.number_format($valueTrans['totalCharges'],2).'</span></td>'.
                                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($sgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['SGST'] . '% </div></td>' .
                                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($cgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['CGST'] . '% </div></td>' .
                                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($valueTrans['TOTAL_CHARGES'], 2) . '</span></td>' .
                                                '</tr>';

                                        $totalCharges = $totalCharges + ($valueTrans['TOTAL_CHARGES']);
                                        $totalPayable = $totalPayable + $valueTrans['PAYABLE_AMOUNT'];
                                    }
                                }
                            }

                            /* $balanceCharge = 0;
                              $balanceChargeType = "";
                              if (strtolower($totalPrevBalanceType) != "cr") { // DEBIT
                              $balanceCharge = $totalCharges + $totalPrevBalance;
                              $balanceChargeType = $totalPrevBalanceType;
                              } else { // CREDIT
                              $balanceCharge = $totalCharges - $totalPrevBalance;
                              $balanceChargeType = ((($totalCharges - $totalPrevBalance) < 0) ? 'CR' : "DR");
                              } */
                            $invoiceItemTransFooter = '</tbody></table>';

                            $htmlInvoiceTax = '<div style="width: 100%;margin-top: 1px; float:right;">' .
                                    //'<div style="width: 45%;padding: 10px 10px 3px 3px;font-size: 9pt;float: left;">'.
                                    //'<div style="white-space: pre-wrap;"><p>&nbsp;</p></div>'.
                                    //'</div>'.
                                    '<div style="width: 100%;">' .
                                    '<table width="100%"><tr><td style="width:50%; float:left;"></td><td style="text-align:right;width:45%; float:right;">' .
                                    '<table class="pcs-totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">' .
                                    '<tbody>' .
                                    '<tr>' .
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>' .
                                    //  '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                                    '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                                    '</tr>' .
                                    '<tr style="height:10px;">' .
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>' .
                                    //  '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .
                                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalSgstAmt, 2) . '</td>' .
                                    '</tr>' .
                                    '<tr style="height:10px;">' .
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>' .
                                    //   '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .

                                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCgstAmt, 2) . '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>' .
                                    // '<td id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . (($totalCharges + $totalTax < 0) ? "CR" : "DR") . '</b></td>' .
                                    '<td id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . (($totalPayable - $totalPayment < 0) ? "CR" : "DR") . '</b></td>' .
                                    //  '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format((($totalCharges + $totalTax < 0) ? (($totalCharges + (($totalTax < 0) ? $totalTax * -1 : $totalTax) * -1) * -1) : $totalCharges + (($totalTax < 0) ? $totalTax * -1 : $totalTax)), 2) . '</b></td>' .
                                    '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayable, 2) . '</b></td>' .
                                    '</tr>' .
                                    /*  '<tr>' .
                                      '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Prev Balance</b></td>' .
                                      //'<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . $totalPrevBalanceType . '</b></td>' .
                                      '<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>1000</b></td>' .
                                      '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> 1000</b></td>' .
                                      '</tr>' .
                                      /* '<tr>' .
                                      '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>' .
                                      '<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>Collection Type</b></td>' .
                                      '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> collection</b></td>' .
                                      '</tr>' .
                                      /* '<tr style="height:40px; background-color: #f5f4f3; color: #000000; font-size: 9pt; font-size: 8pt; background-color: #f5f4f3;color: #000000; font-size: 9pt;" class="pcs-balance">' .
                                      '<td style="padding:5px 10px 5px 0;" valign="middle" align="right"><b>Balance Due</b></td>' .
                                      '<td id="tmp_balance_due" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>typeofBalance</b></td>' .
                                      '<td id="tmp_balance_due" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(1000, 2) . '</b></td>' .
                                      '</tr>' . */
                                    '</tbody>' .
                                    '</table>' .
                                    '</td></tr></table>' .
                                    '</div>' .
                                    '<div style="clear: both;"></div>' .
                                    '</div>';

                            //$balanceChargeType
                            //number_format((($balanceCharge+$totalTax < 0) ? ($balanceCharge+$totalTax)*-1 : $balanceCharge+$totalTax),2)
                            $htmlInvoiceTempTM = '<div style="margin-top:10px;"><b><u>Terms and Conditions</u></b>' .
                                    '<table style="width:100%;font-size: 10px;">' .
                                    '<tbody><tr>' .
                                    '<td style="padding-top:60px;padding-bottom: 8px;">' .
                                    '<span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 2. CORPORATE ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 3. CLUB ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 6. PLEASE ALWAYS MENTION YOUR MEMBERSHIP NUMBER &amp; CONTACT NUMBER BEHIND THE CHEQUE.</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 7. ANY ERROR IN BILL SHOULD BE INFORMED TO THE CLUB IMMEDIATELY.</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 8. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName">  </span></b>".</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '<tr>' .
                                    '<td style="padding-bottom: 8px;">' .
                                    '<span> 9. SUBJECT TO AHMEDABAD JURISDICTION.</span>' .
                                    '</td>' .
                                    '</tr>' .
                                    '</tbody>' .
                                    '</table>' .
                                    '</div><div style="font-size: 14px;"><b>*Please note this is a computer generated invoice and hence dose not need signature.</b></div>';
                            $htmlInvioceTemplateFooter = '</div> <div class="pcs-template-footer" style="background-color: #ffffff;  color: #aaaaaa; font-size: 6pt; height: 0.7in; padding: 0 0.4in 0 0.55in;"></div></div></body></html>';

                            // FINAL HTML
                            $finalHtml = $htmlInvioiceTemplateHeader . $htmlTableLogo . $htmlMemberDetail . ($invoiceItemTransHeader . $invoiceItemTransMid . $invoiceItemTransFooter) . $htmlInvoiceTax . $htmlInvoiceTempTM . $htmlInvioceTemplateFooter;
                            //print_r($finalHtml); exit;
                            //$dompdf_temp = new Dompdf();
                            global $dompdf;
                            $dompdf->load_html($finalHtml);
                            //$dompdf_temp->set_paper("a4", "portrait");
                            $dompdf->set_paper(array(0, 0, 794, 1122), "portrait");
                            $dompdf->render();
                            $pdfOutput = $dompdf->output();

                            //echo $pdfOutput;die;$n
                            $number ='11';
                            
                            $fileLocation = APP_SERVER_ROOT . "/attachment/" . $resStudentData[0]['MEMBERSHIP_NUMBER'] . "-" . time() . ".pdf";
                            $fp = fopen($fileLocation, "a+");
                            fwrite($fp, $pdfOutput);
                            fclose($fp);

                            $clientDetail = getClientDetail($clientId);

                            // TEMPLATE FOR HTML

                            $messageBodyHeader = "<html lang='en'><head>" .
                                    "<style>body {margin: 0;padding: 0;background: #F7F7F7;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 16px;color: rgba(0, 0, 0, 0.87);}" .
                                    ".container {width: 768px;margin: 0 auto}" .
                                    "header {background: #6dbb4a;color: #fff;height: 60px;width: 100%;top: 0;}" .
                                    "ul,li {list-style: none;margin: 0;padding: 0;}" .
                                    "header:after {clear: both;}" .
                                    ".logo {background: #fff url(http://gulmohargreens.com/wp-content/uploads/2016/09/logo.png) no-repeat 0px 5px;height: 60px; width: 60px;display: inline-block;background-size: contain;float: left;}" . //margin-left: 10px;margin-top: 3px;
                                    ".page-title {float: left;width: 87%;font-weight: 300;font-size: 1.8em;padding-left: 25px;}" .
                                    ".page-title small {color: rgba(255, 255, 255, 0.6);font-size: 0.6em;}" .
                                    ".detail-date {float: right;width: 20%;font-size: 1.2em;overflow: hidden;word-wrap: break-word;text-align: right;margin-right: 15px;margin-top: 10px;}" .
                                    "main {float: left;min-height: 83vh;width: 100%;margin-bottom: 10px;}" .
                                    ".reset-head {color: #ecad2a;padding-bottom: 10px;border-bottom: 1px solid;width: 80%;margin: 0 auto; margin-top: 30px}" .
                                    ".detail-pass,.act-link {width: 50%;margin: 0 auto;padding: 20px;box-sizing: border-box;}" .
                                    ".detail-pass ul li {padding: 10px 0px;font-size: 1.2em;color: #777;}" .
                                    "footer {float: left;width: 100%;background: #6dbb4a;height: 60px;}" .
                                    ".footer-link {color: #fff;text-align: right;padding-top: 25px;padding-right: 15px;}" .
                                    ".detail-data span {font-weight: bolder}" .
                                    ".act-link{margin-bottom: 15px;}" .
                                    ".act-link a {color: #fff;text-decoration: none;font-size: 1.4em;background: #008ab1;padding: 15px 20px;border-radius: 30px;}" . //box-shadow: inset 0 0 20px #ddd;transition-duration: 0.5s;
                                    //".act-link a:hover {box-shadow: inset 0 0 10px #ecad2a;color: #ecad2a;transition-duration: 0.5s;}".
                                    ".act-link a:hover {background: #ecad2a;}" .
                                    ".detail-top,.detail-bottom {width: 80%;margin: 0 auto;color: #555;border-bottom: 2px solid #eee;}" .
                                    ".detail-bottom {border-bottom: none;padding-top: 15px;border-top: 2px solid #eee;}" .
                                    ".color-text {color: #008ab1;font-size: 1.2em;text-decoration: none;}" .
                                    "</style>" . "</head>" . "<body>";
                            $messageBodyMid = '<div class="container">' .
                                    '<header>' .
                                    '<a href="#" class="logo"></a>' .
                                    '<h1 class="page-title">Invoice <small>Monthly</small></h1></header>' .
                                    '<main><h1 class="reset-head">Hi ' . $valueStudent['STUDENT_FULL_NAME'] . '</h1>' .
                                    '<div class="detail-top">' .
                                    '<p>' .
                                    "Happiness Is Just A Short Drive Away!<br />Welcome to Gulmohar Greens, Ahmedabad's finest weekend getaway that offers a wide-gamut of activities, a 60,000 sq. ft. Sports complex, suites and deluxe rooms, eclectic cuisines, cinema, spa, Jacuzzi, golfing and whatnot! Spread among a sprawling 75 acres, it is also the preferred choice for a classy, destination wedding." .
                                    '</p>' .
                                    '<p>' .
                                    'Please find the following attachment of your monthly acitivty expenses.' .
                                    '</p>' .
                                    '</div>' .
                                    '<div class="detail-bottom">' .
                                    '<p>' .
                                    'With regards,<br >' .
                                    'Gulmohar Greens-Golf & Country Club Ltd.' .
                                    '</p>' .
                                    '</div>' .
                                    '</main>' .
                                    '<footer>' .
                                    '<section class="footer-link">&copy; Gulmohar Greens-Golf & Country Club Ltd. | ' . date('Y') . '</section>' .
                                    '</footer>' .
                                    '<p>Note : Please do not reply to this mail. This is a system generated email.<br />The information contained in this electronic message and any attachments to this message are intended for exclusive use of the addressee(s) and may contain confidential or privileged information. If you are not the intended recipient, please notify the sender immediately and destroy all copies of this message and any attachments. The views expressed in this E-mail message / Attachments, are those of the individual sender.</p>';



                            $messageBodyFooter = "</div></body></html>";

                            $messageBody = $messageBodyHeader . $messageBodyMid . $messageBodyFooter;
                            //    $fromAddress = $resultSms[0]['CLIENT_EMAIL'];
                            //   $fromName = $resultSms[0]['CLIENTNAME'];


                            


                            $fromAddress = 'support@lithe.in';
                            $fromName = 'Lithe Technology';
                            $email = new SendGrid\Email();
                            $email->addTo($resStudentData[0]['EMAIL']);
                            // $email->addToName($valueSender);
                            $email->setFrom($fromAddress);

                            $allAttachmentArr[] = $fileLocation;

                            //print_r($allAttachmentArr); exit;
                            if (!empty($allAttachmentArr)) {
                                foreach ($allAttachmentArr as $docKey => $docValue) {
                                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                                        $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                                    }
                                }
                            }

                            $subject = "INDE CAMPUS :General Invoice";
                            $email->setFromName($fromName);
                            $email->setSubject($subject);

                            //$messageBody = "please find attached document for ";
                            $email->setHtml($messageBody);
                            //                       print_r($email); exit;
                            $response = $sendGrid->send($email);
                            $responseMail = $response->getBody();


                            if (!empty($allAttachmentArr)) {
                                foreach ($allAttachmentArr as $docKey => $docValue) {
                                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                                        unlink(APP_SERVER_ROOT . "/attachment/" . basename($docValue)); // add attachment 
                                    }
                                }
                            }
                            /* Mail sent code End */
                       }
                    }
                } 
            }

            
        } else {
            echo "No Records"; 
           /* $result = array("status" => NORECORDS);
            http_response_code(400);*/
        }
    } else {
       /* $result = array("status" => STARTISNOTGREATERTHANENDDATE);
        http_response_code(400);*/
    }
} else {
   /* $result = array("status" => PARAM);
    http_response_code(400);*/
}

?>