<?php

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

set_time_limit(0);
require_once '../include/globals.php';
require_once('../include/function_DL.php');
//require_once 'phpMailHandler.php'; //PHP function file for common sms function 

$clientId = 1;

$studentDetail = "SELECT SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,SM.COUNTRY_CODE,SM.MOBILE_NO,SM.EMAIL,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.PINCODE,SM.FK_CITY_ID,SM.FK_STATE_ID,SM.FK_COUNTRY_ID,SM.COLLEGE_NAME,SM.GENDER,SM.BLOOD_GROUP,SM.COURSE,SM.ACDEMIC_YEAR,SM.ADDRESS_PROOF,SM.MEDICAL_CONDITION,SM.AADHAR_NO,SM.PASSPORT_NO,SM.EMERGENCY_CONTACT_PERSON_NO,SM.EMERGENCY_CONTACT_PERSON_NAME,SM.BIRTHDATE,SM.ANNIVERSARYDATE,CO.COUNTRY_NAME,ST.STATE_NAME,CT.CITY_NAME,SM.COLLEGE_NAME,SM.COLLEGE_ADDRESS_LINE1,SM.COLLEGE_ADDRESS_LINE2,SM.COLLEGE_ADDRESS_LINE3,SM.COLLEGE_COUNTRY_ID,SM.COLLEGE_STATE_ID,SM.COLLEGE_CITY_ID,SM.COLLEGE_PINCODE FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . STATE . " ST ON ST.PK_STATE_ID=SM.FK_STATE_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID WHERE SM.DELETE_FLAG=0 AND SM.FK_CLIENT_ID='" . $clientId . "'";
//echo "$studentDetail"; die;
$resultStaudentDetail = fetch_rec_query($studentDetail);

if (count($resultStaudentDetail) > 0) {
    $studentDetailArr = array();
    foreach ($resultStaudentDetail as $keyStudentDetal => $valuStudentDetail) {
        $studentDetail[$keyStudentDetal]['memberShipNo'] = $valuStudentDetail['MEMBERSHIP_NUMBER'];
        $studentDetail[$keyStudentDetal]['firstName'] = $valuStudentDetail['FIRST_NAME'];
        $studentDetail[$keyStudentDetal]['lastName'] = $valuStudentDetail['LAST_NAME'];
        $studentDetail[$keyStudentDetal]['countryCode'] = $valuStudentDetail['COUNTRY_CODE'];
        $studentDetail[$keyStudentDetal]['mobileNo'] = $valuStudentDetail['MOBILE_NO'];
        $studentDetail[$keyStudentDetal]['email'] = $valuStudentDetail['EMAIL'];
        $studentDetail[$keyStudentDetal]['addressLine1'] = $valuStudentDetail['ADDRESS_LINE1'];
        $studentDetail[$keyStudentDetal]['addressLine2'] = $valuStudentDetail['ADDRESS_LINE2'];
        $studentDetail[$keyStudentDetal]['addressLine3'] = $valuStudentDetail['ADDRESS_LINE3'];
        $studentDetail[$keyStudentDetal]['pinCode'] = $valuStudentDetail['PINCODE'];
        $studentDetail[$keyStudentDetal]['fkStateId'] = $valuStudentDetail['FK_STATE_ID'];
        $studentDetail[$keyStudentDetal]['stateName'] = $valuStudentDetail['STATE_NAME'];
        $studentDetail[$keyStudentDetal]['fkCountryId'] = $valuStudentDetail['FK_COUNTRY_ID'];
        $studentDetail[$keyStudentDetal]['countryName'] = $valuStudentDetail['COUNTRY_NAME'];
        $studentDetail[$keyStudentDetal]['countryName'] = $valuStudentDetail['COUNTRY_NAME'];
        $studentDetail[$keyStudentDetal]['cityId'] = $valuStudentDetail['FK_CITY_ID'];
        $studentDetail[$keyStudentDetal]['cityName'] = $valuStudentDetail['CITY_NAME'];
        $studentDetail[$keyStudentDetal]['collegeName'] = $valuStudentDetail['COLLEGE_NAME'];
        $studentDetail[$keyStudentDetal]['collegeAddressLine1'] = $valuStudentDetail['COLLEGE_ADDRESS_LINE1'];
        $studentDetail[$keyStudentDetal]['collegeAddressLine2'] = $valuStudentDetail['COLLEGE_ADDRESS_LINE2'];
        $studentDetail[$keyStudentDetal]['collegeAddressLine3'] = $valuStudentDetail['COLLEGE_ADDRESS_LINE3'];
        $studentDetail[$keyStudentDetal]['collegeCountryId'] = $valuStudentDetail['COLLEGE_COUNTRY_ID'];
        $studentDetail[$keyStudentDetal]['collegeStateId'] = $valuStudentDetail['COLLEGE_STATE_ID'];
        $studentDetail[$keyStudentDetal]['collegeCityId'] = $valuStudentDetail['COLLEGE_CITY_ID'];
        $studentDetail[$keyStudentDetal]['collegePinCode'] = $valuStudentDetail['COLLEGE_PINCODE'];
        $studentDetail[$keyStudentDetal]['gender'] = $valuStudentDetail['GENDER'];
        $studentDetail[$keyStudentDetal]['bloodGroup'] = $valuStudentDetail['BLOOD_GROUP'];
        $studentDetail[$keyStudentDetal]['course'] = $valuStudentDetail['COURSE'];
        $studentDetail[$keyStudentDetal]['acdemicYear'] = $valuStudentDetail['ACDEMIC_YEAR'];
        $studentDetail[$keyStudentDetal]['addressProof'] = $valuStudentDetail['ADDRESS_PROOF'];
        $studentDetail[$keyStudentDetal]['medicalCondition'] = $valuStudentDetail['MEDICAL_CONDITION'];
        $studentDetail[$keyStudentDetal]['aadherNo'] = $valuStudentDetail['AADHAR_NO'];
        $studentDetail[$keyStudentDetal]['passportNo'] = $valuStudentDetail['PASSPORT_NO'];
        $studentDetail[$keyStudentDetal]['emergencyNo'] = $valuStudentDetail['EMERGENCY_CONTACT_PERSON_NO'];
        $studentDetail[$keyStudentDetal]['emergencyName'] = $valuStudentDetail['EMERGENCY_CONTACT_PERSON_NAME'];
        $studentDetail[$keyStudentDetal]['birthDate'] = $valuStudentDetail['BIRTHDATE'];
        $studentDetail[$keyStudentDetal]['anniversaryDate'] = $valuStudentDetail['ANNIVERSARYDATE'];
    }
    $result = array("status" => SCS, "data" => array_values($studentDetail));
    http_response_code(200);
} else {
    $result = array("status" => NORECORDS);
    http_response_code(400);
}
?>