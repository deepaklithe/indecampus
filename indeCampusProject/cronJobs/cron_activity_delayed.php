<?php 

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

set_time_limit(0);
require_once '../include/globals.php';
require_once('../include/function_DL.php'); 
require_once 'phpMailHandler.php'; //PHP function file for common sms function 

$todayDate = date("Y-m-d"); // TODAY DATE 
$time = time(); // CURRENT TIME STAMP

$getQuery = "SELECT * FROM activity_master_act WHERE DUE_DATE < '".$todayDate."'  AND ACT_START_TIME ='0' AND ACT_END_TIME='0' AND DELETE_FLAG=0 AND DELAYED_FLAG=0 AND ACT_STATUS=0 ";
$fetchQuery = fetch_rec_query($getQuery);

$successArr = array();

if(count($fetchQuery)>0){
    foreach ($fetchQuery as $keyAct => $valueAct) {
        
        $updateArr = array();
        $updateArr['DELAYED_FLAG'] = '1';
        $updateArr['CHANGED_BY_DATE'] = time();
        
        $updateWhere = " PK_ACT_ID='".$valueAct['PK_ACT_ID']."' AND ACT_START_TIME ='0' AND ACT_END_TIME='0' AND DELETE_FLAG = 0 AND DELAYED_FLAG = 0 ";
        $updateData = update_rec(ACTIVITYMASTER_ACT, $updateArr,$updateWhere);
        if($updateData){
            echo "update data Client Level Id = ".$valueAct['C_ACT_ID'].'</br>';
            
            $successArr[] = 'Activity Convert In Delayed for Client Act Id : '.$valueAct['C_ACT_ID'].' on Client Id : '.$valueAct['FK_CLIENT_ID'];
            
        }else{
            echo "not update data Client Level Id = ".$valueAct['C_ACT_ID'].'</br>';
        }
    }
}

if(count($successArr)>0 && !empty($successArr)){
    
    $subject = "Activity Convert In Delayed From Cron - ".date("d-m-Y H:i A");

    $messageBody = '<html><body>'.
        '<table width="100%" style="border-collapse: collapse;">'.
            '<thead>'.
                '<tr>'.
                    '<th align="left" style="padding:10px; border: 1px solid black" bgcolor="#d6d3d3">Processed Data</th>'.
                '</tr>'.
            '</thead>'.
            '<tbody>';
                foreach ($successArr as  $valueArr) {
                    $messageBody.='<tr>'.
                    '<td style="padding:10px; border: 1px solid black">'.$valueArr.'</td>'.
                    '</tr>';
                }   

    $messageBody .='</tbody></table></body></html>';

    $reciverAddress = RECIVEREMAILADDRESS;
    $reciverAddress2 = RECIVEREMAILADDRESS2;

// SEND EMAIL
    emailPHPMailer($subject, $messageBody, $reciverAddress,$reciverAddress2);
}

//while ($row = mysql_fetch_assoc($fetchQuery)){
//    $update_delayed_flag= mysql_query("UPDATE activity_master_act set DELAYED_FLAG=1 WHERE PK_ACT_ID='".$row['PK_ACT_ID']."' AND ACT_START_TIME ='0' AND ACT_END_TIME='0' AND DELETE_FLAG=0 AND DELAYED_FLAG=0");
//}

?>