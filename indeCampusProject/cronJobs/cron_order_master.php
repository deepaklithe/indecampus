<?php

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

set_time_limit(0);
require_once '../include/globals.php';
require_once('../include/function_DL.php');
$clientId = 1;

//===PROCESS START==//
/*
 * 1. GET STUDENT DATA.CLIENT ID WISE TO FETCH
 * 2. GET DATA FROM EPOSE SIDE(getOrderListing).
 * 2. GET ITEAM DATA.ITEM_MASTER_FRANCHISER( FOR MEALS AND LAUNDRYSERVICES)()
 * 3. GET EPOSE DATA SUCESS THAN UPDATE QTY.IN CAL_ALLOCATION_TRANS IN INDECAMPUS.
 */
//====END PROCESS==//


$createOrderArr = array();

$studentDetail = "SELECT PK_STUD_ID,MEMBERSHIP_NUMBER FROM " . STUDENTMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG='0'";
$resultStaudentDetail = fetch_rec_query($studentDetail);

if (count($resultStaudentDetail) > 0) {
    $studentDetail = array();
    foreach ($resultStaudentDetail as $keyStudentDetal => $valuStudentDetail) {
        $studentDetail[$keyStudentDetal]['memberShipNo'] = $valuStudentDetail['MEMBERSHIP_NUMBER'];
        $studentDetail[$keyStudentDetal]['studentId'] = $valuStudentDetail['PK_STUD_ID'];

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;

        $result = array();
        $getOrderData = getOrderListing($requestData);
        //get order from epos. //how many meals and laundry order .
        //CREATE ITEM ARRR
        if ($getOrderData['status'] == SCS) {
            $itemDataArr = array();
            foreach ($getOrderData as $keyItem => $valueItem) {

                $sqlgetItem = "SELECT PK_ITEM_ID FROM " . ITEMMASTERFRANCHISOR . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' AND ITEM_NAME = '" . $valueItem['itemName'] . "' "; //ITEM_NAME=3=Meals,4=Laundry Services.
                $getItemName = fetch_rec_query($sqlgetItem);

                $itemDataArr[$keyItem]['PK_ITEM_ID'] = $valueItem['pkitemId'];
                $itemDataArr[$keyItem]['FK_FRAN_ITEM_ID'] = $valueItem['itemId'];
                $itemDataArr[$keyItem]['ITEM_NAME'] = $valueItem['itemName']; //3=Meals,4=Laundry Services.
                $itemDataArr[$keyItem]['ITEM_QTY'] = $valueItem['itemQty'];
                $itemDataArr[$keyItem]['ITEM_PRICE'] = $valueItem['itemPrice'];
                $itemDataArr[$keyItem]['FK_FRAN_PARENT_GROUP_ID'] = $valueItem['groupId'];
                $itemDataArr[$keyItem]['IS_CANCELLED_VOID_NC'] = "false"; //HARD CODED
                $itemDataArr[$keyItem]['CANCEL_VOID_NC_REMARK'] = "";
                $itemDataArr[$keyItem]['STATUS'] = "ORDERED"; //HARD CODED
                $itemDataArr[$keyItem]['OPTIONQTY'] = "1"; //HARD CODED
                $itemDataArr[$keyItem]['OPTIONID'] = "";
                $itemDataArr[$keyItem]['OPTIONNAME'] = "";
                $itemDataArr[$keyItem]['OPTIONRATE'] = "";
                $itemDataArr[$keyItem]['SRNO'] = "";
            }
            $createOrderArr['itemData'] = $itemDataArr;

            $finalRequest['postData'] = json_encode($createOrderArr);
//            print_r($finalRequest);exit;//EPOS DATA.COME OR NOT
            $data = curlCall(MASTERURL, $finalRequest); //change masterurl//set indecampsu url.
            if ($data['status'] == SCS) {
                $updateArr = array();
                $updateArr['QTY'] = $valueItem['QTY'];
                $updateArr['CHANGED_BY'] = 1;
                $updateArr['CHANGED_BY_DATE'] = time();

                $updateWhere = "FK_ITEM_ID = '" . $valueItem['pkitemId'] . "' AND DELETE_FLAG = '0' ";

                $updateData = update_rec(CALALLOCATIONTRANS, $updateArr, $updateWhere);
            } else {
                $result = array("status" => UPDATEFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    }
} else {
    $result = array("status" => NORECORDS);
    http_response_code(400);
}
