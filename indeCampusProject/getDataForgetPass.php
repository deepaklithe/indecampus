<?php

/* * **************************************** */
/*    AUTHOR : DEEPAK PATIL */
/*    CREATION DATE : 24-02-2015           */
/*    FILE TYPE : PHP                      */
/* * **************************************** */


// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

// Include Required File...

require_once "include/globals.php"; // GLOBAL FILE INCLUDE
require_once "include/function_developerMaster_indeCampus.php"; // FORGET FUNCTION FILE
// Default variable
$postData = isset($_POST) ? $_POST : "";
$requestCase = $postData['postData']['requestCase'];

//echo "fddf"; die;
switch ($requestCase) {

    case "forgetPassword" : //user login   
        $result = forgetPasswordFP_BL($postData);
        $jsonData = json_encode($result);
        break;
    case "userforgetPassword" : //user login   
        $result = userforgetPasswordFP_BL($postData);
        $jsonData = json_encode($result);
        break;

    default: // default case
        $result['status'] = CASEERROR;
        $jsonData = json_encode($result);
        break;
}
/* ======= RETURN DATA AFTER REQUESTING BY ANY CALL FROM API ========= */
if ($jsonData) {
    // if return json
    echo $jsonData;
} else {
    //if json is not returned
    $result = array('status' => FAILMSG);
    echo json_encode($result);
}
?>

