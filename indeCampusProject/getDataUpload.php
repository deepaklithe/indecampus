<?php

/*
 *  Author      : DEEPAK PATIL
 *  Created On  : 06/02/2015
 *  Type        : PHP
 *  Purpose     : UPLOAD MANAGEMENT
 */

// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once "include/globals.php"; // GLOBAL VAR FILE
//require_once "include/function_DL.php"; // DL FUNCTION FILE (COMMON FUNCTION FILE)//INCLUDED IN GLOBAL FILE

require_once "include/function_user_indeCampus.php"; // 
require_once "include/function_room_indeCampus.php"; // Room related function in it.
require_once "include/function_student_indeCampus.php"; // student related function in it.
require_once "include/function_developerMaster_indeCampus.php";
require_once "include/function_lookupmaster_indeCampus.php";
require_once "include/function_item_indeCampus.php";
require_once "include/function_payment_indeCampus.php";
require_once 'include/function_report_indeCampus.php';
require_once 'include/function_upload_master_data.php';


// Default variable
//$postData = isset($_POST) ? json_decode($_POST['postData']) : "";
$postData = isset($_REQUEST) ? $_REQUEST : "";
//print_r($postData);exit;
$postFile = $_FILES['attachmentName'];
//print_r($postFile);exit;
if (isset($_FILES['attachmentName'])) {

    $postDataVal = objectToArray(json_decode($postData['postData'])); //CONVERT OBJECT TO ARRAY
    if (!empty($postDataVal) && !empty($postDataVal)) {
        //FOR CHECK JWT TOKEN 
        if (isset($postDataVal['postData'])) {
            $tempPostData = $postDataVal;
            $postData = $postDataVal;
            
        } else {
            $tempPostData['postData'] = $postDataVal;
            $postData['postData'] = $postDataVal;
        }
    } else {

        if (isset($postData['postData'])) {
            $tempPostData = $postData;
        } else {
            $tempPostData['postData'] = $postData;
        }
        $postData = $postData;
    }
} else {
    $postData = $postData;
    //FOR CHECK JWT TOKEN 
    if (isset($postData['postData'])) {
        $tempPostData = $postData;
    } else {
        $tempPostData['postData'] = $postData;
    }
}

//print_r($postData);exit;
$checkSignature = checkJWTToken($tempPostData);
//print_r($checkSignature); exit;;


if ($checkSignature['status'] == SCS) {
    $requestCase = !empty($postData['postData']['requestCase']) ? $postData['postData']['requestCase'] : $postData['requestCase'];

    switch ($requestCase) {

        case 'attachmentStudentUpload' : // ATTACHMENT UPLOAD FOR EMAIL
            $result = attachmentStudentUpload($postData, $postFile);
            $jsonData = json_encode($result);
            break;

        case 'attachmentStudentComplainUpload' : // ATTACHMENT UPLOAD FOR STUDENT COMPLAIN
            $result = attachmentStudentComplainUpload($postData, $postFile);
            $jsonData = json_encode($result);
            break;

        case 'attachmentStudentSuspensionUpload' : // ATTACHMENT UPLOAD FOR STUDENT COMPLAIN
            $result = attachmentStudentSuspensionUpload($postData, $postFile);
            $jsonData = json_encode($result);
            break;

        case 'attachmentStudentProfileUpload' : // ATTACHMENT UPLOAD FOR STUDENT COMPLAIN
            $result = attachmentStudentProfileUpload($postData, $postFile);
            $jsonData = json_encode($result);
            break;

        case 'uploadMasterData' : // MASTER DATA UPLOAD.
            $result = masterDataUpload($postData, $postFile);
            $jsonData = json_encode($result);
            break;
	
	case 'userProfileImageUpload' : // MASTER DATA UPLOAD.
            $result = userProfileImageUpload($postData, $postFile);
            $jsonData = json_encode($result);
            break;

        default: // default case
            $result['status'] = CASEERROR;
            $jsonData = json_encode($result);
            break;
    }
} else {
    $jsonData = json_encode(array("status" => $checkSignature['status']));
}

if ($jsonData) { // if return json 
//    echo $jsonData;
    header("Authorization: Bearer " . trim($checkSignature['data']));
    header("Content-Type: application/x-www-form-urlencoded; charset=UTF-8");
    echo $jsonData;
} else { //if json is not returned
    $result = array('status' => FAILMSG);
    echo json_encode($result);
}
?>
