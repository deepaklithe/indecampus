<?php

/* * ************************************************ */
/* AUTHOR : LITHE TECHNOLOGY PVT. LTD. */
/* DEVELOPED BY : MONTU PATEL */
/* CREATION DATE : 09-02-2018 */
/* FILE TYPE : PHP */
/* PROJECT : CUSTOMIZE SOFTWARE */
/**
 * ************************************************
 */
// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
ini_set('memory_limit', '-1');

// Include Required File...


require_once "include/globals.php"; // GLOBAL VAR FILE
//require_once "include/dompdf/dompdf_config.inc.php"; // PDF 
//require_once "include/function_cal.php"; // CALANDER FUNCTION FILE (CAL DONE)
require_once 'include/function_centralised_indeCampus.php';
//$clientId = 4;
//$flag = getCentralizeFlag($clientId);

if(isset($_REQUEST['postdata']) && !empty($_REQUEST['postdata'])) {
    $data = array();
    $data = objectToArray(json_decode($_REQUEST['postdata']));
    $requestCase = $data[0]['requestCase'];
    $postData['postData']    = $data[0];
} else {
    $postData = isset($_POST) ? $_POST : "";
    $requestCase = $postData ['postData'] ['requestCase'];
}
//FOR ADD API LOG DATA OF THIS USER TO STORE THE REQUEST
apiRequestLogData(1,1, 0,$requestCase,$postData);
//$clientId = 4; //$postData ['postData'] ['clientId'];
//print_r($postData); die;

//FIRST CHECK CENTRALIZE FLAG ON
//$sqlGetFlag = "SELECT CUSTOMIZE_FLAG FROM ".CLIENTMASTER." WHERE PK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0' AND CUSTOMIZE_FLAG=1 ";
//$resultGetFlag = fetch_rec_query($sqlGetFlag);
//if(count($resultGetFlag) > 0){
    switch ($requestCase) { 
        //ePOS
        case "eposSalesTransaction":
            $result = eposSalesTransaction($postData);
            $jsonData = json_encode($result);
            break;
        
        case "eposCreditNoteTransaction":
            $result = eposCreditNoteTransaction($postData);
            $jsonData = json_encode($result);
            break;
        
        case "eposReceipt":
            $result = eposReceipt($postData);
            $jsonData = json_encode($result);
            break;
        
    }
//} else {
//    $result = array("status" => SCS);
//    $jsonData = json_encode($result);
//}
/* ======= RETURN DATA AFTER REQUESTING BY ANY CALL FROM API ========= */
if ($jsonData) {
    // if return json
    echo $jsonData;
} else {
    // if json is not returned
    $result = array(
        'status' => FAILMSG
    );
    echo json_encode($result);
}
?>