<?php

/*
 * Created By : Vaibhav Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 21-Apr-2016.
 * File : function_activity.
 * File Type : .php.
 * Project : eTASK
 * Purpose : Maintain activity for eTASK
 * */


/* ======== START - COMMON UPLOAD fFILE FUNCTION ============ */

function uploadImageActivity($attachment) {
    //print_r($attachment); exit;
    $attachmentSize = $attachment['size'];
    $attachmentName = $attachment['name'];
    $imageType = $attachment['type'];
    $imageTempName = $attachment['tmp_name'];
    // IMAGE PROCESS START HERE
    $filename = stripslashes($attachmentName);

    $ext = strtolower(getExtension($filename));
    $filename = str_replace(' ', '', $filename); //remove all space in file name
    $validExt = array("jpg", "jpeg", "gif", "png", "mp4", "csv", "pdf", ".txt"); // VALID FORAMT TO UPLOAD
    //$uploadPath = UPLDPATH . "/attachment/activity-attachment/";
    $uploadPathRoot = APP_SERVER_ROOT . "/attachment/activity-attachment/";

    //echo $uploadPathRoot; exit;
    if ($attachmentSize < (MAX_SIZE * 1024)) {

        if (in_array($ext, $validExt)) { // EXT. CHECK
            $srctype = "image";
            if ($ext == "mp4") {
                $srctype = "video";
            }
            $attachment_name = time() . "_" . $filename; // NAME OF THE FILE FOR OUR SYSTEM
            $newname = $uploadPathRoot . $attachment_name; // FULL PATH OF FILE DESTINALTION
            $uploadeFile = move_uploaded_file($imageTempName, $newname);
            if ($uploadeFile) { // UPLOAD FILE TO DESTIGNATION FOLDER 
                $result = array("status" => SCS, "fileName" => $attachment_name, "imageType" => $imageType, "srcType" => $srctype, "fileSize" => $attachmentSize, "fileExt" => $ext); // IF SUCCESS THEN RETURN TYPE AND NAME
                http_response_code(200);
            } else { // UPLOAD ERRPR
                $result = array("status" => UPLOADERROR);
                http_response_code(400);
            }
        } else { // FILEFORMAT ERROR
            $result = array("status" => FILEFORMTERROR, "fileExt" => $attachmentName);
            http_response_code(400);
        }
    } else { // FILE SIZE ERROR
        $result = array("status" => FILESIZEERROR);
        http_response_code(400);
    }
    return $result; // RETURN VALUE TO THE CALL FUNCTION 
}

/* ======== END - COMMON UPLOAD FILE FUNCTION ============ */

//START FUNCTION FOR CREATE/UPDATE CUSTOM ACTIVITY 
function createUpdateActivity($postData, $postFile) {
//    print_r($postData);die;
//    print_r($postFile); exit;
    
    $insertAct = array();
    $postdata = !empty($postData['postData']) ? $postData['postData'] : $postData; //POST DATA
    
    $actTitle = isset($postdata['actTitle']) ? addslashes(trim($postdata['actTitle'])) : ""; // TITLE
    $actDesc = isset($postdata['actDesc']) ? addslashes(trim($postdata['actDesc'])) : ""; // DESCRIPTION
    $actDueDate = isset($postdata['actDueDate']) ? addslashes(trim($postdata['actDueDate'])) : ""; // DUE DATE   
    $actDueTime = isset($postdata['actDueTime']) ? addslashes(trim($postdata['actDueTime'])) : ""; // DUE DATE   
    $reminderTime = isset($postdata['reminderTime']) ? addslashes(trim($postdata['reminderTime'])) : ""; // DUE DATE   
    $endDate = isset($postdata['actEndDate']) ? addslashes(trim($postdata['actEndDate'])) : ""; // END DATE   
    $endTime = isset($postdata['actEndTime']) ? addslashes(trim($postdata['actEndTime'])) : ""; // END TIME
    $actMode = isset($postdata['actMode']) ? addslashes(trim($postdata['actMode'])) : ""; // MODE
    $actType = isset($postdata['actType']) ? addslashes(trim($postdata['actType'])) : ""; // TYPE
    $custType = isset($postdata['custType']) ? addslashes(trim($postdata['custType'])) : ""; // TYPE
    $actAttachment = $postFile; // ATTCHMENT
    $status = array();
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $actRefType = (isset($postdata['actRefType']) && !empty($postdata['actRefType'])) ? addslashes(trim($postdata['actRefType'])) : "ACT"; // ACT,ORD,INQ,SERVICE,LEADINQ
    
    $fromInsert = isset($postdata['fromInsert']) ? addslashes(trim($postdata['fromInsert'])) : ""; // shedual act
    
    $projectId = isset($postdata['projId']) ? addslashes(trim($postdata['projId'])) : ""; //PROJECT ID
    $unRegCustName = isset($postdata['unRegCustName']) ? addslashes(trim($postdata['unRegCustName'])) : ""; // UN REGISTER USER
    $unRegCustMobile = isset($postdata['unRegCustMobile']) ? addslashes(trim($postdata['unRegCustMobile'])) : ""; // UN REGISTER USER MOBILE
    
    $logFlag = isset($postdata['logFlag']) ? addslashes(trim($postdata['logFlag'])) : ""; // 1: FOR NOT INSETED DATA IN THE LOG REPORT COME FROM INQ CREATE API
    
//    $conPerMobile = isset($postdata['conPerMobile']) ? addslashes(trim($postdata['conPerMobile'])) : ""; // CONPERMOBILE
//    $conPerName = isset($postdata['conPerName']) ? addslashes(trim($postdata['conPerName'])) : ""; // CONPERNAME
    $contactId = isset($postdata['contactId']) ? addslashes(trim($postdata['contactId'])) : "0"; // CONTACTID
    
    $assigneTo = "";
    if (isset($postdata['assigneTo'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $assigneTo = addslashes(trim($postdata['assigneTo'])); // ACTIVITY ID FOR EDIT
    }

    $isUserGroup = "";
    if (isset($postdata['isUserGroup'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $isUserGroup = addslashes(trim($postdata['isUserGroup'])); // ACTIVITY ID FOR EDIT
    }

    $isCustomerGroups = "";
    if (isset($postdata['isCustomerGroup'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $isCustomerGroups = addslashes(trim($postdata['isCustomerGroup'])); // ACTIVITY ID FOR EDIT
    }

    $actId = "";
    if (isset($postdata['actId'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $actId = addslashes(trim($postdata['actId'])); // ACTIVITY ID FOR EDIT
    }

    $refId = "";
    if (isset($postdata['refId']) && $postdata['refId'] != "") { // IF REFERENCE ID SENDED IN PARAMETER IN CASE OF CLOSE AND CREATE NEW ACTIVITY
        $refId = addslashes(trim($postdata['refId'])); // IF IS CUSTOMER GROUP =1 THEN CUST GROUP ID ,0=CUSTOMER ID
    }
        
    $customerId = "";
    if (isset($postdata['customerId'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $customerId = addslashes(trim($postdata['customerId'])); // IF IS CUSTOMER GROUP =1 THEN CUST GROUP ID ,0=CUSTOMER ID 
    }
    
    $custId = "";
    if (isset($postdata['custId']) && $postdata['custId'] != "") { // IF INQ ID IS SEND IN CASE OF ACTIVITY BIND TO INQUIRY        
        $custId = addslashes(trim($postdata['custId'])); 
    }

    $inqId = "";
    if (isset($postdata['inqId']) && $postdata['inqId'] != "") { // IF INQ ID IS SEND IN CASE OF ACTIVITY BIND TO INQUIRY        
        $inqId = addslashes(trim($postdata['inqId'])); 
        
        if($actRefType == 'INQ' || $actRefType == 'ORD'){
            $table = INQMASTER;
            $whereCond = " AND PK_INQ_ID = '".$inqId."' ";
        }elseif ($actRefType == 'SERVICE') {
            $table = SERVICEMASTER;
            $whereCond = " AND PK_SERVICE_ID = '".$inqId."' ";
        }elseif ($actRefType == 'LEADINQ') {
            $table = LEADINQMASTER;
            $whereCond = " AND PK_LEAD_ID = '".$inqId."' ";
        }else{
            $whereCond = "";
        }
        //ADD BY DARSHAN PATEL FOR THE GET CUSTOMER NAME FROM IDS ON 19-03-2018
        if(!empty($whereCond) && $whereCond != ""){
            $getCustomerId = "SELECT FK_CUST_ID,FK_CONTACT_ID FROM ".$table." WHERE DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$clientId."' ".$whereCond." LIMIT 0,1 ";
            $resultCustomerId = fetch_rec_query($getCustomerId);
            if(count($resultCustomerId)>0){
                $custId = $resultCustomerId[0]['FK_CUST_ID'];
                $customerId = $resultCustomerId[0]['FK_CUST_ID'];
                $contactId = $resultCustomerId[0]['FK_CONTACT_ID'];
            }
        }
    }
    
//    print_r($contactId);exit;
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP

    if ($actDueDate < $todayDate) {
        $result = array("status" => DUEDATENOTGREATERTHENTODAY);
        return $result;
//        exit;
    }
    
    //FOR CONTACT PERSON
//    if(!empty($conPerMobile) && $contactId == '0' && empty($contactId)){
//        
//        $sqlQuery = "SELECT PK_CONTACT_PERSON_ID FROM " . CUSTOMER . " CM INNER JOIN ".MULTICONTACTMASTER." MCP ON MCP.FK_CONTACT_ID = CM.PK_CUST_ID WHERE CM.FK_CLIENT_ID = '".$clientId."' AND MCP.CONTACT_NUMBER = '" . $conPerMobile . "' AND CM.DELETE_FLAG = 0 AND MCP.DELETE_FLAG = 0 LIMIT 0,1";
//        $queryResult = fetch_rec_query($sqlQuery);
//        //print_r($queryResult); exit;
//        if(count($queryResult) > 0){
//            $contactId = $queryResult[0]['PK_CONTACT_PERSON_ID'];
//        }
//    }
    
    // CHECK THE SIZE AND TYPE OF ATTACHMENT WHILE CREATING ACT
    if(!empty($actAttachment)){  
        $validExt = array("image/jpg", "image/jpeg", "image/gif", "image/png", "application/doc", "application/docx", "application/pdf", "application/xlsx","application/csv", "image/bmp", "text/plain"); // VALID FORAMT TO UPLOAD
        for($t=0;$t<count($actAttachment['name']);$t++){ 
//            print_r($actAttachment);
            if($actAttachment['error'][$t] == 0){
                if($actAttachment['size'][$t] > (MAX_SIZE*1024)){
                    $result = array("status" => FILESIZEERROR);
                    http_response_code(400);
                    return $result;
                }else if(!in_array($actAttachment['type'][$t] , $validExt)){ 
                    $result = array("status" => FILEFORMTERROR);
                    http_response_code(400);
                    return $result;
                }
            }
        }
    }  
    
//    print_r($actAttachment); exit;
    
    $insertAct['ACT_ATTACHMENT'] = "";
    $insertAct['FK_CLIENT_ID'] = $clientId;
    $insertAct['FK_ORG_ID'] = $orgId;
    $insertAct['ACT_TITLE'] = $actTitle;
    $insertAct['ACT_DESC'] = $actDesc;
    $insertAct['DUE_DATE'] = date("Y-m-d", strtotime($actDueDate));
    $insertAct['ACT_MODE'] = $actMode;
    $insertAct['ACT_TYPE'] = $actType;
    $insertAct['CUST_TYPE'] = $custType;
    $insertAct['DUE_TIME'] = date("h:i A",strtotime($actDueTime));
    $insertAct['REMINDER_ON'] = $reminderTime;
    $insertAct['END_DATE'] = date("Y-m-d", strtotime($endDate));
    $insertAct['END_TIME'] = date("h:i A",strtotime($endTime));
    $insertAct['FK_PROJECT_ID'] = $projectId;
    $insertAct['UNREG_CUST_NAME'] = $unRegCustName;
    $insertAct['UNREG_CUST_MOBILE'] = $unRegCustMobile;

    if ($actId == "") { // CREATE CUSTOM ACTIVITY
        $insertAct['ACT_COMMENT'] = '';
        $insertAct['ACT_START_TIME'] = 0;
        $insertAct['ACT_END_TIME'] = 0;
        $insertAct['FROM_INSERT'] = $fromInsert;
        $insertAct['ACT_STATUS'] = 0;
        $insertAct['CREATED_BY'] = $userId; // CREATED BY USER ID
        $insertAct['CREATED_BY_DATE'] = time();
        $insertAct['LAST_SYNC_TIME'] = 0; // LAST SYNC TIME        
        $insertAct['REFERENCE_NO'] = $refId; //REFERENCE ID
        $insertAct['FK_INQ_ID'] = $inqId; //INQ ID
        $insertAct['ACT_REFERENCE_TYPE'] = $actRefType; //INQ ID
//        print_r($insertAct);exit;

        $assignedToArr = $custIdArr = array();
        if ($isUserGroup != "" && $assigneTo != "" && $isUserGroup != 0) {//IF USER GROUP SELECTED
            
            if($customerId != ""){
                $assignedToArr[] = $assigneTo;
                $insertAct['ASSIGN_TYPE'] = "GROUP"; //INQ ID
            }else{
                $getIdFromGroups = fetch_rec_query("SELECT FK_CUST_ID FROM " . GROUPCUSTTRNAS . " WHERE FK_GROUP_ID='" . $assigneTo . "' AND DELETE_FLAG=0");
                //print_r($getIdFromGroups); exit;
                if (count($getIdFromGroups) > 0) {
                    for ($i = 0; $i < count($getIdFromGroups); $i++) {
                        $assignedToArr[] = $getIdFromGroups[$i]['FK_CUST_ID'];
                    }
                }
                $insertAct['ASSIGN_TYPE'] = "NORMAL"; //INQ ID
            }
            
            
        } else { // IF USER ID SELECTED
            $assignedToArr[] = $assigneTo;
            $insertAct['ASSIGN_TYPE'] = "NORMAL"; //INQ ID
        }
        if ($customerId != "") { // IF CUSTOMER ID IS NOT BLANK
            if ($isCustomerGroups != 0 && $isCustomerGroups != "") {//IF CUSTOMER GROUP SELECTED
                $getIdFromCustGroups = fetch_rec_query("SELECT FK_CUST_ID FROM " . GROUPCUSTTRNAS . " WHERE FK_GROUP_ID='" . $customerId . "' AND DELETE_FLAG=0");
                if (count($getIdFromCustGroups) > 0) {
                    for ($i = 0; $i < count($getIdFromCustGroups); $i++) {
                        $custIdArr[] = $getIdFromCustGroups[$i]['FK_CUST_ID'];
                    }
                }
            } else { //IF CUSTOMER GROUP NOT SELECTED
                $custIdArr[] = $customerId;
            }
        }
//        print_r($custIdArr);
//        print_r($assignedToArr); exit;
        $attachmentUpload = array();
        $uploadFiles = array();
        
        // START SMS AND EMAIL INTO CRON MAIL MASTER
                    
        
        $clientRequest = getClientDetail($clientId);
        if($clientRequest['status'] == SCS){
            $clientDetail = $clientRequest['data'];
        }

        for ($i = 0; $i < count($assignedToArr); $i++) {
            $assignedToId = $assignedToArr[$i];
            
            if (count($custIdArr) > 0) {  //with Customer loop case              
                for ($j = 0; $j < count($custIdArr); $j++) { //Customer loop
       
                    $customerId1 = $custIdArr[$j];
//                    print_r($contactId);exit;
                    $contactId = '';
                    if(empty($contactId)){
//                        echo "hiii";exit;
                        $getContactPerson = "SELECT PK_CONTACT_PERSON_ID FROM ".MULTICONTACTMASTER." WHERE FK_CONTACT_ID = '".$customerId1."' AND FK_CLIENT_ID = '".$clientId."' ORDER BY PK_CONTACT_PERSON_ID ASC LIMIT 0,1 ";
                        $resultContactPerson = fetch_rec_query($getContactPerson);
//                        print_r($resultContactPerson);exit;
                        
                        if(count($resultContactPerson)>0){
                            $contactId = $resultContactPerson[0]['PK_CONTACT_PERSON_ID'];
                        }
                    }
//                    print_r($contactId);exit;
                    $insertAct['ASSIGN_TO'] = $assignedToId;
                    $insertAct['FK_CUST_ID'] = $customerId1;
                    $insertAct['FK_CONTACT_ID'] = $contactId;
                    $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                    $insertAct['C_ACT_ID'] = $getActId;
                    $insert_activity = insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                    $lastinsertedActId = $insert_activity['lastInsertedId'];
                    
                    // START GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                    $requestData = array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['actId'] = $lastinsertedActId;
                    $createActDetail = activityCalenderData($requestData);
                    $tempActDetail = ($createActDetail['status'] == SCS) ? $createActDetail['data'][0] :  $createActDetail['status'];
                    array_push($status, $tempActDetail);
                   
                    // END GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                    
                    //GOOGLE CAL CODE START
                    if($isUserGroup == '0'){//FOR ONLY SIGNLE USER NOT FOR USER GROUP
                        $startTimeCal = $endTimeCal = array();
                        $tempStartTime = strtotime(date("d-m-Y", strtotime($actDueDate))." ".date("h:i A",strtotime($actDueTime)));
                        $startTimeCal[0]['dateTime'] = dateToCal($tempStartTime); 
                        $startTimeCal[0]['timeZone'] = "Asia/Kolkata"; 

                        $tempEndTime = strtotime(date("d-m-Y", strtotime($endDate))." ".date("h:i A",strtotime($endTime)));
                        $endTimeCal[0]['dateTime'] = dateToCal($tempEndTime); 
                        $endTimeCal[0]['timeZone'] = "Asia/Kolkata"; 
                        //print_r($startTime); exit;
                        $summary = $actTitle;
                        $location = "";
                        $description = $actDesc;

                        insertEventIntoCalander($summary,$location,$description,$startTimeCal,$endTimeCal,$assignedToId,$clientId);                    
                        //FOR ALLOCATION
                        $actAllocationFun = insertInqAllocation($lastinsertedActId, $assignedToId, $clientId, "ACT", $userId);
                    }
                    
                    //GOOGLE CAL CODE START
                    
                    //COMMENT BY DARSHAN PATEL BUG REF : 6315
//                    $actAllocationFun = insertInqAllocation($lastinsertedActId, $assignedToId, $clientId, "ACT", $userId);                   
                    // GET DETAIL FOR REMINDER
                    if(!empty($reminderTime)){
                        // GET USER DETAIL 
                        $whereCondP = "PK_USER_ID = '".$assignedToId."' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$clientId."'";
                        $userDetail = getUserDetails($whereCondP);
                        
                        // GET CUSTOMER DETAIL
                        
                        $sqlCustomerDetail = "SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) CUSTOMERDETAIL,CUSTMOBILE FROM ".CUSTOMER." CM WHERE CM.FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG ='0' AND PK_CUST_ID = '".$customerId1."' LIMIT 0,1 ";
                        $resultCustomerDetail = fetch_rec_query($sqlCustomerDetail);
                    }
                    
                    if(!empty($refId)){
                        
                        $sqlGetAttch = "SELECT ATTACHFILE_LINK FROM ".ATTACHINQMASTER." WHERE ATTACH_CAT = 'ACT' AND FK_INQ_ID = '".$refId."' AND DELETE_FLAG = 0 ";
                        $resultGetAttch = fetch_rec_query($sqlGetAttch);
                        if(count($resultGetAttch) > 0){ // IF FOUND THEN ADD TO NEW INSERTED ACT
                            for($r=0;$r<count($resultGetAttch);$r++){
                                $attachmentLink = $resultGetAttch[$r]['ATTACHFILE_LINK'];
                                $insertAttchment = array();
                                $insertAttchment['ATTACHFILE_LINK'] = $attachmentLink;
                                $insertAttchment['ATTACH_CAT'] = "ACT";
                                $insertAttchment['FK_INQ_ID'] = $insert_activity['lastInsertedId'];
                                $insertAttchment['CREATED_BY'] = $userId;
                                $insertAttchment['CREATED_BY_DATE'] = time();
                                $insertAttchment['DELETE_FLAG'] = 0;
                                $insetAttchRequest = insert_rec(ATTACHINQMASTER, $insertAttchment);
                            }
                        }
                    }
                    //print_r($insert_activity); exit;
                    
                    if (!empty($actAttachment['name']) && empty($uploadFiles)) {
                        for($t=0;$t<count($actAttachment['name']);$t++){
                            $removeSpeChar = cleanString($actAttachment['name'][$t],$actAttachment);
//                            print_r($removeSpeChar);exit;
                            if($actAttachment['error'][$t] == 0){
                                $requestData = $postFile = array();
                                $requestData['clientId'] = $clientId;
                                $requestData['userId'] = $userId;
                                $requestData['actId'] = $lastinsertedActId;
                                $requestData['orgId'] = "";
                                //print_r($requestData);
                                $postFile['name'] =$removeSpeChar;
                                $postFile['type'] =$actAttachment['type'][$t];
                                $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                                $postFile['error'] =$actAttachment['error'][$t];
                                $postFile['size'] =$actAttachment['size'][$t];
                                
                                $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                                //print_r($attachmentUpload); exit;
                                $uploadFiles[$t] = $attachmentUpload['data']['fileLink'];
                            }
                        }
                    }else{
                        //echo "i m here after ".$j;
                        //print_r($uploadFiles); exit;
                        $attachmentLink = array_values($uploadFiles);
                        for($s=0;$s<count($attachmentLink);$s++){
                            $insertAttchment = array();
                            $insertAttchment['ATTACHFILE_LINK'] = $attachmentLink[$s];
                            $insertAttchment['ATTACH_CAT'] = "ACT";
                            $insertAttchment['FK_INQ_ID'] = $lastinsertedActId;
                            $insertAttchment['CREATED_BY'] = $userId;
                            $insertAttchment['CREATED_BY_DATE'] = time();
                            $insertAttchment['DELETE_FLAG'] = 0;
                            $insetAttchRequest = insert_rec(ATTACHINQMASTER, $insertAttchment);
                        }
                    }
                    
                    $insertinqMail['FK_CLIENT_ID'] = $clientId;
                    //print_r($clientDetail); exit;
                    $requestDataCust = array();
                    $requestDataCust['postData']['clientId'] = $clientId;
                    $requestDataCust['postData']['custId'] = $customerId1;
                    $requestDataCust['postData']['userId'] = $userId;
                    $requestDataCust['postData']['orgId'] = !empty($orgId) ? $orgId : '';
                    
                    $custDetail = getCustomerDetailByCustId($requestDataCust);
                    //print_r($custDetail); exit;
                    $creatCronMailSmsFlag = 0;
                    if($custDetail['status'] == SCS){
                        
                        if($actMode == "2"){ // SEND MAIL
                            $insertinqMail['TYPE_OF_MESSAGE'] = "MAIL";
                            for($d=0;$d<count($custDetail['data'][0]['CustomerInfo']['multiEmailData']);$d++){
                                if($custDetail['data'][0]['CustomerInfo']['multiEmailData'][$d]['defaultFlag'] == 1){
                                    $insertinqMail['RECIVER_EMAIL'] = trim($custDetail['data'][0]['CustomerInfo']['multiEmailData'][$d]['emailAddress']);
                                    $insertinqMail['RECIVER_MOBILE'] = "";
                                }
                            }
                            $creatCronMailSmsFlag = 1;
                        }else if($actMode == "3"){ // SEND SMS
                            $insertinqMail['TYPE_OF_MESSAGE'] = "SMS";
                            for($d=0;$d<count($custDetail['data'][0]['CustomerInfo']['multiPhoneData']);$d++){
                                if($custDetail['data'][0]['CustomerInfo']['multiPhoneData'][$d]['defaultFlag'] == 1){
                                    $insertinqMail['RECIVER_MOBILE'] = trim($custDetail['data'][0]['CustomerInfo']['multiPhoneData'][$d]['phone']);
                                    $insertinqMail['RECIVER_EMAIL']="";
                                }
                            }
                            $creatCronMailSmsFlag = 1;
                        }
                        
                        if($creatCronMailSmsFlag){
                            $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                            $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];
                            //$insertinqMail['RECIVER_EMAIL'] = $inqEventDetails[0]['inqCustEmail'];

                            $insertinqMail['RECIVER_NAME']  = $custDetail['data'][0]['CustomerInfo']['Name'];
                            $insertinqMail['MAIL_SUBJECT']  = $actTitle;
                            $insertinqMail['MAIL_CONTENT']  = $actDesc;
                            $insertinqMail['ATTACHMENT_LINK']= $attachmentUpload['data']['fileLink'];
                            $insertinqMail['SEND_STATUS']   = 0;
                            $insertinqMail['TYPE_OF_MAIL']  = "ACT";
                            $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($actDueDate));
                            $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                            $insertinqMail['CREATED_BY']    = $userId;
                            $insertinqMail['CREATED_BY_DATE'] = time();
                            $insertinqMail['DELETE_FLAG'] = 0;
                            //print_r($insertinqMail); exit;
                            // INSERT DATA INTO DATA BASE
                            $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                            /*if($insertActMail){
                                $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                                $insertinqMail['RECIVER_EMAIL'] = "";
                                $insertinqMail['RECIVER_MOBILE'] = "";
                                $insertinqMail['RECIVER_NAME']  = "";
                                $insertinqMail['MAIL_SUBJECT']  = "";
                                $insertinqMail['MAIL_CONTENT']  = "";
                                $insertinqMail['ATTACHMENT_LINK']= "";
                                $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                            }*/
                        }                        
                    }
                    
                    $requestData = array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['appName'] = "POTG";
                    $requestData['postData']['deviceType'] = "UBANTU";
                    $requestData['postData']['alertSubId'] = $assignedToId; 
                    $requestData['postData']['messageType'] = "ALERT";
                    $requestData['postData']['actionFlag'] = '0';
                    $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                    $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                    createAlertStaging($requestData);


                    //START - ADD NOTIFICATION TO CRONMAILMASTER FOR USER
                    //ADDED BY DARSHA PATEL ON - 29/12/2016
                    $insertinqMail['FK_CLIENT_ID'] = $clientId;
                    $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                    $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];  
                    $insertinqMail['RECIVER_EMAIL'] = "";
                    $insertinqMail['RECIVER_MOBILE'] = "";
                    $insertinqMail['RECIVER_NAME']  = "";
                    $insertinqMail['MAIL_SUBJECT']  = "";
                    $insertinqMail['MAIL_CONTENT']  = "";
                    $insertinqMail['ATTACHMENT_LINK']= "";
                    $insertinqMail['SEND_STATUS']   = 0;
                    $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                    $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($actDueDate));
                    $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                    $insertinqMail['CREATED_BY']    = $userId;
                    $insertinqMail['CREATED_BY_DATE'] = time();
                    $insertinqMail['DELETE_FLAG'] = 0;
                    //print_r($insertinqMail); exit;
                    // INSERT DATA INTO DATA BASE
                    $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                }
            } 
            else { //create Activity without Customer case
                $insertAct['ASSIGN_TO'] = $assignedToId;
                $insertAct['FK_CUST_ID'] = $custId; // IF ACTIVITY BIND WITH CUSTOMER THEN THIS FIELD WILL BE FILLED ELSE BLANK
                $insertAct['FK_CONTACT_ID'] = $contactId;
                $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                $insertAct['C_ACT_ID'] = $getActId;
                $insert_activity = insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                $actAllocationFun = insertInqAllocation($insert_activity['lastInsertedId'], $assignedToId, $clientId, "ACT", $userId);
                
                
                // START GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['actId'] = $insert_activity['lastInsertedId'];
                $createActDetail = activityCalenderData($requestData);

                $tempActDetail = ($createActDetail['status'] == SCS) ? $createActDetail['data'][0] :  $createActDetail['status'];
                array_push($status, $tempActDetail);
                    

                // END GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                
                //GOOGLE CAL CODE START
                if($isUserGroup == '0'){//FOR ONLY SIGNLE USER NOT FOR USER GROUP
                    $startTimeCal = $endTimeCal = array();
                    $tempStartTime = strtotime(date("d-m-Y", strtotime($actDueDate))." ".date("h:i A",strtotime($actDueTime)));
                    $startTimeCal[0]['dateTime'] = dateToCal($tempStartTime); 
                    $startTimeCal[0]['timeZone'] = "Asia/Kolkata"; 

                    $tempEndTime = strtotime(date("d-m-Y", strtotime($endDate))." ".date("h:i A",strtotime($endTime)));
                    $endTimeCal[0]['dateTime'] = dateToCal($tempEndTime); 
                    $endTimeCal[0]['timeZone'] = "Asia/Kolkata"; 
                    //print_r($startTime); exit;
                    $summary = $actTitle;
                    $location = "";
                    $description = $actDesc;

                    insertEventIntoCalander($summary,$location,$description,$startTimeCal,$endTimeCal,$assignedToId,$clientId);
    //                print_r($data);exit;
                }
                
                 //GOOGLE CAL CODE END
                
                $insertinqMail['FK_CLIENT_ID'] = $clientId;
                $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];  
                $insertinqMail['RECIVER_EMAIL'] = "";
                $insertinqMail['RECIVER_MOBILE'] = "";
                $insertinqMail['RECIVER_NAME']  = "";
                $insertinqMail['MAIL_SUBJECT']  = "";
                $insertinqMail['MAIL_CONTENT']  = "";
                $insertinqMail['ATTACHMENT_LINK']= "";
                $insertinqMail['SEND_STATUS']   = 0;
                $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($actDueDate));
                $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                $insertinqMail['CREATED_BY']    = $userId;
                $insertinqMail['CREATED_BY_DATE'] = time();
                $insertinqMail['DELETE_FLAG'] = 0;
                //print_r($insertinqMail); exit;
                // INSERT DATA INTO DATA BASE
                $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                
                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['appName'] = "POTG";
                $requestData['postData']['deviceType'] = "UBANTU";
                $requestData['postData']['alertSubId'] = $assignedToId; 
                $requestData['postData']['messageType'] = "ALERT";
                $requestData['postData']['actionFlag'] = '0';
                $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                createAlertStaging($requestData);
                
                // START INSERTING ATTACHMENT INTO ATTACHMENT MASTER AGAINST ACTIVITY ID
                //print_r($actAttachment); exit;

                if (!empty($actAttachment['name']) && empty($uploadFiles)) {
                    for($t=0;$t<count($actAttachment['name']);$t++){
                        $removeSpeChar = cleanString($actAttachment['name'][$t],$actAttachment);                       
                        if($actAttachment['error'][$t] == 0){
                            $requestData = $postFile = array();
                            $requestData['clientId'] = $clientId;
                            $requestData['userId'] = $userId;
                            $requestData['actId'] = $insert_activity['lastInsertedId'];
                            $requestData['orgId'] = "";

                            $postFile['name'] =$removeSpeChar;
                            $postFile['type'] =$actAttachment['type'][$t];
                            $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                            $postFile['error'] =$actAttachment['error'][$t];
                            $postFile['size'] =$actAttachment['size'][$t];
                            $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                            $uploadFiles[$t] = $attachmentUpload['data']['fileLink'];
                        }
                    }
                }else{
                    //echo "ada s a here";
                    //print_r($uploadFiles); 
                    $attachmentLink = array_values($uploadFiles);
                    for($s=0;$s<count($attachmentLink);$s++){
                        $insertAttchment = array();
                        $insertAttchment['ATTACHFILE_LINK'] = $attachmentLink[$s];
                        $insertAttchment['ATTACH_CAT'] = "ACT";
                        $insertAttchment['FK_INQ_ID'] = $insert_activity['lastInsertedId'];
                        $insertAttchment['CREATED_BY'] = $userId;
                        $insertAttchment['CREATED_BY_DATE'] = time();
                        $insertAttchment['DELETE_FLAG'] = 0;
                        $insetAttchRequest = insert_rec(ATTACHINQMASTER, $insertAttchment);
                    }
                }
            }
        }
        if (isset($insert_activity)) {
            
            //START - CODE FOR INQUIRY LOG REPORT ADD BY DARSHAN PATEL ON 30-01-2017
            if($logFlag != '1' && !empty($inqId) && $inqId != ''){
                $insertData = array();
                $insertData['postData']['clientId'] = $clientId;
                $insertData['postData']['userId'] = $userId;
                $insertData['postData']['orgId'] = $orgId;
                $insertData['postData']['inqId'] = $inqId;
                $insertData['postData']['logData'] = 'Activity Created No - '.$getActId;
                addEditLogReportInInquiry($insertData);
            }
            //END - CODE FOR INQUIRY LOG REPORT
            
            $result = array("status" => SCS,"data"=>$status);
            http_response_code(200);
        } else {
            $result = array("status" => ACTINSERTFAIL);
            http_response_code(400);
        }
    } else { // UPDATE ACTIVITY BY ACTIVITY ID
        $checkOwnerAct = checkOwnerAct($actId,$userId);
        if($checkOwnerAct['status'] == SCS){
            $insertAct['CHANGED_BY'] = $userId; //CHANGED BY USER ID
            $insertAct['CHANGED_BY_DATE'] = $time; //CHANGED BY USER ID
            $insertAct['LAST_SYNC_TIME'] = 1; //LAST SYNC TIME
            $whereCond = "PK_ACT_ID='" . $actId . "' AND DELETE_FLAG=0";
            $update_activity = update_rec(ACTIVITYMASTER_ACT, $insertAct, $whereCond);

            if (isset($update_activity) && $update_activity >= 0) {
                
                $result = array("status" => SCS,"data"=>$status);
                http_response_code(200);
            } else {
                $result = array("status" => ACTUPDATEFAIL);
                http_response_code(400);
            }
        }else{
            $result = array("status" => $checkOwnerAct['status']);
            http_response_code(400);
        }
    }
    return $result;
}

//START FUNCTION FOR GET ACTIVITY WITH WHERE CONDITION PARAMETER
function getActivityByWHereCondition($whereCond = "") {

    $todayDate = date("Y-m-d");
    $time = time();
    if ($whereCond != "") {
        $whereCond = "WHERE " . $whereCond." ORDER BY PK_ACT_ID DESC";
    }
    $getQuery = "SELECT AM.*,UM.FULLNAME ASSIGNEDTO,UM1.FULLNAME CREATEDBY,ALM.VALUE ACTMODE,IF(AM.FK_CUST_ID != 0 OR AM.FK_CUST_ID != '' ,(SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1),'--') CUTOMERDETAIL,IF(AM.FK_CONTACT_ID != 0 OR AM.FK_CONTACT_ID != '',(SELECT CONCAT_WS('|--|',CONCAT_WS(' ',FNAME,LNAME),CONTACT_NUMBER) FROM ".MULTICONTACTMASTER." WHERE PK_CONTACT_PERSON_ID = AM.FK_CONTACT_ID LIMIT 0,1),'') CONTACTPERSONDETAIL, IF(AM.FK_CUST_ID != 0 OR AM.FK_CUST_ID != '' ,(SELECT MPM.PHONE_NO FROM ".MULTIPHONEMASTER." MPM WHERE MPM.FK_CUST_ID = AM.FK_CUST_ID AND DEFAULT_FLAG = '1' AND DELETE_FLAG = 0 LIMIT 0,1),'--') PHONE_NO,IF(AM.ACT_START_TIME!=0,FROM_UNIXTIME(AM.ACT_START_TIME,'%d-%m-%Y %r'),'') STARTTIME,IF(AM.ACT_END_TIME!=0,FROM_UNIXTIME(AM.ACT_END_TIME,'%d-%m-%Y %r'),'') ENDTTIME,FROM_UNIXTIME(AM.CREATED_BY_DATE,'%d-%m-%Y %r') CREATEDTIME,IF(AM.CHANGED_BY_DATE!=0,FROM_UNIXTIME(AM.CHANGED_BY_DATE,'%d-%m-%Y %r'),'') CHANGEDTIME,(SELECT DISPLAY_VALUE FROM ".CLIENTLOOKUP." CL WHERE CL.PK_CLT_LKUP_ID = AM.ACT_TYPE ) ACT_TYPE_NAME,IF(AM.FK_CUST_ID != 0 , (SELECT IF(LAST_ACT_DATE != '0000-00-00',LAST_ACT_DATE,'--') FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1 ),'--') LASTACTIVTYDATE, UM.PROFILE_IMAGE ASSIGNTOIMAGE, UM1.PROFILE_IMAGE CRAATEDBYIMAGE,(SELECT CONCAT_WS('|--|',UMAT.FULLNAME,UMAT.PROFILE_IMAGE) FROM ".INQALLOCATIONMASTER." IAM INNER JOIN ".USERMASTER." UMAT ON UMAT.PK_USER_ID = IAM.FK_USER_ID WHERE IAM.ASSIGN_TYPE = 'ACT' AND IAM.FK_INQ_ID = AM.PK_ACT_ID ORDER BY PK_INQ_ASSIGN_ID DESC LIMIT 1,1) ASSIGNBYDETAIL , IF(AM.FK_INQ_ID !=0 AND ACT_REFERENCE_TYPE = 'INQ',(SELECT C_INQ_ID FROM ".INQMASTER." IM WHERE IM.PK_INQ_ID = AM.FK_INQ_ID),IF(AM.FK_INQ_ID !=0 AND ACT_REFERENCE_TYPE = 'SERVICE',(SELECT C_SERVICE_ID FROM ".SERVICEMASTER." SM WHERE SM.PK_SERVICE_ID = AM.FK_INQ_ID),IF(AM.FK_INQ_ID !=0 AND ACT_REFERENCE_TYPE = 'ORD',(SELECT BOOK_NO FROM ".INQMASTER." IM1 WHERE IM1.PK_INQ_ID = AM.FK_INQ_ID),IF(AM.FK_INQ_ID !=0 AND ACT_REFERENCE_TYPE = 'LEADINQ',(SELECT C_LEAD_ID FROM ".LEADINQMASTER." LM1 WHERE LM1.PK_LEAD_ID = AM.FK_INQ_ID),'0')))) C_INQ_ID,END_DATE ACT_END_DATE, END_TIME ACT_END_TIME,IF(AM.FK_PROJECT_ID != 0,(SELECT PROJECT_NAME FROM ".REALESTATEMASTER." WHERE PK_PROJECT_ID = AM.FK_PROJECT_ID AND DELETE_FLAG =0 ),'')PROJECTNAME,IF(APM.NAME != '',APM.NAME,'') PROJECT_TITLE,IF(CPM.CAMPAIGN_TITLE != '',CPM.CAMPAIGN_TITLE,'')CAMPAIGN_TITLE,IF(IM2.PROJECT_NAME != '',IM2.PROJECT_NAME,'') INQ_NAME,IF(IM2.FK_EXHIBITION_ID != '',IM2.FK_EXHIBITION_ID,'') ACT_PROJ_ID,IF(AM.ACT_REFERENCE_TYPE = 'SERVICE','Service',IF(AM.ACT_REFERENCE_TYPE = 'PROJECT','Task',IF(AM.ACT_REFERENCE_TYPE = 'CAMPAIGN','Campaign',IF(AM.ACT_REFERENCE_TYPE = 'LEADINQ','Lead','Activity'))))ACT_REF_TYPE_NAME FROM " . ACTIVITYMASTER_ACT . " AM LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID=AM.ASSIGN_TO INNER JOIN " . USERMASTER . " UM1 ON UM1.PK_USER_ID=AM.CREATED_BY INNER JOIN " . ACTIVITYLOOKUPMASTER . " ALM ON ALM.PK_ACT_LOOKUP_ID=AM.ACT_MODE LEFT JOIN ".ACTPROJECTMASTER." APM ON APM.PK_ACT_PROJ_ID = AM.FK_INQ_ID AND ACT_REFERENCE_TYPE = 'PROJECT' LEFT JOIN ".CAMPAIGNMASTER." CPM ON CPM.PK_CAMPAIGN_ID = AM.FK_INQ_ID AND ACT_REFERENCE_TYPE = 'CAMPAIGN' LEFT JOIN ".INQMASTER." IM2 ON IM2.PK_INQ_ID = AM.FK_INQ_ID AND IM2.DELETE_FLAG = '0' $whereCond";

    $runQuery = fetch_rec_query($getQuery);
//    print_r($runQuery); exit;
    if (count($runQuery) > 0) {
        foreach($runQuery as $keyAct => $valueAct){
            
            $explodeAssignByDetail = !empty($valueAct['ASSIGNBYDETAIL']) ? explode("|--|",$valueAct['ASSIGNBYDETAIL']) : "";
            //print_r($explodeAssignByDetail);
            if(!empty($explodeAssignByDetail)){
                $runQuery[$keyAct]['ASSIGNBY'] = $explodeAssignByDetail[0];
                $runQuery[$keyAct]['ASSIGNBYIMAGE'] = APP_WEB_ROOT."/attachment/".$explodeAssignByDetail[1];
            }else{
                $runQuery[$keyAct]['ASSIGNBY'] = "";
                $runQuery[$keyAct]['ASSIGNBYIMAGE'] = "";
            }
            
            //EDITED BY DARSHAN PATEL ON 4/5/2017
            $explodeContactPersondetail = !empty($valueAct['CONTACTPERSONDETAIL']) ? explode("|--|",$valueAct['CONTACTPERSONDETAIL']) : "--";
            if(!empty($explodeContactPersondetail)){
                $runQuery[$keyAct]['CONTACTPERNAME'] = $explodeContactPersondetail[0];
                $runQuery[$keyAct]['CONTACTPERNO'] = $explodeContactPersondetail[1];
            }else{
                $runQuery[$keyAct]['CONTACTPERNAME'] = "";
                $runQuery[$keyAct]['CONTACTPERNO'] = "";
            }
            
            $runQuery[$keyAct]['ASSIGNTOIMAGE'] = ($valueAct['ASSIGN_TYPE'] == 'GROUP') ? '' : APP_WEB_ROOT."/attachment/".$valueAct['ASSIGNTOIMAGE'];
            $runQuery[$keyAct]['CRAATEDBYIMAGE'] = APP_WEB_ROOT."/attachment/".$valueAct['CRAATEDBYIMAGE'];
            
            $runQuery[$keyAct]['ASSIGNEDTO'] = ($valueAct['ASSIGN_TYPE'] == 'GROUP') ? 'Group' : $valueAct['ASSIGNEDTO'];
            
            //GET LAST INSERTED REMARK
//            $actCommentData = getActCommentByactIdArrayFromJson($valueAct['PK_ACT_ID'],$clientId);
            $json = utf8_encode($valueAct['ACT_COMMENT']);
            $json_data = objectToArray(json_decode($json));
//            $actCommentData = remarkCreatedByChangedBy($json_data,$clientId);
//            print_r(end($json_Data));
            $dataRemark = '';
            if(!empty($json_data) && $json_data !="" ){
                $dataRemark = end($json_data);
            }
            $runQuery[$keyAct]['lastRemark'] = !empty($dataRemark) ? $dataRemark['remark'] : NORECORDS;
            
        }
        $result = array("status" => SCS, "data" => $runQuery);
        http_response_code(200);
    } else {
        $result = array("status" => NOACTIVITYFOUND);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR GET ACTIVITY WITH WHERE CONDITION PARAMETER
//START FUNCTION FOR GETTING LIST OF ACTIVITY
function getActListing($postData) {
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // org ID    
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['requestuserId'] = $userId;
    $getUserHierarchy = getUserHierarchy($requestData);
    //print_r($getUserHierarchy); exit;
    if($getUserHierarchy['status'] == SCS){
        $finalUserId = array();
//        for($f=0;$f<count($getUserHierarchy['data']);$f++){
//            if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                    if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                        $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                    }
//                }
//            }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//            }
//        }
        foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
            if($valueUH['lastLevelFlag'] == 'N'){
                //for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
                foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                    if($valueUD['lastLevelFlag'] == 'Y'){
                        $finalUserId[$keyUH] =  $valueUD['userId'];
                    }
                }
            }elseif($valueUH['lastLevelFlag'] == 'Y'){
                $finalUserId[$keyUH] = $valueUH['userId'];
            }
        }
        //print_r($finalUserId); exit;     
        $userIds = implode(",", $finalUserId);
        $where = " AND ASSIGN_TO IN(" . $userIds . ") ";
    }
    
    $getTodayAct = getActivityByWHereCondition("AM.DUE_DATE = '" . $todayDate . "' AND AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' ".$where." AND AM.ACT_STATUS=0 AND AM.DELETE_FLAG=0"); //GET TODAY ACTIVITY
    $getDelayedAct = getActivityByWHereCondition("AM.DUE_DATE < '" . $todayDate . "' AND AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' ".$where." AND AM.ACT_STATUS=0 AND AM.DELETE_FLAG=0 AND DELAYED_FLAG=1"); //GET DELAYED ACTIVITY
    $getUpcomingAct = getActivityByWHereCondition("AM.DUE_DATE > '" . $todayDate . "' AND AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' ".$where." AND AM.ACT_STATUS=0 AND AM.DELETE_FLAG=0"); //GET UP COMING ACTIVITY
    $getInProcessAct = getActivityByWHereCondition("AM.ACT_STATUS=1 AND AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' ".$where." AND AM.DELETE_FLAG=0");
    $todayAct = $delayedAct = $upccomingAct = $inProcessAct = $finalArr = array();

    if ($getTodayAct['status'] == SCS) {
        $todayAct = $getTodayAct['data'];
    }
    if ($getDelayedAct['status'] == SCS) {
        $delayedAct = $getDelayedAct['data'];
    }
    if ($getUpcomingAct['status'] == SCS) {
        $upccomingAct = $getUpcomingAct['data'];
    }
    if ($getInProcessAct['status'] == SCS) {
        $inProcessAct = $getInProcessAct['data'];
    }

    $finalArr['todayAct'] = $todayAct;
    $finalArr['delayedAct'] = $delayedAct;
    $finalArr['upccomingAct'] = $upccomingAct;
    $finalArr['inProcessAct'] = $inProcessAct;

    $result = array("status" => SCS, "data" => $finalArr);
    http_response_code(200);
    return $result;
}

//END FUNCTION FOR GETTING LIST OF ACTIVITY
//START FUNCTION FOR GET ACTIVITY DETAILS BY ACTIVITY ID
function getActivityDetails($postData) {

    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID    
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID    
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP   
    $getActDetails = getActivityByWHereCondition("AM.PK_ACT_ID = '" . $actId . "' AND AM.FK_CLIENT_ID='" . $clientId ."' AND AM.FK_ORG_ID = '".$orgId."' AND AM.DELETE_FLAG=0"); //GET ACTIVITY DETAILS BY ACTIVITY ID

    if ($getActDetails['status'] == SCS) {
        $actCommentData = getActCommentByactIdArrayFromJson($actId,$clientId);
//        print_r($actCommentData); exit;
        
        if(!empty($actCommentData) && $actCommentData!= ""){
            $lastArray = end($actCommentData);//GET LAST DATA IN ARRAY FOR EXPLODE 
            $lastRemark = $lastArray['remark'];//TAKE REMARK 
            if(strpos($lastRemark,"|---|")!== false){
                $tempData = implode("\n\r",explode("|---|", $lastRemark));
                $actCommentData[(count($actCommentData)-1)]['remark'] = $tempData;
            }
        }
        
        //print_r($actCommentData);exit;
        
        $finalArr['actData'] = $getActDetails['data'];
        $finalArr['remark'] = !empty($actCommentData) ? array_reverse($actCommentData) : "";
        
        // GET THE ALLOCATION THROUGH ACTIVITY ID
        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['orgId'] = $orgId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['actId'] = $actId;
        $requestData['postData']['type'] = "ACT";
        $allocationData = getActivityAssignmentReport($requestData);
        if($allocationData['status'] == SCS){
            $finalArr['allocationHistory'] = $allocationData['data'];
        }else{
            $finalArr['allocationHistory'] = $allocationData['status'];
        }
        $result = array("status" => SCS, "data" => $finalArr);
        http_response_code(200);
    } else {
        $result = array("status" => NOACTIVITYFOUND);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR GET ACTIVITY DETAILS BY ACTIVITY ID
//START FUNCTION FOR UPDATE ACTIVITY DETAILS BY ACTIVITY ID
function updateActivityDetailsByActId($actId, $updateActArr = array()) {
    $actId = $actId;
    $updateActArr = $updateActArr;
    if (count($updateActArr) > 0 && $actId != "") {
        $whereActId = "PK_ACT_ID='" . $actId . "'";
        $updateActivity = update_rec(ACTIVITYMASTER_ACT, $updateActArr, $whereActId);
        if ($updateActivity > 0) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => ACTUPDATEFAIL);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NOACTID);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR UPDATE ACTIVITY DETAILS BY ACTIVITY ID
//START FUNCTION FOR LISTING BY CREATED BY ACTIVITY
function getActivityListingByCreatedBy($postData) {
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID    
//    $typeOfActListing = isset($postdata['typeOfActListing']) ? addslashes(trim($postdata['typeOfActListing'])) : ""; // 0 : MY AND 1 : ALL
    $ownerType = isset($postdata['ownerType']) ? addslashes(trim($postdata['ownerType'])) : ""; // 1 : MY AND 2 : ALL AND 3 : CREATED BY
//    $createdBy = isset($postdata['createdBy']) ? addslashes(trim($postdata['createdBy'])) : ""; // 1 :  CREATED BY
    $actCategory = isset($postdata['actCategory']) ? addslashes(trim($postdata['actCategory'])) : ""; //1-TODAY -- 2-DELAYED -- 3-PLANNED -- 4-INPROCESS -- 5-COMPLETED  
    
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    $curreYear= date("Y");
    $getCurrentFiscalYear = getFiscalyear($curreYear);
    if(count($getCurrentFiscalYear) > 0){
        $whereFicalYear = "AND AM.DUE_DATE BETWEEN '".$getCurrentFiscalYear['STARTDATE']."' AND '".$getCurrentFiscalYear['ENDDATE']."' ";
    }
    //print_r($getCurrentFiscalYear); exit;
    
    
    if(!empty($ownerType) && $ownerType == '2'){ //ALL UNDER MY
        
        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if($getUserHierarchy['status'] == SCS){
            $finalUserId = array();
            foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
                if($valueUH['lastLevelFlag'] == 'N'){
                    //for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
                    foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                        if($valueUD['lastLevelFlag'] == 'Y'){
                            $finalUserId[$keyUH] =  $valueUD['userId'];
                        }
                    }
                }elseif($valueUH['lastLevelFlag'] == 'Y'){
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }
//            for($f=0;$f<count($getUserHierarchy['data']);$f++){
//                if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                    for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                        if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                            $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                        }
//                    }
//                }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                    $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//                }
//            }
            //print_r($finalUserId); exit;     
            $userIds = implode(",", $finalUserId);
            $sqlGetAllGroup = "SELECT GROUP_CONCAT(CONVERT(FK_GROUP_ID,CHAR(20))) GROUPLIST FROM ".GROUPCUSTTRNAS." GCT WHERE GCT.FK_CUST_ID IN (".$userIds.") LIMIT 0,1 ";
            $resultGetAllGroup = fetch_rec_query($sqlGetAllGroup);
            if(!empty($resultGetAllGroup[0]['GROUPLIST'])){
                $where = " AND (AM.ASSIGN_TO IN (" . $userIds . ") OR AM.CREATED_BY IN (".$userId.") OR (AM.ASSIGN_TO IN (" . $resultGetAllGroup[0]['GROUPLIST'] . ") AND AM.ASSIGN_TYPE = 'GROUP'))";
            }else{
                $where = " AND (AM.ASSIGN_TO IN (" . $userIds . ") OR AM.CREATED_BY IN (".$userId."))";
            }
            //$where = " AND AM.ASSIGN_TO IN (" . $userIds . ") OR AM.CREATED_BY IN (".$userId.") ";
        }
        elseif($userId == '-1'){
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
            $where = " AND (AM.ASSIGN_TO IN (" . $userIds . ") OR AM.CREATED_BY IN (".$userIds."))";
        }
    }
    else if(!empty($ownerType) && $ownerType == '1'){//ONLY ASSIGN TO ME
        
        $sqlGetAllGroup = "SELECT GROUP_CONCAT(CONVERT(FK_GROUP_ID,CHAR(20))) GROUPLIST FROM ".GROUPCUSTTRNAS." GCT WHERE GCT.FK_CUST_ID IN (".$userId.") LIMIT 0,1 ";
        $resultGetAllGroup = fetch_rec_query($sqlGetAllGroup);
        if(!empty($resultGetAllGroup[0]['GROUPLIST'])){
            $where = " AND (AM.ASSIGN_TO IN (" . $userId . ")  OR (AM.ASSIGN_TO IN (" . $resultGetAllGroup[0]['GROUPLIST'] . ") AND AM.ASSIGN_TYPE = 'GROUP')) ";
        }else{
            $where = " AND (AM.ASSIGN_TO IN (" . $userId . ") ) ";
        }
        
    }
    else{//ONLY CREATED
        $where = " AND (AM.CREATED_BY IN (".$userId.") AND (AM.ASSIGN_TO NOT IN (" . $userId . "))) ";
    }
    
    
    //FOR CHECK ACTIVITY IN WHICH STATUS
    $whereDate = "";
    $limit = "";
    if($actCategory == 1){ // TODAY
        $whereDate = " AND AM.DELAYED_FLAG = '0' AND DUE_DATE = '".DATE("Y-m-d")."' AND AM.ACT_STATUS = '0' ";
    }else if($actCategory == 2){ // DELAYED
        $whereDate = " AND AM.DELAYED_FLAG = '1' AND AM.ACT_STATUS = '0' ";
    }else if($actCategory == 3){ // PLANNED
        $whereDate = " AND AM.DELAYED_FLAG = '0' AND DUE_DATE > '".DATE("Y-m-d")."' AND AM.ACT_STATUS = '0' ";
    }else if($actCategory == 4){ // INPROCESS
        $whereDate = "AND AM.ACT_STATUS = '1' ";
    }else if($actCategory == 5){ // COMPELETED
        $whereDate = " AND AM.ACT_STATUS = '2'";
        $limit = " LIMIT 0,100";
    }
    
    
    $sqlTotalCount = "SELECT COUNT(*) ACTCOUNT, AM.ACT_STATUS FROM ".ACTIVITYMASTER_ACT." AM WHERE AM.FK_CLIENT_ID = '".$clientId."' AND AM.FK_ORG_ID = '".$orgId."' AND AM.DELETE_FLAG = '0' ".$where." GROUP BY AM.ACT_STATUS ORDER BY AM.ACT_STATUS ";
    $resultTotalCount = fetch_rec_query($sqlTotalCount);
    //print_r($resultTotalCount); exit;
    if(count($resultTotalCount) > 0){

        $sqlGetActCount = "SELECT COUNT(*) TOTALPENDINGACT FROM ".ACTIVITYMASTER_ACT." AM WHERE AM.DELETE_FLAG = '0' AND AM.FK_CLIENT_ID = '".$clientId."' AND AM.FK_ORG_ID = '".$orgId."' AND AM.ACT_STATUS = '0' AND AM.DUE_DATE = '".DATE('Y-m-d')."' AND AM.DELAYED_FLAG = '0' ".$where." GROUP BY AM.ACT_STATUS ORDER BY AM.ACT_STATUS ";
        $resultGetActCount = fetch_rec_query($sqlGetActCount);

        $sqlGetActCountDelay = "SELECT COUNT(*) TOTALDELAYED FROM ".ACTIVITYMASTER_ACT." AM WHERE AM.DELETE_FLAG = '0' AND AM.FK_CLIENT_ID = '".$clientId."' AND AM.FK_ORG_ID = '".$orgId."' AND AM.FK_ORG_ID = '".$orgId."' AND AM.ACT_STATUS = '0' AND AM.DELAYED_FLAG = '1'  ".$where." GROUP BY AM.ACT_STATUS ORDER BY AM.ACT_STATUS ";
        $resultGetActCountDelay = fetch_rec_query($sqlGetActCountDelay);

        $sqlGetActCountPlanned = "SELECT COUNT(*) TOTALPLANNED FROM ".ACTIVITYMASTER_ACT." AM WHERE AM.DELETE_FLAG = '0' AND AM.FK_CLIENT_ID = '".$clientId."' AND AM.FK_ORG_ID = '".$orgId."' AND AM.ACT_STATUS = '0' AND AM.DUE_DATE > '".DATE('Y-m-d')."' AND AM.DELAYED_FLAG = '0' ".$where." GROUP BY AM.ACT_STATUS ORDER BY AM.ACT_STATUS ";
        $resultGetActCountPlanned = fetch_rec_query($sqlGetActCountPlanned);

        $finalArr['count']['todays'] = !empty($resultGetActCount[0]['TOTALPENDINGACT']) ? $resultGetActCount[0]['TOTALPENDINGACT'] : 0;
        $finalArr['count']['delayed'] = !empty($resultGetActCountDelay[0]['TOTALDELAYED']) ? $resultGetActCountDelay[0]['TOTALDELAYED'] : 0;
        $finalArr['count']['planned'] = !empty($resultGetActCountPlanned[0]['TOTALPLANNED']) ? $resultGetActCountPlanned[0]['TOTALPLANNED'] : 0;
        $finalArr['count']['inprocess'] = 0;
        $finalArr['count']['completed'] = 0;
        foreach($resultTotalCount as $keyCount => $valueCount){
            
            if($valueCount['ACT_STATUS'] == 1){
                $finalArr['count']['inprocess'] = $valueCount['ACTCOUNT'];
            }
            
            if($valueCount['ACT_STATUS'] == 2){
                $finalArr['count']['completed'] = $valueCount['ACTCOUNT'];
            }
        }
    }else{
        $finalArr['count']['todays'] = 0;
        $finalArr['count']['delayed'] = 0;
        $finalArr['count']['planned'] = 0;
        $finalArr['count']['inprocess'] = 0;
        $finalArr['count']['completed'] = 0;
    }
    
    //$whereCond = "WHERE AM.FK_CLIENT_ID='" . $clientId . "' AND AM.DELETE_FLAG=0 ".$where." ".$whereFicalYear." ORDER BY PK_ACT_ID DESC";
    
    //ADDED BY DARSHAN PATEL BUG REF : 4728
//    $whereCond = "WHERE AM.FK_CLIENT_ID='" . $clientId . "' AND AM.DELETE_FLAG=0 ".$where." ".$whereDate." ORDER BY PK_ACT_ID DESC $limit ";
    $whereCond = "WHERE  AM.DELETE_FLAG = '0' AND AM.FK_CLIENT_ID = '".$clientId."' AND AM.FK_ORG_ID = '".$orgId."' ".$where." ".$whereDate." ORDER BY PK_ACT_ID DESC $limit ";
    
//    $getQuery = "SELECT AM.ACT_COMMENT,AM.ACT_DESC,AM.ACT_END_TIME,AM.ACT_MODE,AM.ACT_STATUS,AM.ACT_TITLE,AM.ASSIGN_TO,AM.ASSIGN_TYPE,AM.C_ACT_ID,AM.PK_ACT_ID,AM.DUE_DATE,AM.DUE_TIME,AM.END_DATE,AM.END_TIME,AM.FK_CUST_ID,AM.REMINDER_ON,AM.ACT_REFERENCE_TYPE,IF(AM.ACT_REFERENCE_TYPE = 'INQ','Inquiry',IF(AM.ACT_REFERENCE_TYPE = 'ORD','Order',IF(AM.ACT_REFERENCE_TYPE = 'SERVICE','Service',IF(AM.ACT_REFERENCE_TYPE = 'PROJECT','Task',IF(AM.ACT_REFERENCE_TYPE = 'CAMPAIGN','Campaign',IF(AM.ACT_REFERENCE_TYPE = 'LEADINQ','Lead','Activity'))))))ACT_REF_TYPE_NAME,UM.FULLNAME ASSIGNEDTO,UM1.FULLNAME CREATEDBY,ALM.VALUE ACTMODE,IF(AM.FK_CUST_ID != 0 OR AM.FK_CUST_ID != '' ,(SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1),'--') CUTOMERDETAIL,IF(AM.FK_CUST_ID != 0 OR AM.FK_CUST_ID != '',(SELECT CONCAT_WS('|--|',CONTACT_NAME,CONTACT_NUMBER) FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1),'') CONTACTPERSONDETAIL, IF(FK_CUST_ID != 0 OR FK_CUST_ID != '' ,(SELECT IF(PHONE_NO != '',PHONE_NO,'') FROM ".MULTIPHONEMASTER." WHERE FK_CUST_ID = AM.FK_CUST_ID AND DEFAULT_FLAG = '1' AND DELETE_FLAG = 0 LIMIT 0,1),'--') PHONE_NO,IF(AM.ACT_START_TIME!=0,FROM_UNIXTIME(AM.ACT_START_TIME,'%d-%m-%Y %r'),'') STARTTIME,IF(AM.ACT_END_TIME!=0,FROM_UNIXTIME(AM.ACT_END_TIME,'%d-%m-%Y %r'),'') ENDTTIME,FROM_UNIXTIME(AM.CREATED_BY_DATE,'%d-%m-%Y %r') CREATEDTIME,(SELECT DISPLAY_VALUE FROM ".CLIENTLOOKUP." CL WHERE CL.PK_CLT_LKUP_ID = AM.ACT_TYPE ) ACT_TYPE_NAME,IF(ACT_STATUS = 0,'Pending',IF(ACT_STATUS = 1,'In Process','Close')) ACT_STATUS_NAME,IF(AM.FK_CUST_ID != 0 , (SELECT IF(LAST_ACT_DATE != '0000-00-00', DATE_FORMAT(LAST_ACT_DATE,'%d-%m-%Y'),'--') FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1 ),'--') LASTACTIVTYDATE , UM.PROFILE_IMAGE ASSIGNTOIMAGE  FROM " . ACTIVITYMASTER_ACT . " AM LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID=AM.ASSIGN_TO INNER JOIN " . USERMASTER . " UM1 ON UM1.PK_USER_ID=AM.CREATED_BY INNER JOIN " . ACTIVITYLOOKUPMASTER . " ALM ON ALM.PK_ACT_LOOKUP_ID=AM.ACT_MODE  $whereCond";
    
    $getQuery = "SELECT AM.ACT_COMMENT,AM.ACT_DESC,AM.ACT_END_TIME,AM.ACT_MODE,AM.ACT_STATUS,AM.ACT_TITLE,AM.ASSIGN_TO,AM.ASSIGN_TYPE,AM.C_ACT_ID,AM.PK_ACT_ID,AM.DUE_DATE,AM.DUE_TIME,AM.END_DATE,AM.END_TIME,AM.FK_CUST_ID,AM.REMINDER_ON,AM.ACT_REFERENCE_TYPE,IF(AM.ACT_REFERENCE_TYPE = 'INQ','Inquiry',IF(AM.ACT_REFERENCE_TYPE = 'ORD','Order',IF(AM.ACT_REFERENCE_TYPE = 'SERVICE','Service',IF(AM.ACT_REFERENCE_TYPE = 'PROJECT','Task',IF(AM.ACT_REFERENCE_TYPE = 'CAMPAIGN','Campaign',IF(AM.ACT_REFERENCE_TYPE = 'LEADINQ','Lead','Activity'))))))ACT_REF_TYPE_NAME,UM.FULLNAME ASSIGNEDTO,UM1.FULLNAME CREATEDBY,ALM.VALUE ACTMODE,IF(AM.FK_CUST_ID != 0 OR AM.FK_CUST_ID != '' ,(SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1),'--') CUTOMERDETAIL,IF(AM.FK_CONTACT_ID != 0 OR AM.FK_CONTACT_ID != '',(SELECT CONCAT_WS('|--|',CONCAT_WS(' ',FNAME,LNAME),CONTACT_NUMBER) FROM ".MULTICONTACTMASTER." WHERE PK_CONTACT_PERSON_ID = AM.FK_CONTACT_ID LIMIT 0,1),'') CONTACTPERSONDETAIL, IF(FK_CUST_ID != 0 OR FK_CUST_ID != '' ,(SELECT IF(PHONE_NO != '',PHONE_NO,'') FROM ".MULTIPHONEMASTER." WHERE FK_CUST_ID = AM.FK_CUST_ID AND DEFAULT_FLAG = '1' AND DELETE_FLAG = 0 LIMIT 0,1),'--') PHONE_NO,IF(AM.ACT_START_TIME!=0,FROM_UNIXTIME(AM.ACT_START_TIME,'%d-%m-%Y %r'),'') STARTTIME,IF(AM.ACT_END_TIME!=0,FROM_UNIXTIME(AM.ACT_END_TIME,'%d-%m-%Y %r'),'') ENDTTIME,FROM_UNIXTIME(AM.CREATED_BY_DATE,'%d-%m-%Y %r') CREATEDTIME,(SELECT DISPLAY_VALUE FROM ".CLIENTLOOKUP." CL WHERE CL.PK_CLT_LKUP_ID = AM.ACT_TYPE ) ACT_TYPE_NAME,IF(ACT_STATUS = 0,'Pending',IF(ACT_STATUS = 1,'In Process','Close')) ACT_STATUS_NAME,IF(AM.FK_CUST_ID != 0 , (SELECT IF(LAST_ACT_DATE != '0000-00-00', DATE_FORMAT(LAST_ACT_DATE,'%d-%m-%Y'),'--') FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1 ),'--') LASTACTIVTYDATE , UM.PROFILE_IMAGE ASSIGNTOIMAGE  FROM " . ACTIVITYMASTER_ACT . " AM LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID=AM.ASSIGN_TO INNER JOIN " . USERMASTER . " UM1 ON UM1.PK_USER_ID=AM.CREATED_BY INNER JOIN " . ACTIVITYLOOKUPMASTER . " ALM ON ALM.PK_ACT_LOOKUP_ID=AM.ACT_MODE  $whereCond";

    $runQuery = fetch_rec_query($getQuery);
//    print_r($runQuery);exit;
    if (count($runQuery) > 0) {
        
//        $finalArr['actData'] =$getActDetails['data'];
        $finalJson = array();
       
        foreach($runQuery as $keyAct => $valueAct){
            //print_r($valueAct); exit;
            if($valueAct['ASSIGN_TYPE'] == "GROUP"){ // CHECK CURRENT USER IS IN THIS GROUP
                $sqlCheckCurrUserGroup = "SELECT GCT.FK_CUST_ID,UM.FULLNAME FROM ".GROUPCUSTTRNAS." GCT INNER JOIN ".USERMASTER." UM ON UM.PK_USER_ID = GCT.FK_CUST_ID  WHERE GCT. FK_GROUP_ID IN (".$valueAct['ASSIGN_TO'].") AND GCT.FK_CUST_ID IN (".$userId.") LIMIT 0,1 ";
                $resultCheckCurrUserGroup = fetch_rec_query($sqlCheckCurrUserGroup);
                if(count($resultCheckCurrUserGroup) > 0){
                    $valueAct['ASSIGN_TO'] = $resultCheckCurrUserGroup[0]['FK_CUST_ID'];
                    $valueAct['ASSIGNEDTO'] = $resultCheckCurrUserGroup[0]['FULLNAME'];
                }
            }
            
            $valueAct['CUTOMERDETAIL'] = !empty($valueAct['CUTOMERDETAIL']) ? $valueAct['CUTOMERDETAIL'] : "--";
            
            //ADDED BY DARSHAN PATEL ON 12/01/2017
            $tempTitle = explode("|---|",$valueAct['ACT_TITLE']);
            //print_r($tempTitle); exit;
            $valueAct['ACT_TITLE'] = $tempTitle[0];
            if(!empty($tempTitle[1])){
                $valueAct['CUTOMERDETAIL'] = !empty($tempTitle[1]) ? $tempTitle[1] : "";
            }
            if(!empty($tempTitle[2])){
                $valueAct['PHONE_NO'] = !empty($tempTitle[2]) ? $tempTitle[2] : "--";
            }
            
            //EDITED BY DARSHAN PATEL ON 4/5/2017
            $explodeContactPersondetail = !empty($valueAct['CONTACTPERSONDETAIL']) ? explode("|--|",$valueAct['CONTACTPERSONDETAIL']) : "";
            if(!empty($explodeContactPersondetail)){
                $valueAct['CONTACTPERNAME'] = $explodeContactPersondetail[0];
                $valueAct['CONTACTPERNO'] = $explodeContactPersondetail[1];
            }else{
                $valueAct['CONTACTPERNAME'] = "";
                $valueAct['CONTACTPERNO'] = "";
            }
            
            $valueAct['LASTACTIVTYDATE'] = (!empty($valueAct['LASTACTIVTYDATE']) && $valueAct['LASTACTIVTYDATE'] != '--') ? date("Y-m-d",strtotime($valueAct['LASTACTIVTYDATE'])) : "--";
            
            $valueAct['ASSIGNTOIMAGE'] = !empty($valueAct['ASSIGNTOIMAGE']) ? APP_WEB_ROOT."/attachment/".$valueAct['ASSIGNTOIMAGE'] : "";
            
            //GET LAST INSERTED REMARK
//            $actCommentData = getActCommentByactIdArrayFromJson($valueAct['PK_ACT_ID'],$clientId);
            $json = utf8_encode($valueAct['ACT_COMMENT']);
            $json_data = objectToArray(json_decode($json));
//            $actCommentData = remarkCreatedByChangedBy($json_data,$clientId);
//            print_r(end($json_Data));
            $dataRemark = '';
            if(!empty($json_data) && $json_data !="" ){
                $dataRemark = end($json_data);
            }
            $valueAct['lastRemark'] = !empty($dataRemark) ? $dataRemark['remark'] : NORECORDS;
            
            $finalJson []= $valueAct;
        }
        $finalArr['actData'] = $finalJson;

        $result = array("status" => SCS, "data" => $finalArr);
        http_response_code(200);
    } else {
        $result = array("status" => NOACTIVITYFOUND, "data"=>$finalArr);  
        http_response_code(200);
    }
    //$getActDetails = getActivityByWHereCondition("AM.FK_CLIENT_ID = '".$clientId."' AND AM.DELETE_FLAG=0 ".$where);
//    if ($getActDetails['status'] == SCS) {
//
//        $finalArr['actData'] = $getActDetails['data'];
//
//        $result = array("status" => SCS, "data" => $finalArr);
//    } else {
//        $result = array("status" => NOACTIVITYFOUND);        
//    }
    return $result;
}

//START FUNCTION FOR INSERT ACTIVITY REMARK BY ACTIVITY ID
function insertCommentInAct($postData) {
    $postdata = $postData['postData'];
//    print_r($postdata); exit;
    $requestCase = isset($postdata['requestCase']) ? addslashes(trim($postdata['requestCase'])) : ""; // RequestCase
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID    
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID    
    $remark = isset($postdata['remark']) ? addslashes(trim($postdata['remark'])) : ""; // REMARK  
    $closeActReasonId = isset($postdata['closeActReasonId']) ? addslashes(trim($postdata['closeActReasonId'])) : "";
    $closeActReasonText = isset($postdata['closeActReasonText']) ? addslashes(trim($postdata['closeActReasonText'])) : "";
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP 
    
    $checkOwnerAct = checkOwnerAct($actId,$userId);
    if($checkOwnerAct['status'] == SCS){
        $flag = false;
        if($postdata['requestCase'] == "transferActivity" || $postdata['requestCase'] == "startActivity" || $postdata['requestCase'] == "closeActivity" || $postdata['requestCase'] == "closeAndCreateNewActivity" || $postdata['requestCase'] == "assignActivity"){
            $flag = true;
            $tmpArr['editFlag'] = "NO"; // 
        }else{
            $sqlCheckStartedAct = "SELECT PK_ACT_ID FROM ".ACTIVITYMASTER_ACT." ACT WHERE ACT.ACT_START_TIME != '' AND PK_ACT_ID = '".$actId."'   AND DELETE_FLAG = 0 LIMIT 0,1";
            $resultCheck = fetch_rec_query($sqlCheckStartedAct);

            if(count($resultCheck)>0){
                $flag = true;
                if(isset($postdata['editFlag'])){
                    $tmpArr['editFlag'] = $postdata['editFlag'];
                }else{
                    $tmpArr['editFlag'] = "YES";
                }
                
            }

            $sqlCheckStartedAct = "SELECT PK_ACT_ID FROM ".ACTIVITYMASTER_ACT." ACT WHERE ACT.ACT_START_TIME != '' AND ACT.ACT_END_TIME = '' AND PK_ACT_ID = '".$actId."' AND DELETE_FLAG = 0 LIMIT 0,1";
            $resultCheck = fetch_rec_query($sqlCheckStartedAct);
            
            if(count($resultCheck) > 0){
                $flag = true;
                if(isset($postdata['editFlag'])){
                    $tmpArr['editFlag'] = $postdata['editFlag'];
                }else{
                    $tmpArr['editFlag'] = "YES";
                }
            }else{
                $flag = false;
            }

        }

        //print_r($resultCheck); exit;
        if($flag){
            $actCommentData = getActCommentByactIdArrayFromJson($actId,$clientId);
            $tmpArr['remark'] = htmlspecialchars(strip_slash($remark),ENT_QUOTES);
            $tmpArr['createdBy'] = $userId;
            $tmpArr['createdByDate'] = $time;
            $tmpArr['changedBy'] = 0;
            $tmpArr['changedByDate'] = '';
            $actCommentData[] = $tmpArr;
            //print_r($actCommentData); exit;

            $updateComment['ACT_COMMENT'] = json_encode($actCommentData);
            $whereCond = "PK_ACT_ID='" . $actId . "'";
//            print_r($updateComment); die;
            $update_comment1 = update_rec(ACTIVITYMASTER_ACT, $updateComment, $whereCond);
//            echo $update_comment1; die;
            
            if($requestCase == 'closeActivity' || $requestCase == 'closeAndCreateNewActivity'){
                $tmpArr['remark'] = $closeActReasonId . "|---|" . $closeActReasonText;
                $tmpArr['createdBy'] = $userId;
                $tmpArr['createdByDate'] = $time;
                $tmpArr['changedBy'] = 0;
                $tmpArr['changedByDate'] = '';
                $actCommentData[] = $tmpArr;

                $updateComment['ACT_COMMENT'] = json_encode($actCommentData);
                $whereCond = "PK_ACT_ID='" . $actId . "'";
                //print_r($updateComment); die;
                $update_comment2 = update_rec(ACTIVITYMASTER_ACT, $updateComment, $whereCond);
            }
            
            if ($update_comment1 >= 0) {
                
               $getActivtyListing = getActivityByWHereCondition("AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' AND AM.DELETE_FLAG=0 AND PK_ACT_ID = '".$actId."' ");
            
                $result = array("status" => SCS,"data"=>($getActivtyListing['status'] == SCS) ? $getActivtyListing['data'] : $getActivtyListing['status']);
                http_response_code(200);
//                $result = array("status" => SCS,"data"=>$valueAct);
            } else {
                $result = array("status" => INSERTREMARKFAIL);
                http_response_code(400);
            }
        }else{
            $result = array("status" => NOALLOWEDCOMMENTONNONSTRATACT);
            http_response_code(400);
        }
    }else{
        $result = array("status" => $checkOwnerAct['status']);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR INSERT ACTIVITY REMARK BY ACTIVITY ID
//START FUNCTION FOR INSERT ACTIVITY REMARK BY ACTIVITY ID
function updateCommentInAct($postData) {
    
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID    
    $remark = isset($postdata['remark']) ? addslashes(trim($postdata['remark'])) : ""; // REMARK    
    $remarkIndexId = isset($postdata['remarkIndexId']) ? addslashes(trim($postdata['remarkIndexId'])) : ""; // REMARK    
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP 

    $actCommentData = getActCommentByactIdArrayFromJson($actId,$clientId);
    //print_r($actCommentData);
    $actCommentData[$remarkIndexId]['remark'] = $remark;
    $actCommentData[$remarkIndexId]['createdBy'] = $userId;
    $actCommentData[$remarkIndexId]['createdByDate'] = $time;
    $actCommentData[$remarkIndexId]['changedBy'] = 0;
    $actCommentData[$remarkIndexId]['changedByDate'] = '';
    //print_r($actCommentData);

    $updateComment['ACT_COMMENT'] = json_encode($actCommentData);
    $update_comment = update_rec(ACTIVITYMASTER_ACT, $updateComment, "PK_ACT_ID='" . $actId . "'");

    if ($update_comment) {
        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTREMARKFAIL);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR INSERT ACTIVITY REMARK BY ACTIVITY ID
//START FUNCTION FOR START ACTIVITY
function startActivity($postData) {
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID    
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID    
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP    
    $remark = "Activity Started";
    if (isset($postdata['remark'])) {
        $remark = isset($postdata['remark']) ? addslashes(trim($postdata['remark'])) : ""; // REMARK
    }
    $postData['postData']['remark'] = $remark;
    //print_r($postData); die;
    
    $checkOwnerAct = checkOwnerAct($actId,$userId);
    if($checkOwnerAct['status'] == SCS){
    
        $sqlCheckAct = "SELECT PK_ACT_ID FROM ".ACTIVITYMASTER_ACT." WHERE PK_ACT_ID = '".$actId."' AND ACT_STATUS IN (1,2) AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$clientId."' LIMIT 0,1 ";
        $resultCheckAct = fetch_rec_query($sqlCheckAct);
        
        if(count($resultCheckAct)==0){
            $insertComment = insertCommentInAct($postData);

            if ($insertComment['status'] == SCS) {

                $allocationCheck = "SELECT * FROM ".INQALLOCATIONMASTER." WHERE FK_INQ_ID = '".$actId."' AND ASSIGN_TYPE = 'ACT' ";
                $resultAllocation = fetch_rec_query($allocationCheck);
    //            print_r($resultAllocation);exit;
                if(count($resultAllocation) == 0){
                    insertInqAllocation($actId, $userId, $clientId, "ACT", $userId);
                }

                $updateActStatus['ACT_STATUS'] = 1;
                $updateActStatus['ACT_START_TIME'] = $time;
                $updateActStatus['ASSIGN_TO'] = $userId;
                $updateActStatus['ASSIGN_TYPE'] = "NORMAL";
                $updateActStatus['CHANGED_BY'] = $userId;
                $updateActStatus['CHANGED_BY_DATE'] = $time;
                $whereCond = "PK_ACT_ID='" . $actId . "'";
                $update_status = update_rec(ACTIVITYMASTER_ACT, $updateActStatus, $whereCond);
                // comment made by deepak patil on 18-05-2016
                //print_r($update_status);
                //if ($update_status) {
                $getActivtyListing = getActivityByWHereCondition("AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' AND AM.DELETE_FLAG=0 AND PK_ACT_ID = '".$actId."' ");

                $result = array("status" => SCS,"data"=>($getActivtyListing['status'] == SCS) ? $getActivtyListing['data'] : $getActivtyListing['status']);
                http_response_code(200);
            } else {
                $result = array("status" => $insertComment['status']);
                http_response_code(400);
            }
        }else{
            $result = array("status" => ACTSTARTERROR);
            http_response_code(400);
        }
    }else{
        $result = array("status" => $checkOwnerAct['status']);
        http_response_code(400);
    }
    return $result;
}

//START FUNCTION FOR CLOSE ACTIVITY
function closeActivity($postData) {
    $postdata = $postData['postData'];
//    print_r($postdata);exit;
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID    
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID    
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP    
    $remark = "Activity Closed";
    $closeActReasonId = isset($postdata['closeActReasonId']) ? addslashes(trim($postdata['closeActReasonId'])) : "";
    $closeActReasonText = isset($postdata['closeActReasonText']) ? addslashes(trim($postdata['closeActReasonText'])) : "";
    if (isset($postdata['remark'])) {
        $remark = isset($postdata['remark']) ? addslashes(trim($postdata['remark'])) : ""; // REMARK
    }
    
   
    $sqlCheckAct = "SELECT PK_ACT_ID FROM ".ACTIVITYMASTER_ACT." WHERE PK_ACT_ID = '".$actId."' AND ACT_STATUS = '2' AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$clientId."' LIMIT 0,1 ";
    $resultCheckAct = fetch_rec_query($sqlCheckAct);
    
    if(count($resultCheckAct) == '0'){
    
        //GET LOOKUP NAME FROM LOOKUP ID
        if(!empty($closeActReasonId) && $closeActReasonId != ""){
            $sqlActReasonId = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientId . "' AND PK_CLT_LKUP_ID = '".$closeActReasonId."'  AND DELETE_FLAG = '0' LIMIT 0,1 ";
            $resultActReasonId = fetch_rec_query($sqlActReasonId);
            
        }else{//FOR GETTING DEFAULT VALUE
            
            $sqlActReasonId = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientId . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM ".CLIENTLOOKUP." CLT1 WHERE CLT1.LOOK_TYPE = 'ACTIVITY_CLOSE_REASON' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientId . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG = '1' ";
            $resultActReasonId = fetch_rec_query($sqlActReasonId);
        }

        $checkOwnerAct = checkOwnerAct($actId,$userId);
//        print_r($checkOwnerAct); exit;
        if($checkOwnerAct['status'] == SCS){
            //$postData['remark'] = $remark;
            $postData['postData']['remark'] = $remark;
            $postData['postData']['closeActReasonId'] = $resultActReasonId[0]['DISPLAY_VALUE'];
            $postData['postData']['closeActReasonText'] = $closeActReasonText;
//            print_r($postData);exit;
            $insertComment = insertCommentInAct($postData);
            //print_r($insertComment); die;
    //        $insertComment['status'] = 'Success';
            if ($insertComment['status'] == SCS) {
                
                $updateActStatus['ACT_STATUS'] = 2; // 0=PENDING,1=IN-PROCESS,2=CLOSED
                $updateActStatus['ACT_END_TIME'] = $time; //ACTIVITY END TIME
                $updateActStatus['CHANGED_BY'] = $userId;
                $updateActStatus['CHANGED_BY_DATE'] = $time;
                
                $whereCond = "PK_ACT_ID='" . $actId . "'";
                $update_status = update_rec(ACTIVITYMASTER_ACT, $updateActStatus, $whereCond);
                
                if ($update_status >= 0) {
                    
                    //FOR UPDATE LAST ACT DATE ON CUSTOMER
                    updateLastActDateOnCustomer($actId);
                    
                    $getActivtyListing = getActivityByWHereCondition("AM.FK_CLIENT_ID='" . $clientId . "' AND AM.FK_ORG_ID = '".$orgId."' AND AM.DELETE_FLAG=0 AND PK_ACT_ID = '".$actId."' ");
                    $result = array("status" => SCS,"data"=>($getActivtyListing['status'] == SCS) ? $getActivtyListing['data'] : $getActivtyListing['status']);
                    
                    //$result = array("status" => SCS);

                    //CODE ADDED BY DARSHAN PATEL ON 03/10/2017
                    $sqlSms = "SELECT MS.MAILCONFIG_USERNAME,MS.MAILCONFIG_PASSWORD,CM.CLIENT_EMAIL,CM.CLIENTNAME,MS.SMS_SID,MS.SMS_API_URL,MS.SMS_USERNAME,MS.SMS_PASSWORD,CM.CLIENT_WEBADDRESS FROM ".MAILSMSCONFIG." MS INNER JOIN ".CLIENTMASTER." CM ON CM.PK_CLIENT_ID = MS.FK_CLIENT_ID WHERE MS.FK_CLIENT_ID=".$clientId." AND MS.DELETE_FLAG = '0' ";
                    $resultSms = fetch_rec_query($sqlSms);

                    $username=$resultSms[0]['MAILCONFIG_USERNAME'];
                    $password=$resultSms[0]['MAILCONFIG_PASSWORD'];
                    $sendgrid_username = "arunnagpal";
                    $sendgrid_password = "devil1502";

                    $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
                    
                    $fromAddress=$resultSms[0]['CLIENT_EMAIL'];
                    $fromName=$resultSms[0]['CLIENTNAME'];
                    $subject="Activity Closure Mail";

                    $sqlCust = "SELECT UM.EMAIL,UM.FULLNAME,UM.CONTACT_NO,ACT.ACT_TITLE,ACT.C_ACT_ID,ACT.ACT_DESC,ACT.ACT_COMMENT FROM ".USERMASTER." UM INNER JOIN ".ACTIVITYMASTER_ACT." ACT ON ACT.CREATED_BY = UM.PK_USER_ID WHERE ACT.PK_ACT_ID='".$actId."' ";
                    $resultCust = fetch_rec_query($sqlCust);

                    $reciverAddress=$resultCust[0]['EMAIL'];
                    $reciverName=$resultCust[0]['FULLNAME'];
                    $actTitle=$resultCust[0]['ACT_TITLE'];
                    $actCid=$resultCust[0]['C_ACT_ID'];
                    $actDesc=$resultCust[0]['ACT_DESC'];

                    $actCommentData = getActCommentByactIdArrayFromJson($actId,$clientId);
                    //print_r($actCommentData);

                    //GET DATA FROM INQ ALLOCATION MASTER FOR ACT ALLOCATION MASTER HISTORY
                     $sqlActAllocation = "SELECT IAM.CREATED_BY_DATE,UM.FULLNAME,UM1.FULLNAME AS ASSIGNBY FROM ".INQALLOCATIONMASTER." IAM INNER JOIN ".USERMASTER." UM ON UM.PK_USER_ID = IAM.FK_USER_ID INNER JOIN ".USERMASTER." UM1 ON UM1.PK_USER_ID = IAM.CREATED_BY WHERE IAM.FK_INQ_ID = '".$actId."' AND IAM.ASSIGN_TYPE = 'ACT' AND IAM.FK_CLIENT_ID = '".$clientId."' ";
                    $resultActAllocation = fetch_rec_query($sqlActAllocation);
                    //print_r($resultActAllocation);EXIT;

                    $year = date("Y");
                    //$reciverNumberContact=$resultCust[0]['CUST_NAME'];
                    //$customerMobile = $resultCust[0]['CUSTMOBILE'];
                    $customerMobile = $resultCust[0]['CONTACT_NO']; // CHANGE MADE ON 17-12-2016 BY DEEPAK - REF BUG 4385
                    // GET THE MAIL ADDRESS OF ASSIGN USER TO THIS INQUIRY
                    $getClientDetail = getClientDetail($clientId);
                    
                    $checkFlag = checkFlagOnClient($clientId,'1','closeAct',$orgId);//1:SMS
    //                        print_r($checkFlag);exit;
                    
                    if(!empty($customerMobile) && $checkFlag['status'] == SCS && $checkFlag['data'][0]['isActive'] == '1' ){                                             
                        $assignedUserName = "";
                    $sqlGetAssignUserDetail = "SELECT UM.FULLNAME FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$userId."'  AND UM.DELETE_FLAG = 0 LIMIT 0,1 ";
                        $resultGetAssignUserDetail = fetch_rec_query($sqlGetAssignUserDetail);
                        if(count($resultGetAssignUserDetail) > 0){
                            $assignedUserName = !empty($resultGetAssignUserDetail[0]['FULLNAME']) ? $resultGetAssignUserDetail[0]['FULLNAME'] : "";
                        }

                        $senderSid = (isset($resultSms[0]['SMS_SID']) ) ? $resultSms[0]['SMS_SID'] : ''; // SENDER SID 
                        $apiurl = (isset($resultSms[0]['SMS_API_URL']) ) ? $resultSms[0]['SMS_API_URL'] : ''; // SENDER API URL 
                        $users = (isset($resultSms[0]['SMS_USERNAME']) ) ? $resultSms[0]['SMS_USERNAME'] : ''; // user of sms api
                        $pass = (isset($resultSms[0]['SMS_PASSWORD'])) ? $resultSms[0]['SMS_PASSWORD'] : ''; // user pass
                        //to check +91 remove
                        $checkNo = substr($customerMobile, 0,3);
                        if($checkNo == '+91'){
                            $newContactNo = substr($customerMobile, 3);
                        }elseif (strlen($customerMobile) == '10') {
                            $newContactNo = $customerMobile;
                        }

                        if(!empty($checkFlag['data'][0]['templateFormat']) && $checkFlag['data'][0]['templateFormat'] != ""){
                            $message = str_replace("{USERNAME}", $reciverName,$checkFlag['data'][0]['templateFormat']);
                            $message = str_replace("{ACTIVITYNO}", $actCid,$message);

                        }else{
                            $message = "Hello ".$reciverName." (Act:".$actCid.") ".$actTitle." - Activity is Successfully Completed By ".$assignedUserName." ";
                        }
                        
                        if(!empty($apiurl) && !empty($senderSid)){
                                //EDITED BY DARSHAN PATEL ON - 15/02/2017
                            if($getClientDetail['data'][0]['CLIENT_SMS_TYPE'] == 0){
//                                $url = $apiurl."?authkey=".$senderSid."&mobiles=" . $customerMobile . "&message=" . $message . "&sender=LTEPOS&route=4";
                                $url = $apiurl."?usr=".$users."&key=".$senderSid."&sndr=LitheT&ph=".$newContactNo."&text=".urlencode($message)."&rpt=1";
                            }elseif($getClientDetail['data'][0]['CLIENT_SMS_TYPE'] == 1){
                                $url = $apiurl."?mobile=".$resultSms[0]['SMS_USERNAME']."&pass=" .$resultSms[0]['SMS_PASSWORD']. "&senderid=" .$senderSid. "&to=" .$newContactNo. "&msg=" . urlencode($message) ."";
                            }

                            $Curl_Session = curl_init();

                            curl_setopt_array($Curl_Session, array(
                                CURLOPT_URL => $url,
                                CURLOPT_HEADER => 0, 
                                CURLOPT_RETURNTRANSFER => TRUE, 
                                CURLOPT_SSL_VERIFYPEER => FALSE,
                                CURLOPT_TIMEOUT => 4 
                            ));

                            if( ! $resultSendSMS = curl_exec($Curl_Session)) { 
                                trigger_error(curl_error($Curl_Session)); 
                            } 
                            curl_close($Curl_Session); 

                        }

                    }

                    
                    $checkFlagEmail = checkFlagOnClient($clientId,'2','actClose',$orgId);//2:email
    //                        print_r($checkFlag);exit;
                    
                    if(!empty($reciverAddress) && $checkFlagEmail['status'] == SCS && $checkFlagEmail['data'][0]['isActive'] == '1' ){
                        
                        //GET BRANCH LOGO ADD BY DARSHAN PATEL ON 02-05-2018
                        $branchLogo = "";
                        $branchLogoData = getBranchLogo($clientId, $orgId);
                //        print_r($branchLogoData);exit;
                        if($branchLogoData['status'] == SCS){
                //            $branchLogo = $branchLogoData['data'][0]['logo'];
                            $branchLogo = "<img src='".$branchLogoData['data'][0]['logo']."' height='60px' width='60px'>";
                        }
                        
                        $assignedUserEmail = "";
                        $assignedUserName = "";

                        $sqlGetAssignUserDetail = "SELECT UM.EMAIL,UM.FULLNAME FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$userId."'  AND UM.DELETE_FLAG = 0 LIMIT 0,1 ";
                        $resultGetAssignUserDetail = fetch_rec_query($sqlGetAssignUserDetail);

                        if(count($resultGetAssignUserDetail) > 0){
                            $assignedUserEmail = !empty($resultGetAssignUserDetail[0]['EMAIL']) ? $resultGetAssignUserDetail[0]['EMAIL'] : "";
                            $assignedUserName = !empty($resultGetAssignUserDetail[0]['FULLNAME']) ? $resultGetAssignUserDetail[0]['FULLNAME'] : "";
                        }

                        $sqlCustomerDetail = "SELECT CM.FIRST_NAME,CM.LAST_NAME,CM.CUSTEMAIL,CM.CUSTMOBILE,CM.CONTACT_NAME,CM.CONTACT_NUMBER,CM.ADDRESS,IF(CM.FK_STATE_ID != '' OR CM.FK_STATE_ID != 0 , (SELECT STATE_NAME FROM ".STATEMASTER." SM WHERE SM.PK_STATE_ID = CM.FK_STATE_ID ),'-') STATENAME,IF(CM.FK_CITY_ID != '' OR CM.FK_CITY_ID != 0 , (SELECT CITY_NAME FROM ".CITYMASTER." CM1 WHERE CM1.PK_CITY_ID = CM.FK_CITY_ID ),'-') CITYNAME FROM ".CUSTOMER." CM INNER JOIN ".ACTIVITYMASTER_ACT." ACT ON ACT.FK_CUST_ID = CM.PK_CUST_ID WHERE ACT.PK_ACT_ID = '".$actId."' AND ACT.DELETE_FLAG ='0' AND ACT.ACT_STATUS = '2' LIMIT 0,1 ";
                        $resultCustDetail = fetch_rec_query($sqlCustomerDetail);

                        if(count($resultCustDetail)>0){

                            $customerDetail = '<span class="activity-title">'.
                                    'Customer Detail'.
                            '</span>'.
                            '<div class="box" >'.
                            '<table style = "width:100%;">'.
                                '<tr>'.
                                    '<td style = "width:100%;">'.
                                        '<b>Customer</b> : '.$resultCustDetail[0]['FIRST_NAME'].' '.$resultCustDetail[0]['LAST_NAME']." - ".$resultCustDetail[0]['CUSTMOBILE'].
                                    '</td>'.
                                '</tr>'.

                                '<tr>'.
                                    '<td style = "width:100%;">'.
                                        '<b>Contact Person</b> : '.$resultCustDetail[0]['CONTACT_NAME']." - ".$resultCustDetail[0]['CONTACT_NUMBER'].
                                    '</td>'.
                                '</tr>'.                               

                                '<tr>'.
                                    '<td style = "width:100%;">'.
                                        '<b>Address</b> : '.wordwrap($resultCustDetail[0]['ADDRESS'],70).' ,'.$resultCustDetail[0]['CITYNAME'].','.$resultCustDetail[0]['STATENAME'].
                                    '</td>'.
                                '</tr>'.
                                '<tr>'.
                                    '<td style = "width:100%;">'.
                                        '<b>Email</b> : '.$resultCustDetail[0]['CUSTEMAIL'].
                                    '</td>'.
                                '</tr>'.
                            '</table>'.
                            '</div>';

                        }else{
                            $customerDetail = '';
                        }

                        // START GETTING COMPANY BROCHURE
                        $sqlGetCompanyBrochure = "SELECT * FROM ".DOCUMENTLISTING." DM WHERE FK_CLIENT_ID = '".$clientId."' AND DOC_NAME LIKE '%PROFILE%' AND DELETE_FLAG = '0' AND DOC_TYPE = '1' ";
                        $resultGetCompanyBrochure = fetch_rec_query($sqlGetCompanyBrochure);

                        $messageBodyHeader = "<html lang='en'><head>".
                                "<style>body {margin: 0;padding: 0;background: #F7F7F7;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 16px;color: rgba(0, 0, 0, 0.87);}".
                        ".container {width: 768px;margin: 0 auto}".
                        "header {background: #008ab1;color: #fff;height: 60px;width: 100%;top: 0;}".        
                        "ul,li {list-style: none;margin: 0;padding: 0;}".        
                        "header:after {clear: both;}".
                        ".logo {background: #fff no-repeat 10px 5px;height: 60px; width: 60px;display: inline-block;background-size: contain;float: left;}".        //margin-left: 10px;margin-top: 3px;
                        ".page-title {float: left;width: 87%;font-weight: 300;font-size: 1.8em;padding-left: 25px;}".        
                        ".page-title small {color: rgba(255, 255, 255, 0.6);font-size: 0.6em;}".
                        ".detail-date {float: right;width: 20%;font-size: 1.2em;overflow: hidden;word-wrap: break-word;text-align: right;margin-right: 15px;margin-top: 10px;}".        
                        "main {float: left;min-height: 83vh;width: 100%;margin-bottom: 10px;}".        
                        ".reset-head {color: #ecad2a;padding-bottom: 10px;border-bottom: 1px solid;width: 80%;margin: 0 auto; margin-top: 30px}".        
                        ".detail-pass,.act-link {width: 50%;margin: 0 auto;padding: 20px;box-sizing: border-box;}".        
                        ".detail-pass ul li {padding: 10px 0px;font-size: 1.2em;color: #777;}".        
                        "footer {float: left;width: 100%;background: #008ab1;height: 60px;}".        
                        ".footer-link {color: #fff;text-align: right;padding-top: 25px;padding-right: 15px;}".        
                        ".detail-data span {font-weight: bolder}".
                        ".act-link{margin-bottom: 15px;}".
                        ".act-link a {color: #fff;text-decoration: none;font-size: 1.4em;background: #008ab1;padding: 15px 20px;border-radius: 30px;}".
                        ".act-link a:hover {background: #ecad2a;}".        
                        ".detail-top,.detail-bottom {width: 80%;margin: 0 auto;color: #555;border-bottom: 2px solid #eee;}".
                        ".detail-bottom { border-bottom: none;padding-top: 15px;border-top: 2px solid #eee;}".
                        ".color-text {color: #008ab1;font-size: 1.2em;text-decoration: none;}".
                        ".activity-title{ font-size:20px; color:#008ab1; text-align:left; width:96%; float:left; border-bottom:1px #008ab1 solid; padding:15px 2%; margin-bottom:15px;}".
                        ".activity-block{ display:block; width:80%; margin: 0 auto;}".
                        ".activity-block .title h2{ color:#ffc107; font-size:20px; font-weight:400; }".
                        ".box{ margin-bottom:20px; border:1px solid #e7e7e7; padding:10px 2%; float:left; width:96%; }".
                        ".content-block { float:left; width:43%; background:#f7f8fa;  border:1px solid #e7e7e7; padding:10px 2%; margin-bottom:15px; min-height:90px; }".                        
                        ".dec-block{ background:#f7f8fa; border:1px solid #e7e7e7; float:left; padding:0px 2%; margin:0 auto 20px auto; width:96%; }".
                        ".activity-info{ width:49%; float:left; font-size:16px; line-height:22px; }". 
                        ".logo1 {background: #fff url(http://live.proposalotg.com/assets/admin1/img/logo-web.png) no-repeat 10px 5px;height: 60px; width: 60px;display: inline-block;background-size: contain;float: right;}".
                        "</style>"."</head>"."<body>";

                        $messageBodyMid = '<div class="container">'.
                                '<header>'.
                            '<a href="#" class="logo">'.$branchLogo.'</a>'.
                            '<h1 class="page-title">Activity Closure <small></small></h1></header>'.
                            '<main><h1 class="reset-head">Dear '.(!empty($reciverName) ? $reciverName : 'Sir / Madam,').'</h1>'.
                            '<div class="detail-top">'.
                                '<p>'.
                                    'Greetings !!'.
                                '</p>'.
                                '<p>'.
                                    '(Act :'. $actCid.') '.$actTitle.' - Activity is Successfully Completed By <b>'.$assignedUserName.'</b>'.
                                '</p>'.                                                          
                            '</div>'. 

                            '<div class="activity-block">'.

                                stripcslashes($customerDetail).

                                '<span class="activity-title">'.
                                    'Activity Detail'.
                                '</span>'.
                                '<div class="box" >'.
                                    '<span class="title">'.
                                        '<h2>'.
                                            'Description'.
                                        '</h2>'.
                                    '</span>'.
                                    '<div class="dec-block">'.
                                        '<p>'.
                                            $actDesc .
                                        '</p>'.
                                    '</div>'.
                                '</div>'.
                                '<div class="box" >'.
                                    '<span class="title">'.
                                        '<h2>'.
                                            'Remarks'.
                                        '</h2>'.
                                    '</span>';
//                                    for($i=0;$i<count($actCommentData);$i++){
                                    foreach($actCommentData as $keyComment => $valueComment){
                                        $message = 'margin :0 15px 0 0';
                                        if($keyComment%2 != 0) {
                                            $message = '';
                                        }
                                        $messageBodyMid.='<div style="'.$message.'" class="content-block">'.
                                            '<div class="activity-info">'.
                                                '<span>'.$valueComment["remark"].'</span>'.
                                                '<br/>'.
                                                '<span>'.$valueComment["createdByName"].'</span>'.
                                            '</div>'.
                                            '<div class="activity-info">'.
                                                '<span>'.$valueComment["createdByDate_Date"].'</span>'.
                                                '<br/>'.
                                                '<span>'.$valueComment["createdByDate_Time"].'</span>'.
                                            '</div>'.
                                        '</div>';
                                    }            
                                $messageBodyMid.='</div>'.
                                '<div class="box" >'.
                                    '<span class="title">'.
                                        '<h2>'.
                                            'Activity Allocation'.
                                        '</h2>'.
                                    '</span>';
//                                    for($j=0;$j<count($resultActAllocation);$j++){
                                    foreach($resultActAllocation as $keyActAllo => $valueActAllo){
                                        
                                        $message = 'margin :0 15px 0 0';
                                        if($keyActAllo%2 != 0) {
                                            $message = '';
                                        }
                                        
                                        $messageBodyMid1 = '<div style="'.$message.'" class="content-block">'.
                                            '<div class="activity-info">'.
                                                '<span>'.$valueActAllo["FULLNAME"].'</span>'.
                                                '<br/><br/>'.
                                                '<span>Assign By</span>'.
                                            '</div>'.
                                            '<div class="activity-info">'.
                                                '<span>'.date("d-m-Y h:i:s A",$valueActAllo["CREATED_BY_DATE"]).'</span>'.
                                                '<br/>'.
                                                '<span>'.$valueActAllo['ASSIGNBY'].'</span>'.
                                            '</div>'.
                                        '</div>';
                                    }            
                                $messageBodyMid1.='</div>'.       
                            '</div>'.

                            '<div class="detail-bottom">'.
                                '<p class="color-text">'.
                                    'Thanks & Regards'.
                                    '<br> '.$assignedUserName.
                                '</p>'.
                            '</div>'.
                            '</main>'.
                            '<footer>'.
                            '<a href="#" class="logo1"></a>'.
                            '<section class="footer-link">&copy; Proposal Technologies | '.$year.' &nbsp;</section>'.
                            '</footer>'.
                            '<p >'.
                            'Note : This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.'.
                            '</p>';

                        $messageBodyFooter = '</div></body></html>';

                        $messageBody = $messageBodyHeader.$messageBodyMid.$messageBodyMid1.$messageBodyFooter;
//                        print_r($messageBody);exit;

                        $email = new SendGrid\Email();
                        $email->addTo($reciverAddress);
                        $email->addToName($reciverName);
                        $email->setFrom($fromAddress);
                        if(!empty($assignedUserEmail)){
                            $email->addCc($assignedUserEmail, (!empty($assignedUserName) ? $assignedUserName : ''));
                        }
                        $email->setFromName($fromName);
                        $email->setSubject($subject);
                        //print_r($allAttachmentArr); die;

                        $email->setHtml($messageBody);
                        //print_r($email); exit;
                        $response = $sendGrid->send($email);
                        $responseMail = $response->getBody();
                    }
                    
                    http_response_code(200);

                }
            } else {
                $result = array("status" => CLOSEACTFAIL);
                http_response_code(400);
            }
        }else{
            $result = array("status" => $checkOwnerAct['status']);
            http_response_code(400);
        }
        
    }else{
        $result = array("status" => 'Error. Seems activity is in Complete Status.');
        http_response_code(400);
    }
    
    return $result;
}

//END FUNCTION FOR CLOSE ACTIVITY
//START FUNCTION FOR CLOSE AND CREATE NEW ACTIVITY 
function closeAndCreateNewActivity($postData,$postFile) {
//    print_r($postData);
//    print_r($postFile); exit;
//    $postdata_1['postData'] = $postData;
    
    $flag = '0';//ERROR REQ PARAM MISSING
    if(!empty($postData['postData']['customerId']) && !empty($postData['postData']['contactId'])){//BOTH NOT EMPTY
        $flag = '1';
    }elseif (empty($postData['postData']['customerId']) && empty($postData['postData']['contactId'])) {//ACT WITHOUT CUSTOMER
        $flag = '1';
    }
    
    if($flag){
        $actAttachment = $postFile; // ATTCHMENT
        $closeActivity = closeActivity($postData);
        if ($closeActivity['status'] == SCS) {
            
            //GET INQ,SERVICE,ORD ID FROM ACT ID
            $getInqId = "SELECT FK_INQ_ID,ACT_REFERENCE_TYPE FROM ".ACTIVITYMASTER_ACT." WHERE PK_ACT_ID = '".$postData['postData']['actId']."' AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$postData['postData']['clientId']."' AND FK_ORG_ID = '".$postData['postData']['orgId']."' LIMIT 0,1 ";
            
            $resultInqId = fetch_rec_query($getInqId);
            
            $postData['postData']['inqId'] = !empty($resultInqId[0]['FK_INQ_ID']) ? $resultInqId[0]['FK_INQ_ID'] : $postData['postData']['inqId'];
//            $postData['postData']['actRefType'] = !empty($resultInqId[0]['ACT_REFERENCE_TYPE']) ? $resultInqId[0]['ACT_REFERENCE_TYPE'] : $postData['postData']['actRefType'];
            
            $postData['postData']['actId'] = "";
            $createNewActivity = createUpdateActivity($postData, $actAttachment);
            if ($createNewActivity['status'] == SCS) {
                $result = array("status" => SCS);
                http_response_code(200);
            } else {
                $result = array("status" => $createNewActivity['status']);
                http_response_code(400);
            }
        } else {
            $result = array("status" => $closeActivity['status']);
            http_response_code(400);
        }
    }else{
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR CLOSE AND CREATE NEW ACTIVITY 
//START FUNCTION FOR GET COMMENT ARRAY FROM ACTIVITY JSON
function getActCommentByactIdArrayFromJson($actId,$clientId) {
    $json_data = array();
    $getComment = "SELECT ACT_COMMENT FROM " . ACTIVITYMASTER_ACT . " WHERE PK_ACT_ID='" . $actId . "'";
    $fetchCommnt = fetch_rec_query($getComment);
    //print_r($fetchCommnt); exit;
    if (count($fetchCommnt) > 0) {
        $string = strip_slash($fetchCommnt[0]['ACT_COMMENT']);
        //print_r($string);
        $json = utf8_encode($string);
        $json_data = objectToArray(json_decode($json));
        $json_Data = remarkCreatedByChangedBy($json_data,$clientId);
    }
    return $json_Data;
}

//END FUNCTION FOR GET COMMENT ARRAY FROM ACTIVITY JSON
//START FUNCTINO FOR GET CREATED BY AND CHANGED BY FOR REMARK
function remarkCreatedByChangedBy($json_data,$clientId) {
    //print_r($json_data); exit;
    $json_Data = $json_data;
    if (count($json_Data) > 0) {
        for ($i = 0; $i < count($json_Data); $i++) {
            $postData = array();
            $postData['postData']['userId'] = $json_Data[$i]['createdBy'];
            $postData['postData']['clientId'] = $clientId;
            $createdBy = getUserListing($postData);
            if ($createdBy['status'] == SCS) {
                $createdByName = $createdBy['data'][0]['USERNAME'];
            }
            $json_Data[$i]['createdByName'] = $createdByName;
            
            $postData = array();
            $postData['postData']['userId'] = $json_Data[$i]['changedBy'];
            $postData['postData']['clientId'] = $clientId;
            $changedBy = getUserListing($postData);
            $changedByName = "";
            if ($changedBy['status'] == SCS) {
                $changedByName = $changedBy['data'][0]['USERNAME'];
            }
            $json_Data[$i]['changedByName'] = $changedByName;
            $json_Data[$i]['createdByDateTime'] = "";
            $json_Data[$i]['changedByDateTime'] = "";
            if ($json_Data[$i]['createdByDate'] != "") {
                $json_Data[$i]['createdByDate_Date'] = date("d-m-Y", $json_Data[$i]['createdByDate']);
                $json_Data[$i]['createdByDate_Time'] = date("h:i:s A", $json_Data[$i]['createdByDate']);
            }

            if ($json_Data[$i]['changedByDate'] != "") {
                $json_Data[$i]['createdByDate_Date'] = date("d-m-Y", $json_Data[$i]['createdByDate']);
                $json_Data[$i]['createdByDate_Time'] = date("h:i:s A", $json_Data[$i]['createdByDate']);
            }
            
            //ADDED BY DASRAHN FOR INDEX OF REMARKS
            $json_Data[$i]['index'] = $i;
        }
        return $json_Data;
    }
}

//START FUNCTION FOR GET LISTING OF USER GROUP/CUSTOMER GROUP
function getUserCustomerGroup($postData) {
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    $tmpArr = array();
    // CHANGE MADE BY DEEPAK PATIL ON 04-07-2016 
    //$getUserGroup = "SELECT * FROM " . GROUPMASTERACT . " WHERE FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG= 0 ";
//    $getUserGroup = "SELECT * FROM " . GROUPMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG= 0 AND GROUP_TYPE = 'USER'  ";
    $getUserGroup = "SELECT GM.* FROM " . GROUPMASTER . " GM INNER JOIN ".GROUPCUSTTRNAS." GCT ON GCT.FK_GROUP_ID = GM.PK_GROUP_ID AND GCT.DELETE_FLAG = '0' WHERE GM.FK_CLIENT_ID='" . $clientId . "' AND GM.DELETE_FLAG= 0 AND GM.GROUP_TYPE = 'USER' GROUP BY PK_GROUP_ID ";
    $fetchUserGroup = fetch_rec_query($getUserGroup);
    if (count($fetchUserGroup) > 0) {
        $tmpArr['userGroupList'] = $fetchUserGroup;
    } else {
        $tmpArr['userGroupList'] = array();
    }

//    $getCustomerGroup = "SELECT * FROM " . GROUPMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG= 0 AND GROUP_TYPE = 'CUSTOMER' ";
    $getCustomerGroup = "SELECT GM.* FROM " . GROUPMASTER . " GM INNER JOIN ".GROUPCUSTTRNAS." GCT ON GCT.FK_GROUP_ID = GM.PK_GROUP_ID AND GCT.DELETE_FLAG = '0'  WHERE GM.FK_CLIENT_ID='" . $clientId . "' AND GM.DELETE_FLAG= 0 AND GM.GROUP_TYPE = 'CUSTOMER' GROUP BY PK_GROUP_ID ";
    $fetchCustomerGroup = fetch_rec_query($getCustomerGroup);
//    print_r($fetchCustomerGroup);exit;
    if (count($fetchCustomerGroup) > 0) {
        $tmpArr['customerGroupList'] = $fetchCustomerGroup;
    } else {
        $tmpArr['customerGroupList'] = array();
    }
    $finalArr = $tmpArr;
    $result = array("status" => SCS, "data" => $finalArr);
    http_response_code(200);
    return $result;
}

//START FUNCTION FOR GET ACTIVITY ROLE NAME FOR USER AUTHORIZATION
function getUserAuthForActivity($postData) {
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    $tmpArr = array();
    $getActivityRole = fetch_rec_query("SELECT * FROM " . ACTIVITYROLEACCESS . " WHERE FK_CLIENT_ID='" . $clientId . "' AND FK_USER_ID='" . $userId . "' AND DELETE_FLAG=0");
    if (count($getActivityRole) > 0) {
        $result = array("status" => SCS, "data" => $getActivityRole);
        http_response_code(200);
    } else {
        $result = array("status" => FAILMSG);
        http_response_code(400);
    }

    return $result;
}

//END FUNCTION FOR GET ACTIVITY ROLE NAME FOR USER AUTHORIZATION

function transferActivity($postData){
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID
    $assigneTo = isset($postdata['assigneTo']) ? addslashes(trim($postdata['assigneTo'])) : ""; // ASSIGN TO ID
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    
    // CHECK THE OWNER OF THE INQUIRY. REMAIN....
    $checkOwnerAct = checkOwnerAct($actId,$userId);
    if($checkOwnerAct['status'] == SCS){
        $updateInqAll = updateInqAllocation($actId, $clientId, $userId,"ACT");
        if($updateInqAll){
            $remark = "Activity Transfered";
            if (isset($postdata['remark'])) {
                $remark = addslashes(trim($postdata['remark'])); // REMARK
            }
            $postData['postData']['remark'] = $remark;
            //print_r($postData); die;
            $insertComment = insertCommentInAct($postData);
            if ($insertComment['status'] == SCS) {

                if($actId!="" && $assigneTo!=""){
                    
                    $transferactivity['ASSIGN_TO'] = $assigneTo;
                    $transferactivity['CHANGED_BY'] = $userId;
                    $transferactivity['CHANGED_BY_DATE'] = $time;
                    
                    $whereCond = "PK_ACT_ID='".$actId."' AND DELETE_FLAG=0";
                    $transfer_activity = update_rec(ACTIVITYMASTER_ACT, $transferactivity,$whereCond);
                    
                    if($transfer_activity>=0){
                        $result = array("status"=>SCS);
                        http_response_code(200);
                        
                        insertInqAllocation($actId, $assigneTo, $clientId, "ACT", $userId);
                        // GET DETAIL FOR REMINDER
                        //if(!empty($reminderTime)){
                            // GET USER DETAIL 
                            $whereCondP = "PK_USER_ID = '".$assigneTo."' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$clientId."'";
                            $userDetail = getUserDetails($whereCondP);

                            // START GETTING ACT DETAIL 
                            $sqlActDetail = "SELECT * FROM ".ACTIVITYMASTER_ACT." ACT WHERE ACT.PK_ACT_ID = '".$actId."' LIMIT 0,1 ";
                            $resultActDetail = fetch_rec_query($sqlActDetail);
                            // GET CUSTOMER DETAIL
                            if(!empty($resultActDetail[0]['FK_CUST_ID'])){
                                $sqlCustomerDetail = "SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) CUSTOMERDETAIL,CUSTMOBILE FROM ".CUSTOMER." CM WHERE CM.FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG ='0' AND PK_CUST_ID = '".$resultActDetail[0]['FK_CUST_ID']."' LIMIT 0,1 ";
                                $resultCustomerDetail = fetch_rec_query($sqlCustomerDetail);
                                $customerName = $resultCustomerDetail[0]['CUSTOMERDETAIL'];
                                $customerMobile = $resultCustomerDetail[0]['CUSTMOBILE'];
                                
                            }else{
                                $customerName = "";
                                $customerMobile = "";
                            }
                            
                            
                            
                            // NOTIFICATION --
                            $requestData = array();
                            $requestData['postData']['clientId'] = $clientId;
                            $requestData['postData']['userId'] = $userId;
                            $requestData['postData']['orgId'] = $orgId;
                            $requestData['postData']['appName'] = "POTG";
                            $requestData['postData']['deviceType'] = "UBANTU";
                            $requestData['postData']['alertSubId'] = $assigneTo;
                            $requestData['postData']['messageType'] = "ALERT";
                            $requestData['postData']['actionFlag'] = '0';
                            $requestData['postData']['message'] = "(Act : ".$resultActDetail[0]['C_ACT_ID'].") Activity Assigned To You.";
                            $requestData['postData']['refId'] = $actId;
                            createAlertStaging($requestData);
                            
                            // GET THE SUPERVISOR ID FROM USER MASTER
                            $sqlGetSupervisor = "SELECT * FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$assigneTo."' AND FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0' LIMIT 0,1";
                            $resultGetSupervisor = fetch_rec_query($sqlGetSupervisor);

                            if(!empty($resultGetSupervisor[0]['FK_SUPERVISOR_ID'])){
                                $requestData = array();
                                $requestData['postData']['clientId'] = $clientId;
                                $requestData['postData']['userId'] = $userId;
                                $requestData['postData']['orgId'] = $orgId;
                                $requestData['postData']['appName'] = "POTG";
                                $requestData['postData']['deviceType'] = "UBANTU";
                                $requestData['postData']['alertSubId'] = $resultGetSupervisor[0]['FK_SUPERVISOR_ID'];
                                $requestData['postData']['messageType'] = "ALERT";
                                $requestData['postData']['actionFlag'] = '0';

                                $requestData['postData']['message'] = "( Act - ".$resultActDetail[0]['C_ACT_ID'].") - Activity has been transfer to ".$resultGetSupervisor[0]['FULLNAME']." ";
                                $requestData['postData']['refId'] = $actId;
                                createAlertStaging($requestData);
                            }
                        //}
                            
                            //START CODE FOR SMS & EMAIL TO ASSIGN USER
                        $sqlSms = "SELECT MS.MAILCONFIG_USERNAME,MS.MAILCONFIG_PASSWORD,CM.CLIENT_EMAIL,CM.CLIENTNAME,MS.SMS_SID,MS.SMS_API_URL,MS.SMS_USERNAME,MS.SMS_PASSWORD FROM ".MAILSMSCONFIG." MS INNER JOIN ".CLIENTMASTER." CM ON CM.PK_CLIENT_ID = MS.FK_CLIENT_ID WHERE MS.FK_CLIENT_ID=".$clientId." AND MS.DELETE_FLAG = '0' ";
                        $resultSms = fetch_rec_query($sqlSms);

                        $username = $resultSms[0]['MAILCONFIG_USERNAME'];
                        $password = $resultSms[0]['MAILCONFIG_PASSWORD'];
                        $sendgrid_username = "arunnagpal";
                        $sendgrid_password = "devil1502";

                        $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

                        $fromAddress=$resultSms[0]['CLIENT_EMAIL'];
                        $fromName=$resultSms[0]['CLIENTNAME'];
                        
                        $getClientDetail = getClientDetail($clientId);
               //                    print_r($getClientDetail);exit;
                        $sqlUser = "SELECT CONTACT_NO,FULLNAME,EMAIL FROM ".USERMASTER." WHERE PK_USER_ID = ".$assigneTo." AND FK_CLIENT_ID = ".$clientId." AND DELETE_FLAG = 0 ";
                        $resultUser = fetch_rec_query($sqlUser);

                        $userMobile = $resultUser[0]['CONTACT_NO'];
                        $userName = $resultUser[0]['FULLNAME'];
                        $userAddress = $resultUser[0]['EMAIL'];
                        $actNo = $resultActDetail[0]['C_ACT_ID'];

                        if(!empty($userMobile) && $getClientDetail['data'][0]['ACTIVITY_SMS_FLAG_AUTO'] == 1 ){

                           $senderSid = (isset($resultSms[0]['SMS_SID']) ) ? $resultSms[0]['SMS_SID'] : ''; // SENDER SID 
                           $apiurl = (isset($resultSms[0]['SMS_API_URL']) ) ? $resultSms[0]['SMS_API_URL'] : ''; // SENDER API URL 
                           $users = (isset($resultSms[0]['SMS_USERNAME']) ) ? $resultSms[0]['SMS_USERNAME'] : ''; // user of sms api
                           $clientType = (isset($getClientDetail['data'][0]['CLIENT_SMS_TYPE'])) ? $getClientDetail['data'][0]['CLIENT_SMS_TYPE'] : ''; //GET CLIENT TYPE 0 =ALL & 1= PIONEER

                           $pass = (isset($resultSms[0]['SMS_PASSWORD'])) ? $resultSms[0]['SMS_PASSWORD'] : ''; // user pass   
                           //$message = "Hello ".$reciverNameContact.",We got your inquiry. We will get back to you soon.Thanks and Regards,".$fromName;
                           
                           //to check +91 remove
                            $checkNo = substr($userMobile, 0,3);
                            if($checkNo == '+91'){
                                $newContactNo = substr($userMobile, 3);
                            }elseif (strlen($userMobile) == '10') {
                                $newContactNo = $userMobile;
                            }

                            if(!empty($apiurl) && !empty($senderSid)){                       
                                //FOR CHECK FLAG OF THIS ADDED BY DARSHAN PATEL ON 8/12/2017
                                $checkFlag = checkFlagOnClient($clientId,'1','transferActivity',$orgId);//1:SMS
            //                        print_r($checkFlag);exit;
                                if($checkFlag['status'] == SCS && $checkFlag['data'][0]['isActive'] == '1'){
                                    if(!empty($checkFlag['data'][0]['templateFormat']) && $checkFlag['data'][0]['templateFormat'] != ""){
                                        $message = str_replace("{USERNAME}", $userName,$checkFlag['data'][0]['templateFormat']);
                                        $message = str_replace("{ACTIVITYNO}", $actNo,$message);

                                    }else{
                                        $message = "Dear, ".$userName." New Activity has been Assigned to you , Please Check in CRM ";
                                    }
                                    //COMMON FUNCTION FOR SMS SEND EDITED BY DARSHAN ON 25/7/17
//                                    $sendSms = commonSmsSend($apiurl,$users,$senderSid,$newContactNo,$message,$clientType,$pass);
                                    $sendSms = commonSmsSend($clientId,$apiurl,$users,$senderSid,$newContactNo,$message,$clientType,$pass);
                                }
                            }
                            
                       } 
                    }else{
                        $result = array("status"=>TRANSFERFAIL);
                        http_response_code(400);
                    }
                }
            }else{
                $result = array("status"=>$insertComment['status']);
                http_response_code(400);
            }
        }else{
            $result = array("status"=>UPDATEFAIL." Actvity Assignment.");
            http_response_code(400);
        }
    }else{
        $result = array("status"=>$checkOwnerAct['status']);
        http_response_code(400);
    }
    
    return $result;
    
}

function assignActivity($postData){
//    print_r($postData);exit;
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID
    $assigneTo = isset($postdata['assigneTo']) ? addslashes(trim($postdata['assigneTo'])) : ""; // ASSIGN TO ID
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : ""; // ACTIVITY ID
    $notifFlag = isset($postdata['notifFlag']) ? addslashes(trim($postdata['notifFlag'])) : ""; // ACTIVITY ID
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP    
    
    if($actId!="" && $assigneTo!=""){
        
        $remark = "Activity Transfered";
        if (isset($postdata['remark'])) {
            $remark = addslashes(trim($postdata['remark'])); // REMARK
        }
        $postData['postData']['editFlag'] = $postdata['editableFlag'];
        $postData['postData']['remark'] = $remark;
        //print_r($postData); die;
        $insertComment = insertCommentInAct($postData);
        
        $transferactivity['ASSIGN_TO'] = $assigneTo;
        $transferactivity['CHANGED_BY'] = $userId;
        $transferactivity['CHANGED_BY_DATE'] = $time;
        $whereCond = "PK_ACT_ID='".$actId."' AND DELETE_FLAG=0";
        $transfer_activity = update_rec(ACTIVITYMASTER_ACT, $transferactivity,$whereCond);
        
        updateInqAllocation($actId,$clientId,$userId,"ACT");
        insertInqAllocation($actId, $assigneTo, $clientId, "ACT", $userId);
        
        if($transfer_activity>=0){
            
            //ADDED BY MONTU ON 20-10-2016 FOR GET TODAY'S TOTAL INQUIRY
            $requestData = array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['requestuserId'] = $userId;
            $getUserHierarchy = getUserHierarchy($requestData);
            //print_r($getUserHierarchy); exit;
            if($getUserHierarchy['status'] == SCS){
                $finalUserId = array();
//                for($f=0;$f<count($getUserHierarchy['data']);$f++){
//                    if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                        for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                            if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                                $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                            }
//                        }
//                    }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                        $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//                    }
//                }
                foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
                    if($valueUH['lastLevelFlag'] == 'N'){
                //for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
                        foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                            if($valueUD['lastLevelFlag'] == 'Y'){
                                 $finalUserId[$keyUH] =  $valueUD['userId'];
                            }
                        }
                    }elseif($valueUH['lastLevelFlag'] == 'Y'){
                        $finalUserId[$keyUH] = $valueUH['userId'];
                    }
                }
                $userIds = implode(",", $finalUserId);
                $where = " AND ASSIGN_TO IN(" . $userIds . ") ";
            }
            $finalArr = array();
            $todayDate = date("Y-m-d"); //TODAY DATE
//            $whereCond = " WHERE AM.DUE_DATE = '" . $todayDate . "' AND AM.FK_CLIENT_ID='".$clientId."'  ".$where." AND AM.ACT_STATUS=0 AND AM.DELETE_FLAG=0 ORDER BY PK_ACT_ID DESC";
            $whereCond = " WHERE AM.DUE_DATE = '" . $todayDate . "' AND AM.FK_CLIENT_ID='".$clientId."' AND AM.FK_ORG_ID = '".$orgId."' ".$where." AND AM.ACT_STATUS=0 AND AM.DELETE_FLAG=0 ORDER BY PK_ACT_ID DESC";

            $sqlGetActCount = "SELECT COUNT(PK_ACT_ID) actId FROM " . ACTIVITYMASTER_ACT . " AM LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID=AM.ASSIGN_TO INNER JOIN " . USERMASTER . " UM1 ON UM1.PK_USER_ID=AM.CREATED_BY INNER JOIN " . ACTIVITYLOOKUPMASTER . " ALM ON ALM.PK_ACT_LOOKUP_ID=AM.ACT_MODE $whereCond";
            $resultActivity = fetch_rec_query($sqlGetActCount);
            
            //ADDED BY MONTU ON 20-10-2016 FOR GET TODAY'S TOTAL INQUIRY
            
            if($notifFlag != '1'){
                //$result = array("status"=>SCS);
                $getActivtyListing = getActivityByWHereCondition("AM.FK_CLIENT_ID='" . $clientId . "' AND AM.DELETE_FLAG=0 AND PK_ACT_ID = '".$actId."' ");
                $getActivtyListing['data']['totalInqCount'] = $resultActivity[0]['actId'];
                //print_r($getActivtyListing); exit;
                // NOTIFICATION --
                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['appName'] = "POTG";
                $requestData['postData']['deviceType'] = "UBANTU";
                $requestData['postData']['alertSubId'] = $assigneTo;
                $requestData['postData']['messageType'] = "ALERT";
                $requestData['postData']['actionFlag'] = '0';
                $requestData['postData']['message'] = "(Act : ".$getActivtyListing['data'][0]['C_ACT_ID'].") Activity Assigned To You.";
                $requestData['postData']['refId'] = $actId;
                createAlertStaging($requestData);
                //print_r($getActivtyListing); die;
                // GET THE SUPERVISOR ID FROM USER MASTER
                $sqlGetSupervisor = "SELECT UM.FK_SUPERVISOR_ID,UM.FULLNAME FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$assigneTo."' AND FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0' LIMIT 0,1";
                $resultGetSupervisor = fetch_rec_query($sqlGetSupervisor);

                if(!empty($resultGetSupervisor[0]['FK_SUPERVISOR_ID'])){
                    $requestData = array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['appName'] = "POTG";
                    $requestData['postData']['deviceType'] = "UBANTU";
                    $requestData['postData']['alertSubId'] = $resultGetSupervisor[0]['FK_SUPERVISOR_ID'];
                    $requestData['postData']['messageType'] = "ALERT";
                    $requestData['postData']['actionFlag'] = '0';

                    $requestData['postData']['message'] = "( Act - ".$getActivtyListing['data'][0]['C_ACT_ID'].") - Activity has been transfer to ".$resultGetSupervisor[0]['FULLNAME']." ";
                    $requestData['postData']['refId'] = $actId;
                    createAlertStaging($requestData);
                }

                //START CODE FOR SMS & EMAIL TO ASSIGN USER
                $sqlSms = "SELECT MS.MAILCONFIG_USERNAME,MS.MAILCONFIG_PASSWORD,CM.CLIENT_EMAIL,CM.CLIENTNAME,MS.SMS_SID,MS.SMS_API_URL,MS.SMS_USERNAME,MS.SMS_PASSWORD FROM ".MAILSMSCONFIG." MS INNER JOIN ".CLIENTMASTER." CM ON CM.PK_CLIENT_ID = MS.FK_CLIENT_ID WHERE MS.FK_CLIENT_ID=".$clientId." AND MS.DELETE_FLAG = '0' ";
                $resultSms = fetch_rec_query($sqlSms);

                $username=$resultSms[0]['MAILCONFIG_USERNAME'];
                $password=$resultSms[0]['MAILCONFIG_PASSWORD'];
                $sendgrid_username = "arunnagpal";
                $sendgrid_password = "devil1502";

                $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

                $fromAddress=$resultSms[0]['CLIENT_EMAIL'];
                $fromName=$resultSms[0]['CLIENTNAME'];
                $subject="Inquiry Assign Mail";

                 $getClientDetail = getClientDetail($clientId);
        //                    print_r($getClientDetail);exit;
                 $sqlUser = "SELECT CONTACT_NO,FULLNAME,EMAIL FROM ".USERMASTER." WHERE PK_USER_ID = ".$assigneTo." AND FK_CLIENT_ID = ".$clientId." AND DELETE_FLAG = 0 ";
                 $resultUser = fetch_rec_query($sqlUser);

                 $userMobile = $resultUser[0]['CONTACT_NO'];
                 $userName = $resultUser[0]['FULLNAME'];
                 $userAddress = $resultUser[0]['EMAIL'];
                 $actNo = $getActivtyListing['data'][0]['C_ACT_ID'];

                 if(!empty($userMobile)){

                    $senderSid = (isset($resultSms[0]['SMS_SID']) ) ? $resultSms[0]['SMS_SID'] : ''; // SENDER SID 
                    $apiurl = (isset($resultSms[0]['SMS_API_URL']) ) ? $resultSms[0]['SMS_API_URL'] : ''; // SENDER API URL 
                    $users = (isset($resultSms[0]['SMS_USERNAME']) ) ? $resultSms[0]['SMS_USERNAME'] : ''; // user of sms api
                    $clientType = (isset($getClientDetail['data'][0]['CLIENT_SMS_TYPE'])) ? $getClientDetail['data'][0]['CLIENT_SMS_TYPE'] : ''; //GET CLIENT TYPE 0 =ALL & 1= PIONEER

                    $pass = (isset($resultSms[0]['SMS_PASSWORD'])) ? $resultSms[0]['SMS_PASSWORD'] : ''; // user pass   
                    //$message = "Hello ".$reciverNameContact.",We got your inquiry. We will get back to you soon.Thanks and Regards,".$fromName;
                    $message = "Dear, ".$userName." New Activity has been Assigned to you , Please Check in CRM ";
                    //to check +91 remove
                    $checkNo = substr($userMobile, 0,3);
                    if($checkNo == '+91'){
                        $newContactNo = substr($userMobile, 3);
                    }elseif (strlen($userMobile) == '10') {
                        $newContactNo = $userMobile;
                    }

                    if(!empty($apiurl) && !empty($senderSid)){                       
                        //FOR CHECK FLAG OF THIS ADDED BY DARSHAN PATEL ON 8/12/2017
                        $checkFlag = checkFlagOnClient($clientId,'1','transferActivity',$orgId);//1:SMS
//                            print_r($checkFlag);exit;
                        if($checkFlag['status'] == SCS && $checkFlag['data'][0]['isActive'] == '1'){
                            if(!empty($checkFlag['data'][0]['templateFormat']) && $checkFlag['data'][0]['templateFormat'] != ""){
                                $message = str_replace("{USERNAME}", $userName,$checkFlag['data'][0]['templateFormat']);
                                $message = str_replace("{ACTIVITYNO}", $actNo,$message);

                            }else{
                                $message = "Dear, ".$userName." New Activity has been Assigned to you , Please Check in CRM ";
                            }
                            //COMMON FUNCTION FOR SMS SEND EDITED BY DARSHAN ON 25/7/17
//                            $sendSms = commonSmsSend($apiurl,$users,$senderSid,$newContactNo,$message,$clientType,$pass);
                            $sendSms = commonSmsSend($clientId,$apiurl,$users,$senderSid,$newContactNo,$message,$clientType,$pass);
                        }
                    }
                }               
            }
            $result = array("status" => SCS,"data"=>($getActivtyListing['status'] == SCS) ? $getActivtyListing['data'] : $getActivtyListing['status']);
            http_response_code(200);
        }else{
            $result = array("status"=>ASSIGNFAIL);
            http_response_code(400);
        }
    }
    
    return $result;
    
}

/*==================== START CUSTOM ACTIVITY CODE  (SCHEDULE)====================*/

//START FUNCTION FOR GENERATE DATES BETWEEN GIVEN TWO DATES WITH FREQUENCY
function addFrequencyDaysToStartEndDate($startDate, $endDate, $frequency) {
    $begin = new DateTime($startDate);
    $end = new DateTime($endDate);
    $format = "Y-m-d";
    $interval = new DateInterval('P' . $frequency . 'D');
    $dateRange = new DatePeriod($begin, $interval, $end);
    $range = [];
    foreach ($dateRange as $date) {
        $range[] = $date->format($format);
    }
    return $range;
}

//END FUNCTION FOR GENERATE DATES BETWEEN GIVEN TWO DATES WITH FREQUENCY

//START FUNCTION FOR CREATE/UPDATE CUSTOM ACTIVITY 
function createUpdateCustomActivity($postData, $postFile) {
//    print_r($postData);
//    print_r($postFile); exit;
    $postdata = !empty($postData['postData'])?$postData['postData']:$postData; //POST DATA
    $actTitle = isset($postdata['actTitle']) ? addslashes(trim($postdata['actTitle'])) : ""; // TITLE
    $actDesc = isset($postdata['actDesc']) ? addslashes(trim($postdata['actDesc'])) : ""; // DESCRIPTION      
    $actMode = isset($postdata['actMode']) ? addslashes(trim($postdata['actMode'])) : ""; // MODE    
    $actType = isset($postdata['actType']) ? addslashes(trim($postdata['actType'])) : ""; // TYPE    
    $actAttachment = $postFile; // ATTCHMENT   
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // USER ID   
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // USER ID      
    $startDate = isset($postdata['actStartDate']) ? addslashes(trim($postdata['actStartDate'])) : ""; // START DATE     
    $endDate = isset($postdata['actEndDate']) ? addslashes(trim($postdata['actEndDate'])) : ""; // END DATE      
    $frequency = isset($postdata['actFrequency']) ? addslashes(trim($postdata['actFrequency'])) : ""; // FREQUENCY 
    $actDueTime = isset($postdata['actDueTime']) ? addslashes(trim($postdata['actDueTime'])) : ""; // DUE DATE  
    $actEndTime = isset($postdata['actEndTime']) ? addslashes(trim($postdata['actEndTime'])) : ""; // DUE DATE  
    $reminderTime = isset($postdata['reminderTime']) ? addslashes(trim($postdata['reminderTime'])) : ""; // REMINDER TIME
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    $actRefType = isset($postdata['actRefType']) ? addslashes(trim($postdata['actRefType'])) : "ACT"; // ACT,ORD,INQ,SERVICE,PROJECT,CAMPAIGN
    $projectId = isset($postdata['projId']) ? addslashes(trim($postdata['projId'])) : ""; //PROJECT ID
    $unRegCustName = isset($postdata['unRegCustName']) ? addslashes(trim($postdata['unRegCustName'])) : ""; // UN REGISTER USER
    $unRegCustMobile = isset($postdata['unRegCustMobile']) ? addslashes(trim($postdata['unRegCustMobile'])) : ""; // UN REGISTER USER MOBILE
//    $conPerMobile = isset($postdata['conPerMobile']) ? addslashes(trim($postdata['conPerMobile'])) : ""; // CONPERMOBILE
//    $conPerName = isset($postdata['conPerName']) ? addslashes(trim($postdata['conPerName'])) : ""; // CONPERNAME
    $contactId = isset($postdata['contactId']) ? addslashes(trim($postdata['contactId'])) : "0"; // CONTACTID
    
    $status = "";
    if ($startDate < $todayDate || $endDate < $todayDate) {
        $result = array("status" => DATEGREATERSELECT);
        http_response_code(400);
        return $result;
    }
    
    // CHECK THE SIZE AND TYPE OF ATTACHMENT WHILE CREATING ACT
    $validExt = array("image/jpg", "image/jpeg", "image/gif", "image/png", "application/doc", "application/docx", "application/pdf", "application/xlsx", "image/bmp", "text/plain"); // VALID FORAMT TO UPLOAD
    for($t=0;$t<count($actAttachment['name']);$t++){
        if($actAttachment['error'][$t] == 0){
            if($actAttachment['size'][$t] > (MAX_SIZE*1024)){
                $result = array("status" => FILESIZEERROR);
                http_response_code(400);
                return $result;
            }else if(!in_array($actAttachment['type'][$t] , $validExt)){
                $result = array("status" => FILEFORMTERROR);
                http_response_code(400);
                return $result;
            }
        }
    }   

    $assigneTo = "";
    if (isset($postdata['assigneTo'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $assigneTo = addslashes(trim($postdata['assigneTo'])); // ACTIVITY ID FOR EDIT
    }

    $isUserGroup = 0;
    if (isset($postdata['isUserGroup'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $isUserGroup = addslashes(trim($postdata['isUserGroup'])); // ACTIVITY ID FOR EDIT
    }


    $isCustomerGroups = "";
    if (isset($postdata['isCustomerGroup'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $isCustomerGroups = addslashes(trim($postdata['isCustomerGroup'])); // ACTIVITY ID FOR EDIT
    }    

    $customerId = "";
    if (isset($postdata['customerId'])) { // IF ACTIVITY ID SENDED IN PARAMETER
        $customerId = addslashes(trim($postdata['customerId'])); // IF IS CUSTOMER GROUP =1 THEN CUST GROUP ID ,0=CUSTOMER ID 
    }
    
    $inqId = "";
    if (isset($postdata['inqId']) && $postdata['inqId'] != "") { // IF INQ ID IS SEND IN CASE OF ACTIVITY BIND TO INQUIRY 
        $inqId = addslashes(trim($postdata['inqId'])); 
        
        if($actRefType == 'INQ' || $actRefType == 'ORD'){
            $table = INQMASTER;
            $whereCond = " AND PK_INQ_ID = '".$inqId."' ";
        }elseif ($actRefType == 'SERVICE') {
            $table = SERVICEMASTER;
            $whereCond = " AND PK_SERVICE_ID = '".$inqId."' ";
        }elseif ($actRefType == 'LEADINQ') {
            $table = LEADINQMASTER;
            $whereCond = " AND PK_LEAD_ID = '".$inqId."' ";
        }else{
            $whereCond = "";
        }
        //ADD BY DARSHAN PATEL FOR THE GET CUSTOMER NAME FROM IDS ON 19-03-2018
        if(!empty($whereCond) && $whereCond != ""){
            $getCustomerId = "SELECT FK_CUST_ID,FK_CONTACT_ID FROM ".$table." WHERE DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$clientId."' ".$whereCond." LIMIT 0,1 ";
            $resultCustomerId = fetch_rec_query($getCustomerId);
            if(count($resultCustomerId)>0){
                $custId = $resultCustomerId[0]['FK_CUST_ID'];
                $customerId = $resultCustomerId[0]['FK_CUST_ID'];
                $contactId = $resultCustomerId[0]['FK_CONTACT_ID'];
            }
        }
    }

    //FOR CONTACT PERSON
//    if(!empty($conPerMobile) && $contactId == '0' && empty($contactId)){
//        
//        $sqlQuery = "SELECT PK_CONTACT_PERSON_ID FROM " . CUSTOMER . " CM INNER JOIN ".MULTICONTACTMASTER." MCP ON MCP.FK_CONTACT_ID = CM.PK_CUST_ID WHERE CM.FK_CLIENT_ID = '".$clientId."' AND MCP.CONTACT_NUMBER = '" . $conPerMobile . "' AND CM.DELETE_FLAG = 0 AND MCP.DELETE_FLAG = 0 LIMIT 0,1";
//        $queryResult = fetch_rec_query($sqlQuery);
//        //print_r($queryResult); exit;
//        if(count($queryResult) > 0){
//            $contactId = $queryResult[0]['PK_CONTACT_PERSON_ID'];
//        }
//    }
    
    
    $insertActivityArr['FK_CLIENT_ID'] = $clientId;
    $insertActivityArr['FK_ORG_ID'] = $orgId;
    $insertActivityArr['ACT_CUSTOM_TITLE'] = $actTitle;
    $insertActivityArr['ACT_CUSTOM_DESC'] = $actDesc;
    //$insertActivityArr['ACT_CUSTOM_ATTACHMENT'] = $finalUploadList;
    //$insertActivityArr['DUE_DATE_CUSTOM'] = $getDatesArr[0];    
    $insertActivityArr['ASSIGN_TO'] = $assigneTo;
    $insertActivityArr['START_DATE'] = $startDate;
    $insertActivityArr['END_DATE'] = $endDate;
    $insertActivityArr['FREQUENCY'] = $frequency;
    $insertActivityArr['ACT_CUSTOM_MODE'] = $actMode;
    $insertActivityArr['IS_USER_GROUP'] = $isUserGroup;
    $insertActivityArr['IS_CUST_GROUP'] = $isCustomerGroups;
    $insertActivityArr['CUSTOMER_ID'] = $customerId;    
    $insertActivityArr['CONTACT_ID'] = $contactId;    
    $insertActivityArr['CREATED_BY'] = $userId;
    $insertActivityArr['CREATED_BY_DATE'] = $time;
    $insertActivityArr['CHANGED_BY'] = 0;
    $insertActivityArr['CHANGED_BY_DATE'] = "";
    $insertActivityArr['LAST_SYNC_TIME'] = 0;
    $insertActivityArr['DELETE_FLAG'] = 0;
    $insertCustom_activity = insert_rec(ACTIVITYMASTER_ACTCUSTOM, $insertActivityArr);
    $refId = "";
    if($insertCustom_activity['lastInsertedId'] >0 ){
        $refId = $insertCustom_activity['lastInsertedId'];
    }    

//    print_r($refId);exit;
    $insertAct['FK_CLIENT_ID'] = $clientId;
    $insertAct['FK_ORG_ID'] = $orgId;
    $insertAct['ACT_TITLE'] = $actTitle;
    $insertAct['ACT_DESC'] = $actDesc;
    $insertAct['ACT_MODE'] = $actMode;
    $insertAct['ACT_TYPE'] = $actType;
    $insertAct['FK_INQ_ID'] = $inqId; //INQ ID
    //$insertAct['ACT_ATTACHMENT'] = $finalUploadList;
    $insertAct['ACT_COMMENT'] = '';
    $insertAct['ACT_START_TIME'] = 0;
    $insertAct['ACT_END_TIME'] = 0;
    $insertAct['ACT_STATUS'] = 0;
    $insertAct['CREATED_BY'] = $userId; // CREATED BY USER ID
    $insertAct['FROM_INSERT'] = SCHEDULE;
    $insertAct['LAST_SYNC_TIME'] = 0; // LAST SYNC TIME 
    $insertAct['REFERENCE_NO'] = $refId; //REFERENCE ID
    $insertAct['ACT_REFERENCE_TYPE'] = $actRefType; //REFERENCE ID
    $insertAct['FK_PROJECT_ID'] = $projectId;
    $insertAct['UNREG_CUST_NAME'] = $unRegCustName;
    $insertAct['UNREG_CUST_MOBILE'] = $unRegCustMobile;
    
    // ADDED BY DEEPAK PATIL ON 24-05-2016 FOR START DATE AND END DATE IS SAME THEN PASS DEFAULT DATE IN START DATE.
//    if($startDate == $endDate){
        $getDatesArr[] = $startDate;
//    }else{
//        $getDatesArr = addFrequencyDaysToStartEndDate($startDate, $endDate, $frequency);    
//    }
    
    if (count($getDatesArr) > 0) {
        $uploadForAttachment = array();
        for ($k = 0; $k < count($getDatesArr); $k++) {
            $explodedReminderTime = explode("|",$reminderTime);
            $insertAct['DUE_DATE'] = $getDatesArr[$k];
            $insertAct['DUE_TIME'] = $actDueTime;
            $insertAct['END_DATE'] = $getDatesArr[$k];
            $explodeTime = explode("|",$actEndTime);
            $insertAct['END_TIME'] = $explodeTime[1];
            $insertAct['REMINDER_ON'] = $getDatesArr[$k]."|".$explodedReminderTime[1];
            $reminderTime24Hr = date("Y-m-d H:i:s",strtotime($getDatesArr[$k]." ".$explodedReminderTime[1]));
            $assignedToArr = $custIdArr = array();

            if ($assigneTo != "" && !empty($isUserGroup)) {//IF USER GROUP SELECTED
                
                if($customerId != ""){
                    $assignedToArr[] = $assigneTo;
                    $insertAct['ASSIGN_TYPE'] = "GROUP"; //INQ ID
                }else{
                    $getIdFromGroups = fetch_rec_query("SELECT FK_CUST_ID FROM " . GROUPCUSTTRNAS . " WHERE FK_GROUP_ID='" . $assigneTo . "' AND DELETE_FLAG=0");
                    //print_r($getIdFromGroups); exit;
                    if (count($getIdFromGroups) > 0) {
                        for ($i = 0; $i < count($getIdFromGroups); $i++) {
                            $assignedToArr[] = $getIdFromGroups[$i]['FK_CUST_ID'];
                        }
                    }
                    $insertAct['ASSIGN_TYPE'] = "NORMAL"; //INQ ID
                }
            } else { // IF USER ID SELECTED
                $assignedToArr[] = $assigneTo;
            }
            //print_r($assignedToArr); EXIT;
            if ($customerId != "") { // IF CUSTOMER ID IS NOT BLANK
                if ($isCustomerGroups != 0 && $isCustomerGroups != "") {//IF CUSTOMER GROUP SELECTED
                    $getIdFromCustGroups = fetch_rec_query("SELECT FK_CUST_ID FROM " . GROUPCUSTTRNAS . " WHERE FK_GROUP_ID='" . $customerId . "' AND DELETE_FLAG = 0");
                    if (count($getIdFromCustGroups) > 0) {
                        for ($i = 0; $i < count($getIdFromCustGroups); $i++) {
                            $custIdArr[] = $getIdFromCustGroups[$i]['FK_CUST_ID'];
                        }
                    }
                } else { //IF CUSTOMER GROUP NOT SELECTED
                    $custIdArr[] = $customerId;
                }
            }

            $clientRequest = getClientDetail($clientId);
            if($clientRequest['status'] == SCS){
                $clientDetail = $clientRequest['data'];
            }

            for ($i = 0; $i < count($assignedToArr); $i++) {
                $assignedToId = $assignedToArr[$i];
                if (count($custIdArr) > 0) {
                    for ($j = 0; $j < count($custIdArr); $j++) {
                        $customerId1 = $custIdArr[$j];
                        $insertAct['ASSIGN_TO'] = $assignedToId;
                        $insertAct['FK_CUST_ID'] = $customerId1;
                        $insertAct['FK_CONTACT_ID'] = $contactId;
                        $insertAct['CREATED_BY'] = $userId;
                        $insertAct['CREATED_BY_DATE'] = $time;
                        $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                        $insertAct['C_ACT_ID'] = $getActId;
                        $insert_activity = insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                        
                        // START GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                        $requestData = array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['actId'] = $insert_activity['lastInsertedId'];
                        $createActDetail = activityCalenderData($requestData);
                        $tempActDetail = ($createActDetail['status'] == SCS) ? $createActDetail['data'][0] :  $createActDetail['status'];
                        array_push($status, $tempActDetail);
                        
                        // END GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                        
                        //$getDatesArr[$k]." ".$explodedReminderTime[1];
                        
                        // ACTIVITY ASSIGNMENT CALL
                        $requestData = array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['appName'] = "POTG";
                        $requestData['postData']['deviceType'] = "UBANTU";
                        $requestData['postData']['alertSubId'] = $assigneTo;
                        $requestData['postData']['messageType'] = "ALERT";
                        $requestData['postData']['actionFlag'] = '0';
                        $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                        $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                        createAlertStaging($requestData);
                        
                        
                        //GOOGLE CAL CODE START
                        if($isUserGroup == '0'){//FOR ONLY SIGNLE USER NOT FOR USER GROUP
                            $startTimeCal = $endTimeCal = array();
                            $tempStartTime = strtotime(date("d-m-Y", strtotime($startDate))." ".date("h:i A",strtotime($actDueTime)));
                            $startTimeCal[0]['dateTime'] = dateToCal($tempStartTime); 
                            $startTimeCal[0]['timeZone'] = "Asia/Kolkata"; 

                            $tempEndTime = strtotime(date("d-m-Y", strtotime($endDate))." ".date("h:i A",strtotime($explodeTime[1])));
                            $endTimeCal[0]['dateTime'] = dateToCal($tempEndTime); 
                            $endTimeCal[0]['timeZone'] = "Asia/Kolkata"; 
                            //print_r($startTime); exit;
                            $summary = $actTitle;
                            $location = "";
                            $description = $actDesc;

                            insertEventIntoCalander($summary,$location,$description,$startTimeCal,$endTimeCal,$assignedToId,$clientId);
                        }
                        //GOOGLE CAL CODE END
                        
                        // START INSERTING ATTACHMENT INTO ATTACHMENT MASTER AGAINST ACTIVITY ID
                        
                        if (!empty($actAttachment['name']) && empty($uploadForAttachment)) {
                            for($t=0;$t<count($actAttachment['name']);$t++){
                                
                                if($actAttachment['error'][$t] == 0){
                                    $requestData = $postFile = array();
                                    $requestData['clientId'] = $clientId;
                                    $requestData['userId'] = $userId;
                                    $requestData['actId'] = $insert_activity['lastInsertedId'];
                                    $requestData['orgId'] = "";

                                    $postFile['name'] =$actAttachment['name'][$t];
                                    $postFile['type'] =$actAttachment['type'][$t];
                                    $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                                    $postFile['error'] =$actAttachment['error'][$t];
                                    $postFile['size'] =$actAttachment['size'][$t];
                                    $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                                    $uploadForAttachment[] = $attachmentUpload['data'];
                                }
                            }
                            
                        }else{
                           
                            for($d=0;$d<count($uploadForAttachment);$d++){
                                $insertAttach = array();
                                $insertAttach['ATTACH_CAT'] = "ACT";
                                $insertAttach['FK_INQ_ID'] = $insert_activity['lastInsertedId'];
                                $insertAttach['ATTACHFILE_LINK'] = APP_WEB_ROOT."/attachment/".$uploadForAttachment[$d]['fileName'];
                                $insertAttach['CREATED_BY'] = $userId;
                                $insertAttach['CREATED_BY_DATE'] = time();
                                $insertAttach['DELETE_FLAG'] = 0;
                                $insertAttachRequest = insert_rec(ATTACHINQMASTER, $insertAttach); // INSERTING DATA TO TABLE
                            }
                        }

                        //ADDED BY DARSHAN PATEL ON 29/12/2016    
                        $insertinqMail['FK_CLIENT_ID'] = $clientId;
                        //print_r($clientDetail); exit;
                        $requestDataCust = array();
                        $requestDataCust['postData']['clientId'] = $clientId;
                        $requestDataCust['postData']['custId'] = $customerId1;
                        $requestDataCust['postData']['userId'] = $userId;
                        $requestDataCust['postData']['orgId'] = !empty($orgId) ? $orgId : '1';
                        
                        $custDetail = getCustomerDetailByCustId($requestDataCust);
                        //print_r($custDetail); exit;
                        $creatCronMailSmsFlag = 0;
                        if($custDetail['status'] == SCS){
                            
                            if($actMode == "2"){ // SEND MAIL
                                $insertinqMail['TYPE_OF_MESSAGE'] = "MAIL";
                                for($d=0;$d<count($custDetail['data'][0]['CustomerInfo']['multiEmailData']);$d++){
                                    if($custDetail['data'][0]['CustomerInfo']['multiEmailData'][$d]['defaultFlag'] == 1){
                                        $insertinqMail['RECIVER_EMAIL'] = trim($custDetail['data'][0]['CustomerInfo']['multiEmailData'][$d]['emailAddress']);
                                        $insertinqMail['RECIVER_MOBILE'] = "";
                                    }
                                }
                                $creatCronMailSmsFlag = 1;
                            }else if($actMode == "3"){ // SEND SMS
                                $insertinqMail['TYPE_OF_MESSAGE'] = "SMS";
                                for($d=0;$d<count($custDetail['data'][0]['CustomerInfo']['multiPhoneData']);$d++){
                                    if($custDetail['data'][0]['CustomerInfo']['multiPhoneData'][$d]['defaultFlag'] == 1){
                                        $insertinqMail['RECIVER_MOBILE'] = trim($custDetail['data'][0]['CustomerInfo']['multiPhoneData'][$d]['phone']);
                                        $insertinqMail['RECIVER_EMAIL']="";
                                    }
                                }
                                $creatCronMailSmsFlag = 1;
                            }
                            
                            if($creatCronMailSmsFlag){
                                $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                                $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];
                                //$insertinqMail['RECIVER_EMAIL'] = $inqEventDetails[0]['inqCustEmail'];

                                $insertinqMail['RECIVER_NAME']  = $custDetail['data'][0]['CustomerInfo']['Name'];
                                $insertinqMail['MAIL_SUBJECT']  = $actTitle;
                                $insertinqMail['MAIL_CONTENT']  = $actDesc;
                                $insertinqMail['ATTACHMENT_LINK']= $attachmentUpload['data']['fileLink'];
                                $insertinqMail['SEND_STATUS']   = 0;
                                $insertinqMail['TYPE_OF_MAIL']  = "ACT";
                                $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($getDatesArr[$k]));
                                $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                                $insertinqMail['CREATED_BY']    = $userId;
                                $insertinqMail['CREATED_BY_DATE'] = time();
                                $insertinqMail['DELETE_FLAG'] = 0;
                                //print_r($insertinqMail); exit;
                                // INSERT DATA INTO DATA BASE
                                $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                                /*if($insertActMail){
                                    $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                                    $insertinqMail['RECIVER_EMAIL'] = "";
                                    $insertinqMail['RECIVER_MOBILE'] = "";
                                    $insertinqMail['RECIVER_NAME']  = "";
                                    $insertinqMail['MAIL_SUBJECT']  = "";
                                    $insertinqMail['MAIL_CONTENT']  = "";
                                    $insertinqMail['ATTACHMENT_LINK']= "";
                                    $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                                }*/
                            }                        
                        }

                        //START - ADD NOTIFICATION TO CRONMAILMASTER FOR USER
                        //ADDED BY DARSHA PATEL ON - 29/12/2016
                        $insertinqMail['FK_CLIENT_ID'] = $clientId;
                        $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                        $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];  
                        $insertinqMail['RECIVER_EMAIL'] = "";
                        $insertinqMail['RECIVER_MOBILE'] = "";
                        $insertinqMail['RECIVER_NAME']  = "";
                        $insertinqMail['MAIL_SUBJECT']  = "";
                        $insertinqMail['MAIL_CONTENT']  = "";
                        $insertinqMail['ATTACHMENT_LINK']= "";
                        $insertinqMail['SEND_STATUS']   = 0;
                        $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                        $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($getDatesArr[$k]));
                        $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                        $insertinqMail['CREATED_BY']    = $userId;
                        $insertinqMail['CREATED_BY_DATE'] = time();
                        $insertinqMail['DELETE_FLAG'] = 0;
                        //print_r($insertinqMail); exit;
                        // INSERT DATA INTO DATA BASE
                        $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);

                    }
                }
                else {
                    $insertAct['ASSIGN_TO'] = $assignedToId;
                    $insertAct['FK_CUST_ID'] = '';
                    $insertAct['FK_CONTACT_ID'] = '';
                    $insertAct['CREATED_BY'] = $userId;
                    $insertAct['CREATED_BY_DATE'] = $time;
                    $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                    $insertAct['C_ACT_ID'] = $getActId;
                    $insert_activity = insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                    
                    // START GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                        $requestData = array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['actId'] = $insert_activity['lastInsertedId'];
                        $createActDetail = activityCalenderData($requestData);
                        
                        $tempActDetail = ($createActDetail['status'] == SCS) ? $createActDetail['data'][0] :  $createActDetail['status'];
                        array_push($status, $tempActDetail);
                        
                    // END GETTING INSERTED DATA FOR CALENDAR ADDED BY DEEPAK ON 23-02-2018
                    
                    //GOOGLE CAL CODE START
                    if($isUserGroup == '0'){//FOR ONLY SIGNLE USER NOT FOR USER GROUP
                        $startTimeCal = $endTimeCal = array();
                        $tempStartTime = strtotime(date("d-m-Y", strtotime($startDate))." ".date("h:i A",strtotime($actDueTime)));
                        $startTimeCal[0]['dateTime'] = dateToCal($tempStartTime); 
                        $startTimeCal[0]['timeZone'] = "Asia/Kolkata"; 

                        $tempEndTime = strtotime(date("d-m-Y", strtotime($endDate))." ".date("h:i A",strtotime($explodeTime[1])));
                        $endTimeCal[0]['dateTime'] = dateToCal($tempEndTime); 
                        $endTimeCal[0]['timeZone'] = "Asia/Kolkata"; 
                        //print_r($startTime); exit;
                        $summary = $actTitle;
                        $location = "";
                        $description = $actDesc;

                        insertEventIntoCalander($summary,$location,$description,$startTimeCal,$endTimeCal,$assignedToId,$clientId);
                    }
                    //GOOGLE CAL CODE START
                    
                    
                    //ADDED BY DARSHAN PATEL ON 29/12/2016
                    $insertinqMail['FK_CLIENT_ID'] = $clientId;
                    $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                    $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];  
                    $insertinqMail['RECIVER_EMAIL'] = "";
                    $insertinqMail['RECIVER_MOBILE'] = "";
                    $insertinqMail['RECIVER_NAME']  = "";
                    $insertinqMail['MAIL_SUBJECT']  = "";
                    $insertinqMail['MAIL_CONTENT']  = "";
                    $insertinqMail['ATTACHMENT_LINK']= "";
                    $insertinqMail['SEND_STATUS']   = 0;
                    $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                    $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($getDatesArr[$k]));
                    $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                    $insertinqMail['CREATED_BY']    = $userId;
                    $insertinqMail['CREATED_BY_DATE'] = time();
                    $insertinqMail['DELETE_FLAG'] = 0;
                    //print_r($insertinqMail); exit;
                    // INSERT DATA INTO DATA BASE
                    $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                    
                    // ACTIVITY ASSIGNMENT CALL
                    $requestData = array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['appName'] = "POTG";
                    $requestData['postData']['deviceType'] = "UBANTU";
                    $requestData['postData']['alertSubId'] = $assigneTo;
                    $requestData['postData']['messageType'] = "ALERT";
                    $requestData['postData']['actionFlag'] = '0';
                    $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                    $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                    createAlertStaging($requestData);
                    
                    // START INSERTING ATTACHMENT INTO ATTACHMENT MASTER AGAINST ACTIVITY ID
                    //print_r($actAttachment); exit;

                    if (!empty($actAttachment['name']) && empty($uploadForAttachment)) {
                        for($t=0;$t<count($actAttachment['name']);$t++){
                            if($actAttachment['error'][$t] == 0){
                                $requestData = $postFile = array();
                                $requestData['clientId'] = $clientId;
                                $requestData['userId'] = $userId;
                                $requestData['actId'] = $insert_activity['lastInsertedId'];
                                $requestData['orgId'] = "";

                                $postFile['name'] =$actAttachment['name'][$t];
                                $postFile['type'] =$actAttachment['type'][$t];
                                $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                                $postFile['error'] =$actAttachment['error'][$t];
                                $postFile['size'] =$actAttachment['size'][$t];
                                //print_r($postFile);
                                $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                                //print_r($attachmentUpload);
                                $uploadForAttachment[] = $attachmentUpload['data'];
                            }
                        }
                        
                    }else{
                        //print_r($uploadForAttachment);
                        for($d=0;$d<count($uploadForAttachment);$d++){
                            $insertAttach = array();
                            $insertAttach['ATTACH_CAT'] = "ACT";
                            $insertAttach['FK_INQ_ID'] = $insert_activity['lastInsertedId'];
                            $insertAttach['ATTACHFILE_LINK'] = APP_WEB_ROOT."/attachment/".$uploadForAttachment[$d]['fileName'];
                            $insertAttach['CREATED_BY'] = $userId;
                            $insertAttach['CREATED_BY_DATE'] = time();
                            $insertAttach['DELETE_FLAG'] = 0;
                            $insertAttachRequest = insert_rec(ATTACHINQMASTER, $insertAttach); // INSERTING DATA TO TABLE
                        }
                    }
                }
            }
            if (isset($insert_activity)) {
                $result = array("status" => SCS,"data"=>$status);
                http_response_code(200);
            } else {
                $result = array("status" => ACTINSERTFAIL);
                http_response_code(400);
            }
        }
    }
    return $result;
}

//END FUNCTION FOR CREATE/UPDATE CUSTOM ACTIVITY
//START FUNCTION FOR DISABLE CUSTOM ACTIVITY
function disableCustomActivity($postData) {
    $postdata = $postData['postData']; //POST DATA
    $todayDate = date("Y-m-d"); //TODAY DATE
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // USER ID
    $time = time(); // CURRENT TIME STAMP
    $customActId = "";
    if (isset($postdata['customActId'])) { // IF CUSTOM ACTIVITY ID SENDED IN PARAMETER
        $customActId = addslashes(trim($postdata['customActId'])); // CUSTOM ACTIVITY ID FOR EDIT
    } else {
        $result = array("status" => PROVIDEACTID);
        http_response_code(400);
        return $result;
    }

    $enableDisableFlag = isset($postdata['enableDisableFlag']) ? addslashes(trim($postdata['enableDisableFlag'])) : ""; // 0 or 1
    $updateDeleteFlag['DELETE_FLAG'] = $enableDisableFlag;
    $updateDeleteFlag['CHANGED_BY'] = $userId;
    $updateDeleteFlag['CHANGED_BY_DATE'] = $time;
    
    $whereCond = "PK_ACT_CUSTOM_ID='" . $customActId . "'";
    $update_custom_activity = update_rec(ACTIVITYMASTER_ACTCUSTOM, $updateDeleteFlag, $whereCond);
    
    if (isset($update_custom_activity) && $update_custom_activity > 0) {
        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => CUSTOMACTUPDATEFAIL);
        http_response_code(400);
    }
}

//END FUNCTION FOR DISABLE CUSTOM ACTIVITY
//START FUNCTION FOR CUSTOM ACTIVITY LISTING 
function getCustomActListing($postData) {
    
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['userId']) ? addslashes(trim($postdata['clientId'])) : ""; // USER ID  
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // USER ID  
    $todayDate = date("Y-m-d");
    $enableActFlag = 0;
    
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['requestuserId'] = $userId;
    $getUserHierarchy = getUserHierarchy($requestData);
    //print_r($getUserHierarchy); exit;
    if($getUserHierarchy['status'] == SCS){
        $finalUserId = array();
//        for($f=0;$f<count($getUserHierarchy['data']);$f++){
//            if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                    if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                        $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                    }
//                }
//            }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//            }
//        }
        foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
            if($valueUH['lastLevelFlag'] == 'N'){
                //for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
                foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                    if($valueUD['lastLevelFlag'] == 'Y'){
                        $finalUserId[$keyUH] =  $valueUD['userId'];
                    }
                }
            }elseif($valueUH['lastLevelFlag'] == 'Y'){
                $finalUserId[$keyUH] = $valueUH['userId'];
            }
        }
        
        //print_r($finalUserId); exit;     
        $userIds = implode(",", $finalUserId);
        $where = " AND CREATED_BY IN (".$userIds.")";
    }
    
    $whereCondArr = array();
    if (isset($postdata['enableActFlag'])) { //IF PARAMETER RECIEVED THEN GIVE DELETE_FLAG = 0
        $enableActFlag = addslashes(trim($postdata['enableActFlag'])); // DELETE FLAG CONDITION
        $whereCondArr[] = "DELETE_FLAG=0";
    }

    $currentDataFlag = 0;
    if (isset($postdata['currentDataFlag'])) {
        $currentDataFlag = addslashes(trim($postdata['currentDataFlag'])); // IF END DATE LESS THEN TODAY DATE THEN 
        $whereCondArr[] = "END_DATE >='" . $todayDate . "'";
    }

    $customActId = 0;
    if (isset($postdata['customActId'])) {
        $customActId = addslashes(trim($postdata['customActId'])); // IF CUSTOM ACTIVITY ID RECIEVED IN PARAMETER THEN GIVE PARTICULAR ID DETAILS
        $whereCondArr[] = "PK_ACT_CUSTOM_ID ='" . $customActId . "'";
    }
    

    $whereCondArr[] = "FK_CLIENT_ID='".$clientId."' AND FK_ORG_ID = '".$orgId."' $where";
    if (count($whereCondArr) > 0) {
        $whereCond = "WHERE ";
        for ($i = 0; $i < count($whereCondArr); $i++) {
            if ($i == 0) {
                $whereCond .= $whereCondArr[$i];
            } else {
                $whereCond .= " AND " . $whereCondArr[$i];
            }
        }
    }
    
    $getCustomAct = "SELECT * FROM " . ACTIVITYMASTER_ACTCUSTOM . " AMC $whereCond";
    //echo $getCustomAct;
    $fetchCustomAct = fetch_rec_query($getCustomAct);
    if (count($fetchCustomAct) > 0) {
        $result = array("status" => SCS, "data" => $fetchCustomAct);
        http_response_code(200);
    } else {
        $result = array("status" => NOCUSTOMACTFOUND);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR CUSTOM ACTIVITY LISTING
//START FUNCTION FOR GET USER GROUP DATA
function getUserGroupListing($postData) {
    $postdata = $postData['postData']; // POST DATA
    $todayDate = date("Y-m-d"); // TODAY DATE
    $clientId = isset($postdata['clientId']) ? $postdata['clientId'] : ""; //CLIENT ID

    $getGroups = "SELECT * FROM " . GROUPMASTERACT . " WHERE DELETE_FLAG=0 AND FK_CLIENT_ID IN($clientId)";
    $fetchGroups = fetch_rec_query($getGroups);
    if (count($fetchGroups) > 0) {
        $result = array("status" => SCS, "data" => $fetchGroups);
        http_response_code(200);
    } else {
        $result = array("status" => NOUSERGROUPFOUND);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR GET USER GROUP DATA
//START FUNCTION FOR GET CUSTOMER GROUP DATA
function getCustomerGroupListing($postData) {
    $postdata = $postData['postData']; // POST DATA
    $todayDate = date("Y-m-d"); // TODAY DATE
    $clientId = isset($postdata['clientId']) ? $postdata['clientId'] : ""; //CLIENT ID

    $getGroups = "SELECT * FROM " . GROUPMASTER . " WHERE DELETE_FLAG=0 AND FK_CLIENT_ID IN($clientId)";
    $fetchGroups = fetch_rec_query($getGroups);
    if (count($fetchGroups) > 0) {
        $result = array("status" => SCS, "data" => $fetchGroups);
        http_response_code(200);
    } else {
        $result = array("status" => NOUSERGROUPFOUND);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR GET CUSTOMER GROUP DATA
//START FUNCTION FOR GET USER LIST FOR ASSIGN USER
function getUserListing($postData) {
    $postdata = $postData['postData'];
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID     
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP

    // $getUsers = "SELECT UGM.FK_USER_ID,UGM.USERNAME FROM " . USERGROUPMASTERACT . "   UGM INNER JOIN " . GROUPMASTERACT . " GM ON GM.PK_ACT_GRP_ID = UGM.FK_ACT_GRP_ID WHERE GM.FK_CLIENT_ID='" . $clientId . "' AND UGM.FK_USER_ID !='" . $userId . "' GROUP BY UGM.FK_USER_ID";
    $getUsers = "SELECT UGM.PK_USER_ID,UGM.FULLNAME USERNAME FROM " . USERMASTER . " UGM  WHERE UGM.PK_USER_ID IN(" . $userId . ") AND UGM.DELETE_FLAG = '0' AND UGM.FK_CLIENT_ID = '".$clientId."'";
    
    $fetchUsers = fetch_rec_query($getUsers);
    if (count($fetchUsers) > 0) {
        $result = array("status" => SCS, "data" => $fetchUsers);
        http_response_code(200);
    } else {
        $result = array("status" => FAILMSG);
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR GET USER LIST FOR ASSIGN USER
/*==================== END CUSTOM ACTIVITY CODE (SCHEDULE)====================*/

function  checkOwnerAct($actId, $userId){
    
    $sqlCheckAct = "SELECT ASSIGN_TYPE,ASSIGN_TO,CREATED_BY,PK_ACT_ID FROM ".ACTIVITYMASTER_ACT." WHERE PK_ACT_ID = '".$actId."' LIMIT 0,1";
    $resultCheckAct = fetch_rec_query($sqlCheckAct);
    
    if(count($resultCheckAct) > 0){
        
        if($resultCheckAct[0]['ASSIGN_TYPE'] == "GROUP"){
            
            $sqlCheckCurrUserGroup = "SELECT GCT.FK_CUST_ID FROM ".GROUPCUSTTRNAS." GCT INNER JOIN ".USERMASTER." UM ON UM.PK_USER_ID = GCT.FK_CUST_ID  WHERE GCT. FK_GROUP_ID IN (".$resultCheckAct[0]['ASSIGN_TO'].") AND GCT.FK_CUST_ID IN (".$userId.") LIMIT 0,1 ";
            $resultCheckCurrUserGroup = fetch_rec_query($sqlCheckCurrUserGroup);
            
            if(count($resultCheckCurrUserGroup) > 0){
                $finalJson = array("ownerId"=>$resultCheckCurrUserGroup[0]['FK_CUST_ID']);
                $result = array("status"=>SCS,"data"=>$finalJson);
                http_response_code(200);
            }else{
                $result = array("status"=>NOTAOWNERACT);
                http_response_code(400);
            }
                
        }else if($resultCheckAct[0]['ASSIGN_TO'] ==  $userId){
            
            $finalJson = array("ownerId"=>$resultCheckAct[0]['ASSIGN_TO']);
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else if($resultCheckAct[0]['CREATED_BY'] ==  $userId){
            
            $finalJson = array("ownerId"=>$resultCheckAct[0]['CREATED_BY']);
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }
    }else{
        $result = array("status"=>NOTAOWNERACT);
        http_response_code(400);
    }
    return $result;
}

//Added By Nikunj Bandhiya 8-7-2016
function createCustomerDropdown($postData){
//    print_r($postData); exit;
    $clientId = isset($postData['clientId']) ? addslashes(trim($postData['clientId'])) : "";
    $userId = isset($postData['userId']) ? addslashes(trim($postData['userId'])) : "";
    $orgId = isset($postData['orgId']) ? addslashes(trim($postData['orgId'])) : "";
    
    $ethnicity = isset($postData['ethnicity']) ? addslashes(trim($postData['ethnicity'])) : "";
    $customer = isset($postData['customerName']) ? addslashes(trim($postData['customerName'])) : "";
    $customergroup = isset($postData['customerGroup']) ?addslashes(trim($postData['customerGroup'])) : "";
    $city = isset($postData['city']) ? addslashes(trim($postData['city'])) :"";
    $state = isset($postData['state']) ? addslashes(trim($postData['state'])) : "";
    $birthDate = isset($postData['birthdate']) ? addslashes(trim($postData['birthdate'])) : "";
    $bDateRange = isset($postData['birthdays']) ? addslashes(trim($postData['birthdays'])) : "";
    $anniversaryDate = isset($postData['anniversaryDate']) ? addslashes(trim($postData['anniversaryDate'])) : "";
    $aDateRange = isset($postData['anniversaryDays']) ? addslashes(trim($postData['anniversaryDays'])) : "";
    $catchArea = isset($postData['catchArea']) ? addslashes(trim($postData['catchArea'])) : "";
    
    $itemId = isset($postData['itemId']) ? addslashes(trim($postData['itemId'])) : "";
    $group = isset($postData['groupId']) ? addslashes(trim($postData['groupId'])) : "";
    
    $lytd = isset($postData['lytd']) ? addslashes(trim($postData['lytd'])) : "";
    $salesDate = isset($postData['salesYear']) ? addslashes(trim($postData['salesYear'])) : "";
    $amtStart = isset($postData['salesAmtFrom']) ? addslashes(trim($postData['salesAmtFrom'])) : "";
    $amtEnd = isset($postData['salesAmtTo']) ? addslashes(trim($postData['salesAmtTo'])) : "";
    $ltdytdAmt = isset($postData['ltdytdAmt']) ? addslashes(trim($postData['ltdytdAmt'])) : "";
    $customerType = isset($postData['custType']) ? addslashes(trim($postData['custType'])) : "";//for customer type filter
    $countryId = isset($postData['countryId']) ? addslashes(trim($postData['countryId'])) : "";//for customer country
 
    
    $bEndDate=addDayswithdate($birthDate,$bDateRange);
    $aEndDate=addDayswithdate($anniversaryDate,$aDateRange);
    $where="";
    $tempArray = array();
    
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['requestuserId'] = $userId;
    $getUserHierarchy = getUserHierarchy($requestData);
    if($getUserHierarchy['status'] == SCS){
        $finalUserId = array();
//        for($f=0;$f<count($getUserHierarchy['data']);$f++){
//            if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                    if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                        $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                    }
//                }
//            }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//            }
//        }
        foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
            if($valueUH['lastLevelFlag'] == 'N'){                
                foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                    if($valueUD['lastLevelFlag'] == 'Y'){
                        $finalUserId[$keyUH] =  $valueUD['userId'];
                    }
                }
            }elseif($valueUH['lastLevelFlag'] == 'Y'){
                $finalUserId[$keyUH] = $valueUH['userId'];
            }
        }
        //print_r($finalUserId); exit;     
        $userIds = implode(",", $finalUserId);
    }
    
    if(!empty($customer) && $customer!=""){
        $where.=" AND CU.PK_CUST_ID IN($customer)";
    }
     if(!empty($customergroup) && $customergroup!=""){
        $where.=" AND GT.FK_GROUP_ID IN($customergroup)";
    }
    if(!empty($ethnicity) && $ethnicity!="" && $ethnicity!="-1"){
        $where.=" AND CU.ETHNICITY='".$ethnicity."' ";
    }
    if(!empty($city) && $city!="" && $city!="-1"){
        $where.=" AND CU.FK_CITY_ID IN (".$city.") ";
    }
    if(!empty($state) && $state!="" && $state!="-1"){
        $where.=" AND CU.FK_STATE_ID='".$state."' ";
    }
    if(!empty($countryId) && $countryId!="" && $countryId!="-1"){
        $where.=" AND CU.FK_COUNTRY_ID='".$countryId."' ";
    }
    if(!empty($birthDate) &&  $birthDate!="" && $bDateRange!="" && $bDateRange != '0'){
//        $where.=" AND (CU.BIRTHDATE BETWEEN '".$birthDate."' AND '".$bEndDate."') ";//EDIT BY DARSHAN PATEL ON 20-02-2018 FOR BIRTHDAY ON CONTACT PERSON
        $where.=" AND (MCM.BIRTHDATE BETWEEN '".$birthDate."' AND '".$bEndDate."') ";
    }else if(!empty($birthDate) &&  $birthDate!=""){
//        $where.=" AND CU.BIRTHDATE = '".$birthDate."' ";
        $where.=" AND MCM.BIRTHDATE = '".$birthDate."' ";
    }
    if(!empty($anniversaryDate) && $anniversaryDate!="" && $aDateRange!="" && $aDateRange!='0'){
//        $where.=" AND (CU.ANNIVERSARYDATE BETWEEN '".$anniversaryDate."' AND '".$aEndDate."') ";
        $where.=" AND (MCM.ANNIVERSARYDATE BETWEEN '".$anniversaryDate."' AND '".$aEndDate."') ";
    }elseif (!empty($anniversaryDate) && $anniversaryDate!="") {
//        $where.=" AND CU.ANNIVERSARYDATE = '".$anniversaryDate."' ";
        $where.=" AND MCM.ANNIVERSARYDATE = '".$anniversaryDate."' ";
    }
    if(!empty($catchArea) && $catchArea!=""){
        $where.=" AND MAM.CATCHMENT_AREA  LIKE '%".$catchArea."%' ";
    }
    if(!empty($customerType) && $customerType!=""){
        $where.=" AND CU.FK_CUSTTYPE_FLAG= '".$customerType."' ";
    }
    
//    if(!empty($itemId) && $itemId!=""){
//        $where.=" AND IT.FK_ITEM_ID IN ($itemId) ";
//    }
//    if(!empty($group) &&  $group!=""){
//        $where.=" AND IM.FK_GROUP_ID IN($group) ";
//    }
    $getClientDetail = getClientDetail($clientId);
//    print_r($getClientDetail); exit;
    if($getClientDetail['status'] == SCS){
        if($getClientDetail['data'][0]['CUSTOMER_LEVEL'] == '0'){//user level
            if(!empty($where) &&  $where!=""){
                $whereQry="WHERE CU.FK_CLIENT_ID = '".$clientId."' AND CU.FK_ASSIGN_USER_ID IN (".$userIds.") AND CU.DELETE_FLAG = '0' $where"; // CHANGE MADE BY DEEPAK ON 18-07-2016 REMOVE WHERE 1=1
            }
        }else{ // CLIENT LEVEL
            if(!empty($where) &&  $where!=""){
                $whereQry = "WHERE CU.FK_CLIENT_ID = '".$clientId."' AND CU.DELETE_FLAG = '0' $where";
            }
        }
    }
    
    if(!empty($where)){
        $getQuery = "SELECT DISTINCT CU.PK_CUST_ID FROM ".CUSTOMER." CU LEFT JOIN ".INQMASTER." INQ ON INQ.FK_CUST_ID=CU.PK_CUST_ID AND INQ.DELETE_FLAG = '0' LEFT JOIN ".INQTRANS." IT ON IT.FK_INQ_ID=INQ.PK_INQ_ID AND IT.DELETE_FLAG = '0' LEFT JOIN ".ITEMMASTER." IM ON IM.PK_ITEM_ID=IT.FK_ITEM_ID AND IM.DELETE_FLAG = '0' LEFT JOIN ".GROUPCUSTTRNAS." GT ON GT.FK_CUST_ID=CU.PK_CUST_ID AND GT.DELETE_FLAG = '0' LEFT JOIN ".MULTIADDRESSMASTER." MAM ON CU.PK_CUST_ID=MAM.FK_CUST_ID AND MAM.DELETE_FLAG = '0' LEFT JOIN ".MULTICONTACTMASTER." MCM ON MCM.FK_CONTACT_ID = CU.PK_CUST_ID AND MCM.DELETE_FLAG = '0' $whereQry";
        $runQuery = fetch_rec_query($getQuery);
        if(count($runQuery) > 0){
            foreach ($runQuery as $keyD => $valueD){
                $tempArray[] = $valueD['PK_CUST_ID'];
            }
//            for($t=0;$t<count($runQuery);$t++){
//                $tempArray[] = $runQuery[$t]['PK_CUST_ID'];                 
//            }            
        }
    }
    
    //print_r($tempArray); exit;
    
    $where1= "";
    $salesFlag = 0;
    $customerIdsFromCustFilter =$tempArray;
    if($lytd==1 && !empty($ltdytdAmt) && $ltdytdAmt!=""){
       $where1.=" ";
       $whereHaving[]= " SUM(SK.PROPOSAL_AMT)  >= '".$ltdytdAmt."'";
       $salesFlag = 1;
    }
    if($lytd==2 && !empty($ltdytdAmt) && $ltdytdAmt!=""){
       $where1.="AND (SK.PROPOSAL_CONF_DATE BETWEEN '".date('Y')."-04-01' AND '".date('Y-m-d')."') "; 
       $whereHaving[]= " SUM(SK.PROPOSAL_AMT)  >= '".$ltdytdAmt."'";
       $salesFlag = 1;
    }

    if(!empty($salesDate) && $salesDate!="" && $salesDate!="-1"){
       $where1.="AND SK.FISCAL_YEAR='".$salesDate."' "; 
       $salesFlag = 1;
    }
    if($amtStart!="" && $amtEnd!="" && !empty($salesDate)){
       $where1 .= " ";
       $whereHaving[]= " SUM(SK.PROPOSAL_AMT) BETWEEN '".$amtStart."' AND '".$amtEnd."' ";
       $salesFlag = 1;
       //$where1.="AND (SK.PROPOSAL_AMT BETWEEN '".$amtStart."' AND '".$amtEnd."') ";
    }
    if(!empty($itemId) && $itemId!=""){
        $where1.="AND SK.ITEM_ID IN($itemId) ";
        $salesFlag = 1;
    }
    
    if(!empty($group) && $group!=""){
        $where1.="AND SK.ITEM_GROUP_ID IN($group) ";
        $salesFlag = 1;
    }
    if(!empty($customer) && $customer!=""){
        $where1.="AND SK.CUST_ID IN($customer)";
    }
    
    //EDITED BY DARSHAN PATEL REF BUG NO-4774
    $getClientDetail = getClientDetail($clientId);
//    print_r($getClientDetail); exit;
    if($getClientDetail['status'] == SCS){
        if($getClientDetail['data'][0]['CUSTOMER_LEVEL'] == '0'){//user level
            $whereQry1="WHERE CU.FK_CLIENT_ID = '".$clientId."' AND CU.FK_ASSIGN_USER_ID IN (".$userIds.") AND CU.DELETE_FLAG = '0' $where1";           
        }else{ // CLIENT LEVEL
            $whereQry1="WHERE CU.FK_CLIENT_ID = '".$clientId."' AND  CU.DELETE_FLAG = '0' $where1";
        }
    }
    
    
//    if($where1!=" "){
//        $whereQry1="WHERE CU.FK_CLIENT_ID = '".$clientId."' AND CU.FK_ASSIGN_USER_ID IN (".$userIds.") AND CU.DELETE_FLAG = '0' $where1";
//    }
    
    if(!empty($whereHaving)){ // HAVING QUERY FOR AGGREGATION
        $finalAggQuery = "";
        if(count($whereHaving) > 0){
            for($f=0;$f<count($whereHaving);$f++){
                if($f==0){
                    $finalAggQuery.= $whereHaving[$f];
                }else{
                    $finalAggQuery.= " AND ".$whereHaving[$f];
                }
            }
            $whereQueryAgg = "HAVING ".$finalAggQuery;
        }else{
            $whereQueryAgg = "";
        }
    }else{
        $whereQueryAgg = "";
    }
    
    if(!empty($where1) && $salesFlag){
        $tempArray2 = array();
        // print_r($customerIdsFromCustFilter);
        $getQuery1 = "SELECT CU.PK_CUST_ID FROM ".CUSTOMER." CU INNER JOIN ".SALESKPI." SK ON SK.CUST_ID=CU.PK_CUST_ID $whereQry1 GROUP BY PK_CUST_ID ASC ".$whereQueryAgg;
        $runQuery1 = fetch_rec_query($getQuery1);
        if(count($runQuery1) > 0){
//            for($d=0;$d<count($runQuery1);$d++){
            foreach($runQuery1 as $keyQ => $valueQ){
                //edited by darshan patel on 29/04/17 ref - bug no-4774
                //if(!empty($customerIdsFromCustFilter)){
                    //for($e=0;$e<count($customerIdsFromCustFilter);$e++){
                        //if($customerIdsFromCustFilter[$e] == $runQuery1[$d]['PK_CUST_ID']){
                            $tempArray2[] = $valueQ['PK_CUST_ID']; 
                        //}
                    //}
               // }                
            }
            $finalArr  = $tempArray2;
        }else{
            $finalArr  = $tempArray2;
        }
    }else{
       $finalArr  = $tempArray;
    } 
    
    
    
//    print_r($runQuery); exit;
//    print_r($tempArray); exit;
  //return $runQuery1;
     
//    if(!empty($tempArray)) { 
//       $finalarray=$tempArray; 
//    }
    
//    if(!empty($finalArr)) {
    // SOLVE BUG 0005576 CHANGE BY DHRUV BHALGAMDIYA ON 22/10/2017
    if( !empty($finalArr) && (!empty($where) || !empty ($where1)) ) {
       
       $result = array("status"=>SCS,"data"=>implode(',', $finalArr)); 
       
    }else{
        $result = array("status"=>NORECORDS);
        
    }
    return $result;
    
}

function addDayswithdate($date,$days){

    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);
    
//    for add minit in time
//    date_default_timezone_set('Asia/Kolkata');
//    echo date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +1 minutes"));
//    
}

// CREATE BY DEEPAK PATIL ON 24-08-2016
function getCustomerForSmartAct($postData){
    //print_r($postData['postData']); exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; // USER ID    
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; // USER ID    
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // USER ID    
    $notCalledScine = isset($postData['postData']['notCalled']) ? addslashes(trim($postData['postData']['notCalled'])) : ""; // USER ID    
    $notSelected = isset($postData['postData']['notSelected']) ? addslashes(trim($postData['postData']['notSelected'])) : ""; // 1= NO FILTER SELECTED   //0 = FILTER SELECTED
    
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['requestuserId'] = $userId;
    $getUserHierarchy = getUserHierarchy($requestData);
    if($getUserHierarchy['status'] == SCS){
        $finalUserId = array();
//        for($f=0;$f<count($getUserHierarchy['data']);$f++){
//            if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                    if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                        $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                    }
//                }
//            }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//            }
//        }
        foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
                if($valueUH['lastLevelFlag'] == 'N'){
                    //for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
                    foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                        if($valueUD['lastLevelFlag'] == 'Y'){
                            $finalUserId[$keyUH] =  $valueUD['userId'];
                        }
                    }
                }elseif($valueUH['lastLevelFlag'] == 'Y'){
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }
        //print_r($finalUserId); exit;     
        $userIds = implode(",", $finalUserId);
    }
    
    if($notSelected == '1'){//IF NOT ENY SMART FILTER SELECTED THEN GIVE ALL CUSTOMER
        //FOR CHECK CLIENT IS ON USER LEVEL OR CLIENT LEVEL
        $getClientDetail = getClientDetail($clientId);
    //    print_r($getClientDetail); exit;
        if($getClientDetail['status'] == SCS){
            
            if($getClientDetail['data'][0]['CUSTOMER_LEVEL'] == '0'){//user level
                $whereQry1=" CUST.FK_CLIENT_ID = '".$clientId."' AND CUST.FK_ASSIGN_USER_ID IN (".$userIds.") AND CUST.DELETE_FLAG = '0' ";           
            }else{ // CLIENT LEVEL
                $whereQry1=" CUST.FK_CLIENT_ID = '".$clientId."' AND  CUST.DELETE_FLAG = '0' ";
            }
        }
        
        //GET CUSTOMER 
        $sqlCustomerDetail = "SELECT CUST.FIRST_NAME,CUST.LAST_NAME,CUST.PK_CUST_ID,CUST.LAST_ACT_DATE,CUST.TOTAL_INQ,CUST.TOTAL_BOOK,CUST.CUSTMOBILE,(SELECT PHONE_NO FROM ".MULTIPHONEMASTER." MPM WHERE MPM.FK_CUST_ID = CUST.PK_CUST_ID AND MPM.DEFAULT_FLAG = '1' AND MPM.DELETE_FLAG = '0' LIMIT 0,1) PHONEDETAIL FROM ".CUSTOMER." CUST WHERE  $whereQry1";
        $getCustomerDetail = fetch_rec_query($sqlCustomerDetail);
//        print_r($getCustomerDetail);EXIT;
        if(count($getCustomerDetail) > 0){
            
            $finalJson = array();
//            for($j=0;$j<count($getCustomerDetail);$j++){
            foreach ($getCustomerDetail as $keyCust => $valueCust) {
                    
                $finalJson[$keyCust]['custName'] = $valueCust['FIRST_NAME']." ".$valueCust['LAST_NAME'];
                $finalJson[$keyCust]['custId'] = $valueCust['PK_CUST_ID'];
                $finalJson[$keyCust]['custNumber'] = !empty($valueCust['PHONEDETAIL']) ? $valueCust['PHONEDETAIL'] : (!empty($valueCust['CUSTMOBILE'] ? $valueCust['CUSTMOBILE']  : '--'));
                $finalJson[$keyCust]['totalInqCount'] = $valueCust['TOTAL_INQ'];
                $finalJson[$keyCust]['totalOrderCount'] = $valueCust['TOTAL_BOOK'];
                $finalJson[$keyCust]['lastActDate'] = ($valueCust['LAST_ACT_DATE'] != '0000-00-00' && $valueCust['LAST_ACT_DATE'] != '') ? date("d-m-Y",  strtotime($valueCust['LAST_ACT_DATE'])) : "--";
            }
            
            $result = array("status" => SCS,"data"=>$finalJson); 
            http_response_code(200);
            
        }else{ // IF NO CUSTOMER FOUND THE GIVE BELOW ERROR
            $result = array("status" => CUSNOTFOUND); 
            http_response_code(400);
        }
        
    }else{//IF FILTER IS SELECTED THEN GIVE FILTER WISE DATA 
        
        $custDetail= createCustomerDropdown($postData['postData']); 
//    print_r($custDetail); EXIT;
        if($custDetail['status'] == SCS){ // IF CUSTOMER FOUND THE GO INSIDE

            $finalCustList = $custDetail['data'];

            if(!empty($finalCustList)){ // IF NOT EMPTY OF CUSTOMER LISTING THEN GO INSIDE

                $sqlCustomerDetail = "SELECT CUST.FIRST_NAME,CUST.LAST_NAME,CUST.PK_CUST_ID,CUST.LAST_ACT_DATE,CUST.TOTAL_INQ,CUST.TOTAL_BOOK,CUST.CUSTMOBILE,(SELECT PHONE_NO FROM ".MULTIPHONEMASTER." MPM WHERE MPM.FK_CUST_ID = CUST.PK_CUST_ID AND  MPM.DEFAULT_FLAG = '1' AND MPM.DELETE_FLAG = '0' LIMIT 0,1) PHONEDETAIL FROM ".CUSTOMER." CUST WHERE CUST.FK_CLIENT_ID = '".$clientId."' AND CUST.DELETE_FLAG = 0 AND CUST.PK_CUST_ID IN (".$finalCustList.")";
                $getCustomerDetail = fetch_rec_query($sqlCustomerDetail);
                if(count($getCustomerDetail) > 0){
                    
                    $finalJson = array();
                    foreach ($getCustomerDetail as $keyCust => $valueCust) {
                    
                        $finalJson[$keyCust]['custName'] = $valueCust['FIRST_NAME']." ".$valueCust['LAST_NAME'];
                        $finalJson[$keyCust]['custId'] = $valueCust['PK_CUST_ID'];
                        $finalJson[$keyCust]['custNumber'] = !empty($valueCust['PHONEDETAIL']) ? $valueCust['PHONEDETAIL'] : ( !empty($valueCust['CUSTMOBILE'] ? $valueCust['CUSTMOBILE']  : '--') );
                        $finalJson[$keyCust]['totalInqCount'] = $valueCust['TOTAL_INQ'];
                        $finalJson[$keyCust]['totalOrderCount'] = $valueCust['TOTAL_BOOK'];
                        $finalJson[$keyCust]['lastActDate'] = ($valueCust['LAST_ACT_DATE'] != '0000-00-00' && $valueCust['LAST_ACT_DATE'] != '') ? date("d-m-Y",  strtotime($valueCust['LAST_ACT_DATE'])) : "--";
                    }
                    
                    $result = array("status" => SCS,"data"=>$finalJson); 
                    http_response_code(200);
                }else{ // IF NO CUSTOMER FOUND THE GIVE BELOW ERROR
                    $result = array("status" => NORECORDS); 
                    http_response_code(400);
                }

            }else{ // IF NO CUSTOMER FOUND THEN GIVE BELOW ERROR
                $result = array("status" => CUSNOTFOUND);
                http_response_code(400);
            }
        }else{ 
            $result = array("status" => CUSNOTFOUND); 
            http_response_code(400);
        }
    }
    
    return $result; // RETURN DATA TO API
}


//Added By Nikunj Bandhiya 12-07-2016
function createSmartActivity($postData, $postFile) {
//  print_r($postData); exit;
    $postdata = !empty($postData['postData']) ? $postData['postData'] : $postData; //POST DATA
//    print_r($postdata);exit;
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // USER ID       
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID       
    
    $actTitle = isset($postdata['actTitle']) ? addslashes(trim($postdata['actTitle'])) : ""; // TITLE
    $actDesc = isset($postdata['actDesc']) ? addslashes(trim($postdata['actDesc'])) : ""; // DESCRIPTION      
    $actMode = isset($postdata['actMode']) ? addslashes(trim($postdata['actMode'])) : ""; // MODE    
    $actType = isset($postdata['actType']) ? addslashes(trim($postdata['actType'])) : ""; // TYPE
    $isUserGroup = isset($postdata['isUserGroup']) ? addslashes(trim($postdata['isUserGroup'])) : ""; // TYPE by darshan patel on 08/02/2017
    $custType = isset($postdata['custType']) ? addslashes(trim($postdata['custType'])) : ""; // TYPE    
    $assigneTo = isset($postdata['assigneTo']) ? addslashes(trim($postdata['assigneTo'])) : ""; // assigneTo    
    $actDueDate = isset($postdata['actDueDate']) ? addslashes(trim($postdata['actDueDate'])) : ""; //DATE   
    $actDueTime = isset($postdata['actDueTime']) ? addslashes(trim($postdata['actDueTime'])) : ""; //DATE   
    $reminderTime = isset($postdata['reminderTime']) ? addslashes(trim($postdata['reminderTime'])) : ""; //REMINDER TIME   
    $actEndDate = isset($postdata['actEndDate']) ? addslashes(trim($postdata['actEndDate'])) : ""; //END DATE
    $actEndTime = isset($postdata['actEndTime']) ? addslashes(trim($postdata['actEndTime'])) : ""; //END TIME
    //$isUserGroup = addslashes(trim($postdata['postData']['isUserGroup'])); // TYPE    
    $actAttachment = $postFile; // ATTCHMENT   
    $customerIds = isset($postdata['customerIds']) ? addslashes(trim($postdata['customerIds'])) : ""; // TYPE
    $todayDate = date("Y-m-d"); //TODAY DATE
    $time = time(); // CURRENT TIME STAMP
    
    $projectId = isset($postdata['projId']) ? addslashes(trim($postdata['projId'])) : ""; //PROJECT ID
    $unRegCustName = isset($postdata['unRegCustName']) ? addslashes(trim($postdata['unRegCustName'])) : ""; // UN REGISTER USER
    $unRegCustMobile = isset($postdata['unRegCustMobile']) ? addslashes(trim($postdata['unRegCustMobile'])) : ""; // UN REGISTER USER MOBILE
    $actRefType = isset($postdata['actRefType']) ? addslashes(trim($postdata['actRefType'])) : ""; // ACT,ORD,INQ,SERVICE,PROJECT,CAMPAIGN
    $inqId = isset($postdata['inqId']) ? addslashes(trim($postdata['inqId'])) : "0"; // ACT,ORD,INQ,SERVICE,PROJECT,CAMPAIGN
    
     // CHECK THE SIZE AND TYPE OF ATTACHMENT WHILE CREATING ACT
    $validExt = array("image/jpg", "image/jpeg", "image/gif", "image/png", "application/doc", "application/docx", "application/pdf", "application/xlsx", "image/bmp", "text/plain"); // VALID FORAMT TO UPLOAD
    if(!empty($actAttachment) && $actAttachment != ""){
        for($t=0;$t<count($actAttachment['name']);$t++){
            if($actAttachment['error'][$t] == 0){
                if($actAttachment['size'][$t] > (MAX_SIZE*1024)){
                    $result = array("status" => FILESIZEERROR);
                    http_response_code(400);
                    return $result;
                }else if(!in_array($actAttachment['type'][$t] , $validExt)){
                    $result = array("status" => FILEFORMTERROR);
                    http_response_code(400);
                    return $result;
                }
            }
        }
    }
    
    // CHANGE MADE BY DEEPAK ON  18-07-2016
    // PURPOSE - ADDED CUSTOMER LTD , YTD into query
    //$custDetail= createCustomerDropdown($postData); 
    //print_r($custDetail); exit;
    //if($custDetail['status'] == SCS){
    
    if(!empty($customerIds)){
        
        $custAry=explode(',', $customerIds);
        
        $insertAct['FK_CLIENT_ID'] = $clientId;
        $insertAct['FK_ORG_ID'] = $orgId;
        $insertAct['ACT_TITLE'] = $actTitle;
        $insertAct['ACT_DESC'] = $actDesc;
        $insertAct['DUE_DATE'] = date("Y-m-d", strtotime($actDueDate));
        $insertAct['DUE_TIME'] = $actDueTime;
        $insertAct['END_DATE'] = date("Y-m-d",strtotime($actEndDate));
        $insertAct['END_TIME'] = $actEndTime;
        $insertAct['REMINDER_ON'] = $reminderTime;
        $insertAct['ACT_MODE'] = $actMode;
        $insertAct['ACT_TYPE'] = $actType;
        //$insertAct['CUST_TYPE'] = $custType;
        //$insertAct['ACT_ATTACHMENT'] = $finalUploadList;
        $insertAct['ACT_COMMENT'] = '';
        $insertAct['ACT_START_TIME'] = 0;
        $insertAct['ACT_END_TIME'] = 0;
        $insertAct['ACT_STATUS'] = 0;
        $insertAct['CREATED_BY'] = $userId; // CREATED BY USER ID
        $insertAct['CREATED_BY_DATE'] = time();
        $insertAct['LAST_SYNC_TIME'] = 0; // LAST SYNC TIME 
        $insertAct['FK_PROJECT_ID'] = $projectId;
        $insertAct['UNREG_CUST_NAME'] = $unRegCustName;
        $insertAct['UNREG_CUST_MOBILE'] = $unRegCustMobile;
        $insertAct['FK_INQ_ID'] = $inqId; //INQ ID
        $insertAct['ACT_REFERENCE_TYPE'] = $actRefType; //INQ ID
       
        $uploadFiles = array();
        foreach($custAry as $cust){ 
            $lastinsertedActId = "";
            if($isUserGroup==0){
                
                $getContactPerson = "SELECT PK_CONTACT_PERSON_ID FROM ".MULTICONTACTMASTER." WHERE FK_CONTACT_ID = '".$cust."' AND FK_CLIENT_ID = '".$clientId."' ORDER BY PK_CONTACT_PERSON_ID ASC LIMIT 0,1 ";
                $resultContactPerson = fetch_rec_query($getContactPerson);
//                print_r($resultContactPerson);exit;
                if(count($resultContactPerson)>0){
                    $conatctId = $resultContactPerson[0]['PK_CONTACT_PERSON_ID'];
                }
                
                $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                
                $insertAct['FK_CUST_ID'] = $cust;
                $insertAct['FK_CONTACT_ID'] = $conatctId;
                $insertAct['ASSIGN_TO'] = $assigneTo;
                $insertAct['ASSIGN_TYPE'] = "NORAML";
                $insertAct['CUST_TYPE'] = "SINGLE";
                $insertAct['C_ACT_ID'] = $getActId;
                
                $insert_activity=insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                $lastinsertedActId = $insert_activity['lastInsertedId'];
                $actAllocationFun = insertInqAllocation($lastinsertedActId, $assigneTo, $clientId, "ACT", $userId);
                
                //GOOGLE CAL START CODE
                $startTimeCal = $endTimeCal = array();
                $tempStartTime = strtotime(date("d-m-Y", strtotime($actDueDate))." ".date("h:i A",strtotime($actDueTime)));
                $startTimeCal[0]['dateTime'] = dateToCal($tempStartTime); 
                $startTimeCal[0]['timeZone'] = "Asia/Kolkata"; 

                $tempEndTime = strtotime(date("d-m-Y", strtotime($actEndDate))." ".date("h:i A",strtotime($actEndTime)));
                $endTimeCal[0]['dateTime'] = dateToCal($tempEndTime); 
                $endTimeCal[0]['timeZone'] = "Asia/Kolkata"; 
                //print_r($startTime); exit;
                $summary = $actTitle;
                $location = "";
                $description = $actDesc;

                insertEventIntoCalander($summary,$location,$description,$startTimeCal,$endTimeCal,$assigneTo,$clientId);
                //GOOGLE CAL END CODE
                
                
                if (!empty($actAttachment['name']) && empty($uploadFiles)) {
                    for($t=0;$t<count($actAttachment['name']);$t++){
                        if($actAttachment['error'][$t] == 0){
                            $requestData = $postFile = array();
                            $requestData['clientId'] = $clientId;
                            $requestData['userId'] = $userId;
                            $requestData['actId'] = $lastinsertedActId;
                            $requestData['orgId'] = "";
                            //print_r($requestData);
                            $postFile['name'] =$actAttachment['name'][$t];
                            $postFile['type'] =$actAttachment['type'][$t];
                            $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                            $postFile['error'] =$actAttachment['error'][$t];
                            $postFile['size'] =$actAttachment['size'][$t];

                            $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                            //print_r($attachmentUpload); exit;
                            $uploadFiles[$t] = $attachmentUpload['data']['fileLink'];
                        }
                    }
                }else{
                    
                    $attachmentLink = $uploadFiles;
                    for($s=0;$s<count($attachmentLink);$s++){
                        $insertAttchment = array();
                        $insertAttchment['ATTACHFILE_LINK'] = $attachmentLink[$s];
                        $insertAttchment['ATTACH_CAT'] = "ACT";
                        $insertAttchment['FK_INQ_ID'] = $lastinsertedActId;
                        $insertAttchment['CREATED_BY'] = $userId;
                        $insertAttchment['CREATED_BY_DATE'] = time();
                        $insertAttchment['DELETE_FLAG'] = 0;
                        $insetAttchRequest = insert_rec(ATTACHINQMASTER, $insertAttchment);
                    }

                }
                // ACTIVITY ASSIGNMENT CALL
                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['appName'] = "POTG";
                $requestData['postData']['deviceType'] = "UBANTU";
                $requestData['postData']['alertSubId'] = $assigneTo;
                $requestData['postData']['messageType'] = "ALERT";
                $requestData['postData']['actionFlag'] = '0';
                $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                createAlertStaging($requestData);
            }
            if($isUserGroup==1){
                $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                $insertAct['C_ACT_ID'] = $getActId;
                $insertAct['ASSIGN_TO'] = $assigneTo;
                $insertAct['ASSIGN_TYPE'] = "GROUP";
                $insertAct['CUST_TYPE'] = "GROUP";
                $insertAct['FK_CUST_ID'] = $cust;
                //$insertAct['ACT_TYPE'] = "group";
                $insert_activity=insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                
                $getIdFromGroups = fetch_rec_query("SELECT FK_CUST_ID FROM " . GROUPCUSTTRNAS . " GT WHERE FK_GROUP_ID=".$assigneTo."");
                if (count($getIdFromGroups) > 0) {
                    for ($i = 0; $i < count($getIdFromGroups); $i++) {                        
                        // ACTIVITY ASSIGNMENT CALL
                        $requestData = array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['appName'] = "POTG";
                        $requestData['postData']['deviceType'] = "UBANTU";
                        $requestData['postData']['alertSubId'] = $getIdFromGroups[$i]['FK_CUST_ID'];
                        $requestData['postData']['messageType'] = "ALERT";
                        $requestData['postData']['actionFlag'] = '0';
                        $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                        $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                        createAlertStaging($requestData);
                        
                        // START INSERTING ATTACHMENT INTO ATTACHMENT MASTER AGAINST ACTIVITY ID
                        //print_r($actAttachment); exit;
                    }
                }
                
                //$getIdFromGroups = fetch_rec_query("SELECT FK_CUST_ID FROM " . GROUPCUSTTRNAS . " GT WHERE FK_GROUP_ID=".$assigneTo."");
                if (count($getIdFromGroups) > 0) {
                    for ($i = 0; $i < count($getIdFromGroups); $i++) {
    //                        $insertAct['ASSIGN_TO'] = $getIdFromGroups[$i]['FK_CUST_ID'];
    //                        $insertAct['FK_CUST_ID'] = $cust;
    //                        $insertAct['ACT_TYPE'] = "group";
    //                        $insert_activity=insert_rec(ACTIVITYMASTER_ACT, $insertAct);
    //                        $lastinsertedActId = $insert_activity['lastInsertedId'];

                        if (!empty($actAttachment['name']) && empty($uploadFiles)) {
                            for($t=0;$t<count($actAttachment['name']);$t++){
                                if($actAttachment['error'][$t] == 0){
                                    $requestData = $postFile = array();
                                    $requestData['clientId'] = $clientId;
                                    $requestData['userId'] = $userId;
                                    $requestData['actId'] = $lastinsertedActId;
                                    $requestData['orgId'] = "";
                                    //print_r($requestData);
                                    $postFile['name'] =$actAttachment['name'][$t];
                                    $postFile['type'] =$actAttachment['type'][$t];
                                    $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                                    $postFile['error'] =$actAttachment['error'][$t];
                                    $postFile['size'] =$actAttachment['size'][$t];

                                    $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                                    //print_r($attachmentUpload); exit;
                                    $uploadFiles[$t] = $attachmentUpload['data']['fileLink'];
                                }
                            }
                        }else{

                            $attachmentLink = $uploadFiles;
                            for($s=0;$s<count($attachmentLink);$s++){
                                $insertAttchment = array();
                                $insertAttchment['ATTACHFILE_LINK'] = $attachmentLink[$s];
                                $insertAttchment['ATTACH_CAT'] = "ACT";
                                $insertAttchment['FK_INQ_ID'] = $lastinsertedActId;
                                $insertAttchment['CREATED_BY'] = $userId;
                                $insertAttchment['CREATED_BY_DATE'] = time();
                                $insertAttchment['DELETE_FLAG'] = 0;
                                $insetAttchRequest = insert_rec(ATTACHINQMASTER, $insertAttchment);
                            }

                        }
                       
                    }
                }
            }
            if($isUserGroup==2){
                $getIdFromCustomer = fetch_rec_query("SELECT FK_ASSIGN_USER_ID FROM " . CUSTOMER . " CU WHERE CU.PK_CUST_ID=".$cust."");
                if (count($getIdFromCustomer) > 0) {
                    
                    $getContactPerson = "SELECT PK_CONTACT_PERSON_ID FROM ".MULTICONTACTMASTER." WHERE FK_CONTACT_ID = '".$cust."' AND FK_CLIENT_ID = '".$clientId."' ORDER BY PK_CONTACT_PERSON_ID ASC LIMIT 0,1 ";
                    $resultContactPerson = fetch_rec_query($getContactPerson);
    //                print_r($resultContactPerson);exit;
                    if(count($resultContactPerson)>0){
                        $conatctId = $resultContactPerson[0]['PK_CONTACT_PERSON_ID'];
                    }
                    
                    for ($i = 0; $i < count($getIdFromCustomer); $i++) {
                        $insertAct['ASSIGN_TO'] = $getIdFromCustomer[$i]['FK_ASSIGN_USER_ID'];
                        $insertAct['FK_CUST_ID'] = $cust;
                        $insertAct['FK_CONTACT_ID'] = $conatctId;
                        $insertAct['ASSIGN_TYPE'] = "NORMAL";
                        $insertAct['CUST_TYPE'] = "SINGLE";
                        $getActId = generateId(ACTIVITYMASTER_ACT, "", $clientId);
                        $insertAct['C_ACT_ID'] = $getActId;
                        $insert_activity=insert_rec(ACTIVITYMASTER_ACT, $insertAct);
                        $lastinsertedActId = $insert_activity['lastInsertedId'];
                        $actAllocationFun = insertInqAllocation($lastinsertedActId, $assigneTo, $clientId, "ACT", $userId);

                        //GOOGLE CAL START CODE
                        $startTimeCal = $endTimeCal = array();
                        $tempStartTime = strtotime(date("d-m-Y", strtotime($actDueDate))." ".date("h:i A",strtotime($actDueTime)));
                        $startTimeCal[0]['dateTime'] = dateToCal($tempStartTime); 
                        $startTimeCal[0]['timeZone'] = "Asia/Kolkata"; 

                        $tempEndTime = strtotime(date("d-m-Y", strtotime($actEndDate))." ".date("h:i A",strtotime($actEndTime)));
                        $endTimeCal[0]['dateTime'] = dateToCal($tempEndTime); 
                        $endTimeCal[0]['timeZone'] = "Asia/Kolkata"; 
                        //print_r($startTime); exit;
                        $summary = $actTitle;
                        $location = "";
                        $description = $actDesc;

                        insertEventIntoCalander($summary,$location,$description,$startTimeCal,$endTimeCal,$getIdFromCustomer[$i]['FK_ASSIGN_USER_ID'],$clientId);
                        //GOOGLE CAL END CODE
                        
                        
                        if (!empty($actAttachment['name']) && empty($uploadFiles)) {
                            for($t=0;$t<count($actAttachment['name']);$t++){
                                if($actAttachment['error'][$t] == 0){
                                    $requestData = $postFile = array();
                                    $requestData['clientId'] = $clientId;
                                    $requestData['userId'] = $userId;
                                    $requestData['actId'] = $lastinsertedActId;
                                    $requestData['orgId'] = "";
                                    //print_r($requestData);
                                    $postFile['name'] =$actAttachment['name'][$t];
                                    $postFile['type'] =$actAttachment['type'][$t];
                                    $postFile['tmp_name'] =$actAttachment['tmp_name'][$t];
                                    $postFile['error'] =$actAttachment['error'][$t];
                                    $postFile['size'] =$actAttachment['size'][$t];

                                    $attachmentUpload = actAttachmentUpload($requestData,$postFile);
                                    //print_r($attachmentUpload); exit;
                                    $uploadFiles[$t] = $attachmentUpload['data']['fileLink'];
                                }
                            }
                        }else{
                            $attachmentLink = $uploadFiles;
                            for($s=0;$s<count($attachmentLink);$s++){
                                $insertAttchment = array();
                                $insertAttchment['ATTACHFILE_LINK'] = $attachmentLink[$s];
                                $insertAttchment['ATTACH_CAT'] = "ACT";
                                $insertAttchment['FK_INQ_ID'] = $lastinsertedActId;
                                $insertAttchment['CREATED_BY'] = $userId;
                                $insertAttchment['CREATED_BY_DATE'] = time();
                                $insertAttchment['DELETE_FLAG'] = 0;
                                $insetAttchRequest = insert_rec(ATTACHINQMASTER, $insertAttchment);
                            }

                        }
                        // ACTIVITY ASSIGNMENT CALL
                        $requestData = array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['appName'] = "POTG";
                        $requestData['postData']['deviceType'] = "UBANTU";
                        $requestData['postData']['alertSubId'] = $assigneTo;
                        $requestData['postData']['messageType'] = "ALERT";
                        $requestData['postData']['actionFlag'] = '0';
                        $requestData['postData']['message'] = "(Act : ".$getActId.") Activity Assigned To You.";
                        $requestData['postData']['refId'] = $insert_activity['lastInsertedId'];
                        createAlertStaging($requestData);
                    }
                }
            }
            
            $clientRequest = getClientDetail($clientId);
            if($clientRequest['status'] == SCS){
                $clientDetail = $clientRequest['data'];
            }
            
            //ADDED BY DARSHAN PATEL ON 29-12-2016
            $insertinqMail['FK_CLIENT_ID'] = $clientId;
            //print_r($clientDetail); exit;
            $requestDataCust = array();
            $requestDataCust['postData']['clientId'] = $clientId;
            $requestDataCust['postData']['custId'] = $customerIds;
            $requestDataCust['postData']['userId'] = $userId;
//            $requestDataCust['postData']['orgId'] = !empty($orgId) ? $orgId : '1';
            $requestDataCust['postData']['orgId'] = $orgId;
            
            $custDetail = getCustomerDetailByCustId($requestDataCust);
            //print_r($custDetail); exit;
            $creatCronMailSmsFlag = 0;
            if($custDetail['status'] == SCS){
                
                if($actMode == "2"){ // SEND MAIL
                    $insertinqMail['TYPE_OF_MESSAGE'] = "MAIL";
                    for($d=0;$d<count($custDetail['data'][0]['CustomerInfo']['multiEmailData']);$d++){
                        if($custDetail['data'][0]['CustomerInfo']['multiEmailData'][$d]['defaultFlag'] == 1){
                            $insertinqMail['RECIVER_EMAIL'] = trim($custDetail['data'][0]['CustomerInfo']['multiEmailData'][$d]['emailAddress']);
                            $insertinqMail['RECIVER_MOBILE'] = "";
                        }
                    }
                    $creatCronMailSmsFlag = 1;
                }else if($actMode == "3"){ // SEND SMS
                    $insertinqMail['TYPE_OF_MESSAGE'] = "SMS";
                    for($d=0;$d<count($custDetail['data'][0]['CustomerInfo']['multiPhoneData']);$d++){
                        if($custDetail['data'][0]['CustomerInfo']['multiPhoneData'][$d]['defaultFlag'] == 1){
                            $insertinqMail['RECIVER_MOBILE'] = trim($custDetail['data'][0]['CustomerInfo']['multiPhoneData'][$d]['phone']);
                            $insertinqMail['RECIVER_EMAIL']="";
                        }
                    }
                    $creatCronMailSmsFlag = 1;
                }
                
                if($creatCronMailSmsFlag){

                    $insertinqMail['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                    $insertinqMail['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];
                    //$insertinqMail['RECIVER_EMAIL'] = $inqEventDetails[0]['inqCustEmail'];

                    $insertinqMail['RECIVER_NAME']  = $custDetail['data'][0]['CustomerInfo']['Name'];
                    $insertinqMail['MAIL_SUBJECT']  = $actTitle;
                    $insertinqMail['MAIL_CONTENT']  = $actDesc;
                    $insertinqMail['ATTACHMENT_LINK']= $attachmentUpload['data']['fileLink'];
                    $insertinqMail['SEND_STATUS']   = 0;
                    $insertinqMail['TYPE_OF_MAIL']  = "ACT";
                    $insertinqMail['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($actDueDate));
                    $insertinqMail['REF_ID']  = $insert_activity['lastInsertedId'];
                    $insertinqMail['CREATED_BY']    = $userId;
                    $insertinqMail['CREATED_BY_DATE'] = time();
                    $insertinqMail['DELETE_FLAG'] = 0;
                    //print_r($insertinqMail); exit;
                    // INSERT DATA INTO DATA BASE
                    $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                    //print_r($insertActMail);
                    /*if($insertActMail){
                        $insertinqMail['TYPE_OF_MAIL']  = "ACT-NOTI";
                        $insertinqMail['RECIVER_EMAIL'] = "";
                        $insertinqMail['RECIVER_MOBILE'] = "";
                        $insertinqMail['RECIVER_NAME']  = "";
                        $insertinqMail['MAIL_SUBJECT']  = "";
                        $insertinqMail['MAIL_CONTENT']  = "";
                        $insertinqMail['ATTACHMENT_LINK']= "";
                        $insertActMail = insert_rec(CRONMAILMASTER, $insertinqMail);
                    }*/
                }  

                $insertactMailUser = array();
                $insertactMailUser['FK_CLIENT_ID'] = $clientId;
                $insertactMailUser['SENDER_EMAIL']  = $clientDetail[0]['MAILCONFIG_FROMADDRESS'];
                $insertactMailUser['SENDER_NAME']   = $clientDetail[0]['SENDERFNAME']." ".$clientDetail[0]['SENDERLNAME'];  
                $insertactMailUser['RECIVER_EMAIL'] = "";
                $insertactMailUser['RECIVER_MOBILE'] = "";
                $insertactMailUser['RECIVER_NAME']  = "";
                $insertactMailUser['MAIL_SUBJECT']  = "";
                $insertactMailUser['MAIL_CONTENT']  = "";
                $insertactMailUser['ATTACHMENT_LINK']= "";
                $insertactMailUser['SEND_STATUS']   = 0;
                $insertactMailUser['TYPE_OF_MAIL']  = "ACT-NOTI";
                $insertactMailUser['SCH_TIME_TO_SEND']  = date("Y-m-d", strtotime($actDueDate));
                $insertactMailUser['REF_ID']  = $insert_activity['lastInsertedId'];
                $insertactMailUser['CREATED_BY']    = $userId;
                $insertactMailUser['CREATED_BY_DATE'] = time();
                $insertactMailUser['DELETE_FLAG'] = 0;
                //print_r($insertinqMail); exit;
                // INSERT DATA INTO DATA BASE
                $insertActMail = insert_rec(CRONMAILMASTER, $insertactMailUser);                     
            }


        }
        if (isset($insert_activity)) {
            $finalJson['actId'] = $lastinsertedActId;
            $result = array("status" => SCS,"data"=>$finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => ACTINSERTFAIL);
            http_response_code(400);
        }
        
    }else{
       return  $result = array("status" => CUSNOTFOUND); 
       http_response_code(400);
    }
    
    return $result;
}

function getClientDetail($clientId){
    // GET CLIENT DETAIL
    $sqlClientDetail = "SELECT * FROM ".CLIENTMASTER." CM INNER JOIN ".MAILSMSCONFIG." MSC ON MSC.FK_CLIENT_ID = CM.PK_CLIENT_ID WHERE CM.PK_CLIENT_ID = '".$clientId."' AND CM.DELETE_FLAG = 0 LIMIT 0,1";
    $resultClientDetail = fetch_rec_query($sqlClientDetail);
    if(count($resultClientDetail) > 0){
        $result = array("status"=>SCS,"data"=>$resultClientDetail);
    }else{
        $result = array("status"=>NORECORDS);
    }
    return $result;
}

//function sendReminderSmsScheduler($clientId,$reciverNumber,$actTitle,$customerName,$custMobileNo,$dueTime,$actDueTime){
//    
//    $sqlEMailConfig = "SELECT * FROM " . MAILSMSCONFIG . " WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0'";
//    $resultEMailConfig = fetch_rec_query($sqlEMailConfig);
//
//    if(!empty($resultEMailConfig) && $resultEMailConfig[0]['ACTIVITY_SMS_FLAG_AUTO']  == 1 && $resultEMailConfig[0]['POTG_OTP_FLAG'] == 1){
//           
//        $senderSid = (isset($resultEMailConfig[0]['SMS_SID']) ) ? $resultEMailConfig[0]['SMS_SID'] : ''; // SENDER SID 
//        $apiurl = (isset($resultEMailConfig[0]['SMS_API_URL']) ) ? $resultEMailConfig[0]['SMS_API_URL'] : ''; // SENDER API URL 
//        $subMessage = "";
//        if(!empty($customerName) && !empty($custMobileNo)){
//            $subMessage = substr($customerName, 0, 25)." - ".$custMobileNo;
//        }
//        $message = "You have an activity (".substr($actTitle, 0, 50).") due at ".$actDueTime.".".$subMessage;
//        //EDITED BY DARSHAN PATEL ON 15/02/2017
//        if($resultEMailConfig[0]['CLIENT_SMS_TYPE']  == 0){            
//            $url = $apiurl."?authkey=".$senderSid."&mobiles=" . $reciverNumber . "&message=" . $message . "&sender=LTEPOS&route=4&country=91&schtime=".$dueTime."";
//        }elseif ($resultEMailConfig[0]['CLIENT_SMS_TYPE']  == 1) {
//             $url = $apiurl."?mobile=".$resultEMailConfig[0]['SMS_USERNAME']."&pass=" .$resultEMailConfig[0]['SMS_PASSWORD']. "&senderid=" .$senderSid. "&to=" .$reciverNumber. "&msg=" . urlencode($message) ."";
//        }
//            //exit;
//        $Curl_Session = curl_init();
//        curl_setopt($Curl_Session, CURLOPT_URL, $url);
//        curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($Curl_Session, CURLOPT_SSL_VERIFYPEER, false);
//        $result = curl_exec($Curl_Session);
//        if ($result) {
//            $result = array("status"=>SCS);
//        }else{
//            $result = array("status"=>SMSERROR);
//        }
//        
//    }else{
//        $result = array("status"=>NOEMAILSMSCONFIG);
//    }
//    
//    return $result;
//    
//}

function getActDetailBreakUp($postData){
    $postdata = $postData['postData']; //POST DATA
//    print_r($postdata); exit;
    
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // CLIENT ID
    $actType = isset($postdata['actType']) ? addslashes(trim($postdata['actType'])) : ""; // ACT TYPE
    $actStatus = isset($postdata['actStatus']) ? addslashes(trim($postdata['actStatus'])) : ""; // ACT STATUS
    $userName = isset($postdata['userName']) ? addslashes(trim($postdata['userName'])) : ""; // USER NAME
    $startDate = isset($postdata['startDate']) ? addslashes(trim($postdata['startDate'])) : ""; // ACT START DATE
    $endDate = isset($postdata['endDate']) ? addslashes(trim($postdata['endDate'])) : ""; // ACT END DATE
    
    $statusId = "";
    if(!empty($actType)){
        $getIdFromActType = getActTypeIdFromName($clientId,$actType);
    }else{
        $getIdFromActType['status'] = SCS;
        $getIdFromActType['data'] = "";
    }
    
//    print_r($getIdFromActType); exit;
    if($actStatus == "Pending"){
        $statusId = 0;
    }else if($actStatus == "In - Process"){
        $statusId = 1;
    }else if($actStatus == "Completed"){
        $statusId = 2;
    }
    
    $getIdFromUserName = getUserIdFromName($clientId,$userName);
//    print_r($getIdFromUserName); exit;
    if($getIdFromActType['status'] == SCS && $getIdFromUserName['status'] == SCS && !empty($startDate) && !empty($endDate)){
        
        if(!empty($getIdFromActType['data'])){
            $whereActType  = "AND AM.ACT_TYPE = '".$getIdFromActType['data']."'";
        }else{
            $whereActType  = "";
        }
        $sqlGetAct = "SELECT C_ACT_ID,PK_ACT_ID,ACT_TITLE,DUE_DATE,DUE_TIME,IF(ACT_STATUS = '0','Pending',IF(ACT_STATUS='1','In Process','Completed')) ACTSTATUS,UM.FULLNAME,IF(AM.FK_CUST_ID != 0 OR AM.FK_CUST_ID != '' ,(SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) FROM ".CUSTOMER." WHERE PK_CUST_ID = AM.FK_CUST_ID LIMIT 0,1),'--') CUTOMERDETAIL, IF(FK_CUST_ID != 0 OR FK_CUST_ID != '' ,(SELECT PHONE_NO FROM ".MULTIPHONEMASTER." WHERE FK_CUST_ID = AM.FK_CUST_ID AND DEFAULT_FLAG = '1' AND DELETE_FLAG = 0 LIMIT 0,1),'--') PHONE_NO,ALM.VALUE FROM ".ACTIVITYMASTER_ACT." AM INNER JOIN ".USERMASTER." UM ON UM.PK_USER_ID = AM.ASSIGN_TO INNER JOIN " . ACTIVITYLOOKUPMASTER . " ALM ON ALM.PK_ACT_LOOKUP_ID=AM.ACT_MODE WHERE AM.DUE_DATE BETWEEN '" . date("Y-m-d",strtotime($startDate)) . "' AND '".date("Y-m-d",strtotime($endDate))."' AND AM.FK_CLIENT_ID='" . $clientId . "' AND AM.ACT_STATUS = '".$statusId."' AND AM.ASSIGN_TO = '".$getIdFromUserName['data']."' AND AM.FK_ORG_ID = '".$orgId."' ".$whereActType." AND AM.DELETE_FLAG=0";
        $resultGetAct = fetch_rec_query($sqlGetAct);
//        print_r($resultGetAct); exit;
        if(count($resultGetAct) > 0){
            $finalJson = $resultGetAct;
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else {
            $result = array("status"=>NORECORDS);
            http_response_code(400);
        }
    }else{
        $result = array("status"=>"Required " .PARAM." Into request case. Please check and try again.");
        http_response_code(400);
    }
    return $result;
}

function getActTypeIdFromName($clientId,$actType){
    
    $sqlGetActType = "SELECT PK_CLT_LKUP_ID FROM ".CLIENTLOOKUP." CL WHERE DISPLAY_VALUE LIKE '".$actType."' AND FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM ".CLIENTLOOKUP." CLT1 WHERE CLT1.LOOK_TYPE = 'ACTIVITY_TYPE' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientId . "') LIMIT 0,1";
    $resultGetActType = fetch_rec_query($sqlGetActType);
    if(count($resultGetActType) > 0){
        $result = array("status"=>SCS,"data"=>$resultGetActType[0]['PK_CLT_LKUP_ID']);
        http_response_code(200);
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getUserIdFromName($clientId,$userName){
    
    $sqlGetActType = "SELECT UM.PK_USER_ID FROM ".USERMASTER." UM WHERE UM.FULLNAME LIKE '".$userName."' AND FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0' LIMIT 0,1";
    $resultGetActType = fetch_rec_query($sqlGetActType);
    
    if(count($resultGetActType) > 0){
        $result = array("status"=>SCS,"data"=>$resultGetActType[0]['PK_USER_ID']);
        http_response_code(200);
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function cleanString($string,$actAttachment) { // THIS FUNCTION WILL REMOVE ALL WHITESPAC AND SPECIAL CHAR FOR GIVEN STRING
    $outPut = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9.]/', '', $outPut); // Removes special chars.
}

//GET ACT ON SAME DUE DATE & DUE TIME ADDED BY DARSHAN PATEL ON 29/8/17
function getActDetailOnDueDateTime($postData){
    
    $postdata = $postData['postData']; //POST DATA
//    print_r($postdata); exit;    
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; 
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : "";
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; 
    $dueDate = isset($postdata['dueDate']) ? addslashes(trim(date('Y-m-d',  strtotime($postdata['dueDate'])))) : ""; 
    $dueTime = isset($postdata['dueTime']) ? addslashes(trim(date("h:i A",strtotime($postdata['dueTime'])))) : ""; 
    
    //GET ACT DATA ON SAME DUE DATE & TIME ONLT IN OPEN STATUS
    $actData = "SELECT ACT.PK_ACT_ID,ACT.C_ACT_ID,ACT.ACT_TITLE,ACT.DUE_DATE,ACT.DUE_TIME,IF(ACT.FK_CUST_ID != 0 OR ACT.FK_CUST_ID != '' ,(SELECT CONCAT_WS(' ',FIRST_NAME,LAST_NAME) FROM ".CUSTOMER." WHERE PK_CUST_ID = ACT.FK_CUST_ID LIMIT 0,1),'--') CUTOMERDETAIL,IF(ACT.FK_CUST_ID != 0 OR ACT.FK_CUST_ID != '' ,(SELECT PHONE_NO FROM ".MULTIPHONEMASTER." WHERE FK_CUST_ID = ACT.FK_CUST_ID AND DEFAULT_FLAG = '1' AND DELETE_FLAG = 0 LIMIT 0,1),'--') PHONE_NO FROM ".ACTIVITYMASTER_ACT." ACT WHERE ACT.DUE_DATE = '".$dueDate."' AND ACT.ACT_STATUS = '0' AND ACT.DELETE_FLAG = '0' AND ACT.FK_CLIENT_ID = '".$clientId."' AND ACT.FK_ORG_ID = '".$orgId."' AND ACT.ASSIGN_TO = '".$userId."' AND ACT.ASSIGN_TYPE = 'NORMAL' ";
    $resultActData = fetch_rec_query($actData);
    
    if(count($resultActData) >0 ){
        $finalJson = array();
        foreach ($resultActData as $keyAct => $valueAct) {
            $finalJson[$keyAct]['actId'] = $valueAct['PK_ACT_ID'];
            $finalJson[$keyAct]['cActId'] = $valueAct['C_ACT_ID'];
            $finalJson[$keyAct]['actTitle'] = $valueAct['ACT_TITLE'];
            $finalJson[$keyAct]['custName'] = $valueAct['CUTOMERDETAIL'];
            $finalJson[$keyAct]['custNo'] = $valueAct['PHONE_NO'];
            $finalJson[$keyAct]['dueDate'] = $valueAct['DUE_DATE'];
            $finalJson[$keyAct]['dueTime'] = $valueAct['DUE_TIME'];
        }
       
        if(!empty($finalJson) && $finalJson != ''){
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else{
            $result = array("status"=>NORECORDS);
            http_response_code(400);
        }
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    
    return $result;
}


function activityCalender($postData){
    
    $postdata = $postData['postData']; //POST DATA  
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; 
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : "";
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; 
    
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['requestuserId'] = $userId;
    $getUserHierarchy = getUserHierarchy($requestData);
    //print_r($getUserHierarchy); exit;
    if($getUserHierarchy['status'] == SCS){
        $finalUserId = array();
//        for($f=0;$f<count($getUserHierarchy['data']);$f++){
//            if($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'N'){
//                for($g=0;$g<count($getUserHierarchy['data'][$f]['userDetail']);$g++){
//                    if($getUserHierarchy['data'][$f]['userDetail'][$g]['lastLevelFlag'] == 'Y'){
//                        $finalUserId[$f] =  $getUserHierarchy['data'][$f]['userDetail'][$g]['userId'];
//                    }
//                }
//            }elseif($getUserHierarchy['data'][$f]['lastLevelFlag'] == 'Y'){
//                $finalUserId[$f] = $getUserHierarchy['data'][$f]['userId'];
//            }
//        }
        foreach($getUserHierarchy['data'] as $keyUH => $valueUH){
            if($valueUH['lastLevelFlag'] == 'N'){               
                foreach($valueUH['userDetail'] as $keyUD => $valueUD){
                    if($valueUD['lastLevelFlag'] == 'Y'){
                        $finalUserId[$keyUH] =  $valueUD['userId'];
                    }
                }
            }elseif($valueUH['lastLevelFlag'] == 'Y'){
                $finalUserId[$keyUH] = $valueUH['userId'];
            }
        }
        //print_r($finalUserId); exit;     
        $userIds = implode(",", $finalUserId);
//        $where = " AND ASSIGN_TO IN(" . $userIds . ") ";
    }else{
        $userIds = $userId;
    }
    
    $calenderData = activityCalenderData($postData,$userIds);
    if($calenderData['status']==SCS){
        
        $result = array("status"=>SCS,"data"=>$calenderData['data']);
        http_response_code(200);
        
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    
    return $result;
}


//ADDED BY DARSHAN PATEL ON 9/11/2017 FOR GET ACT COUNT MONTH WISE
function activityCalenderData($postData,$userIds = ""){
    
    $postdata = $postData['postData']; //POST DATA  
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; 
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : "";
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; 
    $actId = isset($postdata['actId']) ? addslashes(trim($postdata['actId'])) : "";  // ADDED BY DEEPAK ON 23_FEB-2018 FOR CALENDER LISTING 
//    $monthId = isset($postdata['monthId']) ? addslashes(trim($postdata['monthId'])) : ""; 
     
    $whereUserId = "";
    if(!empty($userIds) && $userIds != ""){
        $whereUserId = " AND ACT.ASSIGN_TO IN (".$userIds.") ";
    }
    $whereActId = "";
    if(!empty($actId) && $actId != ""){
        $whereActId = " AND ACT.PK_ACT_ID IN (".$actId.") ";
    }
    
    $sqlActCount = "SELECT ACT.PK_ACT_ID,ACT.C_ACT_ID,ACT.ACT_STATUS,ACT.DUE_DATE,ACT.DUE_TIME,ACT.REMINDER_ON,ACT.ACT_TITLE,ACT.ACT_DESC,IF('ACT.ACT_STATUS' = '0','Pending',IF('ACT.ACT_STATUS' = '1','InProcess','Closed')) ACT_STATUS_NAME,ACT.FK_CUST_ID,ACT.ACT_MODE,ACT.CREATED_BY,ACT.CREATED_BY_DATE,CONCAT(CM.FIRST_NAME,' ',CM.LAST_NAME) CUTOMERDETAIL,CM.CUSTMOBILE,UM.FULLNAME,IF(ACT.FK_CUST_ID != 0 , (SELECT IF(LAST_ACT_DATE != '0000-00-00', DATE_FORMAT(LAST_ACT_DATE,'%d-%m-%Y'),'--') FROM ".CUSTOMER." WHERE PK_CUST_ID = ACT.FK_CUST_ID LIMIT 0,1 ),'--') LASTACTIVTYDATE,UM1.FULLNAME CREATED_BY,ALM.VALUE ACTMODE FROM ".ACTIVITYMASTER_ACT." ACT INNER JOIN ".USERMASTER." UM ON UM.PK_USER_ID = ACT.ASSIGN_TO AND UM.DELETE_FLAG = '0' INNER JOIN " . USERMASTER . " UM1 ON UM1.PK_USER_ID = ACT.CREATED_BY AND UM1.DELETE_FLAG = '0' INNER JOIN " . ACTIVITYLOOKUPMASTER . " ALM ON ALM.PK_ACT_LOOKUP_ID = ACT.ACT_MODE LEFT JOIN ".CUSTOMER." CM ON CM.PK_CUST_ID = ACT.FK_CUST_ID AND CM.DELETE_FLAG = '0' WHERE ACT.DELETE_FLAG = '0' AND (ACT.DUE_DATE != '' OR ACT.DUE_DATE != '0000-00-00') AND ACT.FK_CLIENT_ID = '".$clientId."' AND ACT.FK_ORG_ID = '".$orgId."'  ".$whereUserId." ".$whereActId."  ";
    
    $resultActCount = fetch_rec_query($sqlActCount);
//    print_r($resultActCount);exit;
    if(count($resultActCount)>0){
        $finalJson = array();
        foreach ($resultActCount as $keyAct => $valueAct) {
            
            $finalJson[$keyAct]['ACT_STATUS'] = $valueAct['ACT_STATUS'];
            $finalJson[$keyAct]['DUE_DATE'] = $valueAct['DUE_DATE'];
            $finalJson[$keyAct]['DUE_TIME'] = $valueAct['DUE_TIME'];
            $finalJson[$keyAct]['REMINDER_ON'] = $valueAct['REMINDER_ON'];
            $finalJson[$keyAct]['ACT_TITLE'] = $valueAct['ACT_TITLE'];
            $finalJson[$keyAct]['ACT_DESC'] = $valueAct['ACT_DESC'];
            $finalJson[$keyAct]['PK_ACT_ID'] = $valueAct['PK_ACT_ID'];
            $finalJson[$keyAct]['C_ACT_ID'] = $valueAct['C_ACT_ID'];
            $finalJson[$keyAct]['CREATEDBY'] = $valueAct['CREATED_BY'];
            $finalJson[$keyAct]['CREATEDTIME'] = !empty($valueAct['CREATED_BY_DATE']) ?  date("d-m-Y h:i:s A",($valueAct['CREATED_BY_DATE'])): "--";
            $finalJson[$keyAct]['ACT_MODE'] = $valueAct['ACT_MODE'];
            $finalJson[$keyAct]['ACTMODE'] = $valueAct['ACTMODE'];
            $finalJson[$keyAct]['FK_CUST_ID'] = $valueAct['FK_CUST_ID'];
            $finalJson[$keyAct]['CUTOMERDETAIL'] = !empty($valueAct['CUTOMERDETAIL']) ? $valueAct['CUTOMERDETAIL'] : "";
            $finalJson[$keyAct]['PHONE_NO'] = !empty($valueAct['CUTOMERDETAIL']) ? $valueAct['CUSTMOBILE'] : "";
            $finalJson[$keyAct]['ACT_STATUS_NAME'] = $valueAct['ACT_STATUS_NAME'];
            $finalJson[$keyAct]['ASSIGNEDTO'] = $valueAct['FULLNAME'];
            $finalJson[$keyAct]['LASTACTIVTYDATE'] = (!empty($valueAct['LASTACTIVTYDATE']) && $valueAct['LASTACTIVTYDATE'] != '--') ? date("Y-m-d",strtotime($valueAct['LASTACTIVTYDATE'])) : "--";
            
        }
        $result = array("status"=>SCS,"data"=>$finalJson);
        http_response_code(200);
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    
    return $result;
}


function createScheduleSmartActivity($postData){
//    print_r($postData);
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; // USER ID    
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; // USER ID    
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // USER ID  
    $schedulerActTitle = isset($postData['postData']['schedulerActTitle']) ? addslashes(trim($postData['postData']['schedulerActTitle'])) : ""; // USER ID  
    $schedulerActDesc = isset($postData['postData']['schedulerActDesc']) ? addslashes(trim($postData['postData']['schedulerActDesc'])) : ""; // USER ID  
    $schedulerActMode = isset($postData['postData']['schedulerActMode']) ? addslashes(trim($postData['postData']['schedulerActMode'])) : ""; // USER ID  
    $schedulerTypeList = isset($postData['postData']['schedulerTypeList']) ? addslashes(trim($postData['postData']['schedulerTypeList'])) : ""; // USER ID  
    $frequencyType = isset($postData['postData']['schedulerActFreq']) ? addslashes(trim($postData['postData']['schedulerActFreq'])) : ""; // FREQUENCY TYPE  
    $today = date("Y-m-d");
//    print_r($postData['postData']);exit;
    if( (!empty($postData['postData']['customerName']) && $postData['postData']['customerName'] != '') || (!empty($postData['postData']['customerGroup']) && $postData['postData']['customerGroup'] != '') || (!empty($postData['postData']['custType']) && $postData['postData']['custType'] != '') || (!empty($postData['postData']['catchArea']) && $postData['postData']['catchArea'] != '') || (!empty($postData['postData']['birthdate']) && $postData['postData']['birthdate'] != '') || (!empty($postData['postData']['anniversaryDate']) && $postData['postData']['anniversaryDate'] != '') || (!empty($postData['postData']['ethnicity']) && $postData['postData']['ethnicity'] != '') || (!empty($postData['postData']['countryId']) && $postData['postData']['countryId'] != '') || (!empty($postData['postData']['itemId']) && $postData['postData']['itemId'] != '') ) {
 
           
        $postData['postData']['schedulerActDesc'] = '';
        $postData['postData']['schedulerActTitle'] = '';

        if(!empty($userId) && $userId != ""){

            $insertData = array();
            $insertData['FK_CLIENT_ID'] = $clientId;
            $insertData['FK_ORG_ID'] = $orgId;
            $insertData['FK_USER_ID'] = $userId;
            $insertData['DATE'] = $today;
    //        $insertData['LAST_ACT_DATE'] = $today;
            $insertData['ACT_TITLE'] = $schedulerActTitle;
            $insertData['ACT_DESC'] = $schedulerActDesc;
            $insertData['ACT_MODE'] = $schedulerActMode;
            $insertData['ACT_TYPE'] = $schedulerTypeList;

            if($frequencyType == '1'){
                $insertData['FREQUENCY_TYPE'] = "Daily";
            }else if($frequencyType == '7'){
                $insertData['FREQUENCY_TYPE'] = 'Weekly';       
            }else if($frequencyType == '30'){
                $insertData['FREQUENCY_TYPE'] = 'Monthly';
            }else if($frequencyType == '90'){
                $insertData['FREQUENCY_TYPE'] = 'Quaterly';
            }else if($frequencyType == '180'){
                $insertData['FREQUENCY_TYPE'] = 'Half Yearly';
            }else if($frequencyType == '365'){
                $insertData['FREQUENCY_TYPE'] = 'Yearly';
            }

            $insertData['FILTER_JSON_DATA'] = json_encode($postData);
            $insertData['CREATED_BY'] = $userId;
            $insertData['CREATED_BY_DATE'] = time();
            $insertData['DELETE_FLAG'] = '0';

            $insertReq = insert_rec(SMARTACT_SHEDULE, $insertData);
            if($insertReq){
                $result = array("status"=>SCS);
                http_response_code(200);
            }else{
                $result = array("status"=>INSERTFAIL);
                http_response_code(400);
            }

        }else{
            $result = array("status"=>NORECORDS);
            http_response_code(400);
        }
    }else{
        $msg = "Please select atleast one smart option to create scheduler";
        $result = array("status"=>$msg);
        http_response_code(400);
    }
    
    return $result;
    
}

function updateLastActDateOnCustomer($actId){
//    $sqlGetCustomer = "SELECT * FROM ".ACTIVITYMASTER_ACT." ACT INNER JOIN ".CUSTOMER." CM ON CM.PK_CUST_ID = ACT.FK_CUST_ID WHERE ACT.PK_ACT_ID = '".$actId."' LIMIT 0,1";
    $sqlGetCustomer = "SELECT CM.PK_CUST_ID, ACT.PK_ACT_ID FROM ".ACTIVITYMASTER_ACT." ACT INNER JOIN ".CUSTOMER." CM ON CM.PK_CUST_ID = ACT.FK_CUST_ID WHERE ACT.PK_ACT_ID = '".$actId."' LIMIT 0,1";
    $resultGetCustomer = fetch_rec_query($sqlGetCustomer);
//    print_r($resultGetCustomer);exit;
    if(count($resultGetCustomer) > 0){ // START UPDATING LAST ACT DATE
        
        $updateCustomer = array();
        $updateCustomer['LAST_ACT_DATE'] = date("Y-m-d");
        
        $whereCustomerCond = " PK_CUST_ID =  '".$resultGetCustomer[0]['PK_CUST_ID']."' " ;
        $update_status = update_rec(CUSTOMER, $updateCustomer, $whereCustomerCond);
        
    }
}

//ALLOCATION HISTORY OF ACTIVITY
function getActivityAssignmentReport($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $actId = isset($postData['postData']['actId']) ? addslashes(trim($postData['postData']['actId'])) : '0';
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    
    $sqlGetInqAllocation = "SELECT IFNULL(GROUP_CONCAT(CONVERT(CONCAT_WS('|---|',FK_USER_ID,CREATED_BY),CHAR(500)) ORDER BY PK_INQ_ASSIGN_ID ASC),'') USERIDS,IFNULL(GROUP_CONCAT(CONVERT(CREATED_BY_DATE,CHAR(500)) ORDER BY PK_INQ_ASSIGN_ID ASC),'') CREATEDON,IFNULL(GROUP_CONCAT(CONVERT(CHANGED_BY_DATE,CHAR(500)) ORDER BY PK_INQ_ASSIGN_ID ASC),'') CHANGEON,IFNULL(FK_INQ_ID,'') FK_INQ_ID,GROUP_CONCAT(IAM.DELETE_FLAG) DELETEFLAG FROM ".INQALLOCATIONMASTER." IAM WHERE FK_INQ_ID IN (".$actId.") AND FK_CLIENT_ID = '".$clientId."' AND ASSIGN_TYPE = '".$type."'";
    $resultInqAllocation = fetch_rec_query($sqlGetInqAllocation);
//    print_r($resultInqAllocation); exit;
    if(count($resultInqAllocation) > 0){
        
        foreach($resultInqAllocation as $keyAllocation =>$valueAllocation){
            
            if(!empty($valueAllocation['USERIDS']) && $valueAllocation['USERIDS'] !="" && $valueAllocation['FK_INQ_ID'] != "" && !empty($valueAllocation['FK_INQ_ID'])){
                
                $explodeUserIds = explode(",",$valueAllocation['USERIDS']);
                $explodecreateOn = explode(",",$valueAllocation['CREATEDON']);
                $explodeChangeOn = explode(",",$valueAllocation['CHANGEON']);
                $explodeDeleteFlag = explode(",",$valueAllocation['DELETEFLAG']);
                
                $innerJson = array();
                foreach($explodeUserIds as $keyUserId=>$valueUserId){
                    //print_r($valueUserId); exit;
                    $userDetailExplode = explode("|---|",$valueUserId);
                    $sqlGetUser = "SELECT FULLNAME FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$userDetailExplode[0]."' AND FK_CLIENT_ID = '".$clientId."' LIMIT 0,1 ";
                    $resultGetUser = fetch_rec_query($sqlGetUser);
                    //print_r($resultGetUser); exit;
                    if($keyUserId == 0){
                        $sqlGetUserCreated = "SELECT FULLNAME FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$userDetailExplode[1]."' AND FK_CLIENT_ID = '".$clientId."' LIMIT 0,1 ";
                        $resultGetUserCreated = fetch_rec_query($sqlGetUserCreated);
                        $prevUser = $resultGetUser[0]['FULLNAME'];
                        $prevChangeBy = !empty($explodeChangeOn[$keyUserId]) ? $explodeChangeOn[$keyUserId] : $explodecreateOn[$keyUserId];
                        $innerJson[$keyUserId]['inqCreatedText'] = "Actvity Assign To '".$resultGetUser[0]['FULLNAME']."' On ".date("d-m-Y h:i:s A",$explodecreateOn[$keyUserId])." by '".$resultGetUserCreated[0]['FULLNAME']."' On ".date("d-m-Y h:i:s A",$prevChangeBy)."";
                        $innerJson[$keyUserId]['inqAssignBY'] = $resultGetUserCreated[0]['FULLNAME'];
                        $innerJson[$keyUserId]['inqAssignByOn'] = date("d-m-Y h:i:s A", $explodecreateOn[$keyUserId]);
                        $innerJson[$keyUserId]['inqAssignTo'] = $resultGetUser[0]['FULLNAME'];
                        $innerJson[$keyUserId]['inqAssignToOn'] = date("d-m-Y h:i:s A",$explodecreateOn[$keyUserId]);
                        $innerJson[$keyUserId]['currentOwner'] = ($explodeDeleteFlag[$keyUserId] == '0') ? '1' : '0';
                    }else{
                        $innerJson[$keyUserId]['inqCreatedText'] = "Actvity Assign To '".$resultGetUser[0]['FULLNAME']."' On ".date("d-m-Y h:i:s A",$explodecreateOn[$keyUserId])." by '".$prevUser."' On ".date("d-m-Y h:i:s A",$prevChangeBy)." ";
                        $innerJson[$keyUserId]['inqAssignBY'] = $prevUser;
                        $innerJson[$keyUserId]['inqAssignByOn'] = date("d-m-Y h:i:s A",$prevChangeBy);
                        $innerJson[$keyUserId]['inqAssignTo'] = $resultGetUser[0]['FULLNAME'];
                        $innerJson[$keyUserId]['inqAssignToOn'] = date("d-m-Y h:i:s A",$explodecreateOn[$keyUserId]);
                        $innerJson[$keyUserId]['currentOwner'] = ($explodeDeleteFlag[$keyUserId] == '0') ? '1' : '0';
                        $prevUser = $resultGetUser[0]['FULLNAME'];
                        $prevChangeBy = $explodeChangeOn[$keyUserId];
                    }
                }
            }
        }
        if(!empty($innerJson)){
            $result =  array("status"=>SCS,"data"=>  array_reverse(array_values($innerJson)));
            http_response_code(200);
        }else{
            $result =  array("status"=>NORECORDS);
            http_response_code(400);
        }
    }else{
        $result =  array("status"=>NORECORDS);
        http_response_code(400);
    }
    
    return $result;
    
}


//  SMART SHEDULE ACT LISTING ADDED BY DHRUV ON 25-01-2018
function ScheduleSmartActivityListing($postData){
//    print_r($postData);exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; // ORG ID    
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // USER ID  
        
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['requestuserId'] = $userId;
    $getUserHierarchy = getUserHierarchy($requestData);
//        print_r($getUserHierarchy); exit;
    if($getUserHierarchy['status'] == SCS){
        $finalUserId = array();
        // START - CHANGES MADE BY DEEPAK PATIL ON 02-01-2018
        foreach($getUserHierarchy['data'] as $keyData => $valueData){
            if($valueData['lastLevelFlag'] == 'N'){
                foreach($valueData['userDetail'] as $keyUser => $valueUser){
                    if($valueUser['lastLevelFlag'] == 'Y'){
                        $finalUserId[$keyData] =  $valueUser['userId'];
                    }
                }
            }else if($valueData['lastLevelFlag'] == 'Y'){
                $finalUserId[$keyData] = $valueData['userId'];
            }
        }             
        $userIds = implode(",", $finalUserId);  
    }        
    
    $sqlGetAct = "SELECT UM.FULLNAME, SS.PK_SMART_ACT_ID, SS.FILTER_JSON_DATA, SS.FREQUENCY_TYPE, SS.ACT_TITLE, SS.ACT_DESC, SS.ACT_MODE, SS.ACT_TYPE, DATE, SS.LAST_ACT_DATE FROM ".SMARTACT_SHEDULE." SS INNER JOIN ".USERMASTER." UM ON UM.PK_USER_ID = SS.FK_USER_ID AND UM.DELETE_FLAG = '0' WHERE SS.FK_CLIENT_ID = '".$clientId."' AND SS.FK_USER_ID IN (".$userIds.")  AND SS.DELETE_FLAG = '0' ORDER BY SS.PK_SMART_ACT_ID DESC";
    $resultGetAct = fetch_rec_query($sqlGetAct);
    
    if(count($resultGetAct) > 0){
        foreach($resultGetAct as $keyAct => $valueAct){
            $finalJson[$keyAct]['filterValue'] =  (!empty($valueAct['FILTER_JSON_DATA']) && $valueAct['FILTER_JSON_DATA'] != "")  ? objectToArray(json_decode($valueAct['FILTER_JSON_DATA'])) : "";
            $finalJson[$keyAct]['frequencyType'] = $valueAct['FREQUENCY_TYPE'];
            $finalJson[$keyAct]['smartId'] = $valueAct['PK_SMART_ACT_ID'];
            $finalJson[$keyAct]['actTiele'] = $valueAct['ACT_TITLE'];
            $finalJson[$keyAct]['actDesc'] = $valueAct['ACT_DESC'];
            $finalJson[$keyAct]['actMode'] = $valueAct['ACT_MODE'];
            $finalJson[$keyAct]['actType'] = $valueAct['ACT_TYPE'];
            $finalJson[$keyAct]['date'] = $valueAct['DATE'];
            $finalJson[$keyAct]['userName'] = $valueAct['FULLNAME'];
            $finalJson[$keyAct]['lastActDate'] = $valueAct['LAST_ACT_DATE'];
        }
        $result = array("status"=>SCS,"data"=> array_values($finalJson));
        http_response_code(200);

    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    
    return $result;
    
}

// SMART SHEDULE ACTIVITY UPDATE ADDED BY DHRUV ON 25-01-2018 
function editCreateScheduleSmartActivity($postData){
//    print_r($postData);exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; // ORG ID    
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // USER ID  
    $schedulerActTitle = isset($postData['postData']['schedulerActTitle']) ? addslashes(trim($postData['postData']['schedulerActTitle'])) : ""; //  FOR ACTIVITY TITLE
    $schedulerActDesc = isset($postData['postData']['schedulerActDesc']) ? addslashes(trim($postData['postData']['schedulerActDesc'])) : ""; //  FOR ACTIVITY DESCRIPTION
    $schedulerActMode = isset($postData['postData']['schedulerActMode']) ? addslashes(trim($postData['postData']['schedulerActMode'])) : ""; //  FOR ACTIVITY MODE
    $schedulerTypeList = isset($postData['postData']['schedulerTypeList']) ? addslashes(trim($postData['postData']['schedulerTypeList'])) : ""; // SHEUDLAR TYPE LIST 
    $frequencyType = isset($postData['postData']['schedulerActFreq']) ? addslashes(trim($postData['postData']['schedulerActFreq'])) : ""; // FREQUENCY TYPE  
    $smartId = isset($postData['postData']['smartId']) ? addslashes(trim($postData['postData']['smartId'])) : ""; // FREQUENCY TYPE  
    $today = date("Y-m-d");
    
    //FOR JSON ENCODE FOR THE SPECIAL CHAR TO BLANK
    $postData['postData']['schedulerActDesc'] = "";
    $postData['postData']['schedulerActTitle'] = "";
    
    if(!empty($userId) && $userId != ""){
        
        $updateData = array();
        $updateData['DATE'] = $today;
//        $updateData['LAST_ACT_DATE'] = $today;
        $updateData['ACT_TITLE'] = $schedulerActTitle;
        $updateData['ACT_DESC'] = $schedulerActDesc;
        $updateData['ACT_MODE'] = $schedulerActMode;
        $updateData['ACT_TYPE'] = $schedulerTypeList;
        
        if($frequencyType == '1'){
            $updateData['FREQUENCY_TYPE'] = "Daily";
        }else if($frequencyType == '7'){
            $updateData['FREQUENCY_TYPE'] = 'Weekly';       
        }else if($frequencyType == '30'){
            $updateData['FREQUENCY_TYPE'] = 'Monthly';
        }else if($frequencyType == '90'){
            $updateData['FREQUENCY_TYPE'] = 'Quaterly';
        }else if($frequencyType == '180'){
            $updateData['FREQUENCY_TYPE'] = 'Half Yearly';
        }else if($frequencyType == '365'){
            $updateData['FREQUENCY_TYPE'] = 'Yearly';
        }
       
        $updateData['FILTER_JSON_DATA'] = json_encode($postData);
        $updateData['CHANGED_BY'] = $userId;
        $updateData['CHANGED_BY_DATE'] = time();
        $updateData['DELETE_FLAG'] = '0';
//        print_r($updateData);exit;
        $updateWhere = "PK_SMART_ACT_ID = '".$smartId."' AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$clientId."' ";
//        print_r($updateWhere);exit;
        $updateE = update_rec(SMARTACT_SHEDULE, $updateData, $updateWhere);       
        if($updateE){
            $result = array("status"=>SCS);
            http_response_code(200);
        }else{
            $result = array("status"=>UPDATEFAIL);
            http_response_code(400);
        }
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    
    return $result;
    
} 

//SMART SHEDULE ACTIVITY DELETE ADDED BY DHRUV ON 25-01-2018
function deleteCreateScheduleSmartActivity($postData){
//    print_r($postData);exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; // CLIENT ID    
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; // ORG ID    
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // USER ID  
    $schedulerActTitle = isset($postData['postData']['schedulerActTitle']) ? addslashes(trim($postData['postData']['schedulerActTitle'])) : ""; //  FOR ACTIVITY TITLE
    $schedulerActDesc = isset($postData['postData']['schedulerActDesc']) ? addslashes(trim($postData['postData']['schedulerActDesc'])) : ""; //  FOR ACTIVITY DESCRIPTION
    $schedulerActMode = isset($postData['postData']['schedulerActMode']) ? addslashes(trim($postData['postData']['schedulerActMode'])) : ""; //  FOR ACTIVITY MODE
    $schedulerTypeList = isset($postData['postData']['schedulerTypeList']) ? addslashes(trim($postData['postData']['schedulerTypeList'])) : ""; // SHEUDLAR TYPE LIST 
    $frequencyType = isset($postData['postData']['schedulerActFreq']) ? addslashes(trim($postData['postData']['schedulerActFreq'])) : ""; // FREQUENCY TYPE  
    $smartId = isset($postData['postData']['smartId']) ? addslashes(trim($postData['postData']['smartId'])) : ""; // FREQUENCY TYPE  
    $today = date("Y-m-d");
      
    $updateActArray = array();
    $updateActArray['DELETE_FLAG'] = 1;
    $updateActArray['CREATED_BY_DATE'] = time();
    $updateActArray['CHANGED_BY'] = $userId;
   
    $updateActArrayWhere = "PK_SMART_ACT_ID = '".$smartId."' AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '".$clientId."' "; 
    $updateAD = update_rec(SMARTACT_SHEDULE ,$updateActArray, $updateActArrayWhere);
    if($updateAD){
        $result = array("status"=>SCS);
        http_response_code(200);
    }else{
        $result = array("status"=>UPDATEFAIL);
        http_response_code(400);
    }
        
    return $result; 
}

?>