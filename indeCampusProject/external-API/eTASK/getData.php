<?php

/* * **************************************** */
/*    AUTHOR : VAIBHAV PATEL                  */
/*    CREATION DATE : 21-Apr-2016             */
/*    FILE TYPE : PHP                         */ 
/*    PROJECT : eTASK                         */ 
/*    PURPOSE : ACTIVITES MODULE              */
/* * **************************************** */

//if (isset($_SERVER['HTTP_ORIGIN'])) {
//     // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
//     // you want to allow, and if so:
//     header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
//     header('Access-Control-Allow-Credentials: true');
//     header('Access-Control-Max-Age: 86400');    // cache for 1 day
// }
//
//// // Access-Control headers are received during OPTIONS requests
// if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
//
//     if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
//         // may also be using PUT, PATCH, HEAD etc
//         header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
//
//     if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
//         header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
//
//     exit(0);
// }

// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
ini_set("display_errors", 1); // PLEASE CHANGE THIS VALUE TO 0 BEFORE PRODUCTION
// START Include Required File...

require_once "../../include/globals.php"; // DB CLASS FILE

//$mail = new PHPMailer(); // CALL PHPMAILER CLASS
require_once "../../include/function_userManagment_potg.php"; // INQUIRY FUNCTION FILE (INQ WITH CUSTOMER / FOLLOWUP / INQ )
require_once "../../include/function_CRM_potg.php"; // INQUIRY FUNCTION FILE (INQ WITH CUSTOMER / FOLLOWUP / INQ )
require_once "../../include/function_alertManagement_potg.php"; // INQUIRY FUNCTION FILE (INQ WITH CUSTOMER / FOLLOWUP / INQ )
require_once "../../include/function_document_potg.php"; // INQUIRY FUNCTION FILE (INQ WITH CUSTOMER / FOLLOWUP / INQ )
require_once "../../include/function_inquiry_potg.php"; // INQUIRY FUNCTION FILE (INQ WITH CUSTOMER / FOLLOWUP / INQ )
require_once "../../include/function_target_potg.php"; // INQUIRY FUNCTION FILE (INQ WITH CUSTOMER / FOLLOWUP / INQ )
require_once "include/function_activity.php"; // BILLING FILE
require_once "../../include/function_Google_Cal_API.php"; // GOOGLE CALANDER FILE
require_once "../../include/function_developerMaster_potg.php";
// END Include Required File...


$postData = isset($_POST) ? $_POST : ""; // POST DATA
//print_r($postData['postData']['requestCase']);die;
//print_r($_FILES);

 if(isset($_FILES['actAttachment']) || is_object($postData)){
    
    $postdata = objectToArray(json_decode($postData['postData'])); 
    //print_r($postData); die;
    $requestCase = $postdata['requestCase'];  
    $postData['postData'] = $postdata;  
//    print_r($postData); die;
    $postFile = $_FILES['actAttachment'];
    //print_r($postFile); exit;
    //$tempPostData['postData'] = $postData;
}else{
    //$tempPostData = $postData;
    $requestCase = isset($postData['postData']['requestCase']) ? $postData['postData']['requestCase'] : ""; // REQUEST CASE
    //$postdata = $postData;
}

//FOR CHECK JWT TOKEN 
$checkSignature = checkJWTToken($postData);
//print_r($checkSignature); exit;
if($checkSignature['status'] == SCS){

    if ($requestCase != "") {

        //FOR ADD API LOG DATA OF THIS USER TO STORE THE REQUEST
        apiRequestLogData($postData['postData']['clientId'],$postData['postData']['orgId'], $postData['postData']['userId'],$requestCase,$postData);
        
        switch ($requestCase) {

            case "createActivity":  // CREATE ACTIVITY

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create           
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = createUpdateActivity($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "updateActivity":  // UPDATE ACTIVITY            
                $moduleName      = "2"; //Activity
                $activityName   = "7"; //Edit

                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = createUpdateActivity($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;  


            case "getActListing":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getActListing($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;        


            case "getActivityDetails":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getActivityDetails($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break; 


            case "insertCommentInAct":  

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = insertCommentInAct($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "updateCommentInAct":  

                $moduleName      = "2"; //Activity
                $activityName   = "7"; //Edit
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = updateCommentInAct($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "startActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "7"; //Edit
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = startActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "closeActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "7"; //Edit
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = closeActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "getUserCustomerGroup":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getUserCustomerGroup($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "getUserAuthForActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getUserAuthForActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "getActivityListingByCreatedBy":  //GET ACTIVITY LISTING FOR ONLY CREATED BY
    //            $result = getActivityListingByCreatedBy($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "getActivityListingByCreatedBy":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getActivityListingByCreatedBy($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "closeAndCreateNewActivity":  //CLOSE AND CREATE NEW ACTIVITY
    //            $result = closeAndCreateNewActivity($postData,$postFile);
    //            $jsonData = json_encode($result);
    //            break;

            case "closeAndCreateNewActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = closeAndCreateNewActivity($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "transferActivity":  //TRANSFER ACTIVITY
    //            $result = transferActivity($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "transferActivity":  // TRANSFER ACTIVITY

                $moduleName      = "2"; //Activity
                $activityName   = "109"; //Transfer
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = transferActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "assignActivity":  //ASSIGN ACTIVITY
    //            $result = assignActivity($postData);
    //            $jsonData = json_encode($result);
    //            break;   
            case "assignActivity":  // ASSIGN ACTIVITY

                $moduleName      = "2"; //Activity
                $activityName   = "107"; //Assign
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = assignActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;

            //START CASES FOR CUSTOM ACTIVITY
    //        case "getCustomActListing":  //CUSTOM ACTIVITY LISTING
    //            $result = getCustomActListing($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "getCustomActListing":  // CUSTOM ACTIVITY LISTING

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getCustomActListing($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "createCustomActivity":  //CREATE CUSTOM ACTIVITY
    //            $result = createUpdateCustomActivity($postData,$postFile);
    //            $jsonData = json_encode($result);
    //            break;

            case "createCustomActivity":  //CREATE CUSTOM ACTIVITY

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = createUpdateCustomActivity($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


            case "updateCustomActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "7"; //Edit
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = createUpdateCustomActivity($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "getUserGroupListing":  //GET USER GROUP LISTING
    //            $result = getUserGroupListing($postData);
    //            $jsonData = json_encode($result);
    //            break;   

            case "getUserGroupListing":   //GET USER GROUP LISTING

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getUserGroupListing($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "getCustomerGroupListing":  //GET CUSTOMER GROUP LISTING
    //            $result = getCustomerGroupListing($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "getCustomerGroupListing":  //GET CUSTOMER GROUP LISTING

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getCustomerGroupListing($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;

    //        case "getUserListing":  //GET USER LISTING
    //            $result = getUserListing($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "getUserListing":  //GET USER LISTING

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getUserListing($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "getCustomerForSmartAct":  //CREATE SMART ACTIVITY 
    //            $result = getCustomerForSmartAct($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "getCustomerForSmartAct":  //CREATE SMART ACTIVITY 

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getCustomerForSmartAct($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;

    //        case "createSmartActivity":  //CREATE SMART ACTIVITY 
    //            $result = createSmartActivity($postData,$postFile);
    //            $jsonData = json_encode($result);
    //            break;

            case "createSmartActivity":  //CREATE SMART ACTIVITY 

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = createSmartActivity($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "getActDetailBreakUp":  //CREATE SMART ACTIVITY 
    //            $result = getActDetailBreakUp($postData,$postFile);
    //            $jsonData = json_encode($result);
    //            break;

            case "getActDetailBreakUp":  

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getActDetailBreakUp($postData,$postFile);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "getActDetailOnDueDateTime":  //CREATE SMART ACTIVITY 
    //            $result = getActDetailOnDueDateTime($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "getActDetailOnDueDateTime":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = getActDetailOnDueDateTime($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "activityCalenderData":  //GET ACT COUNT MONTH WISE ADDED BY DARSHAN 9/11/17S
    //            $result = activityCalenderData($postData);
    //            $jsonData = json_encode($result);
    //            break;

            case "activityCalender":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //View
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = activityCalender($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;


    //        case "createScheduleSmartActivity":  //GET ACT COUNT MONTH WISE ADDED BY DARSHAN 9/11/17
    //            $result = createScheduleSmartActivity($postData);
    //            $jsonData = json_encode($result);
    //            break;
                
            case "createScheduleSmartActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "5"; //Create
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = createScheduleSmartActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;
                
                
            case "ScheduleSmartActivityListing":  

                $moduleName      = "2"; //Activity
                $activityName   = "6"; //listing
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = ScheduleSmartActivityListing($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;
                
                
            case "editCreateScheduleSmartActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "7"; //edit
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = editCreateScheduleSmartActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;
                
                
            case "deleteCreateScheduleSmartActivity":  

                $moduleName      = "2"; //Activity
                $activityName   = "8"; //delete
                $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'],$postData['postData']['orgId']);
                if($checkAuth['status'] == SCS){
                    if($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']){
                        $result = deleteCreateScheduleSmartActivity($postData);
                    }else{
                        $result = array("status"=>ILLIGELACCESS);
                        $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                        $DData['OrgMasterReason'] = "Org Master Error";                            
                        commonFunctionforAuthError_BL($postData,$DData);
                    }
                }else{
                    $result = array("status"=>ILLIGELACCESS);
                    $DData['ModuleId'] = $moduleName;
                    $DData['ModuleName'] = $moduleId;
                    $DData['ActvitiyId'] = $activityName;         
                    $DData['ActvitiyName'] = $activityId;         
                    $DData['Reason'] = "Unauthorized Access";         
                    commonFunctionforAuthError_BL($postData,$DData);
                }

                $jsonData = json_encode($result);
                break;
        }
    }else{
        $jsonData = "CASE ERROR";
    }
}else{
    $jsonData = json_encode(array("status"=>$checkSignature['status']));
}

/* ======= RETURN DATA AFTER REQUESTING BY ANY CALL FROM API ========= */
if (isset($jsonData)) {
    // if return json
    header("Authorization: Bearer ".trim($checkSignature['data']));
    header("Content-Type: application/x-www-form-urlencoded; charset=UTF-8");
    echo $jsonData;
} else {
    //if json is not returned
    $result = array('status' => FAIL);
    echo json_encode($result);
}
?>