<?php
require_once('loader.php');

// return json response 
$json = array();

$nameUser  = $_POST["name"];
$nameEmail = $_POST["email"];
$gcmRegID  = $_POST["regId"]; // GCM Registration ID got from device
$userId  = $_POST["PK_USER_ID"]; // GCM Registration ID got from device
$appdata=  explode("_", $_POST["APP_NAME"]);
$appName  = $appdata[0]; // GCM Registration ID got from device
$deviceType  = $appdata[1]; // GCM Registration ID got from device
$loginsession  = $_POST["LOGINSESSIONID"]; // GCM Registration ID got from device

/**
 * Registering a user device in database
 * Store reg id in users table
 */
if (isset($nameUser) && isset($nameEmail) && isset($gcmRegID)) {
    
	// Store user details in db
    $res = storeUser($nameUser, $nameEmail, $gcmRegID,$userId,$appName,$deviceType,$loginsession);

    $result=array("deviceId"=>$gcmRegID,"response"=>$res);
    $jsonData = json_encode($result);

    echo $jsonData;
} else {
    $result=array("response"=>"DATA MISSING");
    $jsonData = json_encode($result);
    // user details not found
}
?>