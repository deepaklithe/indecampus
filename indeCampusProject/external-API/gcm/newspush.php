<?php
date_default_timezone_set('Asia/Kolkata'); 
// Put your device token here (without spaces):
//$deviceToken = '4c9681dcf627b6edf837b91b9b5b33eb0a166c05e28c38d3077dbeeff957223b';

// Put your private key's passphrase here:
$passphrase = '';

// Put your alert message here:
// Put your alert message here:
//$message = $argv[1]; 
//$url = $argv[2];
$deviceToken    = $_GET["regId"]; // GCM Registration ID got from device
$message = $_GET["message"];
$url = "www.lithe.in";
$template=array( 'title'=>'App Push at '.date('d-m-Y H:i:s'),
                                        'description'=>$message,
                                        'user_id'=>'1',
                                        'notification_type'=>'app',
                                        'date'=>date('Y-m-d'),
                                        'time'=>date('H:i:s')
                                      );
	$message = json_encode($template);

if (!$message || !$url)
    exit('Example Usage: $php newspush.php \'Breaking News!\' \'https://raywenderlich.com\'' . "\n");

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
  'ssl://gateway.push.apple.com:2195', $err,
  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
  'alert' => 'App Push at '.date('d-m-Y H:i:s'),
  'sound' => 'default',
  'link_url' => $url,
  'category' => "NEWS_CATEGORY",
  );
$body['sentfrom']=$message;

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
  echo 'Message not delivered' . PHP_EOL;
else
  echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
