<?php

function fetch_rec_query($f_qry, $f_shoqry = 0) { //fetch records of a query (fields,tablename,where and order by)
    global $db;
    
    $fresarr  = array();
    $fresarr = $db->get_results($f_qry);
    return $fresarr;
}

function onlyQuery($f_qry, $f_shoqry = 0) {
    global $db;
    
    $fresarr  = array();
    $fresarr = $db->query($f_qry);
    return $fresarr;
}
// ADDED BY KAVITA PATEL ON 10-05-2018 FOR MULTI INSERT DATA START
function insert_multi_rec($f_tbl,$f_fldkey, $f_fldvals, $f_excl = array(), $f_stdfld = 0) { //insert records
    global $db;
    
    $insert = $db->insert_multi($f_tbl,$f_fldkey,$f_fldvals);
    $f_fldsnot['lastInsertedId'] = ($insert > 0) ? $insert : 0;
    return $f_fldsnot;
}
// ADDED BY KAVITA PATEL ON 10-05-2018 FOR MULTI INSERT DATA END
// COMMENTED BY KAUSHA SHAH ON 14-08-2018 START
//function insert_rec($f_tbl, $f_fldvals, $f_excl = array(), $f_stdfld = 0) { //insert records
//    global $db;
//    
//    $insert = $db->insert($f_tbl,$f_fldvals);
//    $f_fldsnot['lastInsertedId'] = ($insert > 0) ? $insert : 0;
//    return $f_fldsnot;
//}
// COMMENTED BY KAUSHA SHAH ON 14-08-2018 END

// ADDED BY KAUSHA SHAH ON 14-08-2018 START
function insert_rec($f_tbl, $f_fldvals, $f_excl = array(), $f_stdfld = 0) { //insert records
    global $db;
    
    $f_fldsfnd = $f_fldsnot = array();
    $f_except = array();
    
    reset($f_excl);
    while (list($key, $val) = each($f_excl))
        $f_except[] = $val;
    
    
    $fdbres = $db->list_fields("SELECT * FROM $f_tbl LIMIT 0,1") ;
    for ($fi = 0; $fi < count($fdbres); $fi++) {
	if (!in_array($key, $f_except))
            $f_fldsfnd[$fdbres[$fi]->name] = $fdbres[$fi]->type;
    }
    
    $fdbsql = 'INSERT INTO ' . $f_tbl . '(';
    $finstr = '';
    reset($f_fldvals);
    
    while (list($key, $val) = each($f_fldvals)) {
        if (isset($f_fldsfnd[$key])) {
            $fdbsql .= $key . ',';
            $finstr .= "'" . $val . "',";
        } else {
            $f_fldsnot[$key] = $val;
        }
    }

    $fdbsql = substr($fdbsql, 0, strlen($fdbsql) - 1);
    $finstr = substr($finstr, 0, strlen($finstr) - 1);
    $fdbsql .= ') VALUES (' . $finstr . ')';
    
    $query = $db->query( $fdbsql );
    $f_fldsnot['lastInsertedId'] = $db->lastid() ;
    
    return $f_fldsnot;
}
// ADDED BY KAUSHA SHAH ON 14-08-2018 END

// COMMENTED BY KAUSHA SHAH ON 14-08-2018 START
/*function update_rec($f_tbl, $f_fldvals, $f_whr = '') { //update records
    global $db;
    $fresarr = $db->update($f_tbl,$f_fldvals,$f_whr);
   return ($fresarr) ? 1 : 0;
}*/
// COMMENTED BY KAUSHA SHAH ON 14-08-2018 START

// ADDED BY KAUSHA SHAH ON 14-08-2018 START
function update_rec($f_tbl, $f_fldvals, $f_whr = '', $f_excl = array(), $f_stdfld = 0) { //update records
    
    global $db;
    
    $affectRows = "";
    $f_fldsfnd = $f_fldsnot = array();
    $f_except = array();
    
    reset($f_excl);
    while (list($key, $val) = each($f_excl))
        $f_except[] = $val;
    
    $fdbres = $db->list_fields("SELECT * FROM $f_tbl LIMIT 0,1") ;
    for ($fi = 0; $fi < count($fdbres); $fi++) {
	if (!in_array($key, $f_except))
            $f_fldsfnd[$fdbres[$fi]->name] = $fdbres[$fi]->type;
    }
    
    if ($f_stdfld == 1) {
        $f_fldvals['CHANGED_BY_DATE'] = time();
    }
    
    $fdbsql = 'UPDATE ' . $f_tbl . ' SET ';
    reset($f_fldvals);
    while (list($key, $val) = each($f_fldvals)) {
        if (isset($f_fldsfnd[$key]))
            $fdbsql .= "$key='" . $val . "',";
        else
            $f_fldsnot[$key] = $val;
    }


    $fdbsql = substr($fdbsql, 0, strlen($fdbsql) - 1);
    $fdbsql .= (!empty($f_whr)) ? " WHERE $f_whr" : '';
    $fdbres = $db->query($fdbsql);
    return $db->affected() ;
    
}
// ADDED BY KAUSHA SHAH ON 14-08-2018 END

function delete_rec($f_tbl, $f_whr = '') { //delete records
    global $db;
    
    $fdbres = $db->delete($f_tbl, $f_whr);
    return ($fdbres > 0) ? $fdbres : 0;
}

//remove slash (values retrieved from db)
function strip_slash($f_dbval) {

    //REMOVE SLASHES BEFORE SPECIAL CHARS
    if (is_array($f_dbval)) { //if it's an array
        while (list($key, $value) = each($f_dbval)) {
            $f_dbval[$key] = stripslashes($f_dbval[$key]);
        }
    } else { //if it's a variable
        $f_dbval = stripslashes($f_dbval);
    }

    return $f_dbval;
}

function strip_html($f_var, $f_slash = true) { //f_slash = false - no slashes added to special chars
    //STRIP HTML TAGS AND ESCAPE SPECIAL CHARS
    if (is_array($f_var)) { //if it's an array
        while (list($key, $value) = each($f_var)) {
            if (is_array($value)) { //if value within subscript is an array
                while (list($kee, $vel) = each($value)) {
                    $value[$kee] = trim(preg_replace('/<[^>]*>/', '', $vel));
                    $value[$kee] = preg_replace('/\|/', '', $value[$kee]); //remove pipe since this used in import/export of data (CSV file)
                    if ($f_slash == true)
                        $value[$kee] = addslashes($value[$kee]);
                }
            } else {
                $f_var[$key] = trim(preg_replace('/<[^>]*>/', '', $value));
                $f_var[$key] = preg_replace('/\|/', '', $f_var[$key]); //remove pipe since this used in import/export of data (CSV file)
                if ($f_slash == true)
                    $f_var[$key] = addslashes($f_var[$key]);
            }
        }
    } else { //if it's a variable
        $f_var = trim(preg_replace('/<[^>]*>/', '', $f_var));
        if ($f_slash == true)
            $f_var = addslashes($f_var);
    }
    return $f_var;
}

?>
