<?php

/*
 *
 * Created By : Vaibhav Patel.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 20-Feb-2018.
 * File : tallytrans_sync.
 * File Type : .php.
 * Project : ePOS
 * Purpose : tally_transaction_staging data sync 
 * */
ini_set("display_errors", 1);
set_time_limit(0);
ini_set('max_execution_time', 0); //0=NOLIMIT
ini_set('memory_limit', '-1');
$appWebRoot = $_SERVER['DOCUMENT_ROOT'];

//constants
define("LOCALDB", "indeCampus"); // DB NAME
define("LOCALHOSTNAME", "192.168.129.207"); // HOST NAME 
define("LOCALUSERNAME", "inde"); // HOST USERNAME
define("LOCALPASSWORD", "inde"); // HOST PASSWORD
//define("TALLYSYNCURL", "http://analytic.southeastasia.cloudapp.azure.com/tally/tally_cloud/sync_data/api_deltasync.php"); // ADDED BY VAIBHAV ON 23-09-2015 FOR GET SERVER PHASE 2 TABLES
define("TALLYSYNCURL", "http://192.168.129.205/development/epostally/tally_cloud/sync_data/api_deltasync.php");
define("TALLYTRANSSTAGING", "tally_transaction_staging"); // TALLY TRANSACTION TABLE
define("SCS", "Success");
define("FAIL", "Fail");
define("SEQUENCETABLE","sequencetable") ; // ADDED BY KAUSHA SHAH ON 04-04-2018
include "class_db.php"; // ADDED BY KAUSHA SHAH ON 16-04-2018
include "function_DL.php";

$db = new DB(LOCALHOSTNAME, LOCALUSERNAME, LOCALPASSWORD,LOCALDB);

$_GET['deltasynctrans'] = 'ALL';

function objectToArray($d) {
    if (is_object($d)) {
	// Gets the properties of the given object
	// with get_object_vars function
	$d = get_object_vars($d);
    }
    if (is_array($d)) {
	/*
	 * Return array converted to object
	 * Using __FUNCTION__ (Magic constant)
	 * for recursive call
	 */
	return array_map(__FUNCTION__, $d);
    } else {
	// Return array
	return $d;
    }
}

$postField = $_GET['deltasynctrans'];
$time = time();

$last_id = 0;
$deltaTransSyncUrl = TALLYSYNCURL;
$spaceId =isset($_GET['spaceid']) ? $_GET['spaceid'] : 1;  // CHANGED BY KAUSHA SHAH ON 12-04-2018;
$custId = 1;
$pId = isset($_GET['projid']) ? $_GET['projid'] : 1; // 1 = epos // CHANGED BY KAUSHA SHAH ON 12-04-2018;

function generateIdSyncTrans($tablename) {//(tablename,1st two word for ID, Spaceid))
    $getdata = fetch_rec_query("SELECT SEQ_VALUE FROM " . SEQUENCETABLE . " WHERE TABLE_NAME = '".$tablename."' ");
    
    if (count($getdata) > 0) {
	$value = $getdata[0]['SEQ_VALUE'];
	if ($value == 9999) {
	    $updateSeq = array();
            $updateSeq['SEQ_VALUE'] = 1;
            $updateSeqWhere = "TABLE_NAME ='".$tablename."'";
            $updatevalue = update_rec(SEQUENCETABLE,$updateSeq,$updateSeqWhere);
	    $value = 1;
	}
	if (strlen($value) == 1) {
	    $id = "000" . $value;
	} else if (strlen($value) == 2) {
	    $id = "00" . $value;
	} else if (strlen($value) == 3) {
	    $id = "0" . $value;
	} else if (strlen($value) == 4) {
	    $id = $value;
	}
	$valueupdate = $value + 1;
	    $updateSeq = array();
            $updateSeq['SEQ_VALUE'] = $valueupdate ;
            $updateSeqWhere = "TABLE_NAME ='".$tablename."'";
            $updatevalue = update_rec(SEQUENCETABLE,$updateSeq,$updateSeqWhere);
	return $id;
    }
}
function getDataSyncTrans() {
    $query = "SELECT * FROM " . TALLYTRANSSTAGING . " WHERE LAST_SYNC_TIME < 2 ORDER BY CREATED_BY_DATE ASC";
    $fetchQuery = fetch_rec_query($query);
    if (count($fetchQuery) > 0) {
	if ($fetchQuery > 100) {
	    $countinueFlag = "true";
	} else {
	    $countinueFlag = "false";
	}
	$getIds = '' ; $row1 = array(); // CHANGED BY KAVITA PATEL ON 16-07-2018
	$tokenNo = generateIdSyncTrans(TALLYTRANSSTAGING);
	$i = 0;
	foreach ($fetchQuery as $row) {
	    if ($countinueFlag == "true" && $i == 100) {
		break;
	    }
	    $getIds .= "'" . $row['PK_TRANS_STAGING_ID'] . "',";
	    $row['TOKEN_NO'] = $tokenNo;
	    $row['NO_OF_RECORDS'] = count($fetchQuery);
	    $row1[] = $row;
	    $i++;
	}
	$inIds = rtrim($getIds, ",");
	$numCount = "";
	$updateQuery = array();
	$updateQuery['TOKEN_NO'] = $tokenNo ;
	if (isset($numCount) && $numCount == '') {
	 $updateQuery['NO_OF_RECORDS'] = count($fetchQuery) ;
	}else {
	    $updateQuery['NO_OF_RECORDS'] = '' ;
	}
	$runUpdateQuerywhere = "PK_TRANS_STAGING_ID IN ($inIds)";
	$updatevalue = update_rec(TALLYTRANSSTAGING,$updateQuery,$runUpdateQuerywhere);
	$tmpArr['count'] = count($row1);
	$tmpArr['data'] = $row1;
	$tmpArr['countinueFlag'] = $countinueFlag;
	$result = array("status" => SCS, "data" => $tmpArr);
    } else {
	$result = array("status" => FAIL);
    }
    return $result;
}

function updateLastSyncTimeSyncTrans($transData) {
    if (count($transData) > 0) {
	for ($i = 0; $i < count($transData); $i++) {
	    $updateQuery1 = array();
            $updateQuery1['LAST_SYNC_TIME'] = $transData[$i]['lastSyncTIme'] ;
	    $whereCond = "PK_TRANS_STAGING_ID ='".$transData[$i]['pkDeltaId']."'";
            $updatevalue = update_rec(TALLYTRANSSTAGING,$updateQuery1,$whereCond);
	}
    }
}

function curlCall($deltaTransSyncUrl, $newArr) {
    global $spaceId, $custId;
    $randnum = generateRandomString(); // GENERATE RANDOM NUMBER 10 DIGIT
    $randnum1 = generateRandomString(); // GENERATE RANDOM NUMBER 10 DIGIT
    $enc = md5($spaceId . "|--|" . $custId); //CREATE MD5 STRING WITH SPACE ID AND CLIENT ID
    $encryptedId = $randnum . $enc . $randnum1;
    $newArr['spaceId'] = $spaceId;
    $newArr['clientId'] = $custId;
    $newArr['datavalue'] = $encryptedId;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $deltaTransSyncUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $newArr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $abc1 = curl_exec($ch);
    curl_close($ch);
    $abc = objectToArray(json_decode($abc1));
    return $abc;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
	$randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$getData = getDataSyncTrans(); // CALL FUNCTION TO GET TRANS DATA FOR SYNC
if ($getData['status'] == SCS) {
    $newArr['data'] = json_encode($getData['data']);
    $newArr['pId'] = $pId; //  1 = epos
    $newArr['tableName'] = TALLYTRANSSTAGING; //  1 = epos
    $newArr['requestCase'] = "collect_data_synctrans_initiate";
    $curlData = curlCall($deltaTransSyncUrl, $newArr);
    //PRINT_R($curlData); die;
    if ($curlData['status'] == SCS) {
	updateLastSyncTimeSyncTrans($curlData['syncData']);
    }
}
?>
