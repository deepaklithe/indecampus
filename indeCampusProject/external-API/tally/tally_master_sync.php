<?php

/*
 *
 * Created By : Hasmukh Jadav.
 * Company : Lithe Technologies Pvt Ltd.
 * Date : 30-April-2015.
 * File : masterdeltasync.
 * File Type : .php.
 * Project : ePOS
 * Purpose : Master Delta Sync 
 * */
ini_set("display_errors", 0);
set_time_limit(0);
ini_set('max_execution_time', 0); //0=NOLIMIT
ini_set('memory_limit', '-1');
$appWebRoot = $_SERVER['DOCUMENT_ROOT'];
include "class_db.php"; // ADDED BY KAUSHA SHAH ON 16-04-2018
include "function_DL.php";

//constants
define("LOCALDB", "indeCampus"); // DB NAME
define("LOCALHOSTNAME", "192.168.129.207"); // HOST NAME 
define("LOCALUSERNAME", "inde"); // HOST USERNAME
define("LOCALPASSWORD", "inde"); // HOST PASSWORD
//define("TALLYSYNCURL", "http://analytic.southeastasia.cloudapp.azure.com/tally/tally_cloud/sync_data/api_deltasync.php"); // ADDED BY VAIBHAV ON 23-09-2015 FOR GET SERVER PHASE 2 TABLES
define("TALLYSYNCURL", "http://192.168.129.205/development/epostally/tally_cloud/sync_data/api_deltasync.php");
define("TALLYTRANSSTAGING", "tally_transaction_staging"); // TALLY TRANSACTION TABLE
define("TALLYTRANSSTAGINGCONVERTED", "tally_transaction_staging_converted"); // TALLY TRANSACTION TABLE converted
define("TALLYTRANSSTAGINGCONVERTEDEXPORT", "tally_transaction_staging_export"); // TALLY TRANSACTION TABLE converted
define("TALLYTRANSSTAGINGCONVERTEDEXPORT1", "tally_transaction_staging_converted_export"); // ADDED BY KAUSHA SHAH ON 12-04-2018 START
define("SPACEMASTER", "spacemaster"); //SPACEMASTER
define("TALLYCOMPLOCATION", "tally_company_store_location"); // TALLY COMPANY LOCATION TABLEs
define("TALLYCOMPANY", "tally_company"); // TALLY COMPANY TABLE
define("TALLYCOMPANYSTORE", "tally_company_store"); // TALLY COMPANY TABLE
define("SCS", "Success");
define("FAIL", "Fail");

$pId = 6; // COMPANY ID
$spaceId = isset($_GET['spaceid']) ? $_GET['spaceid'] : 0;  // CHANGED BY KAUSHA SHAH ON 12-04-2018
//db connection
//mysql_connect(LOCALHOSTNAME, LOCALUSERNAME, LOCALPASSWORD) or die(mysql_error());
//mysql_select_db(LOCALDB) or die(mysql_error());

//$db = new DB(LOCALDB, LOCALHOSTNAME, LOCALUSERNAME, LOCALPASSWORD); // ADDED BY KAUSHA SHAH ON 16-04-2018
$db = new DB(LOCALHOSTNAME, LOCALUSERNAME, LOCALPASSWORD,LOCALDB);

function objectToArray($d) {
    if (is_object($d)) {
	// Gets the properties of the given object
	// with get_object_vars function
	$d = get_object_vars($d);
    }
    if (is_array($d)) {
	/*
	 * Return array converted to object
	 * Using __FUNCTION__ (Magic constant)
	 * for recursive call
	 */
	return array_map(__FUNCTION__, $d);
    } else {
	// Return array
	return $d;
    }
}


//$getSyncTimeQuery = mysql_query("SELECT PK_SPACE_ID,TRANS_SYNC24,P2,P3,FK_FRAN_ID,DELTA_SYNC_FLAG,OFFLINEUTILITY_FLAG FROM " . SPACEMASTER . "");
//$getSyncTime = mysql_fetch_array($getSyncTimeQuery);
$spaceId = isset($_GET['spaceid']) ? $_GET['spaceid'] : 1;  // CHANGED BY KAUSHA SHAH ON 12-04-2018;
$clientId = 1;
$pId = isset($_GET['projid']) ? $_GET['projid'] : 1;  // CHANGED BY KAUSHA SHAH ON 12-04-2018; // 1 = epos

$postField = 'ALL';
$time = time();
$dataCount = 0;



function curlCall($deltaMasterSyncUrl, $newArr) {
    global $spaceId, $clientId;
    $randnum = generateRandomString(); // GENERATE RANDOM NUMBER 10 DIGIT
    $randnum1 = generateRandomString(); // GENERATE RANDOM NUMBER 10 DIGIT
    $enc = md5($spaceId . "|--|" . $clientId); //CREATE MD5 STRING WITH SPACE ID AND CLIENT ID
    $encryptedId = $randnum . $enc . $randnum1;
    $custId = $clientId;
    $newArr['spaceId'] = $spaceId;
    $newArr['clientId'] = $custId;
    $newArr['datavalue'] = $encryptedId;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $deltaMasterSyncUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $newArr);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $abc1 = curl_exec($ch);
    curl_close($ch);
    $string = preg_replace("/[\r\n]+/", " ", $abc1);
    $json = utf8_encode($string);
    $abc = objectToArray(json_decode($json));
    return $abc;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
	$randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function deleteTokenNo($deltaMasterSyncUrl
, $tokenNo) {
    $newArr['requestCase'] = "confirm_data_token";
    $newArr['tokenNo'] = $tokenNo;
    $curlResData = curlCall($deltaMasterSyncUrl, $newArr);
}

function callCountinueFn($deltaMasterSyncUrl, $tokenNo) {
    $newArr['requestCase'] = "collect_data_sync_countinue";
    $newArr['tokenNo'] = $tokenNo;
    curlCallForDeltaSync($deltaMasterSyncUrl, $newArr);
}

function curlCallForDeltaSync($deltaMasterSyncUrl, $newArr) {
    $abc = curlCall($deltaMasterSyncUrl, $newArr);
    //echo '<pre>' ; print_r($abc) ; 
    if ($abc['status'] == SCS) {
	$countinueFlag = $abc ['data']['countinueFlag'];
	$countRec = $abc['data']['count'];
	$tokenNo = $abc['data']['data'][0]['TOKEN_NO'];
	$data = $abc['data']['data'];
	if (count($data) > 0) {
	    for ($i = 0; $i < count($data); $i++) {
                //echo '<pre>' ; print_r($data[$i]) ; 
		// ADDED BY KAUSHA SHAH ON 22-06-2018 START
		$insertArr = array() ;
		$insertArr['FK_COMP_STORE_ID'] = $data[$i]['FK_COMP_STORE_ID'] ;
		//$insertArr['FK_COMP_ID'] = $data[$i]['FK_COMP_ID'] ;
		$insertArr['FK_COMP_ID'] = $data[$i]['FK_PROJECT_ID'] ;
		$insertArr['FK_EXPORT_PROCESS_MASTER_ID'] = 0 ;
		$insertArr['FUNCATION_NAME'] = $data[$i]['FUNCATION_NAME'] ;
		$insertArr['JSON_DATA'] = $data[$i]['JSON_DATA'] ;
		$insertArr['UPDATED_JSON_DATA'] = '' ;
		$insertArr['STATUS'] = $data[$i]['STATUS'] ;
		$insertArr['CREATED_BY'] = $data[$i]['CREATED_BY'] ;
		$insertArr['CREATED_BY_DATE'] = $data[$i]['CREATED_BY_DATE'] ;
		$insertArr['CHANGED_BY'] = $data[$i]['CHANGED_BY'] ;
		$insertArr['CHANGED_BY_DATE'] = $data[$i]['CHANGED_BY_DATE'] ;
		$test = insert_rec(TALLYTRANSSTAGINGCONVERTEDEXPORT, $insertArr, array(),0);
		// ADDED BY KAUSHA SHAH ON 22-06-2018 END
		//$test = insert_rec(TALLYTRANSSTAGINGCONVERTEDEXPORT, $data[$i], array(),0);
                //echo '<pre>' ; print_r($test) ; exit;
	    }
	    if ($countinueFlag == "true") {
		callCountinueFn($deltaMasterSyncUrl, $tokenNo);
	    } else {
		deleteTokenNo($deltaMasterSyncUrl, $tokenNo);
		$result = array("status" => SCS);
	    }
	} else {

	    $result = array("status" => "No Data For Sync");
	}
    } else {
	$result = array("status" => $abc['status']);
    }
    return $result;
}

//END FUNCTION FOR CHECK PENDING ORDER

    $deltaMasterSyncUrl = TALLYSYNCURL; // DELTA MASTER SYNC URL
    $newArr['spaceId'] = $spaceId;
    $newArr['pId'] = $pId;
    $newArr['tableName'] = TALLYTRANSSTAGINGCONVERTEDEXPORT1; // ADDED BY KAUSHA SHAH ON 12-04-2018 START
    $newArr['requestCase'] = "collect_data_sync_initiate";
    $returnData = curlCallForDeltaSync($deltaMasterSyncUrl, $newArr);

//insertIntoMasterTables(); //CALL FUNCTION FOR INSRT INTO LIVE MASTER TABLE IN LOCAL FROM SYNC_MASTER
?>
