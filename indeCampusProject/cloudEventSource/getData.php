<?php 

/***************************************************/
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : DEEPAK PATIL                   */
/*   CREATION DATE : 20-04-2016                    */
/*   FILE TYPE : PHP                               */
/*   PROJECT NAME : CLOUD TELEPHONY                */
/***************************************************/

// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once "include/globals.php"; // GLOBAL VAR FILE
require_once "include/function_DL.php"; // DL FUNCTION FILE (COMMON FUNCTION FILE)
require_once "include/function_telephony.php"; // DL FUNCTION FILE (COMMON FUNCTION FILE)

$postData           = isset($_POST) ? $_POST : "";
$requestCase        = $postData['postData']['requestCase'];

if (!isset($postData['postData']['clientId']) || !isset($postData['postData']['userId']) || !isset($postData['postData']['requestCase'])) {
 //IF POSTDATA PARAMETER NOT COMING
 $result = array("status" => PARAM);
 $jsonData = json_encode($result);

}else{ // IF REQUEST CASE IS PROPER THEN CALL REQUEST API
	switch ($requestCase) {
		case "checkCallLog":
   $result = getCallLogDetails_BL($postData);
   $jsonData = json_encode($result);
   break;	
	}
	
}


/* ======= RETURN DATA AFTER REQUESTING BY ANY CALL FROM API ========= */
if ($jsonData) {
 // if return json
 echo $jsonData;
} else {
 //if json is not returned
 $result = array('status' => FAILMSG);
 echo json_encode($result);
}

?>