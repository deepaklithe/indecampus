<?php 

/*function getCallLogDetails_BL($postData){

	$clientId = $postData['postData']['clientId'];
	$sqlCloudData = "SELECT POST_DATA,API_TYPE,ID,FK_CLIENT_ID FROM ".CLOUDTELEPHONY." WHERE ACTION_STATUS = 0";
	$resultCloudData = fetch_rec_query($sqlCloudData);
	//print_r($resultCloudData); exit;
	if(count($resultCloudData)>0){
		$cloudDataArr = array();
		for($i=0;$i<count($resultCloudData);$i++){
			$array=json_decode(trim($resultCloudData[$i]['POST_DATA'],'"'),true);
			$cloudDataArr[$i]['data'] = $array;
			$cloudDataArr[$i]['status'] = $resultCloudData[$i]['API_TYPE'];
			$cloudDataArr[$i]['id'] = $resultCloudData[$i]['ID'];
			$cloudDataArr[$i]['clientId'] = $resultCloudData[$i]['FK_CLIENT_ID'];
		}

		//print_r($cloudDataArr); exit;	
		$finalArr = array();
		for($j=0;$j<count($cloudDataArr);$j++){
				$insertCallLog = array();
				$insertCallLogUpdate = array();
				$insertCallLogUpdateWhere = "";
				$updateCallLog = array();
				$updateCallLogWhere = array();
				$flag = ERR;
				if($cloudDataArr[$j]['status'] == ONCALL){ // START INSERTING INTO CALL LOG TABLE
					// GETTING DATA FROM CALL LOG - IF FOUND THEN UPDATE SAME ENTRY ELES INSERT NEW ENTRY.
					$sqlCheckSameId = "SELECT CALL_ID,FK_CLIENT_ID FROM ".CALLLOG." WHERE CALL_ID = '".$cloudDataArr[$j]['data']['callid']."' AND CALL_TO = '".$cloudDataArr[$j]['data']['callto']."' AND CALL_FROM = '".$cloudDataArr[$j]['data']['callfrom']."'  AND DELETE_FLAG = 0 ";
					$resultCheckSameId = fetch_rec_query($sqlCheckSameId);

					if(count($resultCheckSameId)>0){ // UPDATE SAEM ENTRY DATA WITH NEW DATA
						// START GETTING CUSTOMER AND USER DETAIL
						$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr[$j]['data']['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$cloudDataArr[$j]['clientId']."' LIMIT 0 , 1";
						$resultUser = fetch_rec_query($sqlUser);
						//print_r($resultUser);
						if(count($resultUser) > 0){
								$insertCallLogUpdate['FK_USER_ID'] = $resultUser[0]['PK_USER_ID'];
						}else{
								$insertCallLogUpdate['FK_USER_ID'] = 0;
						}
						
						$insertCallLogUpdate['CALL_ID'] = $cloudDataArr[$j]['data']['callid'];
						$insertCallLogUpdate['CAL_ID'] = $cloudDataArr[$j]['data']['calid'];
						$insertCallLogUpdate['GID'] = $cloudDataArr[$j]['data']['gid'];
						$insertCallLogUpdate['SOURCE'] = $cloudDataArr[$j]['data']['source'];
						$insertCallLogUpdate['LANDING_NUMBER'] = $cloudDataArr[$j]['data']['landingnumber'];
						$insertCallLogUpdate['CALL_FROM'] = $cloudDataArr[$j]['data']['callfrom'];
						$insertCallLogUpdate['CALL_TO'] = $cloudDataArr[$j]['data']['callto'];
						$insertCallLogUpdate['START_TIME'] = $cloudDataArr[$j]['data']['starttime'];
						$insertCallLogUpdate['END_TIME'] = $cloudDataArr[$j]['data']['endtime'];
						$insertCallLogUpdate['FILE_NAME'] = $cloudDataArr[$j]['data']['filename'];
						$insertCallLogUpdate['DAIL_STATUS'] = $cloudDataArr[$j]['data']['dialstatus'];
						$insertCallLogUpdate['CALL_STATUS'] = "MISSCALL";
						$insertCallLogUpdate['BUSINESS_NAME'] = $cloudDataArr[$j]['data']['businessname'];
						$insertCallLogUpdate['DIVERT_TYPE'] = !empty($cloudDataArr[$j]['data']['empemail']) ? 0 : 1;
						$insertCallLogUpdate['EMPNUMBER'] = $cloudDataArr[$j]['data']['empnumber'];
						$insertCallLogUpdate['EMPEMAIL'] = $cloudDataArr[$j]['data']['empemail'];
						$insertCallLogUpdate['API_KEY'] = $cloudDataArr[$j]['data']['apikey'];
						$insertCallLogUpdate['CHANGE_BY'] = $clientId;
						$insertCallLogUpdate['CHANGE_BY_DATE'] = time();

						// WHERE
						$insertCallLogUpdateWhere = "CALL_ID = '".$resultCheckSameId[0]['CALL_ID']."'";
						$insertCallLogUpdateResult = update_rec(CALLLOG,$insertCallLogUpdate,$insertCallLogUpdateWhere);
						if($insertCallLogUpdateResult > 0){
							$flag = SCS;
						}
					}else{ // INSERT NEW DATA

						// START GETTING CUSTOMER AND USER DETAIL
						$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr[$j]['data']['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$cloudDataArr[$j]['clientId']."' LIMIT 0 , 1";
						$resultUser = fetch_rec_query($sqlUser);
						//print_r($resultUser);
						if(count($resultUser) > 0){
								$insertCallLog['FK_USER_ID'] = $resultUser[0]['PK_USER_ID'];
						}else{
								$insertCallLog['FK_USER_ID'] = 0;
						}

						// START GETTING CUSTOMER DETAIL
						$sqlCustomer = "SELECT * FROM ".MULTIPHONEMASTER." MCM INNER JOIN ".CUSTOMER." CM ON CM.PK_CUST_ID = MCM.FK_CUST_ID WHERE MCM.PHONE_NO = CONCAT('+91',".$cloudDataArr[$j]['data']['callfrom'].") LIMIT 0,1";
						$resultCustomer = fetch_rec_query($sqlCustomer);
						if(count($resultCustomer)>0){
								$insertCallLog['FK_CUST_ID'] = $resultCustomer[0]['FK_CUST_ID'];
						}else{
								$insertCallLog['FK_CUST_ID'] = 0;
						}

						$insertCallLog['FK_CLIENT_ID'] = $cloudDataArr[$j]['clientId'];
						$insertCallLog['CALL_ID'] = $cloudDataArr[$j]['data']['callid'];
						$insertCallLog['CAL_ID'] = $cloudDataArr[$j]['data']['calid'];
						$insertCallLog['GID'] = $cloudDataArr[$j]['data']['gid'];
						$insertCallLog['SOURCE'] = $cloudDataArr[$j]['data']['source'];
						$insertCallLog['LANDING_NUMBER'] = $cloudDataArr[$j]['data']['landingnumber'];
						$insertCallLog['CALL_FROM'] = $cloudDataArr[$j]['data']['callfrom'];
						$insertCallLog['CALL_TO'] = $cloudDataArr[$j]['data']['callto'];
						$insertCallLog['START_TIME'] = $cloudDataArr[$j]['data']['starttime'];
						$insertCallLog['END_TIME'] = $cloudDataArr[$j]['data']['endtime'];
						$insertCallLog['FILE_NAME'] = $cloudDataArr[$j]['data']['filename'];
						$insertCallLog['DAIL_STATUS'] = $cloudDataArr[$j]['data']['dialstatus'];
						$insertCallLog['CALL_STATUS'] = "MISSCALL";
						$insertCallLog['BUSINESS_NAME'] = $cloudDataArr[$j]['data']['businessname'];
						$insertCallLog['DIVERT_TYPE'] = !empty($cloudDataArr[$j]['data']['empemail']) ? 0 : 1;
						$insertCallLog['EMPNUMBER'] = $cloudDataArr[$j]['data']['empnumber'];
						$insertCallLog['EMPEMAIL'] = $cloudDataArr[$j]['data']['empemail'];
						$insertCallLog['API_KEY'] = $cloudDataArr[$j]['data']['apikey'];
						$insertCallLog['CREATED_BY'] = $clientId;
						$insertCallLog['CREATE_BY_DATE'] = time();
						$insertCallLog['DELETE_FLAG'] = 0;
						//print_r($insertCallLog); exit;
						$insertCallLogResult = insert_rec(CALLLOG,$insertCallLog);
						if($insertCallLogResult['lastInsertedId'] > 0){
								$flag = SCS;
						}
					}
					
					if($flag == SCS){
						$updateCloudTele = array();
						$updateCloudTeleWhere = "";
						$updateResultCloudTele = "";
						// START UPDATING CLOUD TELEPHONY STATUS
						$updateCloudTele['ACTION_STATUS'] = "1";
						$updateCloudTeleWhere = "ID = '".$cloudDataArr[$j]['id']."'";
						$updateResultCloudTele = update_rec(CLOUDTELEPHONY,$updateCloudTele,$updateCloudTeleWhere);
					}
				}else if($cloudDataArr[$j]['status'] == ONHANGUP){ // START UPDATING INTO CALL LOG TABLE TO EXISTING 

						// START GETTING CUSTOMER AND USER DETAIL
						$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr[$j]['data']['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$cloudDataArr[$j]['clientId']."' LIMIT 0 , 1";
						$resultUser = fetch_rec_query($sqlUser);
						if(count($resultUser) > 0){
								$updateCallLog['FK_USER_ID'] = $resultUser[0]['PK_USER_ID'];
						}else{
								$updateCallLog['FK_USER_ID'] = 0;
						}

						
						$updateCallLog['CALL_ID'] = $cloudDataArr[$j]['data']['callid'];
						$updateCallLog['CAL_ID'] = $cloudDataArr[$j]['data']['calid'];
						$updateCallLog['GID'] = $cloudDataArr[$j]['data']['gid'];
						$updateCallLog['SOURCE'] = $cloudDataArr[$j]['data']['source'];
						$updateCallLog['LANDING_NUMBER'] = $cloudDataArr[$j]['data']['landingnumber'];
						$updateCallLog['CALL_FROM'] = $cloudDataArr[$j]['data']['callfrom'];
						$updateCallLog['CALL_TO'] = $cloudDataArr[$j]['data']['callto'];
						$updateCallLog['START_TIME'] = $cloudDataArr[$j]['data']['starttime'];
						$updateCallLog['END_TIME'] = $cloudDataArr[$j]['data']['endtime'];
						$updateCallLog['FILE_NAME'] = $cloudDataArr[$j]['data']['filename'];
						$updateCallLog['DAIL_STATUS'] = $cloudDataArr[$j]['data']['dialstatus'];
						$updateCallLog['CALL_STATUS'] = "RECEIVED";
						$updateCallLog['BUSINESS_NAME'] = $cloudDataArr[$j]['data']['businessname'];
						$updateCallLog['DIVERT_TYPE'] = !empty($cloudDataArr[$j]['data']['empemail']) ? 0 : 1;
						$updateCallLog['EMPNUMBER'] = $cloudDataArr[$j]['data']['empnumber'];
						$updateCallLog['EMPEMAIL'] = $cloudDataArr[$j]['data']['empemail'];
						$updateCallLog['API_KEY'] = $cloudDataArr[$j]['data']['apikey'];
						$updateCallLog['CHANGE_BY'] = $clientId;
						$updateCallLog['CHANGE_BY_DATE'] = time();
						// WHERE
						$updateCallLogWhere = "CALL_ID = '".$cloudDataArr[$j]['data']['callid']."' AND EMPNUMBER = '".$cloudDataArr[$j]['data']['empnumber']."'";
						$updateRecordCallLog = update_rec(CALLLOG,$updateCallLog,$updateCallLogWhere);
						
						// UPDATE CLOUD TELEPHONY TABLE
						$updateCloudTele = array();
						$updateCloudTeleWhere = "";
						$updateResultCloudTele = "";	
						if($updateRecordCallLog > 0){
							// START UPDATING CLOUD TELEPHONY STATUS
							$updateCloudTele['ACTION_STATUS'] = "1";
							$updateCloudTeleWhere = "ID = '".$cloudDataArr[$j]['id']."'";
							$updateResultCloudTele = update_rec(CLOUDTELEPHONY,$updateCloudTele,$updateCloudTeleWhere);
						}
				} // END ELSE
		} // END FOR LOOP
	}
	
} // END FUNCTION */
?>