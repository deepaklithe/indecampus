<?php

// Table Constant Listing 
define("CLOUDTELEPHONY", "cloud_telephony");
define("CALLLOG", "call_log");
define("USERMASTER", "user_master");
define("CUSTOMER", "customer");
define("MULTIPHONEMASTER", "multi_phone_master");


// MSG CONSTANT LISTING 
define("PARAM", "Parameter Missing");
define("FAILMSG", "Fail");
define("ONCALL", "ONCALL");
define("ONHANGUP", "ONHANGUP");
define("ONCHANGE", "ONCHANGE");
define("ONEDIT", "ONEDIT");
define("SCS", "Success");
define("ERR", "Error");
define("FAIL", "Fail");
define("INTEGRATION_NOT_ALLOW","Tally integration is not allowed") ; // ADDED KAUSHA SHAH ON 24-08-2018

// ALERT LISTING CONSTANT
define("INSERTFAIL","Insert Query fail for ");
define("UPDATEFAIL","Update Query fail for ");
define("NORECORDFROMPOST","There is not data to process right now");
define("ANSWER","ANSWER");
define("CONNECTING","CONNECTING");