<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once 'include/globals.php'; // GLOBAL FILES
//print_r($_POST); exit;
function objectToArray($d) {

	 if (is_object($d)) {
	 // Gets the properties of the given object
	 // with get_object_vars function
	 $d = get_object_vars($d);
	 }
	 
	 if (is_array($d)) {
	 /*
	 * Return array converted to object
	 * Using __FUNCTION__ (Magic constant)
	 * for recursive call
	 */
	 return array_map(__FUNCTION__, $d);
	 }
	 else {
	 // Return array
	 return $d;
	 }
 }

$cloudDataArr = (isset($_POST) && !empty($_POST)) ? objectToArray(json_decode($_POST['data'])) : "";
$clientId = "4";
if(!empty($cloudDataArr) && $cloudDataArr['dialstatus'] == CONNECTING){
		$userId = "";
		$custId = "";
	 // START SEARCHING USER MASTER
		$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$clientId."' LIMIT 0 , 1";
		$resultUser = fetch_rec_query($sqlUser);
		if(count($resultUser) > 0){
				$userId = $resultUser[0]['PK_USER_ID'];
		}else{
				$userId = 0;
		}

		// START SERCHING CUSTOMER DETAIL
		$sqlCustomer = "SELECT * FROM ".MULTIPHONEMASTER." MCM INNER JOIN ".CUSTOMER." CM ON CM.PK_CUST_ID = MCM.FK_CUST_ID WHERE MCM.PHONE_NO = CONCAT('+91',".$cloudDataArr['callfrom'].") AND CM.FK_CLIENT_ID = '".$clientId."' LIMIT 0,1";
		$resultCustomer = fetch_rec_query($sqlCustomer);
		if(count($resultCustomer)>0){
				$custId = $resultCustomer[0]['FK_CUST_ID'];
		}else{
				$custId = 0;
		}

		$insertCT['POST_DATA'] = json_encode(trim($_POST['data'],'"'));
		$insertCT['FK_USER_ID'] = $userId;
		$insertCT['FK_CUST_ID'] = $custId;
		$insertCT['FK_CLIENT_ID'] = $clientId;
		$insertCT['CALL_ID'] = $cloudDataArr['callid'];
		$insertCT['CAL_ID'] = $cloudDataArr['calid'];
		$insertCT['GID'] = $cloudDataArr['gid'];
		$insertCT['SOURCE'] = $cloudDataArr['source'];
		$insertCT['LANDING_NUMBER'] = $cloudDataArr['landingnumber'];
		$insertCT['CALL_FROM'] = $cloudDataArr['callfrom'];
		$insertCT['CALL_TO'] = $cloudDataArr['callto'];
		$insertCT['DAIL_STATUS'] = $cloudDataArr['dialstatus'];
		$insertCT['DIVERT_TYPE'] = 0;
		$insertCT['EMPNUMBER'] = $cloudDataArr['empnumber'];
		$insertCT['API_TYPE'] = 'ONCALL';
		$insertCT['ACTION_STATUS'] = '0';
		$insertCT['CREATED_BY_TIME'] = time();

		$insertCTResult = insert_rec(CLOUDTELEPHONY,$insertCT);
		if($insertCTResult['lastInsertedId'] > 0){

					$insertCallLog['FK_USER_ID'] = $userId;
					$insertCallLog['FK_CUST_ID'] = $custId;
				 $insertCallLog['FK_CLIENT_ID'] = $clientId;
					$insertCallLog['CALL_ID'] = $cloudDataArr['callid'];
					$insertCallLog['CAL_ID'] = $cloudDataArr['calid'];
					$insertCallLog['GID'] = $cloudDataArr['gid'];
					$insertCallLog['SOURCE'] = $cloudDataArr['source'];
					$insertCallLog['LANDING_NUMBER'] = $cloudDataArr['landingnumber'];
					$insertCallLog['CALL_FROM'] = $cloudDataArr['callfrom'];
					$insertCallLog['CALL_TO'] = $cloudDataArr['callto'];
					$insertCallLog['START_TIME'] = $cloudDataArr['starttime'];
					$insertCallLog['END_TIME'] = $cloudDataArr['endtime'];
					$insertCallLog['FILE_NAME'] = $cloudDataArr['filename'];
					$insertCallLog['DAIL_STATUS'] = $cloudDataArr['dialstatus'];
					$insertCallLog['CALL_STATUS'] = "MISSCALL";
					$insertCallLog['BUSINESS_NAME'] = $cloudDataArr['businessname'];
					$insertCallLog['DIVERT_TYPE'] = !empty($cloudDataArr['empemail']) ? 0 : 1;
					$insertCallLog['EMPNUMBER'] = $cloudDataArr['empnumber'];
					$insertCallLog['EMPEMAIL'] = $cloudDataArr['empemail'];
					$insertCallLog['API_KEY'] = $cloudDataArr['apikey'];
					$insertCallLog['CREATED_BY'] = $clientId;
					$insertCallLog['CREATE_BY_DATE'] = time();
					$insertCallLog['DELETE_FLAG'] = 0;
					//print_r($insertCallLog); exit;
					$insertCallLogResult = insert_rec(CALLLOG,$insertCallLog);
					if($insertCallLogResult['lastInsertedId'] > 0){
							$result = array('status' => SCS);
							//echo json_encode($result);
					}else{
							$result = array('status' => INSERTFAIL.CALLLOG);
					}
		}else{
				$result = array('status' => INSERTFAIL.CLOUDTELEPHONY);
		}
	
}else{
	$result = array("status"=>NORECORDFROMPOST);
}
echo json_encode($result);

?>
