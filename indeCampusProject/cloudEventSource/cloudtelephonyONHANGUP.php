<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST ,REQUEST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once 'include/globals.php'; // GLOBAL FILES

function objectToArray($d) {

	 if (is_object($d)) {
	 // Gets the properties of the given object
	 // with get_object_vars function
	 $d = get_object_vars($d);
	 }
	 
	 if (is_array($d)) {
	 /*
	 * Return array converted to object
	 * Using __FUNCTION__ (Magic constant)
	 * for recursive call
	 */
	 return array_map(__FUNCTION__, $d);
	 }
	 else {
	 // Return array
	 return $d;
	 }
 }
$clientId = "4";
$cloudDataArr = (isset($_POST) && !empty($_POST)) ? objectToArray(json_decode($_POST['data'])) : "";
if(!empty($cloudDataArr) && $cloudDataArr['dialstatus'] == ANSWER){
		$userId = "";
		$custId = "";
		// START SEARCHING USER MASTER
		$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$clientId."' LIMIT 0 , 1";
		$resultUser = fetch_rec_query($sqlUser);
		if(count($resultUser) > 0){
				$userId = $resultUser[0]['PK_USER_ID'];
		}else{
				$userId = 0;
		}

		// START SERCHING CUSTOMER DETAIL
		$sqlCustomer = "SELECT * FROM ".MULTIPHONEMASTER." MCM INNER JOIN ".CUSTOMER." CM ON CM.PK_CUST_ID = MCM.FK_CUST_ID WHERE MCM.PHONE_NO = CONCAT('+91',".$cloudDataArr['callfrom'].") AND CM.FK_CLIENT_ID = '".$clientId."' LIMIT 0,1";
		$resultCustomer = fetch_rec_query($sqlCustomer);
		if(count($resultCustomer)>0){
				$custId = $resultCustomer[0]['FK_CUST_ID'];
		}else{
				$custId = 0;
		}

		$insertCT['POST_DATA'] = json_encode(trim($_POST['data'],'"'));
		$insertCT['FK_USER_ID'] = $userId;
		$insertCT['FK_CUST_ID'] = $custId;
		$insertCT['FK_CLIENT_ID'] = $clientId;
		$insertCT['CALL_ID'] = $cloudDataArr['callid'];
		$insertCT['CAL_ID'] = $cloudDataArr['calid'];
		$insertCT['GID'] = $cloudDataArr['gid'];
		$insertCT['SOURCE'] = $cloudDataArr['source'];
		$insertCT['LANDING_NUMBER'] = $cloudDataArr['landingnumber'];
		$insertCT['CALL_FROM'] = $cloudDataArr['callfrom'];
		$insertCT['CALL_TO'] = $cloudDataArr['callto'];
		$insertCT['DAIL_STATUS'] = $cloudDataArr['dialstatus'];
		$insertCT['DIVERT_TYPE'] = 0;
		$insertCT['EMPNUMBER'] = $cloudDataArr['empnumber'];
		$insertCT['API_TYPE'] = 'ONHANGUP';
		$insertCT['ACTION_STATUS'] = '0';
		$insertCT['CREATED_BY_TIME'] = time();

		$insertCTResult = insert_rec(CLOUDTELEPHONY,$insertCT);
		if($insertCTResult['lastInsertedId'] > 0){
			
				/*// START GETTING CUSTOMER AND USER DETAIL
				$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '4' LIMIT 0 , 1";
				$resultUser = fetch_rec_query($sqlUser);
				if(count($resultUser) > 0){
						$updateCallLog['FK_USER_ID'] = $resultUser[0]['PK_USER_ID'];
				}else{
						$updateCallLog['FK_USER_ID'] = 0;
				}*/

			/*	// START GETTING CUSTOMER AND USER DETAIL
						$sqlUser = "SELECT * FROM ".USERMASTER." UM WHERE UM.CONTACT_NO =  CONCAT('+91',".$cloudDataArr['callto'].") AND UM.DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$cloudDataArr[$j]['clientId']."' LIMIT 0 , 1";
						$resultUser = fetch_rec_query($sqlUser);
						if(count($resultUser) > 0){
								$updateCallLog['FK_USER_ID'] = $resultUser[0]['PK_USER_ID'];
						}else{
								$updateCallLog['FK_USER_ID'] = 0;
						}*/

						
						$updateCallLog['CALL_ID'] = $cloudDataArr['callid'];
						$updateCallLog['CAL_ID'] = $cloudDataArr['calid'];
						$updateCallLog['GID'] = $cloudDataArr['gid'];
						$updateCallLog['SOURCE'] = $cloudDataArr['source'];
						$updateCallLog['LANDING_NUMBER'] = $cloudDataArr['landingnumber'];
						$updateCallLog['CALL_FROM'] = $cloudDataArr['callfrom'];
						$updateCallLog['CALL_TO'] = $cloudDataArr['callto'];
						$updateCallLog['START_TIME'] = $cloudDataArr['starttime'];
						$updateCallLog['END_TIME'] = $cloudDataArr['endtime'];
						$updateCallLog['FILE_NAME'] = $cloudDataArr['filename'];
						$updateCallLog['DAIL_STATUS'] = $cloudDataArr['dialstatus'];
						$updateCallLog['CALL_STATUS'] = "RECEIVED";
						$updateCallLog['BUSINESS_NAME'] = $cloudDataArr['businessname'];
						$updateCallLog['DIVERT_TYPE'] = !empty($cloudDataArr['empemail']) ? 0 : 1;
						$updateCallLog['EMPNUMBER'] = $cloudDataArr['empnumber'];
						$updateCallLog['EMPEMAIL'] = $cloudDataArr['empemail'];
						$updateCallLog['API_KEY'] = $cloudDataArr['apikey'];
						$updateCallLog['CHANGE_BY'] = $clientId;
						$updateCallLog['CHANGE_BY_DATE'] = time();
						// WHERE
						$updateCallLogWhere = "CALL_ID = '".$cloudDataArr['callid']."' AND CALL_FROM = '".$cloudDataArr['callfrom']."' AND CALL_TO = '".$cloudDataArr['callto']."'";
						$updateRecordCallLog = update_rec(CALLLOG,$updateCallLog,$updateCallLogWhere);
						// UPDATE CLOUD TELEPHONY TABLE
						$updateCloudTele = array();
						$updateCloudTeleWhere = "";
						$updateResultCloudTele = "";	
						if($updateRecordCallLog > 0){
							// START UPDATING CLOUD TELEPHONY STATUS
							$updateCloudTele['ACTION_STATUS'] = "1";
							$updateCloudTeleWhere = "ID = '".$cloudDataArr[$j]['id']."'";
							$updateResultCloudTele = update_rec(CLOUDTELEPHONY,$updateCloudTele,$updateCloudTeleWhere);
								$result = array('status' => SCS);
						}else{
								$result = array('status' => UPDATEFAIL.CALLLOG);			
						}

		}else{
				$result = array('status' => UPDATEFAIL.CLOUDTELEPHONY);
		}
}else{
	$result = array("status"=>NORECORDFROMPOST);
}
/*$insert_query = "INSERT INTO ".CLOUDTELEPHONY." (POST_DATA,API_TYPE) VALUES ('".json_encode($_POST['data'])."','ONHANGUP')";
$run_query = mysql_query($insert_query) or die(mysql_error());
$result = array('status' => 'success');*/
echo json_encode($result);
?>
