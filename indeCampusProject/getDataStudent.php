<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : Darshan Patel                  */
/*   CREATION DATE : 03-07-2018                    */
/*   FILE TYPE : PHP                               */
/* * ************************************************ */

// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

ini_set('memory_limit', '-1');
ini_set("display_errors", 1); // PLEASE CHANGE THIS VALUE TO 0 BEFORE PRODUCTION
// Include Required File...

require_once "include/globals.php"; // GLOBAL VAR FILE
//require_once "include/function_DL.php"; // DL FUNCTION FILE (COMMON FUNCTION FILE)//INCLUDED IN GLOBAL FILE

require_once "include/function_user_indeCampus.php"; // 
require_once "include/function_room_indeCampus.php"; // Room related function in it.
require_once "include/function_student_indeCampus.php"; // student related function in it.
require_once "include/function_developerMaster_indeCampus.php";
require_once "include/function_lookupmaster_indeCampus.php";
require_once "include/function_item_indeCampus.php";
require_once "include/function_payment_indeCampus.php";
require_once "include/function_property_indeCampus.php";
require_once "include/function_customer_indeCampus.php";
require_once "include/function_events_indeCampus.php";
require_once 'include/sendgrid-php.php';
require_once 'include/function_complaint_indeCampus.php';
require_once 'include/function_centralised_indeCampus.php';
require_once 'include/function_report_indeCampus.php';

// Default variable
$postData = isset($_POST) ? $_POST : "";
if (!is_object($postData)) {
    $postData = $postData;
} else {
    $postData = objectToArray(json_decode($postData['postData']));
}
$requestCase = $postData['postData']['requestCase'];

//print_r($postData);exit;

$checkSignature = checkJWTToken($postData); // CHECK THE AUTH OF JWT
//print_r($checkSignature); exit;
//// $checkSignature['status'] = SCS;
//$jsonData = array();
if ($checkSignature['status'] != SCS) {
    $jsonData = json_encode(array("status" => $checkSignature['status']));
} else {

    if ($postData['postData']['requestCase'] == "studentLogin" || $postData['postData']['requestCase'] == "loadauthStudent" || $postData['postData']['requestCase'] == "forgetPassword" || $postData['postData']['requestCase'] == "changePasswordStudent" || $postData['postData']['requestCase'] == "listCmsData" || $postData['postData']['requestCase'] == "upcomingEventList") {
        switch ($requestCase) {

            case 'studentLogin' : // CUSTOMER LOGIN
                $result = studentLogin($postData);
                $jsonData = json_encode($result);
                break;

            case "loadauthStudent" : // load authorization

                $result = loadAuthStudent_BL($postData);
                $jsonData = json_encode($result);
                break;

            case "changePasswordStudent" : // load authorization

                $result = changePasswordStudent($postData);
                $jsonData = json_encode($result);
                break;

            case "forgetPassword" : //user login   
                $result = forgetPasswordFP_BL($postData);
                $jsonData = json_encode($result);
                break;

            case "listCmsData" :
                $result = listCmsData($postData);
                $jsonData = json_encode($result);
                break;

            case "upcomingEventList" :
                $result = upcomingEventList($postData);
                $jsonData = json_encode($result);
                break;

            default:
                break;
        }
    } else {

        if (!isset($postData['postData']['clientId']) || !isset($postData['postData']['requestCase']) || !isset($postData['postData']['studentId'])) {
            //IF POSTDATA PARAMETER NOT COMING
            $result = array("status" => PARAM);
            $jsonData = json_encode($result);
        } else {
            switch ($requestCase) {

                case 'pageContent' : // CUSTOMER LOGIN
                    $result = getPageSectionDetail($postData);
                    $jsonData = json_encode($result);
                    break;

                case 'updateProfileStudent' : // CUSTOMER LOGIN
                    $result = addEditStudent($postData);
                    $jsonData = json_encode($result);
                    break;

                case 'billingHistory' : // CUSTOMER LOGIN
                    $result = billingHistory($postData);
                    $jsonData = json_encode($result);
                    break;

                case 'studentProfileData' : // CUSTOMER LOGIN
                    $result = getStudentData($postData);
                    $jsonData = json_encode($result);
                    //print_r($jsonData); die;
                    break;

                case 'ledgerHistory' : // CUSTOMER LOGIN
                    //$result = upcomingEventList($postData);
                    $result = ledgerHistory($postData);
                    $jsonData = json_encode($result);
                    break;

                case 'addEditstudentEventsBooking' : // CUSTOMER LOGIN
                    $result = addEditEventsBooking($postData);
                    $jsonData = json_encode($result);
                    break;

                case 'getStateCity' : // Get State city data
                    $result = getStateCity($postData);
                    $jsonData = json_encode($result);
                    break;

                case 'getStudentEventList' : // Get State city data
                    $result = getItemsData($postData);
                    $jsonData = json_encode($result);
                    break;

                case "getStudentInvoiceData" : //user login   
                    $result = getStudentInvoiceData($postData);
                    $jsonData = json_encode($result);
                    break;


                case "addEditNonAvailingService" : //user login   
                    $result = addEditNonAvailingService($postData);
                    $jsonData = json_encode($result);
                    break;

                case "listStudentNonAvailingService" :
                    $result = listStudentNonAvailingService($postData);
                    $jsonData = json_encode($result);
                    break;

                case "deleteStudentService" :
                    $result = deleteStudentService($postData);
                    $jsonData = json_encode($result);
                    break;

                case "updateServiceStatus" :
                    $result = updateServiceStatus($postData);
                    $jsonData = json_encode($result);
                    break;

                case "getComplaintsData" : // ADDED BY KAUSHA SHAH ON 09-08-2018 
                    $result = getComplaintsData($postData);
                    $jsonData = json_encode($result);
                    break;

                case "getUsageHistory" : // ADDED BY SATISH KARENA ON 09-08-2018 
                    $result = getUsageHistory($postData);
                    $jsonData = json_encode($result);
                    break;

                case "departmentCategoryList" : // ADDED BY SATISH KARENA ON 09-08-2018 
                    $result = departmentCategoryList($postData);
                    $jsonData = json_encode($result);
                    break;

                case "addComplaint" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = addEditComplaint($postData);
                    $jsonData = json_encode($result);
                    break;

                //COMMENT BY DARSHAN ON 18-8-2018 BECAUSE THIS WILL BYPASS FROM JWT IF TUDENT IS NOT LOGIN
//                case "listCmsData" : // ADDED BY SATISH KARENA ON 16-08-2018 
//                    $result = listCmsData($postData);
//                    $jsonData = json_encode($result);
//                    break;


                case "cancelComplaint" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = cancelComplaint($postData);
                    $jsonData = json_encode($result);
                    break;


                case "reopenComplaint" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = reopenComplaint($postData);
                    $jsonData = json_encode($result);
                    break;

                case "addComplaintComment" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = addComplaintComment($postData);
                    $jsonData = json_encode($result);
                    break;

                case "getItemsData" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = getItemsData($postData);
                    $jsonData = json_encode($result);
                    break;

                case "closeComplaintData" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = closeComplaintData($postData);
                    $jsonData = json_encode($result);
                    break;
                
                 case "deleteComplaintAttachment" : // ADDED BY SATISH KARENA ON 16-08-2018 
                    $result = deleteComplaintAttachment($postData);
                    $jsonData = json_encode($result);
                    break;

                    //COMMENT BY DARSHAN ON 18-8-2018 BECAUSE THIS WILL BYPASS FROM JWT IF TUDENT IS NOT LOGIN
//                case "upcomingEventList" : // ADDED BY SATISH KARENA ON 17-08-2018 
//                    $result = upcomingEventList($postData);
//                    $jsonData = json_encode($result);
//                    break;

                    ;
            }
        }
    }
//print_r($jsonData); die;
}

if ($jsonData) {
    //print_r($jsonData); die;
    // if return json
    $jsonDecode = json_decode($jsonData);
    //print_r($jsonDecode->data->JWTrefreshToken); exit;
    //print_r($checkSignature); exit;
    $token = (isset($jsonDecode->data->JWTrefreshToken) ? $jsonDecode->data->JWTrefreshToken : (isset($checkSignature['data']) ? $checkSignature['data'] : ""));
    //echo trim($token); exit;
    header("Authorization: Bearer " . trim($token));
    header("Content-Type: application/x-www-form-urlencoded; charset=UTF-8");


    echo json_encode($jsonDecode);
} else {
    //if json is not returned
    $result = array('status' => FAILMSG);
    echo json_encode($result);
}