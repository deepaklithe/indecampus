<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : DEEPAK PATIL                   */
/*   CREATION DATE : 05-12-2014                    */
/*   FILE TYPE : PHP                               */
/*   VERSION : 2.0 / 03-10-2016                    */
/* * ************************************************ */


// Cross Domain ajax request Headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

ini_set('memory_limit', '-1');
ini_set("display_errors", 1); // PLEASE CHANGE THIS VALUE TO 0 BEFORE PRODUCTION
// Include Required File...

require_once "include/globals.php"; // GLOBAL VAR FILE
//require_once "include/function_DL.php"; // DL FUNCTION FILE (COMMON FUNCTION FILE)//INCLUDED IN GLOBAL FILE

require_once "include/function_user_indeCampus.php"; // 
require_once "include/function_room_indeCampus.php"; // Room related function in it.
require_once "include/function_student_indeCampus.php"; // student related function in it.
require_once "include/function_developerMaster_indeCampus.php";
require_once "include/function_lookupmaster_indeCampus.php";
require_once "include/function_item_indeCampus.php";
require_once "include/function_payment_indeCampus.php";
require_once "include/function_property_indeCampus.php";
require_once "include/function_customer_indeCampus.php";
require_once "include/function_events_indeCampus.php";
require_once 'include/sendgrid-php.php';
require_once 'include/function_complaint_indeCampus.php';
require_once 'include/function_centralised_indeCampus.php';
require_once 'include/function_report_indeCampus.php';
require_once 'include/function_statusManagement_indeCampus.php';
//require_once "include/dompdf/autoload.inc.php"; // PDF 
// Default variable
$postData = isset($_POST) ? $_POST : "";
if (!is_object($postData)) {
    $postData = $postData;
} else {
    $postData = objectToArray(json_decode($postData['postData']));
}
$requestCase = $postData['postData']['requestCase'];
// print_r($postData);exit;
//FOR CHECK JWT TOKEN 
$checkSignature = checkJWTToken($postData); // CHECK THE AUTH OF JWT
//print_r($checkSignature); exit;
//$checkSignature['status'] = SCS;
if ($checkSignature['status'] != SCS) {

    $jsonData = json_encode(array("status" => $checkSignature['status']));
} else {
//    $securityTokenTemp  = isset($postData['postData']['dataValue']) ? explode("|--|",base64_decode(trim($postData['postData']['dataValue']))) : 0;
//    //$securityTokenTemp  = base64_decode(trim($postData['postData']['dataValue'])); 
//    $VsecurityToken     = substr($securityTokenTemp[0], 10,-10);
//    $DclientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : 0;
//    $DuserId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : 0;
//    $DorignalToken      = md5((int)$DclientId.'|'.(int)$DuserId);
//echo "<pre>"; print_r($checkSignature); exit;
    if ($postData['postData']['requestCase'] == "userlogin" || $postData['postData']['requestCase'] == "loadauth" || $postData['postData']['requestCase'] == "forgetPassword" || $postData['postData']['requestCase'] == "registerAlertId") {
        switch ($requestCase) {

            case "userlogin" : //user login     

                $result = checkLogin_BL($postData);
                $jsonData = json_encode($result);

                break;

            case "loadauth" : // load authorization

                $result = loadAuth_BL($postData);
                $jsonData = json_encode($result);

                break;

            case "registerAlertId" :
                if (count($resultCheckDevice) > 0) {
                    $result = registerAlertId($postData);
                    $jsonData = json_encode($result);
                } else {

                    $DData['ModuleId'] = "-10"; // // DEVICE MODULE CUSTOM          
                    $DData['ModuleName'] = "DEVICE API";
                    $DData['ActvitiyId'] = "-10"; // DEVICE ACTIVITY CUSTOM          
                    $DData['ActvitiyName'] = "API ACCESS";
                    $DData['Reason'] = "Unauthorized device access";
                    commonFunctionforAuthError_BL($postData, $DData);

                    $result = array("status" => DEVICENOTREGISTER);
                    $jsonData = json_encode($result);
                }
                break;

            /* ======= FORGET PASSWORD CASE START HERE ======== */
            case "forgetPassword" : //user login   
                // Get cURL resource
                $url = FORGETPATH;
                $curl = curl_init($url);
                // Set some options - we are passing in a useragent too here
                //set the url, number of POST vars, POST data
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, count($postData));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
                // Send the request & save response to $resp
                curl_exec($curl);
                //$result = forgetPasswordFP_BL($postData);
                // Close request to clear up some resources
                curl_close($curl);
                $jsonData = json_encode(array("status" => SCS));
                break;
            /* ======= FORGET PASSWORD CASE END HERE ======== */

            default:
                break;
        }
    } else {

        // IF PARAMETERS MISSING
        if (!isset($postData['postData']['clientId']) || !isset($postData['postData']['orgId']) || !isset($postData['postData']['userId']) || !isset($postData['postData']['requestCase']) || !isset($postData['postData']['dataValue'])) {
            //IF POSTDATA PARAMETER NOT COMING
            $result = array("status" => PARAM);
            $jsonData = json_encode($result);
        } else {

            //if($VsecurityToken == $DorignalToken){
            //FOR ADD API LOG DATA OF THIS USER TO STORE THE REQUEST
            //apiRequestLogData($postData['postData']['clientId'],$postData['postData']['orgId'], $postData['postData']['userId'],$requestCase,$postData);
            // If Required parameters is avaliable..*/
            switch ($requestCase) {
                /* ======= FOLLOWUP DASHBOARD CASE START HERE ======== */

                case 'getDashboard' : // FOLLOWUP DASHBOARD
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; // 1 = FOLLOWUP DASHBOARD
                    $activityName = "2"; // 2 = VIEW
                    $moduleId = "Dashboard";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getDashboard($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "createrole" : //CREATE ROLE
                    // CHECK EXTRA SECURITIES
                    $moduleName = "9";
                    $activityName = "36";

                    $moduleId = "Role Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditRole($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "editrole" : //EDIT ROLE
                    // CHECK EXTRA SECURITIES
                    $moduleName = "9";
                    $activityName = "38";

                    $moduleId = "Role Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditRole($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "deleterole" : // DELETE ROLE
                    // CHECK EXTRA SECURITIES
                    $moduleName = "9";
                    $activityName = "39";

                    $moduleId = "Role Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteRole_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getrole" : // GET ALL ROLE DETAILS
                    // CHECK EXTRA SECURITIES
                    $moduleName = "9";
                    $activityName = "37";

                    $moduleId = "Role Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getRoleDetails_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getroledetails" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "9";
                    $activityName = "37";

                    $moduleId = "Role Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getRoleDetails_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createModule' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "2";
                    $activityName = "5";

                    $moduleId = "Module Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createModule($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateModule' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "2";
                    $activityName = "7";

                    $moduleId = "Module Management";
                    $activityId = "Edit";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateModule($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteModule' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "2";
                    $activityName = "8";

                    $moduleId = "Module Management";
                    $activityId = "Delete";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteModule($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getModuleListing' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "2";
                    $activityName = "6";

                    $moduleId = "Module Management";
                    $activityId = "View";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getModuleListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createAvtivity' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "3";
                    $activityName = "9";

                    $moduleId = "Permission Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createActivity($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateAvtivity' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "3";
                    $activityName = "11";

                    $moduleId = "Permission Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateActivity($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteAvtivity' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "3";
                    $activityName = "12";

                    $moduleId = "Permission Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteActivity($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getPermissionListing' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "3";
                    $activityName = "10";
                    //$userId = 5;
                    $moduleId = "Permission Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getPermissionListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'addStudent' : // ADD STUDENT DETAIL
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11 = Student Management
                    $activityName = "44"; // 56 = Create
                    $moduleId = "Student Management";
                    $activityId = "Create";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditStudent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addRoom' : // Add ROOM API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "13"; // 13 - Room Management
                    $activityName = "53";  // 53 - Create
                    $moduleId = "Room Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditRoom($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'editRoom' : // EDIT ROOM API

                    $moduleName = "13"; // 13 = Room Management
                    $activityName = "55"; // 55 = Edit
                    $moduleId = "Room Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = editRoom($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteRoom' : // DELETE ROOM API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "13"; // 13 = Room Management
                    $activityName = "56"; // 56= Delete
                    $moduleId = "Room Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteRoom($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getlitheLookupManagement' :

                    $moduleName = "7";
                    $activityName = "26";
                    $moduleId = "Lookup Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = getLitheLookupListing_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createLitheLookup' : // FOR LISTING LITHELOOKUP

                    $moduleName = "7";
                    $activityName = "25";
                    //$userId = 5;
                    $moduleId = "Lookup Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = createLitheLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateLitheLookup' : // FOR UPDATE LITHELOOKUP
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "27";
                    //$userId = 5;
                    $moduleId = "Lookup Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateLitheLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteLitheLookup' : // FOR DELETE LITHELOOKUP
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "28";
                    $moduleId = "Lookup Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteLitheLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getLitheListingType" : // GET LITHE Type Listing
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "22";

                    $moduleId = "Type";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getLitheListingType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "createLitheType" : // CREATE LITHE TYPE
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "21";

                    $moduleId = "Type";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = litheCreateEditType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "editLitheType" : // UPDATE LITHE TYPE
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "23";

                    $moduleId = "Type";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = litheCreateEditType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "deleteLitheType" : // DELETE LITHE TYPE
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "24";

                    $moduleId = "Type";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteLitheType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "createType" : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "21";

                    $moduleId = "Type";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "editType" : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "23";

                    $moduleId = "Type";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "deleteType" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "24";

                    $moduleId = "Type";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getListingType" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "22";

                    $moduleId = "Type";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getListingType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getLookupManagement' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "26";

                    $moduleId = "Lookup Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getLookupListing_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createLookup' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "25";

                    $moduleId = "Lookup Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateLookup' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "27";

                    $moduleId = "Lookup Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteLookup' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "28";
                    //$userId = 5;
                    $moduleId = "Lookup Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createEditClient' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "13";

                    $moduleId = "Client Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditClient($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getClientListing' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "14";

                    $moduleId = "Client Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = clientListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createEditBranch' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "5";
                    $activityName = "17";

                    $moduleId = "Branch Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditBranch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'branchListing' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "5";
                    $activityName = "18";

                    $moduleId = "Branch Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = branchListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteBranch' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "5";
                    $activityName = "20";

                    $moduleId = "Branch Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteBranch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createUpdateSms' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "13";

                    $moduleId = "Client Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createUpdateSms($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'SMSData' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "14";

                    $moduleId = "Client Management";
                    $activityId = "view";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = SMSData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createUpdateEmail' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "13";

                    $moduleId = "Client Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createUpdateEmail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'EMAILData' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "14";

                    $moduleId = "Client Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = EMAILData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addPageSectionDetail' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "57";

                    $moduleId = "Page Content Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditPageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'editPageSectionDetail' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "59";

                    $moduleId = "Page Content Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditPageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getPageSectionDetail' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "58";

                    $moduleId = "Page Content Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getPageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'removePageSectionDetail' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "60";

                    $moduleId = "Page Content Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = removePageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createUser' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8";
                    $activityName = "29";

                    $moduleId = "User Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditUser_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "deleteLitheType" : // Delete Lithe Type
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "24";

                    $moduleId = "Type";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteLitheType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "createType" : // GET DOCUMENT LISTING FOR DOCUMENT CONTROLLER
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "21";

                    $moduleId = "Type";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "editType" : // GET DOCUMENT LISTING FOR DOCUMENT CONTROLLER
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "23";

                    $moduleId = "Type";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "deleteType" : // GET DOCUMENT LISTING FOR DOCUMENT CONTROLLER
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "24";

                    $moduleId = "Type";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getListingType" : // GET DOCUMENT LISTING FOR DOCUMENT CONTROLLER
                    // CHECK EXTRA SECURITIES
                    $moduleName = "6";
                    $activityName = "22";

                    $moduleId = "Type";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getListingType($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getLookupManagement' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "26";

                    $moduleId = "Lookup Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getLookupListing_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createLookup' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "25";

                    $moduleId = "Lookup Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateLookup' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "27";

                    $moduleId = "Lookup Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteLookup' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "7";
                    $activityName = "28";
                    //$userId = 5;
                    $moduleId = "Lookup Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteLookup($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createEditClient' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "13";

                    $moduleId = "Client Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditClient($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getClientListing' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "14";

                    $moduleId = "Client Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = clientListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createEditBranch' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "5";
                    $activityName = "17";

                    $moduleId = "Branch Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditBranch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'branchListing' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "5";
                    $activityName = "18";

                    $moduleId = "Branch Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = branchListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteBranch' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "5";
                    $activityName = "20";

                    $moduleId = "Branch Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteBranch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createUpdateSms' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "13";

                    $moduleId = "Client Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createUpdateSms($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'SMSData' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "14";

                    $moduleId = "Client Management";
                    $activityId = "view";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = SMSData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createUpdateEmail' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "13";

                    $moduleId = "Client Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createUpdateEmail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'EMAILData' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "4";
                    $activityName = "14";

                    $moduleId = "Client Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = EMAILData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addPageSectionDetail' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "57";

                    $moduleId = "Page Content Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditPageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'editPageSectionDetail' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "59";

                    $moduleId = "Page Content Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditPageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getPageSectionDetail' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "58";

                    $moduleId = "Page Content Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getPageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'removePageSectionDetail' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14";
                    $activityName = "60";

                    $moduleId = "Page Content Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = removePageSectionDetail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addItem' : // Add ITEM API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "61";  // 61 - Create
                    $moduleId = "Item Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditItem($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'editItem' : // EDIT ITEM API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "63";  // 63 - Edit
                    $moduleId = "Item Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditItem($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getItemsData' : // LIST ALL ITEM DATA
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "62";  // 62 - View
                    $moduleId = "Item Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getItemsData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteItem' : // Delete Item API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; //15- Item Management
                    $activityName = "64";  // 64 - Delete
                    $moduleId = "Item Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteItem($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'useredit' : // followup edit detail
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER MANAGEMENT
                    $activityName = "31"; // EDIT
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createEditUser_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getUserListing" : // ADD / UPDATE TARGET
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER MANAGEMENT
                    $activityName = "30"; // VIEW
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = userListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "deleteUser" : // user details by userid
                    // CHECK EXTRA SECURITIES

                    $moduleName = "8"; // 8 - user management
                    $activityName = "32";  // 32 - delete
                    $moduleId = "user management";
                    $activityId = "delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteUser_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'insertUserLevel' : // GET LEVEL 10 USERS FOR CREATE TARGET
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER LEVEL MANAGEMENT
                    $activityName = "29"; // CREATE
                    //$userId = 5;
                    $moduleId = "user management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = insertUpdateUserLevel($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateUserLevel' : // GET LEVEL 10 USERS FOR CREATE TARGET
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER LEVEL MANAGEMENT
                    $activityName = "31"; // CREATE
                    //$userId = 5;
                    $moduleId = "user management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = insertUpdateUserLevel($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getUserLevel' : // GET LEVEL 10 USERS FOR CREATE TARGET
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER LEVEL MANAGEMENT
                    $activityName = "30"; // VIEW
                    //$userId = 5;
                    $moduleId = "user management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getUserLevel($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "getDepartmentServiceList" : // LIST ALL DEPARTMENT SERVICE NAME
                    // CHECK EXTRA SECURITIES

                    $moduleName = "11"; // 11- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getDepartmentServiceList($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "addComplaint" : // Add Complaint
                    // CHECK EXTRA SECURITIES

                    $moduleName = "22"; // 14- Item Management
                    $activityName = "89";  // 1 - Create
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditComplaint($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "editComplaint" : // Edit Complaint
                    // CHECK EXTRA SECURITIES

                    $moduleName = "22"; // 15- Item Management
                    $activityName = "91";  // 63 - Create
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditComplaint($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "deleteComplaint" : // Delete Complaint
                    // CHECK EXTRA SECURITIES

                    $moduleName = "22"; // 22- Request And Complaint Management
                    $activityName = "92";  // 92 - Delete
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteComplaint($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getComplaintsData' : // LIST ALL COMPLAINTS DATA
                    // CHECK EXTRA SECURITIES
                    $moduleName = "22"; // 22- Request And Complaint Management
                    $activityName = "90";  // 90 - View
                    $moduleId = "Request And Complaint Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getComplaintsData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;



                case 'assignUserDepartment' : // GET LEVEL 10 USERS FOR CREATE TARGET
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER MANAGEMENT
                    $activityName = "33"; // VIEW
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "Assign";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = assignUserDepartment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getUserListingByName' : // GET LEVEL 10 USERS FOR CREATE TARGET
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // User Management
                    $activityName = "30"; // VIEW
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getUserListingByName($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'departmentListing' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // User Management
                    $activityName = "30"; // VIEW
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = departmentListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'removeUserFromDep' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // USER LEVEL MANAGEMENT
                    $activityName = "30"; // VIEW
                    //$userId = 5;
                    $moduleId = "User Level Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = removeUserFromDep($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;



                case 'editStudent' : // Edit STUDENT DETAIL
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11 = Student Management
                    $activityName = "46"; // 46 = Edit
                    $moduleId = "Student Management";
                    $activityId = "Edit";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditStudent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createDepartment' : // ADD STUDENT DETAIL
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11 = Student Management
                    $activityName = "46"; // 56 = Create
                    $moduleId = "Student Management";
                    $activityId = "Edit";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createDepartment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addChargePosting' : // Add Charge Posting API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "24"; // 24- Charge Posting Management
                    $activityName = "97";  // 97 - Create
                    $moduleId = "Charge Posting Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditChargePosting($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'editChargePosting' : // Edit Charge Posting API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "24"; // 15- Charge Posting Management
                    $activityName = "99";  // 63 - Edit
                    $moduleId = "Charge Posting Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditChargePosting($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'listChargePosting' : // Listing Charge Posting API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "24"; // 24- Charge Posting Management
                    $activityName = "98";  // 98 - View
                    $moduleId = "Charge Posting Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listChargePosting($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getPostingChargeData' : // Listing Charge Posting API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "24"; // 24- Charge Posting Management
                    $activityName = "98";  // 98 - View
                    $moduleId = "Charge Posting Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getPostingChargeData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'createInvoice' : // Create INVOICE API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 23- Invoice Management
                    $activityName = "93";  // 93 - Create
                    $moduleId = "Invoice Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createInvoice($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'listInvoiceData' : // Listing INVOICE DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 23- Invoice Management
                    $activityName = "94";  // 94- View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listInvoiceData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'cancelInvoiceTrans' : // CANCEL INVOICE TRANS API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 16- Invoice Management
                    $activityName = "96";  // 96 - Delete
                    $moduleId = "Invoice Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = cancelInvoiceTrans($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getInvoiceTrans' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getInvoiceTrans($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addPaymentData' : // Create Payment
                    // CHECK EXTRA SECURITIES
                    $moduleName = "17"; // 17- Payment Management
                    $activityName = "69";  // 69 - Create
                    $moduleId = "Payment Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addPaymentData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'listPaymentData' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "17"; // 17- Payment Management
                    $activityName = "70";  // 70 - View
                    $moduleId = "Payment Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listPaymentData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getUserLevelForUserManagement' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // 8- User Management
                    $activityName = "30";  // 30 - View
                    $moduleId = "User Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getUserLevelForUserManagement($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'userdetails' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // 8- User Management
                    $activityName = "30";  // 30 - View
                    $moduleId = "User Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getUserDetails_BL($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteStudentData' : // Delete Studnet DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11- Student Management
                    $activityName = "47";  // 47 - Delete
                    $moduleId = "Student Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteStudentData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getStudentData' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getStudentData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "getStateCity" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; //   1 -Dashboard
                    $activityName = "2"; // 2 -View
                    $moduleId = "Dashboard";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getStateCity($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addStudentAllotment' : // Create Student Allotment DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11 = Student Management
                    $activityName = "44"; // 44 = Create
                    $moduleId = "Student Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditStudentAllotment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'editStudentAllotment' : // Edit Student Allotment DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11 = Student Management
                    $activityName = "46"; // 46 = EDIT
                    $moduleId = "Student Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditStudentAllotment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getStudentAllotment' : // Listing Student Allotment DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getStudentAllotment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'listStudentAttachment' : // Listing Student Attachment DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listStudentAttachment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteStudentAttachment' : // Delete Student Attachment DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11- Student Management
                    $activityName = "47";  // 47 - DELETE
                    $moduleId = "Student Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteStudentAttachment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'roomList' : // ROOM LIST DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "13"; // 13 - Room Management
                    $activityName = "54";  // 54 -View
                    $moduleId = "Room Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = roomList($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'addProperty' : // Create Property DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "18"; // 18 = Property Management
                    $activityName = "73"; // 73 = Create
                    $moduleId = "Property Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditProperty($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'editProperty' : //Edit Property DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "18"; // 18 = Property Management
                    $activityName = "75"; // 75 = EDIT
                    $moduleId = "Property Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditProperty($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getPropertyData' : // List Property DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "18"; // 18 = Property Management
                    $activityName = "74"; // 74 = View
                    $moduleId = "Property Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getPropertyData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteProperty' : // Delete Property DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "18"; // 18 = Property Management
                    $activityName = "76"; // 76 = Delete
                    $moduleId = "Property Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteProperty($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addTimezone' : // Add Timezone DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "31"; // 31 = Timezone Management
                    $activityName = "116"; // 116 = Create
                    $moduleId = "Timezone Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditTimeZone($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'editTimezone' : // Edit Timezone DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "31"; // 31 = Timezone Management
                    $activityName = "118"; // 118 = Edit
                    $moduleId = "Timezone Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditTimeZone($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getTimezoneData' : // List Timezone DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "31"; // 31 = Timezone Management
                    $activityName = "117"; // 117 = View
                    $moduleId = "Timezone Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getTimeZoneData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteTimezone' : // Delete Timezone DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "31"; // 31 = Timezone Management
                    $activityName = "119"; // 119 = Delete
                    $moduleId = "Timezone Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteTimeZone($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addCustomer' : // Create Customer DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "19"; // 19 = Customer Management
                    $activityName = "77"; // 77 = Create
                    $moduleId = "Customer Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditCustomer($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'editCustomer' : // Edit Customer DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "19"; // 19 = Customer Management
                    $activityName = "79"; // 79 = EDIT
                    $moduleId = "Customer Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditCustomer($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getCustomerData' : // List Customer DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "19"; // 19 = Customer Management
                    $activityName = "78"; // 78 = View
                    $moduleId = "Customer Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getCustomerData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteCustomerData' : // Delete Customer DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "19"; // 19 = Customer Management
                    $activityName = "80"; // 80 = Delete
                    $moduleId = "Customer Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteCustomerData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'addInquiry' : // Create Inquiry DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "20"; // 20 = Inquiry Management
                    $activityName = "81"; // 81 = Create
                    $moduleId = "Inquiry Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditInquiry($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'editInquiry' : // Edit Inquiry DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "20"; // 20 = Inquiry Management
                    $activityName = "83"; // 83 = EDIT
                    $moduleId = "Inquiry Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditInquiry($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getInquiryData' : // List Inquiry DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "20"; // 20 = Inquiry Management
                    $activityName = "82"; // 82 = View
                    $moduleId = "Inquiry Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getInquiryData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteInquiryData' : // Delete Inquiry DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "20"; // 20 = Inquiry Management
                    $activityName = "84"; // 84 = Delete
                    $moduleId = "Inquiry Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteInquiryData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'customerSearch' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "19"; // 19 = Customer Management
                    $activityName = "78"; // 78 = View
                    $moduleId = "Customer Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = customerSearch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                //ADD BY DEVIKA.16.7.2018
                case 'getInquiryDataByProperty' : // List Inquiry DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "20"; // 20 = Inquiry Management
                    $activityName = "82"; // 82 = View
                    $moduleId = "Inquiry Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getInquiryDataByProperty($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }
                    $jsonData = json_encode($result);
                    break;
                //END BY DEVIKA.16.7.2018


                case 'updateInqStatus' : // List Inquiry DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "20"; // 20 = Inquiry Management
                    $activityName = "83"; // 83 = Edit
                    $moduleId = "Inquiry Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateInqStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }
                    $jsonData = json_encode($result);
                    break;

                //ADD BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI PHONE MASTER
                case 'updateInsertMultiEntryPhone' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 1- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateInsertMultiEntryPhone($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                //END BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI PHONE MASTER
                //ADD BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI ADDRESS MASTER
                case 'updateInsertMultiEntryAddress' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 1- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateInsertMultiEntryAddress($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                //ADD BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI EMAIL MASTER
                case 'updateInsertMultiEntryEmail' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 1- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateInsertMultiEntryEmail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                //END BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI EMAIL MASTER

                case 'getStudentDetailData' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 1- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getStudentDetailData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'eventBooking' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- Event Management
                    $activityName = "85";  // 85 - Create
                    $moduleId = "Event Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditEventsBooking($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getRoomCategory' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "13"; // 13- Item Management
                    $activityName = "54";  // 54 - View
                    $moduleId = "Room Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getRoomCategory($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createInvoiceStudentNew' : // Listing STUDENT DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 1- Invoice Management
                    $activityName = "65";  // 65 - Create
                    $moduleId = "Invoice Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createInvoiceStudentNew($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'itemSearchForChargePosting' : //ITEM SEARCH API FOR CHARGE POSTING.
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "63";  // 63 - View
                    $moduleId = "Item Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = itemSearchForChargePosting($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'studentSearch' : //STUDENT SEARCH API FOR CHARGE POSTING.
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11- Student Management
                    $activityName = "45";  // 45 - View
                    $moduleId = "Student Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = studentSearch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "userActiveDeActive" : //USER ACTIVE/IN ACTIVE STATUS
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8";
                    $activityName = "32";

                    $moduleId = "User Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = userActiveDeActive($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "studentResetPassword" : //Student Reset Password.
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11";
                    $activityName = "44";

                    $moduleId = "Student Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = studentResetPassword($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "getInvoiceListByStudent" : //Student Reset Password.
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 16- Invoice Management
                    $activityName = "66";  // 66- View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getInvoiceListByStudent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateStudentStatus' : // ADD STUDENT DETAIL
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; // 11 = Student Management
                    $activityName = "46"; // 46 = Edit
                    $moduleId = "Student Management";
                    $activityId = "Edit";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateStudentStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'transferStudentInvoice' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 16- Invoice Management
                    $activityName = "67";  // 67 - Edit
                    $moduleId = "Invoice Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = transferStudentInvoice($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getStudentInvoiceData' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 1- Invoice Management
                    $activityName = "66";  // 66 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getStudentInvoiceData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'sendStudentInvoiceMail' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 1- Invoice Management
                    $activityName = "66";  // 66 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = sendStudentInvoiceMail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                //ADD BY DEVIKA.1.8.2018
                case 'updateEventStatus' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- Event Management
                    $activityName = "87";  // 87- Edit
                    $moduleId = "Event Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateEventStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                //END BY DEVIKA.1.8.2018

                case 'addEvent' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- Event Management
                    $activityName = "85";  // 85 - Create
                    $moduleId = "Event Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditEvent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'editEvent' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- Event Management
                    $activityName = "87";  // 87 - Edit
                    $moduleId = "Event Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditEvent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                //ADD BY DEVIKA.1.8.2018
                case 'listEventNoSearch' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- IEvent Management
                    $activityName = "86";  // 86 - View
                    $moduleId = "Event Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listEventNoSearch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                //END BY DEVIKA.1.8.2018

                case 'getEventDetails' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- Event Management
                    $activityName = "86";  // 86 - View
                    $moduleId = "Event Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getEventDetails($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getEventInvoiceData' : // Listing INVOICE TRNAS POSTING DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21- Event Management
                    $activityName = "86";  // 86 - View
                    $moduleId = "Event Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getEventInvoiceData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'invoiceRemainingPayment' : // Create Payment
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = invoiceRemainingPayment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;



                case 'createDepartmentCategory' : // Create Payment
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "61";  // 61 - Create
                    $moduleId = "Item Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createDepartmentCategory($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'departmentCategoryList' : // //departmentCategoryList
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "62";  // 62 - View
                    $moduleId = "Item Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = departmentCategoryList($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'listDepartmentCategoryData' : // //departmentCategoryList
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; // 15- Item Management
                    $activityName = "62";  // 62 - View
                    $moduleId = "Item Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listDepartmentCategoryData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteDepartmentCategory' : // //departmentCategoryList
                    // CHECK EXTRA SECURITIES
                    $moduleName = "15"; //15- Item Management
                    $activityName = "64";  // 64 - Delete
                    $moduleId = "Item Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteDepartmentCategory($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'userSearch' : //STUDENT SEARCH API FOR CHARGE POSTING.
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // 8- User Management
                    $activityName = "30";  // 30 - View
                    $moduleId = "User Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = userSearch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'assignComplain' : //STUDENT SEARCH API FOR CHARGE POSTING.
                    // CHECK EXTRA SECURITIES
                    $moduleName = "22"; //22- Request And Complaint Management
                    $activityName = "106";  // 106 - Assign
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Assign";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = assignComplain($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteComplaintAttachment' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "22"; //22- Request And Complaint Management
                    $activityName = "92";  // 92 - Delete
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteComplaintAttachment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addComplaintComment' : // 
                    // ADD COMPLAINT COMMENT
                    $moduleName = "22"; //22- Request And Complaint Management
                    $activityName = "89";  // 89 - Create
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addComplaintComment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addEditStudentSuspension' : // 
                    // ADD COMPLAINT COMMENT
                    $moduleName = "25"; //25- Student Suspension & Termination Management
                    $activityName = "101";  // 101 - Create
                    $moduleId = "Student Suspension & Termination Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditStudentSuspension($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;



                case 'deleteStudentSuspension' : // 
                    // DELETE STUDENT SUSPENSION

                    $moduleName = "25"; //25- Student Suspension & Termination Management
                    $activityName = "104";  // 104 - Delete
                    $moduleId = "Student Suspension & Termination Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteStudentSuspension($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'revokeStudentSuspension' : // 
                    // ADD COMPLAINT COMMENT
                    $moduleName = "25"; //25- Student Suspension & Termination Management
                    $activityName = "103";  // 103 - Edit
                    $moduleId = "Student Suspension & Termination Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = revokeStudentSuspension($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'closeComplaintData' : // 
                    // ADD COMPLAINT COMMENT
                    $moduleName = "22"; //22- Request And Complaint Management
                    $activityName = "92";  // 92 - Delete
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = closeComplaintData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'reopenComplaint' : // 
                    $moduleName = "22"; //22- Request And Complaint Management
                    $activityName = "89";  // 89 - Create
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = reopenComplaint($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'cancelComplaint' : // 
                    $moduleName = "22"; //22- Request And Complaint Management
                    $activityName = "92";  // 92 - Delete
                    $moduleId = "Request And Complaint Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = cancelComplaint($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteComplaintAttachment' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; //11- Student Management
                    $activityName = "47";  // 47 - Delete
                    $moduleId = "Student Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteComplaintAttachment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateChargePosting' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "24"; //24- Charge Posting Management
                    $activityName = "99";  // 99 - Edit
                    $moduleId = "Charge Posting Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateChargePosting($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getCentraliseInvoices' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; //23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getCentraliseInvoices($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'createReceipt' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; //23- Invoice Management
                    $activityName = "93";  // 93 - Create
                    $moduleId = "Invoice Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createReceipt($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'createGeneralInvoice' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; //23- Invoice Management
                    $activityName = "93";  // 93 - Create
                    $moduleId = "Invoice Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createGeneralInvoice($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "renewStudentAllotment" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; //   11 -Student Management
                    $activityName = "44"; // 44 -Create
                    $moduleId = "Student Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = renewStudentAllotment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "listStudentSuspensionData" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; //   11 -Student Management
                    $activityName = "45"; // 45 -View
                    $moduleId = "Student Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listStudentSuspensionData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "studentEnrollmentReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = studentEnrollmentReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteSuppesionAttachment' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "25"; //25- Student Suspension & Termination Management
                    $activityName = "104";  // 104 - Delete
                    $moduleId = "Student Suspension & Termination Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteSuppesionAttachment($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getDashboardData' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; //1  - Dashboard
                    $activityName = "2";  // 2 - View
                    $moduleId = "Dashboard";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getDashboardData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "revenueReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = revenueReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "requestReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = requestReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "complaintReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = complaintReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "suspensionReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = suspensionReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "rentalSpaceReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = rentalSpaceReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "eventMangementReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = eventMangementReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "studentTermination" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "11"; //   1 -Student Management
                    $activityName = "44"; // 44 -Create
                    $moduleId = "Student Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = studentTermination($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "terminationReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = terminationReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'eventSearch' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; // 21 = Event Management
                    $activityName = "86"; // 86 = View
                    $moduleId = "Item Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = eventSearch($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getSuperVisorListing' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; //   8 -User Management
                    $activityName = "30"; // 30 -View
                    $moduleId = "User Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getSuperVisorListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'addCmsData' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14"; //   14 -Page Content Management
                    $activityName = "57"; // 57 -Create
                    $moduleId = "Page Content Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addCmsData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'listCmsData' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "14"; //   14 -Page Content Management
                    $activityName = "58"; // 58 -View
                    $moduleId = "Page Content Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listCmsData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                // ADDED BY KAVITA PATEL ON 16-08-2018 START
                case "createUpdateDashbordGrid" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; // 1= Dashboard
                    $activityName = "2"; // 2 = VIEW
                    //$userId = 5;
                    $moduleId = "Dashboard";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = createUpdateDashbordGrid($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "getDashbordGrid" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; // 1= Dashboard
                    $activityName = "2"; // 2 = VIEW
                    //$userId = 5;
                    $moduleId = "Dashboard";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getDashbordGrid($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "deleteDashbordGrid" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; // 1= Dashboard
                    $activityName = "4"; // 4 = Delete
                    //$userId = 5;
                    $moduleId = "Dashboard";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteDashbordGrid($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "checkUncheckDashbordUserWise" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; // 1= Dashboard
                    $activityName = "3"; // 3 = Edit
                    //$userId = 5;
                    $moduleId = "Dashboard";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = checkUncheckDashbordUserWise($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                // ADDED BY KAVITA PATEL ON 16-08-2018 END    


                case 'addEditNonAvailingService' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "27"; //   27 -Non Availing Services
                    $activityName = "107"; // 107 -Create
                    $moduleId = "Non Availing Services";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addEditNonAvailingService($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'listStudentNonAvailingService' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "27"; //   27 -Non Availing Services
                    $activityName = "108"; // 108 -View
                    $moduleId = "Non Availing Services";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = listStudentNonAvailingService($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'deleteStudentService' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "27"; //   27 -Non Availing Services
                    $activityName = "110"; // 110 -Delete
                    $moduleId = "Non Availing Services";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteStudentService($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'updateServiceStatus' : // Customer Search DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "27"; //   27 -Non Availing Services
                    $activityName = "109"; // 109 -Edit
                    $moduleId = "Non Availing Services";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateServiceStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getCustomerInvoiceData' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 16- Invoice Management
                    $activityName = "66";  // 66 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getCustomerInvoiceData($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'sendCustomerInvoiceMail' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "16"; // 16- Invoice Management
                    $activityName = "66";  // 66 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = sendCustomerInvoiceMail($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getCategoryForStatus' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "41";  // 
                    $moduleId = "Status Management";
                    $activityId = "View";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = getCategoryForStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }
                    $jsonData = json_encode($result);
                    break;


                case 'updateStatusCategory' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "42";  // 
                    $moduleId = "Status Management";
                    $activityId = "update";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = createUpdateCategoryForStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }
                    $jsonData = json_encode($result);
                    break;


                case 'createCategoryForStatus' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "40";  // 
                    $moduleId = "Status Management";
                    $activityId = "CREATE";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = createUpdateCategoryForStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getStatusListing' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "41";  // 
                    $moduleId = "Status Management";
                    $activityId = "View";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = getStatusListing($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'createStatus' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "40";  // 
                    $moduleId = "Status Management";
                    $activityId = "CREATE";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = createEditStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'updateStatus' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "42";  // 
                    $moduleId = "Status Management";
                    $activityId = "UPDATE";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = createEditStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'deleteStatus' :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 
                    $activityName = "43";  // 
                    $moduleId = "Status Management";
                    $activityId = "DELETE";

                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {

                            $result = deleteStatus($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "changeThirdPartyFlag" : // GET DOCUMENT LISTING FOR DOCUMENT CONTROLLER
                    // CHECK EXTRA SECURITIES
                    $moduleName = "10"; // 10 = STATUS MANAGEMENT
                    $activityName = "42"; // 42  = EDIT
                    //$userId = 5;
                    $moduleId = "Status Management";
                    $activityId = "Edit";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = changeThirdPartyFlag($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "updateStatusOnClick" : // GET DOCUMENT LISTING FOR DOCUMENT CONTROLLER
                    // CHECK EXTRA SECURITIES
                    $moduleName = "1"; // 
                    $activityName = "2"; // 
                    //$userId = 5;
                    $moduleId = "Dashboard";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = updateStatusOnClick($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case "saveCentralisedDataToTally" : // ADDED BY KAUSHA SHAH ON 20-08-2018			
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; //23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {

                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = prepareDataForTally($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);

                    break;

                //COMMENT BY DEVIKA.4.9.2018
//                case "eposReceipt" : // ADDED BY DEVIKA  ON 21-08-2018			
//                    // CHECK EXTRA SECURITIES
//                    $moduleName = "23"; //23- Invoice Management
//                    $activityName = "94";  // 94 - View
//                    $moduleId = "Invoice Management";
//                    $activityId = "View";
//                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
//                    if ($checkAuth['status'] == SCS) {
//
//                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
//                            $result = eposReceipt($postData);
//                        } else {
//                            $result = array("status" => ILLIGELACCESS);
//                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
//                            $DData['OrgMasterReason'] = "Org Master Error";
//                            commonFunctionforAuthError_BL($postData, $DData);
//                        }
//                    } else {
//                        $result = array("status" => ILLIGELACCESS);
//                        $DData['ModuleId'] = $moduleName;
//                        $DData['ModuleName'] = $moduleId;
//                        $DData['ActvitiyId'] = $activityName;
//                        $DData['ActvitiyName'] = $activityId;
//                        $DData['Reason'] = "Unauthorized Access";
//                        commonFunctionforAuthError_BL($postData, $DData);
//                    }
//
//                    $jsonData = json_encode($result);
//
//                    break;
//
//                case "eposCreditNoteTransaction" : // ADDED BY DEVIKA  ON 21-08-2018			
//                    // CHECK EXTRA SECURITIES
//                    $moduleName = "23"; //23- Invoice Management
//                    $activityName = "94";  // 94 - View
//                    $moduleId = "Invoice Management";
//                    $activityId = "View";
//                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
//                    if ($checkAuth['status'] == SCS) {
//
//                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
//                            $result = eposCreditNoteTransaction($postData);
//                        } else {
//                            $result = array("status" => ILLIGELACCESS);
//                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
//                            $DData['OrgMasterReason'] = "Org Master Error";
//                            commonFunctionforAuthError_BL($postData, $DData);
//                        }
//                    } else {
//                        $result = array("status" => ILLIGELACCESS);
//                        $DData['ModuleId'] = $moduleName;
//                        $DData['ModuleName'] = $moduleId;
//                        $DData['ActvitiyId'] = $activityName;
//                        $DData['ActvitiyName'] = $activityId;
//                        $DData['Reason'] = "Unauthorized Access";
//                        commonFunctionforAuthError_BL($postData, $DData);
//                    }
//
//                    $jsonData = json_encode($result);
//
//                    break;
//                case "eposSalesTransaction" : // ADDED BY DEVIKA  ON 21-08-2018			
//                    // CHECK EXTRA SECURITIES
//                    $moduleName = "23"; //23- Invoice Management
//                    $activityName = "94";  // 94 - View
//                    $moduleId = "Invoice Management";
//                    $activityId = "View";
//                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
//                    if ($checkAuth['status'] == SCS) {
//
//                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
//                            $result = eposSalesTransaction($postData);
//                        } else {
//                            $result = array("status" => ILLIGELACCESS);
//                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
//                            $DData['OrgMasterReason'] = "Org Master Error";
//                            commonFunctionforAuthError_BL($postData, $DData);
//                        }
//                    } else {
//                        $result = array("status" => ILLIGELACCESS);
//                        $DData['ModuleId'] = $moduleName;
//                        $DData['ModuleName'] = $moduleId;
//                        $DData['ActvitiyId'] = $activityName;
//                        $DData['ActvitiyName'] = $activityId;
//                        $DData['Reason'] = "Unauthorized Access";
//                        commonFunctionforAuthError_BL($postData, $DData);
//                    }
//
//                    $jsonData = json_encode($result);
//
//                    break;
                //COMMENT BY DEVIKA.4.9.2018

                case 'deleteEvent' : // Delete EVENT API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "21"; //21- Event Management
                    $activityName = "88";  // 88 - Delete
                    $moduleId = "Event Management";
                    $activityId = "Delete";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = deleteEvent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getclosingbalanceByStudent' : // Create Payment
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getclosingbalanceByStudent($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'getCentraliseTransactionInvoices' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; //23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getCentraliseTransactionInvoices($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "changePasswordUser" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // 1= dashbord
                    $activityName = "35"; // 2 = Change Password
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "Change Password";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = changePasswordUser($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "resetPasswordUser" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "8"; // 1= dashbord
                    $activityName = "35"; // 2 = Change Password
                    //$userId = 5;
                    $moduleId = "User Management";
                    $activityId = "Change Password";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = resetPasswordUser($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'ledgerHistory' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; //23- Invoice Management
                    $activityName = "94";  // 94 - View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = ledgerHistory($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case 'getPropertyTimeZone' : // 
                    // CHECK EXTRA SECURITIES
                    $moduleName = "18"; //18- Property Management
                    $activityName = "74";  // 74 - View
                    $moduleId = "Property Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getPropertyTimeZone($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;

                case 'addTimeZoneToProperty' : // Create Property DATA API
                    // CHECK EXTRA SECURITIES
                    $moduleName = "31"; // 31 = Timezone Management
                    $activityName = "116"; // 116 = Create
                    $moduleId = "Timezone Management";
                    $activityId = "Create";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    //print_r($checkAuth); exit;

                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = addTimeZoneToProperty($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;


                case "allotmentReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = allotmentReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                    
                    
                    case "studentDepartmentwiseComplaintReport" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "26"; //   26 -Report Management
                    $activityName = "105"; // 105-View
                    $moduleId = "Report Management";
                    $activityId = "VIEW";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = studentDepartmentwiseComplaintReport($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                    
                    
                    case "getUsageHistory" :
                    // CHECK EXTRA SECURITIES
                    $moduleName = "23"; // 23- Invoice Management
                    $activityName = "94";  // 94- View
                    $moduleId = "Invoice Management";
                    $activityId = "View";
                    $checkAuth = checkAuthRequestoFOrgType($moduleName, $activityName, $postData['postData']['userId'], $postData['postData']['clientId'], $postData['postData']['orgId']);
                    if ($checkAuth['status'] == SCS) {
                        if ($checkAuth['data'][0]['PK_ORG_ID'] === $postData['postData']['orgId']) {
                            $result = getUsageHistory($postData);
                        } else {
                            $result = array("status" => ILLIGELACCESS);
                            $DData['OrgMasterData'] = $checkAuth['data'][0]['PK_ORG_ID'];
                            $DData['OrgMasterReason'] = "Org Master Error";
                            commonFunctionforAuthError_BL($postData, $DData);
                        }
                    } else {
                        $result = array("status" => ILLIGELACCESS);
                        $DData['ModuleId'] = $moduleName;
                        $DData['ModuleName'] = $moduleId;
                        $DData['ActvitiyId'] = $activityName;
                        $DData['ActvitiyName'] = $activityId;
                        $DData['Reason'] = "Unauthorized Access";
                        commonFunctionforAuthError_BL($postData, $DData);
                    }

                    $jsonData = json_encode($result);
                    break;
                    


                default: // default case
                    $result['status'] = CASEERROR;
                    $jsonData = json_encode($result);
                    break;
            }
            //}
            //            else{
            //                result = array("status" => TOKANMISMATCH);
            //                $jsonData = json_encode($result);
            //                // CALL THE ERROR FUNCTION TO GET MAIL 
            //                $DData['DTokenId'] = $DorignalToken;
            //                $DData['Reason'] = "Unauthorized Access (Token Mis-match)";  
            //                commonFunctionforAuthError_BL($postData,$DData);
            //            }
        }
    }
}
/* ======= RETURN DATA AFTER REQUESTING BY ANY CALL FROM API ========= */
if ($jsonData) {
    $checkSignature['data'] = (!empty($checkSignature['data'])) ? $checkSignature['data'] : "";
    // if return json
    header("Authorization: Bearer " . trim($checkSignature['data']));
    header("Content-Type: application/x-www-form-urlencoded; charset=UTF-8");
    echo $jsonData;
    //print_r($jsonDecode); exit;
} else {
    //if json is not returned
    $result = array('status' => FAILMSG);
    echo json_encode($result);
}
?>
