<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 06-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR ADD PAYMENT DETAIL. 
function addPaymentData($postData) {


// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";
    $bankName = isset($postData['postData']['bankName']) ? addslashes(trim($postData['postData']['bankName'])) : "";
    $branchName = isset($postData['postData']['branchName']) ? addslashes(trim($postData['postData']['branchName'])) : "";
    $chequeDDNeftNo = isset($postData['postData']['chequeDDNeftNo']) ? addslashes(trim($postData['postData']['chequeDDNeftNo'])) : "";
    $chequeDDNeftDate = !empty($postData['postData']['chequeDDNeftDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chequeDDNeftDate'])))) : "0000-00-00";

    $paidAmount = isset($postData['postData']['paidAmount']) ? addslashes(trim($postData['postData']['paidAmount'])) : "";
    $paymentMode = isset($postData['postData']['paymentMode']) ? addslashes(trim($postData['postData']['paymentMode'])) : "";
    $paymentModeName = isset($postData['postData']['paymentModeName']) ? addslashes(trim($postData['postData']['paymentModeName'])) : "";
    $paymentRemark = isset($postData['postData']['paymentRemark']) ? addslashes(trim($postData['postData']['paymentRemark'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $invoiceTotalAmt = isset($postData['postData']['invoiceTotalAmt']) ? addslashes(trim($postData['postData']['invoiceTotalAmt'])) : "";
    $lastFourDigitCardNo = isset($postData['postData']['lastFourDigitCardNo']) ? addslashes(trim($postData['postData']['lastFourDigitCardNo'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($paymentMode) && !empty($invoiceId) && !empty($paidAmount)) { // CHECK REQURIED FIELD CONDITION
        if (is_numeric($paidAmount)) {

            // PAYMENT INSERT ARRAY.
            //$sqlCheckPayment = "SELECT IF(SUM(PM.PAID_AMOUNT)>0,SUM(PM.PAID_AMOUNT),0) AS PAID_AMOUNT,IM.TOTAL_CHARGES AS INVOICE_AMOUNT,IM.FK_STUDENT_ID,IM.FK_CUST_ID FROM " . INVOICEMASTER . " IM LEFT JOIN " . PAYMENTMASTER . " PM ON PM.FK_INVOICE_ID=IM.PK_INVOICE_ID WHERE IM.DELETE_FLAG ='0' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' AND IM.PK_INVOICE_ID ='" . $invoiceId . "' GROUP BY(PM.FK_INVOICE_ID)"; // QUERY FOR PAYMENT AMOUNT AGAINST INVOICE NO.
            $sqlGetInvoice = "SELECT IM.TOTAL_CHARGES AS INVOICE_AMOUNT,IM.FK_STUDENT_ID,IM.FK_CUST_ID FROM " . INVOICEMASTER . " IM WHERE IM.DELETE_FLAG ='0' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' AND IM.C_INVOICE_ID ='" . $invoiceId . "' LIMIT 0,1"; // QUERY FOR PAYMENT AMOUNT AGAINST INVOICE NO.
            $resultGetInvoice = fetch_rec_query($sqlGetInvoice); // RESULT FOR PAYMENT AMOUNT AGAINST INVOICE NO.
            if (count($resultGetInvoice) > 0) {

                $sqlGetPaymnet = "SELECT IF(SUM(PM.PAID_AMOUNT)>0,SUM(PM.PAID_AMOUNT),0) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " PM WHERE PM.DELETE_FLAG = '0' AND PM.FK_INVOICE_ID = '" . $invoiceId . "'";
                $resultCheckPayment = fetch_rec_query($sqlGetPaymnet);

                if (count($resultCheckPayment) > 0) {
                    $totalPayment = floatval($resultCheckPayment[0]['PAID_AMOUNT']) + floatval($paidAmount); // TOTAL PAYMENT.
                    if (floatval($totalPayment) > floatval($resultGetInvoice[0]['INVOICE_AMOUNT'])) { // CHECK TOTAL PAYMENT IS NOT GREATER THAN INVOICE AMOUNT.
                        $result = array("status" => PAIDAMTISMOTMOREREMAININGAMT);
                        http_response_code(400);
                        return $result;
                    }
                }


                $customerId = $resultGetInvoice[0]['FK_CUST_ID'];
                $studentId = $resultGetInvoice[0]['FK_STUDENT_ID'];



                $msg = "";
                $insertPaymentArray = Array(); //PAYMENT ARRAY CREATE
                $insertPaymentArray['C_PAYMENT_ID'] = generateId(PAYMENTMASTER, 'PAY', $clientId); // AGAINST INVOICE
                $insertPaymentArray['PAYMENT_TYPE'] = '2'; // AGAINST INVOICE
                if ($type == 'customer') {
                    $insertPaymentArray['FK_CUST_ID'] = $customerId;
                } else {
                    $insertPaymentArray['FK_STUDENT_ID'] = $studentId;
                }

                $insertPaymentArray['FK_CLIENT_ID'] = $clientId;
                $insertPaymentArray['FK_ORG_ID'] = $orgId;
                $insertPaymentArray['BANK_NAME'] = $bankName;
                $insertPaymentArray['BRANCH_NAME'] = $branchName;
                $insertPaymentArray['PAYMENT_REFERENCE_NO'] = $chequeDDNeftNo;
                $insertPaymentArray['PAYMENT_REFERENECE_DATE'] = $chequeDDNeftDate;
                $insertPaymentArray['FK_INVOICE_ID'] = $invoiceId;
                $insertPaymentArray['PAID_AMOUNT'] = $paidAmount;
                $insertPaymentArray['PAYMENT_MODE'] = $paymentMode;
                $insertPaymentArray['PAYMENT_REMARK'] = $paymentRemark;
                $insertPaymentArray['PAYMENT_DATE'] = date("Y-m-d");
                $insertPaymentArray['LAST_FOUR_DIGIT'] = $lastFourDigitCardNo;
                $insertPaymentArray['CREATED_BY'] = $userId;
                $insertPaymentArray['CREATED_BY_DATE'] = time();
                $insertItem = insert_rec(PAYMENTMASTER, $insertPaymentArray); // INSERT RECORD IN PAYMENT_MASTER TABLE.          
                if (!isset($insertItem['lastInsertedId']) || $insertItem['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . PAYMENTMASTER;
                }
                if ($msg == "") {

                    if ($type == 'customer') {
                        $sqlGetStudent = "SELECT PK_CUST_ID,C_CUST_ID,FIRST_NAME,LAST_NAME FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID='" . $studentId . "' AND DELETE_FLAG=0 LIMIT 0,1 ";
                        $resGetStudent = fetch_rec_query($sqlGetStudent);
                    } else {
                        $sqlGetStudent = "SELECT MEMBERSHIP_NUMBER,FIRST_NAME,LAST_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "' AND DELETE_FLAG=0 LIMIT 0,1 ";
                        $resGetStudent = fetch_rec_query($sqlGetStudent);
                    }
                    $totalPaidAmount = 0;
                    $sqlGetPayment = "SELECT FK_INVOICE_ID,SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE FK_INVOICE_ID='" . $invoiceId . "' AND DELETE_FLAG=0 LIMIT 0,1 ";
                    $resGetPayment = fetch_rec_query($sqlGetPayment);
                    if (count($resGetPayment) > 0) {
                        $totalPaidAmount = $resGetPayment[0]['PAID_AMOUNT'];
                    }

                    $pendingAmount = $invoiceTotalAmt - $totalPaidAmount;


                    $pkReceiptId = generateId(CENTRALISECUSTOMRECEIPT, 'REC', $clientId);

                    //AGAINST INVOICE AND AGAINST ACOOUNT IN INSERT CENTRALISED_CUSTOME_RECIPT CODE.

                    if ($type == 'customer') {
                        $insertReceipt['FK_STUD_ID'] = $studentId;
                        $insertReceipt['MEMBERSHIP_NUMBER'] = $resGetStudent[0]['C_CUST_ID'];
                        $insertReceipt['STUDENT_NAME'] = $resGetStudent[0]['FIRST_NAME'];
                    } else {
                        $insertReceipt['FK_STUD_ID'] = $studentId;
                        $insertReceipt['MEMBERSHIP_NUMBER'] = $resGetStudent[0]['MEMBERSHIP_NUMBER'];
                        $insertReceipt['STUDENT_NAME'] = $resGetStudent[0]['FIRST_NAME'] . " " . $resGetStudent[0]['LAST_NAME'];
                    }

                    $insertReceipt['PK_RECEIPT_ID'] = $pkReceiptId;
                    $insertReceipt['INVOICE_ID'] = $invoiceId; //AGAINST INVOICE ID=INVOICEiD  AND AGAINSTACCOUNT=-1;
                    $insertReceipt['INVOICE_TYPE'] = 2; //1=AGAINST ACOOUNT, 2=AGAINST INVOICE
                    $insertReceipt['VOUCHER_TYPE'] = 'Receipt';
                    $insertReceipt['FK_CLIENT_ID'] = $clientId;
                    $insertReceipt['FK_ORG_ID'] = $orgId;

                    $insertReceipt['RECEIPT_DATE'] = date("Y-m-d");
                    $insertReceipt['PAYMENT_MODE_ID'] = $paymentMode;
                    $insertReceipt['PAYMENT_MODE'] = $paymentModeName;
                    $insertReceipt['BANK_NAME'] = $bankName;
                    $insertReceipt['BANK_BRANCH_NAME'] = $branchName;
                    $insertReceipt['CHEQUE_NO'] = $chequeDDNeftNo;
                    $insertReceipt['PAYMENT_AMOUNT'] = $paidAmount;
                    $insertReceipt['TOTAL_PAYMENTAMOUNT'] = $invoiceTotalAmt;
                    $insertReceipt['PENDING_AMOUNT'] = $pendingAmount;
                    $insertReceipt['NARRATION'] = $pkReceiptId." - ". $paymentRemark;
                    $insertReceipt['LAST_FOUR_DIGIT'] = $lastFourDigitCardNo;

                    $insertReceipt['CREATED_BY'] = $userId;
                    $insertReceipt['CREATED_BY_DATE'] = time();
                    $insertReceipt['DELETE_FLAG'] = 0;
                    //print_r($insertReceipt); die;
                    $requestReceipt = insert_rec(CENTRALISECUSTOMRECEIPT, $insertReceipt);

                    if ($type != 'customer') {
                        $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;
                        $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT,CLOSINGBAL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $studentId . "' AND DELETE_FLAG ='0'";
                        $resStudentData = fetch_rec_query($sqlStudentData); // 
                        if (count($resStudentData) > 0) {
                            $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                            $billedAmt = $resStudentData[0]['BILL_AMT'];
                            $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                        }


                        $closingBalAmt = floatval($billedAmt) - (floatval($receiptAmt) + floatval($paidAmount));
                        $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR";

                        $updateStudentArr = ARRAY();
                        $updateStudentArr['RECEIPT_AMT'] = floatval($paidAmount) + floatval($receiptAmt);
                        $updateStudentArr['RECEIPT_TYPE'] = 'CR';
                        $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
                        $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;

                        $updateStudentArr['CHANGED_BY'] = $userId;
                        $updateStudentArr['CHANGED_BY_DATE'] = time();
                        $whereStudCond = "PK_STUD_ID='" . $studentId . "'";
                        $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                    }

                    /* START : Receipt Sent to the student and as well as Parents */

                    if ($type != 'customer') {
                        // $pkReceiptId
                        $sqlStudentMailData = "SELECT CR.PK_RECEIPT_ID,CR.RECEIPT_DATE,CR.PAYMENT_AMOUNT,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.TOTAL_CHARGES,CR.STUDENT_NAME,CR.PAYMENT_MODE,CR.BANK_BRANCH_NAME,SM.EMAIL AS STUD_EMAIL,PM.EMAIL AS PARENT_EMAIL,LM.VALUE AS BANK_NAME,CR.CHEQUE_NO,CR.FK_STUD_ID FROM " . CENTRALISECUSTOMRECEIPT . " CR INNER JOIN " . INVOICEMASTER . " IM ON CR.INVOICE_ID =IM.C_INVOICE_ID LEFT JOIN " . LOOKUPMASTER . " LM ON LM.PK_LOOKUP_ID=CR.BANK_NAME INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =CR.FK_STUD_ID INNER JOIN " . PARENTMASTER . " PM ON  PM.FK_STUD_ID = CR.FK_STUD_ID AND PM.USER_NAME !='' WHERE PK_RECEIPT_ID='" . $pkReceiptId . "' AND CR.DELETE_FLAG ='0' AND CR.FK_CLIENT_ID ='" . $clientId . "' AND CR.FK_ORG_ID ='" . $orgId . "'";
                        $resStudentMailData = fetch_rec_query($sqlStudentMailData);


                        if (!empty($resStudentMailData[0]['STUD_EMAIL'])) {



                            $paymentDate = $instrumentDate = $roomName = "";
                            if ($chequeDDNeftDate != "" && $chequeDDNeftDate != "0000-00-00") {
                                $instrumentDate = date("d-m-Y", strtotime($chequeDDNeftDate));
                            }
                            if (isset($resStudentMailData[0]['RECEIPT_DATE']) && $resStudentMailData[0]['RECEIPT_DATE'] != "0000-00-00") {
                                $paymentDate = date("d-m-Y", strtotime($resStudentMailData[0]['RECEIPT_DATE']));
                            }

                            $sqlStudentRoomData = "SELECT ROOM_NAME FROM " . ALLOTMASTER . " WHERE FK_STUDENT_ID ='" . $resStudentMailData[0]['FK_STUD_ID'] . "' AND FK_CLIENT_ID ='" . $clientId . "' AND DELETE_FLAG ='0' ORDER BY PK_ALLOTMENT_ID DESC LIMIT 0,1";
                            $resStudentRoomData = fetch_rec_query($sqlStudentRoomData);
                            if (count($resStudentRoomData) > 0) {
                                $roomName = $resStudentRoomData[0]['ROOM_NAME'];
                            }

                            $messageBody = '<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Receipt</title>

	</head>
    <style>
        html { font: 16px/1 Open Sans, sans-serif; overflow: auto; }
        article:after { clear: both; content: ""; display: table; }
        header:after { clear: both; content: ""; display: table; }
    </style>
    <div style="height:0.1in;padding:0 0.4in 0 0.55in;background-color:#ffffff;color:#333333;font-size:9pt"></div>
	<body style=" background: #FFF; border-radius: 1px; box-sizing: border-box; margin: 0 auto; overflow: hidden; padding: 0px 0.4in 0px 0.4in; ">  
        <table style="border-collapse: collapse;width: 100%;">
           <tr>
                <td valign="top" style="font-size:12px; color:#000000;padding:10px 5px;width:29%;">                   
<!--               <img src="#" alt="" style="height:86px; width:auto;">-->
                </td>
                <td valign="top" style="font-size:12px; color:#000000;padding:10px 5px;width:64%;">                   
                     
                    <address style="vertical-align:top; padding-top:10px; text-align:center; font-size:12px; line-height:14px;font-style:normal;display:inline-block;">
                        <span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020
                    </address>
                </td>
            </tr> 
        
        </table>
        <table class="balance" style="border-collapse: collapse; border-spacing: 0px; font-size: 75%; table-layout: fixed; width: 100%;margin: 0 0 1em;">
                      
            <tr>
                <td colspan="2" style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Receipt Number : <span>' . $resStudentMailData[0]['PK_RECEIPT_ID'] . '</span></td>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Receipt Date : <span>Date: ' . $paymentDate . '</span></td>                
            </tr>
            <tr>
                <td colspan="3" style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Receive With Thanks From :  <span>' . $resStudentMailData[0]['STUDENT_NAME'] . ' </span></td>
            </tr>
            <tr>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Room Number :  <span>' . $roomName . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Rs. :<span>' . $resStudentMailData[0]['PAYMENT_AMOUNT'] . '</span></td>                
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Rs. In Word :  <span>' . convertNumber($resStudentMailData[0]['PAYMENT_AMOUNT']) . '	</span></td> 
            </tr>
            <tr>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Invoice Number :  <span>' . $resStudentMailData[0]['C_INVOICE_ID'] . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Invoice Date :<span>' . $paymentDate . '</span></td> 
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Invoice Amount :  <span>' . $resStudentMailData[0]['TOTAL_CHARGES'] . '</span></td>   
            </tr>
                
                      <tr>                   
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Mode Of Payment :<span>' . $resStudentMailData[0]['PAYMENT_MODE'] . '</span></td>                
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Instrument Number:  <span>' . $resStudentMailData[0]['CHEQUE_NO'] . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Instrument Date :<span>' . $instrumentDate . '</span></td>
            </tr>
            <tr>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;"> Instrument Amount :  <span>' . $resStudentMailData[0]['PAYMENT_AMOUNT'] . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Bank Name :<span>' . $resStudentMailData[0]['BANK_NAME'] . '</span></td>                
                <td style="border:1px #ddd solid; font-size:14px; color:#000000;padding:10px 5px;"> Branch Name:  <span>' . $resStudentMailData[0]['BANK_BRANCH_NAME'] . '</span></td> 
            </tr>
           
            <tr>
                <td colspan="3" style="border:1px #ddd solid; font-size:14px; color:#000000;padding:10px 5px;text-align:center"> <b>Note :</b> This is System Generated Receipt,therefore signature is not required</td>          

            </tr>
            
        </table>
	</body>
</html>';


                            $sendgrid_username = "arunnagpal";
                            $sendgrid_password = "devil1502";

                            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

                            $fromAddress = 'support@lithe.in';
                            $fromName = 'Lithe Technology';
                            $email = new SendGrid\Email();
                            $email->addTo($resStudentMailData[0]['STUD_EMAIL']);

                            // $email->addToName($valueSender);
                            $email->setFrom($fromAddress);

                            $allAttachmentArr = array();

                            //print_r($allAttachmentArr); exit;
                            if (!empty($allAttachmentArr)) {
                                foreach ($allAttachmentArr as $docKey => $docValue) {
                                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                                        $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                                    }
                                }
                            }

                            $subject = "INDE CAMPUS :Student Payment Receipt";
                            $email->setFromName($fromName);
                            $email->setSubject($subject);

                            //$messageBody = "please find attached document for ";
                            $email->setHtml($messageBody);
                            //                       print_r($email); exit;
                            $response = $sendGrid->send($email);
                            $responseMail = $response->getBody();

                            if ($resStudentMailData[0]['STUD_EMAIL'] != "") {
                                $sendgrid_username = "arunnagpal";
                                $sendgrid_password = "devil1502";

                                $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

                                $fromAddress = 'support@lithe.in';
                                $fromName = 'Lithe Technology';
                                $email = new SendGrid\Email();
                                $email->addTo($resStudentMailData[0]['PARENT_EMAIL']);
                                // $email->addToName($valueSender);
                                $email->setFrom($fromAddress);


                                $allAttachmentArr = array();
                                //print_r($allAttachmentArr); exit;
                                if (!empty($allAttachmentArr)) {
                                    foreach ($allAttachmentArr as $docKey => $docValue) {
                                        if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                                            $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                                        }
                                    }
                                }

                                $subject = "INDE CAMPUS :Student Payment Receipt";
                                $email->setFromName($fromName);
                                $email->setSubject($subject);

                                //$messageBody = "please find attached document for ";
                                $email->setHtml($messageBody);
                                //                       print_r($email); exit;
                                $response = $sendGrid->send($email);
                                $responseMail = $response->getBody();
                            }
                        }
                    }

                    /* END : Receipt Sent to the student and as well as Parents */
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['studentId'] = $studentId;
                    $requestData['postData']['invoiceId'] = $invoiceId;

                    $paymentData = listPaymentData($requestData); // GET ALL PAYMENT DATA.
                    $paymentAllData = ($paymentData['status'] == SCS) ? $paymentData['data'] : $paymentData['status'];
                    $result = array("status" => SCS, "data" => $paymentAllData);
                    http_response_code(200);
                } else {
                    $result = array("status" => $msg);
                    http_response_code(400);
                }
            } else { // GIVEN INVOICE ID IS WRONG THROUGH ERROR
                $result = array("status" => WRONGINVOICEIDGIVEN);
                http_response_code(400);
            }
        } else {
            $result = array("status" => AMOUNTISONLYNUMERIC);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

// GET PAYMENT ALL DATA.
function listPaymentData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    $paymentArr = array();
    $wherePaymentCond = $whereStudentCond = "";
    // INVOICE ID CONDITION START
    if (!empty($invoiceId)) {
        $wherePaymentCond = " AND PM.PAYMENT_TYPE ='2' AND PM.FK_INVOICE_ID ='" . $invoiceId . "'"; // CHECK PAYMENT INVOICE CONDITION
    }
    // INVOICE ID CONDITION END
    // STUDENT ID CONDITION START
    if (!empty($studentId)) {
        $whereStudentCond = " AND PM.FK_STUDENT_ID ='" . $studentId . "'"; // CHECK STUDENT ID CONDITION
    }
    // STUDENT ID CONDITION END


    $sqlPaymentData = "SELECT PM.C_PAYMENT_ID,PM.PK_PAYMENT_ID,PM.PAYMENT_TYPE,PM.FK_STUDENT_ID,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME)AS STUD_NAME,PM.FK_INVOICE_ID,PM.PAID_AMOUNT,PM.PAYMENT_DATE,PM.PAYMENT_MODE,PM.PAYMENT_REMARK,PM.BANK_NAME,PM.BRANCH_NAME,PM.PAYMENT_REFERENCE_NO,PM.PAYMENT_REFERENECE_DATE,CL.DISPLAY_VALUE,PM.FK_CLIENT_ID,PM.FK_ORG_ID ,LM.VALUE FROM " . PAYMENTMASTER . " PM INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =PM.FK_STUDENT_ID  INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID = PM.PAYMENT_MODE LEFT JOIN " . LOOKUPMASTER . " LM ON LM.PK_LOOKUP_ID=PM.BANK_NAME WHERE PM.DELETE_FLAG ='0' AND PM.FK_CLIENT_ID='" . $clientId . "' AND PM.FK_ORG_ID='" . $orgId . "' " . $wherePaymentCond . $whereStudentCond . " ORDER BY PM.PK_PAYMENT_ID DESC"; // QUERY FOR PAYMENT DATA

    $resPaymentData = fetch_rec_query($sqlPaymentData); // RESULT FOR PAYMENT DATA
    $paymentArr = Array();
    if (count($resPaymentData) > 0) {
        foreach ($resPaymentData as $keyPayment => $valuePayment) { // PAYMENT DATA FOREACH LOOP.
            $paymentArr[$keyPayment]['clientpaymentId'] = generateId(PAYMENTMASTER, "PAY", $clientId);
            $paymentArr[$keyPayment]['paymentId'] = $valuePayment['PK_PAYMENT_ID'];
            $paymentArr[$keyPayment]['paymentType'] = $valuePayment['PAYMENT_TYPE'];
            $paymentArr[$keyPayment]['studId'] = $valuePayment['FK_STUDENT_ID'];
            $paymentArr[$keyPayment]['studName'] = $valuePayment['STUD_NAME'];
            $paymentArr[$keyPayment]['inoiceId'] = $valuePayment['FK_INVOICE_ID'];
            $paymentArr[$keyPayment]['paidAmount'] = $valuePayment['PAID_AMOUNT'];
            $paymentArr[$keyPayment]['paymentDate'] = $valuePayment['PAYMENT_DATE'];
            $paymentArr[$keyPayment]['paymentMode'] = $valuePayment['PAYMENT_MODE'];
            $paymentArr[$keyPayment]['paymentRemark'] = $valuePayment['PAYMENT_REMARK'];
            $paymentArr[$keyPayment]['bankName'] = $valuePayment['BANK_NAME'];
            $paymentArr[$keyPayment]['branchName'] = $valuePayment['BRANCH_NAME'];
            $paymentArr[$keyPayment]['paymentReferenceNo'] = $valuePayment['PAYMENT_REFERENCE_NO'];
            $paymentArr[$keyPayment]['paymentReferenceDate'] = $valuePayment['PAYMENT_REFERENECE_DATE'];
            $paymentArr[$keyPayment]['paymentModeName'] = $valuePayment['DISPLAY_VALUE'];
            $paymentArr[$keyPayment]['clientId'] = $valuePayment['FK_CLIENT_ID'];
            $paymentArr[$keyPayment]['orgId'] = $valuePayment['FK_ORG_ID'];
        }
        $result = array("status" => SCS, "data" => array_values($paymentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getInvoiceListByStudent($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    $sqlPaymentData = " SELECT IM.PK_INVOICE_ID,IM.C_INVOICE_ID,IM.TOTAL_CHARGES,IF(SUM(PM.PAID_AMOUNT)> 0,SUM(PM.PAID_AMOUNT),0) AS TOTAL_PAID,(IM.TOTAL_CHARGES-IF(SUM(PM.PAID_AMOUNT) >0,SUM(PM.PAID_AMOUNT),0)) AS REMAIN_AMT FROM  " . INVOICEMASTER . " IM LEFT JOIN " . PAYMENTMASTER . " PM ON PM.FK_INVOICE_ID=IM.C_INVOICE_ID  WHERE IM.DELETE_FLAG ='0' AND IM.FK_STUDENT_ID ='" . $studentId . "' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' GROUP BY(IM.C_INVOICE_ID) HAVING REMAIN_AMT > 0"; // QUERY FOR PAYMENT DATA

    $resPaymentData = fetch_rec_query($sqlPaymentData); // RESULT FOR INVOICE DATA
    $invoiceArr = Array();
    if (count($resPaymentData) > 0) {
        foreach ($resPaymentData as $keyPayment => $valuePayment) { // INVOICE DATA FOREACH LOOP.
            $invoiceArr[$keyPayment]['invoiceId'] = $valuePayment['PK_INVOICE_ID'];
            $invoiceArr[$keyPayment]['clientInvoiceId'] = $valuePayment['C_INVOICE_ID'];
            $invoiceArr[$keyPayment]['totalCharges'] = $valuePayment['TOTAL_CHARGES'];
            $invoiceArr[$keyPayment]['remainingAmt'] = $valuePayment['REMAIN_AMT'];
        }
        $result = array("status" => SCS, "data" => array_values($invoiceArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function invoiceRemainingPayment($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";


    $sqlPaymentData = " SELECT IM.PK_INVOICE_ID,IM.C_INVOICE_ID,IM.TOTAL_CHARGES,IF(SUM(PM.PAID_AMOUNT)> 0,SUM(PM.PAID_AMOUNT),0) AS TOTAL_PAID,(IM.TOTAL_CHARGES-IF(SUM(PM.PAID_AMOUNT) >0,SUM(PM.PAID_AMOUNT),0)) AS REMAIN_AMT,IM.TOTAL_CHARGES FROM  " . INVOICEMASTER . " IM LEFT JOIN " . PAYMENTMASTER . " PM ON PM.FK_INVOICE_ID=IM.C_INVOICE_ID  WHERE IM.DELETE_FLAG ='0' AND IM.C_INVOICE_ID ='" . $invoiceId . "' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "'  GROUP BY(PM.FK_INVOICE_ID) HAVING REMAIN_AMT > 0"; // QUERY FOR PAYMENT DATA

    $resPaymentData = fetch_rec_query($sqlPaymentData); // RESULT FOR INVOICE DATA
    $invoiceArr = Array();
    if (count($resPaymentData) > 0) {
        foreach ($resPaymentData as $keyPayment => $valuePayment) { // INVOICE DATA FOREACH LOOP.
            $invoiceArr[$keyPayment]['invoiceId'] = $valuePayment['PK_INVOICE_ID'];
            $invoiceArr[$keyPayment]['clientInvoiceId'] = $valuePayment['C_INVOICE_ID'];
            $invoiceArr[$keyPayment]['remainingAmt'] = $valuePayment['REMAIN_AMT'];
            $invoiceArr[$keyPayment]['totalCharges'] = $valuePayment['TOTAL_CHARGES'];
        }
        $result = array("status" => SCS, "data" => array_values($invoiceArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getclosingbalanceByStudent($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    $sqlClosingBalData = "SELECT UNBILLED_AMT,UNBILLED_TYPE,BILL_AMT,BILL_TYPE,CLOSINGBAL_AMT,CLOSINGBAL_TYPE FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR PAYMENT DATA

    $resClosingBalData = fetch_rec_query($sqlClosingBalData); // RESULT FOR INVOICE DATA
    $closingBal = Array();
    if (count($resClosingBalData) > 0) {
        foreach ($resClosingBalData as $keyClosingbal => $valueClosingbal) { // INVOICE DATA FOREACH LOOP.
            $resClosingBalData[$keyClosingbal]['closingBalAmt'] = $valueClosingbal['CLOSINGBAL_AMT'];
        }
        $result = array("status" => SCS, "data" => array_values($resClosingBalData));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

?>
