<?php 

/***************************************************/
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : DEEPAK PATIL                   */
/*   CREATION DATE : 27/11/2014                    */
/*   FILE TYPE : PHP                               */
/*   FILE NAME : GOOGLE CALENDAR                   */
/***************************************************/

session_start();
error_reporting(E_ALL);

require_once   __DIR__ . '/G-Calendar/vendor/autoload.php';

define('APPLICATION_NAME', 'Google Calendar API PHP - POTG PROJECT');
define('CLIENT_SECRET_PATH', __DIR__ . '/G-Calendar/POTG-CALENDAR-b7499fcb7305.p12');
define('SCOPES', implode(' ', array(Google_Service_Calendar::CALENDAR)));

//function dateToCal($timestamp) {
//    //echo $timestamp;
//  return gmdate("Y-m-d\TH:i:s\Z", $timestamp);
//}
//
//$startTime = $endTime = array();
//$startTime[0]['dateTime'] = dateToCal(strtotime("10-10-2017 12:00 AM")); 
//$startTime[0]['timeZone'] = "Asia/Kolkata"; 
//$endTime[0]['dateTime'] = dateToCal(strtotime("10-10-2017 11:59 PM")); 
//$endTime[0]['timeZone'] = "Asia/Kolkata"; 
////print_r($startTime); exit;
//$summary = "Summary : Gracia Benquate is Booked / Almt Confirm";
//$location = "YMCA - Ahmedabad - 380015";
//$description = "Customer : Deepak patil, Booking Date : 10-10-2017, Event Date : 10-10-2017, Occasion : Marriage";
//$calId = "darshan.patel@lithe.in";
//
//
//$insertEvent = insertEventIntoCalander($summary,$location,$description,$startTime,$endTime,$calId);
//print_r($insertEvent);



//$event = new Google_Event();
//$event->setSummary('Appointment');
//$event->setLocation('Somewhere');
//$start = new Google_EventDateTime();
//$start->dateToCal(strtotime("10-10-2017 12:00 AM"));
//$start->setTimeZone('Asia/Kolkata');
//$event->setStart($start);
//$end = new Google_EventDateTime();
//$end->dateToCal(strtotime("10-10-2017 11:59 PM"));
//$end->setTimeZone('Asia/Kolkata');
//$event->setEnd($end);
//$attendee1 = new Google_EventAttendee();
//$attendee1->setEmail('darshan.patel@lithe.in');
//$event->attendees = array($attendee1);
//
//$calendar->events->insert('primary', $event);

function escapeString($string) {
  return preg_replace('/([\,;])/','\\\$1', $string);
}

// UI CODE FOR CALANDER - WITHOUT AUTH 2 THORUGH SERVICE ACCOUNT

function getClient(){
    
	$client_email = "potg-activity-calendar@potg-calendar.iam.gserviceaccount.com";
	$client_id = "116819520256443182854";
	$private_key = file_get_contents(CLIENT_SECRET_PATH);
	$privateKeyPassword='notasecret'; // the default one when generated in the console
	$scopes = array(SCOPES);
	$credentials = new Google_Auth_AssertionCredentials(
	    $client_email,
	    $scopes,
	    $private_key,
	    $privateKeyPassword                                 // Default P12 password
	);        
        //print_r($credentials); exit;
        //echo "<pre>";
	$client = new Google_Client();
        //print_r($client); exit;
	$client->setAssertionCredentials($credentials);
	$client->setClientId($client_id);
        
	if ($client->getAuth()->isAccessTokenExpired()) {
	  $client->getAuth()->refreshTokenWithAssertion();
	}
//        print_r($client); exit;
	return $client;
}

function insertEventIntoCalander($summary,$location,$description,$startTime,$endTime,$userId,$clientId=""){
   
    $userDetail = getUserDetail($clientId, $userId);
//    $userDetail['status'] = "SCS";
//    print_r($userDetail);exit;
    if($userDetail['status'] == "SCS" && (!empty($userDetail['data'][0]['USER_G_CAL_ID']) || $userDetail['data'][0]['USER_G_CAL_ID'] != "" )){
        
        $client = getClient();
		//print_r($client); exit;
        $service = new Google_Service_Calendar($client);
        
//        print_r($service);exit;
        //$calendarId = ;
        
        //$calendarId = $userId;
        $calendarId = "primary";
        
        $dateAndTimeStart = escapeString($startTime[0]['dateTime']);
        $dateAndTimeEnd = escapeString($endTime[0]['dateTime']);
        $timeZoneEnd = escapeString($endTime[0]['timeZone']);
        $timeZoneStart = escapeString($startTime[0]['timeZone']);
        $event = new Google_Service_Calendar_Event(array(
          'summary' => escapeString($summary),
          'location' => escapeString($location),
          'description' => escapeString($description),
          'start' => array(
            'dateTime' => $dateAndTimeStart,
            'timeZone' => escapeString($timeZoneStart),
          ),
          'end' => array(
            'dateTime' => $dateAndTimeEnd,
            'timeZone' => escapeString($timeZoneEnd),
          ),
           'attendees' => array(
            array('email' => $userDetail['data'][0]['USER_G_CAL_ID']),
          )
        ));
//        echo "<pre>";
//        print_r($event);exit;
        try { 
//            echo "hiiii";exit;
            $insertevent = $service->events->insert($calendarId, $event);
//            print_r($insertevent);exit;
            if($insertevent){
                $result = array("status"=>'SCS');
            }else{
                $result = array("status"=>INSERTFAIL." google calender.");
            }
            
        }catch (Exception $ex){
            
            $result = array("status"=>$ex->getMessage());
        }
    }else{
        $result = array("status"=>'SCS');
    }
   
    // return data to insert cal id into event inq table
    return $result;
}


function updateEventIntoCalander($calId,$eventId,$summary){
	$client = getClient();
	$service = new Google_Service_Calendar($client);
	$event = $service->events->get($calId, $eventId);
	//print_r($event);exit;
	$event->setSummary(escapeString($summary));
	//echo $event->getId(); exit;
	$updatedEvent = $service->events->update($calId, $eventId, $event);
	//echo "<pre>";
	return $updatedEvent;
}


function deleteEventIntoCalander($calId,$eventId){
	$client = getClient();
	$service = new Google_Service_Calendar($client);
	$deleteEvent  = $service->events->delete($calId, $eventId);

	return $deleteEvent;

}
?>