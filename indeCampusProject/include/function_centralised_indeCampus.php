<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : DEVIKA PATEL                     */
/*   CREATION DATE : 08-AUG-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */

/* ============= CENTARLISED INVOICEING ====================== */

//GET CENTRALIZE INVOICE CODE START

function getCentraliseInvoices($postData) {
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $startDate = isset($postData['postData']['startDate']) ? $postData['postData']['startDate'] : "";
    $endDate = isset($postData['postData']['endDate']) ? $postData['postData']['endDate'] : "";
    $data = array();

    /* === PROCESS START === */
    //====EPOSE SALES ======
    // 1. CREATE SALES IN EPOSE SIDE.
    // 2. GET THE DATA FROM CENTRALISED EPOS SALES
    // 3. GET EPOSE SALES ITEM FROM  CENTRALISED EPOS ITEMS
    // 4. GET EPOSE SALES TAX FROM CENTRALISED EPOSE SALES TAX.
    /* === PROCESS END === */

    //GET EPOS SALES DATA

    $eposType = "";
    $sqlEposSales = fetch_rec_query("SELECT PK_SALES_ID,FK_SPACE_ID,LOCATION,BILL_NO,BILL_DATE,MEMBERSHIP_NUMBER,STUDENT_NAME,BASIC_AMOUNT,DISCOUNT_AMOUNT,DISCOUNT_TYPE,ROUND_OFF,FINAL_AMOUNT,NARRATION FROM " . CENTRALISEEPOSSALES . " WHERE DELETE_FLAG=0 AND BILL_DATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_SALES_ID DESC ");
//echo '<pre>';print_r($sqlEposSales);die;


    $sqlclientData = "SELECT FNB_SPACE_ID,FNB_LAUNDRY_ID,PK_CLIENT_ID FROM " . CLIENTMASTER . " WHERE PK_CLIENT_ID ='" . $clientId . "'";
    $resClientData = fetch_rec_query($sqlclientData);
    $fnbSpaceId = $resClientData[0]['FNB_SPACE_ID'];
    $laundryId = $resClientData[0]['FNB_LAUNDRY_ID'];


    if (count($sqlEposSales) > 0) {

        foreach ($sqlEposSales as $keySales => $valueSales) {

            if ($valueSales['FK_SPACE_ID'] == $fnbSpaceId) {//Laundry : 101 FNB : 100
                $eposType = 'FNB';
            } else if ($valueSales['FK_SPACE_ID'] == $laundryId) {//Laundry : 101 FNB : 100
                $eposType = 'Laundry';
            }

            $data[$eposType]['eposSalesInvoice'][$keySales] = $valueSales;

            if (!empty($data[$eposType]['eposSalesInvoice'])) {
                $data[$eposType]['eposSalesInvoice'][$keySales]['VOUCHER_TYPE'] = 'SALES';
            }

            $salesId = $valueSales['PK_SALES_ID'];
            $billNo = $valueSales['BILL_NO'];

            //SALES ITEMS
            $sqlEposItems = fetch_rec_query("SELECT FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,ITEM_AMOUNT FROM " . CENTRALISEEPOSSALESITEMS . " WHERE FK_SALES_ID='" . $salesId . "' ");
            if (count($sqlEposItems) > 0) {
                $data[$eposType]['eposSalesInvoice'][$keySales]['salesItems'] = $sqlEposItems;
            }

            //$salesItemId = $sqlEposItems[$i]['FK_ITEM_ID'];
            //SALES TAX
            $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,FK_ITEM_ID,TAX_NAME,TAX_PERCENTAGE,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSSALESTAX . " WHERE FK_SALES_ID='" . $salesId . "' AND BILL_NO='" . $billNo . "' GROUP BY TAX_NAME, TAX_PERCENTAGE ");
            if (count($sqlEposTax) > 0) {
                $data[$eposType]['eposSalesInvoice'][$keySales]['salesTax'] = $sqlEposTax;
            } else {
                $data[$eposType]['eposSalesInvoice'][$keySales]['salesTax'] = 0;
            }
            //$d++;
        }
    } else {
//        $result = array("status" => NORECORDS);
//        http_response_code(400);
    }
    //GET EPOS SALES DATA

    /* === PROCESS START === */
    //====EPOSE CREDIT NOTE ======
    // 1. CREATE CREDIT NOTE IN EPOSE SIDE.
    // 2. GET THE DATA FROM CENTRALISED EPOS CREDIT NOTE
    // 3. GET EPOSE SALES ITEM FROM  CENTRALISED EPOS CREDIT NOTE ITEMS//Pizza,cake etc
    // 4. GET EPOSE SALES TAX FROM CENTRALISED EPOSE CREDIT NOTE TAX.//cgst,sgst
    /* === PROCESS END === */

    $eposType1 = "";
    //GET EPOS CREDIT NOTE DATA
    $sqlEposCrNote = fetch_rec_query("SELECT PK_CREDIT_NOTE_ID,FK_SPACE_ID,LOCATION,BILL_NO,BILL_DATE,MEMBERSHIP_NUMBER,STUDENT_NAME,BASIC_AMOUNT,TAX_AMOUNT,DISCOUNT_AMOUNT,DISCOUNT_TYPE,ROUND_OFF,FINAL_AMOUNT,NARRATION,REFERENCE_NO,BILL_NO FROM " . CENTRALISEEPOSCREDITNOTE . " WHERE DELETE_FLAG=0 AND BILL_DATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_CREDIT_NOTE_ID DESC ");

    if (count($sqlEposCrNote) > 0) {
        //$c = 0;
        foreach ($sqlEposCrNote as $keyCrNote => $valueCrNote) {

            if ($valueCrNote['FK_SPACE_ID'] == $fnbSpaceId) {//Laundry : 101 FNB : 100
                $eposType1 = 'FNB';
            } else if ($valueCrNote['FK_SPACE_ID'] == $laundryId) {//Laundry : 101 FNB : 100
                $eposType1 = 'Laundry';
            }

            $data[$eposType1]['eposCrNoteInvoice'][$keyCrNote] = $valueCrNote;

            if (!empty($data[$eposType1]['eposCrNoteInvoice'])) {
                $data[$eposType1]['eposCrNoteInvoice'][$keyCrNote]['VOUCHER_TYPE'] = 'CREDIT NOTE';
            }


            $crNoteId = $sqlEposCrNote[$keyCrNote]['PK_CREDIT_NOTE_ID'];
            $billNo = $sqlEposCrNote[$keyCrNote]['BILL_NO'];

            //SALES ITEMS
            $sqlEposItems = fetch_rec_query("SELECT PK_CREDIT_NOTE_ITEM_ID,FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,ITEM_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTEITEMS . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' ");
            if (count($sqlEposItems) > 0) {
                $data[$eposType1]['eposCrNoteInvoice'][$keyCrNote]['crNoteItems'] = $sqlEposItems;
            }
//           
            $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,FK_ITEM_ID,TAX_NAME,TAX_PERCENTAGE,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTETAX . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' AND BILL_NO='" . $billNo . "' GROUP BY TAX_NAME, TAX_PERCENTAGE ");
            if (count($sqlEposTax) > 0) {
                $data[$eposType1]['eposCrNoteInvoice'][$keyCrNote]['crNoteTax'] = $sqlEposTax;
            } else {
                $data[$eposType1]['eposCrNoteInvoice'][$keyCrNote]['crNoteTax'] = 0;
            }
        }
    } else {
//        $result = array("status" => NORECORDS);
//        http_response_code(400);
    }
    //GET EPOS CREDIT NOTE DATA

    /* === PROCESS START === */
    //====EPOSE RECIPT NOTE ======
    // 1. CREATE RECIPT NOTE IN EPOSE SIDE.
    // 2. GET THE DATA FROM CENTRALISED EPOS RECIPT
    /* === PROCESS END === */

    //GET EPOS RECEIPT DATA
    $eposType2 = '';


    $sqlEposReceipt = fetch_rec_query("SELECT PK_RECEIPT_ID,FK_SPACE_ID,BILL_NO,BILL_DATE,STUDENT_NAME,MEMBERSHIP_NUMBER,PAYMENT_MODE,IF(PK_RECEIPT_ID != '','Receipt','') AS VOUCHER_TYPE,PAYMENT_AMOUNT,NARRATION,PARTY_ADDRESS,CHEQUE_NUMBER,REFERENCE_NO FROM " . CENTRALISEEPOSRECEIPT . " WHERE DELETE_FLAG=0 AND BILL_DATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_RECEIPT_ID DESC ");
    if (count($sqlEposReceipt) > 0) {

//COMMENT BY DEVIKA.23.8.2018
//        if ($sqlEposReceipt[0]['FK_SPACE_ID'] == '1') {
//            $eposType2 = 'FNB';
//        } else if ($sqlEposReceipt[0]['FK_SPACE_ID'] == '2') {
//            $eposType2 = 'Laundry';
//        }
//	$data[$eposType2]['eposReceiptInvoice'] = $sqlEposReceipt;
        //CHANGE BY DEVIKA.23.8.2018.
        //$a = 0;
        foreach ($sqlEposReceipt as $keyEposRecipt => $valueEposRecipt) {

            if ($valueEposRecipt['FK_SPACE_ID'] == $fnbSpaceId) {//Laundry : 101 FNB : 100
                $eposType2 = 'FNB';
            } else if ($valueEposRecipt['FK_SPACE_ID'] == $laundryId) {//Laundry : 101 FNB : 100
                $eposType2 = 'Laundry';
            }

            $data[$eposType2]['eposReceiptInvoice'][$keyEposRecipt] = $valueEposRecipt;

            if (!empty($data[$eposType2]['eposReceiptInvoice'])) {
                $data[$eposType2]['eposReceiptInvoice'][$keyEposRecipt]['VOUCHER_TYPE'] = 'Receipt';
            }
        }
    }

    if (!isset($data['FNB'])) {
        $data['FNB'] = array();
    }

    if (!isset($data['Laundry'])) {
        $data['Laundry'] = array();
    }

    //GET EPOS RECEIPT DATA

    /* === PROCESS START === */
    //====GENERAL RECIPT NOTE ======
    // 1. CREATE RECIPT IN INDECAMPSU
    // 2. GET THE DATA FROM CENTRALISED CUSTOME RECIPT
    /* === PROCESS END === */

    //GET CUSTOM RECEIPT DATA
    $sqlCustomReceipt = fetch_rec_query("SELECT PK_RECEIPT_ID,INVOICE_ID,MEMBERSHIP_NUMBER,STUDENT_NAME,NARRATION,RECEIPT_DATE,PAYMENT_MODE,BANK_NAME,CHEQUE_NO,PAYMENT_AMOUNT,NARRATION FROM " . CENTRALISECUSTOMRECEIPT . " WHERE DELETE_FLAG=0 AND FK_CLIENT_ID = '" . $clientId . "' AND RECEIPT_DATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' ORDER BY RECEIPT_DATE,PK_RECEIPT_ID DESC ");
    $data['receipt'] = $sqlCustomReceipt;


    //GET CUSTOM RECEIPT DATA

    /* === PROCESS START === */
    //====GENERAL INVOICE NOTE ======
    // 1. CREATE INVOICE IN INDECAMPSU
    // 2. GET THE DATA FROM CENTRALISED CUSTOME INVOICE
    /* === PROCESS END === */

    //GET GENERAL INVOICE DATA

    $sqlCustomInvoice = fetch_rec_query("SELECT IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.INVOICE_NAME,IM.PERIOD_FROM,IM.PERIOD_TO,IM.FK_STUDENT_ID,SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,CONCAT_WS(' ',SM.FIRST_NAME,SM.LAST_NAME)AS STUDENTNAME,CONCAT_WS(' ',SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3) ADDRESS,SM.PINCODE,SM.FK_CITY_ID,SM.FK_STATE_ID,SM.FK_COUNTRY_ID,CT.CITY_NAME,CT.STATE_NAME,CO.COUNTRY_NAME FROM " . INVOICEMASTER . " IM LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID=IM.FK_STUDENT_ID LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = SM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=SM.FK_CITY_ID  WHERE IM.INVOICE_DATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND IM.DELETE_FLAG=0 AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' AND IM.INVOICE_TYPE='GENERAL'");
    $data['generalInvoice'] = $sqlCustomInvoice;


    if (count($sqlCustomInvoice) > 0) {

        foreach ($sqlCustomInvoice as $keyCustomeInvoice => $valueCustomeInvoice) {
            $invoiceId = $valueCustomeInvoice['C_INVOICE_ID'];

            //INVOICE ITEMS
//            $sqlInvoiceItems = fetch_rec_query("SELECT ITEM_NAME,ITEM_TOTAL_PRICE FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $invoiceId . "' ");
            $sqlInvoiceItems = fetch_rec_query("SELECT ITEM_NAME,TAX_CGST,TAX_SGST,TAX_IGST,HSN_CODE,QTY_OF_SESSION,ITEM_BASE_PRICE,ITEM_TOTAL_PRICE,TOTAL_TAX,INVOICE_ID FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $invoiceId . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1'");
            if (count($sqlInvoiceItems) > 0) {
                $data['generalInvoice'][$keyCustomeInvoice]['invoiceItems'] = $sqlInvoiceItems;
            } else {
                $data['generalInvoice'][$keyCustomeInvoice]['invoiceItems'] = NORECORDS;
            }
        }
    }
    //GET CUSTOM INVOICE DATA

    /* === PROCESS START === */
    //====INDECAMPUS INVOICE NOTE ======
    // 1. CREATE INVOICE IN INDECAMPSU 
    // 2. GET THE DATA FROM CENTRALISED CUSTOME INVOICE
    /* === PROCESS END === */

    //GET INVOICE DATA.
    $sqlIndInvoice = fetch_rec_query("SELECT CONCAT_WS(' ',SM.FIRST_NAME,SM.LAST_NAME) STUDENTNAME,CONCAT_WS(' ',SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3) ADDRESS,SM.MEMBERSHIP_NUMBER,IM.TOTAL_CHARGES,IM.INVOICE_NAME,IM.INVOICE_DATE,IM.C_INVOICE_ID,IM.PK_INVOICE_ID FROM " . INVOICEMASTER . " IM INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID = IM.FK_STUDENT_ID AND SM.DELETE_FLAG = '0' WHERE  IM.INVOICE_DATE BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND IM.DELETE_FLAG = '0' AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.INVOICE_TYPE !='GENERAL' ORDER BY C_INVOICE_ID DESC ");
    $data['invoice'] = $sqlIndInvoice;
    // $data['invoice']['VOUCHER_TYPE'] = 'Invoice';
    //GET INVOICE DATA.
    //print_r($data);die;
    $result = array("status" => SCS, "data" => $data);
    http_response_code(200);

    return $result;
}

//GET CENTRALIZE INVOICE CODE END

function createReceipt($postData) {
    //print_r($postData);die;
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $time = time();

    $studentId = isset($postData['postData']['studentId']) ? $postData['postData']['studentId'] : "";
    $invoiceId = isset($postData['postData']['invoiceList']) ? $postData['postData']['invoiceList'] : "";
    $invoiceType = isset($postData['postData']['invoiceType']) ? $postData['postData']['invoiceType'] : "";
    $receiptDate = isset($postData['postData']['receiptDate']) ? $postData['postData']['receiptDate'] : "";
    $paymentMethodId = isset($postData['postData']['paymentMethodId']) ? $postData['postData']['paymentMethodId'] : "";
    $paymentMethod = isset($postData['postData']['paymentMethod']) ? $postData['postData']['paymentMethod'] : "";
    $bankName = isset($postData['postData']['bankName']) ? $postData['postData']['bankName'] : "";
    $chequeNo = isset($postData['postData']['chequeNo']) ? $postData['postData']['chequeNo'] : "";
    $bankBranchName = isset($postData['postData']['bankBranchName']) ? $postData['postData']['bankBranchName'] : "";
    $suspenseAccountflag = isset($postData['postData']['suspenseAccountflag']) ? $postData['postData']['suspenseAccountflag'] : "";
    $payAmount = isset($postData['postData']['payAmount']) ? $postData['postData']['payAmount'] : ""; //pay amount
    $pendingAmount = isset($postData['postData']['pendingAmount']) ? $postData['postData']['pendingAmount'] : ""; //pay amount
    $totalpaymentAmount = isset($postData['postData']['amount']) ? $postData['postData']['amount'] : ""; //final invoice amount
    $narration = isset($postData['postData']['narration']) ? $postData['postData']['narration'] : "";
    $lastFourDigitCardNo = isset($postData['postData']['lastFourDigitCardNo']) ? $postData['postData']['lastFourDigitCardNo'] : "";

    //=========PROCESS============//
    /* 1. STUDENT SEARCH STUDENT WISE AND
     * 2. INVOICE TYPE 1 AGAINST INVOICE -LIST INVOICE ID.(getInvoiceListByStudent).
     * 3. INVOICE TYPE 2 AGAINST ACCOUNT (getclosingbalanceByStudent).
     * 4. INVOICE SELECT AGINST INVOICE AND SELECT INVOICE THAN DISPLAY AMOUNT.(READ ONLY).
     * 4. FILL UP ALL DETAILS AND ENTERD AMOUNT.
     * 5. 1 ENTERY IN CENTRALISED_CUSTOME_RECIPT.
     * 6. 2 ENTRY IN PAYMENT MASTER
     * 7. IF SELECT  INVOICE TYPE 2.AGIANST ACCOUNT DISPLAY CLOSING BAL AMOUNT.
     * 8. RECIPT AMOUNT UPDATE  IN RECIPT_AMOUNT PLUS AND RECIPTTYPE. STUDENTID WAIE.
     * 9. AND STORED IN CENTRELISED_CUSTOME_RECIPT MANAGE INVOICE ID.//AGAINST INVOICE ID=INVOICEiD  AND AGAINSTACCOUNT=-1;
     * 10. AND STORED IN PAYMENT_MASTER. MANAGE INVOICE ID//AGAINST INVOICE ID=INVOICEiD  AND AGAINSTACCOUNT=-1;
     */
    //========END PROCESS========//

    $sqlGetStudent = fetch_rec_query("SELECT MEMBERSHIP_NUMBER,FIRST_NAME,LAST_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "' AND DELETE_FLAG=0 LIMIT 0,1 ");

    if (count($sqlGetStudent) > 0) {

        $pkReceiptId = generateId(CENTRALISECUSTOMRECEIPT, 'REC', $clientId);

        //AGAINST INVOICE AND AGAINST ACOOUNT IN INSERT CENTRALISED_CUSTOME_RECIPT CODE.

        $insertReceipt['PK_RECEIPT_ID'] = $pkReceiptId;
        $insertReceipt['INVOICE_ID'] = $invoiceId; //AGAINST INVOICE ID=INVOICEiD  AND AGAINSTACCOUNT=-1;
        $insertReceipt['INVOICE_TYPE'] = $invoiceType; //1=AGAINST INVOICE, 2=AGAINST ACOOUNT
        $insertReceipt['VOUCHER_TYPE'] = 'Receipt';
        $insertReceipt['FK_CLIENT_ID'] = $clientId;
        $insertReceipt['FK_ORG_ID'] = $orgId;
        $insertReceipt['FK_STUD_ID'] = $studentId;
        $insertReceipt['MEMBERSHIP_NUMBER'] = $sqlGetStudent[0]['MEMBERSHIP_NUMBER'];
        $insertReceipt['STUDENT_NAME'] = $sqlGetStudent[0]['FIRST_NAME'] . " " . $sqlGetStudent[0]['LAST_NAME'];
        $insertReceipt['RECEIPT_DATE'] = date('Y-m-d', strtotime($receiptDate));
        $insertReceipt['PAYMENT_MODE_ID'] = $paymentMethodId;
        $insertReceipt['PAYMENT_MODE'] = $paymentMethod;
        $insertReceipt['BANK_NAME'] = $bankName;
        $insertReceipt['BANK_BRANCH_NAME'] = $bankBranchName;
        $insertReceipt['CHEQUE_NO'] = $chequeNo;
        $insertReceipt['PAYMENT_AMOUNT'] = $payAmount;
        $insertReceipt['TOTAL_PAYMENTAMOUNT'] = $totalpaymentAmount; //READ ONLY.
        $insertReceipt['PENDING_AMOUNT'] = $pendingAmount;
        $insertReceipt['NARRATION'] = $pkReceiptId." - ". $narration;
        $insertReceipt['SUSPENSE_ACCOUNT'] = $suspenseAccountflag;
        $insertReceipt['LAST_FOUR_DIGIT'] = $lastFourDigitCardNo;

        $insertReceipt['CREATED_BY'] = $userId;
        $insertReceipt['CREATED_BY_DATE'] = $time;
        $insertReceipt['DELETE_FLAG'] = 0;
        //print_r($insertReceipt); die;
        $requestReceipt = insert_rec(CENTRALISECUSTOMRECEIPT, $insertReceipt);

        if ($requestReceipt) {

            $insertArr = array();
            $insertArr['C_PAYMENT_ID'] = $pkReceiptId;
            $insertArr['PAYMENT_TYPE'] = $invoiceType; //INVOICE TYPE=AGAINST INVOICE
            $insertArr['FK_CLIENT_ID'] = $clientId;
            $insertArr['FK_ORG_ID'] = $orgId;
            $insertArr['FK_STUDENT_ID'] = $studentId;
            $insertArr['FK_INVOICE_ID'] = $invoiceId; //AGAINST INVOICE ID=INVOICEiD  AND AGAINSTACCOUNT=-1;
            $insertArr['PAYMENT_MODE'] = $paymentMethod;
            $insertArr['BANK_NAME'] = $bankName;
            $insertArr['BRANCH_NAME'] = $bankBranchName;
            $insertArr['PAYMENT_MODE'] = $paymentMethodId;
            $insertArr['PAYMENT_REFERENECE_DATE'] = date('Y-m-d', strtotime($receiptDate));
            $insertArr['PAYMENT_DATE'] = date('Y-m-d', strtotime($receiptDate));
            $insertArr['PAID_AMOUNT'] = $payAmount;
            $insertArr['PAYMENT_REMARK'] = $narration;
            $insertArr['LAST_FOUR_DIGIT'] = $lastFourDigitCardNo;
            $insertArr['CHANGED_BY'] = $userId;
            $insertArr['CHANGED_BY_DATE'] = $time;
            //print_r($insertArr); die;
            $updateBookingTrans = insert_rec(PAYMENTMASTER, $insertArr);

            //AGAINST ACCOUNT CODE.
            // if ($invoiceType == 1) {

            /*   $studentGetData = fetch_rec_query("SELECT RECEIPT_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID = '" . $studentId . "'");
              $recAmt = $studentGetData[0]['RECEIPT_AMT'];
              $updateArr = array();
              $updateArr['RECEIPT_AMT'] = $recAmt + $payAmount;
              $updateArr['RECEIPT_TYPE'] = 'CR';
              $updateArr['CLOSINGBAL_AMT'] = $pendingAmount;


             */
            $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT,CLOSINGBAL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $studentId . "' AND DELETE_FLAG ='0'";
            $resStudentData = fetch_rec_query($sqlStudentData); // 
            if (count($resStudentData) > 0) {
                $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                $billedAmt = $resStudentData[0]['BILL_AMT'];
                $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
            }


            $closingBalAmt = floatval($billedAmt) - (floatval($receiptAmt) + floatval($payAmount));
            $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR";

            $updateStudentArr = ARRAY();
            $updateStudentArr['RECEIPT_AMT'] = floatval($payAmount) + floatval($receiptAmt);
            $updateStudentArr['RECEIPT_TYPE'] = 'CR';
            $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
            $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;



            $whereUpdateColsingbal = "PK_STUD_ID='" . $studentId . "' AND DELETE_FLAG='0'";
            $updateBookingTrans = update_rec(STUDENTMASTER, $updateStudentArr, $whereUpdateColsingbal);
            
            
              $sqlStudentMailData = "SELECT CR.PK_RECEIPT_ID,CR.RECEIPT_DATE,CR.PAYMENT_AMOUNT,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.TOTAL_CHARGES,CR.STUDENT_NAME,CR.PAYMENT_MODE,CR.BANK_BRANCH_NAME,SM.EMAIL AS STUD_EMAIL,PM.EMAIL AS PARENT_EMAIL,LM.VALUE AS BANK_NAME,CR.CHEQUE_NO,CR.FK_STUD_ID FROM " . CENTRALISECUSTOMRECEIPT . " CR INNER JOIN " . INVOICEMASTER . " IM ON CR.INVOICE_ID =IM.C_INVOICE_ID LEFT JOIN " . LOOKUPMASTER . " LM ON LM.PK_LOOKUP_ID=CR.BANK_NAME INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =CR.FK_STUD_ID INNER JOIN " . PARENTMASTER . " PM ON  PM.FK_STUD_ID = CR.FK_STUD_ID AND PM.USER_NAME !='' WHERE PK_RECEIPT_ID='" . $pkReceiptId . "' AND CR.DELETE_FLAG ='0' AND CR.FK_CLIENT_ID ='" . $clientId . "' AND CR.FK_ORG_ID ='" . $orgId . "'";
           
           
                        $resStudentMailData = fetch_rec_query($sqlStudentMailData);
                        if (count($resStudentMailData) > 0 && !empty($resStudentMailData[0]['STUD_EMAIL'])) {



                            $paymentDate = $instrumentDate = $roomName = "";
                            if ($receiptDate != "" && $receiptDate != "0000-00-00") {
                                $instrumentDate = date("d-m-Y", strtotime($receiptDate));
                            }
                            if (isset($resStudentMailData[0]['RECEIPT_DATE']) && $resStudentMailData[0]['RECEIPT_DATE'] != "0000-00-00") {
                                $paymentDate = date("d-m-Y", strtotime($resStudentMailData[0]['RECEIPT_DATE']));
                            }

                            $sqlStudentRoomData = "SELECT ROOM_NAME FROM " . ALLOTMASTER . " WHERE FK_STUDENT_ID ='" . $resStudentMailData[0]['FK_STUD_ID'] . "' AND FK_CLIENT_ID ='" . $clientId . "' AND DELETE_FLAG ='0' ORDER BY PK_ALLOTMENT_ID DESC LIMIT 0,1";
                            $resStudentRoomData = fetch_rec_query($sqlStudentRoomData);
                            if (count($resStudentRoomData) > 0) {
                                $roomName = $resStudentRoomData[0]['ROOM_NAME'];
                            }

                            $messageBody = '<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Receipt</title>

	</head>
    <style>
        html { font: 16px/1 Open Sans, sans-serif; overflow: auto; }
        article:after { clear: both; content: ""; display: table; }
        header:after { clear: both; content: ""; display: table; }
    </style>
    <div style="height:0.1in;padding:0 0.4in 0 0.55in;background-color:#ffffff;color:#333333;font-size:9pt"></div>
	<body style=" background: #FFF; border-radius: 1px; box-sizing: border-box; margin: 0 auto; overflow: hidden; padding: 0px 0.4in 0px 0.4in; ">  
        <table style="border-collapse: collapse;width: 100%;">
           <tr>
                <td valign="top" style="font-size:12px; color:#000000;padding:10px 5px;width:29%;">                   
<!--               <img src="#" alt="" style="height:86px; width:auto;">-->
                </td>
                <td valign="top" style="font-size:12px; color:#000000;padding:10px 5px;width:64%;">                   
                     
                    <address style="vertical-align:top; padding-top:10px; text-align:center; font-size:12px; line-height:14px;font-style:normal;display:inline-block;">
                        <span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020
                    </address>
                </td>
            </tr> 
        
        </table>
        <table class="balance" style="border-collapse: collapse; border-spacing: 0px; font-size: 75%; table-layout: fixed; width: 100%;margin: 0 0 1em;">
                      
            <tr>
                <td colspan="2" style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Receipt Number : <span>' . $resStudentMailData[0]['PK_RECEIPT_ID'] . '</span></td>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Receipt Date : <span>Date: ' . $paymentDate . '</span></td>                
            </tr>
            <tr>
                <td colspan="3" style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Receive With Thanks From :  <span>' . $resStudentMailData[0]['STUDENT_NAME'] . ' </span></td>
            </tr>
            <tr>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Room Number :  <span>' . $roomName . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Rs. :<span>' . $resStudentMailData[0]['PAYMENT_AMOUNT'] . '</span></td>                
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Rs. In Word :  <span>' . convertNumber($resStudentMailData[0]['PAYMENT_AMOUNT']) . '	</span></td> 
            </tr>
            <tr>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Invoice Number :  <span>' . $resStudentMailData[0]['C_INVOICE_ID'] . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Invoice Date :<span>' . $paymentDate . '</span></td> 
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Invoice Amount :  <span>' . $resStudentMailData[0]['TOTAL_CHARGES'] . '</span></td>   
            </tr>
                
                      <tr>                   
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Mode Of Payment :<span>' . $resStudentMailData[0]['PAYMENT_MODE'] . '</span></td>                
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Instrument Number:  <span>' . $resStudentMailData[0]['CHEQUE_NO'] . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Instrument Date :<span>' . $instrumentDate . '</span></td>
            </tr>
            <tr>
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;"> Instrument Amount :  <span>' . $resStudentMailData[0]['PAYMENT_AMOUNT'] . '</span></td>      
                <td style="border:1px #ddd solid; border-bottom:none; font-size:14px; color:#000000;padding:10px 5px;">Bank Name :<span>' . $resStudentMailData[0]['BANK_NAME'] . '</span></td>                
                <td style="border:1px #ddd solid; font-size:14px; color:#000000;padding:10px 5px;"> Branch Name:  <span>' . $resStudentMailData[0]['BANK_BRANCH_NAME'] . '</span></td> 
            </tr>
           
            <tr>
                <td colspan="3" style="border:1px #ddd solid; font-size:14px; color:#000000;padding:10px 5px;text-align:center"> <b>Note :</b> This is System Generated Receipt,therefore signature is not required</td>          

            </tr>
            
        </table>
	</body>
</html>';


                            $sendgrid_username = "arunnagpal";
                            $sendgrid_password = "devil1502";

                            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

                            $fromAddress = 'support@lithe.in';
                            $fromName = 'Lithe Technology';
                            $email = new SendGrid\Email();
                            $email->addTo($resStudentMailData[0]['STUD_EMAIL']);

                            // $email->addToName($valueSender);
                            $email->setFrom($fromAddress);

                            $allAttachmentArr = array();

                            //print_r($allAttachmentArr); exit;
                            if (!empty($allAttachmentArr)) {
                                foreach ($allAttachmentArr as $docKey => $docValue) {
                                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                                        $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                                    }
                                }
                            }

                            $subject = "INDE CAMPUS :Student Payment Receipt";
                            $email->setFromName($fromName);
                            $email->setSubject($subject);

                            //$messageBody = "please find attached document for ";
                            $email->setHtml($messageBody);
                            //                       print_r($email); exit;
                            $response = $sendGrid->send($email);
                            $responseMail = $response->getBody();

                            if ($resStudentMailData[0]['STUD_EMAIL'] != "") {
                                $sendgrid_username = "arunnagpal";
                                $sendgrid_password = "devil1502";

                                $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

                                $fromAddress = 'support@lithe.in';
                                $fromName = 'Lithe Technology';
                                $email = new SendGrid\Email();
                                $email->addTo($resStudentMailData[0]['PARENT_EMAIL']);
                                // $email->addToName($valueSender);
                                $email->setFrom($fromAddress);


                                $allAttachmentArr = array();
                                //print_r($allAttachmentArr); exit;
                                if (!empty($allAttachmentArr)) {
                                    foreach ($allAttachmentArr as $docKey => $docValue) {
                                        if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                                            $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                                        }
                                    }
                                }

                                $subject = "INDE CAMPUS :Student Payment Receipt";
                                $email->setFromName($fromName);
                                $email->setSubject($subject);

                                //$messageBody = "please find attached document for ";
                                $email->setHtml($messageBody);
                                //                       print_r($email); exit;
                                $response = $sendGrid->send($email);
                                $responseMail = $response->getBody();
                            }
                        }
            //$result = array("status" => SCS);
            //  }
            $data = array();
            $getStudentData = fetch_rec_query("SELECT PK_STUD_ID,MEMBERSHIP_NUMBER,FIRST_NAME,LAST_NAME,ADDRESS_LINE1,ADDRESS_LINE2,ADDRESS_LINE3 FROM " . STUDENTMASTER . " WHERE DELETE_FLAG=0 AND PK_STUD_ID='" . $studentId . "' AND MEMBERSHIP_NUMBER!='' AND IS_ACTIVE='1' ");
            //print_r($getStudentData); die;
            if (count($getStudentData) > 0) {
                $data['receiptNo'] = $pkReceiptId;
                $data['receiptDate'] = date('d-m-Y', strtotime($receiptDate));
                $data['studentId'] = $studentId;
                $data['membershipNo'] = $getStudentData[0]['MEMBERSHIP_NUMBER'];
                $data['studentName'] = $getStudentData[0]['FIRST_NAME'] . ' ' . $getStudentData[0]['LAST_NAME'];
                $data['studentAddress'] = stripslashes($getStudentData[0]['ADDRESS_LINE1'] . " " . $getStudentData[0]['ADDRESS_LINE2'] . " " . $getStudentData[0]['ADDRESS_LINE3']);
                $data['receiptNarration'] = $narration;
                $data['receiptAmount'] = $payAmount;
                //print_r($data); die;
                $result = array("status" => SCS, "data" => $data);
            } else {
                $result = array("status" => "Student Member code not valid");
            }
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => FAILMSG);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function createGeneralInvoice($postData) {
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $time = time();

    $studentId = isset($postData['postData']['studentId']) ? $postData['postData']['studentId'] : "";
    $narration = isset($postData['postData']['narration']) ? $postData['postData']['narration'] : "";
    $invDate = isset($postData['postData']['invDate']) ? $postData['postData']['invDate'] : "";
    $itemArr = $postData['postData']['itemData'];



    $sqlGetStudent = fetch_rec_query("SELECT SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.FK_CITY_ID,SM.FK_STATE_ID,SM.FK_COUNTRY_ID,CT.CITY_NAME,CT.STATE_NAME,CO.COUNTRY_NAME FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = SM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=SM.FK_CITY_ID WHERE PK_STUD_ID='" . $studentId . "' AND DELETE_FLAG=0 
LIMIT 0,1 ");
//echo "abc"; die;
    if (count($sqlGetStudent) > 0) {


        $pkInvoiceId = generateId(CENTRALISECUSTOMINVOICE, 'GLINV', $clientId);

        $tempItemIdArr = array();
        //CENTRALISED CUSTOME ITEM MASTER.
        if (!empty($itemArr)) {//IF ITEM ARRAY NOT EMPTY
            $totalAmt = 0;
            foreach ($itemArr as $keyItem => $valueItem) {
                $itemId = $valueItem['itemId'];
                $itemName = $valueItem['itemName'];
                $itemAmount = $valueItem['itemAmount'];
                $hsnCode = $valueItem['hsnCode'];
                $totalAmt = $totalAmt + $itemAmount;

                if ($itemId == "") {
                    //echo "abcd"; die;
                    $glitemId = generateId(CENTRALISECUSTOMITEMMASTER, 'GLITEM', $clientId);
                    $insertItem = array();
                    $insertItem['PK_ITEM_ID'] = $glitemId;
                    $insertItem['FK_CLIENT_ID'] = $clientId;
                    $insertItem['FK_ORG_ID'] = $orgId;
                    $insertItem['ITEM_NAME'] = $itemName;
                    $insertItem['CREATED_BY_DATE'] = $time;
                    $insertItem['CREATED_BY'] = $userId;
                    //print_r($insertItem); die;
                    $requestItem = insert_rec(CENTRALISECUSTOMITEMMASTER, $insertItem);

                    $tempItemIdArr[$keyItem] = $glitemId;
                }
            }
        }

        $totalAmt = 0;
        $chargeMstIdArr = array();
        if (!empty($itemArr)) {//IF ITEM ARRAY NOT EMPTY
            foreach ($itemArr as $keyItem => $valueItem) {
                $itemId = $valueItem['itemId'];
                $itemName = $valueItem['itemName'];
                $itemAmount = $valueItem['itemAmount'];
                $hsnCode = $valueItem['hsnCode'];
                $cgst = $valueItem['cgst'];
                $sgst = $valueItem['sgst'];
                $igst = $valueItem['igst'];
                $taxcgst = ($cgst > 0) ? ($itemAmount * $cgst) / 100 : 0;
                $taxsgst = ($sgst > 0) ? ($itemAmount * $sgst) / 100 : 0;
                $taxigst = ($igst > 0) ? ($itemAmount * $igst) / 100 : 0;
                $calTotalTax = $taxcgst + $taxsgst + $taxigst;
                $calItemTotalPrice = $itemAmount + $calTotalTax;
                $totalAmt = $totalAmt + $itemAmount + $calTotalTax;
                //print_r($totalAmt); 
                $insertChargeMaster = array();
                $insertChargeMaster['FK_CLIENT_ID'] = $clientId;
                $insertChargeMaster['FK_ORG_ID'] = $orgId;
                $insertChargeMaster['TYPEOFCUST'] = 'STUDENT';
                $insertChargeMaster['FK_ITEM_ID'] = ($itemId == '') ? $tempItemIdArr[$keyItem] : $itemId;
                $insertChargeMaster['REF_ITEM_ID'] = ($itemId == '') ? $tempItemIdArr[$keyItem] : $itemId;
                $insertChargeMaster['ITEM_NAME'] = $itemName;
                $insertChargeMaster['STUDENT_ID'] = $studentId;
                $insertChargeMaster['STUDENT_FULL_NAME'] = $sqlGetStudent[0]['FIRST_NAME'] . " " . $sqlGetStudent[0]['LAST_NAME'];
                $insertChargeMaster['QTY_OF_SESSION'] = '1';
                $insertChargeMaster['BIILED_SESSION'] = '1';
                $insertChargeMaster['TAX_CGST'] = ($cgst == '') ? "0" : $cgst;
                $insertChargeMaster['TAX_SGST'] = ($sgst == '') ? "0" : $sgst;
                $insertChargeMaster['TAX_IGST'] = ($igst == '') ? "0" : $igst;
                $insertChargeMaster['HSN_CODE'] = $hsnCode;
                $insertChargeMaster['ITEM_BASE_PRICE'] = $itemAmount;
                $insertChargeMaster['TOTAL_TAX'] = $calTotalTax;
                $insertChargeMaster['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;
                $insertChargeMaster['INVOICE_FLAG'] = '';
                $insertChargeMaster['TYPE'] = 'GENERAL';
                $insertChargeMaster['TOLLERANCE_FALG'] = '';
                $insertChargeMaster['IS_TRANSFER'] = '0';
                $insertChargeMaster['TRANSFER_REF_ID'] = '0';
                $insertChargeMaster['INVOICE_ID'] = $pkInvoiceId;
                $insertChargeMaster['CHARGE_DATE'] = date('Y-m-d', strtotime($invDate));
                $insertChargeMaster['CREATED_BY'] = $userId;
                $insertChargeMaster['CREATED_BY_DATE'] = time();
                //print_r($insertChargeMaster); die;
                $insertChargeMasterData = insert_rec(CHARGEMASTER, $insertChargeMaster);
                array_push($chargeMstIdArr, $insertChargeMasterData['lastInsertedId']);

                $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;

                $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT,CLOSINGBAL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $sqlGetStudent[0]['PK_STUD_ID'] . "'";
                $resStudentData = fetch_rec_query($sqlStudentData); // 
                if (count($resStudentData) > 0) {
                    $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                    $billedAmt = $resStudentData[0]['BILL_AMT'];
                    $closingBalAmt = $resStudentData[0]['CLOSINGBAL_AMT'];
                    $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                }
                //echo $calItemTotalPrice; die;

                $updateStudentArr = ARRAY();
                $updateStudentArr['UNBILLED_AMT'] = floatval($calItemTotalPrice) + floatval($unbilledAmt);
                $updateStudentArr['UNBILLED_TYPE'] = 'DR';
                $updateStudentArr['CHANGED_BY'] = $userId;
                $updateStudentArr['CHANGED_BY_DATE'] = time();
                $whereStudCond = "PK_STUD_ID='" . $sqlGetStudent[0]['PK_STUD_ID'] . "'";
                $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                if (!$updateStud) {
                    $msg = UPDATEFAIL . " " . STUDENTMASTER;
                }
            }
            if ($insertChargeMasterData['lastInsertedId'] > 0) {

                $insertInvoiceMaster = array();
                $insertInvoiceMaster['FK_CLIENT_ID'] = $clientId;
                $insertInvoiceMaster['FK_ORG_ID'] = $orgId;
                $insertInvoiceMaster['FK_STUDENT_ID'] = $studentId;
                $insertInvoiceMaster['C_INVOICE_ID'] = $pkInvoiceId;
                $insertInvoiceMaster['INVOICE_DATE'] = date('Y-m-d', strtotime($invDate));
                $insertInvoiceMaster['INVOICE_NAME'] = 'General Invoice';
                $insertInvoiceMaster['INVOICE_TYPE'] = 'GENERAL';
                $insertInvoiceMaster['NARRATION'] = $narration;
                $insertInvoiceMaster['PERIOD_FROM'] = date('Y-m-d');
                $insertInvoiceMaster['PERIOD_TO'] = date('Y-m-d');
                $insertInvoiceMaster['INVOICE_REF_ID'] = 0; //misc
                $insertInvoiceMaster['TOTAL_CHARGES'] = $totalAmt;
                $insertInvoiceMaster['CREATED_BY'] = $userId;
                $insertInvoiceMaster['CREATED_BY_DATE'] = time();
                $insertInvoiceMasterData = insert_rec(INVOICEMASTER, $insertInvoiceMaster);

                if ($insertInvoiceMasterData['lastInsertedId'] > 0) {
                    //$totalAmt = 0;
                    //print_r($tempItemIdArr);
                    foreach ($itemArr as $keyItem => $valueItem) {
                        //$fkItemId = $insertInvoiceMasterData['lastInsertedId'];
                        $itemId = $valueItem['itemId'];
                        $itemName = $valueItem['itemName'];
                        $itemAmount = $valueItem['itemAmount'];
                        $hsnCode = $valueItem['hsnCode'];
                        $cgst = $valueItem['cgst'];
                        $sgst = $valueItem['sgst'];
                        $igst = $valueItem['igst'];
                        $taxcgst = ($cgst > 0) ? ($itemAmount * $cgst) / 100 : 0;
                        $taxsgst = ($sgst > 0) ? ($itemAmount * $sgst) / 100 : 0;
                        $taxigst = ($igst > 0) ? ($itemAmount * $igst) / 100 : 0;
                        $calTotalTax = $taxcgst + $taxsgst + $taxigst;
                        $calItemTotalPrice = $itemAmount + $calTotalTax;
                        //$totalAmt = $totalAmt + $itemAmount;
                        // $totalAmt = $totalAmt + $itemAmount;
                        $insertItem1 = array();
                        $insertItem1['FK_INVOICE_ID'] = $pkInvoiceId;
                        $insertItem1['FK_ITEM_ID'] = ($itemId == '') ? $tempItemIdArr[$keyItem] : $itemId; //ITEM ID FROM CUSTOM ITEM MASTER TABLE
                        $insertItem1['REF_ITEM_ID'] = ($itemId == '') ? $tempItemIdArr[$keyItem] : $itemId; //ITEM ID FROM CUSTOM ITEM MASTER TABLE
                        $insertItem1['CHARGE_TYPE'] = 'STUDENT';
                        $insertItem1['FK_STUDENT_ID'] = $studentId;

                        $insertItem1['ITEM_NAME'] = $itemName; //MISC
                        $insertItem1['ITEM_BASIC_PRICE'] = $itemAmount; //MISC
                        $insertItem1['QTY'] = 1; //MISC
                        $insertItem1['SGST'] = ($sgst == '') ? "0" : $sgst;
                        $insertItem1['CGST'] = ($cgst == '') ? "0" : $cgst;
                        $insertItem1['IGST'] = ($igst == '') ? "0" : $igst;
                        $insertItem1['HSN_CODE'] = $hsnCode;
                        $insertItem1['TOTAL_TAX'] = $calTotalTax;
                        $insertItem1['TOTAL_CHARGES'] = $itemAmount; //MISC
                        $insertItem1['PAYABLE_AMOUNT'] = $calItemTotalPrice; //MISC
                        $insertItem1['CREATED_BY'] = $userId;
                        $insertItem1['CREATED_BY_DATE'] = $time;
                        $insertItem1['DELETE_FLAG'] = 0;
                        $insertinvoiceTrnas = insert_rec(INVOICETRANSMASTER, $insertItem1);
                    }
                }

                // UPDATE CHARGE MASTER TABLE
                $chargeIds = implode(',', $chargeMstIdArr);
                $updateArr = array();
                $updateArr['INVOICE_FLAG'] = 1;
                $updateArr['CHANGED_BY'] = $userId;
                $updateArr['CHANGED_BY_DATE'] = $time;

                $updateWhere = "PK_CHARGE_ID IN($chargeIds) AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                $updateCharge = update_rec(CHARGEMASTER, $updateArr, $updateWhere);
            } else {
                $result = array("status" => INSERTFAIL . " " . INVOICEMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => INSERTFAIL . " " . CHARGEMASTER);
            http_response_code(400);
        }

        //ADD BY DEVIKA.1.9.2018.
        // UPDATE DATA INTO STUDENT TABLE
        // 1. BILLED AMT / TYPE
        // 2. CB BALANCE / TYPE
        $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;

        $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $sqlGetStudent[0]['PK_STUD_ID'] . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
        $resStudentData = fetch_rec_query($sqlStudentData); // 
        if (count($resStudentData) > 0) {
            $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
            $billedAmt = $resStudentData[0]['BILL_AMT'];
            $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
        }


        $closingBalAmt = (floatval($billedAmt) + floatval($totalAmt)) - floatval($receiptAmt);
        $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR"; // IF CB IS IN - THEN CR OF STUDENT ELSE DR OF STUDENT


        $updateStudentArr = ARRAY();
        $updateStudentArr['UNBILLED_AMT'] = $unbilledAmt - floatval($totalAmt);
        $updateStudentArr['UNBILLED_TYPE'] = 'DR';
        $updateStudentArr['BILL_AMT'] = floatval($totalAmt) + floatval($billedAmt);
        $updateStudentArr['BILL_TYPE'] = 'DR';
        $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
        $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;
        $updateStudentArr['CHANGED_BY'] = $userId;
        $updateStudentArr['CHANGED_BY_DATE'] = time();
        $whereStudCond = "PK_STUD_ID='" . $sqlGetStudent[0]['PK_STUD_ID'] . "'";
        $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);

        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.10.8.2018.
function listStudentNonAvailingService($postData) {
    // POST DATA
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $serviceId = isset($postData['postData']['serviceId']) ? addslashes(trim($postData['postData']['serviceId'])) : "";
    $date = date('Y-m-d');

    if (!empty($clientId) && !empty($orgId)) {
        $whereCond = "";
        if (!empty($studentId)) {
            $whereCond = " AND FK_STUD_ID ='" . $studentId . "'";
        }
        $whereServiceCond = "";
        if (!empty($serviceId)) {
            $whereServiceCond = " AND PK_SERVICE_ID ='" . $serviceId . "'";
        }

        $sqlServiceData = "SELECT SS.PK_SERVICE_ID,SS.C_SERVICE_ID,SS.FK_STUD_ID,SS.SERVICE_START_DATE,SS.SERVICE_END_DATE,SS.FK_ITEM_ID,SS.DESCRIPTION,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME FROM " . SERVICEMASTER . " SS INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID=SS.FK_STUD_ID WHERE SS.DELETE_FLAG ='0' AND SS.FK_CLIENT_ID = '" . $clientId . "' AND SS.FK_ORG_ID = '" . $orgId . "'" . $whereCond . $whereServiceCond . " ORDER BY SS.PK_SERVICE_ID DESC"; // QUERY FOR ITEM DATA.

        $resServiceData = fetch_rec_query($sqlServiceData); // RESULT FOR ITEM DATA.
        if (count($resServiceData) > 0) {
            $serviceArr = array();
            foreach ($resServiceData as $keyService => $valueService) { // ITEM DATA LOOP
                $serviceArr[$keyService]['cServiceId'] = $valueService['C_SERVICE_ID'];
                $serviceArr[$keyService]['serviceId'] = $valueService['PK_SERVICE_ID'];
                $serviceArr[$keyService]['serviceStartDate'] = $valueService['SERVICE_START_DATE'];
                $serviceArr[$keyService]['serviceEndDate'] = $valueService['SERVICE_END_DATE'];
                $serviceArr[$keyService]['itemId'] = $valueService['FK_ITEM_ID'];
                $serviceArr[$keyService]['description'] = $valueService['DESCRIPTION'];
                $serviceArr[$keyService]['studName'] = $valueService['STUD_NAME'];

                $sqlservicetrnsDate = "SELECT PK_SERVICE_TRANS_ID,SERVICE_DATE,FK_SERVICE_ID,STATUS FROM " . SERVICETRANS . " WHERE FK_SERVICE_ID='" . $valueService['PK_SERVICE_ID'] . "' AND DELETE_FLAG ='0'";
                $resServicetransData = fetch_rec_query($sqlservicetrnsDate);
                if (count($resServicetransData) > 0) {
                    $servicetransArr = array();
                    foreach ($resServicetransData as $keyServicetrans => $valueServicetrans) {
                        $servicetransArr[$keyServicetrans]['serviceDate'] = $valueServicetrans['SERVICE_DATE'];
                        $servicetransArr[$keyServicetrans]['status'] = $valueServicetrans['STATUS'];
                        $servicetransArr[$keyServicetrans]['transId'] = $valueServicetrans['PK_SERVICE_TRANS_ID'];
                        $date = new DateTime();
                        $date2 = new DateTime($servicetransArr[$keyServicetrans]['serviceDate']);

                        if ($date > $date2) {
                            $servicetransArr[$keyServicetrans]['hide'] = '1';
                        } else {
                            $servicetransArr[$keyServicetrans]['hide'] = '0';
                        }
                    }

                    $serviceArr[$keyService]['servicetransData'] = array_values($servicetransArr);
                } else {
                    $serviceArr[$keyService]['servicetransData'] = NORECORDS;
                }
            }
            $result = array("status" => SCS, "data" => array_values($serviceArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//END BY DEVIKA.10.8.2018

function deleteStudentService($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $serviceId = isset($postData['postData']['serviceId']) ? addslashes(trim($postData['postData']['serviceId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($serviceId)) {

        $updateServiceArray = Array(); //ITEM ARRAY UPDATE
        $updateServiceArray['DELETE_FLAG'] = 1;
        $updateServiceArray['CHANGED_BY'] = $userId;
        $updateServiceArray['CHANGED_BY_DATE'] = time();

        $whereServiceCond = "PK_SERVICE_ID ='" . $serviceId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
        $updateService = update_rec(SERVICEMASTER, $updateServiceArray, $whereServiceCond);  // UPDATE DELETE_FLAG =1 IN STUDENT_SUSPENSION TABLE.

        if ($updateService) {

            $updateserviceTrnasArray = Array(); //ITEM ARRAY UPDATE
            $updateserviceTrnasArray['DELETE_FLAG'] = 1;
            $updateserviceTrnasArray['CHANGED_BY'] = $userId;
            $updateserviceTrnasArray['CHANGED_BY_DATE'] = time();

            $whereserviceTransCond = "FK_SERVICE_ID ='" . $serviceId . "'";
            $updateServicetrans = update_rec(SERVICETRANS, $updateserviceTrnasArray, $whereserviceTransCond);
            if ($updateServicetrans) {
                $result = array("status" => SCS);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . SERVICETRANS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => UPDATEFAIL . " " . SERVICEMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function updateServiceStatus($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $servicetransId = isset($postData['postData']['servicetransId']) ? addslashes(trim($postData['postData']['servicetransId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($servicetransId)) {
        $updateServiceStatus = array();
        $updateServiceStatus['CHANGED_BY'] = $userId;
        $updateServiceStatus['CHANGED_BY_DATE'] = time();
        $updateServiceStatus['STATUS'] = 2;

        $whereServiceStatus = "PK_SERVICE_TRANS_ID = '" . $servicetransId . "'";
        $updateserviceStatusQuery = update_rec(SERVICETRANS, $updateServiceStatus, $whereServiceStatus);
        if ($updateserviceStatusQuery) {

            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.10.8.2018.
function listCmsData($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";

    $whereClientIdCond = $whereOrgIdCond = "";

    if (!empty($clientId)) {
        $whereClientIdCond = " AND FK_CLIENT_ID = '" . $clientId . "'";
    }
    if (!empty($orgId)) {
        $whereOrgIdCond = " AND FK_ORG_ID = '" . $orgId . "'";
    }

    $sqlCmsData = "SELECT PK_SECTION_ID,SECTION_NAME,SECTION_DETAIL,FK_CLIENT_ID,FK_ORG_ID FROM " . PAGESECTION . " WHERE DELETE_FLAG ='0' " . $whereClientIdCond . $whereOrgIdCond; // QUERY FOR ITEM DATA.

    $resCmsData = fetch_rec_query($sqlCmsData); // RESULT FOR ITEM DATA.
    if (count($resCmsData) > 0) {
        $listcmsDataArr = array();
        foreach ($resCmsData as $keyCms => $valueCms) { // ITEM DATA LOOP
            $listcmsDataArr[$keyCms]['sectionId'] = $valueCms['PK_SECTION_ID'];
            $listcmsDataArr[$keyCms]['sectionName'] = $valueCms['SECTION_NAME'];
            $listcmsDataArr[$keyCms]['sectionDetail'] = $valueCms['SECTION_DETAIL'];
            $listcmsDataArr[$keyCms]['clientId'] = $valueCms['FK_CLIENT_ID'];
            $listcmsDataArr[$keyCms]['orgId'] = $valueCms['FK_ORG_ID'];
        }
        $result = array("status" => SCS, "data" => array_values($listcmsDataArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.10.8.2018
//ADD BY DEVIKA.17.8.2018//EPOSE CREATE RECIPT USED
//remove TALLY_ALIAS//used in tally 
//studentId=STU000080
function getStudentTallyName($studentId) {
    $result = Array();
    //ECHO "SELECT PK_STUD_ID,MEMBERSHIP_NUMBER,FIRST_NAME,LAST_NAME FROM " . STUDENTMASTER . " WHERE FK_EPOS_CUST_ID ='" . $studentId . "' AND IS_ACTIVE='1' AND DELETE_FLAG=0 ";
    $sqlGetTallyName = fetch_rec_query("SELECT PK_STUD_ID,MEMBERSHIP_NUMBER,FIRST_NAME,LAST_NAME FROM " . STUDENTMASTER . " WHERE FK_EPOS_CUST_ID ='" . $studentId . "' AND IS_ACTIVE='1' AND DELETE_FLAG=0 ");
    if (count($sqlGetTallyName) > 0) {


        $result['studentName'] = $sqlGetTallyName[0]['FIRST_NAME'] . ' ' . $sqlGetTallyName[0]['LAST_NAME'];
        $result['studentId'] = $sqlGetTallyName[0]['PK_STUD_ID'];
    }
    return $result;
}

//END BY DEVIKA.17.8.2018.
//ADDD BY DEVIKA.16.8.2018
//EPOS RECIPT CREATE./START FUNCTION FOR ADD RECEIPT TRANSACTION DATA
function eposReceipt($postData) {
    //print_r($postData);die;
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $spaceId = isset($postData['postData']['spaceId']) ? $postData['postData']['spaceId'] : ""; //Laundry : 101 FNB : 100
    $location = isset($postData['postData']['location']) ? $postData['postData']['location'] : "";

    $studentId = isset($postData['postData']['studentId']) ? $postData['postData']['studentId'] : ""; ////STU000030
    $paymentId = isset($postData['postData']['paymentId']) ? $postData['postData']['paymentId'] : "";
    $billNo = isset($postData['postData']['billNo']) ? $postData['postData']['billNo'] : "";
    $billDate = isset($postData['postData']['billDate']) ? $postData['postData']['billDate'] : "";
    $paymentModeId = isset($postData['postData']['paymentModeId']) ? $postData['postData']['paymentModeId'] : "";
    $paymentMode = isset($postData['postData']['paymentMode']) ? $postData['postData']['paymentMode'] : "";
    $paymentAmount = isset($postData['postData']['paymentAmount']) ? $postData['postData']['paymentAmount'] : "";
    $narration = isset($postData['postData']['narration']) ? $postData['postData']['narration'] : "";
    $referenceId = isset($postData['postData']['referenceId']) ? $postData['postData']['referenceId'] : "";
    $chequeNo = isset($postData['postData']['chequeNo']) ? $postData['postData']['chequeNo'] : "";
    $partyAddress = isset($postData['postData']['partyAddress']) ? $postData['postData']['partyAddress'] : "";

    //=====process======//
    /* 1.epos receipt create epose side.
     * 2.get the student information by getStudentTallyName().
     * 3.insert into centralised_epos_recipt.
     * 4.insert into centrlised_ladger_master.
     * 5.if tally staging flag on than insert into tallystaging table..
     * 6. 
     */
    //=====process=======//

    $studentNameData = getStudentTallyName($studentId); //Get member tally alis name
    $studentName = $studentNameData['studentName'];
    $studentFkId = $studentNameData['studentId']; //Get member tally alis name



    $InsertArr = array();
    $InsertArr['FK_SPACE_ID'] = $spaceId; //Laundry : 101 FNB : 100
    $InsertArr['FK_CLIENT_ID'] = $clientId;
    $InsertArr['FK_ORG_ID'] = $orgId;
    $InsertArr['VOUCHER_TYPE'] = 'Receipt';
    $InsertArr['LOCATION'] = $location;
    $InsertArr['REFERENCE_NO'] = $referenceId;
    $InsertArr['BILL_NO'] = $billNo;
    $InsertArr['BILL_DATE'] = date('Y-m-d', strtotime($billDate));
    $InsertArr['MEMBERSHIP_NUMBER'] = $studentId; ////STU000030
    $InsertArr['STUDENT_NAME'] = $studentName;
    $InsertArr['PAYMENT_ID'] = $paymentId; //payment id is payment primary key
    $InsertArr['PAYMENT_MODE_ID'] = $paymentModeId; //payment mode id is payment master mode id
    $InsertArr['PAYMENT_MODE'] = $paymentMode;
    $InsertArr['PAYMENT_AMOUNT'] = $paymentAmount;
    $InsertArr['NARRATION'] = $narration;
    $InsertArr['CHEQUE_NUMBER'] = $chequeNo;
    $InsertArr['PARTY_ADDRESS'] = $partyAddress;
    $insertedId = insert_rec(CENTRALISEEPOSRECEIPT, $InsertArr);
    $lastId = $insertedId['lastInsertedId'];


    $result = array("status" => SCS);
    http_response_code(200);
    return $result;
}

//END FUNCTION FOR ADD RECEIPT TRANSACTION DATA
//END BY DEVIKA.16.8.2018.
//ADD BY DEVIKA.16.8.2018.
//START FUNCTION FOR ADD CREDIT NOTE TRANSACTION DATA
function eposCreditNoteTransaction($postData) {
    //print_r($postData);die;
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $spaceId = isset($postData['postData']['spaceId']) ? $postData['postData']['spaceId'] : ""; //Laundry : 101 FNB : 100
    $location = isset($postData['postData']['location']) ? $postData['postData']['location'] : "";

    $studentId = isset($postData['postData']['studentId']) ? $postData['postData']['studentId'] : ""; //STU000018
    $billNo = isset($postData['postData']['billNo']) ? $postData['postData']['billNo'] : "";
    $billDate = isset($postData['postData']['billDate']) ? $postData['postData']['billDate'] : "";
    $basicAmount = isset($postData['postData']['basicAmount']) ? $postData['postData']['basicAmount'] : "";
    $taxAmount = isset($postData['postData']['taxAmount']) ? $postData['postData']['taxAmount'] : "";
    $discountAmount = isset($postData['postData']['discountAmount']) ? $postData['postData']['discountAmount'] : "";
    $discountPer = isset($postData['postData']['discountPer']) ? $postData['postData']['discountPer'] : "";
    $roundOff = isset($postData['postData']['roundOff']) ? $postData['postData']['roundOff'] : "";
    $finalAmount = isset($postData['postData']['finalAmount']) ? $postData['postData']['finalAmount'] : "";
    $narration = isset($postData['postData']['narration']) ? $postData['postData']['narration'] : "";
    $referenceId = isset($postData['postData']['referenceId']) ? $postData['postData']['referenceId'] : "";

    //=====process======//
    /*
     * 1.create epose creditnote Transaction.
     * 2. get the student information by getStudentTallyName().
     * 3. insert into centralised epose credit note.
     * 4. insert multiple items insert in to centralised epos credit note items
     * 5. insert cgst and sgst tax insert into centrlised epos tax master
     * 6. 
     * 7. if tally staging flag on than insert into tallystaging table..
     * 
     */
    //======process=====//

    $studentNameData = getStudentTallyName($studentId); //Get member tally alis name
    $studentName = $studentNameData['studentName'];
    $studentFkId = $studentNameData['studentId']; //Get student detal

    $InsertArr = array();
    $InsertArr['FK_SPACE_ID'] = $spaceId; //Laundry : 101 FNB : 100
    $InsertArr['FK_CLIENT_ID'] = $clientId;
    $InsertArr['FK_ORG_ID'] = $orgId;
    $InsertArr['VOUCHER_TYPE'] = 'CREDIT NOTE';
    $InsertArr['LOCATION'] = $location;
    $InsertArr['REFERENCE_NO'] = $referenceId;
    $InsertArr['BILL_NO'] = $billNo;
    $InsertArr['BILL_DATE'] = date('Y-m-d', strtotime($billDate));
    $InsertArr['MEMBERSHIP_NUMBER'] = $studentId; //STU000018
    $InsertArr['STUDENT_NAME'] = $studentName;
    $InsertArr['BASIC_AMOUNT'] = $basicAmount;
    $InsertArr['TAX_AMOUNT'] = $taxAmount;
    $InsertArr['DISCOUNT_AMOUNT'] = $discountAmount;
    $InsertArr['DISCOUNT_TYPE'] = $discountPer;
    $InsertArr['ROUND_OFF'] = $roundOff;
    $InsertArr['FINAL_AMOUNT'] = $finalAmount;
    $InsertArr['NARRATION'] = $narration;
    $insertedId = insert_rec(CENTRALISEEPOSCREDITNOTE, $InsertArr);
    $lastId = $insertedId['lastInsertedId'];

    if ($lastId > 0 && $lastId != "") {

        //ITEMS TRANS DATA
        $itemsArr = $postData['postData']['items'];
        if (!empty($itemsArr)) {
            foreach ($itemsArr as $keyItem => $valuItem) {
                $billNo = $valuItem['billNo'];
                $itemId = $valuItem['itemId'];
                $itemName = $valuItem['itemName'];
                $itemRate = $valuItem['itemRate'];
                $itemQty = $valuItem['itemQty'];
                $uomId = $valuItem['uomId'];
                $uomName = $valuItem['uomName'];
                $itemAmount = $valuItem['itemAmount'];

                $itemInsertArr = array();
                $itemInsertArr['FK_CREDIT_NOTE_ID'] = $lastId;
                $itemInsertArr['FK_CLIENT_ID'] = $clientId;
                $itemInsertArr['FK_ORG_ID'] = $orgId;
                $itemInsertArr['BILL_NO'] = $billNo;
                $itemInsertArr['FK_ITEM_ID'] = $itemId;
                $itemInsertArr['ITEM_NAME'] = $itemName;
                $itemInsertArr['ITEM_RATE'] = $itemRate;
                $itemInsertArr['ITEM_QTY'] = $itemQty;
                $itemInsertArr['UOM_ID'] = $uomId;
                $itemInsertArr['UOM_NAME'] = $uomName;
                $itemInsertArr['ITEM_AMOUNT'] = $itemAmount;
                insert_rec(CENTRALISEEPOSCREDITNOTEITEMS, $itemInsertArr);
            }
        }
    }

    $result = array("status" => SCS);
    http_response_code(200);
    return $result;
}

//END FUNCTION FOR ADD CREDIT NOTE TRANSACTION DATA
//END BY DEVIKA.16.8.2018.
//ADD BY DEVIKA.18.8.2018
//START FUNCTION FOR ADD SALES TRANSACTION DATA
function eposSalesTransaction($postData) {


    //ECHO "<PRE>";
    //print_r($postData);die;
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $spaceId = isset($postData['postData']['spaceId']) ? $postData['postData']['spaceId'] : ""; //Laundry : 101 FNB : 100
    $location = isset($postData['postData']['location']) ? $postData['postData']['location'] : "";

    $studentId = isset($postData['postData']['customerId']) ? $postData['postData']['customerId'] : ""; //STU000011
    $onlinecustomerId = isset($postData['postData']['onlinecustomerId']) ? $postData['postData']['onlinecustomerId'] : ""; //STU000011
    $billNo = isset($postData['postData']['billNo']) ? $postData['postData']['billNo'] : "";
    $billDate = isset($postData['postData']['billDate']) ? $postData['postData']['billDate'] : "";
    $basicAmount = isset($postData['postData']['basicAmount']) ? $postData['postData']['basicAmount'] : "";
    $taxAmount = isset($postData['postData']['taxAmount']) ? $postData['postData']['taxAmount'] : "";
    $discountAmount = isset($postData['postData']['discountAmount']) ? $postData['postData']['discountAmount'] : "";
    $discountPer = isset($postData['postData']['discountType']) ? $postData['postData']['discountType'] : "";
    $roundOff = isset($postData['postData']['roundOff']) ? $postData['postData']['roundOff'] : "";
    $finalAmount = isset($postData['postData']['finalAmount']) ? $postData['postData']['finalAmount'] : "";
    $narration = isset($postData['postData']['narration']) ? $postData['postData']['narration'] : "";


    //=====process======//
    /*
     * 1. create eposSalesTransaction.
     * 2. get the student information by getStudentTallyName().
     * 3. insert into centralised epose Sales.
     * Condition :
     *    Case 1:Laundry 
     *          1. IF total qty is grater than 50 then need to check the bill qty will be charge partail or full.
     *          2. 45 qty used and new bill is 7 then you need to make bill charges partial.
     *          3. 45 + 7 = 52 - 50 (cal table only when used qty is less than 50 then take 50 as constant ) = 2 ==> if qty of bill != 50 then partail else full (partail charges).
     * 4. insert multiple items insert in to centralised epos credit note items
     * 5. insert cgst and sgst tax insert into centrlised epos tax master
     *  Case 2:FNB
     *          1. O Value bill.
     * 6. Update used qty in cal table.
     * 
     * 
     */
    //======process=====//
    $sqlclientData = "SELECT FNB_SPACE_ID,FNB_LAUNDRY_ID,PK_CLIENT_ID FROM " . CLIENTMASTER . " WHERE EPOS_CLIENT_ID ='" . $clientId . "'";
    $resClientData = fetch_rec_query($sqlclientData);
    $fnbSpaceId = $resClientData[0]['FNB_SPACE_ID'];
    $laundryId = $resClientData[0]['FNB_LAUNDRY_ID'];
    $clientId = $resClientData[0]['PK_CLIENT_ID'];


    $studentNameData = getStudentTallyName($onlinecustomerId); //Get member tally alis name
    //print_r($studentNameData); EXIT;
    $studentName = $studentNameData['studentName'];
    $studentFkId = $studentNameData['studentId'];
    $updateArr = array();

    //ADD BY DEVIKA.23.8.2018.
    // GET THE ACTUAL QTY USED FROM CAL TABLE AGAINST STUDENT AND DATE

    $sqlGetCallData = "SELECT CT.QTY,CT.USED_QTY,CT.FK_ITEM_ID,CT.FK_ORG_ID FROM " . CALALLOCATIONTRANS . " CT INNER JOIN " . ITEMMASTER . " IM ON IM.PK_ITEM_ID = CT.FK_ITEM_ID AND IM.DELETE_FLAG = 0 WHERE '" . date('Y-m-d', strtotime($billDate)) . "' BETWEEN ALLOCATION_START_DATE AND ALLOCATION_END_DATE AND FK_STUDENT_ID='" . $studentFkId . "' AND IM.EPOS_SPACE_ID ='" . $spaceId . "'  LIMIT 0,1";


    $resultGetCallData = fetch_rec_query($sqlGetCallData);
    $usedQty = "0";
    if (count($resultGetCallData) > 0) {
        $usedQty = $resultGetCallData[0]['USED_QTY'];
        $orgId = $resultGetCallData[0]['FK_ORG_ID'];
    }

    $itemsArr = $postData['postData']['items'];

    $taxArr = isset($postData['postData']['tax']) ? $postData['postData']['tax'] : Array();
    if ($spaceId == $laundryId) { // LAUNDRY CASE STARTED
        //ITEMS TRANS DATA
        if (!empty($itemsArr)) {
            //for ($i = 0; $i < count($itemsArr); $i++) {
            $totalItemQty = 0;
            foreach ($itemsArr as $keyItem => $valueItem) {
                $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $valueItem['itemId'] . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                $resItemData = fetch_rec_query($sqlItemData);

                if (isset($resItemData[0]['PK_ITEM_ID']) && $resItemData[0]['PK_ITEM_ID'] > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.   
                    $itemQty = $valueItem['itemQty'];
                    $totalItemQty += $itemQty;
                }
            }
        }

        if ($usedQty > 50) { // GREATER THAN 50
            // $calculatedBillQty = $totalItemQty;
            $InsertArr = array();
            $InsertArr['FK_SPACE_ID'] = $spaceId; //Laundry : 101 FNB : 100
            $InsertArr['FK_CLIENT_ID'] = $clientId;
            $InsertArr['FK_ORG_ID'] = $orgId;
            $InsertArr['VOUCHER_TYPE'] = 'SALES';
            $InsertArr['LOCATION'] = $location;
            $InsertArr['BILL_NO'] = $billNo;
            $InsertArr['BILL_DATE'] = date('Y-m-d', strtotime($billDate));
            //$InsertArr['MEMBERSHIP_NUMBER'] = $studentId; //STU000011 STU000011 COMMENTED BY KAUSHA SHAH ON 15-09-2018 FOR CONSIDERING ONLINE CUSTOMER ID INSTEAD OF CUSTOMER ID
            $InsertArr['MEMBERSHIP_NUMBER'] = $onlinecustomerId;
            $InsertArr['STUDENT_NAME'] = $studentName;
            $InsertArr['BASIC_AMOUNT'] = $basicAmount;
            $InsertArr['TAX_AMOUNT'] = $taxAmount;
            $InsertArr['DISCOUNT_AMOUNT'] = $discountAmount;
            $InsertArr['DISCOUNT_TYPE'] = $discountPer;
            $InsertArr['ROUND_OFF'] = $roundOff;
            $InsertArr['FINAL_AMOUNT'] = $finalAmount;

            $InsertArr['BACKUP_BASIC_AMOUNT'] = $basicAmount;
            $InsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;
            $InsertArr['BACKUP_DISCOUNT_AMOUNT'] = $discountAmount;
            $InsertArr['BACKUP_DISCOUNT_TYPE'] = $discountPer;
            $InsertArr['BACKUP_ROUND_OFF'] = $roundOff;
            $InsertArr['BACKUP_FINAL_AMOUNT'] = $finalAmount;
            $InsertArr['NARRATION'] = $narration;
            $insertedId = insert_rec(CENTRALISEEPOSSALES, $InsertArr);
            $lastId = $insertedId['lastInsertedId'];

            if ($insertedId) {
                $finalItemCount = $finalAmountTotal = $finalTaxTotal = $finalTotal = 0;
                // START ITEM ADD INTO CENTRALISE_EPOS_SALES_ITEMS  
                foreach ($itemsArr as $keyItem => $valueItem) {

                    $billNo = $valueItem['billNo'];
                    $itemId = $valueItem['itemId'];
                    $itemName = $valueItem['itemName'];
                    $itemRate = $valueItem['itemRate'];
                    $itemQty = $valueItem['itemQty'];
                    $uomId = $valueItem['uomId'];
                    $uomName = $valueItem['uomName'];
                    $itemAmount = $valueItem['itemAmount'];

                    $itemInsertArr = array();
                    $itemInsertArr['FK_SALES_ID'] = $lastId;
                    $itemInsertArr['FK_CLIENT_ID'] = $clientId;
                    $itemInsertArr['FK_ORG_ID'] = $orgId;
                    $itemInsertArr['BILL_NO'] = $billNo;
                    $itemInsertArr['FK_ITEM_ID'] = $itemId;
                    $itemInsertArr['ITEM_NAME'] = $itemName;
                    $itemInsertArr['UOM_ID'] = $uomId;
                    $itemInsertArr['UOM_NAME'] = $uomName;
                    $itemInsertArr['BACKUP_ITEM_RATE'] = $itemRate;
                    $itemInsertArr['BACKUP_ITEM_QTY'] = $itemQty;
                    $itemInsertArr['BACKUP_ITEM_AMOUNT'] = $itemAmount;

                    $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                    $resItemData = fetch_rec_query($sqlItemData);

                    if (isset($resItemData[0]['PK_ITEM_ID']) && $resItemData[0]['PK_ITEM_ID'] > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                        $itemInsertArr['ITEM_RATE'] = $itemRate;
                        $itemInsertArr['ITEM_QTY'] = $itemQty;
                        $itemInsertArr['ITEM_AMOUNT'] = $itemAmount;
                        $finalAmountTotal = $finalAmountTotal + ($itemRate * $itemQty);
                        $finalItemCount += $itemQty;
                    } else { // ELSE REQUIRED FILED TO AS IT IS.
                        $itemInsertArr['ITEM_RATE'] = $itemRate;
                        $itemInsertArr['ITEM_QTY'] = $itemQty;
                        $itemInsertArr['ITEM_AMOUNT'] = $itemAmount;
                        $finalAmountTotal = $finalAmountTotal + ($itemRate * $itemQty);
                    }


                    insert_rec(CENTRALISEEPOSSALESITEMS, $itemInsertArr);
                }
                // END ITEM ADD INTO CENTRALISE_EPOS_SALES_ITEMS 
                // START TAX ADD INTO CENTRALISE_EPOS_SALES_TAX  
                foreach ($taxArr as $keyTax => $valueTax) {
                    $billNo = $valueTax['billNo'];
                    $itemId = $valueTax['itemId'];
                    $taxId = $valueTax['taxId'];
                    $taxName = $valueTax['taxName'];
                    $taxPercentage = $valueTax['taxPercentage'];
                    $taxAmount = $valueTax['taxAmount'];

                    $taxInsertArr = array();
                    $taxInsertArr['FK_SALES_ID'] = $lastId;
                    $taxInsertArr['FK_CLIENT_ID'] = $clientId;
                    $taxInsertArr['FK_ORG_ID'] = $orgId;
                    $taxInsertArr['FK_ITEM_ID'] = $itemId;
                    $taxInsertArr['BILL_NO'] = $billNo;
                    $taxInsertArr['FK_TAX_ID'] = $taxId;
                    $taxInsertArr['TAX_NAME'] = $taxName;
                    $taxInsertArr['TAX_PERCENTAGE'] = $taxPercentage;

                    $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                    $resItemData = fetch_rec_query($sqlItemData);
                    if (($resItemData[0]['PK_ITEM_ID']) > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                        $taxInsertArr['TAX_AMOUNT'] = $taxAmount;
                        $finalTaxTotal = $finalTaxTotal + $taxAmount;
                    } else { // ELSE REQUIRED FILED TO AS IT IS.
                        $taxInsertArr['TAX_AMOUNT'] = $taxAmount;
                        $finalTaxTotal = $finalTaxTotal + $taxAmount;
                    }

                    $taxInsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;

                    insert_rec(CENTRALISEEPOSSALESTAX, $taxInsertArr);
                }
                // END TAX ADD INTO CENTRALISE_EPOS_SALES_TAX  
            }

            $updateArr['USED_QTY'] = $usedQty + $finalItemCount; //EPOS FNB
            $finalTotal = $finalTaxTotal + $finalAmountTotal;
            $whereSalesCond = "PK_SALES_ID ='" . $lastId . "'";
            $updateSalesArr = array();
            $updateSalesArr['BASIC_AMOUNT'] = $finalAmountTotal;
            $updateSalesArr['TAX_AMOUNT'] = $finalTaxTotal;
            $updateSalesArr['FINAL_AMOUNT'] = $finalTotal;
            $updateSalesData = update_rec(CENTRALISEEPOSSALES, $updateSalesArr, $whereSalesCond);

            if ($finalTotal > 0) {
                 $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;
                $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT FROM " . STUDENTMASTER . " WHERE FK_EPOS_CUST_ID ='" . $onlinecustomerId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                $resStudentData = fetch_rec_query($sqlStudentData); // 
                if (count($resStudentData) > 0) {
                    $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                    $billedAmt = $resStudentData[0]['BILL_AMT'];
                    $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                }


                $closingBalAmt = (floatval($billedAmt) + floatval($finalTotal)) - floatval($receiptAmt);
                $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR"; // IF CB IS IN - THEN CR OF STUDENT ELSE DR OF STUDENT


                $updateStudentArr = ARRAY();
                $updateStudentArr['BILL_AMT'] = floatval($finalTotal) + floatval($billedAmt);
                $updateStudentArr['BILL_TYPE'] = 'DR';
                $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
                $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;
                $updateStudentArr['CHANGED_BY'] = $userId;
                $updateStudentArr['CHANGED_BY_DATE'] = time();
                $whereStudCond = "FK_EPOS_CUST_ID='" . $onlinecustomerId  . "'";
                $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                
            }
        } else if ($usedQty < 50 && (intval($totalItemQty) + intval($usedQty)) <= 50) { // USER QTY IS SMALLER THAN 50 (35+7)
            $calculatedBillQty = 0;
            // $calculatedBillQty = $totalItemQty;
            $InsertArr = array();
            $InsertArr['FK_SPACE_ID'] = $spaceId; //Laundry : 101 FNB : 100
            $InsertArr['FK_CLIENT_ID'] = $clientId;
            $InsertArr['FK_ORG_ID'] = $orgId;
            $InsertArr['VOUCHER_TYPE'] = 'SALES';
            $InsertArr['LOCATION'] = $location;
            $InsertArr['BILL_NO'] = $billNo;
            $InsertArr['BILL_DATE'] = date('Y-m-d', strtotime($billDate));
            //$InsertArr['MEMBERSHIP_NUMBER'] = $studentId; //STU000011 STU000011 COMMENTED BY KAUSHA SHAH ON 15-09-2018 FOR CONSIDERING ONLINE CUSTOMER ID INSTEAD OF CUSTOMER ID
            $InsertArr['MEMBERSHIP_NUMBER'] = $onlinecustomerId;
            $InsertArr['STUDENT_NAME'] = $studentName;
            $InsertArr['BASIC_AMOUNT'] = 0;
            $InsertArr['TAX_AMOUNT'] = 0;
            $InsertArr['DISCOUNT_AMOUNT'] = $discountAmount;
            $InsertArr['DISCOUNT_TYPE'] = $discountPer;
            $InsertArr['ROUND_OFF'] = $roundOff;
            $InsertArr['FINAL_AMOUNT'] = 0;

            $InsertArr['BACKUP_BASIC_AMOUNT'] = $basicAmount;
            $InsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;
            $InsertArr['BACKUP_DISCOUNT_AMOUNT'] = $discountAmount;
            $InsertArr['BACKUP_DISCOUNT_TYPE'] = $discountPer;
            $InsertArr['BACKUP_ROUND_OFF'] = $roundOff;
            $InsertArr['BACKUP_FINAL_AMOUNT'] = $finalAmount;
            $InsertArr['NARRATION'] = $narration;
            $insertedId = insert_rec(CENTRALISEEPOSSALES, $InsertArr);
            $lastId = $insertedId['lastInsertedId'];
            if ($insertedId) {
                $finalItemCount = $finalAmountTotal = $finalTaxTotal = $finalTotal = 0;
                foreach ($itemsArr as $keyItem => $valueItem) {

                    $billNo = $valueItem['billNo'];
                    $itemId = $valueItem['itemId'];
                    $itemName = $valueItem['itemName'];
                    $itemRate = $valueItem['itemRate'];
                    $itemQty = $valueItem['itemQty'];
                    $uomId = $valueItem['uomId'];
                    $uomName = $valueItem['uomName'];
                    $itemAmount = $valueItem['itemAmount'];

                    $itemInsertArr = array();
                    $itemInsertArr['FK_SALES_ID'] = $lastId;
                    $itemInsertArr['FK_CLIENT_ID'] = $clientId;
                    $itemInsertArr['FK_ORG_ID'] = $orgId;
                    $itemInsertArr['BILL_NO'] = $billNo;
                    $itemInsertArr['FK_ITEM_ID'] = $itemId;
                    $itemInsertArr['ITEM_NAME'] = $itemName;
                    $itemInsertArr['UOM_ID'] = $uomId;
                    $itemInsertArr['UOM_NAME'] = $uomName;
                    $itemInsertArr['BACKUP_ITEM_RATE'] = $itemRate;
                    $itemInsertArr['BACKUP_ITEM_QTY'] = $itemQty;
                    $itemInsertArr['BACKUP_ITEM_AMOUNT'] = $itemAmount;

                    $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                    $resItemData = fetch_rec_query($sqlItemData);

                    if (isset($resItemData[0]['PK_ITEM_ID']) && $resItemData[0]['PK_ITEM_ID'] > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                        $itemInsertArr['ITEM_RATE'] = 0;
                        $itemInsertArr['ITEM_QTY'] = 0;
                        $itemInsertArr['ITEM_AMOUNT'] = 0;
                        $finalItemCount += $itemQty;
                        $finalAmountTotal = $finalAmountTotal + 0;
                    } else { // ELSE REQUIRED FILED TO AS IT IS.
                        $itemInsertArr['ITEM_RATE'] = $itemRate;
                        $itemInsertArr['ITEM_QTY'] = $itemQty;
                        $itemInsertArr['ITEM_AMOUNT'] = $itemAmount;
                        $finalAmountTotal = $finalAmountTotal + ($itemRate * $itemQty);
                    }

                    insert_rec(CENTRALISEEPOSSALESITEMS, $itemInsertArr);
                }

                foreach ($taxArr as $keyTax => $valueTax) {
                    $billNo = $valueTax['billNo'];
                    $itemId = $valueTax['itemId'];
                    $taxId = $valueTax['taxId'];
                    $taxName = $valueTax['taxName'];
                    $taxPercentage = $valueTax['taxPercentage'];
                    $taxAmount = $valueTax['taxAmount'];

                    $taxInsertArr = array();
                    $taxInsertArr['FK_SALES_ID'] = $lastId;
                    $taxInsertArr['FK_CLIENT_ID'] = $clientId;
                    $taxInsertArr['FK_ORG_ID'] = $orgId;
                    $taxInsertArr['FK_ITEM_ID'] = $itemId;
                    $taxInsertArr['BILL_NO'] = $billNo;
                    $taxInsertArr['FK_TAX_ID'] = $taxId;
                    $taxInsertArr['TAX_NAME'] = $taxName;
                    $taxInsertArr['TAX_PERCENTAGE'] = $taxPercentage;
                    $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                    $resItemData = fetch_rec_query($sqlItemData);
                    if (($resItemData[0]['PK_ITEM_ID']) > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                        $taxInsertArr['TAX_AMOUNT'] = 0;
                        $finalTaxTotal = $finalTaxTotal + 0;
                    } else { // ELSE REQUIRED FILED TO AS IT IS.
                        $taxInsertArr['TAX_AMOUNT'] = $taxAmount;
                        $finalTaxTotal = $finalTaxTotal + $taxAmount;
                    }

                    $taxInsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;
                    insert_rec(CENTRALISEEPOSSALESTAX, $taxInsertArr);
                }
            }
            $updateArr['USED_QTY'] = $usedQty + $finalItemCount; //EPOS FNB
            $finalTotal = $finalTaxTotal + $finalAmountTotal;
            $whereSalesCond = "PK_SALES_ID ='" . $lastId . "'";
            $updateSalesArr = array();
            $updateSalesArr['BASIC_AMOUNT'] = $finalAmountTotal;
            $updateSalesArr['TAX_AMOUNT'] = $finalTaxTotal;
            $updateSalesArr['FINAL_AMOUNT'] = $finalTotal;
            $updateSalesData = update_rec(CENTRALISEEPOSSALES, $updateSalesArr, $whereSalesCond);
            
            if ($finalTotal > 0) {
                 $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;
                $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT FROM " . STUDENTMASTER . " WHERE FK_EPOS_CUST_ID ='" . $onlinecustomerId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                $resStudentData = fetch_rec_query($sqlStudentData); // 
                if (count($resStudentData) > 0) {
                    $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                    $billedAmt = $resStudentData[0]['BILL_AMT'];
                    $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                }


                $closingBalAmt = (floatval($billedAmt) + floatval($finalTotal)) - floatval($receiptAmt);
                $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR"; // IF CB IS IN - THEN CR OF STUDENT ELSE DR OF STUDENT


                $updateStudentArr = ARRAY();
                $updateStudentArr['BILL_AMT'] = floatval($finalTotal) + floatval($billedAmt);
                $updateStudentArr['BILL_TYPE'] = 'DR';
                $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
                $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;
                $updateStudentArr['CHANGED_BY'] = $userId;
                $updateStudentArr['CHANGED_BY_DATE'] = time();
                $whereStudCond = "FK_EPOS_CUST_ID='" . $onlinecustomerId  . "'";
                $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                
            }
        } else if ($usedQty <= 50 && (intval($totalItemQty) + intval($usedQty)) > 50) { // IF USED COUNT IS SMALLER AND AFTER ADDDING BILL QTY IS GREATER 45 + 7
            $finalItemCount = $finalAmountTotal = $finalTaxTotal = $finalTotal = 0;


            $InsertArr = array();
            $InsertArr['FK_SPACE_ID'] = $spaceId; //Laundry : 101 FNB : 100
            $InsertArr['FK_CLIENT_ID'] = $clientId;
            $InsertArr['FK_ORG_ID'] = $orgId;
            $InsertArr['VOUCHER_TYPE'] = 'SALES';
            $InsertArr['LOCATION'] = $location;
            $InsertArr['BILL_NO'] = $billNo;
            $InsertArr['BILL_DATE'] = date('Y-m-d', strtotime($billDate));
            //$InsertArr['MEMBERSHIP_NUMBER'] = $studentId; //STU000011 STU000011 COMMENTED BY KAUSHA SHAH ON 15-09-2018 FOR CONSIDERING ONLINE CUSTOMER ID INSTEAD OF CUSTOMER ID
            $InsertArr['MEMBERSHIP_NUMBER'] = $onlinecustomerId;
            $InsertArr['STUDENT_NAME'] = $studentName;
            $InsertArr['BASIC_AMOUNT'] = 0;
            $InsertArr['TAX_AMOUNT'] = 0;
            $InsertArr['DISCOUNT_AMOUNT'] = $discountAmount;
            $InsertArr['DISCOUNT_TYPE'] = $discountPer;
            $InsertArr['ROUND_OFF'] = $roundOff;
            $InsertArr['FINAL_AMOUNT'] = 0;

            $InsertArr['BACKUP_BASIC_AMOUNT'] = $basicAmount;
            $InsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;
            $InsertArr['BACKUP_DISCOUNT_AMOUNT'] = $discountAmount;
            $InsertArr['BACKUP_DISCOUNT_TYPE'] = $discountPer;
            $InsertArr['BACKUP_ROUND_OFF'] = $roundOff;
            $InsertArr['BACKUP_FINAL_AMOUNT'] = $finalAmount;
            $InsertArr['NARRATION'] = $narration;


            $insertedId = insert_rec(CENTRALISEEPOSSALES, $InsertArr);
            $lastId = $insertedId['lastInsertedId'];

            foreach ($itemsArr as $keyItem => $valueItem) {

                $billNo = $valueItem['billNo'];
                $itemId = $valueItem['itemId'];
                $itemName = $valueItem['itemName'];
                $itemRate = $valueItem['itemRate'];
                $itemQty = $valueItem['itemQty'];
                $uomId = $valueItem['uomId'];
                $uomName = $valueItem['uomName'];
                $itemAmount = $valueItem['itemAmount'];

                $itemInsertArr = array();
                $itemInsertArr['FK_SALES_ID'] = $lastId;
                $itemInsertArr['FK_CLIENT_ID'] = $clientId;
                $itemInsertArr['FK_ORG_ID'] = $orgId;
                $itemInsertArr['BILL_NO'] = $billNo;
                $itemInsertArr['FK_ITEM_ID'] = $itemId;
                $itemInsertArr['ITEM_NAME'] = $itemName;

                $itemInsertArr['UOM_ID'] = $uomId;
                $itemInsertArr['UOM_NAME'] = $uomName;

                $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                $resItemData = fetch_rec_query($sqlItemData);

                if (isset($resItemData[0]['PK_ITEM_ID']) && $resItemData[0]['PK_ITEM_ID'] > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.   
                    $calculatedBillQty = (intval($itemQty) + intval($usedQty) - 50);

                    $itemInsertArr['ITEM_RATE'] = $itemRate;
                    $itemInsertArr['ITEM_QTY'] = $calculatedBillQty;

                    $itemInsertArr['ITEM_AMOUNT'] = $itemRate * $calculatedBillQty;
                    $finalAmountTotal = $finalAmountTotal + ($itemRate * $calculatedBillQty);
                    $finalItemCount += $itemQty;
                } else { // ELSE REQUIRED FILED TO AS IT IS.
                    $itemInsertArr['ITEM_RATE'] = $itemRate;
                    $itemInsertArr['ITEM_QTY'] = $itemQty;
                    $itemInsertArr['ITEM_AMOUNT'] = $itemAmount;
                    $finalAmountTotal = $finalAmountTotal + ($itemRate * $itemQty);
                }


                $itemInsertArr['BACKUP_ITEM_RATE'] = $itemRate;
                $itemInsertArr['BACKUP_ITEM_QTY'] = $itemQty;
                $itemInsertArr['BACKUP_ITEM_AMOUNT'] = $itemAmount;
                insert_rec(CENTRALISEEPOSSALESITEMS, $itemInsertArr);
            }



            foreach ($taxArr as $keyTax => $valueTax) {
                $billNo = $valueTax['billNo'];
                $itemId = $valueTax['itemId'];
                $taxId = $valueTax['taxId'];
                $taxName = $valueTax['taxName'];
                $taxPercentage = $valueTax['taxPercentage'];
                $taxAmount = $valueTax['taxAmount'];

                $taxInsertArr = array();
                $taxInsertArr['FK_SALES_ID'] = $lastId;
                $taxInsertArr['FK_CLIENT_ID'] = $clientId;
                $taxInsertArr['FK_ORG_ID'] = $orgId;
                $taxInsertArr['FK_ITEM_ID'] = $itemId;
                $taxInsertArr['BILL_NO'] = $billNo;
                $taxInsertArr['FK_TAX_ID'] = $taxId;
                $taxInsertArr['TAX_NAME'] = $taxName;
                $taxInsertArr['TAX_PERCENTAGE'] = $taxPercentage;

                $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                $resItemData = fetch_rec_query($sqlItemData);
                if (($resItemData[0]['PK_ITEM_ID']) > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                    $finalNewTax = ($taxAmount / $totalItemQty) * $calculatedBillQty;
                    $taxInsertArr['TAX_AMOUNT'] = $finalNewTax;
                    $finalTaxTotal = $finalTaxTotal + $finalNewTax;
                } else { // ELSE REQUIRED FILED TO AS IT IS.
                    $taxInsertArr['TAX_AMOUNT'] = $taxAmount;
                    $finalTaxTotal = $finalTaxTotal + $taxAmount;
                }

                $taxInsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;

                insert_rec(CENTRALISEEPOSSALESTAX, $taxInsertArr);
                $finalTaxTotal = $finalTaxTotal + $finalNewTax;
            }
            $updateArr['USED_QTY'] = $usedQty + $finalItemCount; //EPOS 

            $finalTotal = $finalTaxTotal + $finalAmountTotal;

            $whereSalesCond = "PK_SALES_ID ='" . $lastId . "'";
            $updateSalesArr = array();
            $updateSalesArr['BASIC_AMOUNT'] = $finalAmountTotal;
            $updateSalesArr['TAX_AMOUNT'] = $finalTaxTotal;
            $updateSalesArr['FINAL_AMOUNT'] = $finalTotal;


            $updateSalesData = update_rec(CENTRALISEEPOSSALES, $updateSalesArr, $whereSalesCond);
            
             if ($finalTotal > 0) {
                 $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;
                $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT FROM " . STUDENTMASTER . " WHERE FK_EPOS_CUST_ID ='" . $onlinecustomerId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                $resStudentData = fetch_rec_query($sqlStudentData); // 
                if (count($resStudentData) > 0) {
                    $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                    $billedAmt = $resStudentData[0]['BILL_AMT'];
                    $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                }


                $closingBalAmt = (floatval($billedAmt) + floatval($finalTotal)) - floatval($receiptAmt);
                $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR"; // IF CB IS IN - THEN CR OF STUDENT ELSE DR OF STUDENT


                $updateStudentArr = ARRAY();
                $updateStudentArr['BILL_AMT'] = floatval($finalTotal) + floatval($billedAmt);
                $updateStudentArr['BILL_TYPE'] = 'DR';
                $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
                $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;
                $updateStudentArr['CHANGED_BY'] = $userId;
                $updateStudentArr['CHANGED_BY_DATE'] = time();
                $whereStudCond = "FK_EPOS_CUST_ID='" . $onlinecustomerId  . "'";
                $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                
            }
        }
        // START UPDATIND DATA INTO CAL TABLE AND GIVE SUCCESS AFTER UpDATE
        $whereUpdate = "FK_STUDENT_ID='" . $studentFkId . "' AND '" . date('Y-m-d', strtotime($billDate)) . "' BETWEEN ALLOCATION_START_DATE AND ALLOCATION_END_DATE AND FK_ITEM_ID ='" . $resultGetCallData[0]['FK_ITEM_ID'] . "'";

        $updateqtyData = update_rec(CALALLOCATIONTRANS, $updateArr, $whereUpdate);
    } else if ($spaceId == $fnbSpaceId) { // FNB CASE STARTED
        //
        // $calculatedBillQty = $totalItemQty;
        $InsertArr = array();
        $InsertArr['FK_SPACE_ID'] = $spaceId; //Laundry : 101 FNB : 100
        $InsertArr['FK_CLIENT_ID'] = $clientId;
        $InsertArr['FK_ORG_ID'] = $orgId;
        $InsertArr['VOUCHER_TYPE'] = 'SALES';
        $InsertArr['LOCATION'] = $location;
        $InsertArr['BILL_NO'] = $billNo;
        $InsertArr['BILL_DATE'] = date('Y-m-d', strtotime($billDate));
        //$InsertArr['MEMBERSHIP_NUMBER'] = $studentId; //STU000011 COMMENTED BY KAUSHA SHAH ON 15-09-2018 FOR CONSIDERING ONLINE CUSTOMER ID INSTEAD OF CUSTOMER ID
        $InsertArr['MEMBERSHIP_NUMBER'] = $onlinecustomerId;
        $InsertArr['STUDENT_NAME'] = $studentName;
        $InsertArr['BASIC_AMOUNT'] = 0;
        $InsertArr['TAX_AMOUNT'] = 0;
        $InsertArr['DISCOUNT_AMOUNT'] = $discountAmount;
        $InsertArr['DISCOUNT_TYPE'] = $discountPer;
        $InsertArr['ROUND_OFF'] = $roundOff;
        $InsertArr['FINAL_AMOUNT'] = 0;

        $InsertArr['BACKUP_BASIC_AMOUNT'] = $basicAmount;
        $InsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;
        $InsertArr['BACKUP_DISCOUNT_AMOUNT'] = $discountAmount;
        $InsertArr['BACKUP_DISCOUNT_TYPE'] = $discountPer;
        $InsertArr['BACKUP_ROUND_OFF'] = $roundOff;
        $InsertArr['BACKUP_FINAL_AMOUNT'] = $finalAmount;
        $InsertArr['NARRATION'] = $narration;
        $insertedId = insert_rec(CENTRALISEEPOSSALES, $InsertArr);
        $lastId = $insertedId['lastInsertedId'];
        if ($insertedId) {

            $finalItemCount = $finalAmountTotal = $finalTaxTotal = $finalTotal = 0;
            // START ITEM ADD INTO CENTRALISE_EPOS_SALES_ITEMS  
            foreach ($itemsArr as $keyItem => $valueItem) {

                $billNo = $valueItem['billNo'];
                $itemId = $valueItem['itemId'];
                $itemName = $valueItem['itemName'];
                $itemRate = $valueItem['itemRate'];
                $itemQty = $valueItem['itemQty'];
                $uomId = $valueItem['uomId'];
                $uomName = $valueItem['uomName'];
                $itemAmount = $valueItem['itemAmount'];

                $itemInsertArr = array();
                $itemInsertArr['FK_SALES_ID'] = $lastId;
                $itemInsertArr['FK_CLIENT_ID'] = $clientId;
                $itemInsertArr['FK_ORG_ID'] = $orgId;
                $itemInsertArr['BILL_NO'] = $billNo;
                $itemInsertArr['FK_ITEM_ID'] = $itemId;
                $itemInsertArr['ITEM_NAME'] = $itemName;
                $itemInsertArr['UOM_ID'] = $uomId;
                $itemInsertArr['UOM_NAME'] = $uomName;
                $itemInsertArr['BACKUP_ITEM_RATE'] = $itemRate;
                $itemInsertArr['BACKUP_ITEM_QTY'] = $itemQty;
                $itemInsertArr['BACKUP_ITEM_AMOUNT'] = $itemAmount;

                $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                $resItemData = fetch_rec_query($sqlItemData);

                if (isset($resItemData[0]['PK_ITEM_ID']) && $resItemData[0]['PK_ITEM_ID'] > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                    $itemInsertArr['ITEM_RATE'] = 0;
                    $itemInsertArr['ITEM_QTY'] = 0;
                    $itemInsertArr['ITEM_AMOUNT'] = 0;
                    $finalItemCount += $itemQty;
                } else { // ELSE REQUIRED FILED TO AS IT IS.
                    $itemInsertArr['ITEM_RATE'] = $itemRate;
                    $itemInsertArr['ITEM_QTY'] = $itemQty;
                    $itemInsertArr['ITEM_AMOUNT'] = $itemAmount;
                    $finalAmountTotal = $finalAmountTotal + ($itemRate * $itemQty);
                }
                insert_rec(CENTRALISEEPOSSALESITEMS, $itemInsertArr);
            }
            // END ITEM ADD INTO CENTRALISE_EPOS_SALES_ITEMS 
            // START TAX ADD INTO CENTRALISE_EPOS_SALES_TAX  
            foreach ($taxArr as $keyTax => $valueTax) {
                $billNo = $valueTax['billNo'];
                $itemId = $valueTax['itemId'];
                $taxId = $valueTax['taxId'];
                $taxName = $valueTax['taxName'];
                $taxPercentage = $valueTax['taxPercentage'];
                $taxAmount = $valueTax['taxAmount'];

                $taxInsertArr = array();
                $taxInsertArr['FK_SALES_ID'] = $lastId;
                $taxInsertArr['FK_CLIENT_ID'] = $clientId;
                $taxInsertArr['FK_ORG_ID'] = $orgId;
                $taxInsertArr['FK_ITEM_ID'] = $itemId;
                $taxInsertArr['BILL_NO'] = $billNo;
                $taxInsertArr['FK_TAX_ID'] = $taxId;
                $taxInsertArr['TAX_NAME'] = $taxName;
                $taxInsertArr['TAX_PERCENTAGE'] = $taxPercentage;
                $sqlItemData = "SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND EPOS_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' LIMIT 0,1";

                $resItemData = fetch_rec_query($sqlItemData);

                if (($resItemData[0]['PK_ITEM_ID']) > 0) { // IF ITEM FOUND THEN CHANGE REQUIRED FILED TO 0.    
                    $taxInsertArr['TAX_AMOUNT'] = 0;
                } else { // ELSE REQUIRED FILED TO AS IT IS.
                    $taxInsertArr['TAX_AMOUNT'] = $taxAmount;
                    $finalTaxTotal = $finalTaxTotal + $taxAmount;
                }

                $taxInsertArr['BACKUP_TAX_AMOUNT'] = $taxAmount;

                insert_rec(CENTRALISEEPOSSALESTAX, $taxInsertArr);
            }
            // END TAX ADD INTO CENTRALISE_EPOS_SALES_TAX  
        }


        // $updateArr['USED_QTY'] = $finalItemCount; //EPOS FNB
        $updateArr['USED_QTY'] = $usedQty + $finalItemCount; //EPOS FNB

        $finalTotal = $finalTaxTotal + $finalAmountTotal;

        $whereSalesCond = "PK_SALES_ID ='" . $lastId . "'";
        $updateSalesArr = array();
        $updateSalesArr['BASIC_AMOUNT'] = $finalAmountTotal;
        $updateSalesArr['TAX_AMOUNT'] = $finalTaxTotal;
        $updateSalesArr['FINAL_AMOUNT'] = $finalTotal;
        $updateSalesData = update_rec(CENTRALISEEPOSSALES, $updateSalesArr, $whereSalesCond);
        
        if ($finalTotal > 0) {
                 $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;
                $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT FROM " . STUDENTMASTER . " WHERE FK_EPOS_CUST_ID ='" . $onlinecustomerId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                $resStudentData = fetch_rec_query($sqlStudentData); // 
                if (count($resStudentData) > 0) {
                    $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                    $billedAmt = $resStudentData[0]['BILL_AMT'];
                    $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                }


                $closingBalAmt = (floatval($billedAmt) + floatval($finalTotal)) - floatval($receiptAmt);
                $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR"; // IF CB IS IN - THEN CR OF STUDENT ELSE DR OF STUDENT


                $updateStudentArr = ARRAY();
                $updateStudentArr['BILL_AMT'] = floatval($finalTotal) + floatval($billedAmt);
                $updateStudentArr['BILL_TYPE'] = 'DR';
                $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
                $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;
                $updateStudentArr['CHANGED_BY'] = $userId;
                $updateStudentArr['CHANGED_BY_DATE'] = time();
                $whereStudCond = "FK_EPOS_CUST_ID='" . $onlinecustomerId  . "'";
                $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                
            }


        // START UPDATIND DATA INTO CAL TABLE AND GIVE SUCCESS AFTER UpDATE
        $whereUpdate = "FK_STUDENT_ID='" . $studentFkId . "' AND '" . date('Y-m-d', strtotime($billDate)) . "' BETWEEN ALLOCATION_START_DATE AND ALLOCATION_END_DATE AND FK_ITEM_ID ='" . $resultGetCallData[0]['FK_ITEM_ID'] . "'";

        $updateqtyData = update_rec(CALALLOCATIONTRANS, $updateArr, $whereUpdate);
    }

    if ($updateqtyData) {
        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => UPDATEFAIL . " cal allocation table.");
        http_response_code(400);
    }
    return $result;
}

//END FUNCTION FOR ADD SALES TRANSACTION DATA
//END BY DEVIKA 18.8.2018.
// ADDED BY KAUSHA SHAH ON 20-08-2018 START TO PREPARE DATA FOR TALLY
// GET DATA FOR EPOS SALES
function prepareDataForTally($postData) {

    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $tallyFlagId = isset($postData['postData']['tallyFlagId']) ? $postData['postData']['tallyFlagId'] : "";
    $tallyIntegrationFlag = isset($postData['postData']['tallyIntegrationFlag']) ? $postData['postData']['tallyIntegrationFlag'] : "";

    if ($tallyIntegrationFlag == 1) {

        $data = array();

        // PREPARE DATA FOR EPOS SALES
        $eposSales = prepareJsonOfEposSales($userId, $clientId, $tallyFlagId);
        //echo '<pre>' ; print_r($eposSales) ; 
        $eposCredit = prepareJsonOfEposCreditNote($userId, $clientId, $tallyFlagId);
        //echo '<pre>' ; print_r($eposCredit) ; 
        $eposReceipt = prepareJsonOfEposReceipt($userId, $clientId, $tallyFlagId);
        //echo '<pre>' ; print_r($eposReceipt) ; 
        $customReceipt = prepareJsonOfCustomReceipt($userId, $clientId, $tallyFlagId);
        //echo '<pre>' ; print_r($customReceipt) ; 
        $customInvoice = prepareJsonOfCustomInvoice($userId, $clientId, $tallyFlagId);
        //echo '<pre>' ; print_r($customInvoice) ; 
        $customCredit = prepareJsonOfCustomCredit($userId, $clientId, $tallyFlagId);
        //echo '<pre>' ; print_r($customCredit) ;

        if ($eposSales == NODATAFORTALLYSYNC && $eposCredit == NODATAFORTALLYSYNC && $eposReceipt == NODATAFORTALLYSYNC && $customReceipt == NODATAFORTALLYSYNC && $customInvoice == NODATAFORTALLYSYNC && $customCredit == NODATAFORTALLYSYNC) {
            //if($eposCredit == NODATAFORTALLYSYNC) {
            $result = array("status" => FAILMSG, "data" => NODATAFORTALLYSYNC);
            http_response_code(400);
        } else {

            $msg = '';
            $msgStingAppend = ' , ';

            if ($eposSales != NODATAFORTALLYSYNC) {
                $eposSalesMsg = count($eposSales) . ' Epos Sales ';
                $msg .= ($msg != '') ? $msgStingAppend . $eposSalesMsg : $eposSalesMsg;
            }
            if ($eposCredit != NODATAFORTALLYSYNC) {
                $eposCreditMsg = count($eposCredit) . ' Epos CN ';
                $msg .= ($msg != '') ? $msgStingAppend . $eposCreditMsg : $eposCreditMsg;
            }
            if ($eposReceipt != NODATAFORTALLYSYNC) {
                $eposReceiptMsg = count($eposReceipt) . ' Epos receipt ';
                $msg .= ($msg != '') ? $msgStingAppend . $eposReceiptMsg : $eposReceiptMsg;
            }
            if ($customReceipt != NODATAFORTALLYSYNC) {
                $customReceiptMsg = count($customReceipt) . ' Custom receipt ';
                $msg .= ($msg != '') ? $msgStingAppend . $customReceiptMsg : $customReceiptMsg;
            }
            if ($customInvoice != NODATAFORTALLYSYNC) {
                $customInvoiceMsg = count($customInvoice) . ' Custom Invoice ';
                $msg .= ($msg != '') ? $msgStingAppend . $customInvoiceMsg : $customInvoiceMsg;
            }
            if ($customCredit != NODATAFORTALLYSYNC) {
                $customCreditMsg = count($customCredit) . ' Custom CN ';
                $msg .= ($msg != '') ? $msgStingAppend . $customCreditMsg : $customCreditMsg;
            }
            $msg .= 'sync to tally.';
            $result = array("status" => SCS, "data" => $msg);

            http_response_code(200);
        }
    } else {
        $result = array("status" => INTEGRATION_NOT_ALLOW);
        http_response_code(400);
    }
    return $result;
}

function prepareJsonOfEposSales($userId, $clientId, $tallyFlagId) {


    $sqlEposSales = fetch_rec_query("SELECT CS.PK_SALES_ID, CS.MEMBERSHIP_NUMBER, CS.STUDENT_NAME, CS.BILL_DATE, CS.BILL_NO, CS.FINAL_AMOUNT, CS.ROUND_OFF, CS.NARRATION, SM.FK_EPOS_CUST_ID,SM.MEMBERSHIP_NUMBER FROM " . CENTRALISEEPOSSALES . " CS INNER JOIN ".STUDENTMASTER." SM ON SM.FK_EPOS_CUST_ID=CS.MEMBERSHIP_NUMBER AND SM.DELETE_FLAG=0 WHERE CS.DELETE_FLAG=0 AND CS.LOCATION=1 AND CS.TALLY_SYNC_FLAG = 0 AND CS.FINAL_AMOUNT > 0 ORDER BY CS.PK_SALES_ID ASC ");

    if (count($sqlEposSales) > 0) {

        $finalArr = array();

        foreach ($sqlEposSales as $sqlEposSale) {

            $data = array();

            $salesId = $sqlEposSale['PK_SALES_ID'];

            $data['systemCustomerId'] = $sqlEposSale['MEMBERSHIP_NUMBER']; // CPMMENTED BY KAUSHA SHAH ON 15-09-2018 // UNCOMMENTED BY SATISH KARENA ON 01-10-2018
           // $data['systemCustomerId'] = $sqlEposSale['FK_EPOS_CUST_ID'];
            $data['systemCustomerName'] = $sqlEposSale['STUDENT_NAME'];
            $data['billDate'] = $sqlEposSale['BILL_DATE'];
            $data['billNo'] = $sqlEposSale['BILL_NO'];
            $data['totalAmount'] = $sqlEposSale['FINAL_AMOUNT'];
            $data['placeOfSupply'] = '';
            $data['roundOfAmount'] = $sqlEposSale['ROUND_OFF'];
            $data['extraChargeAmount'] = 0;
            $data['billRemark'] = $sqlEposSale['NARRATION'];

            // GET SALES ITEM DATA
            $sqlEposItems = fetch_rec_query("SELECT FK_ITEM_ID,ITEM_NAME,ITEM_RATE,ITEM_QTY,ITEM_AMOUNT,UOM_NAME,UOM_ID FROM " . CENTRALISEEPOSSALESITEMS . " WHERE FK_SALES_ID='" . $salesId . "' AND ITEM_AMOUNT > 0 AND ITEM_QTY > 0");
            if (count($sqlEposItems) > 0) {

                for ($i = 0; $i < count($sqlEposItems); $i++) {

                    $data['item'][$i]['systemItemId'] = $sqlEposItems[$i]['FK_ITEM_ID'];
                    $data['item'][$i]['systemItemName'] = $sqlEposItems[$i]['ITEM_NAME'];
                    $data['item'][$i]['itemRate'] = $sqlEposItems[$i]['ITEM_RATE'];
                    $data['item'][$i]['itemQty'] = $sqlEposItems[$i]['ITEM_QTY'];
                    $data['item'][$i]['itemAmount'] = $sqlEposItems[$i]['ITEM_AMOUNT'];
                    //$data['item'][$i]['itemUOM'] = $sqlEposItems[$i]['UOM_NAME'] ;
                    //$data['item'][$i]['itemUOMId'] = $sqlEposItems[$i]['UOM_ID'] ;
                }
            }

            // GET SALES TAX DATA
            $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,TAX_NAME,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSSALESTAX . " WHERE FK_SALES_ID='" . $salesId . "' GROUP BY TAX_NAME");
            if (count($sqlEposTax) > 0) {
                for ($j = 0; $j < count($sqlEposTax); $j++) {
                    $data['tax'][$j]['systemTaxId'] = $sqlEposTax[$j]['FK_TAX_ID'];
                    $data['tax'][$j]['systemTaxName'] = $sqlEposTax[$j]['TAX_NAME'];
                    $data['tax'][$j]['taxAmount'] = $sqlEposTax[$j]['TAX_AMOUNT'];
                }
            }

            $jsonData = json_encode($data);
            //$finalArr[] = array('json'=>$jsonData,'PKID'=>$salesId) ;
            // INSERT IN tally_transaction_staging TABLE
            $insertArr = array();

            $insertArr['FK_COMP_STORE_ID'] = $clientId;
            $insertArr['FK_PROJECT_ID'] = $tallyFlagId;
            $insertArr['FUNCATION_NAME'] = 'createSalesItemGL';
            $insertArr['JSON_DATA'] = $jsonData;
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();
            $insert = insert_rec(TALLYTRANSACTIONSTAGING, $insertArr);
            if ($insert) {

                //UPDATE centralise_epos_sales TABLE
                $updateArr = array();

                $updateArr['TALLY_SYNC_FLAG'] = 1;
                $updateArr['TALLY_SYNC_TIME'] = time();
                $updateArr['FK_TRANS_STAGING_ID'] = $insert['lastInsertedId'];

                update_rec(CENTRALISEEPOSSALES, $updateArr, 'PK_SALES_ID = "' . $salesId . '"');
            }

            $finalArr[] = 'updated';
        }
    } else {
        $finalArr = NODATAFORTALLYSYNC;
    }

    return $finalArr;
}

function prepareJsonOfEposCreditNote($userId, $clientId, $tallyFlagId) {


    $sqlEposCrNotes = fetch_rec_query("SELECT CM.PK_CREDIT_NOTE_ID,CM.MEMBERSHIP_NUMBER,CM.STUDENT_NAME,CM.BILL_NO,CM.BILL_DATE,CM.REFERENCE_NO, CM.FINAL_AMOUNT,CM.NARRATION, SM.FK_EPOS_CUST_ID,SM.MEMBERSHIP_NUMBER FROM " . CENTRALISEEPOSCREDITNOTE . " CM INNER JOIN ".STUDENTMASTER." SM ON SM.FK_EPOS_CUST_ID=CM.MEMBERSHIP_NUMBER AND SM.DELETE_FLAG=0 WHERE CM.DELETE_FLAG=0 AND CM.LOCATION=1 AND CM.TALLY_SYNC_FLAG = 0 AND CM.FINAL_AMOUNT > 0 ORDER BY CM.PK_CREDIT_NOTE_ID ASC");
    if (count($sqlEposCrNotes) > 0) {

        $finalArr = array();

        foreach ($sqlEposCrNotes as $sqlEposCrNote) {

            $data = array();
            $crNoteId = $sqlEposCrNote['PK_CREDIT_NOTE_ID'];

              $data['systemCustomerId'] = $sqlEposCrNote['MEMBERSHIP_NUMBER']; // CPMMENTED BY KAUSHA SHAH ON 15-09-2018 // UNCOMMENTED BY SATISH KARENA ON 01-10-2018
	   // $data['systemCustomerId'] = $sqlEposCrNote['FK_EPOS_CUST_ID'] ;
            $data['systemCustomerName'] = $sqlEposCrNote['STUDENT_NAME'];
            $data['returnDate'] = $sqlEposCrNote['BILL_DATE'];
            $data['returnId'] = $sqlEposCrNote['BILL_NO'];
            $data['returnBillNo'] = $sqlEposCrNote['REFERENCE_NO'];
            $data['totalAmount'] = $sqlEposCrNote['FINAL_AMOUNT'];
            $data['placeOfSupply'] = '';
            $data['returnRemark'] = $sqlEposCrNote['NARRATION'];

            //CREDIT NOTE ITEMS
            $sqlEposItems = fetch_rec_query("SELECT PK_CREDIT_NOTE_ITEM_ID,FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,UOM_ID,ITEM_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTEITEMS . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' AND ITEM_AMOUNT > 0 AND ITEM_QTY > 0");
            if (count($sqlEposItems) > 0) {

                for ($i = 0; $i < count($sqlEposItems); $i++) {

                    $data['item'][$i]['systemItemId'] = $sqlEposItems[$i]['FK_ITEM_ID'];
                    $data['item'][$i]['systemItemName'] = $sqlEposItems[$i]['ITEM_NAME'];
                    $data['item'][$i]['itemRate'] = $sqlEposItems[$i]['ITEM_NAME'];
                    $data['item'][$i]['itemQty'] = $sqlEposItems[$i]['ITEM_QTY'];
                    $data['item'][$i]['itemAmount'] = $sqlEposItems[$i]['ITEM_AMOUNT'];
                    $data['item'][$i]['itemUOM'] = $sqlEposItems[$i]['UOM_NAME'];
                    $data['item'][$i]['itemUOMId'] = $sqlEposItems[$i]['UOM_ID'];
                }
            }

            //CREDIT NOTE tax
            $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,TAX_NAME,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTETAX . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' GROUP BY TAX_NAME");
            if (count($sqlEposTax) > 0) {
                for ($j = 0; $j < count($sqlEposTax); $j++) {
                    $data['tax'][$j]['systemTaxId'] = $sqlEposTax[$j]['FK_TAX_ID'];
                    $data['tax'][$j]['systemTaxName'] = $sqlEposTax[$j]['TAX_NAME'];
                    $data['tax'][$j]['taxAmount'] = $sqlEposTax[$j]['TAX_AMOUNT'];
                }
            }

            $jsonData = json_encode($data);
            //$finalArr[] = array('json'=>$jsonData,'PKID'=>$crNoteId) ;
            // INSERT IN tally_transaction_staging TABLE
            $insertArr = array();

            $insertArr['FK_COMP_STORE_ID'] = $clientId;
            $insertArr['FK_PROJECT_ID'] = $tallyFlagId;
            $insertArr['FUNCATION_NAME'] = 'createCreditNoteGL';
            $insertArr['JSON_DATA'] = $jsonData;
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();

            $insert = insert_rec(TALLYTRANSACTIONSTAGING, $insertArr);
            if ($insert) {

                //UPDATE centralise_epos_credit_note TABLE
                $updateArr = array();

                $updateArr['TALLY_SYNC_FLAG'] = 1;
                $updateArr['TALLY_SYNC_TIME'] = time();
                $updateArr['FK_TRANS_STAGING_ID'] = $insert['lastInsertedId'];

                update_rec(CENTRALISEEPOSCREDITNOTE, $updateArr, 'PK_CREDIT_NOTE_ID = "' . $crNoteId . '"');
            }

            $finalArr[] = 'updated';
        }
    } else {
        $finalArr = NODATAFORTALLYSYNC;
    }

    return $finalArr;
}

function prepareJsonOfEposReceipt($userId, $clientId, $tallyFlagId) {

    $sqlEposReceipts = fetch_rec_query("SELECT CR.PK_RECEIPT_ID,CR.MEMBERSHIP_NUMBER, CR.STUDENT_NAME, CR.BILL_DATE,CR.PAYMENT_AMOUNT,CR.PAYMENT_MODE,CR.PAYMENT_ID,CR.BILL_NO,CR.PAYMENT_MODE_ID,CR.NARRATION,SM.FK_EPOS_CUST_ID,SM.MEMBERSHIP_NUMBER FROM " . CENTRALISEEPOSRECEIPT . " CR INNER JOIN ".STUDENTMASTER." SM ON SM.FK_EPOS_CUST_ID=CR.MEMBERSHIP_NUMBER AND SM.DELETE_FLAG=0 WHERE CR.DELETE_FLAG=0 AND CR.LOCATION=1  AND CR.TALLY_SYNC_FLAG = 0 AND CR.PAYMENT_AMOUNT > 0 ORDER BY CR.PK_RECEIPT_ID ASC");
    if (count($sqlEposReceipts) > 0) {

        $finalArr = array();

        foreach ($sqlEposReceipts as $sqlEposReceipt) {

            $data = array();
            $receiptId = $sqlEposReceipt['PK_RECEIPT_ID'];

            $data['systemCustomerId'] = $sqlEposReceipt['MEMBERSHIP_NUMBER']; // CPMMENTED BY KAUSHA SHAH ON 15-09-2018 // UNCOMMENTED BY SATISH KARENA ON 01-10-2018
	  //  $data['systemCustomerId'] = $sqlEposReceipt['FK_EPOS_CUST_ID'];
            $data['systemCustomerName'] = $sqlEposReceipt['STUDENT_NAME'];
            $data['payDate'] = $sqlEposReceipt['BILL_DATE'];
            $data['paymentId'] = $sqlEposReceipt['PAYMENT_ID'];
            $data['totalAmount'] = $sqlEposReceipt['PAYMENT_AMOUNT'];
            $data['payModeName'] = $sqlEposReceipt['PAYMENT_MODE'];
            $data['billNo'] = $sqlEposReceipt['BILL_NO'];
            $data['payModeId'] = $sqlEposReceipt['PAYMENT_MODE_ID'];
            $data['payRemark'] = $sqlEposReceipt['NARRATION'];

            $jsonData = json_encode($data);
            //$finalArr[] = array('json'=>$jsonData,'PKID'=>$receiptId) ;
            // INSERT IN tally_transaction_staging TABLE
            $insertArr = array();

            $insertArr['FK_COMP_STORE_ID'] = $clientId;
            $insertArr['FK_PROJECT_ID'] = $tallyFlagId;
            $insertArr['FUNCATION_NAME'] = 'createReceipt';
            $insertArr['JSON_DATA'] = $jsonData;
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();
            $insert = insert_rec(TALLYTRANSACTIONSTAGING, $insertArr);
            if ($insert) {

                //UPDATE centralise_epos_credit_note TABLE
                $updateArr = array();

                $updateArr['TALLY_SYNC_FLAG'] = 1;
                $updateArr['TALLY_SYNC_TIME'] = time();
                $updateArr['FK_TRANS_STAGING_ID'] = $insert['lastInsertedId'];

                update_rec(CENTRALISEEPOSRECEIPT, $updateArr, 'PK_RECEIPT_ID = "' . $receiptId . '"');
            }

            $finalArr[] = 'updated';
        }
    } else {
        $finalArr = NODATAFORTALLYSYNC;
    }

    return $finalArr;
}

function prepareJsonOfCustomReceipt($userId, $clientId, $tallyFlagId) {

    $sqlCustomReceipts = fetch_rec_query("SELECT PK_RECEIPT_ID,FK_STUD_ID,MEMBERSHIP_NUMBER,STUDENT_NAME,NARRATION,RECEIPT_DATE,PAYMENT_MODE,BANK_NAME,CHEQUE_NO,PAYMENT_AMOUNT,NARRATION,INVOICE_ID,PAYMENT_MODE_ID FROM " . CENTRALISECUSTOMRECEIPT . " WHERE DELETE_FLAG=0 AND TALLY_SYNC_FLAG = 0 AND PAYMENT_AMOUNT > 0 ORDER BY PK_RECEIPT_ID ASC ");
    if (count($sqlCustomReceipts) > 0) {

        $finalArr = array();

        foreach ($sqlCustomReceipts as $sqlCustomReceipt) {

            $data = array();
            $receiptId = $sqlCustomReceipt['PK_RECEIPT_ID'];

            $data['systemCustomerId'] = $sqlCustomReceipt['MEMBERSHIP_NUMBER'];
            $data['systemCustomerName'] = $sqlCustomReceipt['STUDENT_NAME'];
            $data['payDate'] = $sqlCustomReceipt['RECEIPT_DATE'];
            $data['paymentId'] = $receiptId;
            $data['totalAmount'] = $sqlCustomReceipt['PAYMENT_AMOUNT'];
            $data['payModeName'] = $sqlCustomReceipt['PAYMENT_MODE'];
            $data['billNo'] = $sqlCustomReceipt['INVOICE_ID'];
            $data['payModeId'] = $sqlCustomReceipt['PAYMENT_MODE_ID'];
            $data['payRemark'] = $sqlCustomReceipt['NARRATION'];

            $jsonData = json_encode($data);
            //$finalArr[] = array('json'=>$jsonData,'PKID'=>$receiptId) ;
            // INSERT IN tally_transaction_staging TABLE
            $insertArr = array();

            $insertArr['FK_COMP_STORE_ID'] = $clientId;
            $insertArr['FK_PROJECT_ID'] = $tallyFlagId;
            $insertArr['FUNCATION_NAME'] = 'createReceipt';
            $insertArr['JSON_DATA'] = $jsonData;
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();
            $insert = insert_rec(TALLYTRANSACTIONSTAGING, $insertArr);
            if ($insert) {

                //UPDATE centralise_epos_credit_note TABLE
                $updateArr = array();

                $updateArr['TALLY_SYNC_FLAG'] = 1;
                $updateArr['TALLY_SYNC_TIME'] = time();
                $updateArr['FK_TRANS_STAGING_ID'] = $insert['lastInsertedId'];

                update_rec(CENTRALISECUSTOMRECEIPT, $updateArr, 'PK_RECEIPT_ID = "' . $receiptId . '"');
            }

            $finalArr[] = 'updated';
        }
    } else {
        $finalArr = NODATAFORTALLYSYNC;
    }

    return $finalArr;
}

function prepareJsonOfCustomInvoice($userId, $clientId, $tallyFlagId) {

//    $sqlCustomInvoices = fetch_rec_query("SELECT IM.PK_INVOICE_ID,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.TOTAL_CHARGES,IM.NARRATION, IF(IM.FK_STUDENT_ID=0, IM.FK_CUST_ID, IM.FK_STUDENT_ID) AS MEMBERSHIP_NUMBER,
//IF(IM.FK_STUDENT_ID=0,(SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID=IM.FK_CUST_ID), (SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) FROM " . STUDENTMASTER . " WHERE PK_STUD_ID=IM.FK_STUDENT_ID)) AS STUDENT_NAME FROM " . INVOICEMASTER . " IM WHERE IM.DELETE_FLAG=0 AND IM.TALLY_SYNC_FLAG = 0 AND TOTAL_CHARGES > 0 ORDER BY IM.PK_INVOICE_ID ASC ");
    $sqlCustomInvoices = fetch_rec_query("SELECT IM.PK_INVOICE_ID,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.TOTAL_CHARGES,IM.NARRATION, IF(IM.FK_STUDENT_ID=0,(SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID=IM.FK_CUST_ID), (SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) FROM " . STUDENTMASTER . " WHERE PK_STUD_ID=IM.FK_STUDENT_ID)) AS STUDENT_NAME, IF(IM.FK_STUDENT_ID=0,(SELECT  C_CUST_ID FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID=IM.FK_CUST_ID), (SELECT MEMBERSHIP_NUMBER FROM " . STUDENTMASTER . " WHERE PK_STUD_ID=IM.FK_STUDENT_ID)) AS MEMBERSHIP_NUMBER FROM " . INVOICEMASTER . " IM WHERE IM.DELETE_FLAG=0 AND IM.TALLY_SYNC_FLAG = 0 AND TOTAL_CHARGES > 0 ORDER BY IM.PK_INVOICE_ID ASC");
    if (count($sqlCustomInvoices) > 0) {

        $finalArr = array();

        foreach ($sqlCustomInvoices as $sqlCustomInvoice) {

            $data = array();
            $invoiceId = $sqlCustomInvoice['PK_INVOICE_ID'];
            $C_INVOICE_ID = $sqlCustomInvoice['C_INVOICE_ID'];

            $data['systemCustomerId'] = $sqlCustomInvoice['MEMBERSHIP_NUMBER'];
            $data['systemCustomerName'] = $sqlCustomInvoice['STUDENT_NAME'];
            $data['billDate'] = $sqlCustomInvoice['INVOICE_DATE'];
            $data['billNo'] = $sqlCustomInvoice['C_INVOICE_ID'];
            $data['totalAmount'] = $sqlCustomInvoice['TOTAL_CHARGES'];
            $data['placeOfSupply'] = '';
            $data['roundOfAmount'] = 0;
            $data['extraChargeAmount'] = 0;
            $data['billRemark'] = $sqlCustomInvoice['NARRATION'];

            // GET ITEMS DATA
            $sqlInvoiceItems = fetch_rec_query("SELECT  REF_ITEM_ID,ITEM_NAME, QTY_OF_SESSION, ITEM_TOTAL_PRICE, ITEM_BASE_PRICE FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $C_INVOICE_ID . "' AND DELETE_FLAG=0 AND ITEM_BASE_PRICE > 0 AND QTY_OF_SESSION > 0");
            if (count($sqlInvoiceItems) > 0) {
                for ($i = 0; $i < count($sqlInvoiceItems); $i++) {

                    $itemRate = $sqlInvoiceItems[$i]['ITEM_BASE_PRICE'];
                    $itemQty = $sqlInvoiceItems[$i]['QTY_OF_SESSION'];

                    $data['item'][$i]['systemItemId'] = $sqlInvoiceItems[$i]['REF_ITEM_ID'];
                    $data['item'][$i]['systemItemName'] = $sqlInvoiceItems[$i]['ITEM_NAME'];
                    $data['item'][$i]['itemRate'] = $itemRate;
                    $data['item'][$i]['itemQty'] = $itemQty;
                    $data['item'][$i]['itemAmount'] = ($itemRate * $itemQty);
                }
            }

            // GET TAX DATA
            $sqlInvoiceTax = fetch_rec_query("SELECT QTY_OF_SESSION,ITEM_BASE_PRICE,TAX_CGST,TAX_SGST,TAX_IGST FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $C_INVOICE_ID . "' AND DELETE_FLAG=0");
            if (count($sqlInvoiceTax) > 0) {

                $cgstData = fetch_rec_query("SELECT TYPE,VALUE FROM " . LOOKUPMASTER . " WHERE PK_LOOKUP_ID = '46'");
                $sgstData = fetch_rec_query("SELECT TYPE,VALUE FROM " . LOOKUPMASTER . " WHERE PK_LOOKUP_ID = '47'");
                $igstData = fetch_rec_query("SELECT TYPE,VALUE FROM " . LOOKUPMASTER . " WHERE PK_LOOKUP_ID = '48'");


                $cgstAmnt = $sgstAmnt = $igstAmnt = 0;
                for ($j = 0; $j < count($sqlInvoiceTax); $j++) {

                    $itemBasePrice = $sqlInvoiceTax[$j]['ITEM_BASE_PRICE'];
                    $itemQty = $sqlInvoiceTax[$j]['QTY_OF_SESSION'];
                    $itemPrice = $itemBasePrice * $itemQty;


                    if ($sqlInvoiceTax[$j]['TAX_CGST'] != 0) {

                        $cgstAmnt = $cgstAmnt + (($itemPrice * $sqlInvoiceTax[$j]['TAX_CGST']) / 100);
                    }

                    if ($sqlInvoiceTax[$j]['TAX_SGST'] != 0) {

                        $sgstAmnt = $sgstAmnt + (($itemPrice * $sqlInvoiceTax[$j]['TAX_SGST']) / 100);
                    }

                    if ($sqlInvoiceTax[$j]['TAX_IGST'] != 0) {

                        $igstAmnt = $igstAmnt + (($itemPrice * $sqlInvoiceTax[$j]['TAX_IGST']) / 100);
                    }
                }

                $taxCount = 0;

                if ($cgstAmnt > 0) {

                    $data['tax'][$taxCount]['systemTaxId'] = $cgstData[0]['VALUE'];
                    $data['tax'][$taxCount]['systemTaxName'] = $cgstData[0]['TYPE'];
                    $data['tax'][$taxCount]['taxAmount'] = $cgstAmnt;
                    $taxCount++;
                }

                if ($sgstAmnt > 0) {

                    $data['tax'][$taxCount]['systemTaxId'] = $sgstData[0]['VALUE'];
                    $data['tax'][$taxCount]['systemTaxName'] = $sgstData[0]['TYPE'];
                    $data['tax'][$taxCount]['taxAmount'] = $sgstAmnt;
                    $taxCount++;
                }

                if ($igstAmnt > 0) {

                    $data['tax'][$taxCount]['systemTaxId'] = $igstData[0]['VALUE'];
                    $data['tax'][$taxCount]['systemTaxName'] = $igstData[0]['TYPE'];
                    $data['tax'][$taxCount]['taxAmount'] = $igstAmnt;
                    $taxCount++;
                }
            }

            $jsonData = json_encode($data);
            //$finalArr[] = array('json'=>$jsonData,'PKID'=>$invoiceId) ;
            // INSERT IN tally_transaction_staging TABLE
            $insertArr = array();

            $insertArr['FK_COMP_STORE_ID'] = $clientId;
            $insertArr['FK_PROJECT_ID'] = $tallyFlagId;
            $insertArr['FUNCATION_NAME'] = 'createSalesItemGL';
            $insertArr['JSON_DATA'] = $jsonData;
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();
            $insert = insert_rec(TALLYTRANSACTIONSTAGING, $insertArr);
            if ($insert) {

                //UPDATE centralise_epos_credit_note TABLE
                $updateArr = array();

                $updateArr['TALLY_SYNC_FLAG'] = 1;
                $updateArr['TALLY_SYNC_TIME'] = time();
                $updateArr['FK_TRANS_STAGING_ID'] = $insert['lastInsertedId'];

                update_rec(INVOICEMASTER, $updateArr, 'PK_INVOICE_ID = "' . $invoiceId . '"');
            }

            $finalArr[] = 'updated';
        }
    } else {
        $finalArr = NODATAFORTALLYSYNC;
    }

    return $finalArr;
}

function prepareJsonOfCustomCredit($userId, $clientId, $tallyFlagId) {

    $sqlCustomInvoices = fetch_rec_query("SELECT IM.PK_CREDIT_ID,IM.FK_INVOICE_ID,IF(IM.FK_STUDENT_ID=0, (SELECT  C_CUST_ID FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID=IM.FK_CUST_ID), (SELECT MEMBERSHIP_NUMBER FROM " . STUDENTMASTER . " WHERE PK_STUD_ID=IM.FK_STUDENT_ID)) AS MEMBERSHIP_NUMBER,
IF(IM.FK_STUDENT_ID=0,(SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID=IM.FK_CUST_ID), (SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) FROM " . STUDENTMASTER . " WHERE PK_STUD_ID=IM.FK_STUDENT_ID)) AS STUDENT_NAME,IM.INVOICE_DATE,IM.TOTAL_CHARGES,IM.NARRATION FROM " . CREDITMASTER . "  IM WHERE IM.DELETE_FLAG=0 AND IM.TALLY_SYNC_FLAG = 0 AND TOTAL_CHARGES > 0 ORDER BY IM.PK_CREDIT_ID ASC ");
    if (count($sqlCustomInvoices) > 0) {

        $finalArr = array();

        foreach ($sqlCustomInvoices as $sqlCustomInvoice) {

            $data = array();
            $invoiceId = $sqlCustomInvoice['PK_CREDIT_ID'];
            $C_INVOICE_ID = $sqlCustomInvoice['FK_INVOICE_ID'];

            $data['systemCustomerId'] = $sqlCustomInvoice['MEMBERSHIP_NUMBER'];
            $data['systemCustomerName'] = $sqlCustomInvoice['STUDENT_NAME'];
            $data['returnDate'] = $sqlCustomInvoice['INVOICE_DATE'];
            $data['returnId'] = $sqlCustomInvoice['PK_CREDIT_ID'];
            $data['returnBillNo'] = $sqlCustomInvoice['FK_INVOICE_ID'];
            $data['totalAmount'] = $sqlCustomInvoice['TOTAL_CHARGES'];
            $data['placeOfSupply'] = '';
            $data['returnRemark'] = $sqlCustomInvoice['NARRATION'];


            // GET ITEMS DATA
            $sqlInvoiceItems = fetch_rec_query("SELECT  REF_ITEM_ID,ITEM_NAME, QTY, ITEM_BASIC_PRICE FROM " . CREDITTRANS . " WHERE FK_CREDIT_ID='" . $invoiceId . "' AND DELETE_FLAG=0 AND ITEM_BASIC_PRICE > 0 AND QTY > 0");
            if (count($sqlInvoiceItems) > 0) {
                for ($i = 0; $i < count($sqlInvoiceItems); $i++) {

                    $itemRate = $sqlInvoiceItems[$i]['ITEM_BASIC_PRICE'];
                    $itemQty = $sqlInvoiceItems[$i]['QTY'];

                    $data['item'][$i]['systemItemId'] = $sqlInvoiceItems[$i]['REF_ITEM_ID'];
                    $data['item'][$i]['systemItemName'] = $sqlInvoiceItems[$i]['ITEM_NAME'];
                    $data['item'][$i]['itemRate'] = $itemRate;
                    $data['item'][$i]['itemQty'] = $itemQty;
                    $data['item'][$i]['itemAmount'] = ($itemRate * $itemQty);
                }
            }

            // GET TAX DATA
            $sqlInvoiceTax = fetch_rec_query("SELECT QTY,ITEM_BASIC_PRICE,SGST,CGST,IGST FROM " . CREDITTRANS . " WHERE FK_CREDIT_ID='" . $invoiceId . "' AND DELETE_FLAG=0");
            if (count($sqlInvoiceTax) > 0) {

                $cgstData = fetch_rec_query("SELECT TYPE,VALUE FROM " . LOOKUPMASTER . " WHERE PK_LOOKUP_ID = '46'");
                $sgstData = fetch_rec_query("SELECT TYPE,VALUE FROM " . LOOKUPMASTER . " WHERE PK_LOOKUP_ID = '47'");
                $igstData = fetch_rec_query("SELECT TYPE,VALUE FROM " . LOOKUPMASTER . " WHERE PK_LOOKUP_ID = '48'");

                $cgstAmnt = $sgstAmnt = $igstAmnt = 0;
                for ($j = 0; $j < count($sqlInvoiceTax); $j++) {

                    $itemBasePrice = $sqlInvoiceTax[$j]['ITEM_BASIC_PRICE'];
                    $itemQty = $sqlInvoiceTax[$j]['QTY'];
                    $itemPrice = $itemBasePrice * $itemQty;


                    if ($sqlInvoiceTax[$j]['CGST'] != 0) {

                        $cgstAmnt = $cgstAmnt + (($itemPrice * $sqlInvoiceTax[$j]['CGST']) / 100);
                    }

                    if ($sqlInvoiceTax[$j]['SGST'] != 0) {

                        $sgstAmnt = $sgstAmnt + (($itemPrice * $sqlInvoiceTax[$j]['SGST']) / 100);
                    }

                    if ($sqlInvoiceTax[$j]['IGST'] != 0) {

                        $igstAmnt = $igstAmnt + (($itemPrice * $sqlInvoiceTax[$j]['IGST']) / 100);
                    }
                }

                $taxCount = 0;

                if ($cgstAmnt > 0) {

                    $data['tax'][$taxCount]['systemTaxId'] = $cgstData[0]['VALUE'];
                    $data['tax'][$taxCount]['systemTaxName'] = $cgstData[0]['TYPE'];
                    $data['tax'][$taxCount]['taxAmount'] = $cgstAmnt;
                    $taxCount++;
                }

                if ($sgstAmnt > 0) {

                    $data['tax'][$taxCount]['systemTaxId'] = $sgstData[0]['VALUE'];
                    $data['tax'][$taxCount]['systemTaxName'] = $sgstData[0]['TYPE'];
                    $data['tax'][$taxCount]['taxAmount'] = $sgstAmnt;
                    $taxCount++;
                }

                if ($igstAmnt > 0) {

                    $data['tax'][$taxCount]['systemTaxId'] = $igstData[0]['VALUE'];
                    $data['tax'][$taxCount]['systemTaxName'] = $igstData[0]['TYPE'];
                    $data['tax'][$taxCount]['taxAmount'] = $igstAmnt;
                    $taxCount++;
                }
            }

            $jsonData = json_encode($data);
            //$finalArr[] = array('json'=>$jsonData,'PKID'=>$invoiceId) ;
            // INSERT IN tally_transaction_staging TABLE
            $insertArr = array();

            $insertArr['FK_COMP_STORE_ID'] = $clientId;
            $insertArr['FK_PROJECT_ID'] = $tallyFlagId;
            $insertArr['FUNCATION_NAME'] = 'createCreditNoteGL';
            $insertArr['JSON_DATA'] = $jsonData;
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();
            $insert = insert_rec(TALLYTRANSACTIONSTAGING, $insertArr);
            if ($insert) {

                //UPDATE centralise_epos_credit_note TABLE
                $updateArr = array();

                $updateArr['TALLY_SYNC_FLAG'] = 1;
                $updateArr['TALLY_SYNC_TIME'] = time();
                $updateArr['FK_TRANS_STAGING_ID'] = $insert['lastInsertedId'];

                update_rec(CREDITMASTER, $updateArr, 'PK_CREDIT_ID = "' . $invoiceId . '"');
            }

            $finalArr[] = 'updated';
        }
    } else {
        $finalArr = NODATAFORTALLYSYNC;
    }

    return $finalArr;
}

// ADDED BY KAUSHA SHAH ON 20-08-2018 END
?>