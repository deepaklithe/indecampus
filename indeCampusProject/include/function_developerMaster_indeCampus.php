<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : DARSHAN PATEL                     */
/*   CREATION DATE : 02-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR CREATE MODULE
function createModule($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $moduleName = isset($postData['postData']['moduleName']) ? addslashes(trim($postData['postData']['moduleName'])) : "";

// CREATE MODULE
    $insertModule = array();
    $insertModule['MODULE_NAME'] = $moduleName;
    $insertModule['ORG_TYPE'] = '2';
    $insertModule['FK_CLIENT_ID'] = '1';
    $insertModule['CREATED_BY'] = $userId;
    $insertModule['CREATED_BY_DATE'] = time();
    $insertModule['DELETE_FLAG'] = 0;

    $insertModuleQuery = insert_rec(MODULEMASTER, $insertModule);
    if ($insertModuleQuery) {

        $finalJson = array("moduleId" => $insertModuleQuery['lastInsertedId']);
        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR EDIT MODULE
function updateModule($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $moduleName = isset($postData['postData']['moduleName']) ? addslashes(trim($postData['postData']['moduleName'])) : "";
    $moduleId = isset($postData['postData']['moduleId']) ? addslashes(trim($postData['postData']['moduleId'])) : "";

// UPDATE MODULE
    $updateModule = array();
    $updateModule['MODULE_NAME'] = $moduleName;
    $updateModule['CHANGED_BY'] = $userId;
    $updateModule['CHANGED_BY_DATE'] = time();
    $updateModule['DELETE_FLAG'] = 0;
//WHere
    $where = "PK_MODULE_ID = '" . $moduleId . "'";

    $updateModuleQuery = update_rec(MODULEMASTER, $updateModule, $where);
    if ($updateModuleQuery) {

        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => UPDATEQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR DELETE MODULE
function deleteModule($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $moduleId = isset($postData['postData']['moduleId']) ? addslashes(trim($postData['postData']['moduleId'])) : "";

// DELETE MODULE
    $updateModule = array();
    $updateModule['CHANGED_BY'] = $userId;
    $updateModule['CHANGED_BY_DATE'] = time();
    $updateModule['DELETE_FLAG'] = 1;
//WHere
    $where = "PK_MODULE_ID = '" . $moduleId . "'";

    $updateModuleQuery = update_rec(MODULEMASTER, $updateModule, $where);
    if ($updateModuleQuery) {

        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => UPDATEQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR MODULE LISTING
function getModuleListing($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

// 
    $sqlModule = "SELECT PK_MODULE_ID,MODULE_NAME FROM " . MODULEMASTER . " WHERE DELETE_FLAG = 0 ";
    $resultModule = fetch_rec_query($sqlModule);

    if (count($resultModule) > 0) {

        $finalJson = array();
        foreach ($resultModule as $keyM => $valueM) {
            $finalJson[$keyM]['moduleId'] = $valueM['PK_MODULE_ID'];
            $finalJson[$keyM]['moduleName'] = $valueM['MODULE_NAME'];
        }

        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR CREATE ACTIVITY(PERMISSION RIGHT)
function createActivity($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $moduleId = isset($postData['postData']['moduleId']) ? addslashes(trim($postData['postData']['moduleId'])) : "";
    $activityName = isset($postData['postData']['activityName']) ? addslashes(trim($postData['postData']['activityName'])) : "";

// CREATE MODULE
    $insertActivity = array();
    $insertActivity['ACTIVITY_NAME'] = $activityName;
    $insertActivity['FK_MODULE_ID'] = $moduleId;
    $insertActivity['FK_CLIENT_ID'] = $clientId;
    $insertActivity['CREATED_BY'] = $userId;
    $insertActivity['CREATED_BY_DATE'] = time();
    $insertActivity['DELETE_FLAG'] = 0;

    $insertActivityQuery = insert_rec(ACTIVITYMASTER, $insertActivity);
    if ($insertActivityQuery) {

        $finalJson = array("moduleId" => $moduleId, "activityId" => $insertActivityQuery['lastInsertedId']);
        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR UPDATE ACTIVITY
function updateActivity($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $moduleId = isset($postData['postData']['moduleId']) ? addslashes(trim($postData['postData']['moduleId'])) : "";
    $activityName = isset($postData['postData']['activityName']) ? addslashes(trim($postData['postData']['activityName'])) : "";
    $activityId = isset($postData['postData']['activityId']) ? addslashes(trim($postData['postData']['activityId'])) : "";

// UPDATE ACTIVITY 
    $updateActivity = array();
    $updateActivity['ACTIVITY_NAME'] = $activityName;
    $updateActivity['FK_MODULE_ID'] = $moduleId;
    $updateActivity['FK_CLIENT_ID'] = $clientId;
    $updateActivity['CHANGED_BY'] = $userId;
    $updateActivity['CHANGED_BY_DATE'] = time();
    $updateActivity['DELETE_FLAG'] = 0;

// WHERE
    $whereAvtivity = "PK_ACTIVITY_ID = '" . $activityId . "' AND FK_MODULE_ID = '" . $moduleId . "' AND FK_CLIENT_ID = '" . $clientId . "'";
    $updateActivityQuery = update_rec(ACTIVITYMASTER, $updateActivity, $whereAvtivity);
    if ($updateActivityQuery) {

        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR DELETE ACTIVITY
function deleteActivity($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $activityId = isset($postData['postData']['activityId']) ? addslashes(trim($postData['postData']['activityId'])) : "";

    $updateActivity = array();
    $updateActivity['CHANGED_BY'] = $userId;
    $updateActivity['CHANGED_BY_DATE'] = time();
    $updateActivity['DELETE_FLAG'] = 1;

    $whereAvtivityDelete = "PK_ACTIVITY_ID = '" . $activityId . "' AND FK_CLIENT_ID = '" . $clientId . "'";
    $updateActivityQuery = update_rec(ACTIVITYMASTER, $updateActivity, $whereAvtivityDelete);
    if ($updateActivityQuery) {

        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR PERMISSION LISTING
function getPermissionListing($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

// GET PERMISSION LISTING
    $sqlPermission = "SELECT MM.MODULE_NAME,AM.FK_MODULE_ID,AM.PK_ACTIVITY_ID,AM.ACTIVITY_NAME FROM " . MODULEMASTER . " MM INNER JOIN " . ACTIVITYMASTER . " AM ON AM.FK_MODULE_ID = MM.PK_MODULE_ID WHERE MM.DELETE_FLAG = 0 AND AM.DELETE_FLAG = '0' AND MM.MODULE_TYPE = '1'";
    $resultPermission = fetch_rec_query($sqlPermission);
//print_r($resultPermission); exit;
    if (count($resultPermission) > 0) {

        $finalJson = array();
        foreach ($resultPermission as $keyP => $valueP) {
            $finalJson[$keyP]['moduleName'] = $valueP['MODULE_NAME'];
            $finalJson[$keyP]['moduleId'] = $valueP['FK_MODULE_ID'];
            $finalJson[$keyP]['activityId'] = $valueP['PK_ACTIVITY_ID'];
            $finalJson[$keyP]['activityName'] = $valueP['ACTIVITY_NAME'];
        }
        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR CREATE EDIT CLIENT
function createEditClient($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestedClientId = isset($postData['postData']['requestedClientId']) ? addslashes(trim($postData['postData']['requestedClientId'])) : "";
    $clientName = isset($postData['postData']['clientName']) ? addslashes(trim($postData['postData']['clientName'])) : "";
    $clientEmail = isset($postData['postData']['clientEmail']) ? addslashes(trim($postData['postData']['clientEmail'])) : "";
    $clientAddress = isset($postData['postData']['clientAddress']) ? addslashes(trim($postData['postData']['clientAddress'])) : "";
    $clientContact = isset($postData['postData']['clientContact']) ? addslashes(trim($postData['postData']['clientContact'])) : "";
    $webAddress = isset($postData['postData']['webAddress']) ? addslashes(trim($postData['postData']['webAddress'])) : "";
    $clientValEndDate = isset($postData['postData']['clientValidity']) ? addslashes(trim($postData['postData']['clientValidity'])) : "";
    $smsFlag = isset($postData['postData']['smsFlag']) ? addslashes(trim($postData['postData']['smsFlag'])) : "";
    $emailFlag = isset($postData['postData']['emailFlag']) ? addslashes(trim($postData['postData']['emailFlag'])) : "";
    $developerFlag = isset($postData['postData']['developerFlag']) ? addslashes(trim($postData['postData']['developerFlag'])) : "";
    $apiLogFlag = isset($postData['postData']['apiLogFlag']) ? addslashes(trim($postData['postData']['apiLogFlag'])) : "";
    $tallyFlag = isset($postData['postData']['tallyFlag']) ? addslashes(trim($postData['postData']['tallyFlag'])) : "";
    $tallyFlagId = isset($postData['postData']['tallyFlagId']) ? addslashes(trim($postData['postData']['tallyFlagId'])) : "";
    $tallyAutoSyncFlagId = isset($postData['postData']['tallyAutoSyncFlagId']) ? addslashes(trim($postData['postData']['tallyAutoSyncFlagId'])) : ""; //ADD BY DEVIKA.8.08.2018.
    $orderEposFlag = isset($postData['postData']['orderEposFlag']) ? addslashes(trim($postData['postData']['orderEposFlag'])) : "";
    $eposClientId = isset($postData['postData']['eposClientId']) ? addslashes(trim($postData['postData']['eposClientId'])) : "";
    $fnbSpaceId = isset($postData['postData']['fnbSpaceId']) ? addslashes(trim($postData['postData']['fnbSpaceId'])) : "";
    $laundrySpaceId = isset($postData['postData']['laundrySpaceId']) ? addslashes(trim($postData['postData']['laundrySpaceId'])) : "";
    $eposUrl = isset($postData['postData']['eposUrl']) ? addslashes(trim($postData['postData']['eposUrl'])) : "";

    if (!filter_var($clientEmail, FILTER_VALIDATE_EMAIL) === false) {

        $insertArr = array();
        $insertArr['CLIENTNAME'] = $clientName;
        $insertArr['CLIENT_MOB'] = $clientContact;
        $insertArr['CLIENT_EMAIL'] = $clientEmail;
        $insertArr['CLIENT_ADDRESS'] = $clientAddress;
        $insertArr['CLIENT_DOR'] = date("Y-m-d");
        $insertArr['CLIENT_WEBADDRESS'] = $webAddress;
        $insertArr['ORDER_TO_EPOS'] = $orderEposFlag;
        $insertArr['FNB_SPACE_ID'] = $fnbSpaceId;
        $insertArr['FNB_LAUNDRY_ID'] = $laundrySpaceId;
        $insertArr['EPOS_CLIENT_ID'] = $eposClientId;
        $insertArr['EPOS_URL'] = $eposUrl;
        $insertArr['CLIENT_VAL_END_DATE'] = (!empty($clientValEndDate) && $clientValEndDate != '00-00-0000') ? date("Y-m-d", strtotime($clientValEndDate)) : "";

        if (!empty($requestedClientId) && $requestedClientId != "") {//UPDATE CASE
            $insertArr['CHANGED_BY'] = $userId;
            $insertArr['CHANGED_BY_DATE'] = time();

            $updateWhere = "PK_CLIENT_ID = '" . $requestedClientId . "' AND DELETE_FLAG = '0' ";

            $insertUpdateData = update_rec(CLIENTMASTER, $insertArr, $updateWhere);
        } else {//INSERT CASE
            $insertArr['CREATED_BY'] = $userId;
            $insertArr['CREATED_BY_DATE'] = time();
            $insertArr['DELETE_FLAG'] = '0';

            $insertUpdateData = insert_rec(CLIENTMASTER, $insertArr);

            if ($insertUpdateData['lastInsertedId'] > 0) {

                $insertSeqArr = array();
                $insertSeqArr['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr['TABLE_NAME'] = 'inq_master';
                $insertSeqArr['SEQ_VALUE'] = '1';
                $insertSeqData = insert_rec(SEQUENCETABLE, $insertSeqArr);

                $insertSeqArr1 = array();
                $insertSeqArr1['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr1['TABLE_NAME'] = 'inq_booking';
                $insertSeqArr1['SEQ_VALUE'] = '1';
                $insertSeqData1 = insert_rec(SEQUENCETABLE, $insertSeqArr1);

                $insertSeqArr2 = array();
                $insertSeqArr2['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr2['TABLE_NAME'] = 'events_booking_master';
                $insertSeqArr2['SEQ_VALUE'] = '1';
                $insertSeqData2 = insert_rec(SEQUENCETABLE, $insertSeqArr2);

                $insertSeqArr3 = array();
                $insertSeqArr3['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr3['TABLE_NAME'] = 'student_master';
                $insertSeqArr3['SEQ_VALUE'] = '1';
                $insertSeqData3 = insert_rec(SEQUENCETABLE, $insertSeqArr3);

                $insertSeqArr4 = array();
                $insertSeqArr4['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr4['TABLE_NAME'] = 'parent_master';
                $insertSeqArr4['SEQ_VALUE'] = '1';
                $insertSeqData4 = insert_rec(SEQUENCETABLE, $insertSeqArr4);

                $insertSeqArr5 = array();
                $insertSeqArr5['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr5['TABLE_NAME'] = 'invoice_master';
                $insertSeqArr5['SEQ_VALUE'] = '1';
                $insertSeqData5 = insert_rec(SEQUENCETABLE, $insertSeqArr5);

                $insertSeqArr6 = array();
                $insertSeqArr6['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr6['TABLE_NAME'] = 'event_master';
                $insertSeqArr6['SEQ_VALUE'] = '1';
                $insertSeqData6 = insert_rec(SEQUENCETABLE, $insertSeqArr6);

                $insertSeqArr7 = array();
                $insertSeqArr7['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr7['TABLE_NAME'] = 'complaint_master';
                $insertSeqArr7['SEQ_VALUE'] = '1';
                $insertSeqData7 = insert_rec(SEQUENCETABLE, $insertSeqArr7);

                $insertSeqArr8 = array();
                $insertSeqArr8['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr8['TABLE_NAME'] = 'centralise_custom_receipt';
                $insertSeqArr8['SEQ_VALUE'] = '1';
                $insertSeqData8 = insert_rec(SEQUENCETABLE, $insertSeqArr8);

                $insertSeqArr9 = array();
                $insertSeqArr9['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr9['TABLE_NAME'] = 'centralise_custom_invoice';
                $insertSeqArr9['SEQ_VALUE'] = '1';
                $insertSeqData9 = insert_rec(SEQUENCETABLE, $insertSeqArr9);

                $insertSeqArr10 = array();
                $insertSeqArr10['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr10['TABLE_NAME'] = 'payment_master';
                $insertSeqArr10['SEQ_VALUE'] = '1';
                $insertSeqData10 = insert_rec(SEQUENCETABLE, $insertSeqArr10);

                $insertSeqArr11 = array();
                $insertSeqArr11['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertSeqArr11['TABLE_NAME'] = 'service_master';
                $insertSeqArr11['SEQ_VALUE'] = '1';
                $insertSeqData11 = insert_rec(SEQUENCETABLE, $insertSeqArr11);
            }
        }

        if ($insertUpdateData) {//INSERT UPDATE IN MAIL SMS CONFIG
            $insertConfig = array();
            $insertConfig['SMS_FLAG'] = $smsFlag;
            $insertConfig['EMAIL_FLAG'] = $emailFlag;
            $insertConfig['DEVELOPER_SMS_FLAG'] = $developerFlag;
            $insertConfig['API_LOG_FLAG'] = $apiLogFlag;
            $insertConfig['TALLY_INTEGRATION_FLAG'] = $tallyFlag;
            $insertConfig['TALLY_FLAG_ID'] = ($tallyFlag == '1') ? $tallyFlagId : '0';
            $insertConfig['TALLY_AUTO_SYNC_FLAG'] = $tallyAutoSyncFlagId; //ADD BY DEVIKA.8.08.2018.//by defalut 1=on.0=off
            if (!empty($requestedClientId) && $requestedClientId != "") {//UPDATE CASE
                $clientIdNew = $requestedClientId;
                $insertConfig['CHANGED_BY'] = $userId;
                $insertConfig['CHANGED_BY_DATE'] = time();

                $updateWhereConfig = "FK_CLIENT_ID = '" . $requestedClientId . "' AND DELETE_FLAG = '0' ";

                $insertUpdateConfig = update_rec(MAILSMSCONFIG, $insertConfig, $updateWhereConfig);
            } else {//INSERT CASE
                $clientIdNew = $insertUpdateData['lastInsertedId'];

                $insertConfig['FK_CLIENT_ID'] = $insertUpdateData['lastInsertedId'];
                $insertConfig['CREATED_BY'] = $userId;
                $insertConfig['CREATED_BY_DATE'] = time();
                $insertConfig['DELETE_FLAG'] = '0';

                $insertUpdateConfig = insert_rec(MAILSMSCONFIG, $insertConfig);


//INSERT DEFAULT ROLE
                $getPermission = "SELECT FK_MODULE_ID,PK_ACTIVITY_ID FROM " . ACTIVITYMASTER . " WHERE DELETE_FLAG = '0'  ";
                $resultPermision = fetch_rec_query($getPermission);
                if (count($resultPermision) > 0) {
                    foreach ($resultPermision as $keyP => $valueP) {
                        $createArr[$keyP]['rolename'] = 'defaultRole';
                        $createArr[$keyP]['moduleid'] = $valueP['FK_MODULE_ID'];
                        $createArr[$keyP]['activityid'] = $valueP['PK_ACTIVITY_ID'];
                    }
                }
//        print_r($createArr);exit;
                $requestData = array();
                $requestData['postData']['requestCase'] = 'createrole';
                $requestData['postData']['clientId'] = $clientIdNew;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['role'] = $createArr;
                createEditRole($requestData);

//CODE ADDED BY DARSHAN PATEL FOR START ENTRY FROM LITHELOOKUP TO CLIENTLOOKUP 
                $requestData = array();
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $getLitheLookUp = getLitheLookupListing_BL($requestData);
//print_r($getLitheLookUp); exit;

                if (count($getLitheLookUp['data']) > 0) {

//print_r($getLitheLookUp['data']);exit;
                    foreach ($getLitheLookUp['data'] as $keyLkp => $valueLkp) {

                        $insertClientLookUp = array();
                        $insertClientLookUp['FK_CLIENT_ID'] = $clientIdNew;
                        $insertClientLookUp['LOOK_TYPE'] = $valueLkp['lookupType'];
                        $insertClientLookUp['LOOK_VALUE'] = $valueLkp['lookupTypeName'];
                        $insertClientLookUp['DELETE_FLAG'] = '0';
                        $insertClientLookUp['CREATED_BY'] = $userId;
                        $insertClientLookUp['CREATED_BY_DATE'] = time();

                        $insertClientLookUpReq = insert_rec(CLIENTLOOKUP, $insertClientLookUp);
                        if ($insertClientLookUpReq) {
//print_r($valueLkp['lookupValues']);exit;
                            if ($valueLkp['lookupValues'] != NORECORDS) {
                                foreach ($valueLkp['lookupValues'] as $keyV => $valueV) {
                                    $insertInnerLookUp = array();
                                    $insertInnerLookUp['FK_CLIENT_ID'] = $clientIdNew;
                                    $insertInnerLookUp['LOOK_TYPE'] = $insertClientLookUpReq['lastInsertedId'];
                                    $insertInnerLookUp['LOOK_VALUE'] = '';
                                    $insertInnerLookUp['DISPLAY_VALUE'] = $valueV['displayValue'];
                                    $insertInnerLookUp['ISDEFAULTFLAG'] = $valueV['isDefaultValue'];
                                    $insertInnerLookUp['DELETE_FLAG'] = '0';
                                    $insertInnerLookUp['CREATED_BY'] = $userId;
                                    $insertInnerLookUp['CREATED_BY_DATE'] = time();
                                    $insertInnerLookUpReq = insert_rec(CLIENTLOOKUP, $insertInnerLookUp);
                                }
                            }
                        }
                    }
                }

                //INSERT DEFAULT STATUS FOR BOOKING & PROPORTY & STUDENT ADD BY DARSHAN PATEL ON 28-08-2018 
                $postData['postData']['clientId'] = $clientIdNew;
                insertDefaultStatus($postData);
            }

            if ($insertUpdateConfig) {

                $requestData = array();
                $requestData['postData']['clientId'] = $clientIdNew;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;

                $clientData = clientListing($requestData);

                $result = array("status" => SCS, "data" => $clientData['data']);
                http_response_code(200);
            } else {
                if (!empty($requestedClientId) && $requestedClientId != "") {//UPDATE CASE
                    $result = array("status" => UPDATEQUERYFAIL);
                    http_response_code(400);
                } else {
                    $result = array("status" => INSERTQUERYFAIL);
                    http_response_code(400);

//START REVERT CODE
                    $deleteArr = array();
                    $deleteArr['DELETE_FLAG'] = '1';
                    $deleteArr['CHANGED_BY'] = $userId;
                    $deleteArr['CHANGED_BY_DATE'] = time();

                    $deleteWhere = "PK_CLIENT_ID = '" . $insertUpdateData['lastInsertedId'] . "' AND DELETE_FLAG = '0' ";
                    $deleteData = update_rec(CLIENTMASTER, $deleteArr, $deleteWhere);
                }
            }
        } else {
            if (!empty($requestedClientId) && $requestedClientId != "") {//UPDATE CASE
                $result = array("status" => UPDATEQUERYFAIL);
                http_response_code(400);
            } else {
                $result = array("status" => INSERTQUERYFAIL);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => VALIDEMAIL);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR CLIENT LISTING
function clientListing($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $whereClientId = ""; //FOR LITHE USER
    if ($clientId != "-1") {
        $whereClientId = " AND PK_CLIENT_ID = '" . $clientId . "'";
    }

    $sqlClientData = "SELECT CM.PK_CLIENT_ID,CM.CLIENTNAME,CM.CLIENT_MOB,CM.CLIENT_EMAIL,CM.CLIENT_ADDRESS,CM.CLIENT_DOR,CM.CLIENT_LOGO,CM.CLIENT_WEBADDRESS,CM.CLIENT_VAL_END_DATE,MSC.SMS_FLAG,MSC.EMAIL_FLAG,MSC.DEVELOPER_SMS_FLAG,MSC.API_LOG_FLAG,MSC.TALLY_INTEGRATION_FLAG,MSC.TALLY_FLAG_ID ,CM.ORDER_TO_EPOS,CM.FNB_SPACE_ID,CM.FNB_LAUNDRY_ID,CM.EPOS_CLIENT_ID,CM.EPOS_URL FROM " . CLIENTMASTER . " CM INNER JOIN " . MAILSMSCONFIG . " MSC ON MSC.FK_CLIENT_ID = CM.PK_CLIENT_ID AND MSC.DELETE_FLAG = '0' WHERE CM.DELETE_FLAG = '0' " . $whereClientId . " ";
    $resultClientData = fetch_rec_query($sqlClientData);
    if (count($resultClientData) > 0) {

        $finalJson = array();
        foreach ($resultClientData as $keyClient => $valueClient) {

            $finalJson[$keyClient]['clientId'] = $valueClient['PK_CLIENT_ID'];
            $finalJson[$keyClient]['clientName'] = $valueClient['CLIENTNAME'];
            $finalJson[$keyClient]['clientContact'] = $valueClient['CLIENT_MOB'];
            $finalJson[$keyClient]['clientEmail'] = $valueClient['CLIENT_EMAIL'];
            $finalJson[$keyClient]['clientAddress'] = $valueClient['CLIENT_ADDRESS'];
            $finalJson[$keyClient]['clientCreateDate'] = date("d-m-Y", strtotime($valueClient['CLIENT_DOR']));
            $finalJson[$keyClient]['clientLogo'] = $valueClient['CLIENT_LOGO'];
            $finalJson[$keyClient]['webAddress'] = $valueClient['CLIENT_WEBADDRESS'];
            $finalJson[$keyClient]['clientValidity'] = $valueClient['CLIENT_VAL_END_DATE'];
            $finalJson[$keyClient]['smsFlag'] = $valueClient['SMS_FLAG'];
            $finalJson[$keyClient]['emailFlag'] = $valueClient['EMAIL_FLAG'];
            $finalJson[$keyClient]['developerFlag'] = $valueClient['DEVELOPER_SMS_FLAG'];
            $finalJson[$keyClient]['apiLogFlag'] = $valueClient['API_LOG_FLAG'];
            $finalJson[$keyClient]['tallyFlag'] = $valueClient['TALLY_INTEGRATION_FLAG'];
            $finalJson[$keyClient]['tallyFlagId'] = $valueClient['TALLY_FLAG_ID'];
            $finalJson[$keyClient]['orderEposFlag'] = $valueClient['ORDER_TO_EPOS'];
            $finalJson[$keyClient]['fnbSpaceId'] = $valueClient['FNB_SPACE_ID'];
            $finalJson[$keyClient]['fnbLaundryId'] = $valueClient['FNB_LAUNDRY_ID'];
            $finalJson[$keyClient]['eposClientId'] = $valueClient['EPOS_CLIENT_ID'];
            $finalJson[$keyClient]['eposUrl'] = $valueClient['EPOS_URL'];
        }

        if (!empty($finalJson)) {
            $result = array("status" => SCS, "data" => array_values($finalJson));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR BRANCH CREATE
function createEditBranch($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $resquestClientId = isset($postData['postData']['requestClientId']) ? addslashes(trim($postData['postData']['requestClientId'])) : "";
    $branchName = isset($postData['postData']['branchName']) ? addslashes(trim($postData['postData']['branchName'])) : "";
    $branchAddress = isset($postData['postData']['branchAddress']) ? addslashes(trim($postData['postData']['branchAddress'])) : "";
    $branchEmail = isset($postData['postData']['branchEmail']) ? addslashes(trim($postData['postData']['branchEmail'])) : "";
    $branchMobile = isset($postData['postData']['branchMobile']) ? addslashes(trim($postData['postData']['branchMobile'])) : "";
    $branchId = isset($postData['postData']['branchId']) ? addslashes(trim($postData['postData']['branchId'])) : "";

// INSERT INTO ORG MASTER TABLE
    $insertOrgMaster = array();
    $insertOrgMaster['ORG_ADDRESS'] = $branchAddress;
    $insertOrgMaster['ORGNAME'] = $branchName;
    $insertOrgMaster['ORG_EMAIL'] = $branchEmail;
    $insertOrgMaster['ORG_MOBILE'] = $branchMobile;
    $insertOrgMaster['ORGNAME'] = $branchName;

    if (!empty($branchId) && $branchId != "") {//UPDTEA CASE
        $reqOrgId = $branchId;
        $insertOrgMaster['CHANGED_BY'] = $userId;
        $insertOrgMaster['CHANGED_BY_DATE'] = time();

        $updateWhere = "PK_ORG_ID = '" . $branchId . "' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '" . $clientId . "' ";
        $insertRecord = update_rec(ORGMASTER, $insertOrgMaster, $updateWhere);
    } else {//INSERT CASE
        $insertOrgMaster['FK_CLIENT_ID'] = $resquestClientId;
        $insertOrgMaster['ORG_TYPE'] = 2;
        $insertOrgMaster['CREATED_BY'] = $userId;
        $insertOrgMaster['CREATED_BY_DATE'] = time();
        $insertOrgMaster['DELETE_FLAG'] = 0;

        $insertRecord = insert_rec(ORGMASTER, $insertOrgMaster);
        $reqOrgId = $insertRecord['lastInsertedId'];
    }

    if ($insertRecord) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['orgId'] = $orgId;
        $requestData['postData']['reqOrgId'] = $reqOrgId;

        $branchData = branchListing($requestData);

        $result = array("status" => SCS, "data" => $branchData['data']);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTQUERYFAIL);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR BRANCH LISTING
function branchListing($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $reqOrgId = isset($postData['postData']['reqOrgId']) ? addslashes(trim($postData['postData']['reqOrgId'])) : "";

    if ($clientId == "-1") {
        $whereClient = "";
    } else {
        $whereClient = " AND OM.FK_CLIENT_ID = '" . $clientId . "'";
    }

    $whereOrg = "";
    if (!empty($reqOrgId) && $reqOrgId != "") {
        $whereOrg = " AND OM.PK_ORG_ID = '" . $reqOrgId . "' ";
    }


    $sqlOrgListing = "SELECT OM.ORGNAME,OM.ORG_ADDRESS,OM.PK_ORG_ID,OM.ORG_EMAIL,OM.ORG_MOBILE,CM.CLIENTNAME,CM.PK_CLIENT_ID FROM " . ORGMASTER . " OM INNER JOIN " . CLIENTMASTER . " CM ON CM.PK_CLIENT_ID = OM.FK_CLIENT_ID WHERE OM.DELETE_FLAG = 0 AND ORG_TYPE = '2' " . $whereClient . $whereOrg;
    $resultOrgListing = fetch_rec_query($sqlOrgListing);

    if (count($resultOrgListing) > 0) {
        $finalJson = array();

        foreach ($resultOrgListing as $keyBranch => $valueBranch) {

            $finalJson[$keyBranch]['orgName'] = $valueBranch['ORGNAME'];
            $finalJson[$keyBranch]['orgAddress'] = $valueBranch['ORG_ADDRESS'];
            $finalJson[$keyBranch]['orgId'] = $valueBranch['PK_ORG_ID'];
            $finalJson[$keyBranch]['clientName'] = $valueBranch['CLIENTNAME'];
            $finalJson[$keyBranch]['orgEmail'] = $valueBranch['ORG_EMAIL'];
            $finalJson[$keyBranch]['orgMobile'] = $valueBranch['ORG_MOBILE'];
            $finalJson[$keyBranch]['clientId'] = $valueBranch['PK_CLIENT_ID'];
        }
        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION DELETE BRANCH
function deleteBranch($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $branchId = isset($postData['postData']['branchId']) ? addslashes(trim($postData['postData']['branchId'])) : "";

// UPDATE ORG MASTER TABLE
    $updateBranch = array();
    $updateBranch['DELETE_FLAG'] = 1;
    $updateBranch['CHANGED_BY'] = $userId;
    $updateBranch['CHANGED_BY_DATE'] = time();

// WHERE
    $updateBranchWhere = "PK_ORG_ID = '" . $branchId . "' AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '" . $clientId . "'";
    $updateBranchQuery = update_rec(ORGMASTER, $updateBranch, $updateBranchWhere);

    if ($updateBranchQuery) {
        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => UPDATEQUERYFAIL);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR CREATE UPDATE SMS DATA
function createUpdateSms($postData) {
//    print_r($postData);exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $reqClientId = isset($postData['postData']['reqClientId']) ? addslashes(trim($postData['postData']['reqClientId'])) : "";
    $uname = isset($postData['postData']['uname']) ? addslashes(trim($postData['postData']['uname'])) : "";
    $password = isset($postData['postData']['password']) ? addslashes(trim($postData['postData']['password'])) : "";
    $sid = isset($postData['postData']['sid']) ? addslashes(trim($postData['postData']['sid'])) : "";
    $api = isset($postData['postData']['api']) ? addslashes(trim($postData['postData']['api'])) : "";
    $smsUrl = isset($postData['postData']['smsUrl']) ? addslashes(trim($postData['postData']['smsUrl'])) : ""; //SMS URL WITH ALL CONSTANT
    $smsUrlSchedule = isset($postData['postData']['smsUrlSchedule']) ? addslashes(trim($postData['postData']['smsUrlSchedule'])) : ""; //SMS URL WITH ALL CONSTANT FOR SCHEDULE MESSAGE SEND


    $sqlOrgListing = "SELECT FK_CLIENT_ID FROM " . MAILSMSCONFIG . " WHERE FK_CLIENT_ID = '" . $reqClientId . "' ";
    $totalRec = fetch_rec_query($sqlOrgListing);

    if (count($totalRec) == 0) {//INSERT CASE
        $insertReocrd = array();
        $insertReocrd['FK_CLIENT_ID'] = $reqClientId;
        $insertReocrd['SMS_USERNAME'] = $uname;
        $insertReocrd['SMS_PASSWORD'] = $password;
        $insertReocrd['SMS_SID'] = $sid;
        $insertReocrd['SMS_API_URL'] = $api;
        $insertReocrd['SMS_URL'] = $smsUrl;
        $insertReocrd['SMS_URL_SCHEDULE'] = $smsUrlSchedule;
        $insertReocrd['CREATED_BY'] = $userId;
        $insertReocrd['CREATED_BY_DATE'] = time();
        $insertReocrd['DELETE_FLAG'] = 0;

        $insUpd = insert_rec(MAILSMSCONFIG, $insertReocrd);

        if ($insUpd) {

            $requestData = array();
            $requestData['postData']['clientId'] = $reqClientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['orgId'] = $orgId;

            $requestSmsData = SMSData($requestData);

            $result = array("status" => SCS, "data" => $requestSmsData['data']);
            http_response_code(200);
        } else {
            $result = array("status" => INSERTFAIL);
            http_response_code(400);
        }
    } else {//UPDATE CASE
        $updateLookUp = array();
        $updateLookUp['FK_CLIENT_ID'] = $reqClientId;
        $updateLookUp['SMS_USERNAME'] = $uname;
        $updateLookUp['SMS_PASSWORD'] = $password;
        $updateLookUp['SMS_SID'] = $sid;
        $updateLookUp['SMS_API_URL'] = $api;
        $updateLookUp['SMS_URL'] = $smsUrl;
        $updateLookUp['SMS_URL_SCHEDULE'] = $smsUrlSchedule;
        $updateLookUp['CHANGED_BY'] = $userId;
        $updateLookUp['CHANGED_BY_DATE'] = time();
        $updateLookUp['DELETE_FLAG'] = 0;

        $whereLookup = "FK_CLIENT_ID = '" . $reqClientId . "' ";
        $insUpd = update_rec(MAILSMSCONFIG, $updateLookUp, $whereLookup);

        if ($insUpd) {
            $requestData = array();
            $requestData['postData']['clientId'] = $reqClientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['orgId'] = $orgId;

            $requestSmsData = SMSData($requestData);

            $result = array("status" => SCS, "data" => $requestSmsData['data']);
            http_response_code(200);
        } else {
            $result = array("status" => INSERTFAIL);
            http_response_code(400);
        }
    }

    return $result;
}

//FUNCTION FOR SMS CONFIGURATION
function SMSData($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $sqlSms = "SELECT MS.SMS_USERNAME,MS.SMS_PASSWORD,MS.SMS_SID,MS.SMS_API_URL,CM.CLIENTNAME,CM.PK_CLIENT_ID,MS.SMS_URL,MS.SMS_URL_SCHEDULE FROM " . MAILSMSCONFIG . " MS INNER JOIN " . CLIENTMASTER . " CM ON CM.PK_CLIENT_ID = MS.FK_CLIENT_ID WHERE MS.FK_CLIENT_ID = '" . $clientId . "' AND MS.DELETE_FLAG = '0' AND CM.DELETE_FLAG = '0' ";
    $resultSms = fetch_rec_query($sqlSms);
//    print_r($resultSms);exit;
    if (count($resultSms) > 0) {

        $finalJson = array();
        foreach ($resultSms as $key => $value) {

            $finalJson[$key]['client'] = $value['CLIENTNAME'];
            $finalJson[$key]['clientId'] = $value['PK_CLIENT_ID'];
            $finalJson[$key]['uname'] = $value['SMS_USERNAME'];
            $finalJson[$key]['password'] = $value['SMS_PASSWORD'];
            $finalJson[$key]['sid'] = $value['SMS_SID'];
            $finalJson[$key]['apis'] = $value['SMS_API_URL'];
            $finalJson[$key]['smsUrl'] = $value['SMS_URL'];
            $finalJson[$key]['smsUrlSchedule'] = $value['SMS_URL_SCHEDULE'];
        }
        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function EMAILData($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $sqlSms = "SELECT MS.FK_CLIENT_ID,MS.MAILCONFIG_HOST,MS.MAILCONFIG_PORT,MS.MAILCONFIG_USERNAME,MS.MAILCONFIG_PASSWORD,MS.SENDERFNAME,MS.SENDERLNAME  FROM " . MAILSMSCONFIG . " MS WHERE MS.FK_CLIENT_ID = '" . $clientId . "' AND MS.DELETE_FLAG = '0'";
    $resultSms = fetch_rec_query($sqlSms);

    if (count($resultSms) > 0) {

        $finalJson = array();
        foreach ($resultSms as $keySMS => $valueSMS) {
            $finalJson[$keySMS]['clientId'] = $valueSMS['FK_CLIENT_ID'];
            $finalJson[$keySMS]['hostName'] = $valueSMS['MAILCONFIG_HOST'];
            $finalJson[$keySMS]['portNo'] = $valueSMS['MAILCONFIG_PORT'];
            $finalJson[$keySMS]['email'] = $valueSMS['MAILCONFIG_USERNAME'];
            $finalJson[$keySMS]['password'] = $valueSMS['MAILCONFIG_PASSWORD'];
            $finalJson[$keySMS]['sender'] = $valueSMS['SENDERFNAME'] . " " . $valueSMS['SENDERLNAME'];
        }

        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR EMAIL CONFIGURATION
function createUpdateEmail($postData) {
//    print_r($postData);exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $reqClientId = isset($postData['postData']['reqClientId']) ? addslashes(trim($postData['postData']['reqClientId'])) : "";
    $hostName = isset($postData['postData']['hostName']) ? addslashes(trim($postData['postData']['hostName'])) : "";
    $portNo = isset($postData['postData']['portNo']) ? addslashes(trim($postData['postData']['portNo'])) : "";
    $email = isset($postData['postData']['email']) ? addslashes(trim($postData['postData']['email'])) : "";
    $password = isset($postData['postData']['password']) ? addslashes(trim($postData['postData']['password'])) : "";
    $sender = isset($postData['postData']['sender']) ? addslashes(trim($postData['postData']['sender'])) : "";


    $sqlOrgListing = "SELECT PK_MAILCONFIG_ID FROM " . MAILSMSCONFIG . " WHERE FK_CLIENT_ID = '" . $reqClientId . "' ";
    $totalRec = fetch_rec_query($sqlOrgListing);

    if ($sender != "") {
        $senary = explode(' ', $sender);
        $fname = $senary[0];
        unset($senary[0]);
        $lname = implode(" ", $senary);
    }

    if (count($totalRec) == 0) {

        $insertReocrd = array();
        $insertReocrd['FK_CLIENT_ID'] = $reqClientId;
        $insertReocrd['MAILCONFIG_HOST'] = $hostName;
        $insertReocrd['MAILCONFIG_PORT'] = $portNo;
        $insertReocrd['MAILCONFIG_USERNAME'] = $email;
        $insertReocrd['MAILCONFIG_PASSWORD'] = $password;
        $insertReocrd['SENDERFNAME'] = $fname;
        $insertReocrd['SENDERLNAME'] = $lname;
        $insertReocrd['CREATED_BY'] = $userId;
        $insertReocrd['CREATED_BY_DATE'] = time();
        $insertReocrd['DELETE_FLAG'] = 0;

        $insUpd = insert_rec(MAILSMSCONFIG, $insertReocrd);

        if ($insUpd) {
            $requestData = array();
            $requestData['postData']['clientId'] = $reqClientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['orgId'] = $orgId;

            $requestEmailData = EMAILData($requestData);

            $result = array("status" => SCS, "data" => $requestEmailData['data']);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {

        $updateLookUp = array();
        $updateLookUp['FK_CLIENT_ID'] = $reqClientId;
        $updateLookUp['MAILCONFIG_HOST'] = $hostName;
        $updateLookUp['MAILCONFIG_PORT'] = $portNo;
        $updateLookUp['MAILCONFIG_USERNAME'] = $email;
        $updateLookUp['MAILCONFIG_PASSWORD'] = $password;
        $updateLookUp['SENDERFNAME'] = $fname;
        $updateLookUp['SENDERLNAME'] = $lname;
        $updateLookUp['CHANGED_BY'] = $userId;
        $updateLookUp['CHANGED_BY_DATE'] = time();
        $updateLookUp['DELETE_FLAG'] = 0;

        $whereLookup = "FK_CLIENT_ID = '" . $reqClientId . "' AND DELETE_FLAG = '0' ";

        $insUpd = update_rec(MAILSMSCONFIG, $updateLookUp, $whereLookup);

        if ($insUpd) {
            $requestData = array();
            $requestData['postData']['clientId'] = $reqClientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['orgId'] = $orgId;

            $requestEmailData = EMAILData($requestData);

            $result = array("status" => SCS, "data" => $requestEmailData['data']);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    }
    return $result;
}

//STUDENT LOGIN API ADDED BY DARSHAN PATEL 0N 03/07/2018
function studentLogin($postData) {
//print_r($postData);exit;
    $userName = isset($postData['postData']['userName']) ? addslashes(trim($postData['postData']['userName'])) : "";
    $password = isset($postData['postData']['passWord']) ? addslashes(trim(md5($postData['postData']['passWord']))) : "";
//$membershipNo = isset($postData['postData']['membershipNo']) ? addslashes(trim($postData['postData']['membershipNo'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    if ((!empty($userName)) && !empty($password) && !empty($type)) {

        if ($type == 'student') {
            $sqlGetStudent = "SELECT SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.USER_NAME,SM.PASSWORD,SM.FIRST_NAME,SM.LAST_NAME,SM.FK_CLIENT_ID,SM.MOBILE_NO,SM.EMAIL,SM.IS_ACTIVE,SM.SETPASSWORD_FLAG FROM " . STUDENTMASTER . " SM WHERE SM.DELETE_FLAG = '0' AND (SM.MEMBERSHIP_NUMBER='" . $userName . "' OR SM.USER_NAME = '" . $userName . "')  LIMIT 0,1 ";
            $resultStudent = fetch_rec_query($sqlGetStudent);
        } else {
            $sqlGetStudent = "SELECT PM.PK_PARENT_ID,PM.PARENT_NAME,PM.FK_STUD_ID AS PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.USER_NAME,SM.PASSWORD,SM.FIRST_NAME,SM.LAST_NAME,SM.FK_CLIENT_ID,SM.MOBILE_NO,SM.EMAIL,SM.IS_ACTIVE,SM.SETPASSWORD_FLAG FROM " . PARENTMASTER . " PM INNER JOIN " . STUDENTMASTER . " SM ON PM.FK_STUD_ID = SM.PK_STUD_ID WHERE PM.DELETE_FLAG = '0' AND (PM.MEMBERSHIP_NUMBER='" . $userName . "' OR PM.USER_NAME = '" . $userName . "')  LIMIT 0,1 ";
            $resultStudent = fetch_rec_query($sqlGetStudent);
        }

//        exit;
        //print_r($resultStudent);exit;
        if (count($resultStudent) > 0) {
//            $abc=password_verify($password, $resultStudent[0]['PASSWORD']);
//            print_r($abc); die;
            if (password_verify($password, $resultStudent[0]['PASSWORD'])) {//PASSWORD MATCH//CHANGE BY DEVIKA.19.7.2018
                if ($resultStudent[0]['IS_ACTIVE'] == '1') {//CHANGE BY DEVIKA.19.7.2018
                    $finalStudent = array();
                    $finalStudent[0]['studentId'] = $resultStudent[0]['PK_STUD_ID'];
                    $finalStudent[0]['studentMobile'] = $resultStudent[0]['MOBILE_NO'];
                    $finalStudent[0]['studentName'] = $resultStudent[0]['FIRST_NAME'] . " " . $resultStudent[0]['LAST_NAME'];
                    $finalStudent[0]['studentEmail'] = $resultStudent[0]['EMAIL'];
                    $finalStudent[0]['studentMemberShipno'] = $resultStudent[0]['MEMBERSHIP_NUMBER'];
                    $finalStudent[0]['clientId'] = $resultStudent[0]['FK_CLIENT_ID'];
                    $finalStudent[0]['setPasswordFlag'] = $resultStudent[0]['SETPASSWORD_FLAG'];
//print_r($finalStudent); die;
                    if ($type != 'student') {
                        $finalStudent[0]['parentId'] = $resultStudent[0]['PK_PARENT_ID'];
                        $finalStudent[0]['parentName'] = $resultStudent[0]['PARENT_NAME'];
                    }
                    $result = array("status" => SCS, "data" => $finalStudent);
                    http_response_code(200);
                } else {
                    $result = array("status" => "User Is Inactive. please contact system administartion");
                    http_response_code(400);
                }
            } else {
                $result = array("status" => INVALIDUSERPASS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => INVALIDUSERPASS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => USRNAMEPWDBLNK);
        http_response_code(400);
    }
//print_r($result);exit;
    return $result;
}

//FUNCTION FOR ADD SECTION CONTAIN OF PAGES ADD BY DARSHAN ON 3-7-2018
function addEditPageSectionDetail($postData) {

    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $sectionName = isset($postdata['sectionName']) ? addslashes(trim($postdata['sectionName'])) : "";
    $sectionDetail = isset($postdata['sectionDetail']) ? addslashes(trim($postdata['sectionDetail'])) : "";
    $sectionId = isset($postdata['sectionId']) ? addslashes(trim($postdata['sectionId'])) : "";

    if (!empty($sectionName) && !empty($sectionDetail)) {

        if (!empty($sectionId) && $sectionId != "") {//UPDTAE 
            $updateSection = array();
            $updateSection['SECTION_DETAIL'] = $sectionDetail;
            $updateSection['CHANGE_BY'] = $userId;
            $updateSection['CHANGE_BY_DATE'] = time();

            $updateWhere = "PK_SECTION_ID = '" . $sectionId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' ";
            $updateData = update_rec(PAGESECTION, $updateSection, $updateWhere);
            if ($updateData) {

                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['sectionId'] = $sectionId;

                $getData = getPageSectionDetail($requestData);

                $result = array("status" => SCS, "data" => $getData['data']);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEQUERYFAIL);
                http_response_code(400);
            }
        } else {//INSERT DATA
            $checkName = "SELECT SECTION_NAME FROM " . PAGESECTION . " WHERE DELETE_FLAG = '0' AND FK_CLIENT_ID = '" . $clientId . "' AND SECTION_NAME = '" . $sectionName . "' ";
            $resultName = fetch_rec_query($checkName);

            if (count($resultName) == 0) {
// STAET INSERTING DATA INTO SECTION TABLE
                $insertSection = array();
                $insertSection['SECTION_NAME'] = $sectionName;
                $insertSection['SECTION_DETAIL'] = $sectionDetail;
                $insertSection['FK_CLIENT_ID'] = $clientId;
                $insertSection['FK_ORG_ID'] = $orgId;
                $insertSection['CREATED_BY'] = $userId;
                $insertSection['CREATED_BY_DATE'] = time();
                $insertSection['DELETE_FLAG'] = 0;

                $insertData = insert_rec(PAGESECTION, $insertSection);
                if ($insertData) {

                    $requestData = array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['sectionId'] = $insertData['lastInsertedId'];

                    $getData = getPageSectionDetail($requestData);

                    $result = array("status" => SCS, "data" => $getData['data']);
                    http_response_code(200);
                } else {
                    $result = array("status" => INSERTQUERYFAIL);
                    http_response_code(400);
                }
            } else {
                $result = array("status" => CONTANTADDED);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR THE LISTING OF SECTION DETAIL 3-7-2018
function getPageSectionDetail($postData) {

    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $custId = isset($postdata['custId']) ? addslashes(trim($postdata['custId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $sectionName = isset($postdata['sectionName']) ? addslashes(trim($postdata['sectionName'])) : "";
    $sectionId = isset($postdata['sectionId']) ? addslashes(trim($postdata['sectionId'])) : "";

    $whereSection = "";
    if (!empty($sectionId) && $sectionId != "") {
        $whereSection = " AND PK_SECTION_ID = '" . $sectionId . "' ";
    }

    $whereSectionName = "";
    if (!empty($sectionName) && $sectionName != "") {
        $whereSectionName = " AND SECTION_NAME = '" . $sectionName . "' ";
    }

    $sqlSectionDetail = "SELECT PK_SECTION_ID,SECTION_NAME,SECTION_DETAIL FROM " . PAGESECTION . " WHERE DELETE_FLAG = '0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' " . $whereSection . " " . $whereSectionName . " ";
    $resultSectionDetail = fetch_rec_query($sqlSectionDetail);

    if (count($resultSectionDetail) > 0) {
        $finalJson = array();
        foreach ($resultSectionDetail as $keySec => $valueSec) {
            $finalJson[$keySec]['sectionId'] = $valueSec['PK_SECTION_ID'];
            $finalJson[$keySec]['sectionName'] = $valueSec['SECTION_NAME'];
            $finalJson[$keySec]['sectionDetail'] = $valueSec['SECTION_DETAIL'];
        }
        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR REMOVE THE SECTION DETAIL 3-7-2018
function removePageSectionDetail($postData) {

    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $sectionId = isset($postdata['sectionId']) ? addslashes(trim($postdata['sectionId'])) : "";

    if (!empty($sectionId) && $sectionId != "") {

        $updateSection = array();
        $updateSection['DELETE_FLAG'] = '1';
        $updateSection['CHANGE_BY'] = $userId;
        $updateSection['CHANGE_BY_DATE'] = time();

        $updateWhere = "PK_SECTION_ID = '" . $sectionId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' DELETE_FLAG = '0' ";
        $updateData = update_rec(PAGESECTION, $updateSection, $updateWhere);
        if ($updateData) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.18.7.2018
//FUNCTION FOR update student Detail
function updateProfileStudent($postData) { // CHANGED BY KAVITA PATEL ON 08-06-2018 END
//print_r($postData); die;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    // $roomName = isset($postData['postData']['roomName']) ? addslashes(trim($postData['postData']['roomName'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $fkStudentId = isset($postData['postData']['fkStudentId']) ? addslashes(trim($postData['postData']['fkStudentId'])) : "";
    $firstName = isset($postData['postData']['firstName']) ? addslashes(trim($postData['postData']['firstName'])) : "";
    $lastName = isset($postData['postData']['lastName']) ? addslashes(trim($postData['postData']['lastName'])) : "";
    $userName = isset($postData['postData']['userName']) ? addslashes(trim($postData['postData']['userName'])) : "";
    $collegeName = isset($postData['postData']['collegeName']) ? addslashes(trim($postData['postData']['collegeName'])) : "";
    $mobileNo = isset($postData['postData']['mobileNo']) ? addslashes(trim($postData['postData']['mobileNo'])) : "";
    $email = isset($postData['postData']['email']) ? addslashes(trim($postData['postData']['email'])) : "";
    $gender = isset($postData['postData']['gender']) ? addslashes(trim($postData['postData']['gender'])) : "";
    $bloodGroup = isset($postData['postData']['bloodGroup']) ? addslashes(trim($postData['postData']['bloodGroup'])) : "";
    $course = isset($postData['postData']['course']) ? addslashes(trim($postData['postData']['course'])) : "";

    $countryId = isset($postData['postData']['countryId']) ? addslashes(trim($postData['postData']['countryId'])) : "";

    $stateId = isset($postData['postData']['stateId']) ? addslashes(trim($postData['postData']['stateId'])) : "";
    $cityId = isset($postData['postData']['cityId']) ? addslashes(trim($postData['postData']['cityId'])) : "";
    $birthDate = !empty($postData['postData']['birthDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['birthDate'])))) : "";

    $acdemicStartYear = isset($postData['postData']['acdemicStartYear']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['acdemicStartYear'])))) : "";
    $acdemicEndYear = isset($postData['postData']['acdemicEndYear']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['acdemicEndYear'])))) : "";
    $semester = isset($postData['postData']['semester']) ? addslashes(trim($postData['postData']['semester'])) : "";
    $guarantorName = isset($postData['postData']['guarantorName']) ? addslashes(trim($postData['postData']['guarantorName'])) : "";
    $guarantorAddressLine1 = isset($postData['postData']['guarantorAddressLine1']) ? addslashes(trim($postData['postData']['guarantorAddressLine1'])) : "";
    $guarantorAddressLine2 = isset($postData['postData']['guarantorAddressLine2']) ? addslashes(trim($postData['postData']['guarantorAddressLine2'])) : "";
    $guarantorAddressLine3 = isset($postData['postData']['guarantorAddressLine3']) ? addslashes(trim($postData['postData']['guarantorAddressLine3'])) : "";
    $guarantorMobile1 = isset($postData['postData']['guarantorMobile1']) ? addslashes(trim($postData['postData']['guarantorMobile1'])) : "";
    $guarantorMobile2 = isset($postData['postData']['guarantorMobile2']) ? addslashes(trim($postData['postData']['guarantorMobile2'])) : "";
    $guarantorEmail = isset($postData['postData']['guarantorEmail']) ? addslashes(trim($postData['postData']['guarantorEmail'])) : "";
    $guarantorCityId = isset($postData['postData']['guarantorCityId']) ? addslashes(trim($postData['postData']['guarantorCityId'])) : "";
    $guarantorStateId = isset($postData['postData']['guarantorStateId']) ? addslashes(trim($postData['postData']['guarantorStateId'])) : "";
    $guarantorPincode = isset($postData['postData']['guarantorPincode']) ? addslashes(trim($postData['postData']['guarantorPincode'])) : "";
    $emergencyContactNo = isset($postData['postData']['emergencyContactNo']) ? addslashes(trim($postData['postData']['emergencyContactNo'])) : "";
    $emergencyContactName = isset($postData['postData']['emergencyContactName']) ? addslashes(trim($postData['postData']['emergencyContactName'])) : "";
    $aadharNo = isset($postData['postData']['aadharNo']) ? addslashes(trim($postData['postData']['aadharNo'])) : "";
    $passportNo = isset($postData['postData']['passportNo']) ? addslashes(trim($postData['postData']['passportNo'])) : "";
    $nationality = isset($postData['postData']['nationality']) ? addslashes(trim($postData['postData']['nationality'])) : "";
    // $medicalCertificate = isset($postData['postData']['medicalCertificate']) ? addslashes(trim($postData['postData']['medicalCertificate'])) : "";
    $medicalCondition = isset($postData['postData']['medicalCondition']) ? addslashes(trim($postData['postData']['medicalCondition'])) : "";
    $typeOfRoomRequired = isset($postData['postData']['typeOfRoomRequired']) ? addslashes(trim($postData['postData']['typeOfRoomRequired'])) : "";
    $addressLine1 = isset($postData['postData']['addressLine1']) ? addslashes(trim($postData['postData']['addressLine1'])) : "";
    $addressLine2 = isset($postData['postData']['addressLine2']) ? addslashes(trim($postData['postData']['addressLine2'])) : "";
    $addressLine3 = isset($postData['postData']['addressLine3']) ? addslashes(trim($postData['postData']['addressLine3'])) : "";
    $pinCode = isset($postData['postData']['pinCode']) ? addslashes(trim($postData['postData']['pinCode'])) : "";

    $parentName = isset($postData['postData']['parentName']) ? addslashes(trim($postData['postData']['parentName'])) : "";
    $parentMobileNo = isset($postData['postData']['parentMobileNo']) ? addslashes(trim($postData['postData']['parentMobileNo'])) : "";
    $parentEmailId = isset($postData['postData']['parentEmailId']) ? addslashes(trim($postData['postData']['parentEmailId'])) : "";
    $parentUserName = isset($postData['postData']['parentUserName']) ? addslashes(trim($postData['postData']['parentUserName'])) : "";
    // $parentPasword = isset($postData['postData']['parentPasword']) ? password_hash(addslashes(trim($postData['postData']['parentPasword'])), PASSWORD_DEFAULT) : "";

    $collegeAddressLine1 = isset($postData['postData']['collegeAddressLine1']) ? addslashes(trim($postData['postData']['collegeAddressLine1'])) : "";
    $collegeAddressLine2 = isset($postData['postData']['collegeAddressLine2']) ? addslashes(trim($postData['postData']['collegeAddressLine2'])) : "";
    $collegeAddressLine3 = isset($postData['postData']['collegeAddressLine3']) ? addslashes(trim($postData['postData']['collegeAddressLine3'])) : "";
    $collegeStateId = isset($postData['postData']['collegeStateId']) ? addslashes(trim($postData['postData']['collegeStateId'])) : "";
    $collegeCityId = isset($postData['postData']['collegeCityId']) ? addslashes(trim($postData['postData']['collegeCityId'])) : "";
    $collegePincode = isset($postData['postData']['collegePincode']) ? addslashes(trim($postData['postData']['collegePincode'])) : "";
    $parentAddreLine1 = isset($postData['postData']['parentAddreLine1']) ? addslashes(trim($postData['postData']['parentAddreLine1'])) : "";
    $parentAddreLine2 = isset($postData['postData']['parentAddreLine2']) ? addslashes(trim($postData['postData']['parentAddreLine2'])) : "";
    $parentAddreLine3 = isset($postData['postData']['parentAddreLine3']) ? addslashes(trim($postData['postData']['parentAddreLine3'])) : "";
    $parentCityId = isset($postData['postData']['parentCityId']) ? addslashes(trim($postData['postData']['parentCityId'])) : "";
    $parentStateId = isset($postData['postData']['parentStateId']) ? addslashes(trim($postData['postData']['parentStateId'])) : "";
    $parentCountryId = isset($postData['postData']['parentCountryId']) ? addslashes(trim($postData['postData']['parentCountryId'])) : "";
    $parentRelation = isset($postData['postData']['parentRelation']) ? addslashes(trim($postData['postData']['parentRelation'])) : "";
    $guarantorRelation = isset($postData['postData']['guarantorRelation']) ? addslashes(trim($postData['postData']['guarantorRelation'])) : "";
    $parentPincode = isset($postData['postData']['parentPincode']) ? addslashes(trim($postData['postData']['parentPincode'])) : "";
    $guarantorCountryId = isset($postData['postData']['guarantorCountryId']) ? addslashes(trim($postData['postData']['guarantorCountryId'])) : "";
    $parentId = isset($postData['postData']['parentId']) ? addslashes(trim($postData['postData']['parentId'])) : "";
    $guarantorId = isset($postData['postData']['guarantorId']) ? addslashes(trim($postData['postData']['guarantorId'])) : "";

    $errorArr = Array();

    /* CEHCK GURANTOR MOBILE NO1 VALIDATION START */
    if ($guarantorMobile1 != "") {
        if (strlen($guarantorMobile1) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $guarantorMobile1);
        }
    }
    /* CEHCK GURANTOR MOBILE NO1 VALIDATION END */

    /* CEHCK GURANTOR MOBILE NO2 VALIDATION START */
    if ($guarantorMobile2 != "") {
        if (strlen($guarantorMobile2) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $guarantorMobile2);
        }
    }
    /* CEHCK GURANTOR MOBILE NO2 VALIDATION END */
    /* CEHCK EMERGENCY MOBILE NO VALIDATION START */
    if ($emergencyContactNo != "") {
        if (strlen($emergencyContactNo) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $emergencyContactNo);
        }
    }
    /* CEHCK GURANTOR MOBILE NO2 VALIDATION END */

    /* CEHCK GURANTOR EMAIL VALIDATION START */
    if ($guarantorEmail != "") {
        if (!filter_var($guarantorEmail, FILTER_VALIDATE_EMAIL)) {
            array_push($errorArr, INVALIDEMAILADDRESS . " - " . $guarantorEmail);
        }
    }
    /* CEHCK GURANTOR EMAIL VALIDATION END */

    /* CEHCK EMAIL VALIDATION START */
    if (!empty($email)) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errorArr, INVALIDEMAILADDRESS . " - " . $email);
        }
    }
    /* CEHCK EMAIL VALIDATION END */

    /* CEHCK PARENT EMAIL VALIDATION START */
    if (!empty($parentEmailId)) {
        if (!filter_var($parentEmailId, FILTER_VALIDATE_EMAIL)) {
            array_push($errorArr, INVALIDEMAILADDRESS . " - " . $parentEmailId);
        }
    }
    /* CEHCK PARENT EMAIL VALIDATION END */
    /* CEHCK PARENT MOBILE NO VALIDATION START */
    // if (!empty($parentMobileNo)) {
    //     if (strlen($parentMobileNo) < 10) {
    //         array_push($errorArr, INVALIDMOBILENO . " - " . $parentMobileNo);
    //     } else {
    //         $whereStudId = "";
    //         if (!empty($parentId)) {
    //             $whereStudId = " AND PK_PARENT_ID !='" . $parentId . "'";
    //         }
    //         $sqlCheckMobileExist = "SELECT MOBILE_NO1 FROM " . PARENTSMASTER . " WHERE MOBILE_NO1='" . $parentMobileNo . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0' AND TYPE='Parent'"; // QUERY FOR MOBILE NO IS EXIST OR NOT.
    //         $resCheckMobileExist = fetch_rec_query($sqlCheckMobileExist); // RESULT FOR MOBILE NO IS EXIST OR NOT.
    //         if (count($resCheckMobileExist) > 0) {
    //             array_push($errorArr, ALREADYADDEDMOBILENO . " - " . $parentMobileNo);
    //         }
    //     }
    // }

    /* CEHCK PARENT MOBILE NO VALIDATION END */
    /* CEHCK MOBILE NO VALIDATION START */
    if (!empty($mobileNo)) {
        if (strlen($mobileNo) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $mobileNo);
        } else {
            $whereStudId = "";
            if (!empty($fkStudentId)) {
                $whereStudId = " AND PK_STUD_ID !='" . $fkStudentId . "'";
            }
            $sqlCheckMobileExist = "SELECT MOBILE_NO FROM " . STUDENTMASTER . " WHERE MOBILE_NO='" . $mobileNo . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'"; // QUERY FOR MOBILE NO IS EXIST OR NOT.
            $resCheckMobileExist = fetch_rec_query($sqlCheckMobileExist); // RESULT FOR MOBILE NO IS EXIST OR NOT.
            if (count($resCheckMobileExist) > 0) {
                array_push($errorArr, ALREADYADDEDMOBILENO . " - " . $mobileNo);
            }
        }
    }

    /* CEHCK MOBILE NO VALIDATION END */

    /* CEHCK STUDENT USERNAME VALIDATION START */
    $whereStudId = "";
    if (!empty($fkStudentId)) {
        $whereStudId = " AND PK_STUD_ID !='" . $fkStudentId . "'";
    }
    $sqlCheckUserNameExist = "SELECT USER_NAME FROM " . STUDENTMASTER . " WHERE USER_NAME='" . $userName . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'"; // QUERY FOR USER NAME IS EXIST OR NOT.
    $resCheckUserNameExist = fetch_rec_query($sqlCheckUserNameExist); // RESULT FOR USER NAME IS EXIST OR NOT.
    if (count($resCheckUserNameExist) > 0) {
        array_push($errorArr, ALREADYADDEDUSERNAME . " - " . $userName);
    }
    /* CEHCK STUDENT USERNAME VALIDATION END */

    /* CEHCK STUDENT EMAIL VALIDATION START */
    $whereStudId = "";
    if (!empty($fkStudentId)) {
        $whereStudId = " AND PK_STUD_ID !='" . $fkStudentId . "'";
    }
    $sqlCheckEmailExist = "SELECT USER_NAME FROM " . STUDENTMASTER . " WHERE EMAIL='" . $email . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'"; // QUERY FOR EMAIL IS EXIST OR NOT.
    $resCheckEmailExist = fetch_rec_query($sqlCheckEmailExist); // RESULT FOR EMAIL NAME IS EXIST OR NOT.
    if (count($resCheckEmailExist) > 0) {
        array_push($errorArr, ALREADYADDEDEMAIL . " - " . $email);
    }
    /* CEHCK STUDENT EMAIL VALIDATION END */

    if (empty($errorArr)) {

        if (!empty($clientId) && !empty($orgId) && !empty($firstName) && !empty($lastName) && !empty($userName) && !empty($collegeName) && !empty($gender) && !empty($course) && !empty($guarantorName) && !empty($guarantorAddressLine1) && !empty($guarantorMobile1) && !empty($aadharNo) && !empty($countryId) && !empty($stateId) && !empty($cityId)) { // CHECK REQURIED FIELD CONDITION
            // INSERT STUDENT ARRAY.
            $insertStudArray = Array(); //ROOM ARRAY CREATE
            $insertStudArray['FK_CLIENT_ID'] = $clientId;
            $insertStudArray['FK_ORG_ID'] = $orgId;
            $insertStudArray['FIRST_NAME'] = $firstName;
            $insertStudArray['LAST_NAME'] = $lastName;
            $insertStudArray['USER_NAME'] = $userName;
            // $insertStudArray['SECRET_IDENTIFICATION_CODE'] = md5($userName . " " . $password);
            $insertStudArray['MEMBERSHIP_NUMBER'] = generateId(STUDENTMASTER, "STU", $clientId);
            $insertStudArray['MOBILE_NO'] = $mobileNo;
            $insertStudArray['EMAIL'] = $email;
            $insertStudArray['ADDRESS_LINE1'] = $addressLine1;
            $insertStudArray['ADDRESS_LINE2'] = $addressLine2;
            $insertStudArray['ADDRESS_LINE3'] = $addressLine3;
            $insertStudArray['PINCODE'] = $pinCode;
            $insertStudArray['COLLEGE_NAME'] = $collegeName;
            $insertStudArray['COLLEGE_ADDRESS_LINE1'] = $collegeAddressLine1;
            $insertStudArray['COLLEGE_ADDRESS_LINE2'] = $collegeAddressLine2;
            $insertStudArray['COLLEGE_ADDRESS_LINE3'] = $collegeAddressLine3;
            $insertStudArray['COLLEGE_COUNTRY_ID'] = $countryId;
            $insertStudArray['COLLEGE_STATE_ID'] = $collegeStateId;
            $insertStudArray['COLLEGE_CITY_ID'] = $collegeCityId;
            $insertStudArray['COLLEGE_PINCODE'] = $collegePincode;
            $insertStudArray['GENDER'] = $gender;
            $insertStudArray['BLOOD_GROUP'] = $bloodGroup;
            $insertStudArray['COURSE'] = $course;
            $insertStudArray['ACDEMIC_START_DATE'] = $acdemicStartYear;
            $insertStudArray['ACDEMIC_END_DATE'] = $acdemicEndYear;
            $insertStudArray['SEMESTER'] = $semester;
            $insertStudArray['FK_COUNTRY_ID'] = $countryId;
            $insertStudArray['FK_CITY_ID'] = $cityId;
            $insertStudArray['FK_STATE_ID'] = $stateId;
            $insertStudArray['EMERGENCY_CONTACT_PERSON_NO'] = $emergencyContactNo;
            $insertStudArray['EMERGENCY_CONTACT_PERSON_NAME'] = $emergencyContactName;
            $insertStudArray['BIRTHDATE'] = $birthDate;
            $insertStudArray['AADHAR_NO'] = $aadharNo;
            $insertStudArray['PASSPORT_NO'] = $passportNo;
            $insertStudArray['NATIONALITY'] = $nationality;
            $insertStudArray['MEDICAL_CONDITION'] = $medicalCondition;
            $insertStudArray['TYPE_OF_ROOM_REQUIRED'] = $typeOfRoomRequired;
            $insertStudArray['IS_ACTIVE'] = '1'; // WHERE 1 is ACTIVE.
            // echo "<pre>"; print_r($insertStudArray); exit;

            if ($fkStudentId != "" && $fkStudentId > 0) {
                $insertStudArray['CHANGED_BY'] = $userId;
                $insertStudArray['CHANGED_BY_DATE'] = time();
                // UNSET FIELD AT EDIT TIME.
                unset($insertStudArray['USER_NAME']);
                unset($insertStudArray['MEMBERSHIP_NUMBER']);
                unset($insertStudArray['PARENT_USERNAME']);
                unset($insertStudArray['PARENT_PASSWORD']);
                unset($insertStudArray['PARENT_MEMBERSHIP_NUMBER']);

                $whereStudUpdate = "PK_STUD_ID =" . $fkStudentId;
                $updateStudData = update_rec(STUDENTMASTER, $insertStudArray, $whereStudUpdate); // UPDATE IN STUDENT MASTER TABLE.
                $studentId = $fkStudentId;
            }
            if ($studentId > 0) {

                $insertParentArray = Array();
                $insertParentArray['FK_CLIENT_ID'] = $clientId;
                $insertParentArray['FK_ORG_ID'] = $orgId;
                $insertParentArray['FK_STUD_ID'] = $studentId;
                $insertParentArray['PARENT_NAME'] = $parentName;
                $insertParentArray['USER_NAME'] = $parentUserName;
                // $insertParentArray['PASSWORD'] = $parentPasword;
                $insertParentArray['MEMBERSHIP_NUMBER'] = generateId(PARENTMASTER, "PAR", $clientId);
                $insertParentArray['MOBILE_NO1'] = $parentMobileNo;
                $insertParentArray['EMAIL'] = $parentEmailId;
                $insertParentArray['ADDRESS_LINE1'] = $parentAddreLine1;
                $insertParentArray['ADDRESS_LINE2'] = $parentAddreLine2;
                $insertParentArray['ADDRESS_LINE3'] = $parentAddreLine3;
                $insertParentArray['FK_CITY_ID'] = $parentCityId;
                $insertParentArray['FK_STATE_ID'] = $parentStateId;
                $insertParentArray['FK_COUNTRY_ID'] = $parentCountryId;
                $insertParentArray['TYPE'] = 'Parent';
                $insertParentArray['RELATION'] = $parentRelation;
                $insertParentArray['PINCODE'] = $parentPincode;
                if (!empty($parentId)) {
                    $insertParentArray['CHANGED_BY'] = $userId;
                    $insertParentArray['CHANGED_BY_DATE'] = time();
                    $whereParentUpdate = "PK_PARENT_ID =" . $parentId;
                    $updateParentData = update_rec(PARENTSMASTER, $insertParentArray, $whereParentUpdate); // UPDATE IN PARENT_MASTER TABLE
                }

                $insertGurantorArray = Array();
                $insertGurantorArray['FK_STUD_ID'] = $studentId;
                $insertGurantorArray['FK_CLIENT_ID'] = $clientId;
                $insertGurantorArray['FK_ORG_ID'] = $orgId;
                $insertGurantorArray['PARENT_NAME'] = $guarantorName;
                $insertGurantorArray['MOBILE_NO1'] = $guarantorMobile1;
                $insertGurantorArray['MOBILE_NO2'] = $guarantorMobile2;
                $insertGurantorArray['EMAIL'] = $guarantorEmail;
                $insertGurantorArray['ADDRESS_LINE1'] = $guarantorAddressLine1;
                $insertGurantorArray['ADDRESS_LINE2'] = $guarantorAddressLine2;
                $insertGurantorArray['ADDRESS_LINE3'] = $guarantorAddressLine3;
                $insertGurantorArray['FK_CITY_ID'] = $guarantorCityId;
                $insertGurantorArray['FK_STATE_ID'] = $guarantorStateId;
                $insertGurantorArray['FK_COUNTRY_ID'] = $guarantorCountryId;
                $insertGurantorArray['TYPE'] = 'Guarantor';
                $insertGurantorArray['RELATION'] = $guarantorRelation;
                $insertGurantorArray['PINCODE'] = $guarantorPincode;

                if (!empty($guarantorId)) {
                    $insertGurantorArray['CHANGED_BY'] = $userId;
                    $insertGurantorArray['CHANGED_BY_DATE'] = time();
                    $whereParentUpdate = "PK_PARENT_ID =" . $guarantorId;
                    $updateParentData = update_rec(PARENTSMASTER, $insertGurantorArray, $whereParentUpdate); // UPDATE IN PARENT_MASTER TABLE
                }

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['studentId'] = $studentId;
                $studentData = getStudentData($requestData); // GET ALL STUDENT DATA.
                $studentAllData = ($studentData['status'] == SCS) ? $studentData['data'] : $studentData['status'];

                $result = array("status" => SCS, "data" => $studentAllData);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTFAIL . " " . STUDENTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    } else {
        $result = array("status" => ERROR, "data" => $errorArr);
        http_response_code(400);
    }

    return $result;
}

// CHANGED BY KAVITA PATEL ON 08-06-2018 END
//ADD BY AKSHAT.07.08.2018 // LIST INVOICE DATA(BILLING HISTORY) API.

function billingHistory($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $whereInvoiceCond = "";
    if (!empty($studentId)) {
        $whereInvoiceCond = " AND IM.FK_STUDENT_ID ='" . $studentId . "'";
    }

//    echo  "SELECT CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME ) AS STUD_NAME,INVOICE_NAME,INVOICE_DATE FROM " . INVOICEMASTER . " IM INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =IM.FK_STUDENT_ID AND SM.DELETE_FLAG ='0' WHERE IM.DELETE_FLAG = '0' " . $whereInvoiceCond . " AND IM.FK_CLIENT_ID ='" . $clientId . "' "; die;

    $sqlInvoiceData = "SELECT CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME ) AS STUD_NAME,IM.INVOICE_NAME,IM.INVOICE_DATE,IM.TOTAL_CHARGES,IM.C_INVOICE_ID AS INVOICE_NUMBER,IM.INVOICE_TYPE FROM " . INVOICEMASTER . " IM INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =IM.FK_STUDENT_ID AND SM.DELETE_FLAG ='0' WHERE IM.DELETE_FLAG = '0' " . $whereInvoiceCond . " AND IM.FK_CLIENT_ID ='" . $clientId . "' ORDER BY IM.PK_INVOICE_ID DESC";  // QUERY FOR Invoice Data
    $resInvoiceData = fetch_rec_query($sqlInvoiceData); // RESULT FOR GET Invoice Data
    $invoiceArr = Array();
    if (count($resInvoiceData) > 0) {
        foreach ($resInvoiceData as $keyInvoice => $valueInvoice) { // Invoice Data FOREACH LOOP.
            $invoiceArr[$keyInvoice]['studName'] = $valueInvoice['STUD_NAME'];
            $invoiceArr[$keyInvoice]['invoiceNumber'] = $valueInvoice['INVOICE_NUMBER'];
            $invoiceArr[$keyInvoice]['invoiceType'] = $valueInvoice['INVOICE_TYPE'];
            $invoiceArr[$keyInvoice]['invoiceName'] = $valueInvoice['INVOICE_NAME'];
            $invoiceArr[$keyInvoice]['invoiceDate'] = $valueInvoice['INVOICE_DATE'];
            $invoiceArr[$keyInvoice]['totalCharges'] = $valueInvoice['TOTAL_CHARGES'];
        }
        $result = array("status" => SCS, "data" => array_values($invoiceArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.07.08.2018 // LIST INVOICE DATA (BILLING HISTORY) API.
//ADD BY AKSHAT.07.8.2018.//FUNCTION FOR Add/Edit Events Booking Detail. 
function addEditstudentEventsBooking($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($qty) && !empty($itemId) && !empty($studentId)) { // CHECK REQURIED FIELD CONDITION
        $todayDate = date("Y-m-d");
        $sqlCheckSeatsExist = " SELECT IM.ITEM_NAME,IM.ITEM_PRICE,IM.QTY AS TOTAL_SEATS,IF(SUM(EB.QTY)>0,SUM(EB.QTY),0) AS TOTAL_BOOKING  FROM " . ITEMMASTER . " IM LEFT JOIN " . EVENTSBOOKING . " EB ON IM.PK_ITEM_ID =EB.FK_ITEM_ID WHERE IM.PK_ITEM_ID='" . $itemId . "' AND IM.DELETE_FLAG ='0' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID ='" . $orgId . "' AND IM.ITEM_TYPE='EVENTS' AND ITEM_VALIDITY_END_DATE >='" . $todayDate . "' GROUP BY(EB.FK_ITEM_ID)";

        $resCheckSeatsExist = fetch_rec_query($sqlCheckSeatsExist); // RESULT FOR PROPERTY NAME IS ALREADY AVAILABLE OR NOT.
        if (count($resCheckSeatsExist) > 0) {

            $itemName = $resCheckSeatsExist[0]['ITEM_NAME'];
            $itemPrice = $resCheckSeatsExist[0]['ITEM_PRICE'];
            $totalSeats = $resCheckSeatsExist[0]['TOTAL_SEATS'];
            $totalBooking = $resCheckSeatsExist[0]['TOTAL_BOOKING'];
            if ((intval($totalBooking) + intval($qty)) > intval($totalSeats)) {
                $totalSeatAvailabel = intval($totalSeats) - intval($totalBooking);
                $result = array("status" => BOOKINGISNOTAVAILABLE . " Because of total booking available is :" . intval($totalSeatAvailabel));
                http_response_code(400);
                return $result;
            } else {
                $msg = "";
// EVENTS BOOKING INSERT ARRAY.

                $insertBookingArray = Array(); //EVENTS  BOOKING ARRAY CREATE
                $insertBookingArray['FK_CLIENT_ID'] = $clientId;
                $insertBookingArray['FK_ORG_ID'] = $orgId;
                $insertBookingArray['FK_STUD_ID'] = $studentId;
                $insertBookingArray['FK_ITEM_ID'] = $itemId;
                $insertBookingArray['BOOKING_DATE'] = date("Y-m-d");
                $insertBookingArray['BOOKING_NO'] = generateId(EVENTSBOOKING, "", $clientId);
                ;
                $insertBookingArray['QTY'] = $qty;
                $insertBookingArray['CREATED_BY'] = $userId;
                $insertBookingArray['CREATED_BY_DATE'] = time();

                $insertBooking = insert_rec(EVENTSBOOKING, $insertBookingArray); // INSERT RECORD IN PROPERTY_MASTER TABLE.          
                if (!isset($insertBooking['lastInsertedId']) || $insertBooking['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . EVENTSBOOKING;
                } else {
                    $checkItemData = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_PRICE,ITEM_TYPE,CGST,SGST,IGST FROM " . ITEMMASTER . " WHERE DELETE_FLAG='0' AND PK_ITEM_ID ='" . $itemId . "'";
                    $resCheckItemData = fetch_rec_query($checkItemData);
                    if (count($resCheckItemData) > 0) {
                        foreach ($resCheckItemData as $keyItem => $valueItem) {

                            $sqlStudentData = "SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUD_NAME FROM " . STUDENTMASTER . " WHERE DELETE_FLAG='0' AND PK_STUD_ID ='" . $studentId . "'"; //QUERY FOR  FETCH STUDENT NAME 
                            $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR  FETCH STUDENT NAME   



                            $itemName = $valueItem['ITEM_NAME'];
                            $itemPrice = $valueItem['ITEM_PRICE'];
                            $itemCgst = $valueItem['CGST'];
                            $itemSgst = $valueItem['SGST'];
                            $itemIgst = $valueItem['IGST'];
                            $qtyOfSession = 1;
                            $calItemBasicTPrice = $itemPrice * $qtyOfSession;
                            $cgstAmt = ($itemCgst > 0) ? ($calItemBasicTPrice * $itemCgst) / 100 : 0;
                            $sgstAmt = ($itemSgst > 0) ? ($calItemBasicTPrice * $itemSgst) / 100 : 0;
                            $igstAmt = ($itemIgst > 0) ? ($calItemBasicTPrice * $itemIgst) / 100 : 0;

                            $calTotalTax = $cgstAmt + $sgstAmt + $igstAmt;
                            $calItemTotalPrice = $calItemBasicTPrice + $calTotalTax;

                            $insertChargePosting = Array();
                            $insertChargePostingArray = Array(); //CHARGE POSTING ARRAY CREATE
                            $insertChargePostingArray['FK_CLIENT_ID'] = $clientId;
                            $insertChargePostingArray['FK_ORG_ID'] = $orgId;
                            $insertChargePostingArray['TYPEOFCUST'] = 'student';
                            $insertChargePostingArray['FK_ITEM_ID'] = $itemId;
                            $insertChargePostingArray['REF_ITEM_ID'] = $itemId;
                            $insertChargePostingArray['ITEM_NAME'] = $itemName;
                            $insertChargePostingArray['CHARGE_DATE'] = date("Y-m-d");
                            $insertChargePostingArray['STUDENT_ID'] = $studentId;
                            $insertChargePostingArray['STUDENT_FULL_NAME'] = $resStudentData['0']['STUD_NAME'];
                            $insertChargePostingArray['QTY_OF_SESSION'] = $qty;
                            $insertChargePostingArray['BIILED_SESSION'] = $qty;
                            $insertChargePostingArray['TAX_CGST'] = $itemCgst;
                            $insertChargePostingArray['TAX_SGST'] = $itemSgst;
                            $insertChargePostingArray['TAX_IGST'] = $itemIgst;
                            $insertChargePostingArray['ITEM_BASE_PRICE'] = $itemPrice;
                            $insertChargePostingArray['TOTAL_TAX'] = $calTotalTax;
                            $insertChargePostingArray['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;

                            $insertChargePosting = insert_rec(CHARGEMASTER, $insertChargePostingArray); // INSERT RECORD IN CHARGE_MASTER TABLE.
                            $result = array("status" => SCS);
                            http_response_code(200);
                        }
                    }
                }
            }
        } else {

            $result = array("status" => EVENTNOTFOUND);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//END BY AKSHAT.07.08.2018.

function forgetPasswordFP_BL($postData) {

//print_r($postData); exit;

    $email = isset($postData['postData']['email']) ? addslashes(trim($postData['postData']['email'])) : "";
    $mobileNo = isset($postData['postData']['mobileNo']) ? addslashes(trim($postData['postData']['mobileNo'])) : "";
    $membershipNo = isset($postData['postData']['membershipNo']) ? addslashes(trim($postData['postData']['membershipNo'])) : "";
    ;


    $whereMobileCond = $whereMembershipCond = $whereEmialCond = "";

    if (!empty($mobileNo)) {
        $whereMobileCond = " AND MOBILE_NO='" . $mobileNo . "'";
    }
    if (!empty($membershipNo)) {
        $whereMembershipCond = " AND MEMBERSHIP_NUMBER='" . $membershipNo . "'";
    }
    if (!empty($email)) {
        $whereEmialCond = " AND EMAIL='" . $email . "'";
    }

    $sqlCheckName = "SELECT PK_STUD_ID,FK_CLIENT_ID,EMAIL,MOBILE_NO,MEMBERSHIP_NUMBER,IS_ACTIVE,FIRST_NAME,LAST_NAME FROM " . STUDENTMASTER . " WHERE DELETE_FLAG = 0 " . $whereMobileCond . $whereMembershipCond . $whereEmialCond . " LIMIT 0,1";

    $resultCheckName = fetch_rec_query($sqlCheckName);

    if (count($resultCheckName) > 0) {

        $newPassword = 'Abc@1234';
//var_dump($newPassword); die;
// NOW MAKE THE URL 
        $request = $resultCheckName[0]['PK_STUD_ID']; // CHANGED BY KAUSHA SHAH ON 20-08-2019

        $url = APP_STUD_WEB_ROOT . "/activateUser.php?data=" . encstr($request); // CHANGED BY KAUSHA SHAH ON 20-08-2019
// START GETTING MAIL CONFIG 
        $mailConfig = "SELECT * FROM " . MAILSMSCONFIG . " WHERE FK_CLIENT_ID = '" . $resultCheckName[0]['FK_CLIENT_ID'] . "'";

        $resultConfig = fetch_rec_query($mailConfig);

        if (count($resultConfig) > 0) {

            $sendgrid_username = "arunnagpal";
            $sendgrid_password = "devil1502";

            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

//            print_r($sendGrid);            exit();
            $fromAddress = $resultConfig[0]['MAILCONFIG_USERNAME'];
            $sfName = $resultConfig[0]['SENDERFNAME'];
            $slname = $resultConfig[0]['SENDERLNAME'];
            $fromName = $sfName . "" . $slname;
            $reciverAddress = $resultCheckName[0]['EMAIL'];
//print_r($reciverAddress); die;
            $reciverName = $resultCheckName[0]['FIRST_NAME'] . $resultCheckName[0]['LAST_NAME'];

            $subject = "Forget Password : Link to activate Account.";
            $messageBody = "<div style = 'height:100px'>"
                    . "<div style = 'padding:10px'>Password : " . $newPassword . "</div>"
                    . "<div style = 'padding:10px'>Please <a href=" . $url . ">Click</a> "
                    . "on this link to activate your account.</div>";
//$sendMailStaus = sendMail($host, $port, $username, $password, $fromAddress, $subject, $messageBody, $reciverAddress, "", "", $fromName, $reciverName, "", "");
            $email = new SendGrid\Email();
            $email->addTo($reciverAddress);
            // $email->addTo("kausha.shah@lithe.in");
            $email->addToName($reciverName);
            $email->setFrom($fromAddress);
            $email->setFromName($fromName);
            $email->setSubject($subject);
            $email->setHtml($messageBody);
            //print_r($email); die;
            $response = $sendGrid->send($email);
            $responseMail = $response->getBody();
            if ($responseMail['message'] == "success") {

                // START UPDATING DATA INTO USER TABLE
                $fieldsValue['IS_ACTIVE'] = 0; // ADDED BY KAUSHA SHAH ON 20-08-2018
                $fieldsValue['PASSWORD'] = password_hash(md5(trim(($newPassword))), PASSWORD_DEFAULT); // CHANGED BY KAUSHA SHAH ON 20-08-2018
//print_r($fieldsValue); die;
// WHERE 

                $where = "(EMAIL = '" . $resultCheckName[0]['EMAIL'] . "' OR  MOBILE_NO='" . $resultCheckName[0]['MOBILE_NO'] . "' OR MEMBERSHIP_NUMBER='" . $resultCheckName[0]['MEMBERSHIP_NUMBER'] . "' ) AND FK_CLIENT_ID = '" . $resultCheckName[0]['FK_CLIENT_ID'] . "'";
//print_r($where); die;
                $updateUserMaster = update_rec(STUDENTMASTER, $fieldsValue, $where);

//                $result = array("status" => SCS);
            }
        }
        $result = array("status" => SCS);
        http_response_code(200);
    } else {

        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

/* =============== END - FORGET PASSSOWRD FUNCTION ================ */

/* ============== START - RANDOM NUMBER FUNCTION ================= */

function randomAlphaNum($length) {

    $rangeMin = pow(36, $length - 1); //smallest number to give length digits in base 36 
    $rangeMax = pow(36, $length) - 1; //largest number to give length digits in base 36 
    $base10Rand = mt_rand($rangeMin, $rangeMax); //get the random number 

    $newRand = base_convert($base10Rand, 10, 36); //convert it 
//print_r($newRand); 
//die;
    return $newRand; //spit it out 
}

function userforgetPasswordFP_BL($postData) {


    $email = isset($postData['postData']['userName']) ? addslashes(trim($postData['postData']['userName'])) : "";
    $whereEmialCond = "";
    if (!empty($email)) {
        $whereEmialCond = " AND EMAIL ='" . $email . "'";
    }

    $sqlCheckName = "SELECT PK_USER_ID,FK_CLIENT_ID,FULLNAME,USERNAME,IS_ACTIVE,EMAIL FROM " . USERMASTER . " WHERE DELETE_FLAG = 0 " . $whereEmialCond . " LIMIT 0,1";

    $resultCheckName = fetch_rec_query($sqlCheckName);

    if (count($resultCheckName) > 0) {

        $newPassword = 'Abc@1234';

        $request = $resultCheckName[0]['PK_USER_ID'];

        $url = APP_ADMIN_WEB_ROOT . "/activateUser.php?data=" . encstr($request);

// START GETTING MAIL CONFIG 
        $mailConfig = "SELECT * FROM " . MAILSMSCONFIG . " WHERE FK_CLIENT_ID = '" . $resultCheckName[0]['FK_CLIENT_ID'] . "'";

        $resultConfig = fetch_rec_query($mailConfig);

        if (count($resultConfig) > 0) {

            $sendgrid_username = "arunnagpal";
            $sendgrid_password = "devil1502";

            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

//            print_r($sendGrid);            exit();
            $fromAddress = $resultConfig[0]['MAILCONFIG_USERNAME'];
            $sfName = $resultConfig[0]['SENDERFNAME'];
            $slname = $resultConfig[0]['SENDERLNAME'];
            $fromName = $sfName . "" . $slname;
            $reciverAddress = $resultCheckName[0]['EMAIL'];
//print_r($reciverAddress); die;
            $reciverName = $resultCheckName[0]['FULLNAME'];

            $subject = "Forget Password : Link to activate Account.";
            $messageBody = "<div style = 'height:100px'>"
                    . "<div style = 'padding:10px'>Password : " . $newPassword . "</div>"
                    . "<div style = 'padding:10px'>Please <a href=" . $url . ">Click</a> "
                    . "on this link to activate your account.</div>";
//$sendMailStaus = sendMail($host, $port, $username, $password, $fromAddress, $subject, $messageBody, $reciverAddress, "", "", $fromName, $reciverName, "", "");
            $email = new SendGrid\Email();
            $email->addTo($reciverAddress);
            // $email->addTo("kausha.shah@lithe.in");
            $email->addToName($reciverName);
            $email->setFrom($fromAddress);
            $email->setFromName($fromName);
            $email->setSubject($subject);
            $email->setHtml($messageBody);
            //print_r($email); die;
            $response = $sendGrid->send($email);
            $responseMail = $response->getBody();
            if ($responseMail['message'] == "success") {

                // START UPDATING DATA INTO USER TABLE
                $fieldsValue['IS_ACTIVE'] = 0;
                $fieldsValue['PASSWORD'] = password_hash(md5(trim(($newPassword))), PASSWORD_DEFAULT);

                $where = "EMAIL = '" . $resultCheckName[0]['EMAIL'] . "' AND FK_CLIENT_ID = '" . $resultCheckName[0]['FK_CLIENT_ID'] . "'";

                $updateUserMaster = update_rec(USERMASTER, $fieldsValue, $where);
            }
        }
        $result = array("status" => SCS);
        http_response_code(200);
    } else {

        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

/* ============== END - RANDOM NUMBER FUNCTION ================= */

//ADD BY DEVIKA.23.7.2018 // LIST INVOICE DATA API.

function ledgerHistory($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    $whereInvoiceCond = "";
    if (!empty($studentId)) {
        $whereInvoiceCond = " AND SM.PK_STUD_ID ='" . $studentId . "'";
    }
    $sqlStudentData = "SELECT SM.FK_EPOS_CUST_ID,SM.PK_STUD_ID,SM.OPEN_BALANCE,SM.OPEN_BALANCE_TYPE,SM.RECEIPT_AMT,SM.UNBILLED_AMT,SM.BILL_AMT,SM.CLOSINGBAL_AMT,SM.CLOSINGBAL_TYPE,SM.OPEN_BALANCE FROM " . STUDENTMASTER . " SM  WHERE SM.DELETE_FLAG =0  " . $whereInvoiceCond . " AND SM.FK_CLIENT_ID ='" . $clientId . "' ";  // QUERY FOR STUDENT DATA
    $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR STUDENT DATA

    if (count($resStudentData) > 0) {
        $studArr = Array();
        foreach ($resStudentData as $keyStudent => $valStudent) {
            $studArr[$keyStudent]['openBalance'] = $valStudent['OPEN_BALANCE'];
            $studArr[$keyStudent]['openBalanceType'] = $valStudent['OPEN_BALANCE_TYPE'];
            $studArr[$keyStudent]['receiptAmt'] = $valStudent['RECEIPT_AMT'];
            $studArr[$keyStudent]['unbillerAmt'] = $valStudent['UNBILLED_AMT'];
            $studArr[$keyStudent]['billedAmt'] = $valStudent['BILL_AMT'];
            $studArr[$keyStudent]['closingBalAmt'] = $valStudent['CLOSINGBAL_AMT'];
            $studArr[$keyStudent]['closingBalTypeAmt'] = $valStudent['CLOSINGBAL_TYPE'];

            $sqlInvoiceData = "SELECT IM.INVOICE_NAME,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.TOTAL_CHARGES,IM.OPEN_BALANCE_TYPE FROM " . INVOICEMASTER . " IM  WHERE IM.DELETE_FLAG =0  AND IM.FK_STUDENT_ID='" . $valStudent['PK_STUD_ID'] . "' AND IM.FK_CLIENT_ID ='" . $clientId . "'";  // QUERY FOR Invoice Data


            $resInvoiceData = fetch_rec_query($sqlInvoiceData); // RESULT FOR GET Invoice Data
            $invoiceArr = Array();
            if (count($resInvoiceData) > 0) {
                foreach ($resInvoiceData as $keyInvoice => $valueInvoice) { // Invoice Data FOREACH LOOP.
                    $invoiceArr[$keyInvoice]['ledgerDate'] = $valueInvoice['INVOICE_DATE'];
                    $invoiceArr[$keyInvoice]['ledgerDateTime'] = strtotime($valueInvoice['INVOICE_DATE']);
                    $invoiceArr[$keyInvoice]['voucherNo'] = $valueInvoice['C_INVOICE_ID'];
                    $invoiceArr[$keyInvoice]['voucherType'] = 'Sales Invoice';
                    $invoiceArr[$keyInvoice]['invoiceName'] = $valueInvoice['INVOICE_NAME'];
                    $invoiceArr[$keyInvoice]['debit'] = $valueInvoice['TOTAL_CHARGES'];
                    $invoiceArr[$keyInvoice]['credit'] = "-";
                }
            }

            $sqlPaymentData = "SELECT PAID_AMOUNT,C_PAYMENT_ID,PAYMENT_DATE FROM " . PAYMENTMASTER . " WHERE FK_STUDENT_ID='" . $valStudent['PK_STUD_ID'] . "' AND DELETE_FLAG='0' AND FK_CLIENT_ID='" . $clientId . "'";
            $resPaymentData = fetch_rec_query($sqlPaymentData); // RESULT FOR GET Invoice Data
            $paymentArr = Array();
            if (count($resPaymentData) > 0) {
                foreach ($resPaymentData as $keyPayment => $valuePayment) {
                    $paymentArr[$keyPayment]['ledgerDate'] = $valuePayment['PAYMENT_DATE'];
                    $paymentArr[$keyPayment]['ledgerDateTime'] = strtotime($valuePayment['PAYMENT_DATE']);
                    $paymentArr[$keyPayment]['voucherNo'] = $valuePayment['C_PAYMENT_ID'];
                    $paymentArr[$keyPayment]['voucherType'] = 'Receipt';
                    $paymentArr[$keyPayment]['invoiceName'] = '';
                    $paymentArr[$keyPayment]['credit'] = $valuePayment['PAID_AMOUNT'];
                    $paymentArr[$keyPayment]['debit'] = "-";
                }
            }
            $fnbSpaceId = $laundryId = 0;
            $sqlClientData = "SELECT FNB_SPACE_ID,FNB_LAUNDRY_ID FROM " . CLIENTMASTER . " WHERE PK_CLIENT_ID ='" . $clientId . "' AND DELETE_FLAG ='0'";
            $resClientData = fetch_rec_query($sqlClientData);
            if (count($resClientData) > 0) {
                $fnbSpaceId = $resClientData[0]['FNB_SPACE_ID'];
                $laundryId = $resClientData[0]['FNB_LAUNDRY_ID'];
            }

            $sqlInvoiceData = "SELECT CS.FK_SPACE_ID,CS.BILL_NO,CS.BILL_DATE,CS.FINAL_AMOUNT FROM " . CENTRALISEEPOSSALES . " CS  WHERE CS.DELETE_FLAG =0  AND CS.MEMBERSHIP_NUMBER='" . $valStudent['FK_EPOS_CUST_ID'] . "' AND CS.FK_CLIENT_ID ='" . $clientId . "' AND CS.FINAL_AMOUNT > 0";  // QUERY FOR Invoice Data


            $resInvoiceData = fetch_rec_query($sqlInvoiceData); // RESULT FOR GET Invoice Data
            $centrailiseArr = array();
            if (count($resInvoiceData) > 0) {
                foreach ($resInvoiceData as $keyInvoice => $valueInvoice) { // Invoice Data FOREACH LOOP.
                    $centrailiseArr[$keyInvoice]['ledgerDate'] = $valueInvoice['BILL_DATE'];
                    $centrailiseArr[$keyInvoice]['ledgerDateTime'] = strtotime($valueInvoice['BILL_DATE']);
                    $centrailiseArr[$keyInvoice]['voucherNo'] = $valueInvoice['BILL_NO'];
                    $centrailiseArr[$keyInvoice]['voucherType'] = 'Sales';
                    $centrailiseArr[$keyInvoice]['invoiceName'] = ($valueInvoice['FK_SPACE_ID'] == $fnbSpaceId) ? "FNB" : "Laundry";
                    ;
                    $centrailiseArr[$keyInvoice]['debit'] = $valueInvoice['FINAL_AMOUNT'];
                    $centrailiseArr[$keyInvoice]['credit'] = "-";
                }
            }



            $studentLedgerArr = array_merge($invoiceArr, $paymentArr, $centrailiseArr);
            $studentLedgerArr = multid_sort($studentLedgerArr, 'ledgerDateTime');
            if (!empty($invoiceArr) || !empty($paymentArr)) {
                $studArr[$keyStudent]['ledgerData'] = $studentLedgerArr;
            } else {
                $studArr[$keyStudent]['ledgerData'] = NORECORDS;
            }

            $result = array("status" => SCS, "data" => array_values($studArr));
            http_response_code(200);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.23.7.2018 // LIST INVOICE DATA API.
//ADD BY DEVIKA.23.7.2018 // LIST load auth data at login time DATA API.
function loadAuthStudent_BL($postdata) {
// print_r($postdata); exit;
    $sessionuserid = isset($postdata['postData']['studentId']) ? addslashes(trim($postdata['postData']['studentId'])) : ""; // logged in user
    $clientid = isset($postdata['postData']['clientId']) ? addslashes(trim($postdata['postData']['clientId'])) : "";
    $orgId = isset($postdata['postData']['orgId']) ? addslashes(trim($postdata['postData']['orgId'])) : "";

//query for user details
    if ($sessionuserid != -1 && !empty($sessionuserid) && $sessionuserid != "") {

        $query_User = "SELECT SM.PK_STUD_ID, SM.FK_CLIENT_ID, SM.USER_NAME,CM.CLIENTNAME,CM.CLIENT_VAL_END_DATE FROM " . STUDENTMASTER . " SM INNER JOIN " . CLIENTMASTER . " CM ON CM.PK_CLIENT_ID = SM.FK_CLIENT_ID  WHERE `PK_STUD_ID` ='" . $sessionuserid . "' AND CM.PK_CLIENT_ID = '" . $clientid . "'  AND SM.DELETE_FLAG = 0 ";

        // AND SM.FK_ORG_ID ='".$orgId."'
        $fetch_userdetails = fetch_rec_query($query_User);
    } else if ($sessionuserid == -1) {//LITHE USER
//        $query_Client = "SELECT CM.CLIENT_TYPE clientType FROM " . CLIENTMASTER . " CM  WHERE CM.PK_CLIENT_ID = '" . $clientid . "' AND CM.DELETE_FLAG = 0 ";
        $query_Client = "SELECT CM.CLIENTNAME FROM " . CLIENTMASTER . " CM  WHERE CM.PK_CLIENT_ID = '" . $clientid . "' AND CM.DELETE_FLAG = 0 ";
        $fetch_userdetails_client = fetch_rec_query($query_Client);

        $fetch_userdetails[0]['PK_STUD_ID'] = -1;
        $fetch_userdetails[0]['FK_CLIENT_ID'] = $clientid;
        $fetch_userdetails[0]['USER_NAME'] = "MasterUser";
        $fetch_userdetails[0]['FULLNAME'] = "MasterUser";
        $fetch_userdetails[0]['USER_TYPE'] = -1;
//        $fetch_userdetails[0]['clientType'] = isset($fetch_userdetails_client[0]['clientType']) ? $fetch_userdetails_client[0]['clientType'] : "";
        $fetch_userdetails[0]['CLIENTNAME'] = "Lithe Technologies Pvt Ltd.";
    } else {//STUDENT PORTAL WITHOUT LOGIN
        $query_Client = "SELECT CM.CLIENTNAME FROM " . CLIENTMASTER . " CM  WHERE CM.PK_CLIENT_ID = '" . $clientid . "' AND CM.DELETE_FLAG = 0 ";
        $fetch_userdetails_client = fetch_rec_query($query_Client);

        $fetch_userdetails[0]['PK_STUD_ID'] = -1;
        $fetch_userdetails[0]['FK_CLIENT_ID'] = $clientid;
        $fetch_userdetails[0]['USER_NAME'] = "MasterUser";
        $fetch_userdetails[0]['FULLNAME'] = "MasterUser";
        $fetch_userdetails[0]['USER_TYPE'] = -1;
//        $fetch_userdetails[0]['clientType'] = isset($fetch_userdetails_client[0]['clientType']) ? $fetch_userdetails_client[0]['clientType'] : "";
        $fetch_userdetails[0]['CLIENTNAME'] = isset($fetch_userdetails_client[0]['CLIENTNAME']) ? $fetch_userdetails_client[0]['CLIENTNAME'] : "";
    }

    if (count($fetch_userdetails) > 0) {
        $sqlCountryListing = "SELECT PK_COUNTRY_ID,COUNTRY_SHORT_CODE,COUNTRY_NAME,COUNTRY_PHONE_CODE FROM " . COUNTRY . "";
        $resultCountryListing = fetch_rec_query($sqlCountryListing);
        if (count($resultCountryListing) > 0) {

            $finalCountry = array();
            foreach ($resultCountryListing as $keyCountry => $valueCountry) {

                $finalCountry[$keyCountry]['countryId'] = $valueCountry['PK_COUNTRY_ID'];
                $finalCountry[$keyCountry]['countryShortCode'] = $valueCountry['COUNTRY_SHORT_CODE'];
                $finalCountry[$keyCountry]['countryName'] = $valueCountry['COUNTRY_NAME'];
                $finalCountry[$keyCountry]['countryPhoneCode'] = $valueCountry['COUNTRY_PHONE_CODE'];

                // GET CITY COUNT FOR MOBILE APP
                $sqlCities = "SELECT COUNT(PK_CITY_ID) TOTALCITIES FROM " . CITY . " CM WHERE CM.FK_COUNTRY_ID = '" . $valueCountry['PK_COUNTRY_ID'] . "' ";
                $resultGetCitiesCount = fetch_rec_query($sqlCities);
                $finalCountry[$keyCountry]['totalCitiesCount'] = $resultGetCitiesCount[0]['TOTALCITIES'];
            }
        }

        $query_MedicalCondition = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'MEDICAL_CONDITION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_MedicalCondition = fetch_rec_query($query_MedicalCondition);

//query for getting student relation by client wise from client_lookup
        $query_StudentRelation = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'STUDENT_RELATION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_StudentRelation = fetch_rec_query($query_StudentRelation);

        $query_NonAvalingService = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_TYPE FROM " . ITEMMASTER . " WHERE ITEM_TYPE='SUBSC' AND ITEM_NAME='Meals' AND FK_CLIENT_ID = '" . $clientid . "' AND DELETE_FLAG = '0' ORDER BY PK_ITEM_ID ASC";
        $fetch_NonAvalingService = fetch_rec_query($query_NonAvalingService);

//query for getting department name by client wise from client_lookup
        $query_department = "SELECT PK_LOOKUP_ID ID,VALUE   FROM " . LOOKUPMASTER . " WHERE  TYPE = 'DEPARTMENT' AND DELETE_FLAG = '0' ORDER BY VALUE ASC";
        $fetch_department = fetch_rec_query($query_department);



        $clientLogo = isset($fetch_userdetails[0]['LOGO']) ? basename($fetch_userdetails[0]['LOGO']) : "no-image.png";
//query for calendar configuration
//create array for JSON data
        $data = array("PK_STUD_ID" => $fetch_userdetails[0]['PK_STUD_ID'], "FK_CLIENT_ID" => $fetch_userdetails[0]['FK_CLIENT_ID'], "USER_NAME" => $fetch_userdetails[0]['USER_NAME'], "clientName" => $fetch_userdetails[0]['CLIENTNAME'], "countryListing" => $finalCountry, "medicalCondition" => $fetch_MedicalCondition, "studentRelation" => $fetch_StudentRelation, "NonAvailingService" => $fetch_NonAvalingService, "department" => $fetch_department);
//$data             = array($fetch_userdetails,"calconfig"   =>$fetch_caldetails,"maxtimezone" =>5,"city"        =>$fetch_citydetails,"permission"  =>$fetch_roledetails);
//print_r($fetch_userdetails); EXIT;
//        print_r($data);
        $result = array("status" => SCS, "data" => $data);
        http_response_code(200);
    } else {
        $result = array("status" => NOAUTH);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.23.7.2018
//ADD BY DEVIKA.23.7.2018.
function studentProfileData($postData) {

    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    /* Student id condition start */
    $whereStudentCond = "";
    if (!empty($studentId)) {
        $whereStudentCond = " AND SM.PK_STUD_ID ='" . $studentId . "'";
    }
    /* Student id condition End */
// ADDDED BY KAVITA PATEL ON 06-08-2018 START
    $countryArr = $stateArr = $cityArr = $clientLookupArr = $itemLookupArr = array();
    $countrySql = fetch_rec_query("SELECT PK_COUNTRY_ID,COUNTRY_NAME FROM " . COUNTRY . "");
    if (count($countrySql) > 0) {
        foreach ($countrySql as $countrySqlData) {
            $countryArr[$countrySqlData['PK_COUNTRY_ID']] = $countrySqlData['COUNTRY_NAME'];
        }
    }
    $stateSql = fetch_rec_query("SELECT PK_STATE_ID,STATE_NAME FROM " . STATE . "");
    if (count($stateSql) > 0) {
        foreach ($stateSql as $stateSqlData) {
            $stateArr[$stateSqlData['PK_STATE_ID']] = $stateSqlData['STATE_NAME'];
        }
    }
    $citySql = fetch_rec_query("SELECT PK_CITY_ID,CITY_NAME FROM " . CITY . "");
    if (count($citySql) > 0) {
        foreach ($citySql as $citySqlData) {
            $cityArr[$citySqlData['PK_CITY_ID']] = $citySqlData['CITY_NAME'];
        }
    }
    $cLookupSql = fetch_rec_query("SELECT PK_CLT_LKUP_ID,DISPLAY_VALUE FROM " . CLIENTLOOKUP . "");
    if (count($cLookupSql) > 0) {
        foreach ($cLookupSql as $cLookupSqlData) {
            $clientLookupArr[$cLookupSqlData['PK_CLT_LKUP_ID']] = $cLookupSqlData['DISPLAY_VALUE'];
        }
    }
    $itemMasterSql = fetch_rec_query("SELECT PK_ITEM_ID,ITEM_NAME FROM " . ITEMMASTER . "");
    if (count($itemMasterSql) > 0) {
        foreach ($itemMasterSql as $itemMasterSqlData) {
            $itemLookupArr[$itemMasterSqlData['PK_ITEM_ID']] = $itemMasterSqlData['ITEM_NAME'];
        }
    }
// ADDDED BY KAVITA PATEL ON 06-08-2018 END

    $sqlStudentData = "SELECT SM.PK_STUD_ID,SM.FK_CLIENT_ID,SM.USER_NAME,SM.MEMBERSHIP_NUMBER,SM.FK_ORG_ID,SM.FIRST_NAME,SM.LAST_NAME,SM.FK_COUNTRY_ID,SM.FK_STATE_ID,SM.FK_CITY_ID,SM.MOBILE_NO,SM.EMAIL,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.COLLEGE_NAME,SM.COLLEGE_ADDRESS_LINE1,SM.COLLEGE_ADDRESS_LINE2,SM.COLLEGE_COUNTRY_ID,SM.COLLEGE_STATE_ID,SM.COLLEGE_CITY_ID,SM.COLLEGE_ADDRESS_LINE3,SM.COLLEGE_PINCODE,SM.GENDER,SM.BLOOD_GROUP,SM.COURSE,SM.ACDEMIC_START_DATE,SM.ACDEMIC_END_DATE,SM.SEMESTER,SM.MEDICAL_CONDITION,SM.AADHAR_NO,SM.TYPE_OF_ROOM_REQUIRED,SM.EMERGENCY_CONTACT_PERSON_NO,SM.BIRTHDATE,SM.PASSPORT_NO,AT.ROOM_NAME,AT.TENANCY_START_DATE,AT.TENANCY_END_DATE,AT.TENANCY_DEPOSIT,AT.TENANCY_RENT,AT.FOOD_PREFERENCE,AT.BED_NO,AT.FK_ROOM_ID,AT.FACILITY_ALLOCATION,AT.ASSET_ISSUED,SM.EMERGENCY_CONTACT_PERSON_NAME,SM.PASSPORT_NO,DATE_FORMAT(FROM_UNIXTIME(SM.CREATED_BY_DATE), '%Y-%m-%d') AS ENROLLMENT_DATE,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,PM.PK_PARENT_ID AS parentId,PM.PARENT_NAME AS parentName,PM.USER_NAME AS ParentUserName,PM.MEMBERSHIP_NUMBER AS parentMembershipNumber,PM.MOBILE_NO1 AS parentMobileNo,PM.EMAIL AS parentEmail,PM.ADDRESS_LINE1 AS parentAddress1,PM.ADDRESS_LINE2 AS parentAddress2,PM.ADDRESS_LINE3 AS parentAddress3,PM.FK_CITY_ID AS parentCityId,PM.FK_STATE_ID AS parentStateId,PM.FK_COUNTRY_ID AS parentCountryId,PM.TYPE AS parentType,PM.RELATION AS parentRelation,GM.RELATION AS guarantorRelationId,GM.PK_PARENT_ID AS guarantorId,GM.PARENT_NAME AS guarantorName,GM.MOBILE_NO1 AS guarantorMobileNo1,GM.MOBILE_NO2 AS guarantorMobileNo2,GM.EMAIL AS guarantorEmail,GM.ADDRESS_LINE1 AS guarantorAddress1,GM.ADDRESS_LINE2 AS guarantorAddress2,GM.ADDRESS_LINE3 AS guarantorAddress3,GM.FK_CITY_ID AS guarantorCityId,GM.FK_STATE_ID AS guarantorStateId,GM.FK_COUNTRY_ID AS guarantorCountryId,GM.TYPE AS guarantorType,GM.RELATION AS guarantorRelation,PM.PINCODE AS parentPincode,GM.PINCODE AS guarantorPincode,SM.NATIONALITY,SM.IS_ACTIVE FROM " . STUDENTMASTER . " SM LEFT JOIN " . PARENTSMASTER . " PM ON PM.FK_STUD_ID =SM.PK_STUD_ID AND PM.TYPE ='Parent' LEFT JOIN " . PARENTSMASTER . " GM ON GM.FK_STUD_ID =SM.PK_STUD_ID AND GM.TYPE ='Guarantor' LEFT JOIN " . ALLOTMASTER . " AT ON SM.PK_STUD_ID=AT.FK_STUDENT_ID AND AT.DELETE_FLAG ='0' AND DATE(now()) >= AT.TENANCY_START_DATE AND DATE(now()) <= AT.TENANCY_END_DATE WHERE SM.DELETE_FLAG =0 " . $whereStudentCond . " AND SM.FK_CLIENT_ID ='" . $clientId . "' AND SM.FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR STUDENT DATA.

    $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR STUDENT DATA.
//echo count($resStudentData); die;
    if (count($resStudentData) > 0) {
        $studentArr = $assetVal = Array();
        $facilityVal = array();
        foreach ($resStudentData as $keyStudent => $valueStudent) {

            $countryName = (isset($countryArr[$valueStudent['FK_COUNTRY_ID']]) && $countryArr[$valueStudent['FK_COUNTRY_ID']] != '') ? trim($countryArr[$valueStudent['FK_COUNTRY_ID']]) : ""; // GET COUNTRY NAME
            $stateName = (isset($stateArr[$valueStudent['FK_STATE_ID']]) && $stateArr[$valueStudent['FK_STATE_ID']] != '') ? trim($stateArr[$valueStudent['FK_STATE_ID']]) : ""; // GET STATE NAME
            $cityName = (isset($cityArr[$valueStudent['FK_CITY_ID']]) && $cityArr[$valueStudent['FK_CITY_ID']] != '') ? trim($cityArr[$valueStudent['FK_CITY_ID']]) : ""; // GET CITY NAME
            $guarantorStateName = (isset($stateArr[$valueStudent['guarantorStateId']]) && $stateArr[$valueStudent['guarantorStateId']] != '') ? trim($stateArr[$valueStudent['guarantorStateId']]) : ""; // GET guarantor STATE NAME
            $guarantorCityName = (isset($cityArr[$valueStudent['guarantorCityId']]) && $cityArr[$valueStudent['guarantorCityId']] != '') ? trim($cityArr[$valueStudent['guarantorCityId']]) : "";  // GET GUARANTOR CITY NAME
            $collegeStateName = (isset($stateArr[$valueStudent['COLLEGE_STATE_ID']]) && $stateArr[$valueStudent['COLLEGE_STATE_ID']] != '') ? trim($stateArr[$valueStudent['COLLEGE_STATE_ID']]) : ""; // GET COLLEGE STATE NAME
            $collegeCityName = (isset($cityArr[$valueStudent['COLLEGE_CITY_ID']]) && $cityArr[$valueStudent['COLLEGE_CITY_ID']] != '') ? trim($cityArr[$valueStudent['COLLEGE_CITY_ID']]) : ""; // GET COLLEGE CITY NAME
            $itemName = (isset($clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']]) && $clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']] != '') ? trim($clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']]) : ""; // GET ITEM NAME
            $parentRelation = (isset($clientLookupArr[$valueStudent['parentRelation']]) && $clientLookupArr[$valueStudent['parentRelation']] != '') ? trim($clientLookupArr[$valueStudent['parentRelation']]) : ""; // GET PARENT RELATION


            $studentArr[$keyStudent]['studId'] = $valueStudent['PK_STUD_ID'];
            $studentArr[$keyStudent]['clientId'] = $valueStudent['FK_CLIENT_ID'];
            $studentArr[$keyStudent]['orgId'] = $valueStudent['FK_ORG_ID'];
            $studentArr[$keyStudent]['userName'] = $valueStudent['USER_NAME'];
            $studentArr[$keyStudent]['firstName'] = $valueStudent['FIRST_NAME'];
            $studentArr[$keyStudent]['lastName'] = $valueStudent['LAST_NAME'];
            $studentArr[$keyStudent]['memberShipNumber'] = $valueStudent['MEMBERSHIP_NUMBER'];
            $studentArr[$keyStudent]['studentName'] = $valueStudent['FIRST_NAME'] . " " . $valueStudent['LAST_NAME'];
            $studentArr[$keyStudent]['studAddress'] = $valueStudent['ADDRESS_LINE1'] . ", " . $valueStudent['ADDRESS_LINE2'] . ", " . $valueStudent['ADDRESS_LINE3'];
            $studentArr[$keyStudent]['countryId'] = $valueStudent['FK_COUNTRY_ID'];
            $studentArr[$keyStudent]['countryName'] = $countryName;
            $studentArr[$keyStudent]['stateId'] = $valueStudent['FK_STATE_ID'];
            $studentArr[$keyStudent]['stateName'] = $stateName;
            $studentArr[$keyStudent]['cityId'] = $valueStudent['FK_CITY_ID'];
            $studentArr[$keyStudent]['cityName'] = $cityName;
            $studentArr[$keyStudent]['mobileNo'] = $valueStudent['MOBILE_NO'];
            $studentArr[$keyStudent]['email'] = $valueStudent['EMAIL'];

            $studentArr[$keyStudent]['parentId'] = $valueStudent['parentId'];
            $studentArr[$keyStudent]['parentName'] = $valueStudent['parentName'];
            $studentArr[$keyStudent]['parentEmailId'] = $valueStudent['parentEmail'];
            $studentArr[$keyStudent]['parentMobileNo'] = $valueStudent['parentMobileNo'];
            $studentArr[$keyStudent]['parentUserName'] = $valueStudent['ParentUserName'];
            $studentArr[$keyStudent]['parentMembershipNumber'] = $valueStudent['parentMembershipNumber'];
            $studentArr[$keyStudent]['parentAddressLine1'] = $valueStudent['parentAddress1'];
            $studentArr[$keyStudent]['parentAddressLine2'] = $valueStudent['parentAddress2'];
            $studentArr[$keyStudent]['parentAddressLine3'] = $valueStudent['parentAddress3'];
            $studentArr[$keyStudent]['parentCityId'] = $valueStudent['parentCityId'];
            $studentArr[$keyStudent]['parentStateId'] = $valueStudent['parentStateId'];
            $studentArr[$keyStudent]['parentCountryId'] = $valueStudent['parentCountryId'];
            $studentArr[$keyStudent]['parentRelation'] = $valueStudent['parentRelation'];
            $studentArr[$keyStudent]['parentRelationName'] = $parentRelation;

            $studentArr[$keyStudent]['parentPincode'] = $valueStudent['parentPincode'];

            $studentArr[$keyStudent]['addressLine1'] = $valueStudent['ADDRESS_LINE1'];
            $studentArr[$keyStudent]['addressLine2'] = $valueStudent['ADDRESS_LINE2'];
            $studentArr[$keyStudent]['addressLine3'] = $valueStudent['ADDRESS_LINE3'];
            $studentArr[$keyStudent]['pincode'] = $valueStudent['PINCODE'];
            $studentArr[$keyStudent]['collegeName'] = $valueStudent['COLLEGE_NAME'];
            $studentArr[$keyStudent]['collegeAddressLine1'] = $valueStudent['COLLEGE_ADDRESS_LINE1'];
            $studentArr[$keyStudent]['collegeAddressLine2'] = $valueStudent['COLLEGE_ADDRESS_LINE2'];
            $studentArr[$keyStudent]['collegeAddressLine3'] = $valueStudent['COLLEGE_ADDRESS_LINE3'];
            $studentArr[$keyStudent]['collegeCountryId'] = $valueStudent['COLLEGE_COUNTRY_ID'];
            $studentArr[$keyStudent]['collegeStateId'] = $valueStudent['COLLEGE_STATE_ID'];
            $studentArr[$keyStudent]['collegeCityId'] = $valueStudent['COLLEGE_CITY_ID'];
            $studentArr[$keyStudent]['collegeCountryName'] = $countryName;
            $studentArr[$keyStudent]['collegeStateName'] = $collegeStateName;
            $studentArr[$keyStudent]['collegeCityName'] = $collegeCityName;
            $studentArr[$keyStudent]['collegePinCode'] = $valueStudent['COLLEGE_PINCODE'];


            $studentArr[$keyStudent]['gender'] = $valueStudent['GENDER'];
            $studentArr[$keyStudent]['bloodGroup'] = $valueStudent['BLOOD_GROUP'];
            $studentArr[$keyStudent]['course'] = $valueStudent['COURSE'];
            $studentArr[$keyStudent]['acdemicStartYear'] = $valueStudent['ACDEMIC_START_DATE'];
            $studentArr[$keyStudent]['acdemicEndYear'] = $valueStudent['ACDEMIC_END_DATE'];
            $studentArr[$keyStudent]['semester'] = $valueStudent['SEMESTER'];
            $studentArr[$keyStudent]['guarantorName'] = $valueStudent['guarantorName'];

            $studentArr[$keyStudent]['guarantorId'] = $valueStudent['guarantorId'];
            $studentArr[$keyStudent]['guarantorName'] = $valueStudent['guarantorName'];
            $studentArr[$keyStudent]['guarantorAddress1'] = $valueStudent['guarantorAddress1'];
            $studentArr[$keyStudent]['guarantorAddress2'] = $valueStudent['guarantorAddress2'];
            $studentArr[$keyStudent]['guarantorAddress3'] = $valueStudent['guarantorAddress3'];
            $studentArr[$keyStudent]['guarantorPinCode'] = $valueStudent['guarantorPincode'];
            $studentArr[$keyStudent]['guarantorEmail'] = $valueStudent['guarantorEmail'];
            $studentArr[$keyStudent]['guarantorMobileNo1'] = $valueStudent['guarantorMobileNo1'];
            $studentArr[$keyStudent]['guarantorMobileNo2'] = $valueStudent['guarantorMobileNo2'];
            $studentArr[$keyStudent]['guarantorCityId'] = $valueStudent['guarantorCityId'];
            $studentArr[$keyStudent]['guarantorStateId'] = $valueStudent['guarantorStateId'];
            $studentArr[$keyStudent]['guarantorCountryId'] = $valueStudent['guarantorCountryId'];
            $studentArr[$keyStudent]['guarantorCityName'] = $guarantorCityName;
            $studentArr[$keyStudent]['guarantorStateName'] = $guarantorStateName;
            $studentArr[$keyStudent]['guarantorCountryName'] = $countryName;
            $studentArr[$keyStudent]['guarantorType'] = $valueStudent['guarantorType'];
            $studentArr[$keyStudent]['guarantorRelation'] = $valueStudent['guarantorRelation'];
            $studentArr[$keyStudent]['guarantorRelationId'] = $valueStudent['guarantorRelationId'];

            $studentArr[$keyStudent]['guarantorPincode'] = $valueStudent['guarantorPincode'];

            $studentArr[$keyStudent]['medicalCondition'] = $valueStudent['MEDICAL_CONDITION'];
            $studentArr[$keyStudent]['aadharNo'] = $valueStudent['AADHAR_NO'];
            $studentArr[$keyStudent]['passportNo'] = $valueStudent['PASSPORT_NO'];
            $studentArr[$keyStudent]['typeOfRoomRequired'] = $valueStudent['TYPE_OF_ROOM_REQUIRED'];


            // ADDED BY KAVITA PATEL ON 06-08-2018 START
            $parentCityName = (isset($cityArr[$valueStudent['parentCityId']]) && $cityArr[$valueStudent['parentCityId']] != '') ? trim($cityArr[$valueStudent['parentCityId']]) : "";
            $studentArr[$keyStudent]['parentCityName'] = $parentCityName;
            $parentStateName = (isset($stateArr[$valueStudent['parentStateId']]) && $stateArr[$valueStudent['parentStateId']] != '') ? trim($stateArr[$valueStudent['parentStateId']]) : "";
            $studentArr[$keyStudent]['parentStateName'] = $parentStateName;
            $parentCountryName = (isset($countryArr[$valueStudent['parentCountryId']]) && $countryArr[$valueStudent['parentCountryId']] != '') ? trim($countryArr[$valueStudent['parentCountryId']]) : "";
            $studentArr[$keyStudent]['parentCountryName'] = $parentCountryName;
            $guarantorRelation = (isset($clientLookupArr[$valueStudent['guarantorRelation']]) && $clientLookupArr[$valueStudent['guarantorRelation']] != '') ? trim($clientLookupArr[$valueStudent['guarantorRelation']]) : "";
            $studentArr[$keyStudent]['guarantorRelation'] = $guarantorRelation;

            $roomCategory = 0;
            if (isset($valueStudent['TYPE_OF_ROOM_REQUIRED']) && $valueStudent['TYPE_OF_ROOM_REQUIRED'] != '') {
                $roomCategory = isset($clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']]) ? $clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']] : 0;
            }
            // $roomCategory=(isset($valueStudent['TYPE_OF_ROOM_REQUIRED'])?$clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']] : 0);
            $studentArr[$keyStudent]['roomCategory'] = (isset($roomCategory) && $roomCategory != '') ? trim($roomCategory) : "";
            $studentArr[$keyStudent]['roomNo'] = $valueStudent['ROOM_NAME'];
            $studentArr[$keyStudent]['bedNo'] = $valueStudent['BED_NO'];
            $studentArr[$keyStudent]['foodPreference'] = $valueStudent['FOOD_PREFERENCE'];
            $studentArr[$keyStudent]['tenacyStartDate'] = $valueStudent['TENANCY_START_DATE'];
            $studentArr[$keyStudent]['tenacyEndDate'] = $valueStudent['TENANCY_END_DATE'];
            $studentArr[$keyStudent]['tenacyDeposit'] = $valueStudent['TENANCY_DEPOSIT'];
            $studentArr[$keyStudent]['totalRent'] = $valueStudent['TENANCY_RENT'];
            if (isset($valueStudent['ASSET_ISSUED']) && $valueStudent['ASSET_ISSUED'] != '') {
                $asset = explode(",", $valueStudent['ASSET_ISSUED']);

                for ($i = 0; $i < count($asset); $i++) {
                    $assetVal[] = (isset($clientLookupArr[$asset[$i]]) && $clientLookupArr[$asset[$i]] != '') ? $clientLookupArr[$asset[$i]] : '';
                }

                $assetIssuedVal = implode(",", $assetVal);
                $studentArr[$keyStudent]['assetIssued'] = $assetIssuedVal;
            } else {
                $studentArr[$keyStudent]['assetIssued'] = '';
            }

            if (isset($valueStudent['FACILITY_ALLOCATION']) && $valueStudent['FACILITY_ALLOCATION'] != '') {
                $facilityAlloc = explode(",", $valueStudent['FACILITY_ALLOCATION']);
                for ($i = 0; $i < count($facilityAlloc); $i++) {
                    $facilityVal[] = isset($itemLookupArr[$facilityAlloc[$i]]) ? $itemLookupArr[$facilityAlloc[$i]] : '';
                }
                $facilityAllocation = implode(",", $facilityVal);
                $studentArr[$keyStudent]['facilityAllocation'] = $facilityAllocation;
            } else {
                $studentArr[$keyStudent]['facilityAllocation'] = '';
            }
            $nationality = $countryArr[$valueStudent['NATIONALITY']];
            $studentArr[$keyStudent]['nationalityName'] = (isset($nationality) && $nationality != '') ? trim($nationality) : "";
            // ADDED BY KAVITA PATEL ON 06-08-2018 END
            // ADDED BY KAVITA PATEL ON 06-08-2018 START
            //$parentCityName=$cityArr[$valueStudent['parentCityId']];


            $parentCityName = (isset($cityArr[$valueStudent['parentCityId']]) && $cityArr[$valueStudent['parentCityId']] != '') ? trim($cityArr[$valueStudent['parentCityId']]) : "";
            $studentArr[$keyStudent]['parentCityName'] = $parentCityName;
            $parentStateName = (isset($stateArr[$valueStudent['parentStateId']]) && $stateArr[$valueStudent['parentStateId']] != '') ? trim($stateArr[$valueStudent['parentStateId']]) : "";
            $studentArr[$keyStudent]['parentStateName'] = $parentStateName;
            $parentCountryName = (isset($countryArr[$valueStudent['parentCountryId']]) && $countryArr[$valueStudent['parentCountryId']] != '') ? trim($countryArr[$valueStudent['parentCountryId']]) : "";
            $studentArr[$keyStudent]['parentCountryName'] = $parentCountryName;
            $guarantorRelation = (isset($clientLookupArr[$valueStudent['guarantorRelation']]) && $clientLookupArr[$valueStudent['guarantorRelation']] != '') ? trim($clientLookupArr[$valueStudent['guarantorRelation']]) : "";
            $studentArr[$keyStudent]['guarantorRelation'] = $guarantorRelation;

            $roomCategory = 0;
            if (isset($valueStudent['TYPE_OF_ROOM_REQUIRED']) && $valueStudent['TYPE_OF_ROOM_REQUIRED'] != '') {
                $roomCategory = isset($clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']]) ? $clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']] : 0;
            }
            // $roomCategory=(isset($valueStudent['TYPE_OF_ROOM_REQUIRED'])?$clientLookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']] : 0);
            $studentArr[$keyStudent]['roomCategory'] = (isset($roomCategory) && $roomCategory != '') ? trim($roomCategory) : "";
            $studentArr[$keyStudent]['roomNo'] = $valueStudent['ROOM_NAME'];
            $studentArr[$keyStudent]['bedNo'] = $valueStudent['BED_NO'];
            $studentArr[$keyStudent]['foodPreference'] = $valueStudent['FOOD_PREFERENCE'];
            $studentArr[$keyStudent]['tenacyStartDate'] = $valueStudent['TENANCY_START_DATE'];
            $studentArr[$keyStudent]['tenacyEndDate'] = $valueStudent['TENANCY_END_DATE'];
            $studentArr[$keyStudent]['tenacyDeposit'] = $valueStudent['TENANCY_DEPOSIT'];
            $studentArr[$keyStudent]['totalRent'] = $valueStudent['TENANCY_RENT'];
            if (isset($valueStudent['ASSET_ISSUED']) && $valueStudent['ASSET_ISSUED'] != '') {
                $asset = explode(",", $valueStudent['ASSET_ISSUED']);

                for ($i = 0; $i < count($asset); $i++) {
                    $assetVal[] = (isset($clientLookupArr[$asset[$i]]) && $clientLookupArr[$asset[$i]] != '') ? $clientLookupArr[$asset[$i]] : '';
                }

                $assetIssuedVal = implode(",", $assetVal);
                $studentArr[$keyStudent]['assetIssued'] = $assetIssuedVal;
            } else {
                $studentArr[$keyStudent]['assetIssued'] = '';
            }

            if (isset($valueStudent['FACILITY_ALLOCATION']) && $valueStudent['FACILITY_ALLOCATION'] != '') {
                $facilityAlloc = explode(",", $valueStudent['FACILITY_ALLOCATION']);
                for ($i = 0; $i < count($facilityAlloc); $i++) {
                    $facilityVal[] = isset($itemLookupArr[$facilityAlloc[$i]]) ? $itemLookupArr[$facilityAlloc[$i]] : '';
                }
                $facilityAllocation = implode(",", $facilityVal);
                $studentArr[$keyStudent]['facilityAllocation'] = $facilityAllocation;
            } else {
                $studentArr[$keyStudent]['facilityAllocation'] = '';
            }
            $nationality = (isset($countryArr[$valueStudent['NATIONALITY']]) && $countryArr[$valueStudent['NATIONALITY']] != '') ? trim($countryArr[$valueStudent['NATIONALITY']]) : "";
            $studentArr[$keyStudent]['nationalityName'] = $nationality;
            // ADDED BY KAVITA PATEL ON 06-08-2018 END


            $studentArr[$keyStudent]['emeregencyContactPersonNo'] = $valueStudent['EMERGENCY_CONTACT_PERSON_NO'];
            $studentArr[$keyStudent]['emeregencyContactPersonName'] = $valueStudent['EMERGENCY_CONTACT_PERSON_NAME'];
            $studentArr[$keyStudent]['birthDate'] = $valueStudent['BIRTHDATE'];
            $studentArr[$keyStudent]['passportNo'] = $valueStudent['PASSPORT_NO'];
            $studentArr[$keyStudent]['roomType'] = $itemName;
            $studentArr[$keyStudent]['roomName'] = !empty($valueStudent['ROOM_NAME']) ? $valueStudent['ROOM_NAME'] : "";
            $studentArr[$keyStudent]['enrollmentDate'] = !empty($valueStudent['ENROLLMENT_DATE']) ? $valueStudent['ENROLLMENT_DATE'] : "";
            $studentArr[$keyStudent]['nationality'] = !empty($valueStudent['NATIONALITY']) ? $valueStudent['NATIONALITY'] : "";
            $studentArr[$keyStudent]['status'] = !empty($valueStudent['IS_ACTIVE']) ? $valueStudent['IS_ACTIVE'] : "";


            $requestData = $studentData = Array();
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];
            $requestData['postData']['type'] = 'PHOTO';
            $studentAttachmentPhotoData = listStudentAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentPhotoAllData = ($studentAttachmentPhotoData['status'] == SCS) ? $studentAttachmentPhotoData['data'] : $studentAttachmentPhotoData['status'];
            $studentArr[$keyStudent]['attachmentData']['photosArr'] = $studentAttachmentPhotoAllData;

            $requestData = $studentData = Array();
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];
            $requestData['postData']['type'] = 'MEDICAL';
            $studentAttachmentCertificateData = listStudentAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentCertificateAllData = ($studentAttachmentCertificateData['status'] == SCS) ? $studentAttachmentCertificateData['data'] : $studentAttachmentCertificateData['status'];
            $studentArr[$keyStudent]['attachmentData']['certificateArr'] = $studentAttachmentCertificateAllData;
        }
        $result = array("status" => SCS, "data" => array_values($studentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function changePasswordStudent($postData) {
//print_r($postData); exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $oldpassword = isset($postData['postData']['oldpassword']) ? addslashes(trim($postData['postData']['oldpassword'])) : "";
    $newpassword = isset($postData['postData']['newpassword']) ? addslashes(trim($postData['postData']['newpassword'])) : "";
    $passwordFlag = isset($postData['postData']['passwordFlag']) ? addslashes(trim($postData['postData']['passwordFlag'])) : "";

    $whereClientCond = $whereOrgCond = "";
    if (!empty($clientId)) {
        $whereClientCond = " AND FK_CLIENT_ID ='" . $clientId . "'";
    }
    if (!empty($orgId)) {
        $whereOrgCond = " AND FK_ORG_ID ='" . $orgId . "'";
    }
//EDIT BY DARSHAN PATEL ON 17-8-2018
    $sqlChanePassData = "SELECT SM.PK_STUD_ID,SM.FK_CLIENT_ID,SM.PASSWORD,SM.USER_NAME,SM.MEMBERSHIP_NUMBER,SM.SETPASSWORD_FLAG FROM " . STUDENTMASTER . " SM WHERE SM.DELETE_FLAG = '0' AND SM.PK_STUD_ID ='" . $studentId . "'" . $whereClientCond . $whereOrgCond; // QUERY FOR STUDENT DATA.

    $resChangePassData = fetch_rec_query($sqlChanePassData);


    if ($passwordFlag == 1) {//FOR FORCE FULLY PASSWORD CHANGE
        $fieldsValue = array();
        $fieldsValue['PASSWORD'] = password_hash($newpassword, PASSWORD_DEFAULT);
//        $fieldsValue['PASSWORD1'] = $newpassword;
        $fieldsValue['SETPASSWORD_FLAG'] = $passwordFlag;
        $fieldsValue['CHANGED_BY'] = $studentId;
        $fieldsValue['CHANGED_BY_DATE'] = time();
//        print_r($fieldsValue);exit;
        $whereCond = "PK_STUD_ID= '" . $studentId . "' AND DELETE_FLAG = '0' ";
//print_r($where); die;
        $updateUserMaster = update_rec(STUDENTMASTER, $fieldsValue, $whereCond);
        if ($updateUserMaster) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else {//FOR NORMAL CHANGE PASSWORD
        if (password_verify($oldpassword, $resChangePassData[0]['PASSWORD'])) {

            $updateUser = array();
            $updateUser['PASSWORD'] = password_hash($newpassword, PASSWORD_DEFAULT);
            $updateUser['CHANGED_BY'] = $studentId;
            $updateUser['CHANGED_BY_DATE'] = time();

            $updateWhere = "PK_STUD_ID = '" . $studentId . "' AND DELETE_FLAG = '0' ";

            $updateRequest = update_rec(STUDENTMASTER, $updateUser, $updateWhere);
            if ($updateRequest) {
                $result = array("status" => SCS);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEQUERYFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => INCORRENTPASSWORD);
            http_response_code(400);
        }
    }

//    $sqlChanePassData = "SELECT SM.PK_STUD_ID,SM.FK_CLIENT_ID,SM.PASSWORD,SM.USER_NAME,SM.MEMBERSHIP_NUMBER,SM.SETPASSWORD_FLAG FROM " . STUDENTMASTER . " SM WHERE SM.DELETE_FLAG =0 AND SM.PK_STUD_ID='" . $studentId . "' "; // QUERY FOR STUDENT DATA.
//    $resChangePassData = fetch_rec_query($sqlChanePassData);
//
//    if (count($resChangePassData) > 0) {
//       // $oldDBpassword = password_verify($oldPassword, $resChangePassData[0]['PASSWORD']);
//        //echo $oldDBpassword; die;
//        if ($resChangePassData[0]['SETPASSWORD_FLAG'] == 0) {
//            // if ($oldPassword == $oldDBpassword) {
//
//                $fieldsValue['PASSWORD'] = password_hash($newPassword, PASSWORD_DEFAULT);
//                $fieldsValue['SETPASSWORD_FLAG'] = $passwordFlag;
//                $fieldsValue['CHANGED_BY'] = $studentId;
//                $fieldsValue['CHANGED_BY_DATE'] = time();
//
//                $where = "PK_STUD_ID='" . $resChangePassData[0]['PK_STUD_ID'] . "'";
//                //print_r($where); die;
//                $updateUserMaster = update_rec(STUDENTMASTER, $fieldsValue, $where);
//
//                $result = array("status" => SCS);
//                http_response_code(200);
//            // } else {
//            //     $result = array("status" => INCORRENTPASSWORD);
//            //     http_response_code(400);
//            // }
//        } else {
//            $fieldsValue['PASSWORD'] = password_hash($newPassword, PASSWORD_DEFAULT);
//            $fieldsValue['SETPASSWORD_FLAG'] = $passwordFlag;
//            $fieldsValue['CHANGED_BY'] = $studentId;
//            $fieldsValue['CHANGED_BY_DATE'] = time();
//            //print_r($fieldsValue); die;
//            $where = "PK_STUD_ID='" . $resChangePassData[0]['PK_STUD_ID'] . "'";
//            //print_r($where); die;
//            $updateUserMaster = update_rec(STUDENTMASTER, $fieldsValue, $where);
//
//            $result = array("status" => SCS);
//            http_response_code(200);
//        }
//    } else {
//        $result = array("status" => NORECORDS);
//        http_response_code(400);
//    }


    return $result;
}

//ADD BY AKSHAT.07.08.2018 // LIST STUDENT EVENT DATA API.

function getStudentEventList($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $eventDate = date('Y-m-d');


    $sqlStudentEventData = " SELECT IM.PK_ITEM_ID,IM.ITEM_NAME,DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%Y-%m-%d') AS EVENT_START_DATE_TIME,DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%Y-%m-%d') AS EVENT_END_DATE_TIME,IM.EVENT_VENUE,IM.EVENT_START_DATETIME,IM.EVENT_END_DATETIME,IM.ITEM_PRICE,IF(EB.QTY > 0,IM.QTY-SUM(EB.QTY),IM.QTY) AS REMAINING_SEATS FROM " . ITEMMASTER . " AS IM LEFT JOIN " . EVENTSBOOKING . " EB ON EB.FK_ITEM_ID = IM.PK_ITEM_ID WHERE IM.ITEM_TYPE = 'EVENTS' AND  DATE(DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%Y-%m-%d')) >= '" . $eventDate . "' GROUP BY EB.FK_ITEM_ID";


    $resStudentData = fetch_rec_query($sqlStudentEventData); // RESULT FOR GET Invoice Data
    $studentArr = Array();
    if (count($resStudentData) > 0) {
        foreach ($resStudentData as $keyStudent => $valueStudent) { // Invoice Data FOREACH LOOP.
            $studentArr[$keyStudent]['eventId'] = $valueStudent['PK_ITEM_ID'];
            $studentArr[$keyStudent]['eventName'] = $valueStudent['ITEM_NAME'];
            $studentArr[$keyStudent]['startDateTime'] = date("d-m-Y | h:i:s A", $valueStudent['EVENT_START_DATETIME']);
            $studentArr[$keyStudent]['endDateTime'] = date("d-m-Y | h:i:s A", $valueStudent['EVENT_END_DATETIME']);
            $studentArr[$keyStudent]['venue'] = $valueStudent['EVENT_VENUE'];
            $studentArr[$keyStudent]['remainingSeats'] = $valueStudent['REMAINING_SEATS'];
            $studentArr[$keyStudent]['eventPrice'] = $valueStudent['ITEM_PRICE'];
        }
        $result = array("status" => SCS, "data" => array_values($studentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.07.08.2018 // LIST STUDENT EVENT DATA API.
//ADD BY AKSHAT.07.08.2018 // LIST STUDENT ENROLLMENT REPORT API.

function studentEnrollmentReport1($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $enrollType = isset($postData['postData']['enrollType']) ? addslashes(trim($postData['postData']['enrollType'])) : "";
    $gender = isset($postData['postData']['gender']) ? addslashes(trim($postData['postData']['gender'])) : "";
    $year = date('Y');

    $whereEnrollTypeCond = "";

    if ($enrollType != "") {
        $whereEnrollTypeCond = " AND ENROLLMENT_TYPE = '" . $enrollType . "'";
    }

    $whereGenderCond = "";

    if ($gender != "") {
        $whereGenderCond = " AND GENDER = '" . $gender . "'";
    }

    $sqlStudentEnrollData = " SELECT FIRST_NAME,LAST_NAME,MEMBERSHIP_NUMBER,ACDEMIC_START_DATE,ACDEMIC_END_DATE,ENROLLMENT_TYPE,GENDER,MOBILE_NO,IS_ACTIVE,CREATED_BY FROM " . STUDENTMASTER . " WHERE DATE(DATE_FORMAT(FROM_UNIXTIME(CREATED_BY), '%Y')) = '" . $year . "'" . $whereEnrollTypeCond . $whereGenderCond;

    $resStudentEnrollData = fetch_rec_query($sqlStudentEnrollData); // RESULT FOR GET Invoice Data
    $studentEnrollArr = Array();
    if (count($resStudentEnrollData) > 0) {
        foreach ($resStudentEnrollData as $keyStudentEnroll => $valueStudentEnroll) { // Invoice Data FOREACH LOOP.
            $studentEnrollArr[$keyStudentEnroll]['studentName'] = $valueStudentEnroll['FIRST_NAME'] . " " . $valueStudentEnroll['LAST_NAME'];
            $studentEnrollArr[$keyStudentEnroll]['memberShipId'] = $valueStudentEnroll['MEMBERSHIP_NUMBER'];
            $studentEnrollArr[$keyStudentEnroll]['enrollmentDate'] = date('d-m-Y', $valueStudentEnroll['CREATED_BY']);
            $studentEnrollArr[$keyStudentEnroll]['acdemicYear'] = $valueStudentEnroll['ACDEMIC_START_DATE'] . '/' . $valueStudentEnroll['ACDEMIC_END_DATE'];
            $studentEnrollArr[$keyStudentEnroll]['enrollmentType'] = $valueStudentEnroll['ENROLLMENT_TYPE'];
            $studentEnrollArr[$keyStudentEnroll]['gender'] = $valueStudentEnroll['GENDER'];
            $studentEnrollArr[$keyStudentEnroll]['mobileNo'] = $valueStudentEnroll['MOBILE_NO'];
            $studentEnrollArr[$keyStudentEnroll]['status'] = $valueStudentEnroll['IS_ACTIVE'];
        }
        $result = array("status" => SCS, "data" => array_values($studentEnrollArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.07.08.2018 // LIST STUDENT STUDENT ENROLLMENT REPORT API.

function getUserHierarchy($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestuserId = isset($postData['postData']['requestuserId']) ? addslashes(trim($postData['postData']['requestuserId'])) : "";

    $resquestData = array();
    $resquestData['postData']['clientId'] = $clientId;
    $resquestData['postData']['levelFlag'] = 1;
    $getLevel = getUserLevel($resquestData);
//    print_r($getLevel); exit;
    $finalJson = array();

// START GETTING LEVEL OF USER
//    $sqlGetUserLevel = "SELECT * FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$userId."' AND FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = '0' ";
    $sqlGetUserLevel = "SELECT PK_USER_ID FROM " . USERMASTER . " UM WHERE UM.PK_USER_ID = '" . $userId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' ";
    $resultGetUserLevel = fetch_rec_query($sqlGetUserLevel);

    if (count($resultGetUserLevel) > 0) {
        $finalJson = array();
        $finalJson[] = $resultGetUserLevel[0]['PK_USER_ID'];
        $queryJson = array();
        $queryJson = $finalJson;
        for ($f = 0; $f < count($getLevel['data']) - 1; $f++) {
            $sqlGetUser = "SELECT GROUP_CONCAT(CONVERT(PK_USER_ID,CHAR(20))) PK_USER_ID FROM " . USERMASTER . " UM WHERE UM.FK_SUPERVISOR_ID IN (" . implode(",", $queryJson) . ") AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0'";
            $resultGetUser = fetch_rec_query($sqlGetUser);
            if (!empty($resultGetUser[0]['PK_USER_ID'])) {
                $finalJson[] = $resultGetUser[0]['PK_USER_ID'];
                $queryJson = array();
                $queryJson[] = $resultGetUser[0]['PK_USER_ID'];
            } else {
                break;
            }
        }

        $finalUserList = implode(",", $finalJson);
        $explodeUsers = explode(",", $finalUserList);
        $finalReturnJson = array();
        foreach ($explodeUsers as $usersKey => $usersValue) {

//            $sqlUserDetail = "SELECT * FROM ".USERMASTER." UM WHERE UM.PK_USER_ID = '".$usersValue."' AND FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 LIMIT 0,1";
            $sqlUserDetail = "SELECT UM.FULLNAME,UM.PK_USER_ID FROM " . USERMASTER . " UM WHERE UM.PK_USER_ID = '" . $usersValue . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = 0 LIMIT 0,1";
            $resultUserDetail = fetch_rec_query($sqlUserDetail);
//            print_r($resultUserDetail); exit;
            $finalReturnJson[$usersKey]['userName'] = !empty($resultUserDetail[0]['FULLNAME']) ? $resultUserDetail[0]['FULLNAME'] : "";
            $finalReturnJson[$usersKey]['userId'] = !empty($resultUserDetail[0]['PK_USER_ID']) ? $resultUserDetail[0]['PK_USER_ID'] : "";
            $finalReturnJson[$usersKey]['lastLevelFlag'] = "N";
            $finalReturnJson[$usersKey]['userDetail'][0]['userId'] = !empty($resultUserDetail[0]['PK_USER_ID']) ? $resultUserDetail[0]['PK_USER_ID'] : "";
            $finalReturnJson[$usersKey]['userDetail'][0]['userName'] = !empty($resultUserDetail[0]['FULLNAME']) ? $resultUserDetail[0]['FULLNAME'] : "";
            $finalReturnJson[$usersKey]['userDetail'][0]['lastLevelFlag'] = "Y";
        }
//        print_r($finalReturnJson); exit;
        $result = array("status" => SCS, "data" => $finalReturnJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR ADD DEFAULT STATUS IN THE CLIENT CREATE 
//ADD BY DARHAN PATEL ON 28-08-2018
function insertDefaultStatus($postData) {
//    print_r($postData);exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    ///////////////// ========BOOKING======== ////////////////////////  
    // IF ANY STATUS IS AVALIABLE AGAINST BOOKING THEN NOT ALLOWED TO ADD STATUS AGAIN (DELETE OR NON DELETE STATUS)
    $sqlBookingStatus = "SELECT * FROM " . STATUSMANAGEMENT . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_CATEGORY_ID = '40' ";
    $resultBookingStatus = fetch_rec_query($sqlBookingStatus);

    if (count($resultBookingStatus) == 0) {
        //BOOKING STATUS START
        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Pending';
        $insertStatus['FK_CATEGORY_ID'] = '40';
        $insertStatus['PRIORITY_LEVEL'] = '1';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Booked';
        $insertStatus['FK_CATEGORY_ID'] = '40';
        $insertStatus['PRIORITY_LEVEL'] = '2';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['THRID_PARTY_FLAG'] = '1';
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Cancle';
        $insertStatus['FK_CATEGORY_ID'] = '40';
        $insertStatus['PRIORITY_LEVEL'] = '3';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CANCEL_REMARK_FLAG'] = '1';
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);
        //BOOKING STATUS END
    }


    ///////////////// ========PROPORTY======== ////////////////////////  
    // IF ANY STATUS IS AVALIABLE AGAINST PROPORTY THEN NOT ALLOWED TO ADD STATUS AGAIN (DELETE OR NON DELETE STATUS)
    $sqlProportyStatus = "SELECT * FROM " . STATUSMANAGEMENT . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_CATEGORY_ID = '41' ";
    $resultProportyStatus = fetch_rec_query($sqlProportyStatus);

    if (count($resultProportyStatus) == 0) {
        //PROPORTY STATUS START
        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Active';
        $insertStatus['FK_CATEGORY_ID'] = '41';
        $insertStatus['PRIORITY_LEVEL'] = '1';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'InActive';
        $insertStatus['FK_CATEGORY_ID'] = '41';
        $insertStatus['PRIORITY_LEVEL'] = '2';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Under Maintance';
        $insertStatus['FK_CATEGORY_ID'] = '41';
        $insertStatus['PRIORITY_LEVEL'] = '3';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);
        //PROPORTY STATUS END
    }


    ///////////////// ========STUDENT======== ////////////////////////  
    // IF ANY STATUS IS AVALIABLE AGAINST STUDENT THEN NOT ALLOWED TO ADD STATUS AGAIN (DELETE OR NON DELETE STATUS)
    $sqlStudentStatus = "SELECT * FROM " . STATUSMANAGEMENT . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_CATEGORY_ID = '42' ";
    $resultStudentStatus = fetch_rec_query($sqlStudentStatus);

    if (count($resultStudentStatus) == 0) {
        //STUDENT STATUS START
        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Active';
        $insertStatus['FK_CATEGORY_ID'] = '42';
        $insertStatus['PRIORITY_LEVEL'] = '1';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'InActive';
        $insertStatus['FK_CATEGORY_ID'] = '42';
        $insertStatus['PRIORITY_LEVEL'] = '2';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Termination';
        $insertStatus['FK_CATEGORY_ID'] = '42';
        $insertStatus['PRIORITY_LEVEL'] = '3';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);


        $insertStatus = array();
        $insertStatus['STATUS_NAME'] = 'Suspended';
        $insertStatus['FK_CATEGORY_ID'] = '42';
        $insertStatus['PRIORITY_LEVEL'] = '4';
        $insertStatus['FK_CLIENT_ID'] = $clientId;
        $insertStatus['CREATED_BY'] = $userId;
        $insertStatus['CREATED_BY_DATE'] = time();
        $insertStatus['DELETE_FLAG'] = '0';
        $insertStatusData = insert_rec(STATUSMANAGEMENT, $insertStatus);
        //STUDENT STATUS END
    }
}

function sortByName($a, $b) {
    if ($a['ledgerDateTime'] == $b['ledgerDateTime']) {
        return 0;
    }
    return ($a['ledgerDateTime'] > $b['ledgerDateTime']) ? 1 : -1;
}

?>
