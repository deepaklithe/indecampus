<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 12-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR Add/Edit Customer Detail. 
function addEditCustomer($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $firstName = isset($postData['postData']['firstName']) ? addslashes(trim($postData['postData']['firstName'])) : "";
    $lastName = isset($postData['postData']['lastName']) ? addslashes(trim($postData['postData']['lastName'])) : "";
    $mobileNo = isset($postData['postData']['mobileNo']) ? addslashes(trim($postData['postData']['mobileNo'])) : "";
    $emailId = isset($postData['postData']['emailId']) ? addslashes(trim($postData['postData']['emailId'])) : "";
    $address = isset($postData['postData']['address']) ? addslashes(trim($postData['postData']['address'])) : "";
    $pinCode = isset($postData['postData']['pinCode']) ? addslashes(trim($postData['postData']['pinCode'])) : "";
    $cityId = isset($postData['postData']['cityId']) ? addslashes(trim($postData['postData']['cityId'])) : "";
    $stateId = isset($postData['postData']['stateId']) ? addslashes(trim($postData['postData']['stateId'])) : "";
    $countryId = isset($postData['postData']['countryId']) ? addslashes(trim($postData['postData']['countryId'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $returnCustomerId = isset($postData['postData']['returnCustomerId']) ? addslashes(trim($postData['postData']['returnCustomerId'])) : "";


    if (!empty($clientId) && !empty($orgId) && !empty($firstName) && !empty($mobileNo)) { // CHECK REQURIED FIELD CONDITION
        $whereCond = "";
        if (!empty($customerId)) {
            $whereCond = " AND PK_CUST_ID !=" . $customerId;
        }
        
        $custId = generateId(CUSTOMERMASTER, 'CUST', $clientId);//ADD BY DEVIKA.29.8.2018
        
        $sqlCheckCustomerExist = "SELECT PK_CUST_ID FROM " . CUSTOMERMASTER . " WHERE MOBILE_NO ='" . $mobileNo . "' AND DELETE_FLAG ='0' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'" . $whereCond; // QUERY FOR MOBILE NO IS ALREADY AVAILABLE OR NOT.
        $resCheckCustomerExist = fetch_rec_query($sqlCheckCustomerExist); // RESULT FOR MOBILE NO IS ALREADY AVAILABLE OR NOT.
        if (count($resCheckCustomerExist) > 0) {
            $result = array("status" => MOBILENOISEXIST);
            http_response_code(400);
        } else {
            $msg = "";
// PROPERTY INSERT ARRAY.
            $insertCustomerArray = Array(); //ITEM ARRAY CREATE
            $insertCustomerArray['FK_CLIENT_ID'] = $clientId;
            $insertCustomerArray['C_CUST_ID'] = $custId;//ADD BY DEVIKA.29.8.2018
            $insertCustomerArray['FK_ORG_ID'] = $orgId;
            $insertCustomerArray['FIRST_NAME'] = $firstName;
            $insertCustomerArray['LAST_NAME'] = $lastName;
            $insertCustomerArray['MOBILE_NO'] = $mobileNo;
            $insertCustomerArray['EMAIL_ID'] = $emailId;
            $insertCustomerArray['ADDRESS'] = $address;
            $insertCustomerArray['PIN_CODE'] = $pinCode;
            $insertCustomerArray['FK_CITY_ID'] = $cityId;
            $insertCustomerArray['FK_STATE_ID'] = $stateId;
            $insertCustomerArray['FK_COUNTRY_ID'] = $countryId;

            if (!empty($customerId) && $customerId > 0) {
                $insertCustomerArray['CHANGED_BY'] = $userId;
                $insertCustomerArray['CHANGED_BY_DATE'] = time();
                $whereCustomerCond = "PK_CUST_ID ='" . $customerId . "'";
                $updatCustomer = update_rec(CUSTOMERMASTER, $insertCustomerArray, $whereCustomerCond); // UPDATE RECORD IN PROPERTY_MASTER TABLE.
                if (!$updatCustomer) {
                    $msg = UPDATEFAIL . " " . CUSTOMERMASTER;
                }
            } else {
                $insertCustomerArray['CREATED_BY'] = $userId;
                $insertCustomerArray['CREATED_BY_DATE'] = time();
                $insertCustomer = insert_rec(CUSTOMERMASTER, $insertCustomerArray); // INSERT RECORD IN PROPERTY_MASTER TABLE.          
                if (!isset($insertCustomer['lastInsertedId']) || $insertCustomer['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . CUSTOMERMASTER;
                } else {
                    $customerId = $insertCustomer['lastInsertedId'];
                }
            }

            if ($msg == "") {

                $requestData = $customerData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                if ($returnCustomerId == 1) {
                    $requestData['postData']['customerId'] = $customerId;
                }
                $customerData = getCustomerData($requestData); // GET ALL CUSTOMER DATA.

                $customerAllData = ($customerData['status'] == SCS) ? $customerData['data'] : $customerData['status'];
                $result = array("status" => SCS, "data" => $customerAllData);
                http_response_code(200);
            } else {
                $result = array("status" => $msg);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR DELETE CUSTOMER DATA
function deleteCustomerData($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($customerId)) {
        $updateCustomerArray = Array(); //CUSTOMER ARRAY UPDATE
        $updateCustomerArray['DELETE_FLAG'] = 1;
        $updateCustomerArray['CHANGED_BY'] = $userId;
        $updateCustomerArray['CHANGED_BY_DATE'] = time();

        $whereCustomerUpdate = " PK_CUST_ID =" . $customerId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG = '0'";
        $updateCustomer = update_rec(CUSTOMERMASTER, $updateCustomerArray, $whereCustomerUpdate); // UPDATE DELETE_FLAG =1 IN CUSTOMER_MASTER TABLE.
        if ($updateCustomer) {
            $requestData = $customerData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $customerData = getCustomerData($requestData); // GET ALL CUSTOMER  DATA.

            $customerAllData = ($customerData['status'] == SCS) ? $customerData['data'] : $customerData['status'];
            $result = array("status" => SCS, "data" => $customerAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . CUSTOMERMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

// GET CUSTOMER ALL DATA.
function getCustomerData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";

    $customerArr = array();
    $whereCustomerCond = "";
    if (!empty($customerId)) {
        $whereCustomerCond = " AND CM.PK_CUST_ID='" . $customerId . "'";
    }


    $sqlCustomerData = "  SELECT CM.PK_CUST_ID,CM.C_CUST_ID,CM.FIRST_NAME,CM.LAST_NAME,CM.MOBILE_NO,CM.EMAIL_ID,CM.FK_CLIENT_ID,CM.ADDRESS,CM.PIN_CODE,CM.FK_CITY_ID,CM.FK_STATE_ID,CM.FK_COUNTRY_ID,CO.COUNTRY_NAME,CT.CITY_NAME,CT.STATE_NAME,CM.FK_CLIENT_ID,CM.FK_ORG_ID FROM " . CUSTOMERMASTER . " CM LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = CM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=CM.FK_CITY_ID WHERE CM.DELETE_FLAG =0 " . $whereCustomerCond . " AND CM.FK_CLIENT_ID ='" . $clientId . "' AND CM.FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR CUSTOMER DATA.
    $resCustomerData = fetch_rec_query($sqlCustomerData); // RESULT FOR CUSTOMER DATA.
    if (count($resCustomerData) > 0) {
        foreach ($resCustomerData as $keyCustomer => $valueCustomer) { // CUSTOMER DATA LOOP
            $customerArr[$keyCustomer]['customerId'] = $valueCustomer['PK_CUST_ID'];
            $customerArr[$keyCustomer]['clientCustomerId'] = $valueCustomer['C_CUST_ID'];
            $customerArr[$keyCustomer]['firstName'] = $valueCustomer['FIRST_NAME'];
            $customerArr[$keyCustomer]['lastName'] = $valueCustomer['LAST_NAME'];
            $customerArr[$keyCustomer]['mobileNo'] = $valueCustomer['MOBILE_NO'];
            $customerArr[$keyCustomer]['clientId'] = $valueCustomer['FK_CLIENT_ID'];
            $customerArr[$keyCustomer]['orgId'] = $valueCustomer['FK_ORG_ID'];
            $customerArr[$keyCustomer]['emailId'] = $valueCustomer['EMAIL_ID'];
            $customerArr[$keyCustomer]['address'] = $valueCustomer['ADDRESS'];
            $customerArr[$keyCustomer]['pincode'] = $valueCustomer['PIN_CODE'];
            $customerArr[$keyCustomer]['cityId'] = $valueCustomer['FK_CITY_ID'];
            $customerArr[$keyCustomer]['stateId'] = $valueCustomer['FK_STATE_ID'];
            $customerArr[$keyCustomer]['countryId'] = $valueCustomer['FK_COUNTRY_ID'];
            $customerArr[$keyCustomer]['countryName'] = ($valueCustomer['COUNTRY_NAME']) ? : "";
            $customerArr[$keyCustomer]['stateName'] = ($valueCustomer['STATE_NAME']) ? : "";
            $customerArr[$keyCustomer]['cityName'] = ($valueCustomer['CITY_NAME']) ? : "";
        }
        $result = array("status" => SCS, "data" => $customerArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR Add/Edit Inquiry Detail. 
function addEditInquiry($postData) {

// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $custId = isset($postData['postData']['custId']) ? addslashes(trim($postData['postData']['custId'])) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";
    $propertyName = isset($postData['postData']['propertyName']) ? addslashes(trim($postData['postData']['propertyName'])) : "";
    $timezoneId = isset($postData['postData']['timezoneId']) ? addslashes(trim($postData['postData']['timezoneId'])) : "";
    $timezoneName = isset($postData['postData']['timezoneName']) ? addslashes(trim($postData['postData']['timezoneName'])) : "";
    $bookingStartDate = !empty($postData['postData']['startDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['startDate'])))) : "";
    $bookingEndDate = !empty($postData['postData']['endDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['endDate'])))) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";
    $inqId = isset($postData['postData']['inqId']) ? addslashes(trim($postData['postData']['inqId'])) : "";
    if (strtotime($bookingStartDate) > strtotime($bookingEndDate)) { // CHECK Start date is not greater than End date.
        $result = array("status" => STARTISNOTGREATERTHANENDDATE);
        http_response_code(400);
    } else {

        if (!empty($clientId) && !empty($orgId) && !empty($propertyId) && !empty($propertyName) && !empty($timezoneId) && !empty($timezoneName) && !empty($status)) { // CHECK REQURIED FIELD CONDITION
            $sqlCheckBookingData = "SELECT PK_INQ_ID FROM " . INQMASTER . " WHERE STATUS ='2' AND DELETE_FLAG ='0' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND FK_PROPERTY_ID='" . $propertyId . "' AND (BOOKING_END_DATE BETWEEN '" . $bookingStartDate . "' AND '" . $bookingEndDate . "' OR BOOKING_START_DATE BETWEEN '" . $bookingStartDate . "' AND '" . $bookingEndDate . "' )"; // QUERY FOR CHECK THIS DAY BOOKING IS AVAILABEL OR NOT.


            $resCheckBookingData = fetch_rec_query($sqlCheckBookingData);
            if (count($resCheckBookingData) == 0) {

                $msg = "";
// INQUIRY INSERT ARRAY.

                /* Customer Create Code Start */
                if ($custId == "-2") {
                    $custData = !empty($postData['postData']['custData']) ? $postData['postData']['custData'] : Array();

                    if (!empty($custData)) {
                        $requestData = $customerData = Array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['firstName'] = $custData['firstName'];
                        $requestData['postData']['lastName'] = $custData['lastName'];
                        $requestData['postData']['mobileNo'] = $custData['mobileNo'];
                        $requestData['postData']['emailId'] = $custData['emailId'];
                        $requestData['postData']['returnCustomerId'] = "1"; // WHERE returnCustomerId id is 1 which is identify return customer id or not.  

                        $customerData = addEditCustomer($requestData); // Create Customer Function call.
                        if ($customerData['status'] == SCS) {
                            $custId = $customerData['data'][0]['customerId'];
                        }
                    }
                }
                /* Customer Create Code END */

                $insertInquiryArray = Array(); //INQUIRY ARRAY CREATE
                $insertInquiryArray['C_INQ_ID'] = generateId(INQMASTER, "", $clientId);
                $insertInquiryArray['FK_CLIENT_ID'] = $clientId;
                $insertInquiryArray['FK_ORG_ID'] = $orgId;
                $insertInquiryArray['FK_CUST_ID'] = $custId;
                $insertInquiryArray['INQ_DATE'] = date("Y-m-d");
                $insertInquiryArray['BOOKING_NO'] = '';
                $insertInquiryArray['BOOKING_START_DATE'] = $bookingStartDate;
                $insertInquiryArray['BOOKING_END_DATE'] = $bookingEndDate;
                $insertInquiryArray['FK_PROPERTY_ID'] = $propertyId;
                $insertInquiryArray['FK_TIMEZONE_ID'] = $timezoneId;
                $insertInquiryArray['PROPERTY_NAME'] = $propertyName;
                $insertInquiryArray['TIMEZONE_NAME'] = $timezoneName;
                $insertInquiryArray['STATUS'] = $status;
                
               
                if (!empty($inqId) && $inqId > 0) {
                    $insertInquiryArray['CHANGED_BY'] = $userId;
                    $insertInquiryArray['CHANGED_BY_DATE'] = time();
                    $whereInquiryCond = "PK_INQ_ID ='" . $inqId . "'";
                    $updateInquiry = update_rec(INQMASTER, $insertInquiryArray, $whereInquiryCond); // UPDATE RECORD IN INQ_MASTER TABLE.
                    if (!$updateInquiry) {
                        $msg = UPDATEFAIL . " " . INQMASTER;
                    }
                } else {
                    $insertInquiryArray['CREATED_BY'] = $userId;
                    $insertInquiryArray['CREATED_BY_DATE'] = time();
                    $insertInquiry = insert_rec(INQMASTER, $insertInquiryArray); // INSERT RECORD IN PROPERTY_MASTER TABLE.          
                    if (!isset($insertInquiry['lastInsertedId']) || $insertInquiry['lastInsertedId'] == 0) {
                        $msg = INSERTFAIL . " " . INQMASTER;
                    } else {
                        $inqId = $insertInquiry['lastInsertedId'];
                    }
                }

                if ($msg == "") {

                    $requestData = $customerData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['propertyId'] = $propertyId;
                    $requestData['postData']['startDate'] = date("Y-m-1", strtotime($bookingStartDate));
                    $requestData['postData']['endDate'] = date("Y-m-t", strtotime($bookingEndDate));
                    // $requestData['postData']['inqId'] = $inqId;

                    $customerData = getInquiryDataByProperty($requestData); // GET ALL INQUIRY DATA.                  
                    $customerAllData = ($customerData['status'] == SCS) ? $customerData['data'] : $customerData['status'];

                    $result = array("status" => SCS, "data" => $customerAllData);
                    http_response_code(200);
                } else {
                    $result = array("status" => $msg);
                    http_response_code(400);
                }
            } else {
                $result = array("status" => PROPERTYISALREADYBOOKED);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    }

    return $result;
}

//FUNCTION FOR DELETE INQUIRY DATA
function deleteInquiryData($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $inqId = isset($postData['postData']['inqId']) ? addslashes(trim($postData['postData']['inqId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($inqId)) {
        $updateInqArray = Array(); //INQUIRY ARRAY UPDATE
        $updateInqArray['DELETE_FLAG'] = 1;
        $updateInqArray['CHANGED_BY'] = $userId;
        $updateInqArray['CHANGED_BY_DATE'] = time();

        $whereInqUpdate = " PK_INQ_ID =" . $inqId . " AND FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG = '0' AND FK_ORG_ID='" . $orgId . "'";
        $updateInq = update_rec(INQMASTER, $updateInqArray, $whereInqUpdate); // UPDATE DELETE_FLAG =1 IN INQ_MASTER TABLE.
        if ($updateInq) {
            $requestData = $customerData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $inqData = getInquiryData($requestData); // GET ALL INQ  DATA.

            $inqAllData = ($inqData['status'] == SCS) ? $inqData['data'] : $inqData['status'];
            $result = array("status" => SCS, "data" => $inqAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . INQMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

// GET INQUIRY ALL DATA.
function getInquiryData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $inqId = isset($postData['postData']['inqId']) ? addslashes(trim($postData['postData']['inqId'])) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";

    $inquiryArr = array();
    /* Property Condtion Start */
    $wherePropertyCond = "";
    if (!empty($propertyId)) {
        $wherePropertyCond = " AND FK_PROPERTY_ID='" . $propertyId . "'";
    }
    /* Property Condtion End */
    /* Inq Id Condtion Start */
    $whereInqCond = "";
    if (!empty($inqId)) {
        $whereInqCond = " AND PK_INQ_ID='" . $inqId . "'";
    }
    /* Inq Id Condtion End */


    $sqlInqData = "  SELECT PK_INQ_ID,C_INQ_ID,FK_CUST_ID,FK_CLIENT_ID,FK_ORG_ID,BOOKING_NO,BOOKING_START_DATE,BOOKING_END_DATE,FK_PROPERTY_ID,FK_TIMEZONE_ID,PROPERTY_NAME,TIMEZONE_NAME,STATUS FROM " . INQMASTER . "  WHERE DELETE_FLAG =0 " . $whereInqCond . $wherePropertyCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR INQUIRY DATA.
    $resInqData = fetch_rec_query($sqlInqData); // RESULT FOR INQUIRY DATA.
    if (count($resInqData) > 0) {
        foreach ($resInqData as $keyInq => $valueInq) { // INQUIRY DATA LOOP
            $inquiryArr[$keyInq]['inqId'] = $valueInq['PK_INQ_ID'];
            $inquiryArr[$keyInq]['clientInqId'] = $valueInq['C_INQ_ID'];
            $inquiryArr[$keyInq]['custId'] = $valueInq['FK_CUST_ID'];
            $inquiryArr[$keyInq]['clientId'] = $valueInq['FK_CLIENT_ID'];
            $inquiryArr[$keyInq]['orgId'] = $valueInq['FK_ORG_ID'];
            $inquiryArr[$keyInq]['bookingNo'] = $valueInq['BOOKING_NO'];
            $inquiryArr[$keyInq]['bookingStartDate'] = $valueInq['BOOKING_START_DATE'];
            $inquiryArr[$keyInq]['bookingEndDate'] = $valueInq['BOOKING_END_DATE'];
            $inquiryArr[$keyInq]['propertyId'] = $valueInq['FK_PROPERTY_ID'];
            $inquiryArr[$keyInq]['timezoneId'] = $valueInq['FK_TIMEZONE_ID'];
            $inquiryArr[$keyInq]['propertyName'] = $valueInq['PROPERTY_NAME'];
            $inquiryArr[$keyInq]['timezoneName'] = $valueInq['TIMEZONE_NAME'];
            $inquiryArr[$keyInq]['status'] = $valueInq['STATUS'];
            // CUSTOMER DATA FETCH START
            $requestData = $customerData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['customerId'] = $valueInq['FK_CUST_ID'];


            $customerData = getCustomerData($requestData); // GET ALL PROPERTY DATA.

            $customerAllData = ($customerData['status'] == SCS) ? $customerData['data'] : $customerData['status'];
            $inquiryArr[$keyInq]['customerData'] = $customerAllData;

            // CUSTOMER DATA FETCH END
        }
        $result = array("status" => SCS, "data" => $inquiryArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function customerSearch($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $keyword = isset($postData['postData']['keyword']) ? addslashes(trim($postData['postData']['keyword'])) : "";

    $customerArr = array();

    if (!empty($clientId) && !empty($orgId) && !empty($keyword) && strlen($keyword) >= 2) {
        $whereCustomerCond = "";
        if (!empty($keyword)) {
            $whereCustomerCond = " AND (FIRST_NAME LIKE '%" . $keyword . "%' OR LAST_NAME LIKE'%" . $keyword . "%' OR MOBILE_NO LIKE '%" . $keyword . "%' OR EMAIL_ID LIKE '%" . $keyword . "%')";
        }

        $sqlCustomerData = "  SELECT PK_CUST_ID,C_CUST_ID,FIRST_NAME,LAST_NAME,MOBILE_NO,EMAIL_ID,FK_CLIENT_ID,ADDRESS,PIN_CODE,FK_CITY_ID,FK_STATE_ID,FK_COUNTRY_ID,FK_CLIENT_ID,FK_ORG_ID FROM " . CUSTOMERMASTER . "  WHERE DELETE_FLAG =0 " . $whereCustomerCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR CUSTOMER DATA.

        $resCustomerData = fetch_rec_query($sqlCustomerData); // RESULT FOR CUSTOMER DATA.
        if (count($resCustomerData) > 0) {
            foreach ($resCustomerData as $keyCustomer => $valueCustomer) { // CUSTOMER DATA LOOP
                $customerArr[$keyCustomer]['customerId'] = $valueCustomer['PK_CUST_ID'];
                $customerArr[$keyCustomer]['clientCustomerId'] = $valueCustomer['C_CUST_ID'];
                $customerArr[$keyCustomer]['firstName'] = $valueCustomer['FIRST_NAME'];
                $customerArr[$keyCustomer]['lastName'] = $valueCustomer['LAST_NAME'];
                $customerArr[$keyCustomer]['mobileNo'] = $valueCustomer['MOBILE_NO'];
                $customerArr[$keyCustomer]['clientId'] = $valueCustomer['FK_CLIENT_ID'];
                $customerArr[$keyCustomer]['orgId'] = $valueCustomer['FK_ORG_ID'];
                $customerArr[$keyCustomer]['emailId'] = $valueCustomer['EMAIL_ID'];
                $customerArr[$keyCustomer]['address'] = $valueCustomer['ADDRESS'];
                $customerArr[$keyCustomer]['pincode'] = $valueCustomer['PIN_CODE'];
                $customerArr[$keyCustomer]['cityId'] = $valueCustomer['FK_CITY_ID'];
                $customerArr[$keyCustomer]['stateId'] = $valueCustomer['FK_STATE_ID'];
                $customerArr[$keyCustomer]['countryId'] = $valueCustomer['FK_COUNTRY_ID'];
            }
            $result = array("status" => SCS, "data" => $customerArr);
            http_response_code(200);
        } else {
            //ADD BY DEVIKA.17.7.2018
            $customerSearchArr = Array();
            $customerSearchArr[0]['customerId'] = "-2";
            $customerSearchArr[0]['clientCustomerId'] = "";
            $customerSearchArr[0]['firstName'] = "";
            $customerSearchArr[0]['lastName'] = "";
            $customerSearchArr[0]['mobileNo'] = "";
            $customerSearchArr[0]['clientId'] = "";
            $customerSearchArr[0]['orgId'] = "";
            $customerSearchArr[0]['emailId'] = "";
            $customerSearchArr[0]['address'] = "";
            $customerSearchArr[0]['pincode'] = "";
            $customerSearchArr[0]['cityId'] = "";
            $customerSearchArr[0]['stateId'] = "";
            $customerSearchArr[0]['countryId'] = "";

            $result = array("status" => SCS, "data" => $customerSearchArr);
            http_response_code(200);
            //END BY DEVIKA.17.7.2018
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.17.7.2018
function updateInqStatus($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $inqId = isset($postData['postData']['inqId']) ? addslashes(trim($postData['postData']['inqId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";
    $bookingStartDate = isset($postData['postData']['bookingStartDate']) ? addslashes(trim($postData['postData']['bookingStartDate'])) : "";
    $bookingEndDate = isset($postData['postData']['bookingEndDate']) ? addslashes(trim($postData['postData']['bookingEndDate'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $customerName = isset($postData['postData']['customerName']) ? addslashes(trim($postData['postData']['customerName'])) : "";
    $propertyName = isset($postData['postData']['propertyName']) ? addslashes(trim($postData['postData']['propertyName'])) : "";
    $bookingNo = generateId(INQBOOKING, "", $clientId);
    $todayDate = date('Y-m-d');


    if (!empty($clientId) && !empty($orgId) && !empty($inqId) && !empty($status) && !empty($bookingNo)) {
        $sqlCheckBookingData = "SELECT PK_INQ_ID FROM " . INQMASTER . " WHERE STATUS ='2' AND DELETE_FLAG ='0' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND FK_PROPERTY_ID='" . $propertyId . "' AND (BOOKING_END_DATE BETWEEN '" . $bookingStartDate . "' AND '" . $bookingEndDate . "' OR BOOKING_START_DATE BETWEEN '" . $bookingStartDate . "' AND '" . $bookingEndDate . "' )"; // QUERY FOR CHECK THIS DAY BOOKING IS AVAILABEL OR NOT.

        $resCheckBookingData = fetch_rec_query($sqlCheckBookingData); // RESULT FOR CHECK THIS DAY BOOKING IS AVAILABEL OR NOT.

        if (count($resCheckBookingData) == 0 || $status != "2") {

            $updateInqArray = Array(); //INQUIRY ARRAY UPDATE
            $updateInqArray['BOOKING_NO'] = $bookingNo;
            $updateInqArray['STATUS'] = $status;
            $updateInqArray['BOOKING_DATE'] = $todayDate;
            $updateInqArray['CHANGED_BY'] = $userId;
            $updateInqArray['CHANGED_BY_DATE'] = time();

            $whereInqUpdate = " PK_INQ_ID =" . $inqId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG = '0' AND FK_PROPERTY_ID='" . $propertyId . "'";
            $updateInq = update_rec(INQMASTER, $updateInqArray, $whereInqUpdate); // UPDATE IN INQ_MASTER TABLE.
            if ($updateInq) {
                if ($status == 2) {
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['propertyId'] = $propertyId;
                    $requestData['postData']['customerId'] = $customerId;
                    $requestData['postData']['chargeStartDate'] = $bookingStartDate;
                    $requestData['postData']['chargeEndDate'] = $bookingEndDate;
                    $requestData['postData']['customerId'] = $customerId;
                    $requestData['postData']['customerName'] = $customerName;
                    $requestData['postData']['propertyName'] = $propertyName;
                    $requestData['postData']['invoiceName'] = 'Property Booked';
                    $requestData['postData']['chargeType'] = 'PROPERTY';
                    $requestData['postData']['invoiceType'] = 'PROPERTY';
                    $requestData['postData']['qty'] = '1';

                    $invoiceMasterData = addChargePostingDataForProperty($requestData); // GET ALL INVOICE MASTER DATA.
                    $invoiceData = ($invoiceMasterData['status'] == SCS) ? $invoiceMasterData['inseterdInvoiceId'] : $invoiceMasterData['status'];

                    $result = array("status" => SCS, "inseterdInvoiceId" => $invoiceData);
                    http_response_code(200);
                } else if ($status == 3) {
                    $result = array("status" => SCS);
                    http_response_code(200);
                }
            } else {
                $result = array("status" => UPDATEFAIL . " " . INQMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PROPERTYISALREADYBOOKED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//END BY DEVIKA.17.7.2018
//ADD BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI PHONE MASTER

function updateInsertMultiEntryPhone($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $phoneId = isset($postData['postData']['phoneId']) ? addslashes(trim($postData['postData']['phoneId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $multiPhone = $postData['postData']['multiPhone'] ? $postData['postData']['multiPhone'] : "";

    if (!empty($studentId) && !empty($multiPhone)) {
        foreach ($multiPhone as $keyPhone => $valuePhone) {

            $phoneId = isset($valuePhone['phoneId']) ? $valuePhone['phoneId'] : "";
            $mobileNo = isset($valuePhone['mobileNo']) ? $valuePhone['mobileNo'] : "";
            $comment = isset($valuePhone['comment']) ? $valuePhone['comment'] : "";
            $countryCode = isset($valuePhone['countryCode']) ? $valuePhone['countryCode'] : "";
            $defaultFlag = isset($valuePhone['defaultFlag']) ? $valuePhone['defaultFlag'] : "";
            $deleteFlag = isset($valuePhone['deleteFlag']) ? $valuePhone['deleteFlag'] : "";

            $insertData['FK_STUD_ID'] = $studentId;
            $insertData['FK_CLIENT_ID'] = $clientId;
            $insertData['FK_ORG_ID'] = $orgId;
            $insertData['PHONE_NO'] = $mobileNo;
            $insertData['COMMENT'] = $comment;
            $insertData['COUNTRY_CODE'] = $countryCode;
            $insertData['DEFAULT_FLAG'] = $defaultFlag;
            $insertData['DELETE_FLAG'] = $deleteFlag;
            if (!empty($phoneId)) {
                $insertData['CHANGED_BY'] = $userId;
                $insertData['CHANGED_BY_DATE'] = time();
                $whereUpdate = "PK_PHONE_ID = '" . $phoneId . "' AND FK_STUD_ID = '" . $studentId . "' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG='0'";
                $updateRecords = update_rec(MULTIPHONEMASTER, $insertData, $whereUpdate);
            } else {
                $insertData['CREATED_BY'] = $userId;
                $insertData['CREATED_BY_DATE'] = time();
                $insertRecord = insert_rec(MULTIPHONEMASTER, $insertData);
            }
            $result = array("status" => SCS);
            http_response_code(200);
        }
    } else {

        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI PHONE MASTER
//ADD BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI ADDRESS MASTER

function updateInsertMultiEntryAddress($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $addressId = isset($postData['postData']['addressId']) ? addslashes(trim($postData['postData']['addressId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $multiAddress = $postData['postData']['multiAddress'] ? $postData['postData']['multiAddress'] : "";

    if (!empty($studentId) && !empty($multiAddress)) {
        foreach ($multiAddress as $keyAddress => $valueAddress) {

            $addressId = isset($valueAddress['addressId']) ? $valueAddress['addressId'] : "";
            $address = isset($valueAddress['address']) ? $valueAddress['address'] : "";
            $pinCode = isset($valueAddress['pinCode']) ? $valueAddress['pinCode'] : "";
            $catchArea = isset($valueAddress['catchArea']) ? $valueAddress['catchArea'] : "";
            $comment = isset($valueAddress['comment']) ? $valueAddress['comment'] : "";
            $cityId = isset($valueAddress['cityId']) ? $valueAddress['cityId'] : "";
            $stateId = isset($valueAddress['stateId']) ? $valueAddress['stateId'] : "";
            $countryId = isset($valueAddress['countryId']) ? $valueAddress['countryId'] : "";
            $defaultFlag = isset($valueAddress['defaultFlag']) ? $valueAddress['defaultFlag'] : "";
            $deleteFlag = isset($valueAddress['deleteFlag']) ? $valueAddress['deleteFlag'] : "";

            $insertData['FK_STUD_ID'] = $studentId;
            $insertData['FK_CLIENT_ID'] = $clientId;
            $insertData['FK_ORG_ID'] = $orgId;
            $insertData['ADDRESS'] = $address;
            $insertData['PINCODE'] = $pinCode;
            $insertData['CATCHMENT_AREA'] = $catchArea;
            $insertData['COMMENT'] = $comment;
            $insertData['CITYID'] = $cityId;
            $insertData['STATEID'] = $stateId;
            $insertData['COUNTRYID'] = $countryId;
            $insertData['DEFAULT_FLAG'] = $defaultFlag;
            $insertData['DELETE_FLAG'] = $deleteFlag;
            if (!empty($addressId)) {
                $insertData['CHANGED_BY'] = $userId;
                $insertData['CHANGED_BY_DATE'] = time();
                $whereUpdate = "PK_ADDRESS_ID = '" . $addressId . "' AND FK_STUD_ID = '" . $studentId . "' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG='0'";
                $updateRecords = update_rec(MULTIADDRESSMASTER, $insertData, $whereUpdate);
            } else {
                $insertData['CREATED_BY'] = $userId;
                $insertData['CREATED_BY_DATE'] = time();
                $insertRecord = insert_rec(MULTIADDRESSMASTER, $insertData);
            }
            $result = array("status" => SCS);
            http_response_code(200);
        }
    } else {

        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI ADDRESS MASTER
//ADD BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI EMAIL MASTER

function updateInsertMultiEntryEmail($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $emailAddressId = isset($postData['postData']['emailAddressId']) ? addslashes(trim($postData['postData']['emailAddressId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $multiemailAddress = $postData['postData']['multiemailAddress'] ? $postData['postData']['multiemailAddress'] : "";

    if (!empty($studentId) && !empty($multiemailAddress)) {
        foreach ($multiemailAddress as $keyemailAddress => $valueemailAddress) {

            $emailAddressId = isset($valueemailAddress['emailAddressId']) ? $valueemailAddress['emailAddressId'] : "";
            $emailAddress = isset($valueemailAddress['emailAddress']) ? $valueemailAddress['emailAddress'] : "";
            $comment = isset($valueemailAddress['comment']) ? $valueemailAddress['comment'] : "";
            $defaultFlag = isset($valueemailAddress['defaultFlag']) ? $valueemailAddress['defaultFlag'] : "";
            $deleteFlag = isset($valueemailAddress['deleteFlag']) ? $valueemailAddress['deleteFlag'] : "";

            $insertData['FK_STUD_ID'] = $studentId;
            $insertData['FK_CLIENT_ID'] = $clientId;
            $insertData['FK_ORG_ID'] = $orgId;
            $insertData['EMAIL_ADDRESS'] = $emailAddress;
            $insertData['COMMENT'] = $comment;
            $insertData['DEFAULT_FLAG'] = $defaultFlag;
            $insertData['DELETE_FLAG'] = $deleteFlag;
            if (!empty($emailAddressId)) {
                $insertData['CHANGED_BY'] = $userId;
                $insertData['CHANGED_BY_DATE'] = time();
                $whereUpdate = "PK_EMAIL_ID = '" . $emailAddressId . "' AND FK_STUD_ID = '" . $studentId . "' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG='0'";
                $updateRecords = update_rec(MULTIEMAILMASTER, $insertData, $whereUpdate);
            } else {
                $insertData['CREATED_BY'] = $userId;
                $insertData['CREATED_BY_DATE'] = time();
                $insertRecord = insert_rec(MULTIEMAILMASTER, $insertData);
            }
            $result = array("status" => SCS);
            http_response_code(200);
        }
    } else {

        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA 18.7.2018 UPDATE INSERT INTO MULTI EMAIL MASTER


function addChargePostingDataForProperty($postData) {

    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $itemId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $chargeStartDate = isset($postData['postData']['chargeStartDate']) ? addslashes(trim($postData['postData']['chargeStartDate'])) : "";
    $chargeEndDate = isset($postData['postData']['chargeEndDate']) ? addslashes(trim($postData['postData']['chargeEndDate'])) : "";
    $invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
    $customerName = isset($postData['postData']['customerName']) ? addslashes(trim($postData['postData']['customerName'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $amount = isset($postData['postData']['amount']) ? addslashes(trim($postData['postData']['amount'])) : "";
    $invoiceType = isset($postData['postData']['invoiceType']) ? addslashes(trim($postData['postData']['invoiceType'])) : "";
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "1";
    $chargeType = isset($postData['postData']['chargeType']) ? addslashes(trim($postData['postData']['chargeType'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "1";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "1";



    $checkItemData = "SELECT PK_PROPERTY_ID,PROPERTY_NAME,PRICE,CGST,SGST,IGST,TYPE,HSN_CODE FROM " . PROPERTYMASTER . " WHERE DELETE_FLAG='0' AND PK_PROPERTY_ID ='" . $itemId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
    $resCheckItemData = fetch_rec_query($checkItemData);
    if (count($resCheckItemData) > 0) {
        foreach ($resCheckItemData as $keyItem => $valueItem) {

            /* START Calculate Payment DAYWISE / MONTHWISE FOR PROPERTY */
            $PropertyType = $valueItem['TYPE'];
            $startDate = $chargeStartDate;
            $endDate = $chargeEndDate;
            $count = 0;

            while (strtotime($startDate) <= strtotime($endDate)) {
                if ($PropertyType == "MONTHWISE")
                    $startDate = date('Y-m-d', strtotime($startDate . ' + 1 month'));
                else
                    $startDate = date('Y-m-d', strtotime($startDate . '+1 DAY'));

                $count++;
            }
            /* END Calculate Payment DAYWISE / MONTHWISE FOR PROPERTY */

            $itemName = $valueItem['PROPERTY_NAME'];
            $itemPrice = $valueItem['PRICE'];
            $itemCgst = $valueItem['CGST'];
            $itemSgst = $valueItem['SGST'];
            $itemIgst = $valueItem['IGST'];
            $hsnCode = $valueItem['HSN_CODE'];
            $qtyOfSession = (isset($count) && $count > 1) ? $count : 1;


            $calItemBasicTPrice = $itemPrice * $qtyOfSession;
            $cgstAmt = ($itemCgst > 0) ? ($calItemBasicTPrice * $itemCgst) / 100 : 0;
            $sgstAmt = ($itemSgst > 0) ? ($calItemBasicTPrice * $itemSgst) / 100 : 0;
            $igstAmt = ($itemIgst > 0) ? ($calItemBasicTPrice * $itemIgst) / 100 : 0;

            $calTotalTax = $cgstAmt + $sgstAmt + $igstAmt;
            $calItemTotalPrice = $calItemBasicTPrice + $calTotalTax;
        }
    } else {
        $itemCgst = $itemSgst = $itemIgst = $calTotalTax = "";
        $itemPrice = $calItemTotalPrice = $amount;
    }


    $insertChargePostingArray = Array(); //CHARGE POSTING ARRAY CREATE
    $insertChargePostingArray['FK_CLIENT_ID'] = $clientId;
    $insertChargePostingArray['FK_ORG_ID'] = $orgId;
    $insertChargePostingArray['TYPEOFCUST'] = 'CUSTOMER';
    $insertChargePostingArray['FK_ITEM_ID'] = $itemId;
    $insertChargePostingArray['REF_ITEM_ID'] = $itemId;
    $insertChargePostingArray['ITEM_NAME'] = $itemName;
    $insertChargePostingArray['CHARGE_DATE'] = date("Y-m-d");
    $insertChargePostingArray['STUDENT_ID'] = $customerId;
    $insertChargePostingArray['STUDENT_FULL_NAME'] = $customerName;
    $insertChargePostingArray['QTY_OF_SESSION'] = $qtyOfSession;
    $insertChargePostingArray['BIILED_SESSION'] = $qtyOfSession;
    $insertChargePostingArray['TAX_CGST'] = $itemCgst;
    $insertChargePostingArray['TAX_SGST'] = $itemSgst;
    $insertChargePostingArray['TAX_IGST'] = $itemIgst;
    $insertChargePostingArray['HSN_CODE'] = $hsnCode;
    $insertChargePostingArray['ITEM_BASE_PRICE'] = $itemPrice;
    $insertChargePostingArray['TOTAL_TAX'] = $calTotalTax;
    $insertChargePostingArray['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;
    $insertChargePostingArray['TYPE'] = $chargeType;
    $insertChargePostingArray['CREATED_BY'] = $userId;
    $insertChargePostingArray['CREATED_BY_DATE'] = time();

    $insertChargePosting = insert_rec(CHARGEMASTER, $insertChargePostingArray); // INSERT RECORD IN CHARGE_MASTER TABLE.
    if ($insertChargePosting['lastInsertedId'] > 0) {
        if ($type == -1) {
            $result = array("status" => SCS);
        } else {
            $requestData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['chargeStartDate'] = $chargeStartDate;
            $requestData['postData']['chargeEndDate'] = $chargeEndDate;
            $requestData['postData']['invoiceName'] = $invoiceName;
            $requestData['postData']['itemName'] = $itemName;
            $requestData['postData']['customerId'] = $customerId;
            $requestData['postData']['invoiceType'] = $invoiceType;
            $requestData['postData']['chargeType'] = $chargeType;
            $requestData['postData']['userId'] = $userId;

            $chargeMasterData = createInvoiceForProperty($requestData);
            if ($chargeMasterData['status'] == SCS) {
                $result = array("status" => SCS, "data" => $chargeMasterData['data'], "inseterdInvoiceId" => $chargeMasterData['inseterdInvoiceId']);
            } else {
                $result = array("status" => $chargeMasterData['status']);
            }
        }
    } else {
        $result = array("status" => INSERTFAIL . " charge master table.");
    }
    /* Create Invoice Code Comment END */
    return $result;
}

function createInvoiceForProperty($postData) {

    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $chargeStartDate = !empty($postData['postData']['chargeStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeStartDate'])))) : "";
    $chargeEndDate = !empty($postData['postData']['chargeEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeEndDate'])))) : "";
    $invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $invoiceType = isset($postData['postData']['invoiceType']) ? addslashes(trim($postData['postData']['invoiceType'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $chargeType = isset($postData['postData']['chargeType']) ? addslashes(trim($postData['postData']['chargeType'])) : "";
    // $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "1";
    $periodStartDate = $chargeStartDate;
    $periodEndDate = $chargeEndDate;

    $chargeStartDate = "";
    if (!empty($chargeEndDate) && !empty($invoiceName)) {

        if (strtotime($chargeStartDate) <= strtotime($chargeEndDate) || $chargeStartDate == "") {
            $whereStudentCond = "";
            if (!empty($customerId)) {
                $whereStudentCond = " AND TYPEOFCUST ='CUSTOMER' AND STUDENT_ID='" . $customerId . "'";
            }

            if ($chargeStartDate == "") {
                $wherChargeDate = " AND CHARGE_DATE <='" . $chargeEndDate . "'";
            } else {
                $wherChargeDate = " AND CHARGE_DATE BETWEEN '" . $chargeStartDate . "' AND '" . $chargeEndDate . "'";
            }

            $whereChargeType = "";
            if (!empty($chargeType)) {
                $whereChargeType = " AND TYPE ='" . $chargeType . "'";
            }
            $sqlChargeStudentData = "SELECT STUDENT_FULL_NAME,STUDENT_ID FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND IS_TRANSFER ='0' AND INVOICE_FLAG ='0'  " . $wherChargeDate . $whereStudentCond . $whereChargeType . " GROUP BY STUDENT_ID"; // QUERY    FOR CHARGE MASTER DATA WITHOUT INVOICE


            $resChargeStudentData = fetch_Rec_query($sqlChargeStudentData);
            if (count($resChargeStudentData) > 0) {

                foreach ($resChargeStudentData as $keyStudent => $valueStudent) {// STUDENT DAT FOR EACH LOOP.
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['chargeStartDate'] = $chargeStartDate;
                    $requestData['postData']['chargeEndDate'] = $chargeEndDate;
                    $requestData['postData']['customerId'] = $valueStudent['STUDENT_ID'];
                    $requestData['postData']['chargeType'] = $chargeType;

                    $chargeMasterData = getPostingChargeData($requestData); // GET ALL CHARGE MASTER DATA.
                    if ($chargeMasterData['status'] == SCS) {

                        $chargeMasterArray = $chargeMasterData['data'];
                        $insertInvoiceMaster = Array();
                        $totalPrice = 0;
                        $clientInvoiceId = generateId(INVOICEMASTER, 'INV', $clientId);
                        $insertInvoiceMaster['FK_CLIENT_ID'] = $clientId;
                        $insertInvoiceMaster['FK_ORG_ID'] = $orgId;
                        $insertInvoiceMaster['FK_CUST_ID'] = $customerId;
                        $insertInvoiceMaster['C_INVOICE_ID'] = $clientInvoiceId;
                        $insertInvoiceMaster['INVOICE_DATE'] = date("Y-m-d");
                        $insertInvoiceMaster['INVOICE_NAME'] = $invoiceName;
                        $insertInvoiceMaster['INVOICE_TYPE'] = $invoiceType;
                        $insertInvoiceMaster['INVOICE_REF_ID'] = $itemId;
                        $insertInvoiceMaster['TOTAL_CHARGES'] = '';
                        $insertInvoiceMaster['PERIOD_FROM'] = $periodStartDate;
                        $insertInvoiceMaster['PERIOD_TO'] = $periodEndDate;
                        $insertInvoiceMaster['CREATED_BY'] = $userId;
                        $insertInvoiceMaster['CREATED_BY_DATE'] = time();
                        $insertInvoiceMasterData = insert_rec(INVOICEMASTER, $insertInvoiceMaster);
                        if (isset($insertInvoiceMasterData['lastInsertedId']) && $insertInvoiceMasterData['lastInsertedId'] > 0) {
                            $invoiceNo = $insertInvoiceMasterData['lastInsertedId'];
                            foreach ($chargeMasterArray as $keyCharge => $valueCharge) { // CHARGE MASTER TRANS ARRAY LOOP.
                                if ($valueCharge['totalQty'] > 0 && $valueCharge['itemName'] != "") {
// INVOICE TRANS ARRAY.
                                    $insertInvoiceTrans = array();
                                    $insertInvoiceTrans['FK_INVOICE_ID'] = $clientInvoiceId;
                                    $insertInvoiceTrans['CHARGE_TYPE'] = 'STUDENT';
                                    $insertInvoiceTrans['QTY'] = $valueCharge['totalQty'];
                                    $insertInvoiceTrans['FK_STUDENT_ID'] = $valueStudent['STUDENT_ID'];
                                    $insertInvoiceTrans['FK_ITEM_ID'] = $valueCharge['itemId'];
                                    $insertInvoiceTrans['REF_ITEM_ID'] = $valueCharge['itemId'];
                                    $insertInvoiceTrans['ITEM_NAME'] = $valueCharge['itemName'];
                                    $insertInvoiceTrans['SGST'] = $valueCharge['sgst'];
                                    $insertInvoiceTrans['CGST'] = $valueCharge['cgst'];
                                    $insertInvoiceTrans['IGST'] = $valueCharge['igst'];
                                    $insertInvoiceTrans['HSN_CODE'] = $valueCharge['hsnCode'];
                                    $insertInvoiceTrans['ITEM_BASIC_PRICE'] = $valueCharge['itemBasicPrice'];
                                    $insertInvoiceTrans['TOTAL_CHARGES'] = ($valueCharge['itemBasicPrice'] * $valueCharge['totalQty']);
                                    $insertInvoiceTrans['TOTAL_TAX'] = $valueCharge['totalTax'];
                                    $insertInvoiceTrans['PAYABLE_AMOUNT'] = $valueCharge['totalItemPrice'];
                                    $insertInvoiceTrans['CREATED_BY'] = $userId;
                                    $insertInvoiceTrans['CREATED_BY_DATE'] = time();
                                    $insertInvoiceTransData = insert_rec(INVOICETRANSMASTER, $insertInvoiceTrans);

                                    $totalPrice = $totalPrice + $valueCharge['totalItemPrice'];

                                    $chargeIdsArr = ($valueCharge['chargeIds'] != "") ? explode(",", $valueCharge['chargeIds']) : Array();
                                    if (count($chargeIdsArr) > 0) { // UPDATE  INVOICE_FLAG =1 AND INVOICE ID IN CHARGE MASTER DATA 
                                        foreach ($chargeIdsArr as $keyCharge => $valueChargeIds) { // CHARGE IDS ARRAY LOOP.    
// UPDATE CHARGE ARRAY.
                                            $updateChargeArr = Array();
                                            $updateChargeArr['INVOICE_FLAG'] = 1;
                                            $updateChargeArr['INVOICE_ID'] = $clientInvoiceId;

                                            $whereChargeCond = "PK_CHARGE_ID='" . $valueChargeIds . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; // WHERE CHARGE ID COND.
                                            $updateInvoiceMaster = update_rec(CHARGEMASTER, $updateChargeArr, $whereChargeCond); // UPDATE IN CHARGE_MASTER TABLE.
                                        }
                                    }
                                }
                            }
                            $updateInvoiceArr = Array();
                            $updateInvoiceArr['TOTAL_CHARGES'] = $totalPrice;
                            $whereInvoiceCond = "PK_INVOICE_ID='" . $invoiceNo . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                            $updateInvoiceMaster = update_rec(INVOICEMASTER, $updateInvoiceArr, $whereInvoiceCond); // UPDATE INVOICE TOTAL IN INVOICE MASTER TABLE.

                            $requestData = $studentData = Array();
                            $requestData['postData']['clientId'] = $clientId;
                            $requestData['postData']['orgId'] = $orgId;
                            $requestData['postData']['userId'] = $userId;
                            $requestData['postData']['studentId'] = $valueStudent['STUDENT_ID'];
                            $requestData['postData']['invoiceId'] = $clientInvoiceId;
                            $requestData['postData']['itemName'] = $itemName;

                            $chargeMasterData = sendPropertyInvoiceMail($requestData);  // GENERAL MAIL SENT CODE   
                        }
                    }
                }

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;

                $invoiceMasterData = listInvoiceData($requestData); // GET ALL INVOICE MASTER DATA.
                $invoiceData = ($invoiceMasterData['status'] == SCS) ? $invoiceMasterData['data'] : $invoiceMasterData['status'];
                $result = array("status" => SCS, "data" => $invoiceData, "inseterdInvoiceId" => $clientInvoiceId);
                http_response_code(200);
            } else {

                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => STARTISNOTGREATERTHANENDDATE);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function sendPropertyInvoiceMail($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $clientInvoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";
    $propertyName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";

    $sqlStudentData = "SELECT CONCAT(CM.FIRST_NAME,' ',CM.LAST_NAME) AS STUD_NAME,CM.MOBILE_NO,CM.EMAIL_ID AS EMAIL FROM " . CUSTOMERMASTER . " CM  WHERE CM.DELETE_FLAG ='0' AND CM.PK_CUST_ID='" . $studentId . "' AND CM.FK_CLIENT_ID = '" . $clientId . "' AND CM.FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA

    $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

    $sqlInvoiceTrans = "SELECT ITEM_NAME,QTY,TOTAL_CHARGES,TOTAL_TAX,PAYABLE_AMOUNT,SGST,CGST,IGST,HSN_CODE FROM " . INVOICETRANSMASTER . " WHERE FK_INVOICE_ID='" . $clientInvoiceId . "' AND DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA

    $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA

    $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0'"; // QUERY FOR CLIENT DATA

    $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA

    $sqlPaymentInvoice = "SELECT SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $clientInvoiceId . "' AND FK_STUDENT_ID ='" . $studentId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; //QUERY FOR INVOICE PAYMENT DATA 
    $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);


    $sqlPeriodData = "SELECT PERIOD_FROM,PERIOD_TO FROM " . INVOICEMASTER . " WHERE C_INVOICE_ID = '" . $clientInvoiceId . "' AND DELETE_FLAG = '0'";
    $resPeriodData = fetch_rec_query($sqlPeriodData);

    $totalPayment = 0;
    if (count($resPaymentInvoice) > 0) {
        $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
    }
    if (!empty($resStudentData[0]['EMAIL'])) {

        /* Mail sent code start */
        $sendgrid_username = "arunnagpal";
        $sendgrid_password = "devil1502";

        $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


        //$totalPrevBalance = 0;
        //$totalPrevBalanceType = "";
        $htmlInvioiceTemplateHeader = '<!DOCTYPE html>' .
                '<html lang="en">' .
                '<head>' .
                '<meta charset="utf-8">' .
                '<meta name="viewport" content="width=device-width, initial-scale=1.0">' .
                '<meta name="description" content="">' .
                '<meta name="author" content="Mosaddek">' .
                '<link rel="shortcut icon" href="img/favicon.ico">' .
                '<title></title>' .
                '</head>' .
                '<body class="invoice-page" data-gr-c-s-loaded="true"> <div style="background: #ffffff none repeat scroll 0 0; color: #333333; font-family: DejaVu Sans; sans-serif;font-size: 9pt;"><div class="pcs-template-header pcs-header-content" id="header" style="height: 0.1in; padding: 0 0.4in 0 0.55in; background-color: #ffffff; color: #333333; font-size: 9pt;"></div><div class="pcs-template-body" style="padding: 0 0.4in 0 0.55in;">';

        $htmlTableLogo = '<table style="width:100%;table-layout: fixed;">' .
                '<tbody>' .
                '<tr>' .
                '<td colspan = 2 style="vertical-align: top; width:100%; text-align: center;">' .
                '<span class="pcs-entity-title" style="color: #000000; font-size: 18pt;">Tax Invoice</span><br><span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020<br /><br />' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="vertical-align: top; width:50%;">' .
               
                '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice No &nbsp;&nbsp;: ' . $clientInvoiceId . '</b></span><br>' .
                '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice Date : ' . date("d-m-Y") . '</b></span><br>' .
                // '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Period From : ' . $getInvoiceDetail['data'][0]['invoicePeroidFrom'] . '</b></span>' .
                '</td>' .
                '<td style="vertical-align: top; text-align:right;width:50%;">' .
                '<div>' .
                // '<img src="' . APP_SERVER_ROOT_FRONTEND . '/img/gulmoharInvLogo.png" style="width:160.00px;height:129.00px;" id="logo_content">' .
                '</div>' .
                '</td>' .
                '</tr>' .
                '</tbody>' .
                '</table>';

        $htmlMemberDetail = '<table style="clear:both;width:100%;margin-top:30px;table-layout:fixed;">' .
                '<tbody><tr><td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                '</td>' .
                '<td style="vertical-align:bottom;width: 40%;" align="right"></td>' .
                '</tr><tr>' .
                '<td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                '<div><label style="font-size: 14pt; color:#817d7d" class="pcs-label" id="tmp_billing_address_label">To,</label><br>' .
                '<span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 12pt;">' . $resStudentData[0]['STUD_NAME'] . '</span><br>' .
                '</div>' .
                '</td>' .
                '<td style="vertical-align:bottom;width: 40%;" align="right">' .
                '</td>' .
                '</tr>' .
                '</tbody></table>';

        $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;"><br><table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
                '<thead>' .
                '<tr style="height:32px;">' .
                '<td style="padding:5px 0 5px 5px;text-align: center;word-wrap: break-word;width: 5%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header">#</td>' .
                '<td style="padding:5px 10px 5px 20px;word-wrap: break-word; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;width: 15%; " class="pcs-itemtable-header pcs-itemtable-description">Particular</td>' .
                '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">RATE</td>' .
                '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">QTY</td>' .
                '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">CGST</td>' .
                '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">SGST</td>' .
                 '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">IGST</td>' .
                '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width:20%; background-color: #3c3d3a; color: #ffffff;font-size: 9pt;background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>' .
                '</tr>' .
                '</thead>' .
                '<tbody class="itemBody">';
        $totalCharges = $totalSgstAmt = $totalCgstAmt = $totalPayable = 0;
        $invoiceItemTransMid = "";
        if (!empty($resInvoiceTrans)) {
            $totalSgstAmt = $totalCgstAmt = $totalCharges = $totalPayable = $totalIgstAmt=0;
            foreach ($resInvoiceTrans as $keyTrans => $valueTrans) {

                if (!empty($valueTrans['PAYABLE_AMOUNT'])) {
                    $sgstAmt = $cgstAmt = $igstAmt =0;
                    $itemPrice = $valueTrans['TOTAL_CHARGES'] / $valueTrans['QTY'];
                    $sgstAmt = ($valueTrans['SGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['SGST'] / 100 : 0;
                    $cgstAmt = ($valueTrans['CGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['CGST'] / 100 : 0;
                     $igstAmt = ($valueTrans['IGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['IGST'] / 100 : 0;
                    $totalSgstAmt = $totalSgstAmt + $sgstAmt;
                    $totalCgstAmt = $totalCgstAmt + $cgstAmt;
                    $totalIgstAmt = $totalIgstAmt + $igstAmt;

                    $invoiceItemTransMid .= '<tr>' .
                            '<td style="padding: 10px 0 10px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">' . ($keyTrans + 1) . '</td>' .
                            '<td style="padding: 10px 0px 10px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" ><div><div><span style="word-wrap: break-word;" id="tmp_item_name">' . $valueTrans['ITEM_NAME'] . '</span><br><span class="pcs-item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">' . $valueTrans['HSN_CODE'] . '</span></div></div></td>' .
                            '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($itemPrice, 2) . '</span></td>' .
                            '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . $valueTrans['QTY'] . '</span></td>' .
                            '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($cgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['CGST'] . '% </div></td>' .
                            '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($sgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['SGST'] . '% </div></td>' .
                              '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($igstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['IGST'] . '% </div></td>' .
                            '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($valueTrans['TOTAL_CHARGES'], 2) . '</span></td>' .
                            '</tr>';

                    $totalCharges = $totalCharges + ($valueTrans['TOTAL_CHARGES']);
                    $totalPayable = $totalPayable + $valueTrans['PAYABLE_AMOUNT'];
                }
            }
        }
        $prevBalance = 0;
        $balanceDue = $prevBalance + $totalPayment - $totalPayable;
        $balanceDueType = ($balanceDue > 0) ? 'CR' : 'DR';

        $invoiceItemTransFooter = '</tbody></table>';

        $htmlInvoiceTax = '<div style="width: 100%;margin-top: 1px; float:right;">' .
                '<div style="width: 100%;">' .
                '<table width="100%"><tr><td style="width:50%; float:left;"></td><td style="text-align:right;width:45%; float:right;">' .
                '<table class="pcs-totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">' .
                '<tbody>' .
                '<tr>' .
                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>' .
                '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                '</tr>' .
                '<tr style="height:10px;">' .
                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>' .
                '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalSgstAmt, 2) . '</td>' .
                '</tr>' .
                '<tr style="height:10px;">' .
                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>' .
                '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCgstAmt, 2) . '</td>' .
                '</tr>' .
                 '<tr style="height:10px;">' .
                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>' .
                '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalIgstAmt, 2) . '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>' .
                '<td id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>DR</b></td>' .
                '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayable, 2) . '</b></td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>' .
                '<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>CR</b></td>' .
                '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayment, 2) . '</b></td>' .
                '</tr>' .
                '<tr style="height:40px; background-color: #f5f4f3; color: #000000; font-size: 9pt; font-size: 8pt; background-color: #f5f4f3;color: #000000; font-size: 9pt;" class="pcs-balance">' .
                '<td style="padding:5px 10px 5px 0;" valign="middle" align="right"><b>Balance Due</b></td>' .
                '<td id="tmp_balance_due" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . $balanceDueType . '</b></td>' .
                '<td id="tmp_balance_due" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(abs($balanceDue), 2) . '</b></td>' .
                '</tr>' .
                '</tbody>' .
                '</table>' .
                '</td></tr></table>' .
                '</div>' .
                '<div style="clear: both;"></div>' .
                '</div>';
        $htmlInvoiceTempTM ="";
       // $htmlInvoiceTempTM = '<div style="margin-top:10px;"><b><u>Terms and Conditions</u></b>' .
              /*  '<table style="width:100%;font-size: 10px;">' .
                '<tbody><tr>' .
                '<td style="padding-top:60px;padding-bottom: 8px;">' .
                '<span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom: 8px;">' .
                '<span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom: 8px;">' .
                '<span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom: 8px;">' .
                '<span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom: 8px;">' .
                '<span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom: 8px;">' .
                '<span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus </span></b>".</span>' .
                '</td>' .
                '</tr>' .
                '</tbody>' .
                '</table>' .
                '</div><div style="font-size: 14px;"><b>*Please note this is a computer generated invoice and hence dose not need signature.</b></div>';*/
        $htmlInvioceTemplateFooter = '</div> <div class="pcs-template-footer" style="background-color: #ffffff;  color: #aaaaaa; font-size: 6pt; height: 0.7in; padding: 0 0.4in 0 0.55in;"></div></div></body></html>';

        // FINAL HTML
        $finalHtml = $htmlInvioiceTemplateHeader . $htmlTableLogo . $htmlMemberDetail . ($invoiceItemTransHeader . $invoiceItemTransMid . $invoiceItemTransFooter) . $htmlInvoiceTax . $htmlInvoiceTempTM . $htmlInvioceTemplateFooter;
        //print_r($finalHtml); exit;
        //$dompdf_temp = new Dompdf();


        $dompdf = new Dompdf\Dompdf();
        $dompdf->load_html($finalHtml);
        //$dompdf_temp->set_paper("a4", "portrait");
        $dompdf->set_paper(array(0, 0, 794, 1122), "portrait");
        $dompdf->render();
        $pdfOutput = $dompdf->output();

        //echo $pdfOutput;die;
        $fileLocation = APP_SERVER_ROOT . "/attachment/" . time() . ".pdf";
        $fp = fopen($fileLocation, "a+");
        fwrite($fp, $pdfOutput);
        fclose($fp);

        $clientDetail = getClientDetail($clientId);

        // TEMPLATE FOR HTML
        // $messageBodyMid = '<p>
        //         Please find attched invoice document ' .
        //         '</p>';

        $messageBodyMid = '<!DOCTYPE html>
    <html class="no-js">
   <head>
      <link rel="stylesheet" type="text/css" href="css/app.css">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width">
      <title>Email Template</title>
      <!-- <style> -->
   </head>
    <body style="background:#fff;">
        <table style="background:#0077c0; padding:10px 10px 10px 10px; width:100%; max-width:600px;margin:0 auto;">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" style="border-spacing: 0px;">
                            <tr>
                                <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #a6ce39 solid;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                            </tr>
                        </table>  
                        <table width="100%" style="background:#ffffff;border-spacing: 0px; border-bottom:4px #a6ce39 solid;">
                            <tbody>
                                <tr>
                                    <td valign="middel" style="padding:20px 10px 0px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"><span><b>Property Name : </b> <span>' . $propertyName . '</span></span> <p style="margin:0 0 5px 0;">Indecampus Student Accommodations (Dd2)Private Limited is a Non-govt company Company.</p></td>
                                </tr>
                                <tr>                                    
                                <tr>
                                    <td>
                                        <table width="100%" >
                                            <tr>
                                               <td valign="middel" style="padding:10px 10px 10px 10px; text-align:center; color:#000; font-size:14px;font-family: arial; line-height: 18px;width:50%;">
                                                   <span style="display:block;">
                                                       <b style="display:block; background:#a6ce39;color:#ffffff;padding: 5px 5px;">Tenure From </b> 
                                                       <span style="display:block; background:#0077c0; color:#ffffff; padding: 20px 5px;">' . date('d-m-Y', strtotime($resPeriodData[0]['PERIOD_FROM'])) . '</span>
                                                   </span>                                              
                                               </td>
                                               <td valign="middel" style="padding:10px 10px 10px 10px; text-align:center; color:#000; font-size:14px;font-family: arial; line-height: 18px;width:50%;">
                                                   <span style="display:block;">
                                                       <b style="display:block; background:#a6ce39 ;color:#ffffff;padding: 5px 5px;">Tenure To </b> 
                                                       <span style="display:block; background:#0077c0; color:#ffffff; padding: 20px 5px;">' . date('d-m-Y', strtotime($resPeriodData[0]['PERIOD_TO'])) . '</span>
                                                   </span>                                              
                                               </td>                                               
                                            </tr>
                                        </table>
                                    </td>                                                                        
                                </tr>
                                <tr>
                                    <td valign="middel" colspan="2" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;">Thanks &amp; Regards. <br/> ' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/> ' . $resClientTrans[0]['ADDRESS'] . ' </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" style="border-spacing: 0px;">
                            <tbody>
                                <tr>
                                    <td valign="middel" style="padding:25px 5px 25px 5px; background:#0077c0; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                                </tr>
                            </tbody>      
                        </table> 
                    </td>
                </tr>            
            </tbody>
        </table>
    </body>
</html>

';

        $messageBody = $messageBodyMid;
        //    $fromAddress = $resultSms[0]['CLIENT_EMAIL'];
        //   $fromName = $resultSms[0]['CLIENTNAME'];

        $fromAddress = 'support@lithe.in';
        $fromName = 'Lithe Technology';
        $email = new SendGrid\Email();
        $email->addTo($resStudentData[0]['EMAIL']);
        // $email->addToName($valueSender);
        $email->setFrom($fromAddress);

        $allAttachmentArr[] = $fileLocation;

        //print_r($allAttachmentArr); exit;
        if (!empty($allAttachmentArr)) {
            foreach ($allAttachmentArr as $docKey => $docValue) {
                if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                    $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                }
            }
        }

        $subject = "INDE CAMPUS :Rental Invoice";
        $email->setFromName($fromName);
        $email->setSubject($subject);

        //$messageBody = "please find attached document for ";
        $email->setHtml($messageBody);
        //                       print_r($email); exit;
        $response = $sendGrid->send($email);
        $responseMail = $response->getBody();


        if (!empty($allAttachmentArr)) {
            foreach ($allAttachmentArr as $docKey => $docValue) {
                if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                    unlink(APP_SERVER_ROOT . "/attachment/" . basename($docValue)); // add attachment 
                }
            }
        }
        /* Mail sent code End */
    }
}

?>
