<?php 

/***************************************************/
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : DARSHAN PATEL                  */
/*   CREATION DATE : 03/07/2018                    */
/*   FILE TYPE : PHP                               */
/*   FILE NAME : LITHE LOOKUP MASTER               */
/***************************************************/

//LITHE LOOKUP LISTING
function getLitheLookupListing_BL($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    
    $sampleLookupType  = "SELECT DISTINCT LOOK_TYPE,LOOK_VALUE,PK_CLT_LKUP_ID,ISDEFAULTFLAG FROM ".LITHELOOKUP." WHERE LOOK_TYPE NOT IN('USERLEVEL')  AND DELETE_FLAG = 0 AND LOOK_VALUE != ''";
    $resultSampleType = fetch_rec_query($sampleLookupType);
    //print_r($resultSampleType); exit;
    if(count($resultSampleType) > 0){
        $finaJson = array();
//        for($i=0;$i<count($resultSampleType);$i++){
        foreach($resultSampleType as $keySample => $valueSample){
            
            $finaJson[$keySample]['lookupType'] = $valueSample['LOOK_TYPE'];
            $finaJson[$keySample]['lookupTypeName'] = $valueSample['LOOK_VALUE'];
            $finaJson[$keySample]['lookupTypeId'] = $valueSample['PK_CLT_LKUP_ID'];
            
            
            $sqlLookupMaster = "SELECT PK_CLT_LKUP_ID,LOOK_VALUE,DISPLAY_VALUE,ISDEFAULTFLAG FROM ".LITHELOOKUP." LM WHERE LM.LOOK_TYPE = '".$valueSample['PK_CLT_LKUP_ID']."' AND LM.DELETE_FLAG = 0 ";
            $resultLookup = fetch_rec_query($sqlLookupMaster);
            if(count($resultLookup) > 0){
                $tempInnerLookup = array();
//                for($j=0;$j<count($resultLookup);$j++){
                foreach($resultLookup as $keyLookup => $valueLookup){
                    $tempInnerLookup[$keyLookup]['lookupId'] = $valueLookup['PK_CLT_LKUP_ID'];
                    $tempInnerLookup[$keyLookup]['lookupValue'] = $valueLookup['LOOK_VALUE'];
                    $tempInnerLookup[$keyLookup]['displayValue'] = $valueLookup['DISPLAY_VALUE'];
                    $tempInnerLookup[$keyLookup]['isDefaultValue'] = $valueLookup['ISDEFAULTFLAG'];
                    
                }
                $finaJson[$keySample]['lookupValues'] = array_values($tempInnerLookup);
            }else{
                $finaJson[$keySample]['lookupValues'] = NORECORDS;
            }
        }
        $result = array("status"=>SCS,"data"=>  array_values($finaJson));
        http_response_code(200);
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//CREATE LITHE LOOKUP
function createLitheLookup($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupType = isset($postData['postData']['lookupType']) ? addslashes(trim($postData['postData']['lookupType'])) : "";
    $lookupValue = isset($postData['postData']['lookupValue']) ? addslashes(trim($postData['postData']['lookupValue'])) : "";
    //$displayValue = addslashes(trim($postData['postData']['displayValue']));
    $isDefaultValue = isset($postData['postData']['isDefaultValue']) ? addslashes(trim($postData['postData']['isDefaultValue'])) : "";
    // INSERT RECOD INTO LOOKUP MANAGEMENT
    $checkDefaultFalg = "SELECT PK_CLT_LKUP_ID FROM ".LITHELOOKUP." LM WHERE LM.LOOK_TYPE = '".$lookupType."' AND DELETE_FLAG = 0 AND ISDEFAULTFLAG = 1 ";
    $resultCheckDfltFlag = fetch_rec_query($checkDefaultFalg);
    if(count($resultCheckDfltFlag) <= 0){
        $flag = true;
    }else if($isDefaultValue == 0){
        $flag = true;
    }else{
        $result = array("status"=>NOTALLOWEDTOHAVEMULTIPLEDEFAULT);
        $flag = false;
        http_response_code(400);
    }
    
    if($flag){
        $insertReocrd['LOOK_TYPE'] = $lookupType;
        $insertReocrd['DISPLAY_VALUE'] = $lookupValue;
        $insertReocrd['ISDEFAULTFLAG'] = $isDefaultValue;
        $insertReocrd['CREATED_BY'] = $userId;
        $insertReocrd['CREATED_BY_DATE'] = time();
        $insertReocrd['DELETE_FLAG'] = 0;

        $insertLookup = insert_rec(LITHELOOKUP, $insertReocrd);
        if($insertLookup){
            $finalJson = array("lookupId"=>$insertLookup['lastInsertedId']);
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else{
            $result = array("status"=>INSERTQUERYFAIL);
            http_response_code(400);
        }
    }
    return $result;
}

//UPDATE LITHE LOOKUP
function updateLitheLookup($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupType = isset($postData['postData']['lookupType']) ? addslashes(trim($postData['postData']['lookupType'])) : "";
    $lookupValue = isset($postData['postData']['lookupValue']) ? addslashes(trim($postData['postData']['lookupValue'])) : "";
    $lookupId = isset($postData['postData']['lookupId']) ? addslashes(trim($postData['postData']['lookupId'])) : "";
    $isDefaultValue = isset($postData['postData']['isDefaultValue']) ? addslashes(trim($postData['postData']['isDefaultValue'])) : "";
    
    $checkDefaultFalg = "SELECT PK_CLT_LKUP_ID FROM ".LITHELOOKUP." LM WHERE LM.LOOK_TYPE = '".$lookupType."' AND DELETE_FLAG = 0 AND ISDEFAULTFLAG = 1 AND PK_CLT_LKUP_ID != '".$lookupId."' ";
    $resultCheckDfltFlag = fetch_rec_query($checkDefaultFalg);
    if(count($resultCheckDfltFlag) <= 0){
        $flag = true;
    }else if($isDefaultValue == 0){
        $flag = true;
    }else{
        $result = array("status"=>NOTALLOWEDTOHAVEMULTIPLEDEFAULT);
        $flag = false;
        http_response_code(400);
    }
    
    if($flag){
        // START UPDATING LOOKUP MASTER INTO CLIENT LOOKUP TABLE
        $updateLookUp['LOOK_TYPE'] = $lookupType;
        $updateLookUp['DISPLAY_VALUE'] = $lookupValue;
        $updateLookUp['ISDEFAULTFLAG'] = $isDefaultValue;
        $updateLookUp['CHANGED_BY'] = $userId;
        $updateLookUp['CHANGED_BY_DATE'] = time();
        $updateLookUp['DELETE_FLAG'] = 0;

        // WHERE
        $whereLookup = " PK_CLT_LKUP_ID = '".$lookupId."' ";

        $updateLookup = update_rec(LITHELOOKUP, $updateLookUp, $whereLookup);
        if($updateLookup){
            $result = array("status"=>SCS);
            http_response_code(200);
        }else{
            $result = array("status"=>UPDATEQUERYFAIL);
            http_response_code(400);
        }
    }
    
    return $result;
}

//DELETE LITHE LOOKUP
function deleteLitheLookup($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupId = isset($postData['postData']['lookupId']) ? addslashes(trim($postData['postData']['lookupId'])) : "";
    
    // DELETE RECORD FROM LOOKUP MANAGEMENT
    $updateLookUp = array();
    $updateLookUp['CHANGED_BY'] = $userId;
    $updateLookUp['CHANGED_BY_DATE'] = time();
    $updateLookUp['DELETE_FLAG'] = 1;
    
    // where
    $whereLookup = " PK_CLT_LKUP_ID = '".$lookupId."' ";
    $updateLookup = update_rec(LITHELOOKUP, $updateLookUp, $whereLookup);
    if($updateLookup){
        $result = array("status"=>SCS);
        http_response_code(200);
    }else{
        $result = array("status"=>UPDATEQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//LITHE TYPE LISTING
function getLitheListingType($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    
    // GET LISTING OF TYPE 
    $sqlType = "SELECT PK_CLT_LKUP_ID,LOOK_TYPE,LOOK_VALUE FROM ".LITHELOOKUP." CLT WHERE DISPLAY_VALUE = '' AND DELETE_FLAG = 0 ";
    $resultType = fetch_rec_query($sqlType);
    if(count($resultType)){
        $finalJson = array();

        foreach($resultType as $keyRes => $valueRes){
            $finalJson[$keyRes]['lookupId'] = $valueRes['PK_CLT_LKUP_ID'];
            $finalJson[$keyRes]['lookupShortCode'] = $valueRes['LOOK_TYPE'];
            $finalJson[$keyRes]['lookupValue'] = $valueRes['LOOK_VALUE'];
            
        }
        $result = array("status"=>SCS,"data"=>  array_values($finalJson));
        http_response_code(200);
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//CREATE EDIT LITHE TYPE
function litheCreateEditType($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $typeName = isset($postData['postData']['typeName']) ? addslashes(trim($postData['postData']['typeName'])) : "";
    $typeShortCode = isset($postData['postData']['typeShortCode']) ? addslashes(trim($postData['postData']['typeShortCode'])) : "";
    $typeId = isset($postData['postData']['typeId']) ? addslashes(trim($postData['postData']['typeId'])) : "" ;
    
    
    if(!empty($typeId)){ // UPDATE CASE
        $updateLookup = array();
        $updateLookup['LOOK_TYPE'] = $typeShortCode;
        $updateLookup['LOOK_VALUE'] = $typeName;
        //$updateLookup['DISPLAY_VALUE'] = $typeName;
        $updateLookup['CHANGED_BY'] = $userId;
        $updateLookup['CHANGED_BY_DATE'] = time();
        $updateLookup['DELETE_FLAG'] = 0;
        // WHERE
        $updateWhere = "PK_CLT_LKUP_ID = '".$typeId."'";
        $updateRequest = update_rec(LITHELOOKUP, $updateLookup, $updateWhere);
        if($updateRequest){
            $finalJson = array("lookupId"=>$typeId);
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else{
            $result = array("status"=>UPDATEQUERYFAIL);
            http_response_code(400);
        }
    }else{
        // START INSERTING DATA INTO CLIENT LOOKUP MASTER TABLE
        $insertLookup = array();
        $insertLookup['LOOK_TYPE'] = $typeShortCode;
        $insertLookup['LOOK_VALUE'] = $typeName;
        $insertLookup['ISDEFAULTFLAG'] = 0;
        $insertLookup['CREATED_BY'] = $userId;
        $insertLookup['CREATED_BY_DATE'] = time();
        $insertLookup['DELETE_FLAG'] = 0;

        // INSERT
        $insertLookupRequest = insert_rec(LITHELOOKUP,$insertLookup);
        if($insertLookupRequest){
            $finalJson = array("lookupId"=>$insertLookupRequest['lastInsertedId']);
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else{
            $result = array("status"=>INSERTQUERYFAIL);
            http_response_code(400);
        }
    }
    
    return $result;
}

//DELETE LITHE
function deleteLitheType($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $typeId = isset($postData['postData']['typeId']) ? addslashes(trim($postData['postData']['typeId'])) : "" ;
    if(!empty($typeId)){
        $updateLookup = array();
        $updateLookup['DELETE_FLAG'] = 1;
        $updateLookup['CHANGED_BY'] = $userId;
        $updateLookup['CHANGED_BY_DATE'] = time();
        // WHERE
        $updateWhere = "PK_CLT_LKUP_ID = '".$typeId."' ";
        $updateRequest = update_rec(LITHELOOKUP, $updateLookup, $updateWhere);
        if($updateRequest){
            $finalJson = array("lookupId"=>$typeId);
            $result = array("status"=>SCS,"data"=>$finalJson);
            http_response_code(200);
        }else{
            $result = array("status"=>UPDATEQUERYFAIL);
            http_response_code(400);
        } 
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//CREATE EDIT TYPE
function createEditType($postData) {
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $typeName = isset($postData['postData']['typeName']) ? addslashes(trim($postData['postData']['typeName'])) : "";
    $typeShortCode = isset($postData['postData']['typeShortCode']) ? addslashes(trim($postData['postData']['typeShortCode'])) : "";
    $typeId = isset($postData['postData']['typeId']) ? addslashes(trim($postData['postData']['typeId'])) : "";
   

    if (!empty($typeId)) { // UPDATE CASE
        $updateLookup = array();
        $updateLookup['LOOK_TYPE'] = $typeShortCode;
        $updateLookup['LOOK_VALUE'] = $typeName;
        //$updateLookup['DISPLAY_VALUE'] = $typeName;
        $updateLookup['CHANGED_BY'] = $userId;
        $updateLookup['CHANGED_BY_DATE'] = time();
        $updateLookup['DELETE_FLAG'] = 0;
        // WHERE
        $updateWhere = "PK_CLT_LKUP_ID = '" . $typeId . "' AND FK_CLIENT_ID = '" . $clientId . "'";
        $updateRequest = update_rec(CLIENTLOOKUP, $updateLookup, $updateWhere);
        if ($updateRequest) {
            $finalJson = array("lookupId" => $typeId);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else {
        // START INSERTING DATA INTO CLIENT LOOKUP MASTER TABLE
        $insertLookup = array();
        $insertLookup['LOOK_TYPE'] = $typeShortCode;
        $insertLookup['LOOK_VALUE'] = $typeName;
        $insertLookup['ISDEFAULTFLAG'] = 0;
        $insertLookup['FK_CLIENT_ID'] = $clientId;
        $insertLookup['CREATED_BY'] = $userId;
        $insertLookup['CREATED_BY_DATE'] = time();
        $insertLookup['DELETE_FLAG'] = 0;

        // INSERT
        $insertLookupRequest = insert_rec(CLIENTLOOKUP, $insertLookup);
        if ($insertLookupRequest) {
            $finalJson = array("lookupId" => $insertLookupRequest['lastInsertedId']);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => INSERTQUERYFAIL);
            http_response_code(400);
        }
    }

    return $result;
}

//DELETE TYPE
function deleteType($postData) {
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $typeId = isset($postData['postData']['typeId']) ? addslashes(trim($postData['postData']['typeId'])) : "";
    if (!empty($typeId)) {
        $updateLookup = array();
        $updateLookup['DELETE_FLAG'] = 1;
        $updateLookup['CHANGED_BY'] = $userId;
        $updateLookup['CHANGED_BY_DATE'] = time();
        // WHERE
        $updateWhere = "PK_CLT_LKUP_ID = '" . $typeId . "' AND FK_CLIENT_ID = '" . $clientId . "'";
        $updateRequest = update_rec(CLIENTLOOKUP, $updateLookup, $updateWhere);
        if ($updateRequest) {
            $finalJson = array("lookupId" => $typeId);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//TYPE LISTING
function getListingType($postData) { 
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    // GET LISTING OF TYPE 
    $sqlType = "SELECT PK_CLT_LKUP_ID,LOOK_TYPE,LOOK_VALUE FROM " . CLIENTLOOKUP . "  WHERE DISPLAY_VALUE = '' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = 0 ORDER BY PK_CLT_LKUP_ID DESC";
    $resultType = fetch_rec_query($sqlType);
    if (count($resultType)) {
        $finalJson = array();
        
        foreach ($resultType as $keyType => $valueType) {
            $finalJson[$keyType]['lookupId'] = $valueType['PK_CLT_LKUP_ID'];
            $finalJson[$keyType]['lookupShortCode'] = $valueType['LOOK_TYPE'];
            $finalJson[$keyType]['lookupValue'] = $valueType['LOOK_VALUE'];
            
        }
        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//LOOKUP LISTING
function getLookupListing_BL($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";   
   

    $sampleLookupType = "SELECT DISTINCT LOOK_TYPE,LOOK_VALUE,PK_CLT_LKUP_ID,ISDEFAULTFLAG FROM " . CLIENTLOOKUP . " WHERE LOOK_TYPE NOT IN('USERLEVEL') AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = 0 AND LOOK_VALUE != '' ";
    $resultSampleType = fetch_rec_query($sampleLookupType);
    //print_r($resultSampleType); exit;
    if (count($resultSampleType) > 0) {
        $finaJson = array();
        //for($i=0;$i<count($resultSampleType);$i++){
        foreach ($resultSampleType as $keyST => $valueST) {
            $finaJson[$keyST]['lookupType'] = $valueST['LOOK_TYPE'];
            $finaJson[$keyST]['lookupTypeName'] = $valueST['LOOK_VALUE'];
            $finaJson[$keyST]['lookupTypeId'] = $valueST['PK_CLT_LKUP_ID'];
            
            $sqlLookupMaster = "SELECT PK_CLT_LKUP_ID,LOOK_VALUE,DISPLAY_VALUE,ISDEFAULTFLAG FROM " . CLIENTLOOKUP . " LM WHERE LM.LOOK_TYPE = '" . $valueST['PK_CLT_LKUP_ID'] . "' AND LM.DELETE_FLAG = 0 AND LM.FK_CLIENT_ID = '" . $clientId . "' ";
            $resultLookup = fetch_rec_query($sqlLookupMaster);

            if (count($resultLookup) > 0) {
                $tempInnerLookup = array();
                //for($j=0;$j<count($resultLookup);$j++){
                foreach ($resultLookup as $keyLkp => $valuelkp) {
                    $tempInnerLookup[$keyLkp]['lookupId'] = $valuelkp['PK_CLT_LKUP_ID'];
                    $tempInnerLookup[$keyLkp]['lookupValue'] = $valuelkp['LOOK_VALUE'];
                    $tempInnerLookup[$keyLkp]['displayValue'] = $valuelkp['DISPLAY_VALUE'];
                    $tempInnerLookup[$keyLkp]['isDefaultValue'] = $valuelkp['ISDEFAULTFLAG'];                   
                }
                $finaJson[$keyST]['lookupValues'] = $tempInnerLookup;
            } else {
                $finaJson[$keyST]['lookupValues'] = NORECORDS;
            }
        }
        $result = array("status" => SCS, "data" => $finaJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//UPDATE LOOKUP
function updateLookup($postData) {
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupType = isset($postData['postData']['lookupType']) ? addslashes(trim($postData['postData']['lookupType'])) : "";
    $lookupValue = isset($postData['postData']['lookupValue']) ? addslashes(trim($postData['postData']['lookupValue'])) : "";
    $lookupId = isset($postData['postData']['lookupId']) ? addslashes(trim($postData['postData']['lookupId'])) : "";
    $isDefaultValue = isset($postData['postData']['isDefaultValue']) ? addslashes(trim($postData['postData']['isDefaultValue'])) : "";


    $checkDefaultFalg = "SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " LM WHERE LM.LOOK_TYPE = '" . $lookupType . "' AND DELETE_FLAG = 0 AND ISDEFAULTFLAG = 1 AND PK_CLT_LKUP_ID != '" . $lookupId . "' ";
    $resultCheckDfltFlag = fetch_rec_query($checkDefaultFalg);
    if (count($resultCheckDfltFlag) <= 0) {
        $flag = true;
    } else if ($isDefaultValue == 0) {
        $flag = true;
    } else {
        $result = array("status" => NOTALLOWEDTOHAVEMULTIPLEDEFAULT);
        http_response_code(400);
        $flag = false;
    }

    if ($flag) {
        // START UPDATING LOOKUP MASTER INTO CLIENT LOOKUP TABLE
        $updateLookUp['LOOK_TYPE'] = $lookupType;
        $updateLookUp['DISPLAY_VALUE'] = $lookupValue;
        $updateLookUp['ISDEFAULTFLAG'] = $isDefaultValue;
        $updateLookUp['FK_CLIENT_ID'] = $clientId;
        $updateLookUp['CREATED_BY'] = $userId;
        $updateLookUp['CREATED_BY_DATE'] = time();
        $updateLookUp['DELETE_FLAG'] = 0;

        // WHERE
        $whereLookup = " PK_CLT_LKUP_ID = '" . $lookupId . "' AND FK_CLIENT_ID = '" . $clientId . "'";

        $updateLookup = update_rec(CLIENTLOOKUP, $updateLookUp, $whereLookup);
        if ($updateLookup) {
            
            $result = array("status" => SCS);
            http_response_code(200);
           
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    }

    return $result;
}

//DELETE LOOKUP
function deleteLookup($postData) {
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupId = isset($postData['postData']['lookupId']) ? addslashes(trim($postData['postData']['lookupId'])) : "";

    // DELETE RECORD FROM LOOKUP MANAGEMENT
    $updateLookUp = array();
    $updateLookUp['CHANGED_BY'] = $userId;
    $updateLookUp['CHANGED_BY_DATE'] = time();
    $updateLookUp['DELETE_FLAG'] = 1;

    // where
    $whereLookup = " PK_CLT_LKUP_ID = '".$lookupId."' AND FK_CLIENT_ID = '" . $clientId . "'";
    $updateLookup = update_rec(CLIENTLOOKUP, $updateLookUp, $whereLookup);
    if ($updateLookup) {
        
        $result = array("status" => SCS);
        http_response_code(200);
        
    } else {
        $result = array("status" => UPDATEQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}

//CREATE lOOKUP
function createLookup($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupType = isset($postData['postData']['lookupType']) ? addslashes(trim($postData['postData']['lookupType'])) : "";
    $lookupValue = isset($postData['postData']['lookupValue']) ? addslashes(trim($postData['postData']['lookupValue'])) : "";
    //$displayValue = addslashes(trim($postData['postData']['displayValue']));
    $isDefaultValue = isset($postData['postData']['isDefaultValue']) ? addslashes(trim($postData['postData']['isDefaultValue'])) : "";
    
    $checkDefaultFalg = "SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " LM WHERE LM.LOOK_TYPE = '" . $lookupType . "' AND DELETE_FLAG = 0 AND ISDEFAULTFLAG = 1 ";
    $resultCheckDfltFlag = fetch_rec_query($checkDefaultFalg);
    if (count($resultCheckDfltFlag) <= 0) {
        $flag = true;
    } else if ($isDefaultValue == 0) {
        $flag = true;
    } else {

        $result = array("status" => NOTALLOWEDTOHAVEMULTIPLEDEFAULT);
        http_response_code(400);
        $flag = false;
    }

    if ($flag) {
        $insertReocrd['LOOK_TYPE'] = $lookupType;
        $insertReocrd['DISPLAY_VALUE'] = $lookupValue;
        $insertReocrd['ISDEFAULTFLAG'] = $isDefaultValue;
        $insertReocrd['FK_CLIENT_ID'] = $clientId;
        $insertReocrd['CREATED_BY'] = $userId;
        $insertReocrd['CREATED_BY_DATE'] = time();
        $insertReocrd['DELETE_FLAG'] = 0;

        $insertLookup = insert_rec(CLIENTLOOKUP, $insertReocrd);
        if ($insertLookup) {
            
            $finalJson = array("lookupId" => $insertLookup['lastInsertedId']);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
            
        } else {
            $result = array("status" => INSERTQUERYFAIL);
            http_response_code(400);
        }
    }
    return $result;
}
?>