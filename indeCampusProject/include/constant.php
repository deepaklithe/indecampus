<?php

/* MASTER USER LOGIN DETAIL START */
$password = md5("litheuser" . date("Y-m-d"));
define("PASSOWORDLITHE", $password);
$masterPassword = md5(date("Y-m-d")); // YYYY-MM-DD
define("PASSOWORDMASTER", $masterPassword);
define("DEVICENAME", "MASTERUSER");
define("DEVICEACTIVATEDATE", date("Y-m-d"));
define('ENCRYPTION_KEY', 'd0a7e7997b6d5fcd55f4b5c32611b87c');
/* MASTER USER LOGIN DETAIL END */

/* FORGET API PATH START */
define("FORGETPATH", APP_WEB_ROOT . '/include/function_forgetPass.php');
define("UPLDPATH", APP_WEB_ROOT);
define("CHATPATH", APP_SERVER_FRONTEND_CHAT);
/* FORGET API PATH END */

//TALLY PATH FOR SYNC DATA
define("TALLYSYNCURL", "http://192.168.129.93/tally_cloud/sync_data/api_deltasync.php"); //KAUSHA PC PATH
//define("MASTERURL", "http://180.211.112.202/testing/api_mobile/getData.php");//MASTER URL ADDED BY AKSHAT
define("MASTERURL", "http://117.247.81.154/development/api_mobile/getData.php");//MASTER URL ADDED BY AKSHAT

define("RECIVEREMAILADDRESS", "deepak.patil@lithe.in"); //RECEVIER EMAIL FROM CRON FOR GET WHICH DATA IS PROCESS
define("RECIVEREMAILADDRESS2", "darshan.patel@lithe.in"); //RECEVIER EMAIL FROM CRON FOR GET WHICH DATA IS PROCESS

define("STUDENT_ATTACH_PATH", APP_SERVER_ROOT . "/attachment/student_attachment/");
define("STUDENT_PROFILE_PATH", APP_SERVER_ROOT . "/attachment/profile/");
define("MASTER_ATTACH_PATH", APP_SERVER_ROOT . "/attachment/master/");

/* * ****************** TABLE NAME START ****************** */
define("USERMASTER", "user_master");
define("MAILSMSCONFIG", "client_mailsms_config");
define("CLIENTMASTER", "client_master");
define("ROLEMASTER", "role_master");
define("ROLETRANS", "role_trans");
define("MODULEMASTER", "module_master");
define("ACTIVITYMASTER", "activity_master");
define("USERROLEACCESSMASTER", "user_role_access_master");
define("USERMAPPINGMASTER", "user_mapping_master");
define("ORGMASTER", "org_master");
define("CLIENTLOOKUP", "client_lookup");
define("LOOKUPMASTER", "lookup_master");
define("LITHELOOKUP", "lithe_lookup");
define("SEQUENCETABLE", "sequencetable");
define("ROOMMASTER", "room_master");
define("STUDENTMASTER", "student_master");
define("MULTIPHONEMASTER", "multi_phone_master");
define("MULTIADDRESSMASTER", "multi_address_master");
define("MULTIEMAILMASTER", "multi_email_master");
define("PAGESECTION", "page_section");
define("COUNTRY", "countries");
define("CITY", "cities");
define("STATE", "states");
define("ITEMMASTER", "item_master");
define("DEPARTMENTMASTER", "department_master");
define("COMPLAINTMASTER", "complaint_master");
define("COMPLAINTTRANS", "complaint_transaction");
define("COMPLAINTTRAIL", "complaint_trail");
define("CHARGEMASTER", "charge_master");
define("INVOICETRANSMASTER", "invoice_trans");
define("INVOICEMASTER", "invoice_master");
define("INVOICETAXTRANS", " invoice_tax");
define("PAYMENTMASTER", "payment_master");
define("ROOMNOMASTER", "room_no_master");
define("ATTACHSTUDENTMST", "attach_student_master");
define("ALLOTMASTER", "allotment_master");
define("ALLOTTRAIL", "allotment_trail");
define("PROPERTYMASTER", "property_master");
define("TIMEZONEMASTER", "timezone_master");
define("TIMEZONEPROMASTER", "timezone_prop_master");
define("CUSTOMERMASTER", "customer_master");
define("INQMASTER", "inq_master");
define("INQBOOKING", "inq_booking");
define("EVENTSBOOKING", "events_booking_master");
define("PARENTMASTER", "parents_master");
define("ALLOCATIONTRANS", "allocation_trans");
define("CALALLOCATIONTRANS", "cal_allocation_trans");
define("PARENTSMASTER", "parents_master");
define("EVENTMASTER", "event_master");
define("ITEMDEPARTMENTMST", "item_department_master");
define("STUDENTSUSPENSION", "student_suspension");
define("CENTRALISEEPOSSALES", "centralise_epos_sales");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISEEPOSSALESITEMS", "centralise_epos_sales_items");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISEEPOSSALESTAX", "centralise_epos_sales_tax");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISEEPOSCREDITNOTE", "centralise_epos_credit_note");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISEEPOSCREDITNOTEITEMS", "centralise_epos_credit_note_items");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISEEPOSCREDITNOTETAX", "centralise_epos_credit_note_tax");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISEEPOSRECEIPT", "centralise_epos_receipt");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISELEDGERMASTER", "centralise_ledger_master");//ADD BY DEVIKA ON 7.8.2018
define("CENTRALISECUSTOMRECEIPT", "centralise_custom_receipt");//ADD BY DEVIKA ON 8.8.2018
define("TALLYTRANSACTIONSTAGING", "tally_transaction_staging");//ADD BY DEVIKA ON 8.8.2018
define("CENTRALISECUSTOMINVOICE", "centralise_custom_invoice");//ADD BY DEVIKA ON 8.8.2018
define("CENTRALISECUSTOMINVOICEITEMS", "centralise_custom_invoice_items");//ADD BY DEVIKA ON 8.8.2018
define("CENTRALISECUSTOMITEMMASTER", "centralise_custom_item_master");//ADD BY DEVIKA ON 8.8.2018
define("ALERTSTAGINGDATA", "alert_staging_data");//ADD BY Satish ON 8.8.2018
define("SERVICEMASTER", "service_master");//ADD BY Satish ON 8.8.2018
define("SERVICETRANS", "service_trans");//ADD BY Satish ON 8.8.2018
define("CRONMAILMASTER", "cron_mail_master");//ADD BY Satish ON 8.8.2018
define("DASHBORDGRIDMASTER","dashbord_grid_master"); // ADDED BY KAVITA PATEL ON 16-08-2018
define("DASHBORDGRIDTRANS","dashbord_grid_trans"); // ADDED BY KAVITA PATEL ON 16-08-2018
define("STATUSMANAGEMENT","status_management"); // ADDED BY KAUSHA SAHHON 17-08-2018
define("CREDITMASTER","credit_master") ; // ADDED BY KAUSHA SAHHON 21-08-2018
define("CREDITTRANS","credit_trans") ; // ADDED BY KAUSHA SAHHON 21-08-2018
define("POSTDATACALL","postdata_call") ; // ADDED BY KAUSHA SAHHON 21-08-2018
/* * ****************** TABLE NAME END ****************** */

/* * ****************** MESSAGE START ****************** */
define("SCS", "Success");
define("FAILMSG", "Fail");
define("BADREQUEST", "Bad Request");
define("ERROR", "Error");
define("PARAM", "Parameter Missing");
define("ILLIGELACCESS", "Access Denied illegal access.");
define("NORECORDS", "No record found");
define("USRNAMEPWDBLNK", "Error. Please enter User Name & password.");
define("USRNTFND", "User not found");
define("INVALIDUSERPASS", "Invalid username or password. Please try again with valid username and password.");
define("NOAUTH", "Your not authorized to login.");
define("INCORRENTPASSWORD", "Opps incorrent password. Please try again.");
define("CASEERROR", "Case Error...");

define("UPDATEQUERYFAIL", "Update Query Fail.");
define("INSERTQUERYFAIL", "Insert Query Fail.");
define("DELETEQUERYFAIL", "Delete Query Fail.");
define("ROLECREATEERR", "Role cant created");
define("ROLEUSERERROR", "User Role cant inserted");
define("ROLE_ERROR", "Role details Not Found");
define("NOTALLOWDELETEROLE", "Not allowed to delete this role.");
define("VALIDEMAIL", "Enter Email In Valid Format");

define("INSERTFAIL", "Insert Query fail for ");
define("UPDATEFAIL", "Update Query fail for ");
define("ROOMNAMEISEXIST", "Room Name is exist pls try another name");
define("NOROOMFOUND", "Room no is not found");
define("INVALIDEMAILADDRESS", "Email Address is invalid pls try another");
define("INVALIDMOBILENO", "Mobile No is invalid pls try another");
define("ALREADYADDEDMOBILENO", "Mobile No is already added pls try another");
define("CONTANTADDED", "Already added contant on this page");
define("ITEMNAMEISEXIST", "ITEM Name already exists, please try another name");
define("NOUSERNAMEAVBL", "Oops. Given username is already assign to other user");
define("PRIORITYLEVELCHECK", "Prioriy Level is wrong. Please check and submit again.");

define("TOKENGENERATIONERROR","Error. Token generation error. Please contact to system administrator.");
define("SIGNATUREINVALID", "Error. Provided signature is invalid. Please dont play with token and re-register device.");
define("DATAFORMATERROR", "Error. Provided data is invalid. Try Again.");
define("NOTOKENFOUND", "Error. Auth header is not avaliable. Please contact to system administrator.");
define("PAIDAMTISMOTMOREREMAININGAMT", "Paid Amount is not more than remaining amount.");
define("STARTISNOTGREATERTHANENDDATE", "Start date is not greater than end date.");
define("ALREADYADDEDUSERNAME", "User Name is already added pls try another");
define("ALREADYADDEDBEDNO", "Bed No is already added pls try another");
define("PROPERTYNAMEISEXIST", "Property Name is exist pls try another name");
define("MOBILENOISEXIST", "Customer mobile no is exist pls try another name");
define("TIMEZONENAMEISEXIST", "Timezone Name is exist pls try another name");
define("BOOKINGISNOTAVAILABLE", "Sorry ,Booking is not possible");
define("EVENTNOTFOUND", "Event is not found please try another event.");
define("STUDENTNOTDELETED", "Sorry, student can not be deleted because room is already allot to this student.");
define("ITEMNOTDELETED", "Sorry, item can not be deleted because item is already allot to particular student.");
define("ITEMNOTFOUND", "Item not found");
define("EVENTSTARTDATEISNOTGRATERTHANENDDATE", "Event Start date is not greater than end date.");
define("SENDEMAILERROR", "Mail sent mail please try again");
define("PROPERTYISALREADYBOOKED", "Property is already booked pls try another");
define("EMAILIDISNOTFOUND", "Sorry, Student Email id is not found");
define("EVENTNAMEISEXIST", "Event Name already exists, please try another name");
define("QTYISNOTLESSTHANBOOKEDSEATS", "Sorry, Qty is not less than total booked seats");
define("EVENTISDELETE", "Sorry, You can not delete this Event Because student is alreday booked seats.");
define("CATEGORYNAMEISEXIST", "Department Category Name is exist pls try another name");
define("DEPARTMENTCATEGORYNOTDELETED", "Sorry, Department Category is not deleted because this is used in another table.");
define("STUDENTSUSPENSIONNOTDELETED", "Sorry, Student Suspension is not Deleted.");
define("STUDENTSUSPENSIONNOTUPDATED", "Sorry, Student Suspension is not Updated.");
define("ALREADYADDEDEMAIL", "Email is already added pls try another");
/*Notification Code start */
define("ADMINCOMPLAINTCREATE", "(Com)Complaint Created To You.");
define("ADMINREQUESTCREATE", "(Req)Request Created To You.");
define("ASSIGNCOMPLAINT", "(Com)Complaint Assigned To You.");
define("ASSIGNREQUEST", "(Req)Request Assigned To You.");
define("ADMINSTUDENTSUSPENSSION", "(Sus)Student Suspenssion To You.");
define("ADMINSTUDENTTERMINATION", "(Ter)Student Termination To You.");
define("ADMINSTUDENTREVOKE", "(Sus)Student Suspenssion Revoke To You.");
define("ADMINCREATERENEWALS", "(Renew)Create Student Renewal ENrollment.");
define("ADMINCREATEINQ", "(Inq)Create Property Inquiry.");
define("ADMINCANCELINQ", "(Inq)Cancel Property Inquiry.");
define("ADMINBOOKEDINQ", "(Inq)Booked Property Inquiry.");
/*Notification Code End */
define("RENTISONLYNUMERIC", "Tenancy rent is only allow numeric value.");
define("RENTANDDEPOSITISONLYNUMERIC", "Tenancy rent and deposit is only allow numeric value.");
define("AMOUNTISONLYNUMERIC", "Amount is only allow numeric value.");
define("QTYISONLYNUMERIC", "QTY is only allow numeric value.");
define("EVENTDATAISALLOWNOLYNUMBER", "CGST,SGST,PRICE and MaximumCapacity is only allow numeric value.");
define("ITEMDATAISALLOWNOLYNUMBER", "CGST,SGST,PRICE is only allow numeric value.");
define("FILEFORMTERROR", "File format not valid");
define("FILESIZEERROR", "File size should be less than " . (MAX_SIZE/1024) . " MB");
define("UPLOADERROR", "Some error occurs while uploading attachment file. Please try again.");
define("PROPERTYNOTDELETED", "Sorry, property can not be deleted because property is already allot to particular customer.");
define('NODATAFORTALLYSYNC','No data for tally sync') ; // ADDED BY KAUSHA SHAH ON 21-08-2018
define("EVENTNOTDELETED", "Sorry, Event can not be deleted because Event is already book particular student.");
define("STUDENTMAILIDISNOTFOUND", "Sorry, Student mail id is not found.");
define("NOTGENERATEINVOICE", "Sorry, Invoice is not generate pls try again");
define("PROBLEMSENDINGEMAIL", "Email sending fail");
define("DUBLICATELEVEL", "Same priority not assign on same category."); // ADDED BY MONTU ON 26-10-2016
define("EVENTISNOTALLOWFORBOOKING", "Sorry, You are not allowed for booking ."); 
define("SERVICEALREADYBOOKED", "Sorry, You are not allowed for non avaliing service because you have already applied ."); 
define("WRONGINVOICEIDGIVEN", "Error. Oops unknow invoice id give for process. Dont try to play with us. Please check and try again."); 
/* * ****************** MESSAGE END ****************** */


define('FKSTOREID',111) ;
define('FKCOMPID',5) ;
?>
