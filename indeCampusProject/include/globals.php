<?php

/***************************************************/
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : DEEPAK PATIL                   */
/*   CREATION DATE : 30/05/2016                    */
/*   FILE TYPE : PHP                               */
/*   FILE NAME : GLOBAL FILE                       */
/***************************************************/



set_time_limit(0);
ini_set("display_errors","1");
//date_default_timezone_set("UTC");
date_default_timezone_set('Asia/Kolkata');


//global vars
$G_clientname = 'Lithe Technologies.'; //client name

$G_curpath = '/indecampus/indeCampusProject'; //in case scripts are accessed from a subloder (demo server), turn it blank on production server
$G_studentPortalCurpath = '/indecampus-studentportal'; // ADDED BY KAUSHA SHAH ON 20-08-2018
$G_fontendPath = '/indecampus';
$G_appname = 'INDECAMPUS'; //application name
$G_fullurl = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; //curr.url-no param
$G_curtime = time(); //system time
//$G_prevday = ((strtotime(date('Y-m-d 00:00:00'))) - (60 * 60 * 24)) + (60 * 60 * 23); //prev day 11 pm
//$G_curday = strtotime(date('Y-m-d 23:00:00')); //cur day 11 pm
$G_sendemail = 'deepak.patil@lithe.in'; //system time
$G_sendname = 'Deepak Patil'; //system time
//$G_currency = 'Rs.'; //default currency
$G_dtseprtr = '-'; //date separator
$G_dtformat = 'dd' . $G_dtseprtr . 'mm' . $G_dtseprtr . 'yyyy'; //For JS date picker (only 3 choices viz. dd-mm-yyyy or mm-dd-yyyy or yyyy-mm-dd)
$G_datefrmt = 'd' . $G_dtseprtr . 'm' . $G_dtseprtr . 'Y'; //For php date display ex. system date, current date
$G_dateper = '%d' . $G_dtseprtr . '%m' . $G_dtseprtr . '%Y'; //For jquery date display ex. system date, current date
$G_slash = '/'; //(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')?'\\':'/';
$G_year = date("Y");  // Get current year
$G_month = date("m"); // get current month
$G_date = date("d"); // get current date
$G_currentdate = date('Y-m-d H:i:s');
$G_strtotime = strtotime($G_currentdate);
$G_curhour = date('H', $G_strtotime); // get current hour
$G_curmin = date('i', $G_strtotime); // get current minutes
$G_cursec = date('s', $G_strtotime); // get current sec
//$G_generateid = $G_year . $G_month . $G_date . $G_curhour . $G_curmin . $G_cursec;
$G_generateid = $G_year;
//$uploaddir = "attachment/"; //Image upload directory
define("MAX_SIZE", "2048");
define("SESSIONTIME", "9000"); // PROVIDE DATA INTO SEC FOR JWT
define("APP_SERVER_ROOT", $_SERVER['DOCUMENT_ROOT'] . $G_curpath);
define("APP_SERVER_FRONTEND", strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"].$G_fontendPath);
define("APP_SERVER_FRONTEND_CHAT", strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"].$G_fontendPath);

define("APP_WEB_ROOT", strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"] . $G_curpath);
define("APP_STUD_WEB_ROOT", strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"] . $G_studentPortalCurpath); // ADDED BY KAUSHA SHAH ON 20-08-2018 FOR STUDENT PORTAL
define("ADMIN_WEB_ROOT", strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"] . $G_curpath . '/admin');
define("APP_ADMIN_WEB_ROOT", strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 4)) . '://' . $_SERVER["HTTP_HOST"] . $G_fontendPath);


// function files
//require_once APP_SERVER_ROOT.$G_slash.'include'.$G_slash.'function_BL.php';
require_once APP_SERVER_ROOT . $G_slash . 'include' . $G_slash . 'function_DL.php';
//db class
require_once APP_SERVER_ROOT . $G_slash . 'include' . $G_slash . 'class_db.php';

//JWT PATH FILES START
require_once APP_SERVER_ROOT . $G_slash . 'external-API' . $G_slash . 'php-jwt-master/src/JWT.php';
require_once APP_SERVER_ROOT . $G_slash . 'external-API' . $G_slash . 'php-jwt-master/src/BeforeValidException.php';
require_once APP_SERVER_ROOT . $G_slash . 'external-API' . $G_slash . 'php-jwt-master/src/ExpiredException.php';
require_once APP_SERVER_ROOT . $G_slash . 'external-API' . $G_slash . 'php-jwt-master/src/SignatureInvalidException.php';

$jwtLib = new \Firebase\JWT\JWT();

//JWT PATH FILES END
//$dbName = 'potg_cloud';
define("DBNAME", 'indeCampus');
$db = new DB('192.168.129.207', 'inde', 'inde',''.DBNAME.'');
//error messages
require_once APP_SERVER_ROOT . $G_slash . 'include' . $G_slash . 'constant.php';
require_once APP_SERVER_ROOT . $G_slash . 'include' . $G_slash . 'sendgrid-php.php';
/****************** DOM PDF START **********************************/
//require_once("include/dompdf_bkp/dompdf_config.inc.php"); // PDF 
require_once 'dompdf/autoload.inc.php';
//require_once 'dompdf/src/Dompdf.php';
//use Dompdf\Dompdf;
//$dompdf = new Dompdf();
/****************** DOM PDF END **********************************/


?>
