<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 03-AUG-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

// Add/Edit Complaint 
function addEditComplaint($postData) {

// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $fkStudId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $departmentId = isset($postData['postData']['departmentId']) ? addslashes(trim($postData['postData']['departmentId'])) : "";
    $departmentCatId = isset($postData['postData']['departmentCatId']) ? addslashes(trim($postData['postData']['departmentCatId'])) : "";
    $departmentSubCatId = isset($postData['postData']['departmentSubCatId']) ? addslashes(trim($postData['postData']['departmentSubCatId'])) : "";
    $description = isset($postData['postData']['description']) ? addslashes(trim($postData['postData']['description'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $complaintTransId = isset($postData['postData']['complaintTransId']) ? addslashes(trim($postData['postData']['complaintTransId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($fkStudId) && !empty($description)) { // CHECK REQURIED FIELD CONDITION
        $msg = "";
// COMPLAINT INSERT ARRAY.

        $insertComplaintArray = Array(); //COMPLAINT ARRAY CREATE
        $insertComplaintArray['FK_CLIENT_ID'] = $clientId;
        $insertComplaintArray['FK_ORG_ID'] = $orgId;
        $insertComplaintArray['FK_STUD_ID'] = $fkStudId;
        $insertComplaintArray['TYPE'] = $type; //1=REQUEST,2=COMPLAIN
        $insertComplaintArray['DEPARTMENT_ID'] = $departmentId;
        $insertComplaintArray['DEPARTMENT_CATGORY_ID'] = $departmentCatId;
        $insertComplaintArray['DEPARTMENT_SUBCAT_ID'] = $departmentSubCatId;


        $inertComplaintTransArray = Array();
        $inertComplaintTransArray['DESCRIPTION'] = $description;
        $inertComplaintTransArray['REF_ID'] = $fkStudId;
        $inertComplaintTransArray['TYPE'] = 'STUDENT';


        if (isset($complaintId) && $complaintId > 0) {
            /* SELF PICKUP COMPLAINT AND REQUEST CODE START */
            if (!empty($userId)) {

                $sqlCheckAssignComplaint = "SELECT PK_COMPLAINT_ID FROM " . COMPLAINTMASTER . " WHERE PK_COMPLAINT_ID='" . $complaintId . "' AND ASSIGN_USER =0";
                $resCheckAssignComplaint = fetch_rec_query($sqlCheckAssignComplaint);

                if (count($resCheckAssignComplaint) == 1) { // IF COUNT =0 THEN SELF COMPLAINT ASSIGN.
                    $insertComplaintArray['ASSIGN_USER'] = $userId;
                }
            }
            /* SELF PICKUP COMPLAINT AND REQUEST CODE END */

            $insertComplaintArray['CHANGED_BY'] = $userId;
            $insertComplaintArray['CHANGED_BY_DATE'] = time();
            $whereItemCond = "PK_COMPLAINT_ID ='" . $complaintId . "'";
            $updateComplaint = update_rec(COMPLAINTMASTER, $insertComplaintArray, $whereItemCond); // UPDATE RECORD IN COMPLAINT_MASTER TABLE.
            if (!$updateComplaint) {
                $msg = UPDATEFAIL . " " . COMPLAINTMASTER;
            } else {

                $inertComplaintTransArray['FK_COMPLAINT_ID'] = $complaintId;
                $inertComplaintTransArray['CHANGED_BY'] = $userId;
                $inertComplaintTransArray['CHANGED_BY_DATE'] = time();
                $whereComplaintTransCond = "FK_TRANS_ID ='" . $complaintTransId . "'";
                $insertComplaint = update_rec(COMPLAINTTRANS, $inertComplaintTransArray, $whereComplaintTransCond);  // UPDATE RECORD IN COMPLAINT_TRANS TABLE. 
            }
            // ADDED BY KAVITA PATEL ON 18-08-2018 START FOR GET TICKET ID
            $getTData = fetch_rec_query("SELECT TICKET_ID FROM " . COMPLAINTMASTER . " WHERE PK_COMPLAINT_ID='" . $complaintId . "'");
            $ticketId = $getTData[0]['TICKET_ID'];
            // ADDED BY KAVITA PATEL ON 18-08-2018 END
        } else {
            /* START Code for Set user id when complaint and request create from student portal */
            if (empty($userId)) {
                $sqlUserData = "SELECT UM.PK_USER_ID FROM " . USERMASTER . " UM INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID=UM.USER_LEVEL AND CL.LOOK_TYPE ='USERLEVEL' AND CL.LOOK_VALUE ='Branch Manager' WHERE UM.FK_CLIENT_ID ='" . $clientId . "' AND UM.FK_ORG_ID ='" . $orgId . "' LIMIT 0,1"; // QUERY FOR BRANCH MANAGER
                $resUserData = fetch_rec_query($sqlUserData);
                if (count($resUserData) > 0) {
                    $userId = $resUserData[0]['PK_USER_ID'];
                } else if (count($resUserData) == 0) {
                    $sqlUserData = "SELECT UM.PK_USER_ID FROM " . USERMASTER . " UM INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID=UM.USER_LEVEL AND CL.LOOK_TYPE ='USERLEVEL' AND CL.LOOK_VALUE ='CEO' WHERE UM.FK_CLIENT_ID ='" . $clientId . "' AND UM.FK_ORG_ID ='" . $orgId . "' LIMIT 0,1"; // QUERY FOR CEO
                    $resUserData = fetch_rec_query($sqlUserData);
                    if (count($resUserData) > 0) {
                        $userId = $resUserData[0]['PK_USER_ID'];
                    }
                }
            }

            /* END Code for Set user id when complaint and request create from student portal */

            $ticketId = generateId(COMPLAINTMASTER, 'CRT', $clientId);
            $insertComplaintArray['COMPLAINT_START_DATE_TIME'] = date("Y-m-d H:i:s");
            $insertComplaintArray['TICKET_ID'] = $ticketId;
            $insertComplaintArray['CREATED_BY'] = $userId;
            $insertComplaintArray['CREATED_BY_DATE'] = time();
            //print_r($insertComplaintArray); die;
            $insertComplaint = insert_rec(COMPLAINTMASTER, $insertComplaintArray); // INSERT RECORD IN COMPLAINT_MASTER TABLE.          
            if (!isset($insertComplaint['lastInsertedId']) || $insertComplaint['lastInsertedId'] == 0) {
                $msg = INSERTFAIL . " " . COMPLAINTMASTER;
            } else {
                $complaintId = $insertComplaint['lastInsertedId'];
                $inertComplaintTransArray['FK_COMPLAINT_ID'] = $complaintId;
                $inertComplaintTransArray['CREATED_BY'] = $userId;
                $inertComplaintTransArray['CREATED_BY_DATE'] = time();
                $insertComplaint = insert_rec(COMPLAINTTRANS, $inertComplaintTransArray);  // INSERT RECORD IN COMPLAINT_TRANS TABLE. 
            }
        }

        if ($msg == "") {

            if (!empty($userId) != "") {

                $insertAlertStagingArr = Array(); //ALERT STAGING DATA INSERT
                $insertAlertStagingArr['FK_CLIENT_ID'] = $clientId;
                $insertAlertStagingArr['FK_ORG_ID'] = $orgId;
                $insertAlertStagingArr['APP_NAME'] = 'indecampus';
                $insertAlertStagingArr['DEVICE_TYPE'] = 'UBANTU';
                $insertAlertStagingArr['ALERT_SUB_ID'] = $userId;
                $insertAlertStagingArr['MESSAGE_TYPE'] = 'ALERT';
                $insertAlertStagingArr['ACTION_FLAG'] = '0';
                $insertAlertStagingArr['REF_ID'] = $complaintId;
                //$insertAlertStagingArr['MESSAGE_TEXT'] = ($type == 2) ? ADMINCOMPLAINTCREATE : ADMINREQUESTCREATE; // COMMENT BY KAVITA PATEL ON 17-08-2018 
                // ADDED BY KAVITA PATEL ON 17-08-2018 START
                $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "'");
                $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
                $getStudData = fetch_rec_query("SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUDENT_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $fkStudId . "'"); // GET STUDENT NAME
                $studentName = $getStudData[0]['STUDENT_NAME'];
                if ($type == 2) {
                    $insertAlertStagingArr['MESSAGE_TEXT'] = "(COMP-" . $ticketId . ") Hello Admin, New complaint added by " . $userName . " on behalf of " . $studentName;
                } else {
                    $insertAlertStagingArr['MESSAGE_TEXT'] = "(REQ-" . $ticketId . ") Hello Admin, New request added by " . $userName . " on behalf of " . $studentName;
                }
                // ADDED BY KAVITA PATEL ON 17-08-2018 END
                $insertAlertStagingArr['STATUS'] = '0';
                $insertAlertStagingArr['LOGIN_SESSION_ID'] = $userId;
                $insertAlertStagingArr['CREATED_BY'] = $userId;
                $insertAlertStagingArr['CREATED_BY_DATE'] = time();

                $insertAlertStagingData = insert_rec(ALERTSTAGINGDATA, $insertAlertStagingArr);
            }


            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $complaintData = getComplaintsData($requestData); // GET ALL COMPLAINATS DATA.

            $complaintAllData = ($complaintData['status'] == SCS) ? $complaintData['data'] : $complaintData['status'];
            $result = array("status" => SCS, "data" => $complaintAllData);
            http_response_code(200);
        } else {
            $result = array("status" => $msg);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR DELETE COMPLAINT
function deleteComplaint($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($complaintId)) {
        $updateComplaintArray = Array(); //COMPLAINT ARRAY UPDATE
        $updateComplaintArray['DELETE_FLAG'] = 1;
        $updateComplaintArray['CHANGED_BY'] = $userId;
        $updateComplaintArray['CHANGED_BY_DATE'] = time();

        $whereComplaintUpdate = " FK_COMPLAINT_ID =" . $complaintId . " AND DELETE_FLAG = '0'";
        $updateItem = update_rec(COMPLAINTTRANS, $updateComplaintArray, $whereComplaintUpdate); // UPDATE DELETE_FLAG =1 IN COMPLAINT_MASTER TABLE.
        if ($updateItem) {
            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $complaintData = getComplaintsData($requestData); // GET ALL ITEM DATA.

            $complaintAllData = ($complaintData['status'] == SCS) ? $complaintData['data'] : $complaintData['status'];
            $result = array("status" => SCS, "data" => $complaintAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . COMPLAINTTRANS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR DELETE COMPLAINT
function getComplaintsData($postData) {
//POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : ""; // WHERE 1 = NEW, 2 =MY Complaint And 3 =All

    $whereTypeCond = "";
    if (!empty($type)) {
        if ($type == '1') {
            $whereTypeCond = " AND CM.ASSIGN_USER =''";
        } else if ($type == '2') {
            $whereTypeCond = " AND CM.ASSIGN_USER ='" . $userId . "' OR CM.CREATED_BY ='" . $userId . "'";
        } else if ($type == '3') {
            $whereTypeCond = '';
        }
    }
    $wherComplaintCond = "";
    if (!empty($complaintId)) {
        $wherComplaintCond = " AND CM.PK_COMPLAINT_ID ='" . $complaintId . "'";
    }
    $wherStudentCond = "";
    if (!empty($studentId)) {
        $wherStudentCond = " AND CM.FK_STUD_ID ='" . $studentId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND (CM.CREATED_BY IN (" . $userIds . ") OR (CM.ASSIGN_USER IN (" . $userIds . ")))";
    }
    //add by Saitsh Karena for master user Permission all data END
    
   // $whereUserCond ="";

    $sqlComplaintData = "SELECT CM.PK_COMPLAINT_ID,CM.FK_STUD_ID,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,CM.FK_CLIENT_ID,CM.FK_ORG_ID,CM.COMPLAINT_START_DATE_TIME,CM.TOTAL_TIME_TO_TAKEN,CM.COMPLAINT_END_DATE_TIME,CL.VALUE AS DISPLAY_VALUE,IM.DEPARTMENT_CATEGORY_NAME AS PARENT_CATEGORY_NAME,IM2.DEPARTMENT_CATEGORY_NAME AS CATEGORY_NAME,CM.DEPARTMENT_ID,CM.DEPARTMENT_CATGORY_ID,CM.DEPARTMENT_SUBCAT_ID,CM.TICKET_ID,CM.ASSIGN_USER,UM.FULLNAME AS ASSIGN_USER_NAME,CM.TYPE,CM.STATUS,SM.MEMBERSHIP_NUMBER,CM.COMPLAINT_ASSIGN_START_TIME FROM " . COMPLAINTMASTER . " CM INNER JOIN " . LOOKUPMASTER . " CL ON CL.PK_LOOKUP_ID=CM.DEPARTMENT_ID AND CL.DELETE_FLAG ='0'  INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID=CM.FK_STUD_ID AND SM.DELETE_FLAG ='0' LEFT JOIN " . ITEMDEPARTMENTMST . " IM ON IM.PK_ITEM_DEPARTMENT_ID=CM.DEPARTMENT_CATGORY_ID AND IM.DELETE_FLAG ='0' LEFT JOIN " . ITEMDEPARTMENTMST . " IM2 ON IM2.PK_ITEM_DEPARTMENT_ID=CM.DEPARTMENT_SUBCAT_ID AND IM2.DELETE_FLAG ='0' LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID =CM.ASSIGN_USER  AND UM.DELETE_FLAG ='0' WHERE CM.DELETE_FLAG =0 " . $wherComplaintCond . $wherStudentCond . $whereTypeCond . $whereUserCond . " AND CM.FK_CLIENT_ID ='" . $clientId . "' AND CM.FK_ORG_ID ='" . $orgId . "' ORDER BY CM.PK_COMPLAINT_ID DESC"; // QUERY FOR COMPLAINTS DATA

    $resComplaintData = fetch_rec_query($sqlComplaintData); // RESULT FOR COMPLAINTS DATA
    if (count($resComplaintData) > 0) {
        $complaintArr = Array();
        foreach ($resComplaintData as $keyComplaint => $valueComplaint) { // COMPLAINTS FOR EACH LOOP.
            $complaintArr[$keyComplaint]['complaintId'] = $valueComplaint['PK_COMPLAINT_ID'];
            $complaintArr[$keyComplaint]['studId'] = $valueComplaint['FK_STUD_ID'];
            $complaintArr[$keyComplaint]['studName'] = $valueComplaint['STUD_NAME'];
            $complaintArr[$keyComplaint]['clientId'] = $valueComplaint['FK_CLIENT_ID'];
            $complaintArr[$keyComplaint]['orgId'] = $valueComplaint['FK_ORG_ID'];
            $complaintArr[$keyComplaint]['complaintStartTime'] = $valueComplaint['COMPLAINT_START_DATE_TIME'];
            $complaintArr[$keyComplaint]['totalTimeToTaken'] = ($valueComplaint['TOTAL_TIME_TO_TAKEN'] != "0") ? $valueComplaint['TOTAL_TIME_TO_TAKEN'] : "";
            $complaintArr[$keyComplaint]['complaintEndTime'] = ($valueComplaint['COMPLAINT_END_DATE_TIME'] != "0000-00-00 00:00:00") ? $valueComplaint['COMPLAINT_END_DATE_TIME'] : "";
            $complaintArr[$keyComplaint]['complaintAssignStartTime'] = ($valueComplaint['COMPLAINT_ASSIGN_START_TIME'] != "0000-00-00 00:00:00") ? $valueComplaint['COMPLAINT_ASSIGN_START_TIME'] : "";
            $complaintArr[$keyComplaint]['departmentName'] = $valueComplaint['DISPLAY_VALUE'];
            $complaintArr[$keyComplaint]['categoryName'] = $valueComplaint['CATEGORY_NAME'];
            $complaintArr[$keyComplaint]['parentCategoryName'] = $valueComplaint['PARENT_CATEGORY_NAME'];
            $complaintArr[$keyComplaint]['departmentId'] = $valueComplaint['DEPARTMENT_ID'];
            $complaintArr[$keyComplaint]['departmentCatId'] = $valueComplaint['DEPARTMENT_CATGORY_ID'];
            $complaintArr[$keyComplaint]['departmentSubCatId'] = $valueComplaint['DEPARTMENT_SUBCAT_ID'];
            $complaintArr[$keyComplaint]['ticketId'] = $valueComplaint['TICKET_ID'];
            $complaintArr[$keyComplaint]['assignUserId'] = $valueComplaint['ASSIGN_USER'];
            $complaintArr[$keyComplaint]['assignUserName'] = $valueComplaint['ASSIGN_USER_NAME'];
            $complaintArr[$keyComplaint]['type'] = $valueComplaint['TYPE'];
            $complaintArr[$keyComplaint]['status'] = $valueComplaint['STATUS'];
            $complaintArr[$keyComplaint]['membershipNumber'] = $valueComplaint['MEMBERSHIP_NUMBER'];

            $sqlComplaintTrans = "SELECT CT.FK_TRANS_ID,CT.FK_COMPLAINT_ID,CT.DESCRIPTION,CT.REF_ID,CT.TYPE,IF(CT.TYPE ='STAFF',UM.FULLNAME,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME)) AS USERNAME,CT.CREATED_BY_DATE FROM " . COMPLAINTTRANS . " CT LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID =CT.REF_ID AND CT.TYPE ='STAFF' LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =CT.REF_ID AND CT.TYPE ='STUDENT' WHERE CT.FK_COMPLAINT_ID ='" . $valueComplaint['PK_COMPLAINT_ID'] . "' AND CT.DELETE_FLAG ='0' ORDER BY CT.CREATED_BY_DATE"; // QUERY FOR COMPLAINTS TRANS 
            $resComplaintTrans = fetch_rec_query($sqlComplaintTrans); // RESULT FOR COMPLAINTS TRANS 
            $transArray = array();
            if (count($resComplaintTrans) > 0) {

                foreach ($resComplaintTrans as $keyTrans => $valueTrans) {// COMPLAINT TRANS LOOP.
                    $transArray[$keyTrans]['transId'] = $valueTrans['FK_TRANS_ID'];
                    $transArray[$keyTrans]['complaintId'] = $valueTrans['FK_COMPLAINT_ID'];
                    $transArray[$keyTrans]['description'] = $valueTrans['DESCRIPTION'];
                    $transArray[$keyTrans]['refId'] = $valueTrans['REF_ID'];
                    $transArray[$keyTrans]['type'] = $valueTrans['TYPE'];
                    $transArray[$keyTrans]['username'] = $valueTrans['USERNAME'];
                    $transArray[$keyTrans]['commentDateTime'] = date("d-m-Y h:i:s A", $valueTrans['CREATED_BY_DATE']);
                }
            }
            $complaintArr[$keyComplaint]['transData'] = !empty($transArray) ? array_values($transArray) : NORECORDS; // SET COMPLAINTS TRANS DATA.

            $requestData = $studentAttachmentData = Array();
            $requestData['postData']['complaintId'] = $valueComplaint['PK_COMPLAINT_ID'];
            $requestData['postData']['type'] = 'COMPLAIN';
            $studentAttachmentData = listComplaintAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $complaintArr[$keyComplaint]['attachmentData'] = ($studentAttachmentData['status'] == SCS) ? $studentAttachmentData['data'] : $studentAttachmentData['status'];
        }
        $result = array("status" => SCS, "data" => $complaintArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.3.8.2018.
function userSearch($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $departmentId = isset($postData['postData']['departmentId']) ? addslashes(trim($postData['postData']['departmentId'])) : "";
    $userArr = array();

    if (!empty($clientId) && !empty($orgId)) {
        /* Condition for department id START */
        $whereDepartmentCond = "";
        if (!empty($departmentId)) {
            $whereDepartmentCond = " AND FIND_IN_SET(" . $departmentId . ", FK_DEPARTMENT_ID)";
        }
        /* Condition for department id END */

        $sqlUserData = "  SELECT PK_USER_ID,FULLNAME,CONTACT_NO,FK_DESIGNATION_ID FROM " . USERMASTER . "  WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG ='0' AND IS_ACTIVE ='1'" . $whereDepartmentCond; // QUERY FOR USER DATA.

        $resUserData = fetch_rec_query($sqlUserData); // RESULT FOR USER DATA.
        if (count($resUserData) > 0) {
            foreach ($resUserData as $keyUser => $valueUser) { // USER DATA LOOP
                $userArr[$keyUser]['userId'] = $valueUser['PK_USER_ID'];
                $userArr[$keyUser]['fullName'] = $valueUser['FULLNAME'];
                $userArr[$keyUser]['contactNo'] = $valueUser['CONTACT_NO'];
                $userArr[$keyUser]['designationId'] = $valueUser['FK_DESIGNATION_ID'];
            }
            $result = array("status" => SCS, "data" => $userArr);
            http_response_code(200);
        } else {

            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.3.8.2018.
//ADD BY DEVIKA.3.8.2018.
// assignComplain
function assignComplain($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $complainId = isset($postData['postData']['complainId']) ? addslashes(trim($postData['postData']['complainId'])) : "";
    $assignUserId = isset($postData['postData']['assignUserId']) ? addslashes(trim($postData['postData']['assignUserId'])) : "";
    $assignUserName = isset($postData['postData']['assignUserName']) ? addslashes(trim($postData['postData']['assignUserName'])) : "";
    $loginUserName = isset($postData['postData']['loginUsername']) ? addslashes(trim($postData['postData']['loginUsername'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($complainId)) { // CHECK REQURIED FIELD CONDITION
// COMPLAINT INSERT ARRAY.
        $assignComplainArray = Array(); //COMPLAINT ARRAY CREATE
        $assignComplainArray['ASSIGN_USER'] = $assignUserId;
        $assignComplainArray['COMPLAINT_ASSIGN_START_TIME'] = date("Y-m-d H:i:s");
        $assignComplainArray['CHANGED_BY'] = $userId;
        $assignComplainArray['CHANGED_BY_DATE'] = time();
        $whereComplainCond = "PK_COMPLAINT_ID ='" . $complainId . "'";
        $updateComplain = update_rec(COMPLAINTMASTER, $assignComplainArray, $whereComplainCond); // UPDATE RECORD IN ITEM_MASTER TABLE.
        if (!$updateComplain) {
            $result = array("status" => UPDATEFAIL . " " . COMPLAINTMASTER);
            http_response_code(400);
        } else {
            $inertComplaintTransArray = Array();
            $inertComplaintTransArray['DESCRIPTION'] = $loginUserName . " Assign Complaint to " . $assignUserName;
            $inertComplaintTransArray['REF_ID'] = $userId;
            $inertComplaintTransArray['TYPE'] = 'STAFF';
            $inertComplaintTransArray['FK_COMPLAINT_ID'] = $complainId;
            $inertComplaintTransArray['FK_CLIENT_ID'] = $clientId;
            $inertComplaintTransArray['FK_ORG_ID'] = $orgId;
            $inertComplaintTransArray['CREATED_BY'] = $userId;
            $inertComplaintTransArray['CREATED_BY_DATE'] = time();
            $insertComplain = insert_rec(COMPLAINTTRANS, $inertComplaintTransArray); // INSERT RECORD IN ITEM_MASTER TABLE.    
            if ($insertComplain['lastInsertedId'] > 0) {


                $assignComplainTrailArray = Array();
                $assignComplainTrailArray['FK_CLIENT_ID'] = $clientId;
                $assignComplainTrailArray['FK_ORG_ID'] = $orgId;
                $assignComplainTrailArray['FK_COMPLAINT_ID'] = $complainId;
                $assignComplainTrailArray['TYPE'] = $type;
                $assignComplainTrailArray['REF_ID'] = $assignUserId;
                $assignComplainTrailArray['CREATED_BY'] = $userId;
                $assignComplainTrailArray['CREATED_BY_DATE'] = time();
                //print_r($assignComplainArray); DIE;
                $insertComplain = insert_rec(COMPLAINTTRAIL, $assignComplainTrailArray); // INSERT RECORD IN ITEM_MASTER TABLE.          
                if (!isset($insertComplain['lastInsertedId']) || $insertComplain['lastInsertedId'] == 0) {
                    $result = array("status" => INSERTFAIL . " " . COMPLAINTTRAIL);
                    http_response_code(400);
                } else {
                    $insertAlertStagingArr = Array(); //ALERT STAGING DATA INSERT
                    $insertAlertStagingArr['FK_CLIENT_ID'] = $clientId;
                    $insertAlertStagingArr['FK_ORG_ID'] = $orgId;
                    $insertAlertStagingArr['APP_NAME'] = 'indecampus';
                    $insertAlertStagingArr['DEVICE_TYPE'] = 'UBANTU';
                    $insertAlertStagingArr['ALERT_SUB_ID'] = $assignUserId;
                    $insertAlertStagingArr['MESSAGE_TYPE'] = 'ALERT';
                    $insertAlertStagingArr['ACTION_FLAG'] = '0';
                    $insertAlertStagingArr['REF_ID'] = $complainId;
                    //$insertAlertStagingArr['MESSAGE_TEXT'] = ($type == 2) ? ASSIGNCOMPLAINT : ASSIGNREQUEST; // COMMENT BY KAVITA PATEL ON 17-08-2018
                    // ADDED BY KAVITA PATEL ON 18-08-2018 START FOR GET TICKET ID
                    $getTData = fetch_rec_query("SELECT TICKET_ID FROM " . COMPLAINTMASTER . " WHERE PK_COMPLAINT_ID='" . $complainId . "'");
                    $ticketId = $getTData[0]['TICKET_ID'];
                    // ADDED BY KAVITA PATEL ON 18-08-2018 END
                    // ADDED BY KAVITA PATEL ON 17-08-2018 START
                    $getAssignUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $assignUserId . "'");
                    $assignUserName = $getAssignUserData[0]['FULLNAME']; // GET ASSIGN USER NAME
                    $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "'");
                    $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
                    if ($type == 2) {
                        $insertAlertStagingArr['MESSAGE_TEXT'] = "(COMP-" . $ticketId . ") New complaint assign to you by " . $assignUserName . "(" . $userName . ")";
                    } else {
                        $insertAlertStagingArr['MESSAGE_TEXT'] = "(REQ-" . $ticketId . ") New request assign to you by " . $assignUserName . "(" . $userName . ")";
                    }
                    // ADDED BY KAVITA PATEL ON 17-08-2018 END
                    $insertAlertStagingArr['STATUS'] = '0';
                    $insertAlertStagingArr['LOGIN_SESSION_ID'] = $userId;
                    $insertAlertStagingArr['CREATED_BY'] = $userId;
                    $insertAlertStagingArr['CREATED_BY_DATE'] = time();
                    $insertAlertStagingData = insert_rec(ALERTSTAGINGDATA, $insertAlertStagingArr);
                    if ($insertAlertStagingData['lastInsertedId'] > 0) {

                        $result = array("status" => SCS, "data" => $assignComplainArray);
                        http_response_code(200);
                    } else {
                        $result = array("status" => INSERTFAIL . " " . ALERTSTAGINGDATA);
                        http_response_code(400);
                    }
                }
            } else {
                $result = array("status" => INSERTFAIL . " " . COMPLAINTTRANS);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.3.8.2018

function listComplaintAttachment($postData) {
    // POST DATA
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";

    $whereTypeCond = "";
    if (!empty($type)) {
        $whereTypeCond = " AND ATTACH_CAT='" . $type . "'";
    }

    if (!empty($complaintId)) {
        $sqlStudentAttachment = "SELECT PK_ATTACH_ID,ATTACH_CAT,REF_ID,ATTACHFILE_LINK,TITLE FROM " . ATTACHSTUDENTMST . " WHERE REF_ID='" . $complaintId . "' " . $whereTypeCond . " AND DELETE_FLAG ='0'"; // QUERY FOR LISTING STUDENT ATTACHMENT. 
        $resStudentAttachment = fetch_rec_query($sqlStudentAttachment); // RESULT FOR LISTING STUDENT ATTACHMENT. 
        if (count($resStudentAttachment) > 0) {
            $attachmentArr = Array();
            foreach ($resStudentAttachment as $keyAttachment => $valueAttachment) {
                $attachmentArr[$keyAttachment]['attachmentId'] = $valueAttachment['PK_ATTACH_ID'];
                $attachmentArr[$keyAttachment]['attachType'] = $valueAttachment['ATTACH_CAT'];
                $attachmentArr[$keyAttachment]['refId'] = $valueAttachment['REF_ID'];
                $attachmentArr[$keyAttachment]['attachmentLink'] = $valueAttachment['ATTACHFILE_LINK'];
                $attachmentArr[$keyAttachment]['title'] = $valueAttachment['TITLE'];
            }
            $result = array("status" => SCS, "data" => $attachmentArr);
            http_response_code(400);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function deleteComplaintAttachment($postData) {
    //POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $attachmentId = isset($postData['postData']['attachmentId']) ? addslashes(trim($postData['postData']['attachmentId'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $attachName = isset($postData['postData']['attachName']) ? addslashes(trim($postData['postData']['attachName'])) : "";
    $attachPath = isset($postData['postData']['attachPath']) ? addslashes(trim($postData['postData']['attachPath'])) : "";


    if (!empty($clientId) && !empty($orgId) && !empty($attachmentId)) {
        $updateAttachmentArray = Array(); //ATTACHMENT ARRAY UPDATE
        $updateAttachmentArray['DELETE_FLAG'] = 1;
        $updateAttachmentArray['CHANGED_BY'] = $userId;
        $updateAttachmentArray['CHANGED_BY_DATE'] = time();

        $whereAttachmentUpdate = "DELETE_FLAG ='0' AND PK_ATTACH_ID ='" . $attachmentId . "' AND REF_ID='" . $complaintId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
        $updateAttachment = update_rec(ATTACHSTUDENTMST, $updateAttachmentArray, $whereAttachmentUpdate); // UPDATE DELETE_FLAG =1 IN ATTACH_STUDENT_MASTER TABLE.
        if ($updateAttachment) {

            if (!empty($attachName)) {
                unlink(APP_SERVER_ROOT . "/attachment/student_attachment/" . $attachName); //DELETE ATTACHMENT FILE FROM FOLDER.
            }
            $requestData = $studentAttachmentData = Array();
            $requestData['postData']['complaintId'] = $complaintId;
            $requestData['postData']['type'] = $type;
            $studentAttachmentData = listComplaintAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentAllData = ($studentAttachmentData['status'] == SCS) ? $studentAttachmentData['data'] : $studentAttachmentData['status'];

            $result = array("status" => SCS, "data" => $studentAttachmentAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . ATTACHSTUDENTMST);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function closeComplaintData($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $studentName = isset($postData['postData']['studentName']) ? addslashes(trim($postData['postData']['studentName'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $isCharge = isset($postData['postData']['isCharge']) ? addslashes(trim($postData['postData']['isCharge'])) : 0; // 0 -NON CHGARGE , 1=CHARGE.
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : '';
    $itemArr = !empty($postData['postData']['itemArr']) ? $postData['postData']['itemArr'] : Array();
    $type = !empty($postData['postData']['requestType']) ? $postData['postData']['requestType'] : "1";
    $loginUserName = isset($postData['postData']['loginUsername']) ? addslashes(trim($postData['postData']['loginUsername'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($studentId) && !empty($complaintId) && $isCharge != "") { // CHECK REQURIED FIELD CONDITION
        $whereCond = "PK_COMPLAINT_ID='" . $complaintId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG='0' ";
        $insertItemArray = Array(); //ITEM ARRAY CREATE
        $insertItemArray['STATUS'] = 2;
        $insertItemArray['COMPLAINT_END_DATE_TIME'] = date("Y-m-d H:i:s");
        $insertItemArray['CHANGED_BY'] = $userId;
        $insertItemArray['CHANGED_BY_DATE'] = time();


        $insertItem = update_rec(COMPLAINTMASTER, $insertItemArray, $whereCond); // UPDATE RECORD IN COMPLAINT_MASTER TABLE.    
        //$insertItem = 1;
        if (!($insertItem)) {
            $result = array("status" => UPDATEFAIL . " " . COMPLAINTMASTER);
            http_response_code(400);
        } else {
            $msg = Array();
            if (count($itemArr) > 0) {
                foreach ($itemArr as $keyItem => $valueItem) {

                    $requestData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['itemId'] = $valueItem['itemId'];
                    $requestData['postData']['qty'] = $valueItem['qty'];
                    $requestData['postData']['studentId'] = $studentId;
                    $requestData['postData']['studentName'] = $studentName;
                    $requestData['postData']['itemName'] = $valueItem['itemName'];
                    $requestData['postData']['chargeType'] = 'MISC';
                    $requestData['postData']['type'] = -1; // WHERE -1 is Not send invoice.

                    $chargeMasterData = addChargePostingData($requestData); // ADD DATA IN CHARGE MASTER DATA.  
                    if ($chargeMasterData['status'] != SCS) {
                        $msg [] = $chargeMasterData['status'];
                    }
                }
            }
            if (!empty($msg)) {
                $result = array("status" => "ERROR", "data" => $msg);
                http_response_code(400);
            } else {

                $inertComplaintTransArray = Array();
                $inertComplaintTransArray['DESCRIPTION'] = $loginUserName . " Close Complaint";
                $inertComplaintTransArray['REF_ID'] = $userId;
                $inertComplaintTransArray['FK_CLIENT_ID'] = $clientId;
                $inertComplaintTransArray['FK_ORG_ID'] = $orgId;
                $inertComplaintTransArray['TYPE'] = 'STAFF';
                $inertComplaintTransArray['FK_COMPLAINT_ID'] = $complaintId;
                $inertComplaintTransArray['CREATED_BY'] = $userId;
                $inertComplaintTransArray['CREATED_BY_DATE'] = time();
                $insertComplain = insert_rec(COMPLAINTTRANS, $inertComplaintTransArray); // INSERT RECORD IN ITEM_MASTER TABLE.    

                if ($insertComplain['lastInsertedId'] > 0) {

                    if ($userId != '') {


                        $insertAlertStagingArr = Array(); //ALERT STAGING DATA INSERT
                        $insertAlertStagingArr['FK_CLIENT_ID'] = $clientId;
                        $insertAlertStagingArr['FK_ORG_ID'] = $orgId;
                        $insertAlertStagingArr['APP_NAME'] = 'indecampus';
                        $insertAlertStagingArr['DEVICE_TYPE'] = 'UBANTU';
                        $insertAlertStagingArr['ALERT_SUB_ID'] = $userId;
                        $insertAlertStagingArr['MESSAGE_TYPE'] = 'ALERT';
                        $insertAlertStagingArr['ACTION_FLAG'] = '0';
                        $insertAlertStagingArr['REF_ID'] = $complaintId;
                        //$insertAlertStagingArr['MESSAGE_TEXT'] = ($type == 2) ? ASSIGNCOMPLAINT : ASSIGNREQUEST; // COMMENT BY KAVITA PATEL ON 17-08-2018
                        // ADDED BY KAVITA PATEL ON 18-08-2018 START FOR GET TICKET ID
                        $getTData = fetch_rec_query("SELECT TICKET_ID FROM " . COMPLAINTMASTER . " WHERE PK_COMPLAINT_ID='" . $complaintId . "'");
                        $ticketId = $getTData[0]['TICKET_ID'];
                        // ADDED BY KAVITA PATEL ON 18-08-2018 END
                        // ADDED BY KAVITA PATEL ON 17-08-2018 START
                        $getAssignUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "'");
                        $assignUserName = $getAssignUserData[0]['FULLNAME']; // GET ASSIGN USER NAME
                        $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "'");
                        $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
                        if ($type == 2) {
                            $insertAlertStagingArr['MESSAGE_TEXT'] = "(COMP-" . $ticketId . ") New complaint assign to you by " . $assignUserName . "(" . $userName . ")";
                        } else {
                            $insertAlertStagingArr['MESSAGE_TEXT'] = "(REQ-" . $ticketId . ") New request assign to you by " . $assignUserName . "(" . $userName . ")";
                        }
                        // ADDED BY KAVITA PATEL ON 17-08-2018 END
                        $insertAlertStagingArr['STATUS'] = '0';
                        $insertAlertStagingArr['LOGIN_SESSION_ID'] = $userId;
                        $insertAlertStagingArr['CREATED_BY'] = $userId;
                        $insertAlertStagingArr['CREATED_BY_DATE'] = time();
                        $insertAlertStagingData = insert_rec(ALERTSTAGINGDATA, $insertAlertStagingArr);
                        // if ($insertAlertStagingData['lastInsertedId'] > 0) {
                    }
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $itemData = getComplaintsData($requestData); // GET ALL COMPLAINTS DATA.
                    $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                    $result = array("status" => SCS, "data" => $itemAllData);
                    http_response_code(200);
                    // } else {
                    // }
                } else {
                    $result = array("status" => INSERTFAIL . " " . ALERTSTAGINGDATA);
                    http_response_code(400);
                }
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function reopenComplaint($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : '';

    if (!empty($clientId) && !empty($orgId) && !empty($studentId) && !empty($complaintId)) {

        $whereCond = "PK_COMPLAINT_ID='" . $complaintId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG='0' ";
        $insertItemArray = Array(); //ITEM ARRAY CREATE
        $insertItemArray['STATUS'] = 3;
        $insertItemArray['CHANGED_BY'] = $userId;
        $insertItemArray['CHANGED_BY_DATE'] = time();
        $insertComplainData = update_rec(COMPLAINTMASTER, $insertItemArray, $whereCond); // UPDATE RECORD IN COMPLAINT_MASTER TABLE.   
        if (!$insertComplainData) {
            $result = array("status" => UPDATEFAIL . " " . COMPLAINTMASTER);
            http_response_code(400);
        } else {
            $inertComplaintTransArray = Array();
            $inertComplaintTransArray['DESCRIPTION'] = "Reopen Complaint";
            $inertComplaintTransArray['REF_ID'] = $userId;
            $inertComplaintTransArray['FK_ORG_ID'] = $orgId;
            $inertComplaintTransArray['FK_CLIENT_ID'] = $clientId;
            $inertComplaintTransArray['TYPE'] = 'STAFF';
            $inertComplaintTransArray['FK_COMPLAINT_ID'] = $complaintId;
            $inertComplaintTransArray['CREATED_BY'] = $userId;
            $inertComplaintTransArray['CREATED_BY_DATE'] = time();
            $insertComplain = insert_rec(COMPLAINTTRANS, $inertComplaintTransArray); // INSERT RECORD IN ITEM_MASTER TABLE.    
            if ($insertComplain['lastInsertedId'] > 0) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $itemData = getComplaintsData($requestData); // GET ALL COMPLAINTS DATA.
                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTFAIL . " " . COMPLAINTTRANS);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function cancelComplaint($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : '';

    if (!empty($clientId) && !empty($orgId) && !empty($studentId) && !empty($complaintId)) {

        $whereCond = "PK_COMPLAINT_ID='" . $complaintId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG='0'";
        $insertItemArray = Array(); //ITEM ARRAY CREATE
        $insertItemArray['STATUS'] = 4;
        $insertItemArray['CHANGED_BY'] = $userId;
        $insertItemArray['CHANGED_BY_DATE'] = time();
        $insertComplainData = update_rec(COMPLAINTMASTER, $insertItemArray, $whereCond); // UPDATE RECORD IN COMPLAINT_MASTER TABLE.   
        if (!$insertComplainData) {
            $result = array("status" => UPDATEFAIL . " " . COMPLAINTMASTER);
            http_response_code(400);
        } else {
            $inertComplaintTransArray = Array();
            $inertComplaintTransArray['DESCRIPTION'] = "Cancel Complaint";
            $inertComplaintTransArray['REF_ID'] = $userId;
            $inertComplaintTransArray['FK_CLIENT_ID'] = $clientId;
            $inertComplaintTransArray['FK_ORG_ID'] = $orgId;
            $inertComplaintTransArray['TYPE'] = 'STAFF';
            $inertComplaintTransArray['FK_COMPLAINT_ID'] = $complaintId;
            $inertComplaintTransArray['CREATED_BY'] = $userId;
            $inertComplaintTransArray['CREATED_BY_DATE'] = time();
            $insertComplain = insert_rec(COMPLAINTTRANS, $inertComplaintTransArray); // INSERT RECORD IN ITEM_MASTER TABLE.    
            if ($insertComplain['lastInsertedId'] > 0) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $itemData = getComplaintsData($requestData); // GET ALL COMPLAINTS DATA.
                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTFAIL . " " . COMPLAINTTRANS);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

?>
