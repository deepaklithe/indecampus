<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 12-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR Add/Edit Property Detail. 
function addEditProperty($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $propertyName = isset($postData['postData']['propertyName']) ? addslashes(trim($postData['postData']['propertyName'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $propertyValidityEndDate = !empty($postData['postData']['propertyValidityEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['propertyValidityEndDate'])))) : "";
    $cgst = isset($postData['postData']['cgst']) ? addslashes(trim($postData['postData']['cgst'])) : "";
    $igst = isset($postData['postData']['igst']) ? addslashes(trim($postData['postData']['igst'])) : "";
    $sgst = isset($postData['postData']['sgst']) ? addslashes(trim($postData['postData']['sgst'])) : "";
    $hsnCode = isset($postData['postData']['hsnCode']) ? addslashes(trim($postData['postData']['hsnCode'])) : "";
    $price = isset($postData['postData']['price']) ? addslashes(trim($postData['postData']['price'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $tallyName = isset($postData['postData']['tallyName']) ? addslashes(trim($postData['postData']['tallyName'])) : "";
    $timezoneId = isset($postData['postData']['timezoneId']) ? addslashes(trim($postData['postData']['timezoneId'])) : "";
    $timezoneName = isset($postData['postData']['timezoneName']) ? addslashes(trim($postData['postData']['timezoneName'])) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($propertyName) && !empty($propertyValidityEndDate) && !empty($tallyName)) { // CHECK REQURIED FIELD CONDITION
        if (is_numeric($cgst) && is_numeric($sgst) && is_numeric($price)) {

            $whereCond = "";
            if (!empty($propertyId)) {
                $whereCond = " AND PK_PROPERTY_ID !=" . $propertyId;
            }
            $sqlCheckPropertyExist = "SELECT PK_PROPERTY_ID FROM " . PROPERTYMASTER . " WHERE PROPERTY_NAME='" . $propertyName . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "'" . $whereCond . " AND FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR PROPERTY NAME IS ALREADY AVAILABLE OR NOT.
            $resCheckPropertyExist = fetch_rec_query($sqlCheckPropertyExist); // RESULT FOR PROPERTY NAME IS ALREADY AVAILABLE OR NOT.
            if (count($resCheckPropertyExist) > 0) {
                $result = array("status" => PROPERTYNAMEISEXIST);
                http_response_code(400);
            } else {
                $msg = "";
// PROPERTY INSERT ARRAY.
                $insertPropertyArray = Array(); //ITEM ARRAY CREATE
                $insertPropertyArray['FK_CLIENT_ID'] = $clientId;
                $insertPropertyArray['FK_ORG_ID'] = $orgId;
                $insertPropertyArray['PROPERTY_NAME'] = $propertyName;
                $insertPropertyArray['PROPERTY_VALIDITY_END_DATE'] = $propertyValidityEndDate;
                $insertPropertyArray['CGST'] = $cgst;
                $insertPropertyArray['SGST'] = $sgst;
                $insertPropertyArray['IGST'] = $igst;
                $insertPropertyArray['HSN_CODE'] = $hsnCode;
                $insertPropertyArray['TALLY_NAME'] = $tallyName;
                $insertPropertyArray['PRICE'] = $price;
                $insertPropertyArray['TYPE'] = $type;

                if (!empty($propertyId) && $propertyId > 0) {
                    $insertPropertyArray['CHANGED_BY'] = $userId;
                    $insertPropertyArray['CHANGED_BY_DATE'] = time();
                    $whereItemCond = "PK_PROPERTY_ID ='" . $propertyId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                    $updateProperty = update_rec(PROPERTYMASTER, $insertPropertyArray, $whereItemCond); // UPDATE RECORD IN PROPERTY_MASTER TABLE.
                    if (!$updateProperty) {
                        $msg = UPDATEFAIL . " " . PROPERTYMASTER;
                    }
                } else {
                    $clinetPropertyId = generateId(PROPERTYMASTER, 'PRO', $clientId);

                    $insertPropertyArray['PK_PROPERTY_ID'] = $clinetPropertyId;
                    $insertPropertyArray['CREATED_BY'] = $userId;
                    $insertPropertyArray['CREATED_BY_DATE'] = time();
                    $insertProperty = insert_rec(PROPERTYMASTER, $insertPropertyArray); // INSERT RECORD IN PROPERTY_MASTER TABLE.          
                    if (!$insertProperty) {
                        $msg = INSERTFAIL . " " . PROPERTYMASTER;
                    } else {
                        $propertyId = $insertProperty['lastInsertedId'];
                    }
                }

                if ($msg == "") {

//                    $checkPropertyTimezone = "SELECT PK_PROP_TIMEZONE_ID FROM " . TIMEZONEPROMASTER . " WHERE DELETE_FLAG='0' AND FK_TIMEZONE_ID='" . $timezoneId . "' AND FK_ITEM_ID='" . $propertyId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
//                    $resPropertyTimezone = fetch_rec_query($checkPropertyTimezone);
//                    if (count($resPropertyTimezone) == 1) {
//
//                        $updatePropertyTimezoneArr = Array();
//                        $updatePropertyTimezoneArr['CHANGED_BY'] = $userId;
//                        $updatePropertyTimezoneArr['CHANGED_BY_DATE'] = time();
//
//                        $wherePropertyTimezoneCond = "FK_ITEM_ID ='" . $propertyId . "' AND DELETE_FLAG ='0'";
//                        $updateProperty = update_rec(TIMEZONEPROMASTER, $updatePropertyTimezoneArr, $wherePropertyTimezoneCond); // UPDATE RECORD IN PROPERTY_TIMEZONE_MASTER TABLE.
//                    }else{
//                        // INSERT IN PROPERTY_TIMEZONE_MASTER
//                        $insertPropertyTimezoneArr = Array();
//                        $insertPropertyTimezoneArr['FK_TIMEZONE_ID'] = $timezoneId;
//                        $insertPropertyTimezoneArr['FK_CLIENT_ID'] = $clientId;
//                        $insertPropertyTimezoneArr['FK_ORG_ID'] = $orgId;
//                        $insertPropertyTimezoneArr['FK_ITEM_ID'] = $propertyId;
//                        $insertPropertyTimezoneArr['PROP_TIMEZONENAME'] = $timezoneName;
//                        $insertPropertyTimezoneArr['PROP_NAME'] = $propertyName;
//                        $insertPropertyTimezoneArr['CREATED_BY'] = $userId;
//                        $insertPropertyTimezoneArr['CREATED_BY_DATE'] = time();
//                        $insertPropertyTimezone = insert_rec(TIMEZONEPROMASTER, $insertPropertyTimezoneArr); // INSERT RECORD IN PROPERTY_TIMEZONE_MASTER TABLE.  
//                    }
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;

                    $propertyData = getPropertyData($requestData); // GET ALL PROPERTY DATA.

                    $propertyAllData = ($propertyData['status'] == SCS) ? $propertyData['data'] : $propertyData['status'];

                    $result = array("status" => SCS, "data" => $propertyAllData);
                    http_response_code(200);
                } else {
                    $result = array("status" => $msg);
                    http_response_code(400);
                }
            }
        } else {
            $result = array("status" => ITEMDATAISALLOWNOLYNUMBER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR PROPERTY ITEM
function deleteProperty($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($propertyId)) {

        $checkItemAllotmentAvailabel = "SELECT PK_INQ_ID FROM " . INQMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_PROPERTY_ID ='" . $propertyId . "' AND DELETE_FLAG ='0' AND STATUS != '3'";

        $resStudentItemAvailabel = fetch_rec_query($checkItemAllotmentAvailabel);
        if (count($resStudentItemAvailabel) == 0) {


            $updateItemArray = Array(); //PROPERTY ARRAY UPDATE
            $updateItemArray['DELETE_FLAG'] = 1;
            $updateItemArray['CHANGED_BY'] = $userId;
            $updateItemArray['CHANGED_BY_DATE'] = time();

            $whereItemUpdate = " PK_PROPERTY_ID =" . $propertyId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG = '0'";
            $updateItem = update_rec(PROPERTYMASTER, $updateItemArray, $whereItemUpdate); // UPDATE DELETE_FLAG =1 IN PROPERTY_MASTER TABLE.
            if ($updateItem) {
                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $propertyData = getPropertyData($requestData); // GET ALL PROPERTY DATA.

                $propertyAllData = ($propertyData['status'] == SCS) ? $propertyData['data'] : $propertyData['status'];
                $result = array("status" => SCS, "data" => $propertyAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . PROPERTYMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PROPERTYNOTDELETED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

// GET PROPERTY ALL DATA.
function getPropertyData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $proprtyId = isset($postData['postData']['proprtyId']) ? addslashes(trim($postData['postData']['proprtyId'])) : "";

    $propertyArr = array();
    $wherePropertyCond = "";
    if (!empty($proprtyId)) {
        $wherePropertyCond = " AND PM.PK_PROPERTY_ID='" . $proprtyId . "'";
    }


    $sqlPropertyData = "  SELECT PM.PK_PROPERTY_ID,PM.PROPERTY_NAME,PM.FK_CLIENT_ID,PM.FK_ORG_ID,PM.PROPERTY_VALIDITY_END_DATE,PM.CGST,PM.SGST,PM.IGST,PM.HSN_CODE,PM.TALLY_NAME,PM.TYPE,PM.PRICE FROM " . PROPERTYMASTER . " AS PM   WHERE PM.DELETE_FLAG =0 " . $wherePropertyCond . " AND PM.FK_CLIENT_ID ='" . $clientId . "'  AND PM.FK_ORG_ID ='" . $orgId . "' ORDER BY PM.PK_PROPERTY_ID DESC"; // QUERY FOR PROPERTY DATA.
    $resPropertyData = fetch_rec_query($sqlPropertyData); // RESULT FOR PROPERTY DATA.
    if (count($resPropertyData) > 0) {
        foreach ($resPropertyData as $keyProperty => $valueProperty) { // PROPERTY DATA LOOP
            $propertyArr[$keyProperty]['propertyId'] = $valueProperty['PK_PROPERTY_ID'];
            $propertyArr[$keyProperty]['propertyName'] = $valueProperty['PROPERTY_NAME'];
            $propertyArr[$keyProperty]['clientId'] = $valueProperty['FK_CLIENT_ID'];
            $propertyArr[$keyProperty]['orgId'] = $valueProperty['FK_ORG_ID'];
            $propertyArr[$keyProperty]['propertyValidityEndDate'] = $valueProperty['PROPERTY_VALIDITY_END_DATE'];
            $propertyArr[$keyProperty]['cgst'] = $valueProperty['CGST'];
            $propertyArr[$keyProperty]['sgst'] = $valueProperty['SGST'];
            $propertyArr[$keyProperty]['igst'] = $valueProperty['IGST'];
            $propertyArr[$keyProperty]['price'] = $valueProperty['PRICE'];
            $propertyArr[$keyProperty]['typeName'] = ($valueProperty['TYPE'] == "DAYWISE") ? "Date Wise" : "Month Wise";
            $propertyArr[$keyProperty]['type'] = $valueProperty['TYPE'];
            $propertyArr[$keyProperty]['tallyName'] = $valueProperty['TALLY_NAME'];
            $propertyArr[$keyProperty]['hsnCode'] = $valueProperty['HSN_CODE'];
        }
        $result = array("status" => SCS, "data" => $propertyArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR Add/Edit Time Zone Detail. 
function addEditTimezone($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $timezoneName = isset($postData['postData']['timezoneName']) ? addslashes(trim($postData['postData']['timezoneName'])) : "";
    $timezoneId = isset($postData['postData']['timezoneId']) ? addslashes(trim($postData['postData']['timezoneId'])) : "";

    if (!empty($clientId) && !empty($timezoneName)) { // CHECK REQURIED FIELD CONDITION
        $whereCond = "";
        if (!empty($timezoneId)) {
            $whereCond = " AND PK_TIMEZONE_ID !=" . $timezoneId;
        }
        $sqlCheckTimezoneExist = "SELECT PK_TIMEZONE_ID FROM " . TIMEZONEMASTER . " WHERE TIMEZONENAME='" . $timezoneName . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'" . $whereCond; // QUERY FOR TIMEZONE NAME IS ALREADY AVAILABLE OR NOT.
        $resCheckTimezoneExist = fetch_rec_query($sqlCheckTimezoneExist); // RESULT FOR TIMEZONE NAME IS ALREADY AVAILABLE OR NOT.
        if (count($resCheckTimezoneExist) > 0) {
            $result = array("status" => TIMEZONENAMEISEXIST);
            http_response_code(400);
        } else {
            $msg = "";
// ITEM INSERT ARRAY.
            $insertTimezoneArray = Array(); //ITEM ARRAY CREATE
            $insertTimezoneArray['FK_CLIENT_ID'] = $clientId;
            $insertTimezoneArray['FK_ORG_ID'] = $orgId;
            $insertTimezoneArray['TIMEZONENAME'] = $timezoneName;
            if (!empty($timezoneId) && $timezoneId > 0) {
                $insertTimezoneArray['CHANGED_BY'] = $userId;
                $insertTimezoneArray['CHANGED_BY_DATE'] = time();
                $whereItemCond = "PK_TIMEZONE_ID ='" . $timezoneId . "'";
                $updateProperty = update_rec(TIMEZONEMASTER, $insertTimezoneArray, $whereItemCond); // UPDATE RECORD IN TIMEZONE_MASTER TABLE.
                if (!$updateProperty) {
                    $msg = UPDATEFAIL . " " . TIMEZONEMASTER;
                }
            } else {
                $insertTimezoneArray['CREATED_BY'] = $userId;
                $insertTimezoneArray['CREATED_BY_DATE'] = time();
                $insertProperty = insert_rec(TIMEZONEMASTER, $insertTimezoneArray); // INSERT RECORD IN TIMEZONE_MASTER TABLE.          
                if (!isset($insertProperty['lastInsertedId']) || $insertProperty['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . TIMEZONEMASTER;
                }
            }

            if ($msg == "") {
                $requestData = $timezoneData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;

                $timezoneData = getTimeZoneData($requestData); // GET ALL TIME ZONE DATA.

                $timezoneAllData = ($timezoneData['status'] == SCS) ? $timezoneData['data'] : $timezoneData['status'];

                $result = array("status" => SCS, "data" => $timezoneAllData);
                http_response_code(200);
            } else {
                $result = array("status" => $msg);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR TIMEZONE DELETE
function deleteTimezone($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $timezoneId = isset($postData['postData']['timezoneId']) ? addslashes(trim($postData['postData']['timezoneId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($timezoneId)) {
        $updateTimezoneArray = Array(); //TIMEZONE ARRAY UPDATE
        $updateTimezoneArray['DELETE_FLAG'] = 1;
        $updateTimezoneArray['CHANGED_BY'] = $userId;
        $updateTimezoneArray['CHANGED_BY_DATE'] = time();

        $whereTimezoneUpdate = " PK_TIMEZONE_ID =" . $timezoneId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG = '0'";
        $updateItem = update_rec(TIMEZONEMASTER, $updateTimezoneArray, $whereTimezoneUpdate); // UPDATE DELETE_FLAG =1 IN TIMEZONE_MASTER TABLE.
        if ($updateItem) {
            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $timezoneData = getTimezoneData($requestData); // GET ALL TIME ZONE DATA.

            $timezoneAllData = ($timezoneData['status'] == SCS) ? $timezoneData['data'] : $timezoneData['status'];
            $result = array("status" => SCS, "data" => $timezoneAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . TIMEZONEMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

// GET FOR TIMEZONE ALL DATA.
function getTimezoneData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $timezoneId = isset($postData['postData']['timezoneId']) ? addslashes(trim($postData['postData']['timezoneId'])) : "";

    $timezoneArr = array();
    $whereTimezoneCond = "";
    if (!empty($timezoneId)) {
        $whereTimezoneCond = " AND PK_TIMEZONE_ID='" . $timezoneId . "'";
    }


    $sqlTimezoneData = "  SELECT PK_TIMEZONE_ID,TIMEZONENAME,FK_CLIENT_ID FROM " . TIMEZONEMASTER . "  WHERE DELETE_FLAG =0 " . $whereTimezoneCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' ORDER BY PK_TIMEZONE_ID DESC"; // QUERY FOR TIMEZONE DATA.
    $resTimezoneData = fetch_rec_query($sqlTimezoneData); // RESULT FOR TIMEZONE DATA.
    if (count($resTimezoneData) > 0) {
        foreach ($resTimezoneData as $keyTimezone => $valueTimezone) { // TIMEZONE DATA LOOP
            $timezoneArr[$keyTimezone]['timezoneId'] = $valueTimezone['PK_TIMEZONE_ID'];
            $timezoneArr[$keyTimezone]['timezoneName'] = $valueTimezone['TIMEZONENAME'];
            $timezoneArr[$keyTimezone]['clientId'] = $valueTimezone['FK_CLIENT_ID'];
        }
        $result = array("status" => SCS, "data" => $timezoneArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.16.7.2018
function getInquiryDataByProperty($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $inqId = isset($postData['postData']['inqId']) ? addslashes(trim($postData['postData']['inqId'])) : "";
    $bookingStartDate = isset($postData['postData']['startDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['startDate'])))) : "";
    $bookingEndDate = isset($postData['postData']['endDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['endDate'])))) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";
    $inqListArr = $tempArr = array();

    $wherePropertyCond = "";
    if (!empty($propertyId)) {
        $wherePropertyCond = " AND IM.FK_PROPERTY_ID='" . $propertyId . "'";
    }

    $whereInqCond = "";
    if (!empty($inqId)) {
        $whereInqCond = " AND PK_INQ_ID='" . $inqId . "'";
    }
    //  $inqList = "SELECT IM.PK_INQ_ID,IM.BOOKING_START_DATE,IM.BOOKING_END_DATE,IM.STATUS,IM.FK_PROPERTY_ID,IM.PROPERTY_NAME,IM.FK_CUST_ID,CM.FIRST_NAME FROM " . INQMASTER . " IM INNER JOIN ".CUSTOMERMASTER." CM ON CM.PK_CUST_ID = IM.FK_CUST_ID WHERE IM.FK_PROPERTY_ID = '" . $propertyId . "' AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' AND IM.DELETE_FLAG = 0 AND (BOOKING_END_DATE BETWEEN '" . $bookingStartDate . "' AND '" . $bookingEndDate . "' OR BOOKING_START_DATE BETWEEN '" . $bookingStartDate . "' AND '" . $bookingEndDate . "')" . $whereInqCond;
    $inqList = "SELECT IM.PK_INQ_ID,IM.BOOKING_START_DATE,IM.BOOKING_END_DATE,IM.STATUS,IM.FK_PROPERTY_ID,IM.PROPERTY_NAME,IM.FK_CUST_ID,CM.FIRST_NAME FROM " . INQMASTER . " IM INNER JOIN " . CUSTOMERMASTER . " CM ON CM.PK_CUST_ID = IM.FK_CUST_ID WHERE IM.FK_PROPERTY_ID = '" . $propertyId . "' AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' AND IM.DELETE_FLAG = '0'" . $whereInqCond.$wherePropertyCond." ORDER BY IM.PK_INQ_ID DESC";
    $resultInqList = fetch_rec_query($inqList);
    //print_r($resultInqList); die;
    if (count($resultInqList) > 0) {
        foreach ($resultInqList as $keyInqlist => $valueInqlist) { // INQUIRY DATA LOOP
            $tempArr[$keyInqlist]['inqId'] = $valueInqlist['PK_INQ_ID'];
            $tempArr[$keyInqlist]['propertyId'] = $valueInqlist['FK_PROPERTY_ID'];
            $tempArr[$keyInqlist]['propertyName'] = $valueInqlist['PROPERTY_NAME'];
            $tempArr[$keyInqlist]['custId'] = $valueInqlist['FK_CUST_ID'];
            $tempArr[$keyInqlist]['custName'] = $valueInqlist['FIRST_NAME'];
            $tempArr[$keyInqlist]['bookingStartDate'] = $valueInqlist['BOOKING_START_DATE'];
            $tempArr[$keyInqlist]['bookingEndDate'] = $valueInqlist['BOOKING_END_DATE'];
            $tempArr[$keyInqlist]['status'] = $valueInqlist['STATUS'];
        }
        $inqListArr['bookingData'] = $tempArr;

        $result = array("status" => SCS, "data" => $inqListArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.16.7.2018

function getPropertyTimeZone($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";

    $propArr = array();
    $wherePropCond = "";
    if (!empty($itemId)) {
        $wherePropCond = " AND FK_ITEM_ID='" . $itemId . "'";
    }


    $sqlPropTimezoneData = "  SELECT PK_PROP_TIMEZONE_ID,FK_TIMEZONE_ID,FK_ITEM_ID,PROP_TIMEZONENAME,PROP_NAME FROM " . TIMEZONEPROMASTER . "  WHERE DELETE_FLAG =0 " . $wherePropCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR TIMEZONE DATA.
    $resPropTimezoneData = fetch_rec_query($sqlPropTimezoneData); // RESULT FOR TIMEZONE DATA.
    if (count($resPropTimezoneData) > 0) {
        foreach ($resPropTimezoneData as $keyProp => $valueProp) { // TIMEZONE DATA LOOP
            $propArr[$keyProp]['propTimezoneId'] = $valueProp['PK_PROP_TIMEZONE_ID'];
            $propArr[$keyProp]['timezoneId'] = $valueProp['FK_TIMEZONE_ID'];
            $propArr[$keyProp]['itemId'] = $valueProp['FK_ITEM_ID'];
            $propArr[$keyProp]['propTimezoneName'] = $valueProp['PROP_TIMEZONENAME'];
            $propArr[$keyProp]['propertyName'] = $valueProp['PROP_NAME'];
        }
        $result = array("status" => SCS, "data" => array_values($propArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function addTimeZoneToProperty($postData) {
    //print_r($postData); exit;
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $timezoneId = isset($postData['postData']['timezoneId']) ? addslashes(trim($postData['postData']['timezoneId'])) : "";
    $timzoneName = isset($postData['postData']['timzoneName']) ? addslashes(trim($postData['postData']['timzoneName'])) : "";
    $propertyName = isset($postData['postData']['propertyName']) ? addslashes(trim($postData['postData']['propertyName'])) : "";


    if (!empty($timezoneId) && !empty($itemId)) {

        $checkPropertyTimezone = "SELECT PK_PROP_TIMEZONE_ID FROM " . TIMEZONEPROMASTER . " WHERE DELETE_FLAG='0' AND FK_TIMEZONE_ID='" . $timezoneId . "' AND FK_ITEM_ID='" . $itemId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' LIMIT 0,1";
        $resPropertyTimezone = fetch_rec_query($checkPropertyTimezone);
        if (count($resPropertyTimezone) > 0) {
            $updatePropertyTimezoneArr = Array();

            $updatePropertyTimezoneArr['CHANGED_BY'] = $userId;
            $updatePropertyTimezoneArr['CHANGED_BY_DATE'] = time();
            $updatePropertyTimezoneArr['FK_TIMEZONE_ID'] = $timezoneId;
            $updatePropertyTimezoneArr['PROP_TIMEZONENAME'] = $timzoneName;

            $wherePropertyTimezoneCond = "FK_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' AND FK_TIMEZONE_ID='" . $timezoneId . "' ";
            $updateProperty = update_rec(TIMEZONEPROMASTER, $updatePropertyTimezoneArr, $wherePropertyTimezoneCond); // UPDATE RECORD IN PROPERTY_TIMEZONE_MASTER TABLE.
            if ($updateProperty) {
                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['requestCase'] = "getPropertyTimeZone";
                $requestData['postData']['itemId'] = $itemId;

                $timezoneListing = getPropertyTimeZone($requestData);
                if ($timezoneListing['status'] == SCS) {
                    $data = $timezoneListing['data'];
                } else {
                    $data = $timezoneListing['status'];
                }
                $result = array("status" => SCS, "data" => $data);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " timezone property master table.");
                http_response_code(400);
            }
        } else {
            // INSERT IN PROPERTY_TIMEZONE_MASTER
            $insertPropertyTimezoneArr = Array();
            $insertPropertyTimezoneArr['FK_TIMEZONE_ID'] = $timezoneId;
            $insertPropertyTimezoneArr['FK_CLIENT_ID'] = $clientId;
            $insertPropertyTimezoneArr['FK_ORG_ID'] = $orgId;
            $insertPropertyTimezoneArr['FK_ITEM_ID'] = $itemId;
            $insertPropertyTimezoneArr['PROP_TIMEZONENAME'] = $timzoneName;
            $insertPropertyTimezoneArr['PROP_NAME'] = $propertyName;
            $insertPropertyTimezoneArr['CREATED_BY'] = $userId;
            $insertPropertyTimezoneArr['CREATED_BY_DATE'] = time();
            $insertPropertyTimezone = insert_rec(TIMEZONEPROMASTER, $insertPropertyTimezoneArr); // INSERT RECORD IN PROPERTY_TIMEZONE_MASTER TABLE.  

            if ($insertPropertyTimezone['lastInsertedId'] > 0) {
                $requestData = array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['userId'] = $userId;
                $requestData['postData']['requestCase'] = "getPropertyTimeZone";
                $requestData['postData']['itemId'] = $itemId;

                $timezoneListing = getPropertyTimeZone($requestData);
                if ($timezoneListing['status'] == SCS) {
                    $data = $timezoneListing['data'];
                } else {
                    $data = $timezoneListing['status'];
                }
                $result = array("status" => SCS, "data" => $data);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " timezone property master table.");
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

?>
