<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 02-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR Add Room
function addEditRoom($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $roomsArr = isset($postData['postData']['roomsArr']) ? $postData['postData']['roomsArr'] : "";


    if (!empty($clientId) && !empty($orgId) && !empty($itemId) && !empty($roomsArr) && count($roomsArr) > 0) { // CHECK REQURIED FIELD CONDITION
        foreach ($roomsArr as $keyRoom => $valueRoom) {
            $roomId = isset($valueRoom['roomId']) ? $valueRoom['roomId'] : "";
            $roomCapacity = isset($valueRoom['roomCapacity']) ? $valueRoom['roomCapacity'] : "";
            $whereCond = "";
            if (!empty($roomId)) {
                $whereCond = " AND PK_ROOM_ID !=" . $roomId;
            }
            $sqlCheckRoomExist = "SELECT PK_ROOM_ID FROM " . ROOMNOMASTER . " WHERE ROOM_NAME ='" . $valueRoom['roomName'] . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "'" . $whereCond . " AND FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR ROOM NAME IS ALREADY AVAILABLE OR NOT.
            $resCheckRoomExist = fetch_rec_query($sqlCheckRoomExist); // RESULT FOR ROOM NAME IS ALREADY AVAILABLE OR NOT.
            if (count($resCheckRoomExist) > 0) {
                $result = array("status" => ROOMNAMEISEXIST);
                http_response_code(400);
                return $result;
            } else {
                $msg = "";
                // ROOM INSERT ARRAY.
                $insertRoomArray = Array(); //ROOM ARRAY CREATE
                $insertRoomArray['FK_CLIENT_ID'] = $clientId;
                $insertRoomArray['FK_ORG_ID'] = $orgId;
                $insertRoomArray['FK_ITEM_ID'] = $itemId;
                $insertRoomArray['ROOM_CAPACITY'] = $roomCapacity;

                $insertRoomArray['ROOM_NAME'] = $valueRoom['roomName'];

                if (isset($roomId) && $roomId > 0) {
                    $insertRoomArray['CHANGED_BY'] = $userId;
                    $insertRoomArray['CHANGED_BY_DATE'] = time();
                    $whereRoomCond = "PK_ROOM_ID ='" . $roomId . "' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
                    $updateRoom = update_rec(ROOMNOMASTER, $insertRoomArray, $whereRoomCond); // UPDATE RECORD IN ROOM_MASTER TABLE.
                    if (!$updateRoom) {
                        $msg = UPDATEFAIL . " " . ROOMNOMASTER;
                    }
                } else {
                    $insertRoomArray['CREATED_BY'] = $userId;
                    $insertRoomArray['CREATED_BY_DATE'] = time();
                    $insertRoom = insert_rec(ROOMNOMASTER, $insertRoomArray); // INSERT RECORD IN ROOM_MASTER TABLE.          
                    if (!isset($insertRoom['lastInsertedId']) || $insertRoom['lastInsertedId'] == 0) {
                        $msg = INSERTFAIL . " " . ROOMNOMASTER;
                    }
                }

                if ($msg == "") {
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['itemId'] = $itemId;
                    $roomData = roomList($requestData); // GET ALL ROOM DATA.
                    $roomAllData = ($roomData['status'] == SCS) ? $roomData['data'] : $roomData['status'];

                    $result = array("status" => SCS, "data" => $roomAllData);
                    http_response_code(200);
                } else {
                    $result = array("status" => $msg);
                    http_response_code(400);
                }
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR DELETE ROOM
function deleteRoom($postData) {
    //POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $roomId = isset($postData['postData']['roomId']) ? addslashes(trim($postData['postData']['roomId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($roomId) && !empty($itemId)) {
        $updateRoomArray = Array(); //ROOM ARRAY UPDATE
        $updateRoomArray['DELETE_FLAG'] = 1;
        $updateRoomArray['CHANGED_BY'] = $userId;
        $updateRoomArray['CHANGED_BY_DATE'] = time();

        $whereRoomUpdate = " PK_ROOM_ID =" . $roomId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG ='0' AND FK_ITEM_ID ='".$itemId."'";
        $updateRoom = update_rec(ROOMNOMASTER, $updateRoomArray, $whereRoomUpdate); // UPDATE DELETE_FLAG =1 IN ROOM_MASTER TABLE.
        if ($updateRoom) {
            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['itemId'] = $itemId;
            $roomData = roomList($requestData); // GET ALL ROOM DATA.
            $roomAllData = ($roomData['status'] == SCS) ? $roomData['data'] : $roomData['status'];

            $result = array("status" => SCS, "data" => $roomAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . ROOMNOMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function getRoomsData($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";

    $sqlRoomData = "SELECT PK_ROOM_ID,FK_CLIENT_ID,FK_ORG_ID,ROOM_NAME,PRICE,ROOM_DESCRIPTION,ROOM_CAPACITY FROM " . ROOMMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "'";

    $resRoomData = fetch_rec_query($sqlRoomData);
    if (count($resRoomData) > 0) {
        $roomArr = Array();
        foreach ($resRoomData as $keyRoom => $valueRoom) {
            $roomArr[$keyRoom]['roomId'] = $valueRoom['PK_ROOM_ID'];
            $roomArr[$keyRoom]['clientId'] = $valueRoom['FK_CLIENT_ID'];
            $roomArr[$keyRoom]['orgId'] = $valueRoom['FK_ORG_ID'];
            $roomArr[$keyRoom]['roomName'] = $valueRoom['ROOM_NAME'];
            $roomArr[$keyRoom]['price'] = $valueRoom['PRICE'];
            $roomArr[$keyRoom]['roomDescription'] = $valueRoom['ROOM_DESCRIPTION'];
            $roomArr[$keyRoom]['roomCapacity'] = $valueRoom['ROOM_CAPACITY'];
        }
        $result = array("status" => SCS, "data" => array_values($roomArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

?>
