<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : DARSHAN PATEL                     */
/*   CREATION DATE : 02-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR USER LOGIN
function checkLogin_BL($postData) { // SAMPLE FUNCTION FILE
//    print_r($postData);exit;
    $userName = isset($postData['postData']['userName']) ? addslashes(trim($postData['postData']['userName'])) : "";
    $password = isset($postData['postData']['password']) ? addslashes(trim($postData['postData']['password'])) : "";
    $userType = isset($postData['postData']['userType']) ? addslashes(trim($postData['postData']['userType'])) : "";

    $todayDate = date("Y-m-d");

    $deviceToken = explode("|--|", base64_decode(trim(isset($postData['postData']['deviceId']))));

    $deviceId = $deviceToken[0];
    $deviceName = !empty($deviceToken[1]) ? $deviceToken[1] : 0;
    $tempDeviceDate = !empty($deviceToken[2]) ? $deviceToken[2] : 0;
    $deviceRegDate = date('Y-m-d', strtotime($tempDeviceDate));


    if ($userName != '' && $password != "") {
        if ($userType == 1) {
            $userTypeIn = "1";
        } else if ($userType == "-1") {
            $userTypeIn = "";
        }

        $fetch_details = array();
        if ($userType == "-1") {
            if ($password == PASSOWORDLITHE) {

                $clientId = substr($userName, -10, (strlen($userName) - 9));
                $userNewName = substr($userName, strlen($clientId)); // LITHEUSER 
                $fetch_details[0]['PK_USER_ID'] = -1;
                $fetch_details[0]['FK_CLIENT_ID'] = !empty($clientId) ? $clientId : "-1";
                $fetch_details[0]['USERNAME'] = "MasterUser";
                $fetch_details[0]['FULLNAME'] = "Lithe-User";
                $fetch_details[0]['USER_TYPE'] = "-1";
                $fetch_details[0]['deviceId'] = md5($clientId . '|' . "-1");
                $fetch_details[0]['deviceactivateDate'] = DEVICEACTIVATEDATE;
                $fetch_details[0]['deviceName'] = DEVICENAME;
                $fetch_details[0]['PASSWORD'] = password_hash(PASSOWORDLITHE, PASSWORD_DEFAULT);
                $fetch_details[0]['IS_ACTIVE'] = '1';

                $validEndDate = '1';
            } else {
                $result = array("status" => INCORRENTPASSWORD);
            }
        } else {
            if ($password == PASSOWORDMASTER) {

                $whereusers = "USERNAME = '" . $userName . "' AND USER_TYPE IN(" . $userTypeIn . ") AND DELETE_FLAG = 0 AND IS_ACTIVE = '1' ";

                $fetch_details = fetch_rec_query("SELECT PK_USER_ID,FK_CLIENT_ID,USERNAME,USER_TYPE,ADDRESS,FULLNAME,CONTACT_NO,EMAIL,PROFILE_IMAGE,FK_DESIGNATION_ID,IS_ACTIVE,FK_DEPARTMENT_ID FROM " . USERMASTER . " WHERE " . $whereusers . " ");
                $fetch_details[0]['PASSWORD'] = password_hash(PASSOWORDMASTER, PASSWORD_DEFAULT);
            } else {

                $whereusers = "USERNAME = '" . $userName . "' AND USER_TYPE IN($userTypeIn) AND DELETE_FLAG = 0";

                $fetch_details = fetch_rec_query("SELECT PK_USER_ID,FK_CLIENT_ID,USERNAME,USER_TYPE,ADDRESS,FULLNAME,CONTACT_NO,EMAIL,PROFILE_IMAGE,PASSWORD,FK_DESIGNATION_ID,IS_ACTIVE,FK_DEPARTMENT_ID FROM " . USERMASTER . " WHERE " . $whereusers . " ");
            }

            //FOR CHECK CLIENT VALIDATION END DATE
//            $getClientDetail = getClientDetail($fetch_details[0]['FK_CLIENT_ID']);
//            if($getClientDetail['status'] == SCS && $getClientDetail['data'][0]['CLIENT_VAL_END_DATE'] >= $todayDate){
//                $validEndDate = '1';
//            }
        }

        if (count($fetch_details) > 0) {

            //CHECK PASWORD HASH STRING
            //  if($password == $fetch_details[0]['PASSWORD']){
            if (password_verify($password, $fetch_details[0]['PASSWORD'])) {

//                if($validEndDate == '1'){

                if ($fetch_details[0]['IS_ACTIVE'] == '1') {

		    // COMMENT BY KAVITA PATEL ON 23-08-2018 START
//                    if (!empty($fetch_details[0]['PROFILE_IMAGE'])) {
//                        $fetch_details[0]['PROFILE_IMAGE'] = APP_WEB_ROOT . "/attachment/" . $fetch_details[0]['PROFILE_IMAGE'];
//                    } else {
//                        $fetch_details[0]['PROFILE_IMAGE'] = "";
//                    }
		    // COMMENT BY KAVITA PATEL ON 23-08-2018 END

                    $finalArrayUser = array();
                    $finalArrayUser[0]['PK_USER_ID'] = !empty($fetch_details[0]['PK_USER_ID']) ? $fetch_details[0]['PK_USER_ID'] : "";
                    $finalArrayUser[0]['FK_CLIENT_ID'] = !empty($fetch_details[0]['FK_CLIENT_ID']) ? $fetch_details[0]['FK_CLIENT_ID'] : "";
                    $finalArrayUser[0]['USERNAME'] = !empty($fetch_details[0]['USERNAME']) ? $fetch_details[0]['USERNAME'] : "";
                    $finalArrayUser[0]['USER_TYPE'] = !empty($fetch_details[0]['USER_TYPE']) ? $fetch_details[0]['USER_TYPE'] : "";
                    $finalArrayUser[0]['ADDRESS'] = !empty($fetch_details[0]['ADDRESS']) ? $fetch_details[0]['ADDRESS'] : "";
                    $finalArrayUser[0]['FULLNAME'] = !empty($fetch_details[0]['FULLNAME']) ? $fetch_details[0]['FULLNAME'] : "";
                    $finalArrayUser[0]['CONTACT_NO'] = !empty($fetch_details[0]['CONTACT_NO']) ? $fetch_details[0]['CONTACT_NO'] : "";
                    $finalArrayUser[0]['EMAIL'] = !empty($fetch_details[0]['EMAIL']) ? $fetch_details[0]['EMAIL'] : "";
                    //                    $finalArrayUser[0]['USER_LEVEL'] = $fetch_details[0]['USER_LEVEL'];
                    $finalArrayUser[0]['PROFILE_IMAGE'] = !empty($fetch_details[0]['PROFILE_IMAGE']) ? $fetch_details[0]['PROFILE_IMAGE'] : "";
                    $finalArrayUser[0]['DESIGNATIONID'] = !empty($fetch_details[0]['FK_DESIGNATION_ID']) ? $fetch_details[0]['FK_DESIGNATION_ID'] : "";
                    $finalArrayUser[0]['DEPARTMENTID'] = !empty($fetch_details[0]['FK_DEPARTMENT_ID']) ? $fetch_details[0]['FK_DEPARTMENT_ID'] : "";

                    $result = array("status" => SCS, "data" => $finalArrayUser);
                    http_response_code(200);
                } else {
                    $result = array("status" => "User Is Inactive. please contact system administartion");
                    http_response_code(400);
                }
//                }else{
//                    $result = array("status" => "client validity end date has been pass. please contact system administartion");
//                    http_response_code(400);
//                }
            } else {
                $result = array("status" => INVALIDUSERPASS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => INVALIDUSERPASS);
            http_response_code(400);
        }

//        }
    } else if ($userName == '' || $password == '') {
        $result = array("status" => USRNAMEPWDBLNK);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR CREATE & EDIT ROLE
function createEditRole($postData) {
    //print_r($postData); exit;
    $requestCase = addslashes(trim($postData['postData']['requestCase'])); //request case
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; // client id
    $sessionuserid = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; //logged in user
    $role = isset($postData['postData']['role']) ? $postData['postData']['role'] : ""; //array of role 
    $roleid = isset($postData['postData']['roleId']) ? addslashes(trim($postData['postData']['roleId'])) : ""; //role id if role for edit
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : ""; //role id if role for edit
    $time = time();

    $clientInfo = getClientDetail($clientId);

    if (isset($roleid) && $requestCase == "editrole") { //if role for edit 
        $A_roledetails['PK_ROLE_ID'] = $roleid;
        $A_roledetails['CHANGED_BY'] = $sessionuserid;
        $A_roletrans['CHANGED_BY'] = $sessionuserid;

        $updateArr = array();
        $updateArr['DELETE_FLAG'] = '1';
        $updateArr['CHANGED_BY'] = $sessionuserid;
        $updateArr['CHANGED_BY_DATE'] = time();
        $where_roletrans = "FK_ROLE_ID = '" . $roleid . "'";
        $delete_user_roles = update_rec(ROLETRANS, $updateArr, $where_roletrans); //delete user role details previous for that user
    }

    $A_roledetails['ROLE_NAME'] = $role[0]['rolename'];
    $A_roledetails['FK_CLIENT_ID'] = $clientId;
    $A_roledetails['CREATED_BY'] = $sessionuserid;
    $A_roledetails['CREATED_BY_DATE'] = $time;
    $A_roledetails['DELETE_FLAG'] = 0;

    if ($requestCase == "editrole") { //if role for edit 
        $whereroleid = "PK_ROLE_ID ='" . $roleid . "' AND FK_CLIENT_ID ='" . $clientId . "' ";
        $update_role_master = update_rec(ROLEMASTER, $A_roledetails, $whereroleid);
        $pk_role_id = $roleid;
    } else if ($requestCase == "createrole") {

        $insert_role_master = insert_rec(ROLEMASTER, $A_roledetails);
        $pk_role_id = $insert_role_master['lastInsertedId'];
    }


    if (isset($insert_role_master) || isset($update_role_master)) {

        foreach ($role as $keyRoleD => $valueRoleD) {

            $A_roletrans['FK_ROLE_ID'] = $pk_role_id;
            $A_roletrans['FK_MODULE_ID'] = $valueRoleD['moduleid'];
            $A_roletrans['FK_ACTIVITY_ID'] = $valueRoleD['activityid'];
            $A_roletrans['CREATED_BY'] = $sessionuserid;
            $A_roletrans['DELETE_FLAG'] = 0;

            $insert_role_trans = insert_rec(ROLETRANS, $A_roletrans);
        }

        if (isset($insert_role_master) || isset($update_role_master)) {
            $data = array("PK_ROLE_ID" => $pk_role_id);
            $result = array("status" => SCS, "data" => $data);
            http_response_code(200);
        } else {

            $result = array("status" => ROLEUSERERROR);
            http_response_code(400);
        }
    } else {

        $result = array("status" => ROLECREATEERR);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR DELETE ROLE
function deleteRole_BL($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $roleId = isset($postData['postData']['roleid']) ? addslashes(trim($postData['postData']['roleid'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // LOGGED IN USER

    $update_rolemaster['DELETE_FLAG'] = 1;
    $update_rolemaster['CHANGED_BY'] = $userId;
    $update_rolemaster['CHANGED_BY_DATE'] = time();

    // CHECK IF ANY USER IS ASSIGN TO THIS ROLE

    $sqlQuery = "SELECT PK_USER_ROLE_ID FROM " . USERROLEACCESSMASTER . " WHERE FK_ROLE_ID = '" . $roleId . "' AND DELETE_FLAG = 0";
    $resultQuery = fetch_rec_query($sqlQuery);

    if (count($resultQuery) <= 0) {

        // UPDATE ROLE MASTER
        $where_roleid = "`PK_ROLE_ID` = '" . $roleId . "' AND `FK_CLIENT_ID` ='" . $clientId . "'";
        $update_role_master = update_rec(ROLEMASTER, $update_rolemaster, $where_roleid);

        // UPDATE ROLE TANS TABLE
        $update_role_trans['DELETE_FLAG'] = 1;
        $update_role_trans['CHANGED_BY'] = $userId;
        $update_role_trans['CHANGED_BY_DATE'] = time();

        $where_roleidtrans = "`FK_ROLE_ID` = '" . $roleId . "'";
        $update_role_trans = update_rec(ROLETRANS, $update_role_trans, $where_roleidtrans);

        if ($update_role_master && $update_role_trans) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => FAILMSG);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NOTALLOWDELETEROLE);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR GET ROLE DETAIL
function getRoleDetails_BL($postData) {
    //print_r($postData);
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $roleId = isset($postData['postData']['roleId']) ? addslashes(trim($postData['postData']['roleId'])) : ""; //(optional)
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestCase = isset($postData['postData']['requestCase']) ? addslashes(trim($postData['postData']['requestCase'])) : ""; //request case


    if (isset($roleId) && $requestCase == "getroledetails") { // get role details by roleid 
        $query_Role = "SELECT RT.`FK_ROLE_ID`,RM.`ROLE_NAME`,RT.`FK_MODULE_ID`,GROUP_CONCAT(CONVERT(RT.`FK_ACTIVITY_ID` ,CHAR(20)) ORDER BY RT.`FK_ACTIVITY_ID` ASC) ACTIVITY,GROUP_CONCAT(AM.`ACTIVITY_NAME` ORDER BY AM.`PK_ACTIVITY_ID` ASC) ACTIVITYNAME, MM.`MODULE_NAME`,MM.`ORG_TYPE` FROM " . ROLETRANS . " RT INNER JOIN " . ROLEMASTER . " RM ON RM.`PK_ROLE_ID` = RT.`FK_ROLE_ID` INNER JOIN " . ACTIVITYMASTER . " AM ON AM.`PK_ACTIVITY_ID` = RT.`FK_ACTIVITY_ID` INNER JOIN " . MODULEMASTER . " MM ON MM.`PK_MODULE_ID` = RT.`FK_MODULE_ID` WHERE RM.`FK_CLIENT_ID` = '" . $clientId . "' AND RM.`PK_ROLE_ID` = '" . $roleId . "' AND RM.`DELETE_FLAG` = 0 AND RT.DELETE_FLAG =0 GROUP BY RT.`FK_ROLE_ID`,RT.`FK_MODULE_ID` ";
        $resultGetRole = fetch_rec_query($query_Role);
        if (count($resultGetRole) > 0) {
            $result = array("status" => SCS, "data" => $resultGetRole);
        } else {
            $result = array("status" => NORECORDS);
        }
    } else if ($requestCase == "getrole") { // get all roles and its details
        $sqlGetRole = "SELECT ROLE_NAME,FK_CLIENT_ID,PK_ROLE_ID,ROLE_TYPE FROM " . ROLEMASTER . " RM WHERE RM.FK_CLIENT_ID = '" . $clientId . "' AND RM.ROLE_TYPE = 1 AND RM.DELETE_FLAG = 0 ";
        $resultGetRole = fetch_rec_query($sqlGetRole);

        $roleDetailJson = array();
        if (count($resultGetRole) > 0) {
//            for($r=0;$r<count($resultGetRole);$r++){
            foreach ($resultGetRole as $keyRole => $valueRole) {

                $roleDetailJson[$keyRole]['roleName'] = $valueRole['ROLE_NAME'];
                $roleDetailJson[$keyRole]['roleclientId'] = $valueRole['FK_CLIENT_ID'];
                $roleDetailJson[$keyRole]['roleId'] = $valueRole['PK_ROLE_ID'];
                $roleDetailJson[$keyRole]['roleType'] = $valueRole['ROLE_TYPE'];

                $query_Role = "SELECT RT.`FK_ROLE_ID`,RT.`FK_MODULE_ID`,GROUP_CONCAT(CONVERT(RT.`FK_ACTIVITY_ID` ,CHAR(20)) ORDER BY RT.`FK_ACTIVITY_ID` ASC) ACTIVITY,GROUP_CONCAT( AM.`ACTIVITY_NAME` ORDER BY AM.`PK_ACTIVITY_ID` ASC) ACTIVITYNAME, MM.MODULE_NAME,MM.`ORG_TYPE`,MM.MODULE_NAME FROM " . ROLETRANS . " RT INNER JOIN " . ACTIVITYMASTER . " AM ON AM.`PK_ACTIVITY_ID` = RT.`FK_ACTIVITY_ID` AND AM.DELETE_FLAG = '0' INNER JOIN " . MODULEMASTER . " MM ON MM.PK_MODULE_ID = RT.FK_MODULE_ID AND MM.DELETE_FLAG = '0' WHERE RT.FK_ROLE_ID = '" . $valueRole['PK_ROLE_ID'] . "' AND RT.DELETE_FLAG = '0' GROUP BY RT.`FK_ROLE_ID`,RT.`FK_MODULE_ID`";
                $fetch_roledetails = fetch_rec_query($query_Role);
                //print_r($fetch_roledetails); exit;

                if (count($fetch_roledetails) > 0) {
                    $tempRoleTransArr = array();

//                    for($rt=0;$rt<count($fetch_roledetails);$rt++){
                    foreach ($fetch_roledetails as $keyRd => $valueRd) {

                        $tempRoleTransArr[$keyRd]['FK_ROLE_ID'] = $valueRd['FK_ROLE_ID'];
                        $tempRoleTransArr[$keyRd]['ROLE_NAME'] = $valueRole['ROLE_NAME'];
                        $tempRoleTransArr[$keyRd]['FK_MODULE_ID'] = $valueRd['FK_MODULE_ID'];
                        $tempRoleTransArr[$keyRd]['MODULE_NAME'] = $valueRd['MODULE_NAME'];
                        $tempRoleTransArr[$keyRd]['ACTIVITY'] = $valueRd['ACTIVITY'];
                        $tempRoleTransArr[$keyRd]['ACTIVITYNAME'] = $valueRd['ACTIVITYNAME'];
                        $tempRoleTransArr[$keyRd]['ORG_TYPE'] = $valueRd['ORG_TYPE'];

                        if ($valueRd['ORG_TYPE'] == "1") { // IF ORG TYPE IS 1 THEN RETURN CLIENT NAME AND ID
                            $query_orgname = "SELECT $clientId PK_ORG_ID,(SELECT CLIENTNAME FROM client_master WHERE PK_CLIENT_ID = $clientId) ORG_NAME,'" . $valueRd['ORG_TYPE'] . "' ORG_TYPE , 'Client' ORGELENAME ";
                        } else {

                            $query_orgname = "SELECT GROUP_CONCAT(CONVERT(`PK_ORG_ID`,CHAR(20))) PK_ORG_ID ,GROUP_CONCAT(`ORGNAME`) ORG_NAME,`ORG_TYPE`,(SELECT GROUP_CONCAT(`VALUE`) FROM " . LOOKUPMASTER . " WHERE `PK_LOOKUP_ID` = `ORG_TYPE`) ORGELENAME FROM " . ORGMASTER . "  WHERE `ORG_TYPE` IN (" . $valueRd['ORG_TYPE'] . ") AND " . ORGMASTER . ".FK_CLIENT_ID = " . $clientId . " GROUP BY `ORG_TYPE`";
                            //ECHO $query_orgname."<BR>";
                        }

                        $fetch_orgname = fetch_rec_query($query_orgname);
                        if (count($fetch_orgname) > 0) {
                            $tempOrgDetail = array();
                            for ($j = 0; $j < count($fetch_orgname); $j++) {
                                $tempOrgDetail[$j] = $fetch_orgname[$j];
                            }
                        } else {
                            $tempOrgDetail = NORECORDS;
                        }

                        $tempRoleTransArr[$keyRd]['ORG'] = $tempOrgDetail;
                    }
                    $roleDetailJson[$keyRole]['roleTrans'] = $tempRoleTransArr;
                } else {
                    $roleDetailJson[$keyRole]['roleTrans'] = NORECORDS;
                }
            }
            $result = array("status" => SCS, "data" => $roleDetailJson);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    }
    return $result;
}

//CREATE EDIT USER FUNCTION
function createEditUser_BL($postData) {
//    print_r($postData); exit;
    $requestCase = isset($postData['postData']['requestCase']) ? addslashes(trim($postData['postData']['requestCase'])) : ""; //request case
    $A_insertusr = array();
    $A_roleuser = array();
    $time = time();
    $sessionuserid = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; //logged in user
    $userId = isset($postData['postData']['requestUserId']) ? addslashes(trim($postData['postData']['requestUserId'])) : ""; //user for edit
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : ""; //user for edit
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; //user for edit
    $userName = isset($postData['postData']['userdata']['userName']) ? $postData['postData']['userdata']['userName'] : ""; // USERNAME
    $fullName = isset($postData['postData']['userdata']['fullName']) ? $postData['postData']['userdata']['fullName'] : ""; // FULLNAME
    $userType = isset($postData['postData']['userdata']['userType']) ? $postData['postData']['userdata']['userType'] : ""; // USERTYPE
    $address = isset($postData['postData']['userdata']['address']) ? $postData['postData']['userdata']['address'] : ""; // ADDRESS
    $contactNo = isset($postData['postData']['userdata']['contactNo']) ? $postData['postData']['userdata']['contactNo'] : ""; // CONTACT NO 
    $email = isset($postData['postData']['userdata']['email']) ? $postData['postData']['userdata']['email'] : ""; // EMAIL
    $defaultDashboard = isset($postData['postData']['userdata']['dashboard']) ? $postData['postData']['userdata']['dashboard'] : "";
    $levelId = isset($postData['postData']['userdata']['levelId']) ? $postData['postData']['userdata']['levelId'] : ""; //USER LEVEL ID
    $supervisorId = isset($postData['postData']['userdata']['supervisorId']) ? $postData['postData']['userdata']['supervisorId'] : ""; //SUPERVISOR ID
    $designationId = isset($postData['postData']['userdata']['designationId']) ? $postData['postData']['userdata']['designationId'] : ""; //DESIGNATION ID
    $googleCalId = isset($postData['postData']['userdata']['googleCalKey']) ? $postData['postData']['userdata']['googleCalKey'] : ""; // GOOGLE CAL LINK 
    $birthDay = isset($postData['postData']['userdata']['birthdate']) ? $postData['postData']['userdata']['birthdate'] : "0000-00-00"; // BIRTHDATE
    $anniversaryDay = isset($postData['postData']['userdata']['anniversarydate']) ? $postData['postData']['userdata']['anniversarydate'] : "0000-00-00"; // ANNIVERSARYDATE
    $gender = isset($postData['postData']['userdata']['gender']) ? $postData['postData']['userdata']['gender'] : ""; // GENDER
    $department = isset($postData['postData']['userdata']['department']) ? $postData['postData']['userdata']['department'] : ""; // GENDER
    $A_insertusr['IS_ACTIVE'] = 1; // USER STATUS ON CREATE
    // VALIDATION CHECK FOR USERNAME
    $where = "";
    if (isset($userId) && $requestCase == "useredit") {
        $where = " AND PK_USER_ID != '" . $userId . "'";
    }

    $sqlCheckUserName = "SELECT PK_USER_ID FROM " . USERMASTER . " WHERE USERNAME = '" . $postData['postData']['userdata']['userName'] . "' AND DELETE_FLAG = 0 " . $where;
    $resultCheckUserName = fetch_rec_query($sqlCheckUserName);
    if (count($resultCheckUserName) > 0) {
        $result = array("status" => NOUSERNAMEAVBL);
        return $result;
    }


    $sqlCheckUserLevel = "SELECT PRIORITY FROM " . CLIENTLOOKUP . " WHERE PK_CLT_LKUP_ID = '" . $postData['postData']['userdata']['levelId'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'USERLEVEL' ORDER BY PK_CLT_LKUP_ID ASC LIMIT 0,1 ";
    $resultCheckUserLevel = fetch_rec_query($sqlCheckUserLevel);

    if (count($resultCheckUserLevel) > 0) {
        $sqlCheckPrority = "SELECT PRIORITY FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'USERLEVEL' ORDER BY PK_CLT_LKUP_ID DESC LIMIT 0,1";
        $resultCheckPriority = fetch_rec_query($sqlCheckPrority);
        if ($resultCheckPriority[0]['PRIORITY'] > $resultCheckUserLevel[0]['PRIORITY']) {
            $result = array("status" => PROVIDEROLEDETAIL);
            http_response_code(400);
            return $result;
        }
    } else {
        $result = array("status" => LEVELIDINVALID);
        http_response_code(400);
        return $result;
    }
    //echo "asda sd"; exit;
    // START - DELETE ALL USER ROLE ACCESS MASTER AND RE-ENTRY 
    if (isset($userId) && $requestCase == "useredit") { //if user for edit 
        $A_insertusr['PK_USER_ID'] = $userId;
        $A_insertusr['CHANGED_BY'] = $sessionuserid;
        $A_roleuser['CHANGED_BY'] = $sessionuserid;

        $updateArr = array();
        $updateArr['DELETE_FLAG'] = '1';
        $updateArr['CHANGED_BY'] = $sessionuserid;
        $updateArr['CHANGED_BY_DATE'] = time();
        $where_roletrans = "FK_USER_ID = '" . $userId . "'";
        $delete_user_roles = update_rec(USERROLEACCESSMASTER, $updateArr, $where_roletrans); //delete user role details previous for that user        
        // DELETE USER FROM MAPPING MASTER TABLE        
        $whereMapping = "FK_USER_ID = '" . $userId . "'";
        $delete_user_Mapping = update_rec(USERMAPPINGMASTER, $updateArr, $whereMapping); //delete user role details previous for that user
    }
    // END - DELETE ALL USER ROLE ACCESS MASTER AND RE-ENTRY 
    //START INSERT INTO USER_MASTER TABLE
    $A_insertusr['FK_CLIENT_ID'] = $clientId;
    $A_insertusr['FK_ORG_ID'] = $orgId;
    $A_insertusr['USERNAME'] = $userName; // USERNAME
    $A_insertusr['FULLNAME'] = $fullName; // FULLNAME
    $A_insertusr['USER_TYPE'] = $userType; // USERTYPE
    $A_insertusr['ADDRESS'] = $address; // ADDRESS
    $A_insertusr['CONTACT_NO'] = $contactNo; // CONTACT NO 
    $A_insertusr['EMAIL'] = $email; // EMAIL
    $A_insertusr['DEFAULT_DASH'] = $defaultDashboard;
    $A_insertusr['USER_LEVEL'] = $levelId;
    $A_insertusr['FK_SUPERVISOR_ID'] = $supervisorId; // SUPERVISORID
    $A_insertusr['FK_DESIGNATION_ID'] = $designationId; // DESIGNATIONID
    $A_insertusr['USER_G_CAL_ID'] = $googleCalId; // GOOGLE CAL LINK 
    $A_insertusr['BIRTHDATE'] = $birthDay; // BIRTHDATE
    $A_insertusr['ANNIVERSARYDATE'] = $anniversaryDay; // ANNIVERSARYDATE
    $A_insertusr['GENDER'] = $gender; // GENDER
    $A_insertusr['IS_ACTIVE'] = 1; // USER STATUS ON CREATE
    $A_insertusr['FK_DEPARTMENT_ID'] = $department; // USER STATUS ON CREATE
    //$A_insertusr['REFERENCE'] = $postData['postData']['userdata']['REFERENCE']; // reference
    //print_r($A_insertusr); exit;
    $A_insertusr['DELETE_FLAG'] = 0; // delete flag 
    $roleid = $postData['postData']['role']; // Role array
    $selectedRoleId = $postData['postData']['roleId']; // Role Id array
    $branchIds = $postData['postData']['branch']; // branch (array)

    if (isset($userId) && $requestCase == "useredit") { //if user for edit 
        $whereuserid = "PK_USER_ID ='" . $userId . "' AND FK_CLIENT_ID ='" . $clientId . "' ";
        $update_user = update_rec(USERMASTER, $A_insertusr, $whereuserid);

        $lastinserteduserid = $userId; // last inserted user id

        $insertMapping['FK_USER_ID'] = $lastinserteduserid;
        $insertMapping['FK_CLIENT_ID'] = $clientId;
        $insertMapping['FK_SUPERVISOR_ID'] = $postData['postData']['userdata']['supervisorId'];
        $insertMapping['CREATED_BY'] = $sessionuserid;
        $insertMapping['CREATED_BY_DATE'] = time();
        $insertMapping['DELETE_FLAG'] = 0;
        $insert_Mapping = insert_rec(USERMAPPINGMASTER, $insertMapping); // insert query in user_master
    } else if ($requestCase == "createUser") { //if user for create
        //print_r($getHierarchy); exit;
        $newPass = md5(trim($postData['postData']['userdata']['passWord']));
        $A_insertusr['PASSWORD'] = password_hash($newPass, PASSWORD_DEFAULT); //HASH STRING
        $A_insertusr['SECRET_IDENTIFICATION_CODE'] = md5($postData['postData']['userdata']['userName'] . " " . $newPass); //HASH STRING
        $A_insertusr['CHANGE_PASSWORD_FLAG'] = '0'; // FLAG FOR CHECK FIRST TIME LOGIN PASSWORD CHANGE
        $A_insertusr['CREATED_BY'] = $sessionuserid; // LOGGED IN USER
        $A_insertusr['CREATED_BY_DATE'] = $time; // created by date
        $A_insertusr['NEW_VERSION_FLAG'] = 0;

        $insert_user = insert_rec(USERMASTER, $A_insertusr); // insert query in user_master
        $lastinserteduserid = $insert_user['lastInsertedId']; // last inserted user id
        // GETTING SUPERVISOR HIRERCHY FROM LIST
        $insertMapping['FK_USER_ID'] = $lastinserteduserid;
        $insertMapping['FK_CLIENT_ID'] = $clientId;
        $insertMapping['FK_SUPERVISOR_ID'] = $postData['postData']['userdata']['supervisorId'];
        $insertMapping['CREATED_BY'] = $sessionuserid;
        $insertMapping['CREATED_BY_DATE'] = time();
        $insertMapping['DELETE_FLAG'] = 0;
        $insert_Mapping = insert_rec(USERMAPPINGMASTER, $insertMapping); // insert query in user_master
    }

    //echo $update_user; exit;
    //end insert into user_master table
    if (isset($insert_user) || (isset($update_user) || $update_user > 0 || $update_user == 0)) {
        //start insert into user_role_access_master table

        foreach ($branchIds as $keyBranch => $valueBranch) { //loop for role and module id
//print_r($valueBranch);exit;
            $A_roleuser['FK_USER_ID'] = $lastinserteduserid;
            $A_roleuser['FK_ROLE_ID'] = $selectedRoleId;
            $A_roleuser['FK_ORG_ID'] = $valueBranch;
            $A_roleuser['CREATED_BY'] = $sessionuserid;
            $A_roleuser['CREATED_BY_DATE'] = $time;
            $A_roleuser['DELETE_FLAG'] = 0;
            $insert_user_role = insert_rec(USERROLEACCESSMASTER, $A_roleuser); // insert query in user_role_access_master
        }

        if ($insert_user_role) {
// GET LASTINSERTED USER DETAIL BY DHRUV ON 21-03-2018
            $requestedData = array();
            $requestedData['postData']['clientId'] = $clientId;
            $requestedData['postData']['orgId'] = $orgId;
            $requestedData['postData']['requestUserId'] = $lastinserteduserid;
            $requestedData['postData']['userId'] = $userId;
            $getUserDetails = userListing($requestedData);
            if ($getUserDetails['status'] == SCS) {
                $result = array("status" => SCS, "data" => $getUserDetails['data']);
                http_response_code(200);
            } else {
                $result = array("status" => "lastinserted record not available");
                 http_response_code(400);
            }
        } else {
            $result = array("status" => ROLEUSERERROR);
            http_response_code(400);
        }
    } else {
        $result = array("status" => USER_ERROR);
        http_response_code(400);
    }

    return $result;
}

//USER LISTING ADDED BY DHRUV ON 04-07-2018
function userListing($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestUserId = isset($postData['postData']['requestUserId']) ? addslashes(trim($postData['postData']['requestUserId'])) : "";
    $userType = isset($postData['postData']['userType']) ? addslashes(trim($postData['postData']['userType'])) : "";
    $supervisorId = isset($postData['postData']['supervisorId']) ? addslashes(trim($postData['postData']['supervisorId'])) : ""; //SUPERVISOR ID

    $whereUser = "";
    if (!empty($requestUserId)) {
        $whereUser = " AND UM.PK_USER_ID = '" . $requestUserId . "'";
    }

    $whereSupervisor = "";
    if (!empty($supervisorId) && $supervisorId != "") {
        $whereSupervisor = " AND UM.FK_SUPERVISOR_ID = '" . $supervisorId . "'";
    }

    $sqlUserListing = "SELECT UM.USERNAME,UM.PK_USER_ID,UM.FULLNAME,UM.PROFILE_IMAGE,UM.USER_LEVEL,CLT.LOOK_VALUE,GROUP_CONCAT(DISTINCT URAM.FK_ORG_ID) FK_ORG_ID,GROUP_CONCAT(DISTINCT ORG.ORGNAME) ORGNAME,UM.FK_DESIGNATION_ID,IF(FK_DESIGNATION_ID !='',(SELECT CLT1.DISPLAY_VALUE FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.PK_CLT_LKUP_ID = UM.FK_DESIGNATION_ID AND CLT1.DELETE_FLAG = '0'),'No Designation') DESIGNATIONNAME,IS_ACTIVE FROM " . USERMASTER . " UM INNER JOIN " . CLIENTLOOKUP . " CLT ON CLT.PK_CLT_LKUP_ID = UM.USER_LEVEL AND CLT.DELETE_FLAG = '0' LEFT JOIN " . USERROLEACCESSMASTER . " URAM  ON UM.PK_USER_ID = URAM.FK_USER_ID AND URAM.DELETE_FLAG = '0' LEFT JOIN " . ORGMASTER . " ORG ON ORG.PK_ORG_ID = URAM.FK_ORG_ID AND ORG.DELETE_FLAG = '0'  WHERE  UM.FK_CLIENT_ID = '" . $clientId . "'  AND UM.DELETE_FLAG = 0 AND USER_TYPE != '-1' " . $whereUser . " " . $whereSupervisor . " GROUP BY UM.PK_USER_ID ";

    $resultUserListing = fetch_rec_query($sqlUserListing);
//    print_r($resultUserListing);exit;
    if (count($resultUserListing) > 0) {
        $finalJson = array();
        foreach ($resultUserListing as $keyUser => $valueUser) {

            $finalJson[$keyUser]['userId'] = $valueUser['PK_USER_ID'];
            $finalJson[$keyUser]['userName'] = $valueUser['USERNAME'];
            $finalJson[$keyUser]['userFullName'] = $valueUser['FULLNAME'];
            $finalJson[$keyUser]['userLevel'] = $valueUser['USER_LEVEL'];
            $finalJson[$keyUser]['userLevelName'] = $valueUser['LOOK_VALUE'];
            $finalJson[$keyUser]['userDesignationId'] = $valueUser['FK_DESIGNATION_ID'];
            $finalJson[$keyUser]['userDesignationName'] = $valueUser['DESIGNATIONNAME'];
            $finalJson[$keyUser]['userProfileImage'] = !empty($valueUser['PROFILE_IMAGE']) ? $valueUser['PROFILE_IMAGE'] : ""; // CHANGED BY KAVITA PATEL ON 23-08-2018
            $finalJson[$keyUser]['userBranchName'] = !empty($valueUser['ORGNAME']) ? $valueUser['ORGNAME'] : "";
            $finalJson[$keyUser]['userStatus'] = $valueUser['IS_ACTIVE'];
        }
        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// DELETE USER ADDED BY DHRUV ON 04-07-2018
function deleteUser_BL($postData) {
//    print_r($postData); exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestedUserId = isset($postData['postData']['requestedUserId']) ? addslashes(trim($postData['postData']['requestedUserId'])) : "";
    $sessionuserId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; // LOGGED IN USER
    $current_time = time();

    // CHECK VALIDATION           
    $sqlCheckUser = "SELECT PK_USER_ID FROM " . USERMASTER . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' AND PK_USER_ID = '" . $requestedUserId . "'";
    $resultCheckUser = fetch_rec_query($sqlCheckUser);
//    print_r($resultCheckUser);exit;  
    if (count($resultCheckUser) > 0) {

        $update_usermaster['DELETE_FLAG'] = 1;
        $update_usermaster['CHANGED_BY'] = $sessionuserId;
        $update_usermaster['CHANGED_BY_DATE'] = $current_time;

        $where_userid = "`PK_USER_ID` = '" . $requestedUserId . "' AND `FK_CLIENT_ID` ='" . $clientId . "'";
        $update_user_master = update_rec(USERMASTER, $update_usermaster, $where_userid);
        $result = array("status" => SCS);
        http_response_code(200);

        if ($update_user_master) {
            // UPDATE USER ROLE ACCESS MASTER 
            $update_user_role['DELETE_FLAG'] = 1;
            $update_user_role['CHANGED_BY'] = $sessionuserId;
            $update_user_role['CHANGED_BY_DATE'] = $current_time;

            $where_userid_role = "`FK_USER_ID` = '" . $requestedUserId . "' ";
            $update_userrole_master = update_rec(USERROLEACCESSMASTER, $update_user_role, $where_userid_role);

            // UPDATE USER MAPPING TABLE
            $update_Mapping['DELETE_FLAG'] = 1;
            $update_Mapping['CHANGED_BY'] = $sessionuserId;
            $update_Mapping['CHANGED_BY_DATE'] = $current_time;

            $where_Mapping_role = "`FK_USER_ID` = '" . $requestedUserId . "' ";
            $update_userrole_master = update_rec(USERMAPPINGMASTER, $update_Mapping, $where_Mapping_role);
            if ($update_userrole_master) {
                $result = array("status" => SCS);
                http_response_code(200);
            }
        }
    }
    return $result;
}

// FUNCTION FOR INSERT AND UPDATE USER LEVEL ADDED BY DHRUV ON 04-07-2018
function insertUpdateUserLevel($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupId = isset($postData['postData']['lookupId']) ? addslashes(trim($postData['postData']['lookupId'])) : "";
    $levelName = isset($postData['postData']['levelName']) ? addslashes(trim($postData['postData']['levelName'])) : "";
    $priorityLevel = isset($postData['postData']['priorityLevel']) ? addslashes(trim($postData['postData']['priorityLevel'])) : "";

    // INSERT INTO CLIENT LOOKUP

    if (!empty($lookupId)) { // UPDATE 
        $updateClientLkp['LOOK_VALUE'] = $levelName;
        $updateClientLkp['CHANGED_BY'] = $userId;
        $updateClientLkp['CHANGED_BY_DATE'] = time();
        $updateClientLkp['DELETE_FLAG'] = 0;
        // WHERE
        $whereLevel = "PK_CLT_LKUP_ID = '" . $lookupId . "' AND DELETE_FLAG =0 ";
        $updateRec = update_rec(CLIENTLOOKUP, $updateClientLkp, $whereLevel);
        if ($updateRec) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else { // INSERT
        $vaidationFlag = false;
        // CHECK THE PRIORITY LEVEL VALIDATION   
        $sqlClientLkp = "SELECT PRIORITY FROM " . CLIENTLOOKUP . " WHERE LOOK_TYPE = 'USERLEVEL' AND DELETE_FLAG =0 AND FK_CLIENT_ID = '" . $clientId . "' ORDER BY PRIORITY ASC LIMIT 0,1";
        $resultClientLkp = fetch_rec_query($sqlClientLkp);
        //print_r($resultClientLkp); exit;
        if (count($resultClientLkp) > 0) {
            $currlevel = (int) $resultClientLkp[0]['PRIORITY'] - 1;
            $newlevel = (int) $priorityLevel;

            if ($newlevel == $currlevel) {
                $vaidationFlag = true;
            }
        } else {
            if ($priorityLevel == 10) {
                $vaidationFlag = true;
            } else {
                $vaidationFlag = false;
            }
        }

        if ($vaidationFlag) {
            $insertClientLkp['LOOK_TYPE'] = "USERLEVEL";
            $insertClientLkp['LOOK_VALUE'] = $levelName;
            $insertClientLkp['PRIORITY'] = $priorityLevel;
            $insertClientLkp['FK_CLIENT_ID'] = $clientId;
            $insertClientLkp['CREATED_BY'] = $userId;
            $insertClientLkp['CREATED_BY_DATE'] = time();
            $insertClientLkp['DELETE_FLAG'] = 0;

            $insertRec = insert_rec(CLIENTLOOKUP, $insertClientLkp);
            if ($insertRec) {
                $finalJson = array("levelId" => $insertRec['lastInsertedId']);
                $result = array("status" => SCS, "data" => $finalJson);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTQUERYFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PRIORITYLEVELCHECK);
            http_response_code(400);
        }
    }
    return $result;
}

// FUNCTION FOR GET USER LEVEL ADDED BY DHRUV ON 04-07-2018
function getUserLevel($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $onlyLevelFlag = isset($postData['postData']['levelFlag']) ? addslashes(trim($postData['postData']['levelFlag'])) : "";


    $sqlUserLevel = "SELECT VALUE,PK_LOOKUP_ID FROM " . LOOKUPMASTER . " LM WHERE TYPE = 'USERLEVEL' AND DELETE_FLAG = 0";
    $resultUserLevel = fetch_rec_query($sqlUserLevel);
    if (count($resultUserLevel) > 0) {

        $finalJson = array();
//        for($i=0;$i<count($resultUserLevel);$i++){
        foreach ($resultUserLevel as $keyUl => $valueUl) {

            if (!empty($onlyLevelFlag) && $onlyLevelFlag == 1) {

                $sqlClientLkp = "SELECT LOOK_VALUE,PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE LOOK_TYPE = 'USERLEVEL' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '" . $clientId . "' ORDER BY PRIORITY ASC LIMIT " . ($keyUl) . ",1";
                $resultClientLkp = fetch_rec_query($sqlClientLkp);
                if (count($resultClientLkp) > 0) {
                    $finalJson[$keyUl]['levelDefaultName'] = $valueUl['VALUE'];
                    $finalJson[$keyUl]['lookupDefaultId'] = $valueUl['PK_LOOKUP_ID'];
                    $finalJson[$keyUl]['levelName'] = $resultClientLkp[0]['LOOK_VALUE'];
                    $finalJson[$keyUl]['levelId'] = $resultClientLkp[0]['PK_CLT_LKUP_ID'];
                }
            } else {

                $finalJson[$keyUl]['levelDefaultName'] = $valueUl['VALUE'];
                $finalJson[$keyUl]['lookupDefaultId'] = $valueUl['PK_LOOKUP_ID'];

                // START GETTING NAME FROM CLIENT LOOK IF AVALIABLE
                $sqlClientLkp = "SELECT LOOK_VALUE,PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE LOOK_TYPE = 'USERLEVEL' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '" . $clientId . "' ORDER BY PRIORITY ASC LIMIT " . ($keyUl) . ",1";
                $resultClientLkp = fetch_rec_query($sqlClientLkp);

                if (count($resultClientLkp) > 0) {
                    $finalJson[$keyUl]['levelName'] = $resultClientLkp[0]['LOOK_VALUE'];
                    $finalJson[$keyUl]['levelId'] = $resultClientLkp[0]['PK_CLT_LKUP_ID'];
                } else {
                    $finalJson[$keyUl]['levelName'] = "";
                    $finalJson[$keyUl]['levelId'] = "";
                }
            }
        }
        //print_r($finalJson); exit;
        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// FUNCTION FOR GET USER LISTING BY NAME ADDED BY DHRUV ON 04-07-2018
function getUserListingByName($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $keyWord = isset($postData['postData']['keyWord']) ? addslashes(trim($postData['postData']['keyWord'])) : "";

    $activeInactiveFlag = isset($postData['postData']['activeInactiveFlag']) ? addslashes(trim($postData['postData']['activeInactiveFlag'])) : "-1"; //IF FALG IS ON THEN SEND ALL USER ACTIVE & INACTIVE
    $whereUserAct = "";
    if ($activeInactiveFlag == '-1') {
        $whereUserAct = " AND UM.IS_ACTIVE = '1' ";
    }

    $whereUser = "AND FK_CLIENT_ID = '" . $clientId . "' AND USER_TYPE IN (1,-1) AND (FULLNAME LIKE '%" . $keyWord . "%') AND UAM.FK_ORG_ID IN (" . $orgId . ") $whereUserAct ";

    $sqlUserMaster = "SELECT ADDRESS,CONTACT_NO,DEFAULT_DASH,EMAIL,FK_CLIENT_ID,FK_DESIGNATION_ID,FK_SUPERVISOR_ID,FULLNAME,PK_USER_ID,PROFILE_IMAGE,USERNAME,USER_LEVEL,USER_TYPE FROM " . USERMASTER . " UM INNER JOIN " . USERROLEACCESSMASTER . " UAM ON UAM.FK_USER_ID = UM.PK_USER_ID WHERE UM.DELETE_FLAG = 0 AND UAM.DELETE_FLAG = '0' $whereUser";
    $fetchUser = fetch_rec_query($sqlUserMaster);
//    print_r($fetchUser); exit;
    if (count($fetchUser) > 0) {
        $finalJson = array();
        foreach ($fetchUser as $keyUser => $valueUser) {

            $finalJson[$keyUser]['userName'] = $valueUser['USERNAME'];
            $finalJson[$keyUser]['fullName'] = $valueUser['FULLNAME'];
            $finalJson[$keyUser]['userId'] = $valueUser['PK_USER_ID'];
            $finalJson[$keyUser]['clientId'] = $valueUser['FK_CLIENT_ID'];
        }

        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// FUNCTION FOR ASSIGN USER TO DEPARTMENT ADDED BY DHRUV ON 06-07-2018
function assignUserDepartment($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestId = isset($postData['postData']['requestId']) ? addslashes(trim($postData['postData']['requestId'])) : "";
    $departMentId = isset($postData['postData']['departMentId']) ? addslashes(trim($postData['postData']['departMentId'])) : "";
// GET DEPARTMENT 
    $getDepartMentId = "SELECT PK_DEPARTMENT_ID, ASSIGN_USER FROM " . DEPARTMENTMASTER . " WHERE DELETE_FLAG = '0' AND PK_DEPARTMENT_ID = '" . $departMentId . "'";
    $resdep = fetch_rec_query($getDepartMentId);
    if (count($resdep) > 0) {
//        EXPLOAD USERS
        $updateFlag = '0';
        $expload = explode(",", $resdep[0]['ASSIGN_USER']);
//  VALIDATION FOR USERS      
        if (!empty($expload) && count($expload) > 0) {
            if (in_array($requestId, $expload)) {
                $updateFlag = '1';
            }
        }

        if (empty($updateFlag)) {
//          ADD NEW ARRAY
            array_push($expload, $requestId);
//          IMPLODE 
            $addNewUser = implode(",", $expload);

//        $updatedUsers = $resdep[0]['ASSIGN_USER'].",".$requestId;        
            $updateDep = array();
            $updateDep['ASSIGN_USER'] = $addNewUser;
            $updateDep['CHANGED_BY'] = $userId;
            $updateDep['CHANGED_BY_DATE'] = time();
//        print_r($updateDep);exit;   
            $whereUpdateDep = "PK_DEPARTMENT_ID = '" . $departMentId . "' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '" . $clientId . "'";

            $assignRequest = update_rec(DEPARTMENTMASTER, $updateDep, $whereUpdateDep);
            if ($assignRequest) {
                $result = array("status" => SCS);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTQUERYFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => "user already added");
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// CREATE DEPARTMENT ADDED BY DHRUV ON 04-07-2018
function createDepartment($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $departmentName = isset($postData['postData']['departmentName']) ? addslashes(trim($postData['postData']['departmentName'])) : "";
    $assignUserId = isset($postData['postData']['assignUserId']) ? addslashes(trim($postData['postData']['assignUserId'])) : "";

    if (!empty($clientId) && $clientId != "" && !empty($departmentName) && $departmentName != "") {
        $insertDep = array();
        $insertDep['FK_CLIENT_ID'] = $clientId;
        $insertDep['FK_ORG_ID'] = $orgId;
        $insertDep['DEPARTMENT_NAME'] = $departmentName;
        $insertDep['ASSIGN_USER'] = $assignUserId;
        $insertDep['CREATED_BY'] = $userId;
        $insertDep['CREATED_BY_DATE'] = time();
        $insertDep['DELETE_FLAG'] = 0;
        $insertRec = insert_rec(DEPARTMENTMASTER, $insertDep);
        if ($insertRec) {
            $finalJson = array("departmentId" => $insertRec['lastInsertedId']);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => INSERTQUERYFAIL);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

// DEPARTMENT LISTING ADDED BY DHRUV ON 06-07-2018
function departmentListing($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
//           GET DEPARTMENT LISTING
    $sqlDepartmentListing = "SELECT PK_DEPARTMENT_ID, DEPARTMENT_NAME, ASSIGN_USER FROM " . DEPARTMENTMASTER . " WHERE DELETE_FLAG = 0 AND FK_CLIENT_ID = '" . $clientId . "'  ";
    $resDepartmentListing = fetch_rec_query($sqlDepartmentListing);
    if (count($resDepartmentListing) > 0) {
        $finalJson = array();
        foreach ($resDepartmentListing as $key => $value) {

            $finalJson[$key]['fullName'] = $value['PK_DEPARTMENT_ID'];
            $finalJson[$key]['depName'] = $value['DEPARTMENT_NAME'];
//            GET ASSIGNABLE USERS FROM DEPARTMENT
            $getUserFromDep = "SELECT FULLNAME, USERNAME, PK_USER_ID FROM " . USERMASTER . " WHERE DELETE_FLAG = '0' AND FK_CLIENT_ID = '" . $clientId . "' AND PK_USER_ID IN (" . $value['ASSIGN_USER'] . ") ";
            $resUserFromDep = fetch_rec_query($getUserFromDep);
            $tempArr = array();
            if (count($resUserFromDep) > 0) {
                foreach ($resUserFromDep as $keyUser => $valueUser) {
                    $tempArr[$keyUser]['userName'] = $valueUser['USERNAME'];
                    $tempArr[$keyUser]['userId'] = $valueUser['PK_USER_ID'];
                    $tempArr[$keyUser]['userFullName'] = $valueUser['FULLNAME'];
                }
                $finalJson[$key]['userDetail'] = $tempArr;
            } else {
                $finalJson[$key]['userDetail'] = NORECORDS;
            }
        }
        if ($finalJson) {
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    }
    return $result;
}

// REMOVE ASSIGNABLE USER FROM DEPARTMENT ADDED BY DHRUV ON 06-07-2018
function removeUserFromDep($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $requestId = isset($postData['postData']['requestId']) ? addslashes(trim($postData['postData']['requestId'])) : "";
    $departMentId = isset($postData['postData']['departMentId']) ? addslashes(trim($postData['postData']['departMentId'])) : "";
// GET DEPARTMENT 
    $getDepartMentId = "SELECT PK_DEPARTMENT_ID, ASSIGN_USER FROM " . DEPARTMENTMASTER . " WHERE DELETE_FLAG = '0' AND PK_DEPARTMENT_ID = '" . $departMentId . "'";
    $resdep = fetch_rec_query($getDepartMentId);
    if (count($resdep) > 0) {
//        EXPLODE ARRAY
        $updateFlag = '0';
        $expload = explode(",", $resdep[0]['ASSIGN_USER']);

        if (!empty($expload) && count($expload) > 0) {
            if (in_array($requestId, $expload)) {
                $updateFlag = '1';
            }
        }

        if (!empty($updateFlag)) {
//        REMOVE USER THROUGH ARRAY_SEARCH FUNCTION
            if (($key = array_search($requestId, $expload)) !== false) {
                unset($expload[$key]);
                $finalString = implode(",", $expload);
            }
            $updateDep = array();
            $updateDep['ASSIGN_USER'] = $finalString;
            $updateDep['CHANGED_BY'] = $userId;
            $updateDep['CHANGED_BY_DATE'] = time();

            $whereUpdateDep = "PK_DEPARTMENT_ID = '" . $departMentId . "' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '" . $clientId . "'";

            $assignRequest = update_rec(DEPARTMENTMASTER, $updateDep, $whereUpdateDep);
            if ($assignRequest) {
                $result = array("status" => SCS);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTQUERYFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => "USER NOT IN TABLE");
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function loadAuth_BL($postdata) {
    // print_r($postData); exit;
    $sessionuserid = isset($postdata['postData']['userId']) ? addslashes(trim($postdata['postData']['userId'])) : ""; // logged in user
    $clientid = isset($postdata['postData']['clientId']) ? addslashes(trim($postdata['postData']['clientId'])) : "";
    $orgId = isset($postdata['postData']['orgId']) ? addslashes(trim($postdata['postData']['orgId'])) : "";

    //query for user details
    if ($sessionuserid != -1) {

        $query_User = "SELECT UM.PK_USER_ID, UM.FK_CLIENT_ID, UM.USERNAME,UM.FULLNAME,UM.USER_TYPE,UM.NEW_VERSION_FLAG,UM.CHANGE_PASSWORD_FLAG,CM.CLIENTNAME,CM.CLIENT_VAL_END_DATE,CLT.LOOK_VALUE,CLT.PRIORITY FROM " . USERMASTER . " UM INNER JOIN " . CLIENTMASTER . " CM ON CM.PK_CLIENT_ID = UM.FK_CLIENT_ID INNER JOIN " . CLIENTLOOKUP . " CLT ON CLT.PK_CLT_LKUP_ID = UM.USER_LEVEL  WHERE `PK_USER_ID` ='" . $sessionuserid . "' AND CM.PK_CLIENT_ID = '" . $clientid . "' AND UM.DELETE_FLAG = 0 ";
        $fetch_userdetails = fetch_rec_query($query_User);
    } else {
        $query_Client = "SELECT CM.PK_CLIENT_ID clientId FROM " . CLIENTMASTER . " CM  WHERE CM.PK_CLIENT_ID = '" . $clientid . "' AND CM.DELETE_FLAG = 0 ";
        $fetch_userdetails_client = fetch_rec_query($query_Client);

        $fetch_userdetails[0]['PK_USER_ID'] = -1;
        $fetch_userdetails[0]['FK_CLIENT_ID'] = $clientid;
        $fetch_userdetails[0]['USERNAME'] = "MasterUser";
        $fetch_userdetails[0]['FULLNAME'] = "MasterUser";
        $fetch_userdetails[0]['USER_TYPE'] = -1;
        //$fetch_userdetails[0]['clientType'] = isset($fetch_userdetails_client[0]['clientType']) ? $fetch_userdetails_client[0]['clientType'] : "";
        $fetch_userdetails[0]['CLIENTNAME'] = "Lithe Technologies Pvt Ltd.";
    }

    if (count($fetch_userdetails) > 0) {
        $clientLogo = isset($fetch_userdetails[0]['LOGO']) ? basename($fetch_userdetails[0]['LOGO']) : "no-image.png";
        //query for calendar configuration

        if ($fetch_userdetails[0]['USER_TYPE'] != '-1') { //IF USER IS NOT SUPERADMIN (-1 = SUPERADMIN)
            //$query_Role = "SELECT UR.`FK_MODULE_ID` PK_MODULE_ID,UR.`FK_ACTIVITY_ID` PK_ACTIVITY_ID,ACTIVITY_NAME,IF(MM.ORG_TYPE =1 , '".$clientid."', (SELECT GROUP_CONCAT(DISTINCT CONVERT(UR.`FK_ORG_ID` ,CHAR(20))) FROM ".USERROLEACCESSMASTER." UR1 WHERE UR1.FK_MODULE_ID = UR.FK_MODULE_ID AND UR1.FK_ACTIVITY_ID = UR.FK_ACTIVITY_ID LIMIT 0,1)) PK_ORG_ID FROM ".USERROLEACCESSMASTER." UR INNER JOIN ".MODULEMASTER." MM ON MM.PK_MODULE_ID = UR.`FK_MODULE_ID` INNER JOIN ".ACTIVITYMASTER." AM ON AM.PK_ACTIVITY_ID = UR.FK_ACTIVITY_ID WHERE `FK_USER_ID` IN (".$sessionuserid.") AND UR.`DELETE_FLAG` = 0 AND MM.MODULE_TYPE = '1' GROUP BY UR.FK_MODULE_ID,UR.FK_ACTIVITY_ID"; 
            $query_Role = "SELECT RT.`FK_MODULE_ID` PK_MODULE_ID,MM.MODULE_NAME,RT.`FK_ACTIVITY_ID` PK_ACTIVITY_ID,AM.ACTIVITY_NAME,GROUP_CONCAT(DISTINCT CONVERT(UAM.`FK_ORG_ID` ,CHAR(20))) PK_ORG_ID FROM " . USERROLEACCESSMASTER . " UAM INNER JOIN " . ROLEMASTER . " RM ON RM.PK_ROLE_ID = UAM.FK_ROLE_ID INNER JOIN " . ROLETRANS . " RT ON RT.FK_ROLE_ID = RM.PK_ROLE_ID INNER JOIN " . ACTIVITYMASTER . " AM ON AM.PK_ACTIVITY_ID = RT. FK_ACTIVITY_ID INNER JOIN " . MODULEMASTER . " MM ON MM.PK_MODULE_ID = AM.FK_MODULE_ID WHERE UAM.DELETE_FLAG = '0' AND RT.DELETE_FLAG = '0' AND UAM.FK_USER_ID IN (" . $sessionuserid . ") GROUP BY RT.FK_MODULE_ID,RT.FK_ACTIVITY_ID";
        } else if ($fetch_userdetails[0]['USER_TYPE'] == '-1') { //if superadmin FOR FRONT END
            if (!empty($clientid) && $clientid != "-1") {
                $whereOrg = " AND OM1.FK_CLIENT_ID = '" . $clientid . "'";
            } else {
                $whereOrg = "";
            }
            $query_Role = "SELECT MM.`PK_MODULE_ID`,MM.`MODULE_NAME`,AM.`PK_ACTIVITY_ID`,ACTIVITY_NAME,IF(MM.`ORG_TYPE` = 1," . $clientid . ",(SELECT GROUP_CONCAT( DISTINCT CONVERT(OM1.`PK_ORG_ID` ,CHAR(20))) PK_ORG_ID FROM " . MODULEMASTER . " MM1, " . ORGMASTER . " OM1  WHERE OM1.`DELETE_FLAG` = 0 AND FIND_IN_SET(OM1.`ORG_TYPE`, MM.`ORG_TYPE`) " . $whereOrg . ")) PK_ORG_ID FROM " . MODULEMASTER . " MM  INNER JOIN " . ACTIVITYMASTER . " AM ON AM.`FK_MODULE_ID` = MM.`PK_MODULE_ID` WHERE MM.`DELETE_FLAG` =0";
        }
        //echo $query_Role; exit;
        $fetch_roledetails = fetch_rec_query($query_Role);
        // print_r($fetch_roledetails); EXIT;
        $clientDetail = array();
        if ($fetch_userdetails[0]['PK_USER_ID'] == "-1") { //START GETTING CLIENT LIST
//            $sqlClientMaster = "SELECT * FROM ".CLIENTMASTER." CM WHERE CM.DELETE_FLAG = 0 ";
            $sqlClientMaster = "SELECT CM.CLIENTNAME,CM.PK_CLIENT_ID,CM.CLIENT_EMAIL,CM.CLIENT_MOB,CM.CLIENT_ADDRESS FROM " . CLIENTMASTER . " CM WHERE CM.DELETE_FLAG = 0 ";
        } else {
//            $sqlClientMaster = "SELECT * FROM ".CLIENTMASTER." CM WHERE CM.DELETE_FLAG = 0 AND PK_CLIENT_ID = '".$clientid."'";
            $sqlClientMaster = "SELECT CM.CLIENTNAME,CM.PK_CLIENT_ID,CM.CLIENT_EMAIL,CM.CLIENT_MOB,CM.CLIENT_ADDRESS FROM " . CLIENTMASTER . " CM WHERE CM.DELETE_FLAG = 0 AND PK_CLIENT_ID = '" . $clientid . "'";
        }
        $resultClientMaster = fetch_rec_query($sqlClientMaster);
        if (count($resultClientMaster) > 0) {
//            for($r=0;$r<count($resultClientMaster); $r++){
            foreach ($resultClientMaster as $keyClient => $valueClient) {

                $clientDetail[$keyClient]['clientName'] = $valueClient['CLIENTNAME'];
                $clientDetail[$keyClient]['clientId'] = $valueClient['PK_CLIENT_ID'];
                $clientDetail[$keyClient]['clientEmail'] = $valueClient['CLIENT_EMAIL'];
                $clientDetail[$keyClient]['clientMob'] = $valueClient['CLIENT_MOB'];
                $clientDetail[$keyClient]['clientAddress'] = $valueClient['CLIENT_ADDRESS'];
            }
        } else {
            $clientDetail = NORECORDS;
        }

        //print_r($fetch_roledetails); exit;
        // START GETTING LISTING OF ASSIGN ORG
        $orgIds = array();
        $viewJson = array();
        $m = 0;
        foreach ($fetch_roledetails as $key => $h) {
            $explodedOrgId = explode(",", $h['PK_ORG_ID']);
            for ($d = 0; $d < count($explodedOrgId); $d++) {
                $orgIds[] = $explodedOrgId[$d];
            }

            if (strtolower($h['ACTIVITY_NAME']) == 'view') {
                $viewJson[$m]['MODULE_ID'] = $h['PK_MODULE_ID'];
                $viewJson[$m]['ACTIVITY_ID'] = $h['PK_ACTIVITY_ID'];
                $m++;
            }
        }
        //print_r($viewJson); exit;
        $uniquePids = array_unique($orgIds);
        //print_r($uniquePids); exit;
        $orgListing = array();
        if ($fetch_userdetails[0]['PK_USER_ID'] == "-1") {
//            $sqlOrgMaster = "SELECT * FROM ".ORGMASTER." OM WHERE DELETE_FLAG = 0";
            $sqlOrgMaster = "SELECT OM.ORGNAME,OM.PK_ORG_ID,OM.ORG_TYPE,OM.ORG_ADDRESS,OM.FK_CLIENT_ID,OM.ORG_LOGO FROM " . ORGMASTER . " OM WHERE DELETE_FLAG = 0";
            $resultOrgMaster = fetch_rec_query($sqlOrgMaster);
            if (count($resultOrgMaster) > 0) {

//                for($w=0;$w<count($resultOrgMaster);$w++){
                foreach ($resultOrgMaster as $keyOrg => $valueOrg) {

                    $orgListing[$keyOrg]['orgName'] = $valueOrg['ORGNAME'];
                    $orgListing[$keyOrg]['orgId'] = $valueOrg['PK_ORG_ID'];
                    $orgListing[$keyOrg]['orgType'] = $valueOrg['ORG_TYPE'];
                    $orgListing[$keyOrg]['orgAddress'] = $valueOrg['ORG_ADDRESS'];
                    $orgListing[$keyOrg]['clientId'] = $valueOrg['FK_CLIENT_ID'];
                    $orgListing[$keyOrg]['clientLogo'] = $valueOrg['ORG_LOGO'];
                }
            } else {
                $orgListing = NORECORDS;
            }
        } else {
//            for($o=0;$o<count($uniquePids);$o++){
            if (count($uniquePids) > 0) {
                foreach ($uniquePids as $keyUniquePids => $valueuniquePids) {
                    //                $sqlOrgMaster = "SELECT * FROM ".ORGMASTER." OM WHERE OM.PK_ORG_ID = '".$uniquePids[$o]."' AND DELETE_FLAG = 0 LIMIT 0,1";
                    $sqlOrgMaster = "SELECT OM.ORGNAME,OM.PK_ORG_ID,OM.ORG_TYPE,OM.ORG_ADDRESS,OM.FK_CLIENT_ID,OM.ORG_LOGO FROM " . ORGMASTER . " OM WHERE OM.PK_ORG_ID = '" . $valueuniquePids . "' AND DELETE_FLAG = 0 LIMIT 0,1";
                    $resultOrgMaster = fetch_rec_query($sqlOrgMaster);

                    if (count($resultOrgMaster) > 0) {
                        $orgListing[$keyUniquePids]['orgName'] = $resultOrgMaster[0]['ORGNAME'];
                        $orgListing[$keyUniquePids]['orgId'] = $resultOrgMaster[0]['PK_ORG_ID'];
                        $orgListing[$keyUniquePids]['orgType'] = $resultOrgMaster[0]['ORG_TYPE'];
                        $orgListing[$keyUniquePids]['orgAddress'] = $resultOrgMaster[0]['ORG_ADDRESS'];
                        $orgListing[$keyUniquePids]['clientId'] = $resultOrgMaster[0]['FK_CLIENT_ID'];
                        $orgListing[$keyUniquePids]['clientLogo'] = $resultOrgMaster[0]['ORG_LOGO'];
                    } else {
                        $orgListing = NORECORDS;
                    }
                }
            }
        }

        //query for getting bank name  by client wise from client_lookup
	//ADDED BY KAUSHA SHAH ON 17-08-2018
	$query_bankList = "SELECT PK_LOOKUP_ID ID, VALUE,PRIORITY FROM ".LOOKUPMASTER." WHERE TYPE = 'BANK' AND DELETE_FLAG = 0 ORDER BY PK_LOOKUP_ID ASC" ;
	
        $fetch_bankList = fetch_rec_query($query_bankList);

        //query for getting suspenssion and termination reason by client wise from client_lookup
        $query_suspenssionTerminationReason = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'SUSPENSION_TERMINATION_REASON' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_suspenssionTerminationReason = fetch_rec_query($query_suspenssionTerminationReason);

        //query for getting department name by client wise from client_lookup
	//COMMENTED BY KAUSHA SHAH ON 17-08-2018
        $query_department = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'DEPARTMENT' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        
	//ADDED BY KAUSHA SHAH ON 17-08-2018
	$query_department = "SELECT PK_LOOKUP_ID ID, VALUE,PRIORITY FROM ".LOOKUPMASTER." WHERE TYPE = 'DEPARTMENT' AND DELETE_FLAG = 0 ORDER BY VALUE DESC" ;
	$fetch_department = fetch_rec_query($query_department);

	
        //query for getting student relation by client wise from client_lookup
        $query_StudentRelation = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'STUDENT_RELATION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_StudentRelation = fetch_rec_query($query_StudentRelation);

        //query for getting student medical condtion by client wise from client_lookup
        $query_MedicalCondition = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'MEDICAL_CONDITION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_MedicalCondition = fetch_rec_query($query_MedicalCondition);

        //query for getting property booking status from lookup_master
	// ADDED BY KAUSHA SHAH ON 17-08-2018
        $query_BookingStatusType = "SELECT PK_STATUS_ID ID, STATUS_NAME VALUE, PRIORITY_LEVEL PRIORITY FROM ".STATUSMANAGEMENT." WHERE DELETE_FLAG = 0 AND FK_CATEGORY_ID = (SELECT PK_LOOKUP_ID FROM ".LOOKUPMASTER." WHERE VALUE = 'BOOKING' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$clientid."') ORDER BY PK_STATUS_ID ASC";
        $fetch_BookingStatusType = fetch_rec_query($query_BookingStatusType);

        //query for getting Property status by client wise from client_lookup
	$query_PropertyStatusType = "SELECT PK_STATUS_ID ID, STATUS_NAME VALUE, PRIORITY_LEVEL PRIORITY FROM ".STATUSMANAGEMENT." WHERE DELETE_FLAG = 0 AND FK_CATEGORY_ID = (SELECT PK_LOOKUP_ID FROM ".LOOKUPMASTER." WHERE VALUE = 'PROPORTY' AND DELETE_FLAG = 0 AND FK_CLIENT_ID = '".$clientid."') ORDER BY PK_STATUS_ID ASC";
        $fetch_PropertyStatusType = fetch_rec_query($query_PropertyStatusType);


        //query for getting ASSET by client wise from client_lookup
        $query_AssetType = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'ASSET' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_AssetType = fetch_rec_query($query_AssetType);

        //query for getting  by client wise from client_lookup
	$query_CategoryType = "SELECT PK_LOOKUP_ID ID, VALUE,PRIORITY FROM ".LOOKUPMASTER." WHERE TYPE = 'ITEM_SUB_TYPE' AND DELETE_FLAG = 0 ORDER BY PK_LOOKUP_ID ASC" ;
        $fetch_CategoryType = fetch_rec_query($query_CategoryType);

        //query for getting DESIGNATION by client wise from client_lookup
        $query_Designation = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE, ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'DESIGNATION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0'";
        $fetch_DesignationData = fetch_rec_query($query_Designation);


        //query for getting CUSTOMER TYPE from lookup_master
        $query_customertype = "SELECT PK_LOOKUP_ID ID,VALUE FROM " . LOOKUPMASTER . " WHERE TYPE = 'CUSTOMERTYPE' AND DELETE_FLAG = '0'";
        $fetch_customertype = fetch_rec_query($query_customertype);

        //query for getting CLIENT TYPE from lookup_master
        $queryClientType = "SELECT PK_LOOKUP_ID ID,VALUE FROM " . LOOKUPMASTER . " WHERE TYPE = 'CLIENT_TYPE' AND DELETE_FLAG = '0'";
        $fetchClientType = fetch_rec_query($queryClientType);

        //Edited by nikunj 4/7/2016-start
        $query_ethinicityId = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'ETHNICITY' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG='1' LIMIT 0,1";
        $fetch_ethinicityId = fetch_rec_query($query_ethinicityId);
        $ethinicityId = !empty($fetch_ethinicityId[0]['ID']) ? $fetch_ethinicityId[0]['ID'] : '0';

        $query_occupationId = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'OCCUPATION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG='1' LIMIT 0,1";
        $fetch_occupationId = fetch_rec_query($query_occupationId);
        $occupationId = !empty($fetch_occupationId[0]['ID']) ? $fetch_occupationId[0]['ID'] : '0';

        $query_InqTypeId = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'INQTYPE' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG='1' LIMIT 0,1";
        $fetch_InqTypeId = fetch_rec_query($query_InqTypeId);
        $inqTypeId = !empty($fetch_InqTypeId[0]['ID']) ? $fetch_InqTypeId[0]['ID'] : '0';

        $query_sorceOff = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'SOURCEOF' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG='1' LIMIT 0,1";
        $fetch_sorceOff = fetch_rec_query($query_sorceOff);
        $sorceOffId = !empty($fetch_sorceOff[0]['ID']) ? $fetch_sorceOff[0]['ID'] : '0';

        $query_designation = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'DESIGNATION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG='1' LIMIT 0,1";
        $fetch_designation = fetch_rec_query($query_designation);
        $designationId = !empty($fetch_designation[0]['ID']) ? $fetch_designation[0]['ID'] : '0';

        $query_ocassion = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'OCCASSIONTYPE' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' AND ISDEFAULTFLAG='1' LIMIT 0,1";
        $fetch_ocassion = fetch_rec_query($query_ocassion);
        $ocassionId = !empty($fetch_ocassion[0]['ID']) ? $fetch_ocassion[0]['ID'] : '0';

        //query for getting PAYMENT_TYPE by client wise from client_lookup
        $query_PaymentType = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'PAYMENT_TYPE' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_PaymentType = fetch_rec_query($query_PaymentType);

        //query for getting PAYMENT_MODE by client wise from client_lookup
        $query_PaymentMode = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE VALUE ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientid . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'PAYMENT_MODE' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientid . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
        $fetch_PaymentMode = fetch_rec_query($query_PaymentMode);



        $query_CustomerTypeFlag = "SELECT PK_LOOKUP_ID ID,VALUE FROM " . LOOKUPMASTER . " WHERE TYPE = 'CUST_TYPE' AND DELETE_FLAG = '0'";
        $fetch_CustomerTypeFlag = fetch_rec_query($query_CustomerTypeFlag);


        //GET COUNTY LISTING
        $sqlCountryListing = "SELECT PK_COUNTRY_ID,COUNTRY_SHORT_CODE,COUNTRY_NAME,COUNTRY_PHONE_CODE FROM " . COUNTRY . "";
        $resultCountryListing = fetch_rec_query($sqlCountryListing);
        if (count($resultCountryListing) > 0) {

            $finalCountry = array();
            foreach ($resultCountryListing as $keyCountry => $valueCountry) {

                $finalCountry[$keyCountry]['countryId'] = $valueCountry['PK_COUNTRY_ID'];
                $finalCountry[$keyCountry]['countryShortCode'] = $valueCountry['COUNTRY_SHORT_CODE'];
                $finalCountry[$keyCountry]['countryName'] = $valueCountry['COUNTRY_NAME'];
                $finalCountry[$keyCountry]['countryPhoneCode'] = $valueCountry['COUNTRY_PHONE_CODE'];

                // GET CITY COUNT FOR MOBILE APP
                $sqlCities = "SELECT COUNT(PK_CITY_ID) TOTALCITIES FROM " . CITY . " CM WHERE CM.FK_COUNTRY_ID = '" . $valueCountry['PK_COUNTRY_ID'] . "' ";
                $resultGetCitiesCount = fetch_rec_query($sqlCities);
                $finalCountry[$keyCountry]['totalCitiesCount'] = $resultGetCitiesCount[0]['TOTALCITIES'];
            }
        }
        
        // START GETTING CLIENT LEVEL SETTING 
        $sqlGetConfig = "SELECT MSG.SMS_FLAG,MSG.EMAIL_FLAG,MSG.DEVELOPER_SMS_FLAG,MSG.API_LOG_FLAG,MSG.TALLY_INTEGRATION_FLAG,MSG.TALLY_FLAG_ID FROM " . MAILSMSCONFIG . " MSG WHERE MSG.FK_CLIENT_ID = '" . $clientid . "' AND DELETE_FLAG = '0' LIMIT 0,1 ";
        $resultGetConfig = fetch_rec_query($sqlGetConfig);
//        if(count($resultGetConfig)>0){
        // $getFlagSms = checkFlagOnClient($clientid, '1', 'createInquiryUser', $orgId); //SMS FLAG ON INQ CREATE
        $smsFlag = !empty($resultGetConfig[0]['SMS_FLAG']) ? $resultGetConfig[0]['SMS_FLAG'] : "0";
        $emailFlag = !empty($resultGetConfig[0]['EMAIL_FLAG']) ? $resultGetConfig[0]['EMAIL_FLAG'] : "0";
        $developerSmsFlag = !empty($resultGetConfig[0]['DEVELOPER_SMS_FLAG']) ? $resultGetConfig[0]['DEVELOPER_SMS_FLAG'] : "0";
        $apiLogFlag = !empty($resultGetConfig[0]['API_LOG_FLAG']) ? $resultGetConfig[0]['API_LOG_FLAG'] : "0";
        $tallyIntegrationFlag = !empty($resultGetConfig[0]['TALLY_INTEGRATION_FLAG']) ? $resultGetConfig[0]['TALLY_INTEGRATION_FLAG'] : "0";
        $tallyFlagId = !empty($resultGetConfig[0]['TALLY_FLAG_ID']) ? $resultGetConfig[0]['TALLY_FLAG_ID'] : "0";

        $versionViewed = isset($fetch_userdetails[0]['NEW_VERSION_FLAG']) ? $fetch_userdetails[0]['NEW_VERSION_FLAG'] : "";
        $changePassword = isset($fetch_userdetails[0]['CHANGE_PASSWORD_FLAG']) ? $fetch_userdetails[0]['CHANGE_PASSWORD_FLAG'] : "";
        $clientValidity = isset($fetch_userdetails[0]['CLIENT_VAL_END_DATE']) ? $fetch_userdetails[0]['CLIENT_VAL_END_DATE'] : "";

        // Client Wise Time Zone
        $sqlTimezone = "SELECT PK_TIMEZONE_ID,TIMEZONENAME,FK_CLIENT_ID FROM " . TIMEZONEMASTER . " MSG WHERE MSG.FK_CLIENT_ID = '" . $clientid . "' AND DELETE_FLAG = '0' LIMIT 0,1 ";
        $resultTimezone = fetch_rec_query($sqlTimezone);

        //ADD BY DEVIKA NONAVAILING SERVICE.16.8.2018.
        $query_NonAvalingService = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_TYPE FROM " . ITEMMASTER . " WHERE ITEM_TYPE='SUBSC' AND ITEM_NAME='Meals' AND FK_CLIENT_ID = '" . $clientid . "' AND DELETE_FLAG = '0' ORDER BY PK_ITEM_ID ASC";
        $fetch_NonAvalingService = fetch_rec_query($query_NonAvalingService);
        
	
	// ADDED BY KAUSHA SHAH ONBEHALF OF DEEPAK PATIL ON 17-08-2018 START
	// $sqlGetDistinctStatus= "SELECT * FROM ".LOOKUPMASTER." LM WHERE TYPE = 'STATUSCATEGORY' AND DELETE_FLAG =0";
        $sqlGetDistinctStatus= "SELECT LM.VALUE,LM.PK_LOOKUP_ID FROM ".LOOKUPMASTER." LM WHERE TYPE = 'STATUSCATEGORY' AND DELETE_FLAG =0";
        $resultGetDistinctStatus = fetch_rec_query($sqlGetDistinctStatus);
        if(count($resultGetDistinctStatus) > 0){
            
//            for($e=0;$e<count($resultGetDistinctStatus);$e++){
            foreach ($resultGetDistinctStatus as $keyStatus => $valueStatus) {
                
                $statusListing[$keyStatus]['statusCatName'] = $valueStatus['VALUE'];
                $statusListing[$keyStatus]['statusCatId'] = $valueStatus['PK_LOOKUP_ID'];
                
                $sqlStatus = "SELECT PK_STATUS_ID,STATUS_NAME,PRIORITY_LEVEL,THRID_PARTY_FLAG,CANCEL_REMARK_FLAG,APPROVAL_FLAG,FK_USER_ID FROM ".STATUSMANAGEMENT." ST WHERE ST.FK_CLIENT_ID = '".$clientid."' AND ST.DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$valueStatus['PK_LOOKUP_ID']."'";
                $resultStatus = fetch_rec_query($sqlStatus);
                
                if(count($resultStatus) > 0){                  
                    $tempStatus = array();
//                    for($r=0;$r<count($resultStatus);$r++){
                    foreach ($resultStatus as $keyS => $valueS) {
                        
                        $tempStatus[$keyS]['ID'] = $valueS['PK_STATUS_ID']; 
                        $tempStatus[$keyS]['VALUE'] = $valueS['STATUS_NAME']; 
                        $tempStatus[$keyS]['statusPriority'] = $valueS['PRIORITY_LEVEL']; 
                        $tempStatus[$keyS]['thirdPartyFlag'] = $valueS['THRID_PARTY_FLAG']; 
                        $tempStatus[$keyS]['remarkOnStatusFlag'] = $valueS['CANCEL_REMARK_FLAG']; 
                        $tempStatus[$keyS]['approvalFlag'] = $valueS['APPROVAL_FLAG']; 
                        $tempStatus[$keyS]['assUserId'] = !empty($valueS['FK_USER_ID']) ? $valueS['FK_USER_ID'] : '0'; 
                    }
                    
                    $statusListing[$keyStatus]['statusListing'] = array_values($tempStatus);
                }else{
                    $statusListing[$keyStatus]['statusListing'] = NORECORDS;
                }
            }
        }else{
            $statusListing = NORECORDS;
        }
	// ADDED BY KAUSHA SHAH ONBEHALF OF DEEPAK PATIL ON 17-08-2018 START
        
                //GET ALL DAHBORD GRID MASTER DATA ADDED BY DARSHAN PATEL ON 3/11/2017
        $sqlGetGrid = "SELECT PK_GRID_ID ID , GRID_NAME VALUE , FUNCTON_NAME FUN_NAME FROM ".DASHBORDGRIDMASTER." WHERE DELETE_FLAG = '0' AND STATUS = '1' ";
        $resultGetGrid = fetch_rec_query($sqlGetGrid);
        
        //GET DAHBORD GRID MASTER DATA USER WISE ADDED BY DARSHAN PATEL ON 3/11/2017
        $sqlGridUserWise = "SELECT DGT.FK_GRID_ID ID, DM.GRID_NAME VALUE , DM.FUNCTON_NAME FUN_NAME FROM ".DASHBORDGRIDTRANS." DGT INNER JOIN ".DASHBORDGRIDMASTER." DM ON DM.PK_GRID_ID = DGT.FK_GRID_ID WHERE DGT.DELETE_FLAG = '0' AND DGT.GRID_STATUS = '1' AND DGT.FK_USER_ID = '".$sessionuserid."' AND DGT.FK_CLIENT_ID = '".$clientid."' AND DM.DELETE_FLAG = '0' ";
        $resultGridUserWise = fetch_rec_query($sqlGridUserWise);

        //create array for JSON data
        $data = array("PK_USER_ID" => $fetch_userdetails[0]['PK_USER_ID'], "FK_CLIENT_ID" => $fetch_userdetails[0]['FK_CLIENT_ID'], "USERNAME" => $fetch_userdetails[0]['USERNAME'], "FULLNAME" => $fetch_userdetails[0]['FULLNAME'], "USERTYPE" => $fetch_userdetails[0]['USER_TYPE'], "permission" => $fetch_roledetails, "assetType" => $fetch_AssetType, "inqBookingStatus" => $fetch_BookingStatusType, "propertyStatus" => $fetch_PropertyStatusType, "customertype" => $fetch_customertype, "branchListing" => $orgListing, "clientListing" => $clientDetail, "designation" => $fetch_DesignationData, "userAccessData" => $viewJson, "clientType" => $fetchClientType, "versionViewed" => $versionViewed, "paymentType" => $fetch_PaymentType, "paymentMode" => $fetch_PaymentMode, "clientValidity" => $clientValidity, "customerTypeFlag" => $fetch_CustomerTypeFlag, "changePassword" => $changePassword, "countryListing" => $finalCountry, "apiLogFlag" => $apiLogFlag, "smsFlag" => $smsFlag, "emailFlag" => $emailFlag, "developerSmsFlag" => $developerSmsFlag, "tallyIntegrationFlag" => $tallyIntegrationFlag, 'tallyFlagId' => $tallyFlagId, "timezone" => $resultTimezone, "categoryType" => $fetch_CategoryType, "studentRelation" => $fetch_StudentRelation, "medicalCondition" => $fetch_MedicalCondition, "departmentList" => $fetch_department, "suspenssionTerminationReason" => $fetch_suspenssionTerminationReason, "bankName" => $fetch_bankList, "NonAvailingService" => $fetch_NonAvalingService,"dashbordGridList"=>$resultGetGrid,"userActiveGridList"=>$resultGridUserWise,"statusListing"=>$statusListing);
        $result = array("status" => SCS, "data" => $data);
        http_response_code(200);
    } else {
        $result = array("status" => NOAUTH);
        http_response_code(400);
    }
    return $result;
}

function getUserLevelForUserManagement($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $levelFlag = "1";
    $finalJson = array();
    // START GETTING USER LISTING
    // START GETTING USER LEVEL
    $postData['postData']['levelFlag'] = $levelFlag;
    $getLevel = getUserLevel($postData);
    if ($getLevel['status'] == SCS) {
        $finalJson['userLevel'] = $getLevel['data'];
    } else {
        $finalJson['userLevel'] = $getLevel['status'];
    }

    // START GETTING BRANCH LISTING
    $branchListing = branchListing($postData);
    if ($branchListing['status'] == SCS) {
        $finalJson['branchListing'] = $branchListing['data'];
    } else {
        $finalJson['branchListing'] = $branchListing['status'];
    }

    // START GETTING ROLE 
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['userId'] = $userId;
    $requestData['postData']['orgId'] = $orgId;
    $requestData['postData']['requestCase'] = "getrole";
    $userRole = getRoleDetails_BL($requestData);
    if ($userRole['status'] == SCS) {
        $finalJson['userRole'] = $userRole['data'];
    } else {
        $finalJson['userRole'] = $userRole['status'];
    }

    if (count($finalJson) > 0) {
        $result = array("status" => SCS, "data" => $finalJson);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getUserDetails_BL($postData) {
//    print_r($postdata); die;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['requestedUserId']) ? addslashes(trim($postData['postData']['requestedUserId'])) : "";
    $requestCase = isset($postData['postData']['requestCase']) ? addslashes(trim($postData['postData']['requestCase'])) : ""; //request case
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : ""; //request case
    $sessionuserId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : ""; //request case
    $googleCalKey = isset($postData['postData']['googleCalKey']) ? addslashes(trim($postData['postData']['googleCalKey'])) : ""; //google cal link

    if ($requestCase == "userlisting") {

        $query_User = "SELECT UM.PK_USER_ID, UM.FK_CLIENT_ID,UM.FULLNAME,UM.EMAIL,UM.CONTACT_NO,UM.USERNAME,UM.USER_TYPE,CM.CLIENTNAME,UM.DEFAULT_DAS FROM " . USERMASTER . " UM INNER JOIN " . CLIENTMASTER . " CM ON CM.PK_CLIENT_ID = UM.FK_CLIENT_ID WHERE `FK_CLIENT_ID` ='" . $clientId . "' AND UM.DELETE_FLAG = 0 AND UM.`USER_TYPE` = '1' AND UM.PK_USER_ID != '" . $sessionuserId . "'";
        $fetch_userdetails = fetch_rec_query($query_User);

        if (count($fetch_userdetails) > 0) {
            if (empty($fetch_userdetails[0]['DEFAULT_DASH'])) {
                $fetch_userdetails[0]['DEFAULT_DASH'] = "V1.0";
            }

            $result = array("status" => SCS, "data" => $fetch_userdetails);
            http_response_code(200);
        } else {
            $result = array("status" => USRNTFND);
            http_response_code(400);
        }
    } else if (isset($userId) && $requestCase == "userdetails") {

        $query_User = "SELECT UM.USER_LEVEL,UM.FK_SUPERVISOR_ID,UM.FK_DESIGNATION_ID,UM.USER_G_CAL_ID,UM.`PK_USER_ID`, UM.`FK_CLIENT_ID`,UM.`FULLNAME`,UM.`EMAIL`,UM.`CONTACT_NO`,UM.`USERNAME`,UM.`USER_TYPE`,UM.`ADDRESS`,CM.`CLIENTNAME`,UM.DEFAULT_DASH,UM.BIRTHDATE,UM.ANNIVERSARYDATE,UM.FK_DEPARTMENT_ID,UM.GENDER,(SELECT FULLNAME FROM " . USERMASTER . " UM1 WHERE UM1.PK_USER_ID = UM.FK_SUPERVISOR_ID AND UM1.DELETE_FLAG = 0 AND UM1.FK_CLIENT_ID = '" . $clientId . "') SUPERVISORNAME FROM " . USERMASTER . " UM INNER JOIN " . CLIENTMASTER . " CM ON CM.`PK_CLIENT_ID` = UM.`FK_CLIENT_ID` WHERE `PK_USER_ID` ='" . $userId . "' AND `FK_CLIENT_ID` ='" . $clientId . "' AND UM.`DELETE_FLAG` = 0 AND UM.`USER_TYPE` != '-1'";
        $fetch_userdetails = fetch_rec_query($query_User);
        //print_r($fetch_userdetails); exit;
        if (count($fetch_userdetails) > 0) {

            $sqlGetUniqueRole = "SELECT GROUP_CONCAT(DISTINCT URAM.FK_ROLE_ID) FK_ROLE_ID ,GROUP_CONCAT(DISTINCT URAM.FK_ORG_ID) FK_ORG_ID FROM " . USERROLEACCESSMASTER . " URAM INNER JOIN " . ROLEMASTER . " RM ON RM.PK_ROLE_ID = URAM.FK_ROLE_ID WHERE URAM.FK_USER_ID = '" . $userId . "' AND URAM.DELETE_FLAG = 0 GROUP BY URAM.FK_ROLE_ID ";
            $fechGetUniqueRole = fetch_rec_query($sqlGetUniqueRole);
            if (count($fechGetUniqueRole) > 0) {
                $finalRoleJson['roleIds'] = $fechGetUniqueRole[0]['FK_ROLE_ID'];
                $finalRoleJson['orgIds'] = $fechGetUniqueRole[0]['FK_ORG_ID'];
            } else {
                $finalRoleJson = NORECORDS;
            }

            $data['PK_USER_ID'] = $fetch_userdetails[0]['PK_USER_ID'];
            $data['FK_CLIENT_ID'] = $fetch_userdetails[0]['FK_CLIENT_ID'];
            $data['FULLNAME'] = $fetch_userdetails[0]['FULLNAME'];
            $data['EMAIL'] = $fetch_userdetails[0]['EMAIL'];
            $data['CONTACT_NO'] = $fetch_userdetails[0]['CONTACT_NO'];
            $data['USERNAME'] = $fetch_userdetails[0]['USERNAME'];
            $data['USER_TYPE'] = $fetch_userdetails[0]['USER_TYPE'];
            $data['USER_LEVELID'] = $fetch_userdetails[0]['USER_LEVEL'];
            $data['FK_SUPERVISOR_ID'] = $fetch_userdetails[0]['FK_SUPERVISOR_ID'];
            $data['SUPERVISORNAME'] = $fetch_userdetails[0]['SUPERVISORNAME'];
            $data['FK_DESIGNATION_ID'] = $fetch_userdetails[0]['FK_DESIGNATION_ID'];
            $data['USER_G_CAL_ID'] = $fetch_userdetails[0]['USER_G_CAL_ID'];
            $data['ADDRESS'] = $fetch_userdetails[0]['ADDRESS'];
            $data['CLIENTNAME'] = $fetch_userdetails[0]['CLIENTNAME'];
            $data['BIRTHDATE'] = $fetch_userdetails[0]['BIRTHDATE'];
            $data['ANNIVERSARYDATE'] = $fetch_userdetails[0]['ANNIVERSARYDATE'];
            $data['GENDER'] = $fetch_userdetails[0]['GENDER'];
            $data['FK_DEPARTMENT_ID'] = $fetch_userdetails[0]['FK_DEPARTMENT_ID'];

            if (empty($fetch_userdetails[0]['DEFAULT_DASH'])) {
                $data['DEFAULT_DASH'] = "V1.0";
            } else {
                $data['DEFAULT_DASH'] = $fetch_userdetails[0]['DEFAULT_DASH'];
            }

            //$data['permission'] = $fetch_roledetails;
            $data = array($data, "permission" => $finalRoleJson);

            $result = array("status" => SCS, "data" => $data);
            http_response_code(200);
        } else {
            $result = array("status" => USRNTFND);
            http_response_code(400);
        }
    }

    return $result;
}

function getStateCity($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $countryId = isset($postData['postData']['countryId']) ? addslashes(trim($postData['postData']['countryId'])) : "";
    $stateId = isset($postData['postData']['stateId']) ? addslashes(trim($postData['postData']['stateId'])) : "";

    $whereState = "";
    if (!empty($stateId) && $stateId != "") {
        $whereState = " AND PK_STATE_ID = '" . $stateId . "' ";
    }

    $sqlGetState = "SELECT STATE_NAME,FK_COUNTRY_ID,PK_STATE_ID FROM " . STATE . " WHERE FK_COUNTRY_ID = '" . $countryId . "' " . $whereState . " ";
    $resultState = fetch_rec_query($sqlGetState);
    if ($resultState) {
        $finalJsonState = array();
        foreach ($resultState as $keyState => $valueState) {
            $finalJsonState[$keyState]['stateId'] = $valueState['PK_STATE_ID'];
            $finalJsonState[$keyState]['stateName'] = $valueState['STATE_NAME'];
            $finalJsonState[$keyState]['countryId'] = $valueState['FK_COUNTRY_ID'];

            $sqlGetCity = "SELECT CITY_NAME,FK_STATE_ID,PK_CITY_ID,LATITUDE,LONGITUDE FROM " . CITY . " WHERE FK_STATE_ID = '" . $valueState['PK_STATE_ID'] . "' ";
            $resultCity = fetch_rec_query($sqlGetCity);
            if ($resultCity) {

                $finalJsonCity = array();
                foreach ($resultCity as $keyCity => $valueCity) {
                    $finalJsonCity[$keyCity]['cityId'] = $valueCity['PK_CITY_ID'];
                    $finalJsonCity[$keyCity]['cityName'] = $valueCity['CITY_NAME'];
                    $finalJsonCity[$keyCity]['stateId'] = $valueCity['FK_STATE_ID'];
                    $finalJsonCity[$keyCity]['latitude'] = $valueCity['LATITUDE'];
                    $finalJsonCity[$keyCity]['longitude'] = $valueCity['LONGITUDE'];
                }

                $finalJsonState[$keyState]['cityData'] = array_values($finalJsonCity);
            } else {
                $finalJsonState[$keyState]['cityData'] = NORECORDS;
            }
        }
        $result = array("status" => SCS, "data" => $finalJsonState);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR USER ACTIVE / IN ACTIVE
function userActiveDeActive($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";
    $selectedUserId = isset($postData['postData']['selectedUserId']) ? addslashes(trim($postData['postData']['selectedUserId'])) : "";


    if (!empty($clientId) && !empty($orgId) && $status != "" && !empty($selectedUserId)) {
        $today = date("Y-m-d");
        // UPDATE STATUS IN USER_MASTER  TABLE
        $update_user_status['IS_ACTIVE'] = $status;
        if ($status == 0) {
            $update_user_status['INACTIVE_DATE'] = $today;
        }
        $update_user_status['CHANGED_BY'] = $userId;
        $update_user_status['CHANGED_BY_DATE'] = time();

        $where_userstatus = "`PK_USER_ID` = '" . $selectedUserId . "' AND FK_CLIENT_ID='" . $clientId . "'";
        $update_userstatus = update_rec(USERMASTER, $update_user_status, $where_userstatus);

        if ($update_userstatus) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . USERMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR USER ACTIVE / IN ACTIVE
function addComplaintComment($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $complaintId = isset($postData['postData']['complaintId']) ? addslashes(trim($postData['postData']['complaintId'])) : "";
    $comment = isset($postData['postData']['comment']) ? addslashes(trim($postData['postData']['comment'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($comment) && !empty($complaintId)) {
        $insertComment = array();
        $insertComment['FK_COMPLAINT_ID'] = $complaintId;
        $insertComment['FK_CLIENT_ID'] = $clientId;
        $insertComment['FK_ORG_ID'] = $orgId;
        $insertComment['DESCRIPTION'] = $comment;
        if (!empty($studentId)) {
            $insertComment['REF_ID'] = $studentId;
            $insertComment['TYPE'] = 'STUDENT';
        } else {
            $insertComment['REF_ID'] = $userId;
            $insertComment['TYPE'] = 'STAFF';
        }

        $insertComment['CREATED_BY'] = $userId;
        $insertComment['CREATED_BY_DATE'] = time();
        $insertComment['DELETE_FLAG'] = 0;
        $insertRecCom = insert_rec(COMPLAINTTRANS, $insertComment);
        if ($insertRecCom) {
            $finalJson = array("FK_TRANS_ID" => $insertRecCom['lastInsertedId'], "DESCRIPTION" => $insertComment['DESCRIPTION']);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        } else {
            $result = array("status" => INSERTQUERYFAIL . " " . COMPLAINTTRANS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function getDashboardData($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? trim($postData['postData']['userId']) : ""; // USER ID
    $gridId = isset($postData['postData']['gridId']) ? addslashes(trim($postData['postData']['gridId'])) : "";

    
    $requestData = array();
    $requestData['postData']['clientId'] = $clientId;
    $requestData['postData']['orgId'] = $orgId;
    $requestData['postData']['userId'] = $userId;

    $getGridData = getDashbordGrid($requestData);
    //print_r($getGridData); exit;
    $finalActiveJson = array();
    $exlpodeId = explode(",", $gridId);
    foreach ($exlpodeId as $keyId => $valueId) {
        $requestIndex = findArrByFieldValue($getGridData['data'], "gridId", $valueId);
        $requestCase = $getGridData['data'][$requestIndex]['funName'];
        
        switch ($requestCase) {
        
            case "revenueDashboardData":  // DASHBORDKPI
                $result = revenueDashboardData($requestData);
//                print_r($result);exit;
                $finalActiveJson[$keyId][$requestCase] = ($result['status'] == SCS) ? $result['data'] : NORECORDS;
                $finalActiveJson[$keyId]["keyName"] = $requestCase;
                unset($getGridData['data'][$requestIndex]);
                break;
            
            case "enrollmentDashboardData":  // DASHBORDKPI
                $result = enrollmentDashboardData($requestData);
//                print_r($result);exit;
                $finalActiveJson[$keyId][$requestCase] = ($result['status'] == SCS) ? $result['data'] : NORECORDS;
                $finalActiveJson[$keyId]["keyName"] = $requestCase;
                unset($getGridData['data'][$requestIndex]);
                break;
            
            case "requestComplaintDashboardData":  // DASHBORDKPI
                $result = requestComplaintDashboardData($requestData);
//                print_r($result);exit;
                $finalActiveJson[$keyId][$requestCase] = ($result['status'] == SCS) ? $result['data'] : NORECORDS;
                $finalActiveJson[$keyId]["keyName"] = $requestCase;
                unset($getGridData['data'][$requestIndex]);
                break;
            
            case "suspenssionDashboardData":  // DASHBORDKPI
                $result = suspenssionDashboardData($requestData);
//                print_r($result);exit;
                $finalActiveJson[$keyId][$requestCase] = ($result['status'] == SCS) ? $result['data'] : NORECORDS;
                $finalActiveJson[$keyId]["keyName"] = $requestCase;
                unset($getGridData['data'][$requestIndex]);
                break;
            
            case "propertyDashboardData":  // DASHBORDKPI
                $result = propertyDashboardData($requestData);
//                print_r($result);exit;
                $finalActiveJson[$keyId][$requestCase] = ($result['status'] == SCS) ? $result['data'] : NORECORDS;
                $finalActiveJson[$keyId]["keyName"] = $requestCase;
                unset($getGridData['data'][$requestIndex]);
                break;
            
            case "eventDashboardData":  // DASHBORDKPI
                $result = eventDashboardData($requestData);
//                print_r($result);exit;
                $finalActiveJson[$keyId][$requestCase] = ($result['status'] == SCS) ? $result['data'] : NORECORDS;
                $finalActiveJson[$keyId]["keyName"] = $requestCase;
                unset($getGridData['data'][$requestIndex]);
                break;
            
            default: // default case
                $result['status'] = CASEERROR;
                $jsonData = json_encode($result);
                break;
    }
    }
//    $dashboardArr = Array();
//   
//    $requestData = $revenueDashboardData = $enrollmentDashboardData = $requestComplaintDashboardData = $suspenssionDashboardData = $propertyDashboardData= Array();
//    
//    $requestData['postData']['clientId'] = $clientId;
//    $requestData['postData']['orgId'] = $orgId;
//    
//    $revenueDashboardData = revenueDashboardData($requestData); // GET ALL REVENUE DATA.
//    $enrollmentDashboardData = enrollmentDashboardData($requestData); // GET ALL ENROLLMENT DATA.
//    $requestComplaintDashboardData = requestComplaintDashboardData($requestData); // GET ALL REQUEST COMPLAINT DATA.
//    $suspenssionDashboardData = suspenssionDashboardData($requestData); // GET ALL REQUEST COMPLAINT DATA.
//    $propertyDashboardData =propertyDashboardData($postData); //  GET ALL PROPERTY DATA
//    $eventDashboardData =eventDashboardData($postData);//  GET ALL EVENT DATA
//    
//    $dashboardArr['revenueDashboardData'] =( $revenueDashboardData['status']== SCS)?$revenueDashboardData['data']:$revenueDashboardData['status'];
//    
//    $dashboardArr['enrollmentDashboardData'] =( $enrollmentDashboardData['status']== SCS)?$enrollmentDashboardData['data']:$enrollmentDashboardData['status'];
//    
//    $dashboardArr['requestComplaintDashboardData'] =( $requestComplaintDashboardData['status']== SCS)?$requestComplaintDashboardData['data']:$requestComplaintDashboardData['status'];
//    
//    $dashboardArr['suspenssionDashboardData'] =( $suspenssionDashboardData['status']== SCS)?$suspenssionDashboardData['data']:$suspenssionDashboardData['status'];
//    
//    $dashboardArr['propertyDashboardData'] =( $propertyDashboardData['status']== SCS)?$propertyDashboardData['data']:$propertyDashboardData['status'];
//    
//    $dashboardArr['eventDashboardData'] =( $eventDashboardData['status']== SCS)?$eventDashboardData['data']:$eventDashboardData['status'];
    
    $result = array("status"=>SCS,"data"=>$finalActiveJson);
    http_response_code(200);
    return $result;
}

function getDashboardDataCopy($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $dashboardArr = Array();
    /* TOTAL REVENUE DATA START */

    // START CHANGES MADE BY DEEPAK PATIL ON 10-08-2018 FOR CHANGES INTO DASHBOARD REPORT
    //$totalEnrollmentAmt = $totalRenewalAmt = $totalMiscAmt = $totalEventAmt = $totalPropertyAmt = $totalGeneralAmt = 0;

    $sqlEnrollmentInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL,INVOICE_TYPE FROM " . INVOICEMASTER . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' GROUP BY INVOICE_TYPE";
    $resEnrollmentInvoiceData = fetch_rec_query($sqlEnrollmentInVoiceData);
    if (count($resEnrollmentInvoiceData) > 0) {
        $dashboardArr['revenueData'] = $resEnrollmentInvoiceData;
    } else {
        $dashboardArr['revenueData'] = NORECORDS;
    }

    //$totalEnrollmentAmt = $resEnrollmentInvoiceData[0]['TOTAL'];
//    $sqlRenewalInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL FROM " . INVOICEMASTER . " WHERE INVOICE_TYPE='RENEWALS' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resRenewalInvoiceData = fetch_rec_query($sqlRenewalInVoiceData);
//    $totalRenewalAmt = $resRenewalInvoiceData[0]['TOTAL'];
//
//
//    $sqlMiscInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL FROM " . INVOICEMASTER . " WHERE INVOICE_TYPE='MISC' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resMiscInvoiceData = fetch_rec_query($sqlMiscInVoiceData);
//    $totalMiscAmt = $resMiscInvoiceData[0]['TOTAL'];
//
//
//    $sqlEventInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL FROM " . INVOICEMASTER . " WHERE INVOICE_TYPE='EVENTS' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resEventInvoiceData = fetch_rec_query($sqlEventInVoiceData);
//    $totalEventAmt = $resEventInvoiceData[0]['TOTAL'];
//
//
//    $sqlPropertyInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL FROM " . INVOICEMASTER . " WHERE INVOICE_TYPE='PROPERTY' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resPropertyInvoiceData = fetch_rec_query($sqlPropertyInVoiceData);
//    $totalPropertyAmt = $resPropertyInvoiceData[0]['TOTAL'];
//
//
//    $sqlGeneralInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL FROM " . INVOICEMASTER . " WHERE INVOICE_TYPE='GENERAL' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resGeneralInvoiceData = fetch_rec_query($sqlGeneralInVoiceData);
//    $totalGeneralAmt = $resGeneralInvoiceData[0]['TOTAL'];
//    $invoiceArr['totalEnrollmentAmt'] = $totalEnrollmentAmt;
//    $invoiceArr['totalReEnrollmentAmt'] = $totalRenewalAmt;
//    $invoiceArr['totalMiscAmt'] = $totalMiscAmt;
//    $invoiceArr['totalEventAmt'] = $totalEventAmt;
//    $invoiceArr['totalPropertyAmt'] = $totalPropertyAmt;
//    $invoiceArr['totalGeneralAmt'] = $totalGeneralAmt;
//    $invoiceArr['totalRevenueAmt'] = $totalEnrollmentAmt + $totalRenewalAmt + $totalMiscAmt + $totalEventAmt + $totalPropertyAmt + $totalGeneralAmt;
    //$dashboardArr['revenueData'] = $invoiceArr;
    // END CHANGES MADE BY DEEPAK PATIL ON 10-08-2018
    /* TOTAL REVENUE DATA END */

    /* Total Enrollment Data Start */
    // START COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES
//    $totalNewGeneral = $totalNewHandicapped = $totalReNewGeneral = $totalReNewHandicapped = 0;
//
//    $sqlNewGeneralCount = "SELECT COUNT(PK_STUD_ID) COUNT FROM " . STUDENTMASTER . " WHERE ENROLLMENT_TYPE ='NEW' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."' AND MEDICAL_CONDITION  = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND DISPLAY_VALUE ='General')";
//    $resNewGeneralCount = fetch_rec_query($sqlNewGeneralCount);
//    $totalNewGeneral = $resNewGeneralCount[0]['COUNT'];
//
//    $sqlNewHandicappedCount = "SELECT COUNT(PK_STUD_ID) COUNT FROM " . STUDENTMASTER . " WHERE ENROLLMENT_TYPE ='NEW' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."' AND MEDICAL_CONDITION  = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND DISPLAY_VALUE ='Handicapped')";
//    $resNewHandicappedCount = fetch_rec_query($sqlNewHandicappedCount);
//    $totalNewHandicapped = $resNewHandicappedCount[0]['COUNT'];
//
//
//    $sqlReNewGeneralCount = "SELECT COUNT(PK_STUD_ID) COUNT FROM " . STUDENTMASTER . " WHERE ENROLLMENT_TYPE ='RENEW' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."' AND MEDICAL_CONDITION  = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND DISPLAY_VALUE ='General')";
//    $resReNewGeneralCount = fetch_rec_query($sqlReNewGeneralCount);
//    $totalReNewGeneral = $resReNewGeneralCount[0]['COUNT'];
//
//
//    $sqlReNewHandicappedCount = "SELECT COUNT(PK_STUD_ID) COUNT FROM " . STUDENTMASTER . " WHERE ENROLLMENT_TYPE ='RENEW' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."' AND MEDICAL_CONDITION  = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND DISPLAY_VALUE ='Handicapped')";
//    $resNewHandicappedCount = fetch_rec_query($sqlReNewHandicappedCount);
//    $totalReNewHandicapped = $resNewHandicappedCount[0]['COUNT'];

    $sqlEnrollementType = "SELECT ENROLLMENT_TYPE FROM " . STUDENTMASTER . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' GROUP BY ENROLLMENT_TYPE";
    $resultEnrollmentType = fetch_rec_query($sqlEnrollementType);

    if (count($resultEnrollmentType) > 0) {
        $finalJson = array();
        foreach ($resultEnrollmentType as $keyEnroll => $valueEnroll) {
            $finalJson[$keyEnroll]['enrollType'] = $valueEnroll['ENROLLMENT_TYPE'];
            $sqlGetCondition = "SELECT COUNT(ST.PK_STUD_ID) COUNT,ST.MEDICAL_CONDITION,CL.DISPLAY_VALUE FROM " . STUDENTMASTER . " ST INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID = ST.MEDICAL_CONDITION AND CL.DELETE_FLAG = '0' WHERE ENROLLMENT_TYPE ='" . $valueEnroll['ENROLLMENT_TYPE'] . "' AND ST.FK_CLIENT_ID = '" . $clientId . "' AND ST.FK_ORG_ID = '" . $orgId . "' GROUP BY ST.MEDICAL_CONDITION";
            $resultGetCondition = fetch_rec_query($sqlGetCondition);
            if (count($resultGetCondition) > 0) {
                $finalJson[$keyEnroll]['enrollTypeWiseData'] = $resultGetCondition;
            } else {
                $finalJson[$keyEnroll]['enrollTypeWiseData'] = NORECORDS;
            }
        }
        $dashboardArr['enrollmentData'] = array_values($finalJson);
    } else {
        $dashboardArr['enrollmentData'] = NORECORDS;
    }



//    $enrollmentDataArr = Array();
//    $enrollmentDataArr['newGeneral'] = $totalNewGeneral;
//    $enrollmentDataArr['newHandicapped'] = $totalNewHandicapped;
//    $enrollmentDataArr['renewGeneral'] = $totalReNewGeneral;
//    $enrollmentDataArr['renewHandicapped'] = $totalReNewHandicapped;
//    $enrollmentDataArr['totalEnrollmentCount'] = $totalNewGeneral + $totalNewHandicapped + $totalReNewGeneral + $totalReNewHandicapped;
    // $dashboardArr['enrollmentData'] = $enrollmentDataArr;
    // END COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES
    /* Total Enrollment Data End */

    /* Request And Complaint Data Start */
    //START COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES
    /* $sqlType = "SELECT TYPE,IF(TYPE = 1,'Request','Complain') TYPENAME FROM ".COMPLAINTMASTER." CM WHERE CM.DELETE_FLAG = '0' AND CM.FK_CLIENT_ID = '".$clientId."' AND CM.FK_ORG_ID = '".$orgId."' GROUP BY CM.TYPE ";
      $resultType = fetch_rec_query($sqlType);

      if(count($resultType) > 0){
      $finalJson = array();
      foreach($resultType as $keyType => $valueType){
      $finalJson[$keyType]['TypeId'] = $valueType['TYPE'];
      $finalJson[$keyType]['TypeName'] = $valueType['TYPENAME'];

      $sqlGetDepartment = "SELECT COUNT(PK_COMPLAINT_ID) COUNT,CM.TYPE,CM.DEPARTMENT_ID,CL.DISPLAY_VALUE FROM ".COMPLAINTMASTER." CM INNER JOIN ".CLIENTLOOKUP." CL ON CL.PK_CLT_LKUP_ID = CM.DEPARTMENT_ID AND CL.DELETE_FLAG = 0 WHERE CM.TYPE = '".$valueType['TYPE']."' AND CM.FK_CLIENT_ID = '".$clientId."' AND CM.FK_ORG_ID = '".$orgId."' AND CM.DELETE_FLAG = 0 GROUP BY CM.DEPARTMENT_ID ";
      $resultGetDepartment = fetch_rec_query($sqlGetDepartment);
      if(count($resultGetDepartment) > 0){
      $finalJson[$keyType]['TypewiseData'] = $resultGetDepartment;
      }else{
      $finalJson[$keyType]['TypewiseData'] = NORECORDS;
      }


      }
      $dashboardArr['complaintData'] = array_values($finalJson);
      }else{
      $dashboardArr['complaintData'] = NORECORDS;
      } */
    //END COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES 

    $sqlDepartmentData = "SELECT PK_CLT_LKUP_ID ID,DISPLAY_VALUE AS DEPARTMENT_NAME ,ISDEFAULTFLAG  FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'DEPARTMENT' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientId . "') AND DELETE_FLAG = '0' ORDER BY PK_CLT_LKUP_ID ASC";
    $resDepartmentData = fetch_rec_query($sqlDepartmentData);
    //print_r($resDepartmentData); EXIT;
    if (count($resDepartmentData) > 0) {
        $complaintArr = Array();
        foreach ($resDepartmentData as $keyDepartment => $valDepartment) {


            $totalRequest = $totalComplaint = 0;

            $sqlTotalRequest = "SELECT count(PK_COMPLAINT_ID) AS COUNT FROM " . COMPLAINTMASTER . " WHERE TYPE ='1' AND DEPARTMENT_ID ='" . $valDepartment['ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalRequest = fetch_rec_query($sqlTotalRequest);
            $totalRequest = $resTotalRequest[0]['COUNT'];

            $sqlTotalComplaint = "SELECT count(PK_COMPLAINT_ID) AS COUNT FROM " . COMPLAINTMASTER . " WHERE TYPE ='2' AND DEPARTMENT_ID ='" . $valDepartment['ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalComplaint = fetch_rec_query($sqlTotalComplaint);
            $totalComplaint = $resTotalComplaint[0]['COUNT'];

            $complaintArr[$keyDepartment]['departmentName'] = $valDepartment['DEPARTMENT_NAME'];
            $complaintArr[$keyDepartment]['totalRequest'] = $totalRequest;
            $complaintArr[$keyDepartment]['totalComplaint'] = $totalComplaint;
        }
        $dashboardArr['complaintData'] = $complaintArr;
    } else {
        $dashboardArr['complaintData'] = NORECORDS;
    }

    /* Request And Complaint Data END */

    /* Suspenssion and Termination Code START */

    //START COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES

    $sqlGetSuspension = "SELECT TYPE,IF(TYPE = 1,'Suspension','Termination') TYPENAME,COUNT(PK_SUSPENSION_ID) COUNT  FROM " . STUDENTSUSPENSION . " STS WHERE STS.DELETE_FLAG = '0' AND STS.FK_CLIENT_ID = '" . $clientId . "' AND STS.FK_ORG_ID = '" . $orgId . "' GROUP BY TYPE ";
    $resultGetSupType = fetch_rec_query($sqlGetSuspension);

    if (count($resultGetSupType) > 0) {
        $finalJson = array();
        foreach ($resultGetSupType as $keyType => $valueType) {
            $finalJson[$keyType]['TypeId'] = $valueType['TYPE'];
            $finalJson[$keyType]['TypeName'] = $valueType['TYPENAME'];
            $finalJson[$keyType]['totalCount'] = $valueType['COUNT'];
        }
        $revokArray = array();
        $sqlRevokeCount = "SELECT COUNT(PK_SUSPENSION_ID) AS COUNT FROM " . STUDENTSUSPENSION . " WHERE DELETE_FLAG ='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND REVOKE_BY > 0 LIMIT 0,1";
        $resultRevokCount = fetch_rec_query($sqlRevokeCount);
        if (count($resultRevokCount) > 0) {
            $revokArray[0]['TypeId'] = "3";
            $revokArray[0]['TypeName'] = "Revoked";
            $revokArray[0]['totalCount'] = $resultRevokCount[0]['COUNT'];
        }

        $returnJson = array_merge($finalJson, $revokArray);
        $dashboardArr['suspenssionData'] = array_values($returnJson);
    } else {
        $dashboardArr['suspenssionData'] = NORECORDS;
    }

    //END COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES
//    $totalSuspenssionRecord = $totalSuspenssion = $totalRevoke = $totalTermination = 0;
//
//    $sqlStudentSusspenssiontData = "SELECT COUNT(FK_STUDENT_ID) AS COUNT FROM " . STUDENTSUSPENSION . " WHERE TYPE ='1' AND DELETE_FLAG ='0' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resStudentSusspenssiontData = fetch_rec_query($sqlStudentSusspenssiontData);
//    $totalSuspenssionRecord = $resStudentSusspenssiontData[0]['COUNT'];
//
//    $sqlStudentSusspenssiontData = "SELECT COUNT(FK_STUDENT_ID) AS COUNT FROM " . STUDENTSUSPENSION . " WHERE TYPE ='1' AND REVOKE_BY ='0' AND DELETE_FLAG ='0' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resStudentSusspenssiontData = fetch_rec_query($sqlStudentSusspenssiontData);
//    $totalSuspenssion = $resStudentSusspenssiontData[0]['COUNT'];
//
//    $sqlStudentSusspenssiontData = "SELECT COUNT(FK_STUDENT_ID) AS COUNT FROM " . STUDENTSUSPENSION . " WHERE TYPE ='1' AND REVOKE_BY > 0 AND DELETE_FLAG ='0' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resStudentSusspenssiontData = fetch_rec_query($sqlStudentSusspenssiontData);
//    $totalRevoke = $resStudentSusspenssiontData[0]['COUNT'];
//
//
//    $sqlStudentSusspenssiontData = "SELECT COUNT(FK_STUDENT_ID) AS COUNT FROM " . STUDENTSUSPENSION . " WHERE TYPE ='2' AND DELETE_FLAG ='0' AND FK_CLIENT_ID = '".$clientId."' AND FK_ORG_ID = '".$orgId."'";
//    $resStudentSusspenssiontData = fetch_rec_query($sqlStudentSusspenssiontData);
//    $totalTermination = $resStudentSusspenssiontData[0]['COUNT'];
//    if ($totalSuspenssionRecord > 0) {
//        $suspenssionData['total'] = $totalSuspenssionRecord;
//        $suspenssionData['totalSuspenssion'] = $totalSuspenssion;
//        $suspenssionData['totalRevoke'] = $totalRevoke;
//        $suspenssionData['totalTermination'] = $totalTermination;
//
//        $dashboardArr['suspenssionData'] = $suspenssionData;
//    } else {
//        $dashboardArr['suspenssionData'] = NORECORDS;
//    }

    /* Suspenssion and Termination Code END */


    /* Property Data  START */

    //START COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES
    /* $sqlpropertyData = "SELECT COUNT(PK_INQ_ID) AS COUNT,IM.STATUS,IF(STATUS = 11,'Pending',IF(STATUS = '12','Booked','Canceled')) STATUSNAME FROM " . INQMASTER . " IM INNER JOIN ".PROPERTYMASTER." PM ON PM.PK_PROPERTY_ID = IM.FK_PROPERTY_ID WHERE IM.FK_CLIENT_ID = '".$clientId."' AND IM.FK_ORG_ID = '".$orgId."' GROUP BY IM.STATUS ";
      $resultPropertyData = fetch_rec_query($sqlpropertyData);
      if(count($resultPropertyData) > 0){
      $dashboardArr['propertyData'] = $resultPropertyData;
      }else{

      $dashboardArr['propertyData'] = NORECORDS;
      } */

    //END COMMENT ADDED BY DEEPAK PATIL - 10-08-2018  FOR QUERY CHANGES
    $sqlpropertyData = "SELECT PK_PROPERTY_ID,PROPERTY_NAME FROM " . PROPERTYMASTER . " PM WHERE PM.DELETE_FLAG ='0' AND PM.FK_CLIENT_ID = '" . $clientId . "' AND PM.FK_ORG_ID = '" . $orgId . "'";
    $resPropertyData = fetch_rec_query($sqlpropertyData);
    if (count($resPropertyData) > 0) {
        $propertyArr = Array();
        foreach ($resPropertyData as $keyProperty => $valProperty) {
            $totalInquiry = $totalPendingInquiry = $totalBookedInquiry = $totalDropedInquiry = 0;

            $sqlTotalInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalInquiry = fetch_rec_query($sqlTotalInquiry);
            $totalInquiry = $resTotalInquiry[0]['COUNT'];

            $sqlTotalPendingInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE STATUS ='11' AND FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalPendingInquiry = fetch_rec_query($sqlTotalPendingInquiry);
            $totalPendingInquiry = $resTotalPendingInquiry[0]['COUNT'];

            $sqlTotalBookedInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE STATUS ='12' AND FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalBookedInquiry = fetch_rec_query($sqlTotalBookedInquiry);
            $totalBookedInquiry = $resTotalBookedInquiry[0]['COUNT'];

            $sqlTotalDropedInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE STATUS ='13' AND FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalDropedInquiry = fetch_rec_query($sqlTotalDropedInquiry);
            $totalDropedInquiry = $resTotalDropedInquiry[0]['COUNT'];

            $propertyArr[$keyProperty]['propertyName'] = $valProperty['PROPERTY_NAME'];
            $propertyArr[$keyProperty]['totalInquiry'] = $totalInquiry;
            $propertyArr[$keyProperty]['totalOpenInquiry'] = $totalPendingInquiry;
            $propertyArr[$keyProperty]['totalBookedInquiry'] = $totalBookedInquiry;
            $propertyArr[$keyProperty]['totalCancelInquiry'] = $totalDropedInquiry;
        }
        $dashboardArr['propertyData'] = $propertyArr;
    } else {
        $dashboardArr['propertyData'] = NORECORDS;
    }

    /* Property Data  END */

    $sqlEventData = "SELECT IM.ITEM_NAME,IM.ITEM_TYPE,DATE(DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%Y-%m-%d')) AS EVENT_START_DATETIME,IM.QTY,IF(EB.QTY > 0,SUM(EB.QTY),0)AS TOTAL_BOOKED_SEATS,IF(EB.QTY > 0,IM.QTY-SUM(EB.QTY),IM.QTY) AS REMAINING_SEATS, IM.IS_ACTIVE FROM " . ITEMMASTER . " IM LEFT JOIN " . EVENTSBOOKING . " EB ON EB.FK_ITEM_ID =IM.PK_ITEM_ID AND EB.DELETE_FLAG ='0' WHERE IM.ITEM_TYPE='EVENTS' AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "' GROUP BY (EB.FK_ITEM_ID)";
    $resEventData = fetch_rec_query($sqlEventData);

    if (count($resEventData) > 0) {
        $eventArr = Array();
        foreach ($resEventData as $keyEvent => $valEvent) {
            $eventArr[$keyEvent]['eventName'] = $valEvent['ITEM_NAME'];
            $eventArr[$keyEvent]['eventDate'] = $valEvent['EVENT_START_DATETIME'];
            $eventArr[$keyEvent]['maxCapacity'] = $valEvent['QTY'];
            $eventArr[$keyEvent]['bookedSeats'] = $valEvent['TOTAL_BOOKED_SEATS'];
            $eventArr[$keyEvent]['remainingSeats'] = $valEvent['REMAINING_SEATS'];
            $eventArr[$keyEvent]['status'] = $valEvent['IS_ACTIVE'];
        }
        $dashboardArr['eventData'] = $eventArr;
    } else {
        $dashboardArr['eventData'] = NORECORDS;
    }


    if (!empty($dashboardArr)) {
        $result = array('status' => SCS, "data" => $dashboardArr);
        http_response_code(200);
    } else {
        $result = array('status' => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getSuperVisorListing($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $selectedLevel = isset($postData['postData']['selectedLevelId']) ? addslashes(trim($postData['postData']['selectedLevelId'])) : "";
    $keyWord = isset($postData['postData']['keyWord']) ? addslashes(trim($postData['postData']['keyWord'])) : "";


    // START SEARCHING USER 
//    $sqlSuperUser = "SELECT * FROM ".USERMASTER." UM WHERE FK_CLIENT_ID = '".$clientId."' AND USER_LEVEL = (SELECT PK_CLT_LKUP_ID FROM ".CLIENTLOOKUP." CL WHERE CL.PK_CLT_LKUP_ID < '".$selectedLevel."' AND FK_CLIENT_ID = '".$clientId."' AND LOOK_TYPE = 'USERLEVEL' ORDER BY PK_CLT_LKUP_ID DESC LIMIT 0,1 ) AND DELETE_FLAG = 0 AND FULLNAME LIKE '%".$keyWord."%' ";
    $sqlSuperUser = "SELECT UM.PK_USER_ID,UM.FULLNAME FROM " . USERMASTER . " UM WHERE FK_CLIENT_ID = '" . $clientId . "' AND USER_LEVEL = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CL WHERE CL.PK_CLT_LKUP_ID < '" . $selectedLevel . "' AND FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'USERLEVEL' ORDER BY PK_CLT_LKUP_ID DESC LIMIT 0,1 ) AND DELETE_FLAG = 0 AND FULLNAME LIKE '%" . $keyWord . "%' ";
    $resultSuperUser = fetch_rec_query($sqlSuperUser);

    if (count($resultSuperUser) > 0) {
        $finalJson = array();

//        for($i=0;$i<count($resultSuperUser);$i++){
        foreach ($resultSuperUser as $keyUser => $valueUser) {

            $finalJson[$keyUser]['userId'] = $valueUser['PK_USER_ID'];
            $finalJson[$keyUser]['fullName'] = $valueUser['FULLNAME'];
        }

        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function createUpdateDashbordGrid($postData) {

    $clientId = isset($postData['postData']['clientId']) ? trim($postData['postData']['clientId']) : ""; // CLIENT ID
    $orgId = isset($postData['postData']['orgId']) ? trim($postData['postData']['orgId']) : ""; // ORG ID
    $userId = isset($postData['postData']['userId']) ? trim($postData['postData']['userId']) : ""; // USER ID
    $gridId = isset($postData['postData']['gridId']) ? trim($postData['postData']['gridId']) : "";
    $gridName = isset($postData['postData']['gridName']) ? trim($postData['postData']['gridName']) : "";
    $functionName = isset($postData['postData']['functionName']) ? trim($postData['postData']['functionName']) : "";
//    $status = isset($postData['postData']['status']) ? trim($postData['postData']['status']) : ""; 

    if (!empty($gridId) && $gridId != "") {//UPDATE CASE
        $updateArr = array();
        $updateArr['GRID_NAME'] = $gridName;
        $updateArr['FUNCTON_NAME'] = $functionName;
        $updateArr['STATUS'] = '1';
        $updateArr['CHANGED_BY'] = $userId;
        $updateArr['CHANGED_BY_DATE'] = time();
        $updateArr['DELETE_FLAG'] = '0';

        $updateWhere = " PK_GRID_ID = '" . $gridId . "' AND DELETE_FLAG = '0' ";
        $updateData = update_rec(DASHBORDGRIDMASTER, $updateArr, $updateWhere);

        if ($updateData) {

            $requestData = array();
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['gridId'] = $gridId;

            $getData = getDashbordGrid($requestData);

            $result = array("status" => SCS, "data" => $getData['data']);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {// INSERT CASE
        $insertArr = array();
        $insertArr['GRID_NAME'] = $gridName;
        $insertArr['FUNCTON_NAME'] = $functionName;
        $insertArr['STATUS'] = '1';
        $insertArr['CREATED_BY'] = $userId;
        $insertArr['CREATED_BY_DATE'] = time();
        $insertArr['DELETE_FLAG'] = '0';

        $insertData = insert_rec(DASHBORDGRIDMASTER, $insertArr);

        if ($insertData) {

            $requestData = array();
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['gridId'] = $insertData['lastInsertedId'];

            $getData = getDashbordGrid($requestData);

            $result = array("status" => SCS, "data" => $getData['data']);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    }

    return $result;
}

function getDashbordGrid($postData) {//FOR GETTING DASHBORD GRID DATA
    
    $clientId = isset($postData['postData']['clientId']) ? trim($postData['postData']['clientId']) : ""; // CLIENT ID
    $orgId = isset($postData['postData']['orgId']) ? trim($postData['postData']['orgId']) : ""; // ORG ID
    $userId = isset($postData['postData']['userId']) ? trim($postData['postData']['userId']) : ""; // USER ID
    $gridId = isset($postData['postData']['gridId']) ? trim($postData['postData']['gridId']) : "";

    $where = "";
    if (!empty($gridId) && $gridId != "") {
        $where = " AND PK_GRID_ID = '" . $gridId . "' ";
    }

    $sqlGetGrid = "SELECT PK_GRID_ID,GRID_NAME,FUNCTON_NAME FROM " . DASHBORDGRIDMASTER . " WHERE DELETE_FLAG = '0' " . $where . " ";
    $reultGetGrid = fetch_rec_query($sqlGetGrid);

    if (count($reultGetGrid) > 0) {
        foreach ($reultGetGrid as $keyGrid => $valueGrid) {

            $finalJson[$keyGrid]['gridId'] = $valueGrid['PK_GRID_ID'];
            $finalJson[$keyGrid]['gridName'] = $valueGrid['GRID_NAME'];
            $finalJson[$keyGrid]['funName'] = $valueGrid['FUNCTON_NAME'];
        }

        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

function deleteDashbordGrid($postData) {//FOR DELETE DASHBORD GRID
    $clientId = isset($postData['postData']['clientId']) ? trim($postData['postData']['clientId']) : ""; // CLIENT ID
    $orgId = isset($postData['postData']['orgId']) ? trim($postData['postData']['orgId']) : ""; // ORG ID
    $userId = isset($postData['postData']['userId']) ? trim($postData['postData']['userId']) : ""; // USER ID
    $gridId = isset($postData['postData']['gridId']) ? trim($postData['postData']['gridId']) : "";

    if (!empty($gridId)) {

        $updateArr = array();
        $updateArr['CHANGED_BY'] = $userId;
        $updateArr['CHANGED_BY_DATE'] = time();
        $updateArr['DELETE_FLAG'] = '1';

        $updateWhere = " PK_GRID_ID = '" . $gridId . "' AND DELETE_FLAG = '0' ";
        $updateData = update_rec(DASHBORDGRIDMASTER, $updateArr, $updateWhere);

        if ($updateData) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

function checkUncheckDashbordUserWise($postData) {//FOR CHECK UNCHECK DASHBORD GRID USER WISE
    $clientId = isset($postData['postData']['clientId']) ? trim($postData['postData']['clientId']) : ""; // CLIENT ID
    $orgId = isset($postData['postData']['orgId']) ? trim($postData['postData']['orgId']) : ""; // ORG ID
    $userId = isset($postData['postData']['userId']) ? trim($postData['postData']['userId']) : ""; // USER ID
    $gridId = isset($postData['postData']['gridId']) ? trim($postData['postData']['gridId']) : "";
//    $status = isset($postData['postData']['status']) ? trim($postData['postData']['status']) : ""; //0= INACTIVE , 1 = ACTIVE
    if ($userId != '-1') {
        if (!empty($gridId) && $gridId != "" && $userId != '-1') {

            $sqlGetUserGrid = "SELECT GROUP_CONCAT(PK_GRIDTRANS_ID) DELETEGRIDID FROM " . DASHBORDGRIDTRANS . " WHERE FK_USER_ID = '" . $userId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' ";
            $getUserGrid = fetch_rec_query($sqlGetUserGrid);
            //        print_r($getUserGrid);exit;
            $explodeGridId = explode(",", $gridId);

            foreach ($explodeGridId as $keyGrid => $valueGrid) {

                $insertArr = array();
                $insertArr['FK_CLIENT_ID'] = $clientId;
                $insertArr['FK_ORG_ID'] = $orgId;
                $insertArr['FK_USER_ID'] = $userId;
                $insertArr['FK_GRID_ID'] = $valueGrid;
                $insertArr['GRID_STATUS'] = '1';
                $insertArr['CHANGED_BY'] = $userId;
                $insertArr['CHANGED_BY_DATE'] = time();
                $insertArr['DELETE_FLAG'] = '0';

                $insertData = insert_rec(DASHBORDGRIDTRANS, $insertArr);
                if ($insertData) {
                    $gridNewIdUser[] = $insertData['lastInsertedId'];
                }
            }

            if (!empty($gridNewIdUser)) {

                if ($getUserGrid[0]['DELETEGRIDID'] != "" && !empty($getUserGrid[0]['DELETEGRIDID'])) {

                    $updateArr = array();
                    $updateArr['CHANGED_BY'] = $userId;
                    $updateArr['CHANGED_BY_DATE'] = time();
                    $updateArr['DELETE_FLAG'] = '1';

                    $updateWhere = " PK_GRIDTRANS_ID IN (" . $getUserGrid[0]['DELETEGRIDID'] . ") AND DELETE_FLAG = '0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_USER_ID = '" . $userId . "' ";
                    $updateData = update_rec(DASHBORDGRIDTRANS, $updateArr, $updateWhere);
                }
                $getCustomDashbord = array();
                //GET DAHBORD GRID MASTER DATA USER WISE ADDED BY DARSHAN PATEL ON 7/11/2017
                $sqlGridUserWise = "SELECT DGT.FK_GRID_ID ID, DM.GRID_NAME VALUE , DM.FUNCTON_NAME FUN_NAME FROM " . DASHBORDGRIDTRANS . " DGT INNER JOIN " . DASHBORDGRIDMASTER . " DM ON DM.PK_GRID_ID = DGT.FK_GRID_ID WHERE DGT.DELETE_FLAG = '0' AND DGT.GRID_STATUS = '1' AND DGT.FK_USER_ID = '" . $userId . "' AND DGT.FK_CLIENT_ID = '" . $clientId . "' AND DM.DELETE_FLAG = '0' ORDER BY DGT.PK_GRIDTRANS_ID ASC";
                $resultGridUserWise = fetch_rec_query($sqlGridUserWise);

                if (count($resultGridUserWise) > 0) {
                    $getCustomDashbord['data']['userActiveGridList'] = $resultGridUserWise;
                } else {
                    $getCustomDashbord['data']['userActiveGridList'] = NORECORDS;
                }

                $result = array("status" => SCS, "data" => $getCustomDashbord['data']);
                http_response_code(200);
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => "master user not allow this");
        http_response_code(400);
    }

    return $result;
}

function revenueDashboardData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

      //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND CREATED_BY IN (" . $userIds . ")";
    }
    //add by Saitsh Karena for master user Permission all data END

    $sqlEnrollmentInVoiceData = "SELECT SUM(TOTAL_CHARGES) AS TOTAL,INVOICE_TYPE FROM " . INVOICEMASTER . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . $whereUserCond. "' GROUP BY INVOICE_TYPE";
    $resEnrollmentInvoiceData = fetch_rec_query($sqlEnrollmentInVoiceData);
    if (count($resEnrollmentInvoiceData) > 0) {
        $result = array("status" => SCS, "data" => $resEnrollmentInvoiceData);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function enrollmentDashboardData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    
    $sqlEnrollementType = "SELECT ENROLLMENT_TYPE FROM " . STUDENTMASTER . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' GROUP BY ENROLLMENT_TYPE";
    $resultEnrollmentType = fetch_rec_query($sqlEnrollementType);
    
     //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND ST.CREATED_BY IN (" . $userIds . ")";
    }
    //add by Saitsh Karena for master user Permission all data END
    

    if (count($resultEnrollmentType) > 0) {
        $finalJson = array();
        foreach ($resultEnrollmentType as $keyEnroll => $valueEnroll) {
            $finalJson[$keyEnroll]['enrollType'] = $valueEnroll['ENROLLMENT_TYPE'];
          $sqlGetCondition = "SELECT COUNT(ST.PK_STUD_ID) COUNT,ST.MEDICAL_CONDITION,CL.DISPLAY_VALUE FROM " . STUDENTMASTER . " ST INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID = ST.MEDICAL_CONDITION AND CL.DELETE_FLAG = '0' WHERE ENROLLMENT_TYPE ='" . $valueEnroll['ENROLLMENT_TYPE'] . "' AND ST.FK_CLIENT_ID = '" . $clientId . "' AND ST.FK_ORG_ID = '" . $orgId .$whereUserCond. "' GROUP BY ST.MEDICAL_CONDITION";
           
            $resultGetCondition = fetch_rec_query($sqlGetCondition);
            if (count($resultGetCondition) > 0) {
                $finalJson[$keyEnroll]['enrollTypeWiseData'] = $resultGetCondition;
            } else {
                $finalJson[$keyEnroll]['enrollTypeWiseData'] = NORECORDS;
            }
        }
        $result = array("status" => SCS, "data" => array_values($finalJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function requestComplaintDashboardData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
     $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
     
      //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND CREATED_BY IN (" . $userIds . ")";
    }
    //add by Saitsh Karena for master user Permission all data END

    $sqlDepartmentData = "SELECT PK_LOOKUP_ID ID,VALUE AS DEPARTMENT_NAME  FROM " . LOOKUPMASTER . " WHERE TYPE = 'DEPARTMENT' AND DELETE_FLAG = '0' ORDER BY PK_LOOKUP_ID ASC";
    $resDepartmentData = fetch_rec_query($sqlDepartmentData);
    //print_r($resDepartmentData); EXIT;
    if (count($resDepartmentData) > 0) {
        $complaintArr = Array();
        foreach ($resDepartmentData as $keyDepartment => $valDepartment) {


            $totalRequest = $totalComplaint = 0;

            $sqlTotalRequest = "SELECT count(PK_COMPLAINT_ID) AS COUNT FROM " . COMPLAINTMASTER . " WHERE TYPE ='1' AND DEPARTMENT_ID ='" . $valDepartment['ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'".$whereUserCond;
            $resTotalRequest = fetch_rec_query($sqlTotalRequest);
            $totalRequest = $resTotalRequest[0]['COUNT'];

            $sqlTotalComplaint = "SELECT count(PK_COMPLAINT_ID) AS COUNT FROM " . COMPLAINTMASTER . " WHERE TYPE ='2' AND DEPARTMENT_ID ='" . $valDepartment['ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'".$whereUserCond;
            $resTotalComplaint = fetch_rec_query($sqlTotalComplaint);
            $totalComplaint = $resTotalComplaint[0]['COUNT'];

            $complaintArr[$keyDepartment]['departmentName'] = $valDepartment['DEPARTMENT_NAME'];
            $complaintArr[$keyDepartment]['totalRequest'] = $totalRequest;
            $complaintArr[$keyDepartment]['totalComplaint'] = $totalComplaint;
        }
        $result = array("status" => SCS, "data" => array_values($complaintArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function suspenssionDashboardData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    
     //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND CREATED_BY IN (" . $userIds . ")";
    }
    //add by Saitsh Karena for master user Permission all data END

    $sqlGetSuspension = "SELECT TYPE,IF(TYPE = 1,'Suspension','Termination') TYPENAME,COUNT(PK_SUSPENSION_ID) COUNT  FROM " . STUDENTSUSPENSION . " STS WHERE STS.DELETE_FLAG = '0' AND STS.FK_CLIENT_ID = '" . $clientId . "' AND STS.FK_ORG_ID = '" . $orgId . "'".$whereUserCond." GROUP BY TYPE ";
    $resultGetSupType = fetch_rec_query($sqlGetSuspension);

    if (count($resultGetSupType) > 0) {
        $finalJson = array();
        foreach ($resultGetSupType as $keyType => $valueType) {
            $finalJson[$keyType]['TypeId'] = $valueType['TYPE'];
            $finalJson[$keyType]['TypeName'] = $valueType['TYPENAME'];
            $finalJson[$keyType]['totalCount'] = $valueType['COUNT'];
        }
        $revokArray = array();
        $sqlRevokeCount = "SELECT COUNT(PK_SUSPENSION_ID) AS COUNT FROM " . STUDENTSUSPENSION . " WHERE DELETE_FLAG ='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'".$whereUserCond." AND REVOKE_BY > 0 LIMIT 0,1";
        $resultRevokCount = fetch_rec_query($sqlRevokeCount);
        if (count($resultRevokCount) > 0) {
            $revokArray[0]['TypeId'] = "3";
            $revokArray[0]['TypeName'] = "Revoked";
            $revokArray[0]['totalCount'] = $resultRevokCount[0]['COUNT'];
        }
        $returnJson = array_merge($finalJson, $revokArray);
        $result = array("status" => SCS, "data" => array_values($returnJson));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function propertyDashboardData($postData){
     $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    
     $sqlpropertyData = "SELECT PK_PROPERTY_ID,PROPERTY_NAME FROM " . PROPERTYMASTER . " PM WHERE PM.DELETE_FLAG ='0' AND PM.FK_CLIENT_ID = '" . $clientId . "' AND PM.FK_ORG_ID = '" . $orgId . "'";
    $resPropertyData = fetch_rec_query($sqlpropertyData);
    if (count($resPropertyData) > 0) {
        $propertyArr = Array();
        foreach ($resPropertyData as $keyProperty => $valProperty) {
            $totalInquiry = $totalPendingInquiry = $totalBookedInquiry = $totalDropedInquiry = 0;

            $sqlTotalInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalInquiry = fetch_rec_query($sqlTotalInquiry);
            $totalInquiry = $resTotalInquiry[0]['COUNT'];

            $sqlTotalPendingInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE STATUS ='11' AND FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalPendingInquiry = fetch_rec_query($sqlTotalPendingInquiry);
            $totalPendingInquiry = $resTotalPendingInquiry[0]['COUNT'];

            $sqlTotalBookedInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE STATUS ='12' AND FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalBookedInquiry = fetch_rec_query($sqlTotalBookedInquiry);
            $totalBookedInquiry = $resTotalBookedInquiry[0]['COUNT'];

            $sqlTotalDropedInquiry = "SELECT count(PK_INQ_ID) AS COUNT FROM " . INQMASTER . " WHERE STATUS ='13' AND FK_PROPERTY_ID='" . $valProperty['PK_PROPERTY_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resTotalDropedInquiry = fetch_rec_query($sqlTotalDropedInquiry);
            $totalDropedInquiry = $resTotalDropedInquiry[0]['COUNT'];

            $propertyArr[$keyProperty]['propertyName'] = $valProperty['PROPERTY_NAME'];
            $propertyArr[$keyProperty]['totalInquiry'] = $totalInquiry;
            $propertyArr[$keyProperty]['totalOpenInquiry'] = $totalPendingInquiry;
            $propertyArr[$keyProperty]['totalBookedInquiry'] = $totalBookedInquiry;
            $propertyArr[$keyProperty]['totalCancelInquiry'] = $totalDropedInquiry;
        }
        $result = array("status" => SCS, "data" => array_values($propertyArr));
        http_response_code(200);
        
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
 
}

function eventDashboardData($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    
    
     $sqlEventData = "SELECT IM.ITEM_NAME,IM.ITEM_TYPE,DATE(DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%Y-%m-%d')) AS EVENT_START_DATETIME,DATE(DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%Y-%m-%d')) AS EVENT_END_DATETIME,IM.QTY,IF(EB.QTY > 0,SUM(EB.QTY),0)AS TOTAL_BOOKED_SEATS,IF(EB.QTY > 0,IM.QTY-SUM(EB.QTY),IM.QTY) AS REMAINING_SEATS, IM.IS_ACTIVE FROM " . ITEMMASTER . " IM LEFT JOIN " . EVENTSBOOKING . " EB ON EB.FK_ITEM_ID =IM.PK_ITEM_ID AND EB.DELETE_FLAG ='0' WHERE IM.ITEM_TYPE='EVENTS' AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "' GROUP BY (EB.FK_ITEM_ID)";
    $resEventData = fetch_rec_query($sqlEventData);

    if (count($resEventData) > 0) {
        $eventArr = Array();
        foreach ($resEventData as $keyEvent => $valEvent) {
            $eventArr[$keyEvent]['eventName'] = $valEvent['ITEM_NAME'];
            $eventArr[$keyEvent]['eventStartDate'] = $valEvent['EVENT_START_DATETIME'];
            $eventArr[$keyEvent]['eventEndDate'] = $valEvent['EVENT_END_DATETIME'];
            $eventArr[$keyEvent]['maxCapacity'] = $valEvent['QTY'];
            $eventArr[$keyEvent]['bookedSeats'] = $valEvent['TOTAL_BOOKED_SEATS'];
            $eventArr[$keyEvent]['remainingSeats'] = $valEvent['REMAINING_SEATS'];
            $eventArr[$keyEvent]['status'] = $valEvent['IS_ACTIVE'];
        }
        $result = array("status" => SCS, "data" => array_values($eventArr));
        http_response_code(200);
      } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}
//ADD EDIT CODE END FOR CMS DATA ON 10-08-2018
function addCmsData($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $sectionId = isset($postData['postData']['sectionId']) ? addslashes(trim($postData['postData']['sectionId'])) : "";
    $sectionName = isset($postData['postData']['sectionName']) ? addslashes(trim($postData['postData']['sectionName'])) : "";
    $sectionDetail = isset($postData['postData']['sectionDetail']) ? addslashes(trim($postData['postData']['sectionDetail'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($userId)) {  // CHECK REQURIED FIELD CONDITION
// ITEM INSERT ARRAY.
        $msg = "";
        $insertSuspenArray = Array(); //ITEM ARRAY CREATE
        $insertSuspenArray['SECTION_NAME'] = $sectionName;
        $insertSuspenArray['SECTION_DETAIL'] = $sectionDetail;
        $insertSuspenArray['FK_CLIENT_ID'] = $clientId;
        $insertSuspenArray['FK_ORG_ID'] = $orgId;

        if (isset($sectionId) && $sectionId > 0) {
            $insertSuspenArray['CHANGED_BY'] = $userId;
            $insertSuspenArray['CHANGED_BY_DATE'] = time();
            $whereISuspenCond = "PK_SECTION_ID ='" . $sectionId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
            $updateItem = update_rec(PAGESECTION, $insertSuspenArray, $whereISuspenCond); // UPDATE RECORD IN Student Suspension TABLE.
            if (!$updateItem) {
                $msg = UPDATEFAIL . " " . PAGESECTION;
            }
        } else {
            $insertSuspenArray['CREATED_BY'] = $userId;
            $insertSuspenArray['CREATED_BY_DATE'] = time();
            $insertSuspen = insert_rec(PAGESECTION, $insertSuspenArray); // INSERT RECORD IN Student Suspension TABLE.          
            if (!isset($insertSuspen['lastInsertedId']) || $insertSuspen['lastInsertedId'] == 0) {
                $msg = INSERTFAIL . " " . PAGESECTION;
            }
        }

        $result = array("status" => SCS);
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD EDIT CODE END FOR CMS DATA ON 10-08-2018
// ADDED BY KAVITA PATEL ON 23-08-2018 START
function userProfileImageUpload($postData, $postFile) {
    // POST DATA
    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
   
    $attachmentName = $postFile; // ATTACH NAME 
    // START UPLOADING DATA INTO DATA BASE
    $path = STUDENT_PROFILE_PATH;
    $type = 'profile';
    $uploadAttachment = uploadImage($attachmentName, $path, $type); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
    if ($uploadAttachment['status'] == SCS) {

        // INSERT DATA TO DATABASE FOR FURTHER REF.

        $updateAttach['PROFILE_IMAGE'] = $uploadAttachment['fileName'];
        $updateAttach['CHANGED_BY'] = $userId;
        $updateAttach['CHANGED_BY_DATE'] = time();
       
        $whereUserattachCond = "FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG='0'";
        $updateAttach = update_rec(USERMASTER, $updateAttach, $whereUserattachCond); // INSERTING DATA IN ATTACH_STUDENT_MASTER TABLE

        if ($updateAttach) { // IF SUCCESS THEN GIVE SUCCESS RESPONSE.
            $finalJson = array("fileName" => basename($uploadAttachment['fileName']), "fileLink" => $uploadAttachment['fileName']);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        }
    } else {
        $result = $uploadAttachment;
        http_response_code(400);
    }

    return $result; // RETURN DATA TO PL
}
function changePasswordUser($postData) {
//print_r($postData); exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $oldpassword = isset($postData['postData']['oldpassword']) ? addslashes(trim($postData['postData']['oldpassword'])) : "";
    $newpassword = isset($postData['postData']['newpassword']) ? addslashes(trim($postData['postData']['newpassword'])) : "";
    $passwordFlag = isset($postData['postData']['passwordFlag']) ? addslashes(trim($postData['postData']['passwordFlag'])) : "";

    $sqlChanePassData = "SELECT UM.PK_USER_ID,UM.PASSWORD FROM " . USERMASTER . " UM WHERE UM.DELETE_FLAG = '0' AND UM.PK_USER_ID ='" . $userId . "' AND UM.FK_CLIENT_ID='".$clientId."' AND UM.FK_ORG_ID='".$orgId."'"; // QUERY FOR USER DATA.
    $resChangePassData = fetch_rec_query($sqlChanePassData);


    if ($passwordFlag == 1) {//FOR FORCE FULLY PASSWORD CHANGE
        $fieldsValue = array();
        $fieldsValue['PASSWORD'] = password_hash($newpassword, PASSWORD_DEFAULT);
        $fieldsValue['CHANGE_PASSWORD_FLAG'] = $passwordFlag;
        $fieldsValue['CHANGED_BY'] = $userId;
        $fieldsValue['CHANGED_BY_DATE'] = time();
        $whereCond = "PK_USER_ID= '" . $userId . "' AND DELETE_FLAG = '0' ";
        $updateUserMaster = update_rec(USERMASTER, $fieldsValue, $whereCond);
        if ($updateUserMaster) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    } else {//FOR NORMAL CHANGE PASSWORD
        if (password_verify($oldpassword, $resChangePassData[0]['PASSWORD'])) {

            $updateUser = array();
            $updateUser['PASSWORD'] = password_hash($newpassword, PASSWORD_DEFAULT);
            $updateUser['CHANGED_BY'] = $userId;
            $updateUser['CHANGED_BY_DATE'] = time();

            $updateWhere = "PK_USER_ID = '" . $userId . "' AND DELETE_FLAG = '0' ";

            $updateRequest = update_rec(USERMASTER, $updateUser, $updateWhere);
            if ($updateRequest) {
                $result = array("status" => SCS);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEQUERYFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => INCORRENTPASSWORD);
            http_response_code(400);
        }
    }
    return $result;
}
// ADDED BY KAVITA PATEL ON 23-08-2018 END
function resetPasswordUser($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $newpassword = isset($postData['postData']['newpassword']) ? addslashes(trim($postData['postData']['newpassword'])) : "";
    $confirmpassword = isset($postData['postData']['confirmpassword']) ? addslashes(trim($postData['postData']['confirmpassword'])) : "";
    $reqUserId = isset($postData['postData']['reqUserId']) ? addslashes(trim($postData['postData']['reqUserId'])) : "";
   

     $sqlResetPassData = "SELECT UM.PK_USER_ID,UM.PASSWORD FROM " . USERMASTER . " UM WHERE UM.DELETE_FLAG = '0' AND UM.PK_USER_ID ='" . $reqUserId . "' AND UM.FK_CLIENT_ID='".$clientId."' AND UM.FK_ORG_ID='".$orgId."'"; // QUERY FOR USER DATA.
    $resChangePassData = fetch_rec_query($sqlResetPassData);


    if (count($resChangePassData)>0 && ($newpassword==$confirmpassword) ) {//FOR NEW PWD AND CONFIRM PWD ARE SAME
        $fieldsValue = array();
        $fieldsValue['PASSWORD'] = password_hash($newpassword, PASSWORD_DEFAULT);
	$fieldsValue['CHANGE_PASSWORD_FLAG'] = 1;
        $fieldsValue['CHANGED_BY'] = $userId;
        $fieldsValue['CHANGED_BY_DATE'] = time();
        $whereCond = "PK_USER_ID= '" . $reqUserId . "' AND DELETE_FLAG = '0' ";
        $updateUserMaster = update_rec(USERMASTER, $fieldsValue, $whereCond);
        if ($updateUserMaster) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEQUERYFAIL);
            http_response_code(400);
        }
    }else{
	$result = array("status" => UPDATEQUERYFAIL);
        http_response_code(400);
    }
    
    return $result;
}
?>
