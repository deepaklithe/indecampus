<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 04-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR Add/Edit Item Detail. 
function addEditItem($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $price = isset($postData['postData']['price']) ? addslashes(trim($postData['postData']['price'])) : "";
    $priceStartDate = !empty($postData['postData']['priceStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['priceStartDate'])))) : "";
    $itemValidityEndDate = !empty($postData['postData']['itemValidityEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['itemValidityEndDate'])))) : "";
    $itemType = isset($postData['postData']['itemType']) ? addslashes(trim($postData['postData']['itemType'])) : "";
    $cgst = isset($postData['postData']['cgst']) ? addslashes(trim($postData['postData']['cgst'])) : "";
    $igst = isset($postData['postData']['igst']) ? addslashes(trim($postData['postData']['igst'])) : "";
    $sgst = isset($postData['postData']['sgst']) ? addslashes(trim($postData['postData']['sgst'])) : "";
    $hsnCode = isset($postData['postData']['hsnCode']) ? addslashes(trim($postData['postData']['hsnCode'])) : "";
    $tallyName = isset($postData['postData']['tallyName']) ? addslashes(trim($postData['postData']['tallyName'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $description = isset($postData['postData']['description']) ? addslashes(trim($postData['postData']['description'])) : "";
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "";
    $itemSubType = isset($postData['postData']['itemSubType']) ? addslashes(trim($postData['postData']['itemSubType'])) : 0;
    $eventStartDateTime = isset($postData['postData']['eventStartDateTime']) ? addslashes(trim($postData['postData']['eventStartDateTime'])) : '';
    $eventEndDateTime = isset($postData['postData']['eventEndDateTime']) ? addslashes(trim($postData['postData']['eventEndDateTime'])) : ''; //
    $eventVenue = isset($postData['postData']['eventVenue']) ? addslashes(trim($postData['postData']['eventVenue'])) : ''; //

    $eposItemId = isset($postData['postData']['eposItemId']) ? addslashes(trim($postData['postData']['eposItemId'])) : ''; //
    $eposSpaceId = isset($postData['postData']['eposSpaceId']) ? addslashes(trim($postData['postData']['eposSpaceId'])) : ''; //



    if (!empty($clientId) && !empty($orgId) && !empty($itemName) && !empty($price) && !empty($priceStartDate) && !empty($priceStartDate) && !empty($itemValidityEndDate) && !empty($itemType)) { // CHECK REQURIED FIELD CONDITION
        if (is_numeric($cgst) && is_numeric($sgst) && is_numeric($price)) {
            $whereCond = "";
            if (!empty($itemId)) {
                $whereCond = " AND PK_ITEM_ID !=" . $itemId;
            }
            $sqlCheckItemExist = "SELECT PK_ITEM_ID	FROM " . ITEMMASTER . " WHERE ITEM_NAME ='" . $itemName . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "'" . $whereCond . " AND FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR ITEM NAME IS ALREADY AVAILABLE OR NOT.
            $resCheckItemExist = fetch_rec_query($sqlCheckItemExist); // RESULT FOR ITEM NAME IS ALREADY AVAILABLE OR NOT.
            if (count($resCheckItemExist) > 0) {
                $result = array("status" => ITEMNAMEISEXIST);
                http_response_code(200);
            } else {
                $msg = "";
// ITEM INSERT ARRAY.
                $insertItemArray = Array(); //ITEM ARRAY CREATE
                $insertItemArray['FK_CLIENT_ID'] = $clientId;
                $insertItemArray['FK_ORG_ID'] = $orgId;
                $insertItemArray['ITEM_NAME'] = $itemName;
                $insertItemArray['ITEM_PRICE'] = $price;
                $insertItemArray['DESCRIPTION'] = $description;
                $insertItemArray['QTY'] = $qty;
                $insertItemArray['ITEM_TYPE'] = $itemType;
                $insertItemArray['PRICE_START_DATE'] = $priceStartDate;
                $insertItemArray['FK_USER_ID'] = $userId;
                $insertItemArray['ITEM_VALIDITY_END_DATE'] = $itemValidityEndDate;
                $insertItemArray['CGST'] = $cgst;
                $insertItemArray['SGST'] = $sgst;
                $insertItemArray['IGST'] = $igst;
                $insertItemArray['HSN_CODE'] = $hsnCode;
                $insertItemArray['TALLY_NAME'] = $tallyName;
                $insertItemArray['ITEM_SUB_TYPE'] = $itemSubType;
                $insertItemArray['EPOS_ITEM_ID'] = $eposItemId;
                $insertItemArray['EPOS_SPACE_ID'] = $eposSpaceId;
                if ($itemType == "EVENTS") {
                    $insertItemArray['EVENT_START_DATETIME'] = strtotime($eventStartDateTime);
                    $insertItemArray['EVENT_END_DATETIME'] = strtotime($eventEndDateTime);
                    $insertItemArray['EVENT_NO'] = generateId(EVENTMASTER, "EVE", $clientId);
                    $insertItemArray['EVENT_VENUE'] = $eventVenue;
                }

                if (isset($itemId) && $itemId > 0) {
                    $insertItemArray['CHANGED_BY'] = $userId;
                    $insertItemArray['CHANGED_BY_DATE'] = time();
                    $whereItemCond = "PK_ITEM_ID ='" . $itemId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                    $updateItem = update_rec(ITEMMASTER, $insertItemArray, $whereItemCond); // UPDATE RECORD IN ITEM_MASTER TABLE.
                    if (!$updateItem) {
                        $msg = UPDATEFAIL . " " . ITEMMASTER;
                    }
                } else {
                    $insertItemArray['CREATED_BY'] = $userId;
                    $insertItemArray['CREATED_BY_DATE'] = time();
                    $insertItem = insert_rec(ITEMMASTER, $insertItemArray); // INSERT RECORD IN ITEM_MASTER TABLE.          
                    if (!isset($insertItem['lastInsertedId']) || $insertItem['lastInsertedId'] == 0) {
                        $msg = INSERTFAIL . " " . ITEMMASTER;
                    }
                }

                if ($msg == "") {
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['itemType'] = $itemType;

                    $itemData = getItemsData($requestData); // GET ALL ROOM DATA.

                    $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];

                    $result = array("status" => SCS, "data" => $itemAllData);
                    http_response_code(200);
                } else {
                    $result = array("status" => $msg);
                    http_response_code(400);
                }
            }
        } else {
            $result = array("status" => ITEMDATAISALLOWNOLYNUMBER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR DELETE ITEM
function deleteItem($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $itemType = isset($postData['postData']['itemType']) ? addslashes(trim($postData['postData']['itemType'])) : "";

    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $today = date("Y-m-d");
    if (!empty($clientId) && !empty($orgId) && !empty($itemId)) {

        $checkItemAllotmentAvailabel = "SELECT PK_CHARGE_ID FROM " . CHARGEMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' AND IS_TRANSFER != '2'";

        $resStudentItemAvailabel = fetch_rec_query($checkItemAllotmentAvailabel);
        if (count($resStudentItemAvailabel) == 0) {
            $updateItemArray = Array(); //ITEM ARRAY UPDATE
            $updateItemArray['DELETE_FLAG'] = 1;
            $updateItemArray['CHANGED_BY'] = $userId;
            $updateItemArray['CHANGED_BY_DATE'] = time();

            $whereItemUpdate = " PK_ITEM_ID =" . $itemId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG = '0'";
            $updateItem = update_rec(ITEMMASTER, $updateItemArray, $whereItemUpdate); // UPDATE DELETE_FLAG =1 IN ITEM_MASTER TABLE.
            if ($updateItem) {

                if ($itemType == "Rental") {
                    $whereItemUpdate = " FK_ITEM_ID =" . $itemId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG = '0'";
                    $updateItem = update_rec(ROOMNOMASTER, $updateItemArray, $whereItemUpdate); // UPDATE DELETE_FLAG =1 IN ROOM_NO_MASTER TABLE.
                }
                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['itemType'] = $itemType;
                $roomData = getItemsData($requestData); // GET ALL ITEM DATA.

                $roomAllData = ($roomData['status'] == SCS) ? $roomData['data'] : $roomData['status'];
                $result = array("status" => SCS, "data" => $roomAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . ITEMMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => ITEMNOTDELETED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

// GET ITEM ALL DATA.
function getItemsData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $serviceId = isset($postData['postData']['serviceId']) ? addslashes(trim($postData['postData']['serviceId'])) : "";
    $itemType = isset($postData['postData']['itemType']) ? addslashes(trim($postData['postData']['itemType'])) : "";
    $eventDate = !empty($postData['postData']['eventDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['eventDate'])))) : "";

    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";
    $itemArr = array();
    $whereRoomCond = "";
    $todayDate = date('Y-m-d');

    if (!empty($itemType)) {
        $whereRoomCond = " AND IM.ITEM_TYPE='" . $itemType . "' AND IM.ITEM_VALIDITY_END_DATE >='" . $todayDate . "'";
    } else {
        $whereRoomCond = " AND IM.ITEM_TYPE !='RENTAL' AND IM.ITEM_TYPE!='EVENTS'";
    }

    $today = date("Y-m-d");
    if ($itemType == "EVENTS") {
        /* EVENT Date filter condition Start */
        $whereEventDate = "";
        if (!empty($eventDate)) {
            $whereEventDate = " AND '" . $eventDate . "' BETWEEN DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%Y-%m-%d') AND DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%Y-%m-%d')";
        }

        /* EVENT Date filter condition End */

        /* EVENT Status filter condition Start */
        $whereEventStatus = "";
        if ($status != "") {
            $whereEventStatus = " AND IM.IS_ACTIVE ='" . $status . "'";
        }

        /* EVENT Status filter condition End */


        //add by Saitsh Karena for master user Permission all data START
        $whereUserCond = "";
        if (!empty($userId)) {

            $requestData = array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['requestuserId'] = $userId;
            $getUserHierarchy = getUserHierarchy($requestData);
            //print_r($getUserHierarchy); exit;
            if ($getUserHierarchy['status'] == SCS) {
                $finalUserId = array();
                foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                    if ($valueUH['lastLevelFlag'] == 'N') {
                        foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                            if ($valueUD['lastLevelFlag'] == 'Y') {
                                $finalUserId[$keyUH] = $valueUD['userId'];
                            }
                        }
                    } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                        $finalUserId[$keyUH] = $valueUH['userId'];
                    }
                }

                $userIds = implode(",", $finalUserId);
            } else if ($userId == "-1") {
                //add by Saitsh Karena for see master user all data
                $finalUserId = array();
                $getUserDetail = getUserDetail($clientId);

                for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                    $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                    $finalUserId[$ii] = $idOfUser;
                }
                $userIds = implode(",", array_unique($finalUserId));
            }
            $whereUserCond = " AND IM.CREATED_BY IN (" . $userIds . ")";
        }

//add by Saitsh Karena for master user Permission all data END

        $sqlItemData = "  SELECT IM.PK_ITEM_ID,IM.ITEM_NAME,IM.ITEM_PRICE,IM.DESCRIPTION,IM.QTY,IM.FK_CLIENT_ID,IM.FK_ORG_ID,IM.FK_USER_ID,IM.PRICE_START_DATE,IM.ITEM_VALIDITY_END_DATE,IM.CGST,IM.SGST,IM.IGST,IM.ITEM_TYPE,IM.HSN_CODE,IM.TALLY_NAME,IM.EVENT_START_DATETIME,IM.EVENT_END_DATETIME,EVENT_VENUE,IM.IS_ACTIVE,IM.EVENT_NO,(SELECT IF(COUNT(QTY) > 0,IM.QTY-SUM(QTY),IM.QTY) FROM " . EVENTSBOOKING . " WHERE FK_ITEM_ID =IM.PK_ITEM_ID GROUP BY(FK_ITEM_ID)) AS REMAINING_SEATS,IM.MAX_NOS_OF_BOOK  FROM " . ITEMMASTER . " IM   WHERE IM.DELETE_FLAG =0 " . $whereRoomCond . " AND IM.FK_CLIENT_ID ='" . $clientId . "'  AND IM.FK_ORG_ID ='" . $orgId . "' " . $whereEventDate . " " . $whereEventStatus . " AND IM.ITEM_VALIDITY_END_DATE >= '" . $today . "'  ORDER BY IM.PK_ITEM_ID DESC"; // QUERY FOR ITEM DATA.
    } else {
        $sqlItemData = "  SELECT IM.PK_ITEM_ID,IM.ITEM_NAME,IM.ITEM_PRICE,IM.DESCRIPTION,IM.QTY,IM.FK_CLIENT_ID,IM.FK_ORG_ID,IM.FK_USER_ID,IM.PRICE_START_DATE,IM.ITEM_VALIDITY_END_DATE,IM.CGST,IM.SGST,IM.IGST,IM.ITEM_TYPE,IM.HSN_CODE,IM.TALLY_NAME,IM.EVENT_START_DATETIME,IM.EVENT_END_DATETIME,EVENT_VENUE,IM.IS_ACTIVE,IM.EVENT_NO,IM.EPOS_ITEM_ID,IM.EPOS_SPACE_ID FROM " . ITEMMASTER . " IM  WHERE IM.DELETE_FLAG =0 " . $whereRoomCond . " AND IM.FK_CLIENT_ID ='" . $clientId . "'  AND IM.FK_ORG_ID ='" . $orgId . "' AND IM.ITEM_VALIDITY_END_DATE >= '" . $today . "' ORDER BY IM.PK_ITEM_ID DESC"; // QUERY FOR ITEM DATA.
    }

    $resItemData = fetch_rec_query($sqlItemData); // RESULT FOR ITEM DATA.
    if (count($resItemData) > 0) {
        foreach ($resItemData as $keyItem => $valueItem) { // ITEM DATA LOOP
            $itemArr[$keyItem]['itemId'] = $valueItem['PK_ITEM_ID'];
            $itemArr[$keyItem]['itemName'] = $valueItem['ITEM_NAME'];
            $itemArr[$keyItem]['itemPrice'] = $valueItem['ITEM_PRICE'];
            $itemArr[$keyItem]['qty'] = $valueItem['QTY'];
            $itemArr[$keyItem]['description'] = $valueItem['DESCRIPTION'];
            $itemArr[$keyItem]['clientId'] = $valueItem['FK_CLIENT_ID'];
            $itemArr[$keyItem]['orgId'] = $valueItem['FK_ORG_ID'];
            $itemArr[$keyItem]['userId'] = $valueItem['FK_USER_ID'];
            $itemArr[$keyItem]['priceStartDate'] = $valueItem['PRICE_START_DATE'];
            $itemArr[$keyItem]['itemValidityEndDate'] = $valueItem['ITEM_VALIDITY_END_DATE'];
            $itemArr[$keyItem]['cgst'] = $valueItem['CGST'];
            $itemArr[$keyItem]['sgst'] = $valueItem['SGST'];
            $itemArr[$keyItem]['igst'] = $valueItem['IGST'];
            $itemArr[$keyItem]['itemType'] = $valueItem['ITEM_TYPE'];
            $itemArr[$keyItem]['itemTypeDisplayName'] = (($valueItem['ITEM_TYPE'] == "SUBSC") ? "Subscription" : (($valueItem['ITEM_TYPE'] == "MISC") ? "Misc." : (($valueItem['ITEM_TYPE'] == "EVENTS") ? "Events" : "Others")));
            $itemArr[$keyItem]['hsnCode'] = $valueItem['HSN_CODE'];
            $itemArr[$keyItem]['tallyName'] = $valueItem['TALLY_NAME'];
            $itemArr[$keyItem]['status'] = $valueItem['IS_ACTIVE'];
            if ($valueItem['ITEM_TYPE'] == "SUBSC") {
                $itemArr[$keyItem]['eposItemId'] = $valueItem['EPOS_ITEM_ID'];
                $itemArr[$keyItem]['eposSpaceId'] = $valueItem['EPOS_SPACE_ID'];
            }
            if ($itemType == "EVENTS") {

                $itemArr[$keyItem]['eventStartDateTime'] = date("Y-m-d h:i:s A", $valueItem['EVENT_START_DATETIME']);
                $itemArr[$keyItem]['eventEndDateTime'] = date("Y-m-d h:i:s A", $valueItem['EVENT_END_DATETIME']);
                $itemArr[$keyItem]['eventVenue'] = $valueItem['EVENT_VENUE'];
                $itemArr[$keyItem]['maxNosOfBook'] = $valueItem['MAX_NOS_OF_BOOK'];
                $itemArr[$keyItem]['eventNo'] = $valueItem['EVENT_NO'];
                $itemArr[$keyItem]['remainingSeats'] = ($valueItem['REMAINING_SEATS'] == "" || $valueItem['REMAINING_SEATS'] == null) ? $valueItem['QTY'] : $valueItem['REMAINING_SEATS'];
            }
            if ($valueItem['ITEM_TYPE'] == "RENTAL") {
                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['itemId'] = $valueItem['PK_ITEM_ID'];
                $roomData = roomList($requestData); // GET ALL ROOMS DATA.

                $roomAllData = ($roomData['status'] == SCS) ? $roomData['data'] : NORECORDS;
                $itemArr[$keyItem]['roomsArr'] = $roomAllData;
            }
        }
        $result = array("status" => SCS, "data" => $itemArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// LIST FOR ALL DEPARTMENT NAME.
function getDepartmentServiceList($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";

    $sqlDepartmentData = "SELECT PK_CLT_LKUP_ID,DISPLAY_VALUE FROM " . CLIENTLOOKUP . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE  LOOK_TYPE  ='DEPARTMENT' AND DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "')"; // QUERY FOR DEPARTMENT SERVICE LIST NAME

    $resDepartmentData = fetch_rec_query($sqlDepartmentData); // RESULT FOR DEPARTMENT SERVICE LIST NAME
    if (count($resDepartmentData) > 0) {
        $departmentArr = Array();
        foreach ($resDepartmentData as $keyDepartment => $valueDepartment) { // ITEM DATA FOREACH LOOP.
            $departmentArr[$keyDepartment]['departmentId'] = $valueDepartment['PK_CLT_LKUP_ID'];
            $departmentArr[$keyDepartment]['departmentName'] = $valueDepartment['DISPLAY_VALUE'];
        }
        $result = array("status" => SCS, "data" => array_values($departmentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function addEditChargePosting($postData) {

// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $typeOfCust = isset($postData['postData']['typeOfCust']) ? addslashes(trim($postData['postData']['typeOfCust'])) : "";
    $studentArr = isset($postData['postData']['studentArr']) ? $postData['postData']['studentArr'] : array();

    if (!empty($clientId) && !empty($orgId) != "" && !empty($studentArr)) { // CHECK REQURIED FIELD CONDITION
        if (!empty($studentArr)) {
            foreach ($studentArr as $keyStudent => $valueStudent) {
                $checkItemData = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_PRICE,ITEM_TYPE,CGST,SGST,IGST,HSN_CODE FROM " . ITEMMASTER . " WHERE DELETE_FLAG='0' AND PK_ITEM_ID ='" . $valueStudent['itemId'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                $resCheckItemData = fetch_rec_query($checkItemData);
                if (count($resCheckItemData) > 0) {
                    foreach ($resCheckItemData as $keyItem => $valueItem) {

                        $itemName = $valueItem['ITEM_NAME'];
                        $itemPrice = $valueItem['ITEM_PRICE'];
                        $taxCgst = $valueItem['CGST'];
                        $taxSgst = $valueItem['SGST'];
                        $taxIgst = $valueItem['IGST'];
                        $hsnCode = $valueItem['HSN_CODE'];
                    }
                    $msg = "";
                    $calItemBasicTPrice = $itemPrice * $valueStudent['qty'];
                    $cgstAmt = ($taxCgst > 0) ? ($calItemBasicTPrice * $taxCgst) / 100 : 0;
                    $sgstAmt = ($taxSgst > 0) ? ($calItemBasicTPrice * $taxSgst) / 100 : 0;
                    $igstAmt = ($taxIgst > 0) ? ($calItemBasicTPrice * $taxIgst) / 100 : 0;

                    $calTotalTax = $cgstAmt + $sgstAmt + $igstAmt;
                    $calItemTotalPrice = $calItemBasicTPrice + $calTotalTax;

// CHARGE POSTING INSERT ARRAY.
                    $insertChargePostingArray = Array(); //CHARGE POSTING ARRAY CREATE
                    $insertChargePostingArray['FK_CLIENT_ID'] = $clientId;
                    $insertChargePostingArray['FK_ORG_ID'] = $orgId;
                    $insertChargePostingArray['TYPEOFCUST'] = $typeOfCust;
                    $insertChargePostingArray['FK_ITEM_ID'] = $valueStudent['itemId'];
                    $insertChargePostingArray['REF_ITEM_ID'] = $valueStudent['itemId'];
                    $insertChargePostingArray['ITEM_NAME'] = $valueStudent['itemName'];
                    $insertChargePostingArray['CHARGE_DATE'] = ($valueStudent['chargeDate']) ? date("Y-m-d", strtotime(addslashes(trim($valueStudent['chargeDate'])))) : "";
                    $insertChargePostingArray['STUDENT_ID'] = $valueStudent['studentId'];
                    $insertChargePostingArray['STUDENT_FULL_NAME'] = $valueStudent['studentName'];
                    $insertChargePostingArray['QTY_OF_SESSION'] = $valueStudent['qty'];
                    $insertChargePostingArray['BIILED_SESSION'] = $valueStudent['qty'];
                    $insertChargePostingArray['TAX_CGST'] = $taxCgst;
                    $insertChargePostingArray['TAX_SGST'] = $taxSgst;
                    $insertChargePostingArray['TAX_IGST'] = $taxIgst;
                    $insertChargePostingArray['HSN_CODE'] = $hsnCode;
                    $insertChargePostingArray['ITEM_BASE_PRICE'] = $itemPrice;
                    $insertChargePostingArray['TOTAL_TAX'] = $calTotalTax;
                    $insertChargePostingArray['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;
                    $insertChargePostingArray['TYPE'] = 'MISC';

                    if (isset($chargePostingId) && $chargePostingId > 0) {
                        $insertChargePostingArray['CHANGED_BY'] = $userId;
                        $insertChargePostingArray['CHANGED_BY_DATE'] = time();
                        $whereChargePostingCond = "PK_CHARGE_ID ='" . $chargePostingId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                        $updateChargePosting = update_rec(CHARGEMASTER, $insertChargePostingArray, $whereChargePostingCond); // UPDATE RECORD IN CHARGE_MASTER TABLE.
                        if (!$updateChargePosting) {
                            $msg = UPDATEFAIL . " " . CHARGEMASTER;
                        }
                    } else {
                        $insertChargePostingArray['CREATED_BY'] = $userId;
                        $insertChargePostingArray['CREATED_BY_DATE'] = time();
                        $insertChargePosting = insert_rec(CHARGEMASTER, $insertChargePostingArray); // INSERT RECORD IN CHARGE_MASTER TABLE.    
                        if (!isset($insertChargePosting['lastInsertedId']) || $insertChargePosting['lastInsertedId'] == 0) {
                            $msg = INSERTFAIL . " " . CHARGEMASTER;
                        } else {
                            $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;

                            $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT,CLOSINGBAL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $valueStudent['studentId'] . "'";
                            $resStudentData = fetch_rec_query($sqlStudentData); // 
                            if (count($resStudentData) > 0) {
                                $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                                $billedAmt = $resStudentData[0]['BILL_AMT'];
                                $closingBalAmt = $resStudentData[0]['CLOSINGBAL_AMT'];
                                $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                            }


                            $updateStudentArr = ARRAY();
                            $updateStudentArr['UNBILLED_AMT'] = floatval($calItemTotalPrice) + floatval($unbilledAmt);
                            $updateStudentArr['UNBILLED_TYPE'] = 'DR';
                            $updateStudentArr['CHANGED_BY'] = $userId;
                            $updateStudentArr['CHANGED_BY_DATE'] = time();
                            $whereStudCond = "PK_STUD_ID='" . $valueStudent['studentId'] . "'";
                            $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                            if (!$updateStud) {
                                $msg = UPDATEFAIL . " " . STUDENTMASTER;
                            }
                        }
                    }
                } else {
                    $result = array("status" => ITEMNOTFOUND . "==" . $valueStudent['itemId']);
                    http_response_code(400);
                }
            }
        }
        if ($msg == "") {
            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $itemData = listChargePosting($requestData); // GET ALL NON PROCESSING CHARGE POSTING DATA.

            $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
            $result = array("status" => SCS, "data" => $itemAllData);
            http_response_code(200);
        } else {
            $result = array("status" => $msg);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function listChargePosting($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $chargePostingId = isset($postData['postData']['chargePostingId']) ? addslashes(trim($postData['postData']['chargePostingId'])) : "";

    $whereStudCond = "";
    if (!empty($studentId)) {
        $whereStudCond = " AND CM.STUDENT_ID ='" . $studentId . "'";
    }
    $whereChargePostingCond = "";
    if (!empty($chargePostingId)) {
        $whereChargePostingCond = " AND CM.PK_CHARGE_ID ='" . $chargePostingId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond1 = " AND CM.CREATED_BY IN (" . $userIds . ")";
    }
    $whereUserCond = "";
    //add by Saitsh Karena for master user Permission all data END

    $sqlChargeMasterDataWithOutInvoice = "SELECT CM.PK_CHARGE_ID,CM.FK_CLIENT_ID,CM.FK_ORG_ID,CM.TYPEOFCUST,CM.FK_ITEM_ID,CM.REF_ITEM_ID,CM.ITEM_NAME,CM.CHARGE_DATE,CM.STUDENT_ID,CM.STUDENT_FULL_NAME,CM.QTY_OF_SESSION,CM.BIILED_SESSION,CM.TAX_CGST,CM.TAX_SGST,CM.TAX_IGST,CM.ITEM_BASE_PRICE,CM.TOTAL_TAX,CM.ITEM_TOTAL_PRICE,CM.HSN_CODE,SM.MEMBERSHIP_NUMBER,IM.ITEM_TYPE  FROM " . CHARGEMASTER . " CM LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID = CM.STUDENT_ID AND SM.DELETE_FLAG ='0' LEFT JOIN " . ITEMMASTER . " IM ON IM.PK_ITEM_ID = CM.FK_ITEM_ID AND IM.DELETE_FLAG ='0'  WHERE CM.DELETE_FLAG =0 AND CM.FK_CLIENT_ID ='" . $clientId . "' AND CM.FK_ORG_ID ='" . $orgId . "' " . $whereStudCond . $whereChargePostingCond . $whereUserCond . " AND CM.IS_TRANSFER ='0' AND CM.INVOICE_FLAG ='0' AND (CM.TYPE ='MISC' OR CM.TYPE ='EVENTS') ORDER BY CM.PK_CHARGE_ID DESC"; // QUERY FOR CHARGE MASTER DATA WITHOUT INVOICE


    $resChargeMasterDataWithOutInvoice = fetch_rec_query($sqlChargeMasterDataWithOutInvoice); // RESULT FOR CHARGE MASTER DATA WITHOUT INVOICE
    $chargeMasterArr = $tempArr = Array();
    if (count($resChargeMasterDataWithOutInvoice) > 0) {
        foreach ($resChargeMasterDataWithOutInvoice as $keyChargeMaster => $valueChargeMaster) { // CHARGE MASTER DATA WITHOUT INVOICE DATA FOREACH LOOP.
            $chargeMasterArr[$keyChargeMaster]['chargeId'] = $valueChargeMaster['PK_CHARGE_ID'];
            $chargeMasterArr[$keyChargeMaster]['clientId'] = $valueChargeMaster['FK_CLIENT_ID'];
            $chargeMasterArr[$keyChargeMaster]['ogrId'] = $valueChargeMaster['FK_ORG_ID'];
            $chargeMasterArr[$keyChargeMaster]['typeOfCust'] = $valueChargeMaster['TYPEOFCUST'];
            $chargeMasterArr[$keyChargeMaster]['itemName'] = $valueChargeMaster['ITEM_NAME'];
            $chargeMasterArr[$keyChargeMaster]['itemId'] = $valueChargeMaster['FK_ITEM_ID'];
            $chargeMasterArr[$keyChargeMaster]['itemType'] = $valueChargeMaster['ITEM_TYPE'];
            $chargeMasterArr[$keyChargeMaster]['refItemId'] = $valueChargeMaster['REF_ITEM_ID'];
            $chargeMasterArr[$keyChargeMaster]['chargeDate'] = $valueChargeMaster['CHARGE_DATE'];
            $chargeMasterArr[$keyChargeMaster]['studId'] = $valueChargeMaster['STUDENT_ID'];
            $chargeMasterArr[$keyChargeMaster]['studName'] = $valueChargeMaster['STUDENT_FULL_NAME'];
            $chargeMasterArr[$keyChargeMaster]['studMembershipNo'] = $valueChargeMaster['MEMBERSHIP_NUMBER'];
            $chargeMasterArr[$keyChargeMaster]['qtyOfSession'] = $valueChargeMaster['QTY_OF_SESSION'];
            $chargeMasterArr[$keyChargeMaster]['billedSession'] = $valueChargeMaster['BIILED_SESSION'];
            $chargeMasterArr[$keyChargeMaster]['taxCgst'] = $valueChargeMaster['TAX_CGST'];
            $chargeMasterArr[$keyChargeMaster]['taxIgst'] = $valueChargeMaster['TAX_IGST'];
            $chargeMasterArr[$keyChargeMaster]['taxSgst'] = $valueChargeMaster['TAX_SGST'];
            $chargeMasterArr[$keyChargeMaster]['hsnCode'] = $valueChargeMaster['HSN_CODE'];
            $chargeMasterArr[$keyChargeMaster]['itemBasicPrice'] = $valueChargeMaster['ITEM_BASE_PRICE'];
            $chargeMasterArr[$keyChargeMaster]['itemTotalTax'] = $valueChargeMaster['TOTAL_TAX'];
            $chargeMasterArr[$keyChargeMaster]['itemTotalPrice'] = $valueChargeMaster['ITEM_TOTAL_PRICE'];
        }
        $result = array("status" => SCS, "data" => array_values($chargeMasterArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    return $result;
}

// GET CHARGE POSTING DATA FOR INVOICE CREATE.
function getPostingChargeData($postData) {

    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $chargeStartDate = !empty($postData['postData']['chargeStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeStartDate'])))) : "";
    $chargeEndDate = !empty($postData['postData']['chargeEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeEndDate'])))) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $chargeType = isset($postData['postData']['chargeType']) ? addslashes(trim($postData['postData']['chargeType'])) : "";
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : "";
    $mailSendType = isset($postData['postData']['mailSendType']) ? addslashes(trim($postData['postData']['mailSendType'])) : "";
    $whereStudentCond = $whereDateRange = $whereChargeCond = $wherChargeDate = "";

    if ($chargeStartDate == "") {
        $wherChargeDate = " AND CHARGE_DATE <='" . $chargeEndDate . "'";
    } else {
        $wherChargeDate = " AND CHARGE_DATE BETWEEN '" . $chargeStartDate . "' AND '" . $chargeEndDate . "'";
    }

    if (!empty($studentId)) {
        $whereStudentCond = " AND STUDENT_ID='" . $studentId . "'";
    }

    if (!empty($chargeType)) {
        if ($mailSendType == 1)
            $whereChargeType = " AND (TYPE ='" . $chargeType . "' OR TYPE='EVENTS') ";
        else
            $whereChargeCond = " AND TYPE='" . $chargeType . "'";
    }
    if (!empty($customerId)) {
        $whereStudentCond = " AND TYPEOFCUST ='CUSTOMER' AND STUDENT_ID='" . $customerId . "'";
    }


// CHARGE_DATE
    $sqlChargeMasterDataWithOutInvoice = "SELECT FK_ITEM_ID,STUDENT_ID,GROUP_CONCAT(PK_CHARGE_ID SEPARATOR ',')AS CHARGE_IDS,ITEM_BASE_PRICE,SUM(QTY_OF_SESSION)AS TOTAL_QTY,SUM(TOTAL_TAX)AS TOTAL_TAX,SUM(ITEM_TOTAL_PRICE)AS ITEM_TOTAL_PRICE,ITEM_NAME,TAX_CGST,TAX_SGST,TAX_IGST,TYPE,HSN_CODE FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND IS_TRANSFER ='0' AND INVOICE_FLAG ='0' " . $wherChargeDate . $whereStudentCond . $whereChargeCond . " GROUP BY FK_ITEM_ID"; // QUERY FOR CHARGE MASTER DATA WITHOUT INVOICE 

    $resChargeMasterDataWithOutInvoice = fetch_rec_query($sqlChargeMasterDataWithOutInvoice); // RESULT FOR CHARGE MASTER DATA WITHOUT INVOICE
    $chargeMasterArr = $tempArr = Array();
    if (count($resChargeMasterDataWithOutInvoice) > 0) {
        foreach ($resChargeMasterDataWithOutInvoice as $keyChargeMaster => $valueChargeMaster) { // CHARGE MASTER DATA WITHOUT INVOICE DATA FOREACH LOOP.
            $chargeMasterArr[$keyChargeMaster]['studentId'] = $valueChargeMaster['STUDENT_ID'];
            $chargeMasterArr[$keyChargeMaster]['itemName'] = $valueChargeMaster['ITEM_NAME'];
            $chargeMasterArr[$keyChargeMaster]['itemId'] = $valueChargeMaster['FK_ITEM_ID'];
            $chargeMasterArr[$keyChargeMaster]['chargeIds'] = $valueChargeMaster['CHARGE_IDS'];
            $chargeMasterArr[$keyChargeMaster]['totalQty'] = $valueChargeMaster['TOTAL_QTY'];
            $chargeMasterArr[$keyChargeMaster]['itemBasicPrice'] = $valueChargeMaster['ITEM_BASE_PRICE'];
            $chargeMasterArr[$keyChargeMaster]['totalTax'] = $valueChargeMaster['TOTAL_TAX'];
            $chargeMasterArr[$keyChargeMaster]['totalItemPrice'] = $valueChargeMaster['ITEM_TOTAL_PRICE'];
            $chargeMasterArr[$keyChargeMaster]['cgst'] = $valueChargeMaster['TAX_CGST'];
            $chargeMasterArr[$keyChargeMaster]['sgst'] = $valueChargeMaster['TAX_SGST'];
            $chargeMasterArr[$keyChargeMaster]['igst'] = $valueChargeMaster['TAX_IGST'];
            $chargeMasterArr[$keyChargeMaster]['type'] = $valueChargeMaster['TYPE'];
            $chargeMasterArr[$keyChargeMaster]['hsnCode'] = $valueChargeMaster['HSN_CODE'];
        }
        $result = array("status" => SCS, "data" => array_values($chargeMasterArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// CREATE INVOICE API.
function createInvoice($postData) {

    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $invoiceStartDate = !empty($postData['postData']['invoiceStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['invoiceStartDate'])))) : "";
    $invoiceEndDate = !empty($postData['postData']['invoiceEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['invoiceEndDate'])))) : "";
    $chargeStartDate = !empty($postData['postData']['chargeStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeStartDate'])))) : "";
    $chargeEndDate = !empty($postData['postData']['chargeEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['chargeEndDate'])))) : "";
    $invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $invoiceType = isset($postData['postData']['invoiceType']) ? addslashes(trim($postData['postData']['invoiceType'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $chargeType = isset($postData['postData']['chargeType']) ? addslashes(trim($postData['postData']['chargeType'])) : "";
    $mailSendType = isset($postData['postData']['mailSend']) ? addslashes(trim($postData['postData']['mailSend'])) : "0"; // WHERE MAILSENDTYPE = 1 then call send mail function otherwise not call .
    // $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "1";

    $chargeStartDate = "";

    if (!empty($chargeEndDate) && !empty($invoiceName)) {

        if (strtotime($chargeStartDate) <= strtotime($chargeEndDate) || $chargeStartDate == "") {
            $whereStudentCond = "";

            if (!empty($studentId)) {
                $whereStudentCond = " AND STUDENT_ID='" . $studentId . "'";
            }

            if (empty($chargeStartDate) && $chargeStartDate == "") {
                $wherChargeDate = " AND CHARGE_DATE <='" . $chargeEndDate . "'";
            } else {
                $wherChargeDate = " AND CHARGE_DATE BETWEEN '" . $chargeStartDate . "' AND '" . $chargeEndDate . "'";
            }

            $whereChargeType = "";

            if (!empty($chargeType)) {
                if ($mailSendType == 1)
                    $whereChargeType = " AND (TYPE ='" . $chargeType . "' OR TYPE='EVENTS') ";
                else
                    $whereChargeType = " AND TYPE ='" . $chargeType . "'";
            }


            $sqlChargeStudentData = "SELECT STUDENT_FULL_NAME,STUDENT_ID FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND IS_TRANSFER ='0' AND INVOICE_FLAG ='0'  " . $wherChargeDate . $whereStudentCond . $whereChargeType . " GROUP BY STUDENT_ID"; // QUERY    FOR CHARGE MASTER DATA WITHOUT INVOICE


            $resChargeStudentData = fetch_rec_query($sqlChargeStudentData);
            if (!empty($resChargeStudentData) && count($resChargeStudentData) > 0) {
                if (count($resChargeStudentData) > 1) {
                    $mailSend = 0;
                } else {
                    $mailSend = 1;
                }
                foreach ($resChargeStudentData as $keyStudent => $valueStudent) {// STUDENT DAT FOR EACH LOOP.
                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['chargeStartDate'] = $chargeStartDate;
                    $requestData['postData']['chargeEndDate'] = $chargeEndDate;
                    $requestData['postData']['studentId'] = $valueStudent['STUDENT_ID'];
                    $requestData['postData']['chargeType'] = $chargeType;
                    $requestData['postData']['mailSendType'] = $mailSendType;

                    $chargeMasterData = getPostingChargeData($requestData); // GET ALL CHARGE MASTER DATA.



                    if ($chargeMasterData['status'] == SCS) {

                        $invoiceType = $chargeMasterData['data'][0]['type'];
                        $chargeMasterArray = $chargeMasterData['data'];
                        $insertInvoiceMaster = Array();

                        $clientInvoiceId = generateId(INVOICEMASTER, 'INV', $clientId);
                        $insertInvoiceMaster['FK_CLIENT_ID'] = $clientId;
                        $insertInvoiceMaster['FK_ORG_ID'] = $orgId;
                        $insertInvoiceMaster['FK_STUDENT_ID'] = $valueStudent['STUDENT_ID'];
                        $insertInvoiceMaster['C_INVOICE_ID'] = $clientInvoiceId;
                        $insertInvoiceMaster['INVOICE_DATE'] = date("Y-m-d");
                        $insertInvoiceMaster['INVOICE_NAME'] = $invoiceName;
                        $insertInvoiceMaster['INVOICE_TYPE'] = $invoiceType;
                        $insertInvoiceMaster['INVOICE_REF_ID'] = $itemId;
                        $insertInvoiceMaster['TOTAL_CHARGES'] = '';
                        $insertInvoiceMaster['PERIOD_FROM'] = $invoiceStartDate;
                        $insertInvoiceMaster['PERIOD_TO'] = $invoiceEndDate;
                        $insertInvoiceMaster['CREATED_BY'] = $userId;
                        $insertInvoiceMaster['CREATED_BY_DATE'] = time();
                        $insertInvoiceMasterData = insert_rec(INVOICEMASTER, $insertInvoiceMaster);
                        if (isset($insertInvoiceMasterData['lastInsertedId']) && $insertInvoiceMasterData['lastInsertedId'] > 0) {

                            $invoiceNo = $insertInvoiceMasterData['lastInsertedId'];
                            $totalPrice = 0;
                            foreach ($chargeMasterArray as $keyCharge => $valueCharge) { // CHARGE MASTER TRANS ARRAY LOOP.
                                if ($valueCharge['totalQty'] > 0 && $valueCharge['itemName'] != "") {
// INVOICE TRANS ARRAY.
                                    $insertInvoiceTrans = array();
                                    $insertInvoiceTrans['FK_INVOICE_ID'] = $clientInvoiceId;
                                    $insertInvoiceTrans['CHARGE_TYPE'] = 'STUDENT';
                                    $insertInvoiceTrans['QTY'] = $valueCharge['totalQty'];
                                    $insertInvoiceTrans['FK_STUDENT_ID'] = $valueStudent['STUDENT_ID'];
                                    $insertInvoiceTrans['FK_ITEM_ID'] = $valueCharge['itemId'];
                                    $insertInvoiceTrans['REF_ITEM_ID'] = $valueCharge['itemId'];
                                    $insertInvoiceTrans['ITEM_NAME'] = $valueCharge['itemName'];
                                    $insertInvoiceTrans['SGST'] = $valueCharge['sgst'];
                                    $insertInvoiceTrans['CGST'] = $valueCharge['cgst'];
                                    $insertInvoiceTrans['IGST'] = $valueCharge['igst'];
                                    $insertInvoiceTrans['HSN_CODE'] = $valueCharge['hsnCode'];

                                    $insertInvoiceTrans['TOTAL_CHARGES'] = ($valueCharge['itemBasicPrice'] * $valueCharge['totalQty']);
                                    $insertInvoiceTrans['TOTAL_TAX'] = $valueCharge['totalTax'];
                                    $insertInvoiceTrans['PAYABLE_AMOUNT'] = $valueCharge['totalItemPrice'];
                                    $insertInvoiceTrans['CREATED_BY'] = $userId;
                                    $insertInvoiceTrans['CREATED_BY_DATE'] = time();
                                    $insertInvoiceTransData = insert_rec(INVOICETRANSMASTER, $insertInvoiceTrans);

                                    $totalPrice = $totalPrice + $valueCharge['totalItemPrice']; // TOTAL PAYABLE AMT

                                    $chargeIdsArr = ($valueCharge['chargeIds'] != "") ? explode(",", $valueCharge['chargeIds']) : Array();
                                    if (count($chargeIdsArr) > 0) { // UPDATE  INVOICE_FLAG =1 AND INVOICE ID IN CHARGE MASTER DATA 
                                        foreach ($chargeIdsArr as $keyCharge => $valueChargeIds) { // CHARGE IDS ARRAY LOOP.    
// UPDATE CHARGE ARRAY.
                                            $updateChargeArr = Array();
                                            $updateChargeArr['INVOICE_FLAG'] = 1;
                                            $updateChargeArr['INVOICE_ID'] = $clientInvoiceId;

                                            $whereChargeCond = "PK_CHARGE_ID='" . $valueChargeIds . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; // WHERE CHARGE ID COND.
                                            $updateInvoiceMaster = update_rec(CHARGEMASTER, $updateChargeArr, $whereChargeCond); // UPDATE IN CHARGE_MASTER TABLE.
                                        }
                                    }
                                }
                            }

                            // UPDATE TOTAL CHARGE INTO INVOICE MASTER TABLE AFTER LOOP
                            $updateInvoiceArr = Array();
                            $updateInvoiceArr['TOTAL_CHARGES'] = $totalPrice;
                            $whereInvoiceCond = "PK_INVOICE_ID='" . $invoiceNo . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                            $updateInvoiceMaster = update_rec(INVOICEMASTER, $updateInvoiceArr, $whereInvoiceCond); // UPDATE INVOICE TOTAL IN INVOICE MASTER TABLE.
                            // UPDATE DATA INTO STUDENT TABLE
                            // 1. BILLED AMT / TYPE
                            // 2. CB BALANCE / TYPE
                            $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;

                            $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $valueStudent['STUDENT_ID'] . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                            $resStudentData = fetch_rec_query($sqlStudentData); // 
                            if (count($resStudentData) > 0) {
                                $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                                $billedAmt = $resStudentData[0]['BILL_AMT'];
                                $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
                            }

                            $closingBalAmt = (floatval($billedAmt) + floatval($totalPrice)) - floatval($receiptAmt);
                            $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR"; // IF CB IS IN - THEN CR OF STUDENT ELSE DR OF STUDENT

                            $updateStudentArr = ARRAY();
                            $updateStudentArr['BILL_AMT'] = floatval($totalPrice) + floatval($billedAmt);
                            $updateStudentArr['UNBILLED_AMT'] = (floatval($unbilledAmt) - floatval($totalPrice));
                            $updateStudentArr['BILL_TYPE'] = 'DR';
                            $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
                            $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;
                            $updateStudentArr['CHANGED_BY'] = $userId;
                            $updateStudentArr['CHANGED_BY_DATE'] = time();
                            $whereStudCond = "PK_STUD_ID='" . $valueStudent['STUDENT_ID'] . "'";
                            $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);

                            if ($mailSendType == 1) {
                                $finalInvoiceIdArr = Array();
                                array_push($finalInvoiceIdArr, $clientInvoiceId);
                                $requestData = $studentData = Array();
                                $requestData['postData']['clientId'] = $clientId;
                                $requestData['postData']['orgId'] = $orgId;
                                $requestData['postData']['userId'] = $userId;
                                $requestData['postData']['studentId'] = $valueStudent['STUDENT_ID'];

                                $requestData['postData']['mailSend'] = $mailSend;
                                if ($invoiceType == 'EVENTS') {
                                    $requestData['postData']['invoiceId'] = $clientInvoiceId;
                                    $chargeMasterData = sendEventMail($requestData); // EVENTS MAIL SENT CODE
                                } else {
                                    $requestData['postData']['invoiceId'] = $finalInvoiceIdArr;
                                    $chargeMasterData = sendGeneralInvoiceMail($requestData);  // GENERAL MAIL SENT CODE   
                                }
                                $requestData['postData']['clientId'] = $clientId;
                                $requestData['postData']['orgId'] = $orgId;
                                $requestData['postData']['userId'] = $userId;
                                $chargeMasterData = createCreditNotes($requestData);
                            }
                        }
                    }
                }

                $result = array("status" => SCS, "data" => $clientInvoiceId);
                http_response_code(200);
            } else {

                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => STARTISNOTGREATERTHANENDDATE);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

// LIST INVOICE DATA API.
function listInvoiceData($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : ""; //  IF TYPE = CUSTOMER THEN PROPERTY INVOICE OTHERWISE ALL INVOICE
    $customerId = isset($postData['postData']['customerId']) ? addslashes(trim($postData['postData']['customerId'])) : ""; //  IF TYPE = CUSTOMER THEN PROPERTY INVOICE OTHERWISE ALL INVOICE

    $whereInvoiceTypeCond = "";
    if (!empty($type) && $type == 'customer') {
        $whereInvoiceTypeCond = " AND IM.INVOICE_TYPE ='PROPERTY'";
    }
    $whereInvoiceIdCond = "";
    if (!empty($invoiceId)) {
        $whereInvoiceIdCond = " AND IM.PK_INVOICE_ID ='" . $invoiceId . "'";
    }

    $whereInvoiceCond = "";
    if (!empty($invoiceName)) {
        $whereInvoiceCond = " AND IM.INVOICE_NAME ='" . $invoiceName . "'";
    }
    $whereStudCond = "";
    if (!empty($studentId)) {
        $whereStudCond = " AND IM.FK_STUDENT_ID ='" . $studentId . "'";
    }
    $whereCustCond = "";
    if (!empty($customerId)) {
        $whereCustCond = " AND IM.FK_CUST_ID ='" . $customerId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND IM.CREATED_BY IN (" . $userIds . ")";
    }
    //add by Saitsh Karena for master user Permission all data END

    $sqlInvoiceData = "SELECT IF(IM.INVOICE_TYPE !='PROPERTY',IM.FK_STUDENT_ID,IM.FK_CUST_ID) AS FK_STUDENT_ID,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME ) AS STUD_NAME,IM.PK_INVOICE_ID,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.INVOICE_NAME,IM.PERIOD_FROM,IM.PERIOD_TO,IM.TOTAL_CHARGES,IM.INVOICE_TYPE,IF(IM.INVOICE_TYPE !='PROPERTY',CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME ),CONCAT(CM.FIRST_NAME,' ',CM.LAST_NAME)) AS STUD_NAME,(SELECT IF(PAID_AMOUNT > 0,SUM(PAID_AMOUNT),0) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " where FK_INVOICE_ID = C_INVOICE_ID  GROUP BY (FK_INVOICE_ID)) AS PAID_AMT FROM " . INVOICEMASTER . " IM LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =IM.FK_STUDENT_ID AND IM.INVOICE_TYPE !='PROPERTY' LEFT JOIN " . CUSTOMERMASTER . " CM ON CM.PK_CUST_ID = IM.FK_CUST_ID AND IM.INVOICE_TYPE ='PROPERTY'  WHERE IM.DELETE_FLAG = '0' " . $whereInvoiceCond . $whereStudCond . $whereInvoiceIdCond . $whereInvoiceTypeCond . " AND  IM.FK_CLIENT_ID ='" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "'" . $whereUserCond . "  ORDER BY IM.PK_INVOICE_ID DESC";  // QUERY FOR Invoice Data
    $resInvoiceData = fetch_rec_query($sqlInvoiceData); // RESULT FOR GET Invoice Data
    $invoiceArr = Array();
    if (count($resInvoiceData) > 0) {
        foreach ($resInvoiceData as $keyInvoice => $valueInvoice) { // Invoice Data FOREACH LOOP.
            $invoiceArr[$keyInvoice]['studentId'] = $valueInvoice['FK_STUDENT_ID'];
            $invoiceArr[$keyInvoice]['invoiceId'] = $valueInvoice['PK_INVOICE_ID'];
            $invoiceArr[$keyInvoice]['studName'] = $valueInvoice['STUD_NAME'];
            $invoiceArr[$keyInvoice]['invoiceId'] = $valueInvoice['PK_INVOICE_ID'];
            $invoiceArr[$keyInvoice]['clientInvoiceId'] = $valueInvoice['C_INVOICE_ID'];
            $invoiceArr[$keyInvoice]['invoiceDate'] = $valueInvoice['INVOICE_DATE'];
            $invoiceArr[$keyInvoice]['invoiceName'] = $valueInvoice['INVOICE_NAME'];
            $invoiceArr[$keyInvoice]['invoiceStartDate'] = $valueInvoice['PERIOD_FROM'];
            $invoiceArr[$keyInvoice]['invoiceEndDate'] = $valueInvoice['PERIOD_TO'];
            $invoiceArr[$keyInvoice]['PAID_AMT'] = ($valueInvoice['PAID_AMT'] == NULL) ? 0 : $valueInvoice['PAID_AMT'];
            $invoiceArr[$keyInvoice]['totalCharges'] = $valueInvoice['TOTAL_CHARGES'];
            $invoiceArr[$keyInvoice]['invoiceType'] = $valueInvoice['INVOICE_TYPE'];
            $invoiceArr[$keyInvoice]['status'] = ($valueInvoice['PAID_AMT'] >= $valueInvoice['TOTAL_CHARGES'] ) ? 'PAID' : 'UNPAID';
        }
        $result = array("status" => SCS, "data" => array_values($invoiceArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

// CANCEL INVOICE TRANS.
function cancelInvoiceTrans($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $postChargeId = isset($postData['postData']['chargeId']) ? addslashes(trim($postData['postData']['chargeId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";

    if (!empty($orgId) && !empty($clientId) && !empty($postChargeId) && !empty($studentId) && !empty($invoiceId)) {

        $updateChargeArr = Array();
        $updateChargeArr['CHANGED_BY'] = $userId;
        $updateChargeArr['CHANGED_BY_DATE'] = time();
        $updateChargeArr['IS_TRANSFER'] = 2; // CANCEL INVOICE TRANS 

        $whereChargePostingCond = "PK_CHARGE_ID ='" . $postChargeId . "' AND INVOICE_FLAG ='1' AND INVOICE_ID ='" . $invoiceId . "' AND STUDENT_ID='" . $studentId . "'";

        $updateChargePosting = update_rec(CHARGEMASTER, $updateChargeArr, $whereChargePostingCond);
        if ($updateChargePosting) {

            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['studentId'] = $studentId;
            $requestData['postData']['invoiceId'] = $invoiceId;

            $invoiceMasterData = getInvoiceTrans($requestData); // GET ALL INVOICE CHARGE MASTER DATA.

            $invoiceData = ($invoiceMasterData['status'] == SCS) ? $invoiceMasterData['data'] : $invoiceMasterData['status'];

            $result = array("status" => SCS, "data" => $invoiceData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . CHARGEMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

// GET ALL INVOICE TRANS DATA FOR PARTICULAR INVOICE ID.
function getInvoiceTrans($postData) {

    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";

    if (!empty($orgId) && !empty($clientId) && !empty($studentId) && !empty($invoiceId)) { // CHECK NECESSARY FIELD IS NOT EMPTY OR BLANK.
//    $sqlChargeMasterDataWithInvoice = "SELECT PK_CHARGE_ID,FK_ITEM_ID,ITEM_NAME,STUDENT_ID,STUDENT_FULL_NAME,SUM(QTY_OF_SESSION) AS QTY_OF_SESSION,ITEM_BASE_PRICE,SUM(TOTAL_TAX) AS TOTAL_TAX,SUM(ITEM_TOTAL_PRICE) AS ITEM_TOTAL_PRICE,INVOICE_ID,CHARGE_DATE,TYPEOFCUST,IS_TRANSFER FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND INVOICE_FLAG ='1' AND INVOICE_ID='" . $invoiceId . "' AND STUDENT_ID='" . $studentId . "' GROUP BY FK_ITEM_ID"; // QUERY FOR CHARGE MASTER DATA WITHOUT INVOICE 
//  
        $sqlChargeMasterDataWithInvoice = "SELECT PK_CHARGE_ID,FK_ITEM_ID,ITEM_NAME,STUDENT_ID,STUDENT_FULL_NAME,QTY_OF_SESSION AS QTY_OF_SESSION,ITEM_BASE_PRICE,TOTAL_TAX AS TOTAL_TAX,ITEM_TOTAL_PRICE AS ITEM_TOTAL_PRICE,INVOICE_ID,CHARGE_DATE,TYPEOFCUST,IS_TRANSFER FROM " . CHARGEMASTER . " WHERE DELETE_FLAG =0 AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND INVOICE_FLAG ='1' AND INVOICE_ID='" . $invoiceId . "' AND STUDENT_ID='" . $studentId . "' "; // QUERY FOR CHARGE MASTER DATA WITHOUT INVOICE 
// AND IS_TRANSFER ='0'  REMOVED BY DEEPAK PATIL ON 09-08-2018 (for dispaly cancel entries into listing)
        $resChargeMasterDataWithInvoice = fetch_rec_query($sqlChargeMasterDataWithInvoice); // RESULT FOR CHARGE MASTER DATA WITHOUT INVOICE
        $chargeMasterArr = $tempArr = Array();
        if (count($resChargeMasterDataWithInvoice) > 0) {
            foreach ($resChargeMasterDataWithInvoice as $keyChargeMaster => $valueChargeMaster) { // CHARGE MASTER DATA WITHOUT INVOICE DATA FOREACH LOOP.
                $chargeMasterArr[$keyChargeMaster]['chargeId'] = $valueChargeMaster['PK_CHARGE_ID'];
                $chargeMasterArr[$keyChargeMaster]['itemId'] = $valueChargeMaster['FK_ITEM_ID'];
                $chargeMasterArr[$keyChargeMaster]['itemName'] = $valueChargeMaster['ITEM_NAME'];
                $chargeMasterArr[$keyChargeMaster]['studentId'] = $valueChargeMaster['STUDENT_ID'];
                $chargeMasterArr[$keyChargeMaster]['studeName'] = $valueChargeMaster['STUDENT_FULL_NAME'];
                $chargeMasterArr[$keyChargeMaster]['clientInvoiceId'] = $valueChargeMaster['INVOICE_ID'];
                $chargeMasterArr[$keyChargeMaster]['chargeDate'] = $valueChargeMaster['CHARGE_DATE'];
                $chargeMasterArr[$keyChargeMaster]['qty'] = $valueChargeMaster['QTY_OF_SESSION'];
                $chargeMasterArr[$keyChargeMaster]['itemPrice'] = $valueChargeMaster['ITEM_BASE_PRICE'];
                $chargeMasterArr[$keyChargeMaster]['totalTax'] = $valueChargeMaster['TOTAL_TAX'];
                $chargeMasterArr[$keyChargeMaster]['totalPrice'] = $valueChargeMaster['ITEM_TOTAL_PRICE'];
                $chargeMasterArr[$keyChargeMaster]['type'] = $valueChargeMaster['TYPEOFCUST'];
                $chargeMasterArr[$keyChargeMaster]['status'] = $valueChargeMaster['IS_TRANSFER'];
            }
            $result = array("status" => SCS, "data" => array_values($chargeMasterArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//FUNCTION FOR Add/Edit Item Detail. 
function addEditRooms($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $price = isset($postData['postData']['price']) ? addslashes(trim($postData['postData']['price'])) : "";
    $priceStartDate = !empty($postData['postData']['priceStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['priceStartDate'])))) : "";
    $itemValidityEndDate = !empty($postData['postData']['itemValidityEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['itemValidityEndDate'])))) : "";
    $fkLookupId = isset($postData['postData']['fkLookupId']) ? addslashes(trim($postData['postData']['fkLookupId'])) : "";
    $itemType = isset($postData['postData']['itemType']) ? addslashes(trim($postData['postData']['itemType'])) : "";
    $cgst = isset($postData['postData']['cgst']) ? addslashes(trim($postData['postData']['cgst'])) : "";
    $igst = isset($postData['postData']['igst']) ? addslashes(trim($postData['postData']['igst'])) : "";
    $sgst = isset($postData['postData']['sgst']) ? addslashes(trim($postData['postData']['sgst'])) : "";
    $rooms = !empty($postData['postData']['rooms']) ? $postData['postData']['rooms'] : Array();
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($itemName) && !empty($price) && !empty($priceStartDate) && !empty($itemValidityEndDate) && !empty($fkLookupId) && !empty($itemType)) { // CHECK REQURIED FIELD CONDITION
        $whereCond = "";
        if (!empty($itemId)) {
            $whereCond = " AND PK_ITEM_ID !=" . $itemId;
        }
        $sqlCheckItemExist = "SELECT PK_ITEM_ID	FROM " . ITEMMASTER . " WHERE ITEM_NAME ='" . $itemName . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "'" . $whereCond . " AND FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR ITEM NAME IS ALREADY AVAILABLE OR NOT.
        $resCheckItemExist = fetch_rec_query($sqlCheckItemExist); // RESULT FOR ITEM NAME IS ALREADY AVAILABLE OR NOT.
        if (count($resCheckItemExist) > 0) {
            $result = array("status" => ITEMNAMEISEXIST);
            http_response_code(200);
        } else {
            $msg = "";
// ITEM INSERT ARRAY.
            $insertItemArray = Array(); //ITEM ARRAY CREATE
            $insertItemArray['FK_CLIENT_ID'] = $clientId;
            $insertItemArray['FK_ORG_ID'] = $orgId;
            $insertItemArray['ITEM_NAME'] = $itemName;
            $insertItemArray['ITEM_PRICE'] = $price;
            $insertItemArray['FK_CLT_LKUP_ID'] = $fkLookupId;
            $insertItemArray['PRICE_START_DATE'] = $priceStartDate;
            $insertItemArray['ITEM_TYPE'] = $itemType;
            $insertItemArray['FK_USER_ID'] = $userId;
            $insertItemArray['ITEM_VALIDITY_END_DATE'] = $itemValidityEndDate;
            $insertItemArray['CGST'] = $cgst;
            $insertItemArray['SGST'] = $sgst;
            $insertItemArray['IGST'] = $igst;

            if (isset($itemId) && $itemId > 0) {
                $insertItemArray['CHANGED_BY'] = $userId;
                $insertItemArray['CHANGED_BY_DATE'] = time();
                $whereItemCond = "PK_ITEM_ID ='" . $itemId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                $updateItem = update_rec(ITEMMASTER, $insertItemArray, $whereItemCond); // UPDATE RECORD IN ITEM_MASTER TABLE.
                if (!$updateItem) {
                    $msg = UPDATEFAIL . " " . ITEMMASTER;
                }
            } else {
                $insertItemArray['CREATED_BY'] = $userId;
                $insertItemArray['CREATED_BY_DATE'] = time();
                $insertItem = insert_rec(ITEMMASTER, $insertItemArray); // INSERT RECORD IN ITEM_MASTER TABLE.          
                if (!isset($insertItem['lastInsertedId']) || $insertItem['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . ITEMMASTER;
                }
            }
            if ($msg == "") {
                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $itemData = getItemsData($requestData); // GET ALL ROOM DATA.

                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];

                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => $msg);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function roomList($postData) {

// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $flag = isset($postData['postData']['flag']) ? addslashes(trim($postData['postData']['flag'])) : ""; // IF FLAG =1 THEN LIST ALL ROOM WHICH IS NOT FULL. OTHERWISE DISPLAY ALL ROOM.

    if (!empty($clientId) && !empty($orgId) && !empty($itemId)) {
        
         if ($flag == 1) {
                    
            $sqlRoomNo = "SELECT RM.PK_ROOM_ID,RM.FK_CLIENT_ID,RM.FK_ORG_ID,RM.FK_ITEM_ID,RM.ROOM_NAME,RM.ROOM_CAPACITY,(SELECT COUNT(BED_NO) FROM ".ALLOTMASTER." WHERE FK_ROOM_ID = RM.PK_ROOM_ID   ) AS TOTAL_BED_NO FROM " . ROOMNOMASTER . " RM WHERE RM.FK_ITEM_ID='" . $itemId . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' HAVING ROOM_CAPACITY > TOTAL_BED_NO";
          
        } else {
           $sqlRoomNo = "SELECT PK_ROOM_ID,FK_CLIENT_ID,FK_ORG_ID,FK_ITEM_ID,ROOM_NAME,ROOM_CAPACITY FROM " . ROOMNOMASTER . " WHERE FK_ITEM_ID='" . $itemId . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
          
          
        }
        $resRoomNo = fetch_rec_query($sqlRoomNo);
        if (count($resRoomNo) > 0) {
            $roomArr = Array();
            foreach ($resRoomNo as $keyRoom => $valueRoom) {
                $roomArr[$keyRoom]['roomId'] = $valueRoom['PK_ROOM_ID'];
                $roomArr[$keyRoom]['clientId'] = $valueRoom['FK_CLIENT_ID'];
                $roomArr[$keyRoom]['orgId'] = $valueRoom['FK_ORG_ID'];
                $roomArr[$keyRoom]['itemId'] = $valueRoom['FK_ITEM_ID'];
                $roomArr[$keyRoom]['roomName'] = $valueRoom['ROOM_NAME'];
                $roomArr[$keyRoom]['roomCapacity'] = $valueRoom['ROOM_CAPACITY'];
            }
            $result = array("status" => SCS, "data" => $roomArr);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function getRoomCategory($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";


    if (!empty($clientId) && !empty($orgId)) {
        $sqlRoomCategory = "SELECT ITEM_SUB_TYPE,(SELECT DISPLAY_VALUE FROM " . CLIENTLOOKUP . " WHERE PK_CLT_LKUP_ID=ITEM_SUB_TYPE) AS ITEM_SUB_TYPE_NAME FROM " . ITEMMASTER . " WHERE DELETE_FLAG ='0' AND ITEM_VALIDITY_END_DATE > '" . date("Y-m-d") . "' AND ITEM_TYPE='RENTAL' AND ITEM_SUB_TYPE > 0 AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' GROUP BY(ITEM_SUB_TYPE)";

        $resRoomCategory = fetch_rec_query($sqlRoomCategory);
        if (count($resRoomCategory) > 0) {
            $roomArr = Array();
            foreach ($resRoomCategory as $keyCategory => $valueCategory) {
                $roomArr[$keyCategory]['categoryId'] = $valueCategory['ITEM_SUB_TYPE'];
                $roomArr[$keyCategory]['categoryName'] = $valueCategory['ITEM_SUB_TYPE_NAME'];
            }
            $result = array("status" => SCS, "data" => $roomArr);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.21.7.2018.

function createInvoiceStudentNew($postData) {
//FK_ITEM_ID=-4,REF_ITEM_ID=-4 is used to adobe charges(eg.annual paymemnt)
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $invoiceDate = isset($postData['postData']['invoiceDate']) ? addslashes(trim($postData['postData']['invoiceDate'])) : "";
    $invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
    $hsnCode = isset($postData['postData']['hsnCode']) ? addslashes(trim($postData['postData']['hsnCode'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $itemRate = isset($postData['postData']['itemRate']) ? addslashes(trim($postData['postData']['itemRate'])) : "";
    $itemAmount = isset($postData['postData']['itemAmount']) ? addslashes(trim($postData['postData']['itemAmount'])) : "";
    $fkItemId = -4;
    $refItemId = -4;
    $time = time();

    $studentQuery = "SELECT PK_STUD_ID,FIRST_NAME,LAST_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "' AND DELETE_FLAG=0 AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' LIMIT 0,1";
    $resultStudent = fetch_rec_query($studentQuery);
//print_r($resultStudent); DIE;
    $pkInvoiceId = generateId(INVOICEMASTER, 'INV', $clientId);
//echo $pkInvoiceId; die;
    $insertInvoice = array();
    $insertInvoice['C_INVOICE_ID'] = $pkInvoiceId;
    $insertInvoice['FK_CLIENT_ID'] = $clientId;
    $insertInvoice['FK_ORG_ID'] = $orgId;
    $insertInvoice['FK_STUDENT_ID'] = $resultStudent[0]['PK_STUD_ID'];
    $insertInvoice['INVOICE_DATE'] = date('Y-m-d', strtotime($invoiceDate));
    $insertInvoice['INVOICE_NAME'] = $invoiceName;
    $insertInvoice['TOTAL_CHARGES'] = $itemRate;
    $insertInvoice['OPEN_BALANCE'] = 0;
    $insertInvoice['OPEN_BALANCE_TYPE'] = 'CR';
    $insertInvoice['COLLECTION_LAST_MONTH'] = 0;
    $insertInvoice['COLLECTION_LAST_MONTH_TYPE'] = 'CR';
    $insertInvoice['CLOSING_THIS_MONTH'] = 0;
    $insertInvoice['TYPE_OF_CLOSING_THIS_MONTH'] = 'CR';
    $insertInvoice['TALLY_OPENING'] = 0;
    $insertInvoice['TYPE_OF_TALLY_OPENING'] = 'CR';
    $insertInvoice['PERIOD_FROM'] = '2018-08-01';
    $insertInvoice['PERIOD_TO'] = '2019-05-31';
    $insertInvoice['CREATED_BY'] = $userId;
    $insertInvoice['CREATED_BY_DATE'] = $time;
    $insertInvoice['DELETE_FLAG'] = 0;
//print_r($insertInvoice); die;
    $requestInvoice = insert_rec(INVOICEMASTER, $insertInvoice);


    if (isset($requestInvoice['lastInsertedId']) && $requestInvoice['lastInsertedId'] > 0) {
        $invoiceNo = $requestInvoice['lastInsertedId'];
        $insertInvoiceCharge = array();
//        $insertInvoiceCharge['INVOICE_ID'] = $invoiceNo;
        $insertInvoiceCharge['FK_CLIENT_ID'] = $clientId;
        $insertInvoiceCharge['FK_ORG_ID'] = $orgId;
        $insertInvoiceCharge['TYPEOFCUST'] = 'Student';
        $insertInvoiceCharge['FK_ITEM_ID'] = $fkItemId;
        $insertInvoiceCharge['REF_ITEM_ID'] = $refItemId;
        $insertInvoiceCharge['ITEM_NAME'] = $itemName;
        $insertInvoiceCharge['STUDENT_ID'] = $resultStudent[0]['PK_STUD_ID'];
        ;
        $insertInvoiceCharge['CHARGE_DATE'] = date('Y-m-d', strtotime($invoiceDate));
        $insertInvoiceCharge['STUDENT_FULL_NAME'] = $resultStudent[0]['FIRST_NAME'] . " " . $resultStudent[0]['LAST_NAME'];
        $insertInvoiceCharge['QTY_OF_SESSION'] = 1;
        $insertInvoiceCharge['BIILED_SESSION'] = 1;
        $insertInvoiceCharge['TAX_CGST'] = 0;
        $insertInvoiceCharge['TAX_SGST'] = 0;
        $insertInvoiceCharge['TAX_IGST'] = 0;
        $insertInvoiceCharge['ITEM_BASE_PRICE'] = $itemAmount;
        $insertInvoiceCharge['TOTAL_TAX'] = $itemAmount;
        $insertInvoiceCharge['ITEM_TOTAL_PRICE'] = 0;
//$insertInvoiceCharge['INVOICE_FLAG'] = 0;
        $insertInvoiceCharge['TOLLERANCE_FALG'] = 0;
        $insertInvoiceCharge['IS_TRANSFER'] = 0;
        $insertInvoiceCharge['TRANSFER_REF_ID'] = 0;
        $insertInvoiceCharge['CREATED_BY'] = $userId;
        $insertInvoiceCharge['CREATED_BY_DATE'] = $time;
        $insertInvoiceCharge['DELETE_FLAG'] = 0;
//print_r($insertInvoiceCharge); die;
        $requestChargePosting = insert_rec(CHARGEMASTER, $insertInvoiceCharge);

        if (!isset($requestChargePosting ['lastInsertedId']) || $requestChargePosting ['lastInsertedId'] == 0) {
            $result = INSERTFAIL . " " . CHARGEMASTER;
        } else {
            $requestData = $studentData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['invoiceId'] = $requestInvoice['lastInsertedId'];
            $requestData['postData']['studentId'] = $studentId;

            $itemData = listInvoiceData($requestData); // GET ALL ROOM DATA.

            $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];

            $result = array("status" => SCS, "data" => $itemAllData);
            http_response_code(200);
            //$result = $insertInvoice;
        }
        return $result;
    }
}

function itemSearchForChargePosting($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $keyword = isset($postData['postData']['keyword']) ? addslashes(trim($postData['postData']['keyword'])) : "";

    $itemArr = array();

    if (!empty($clientId) && !empty($orgId) && !empty($keyword) && strlen($keyword) >= 2) {
        $whereItemCond = "";
        if (!empty($keyword)) {
            $whereItemCond = " AND ITEM_NAME LIKE '%" . $keyword . "%'";
        }
        $today = date("Y-m-d");

        $sqlItemData = "  SELECT PK_ITEM_ID,ITEM_NAME,ITEM_TYPE,HSN_CODE,ITEM_PRICE,CGST,SGST,IGST FROM " . ITEMMASTER . "  WHERE DELETE_FLAG =0 " . $whereItemCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND ITEM_TYPE ='MISC' AND ITEM_VALIDITY_END_DATE >= '" . $today . "'"; // QUERY FOR MISC ITEM DATA.

        $resItemData = fetch_rec_query($sqlItemData); // RESULT FOR MISC ITEM DATA
        if (count($resItemData) > 0) {
            foreach ($resItemData as $keyItem => $valueItem) { // MISC ITEM DATA LOOP
                $itemArr[$keyItem]['itemId'] = $valueItem['PK_ITEM_ID'];
                $itemArr[$keyItem]['itemName'] = $valueItem['ITEM_NAME'];
                $itemArr[$keyItem]['itemType'] = $valueItem['ITEM_TYPE'];
                $itemArr[$keyItem]['hsnCode'] = $valueItem['HSN_CODE'];
                $itemArr[$keyItem]['itemPrice'] = $valueItem['ITEM_PRICE'];
                $itemArr[$keyItem]['cgst'] = $valueItem['CGST'];
                $itemArr[$keyItem]['sgst'] = $valueItem['SGST'];
                $itemArr[$keyItem]['igst'] = $valueItem['IGST'];
            }
            $result = array("status" => SCS, "data" => $itemArr);
            http_response_code(200);
        } else {

            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function addChargePostingData($postData) {

// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $invoiceStartDate = isset($postData['postData']['invoiceStartDate']) ? addslashes(trim($postData['postData']['invoiceStartDate'])) : date("Y-m-d");
    $invoiceEndDate = isset($postData['postData']['invoiceEndDate']) ? addslashes(trim($postData['postData']['invoiceEndDate'])) : date("Y-m-d");
    $chargeStartDate = isset($postData['postData']['chargeStartDate']) ? addslashes(trim($postData['postData']['chargeStartDate'])) : "";
    $chargeEndDate = isset($postData['postData']['chargeEndDate']) ? addslashes(trim($postData['postData']['chargeEndDate'])) : "";
    $invoiceName = isset($postData['postData']['invoiceName']) ? addslashes(trim($postData['postData']['invoiceName'])) : "";
    $studentName = isset($postData['postData']['studentName']) ? addslashes(trim($postData['postData']['studentName'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $amount = isset($postData['postData']['amount']) ? addslashes(trim($postData['postData']['amount'])) : "";
    $invoiceType = isset($postData['postData']['invoiceType']) ? addslashes(trim($postData['postData']['invoiceType'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : ""; //  -1 = Not send invoice otherwise send
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "1";
    $chargeType = isset($postData['postData']['chargeType']) ? addslashes(trim($postData['postData']['chargeType'])) : "";
    $studentPassword = isset($postData['postData']['studentPassword']) ? addslashes(trim($postData['postData']['studentPassword'])) : "";
    $parentsPassword = isset($postData['postData']['parentsPassword']) ? addslashes(trim($postData['postData']['parentsPassword'])) : "";


//echo "<pre>";print_r($postData); exit;
    /* Create Invoice Code Comment START */
    $checkItemData = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_PRICE,ITEM_TYPE,CGST,SGST,IGST FROM " . ITEMMASTER . " WHERE DELETE_FLAG='0' AND PK_ITEM_ID ='" . $itemId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
    $resCheckItemData = fetch_rec_query($checkItemData);
    if (count($resCheckItemData) > 0 && $chargeType != "ENROLLMENT") {
        foreach ($resCheckItemData as $keyItem => $valueItem) {

            $itemName = $valueItem['ITEM_NAME'];
            $itemPrice = $valueItem['ITEM_PRICE'];
            $itemCgst = $valueItem['CGST'];
            $itemSgst = $valueItem['SGST'];
            $itemIgst = $valueItem['IGST'];
            $qtyOfSession = (isset($qty) && $qty > 1) ? $qty : 1;
            $calItemBasicTPrice = $itemPrice * $qtyOfSession;
            $cgstAmt = ($itemCgst > 0) ? ($calItemBasicTPrice * $itemCgst) / 100 : 0;
            $sgstAmt = ($itemSgst > 0) ? ($calItemBasicTPrice * $itemSgst) / 100 : 0;
            $igstAmt = ($itemIgst > 0) ? ($calItemBasicTPrice * $itemIgst) / 100 : 0;

            $calTotalTax = $cgstAmt + $sgstAmt + $igstAmt;
            $calItemTotalPrice = $calItemBasicTPrice + $calTotalTax;
        }
    } else {
        $itemCgst = $itemSgst = $itemIgst = $calTotalTax = "";
        $itemPrice = $calItemTotalPrice = $amount;
    }


    $insertChargePostingArray = Array(); //CHARGE POSTING ARRAY CREATE
    $insertChargePostingArray['FK_CLIENT_ID'] = $clientId;
    $insertChargePostingArray['FK_ORG_ID'] = $orgId;
    $insertChargePostingArray['TYPEOFCUST'] = 'student';
    $insertChargePostingArray['FK_ITEM_ID'] = $itemId;
    $insertChargePostingArray['REF_ITEM_ID'] = $itemId;
    $insertChargePostingArray['ITEM_NAME'] = $itemName;
    $insertChargePostingArray['CHARGE_DATE'] = date("Y-m-d");
    $insertChargePostingArray['STUDENT_ID'] = $studentId;
    $insertChargePostingArray['STUDENT_FULL_NAME'] = $studentName;
    $insertChargePostingArray['QTY_OF_SESSION'] = $qty;
    $insertChargePostingArray['BIILED_SESSION'] = $qty;
    $insertChargePostingArray['TAX_CGST'] = $itemCgst;
    $insertChargePostingArray['TAX_SGST'] = $itemSgst;
    $insertChargePostingArray['TAX_IGST'] = $itemIgst;
    $insertChargePostingArray['ITEM_BASE_PRICE'] = $itemPrice;
    $insertChargePostingArray['TOTAL_TAX'] = $calTotalTax;
    $insertChargePostingArray['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;
    $insertChargePostingArray['TYPE'] = $chargeType;
    $insertChargePostingArray['CREATED_BY'] = $userId;
    $insertChargePostingArray['CREATED_BY_DATE'] = time();

    $insertChargePosting = insert_rec(CHARGEMASTER, $insertChargePostingArray); // INSERT RECORD IN CHARGE_MASTER TABLE.
    if ($insertChargePosting['lastInsertedId'] > 0) {
        $unbilledAmt = $closingBalAmt = $billedAmt = $receiptAmt = 0;
        $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT,CLOSINGBAL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $studentId . "' AND DELETE_FLAG ='0'";
        $resStudentData = fetch_rec_query($sqlStudentData); // 
        if (count($resStudentData) > 0) {
            $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
            $billedAmt = $resStudentData[0]['BILL_AMT'];
            $receiptAmt = $resStudentData[0]['RECEIPT_AMT'];
        }


        $closingBalAmt = floatval($billedAmt) - (floatval($receiptAmt) + floatval($calItemTotalPrice));
        $closingBalType = ($closingBalAmt > 0) ? "DR" : "CR";

        $updateStudentArr = ARRAY();
        $updateStudentArr['UNBILLED_AMT'] = floatval($calItemTotalPrice) + floatval($unbilledAmt);
        $updateStudentArr['UNBILLED_TYPE'] = 'DR';
        $updateStudentArr['CLOSINGBAL_AMT'] = abs($closingBalAmt);
        $updateStudentArr['CLOSINGBAL_TYPE'] = $closingBalType;

        $updateStudentArr['CHANGED_BY'] = $userId;
        $updateStudentArr['CHANGED_BY_DATE'] = time();
        $whereStudCond = "PK_STUD_ID='" . $studentId . "'";
        $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);

        $result = array("status" => SCS, "data" => $insertChargePosting['lastInsertedId']);
        http_response_code(200);
    } else {
        $result = array("status" => INSERTFAIL . " charge master table.");
        http_response_code(400);
    }
    /* Create Invoice Code Comment END */
    return $result;
}

function sendStudentInvoiceMail($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $invoiceNo = isset($postData['postData']['invoiceNo']) ? addslashes(trim($postData['postData']['invoiceNo'])) : "";

    if (!empty($orgId) && !empty($clientId) && !empty($studentId) && !empty($invoiceNo)) {

        $sqlStudentData = "SELECT CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,SM.PK_STUD_ID,SM.EMAIL,CONCAT(SM.ADDRESS_LINE1,',',SM.ADDRESS_LINE2,',',SM.ADDRESS_LINE3) AS ADDRESS,SM.PINCODE,CO.COUNTRY_NAME,CT.STATE_NAME,CT.CITY_NAME,SM.MEMBERSHIP_NUMBER FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  WHERE SM.DELETE_FLAG ='0' AND SM.PK_STUD_ID='" . $studentId . "' AND SM.FK_CLIENT_ID = '" . $clientId . "' AND SM.FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA
        $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

        $sqlInvoiceTrans = "SELECT ITEM_NAME,TOTAL_CHARGES,TOTAL_TAX,PAYABLE_AMOUNT,SGST,CGST,HSN_CODE,IGST FROM " . INVOICETRANSMASTER . " WHERE FK_INVOICE_ID='" . $invoiceNo . "' AND DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA

        $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA
//echo "<pre>"; print_r($resInvoiceTrans); exit;

        $sqlPaymentInvoice = "SELECT SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $invoiceNo . "' AND FK_STUDENT_ID ='" . $resStudentData[0]['PK_STUD_ID'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; //QUERY FOR INVOICE PAYMENT DATA 
        $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);

        $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0'"; // QUERY FOR CLIENT DATA

        $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA



        $totalPayment = 0;
        if (count($resPaymentInvoice) > 0) {
            $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
        }

        if (!empty($resStudentData[0]['EMAIL'])) {

            /* Mail sent code start */
            $sendgrid_username = "arunnagpal";
            $sendgrid_password = "devil1502";

            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


            $totalPrevBalance = "";
            $totalPrevBalanceType = "";
            $htmlInvioiceTemplateHeader = '<!DOCTYPE html>' .
                    '<html lang="en">' .
                    '<head>' .
                    '<meta charset="utf-8">' .
                    '<meta name="viewport" content="width=device-width, initial-scale=1.0">' .
                    '<meta name="description" content="">' .
                    '<meta name="author" content="Mosaddek">' .
                    '<link rel="shortcut icon" href="img/favicon.ico">' .
                    '<title></title>' .
                    '</head>' .
                    '<body class="invoice-page" data-gr-c-s-loaded="true"> <div style="background: #ffffff none repeat scroll 0 0; color: #333333; font-family: DejaVu Sans; sans-serif;font-size: 9pt;"><div class="pcs-template-header pcs-header-content" id="header" style="height: 0.1in; padding: 0 0.4in 0 0.55in; background-color: #ffffff; color: #333333; font-size: 9pt;"></div><div class="pcs-template-body" style="padding: 0 0.4in 0 0.55in;">';

            $htmlTableLogo = '<table style="width:100%;table-layout: fixed;">' .
                    '<tbody>' .
                    '<tr>' .
                    '<td colspan = 2 style="vertical-align: top; width:100%; text-align: center;">' .
                    '<span class="pcs-entity-title" style="color: #000000; font-size: 18pt;">Tax Invoice</span><br>
                        <span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020<br /><br />
                    ' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="vertical-align: top; width:50%;">' .
                    '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice No  &nbsp;&nbsp;&nbsp;:  ' . $invoiceNo . '</b></span><br>' .
                    '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice Date : ' . date("d-m-Y") . '</b></span><br>' .
                    // '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Period From : ' . $getInvoiceDetail['data'][0]['invoicePeroidFrom'] . '</b></span>' .
                    '</td>' .
                    '<td style="vertical-align: top; text-align:right;width:50%;">' .
                    '<div>' .
                    // '<img src="' . APP_SERVER_ROOT_FRONTEND . '/img/gulmoharInvLogo.png" style="width:160.00px;height:129.00px;" id="logo_content">' .
                    '</div>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody>' .
                    '</table>';

            $htmlMemberDetail = '<table style="clear:both;width:100%;margin-top:30px;table-layout:fixed;">' .
                    '<tbody><tr><td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                    '</td>' .
                    '<td style="vertical-align:bottom;width: 40%;" align="right"></td>' .
                    '</tr><tr>' .
                    '<td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                    '<div><label style="font-size: 14pt; color:#817d7d" class="pcs-label" id="tmp_billing_address_label">To,</label><br>' .
                    '<span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 12pt;">' . $resStudentData[0]['STUD_NAME'] . '</span><br>' .
                    '<span style="white-space: pre-wrap; font-size: 9pt;">' . wordwrap($resStudentData[0]['ADDRESS'], 70) . '</span><br>' .
                    '<span style="white-space: pre-wrap; font-size: 9pt;">' . strtoupper(strtolower($resStudentData[0]['CITY_NAME'])) . ' ' . strtoupper(strtolower($resStudentData[0]['STATE_NAME'])) . ' ' . strtoupper(strtolower($resStudentData[0]['COUNTRY_NAME'])) . ' - ' . $resStudentData[0]['PINCODE'] . '</span><br>' .
                    '</div>' .
                    '</td>' .
                    '<td style="vertical-align:bottom;width: 40%;" align="right">' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>';

            //  $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;">' . $gstinNo . '<br><b>Membership No</b> : ' . $getInvoiceDetail['data'][0]['memId'] . '</span>' . '<table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
            $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;"><br><b>Membership No</b> : </span>' . $resStudentData[0]['MEMBERSHIP_NUMBER'] . '<table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
                    '<thead>' .
                    '<tr style="height:32px;">' .
                    '<td style="padding:5px 0 5px 5px;text-align: center;word-wrap: break-word;width: 5%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header">#</td>' .
                    '<td style="padding:5px 10px 5px 20px;word-wrap: break-word; background-color: #3c3d3a;color: #ffffff; font-size: 9pt; " class="pcs-itemtable-header pcs-itemtable-description">Particular</td>' .
                    //'<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 11%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>'.
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">CGST</td>' .
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">SGST</td>' .
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">IGST</td>' .
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width:22%; background-color: #3c3d3a; color: #ffffff;font-size: 9pt;background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>' .
                    '</tr>' .
                    '</thead>' .
                    '<tbody class="itemBody">';
            $invoiceItemTransMid = "";
            if (!empty($resInvoiceTrans)) {
                $totalSgstAmt = $totalCgstAmt = $totalCharges = $totalPayable = $totalIgstAmt = 0;
                foreach ($resInvoiceTrans as $keyTrans => $valueTrans) {

                    if (!empty($valueTrans['PAYABLE_AMOUNT'])) {
                        $sgstAmt = $cgstAmt = $igstAmt = 0;
                        $sgstAmt = ($valueTrans['SGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['SGST'] / 100 : 0;
                        $cgstAmt = ($valueTrans['CGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['CGST'] / 100 : 0;
                        $igstAmt = ($valueTrans['IGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['IGST'] / 100 : 0;
                        $totalSgstAmt = $totalSgstAmt + $sgstAmt;
                        $totalCgstAmt = $totalCgstAmt + $cgstAmt;
                        $totalIgstAmt = $totalIgstAmt + $igstAmt;

                        $invoiceItemTransMid .= '<tr>' .
                                '<td style="padding: 10px 0 10px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">' . ($keyTrans + 1) . '</td>' .
                                '<td style="padding: 10px 0px 10px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" ><div><div><span style="word-wrap: break-word;" id="tmp_item_name">' . $valueTrans['ITEM_NAME'] . '</span><br><span class="pcs-item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">' . $valueTrans['HSN_CODE'] . '</span></div></div></td>' .
                                //'<td <td style="padding: 10px 10px 5px 10px;text-align:right;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><span id="tmp_item_qty"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> '.number_format($valueTrans['totalCharges'],2).'</span></td>'.
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($sgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['SGST'] . '% </div></td>' .
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($cgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['CGST'] . '% </div></td>' .
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($igstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['IGST'] . '% </div></td>' .
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($valueTrans['TOTAL_CHARGES'], 2) . '</span></td>' .
                                '</tr>';

                        $totalCharges = $totalCharges + ($valueTrans['TOTAL_CHARGES']);
                        $totalPayable = $totalPayable + $valueTrans['PAYABLE_AMOUNT'];
                    }
                }
            }

            /* $balanceCharge = 0;
              $balanceChargeType = "";
              if (strtolower($totalPrevBalanceType) != "cr") { // DEBIT
              $balanceCharge = $totalCharges + $totalPrevBalance;
              $balanceChargeType = $totalPrevBalanceType;
              } else { // CREDIT
              $balanceCharge = $totalCharges - $totalPrevBalance;
              $balanceChargeType = ((($totalCharges - $totalPrevBalance) < 0) ? 'CR' : "DR");
              } */
            $invoiceItemTransFooter = '</tbody></table>';
            $prevBalance = 0;
            $balanceDue = $prevBalance + $totalPayment - $totalPayable;
            $balanceDueType = ($balanceDue > 0) ? 'CR' : 'DR';

            $htmlInvoiceTax = '<div style="width: 100%;margin-top: 1px; float:right;">' .
                    //'<div style="width: 45%;padding: 10px 10px 3px 3px;font-size: 9pt;float: left;">'.
                    //'<div style="white-space: pre-wrap;"><p>&nbsp;</p></div>'.
                    //'</div>'.
                    '<div style="width: 100%;">' .
                    '<table width="100%"><tr><td style="width:50%; float:left;"></td><td style="text-align:right;width:45%; float:right;">' .
                    '<table class="pcs-totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">' .
                    '<tbody>' .
                    '<tr>' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>' .
                    //  '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                    '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                    '</tr>' .
                    '<tr style="height:10px;">' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>' .
                    //  '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .
                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalSgstAmt, 2) . '</td>' .
                    '</tr>' .
                    '<tr style="height:10px;">' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>' .
                    //   '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .

                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCgstAmt, 2) . '</td>' .
                    '</tr>' .
                    '<tr style="height:10px;">' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>' .
                    //   '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .

                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalIgstAmt, 2) . '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>' .
                    '<td id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>DR</b></td>' .
                    //  '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format((($totalCharges + $totalTax < 0) ? (($totalCharges + (($totalTax < 0) ? $totalTax * -1 : $totalTax) * -1) * -1) : $totalCharges + (($totalTax < 0) ? $totalTax * -1 : $totalTax)), 2) . '</b></td>' .
                    '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayable, 2) . '</b></td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>' .
                    '<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>CR</b></td>' .
                    '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayment, 2) . '</b></td>' .
                    '</tr>' .
                    '<tr style="height:40px; background-color: #f5f4f3; color: #000000; font-size: 9pt; font-size: 8pt; background-color: #f5f4f3;color: #000000; font-size: 9pt;" class="pcs-balance">' .
                    '<td style="padding:5px 10px 5px 0;" valign="middle" align="right"><b>Balance Due</b></td>' .
                    '<td id="tmp_balance_due" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . $balanceDueType . '</b></td>' .
                    '<td id="tmp_balance_due" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(abs($balanceDue), 2) . '</b></td>' .
                    '</tr>' .
                    '</tbody>' .
                    '</table>' .
                    '</td></tr></table>' .
                    '</div>' .
                    '<div style="clear: both;"></div>' .
                    '</div>';

            //$balanceChargeType
            //number_format((($balanceCharge+$totalTax < 0) ? ($balanceCharge+$totalTax)*-1 : $balanceCharge+$totalTax),2)
            $htmlInvoiceTempTM = '<div style="margin-top:10px;"><b><u>Terms and Conditions</u></b>' .
                 /*   '<table style="width:100%;font-size: 10px;">' .
                    '<tbody><tr>' .
                    '<td style="padding-top:60px;padding-bottom: 8px;">' .
                    '<span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus </span></b>".</span>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody>' .
                    '</table>' .*/
                    '</div><div style="font-size: 14px;"><b>*Please note this is a computer generated invoice and hence dose not need signature.</b></div>';
            $htmlInvioceTemplateFooter = '</div> <div class="pcs-template-footer" style="background-color: #ffffff;  color: #aaaaaa; font-size: 6pt; height: 0.7in; padding: 0 0.4in 0 0.55in;"></div></div></body></html>';

            // FINAL HTML
            $finalHtml = $htmlInvioiceTemplateHeader . $htmlTableLogo . $htmlMemberDetail . ($invoiceItemTransHeader . $invoiceItemTransMid . $invoiceItemTransFooter) . $htmlInvoiceTax . $htmlInvoiceTempTM . $htmlInvioceTemplateFooter;
            //print_r($finalHtml); exit;
            //$dompdf_temp = new Dompdf();
            $dompdf = new Dompdf\Dompdf();
            $dompdf->load_html($finalHtml);
            //$dompdf_temp->set_paper("a4", "portrait");
            $dompdf->set_paper(array(0, 0, 794, 1122), "portrait");
            $dompdf->render();
            $pdfOutput = $dompdf->output();

            //echo $pdfOutput;die;
            $fileLocation = APP_SERVER_ROOT . "/attachment/" . $resStudentData[0]['MEMBERSHIP_NUMBER'] . "-" . time() . ".pdf";
            $fp = fopen($fileLocation, "a+");
            fwrite($fp, $pdfOutput);
            fclose($fp);

            //$clientDetail = getClientDetail($clientId);
            // TEMPLATE FOR HTML
            // $messageBodyHeader = "<html lang='en'><head>" .
            //         "<body>";
            // $messageBodyMid = '<div class="container">' .
            //         '<header>' .
            //         '<a href="#" class="logo"></a>' .
            //         '<main><h1 class="reset-head">Hi ' . $resStudentData[0]['STUD_NAME'] . '</h1>' .
            //         '<div class="detail-top">' .
            //         '<p> Please find Attched pdf document' .
            //         '.</p>';
            // $messageBodyFooter = "</div></body></html>";
            // $messageBody = $messageBodyHeader . $messageBodyMid . $messageBodyFooter;

            $messageBodyHeader = '<!DOCTYPE html>
<html class="no-js">

   <head>
      <link rel="stylesheet" type="text/css" href="css/app.css">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width">
      <title>Student Enrollment Email Template</title>
      <!-- <style> -->
   </head>
    <body style="background:#fff;">
        <table style="background:#a6ce39; width:100%; max-width:600px; padding:10px 10px 10px 10px;margin:0 auto;">
            <tr>
                <td>
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #a6ce39 solid;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                        </tr>
                    </table>  
                    <table width="100%" style="background:#ffffff;border-spacing: 0px;">
                        <tr>
                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"> <p style="margin:0 0 5px 0;">Inde Campus is a chain of hostels who wish to ensure that the hostel experience one has gets duly shared with the world class. A bit of us can be seen in all our hostels through the personalized artworks and the little touches put in by our team to enable our student to distinctly remember us by. Student are encouraged to leave their mark at our hostels by way off wall art or by just getting involved.

We believe in providing affordable living with all basic amenities in place along with security. We strive to expand within the next few years to ensure that our student get to stay with us as a part of the inde campus family..</p>  </td>
                        </tr>
                        
                        <tr>
                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;">Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/>' . $resClientTrans[0]['ADDRESS'] . ' </td>
                        </tr>
                    </table>
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td valign="middel" style="padding:25px 5px 25px 5px; background:#0077c0; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                        </tr>
                    </table> 
                </td>
            </tr>            
        </table>
    </body>
</html>

';

            $messageBody = $messageBodyHeader;
            //    $fromAddress = $resultSms[0]['CLIENT_EMAIL'];
            //   $fromName = $resultSms[0]['CLIENTNAME'];





            $fromAddress = 'support@lithe.in';
            $fromName = 'Lithe Technology';
            $email = new SendGrid\Email();
            $email->addTo($resStudentData[0]['EMAIL']);
            // $email->addToName($valueSender);
            $email->setFrom($fromAddress);

            $allAttachmentArr[] = $fileLocation;

            //print_r($allAttachmentArr); exit;
            if (!empty($allAttachmentArr)) {
                foreach ($allAttachmentArr as $docKey => $docValue) {
                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                        $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                    }
                }
            }

            $subject = "INDE CAMPUS :Student Invoice";
            $email->setFromName($fromName);
            $email->setSubject($subject);

            //$messageBody = "please find attached document for ";
            $email->setHtml($messageBody);
            //                       print_r($email); exit;
            $response = $sendGrid->send($email);
            $responseMail = $response->getBody();


            if (!empty($allAttachmentArr)) {
                foreach ($allAttachmentArr as $docKey => $docValue) {
                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                        unlink(APP_SERVER_ROOT . "/attachment/" . basename($docValue)); // add attachment 
                    }
                }
            }
            /* Mail sent code End */
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => EMAILIDISNOTFOUND);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function sendGeneralInvoiceMail($postData) {

    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $clientInvoiceId = isset($postData['postData']['invoiceId']) ? (($postData['postData']['invoiceId'])) : array(); // ARRAY
    $mailSend = isset($postData['postData']['mailSend']) ? addslashes(trim($postData['postData']['mailSend'])) : 1; // 1=SEND MAIL AND 0 =NOT SEND MAIL.
// echo "<pre>"; print_r($postData); exit;

    $finalAttachments = array();
    if (!empty($clientInvoiceId)) {

// GET STUDENT DETAIL
        $sqlStudentData = "SELECT CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,SM.PK_STUD_ID,SM.EMAIL,CONCAT(SM.ADDRESS_LINE1,',',SM.ADDRESS_LINE2,',',SM.ADDRESS_LINE3) AS ADDRESS,SM.PINCODE,CO.COUNTRY_NAME,CT.STATE_NAME,CT.CITY_NAME,SM.MEMBERSHIP_NUMBER,SM.USER_NAME FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  WHERE SM.DELETE_FLAG ='0' AND SM.PK_STUD_ID='" . $studentId . "' AND SM.FK_CLIENT_ID = '" . $clientId . "' AND SM.FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA
        $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

        if (!empty($resStudentData[0]['EMAIL'])) { // IF NOT EMAIL FOUDN THEN DO NOT SEND THE MAIL ELSE SEND
            $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR CLIENT DATA
            $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA

            $parentUserNameSql = fetch_rec_query("SELECT USER_NAME FROM " . PARENTMASTER . " WHERE FK_STUD_ID = '" . $studentId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND DELETE_FLAG = 0");



            // START CODE FOR PDF
            //$totalPrevBalance = 0;
            //$totalPrevBalanceType = "";
            foreach ($clientInvoiceId as $key => $value) {

                // TO CHECK THE BALANCE OF DR OR CR
                $sqlPaymentInvoice = "SELECT SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $value . "' AND FK_STUDENT_ID ='" . $studentId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; //QUERY FOR INVOICE PAYMENT DATA 
                $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);
                $totalPayment = 0;
                if (count($resPaymentInvoice) > 0) {
                    $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
                }


                $sqlInvoiceType = fetch_rec_query("SELECT INVOICE_TYPE FROM " . INVOICEMASTER . " WHERE C_INVOICE_ID = '" . $value . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND DELETE_FLAG = 0");
                if (count($sqlInvoiceType) > 0) {
                    $invoiceType = $sqlInvoiceType[0]['INVOICE_TYPE'];
                }

                $sqlInvoiceTrans = "SELECT ITEM_NAME,QTY,TOTAL_CHARGES,TOTAL_TAX,PAYABLE_AMOUNT,SGST,CGST,IGST,HSN_CODE FROM " . INVOICETRANSMASTER . " WHERE FK_INVOICE_ID='" . $value . "' AND DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA
                $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA

                if (count($resInvoiceTrans) > 0 && !empty($resInvoiceTrans)) {

                    $htmlInvioiceTemplateHeader = '<!DOCTYPE html>' .
                            '<html lang="en">' .
                            '<head>' .
                            '<meta charset="utf-8">' .
                            '<meta name="viewport" content="width=device-width, initial-scale=1.0">' .
                            '<meta name="description" content="">' .
                            '<meta name="author" content="Mosaddek">' .
                            '<link rel="shortcut icon" href="img/favicon.ico">' .
                            '<title></title>' .
                            '</head>' .
                            '<body class="invoice-page" data-gr-c-s-loaded="true"> <div style="background: #ffffff none repeat scroll 0 0; color: #333333; font-family: DejaVu Sans; sans-serif;font-size: 9pt;"><div class="pcs-template-header pcs-header-content" id="header" style="height: 0.1in; padding: 0 0.4in 0 0.55in; background-color: #ffffff; color: #333333; font-size: 9pt;"></div><div class="pcs-template-body" style="padding: 0 0.4in 0 0.55in;">';

                    $htmlTableLogo = '<table style="width:100%;table-layout: fixed;">' .
                            '<tbody>' .
                            '<tr>' .
                            '<td colspan = 2 style="vertical-align: top; width:100%; text-align: center;">' .
                            '<span class="pcs-entity-title" style="color: #000000; font-size: 18pt;">Tax Invoice</span><br>
                        <span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020<br /><br />' .
                            '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="vertical-align: top; width:50%;">' .
                            '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice No &nbsp;&nbsp; : ' . $value . '</b></span><br>' .
                            '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice Date : ' . date("d-m-Y") . '</b></span><br>' .
                            // '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Period From : ' . $getInvoiceDetail['data'][0]['invoicePeroidFrom'] . '</b></span>' .
                            '</td>' .
                            '<td style="vertical-align: top; text-align:right;width:50%;">' .
                            '<div>' .
                            // '<img src="' . APP_SERVER_ROOT_FRONTEND . '/img/gulmoharInvLogo.png" style="width:160.00px;height:129.00px;" id="logo_content">' .
                            '</div>' .
                            '</td>' .
                            '</tr>' .
                            '</tbody>' .
                            '</table>';

                    $htmlMemberDetail = '<table style="clear:both;width:100%;margin-top:30px;table-layout:fixed;">' .
                            '<tbody><tr><td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                            '</td>' .
                            '<td style="vertical-align:bottom;width: 40%;" align="right"></td>' .
                            '</tr><tr>' .
                            '<td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                            '<div><label style="font-size: 14pt; color:#817d7d" class="pcs-label" id="tmp_billing_address_label">To,</label><br>' .
                            '<span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 12pt;">' . $resStudentData[0]['STUD_NAME'] . '</span><br>' .
                            '<span style="white-space: pre-wrap; font-size: 9pt;">' . wordwrap($resStudentData[0]['ADDRESS'], 70) . '</span><br>' .
                            '<span style="white-space: pre-wrap; font-size: 9pt;">' . strtoupper(strtolower($resStudentData[0]['CITY_NAME'])) . ' ' . strtoupper(strtolower($resStudentData[0]['STATE_NAME'])) . ' ' . strtoupper(strtolower($resStudentData[0]['COUNTRY_NAME'])) . ' - ' . $resStudentData[0]['PINCODE'] . '</span><br>' .
                            '</div>' .
                            '</td>' .
                            '<td style="vertical-align:bottom;width: 40%;" align="right">' .
                            '</td>' .
                            '</tr>' .
                            '</tbody></table>';

                    $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;"><br><b>Membership No</b> : </span>' . $resStudentData[0]['MEMBERSHIP_NUMBER'] . '<table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
                            '<thead>' .
                            '<tr style="height:32px;">' .
                            '<td style="padding:5px 0 5px 5px;text-align: center;word-wrap: break-word;width: 5%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header">#</td>' .
                            '<td style="padding:5px 10px 5px 20px;word-wrap: break-word; background-color: #3c3d3a;color: #ffffff; width: 15%;font-size: 9pt; " class="pcs-itemtable-header pcs-itemtable-description">Particular</td>' .
                            '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">RATE</td>' .
                            '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">QTY</td>' .
                            '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 12%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">CGST</td>' .
                            '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 12%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">SGST</td>' .
                            '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 12%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">IGST</td>' .
                            '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width:19%; background-color: #3c3d3a; color: #ffffff;font-size: 9pt;background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>' .
                            '</tr>' .
                            '</thead>' .
                            '<tbody class="itemBody">';
                    $totalCharges = $totalSgstAmt = $totalCgstAmt = $totalPayable = 0;
                    $invoiceItemTransMid = "";
                    if (!empty($resInvoiceTrans)) {
                        $totalSgstAmt = $totalCgstAmt = $totalCharges = $totalPayable = $totalIgstAmt = 0;
                        foreach ($resInvoiceTrans as $keyTrans => $valueTrans) {

                            if (!empty($valueTrans['PAYABLE_AMOUNT'])) {

                                $sgstAmt = $cgstAmt = $igstAmt = 0;
                                $itemPrice = $valueTrans['TOTAL_CHARGES'] / $valueTrans['QTY'];

                                $sgstAmt = ($valueTrans['SGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['SGST'] / 100 : 0;
                                $cgstAmt = ($valueTrans['CGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['CGST'] / 100 : 0;
                                $igstAmt = ($valueTrans['IGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['IGST'] / 100 : 0;

                                $totalSgstAmt = $totalSgstAmt + $sgstAmt;
                                $totalCgstAmt = $totalCgstAmt + $cgstAmt;
                                $totalIgstAmt = $totalIgstAmt + $igstAmt;

                                $invoiceItemTransMid .= '<tr>' .
                                        '<td style="padding: 10px 0 10px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">' . ($keyTrans + 1) . '</td>' .
                                        '<td style="padding: 10px 0px 10px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" ><div><div><span style="word-wrap: break-word;" id="tmp_item_name">' . $valueTrans['ITEM_NAME'] . '</span><br><span class="pcs-item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">' . $valueTrans['HSN_CODE'] . '</span></div></div></td>' .
                                        '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($itemPrice, 2) . '</span></td>' .
                                        '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"> ' . $valueTrans['QTY'] . '</span></td>' .
                                        '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($cgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['CGST'] . '% </div></td>' .
                                        '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($sgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['SGST'] . '% </div></td>' .
                                        '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($igstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['IGST'] . '% </div></td>' .
                                        '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($valueTrans['TOTAL_CHARGES'], 2) . '</span></td>' .
                                        '</tr>';

                                $totalCharges = $totalCharges + ($valueTrans['TOTAL_CHARGES']);
                                $totalPayable = $totalPayable + $valueTrans['PAYABLE_AMOUNT'];
                            }
                        }
                    }
                    $prevBalance = 0;
                    $balanceDue = $prevBalance + $totalPayment - $totalPayable;
                    $balanceDueType = ($balanceDue > 0) ? 'CR' : 'DR';

                    $invoiceItemTransFooter = '</tbody></table>';

                    $htmlInvoiceTax = '<div style="width: 100%;margin-top: 1px; float:right;">' .
                            '<div style="width: 100%;">' .
                            '<table width="100%"><tr><td style="width:50%; float:left;"></td><td style="text-align:right;width:45%; float:right;">' .
                            '<table class="pcs-totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">' .
                            '<tbody>' .
                            '<tr>' .
                            '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>' .
                            '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                            '</tr>' .
                            '<tr style="height:10px;">' .
                            '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>' .
                            '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalSgstAmt, 2) . '</td>' .
                            '</tr>' .
                            '<tr style="height:10px;">' .
                            '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>' .
                            '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCgstAmt, 2) . '</td>' .
                            '</tr>' .
                            '<tr style="height:10px;">' .
                            '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>' .
                            '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalIgstAmt, 2) . '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>' .
                            '<td id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>DR</b></td>' .
                            '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayable, 2) . '</b></td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>' .
                            '<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>CR</b></td>' .
                            '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayment, 2) . '</b></td>' .
                            '</tr>' .
                            '<tr style="height:40px; background-color: #f5f4f3; color: #000000; font-size: 9pt; font-size: 8pt; background-color: #f5f4f3;color: #000000; font-size: 9pt;" class="pcs-balance">' .
                            '<td style="padding:5px 10px 5px 0;" valign="middle" align="right"><b>Balance Due</b></td>' .
                            '<td id="tmp_balance_due" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . $balanceDueType . '</b></td>' .
                            '<td id="tmp_balance_due" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(abs($balanceDue), 2) . '</b></td>' .
                            '</tr>' .
                            '</tbody>' .
                            '</table>' .
                            '</td></tr></table>' .
                            '</div>' .
                            '<div style="clear: both;"></div>' .
                            '</div>';

                    $htmlInvoiceTempTM = '<div style="margin-top:10px;"><b><u>Terms and Conditions</u></b>' .
                         /*   '<table style="width:100%;font-size: 10px;">' .
                            '<tbody><tr>' .
                            '<td style="padding-top:60px;padding-bottom: 8px;">' .
                            '<span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b>' .
                            '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding-bottom: 8px;">' .
                            '<span> 2. CAMPUS ID .</span><b><span id="corpIdNo">  U74999DL2016PTC301794 </span></b>' .
                            '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding-bottom: 8px;">' .
                            '<span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span>' .
                            '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding-bottom: 8px;">' .
                            '<span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                            '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding-bottom: 8px;">' .
                            '<span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                            '</td>' .
                            '</tr>' .
                            '<tr>' .
                            '<td style="padding-bottom: 8px;">' .
                            '<span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus </span></b>".</span>' .
                            '</td>' .
                            '</tr>' .
                            '</tbody>' .
                            '</table>' .*/
                            '</div><div style="font-size: 14px;"><b>*Please note this is a computer generated invoice and hence dose not need signature.</b></div>';
                    $htmlInvioceTemplateFooter = '</div> <div class="pcs-template-footer" style="background-color: #ffffff;  color: #aaaaaa; font-size: 6pt; height: 0.7in; padding: 0 0.4in 0 0.55in;"></div></div></body></html>';

                    // FINAL HTML
                    $finalHtml = $htmlInvioiceTemplateHeader . $htmlTableLogo . $htmlMemberDetail . ($invoiceItemTransHeader . $invoiceItemTransMid . $invoiceItemTransFooter) . $htmlInvoiceTax . $htmlInvoiceTempTM . $htmlInvioceTemplateFooter;
                    //print_r($finalHtml); exit;
                    //$dompdf_temp = new Dompdf();
                    //        global $dompdf;
                    //        $dompdf = new DOMPDF();
                    //  echo $finalHtml;
                    //   exit;
                    $dompdf = new Dompdf\Dompdf();
                    $dompdf->load_html($finalHtml);
                    //$dompdf_temp->set_paper("a4", "portrait");
                    $dompdf->set_paper(array(0, 0, 794, 1122), "portrait");
                    $dompdf->render();
                    $pdfOutput = $dompdf->output();

                    //echo $pdfOutput;die;
                    $fileLocation = APP_SERVER_ROOT . "/attachment/student_attachment/" . $resStudentData[0]['MEMBERSHIP_NUMBER'] . "-" . $key . "-" . time() . ".pdf";
                    $fp = fopen($fileLocation, "a+");
                    fwrite($fp, $pdfOutput);
                    fclose($fp);
                    unset($dompdf);

                    $finalAttachments[] = $fileLocation;
                }
            }
            $mailSubject = "INDE CAMPUS :General Invoice detail with attachment";
            if ($invoiceType == "ENROLLMENT") {
                $mailSubject = "INDE CAMPUS :Enrollment detail Invoice with attachment";
            } else if ($invoiceType == "ENROLLMENT") {
                $mailSubject = "INDE CAMPUS :Renewals Enrollment Invoice detail with attachment";
            } else if ($invoiceType == "GENERAL") {
                $mailSubject = "INDE CAMPUS :Enrollment Invoice detail with attachment";
            } else if ($invoiceType == "MISC") {
                $mailSubject = "INDE CAMPUS :General Invoice detail with attachment";
            }

            // START GETTING HTML TEMPLATE FOR EMAIL

            if ($invoiceType == 'ENROLLMENT') {
                $messageBodyMid = '<!DOCTYPE html>
                            <html class="no-js">

                               <head>
                                  <link rel="stylesheet" type="text/css" href="css/app.css">
                                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                  <meta name="viewport" content="width=device-width">
                                  <title>Charge Posting Email Template</title>
                                  <!-- <style> -->
                               </head>
                                <body style="background:#fff;">
                                    <table style="background:#0077c0; padding:10px 10px 10px 10px; width:100%; max-width:600px;margin:0 auto;">
                                        <tbody><tr>
                                            <td>
                                                <table width="100%" style="border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #0077c0 solid;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                                                        </tr>
                                                    </tbody>
                                                </table>  
                                                <table width="100%" style="background:#ffffff;border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"> <p style="margin:0 0 5px 0;"> Inde Campus is a chain of hostels who wish to ensure that the hostel experience one has gets duly shared with the world class. A bit of us can be seen in all our hostels through the personalized artworks and the little touches put in by our team to enable our student to distinctly remember us by. Student are encouraged to leave their mark at our hostels by way off wall art or by just getting involved.

We believe in providing affordable living with all basic amenities in place along with security. We strive to expand within the next few years to ensure that our student get to stay with us as a part of the inde campus family.</p> </td> 
                                                        </tr>';
                /*  <tr>
                  <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;">
                  <ol style="padding:0 0 0 20px; margin:0;">
                  <li>Student UserName :- ' . $resStudentData[0]['USER_NAME'] . '</li>
                  <li>Student PassWord :- 1234</li>
                  <li>Parent UserName :- ' . $parentUserNameSql[0]['USER_NAME'] . '</li>
                  <li>Parent PassWord :- 1234</li>
                  </ol>
                  </td>
                  </tr> */
                $messageBodyMid .='<tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;"> Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/> ' . $resClientTrans[0]['ADDRESS'] . '</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table width="100%" style="border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:25px 5px 25px 5px; background:#a6ce39; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                                                        </tr>
                                                    </tbody>      
                                                </table> 
                                            </td>
                                        </tr>            
                                    </tbody>
                                </table>
                            </body>
                            </html>';
            } else {
                $messageBodyMid = '<!DOCTYPE html>
                <html class="no-js">

                   <head>
                      <link rel="stylesheet" type="text/css" href="css/app.css">
                      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                      <meta name="viewport" content="width=device-width">
                      <title>Charge Posting Email Template</title>
                      <!-- <style> -->
                   </head>
                    <body style="background:#fff;">
                        <table style="background:#0077c0; padding:10px 10px 10px 10px; width:100%; max-width:600px;margin:0 auto;">
                            <tbody><tr>
                                <td>
                                    <table width="100%" style="border-spacing: 0px;">
                                        <tbody>
                                            <tr>
                                                <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #0077c0 solid;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                                            </tr>
                                        </tbody>
                                    </table>  
                                    <table width="100%" style="background:#ffffff;border-spacing: 0px;">
                                        <tbody>
                                            <tr>
                                                <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"> <p style="margin:0 0 5px 0;"> Inde Campus is a chain of hostels who wish to ensure that the hostel experience one has gets duly shared with the world class. A bit of us can be seen in all our hostels through the personalized artworks and the little touches put in by our team to enable our student to distinctly remember us by. Student are encouraged to leave their mark at our hostels by way off wall art or by just getting involved.

We believe in providing affordable living with all basic amenities in place along with security. We strive to expand within the next few years to ensure that our student get to stay with us as a part of the inde campus family.</p> </td> 
                                            </tr>
                                            <tr>
                                                <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;"> Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/> ' . $resClientTrans[0]['ADDRESS'] . '</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" style="border-spacing: 0px;">
                                        <tbody>
                                            <tr>
                                                <td valign="middel" style="padding:25px 5px 25px 5px; background:#a6ce39; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                                            </tr>
                                        </tbody>      
                                    </table> 
                                </td>
                            </tr>            
                        </tbody>
                    </table>
                </body>
                </html>

                ';
            }
            // EMD GETTING HTML TEMPLATE FOR EMAIL

            /* Mail sent code start */
            $sendgrid_username = "arunnagpal";
            $sendgrid_password = "devil1502";
            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

            $fromAddress = 'support@lithe.in';
            $fromName = 'Lithe Technology';
            $messageBody = $messageBodyMid;

            if ($mailSend == 1) {

                $email = new SendGrid\Email();

                $email->addTo($resStudentData[0]['EMAIL']);
                // $email->addToName($valueSender);
                $email->setFrom($fromAddress);
                $subject = $mailSubject;
                $email->setFromName($fromName);
                $email->setSubject($subject);
                if (!empty($finalAttachments)) {
                    foreach ($finalAttachments as $docKey => $docValue) {
                        if (file_exists(APP_SERVER_ROOT . "/attachment/student_attachment/" . basename($docValue))) {
                            $email->addAttachment(APP_SERVER_ROOT . "/attachment/student_attachment/" . basename($docValue), basename($docValue)); // add attachment 
                        }
                    }
                }

                //$messageBody = "please find attached document for ";
                $email->setHtml($messageBody);
                //print_r($email); exit;

                $response = $sendGrid->send($email);
                $responseMail = $response->getBody();

                if ($mailSend == 1 && $response) {
                    if (!empty($finalAttachments)) {
                        foreach ($finalAttachments as $docKey => $docValue) {
                            if (file_exists(APP_SERVER_ROOT . "/attachment/student_attachment/" . basename($docValue))) {
                                unlink(APP_SERVER_ROOT . "/attachment/student_attachment/" . basename($docValue)); // add attachment 
                            }
                        }
                    }
                }

                $result = array("status" => SCS);
                http_response_code(200);
            } else if ($mailSend == 0) {

                $insertCronMailMaster = Array();
                $insertCronMailMaster['FK_CLIENT_ID'] = $clientId;
                $insertCronMailMaster['TYPE_OF_MESSAGE'] = "MAIL";
                $insertCronMailMaster['SENDER_EMAIL'] = $fromAddress;
                $insertCronMailMaster['SENDER_NAME'] = $fromName;
                $insertCronMailMaster['RECIVER_EMAIL'] = $resStudentData[0]['EMAIL'];
                $insertCronMailMaster['RECIVER_NAME'] = $resStudentData[0]['STUD_NAME'];
                $insertCronMailMaster['MAIL_SUBJECT'] = $mailSubject;
                $insertCronMailMaster['MAIL_CONTENT'] = $messageBody;
                $insertCronMailMaster['ATTACHMENT_LINK'] = $fileLocation;
                $insertCronMailMaster['SEND_STATUS'] = "0";
                $insertCronMailMaster['TYPE_OF_MAIL'] = "INVOICE";
                $insertMailData = insert_rec(CRONMAILMASTER, $insertCronMailMaster);
                if (!$insertMailData) {
                    $result = array("status" => INSERTFAIL . " " . CRONMAILMASTER);
                    http_response_code(400);
                }
                $result = array("status" => SCS);
                http_response_code(200);
            }
        } else {
            $result = array("status" => "Error. Student Email not found.");
            http_response_code(400);
        }
    } else {
        $result = array("status" => "Error. No invoice Id found to process the email.");
        http_response_code(400);
    }

    return $result;
}

function sendEventMail($postData) {


    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $clientInvoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";


    $sqlStudentData = "SELECT CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,SM.PK_STUD_ID,SM.EMAIL,CONCAT(SM.ADDRESS_LINE1,',',SM.ADDRESS_LINE2,',',SM.ADDRESS_LINE3) AS ADDRESS,SM.PINCODE,CO.COUNTRY_NAME,CT.STATE_NAME,CT.CITY_NAME,SM.MEMBERSHIP_NUMBER FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  WHERE SM.DELETE_FLAG ='0' AND SM.PK_STUD_ID='" . $studentId . "' AND SM.FK_CLIENT_ID = '" . $clientId . "' AND SM.FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA

    $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

    $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0'"; // QUERY FOR CLIENT DATA

    $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA

    $sqlInvoiceTrans = "SELECT IT.ITEM_NAME,IT.TOTAL_CHARGES,IT.TOTAL_TAX,IT.PAYABLE_AMOUNT,IT.SGST,IT.CGST,IM.EVENT_NO,IM.ITEM_PRICE,IM.CGST,IM.SGST,IM.HSN_CODE,IM.EVENT_VENUE,IM.EVENT_START_DATETIME,IM.EVENT_END_DATETIME,IT.CREATED_BY_DATE,IT.QTY,IM.IGST FROM " . INVOICETRANSMASTER . " IT INNER JOIN " . ITEMMASTER . " IM ON IM.PK_ITEM_ID = IT.FK_ITEM_ID WHERE IT.FK_INVOICE_ID='" . $clientInvoiceId . "' AND IT.DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA


    $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA

    $sqlPaymentInvoice = "SELECT SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $clientInvoiceId . "' AND FK_STUDENT_ID ='" . $studentId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' GROUP BY(FK_INVOICE_ID)"; //QUERY FOR INVOICE PAYMENT DATA 
    $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);
    $totalPayment = 0;
    $prevBalance = 0;
    if (count($resPaymentInvoice) > 0) {
        $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
    }

    if (!empty($resStudentData[0]['EMAIL'])) {

        $sendgrid_username = "arunnagpal";
        $sendgrid_password = "devil1502";

        $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


        $html = ' <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" data-keyboard="false" data-backdrop="static" id="eventViewInvoiceDetail" class="modal fade">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" class="tooltips btn btn-sm btn-success printBtn" title="" style="position: absolute; right: 49px; top: 10px;" data-original-title="Print Booking Invoice" onclick="printInvoice();"><i class="fa fa-print"></i></a>
                  <h4 class="modal-title"> View Invoice </h4>
                  <span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020<br /><br />
              </div>
              <div class="modal-body" id="receiptPrint">
                <div style="background: #ffffff none repeat scroll 0 0; color: #333333; font-family:  sans-serif;font-size: 14px;" id="receiptPrintHtml">
                     <table style="width:100%;table-layout: fixed;">
                        <tbody>
                           <tr>
                              <td colspan="2" style="vertical-align: top; width:100%; text-align: center;">
                                  <span class="pcs-entity-title" style="color: #000000; font-size: 18pt;">Invoice</span><br>
                                  </td>
                           </tr>
                            <tr>
                              <td style="width:60%;vertical-align:bottom;word-wrap: break-word;line-height: 1.5;">
                                  <label style="font-size: 14pt;color:#000;font-weight: bold;padding: 3.2em 0;" class="label" >Event Booking </label>
                             </td>';
        // <img src="assets/admin1/img/logo-web.png" style="width: auto;height: 100px; float: right;">
        $html .= ' <td style="width:40%;vertical-align:bottom;word-wrap: break-word;">
                                 
                              </td>
                            </tr>
                            <tr>
                              <td style="width:48%; padding-top: 10px;vertical-align:top;word-wrap: break-word;line-height: 1.5;">
                                 <span style="color: #333333; font-size: 12pt;" ><span id="invoiceNoPrint"># ' . $clientInvoiceId . '</span></span><br><span style="white-space: pre-wrap; font-size: 9pt;" id="invoiceDatePrint">Invoice Date:  ' . date('d-m-Y', $resInvoiceTrans[0]['CREATED_BY_DATE']) . '</span><br>
                             </td>
                             <td style="width:50%;     padding-top: 10px;text-align: right;vertical-align:bottom;word-wrap: break-word;">
                                 <div><label style="font-size: 14pt; color:#817d7d" class="pcs-label" id="eventNamePrint">' . $resInvoiceTrans[0]['ITEM_NAME'] . '</label><br>
                                     <span class="student-name" style="color: #333333; font-size: 9pt">Start Date - Time : <span id="startDateTimePrint">' . date('d-m-Y H:i:s', $resInvoiceTrans[0]['EVENT_START_DATETIME']) . '</span></span>
                                     <br>
                                     <span class="student-name" style="color: #333333; font-size: 9pt;">Ent Date - Time : <span id="endDateTimePrint">' . date('d-m-Y H:i:s', $resInvoiceTrans[0]['EVENT_END_DATETIME']) . '</span></span>
                                     <br>
                                     <span style="white-space: pre-wrap; font-size: 9pt;" id="eventVenuePrint">' . $resInvoiceTrans[0]['EVENT_VENUE'] . '</span><br>
                                     <!-- <span style="white-space: pre-wrap; font-size: 9pt;">Ahmedabad Gujarat India - 380051</span><br> -->
                                 </div>
                              </td>
                            </tr>
                        </tbody>
                     </table>
                     <table style="width:100%;margin-top:10px;table-layout:fixed; line-height: 1.5;">
                        <tbody>                           
                           <tr>
                              <td style="width:60%;vertical-align:bottom;word-wrap: break-word;">
                                 <div><label style="font-size: 14pt; color:#817d7d" class="pcs-label">To,</label><br>
                                     <span class="student-name" style="color: #333333; font-size: 12pt;" id="studentNamePrint">' . $resStudentData[0]['STUD_NAME'] . '</span>
                                     <br>
                                     <span style="white-space: pre-wrap; font-size: 9pt;" id="studentAdd1Print">' . $resStudentData[0]['ADDRESS'] . '</span><br>
                                     <span style="white-space: pre-wrap; font-size: 9pt;" id="studentAdd2Print">' . $resStudentData[0]['CITY_NAME'] . ' ' . $resStudentData[0]['STATE_NAME'] . ' ' . $resStudentData[0]['COUNTRY_NAME'] . ' -' . $resStudentData[0]['PINCODE'] . '</span><br>
                                 </div>
                              </td>
                              <td style="vertical-align:bottom;width: 40%;" align="right"></td>
                           </tr>
                        </tbody>
                     </table>                    
                     <span class="student-name" id="student-detail" style="color: #333333; font-size: 10pt;"><br><b id="studMemIdPrint">Student Membership No : ' . $resStudentData[0]['MEMBERSHIP_NUMBER'] . '</b> </span>                  
                     <table style="width:100%;margin-top:10px;table-layout:fixed;" class="itemtable" cellspacing="0" cellpadding="0" border="0">
                        <thead>
                           <tr style="height:32px;">
                              <td style="padding:5px 0 5px 5px;text-align: center;word-wrap: break-word;width: 5%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header">#</td>
                              <td style="padding:5px 10px 5px 20px;word-wrap: break-word; background-color: #3c3d3a;color: #ffffff; font-size: 9pt; width: 15%;" class="itemtable-header itemtable-description">Particular</td>
                               <td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header" align="right">Rate</td>
                               <td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header" align="right">Qty</td>
                              <td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 10%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header" align="right">CGST</td>
                              <td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header" align="right">SGST</td>
                              <td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header" align="right">IGST</td>
                              <td style="padding:5px 10px 5px 5px;word-wrap: break-word;width:20%; background-color: #3c3d3a; color: #ffffff;font-size: 9pt;background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="itemtable-header" align="right">Amount</td>
                           </tr>
                        </thead>
                        <tbody class="itemBody" id="printItemBody1">';
        $totalSgst = $totalCgst = $totalItemPrice = $finalTotalPrice = $totalIgst = 0;
        foreach ($resInvoiceTrans as $keyInvoice => $valueInvoice) {
            $totalPrice = $valueInvoice['ITEM_PRICE'] * $valueInvoice['QTY'];
            $sgstAmt = ($valueInvoice['SGST'] > 0) ? $totalPrice * $valueInvoice['SGST'] / 100 : 0;
            $cgstAmt = ($valueInvoice['CGST'] > 0) ? $totalPrice * $valueInvoice['CGST'] / 100 : 0;
            $igstAmt = ($valueInvoice['IGST'] > 0) ? $totalPrice * $valueInvoice['IGST'] / 100 : 0;
            $totalSgst = $totalSgst + $sgstAmt;
            $totalCgst = $totalCgst + $cgstAmt;
            $totalIgst = $totalIgst + $igstAmt;
            $totalItemPrice = $totalItemPrice + $totalPrice;
            $finalTotalPrice = $finalTotalPrice + $totalPrice + $sgstAmt + $cgstAmt + $igstAmt;


            $html .= '<tr>

                              <td style="padding: 5px 0 5px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">1</td>
                              <td style="padding: 5px 0px 5px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="item-row" valign="top">
                                 <div>
                                    <div><span style="word-wrap: break-word;" id="item_name">' . $valueInvoice['ITEM_NAME'] . '</span><br><span class="item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">' . $valueInvoice['HSN_CODE'] . '</span></div>
                                 </div>
                              </td>
                              <td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">
                                 <span id="item_amount">
                                <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . $valueInvoice['ITEM_PRICE'] . '</span>
                              </td>
                              <td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">
                                 <span id="item_amount">
                                <span></span> ' . $valueInvoice['QTY'] . '</span>
                              </td>
                              <td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">
                                 <div id="item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($cgstAmt, 2) . '</div>
                                 <div class="item-desc" style="color: #727272; font-size: 8pt;">' . $valueInvoice['CGST'] . '% </div>
                              </td>
                              <td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">
                                 <div id="item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($sgstAmt, 2) . '</div>
                                 <div class="item-desc" style="color: #727272; font-size: 8pt;">' . $valueInvoice['SGST'] . '% </div>
                                      <td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right">
                                 <div id="item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($igstAmt, 2) . '</div>
                                 <div class="item-desc" style="color: #727272; font-size: 8pt;">' . $valueInvoice['IGST'] . '% </div>
                              </td>
                              <td style="text-align:right;padding: 5px 10px 5px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">
                                  <span id="item_amount">
                                  <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPrice, 2) . '</span></td>
                           </tr>';
        }

//  $totalSgst = $totalCgst = $totalItemPrice =$finalTotalPrice 
        $balanceDue = $prevBalance + $totalPayment - $finalTotalPrice;
        $balanceDueType = ($balanceDue > 0) ? 'CR' : 'DR';
//$balanceDue=0;
        $html .= '<tr>
                              <td colspan="8" style="text-align:right;">
                                 <table class="totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">
                                    <tbody>
                                       <tr>
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>
                                          <td colspan="2" id="subtotal" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalItemPrice, 2) . '</td>
                                       </tr>
                                       <tr style="height:10px;">
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>
                                          <td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCgst, 2) . '</td>
                                       </tr>
                                       <tr style="height:10px;">
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>
                                          <td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalSgst, 2) . '</td>
                                       </tr>
                                       <tr style="height:10px;">
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>
                                          <td colspan="2" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalIgst, 2) . '</td>
                                       </tr>
                                       <tr>
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>
                                          <td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>DR</b></td>
                                          <td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($finalTotalPrice, 2) . '</b></td>
                                       </tr> 
                                        <tr>
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>
                                          <td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>CR</b></td>
                                          <td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayment, 2) . '</b></td>
                                       </tr>
                                        <tr>
                                          <td colspan="6" style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Balance Due</b></td>
                                          <td id="total" style="width:50px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b>' . $balanceDueType . '</b></td>
                                          <td id="total" style="width:120px;padding: 5px 10px 5px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(abs($balanceDue), 2) . '</b></td>
                                       </tr><tr><td colspan="7"  style="font-size:16px;text-align: left;"><b><u>Terms and Conditions</u></b></td></tr>
                                         <tr><td colspan="7" style="padding-top:10px;padding-bottom: 4px;text-align: left;"><span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b></td></tr>
                                        <tr><td colspan="7" style="padding-bottom: 4px;text-align: left;"><span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b></td></tr>
                                        <tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATE FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span></td></tr>
                                        <tr><td colspan="7" style="padding-bottom: 4px;text-align: left;"><span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>
                                        <tr><td colspan="7" style="padding-bottom: 4px;text-align: left;"><span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span></td></tr>
                                        <tr><td colspan="5" style="padding-bottom: 4px;text-align: left;"><span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus</span></b>".</span></td></tr>
                                     </tbody>
                                 </table>
                              </td>
                            </tr> 
                        </tbody>
                     </table>
                </div>
              </div>
          </div>
      </div>
    </div>';
//'.($balanceDue >0)?'DR':'CR'.'
        $dompdf = new Dompdf\Dompdf();
        $dompdf->load_html($html);
//$dompdf_temp->set_paper("a4", "portrait");
        $dompdf->set_paper(array(0, 0, 794, 1122), "portrait");
        $dompdf->render();
        $pdfOutput = $dompdf->output();

//echo $pdfOutput;die;
        $fileLocation = APP_SERVER_ROOT . "/attachment/" . $resStudentData[0]['MEMBERSHIP_NUMBER'] . "-" . time() . ".pdf";
        $fp = fopen($fileLocation, "a+");
        fwrite($fp, $pdfOutput);
        fclose($fp);

        $fromAddress = 'support@lithe.in';
        $fromName = 'Lithe Technology';
        $email = new SendGrid\Email();
        $email->addTo($resStudentData[0]['EMAIL']);
// $email->addToName($valueSender);
        $email->setFrom($fromAddress);

        $allAttachmentArr[] = $fileLocation;

//print_r($allAttachmentArr); exit;
        if (!empty($allAttachmentArr)) {
            foreach ($allAttachmentArr as $docKey => $docValue) {
                if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                    $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                }
            }
        }


        $allAttachmentArr[] = $fileLocation;

//print_r($allAttachmentArr); exit;
        if (!empty($allAttachmentArr)) {
            foreach ($allAttachmentArr as $docKey => $docValue) {
                if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                    $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                }
            }
        }

        $subject = "INDE CAMPUS :Event Booking Invoice";
        $email->setFromName($fromName);
        $email->setSubject($subject);

// $messageBody = "please find attached document for Event Invoice";
        $messageBody = '<!DOCTYPE html>
<html class="no-js">
   <head>
      <link rel="stylesheet" type="text/css" href="css/app.css">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width">
      <title>Event Email Template</title>
      <!-- <style> -->
   </head>
    <body style="background:#fff;">
        <table style="background:#a6ce39; padding:10px 10px 10px 10px; width:100%; max-width:600px;margin:0 auto;">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" style="border-spacing: 0px;">
                            <tr>
                                <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #a6ce39 solid;"><img src="logo-web.png" alt="" style="width:50px; height:auto;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                            </tr>
                        </table>  
                        <table width="100%" style="background:#ffffff;border-spacing: 0px; border-bottom:4px #a6ce39 solid;">
                            <tbody>
                                <tr>
                                    <td valign="middel" style="padding:20px 10px 0px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"><span><b>Event Name : </b> <span>' . $resInvoiceTrans[0]['ITEM_NAME'] . '</span></span> <p style="margin:0 0 5px 0;"> INDECAMPUS STUDENT ACCOMMODATIONS (DD1) Private Limited is a Non-govt  Company.</p></td>
                                </tr>
                                <tr>                                    
                                <tr>
                                    <td>
                                        <table width="100%" >
                                            <tr>
                                               <td valign="middel" style="padding:10px 10px 10px 10px; text-align:center; color:#000; font-size:14px;font-family: arial; line-height: 18px; width: 33%;">
                                                   <span style="display:block;">
                                                       <b style="display:block; background:#0077c0;color:#ffffff;padding: 5px 5px;">Dates </b> 
                                                       <span style="display:block; background:#a6ce39; color:#ffffff; padding: 20px 5px;">' . date('d-m-Y', $resInvoiceTrans[0]['EVENT_START_DATETIME']) . '|' . date('d-m-Y', $resInvoiceTrans[0]['EVENT_END_DATETIME']) . '</span>
                                                   </span>                                              
                                               </td>
                                               <td valign="middel" style="padding:10px 10px 10px 10px; text-align:center; color:#000; font-size:14px;font-family: arial; line-height: 18px; width: 33%;">
                                                   <span style="display:block;">
                                                       <b style="display:block; background:#0077c0;color:#ffffff;padding: 5px 5px;">Event Venue </b> 
                                                       <span style="display:block; background:#a6ce39; color:#ffffff; padding: 20px 5px;">' . $resInvoiceTrans[0]['EVENT_VENUE'] . '</span>
                                                   </span>                                              
                                               </td>
                                               <td valign="middel" style="padding:10px 10px 10px 10px; text-align:center; color:#000; font-size:14px;font-family: arial; line-height: 18px; width: 33%;">
                                                   <span style="display:block;">
                                                       <b style="display:block; background:#0077c0;color:#ffffff;padding: 5px 5px;">Total Seat Book </b> 
                                                       <span style="display:block; background:#a6ce39; color:#ffffff; padding: 20px 5px;">' . $resInvoiceTrans[0]['QTY'] . '</span>
                                                   </span>                                              
                                               </td>
                                            </tr>
                                        </table>
                                    </td>                                                                        
                                </tr>
                                <tr>
                                    <td valign="middel" colspan="3" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;">Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/> ' . $resClientTrans[0]['ADDRESS'] . '</td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" style="border-spacing: 0px;">
                            <tbody>
                                <tr>
                                    <td valign="middel" style="padding:25px 5px 25px 5px; background:#0077c0; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                                </tr>
                            </tbody>      
                        </table> 
                    </td>
                </tr>            
            </tbody>
        </table>
    </body>
</html>

';
        $email->setHtml($messageBody);
//                       print_r($email); exit;
        $response = $sendGrid->send($email);
        $responseMail = $response->getBody();
        if ($email) {
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => PROBLEMSENDINGEMAIL);
            http_response_code(400);
        }
    } else {
        $result = array("status" => STUDENTMAILIDISNOTFOUND);
        http_response_code(400);
    }
    return $result;
}

function createDepartmentCategory($postData) {

// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $categoryName = isset($postData['postData']['categoryName']) ? addslashes(trim($postData['postData']['categoryName'])) : "";
    $parentCategoryId = isset($postData['postData']['parentCategoryId']) ? addslashes(trim($postData['postData']['parentCategoryId'])) : "";
    $departmentId = isset($postData['postData']['departmentId']) ? addslashes(trim($postData['postData']['departmentId'])) : "";
    $departmentCategoryId = isset($postData['postData']['departmentCategoryId']) ? addslashes(trim($postData['postData']['departmentCategoryId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($categoryName) && !empty($departmentId)) { // CHECK REQURIED FIELD CONDITION
        $whereCond = "";
        if (!empty($departmentCategoryId)) {
            $whereCond = " AND PK_ITEM_DEPARTMENT_ID !=" . $departmentCategoryId;
        }
        $sqlCheckItemExist = "SELECT PK_ITEM_DEPARTMENT_ID	FROM " . ITEMDEPARTMENTMST . " WHERE DEPARTMENT_CATEGORY_NAME ='" . $categoryName . "' AND FK_PARENT_DEPARTMENT_ID ='" . $parentCategoryId . "' AND FK_DEPARTMENT_TYPE_ID ='" . $departmentId . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'" . $whereCond; // QUERY FOR DEPARTMENT CATEGORY NAME IS ALREADY AVAILABLE OR NOT.
        $resCheckItemExist = fetch_rec_query($sqlCheckItemExist); // RESULT FOR DEPARTMENT CATEGORY NAME IS ALREADY AVAILABLE OR NOT.
        if (count($resCheckItemExist) > 0) {
            $result = array("status" => CATEGORYNAMEISEXIST);
            http_response_code(200);
        } else {
            $msg = "";
// ITEM INSERT ARRAY.
            $insertItemArray = Array(); //ITEM ARRAY CREATE
            $insertItemArray['DEPARTMENT_CATEGORY_NAME'] = $categoryName;
            $insertItemArray['FK_PARENT_DEPARTMENT_ID'] = $parentCategoryId;
            $insertItemArray['FK_DEPARTMENT_TYPE_ID'] = $departmentId;
            $insertItemArray['FK_CLIENT_ID'] = $clientId;
            $insertItemArray['FK_ORG_ID'] = $orgId;

            if (isset($departmentCategoryId) && $departmentCategoryId > 0) {
                $insertItemArray['CHANGED_BY'] = $userId;
                $insertItemArray['CHANGED_BY_DATE'] = time();
                $whereItemCond = "PK_ITEM_DEPARTMENT_ID ='" . $departmentCategoryId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                $updateItem = update_rec(ITEMDEPARTMENTMST, $insertItemArray, $whereItemCond); // UPDATE RECORD IN ITEM_MASTER TABLE.
                if (!$updateItem) {
                    $msg = UPDATEFAIL . " " . ITEMDEPARTMENTMST;
                }
            } else {
                $insertItemArray['CREATED_BY'] = $userId;
                $insertItemArray['CREATED_BY_DATE'] = time();
                $insertItem = insert_rec(ITEMDEPARTMENTMST, $insertItemArray); // INSERT RECORD IN ITEM_MASTER TABLE.          
                if (!isset($insertItem['lastInsertedId']) || $insertItem['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . ITEMDEPARTMENTMST;
                }
            }

            if ($msg == "") {
                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;

                $itemData = listDepartmentCategoryData($requestData); // GET ALL DEPARTMENT CATEGORY DATA.

                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];

                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => $msg);
                http_response_code(400);
            }
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.3.8.2018.

function departmentCategoryList($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $fkDepartmentId = isset($postData['postData']['departmentId']) ? addslashes(trim($postData['postData']['departmentId'])) : "";
    $categoryId = isset($postData['postData']['categotyId']) ? addslashes(trim($postData['postData']['categotyId'])) : "";
    $whereCond = "";
    if (!empty($categoryId) && $categoryId > 0) {
        $whereCond = " AND FK_PARENT_DEPARTMENT_ID='" . $categoryId . "'";
    } else {
        $whereCond = " AND FK_PARENT_DEPARTMENT_ID  ='0'";
    }
    $sqlDepartmentCatData = "SELECT PK_ITEM_DEPARTMENT_ID,DEPARTMENT_CATEGORY_NAME,FK_PARENT_DEPARTMENT_ID,FK_DEPARTMENT_TYPE_ID FROM " . ITEMDEPARTMENTMST . " WHERE FK_DEPARTMENT_TYPE_ID='" . $fkDepartmentId . "'  AND DELETE_FLAG =0 " . $whereCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' ORDER BY DEPARTMENT_CATEGORY_NAME DESC"; // QUERY FOR DEPARTMENT SERVICE LIST NAME

    $resDepartmentCatData = fetch_rec_query($sqlDepartmentCatData); // RESULT FOR DEPARTMENT SERVICE LIST NAME
    if (count($resDepartmentCatData) > 0) {
        $departmentCatArr = Array();
        foreach ($resDepartmentCatData as $keyDepartmentCat => $valueDepartmentCat) { // ITEM DATA FOREACH LOOP.
            $departmentCatArr[$keyDepartmentCat]['departmentCategotyId'] = $valueDepartmentCat['PK_ITEM_DEPARTMENT_ID'];
            $departmentCatArr[$keyDepartmentCat]['departmentCatName'] = $valueDepartmentCat['DEPARTMENT_CATEGORY_NAME'];
            $departmentCatArr[$keyDepartmentCat]['departmentId'] = $valueDepartmentCat['FK_DEPARTMENT_TYPE_ID'];
        }
        $result = array("status" => SCS, "data" => array_values($departmentCatArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
    }
    return $result;
}

//END BY DEVIKA.3.8.2018.



function listDepartmentCategoryData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $departmentCategoryId = isset($postData['postData']['departmentCategoryId']) ? addslashes(trim($postData['postData']['departmentCategoryId'])) : "";


    if (!empty($clientId) && !empty($orgId)) {
        $whereCond = "";
        if (!empty($departmentCategoryId)) {
            $whereCond = " AND PK_ITEM_DEPARTMENT_ID ='" . $departmentCategoryId . "'";
        }
        $sqlItemData = "SELECT IM.PK_ITEM_DEPARTMENT_ID,IM.DEPARTMENT_CATEGORY_NAME,IM.FK_PARENT_DEPARTMENT_ID,IM.FK_DEPARTMENT_TYPE_ID,CL.VALUE,(SELECT DEPARTMENT_CATEGORY_NAME FROM " . ITEMDEPARTMENTMST . " WHERE PK_ITEM_DEPARTMENT_ID =IM.FK_PARENT_DEPARTMENT_ID) AS CATEGORY_NAME FROM " . ITEMDEPARTMENTMST . " IM LEFT JOIN " . LOOKUPMASTER . " CL ON CL.PK_LOOKUP_ID = IM.FK_DEPARTMENT_TYPE_ID  WHERE IM.DELETE_FLAG ='0' AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "'" . $whereCond . " ORDER BY IM.PK_ITEM_DEPARTMENT_ID DESC"; // QUERY FOR ITEM DATA.

        $resItemData = fetch_rec_query($sqlItemData); // RESULT FOR ITEM DATA.
        if (count($resItemData) > 0) {
            foreach ($resItemData as $keyItem => $valueItem) { // ITEM DATA LOOP
                $itemArr[$keyItem]['departmentCategoryId'] = $valueItem['PK_ITEM_DEPARTMENT_ID'];
                $itemArr[$keyItem]['categoryName'] = $valueItem['DEPARTMENT_CATEGORY_NAME'];
                $itemArr[$keyItem]['categoryId'] = $valueItem['FK_PARENT_DEPARTMENT_ID'];
                $itemArr[$keyItem]['departmentId'] = $valueItem['FK_DEPARTMENT_TYPE_ID'];
                $itemArr[$keyItem]['departmentName'] = $valueItem['VALUE'];
                $itemArr[$keyItem]['parentCategoryName'] = ($valueItem['CATEGORY_NAME'] == null || $valueItem['CATEGORY_NAME'] == "") ? "" : $valueItem['CATEGORY_NAME'];
            }
            $result = array("status" => SCS, "data" => $itemArr);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);

        http_response_code(400);
    }
    return $result;
}

function deleteDepartmentCategory($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $departmentCategoryId = isset($postData['postData']['departmentCategoryId']) ? addslashes(trim($postData['postData']['departmentCategoryId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($departmentCategoryId)) {

        $checkDepartmentComplaintAvailabel = "SELECT PK_COMPLAINT_ID FROM " . COMPLAINTMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND (DEPARTMENT_CATGORY_ID ='" . $departmentCategoryId . "' OR DEPARTMENT_SUBCAT_ID ='" . $departmentCategoryId . "') AND DELETE_FLAG ='0'";

        $resDepartmentComplaint = fetch_rec_query($checkDepartmentComplaintAvailabel);
        if (count($resDepartmentComplaint) == 0) {
            $updateItemArray = Array(); //ITEM ARRAY UPDATE
            $updateItemArray['DELETE_FLAG'] = 1;
            $updateItemArray['CHANGED_BY'] = $userId;
            $updateItemArray['CHANGED_BY_DATE'] = time();

            $whereItemUpdate = "PK_ITEM_DEPARTMENT_ID =" . $departmentCategoryId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND DELETE_FLAG = '0'";
            $updateItem = update_rec(ITEMDEPARTMENTMST, $updateItemArray, $whereItemUpdate); // UPDATE DELETE_FLAG =1 IN ITEM_MASTER TABLE.
            if ($updateItem) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;

                $itemData = listDepartmentCategoryData($requestData); // GET ALL DEPARTMENT CATEGORY DATA.

                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . ITEMDEPARTMENTMST);
                http_response_code(400);
            }
        } else {
            $result = array("status" => DEPARTMENTCATEGORYNOTDELETED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function updateChargePosting($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $chargePostingId = isset($postData['postData']['chargePostingId']) ? addslashes(trim($postData['postData']['chargePostingId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "";
    $price = isset($postData['postData']['price']) ? addslashes(trim($postData['postData']['price'])) : "";

    if (!empty($orgId) && !empty($orgId) && !empty($orgId) && !empty($orgId) && !empty($orgId)) {

        if (is_numeric($qty)) {
            $checkItemData = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_PRICE,ITEM_TYPE,CGST,SGST,IGST FROM " . ITEMMASTER . " WHERE DELETE_FLAG='0' AND PK_ITEM_ID ='" . $itemId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
            $resCheckItemData = fetch_rec_query($checkItemData);
            if (count($resCheckItemData) > 0) {
                foreach ($resCheckItemData as $keyItem => $valueItem) {

                    $itemName = $valueItem['ITEM_NAME'];
                    $itemPrice = $valueItem['ITEM_PRICE'];
                    $itemCgst = $valueItem['CGST'];
                    $itemSgst = $valueItem['SGST'];
                    $itemIgst = $valueItem['IGST'];
                    $qtyOfSession = (isset($qty) && $qty > 1) ? $qty : 1;
                    $calItemBasicTPrice = $itemPrice * $qtyOfSession;
                    $cgstAmt = ($itemCgst > 0) ? ($calItemBasicTPrice * $itemCgst) / 100 : 0;
                    $sgstAmt = ($itemSgst > 0) ? ($calItemBasicTPrice * $itemSgst) / 100 : 0;
                    $igstAmt = ($itemIgst > 0) ? ($calItemBasicTPrice * $itemIgst) / 100 : 0;

                    $calTotalTax = $cgstAmt + $sgstAmt + $igstAmt;
                    $calItemTotalPrice = $calItemBasicTPrice + $calTotalTax;
                }
            } else {
                $itemCgst = $itemSgst = $itemIgst = $calTotalTax = $cgstAmt = $sgstAmt = 0;
                $itemPrice = $price * $qty;
                $calItemTotalPrice = $price;
            }

            $chargePostingData = Array();
            $chargePostingData['QTY_OF_SESSION'] = $qty;
            $chargePostingData['BIILED_SESSION'] = $qty;
            $chargePostingData['ITEM_BASE_PRICE'] = $itemPrice;
            $chargePostingData['TOTAL_TAX'] = $calTotalTax;
            $chargePostingData['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;
            $chargePostingData['CHANGED_BY'] = $userId;
            $chargePostingData['CHANGED_BY_DATE'] = time();

            $whereUpdateCond = "PK_CHARGE_ID ='" . $chargePostingId . "'";
            $updateChargePosting = update_Rec(CHARGEMASTER, $chargePostingData, $whereUpdateCond); // UPDATE IN CHARGE POSTING TABLE.
            if ($updateChargePosting) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['chargePostingId'] = $chargePostingId;
                $chargePostingData = listChargePosting($requestData); // GET ALL NON PROCESSING CHARGE POSTING DATA.
                $chargePostingAllData = ($chargePostingData['status'] == SCS) ? $chargePostingData['data'] : $chargePostingData['status'];
                $result = array("status" => SCS, "data" => $chargePostingAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . CHARGEMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => QTYISONLYNUMERIC);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function sendCustomerInvoiceMail($postData) {


    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $customerId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $invoiceNo = isset($postData['postData']['invoiceNo']) ? addslashes(trim($postData['postData']['invoiceNo'])) : "";

    if (!empty($orgId) && !empty($clientId) && !empty($customerId) && !empty($invoiceNo)) {



        $sqlInvoiceTrans = "SELECT ITEM_NAME,TOTAL_CHARGES,TOTAL_TAX,PAYABLE_AMOUNT,SGST,CGST FROM " . INVOICETRANSMASTER . " WHERE FK_INVOICE_ID='" . $invoiceNo . "' AND DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA

        $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA



        $sqlStudentData = "SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS CUST_NAME,EMAIL_ID AS EMAIL FROM " . CUSTOMERMASTER . " WHERE PK_CUST_ID='" . $customerId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA
        $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

        $sqlInvoiceTrans = "SELECT ITEM_NAME,TOTAL_CHARGES,TOTAL_TAX,PAYABLE_AMOUNT,SGST,CGST,IGST,HSN_CODE FROM " . INVOICETRANSMASTER . " WHERE FK_INVOICE_ID='" . $invoiceNo . "' AND DELETE_FLAG='0'"; // QUERY FOR INVOICE TRANS DATA


        $resInvoiceTrans = fetch_rec_query($sqlInvoiceTrans); //RESULT FOR INVOICE TRANS DATA
//echo "<pre>"; print_r($resInvoiceTrans); exit;

        $sqlPaymentInvoice = "SELECT SUM(PAID_AMOUNT) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $invoiceNo . "' AND FK_STUDENT_ID ='" . $customerId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; //QUERY FOR INVOICE PAYMENT DATA 
        $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);

        $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0'"; // QUERY FOR CLIENT DATA

        $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA

        $totalPayment = 0;
        if (count($resPaymentInvoice) > 0) {
            $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
        }

        if (!empty($resStudentData[0]['EMAIL'])) {

            /* Mail sent code start */
            $sendgrid_username = "arunnagpal";
            $sendgrid_password = "devil1502";

            $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));


            $totalPrevBalance = "";
            $totalPrevBalanceType = "";
            $htmlInvioiceTemplateHeader = '<!DOCTYPE html>' .
                    '<html lang="en">' .
                    '<head>' .
                    '<meta charset="utf-8">' .
                    '<meta name="viewport" content="width=device-width, initial-scale=1.0">' .
                    '<meta name="description" content="">' .
                    '<meta name="author" content="Mosaddek">' .
                    '<link rel="shortcut icon" href="img/favicon.ico">' .
                    '<title></title>' .
                    '</head>' .
                    '<body class="invoice-page" data-gr-c-s-loaded="true"> <div style="background: #ffffff none repeat scroll 0 0; color: #333333; font-family: DejaVu Sans; sans-serif;font-size: 9pt;"><div class="pcs-template-header pcs-header-content" id="header" style="height: 0.1in; padding: 0 0.4in 0 0.55in; background-color: #ffffff; color: #333333; font-size: 9pt;"></div><div class="pcs-template-body" style="padding: 0 0.4in 0 0.55in;">';

            $htmlTableLogo = '<table style="width:100%;table-layout: fixed;">' .
                    '<tbody>' .
                    '<tr>' .
                    '<td colspan = 2 style="vertical-align: top; width:100%; text-align: center;">' .
                    '<span class="pcs-entity-title" style="color: #000000; font-size: 18pt;">Tax Invoice</span><br>
                        <span style="font-size:16px; line-height:22px;">Indecampus Student Accommodations (DD1) Private Limited</span><br>Address of Principal Place - 126, Vasant Vihar, Dehradun,Uttarakhand, 248001<br>Registered Address - D-158B, Okhla Industrial Area Phase-1, New Delhi-110020"<br /><br /><br />
                    </address>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="vertical-align: top; width:50%;">' .
                   
                    '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice No   &nbsp; &nbsp;: ' . $invoiceNo . '</b></span><br>' .
                    '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Invoice Date : ' . date("d-m-Y") . '</b></span><br>' .
                    // '<span id="tmp_entity_number" style="font-size: 10pt; color:#817d7d" class="pcs-label"><b>Period From : ' . $getInvoiceDetail['data'][0]['invoicePeroidFrom'] . '</b></span>' .
                    '</td>' .
                    '<td style="vertical-align: top; text-align:right;width:50%;">' .
                    '<div>' .
                    // '<img src="' . APP_SERVER_ROOT_FRONTEND . '/img/gulmoharInvLogo.png" style="width:160.00px;height:129.00px;" id="logo_content">' .
                    '</div>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody>' .
                    '</table>';

            $htmlMemberDetail = '<br /><br /><table style="clear:both;width:100%;margin-top:30px;table-layout:fixed;">' .
                    '<tbody><tr><td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                    '</td>' .
                    '<td style="vertical-align:bottom;width: 40%;" align="right"></td>' .
                    '</tr><tr>' .
                    '<td style="width:60%;vertical-align:bottom;word-wrap: break-word;">' .
                    '<div><label style="font-size: 14pt; color:#817d7d" class="pcs-label" id="tmp_billing_address_label">To,</label><br>' .
                    '<span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 12pt;">' . $resStudentData[0]['CUST_NAME'] . '</span><br>' .
                    '</div>' .
                    '</td>' .
                    '<td style="vertical-align:bottom;width: 40%;" align="right">' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>';

            //  $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;">' . $gstinNo . '<br><b>Membership No</b> : ' . $getInvoiceDetail['data'][0]['memId'] . '</span>' . '<table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
            $invoiceItemTransHeader = '<br><span class="pcs-customer-name" id="zb-pdf-customer-detail" style="color: #333333; font-size: 10pt;"><table style="width:100%;margin-top:20px;table-layout:fixed;" class="pcs-itemtable" cellspacing="0" cellpadding="0" border="0">' .
                    '<thead>' .
                    '<tr style="height:32px;">' .
                    '<td style="padding:5px 0 5px 5px;text-align: center;word-wrap: break-word;width: 5%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header">#</td>' .
                    '<td style="padding:5px 10px 5px 20px;word-wrap: break-word; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;width: 15%; " class="pcs-itemtable-header pcs-itemtable-description">Particular</td>' .
                    //'<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 11%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>'.
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">CGST</td>' .
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">SGST</td>' .
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width: 15%; background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">IGST</td>' .
                    '<td style="padding:5px 10px 5px 5px;word-wrap: break-word;width:22%; background-color: #3c3d3a; color: #ffffff;font-size: 9pt;background-color: #3c3d3a;color: #ffffff; font-size: 9pt;" class="pcs-itemtable-header" align="right">Amount</td>' .
                    '</tr>' .
                    '</thead>' .
                    '<tbody class="itemBody">';
            $invoiceItemTransMid = "";
            if (!empty($resInvoiceTrans)) {
                $totalSgstAmt = $totalCgstAmt = $totalCharges = $totalPayable = $totalIgstAmt = 0;
                foreach ($resInvoiceTrans as $keyTrans => $valueTrans) {

                    if (!empty($valueTrans['PAYABLE_AMOUNT'])) {
                        $sgstAmt = $cgstAmt = $igstAmt = 0;
                        $sgstAmt = ($valueTrans['SGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['SGST'] / 100 : 0;
                        $cgstAmt = ($valueTrans['CGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['CGST'] / 100 : 0;
                        $igstAmt = ($valueTrans['IGST'] > 0) ? $valueTrans['TOTAL_CHARGES'] * $valueTrans['IGST'] / 100 : 0;
                        $totalSgstAmt = $totalSgstAmt + $sgstAmt;
                        $totalCgstAmt = $totalCgstAmt + $cgstAmt;
                        $totalIgstAmt = $totalIgstAmt + $igstAmt;

                        $invoiceItemTransMid .= '<tr>' .
                                '<td style="padding: 10px 0 10px 5px;text-align: center;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top">' . ($keyTrans + 1) . '</td>' .
                                '<td style="padding: 10px 0px 10px 20px;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" ><div><div><span style="word-wrap: break-word;" id="tmp_item_name">' . $valueTrans['ITEM_NAME'] . '</span><br><span class="pcs-item-sku" id="tmp_item_hsn" style="color: #444444; font-size: 10px; margin-top: 2px;">' . $valueTrans['HSN_CODE'] . '</span></div></div></td>' .
                                //'<td <td style="padding: 10px 10px 5px 10px;text-align:right;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><span id="tmp_item_qty"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> '.number_format($valueTrans['totalCharges'],2).'</span></td>'.
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($sgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['SGST'] . '% </div></td>' .
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($cgstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['CGST'] . '% </div></td>' .
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word;  background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top" align="right"><div id="tmp_item_tax_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($igstAmt, 2) . '</div><div class="pcs-item-desc" style="color: #727272; font-size: 8pt;">' . $valueTrans['IGST'] . '% </div></td>' .
                                '<td style="text-align:right;padding: 10px 10px 10px 5px;word-wrap: break-word; background-color: #ffffff;border-bottom: 1px solid #e3e3e3;  color: #000000;  font-size: 8pt;" class="pcs-item-row" valign="top"><span id="tmp_item_amount"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($valueTrans['TOTAL_CHARGES'], 2) . '</span></td>' .
                                '</tr>';

                        $totalCharges = $totalCharges + ($valueTrans['TOTAL_CHARGES']);
                        $totalPayable = $totalPayable + $valueTrans['PAYABLE_AMOUNT'];
                    }
                }
            }

            $invoiceItemTransFooter = '</tbody></table>';
            $prevBalance = 0;
            $balanceDue = $prevBalance + $totalPayment - $totalPayable;
            $balanceDueType = ($balanceDue > 0) ? 'CR' : 'DR';

            $htmlInvoiceTax = '<div style="width: 100%;margin-top: 1px; float:right;">' .
                    //'<div style="width: 45%;padding: 10px 10px 3px 3px;font-size: 9pt;float: left;">'.
                    //'<div style="white-space: pre-wrap;"><p>&nbsp;</p></div>'.
                    //'</div>'.
                    '<div style="width: 100%;">' .
                    '<table width="100%"><tr><td style="width:50%; float:left;"></td><td style="text-align:right;width:45%; float:right;">' .
                    '<table class="pcs-totals" cellspacing="0" border="0" style="width:100%; background-color: #ffffff;color: #000000;font-size: 9pt;">' .
                    '<tbody>' .
                    '<tr>' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">Sub Total</td>' .
                    //  '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                    '<td colspan="2"  id="tmp_subtotal" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCharges, 2) . '</td>' .
                    '</tr>' .
                    '<tr style="height:10px;">' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">CGST  </td>' .
                    //  '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .
                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalSgstAmt, 2) . '</td>' .
                    '</tr>' .
                    '<tr style="height:10px;">' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">SGST  </td>' .
                    //   '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .

                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalCgstAmt, 2) . '</td>' .
                    '</tr>' .
                    '<tr style="height:10px;">' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right">IGST  </td>' .
                    //  '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(($totalTax / 2), 2) . '</td>' .
                    '<td colspan="2"  style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalIgstAmt, 2) . '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Total</b></td>' .
                    '<td id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>DR</b></td>' .
                    //  '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format((($totalCharges + $totalTax < 0) ? (($totalCharges + (($totalTax < 0) ? $totalTax * -1 : $totalTax) * -1) * -1) : $totalCharges + (($totalTax < 0) ? $totalTax * -1 : $totalTax)), 2) . '</b></td>' .
                    '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayable, 2) . '</b></td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding: 5px 10px 5px 0;" valign="middle" align="right"><b>Collection</b></td>' .
                    '<td  id="tmp_total" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>CR</b></td>' .
                    '<td id="tmp_total" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format($totalPayment, 2) . '</b></td>' .
                    '</tr>' .
                    '<tr style="height:40px; background-color: #f5f4f3; color: #000000; font-size: 9pt; font-size: 8pt; background-color: #f5f4f3;color: #000000; font-size: 9pt;" class="pcs-balance">' .
                    '<td style="padding:5px 10px 5px 0;" valign="middle" align="right"><b>Balance Due</b></td>' .
                    '<td id="tmp_balance_due" style="width:50px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b>' . $balanceDueType . '</b></td>' .
                    '<td id="tmp_balance_due" style="width:120px;padding: 10px 10px 10px 5px;" valign="middle" align="right"><b><span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span> ' . number_format(abs($balanceDue), 2) . '</b></td>' .
                    '</tr>' .
                    '</tbody>' .
                    '</table>' .
                    '</td></tr></table>' .
                    '</div>' .
                    '<div style="clear: both;"></div>' .
                    '</div><br /><br />';

            //$balanceChargeType
            //number_format((($balanceCharge+$totalTax < 0) ? ($balanceCharge+$totalTax)*-1 : $balanceCharge+$totalTax),2)
            $htmlInvoiceTempTM="";
           /* $htmlInvoiceTempTM = '<br /><br /><br /><div style="margin-top:10px;"><b><u>Terms and Conditions</u></b>' .
                    '<table style="width:100%;font-size: 10px;">' .
                    '<tbody><tr>' .
                    '<td style="padding-top:60px;padding-bottom: 8px;">' .
                    '<span> 1. GSTIN NO.</span><b><span id="serviceTaxNo"> 05AAECI1768G1ZN </span></b>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 2. CAMPUS ID .</span><b><span id="corpIdNo"> U74999DL2016PTC301794 </span></b>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 3. STUDENT ACTIVITY CHARGES SHOULD BE PAID WITHIN 30 DAYS FROM BILL DATEM FAILING WHICH 2% INTEREST PLUS 3% PENALTY WILL CHARGED ON BILL AMOUNT.</span>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 4. THIS BILL DOES NOT INCLUDE PAYMENT MADE AFTER THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 5. BALANCE SHOWS THE BALANCE AS ON THE LAST DATE OF THE MONTH FOR WHICH THIS BILL IS ISSUED.</span>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom: 8px;">' .
                    '<span> 6. ALL CHEQUES/DD TO BE DRAWN IN FAVOUR OF "<b><span id="clientName"> indecampus </span></b>".</span>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody>' .
                    '</table>' .
                    '</div><div style="font-size: 14px;"><b>*Please note this is a computer generated invoice and hence dose not need signature.</b></div>';*/
            $htmlInvioceTemplateFooter = '</div> <div class="pcs-template-footer" style="background-color: #ffffff;  color: #aaaaaa; font-size: 6pt; height: 0.7in; padding: 0 0.4in 0 0.55in;"></div></div></body></html>';

            // FINAL HTML
            $finalHtml = $htmlInvioiceTemplateHeader . $htmlTableLogo . $htmlMemberDetail . ($invoiceItemTransHeader . $invoiceItemTransMid . $invoiceItemTransFooter) . $htmlInvoiceTax . $htmlInvoiceTempTM . $htmlInvioceTemplateFooter;
            //print_r($finalHtml); exit;
            //$dompdf_temp = new Dompdf();
            $dompdf = new Dompdf\Dompdf();
            $dompdf->load_html($finalHtml);
            //$dompdf_temp->set_paper("a4", "portrait");
            $dompdf->set_paper(array(0, 0, 794, 1122), "portrait");
            $dompdf->render();
            $pdfOutput = $dompdf->output();

            //echo $pdfOutput;die;
            $fileLocation = APP_SERVER_ROOT . "/attachment/" . time() . ".pdf";
            $fp = fopen($fileLocation, "a+");
            fwrite($fp, $pdfOutput);
            fclose($fp);

            $messageBodyHeader = '<!DOCTYPE html>
<html class="no-js">

   <head>
      <link rel="stylesheet" type="text/css" href="css/app.css">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width">
      <title>Student Enrollment Email Template</title>
      <!-- <style> -->
   </head>
    <body style="background:#fff;">
        <table style="background:#a6ce39; width:100%; max-width:600px; padding:10px 10px 10px 10px;margin:0 auto;">
            <tr>
                <td>
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #a6ce39 solid;"> <br/> Dear ' . $resStudentData[0]['CUST_NAME'] . '</td>
                        </tr>
                    </table>  
                    <table width="100%" style="background:#ffffff;border-spacing: 0px;">
                        <tr>
                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"> <p style="margin:0 0 5px 0;">Inde Campus is a chain of hostels who wish to ensure that the hostel experience one has gets duly shared with the world class. A bit of us can be seen in all our hostels through the personalized artworks and the little touches put in by our team to enable our student to distinctly remember us by. Student are encouraged to leave their mark at our hostels by way off wall art or by just getting involved.

We believe in providing affordable living with all basic amenities in place along with security. We strive to expand within the next few years to ensure that our student get to stay with us as a part of the inde campus family.</p>  </td>
                        </tr>
                        <tr>
                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;">Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/>' . $resClientTrans[0]['ADDRESS'] . ' </td>
                        </tr>
                    </table>
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td valign="middel" style="padding:25px 5px 25px 5px; background:#0077c0; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                        </tr>
                    </table> 
                </td>
            </tr>            
        </table>
    </body>
</html>

';

            $messageBody = $messageBodyHeader;
            //    $fromAddress = $resultSms[0]['CLIENT_EMAIL'];
            //   $fromName = $resultSms[0]['CLIENTNAME'];

            $fromAddress = 'support@lithe.in';
            $fromName = 'Lithe Technology';
            $email = new SendGrid\Email();
            $email->addTo($resStudentData[0]['EMAIL']);
            // $email->addToName($valueSender);
            $email->setFrom($fromAddress);

            $allAttachmentArr[] = $fileLocation;

            //print_r($allAttachmentArr); exit;
            if (!empty($allAttachmentArr)) {
                foreach ($allAttachmentArr as $docKey => $docValue) {
                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                        $email->addAttachment(APP_SERVER_ROOT . "/attachment/" . basename($docValue), basename($docValue)); // add attachment 
                    }
                }
            }

            $subject = "INDE CAMPUS :Property Invoice With Attachment";
            $email->setFromName($fromName);
            $email->setSubject($subject);

            //$messageBody = "please find attached document for ";
            $email->setHtml($messageBody);
            //                       print_r($email); exit;
            $response = $sendGrid->send($email);
            $responseMail = $response->getBody();


            if (!empty($allAttachmentArr)) {
                foreach ($allAttachmentArr as $docKey => $docValue) {
                    if (file_exists(APP_SERVER_ROOT . "/attachment/" . basename($docValue))) {
                        unlink(APP_SERVER_ROOT . "/attachment/" . basename($docValue)); // add attachment 
                    }
                }
            }
            /* Mail sent code End */
            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => EMAILIDISNOTFOUND);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function deleteEvent($postData) {
//POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $itemType = isset($postData['postData']['itemType']) ? addslashes(trim($postData['postData']['itemType'])) : "";

    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($itemId)) {

        $checkEventBooking = "SELECT FK_ITEM_ID FROM " . EVENTSBOOKING . " WHERE FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_ITEM_ID ='" . $itemId . "' AND DELETE_FLAG ='0' AND QTY != '0'";

        $resEventBooking = fetch_rec_query($checkEventBooking);
        if (count($resEventBooking) == 0) {
            $updateItemArray = Array(); //ITEM ARRAY UPDATE
            $updateItemArray['DELETE_FLAG'] = 1;
            $updateItemArray['CHANGED_BY'] = $userId;
            $updateItemArray['CHANGED_BY_DATE'] = time();

            $whereItemUpdate = " PK_ITEM_ID =" . $itemId . " AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG = '0'";
            $updateItem = update_rec(ITEMMASTER, $updateItemArray, $whereItemUpdate); // UPDATE DELETE_FLAG =1 IN ITEM_MASTER TABLE.
            if ($updateItem) {

                $requestData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['itemType'] = $itemType;
                $roomData = getItemsData($requestData); // GET ALL ITEM DATA.

                $roomAllData = ($roomData['status'] == SCS) ? $roomData['data'] : $roomData['status'];
                $result = array("status" => SCS, "data" => $roomAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . ITEMMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => EVENTNOTDELETED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function createCreditNotes($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    if (!empty($clientId) && !empty($orgId)) {

        $sqlChargeData = "SELECT INVOICE_ID,IS_TRANSFER  FROM " . CHARGEMASTER . " WHERE INVOICE_FLAG ='1' AND IS_TRANSFER ='2' AND TRANSFER_REF_ID ='0'";

        $resChargeData = fetch_rec_query($sqlChargeData);
        if (count($resChargeData) > 0) {
            foreach ($resChargeData as $keyCharge => $valueCharge) {

                $sqlCheckCreditNotes = "SELECT FK_INVOICE_ID,PK_CREDIT_ID FROM " . CREDITMASTER . " WHERE FK_INVOICE_ID ='" . $valueCharge['INVOICE_ID'] . "'";
                $resCheckCreditNotes = fetch_rec_query($sqlCheckCreditNotes);
                $checkCreditNotesRecord = 0;
                if (count($resCheckCreditNotes) > 0) {
                    $checkCreditNotesRecord = count($resCheckCreditNotes);
                    $creditMasterId = $resCheckCreditNotes[0]['PK_CREDIT_ID'];
                }

                // CHANGED BY KAVITA PATEL ON 22-08-2018 START
                $qryInvoiceData = "SELECT C_INVOICE_ID,INVOICE_TYPE,INVOICE_REF_ID,FK_STUDENT_ID,FK_CUST_ID,INVOICE_DATE,INVOICE_NAME,TOTAL_CHARGES,OPEN_BALANCE,OPEN_BALANCE_TYPE,COLLECTION_LAST_MONTH,COLLECTION_LAST_MONTH_TYPE,CLOSING_THIS_MONTH,TYPE_OF_CLOSING_THIS_MONTH,TALLY_OPENING,TYPE_OF_TALLY_OPENING,NARRATION,PERIOD_FROM,PERIOD_TO FROM " . INVOICEMASTER . " WHERE C_INVOICE_ID='" . $valueCharge['INVOICE_ID'] . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";

                $sqlInvoiceData = fetch_rec_query($qryInvoiceData); // GET DATA FROM invoice master table
                if (count($sqlInvoiceData) > 0 && $checkCreditNotesRecord == 0) {

                    $insertCreditMaster['FK_CLIENT_ID'] = $clientId;
                    $insertCreditMaster['FK_ORG_ID'] = $orgId;
                    $insertCreditMaster['FK_INVOICE_ID'] = $sqlInvoiceData[0]['C_INVOICE_ID'];
                    $insertCreditMaster['INVOICE_TYPE'] = $sqlInvoiceData[0]['INVOICE_TYPE'];
                    $insertCreditMaster['INVOICE_REF_ID'] = $sqlInvoiceData[0]['INVOICE_REF_ID'];
                    $insertCreditMaster['FK_STUDENT_ID'] = $sqlInvoiceData[0]['FK_STUDENT_ID'];
                    $insertCreditMaster['FK_CUST_ID'] = $sqlInvoiceData[0]['FK_CUST_ID'];
                    $insertCreditMaster['INVOICE_DATE'] = $sqlInvoiceData[0]['INVOICE_DATE'];
                    $insertCreditMaster['INVOICE_NAME'] = $sqlInvoiceData[0]['INVOICE_NAME'];
                    // $insertCreditMaster['TOTAL_CHARGES'] = $sqlInvoiceData[0]['TOTAL_CHARGES'];
                    $insertCreditMaster['OPEN_BALANCE'] = $sqlInvoiceData[0]['OPEN_BALANCE'];
                    $insertCreditMaster['OPEN_BALANCE_TYPE'] = $sqlInvoiceData[0]['OPEN_BALANCE_TYPE'];
                    $insertCreditMaster['COLLECTION_LAST_MONTH'] = $sqlInvoiceData[0]['COLLECTION_LAST_MONTH'];
                    $insertCreditMaster['COLLECTION_LAST_MONTH_TYPE'] = $sqlInvoiceData[0]['COLLECTION_LAST_MONTH_TYPE'];
                    $insertCreditMaster['CLOSING_THIS_MONTH'] = $sqlInvoiceData[0]['CLOSING_THIS_MONTH'];
                    $insertCreditMaster['TYPE_OF_CLOSING_THIS_MONTH'] = $sqlInvoiceData[0]['TYPE_OF_CLOSING_THIS_MONTH'];
                    $insertCreditMaster['TALLY_OPENING'] = $sqlInvoiceData[0]['TALLY_OPENING'];
                    $insertCreditMaster['TYPE_OF_TALLY_OPENING'] = $sqlInvoiceData[0]['TYPE_OF_TALLY_OPENING'];
                    $insertCreditMaster['NARRATION'] = $sqlInvoiceData[0]['NARRATION'];
                    $insertCreditMaster['PERIOD_FROM'] = $sqlInvoiceData[0]['PERIOD_FROM'];
                    $insertCreditMaster['PERIOD_TO'] = $sqlInvoiceData[0]['PERIOD_TO'];
                    $insertCreditMaster['CREATED_BY'] = $userId;
                    $insertCreditMaster['CREATED_BY_DATE'] = time();
                    $insertCreditMasterData = insert_rec(CREDITMASTER, $insertCreditMaster);
                    if (isset($insertCreditMasterData['lastInsertedId']) && $insertCreditMasterData['lastInsertedId'] > 0) {
                        $creditMasterId = $insertCreditMasterData['lastInsertedId'];
                    }
                }
                $whereChargePostingCond = "INVOICE_FLAG ='1' AND INVOICE_ID ='" . $valueCharge['INVOICE_ID'] . "' AND IS_TRANSFER ='2' AND TRANSFER_REF_ID ='0'";

                // GET DATA FROM CHARGE MASTER TABLE
                $getChargeData = fetch_rec_query("SELECT * FROM " . CHARGEMASTER . " WHERE $whereChargePostingCond");


                //  echo "<pre>"; print_r($getChargeData); exit;
                foreach ($getChargeData as $keycharge => $valTrans) {
                    $totalPrice = $getChargeData[0]['ITEM_TOTAL_PRICE'];
                    $insertInvoiceTrans = array();
                    $insertInvoiceTrans['FK_CREDIT_ID'] = $creditMasterId;
                    $insertInvoiceTrans['FK_INVOICE_ID'] = $valueCharge['INVOICE_ID'];
                    $insertInvoiceTrans['CHARGE_TYPE'] = 'STUDENT';
                    $insertInvoiceTrans['FK_STUDENT_ID'] = $valTrans['STUDENT_ID'];
                    $insertInvoiceTrans['FK_ITEM_ID'] = $valTrans['FK_ITEM_ID'];
                    $insertInvoiceTrans['REF_ITEM_ID'] = $valTrans['REF_ITEM_ID'];
                    $insertInvoiceTrans['ITEM_NAME'] = $valTrans['ITEM_NAME'];
                    $insertInvoiceTrans['ITEM_BASIC_PRICE'] = $valTrans['ITEM_BASE_PRICE'];
                    $insertInvoiceTrans['QTY'] = $valTrans['QTY_OF_SESSION'];
                    $insertInvoiceTrans['SGST'] = $valTrans['TAX_SGST'];
                    $insertInvoiceTrans['CGST'] = $valTrans['TAX_CGST'];
                    $insertInvoiceTrans['IGST'] = $valTrans['TAX_IGST'];
                    $insertInvoiceTrans['TOTAL_CHARGES'] = $valTrans['ITEM_BASE_PRICE'];
                    $insertInvoiceTrans['TOTAL_TAX'] = $valTrans['TOTAL_TAX'];
                    $insertInvoiceTrans['PAYABLE_AMOUNT'] = $valTrans['ITEM_TOTAL_PRICE'];
                    $insertInvoiceTrans['CREATED_BY'] = $userId;
                    $insertInvoiceTrans['CREATED_BY_DATE'] = time();
                    $insertInvoiceTransData = insert_rec(CREDITTRANS, $insertInvoiceTrans);
                    // CHANGED BY KAVITA PATEL ON 22-08-2018 END 

                    /* START UPDATE IN CHARGE MASTER */
                    $updateArr = Array();
                    $updateArr['TRANSFER_REF_ID'] = $creditMasterId;
                    $updateArr['CHANGED_BY'] = $userId;
                    $updateArr['CHANGED_BY_DATE'] = time();

                    $whereUpdateCond = "INVOICE_ID ='" . $valueCharge['INVOICE_ID'] . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND PK_CHARGE_ID='" . $valTrans['PK_CHARGE_ID'] . "'";

                    $updateCreditTrans = update_rec(CHARGEMASTER, $updateArr, $whereUpdateCond);
                    /* END UPDATE IN CHARGE MASTER */
                }
                $sqlInvoiceTransTotal = "SELECT SUM(PAYABLE_AMOUNT) AS PAYABLE_AMOUNT FROM " . CREDITTRANS . " WHERE FK_CREDIT_ID='" . $creditMasterId . "' AND DELETE_FLAG ='0'";
                $resInvoiceTransTotal = fetch_rec_query($sqlInvoiceTransTotal);
                if (count($resInvoiceTransTotal) > 0) {
                    $totalPrice = $resInvoiceTransTotal[0]['PAYABLE_AMOUNT'];
                }
                /* START UPDATE IN CREDIT MASTER */
                $updateArr = Array();
                $updateArr['TOTAL_CHARGES'] = $totalPrice;
                $updateArr['CHANGED_BY'] = $userId;
                $updateArr['CHANGED_BY_DATE'] = time();

                $whereUpdateCond = "FK_INVOICE_ID ='" . $valueCharge['INVOICE_ID'] . "' AND DELETE_FLAG ='0'";
                $updateCreditTrans = update_rec(CREDITMASTER, $updateArr, $whereUpdateCond);
                /* START UPDATE IN CREDIT MASTER */
            }
        }
        $result = array("status" => SCS);
        http_response_code(200);
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}
?>

