<?php

function masterDataUpload($postdata, $postFile) {
//    print_r($postData);exit;
    $postData = $postdata['postData'];
//    print_r($postData);exit;
    $clientId = isset($postData['clientId']) ? $postData['clientId'] : ""; // CLIENT ID
    $userId = isset($postData['userId']) ? $postData['userId'] : ""; // USER ID
    $orgId = isset($postData['orgId']) ? $postData['orgId'] : ""; // ORG ID
    $uploadType = isset($postData['uploadType']) ? trim($postData['uploadType']) : ""; // UPLOAD TYPE 
    $attachmentName = $postFile; // ATTACH NAME    

    require_once(APP_SERVER_ROOT . '/include/parsecsv.lib.php');

    $csv = new parseCSV();

    $uploadPath = APP_SERVER_ROOT . "/attachment/masterCsv/";
    $uploadAttachment = uploadImage($attachmentName, $uploadPath, 'upload');
//    print_r($uploadAttachment);exit;
    if ($uploadAttachment['status'] == SCS) {
        if ($uploadType == "item_master") {

            $csvDelimiters = $csv->auto($uploadAttachment['fileName']);
//            print_r($csvDelimiters);exit;
            $eachLineValidation = validateCsv($csv->data, $csv->titles, $postData);
//            print_r($eachLineValidation);exit;
            if ($eachLineValidation['status'] == FAILMSG && count($eachLineValidation['error']) > 0) {
                $form_errors = array();

                foreach ($eachLineValidation['error'] as $keyError => $valueError) {
                    $form_errors[] = $valueError;
                }
                unlink($uploadAttachment['fileName']);
                $result = array("status" => ERROR, "data" => $form_errors);
                http_response_code(400);
            } else {
                if (!empty($csv->data)) {
//                    print_r($csv->data);exit;
                    $importData = array();
                    foreach ($csv->data as $key => $value) {

                        $insertItemArray = Array(); //ITEM ARRAY CREATE
                        $insertItemArray['ITEM_NAME'] = $value['Item Name'];
                        $insertItemArray['FK_CLIENT_ID'] = $clientId;
                        $insertItemArray['FK_ORG_ID'] = $orgId;
                        $insertItemArray['FK_USER_ID'] = $userId;
                        $insertItemArray['TALLY_NAME'] = $value['Tally Name'];
                        $insertItemArray['HSN_CODE'] = $value['hsn Code'];
                        $insertItemArray['ITEM_TYPE'] = $value['Item Type'];
                        $insertItemArray['ITEM_PRICE'] = $value['Item Price'];
                        $insertItemArray['DESCRIPTION'] = $value['Description'];
                        $insertItemArray['QTY'] = $value['Qty'];
                        $insertItemArray['CGST'] = $value['Cgst'];
                        $insertItemArray['SGST'] = $value['Sgst'];
                        $insertItemArray['IGST'] = $value['Igst'];
                        $insertItemArray['ITEM_VALIDITY_END_DATE'] = $value['Item Validity End Date'];
                        // $insertItemArray['EVENT_START_DATETIME'] = $value['Event Start Datetime'];
                        // $insertItemArray['EVENT_END_DATETIME'] = $value['Event End Datetime'];

                        $checkItemSql = fetch_rec_query("SELECT PK_ITEM_ID FROM " . ITEMMASTER . " WHERE ITEM_NAME = '" . $value['Item Name'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND DELETE_FLAG = '0' LIMIT 0,1"); //CHECK ITEM_NAME EXISTS OR NOT


                        if (count($checkItemSql) > 0) {//ITEM_NAME EXISTS THEN UPDATE RECORD
                            $insertItemArray['CHANGED_BY'] = $userId;
                            $insertItemArray['CHANGED_BY_DATE'] = time();
                            $whereItemCond = "PK_ITEM_ID ='" . $checkItemSql[0]['PK_ITEM_ID'] . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
                            $insertItem = update_rec(ITEMMASTER, $insertItemArray, $whereItemCond); //UPDATE DATA
                        } else {
                            $insertItemArray['CREATED_BY'] = $userId;
                            $insertItemArray['CREATED_BY_DATE'] = time();
                            $insertItem = insert_rec(ITEMMASTER, $insertItemArray); //INSERT DATA
                        }
                    }

                    if ($insertItem) {
                        $successMsg = "Successfully item upload data into database.";
                        $result = array("status" => $successMsg);
                        unlink($uploadAttachment['fileName']); // REMOVE FILE NAME AFTER PROCESS
                        http_response_code(200);
                    }
                } else {
                    $result = array("status" => NORECORDS);
                    unlink($uploadAttachment['fileName']);
                    http_response_code(400);
                }
            }
        } else if ($uploadType == "item_department") {

            $csvDelimiters = $csv->auto($uploadAttachment['fileName']);
            $eachLiveValidation = validateCsv($csv->data, $csv->titles, $postData);
            if ($eachLineValidation['status'] == FAILMSG && count($eachLineValidation['error']) > 0) {
                $form_errors = array();

                foreach ($eachLineValidation['error'] as $keyError => $valueError) {
                    $form_errors[] = $valueError;
                }
                unlink($uploadAttachment['fileName']);
                $result = array("status" => ERROR, "data" => $form_errors);
                http_response_code(400);
            } else {
                if (!empty($csv->data)) {
                    $importData = array();
                    foreach ($csv->data as $key => $value) {

                        $insertItemArray = Array(); //ITEM ARRAY CREATE
                        $insertItemArray['DEPARTMENT_CATEGORY_NAME'] = $value['Department category Name'];
                        $insertItemArray['FK_PARENT_DEPARTMENT_ID'] = $value['Fk Parent Department Id'];
                        $insertItemArray['FK_DEPARTMENT_TYPE_ID'] = $value['Fk Department Type Id'];
                        $insertItemArray['FK_CLIENT_ID'] = $clientId;
                        $insertItemArray['FK_ORG_ID'] = $orgId;


                        $checkItemSql = fetch_rec_query("SELECT PK_ITEM_DEPARTMENT_ID FROM " . ITEMDEPARTMENTMST . " WHERE DEPARTMENT_CATEGORY_NAME = '" . $value['Department category Name'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND DELETE_FLAG = '0' LIMIT 0,1"); //CHECK DEPARTMENT_CATEGORY_NAME IS EXISTS OR NOT


                        if (count($checkItemSql) > 0) {//DEPARTMENT_CATEGORY_NAME EXISTS THEN UPDATE RECORD
                            $insertItemArray['CHANGED_BY'] = $userId;
                            $insertItemArray['CHANGED_BY_DATE'] = time();
                            $whereItemCond = "PK_ITEM_DEPARTMENT_ID ='" . $checkItemSql[0]['PK_ITEM_DEPARTMENT_ID'] . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
                            $insertItem = update_rec(ITEMDEPARTMENTMST, $insertItemArray, $whereItemCond); //UPDATE DATA
                        } else {
                            $insertItemArray['CREATED_BY'] = $userId;
                            $insertItemArray['CREATED_BY_DATE'] = time();
                            $insertItem = insert_rec(ITEMDEPARTMENTMST, $insertItemArray); //INSERT DATA
                        }
                    }

                    if ($insertItem) {
                        $successMsg = "Successfully department upload data into database.";
                        $result = array("status" => $successMsg);
                        unlink($uploadAttachment['fileName']); // REMOVE FILE NAME AFTER PROCESS
                        http_response_code(200);
                    }
                } else {
                    $result = array("status" => NORECORDS);
                    unlink($uploadAttachment['fileName']);
                    http_response_code(400);
                }
            }
        } else if ($uploadType == "property_master") {

            $csvDelimiters = $csv->auto($uploadAttachment['fileName']);
            $eachLineValidation = validateCsv($csv->data, $csv->titles, $postData);
            if ($eachLineValidation['status'] == FAILMSG && count($eachLineValidation['error']) > 0) {
                $form_errors = array();

                foreach ($eachLineValidation['error'] as $keyError => $valueError) {
                    $form_errors[] = $valueError;
                }
                unlink($uploadAttachment['fileName']);
                $result = array("status" => ERROR, "data" => $form_errors);
                http_response_code(400);
            } else {
                if (!empty($csv->data)) {
                    $importData = array();
                    foreach ($csv->data as $key => $value) {

                        $insertItemArray = Array(); //ITEM ARRAY CREATE
                        $insertItemArray['PROPERTY_NAME'] = $value['Property Name'];
                        $insertItemArray['FK_CLIENT_ID'] = $clientId;
                        $insertItemArray['FK_ORG_ID'] = $orgId;
                        $insertItemArray['PROPERTY_VALIDITY_END_DATE'] = $value['Property Validity End Date'];
                        $insertItemArray['CGST'] = $value['Cgst'];
                        $insertItemArray['SGST'] = $value['Sgst'];
                        $insertItemArray['IGST'] = $value['Igst'];
                        $insertItemArray['PRICE'] = $value['Price'];
                        $insertItemArray['TYPE'] = $value['Type'];
                        $insertItemArray['HSN_CODE'] = $value['Hsn Code'];
                        $insertItemArray['TALLY_NAME'] = $value['Tally Name'];

                        $checkItemSql = fetch_rec_query("SELECT PK_PROPERTY_ID FROM " . PROPERTYMASTER . " WHERE PROPERTY_NAME = '" . $value['Property Name'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "' AND DELETE_FLAG = '0' LIMIT 0,1"); //CHECK PROERTY NAME EXISTS OR NOT


                        if (count($checkItemSql) > 0) {//PROPERTY NAME EXISTS THEN UPDATE RECORD
                            $insertItemArray['CHANGED_BY'] = $userId;
                            $insertItemArray['CHANGED_BY_DATE'] = time();
                            $whereItemCond = "PK_PROPERTY_ID ='" . $checkItemSql[0]['PK_PROPERTY_ID'] . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
                            $insertItem = update_rec(PROPERTYMASTER, $insertItemArray, $whereItemCond); //UPDATE DATA
                        } else {
                            $clinetPropertyId = generateId(PROPERTYMASTER, 'PRO', $clientId);

                            $insertItemArray['PK_PROPERTY_ID'] = $clinetPropertyId;
                            $insertItemArray['CREATED_BY'] = $userId;
                            $insertItemArray['CREATED_BY_DATE'] = time();
                            $insertItem = insert_rec(PROPERTYMASTER, $insertItemArray); //INSERT DATA
                        }


                        // PROCESS TO UPLOAD DEPARTMENT DATA
                        // 1.CHECK THE DEPARTNAME IS EXIST INTO SYSTEM - IF EXIST THEN UPDATE ELSE CREATE.
                        // $importData;
                    }

                    if ($insertItem) {
                        $successMsg = "Successfully property upload data into database.";
                        $result = array("status" => $successMsg);
                        unlink($uploadAttachment['fileName']); // REMOVE FILE NAME AFTER PROCESS
                        http_response_code(200);
                    }
                } else {
                    $result = array("status" => NORECORDS);
                    unlink($uploadAttachment['fileName']);
                    http_response_code(400);
                }
            }
        } else if ($uploadType == "user_master") {

            $csvDelimiters = $csv->auto($uploadAttachment['fileName']);
            $eachLineValidation = validateCsv($csv->data, $postData, $csv->titles);
//            $eachLineValidation['status'] = SCS;
//           print_r($eachLineValidation);exit;
            //print_r($csv->titles); exit;
            if ($eachLineValidation['status'] == FAILMSG && count($eachLineValidation['error']) > 0) {

//                for ($a = 0; $a < count($eachLineValidation['error']); $a++) {
//                    $form_errors[] = $eachLineValidation['error'][$a];
//                }
                foreach ($eachLiveValidation['error'] as $keyEachLineValidation => $valueEachLineValidation) {

                    $form_errors[] = $valueEachLineValidation;
                }
                unlink($uploadAttachment['fileName']);
                $result = array("status" => ERROR, "data" => $form_errors);
                http_response_code(400);
            } else {
                if (!empty($csv->data)) {

                    foreach ($csv->data as $key => $value) {
//                        print_r($value);exit;
                        //GETTING COMMA SEPRATE BRANCH NAME 
                        if (!empty($value['Branch'])) {
                            $explodeBranch = explode(',', $value['Branch']);
                            if (!empty($explodeBranch) && $explodeBranch != "") {

                                foreach ($explodeBranch as $branchKey => $branchValue) {

                                    $sqlCheckorg = "SELECT PK_ORG_ID FROM " . ORGMASTER . " WHERE ORGNAME = '" . $branchValue . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' LIMIT 0,1 ";
                                    $resCheckOrg = fetch_rec_query($sqlCheckorg);

                                    if (count($resCheckOrg) > 0) {
                                        $branchId[] = $resCheckOrg[0]['PK_ORG_ID'];
                                    }
                                }
                            }
                        }

//                      CHECK USER LEVEL FROM CLIENTLOOKUP FOR GETTING PK_CLT_LKUP_ID
                        $sqlCheckUserLevel = "SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE LOOK_VALUE = '" . $value['User Level'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'USERLEVEL' AND DELETE_FLAG = '0' LIMIT 0,1";
                        $resultCheckUserLevel = fetch_rec_query($sqlCheckUserLevel);
                        if (count($resultCheckUserLevel) > 0) {
                            $userLevel = $resultCheckUserLevel[0]['PK_CLT_LKUP_ID'];
                        }

//                        CHECK Supervisor Name FROM USERMASTER FOR GETTING PK_USER_ID
                        $sqlCheckSuperId = "SELECT PK_USER_ID FROM " . USERMASTER . " WHERE FULLNAME = '" . $value['Supervisor Name'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' LIMIT 0,1";
                        $resultCheckSuperId = fetch_rec_query($sqlCheckSuperId);
                        if (count($resultCheckSuperId) > 0) {
                            $superVisorId = $resultCheckSuperId[0]['PK_USER_ID'];
                        } else {
                            $superVisorId = '0';
                        }

//                        CALL getDesignation FOR MAKE NEW DESIGNATION
                        $checkDesignation = getDesignation($clientId, $orgId, $value['Designation'], $userId);
                        $desig = $checkDesignation['data'];

//                        CHECK ROLE NAME FROM ROLEMASTER FOR GETTING PK_ROLE_ID
                        $sqlCheckRole = "SELECT PK_ROLE_ID FROM " . ROLEMASTER . " WHERE ROLE_NAME = '" . $value['Role'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' LIMIT 0,1";
                        $resultCheckRole = fetch_rec_query($sqlCheckRole);
                        if (count($resultCheckRole) > 0) {
                            $rollId = $resultCheckRole[0]['PK_ROLE_ID'];
                        }

                        if (!empty($desig) && !empty($rollId) && !empty($branchId) && !empty($userLevel)) {

                            $importUserArr = array();
                            $importUserArr['postData']['requestCase'] = 'createUser';
                            $importUserArr['postData']['clientId'] = $clientId;
                            $importUserArr['postData']['orgId'] = $orgId;
                            $importUserArr['postData']['userId'] = $userId;
                            $importUserArr['postData']['userdata']['userName'] = $value['Username'];
                            $importUserArr['postData']['userdata']['fullName'] = $value['Full Name'];
                            $importUserArr['postData']['userdata']['userType'] = '1';
                            $importUserArr['postData']['userdata']['address'] = $value['Address'];
                            $importUserArr['postData']['userdata']['contactNo'] = $value['Mobile No'];
                            $importUserArr['postData']['userdata']['email'] = $value['Email Address'];
                            $importUserArr['postData']['userdata']['passWord'] = !empty($value['Password']) ? $value['Password'] : "1234";

                            $importUserArr['postData']['userdata']['levelId'] = $userLevel;
                            $importUserArr['postData']['userdata']['supervisorId'] = $superVisorId;
                            $importUserArr['postData']['userdata']['designationId'] = $desig;
                            $importUserArr['postData']['userdata']['birthdate'] = $value['Birth Date'];
                            $importUserArr['postData']['userdata']['anniversarydate'] = $value['Anniversary Date'];
                            $importUserArr['postData']['userdata']['gender'] = ucfirst(strtolower($value['Gender']));
                            $importUserArr['postData']['userdata']['dashboard'] = 'V1.0';
                            $importUserArr['postData']['branch'] = $branchId;
                            $importUserArr['postData']['roleId'] = $rollId;
                            $importUserArr['postData']['googleCalKey'] = $value['Google Calender Email'];
                            $importUserArr['postData']['role'] = '1';
//                            print_r($importUserArr);exit;
                            $createUser = createEditUser_BL($importUserArr);
                            if ($createUser) {
                                $insertData = '1';
                            }
                        }
                    }

                    if ($insertData) {
                        $successMsg = "Successfully user upload data into database.";
                        $result = array("status" => $successMsg);

                        unlink($uploadAttachment['fileName']); // REMOVE FILE NAME AFTER PROCESS
                        http_response_code(200);
                    } else {
                        $successMsg = "Successfully user upload data into database Some of data are miss match.";
                        $result = array("status" => $successMsg);

                        unlink($uploadAttachment['fileName']); // REMOVE FILE NAME AFTER PROCESS
                        http_response_code(400);
                    }
                } else {
                    $result = array("status" => NORECORDS);
                    unlink($uploadAttachment['fileName']);
                    http_response_code(400);
                }
            }
        }
    }

    return $result;
}

function validateCsv($csvData, $csvTitleData, $requestData) {
//    print_r($postData);exit;
    $postData = $csvData;
    $error = array();
    $lineNo = 0;
    if ($requestData['uploadType'] == "item_master") {

        // START - CHECK ALL THE TITLE IS RIGHT OR NOT
        if (isset($csvTitleData[0]) && $csvTitleData[0] != 'Item Name') {
            $error[] = "Enter Correct Title : Item Name";
        }
        if (isset($csvTitleData[1]) && $csvTitleData[1] != 'Tally Name') {
            $error[] = "Enter Correct Title : Tally Name";
        }
        if (isset($csvTitleData[2]) && $csvTitleData[2] != 'hsn Code') {
            $error[] = "Enter Correct Title : hsn Code";
        }
        if (isset($csvTitleData[3]) && $csvTitleData[3] != 'Item Type') {
            $error[] = "Enter Correct Title : Item Type";
        }
        if (isset($csvTitleData[4]) && $csvTitleData[4] != 'Item Price') {
            $error[] = "Enter Correct Title : Item Price";
        }
        if (isset($csvTitleData[5]) && $csvTitleData[5] != 'Description') {
            $error[] = "Enter Correct Title : Description";
        }
        if (isset($csvTitleData[6]) && $csvTitleData[6] != 'Qty') {
            $error[] = "Enter Correct Title : Qty";
        }
        if (isset($csvTitleData[7]) && $csvTitleData[7] != 'Cgst') {
            $error[] = "Enter Correct Title : Cgst";
        }
        if (isset($csvTitleData[8]) && $csvTitleData[8] != 'Sgst') {
            $error[] = "Enter Correct Title : Sgst ";
        }
        if (isset($csvTitleData[9]) && $csvTitleData[9] != 'Igst') {
            $error[] = "Enter Correct Title : Igst";
        }
        if (isset($csvTitleData[10]) && $csvTitleData[10] != 'Item Validity End Date') {
            $error[] = "Enter Correct Title : Item Validity End Date";
        }
        // if (isset($csvTitleData[11]) && $csvTitleData[11] != 'Event Start Datetime') {
        //     $error[] = "Enter Correct Title : Event Start Datetime";
        // }
        // if (isset($csvTitleData[12]) && $csvTitleData[12] != 'Event End Datetime') {
        //     $error[] = "Enter Correct Title : Event End Datetime";
        // }
        // END - CHECK ALL THE TITLE IS RIGHT OR NOT
        // START CHECKING ALL THE ITEM NAME
        $selectCloumnFullname = array_search("Item Name", $csvTitleData);
        $departmentName = array_column($postData, $csvTitleData[$selectCloumnFullname]);
        $departmentNameExit = array();

        foreach ($postData as $key => $value) {

            $errorLine = $lineNo++; // THIS LINE WILL GIVE YOU CSV LINE NUMBER 
            // START CHECKING EVERY RECORDS WITH VALIDATION LIST

            if (!empty($value['Item Name']) && $value['Item Name'] != "") { // CHECK IF SAME DEPART NAME IS AVALIABLE INTO FILE
                $duplicateDepertment = array_search($value['Item Name'], $departmentName); // THIS WILL SEARCH USERNAME IN ALL COLUMN VALUE 
//                   print_r($duplicateCust);exit;
                if (!empty($departmentNameExit) && in_array($duplicateDepertment, $departmentName)) {
                    $lineNod = $duplicateDepertment + 2;
                    $error[] = "Same Item Name on line No " . '<b>' . $errorLine . '</b>' . " With Line No " . '<b>' . $lineNod . '</b>';
                }
                $departmentNameExit[] = $duplicateDepertment; //INSERT SEARCH COLUMN KEY IN ARRAY    
            }

            // THIS FUNCTION IS TO CHECK THE EMAIL ADDRESS VALIDATION 
//            if(!filter_var($value['Email Address'], FILTER_VALIDATE_EMAIL) === false){
//
//            }else{
//                $error[] = "Email is not valid On Record No " .$errorLine; 
//            }
            // FOLLOWING FUNCTION WILL CHECK WITH DATABASE TO CHECK UNIQUE NESS INTO NAME WITH ANY FILEDS
//            $sqlCheckUserName = "SELECT PK_USER_ID FROM ".USERMASTER." WHERE  USERNAME = '".$value['Username']."' AND DELETE_FLAG = 0";
//            $resultCheckUserName = fetch_rec_query($sqlCheckUserName);
//            if (count($resultCheckUserName) > 0) {
//                $error[] = "Enter unique UserName On Record No" .$errorLine; 
//            } 
            //THIS WILL BE CHECK IF CSV FIELED VALUE IN NOT EMPTY
            if (empty($value['Item Name']) && $value['Item Name'] == "") {
                $error[] = "Enter Item Name On Record No" . $errorLine;
            }

            if (empty($value['Item Type']) && $value['Item Type'] == "") {
                $error[] = "Enter Item Type On Record No" . $errorLine;
            }

            if (empty($value['Tally Name']) && $value['Tally Name'] == "") {
                $error[] = "Enter Tally Name On Record No" . $errorLine;
            }

            if (empty($value['Item Price']) && $value['Item Price'] == "" && !is_numeric($value['Item Price'])) {
                $error[] = "Enter Item Price On Record No" . $errorLine;
            }

            if (empty($value['Qty']) && $value['Qty'] == "" && !is_numeric($value['Qty'])) {
                $error[] = "Enter Qty On Record No" . $errorLine;
            }

            if (empty($value['Sgst']) && $value['Sgst'] == "" && !is_numeric($value['Sgst'])) {
                $error[] = "Enter Sgst On Record No" . $errorLine;
            }

            if (empty($value['Cgst']) && $value['Cgst'] == "" && !is_numeric($value['Cgst'])) {
                $error[] = "Enter Cgst On Record No" . $errorLine;
            }

            if (empty($value['Igst']) && $value['Igst'] == "" && !is_numeric($value['Igst'])) {
                $error[] = "Enter Igst On Record No" . $errorLine;
            }
        }
    } elseif ($requestData['uploadType'] == "item_department") {
        // START - CHECK ALL THE TITLE IS RIGHT OR NOT

        if (isset($csvTitleData[0]) && $csvTitleData[0] != 'Department category Name') {
            $error[] = "Enter Correct Title : Department category Name";
        }
        if (isset($csvTitleData[1]) && $csvTitleData[1] != 'Fk Parent Department Id') {
            $error[] = "Enter Correct Title : Fk Parent Department Id";
        }
        if (isset($csvTitleData[2]) && $csvTitleData[2] != 'Fk Department Type Id') {
            $error[] = "Enter Correct Title : Fk Department Type Id";
        }

        // END - CHECK ALL THE TITLE IS RIGHT OR NOT
        // START CHECKING ALL THE DEPARTMENT NAME
        $selectCloumnFullname = array_search("Department category Name", $csvTitleData);
        $departmentName = array_column($postData, $csvTitleData[$selectCloumnFullname]);
        $departmentNameExit = array();

        foreach ($postData as $key => $value) {

            $errorLine = $lineNo++; // THIS LINE WILL GIVE YOU CSV LINE NUMBER 
            // START CHECKING EVERY RECORDS WITH VALIDATION LIST

            if (!empty($value['Department category Name']) && $value['Department category Name'] != "") { // CHECK IF SAME DEPART NAME IS AVALIABLE INTO FILE
                $duplicateDepertment = array_search($value['Department category Name'], $departmentName); // THIS WILL SEARCH USERNAME IN ALL COLUMN VALUE 
                if (!empty($departmentNameExit) && in_array($duplicateDepertment, $departmentName)) {
                    $lineNod = $duplicateDepertment + 2;
                    $error[] = "Same Department category Name on line No " . '<b>' . $errorLine . '</b>' . " With Line No " . '<b>' . $lineNod . '</b>';
                }
                $departmentNameExit[] = $duplicateDepertment; //INSERT SEARCH COLUMN KEY IN ARRAY    
            }



            //THIS WILL BE CHECK IF CSV FIELED VALUE IN NOT EMPTY
            if (empty($value['Department category Name']) && $value['Department category Name'] == "") {
                $error[] = "Enter Department category Name On Record No" . $errorLine;
            }

            if (empty($value['Fk Parent Department Id']) && $value['Fk Parent Department Id'] == "" && !is_numeric($value['Fk Parent Department Id'])) {
                $error[] = "Enter Fk Parent Department Id On Record No" . $errorLine;
            }

            if (empty($value['Fk Department Type Id']) && $value['Fk Department Type Id'] == "" && !is_numeric($value['Fk Department Type Id'])) {
                $error[] = "Enter Fk Department Type Id On Record No" . $errorLine;
            }
        }
    } elseif ($requestData['uploadType'] == "property_master") {

        // START - CHECK ALL THE TITLE IS RIGHT OR NOT

        if (isset($csvTitleData[0]) && $csvTitleData[0] != 'Property Name') {
            $error[] = "Enter Correct Title : Property Name";
        }
        if (isset($csvTitleData[1]) && $csvTitleData[1] != 'Property Validity End Date') {
            $error[] = "Enter Correct Title : Property Validity End Date";
        }
        if (isset($csvTitleData[2]) && $csvTitleData[2] != 'Cgst' && !is_numeric($value['Cgst'])) {
            $error[] = "Enter Correct Title : Cgst";
        }
        if (isset($csvTitleData[3]) && $csvTitleData[3] != 'Sgst' && !is_numeric($value['Sgst'])) {
            $error[] = "Enter Correct Title : Sgst";
        }
        if (isset($csvTitleData[4]) && $csvTitleData[4] != 'Igst' && !is_numeric($value['Igst'])) {
            $error[] = "Enter Correct Title : Igst";
        }
        if (isset($csvTitleData[5]) && $csvTitleData[5] != 'Price' && !is_numeric($value['Price'])) {
            $error[] = "Enter Correct Title : Price";
        }
        if (isset($csvTitleData[6]) && $csvTitleData[6] != 'Type') {
            $error[] = "Enter Correct Title : Type";
        }
        if (isset($csvTitleData[7]) && $csvTitleData[7] != 'Hsn Code' && !is_numeric($value['Hsn Code'])) {
            $error[] = "Enter Correct Title : Hsn Code";
        }
        if (isset($csvTitleData[8]) && $csvTitleData[8] != 'Tally Name') {
            $error[] = "Enter Correct Title : Tally Name";
        }


        // END - CHECK ALL THE TITLE IS RIGHT OR NOT
        // START CHECKING ALL THE DEPARTMENT NAME
        $selectCloumnFullname = array_search("Property Name", $csvTitleData);
        $departmentName = array_column($postData, $csvTitleData[$selectCloumnFullname]);
        $departmentNameExit = array();

        foreach ($postData as $key => $value) {

            $errorLine = $lineNo++; // THIS LINE WILL GIVE YOU CSV LINE NUMBER 
            // START CHECKING EVERY RECORDS WITH VALIDATION LIST

            if (!empty($value['Property Name']) && $value['Property Name'] != "") { // CHECK IF SAME DEPART NAME IS AVALIABLE INTO FILE
                $duplicateDepertment = array_search($value['Property Name'], $departmentName); // THIS WILL SEARCH USERNAME IN ALL COLUMN VALUE 
                if (!empty($departmentNameExit) && in_array($duplicateDepertment, $departmentName)) {
                    $lineNod = $duplicateDepertment + 2;
                    $error[] = "Same Property Name on line No " . '<b>' . $errorLine . '</b>' . " With Line No " . '<b>' . $lineNod . '</b>';
                }
                $departmentNameExit[] = $duplicateDepertment; //INSERT SEARCH COLUMN KEY IN ARRAY    
            }



            //THIS WILL BE CHECK IF CSV FIELED VALUE IN NOT EMPTY
            if (empty($value['Property Name']) && $value['Property Name'] == "") {
                $error[] = "Enter Property Name On Record No" . $errorLine;
            }

            if (empty($value['Property Validity End Date']) && $value['Property Validity End Date'] == "") {
                $error[] = "Enter Property Validity End Date On Record No" . $errorLine;
            }

            if (empty($value['Cgst']) && $value['Cgst'] == "") {
                $error[] = "Enter Cgst On Record No" . $errorLine;
            }

            if (empty($value['Sgst']) && $value['Sgst'] == "") {
                $error[] = "Enter Sgst On Record No" . $errorLine;
            }
            if (empty($value['Igst']) && $value['Igst'] == "") {
                $error[] = "Enter Igst On Record No" . $errorLine;
            }
            if (empty($value['Price']) && $value['Price'] == "") {
                $error[] = "Enter Price On Record No" . $errorLine;
            }
            if (empty($value['Type']) && $value['Type'] == "") {
                $error[] = "Enter Type On Record No" . $errorLine;
            }
            if (empty($value['Hsn Code']) && $value['Hsn Code'] == "") {
                $error[] = "Enter Hsn Code On Record No" . $errorLine;
            }
            if (empty($value['Tally Name']) && $value['Tally Name'] == "") {
                $error[] = "Enter Tally Name On Record No" . $errorLine;
            }
        }
    } else if ($requestData['uploadType'] == "user_master") {
        $newcount = 0;
        $lineNod = 0;
        $userName = array();

        $supervisorData = array();
        $selectCloumnFullname = array_search("Full Name", $csvTitleData); //SEARCH FULLNAME VALUE IN CSV AND PASS
        $fullNameData = array_column($postData, $csvTitleData[$selectCloumnFullname]); //SELECT FULLNAME COLUMN DATA

        $selectCloumn = array_search("Username", $csvTitleData); //SEARCH USERNAME VALUE IN CSV AND PASS
        $userName1 = array_column($postData, $csvTitleData[$selectCloumn]);

        foreach ($postData as $key => $value) {
            $clientId = $requestData['clientId'];
            $errorLine = $lineNo++;

            //ALL FIELD ARE MANDATORY VALIDATION
            if ((empty($value['FullName']) && $value['FullName'] = "" ) && (empty($value['Email Address']) && $value['Email Address'] = "" ) && (empty($value['Mobile No']) && $value['Mobile No'] = "" ) && (empty($value['Username']) && $value['Username'] = "" ) && (empty($value['Password']) && $value['Password'] = "" ) && (empty($value['Address']) && $value['Address'] = "" ) && (empty($value['Role']) && $value['Role'] = "" ) && (empty($value['Designation']) && $value['Designation'] = "" ) && (empty($value['Branch']) && $value['Branch'] = "" ) && (empty($value['User Level']) && $value['User Level'] = "" ) && (empty($value['Birth Date']) && $value['Birth Date'] = "" ) && (empty($value['Gender']) && $value['Gender'] = "")) {

                $error[] = "please fill all field On Record No " . $errorLine;
            }


            //FOR DUPLICATE DATA IN CUSTOMER NO IN CSV
            if (!empty($value['Username']) && $value['Username'] != "") {

                $duplicateCust = array_search($value['Username'], $userName1); //SEARCH WITH THE USER NAME
                if (!empty($userName) && in_array($duplicateCust, $userName)) {//IF MATCH FOUND THEN GIVE THE ERROR
                    $lineNod = $duplicateCust + 2;
                    $error[] = "Same Username on line No " . '<b>' . $errorLine . '</b>' . " With Line No " . '<b>' . $lineNod . '</b>';
                }

                $userName[] = $duplicateCust; //INSERT SEARCH COLUMN KEY IN ARRAY    
            }


//          EMAIL VALIDATION
            if (!filter_var($value['Email Address'], FILTER_VALIDATE_EMAIL) === false) {
                
            } else {
                $error[] = "Email is not valid On Record No" . $errorLine;
            }

            //BIRTH DAY VALIDATION
            $dateFormat = validateDate($value['Birth Date']);
            if ($dateFormat != '1') {
                $error[] = "please enter yyyy-mm-dd format in Birth Date On Line No : " . $errorLine;
            }

            //ANNI DATE VALIDATION
            $dateFormatAnni = validateDate($value['Anniversary Date']);
            if ($dateFormatAnni != '1' && !empty($value['Anniversary Date']) && $value['Anniversary Date'] != "") {
                $error[] = "please enter yyyy-mm-dd format in Anniversary Date On Line No : " . $errorLine;
            }


//          USER NAME UNIQUE VALIDATION 
            $sqlCheckUserName = "SELECT PK_USER_ID FROM " . USERMASTER . " WHERE  USERNAME = '" . $value['Username'] . "' AND DELETE_FLAG = 0";
            $resultCheckUserName = fetch_rec_query($sqlCheckUserName);
            if (count($resultCheckUserName) > 0) {
                $error[] = "Duplicate User Name found On Record No " . $errorLine;
            }


//          USER LEVEL VALIDATION
            $sqlCheckUserLevel = "SELECT PK_CLT_LKUP_ID, PRIORITY FROM " . CLIENTLOOKUP . " WHERE LOOK_VALUE = '" . $value['User Level'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'USERLEVEL' LIMIT 0,1";
            $resultCheckUserLevel = fetch_rec_query($sqlCheckUserLevel);

            if (count($resultCheckUserLevel) > 0) {
                $priority = ($resultCheckUserLevel[0]['PRIORITY'] + 1); //TAKE PRIORITY LEVEL
                //SUPERVISOR NAME  AND USER PRIORITY WISE VALIDATION 
                if ($priority != 11) {

                    $sqlCheckSuperName = "SELECT UM.PK_USER_ID,PRIORITY  FROM " . CLIENTLOOKUP . " CL INNER JOIN " . USERMASTER . " UM ON UM.USER_LEVEL = CL.PK_CLT_LKUP_ID WHERE UM.FK_CLIENT_ID = '" . $clientId . "' AND CL.FK_CLIENT_ID = '" . $clientId . "' AND UM.FULLNAME = '" . $value['Supervisor Name'] . "' AND CL.PRIORITY = '" . $priority . "' AND UM.DELETE_FLAG = '0' AND CL.DELETE_FLAG = '0' LIMIT 0,1";

                    $resCheckSuperName = fetch_rec_query($sqlCheckSuperName);
                    if (count($resCheckSuperName) == 0) {//IF SUPERVISOR IS NOT UPLOADED
                        if (!empty($value['Supervisor Name']) && $value['Supervisor Name'] != "") {

                            $supervisorData = array_search($value['Supervisor Name'], $fullNameData); //GET ALL FULLNAME IN CSV AND SEARCH WITH SUPER VISOR NAME
                            $supervisorName = $fullNameData[$supervisorData]; //TAKE SUPERVISOR NAME
                            $spervisorLevel = $postData[$supervisorData]['User Level']; //TAKE SUPERVISOR USER LEVEL
                            //CHECK WITH THE SUPERVISOR USER LEVEL 
                            $sqlCheckLevel = "SELECT PK_CLT_LKUP_ID, PRIORITY FROM " . CLIENTLOOKUP . " WHERE LOOK_VALUE = '" . $spervisorLevel . "' AND FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'USERLEVEL' LIMIT 0,1";
                            $resultCheckLevel = fetch_rec_query($sqlCheckLevel);

                            if (count($resultCheckLevel) > 0) {
                                if ($resultCheckLevel[0]['PRIORITY'] != $priority) {//IF PRIORITY OF SUPER VISOR IS NOT MATCH THEN GIVE ERROR
                                    $error[] = "please select correct supervisor name On Record No " . $errorLine;
                                }
                            } else {//NOT FOUND USER LEVEL THE GIVE THE ERROR
                                $error[] = "please select correct supervisor name On Record No " . $errorLine;
                            }
                        }
                    }
                }
            } else {
                $error[] = "Enter Correct User Level On Record No" . $errorLine;
            }

//          BRANCH NAME UNIQUE VALIDATION
            if (!empty($value['Branch Name']) && $value['Branch Name'] != "") {

                $explodeBranch = explode(',', $value['Branch Name']);
                if (!empty($explodeBranch) && $explodeBranch != "") {
                    foreach ($explodeBranch as $branchKey => $branchValue) {

                        $sqlCheckorg = "SELECT PK_ORG_ID FROM " . ORGMASTER . " WHERE ORGNAME LIKE '" . $branchValue . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0'LIMIT 0,1 ";
                        $resCheckOrg = fetch_rec_query($sqlCheckorg);

                        if (count($resCheckOrg) == 0) {
                            $error[] = "Enter Correct Branch Name On Record No" . $errorLine;
                        }
                    }
                }
            }

//          ROLE NAME VALIDATION
            $sqlCheckRole = "SELECT PK_ROLE_ID FROM " . ROLEMASTER . " WHERE ROLE_NAME = '" . $value['Role'] . "' AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0'";
            $resultCheckRole = fetch_rec_query($sqlCheckRole);
            if (count($resultCheckRole) <= 0) {
                $error[] = "Enter Correct Roll Name On Record No" . $errorLine;
            }

//          MOBILE NUMBER VALIDATION
            if (strlen($value['Mobile No']) != 10) {
                $error[] = "please enter 10 digit number On Record No" . $errorLine;
            }

//            print_r(strlen($value['Mobile No']));exit;
        }
    }


    if (count($error) > 0 && !empty($error)) { // IF ERROR THEN RETURN FAIL MSG WITH ERRORS ON RECORDS
        $result = array("status" => FAILMSG, "error" => $error);
    } else { // IF NO ERROR FOUND THEN RETURN SUCCESS
        $result = array("status" => SCS);
    }

    return $result;
}

function getDesignation($clientId, $orgId, $designation, $userId) {

    $sqlCheckUserDesig = "SELECT PK_CLT_LKUP_ID  FROM " . CLIENTLOOKUP . " WHERE `FK_CLIENT_ID` = '" . $clientId . "' AND LOOK_TYPE = (SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " CLT1 WHERE CLT1.LOOK_TYPE = 'DESIGNATION' AND CLT1.DELETE_FLAG = 0 AND `FK_CLIENT_ID` = '" . $clientId . "') AND DELETE_FLAG = '0' AND DISPLAY_VALUE = '" . $designation . "' LIMIT 0,1";

    $resultCheckUserDesig = fetch_rec_query($sqlCheckUserDesig);
    if (count($resultCheckUserDesig) > 0) {

        $result = array("status" => SCS, "data" => $resultCheckUserDesig[0]['PK_CLT_LKUP_ID']);
        http_response_code(200);
    } else {

        $sqldesignation = "SELECT PK_CLT_LKUP_ID FROM " . CLIENTLOOKUP . " WHERE FK_CLIENT_ID = '" . $clientId . "' AND LOOK_TYPE = 'DESIGNATION' AND DELETE_FLAG = '0' LIMIT 0,1";
        $resdesination = fetch_rec_query($sqldesignation);

        if (count($resdesination) > 0) {

            $insertRecord['LOOK_TYPE'] = $resdesination[0]['PK_CLT_LKUP_ID'];
            $insertRecord['DISPLAY_VALUE'] = $designation;
            $insertRecord['ISDEFAULTFLAG'] = "0";
            $insertRecord['FK_CLIENT_ID'] = $clientId;
            $insertRecord['CREATED_BY'] = $userId;
            $insertRecord['CREATED_BY_DATE'] = time();
            $insertRecord['DELETE_FLAG'] = 0;
            //                            print_r($insertRecord);exit;
            $insertDesignation = insert_rec(CLIENTLOOKUP, $insertRecord);
            if ($insertDesignation) {
                $result = array("status" => SCS, "data" => $insertDesignation['lastInsertedId']);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTQUERYFAIL);
                http_response_code(400);
            }
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    }
    return $result;
}
