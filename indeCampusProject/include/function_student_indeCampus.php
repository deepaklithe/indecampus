<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 02-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR Add Student Detail
function addEditStudent($postData) {

    // echo '<pre>' ;    print_r($postData) ; 
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    // $roomName = isset($postData['postData']['roomName']) ? addslashes(trim($postData['postData']['roomName'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $fkStudentId = isset($postData['postData']['fkStudentId']) ? addslashes(trim($postData['postData']['fkStudentId'])) : "";
    $firstName = isset($postData['postData']['firstName']) ? addslashes(trim($postData['postData']['firstName'])) : "";
    $lastName = isset($postData['postData']['lastName']) ? addslashes(trim($postData['postData']['lastName'])) : "";
    $userName = isset($postData['postData']['userName']) ? addslashes(trim($postData['postData']['userName'])) : "";
    $password = isset($postData['postData']['password']) ? password_hash(addslashes(md5(trim($postData['postData']['password']))), PASSWORD_DEFAULT) : "";
    $collegeName = isset($postData['postData']['collegeName']) ? addslashes(trim($postData['postData']['collegeName'])) : "";
    $mobileNo = isset($postData['postData']['mobileNo']) ? addslashes(trim($postData['postData']['mobileNo'])) : "";
    $email = isset($postData['postData']['email']) ? addslashes(trim($postData['postData']['email'])) : "";
    $gender = isset($postData['postData']['gender']) ? addslashes(trim($postData['postData']['gender'])) : "";
    $bloodGroup = isset($postData['postData']['bloodGroup']) ? addslashes(trim($postData['postData']['bloodGroup'])) : "";
    $course = isset($postData['postData']['course']) ? addslashes(trim($postData['postData']['course'])) : "";

    $countryId = isset($postData['postData']['countryId']) ? addslashes(trim($postData['postData']['countryId'])) : "";

    $stateId = isset($postData['postData']['stateId']) ? addslashes(trim($postData['postData']['stateId'])) : "";
    $cityId = isset($postData['postData']['cityId']) ? addslashes(trim($postData['postData']['cityId'])) : "";
    $birthDate = !empty($postData['postData']['birthDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['birthDate'])))) : "";

    $acdemicStartYear = isset($postData['postData']['acdemicStartYear']) ? addslashes(trim($postData['postData']['acdemicStartYear'])) : "";
    $acdemicEndYear = isset($postData['postData']['acdemicEndYear']) ? addslashes(trim($postData['postData']['acdemicEndYear'])) : "";
    $semester = isset($postData['postData']['semester']) ? addslashes(trim($postData['postData']['semester'])) : "";
    $guarantorName = isset($postData['postData']['guarantorName']) ? addslashes(trim($postData['postData']['guarantorName'])) : "";
    $guarantorAddressLine1 = isset($postData['postData']['guarantorAddressLine1']) ? addslashes(trim($postData['postData']['guarantorAddressLine1'])) : "";
    $guarantorAddressLine2 = isset($postData['postData']['guarantorAddressLine2']) ? addslashes(trim($postData['postData']['guarantorAddressLine2'])) : "";
    $guarantorAddressLine3 = isset($postData['postData']['guarantorAddressLine3']) ? addslashes(trim($postData['postData']['guarantorAddressLine3'])) : "";
    $guarantorMobile1 = isset($postData['postData']['guarantorMobile1']) ? addslashes(trim($postData['postData']['guarantorMobile1'])) : "";
    $guarantorMobile2 = isset($postData['postData']['guarantorMobile2']) ? addslashes(trim($postData['postData']['guarantorMobile2'])) : "";
    $guarantorEmail = isset($postData['postData']['guarantorEmail']) ? addslashes(trim($postData['postData']['guarantorEmail'])) : "";
    $guarantorCityId = isset($postData['postData']['guarantorCityId']) ? addslashes(trim($postData['postData']['guarantorCityId'])) : "";
    $guarantorStateId = isset($postData['postData']['guarantorStateId']) ? addslashes(trim($postData['postData']['guarantorStateId'])) : "";
    $guarantorPincode = isset($postData['postData']['guarantorPincode']) ? addslashes(trim($postData['postData']['guarantorPincode'])) : "";
    $emergencyContactNo = isset($postData['postData']['emergencyContactNo']) ? addslashes(trim($postData['postData']['emergencyContactNo'])) : "";
    $emergencyContactName = isset($postData['postData']['emergencyContactName']) ? addslashes(trim($postData['postData']['emergencyContactName'])) : "";
    $aadharNo = isset($postData['postData']['aadharNo']) ? addslashes(trim($postData['postData']['aadharNo'])) : "";
    $passportNo = isset($postData['postData']['passportNo']) ? addslashes(trim($postData['postData']['passportNo'])) : "";
    $nationality = isset($postData['postData']['nationality']) ? addslashes(trim($postData['postData']['nationality'])) : "";
    // $medicalCertificate = isset($postData['postData']['medicalCertificate']) ? addslashes(trim($postData['postData']['medicalCertificate'])) : "";
    $medicalCondition = isset($postData['postData']['medicalCondition']) ? addslashes(trim($postData['postData']['medicalCondition'])) : "";
    $typeOfRoomRequired = isset($postData['postData']['typeOfRoomRequired']) ? addslashes(trim($postData['postData']['typeOfRoomRequired'])) : "";
    $addressLine1 = isset($postData['postData']['addressLine1']) ? addslashes(trim($postData['postData']['addressLine1'])) : "";
    $addressLine2 = isset($postData['postData']['addressLine2']) ? addslashes(trim($postData['postData']['addressLine2'])) : "";
    $addressLine3 = isset($postData['postData']['addressLine3']) ? addslashes(trim($postData['postData']['addressLine3'])) : "";
    $pinCode = isset($postData['postData']['pinCode']) ? addslashes(trim($postData['postData']['pinCode'])) : "";

    $parentName = isset($postData['postData']['parentName']) ? addslashes(trim($postData['postData']['parentName'])) : "";
    $parentMobileNo = isset($postData['postData']['parentMobileNo']) ? addslashes(trim($postData['postData']['parentMobileNo'])) : "";
    $parentEmailId = isset($postData['postData']['parentEmailId']) ? addslashes(trim($postData['postData']['parentEmailId'])) : "";
    $parentUserName = isset($postData['postData']['parentUserName']) ? addslashes(trim($postData['postData']['parentUserName'])) : "";
    $parentPasword = isset($postData['postData']['parentPasword']) ? password_hash(addslashes(trim($postData['postData']['parentPasword'])), PASSWORD_DEFAULT) : "";

    $collegeAddressLine1 = isset($postData['postData']['collegeAddressLine1']) ? addslashes(trim($postData['postData']['collegeAddressLine1'])) : "";
    $collegeAddressLine2 = isset($postData['postData']['collegeAddressLine2']) ? addslashes(trim($postData['postData']['collegeAddressLine2'])) : "";
    $collegeAddressLine3 = isset($postData['postData']['collegeAddressLine3']) ? addslashes(trim($postData['postData']['collegeAddressLine3'])) : "";
    $collegeStateId = isset($postData['postData']['collegeStateId']) ? addslashes(trim($postData['postData']['collegeStateId'])) : "";
    $collegeCityId = isset($postData['postData']['collegeCityId']) ? addslashes(trim($postData['postData']['collegeCityId'])) : "";
    $collegePincode = isset($postData['postData']['collegePincode']) ? addslashes(trim($postData['postData']['collegePincode'])) : "";
    $parentAddreLine1 = isset($postData['postData']['parentAddreLine1']) ? addslashes(trim($postData['postData']['parentAddreLine1'])) : "";
    $parentAddreLine2 = isset($postData['postData']['parentAddreLine2']) ? addslashes(trim($postData['postData']['parentAddreLine2'])) : "";
    $parentAddreLine3 = isset($postData['postData']['parentAddreLine3']) ? addslashes(trim($postData['postData']['parentAddreLine3'])) : "";
    $parentCityId = isset($postData['postData']['parentCityId']) ? addslashes(trim($postData['postData']['parentCityId'])) : "";
    $parentStateId = isset($postData['postData']['parentStateId']) ? addslashes(trim($postData['postData']['parentStateId'])) : "";
    $parentCountryId = isset($postData['postData']['parentCountryId']) ? addslashes(trim($postData['postData']['parentCountryId'])) : "";
    $parentRelation = isset($postData['postData']['parentRelation']) ? addslashes(trim($postData['postData']['parentRelation'])) : "";
    $guarantorRelation = isset($postData['postData']['guarantorRelation']) ? addslashes(trim($postData['postData']['guarantorRelation'])) : "";
    $parentPincode = isset($postData['postData']['parentPincode']) ? addslashes(trim($postData['postData']['parentPincode'])) : "";
    $guarantorCountryId = isset($postData['postData']['guarantorCountryId']) ? addslashes(trim($postData['postData']['guarantorCountryId'])) : "";
    $parentId = isset($postData['postData']['parentId']) ? addslashes(trim($postData['postData']['parentId'])) : "";
    $guarantorId = isset($postData['postData']['guarantorId']) ? addslashes(trim($postData['postData']['guarantorId'])) : "";
    $emailArr = isset($postData['postData']['emailArr']) ? $postData['postData']['emailArr'] : array();
    $addressArr = isset($postData['postData']['addressArr']) ? $postData['postData']['addressArr'] : array();
    $phoneArr = isset($postData['postData']['phoneArr']) ? $postData['postData']['phoneArr'] : array();
    $errorArr = Array();

    /* CEHCK GURANTOR MOBILE NO1 VALIDATION START */
    if ($guarantorMobile1 != "") {
        if (strlen($guarantorMobile1) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $guarantorMobile1);
        }
    }
    /* CEHCK GURANTOR MOBILE NO1 VALIDATION END */

    /* CEHCK GURANTOR MOBILE NO2 VALIDATION START */
    if ($guarantorMobile2 != "") {
        if (strlen($guarantorMobile2) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $guarantorMobile2);
        }
    }
    /* CEHCK GURANTOR MOBILE NO2 VALIDATION END */
    /* CEHCK EMERGENCY MOBILE NO VALIDATION START */
    if ($emergencyContactNo != "") {
        if (strlen($emergencyContactNo) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $emergencyContactNo);
        }
    }
    /* CEHCK GURANTOR MOBILE NO2 VALIDATION END */

    /* CEHCK GURANTOR EMAIL VALIDATION START */
    if ($guarantorEmail != "") {
        if (!filter_var($guarantorEmail, FILTER_VALIDATE_EMAIL)) {
            array_push($errorArr, INVALIDEMAILADDRESS . " - " . $guarantorEmail);
        }
    }
    /* CEHCK GURANTOR EMAIL VALIDATION END */

    /* CEHCK EMAIL VALIDATION START */
    if (!empty($email)) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errorArr, INVALIDEMAILADDRESS . " - " . $email);
        }
    }
    /* CEHCK EMAIL VALIDATION END */

    /* CEHCK PARENT EMAIL VALIDATION START */
    if (!empty($parentEmailId)) {
        if (!filter_var($parentEmailId, FILTER_VALIDATE_EMAIL)) {
            array_push($errorArr, INVALIDEMAILADDRESS . " - " . $parentEmailId);
        }
    }
    /* CEHCK PARENT EMAIL VALIDATION END */
    /* CEHCK PARENT MOBILE NO VALIDATION START */
    if (!empty($parentMobileNo)) {
        if (strlen($parentMobileNo) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $parentMobileNo);
        } else {
            $whereStudId = "";
            if (!empty($parentId)) {
                $whereStudId = " AND PK_PARENT_ID !='" . $parentId . "'";
            }
            $sqlCheckMobileExist = "SELECT MOBILE_NO1 FROM " . PARENTSMASTER . " WHERE MOBILE_NO1='" . $parentMobileNo . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0' AND TYPE='Parent'"; // QUERY FOR MOBILE NO IS EXIST OR NOT.
            $resCheckMobileExist = fetch_rec_query($sqlCheckMobileExist); // RESULT FOR MOBILE NO IS EXIST OR NOT.
            if (count($resCheckMobileExist) > 0) {
                array_push($errorArr, ALREADYADDEDMOBILENO . " - " . $parentMobileNo);
            }
        }
    }

    /* CEHCK PARENT MOBILE NO VALIDATION END */
    /* CEHCK MOBILE NO VALIDATION START */
    if (!empty($mobileNo)) {
        if (strlen($mobileNo) < 10) {
            array_push($errorArr, INVALIDMOBILENO . " - " . $mobileNo);
        } else {
            $whereStudId = "";
            if (!empty($fkStudentId)) {
                $whereStudId = " AND PK_STUD_ID !='" . $fkStudentId . "'";
            }
            $sqlCheckMobileExist = "SELECT MOBILE_NO FROM " . STUDENTMASTER . " WHERE MOBILE_NO='" . $mobileNo . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'"; // QUERY FOR MOBILE NO IS EXIST OR NOT.
            $resCheckMobileExist = fetch_rec_query($sqlCheckMobileExist); // RESULT FOR MOBILE NO IS EXIST OR NOT.
            if (count($resCheckMobileExist) > 0) {
                array_push($errorArr, ALREADYADDEDMOBILENO . " - " . $mobileNo);
            }
        }
    }

    /* CEHCK MOBILE NO VALIDATION END */

    /* CEHCK STUDENT USERNAME VALIDATION START */
    $whereStudId = "";
    if (!empty($fkStudentId)) {
        $whereStudId = " AND PK_STUD_ID !='" . $fkStudentId . "'";
    }
    $sqlCheckUserNameExist = "SELECT USER_NAME FROM " . STUDENTMASTER . " WHERE USER_NAME='" . $userName . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'"; // QUERY FOR USER NAME IS EXIST OR NOT.
    $resCheckUserNameExist = fetch_rec_query($sqlCheckUserNameExist); // RESULT FOR USER NAME IS EXIST OR NOT.
    if (count($resCheckUserNameExist) > 0) {
        array_push($errorArr, ALREADYADDEDUSERNAME . " - " . $userName);
    }
    /* CEHCK STUDENT USERNAME VALIDATION END */


    /* CEHCK STUDENT EMAIL VALIDATION START */
    /* $whereStudId = "";
      if (!empty($fkStudentId)) {
      $whereStudId = " AND PK_STUD_ID !='" . $fkStudentId . "'";
      }
      $sqlCheckEmailExist = "SELECT USER_NAME FROM " . STUDENTMASTER . " WHERE EMAIL='" . $email . "' AND FK_CLIENT_ID ='" . $clientId . "' " . $whereStudId . " AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'"; // QUERY FOR EMAIL IS EXIST OR NOT.
      $resCheckEmailExist = fetch_rec_query($sqlCheckEmailExist); // RESULT FOR EMAIL NAME IS EXIST OR NOT.
      if (count($resCheckEmailExist) > 0) {
      array_push($errorArr, ALREADYADDEDEMAIL . " - " . $email);
      } */
    /* CEHCK STUDENT EMAIL VALIDATION END */

    if (empty($errorArr)) {

        if (!empty($clientId) && !empty($orgId) && !empty($firstName) && !empty($lastName) && !empty($userName) && !empty($collegeName) && !empty($gender) && !empty($course) && !empty($guarantorName) && !empty($guarantorAddressLine1) && !empty($guarantorMobile1) && !empty($aadharNo) && !empty($countryId) && !empty($stateId) && !empty($cityId)) { // CHECK REQURIED FIELD CONDITION
            // !empty($typeOfRoomRequired) &&
            // INSERT STUDENT ARRAY.
            $insertStudArray = Array(); //ROOM ARRAY CREATE
            $insertStudArray['FK_CLIENT_ID'] = $clientId;
            $insertStudArray['FK_ORG_ID'] = $orgId;
            $insertStudArray['FIRST_NAME'] = $firstName;
            $insertStudArray['LAST_NAME'] = $lastName;
            $insertStudArray['USER_NAME'] = $userName;
            $insertStudArray['PASSWORD'] = $password;
            $insertStudArray['SECRET_IDENTIFICATION_CODE'] = md5($userName . " " . $password);
            $insertStudArray['MEMBERSHIP_NUMBER'] = generateId(STUDENTMASTER, "STU", $clientId);
            //$insertStudArray['FK_EPOS_CUST_ID'] = '1'; //ADD BY DEVIKA.20.8.2018
            $insertStudArray['MOBILE_NO'] = $mobileNo;
            $insertStudArray['EMAIL'] = $email;
            $insertStudArray['ADDRESS_LINE1'] = $addressLine1;
            $insertStudArray['ADDRESS_LINE2'] = $addressLine2;
            $insertStudArray['ADDRESS_LINE3'] = $addressLine3;
            $insertStudArray['PINCODE'] = $pinCode;
            $insertStudArray['COLLEGE_NAME'] = $collegeName;
            $insertStudArray['COLLEGE_ADDRESS_LINE1'] = $collegeAddressLine1;
            $insertStudArray['COLLEGE_ADDRESS_LINE2'] = $collegeAddressLine2;
            $insertStudArray['COLLEGE_ADDRESS_LINE3'] = $collegeAddressLine3;
            $insertStudArray['COLLEGE_COUNTRY_ID'] = $countryId;
            $insertStudArray['COLLEGE_STATE_ID'] = $collegeStateId;
            $insertStudArray['COLLEGE_CITY_ID'] = $collegeCityId;
            $insertStudArray['COLLEGE_PINCODE'] = $collegePincode;
            $insertStudArray['GENDER'] = $gender;
            $insertStudArray['BLOOD_GROUP'] = $bloodGroup;
            $insertStudArray['COURSE'] = $course;
            $insertStudArray['ACDEMIC_START_DATE'] = $acdemicStartYear;
            $insertStudArray['ACDEMIC_END_DATE'] = $acdemicEndYear;
            $insertStudArray['SEMESTER'] = $semester;
            $insertStudArray['FK_COUNTRY_ID'] = $countryId;
            $insertStudArray['FK_CITY_ID'] = $cityId;
            $insertStudArray['FK_STATE_ID'] = $stateId;
            $insertStudArray['EMERGENCY_CONTACT_PERSON_NO'] = $emergencyContactNo;
            $insertStudArray['EMERGENCY_CONTACT_PERSON_NAME'] = $emergencyContactName;
            $insertStudArray['BIRTHDATE'] = $birthDate;
            $insertStudArray['AADHAR_NO'] = $aadharNo;
            $insertStudArray['PASSPORT_NO'] = $passportNo;
            $insertStudArray['NATIONALITY'] = $nationality;
            $insertStudArray['MEDICAL_CONDITION'] = $medicalCondition;
            $insertStudArray['TYPE_OF_ROOM_REQUIRED'] = $typeOfRoomRequired;
            $insertStudArray['IS_ACTIVE'] = '1'; // WHERE 1 is ACTIVE.
            // echo "<pre>"; print_r($insertStudArray); exit;
            $sqlclientData = "SELECT EPOS_CLIENT_ID,EPOS_URL FROM " . CLIENTMASTER . " WHERE PK_CLIENT_ID ='" . $clientId . "'";
            $resClientData = fetch_rec_query($sqlclientData);
            $eposClientId = $resClientData[0]['EPOS_CLIENT_ID'];
            $eposMasterUrl = $resClientData[0]['EPOS_URL'];

            if ($fkStudentId != "" && $fkStudentId > 0) {

                $insertStudArray['CHANGED_BY'] = $userId;
                $insertStudArray['CHANGED_BY_DATE'] = time();
                // UNSET FIELD AT EDIT TIME.
                unset($insertStudArray['USER_NAME']);
                unset($insertStudArray['PASSWORD']);
                unset($insertStudArray['MEMBERSHIP_NUMBER']);
                unset($insertStudArray['PARENT_USERNAME']);
                unset($insertStudArray['PARENT_PASSWORD']);
                unset($insertStudArray['PARENT_MEMBERSHIP_NUMBER']);

                $whereStudUpdate = "PK_STUD_ID =" . $fkStudentId;
                $updateStudData = update_rec(STUDENTMASTER, $insertStudArray, $whereStudUpdate); // UPDATE IN STUDENT MASTER TABLE.
                $studentId = $fkStudentId;

                $newArr = array();
                $newArr['requestCase'] = "updateCustomersDetail";
                $newArr['PK_CUSTOMER_ID'] = $fkStudentId;
                // $newArr['FK_CLIENT_ID'] = '256'; //HARDCODED
                $newArr['clientId'] = $eposClientId; //CLIENT ID GET FROM CLIENT MASTER TABLE.
                $newArr['MOBILE_PHONE'] = $mobileNo;
                $newArr['CUSTOMER_FIRST_NAME'] = $firstName;
                $newArr['CUSTOMER_LAST_NAME'] = $lastName;
                if ($addressLine1 != "") {
                    $newArr['FLAT_HOUSE_NO'] = $addressLine1;
                    $newArr['STREET_SOCIETY'] = $addressLine2 . $addressLine3;
                }
                $newArr['MAX_CREDIT_LIMIT'] = '0';
                $newArr['FK_REGION_ID'] = '';
                $newArr['FK_AREA_ID'] = '';

                $finalRequest['postData'] = json_encode($newArr);
                //print_r($finalRequest);exit;
                $data = curlCall($eposMasterUrl, $finalRequest);
            } else {
                $insertStudArray['CREATED_BY'] = $userId;
                $insertStudArray['CREATED_BY_DATE'] = time();

                $insertStudent = insert_rec(STUDENTMASTER, $insertStudArray); // INSERT RECORD IN STUDENT_MASTER TABLE.
                $studentId = isset($insertStudent['lastInsertedId']) ? $insertStudent['lastInsertedId'] : "";

                $newArr = array();
                $newArr['requestCase'] = "registerCustomer";
                //$newArr['clientId'] = '256'; //HARDCODED
                $newArr['clientId'] = $eposClientId; //CLIENT ID GET FROM CLIENT MASTER TABLE.
                $newArr['MOBILE_PHONE'] = $mobileNo;
                $newArr['CUSTOMER_FIRST_NAME'] = $firstName;
                $newArr['CUSTOMER_LAST_NAME'] = $lastName;
                $newArr['FLAT_HOUSE_NO'] = $addressLine1;
                $newArr['STREET_SOCIETY'] = $addressLine2 . $addressLine3;
                $newArr['MAX_CREDIT_LIMIT'] = '0';
                $newArr['FK_REGION_ID'] = "1"; //HARDCODED
                $newArr['FK_AREA_ID'] = "1"; //HARDCODED


                $finalRequest['postData'] = json_encode($newArr);
                //  print_r($finalRequest);exit;
                $data = curlCall($eposMasterUrl, $finalRequest);


                // UPDATE DATA INTO STUDENT MASTER TABLE BY DEEPAK PATIL ON 04-SEP-2018

                $updateStudent = array();
                $updateStudent['FK_EPOS_CUST_ID'] = ($data['status'] == SCS) ? $data['data'][0]['PK_CUSTOMER_ID'] : "";
                $whereStudentUpdate = "PK_STUD_ID =" . $studentId . " AND FK_CLIENT_ID = '" . $clientId . "' AND DELETE_FLAG = '0' ";
                $updateParentData = update_rec(STUDENTMASTER, $updateStudent, $whereStudentUpdate); // UPDATE IN PARENT_MASTER TABLE
                //print_r($data);
            }
            if (!empty($studentId) && $studentId > 0) {

                $insertParentArray = Array();
                $insertParentArray['FK_CLIENT_ID'] = $clientId;
                $insertParentArray['FK_ORG_ID'] = $orgId;
                $insertParentArray['FK_STUD_ID'] = $studentId;
                $insertParentArray['PARENT_NAME'] = $parentName;
                $insertParentArray['USER_NAME'] = $parentUserName;
                $insertParentArray['PASSWORD'] = $parentPasword;
                $insertParentArray['MEMBERSHIP_NUMBER'] = generateId(PARENTMASTER, "PAR", $clientId);
                $insertParentArray['MOBILE_NO1'] = $parentMobileNo;
                $insertParentArray['EMAIL'] = $parentEmailId;
                $insertParentArray['ADDRESS_LINE1'] = $parentAddreLine1;
                $insertParentArray['ADDRESS_LINE2'] = $parentAddreLine2;
                $insertParentArray['ADDRESS_LINE3'] = $parentAddreLine3;
                $insertParentArray['FK_CITY_ID'] = $parentCityId;
                $insertParentArray['FK_STATE_ID'] = $parentStateId;
                $insertParentArray['FK_COUNTRY_ID'] = $parentCountryId;
                $insertParentArray['TYPE'] = 'Parent';
                $insertParentArray['RELATION'] = $parentRelation;
                $insertParentArray['PINCODE'] = $parentPincode;
                if (!empty($parentId)) {
                    $insertParentArray['CHANGED_BY'] = $userId;
                    $insertParentArray['CHANGED_BY_DATE'] = time();
                    $whereParentUpdate = "PK_PARENT_ID =" . $parentId;
                    $updateParentData = update_rec(PARENTSMASTER, $insertParentArray, $whereParentUpdate); // UPDATE IN PARENT_MASTER TABLE
                } else {
                    $insertParentArray['CREATED_BY'] = $userId;
                    $insertParentArray['CREATED_BY_DATE'] = time();
                    $insertParent = insert_rec(PARENTSMASTER, $insertParentArray); // INSERT RECORD IN PARENT_MASTER TABLE.
                }


                $insertGurantorArray = Array();
                $insertGurantorArray['FK_STUD_ID'] = $studentId;
                $insertGurantorArray['FK_CLIENT_ID'] = $clientId;
                $insertGurantorArray['FK_ORG_ID'] = $orgId;
                $insertGurantorArray['PARENT_NAME'] = $guarantorName;
                $insertGurantorArray['MOBILE_NO1'] = $guarantorMobile1;
                $insertGurantorArray['MOBILE_NO2'] = $guarantorMobile2;
                $insertGurantorArray['EMAIL'] = $guarantorEmail;
                $insertGurantorArray['ADDRESS_LINE1'] = $guarantorAddressLine1;
                $insertGurantorArray['ADDRESS_LINE2'] = $guarantorAddressLine2;
                $insertGurantorArray['ADDRESS_LINE3'] = $guarantorAddressLine3;
                $insertGurantorArray['FK_CITY_ID'] = $guarantorCityId;
                $insertGurantorArray['FK_STATE_ID'] = $guarantorStateId;
                $insertGurantorArray['FK_COUNTRY_ID'] = $guarantorCountryId;
                $insertGurantorArray['TYPE'] = 'Guarantor';
                $insertGurantorArray['RELATION'] = $guarantorRelation;
                $insertGurantorArray['PINCODE'] = $guarantorPincode;

                //echo '<pre>' ; print_r($insertGurantorArray) ; die;
                if (!empty($guarantorId)) {
                    $insertGurantorArray['CHANGED_BY'] = $userId;
                    $insertGurantorArray['CHANGED_BY_DATE'] = time();
                    $whereParentUpdate = "PK_PARENT_ID =" . $guarantorId;
                    $updateParentData = update_rec(PARENTSMASTER, $insertGurantorArray, $whereParentUpdate); // UPDATE IN PARENT_MASTER TABLE
                } else {
                    $insertGurantorArray['CREATED_BY'] = $userId;
                    $insertGurantorArray['CREATED_BY_DATE'] = time();
                    $insertParent = insert_rec(PARENTSMASTER, $insertGurantorArray); // INSERT RECORD IN PARENT_MASTER TABLE.
                }

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['studentId'] = $studentId;
                $studentData = getStudentData($requestData); // GET ALL STUDENT DATA.
                $studentAllData = ($studentData['status'] == SCS) ? $studentData['data'] : $studentData['status'];

                $result = array("status" => SCS, "data" => $studentAllData);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTFAIL . " " . STUDENTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    } else {
        $result = array("status" => ERROR, "data" => $errorArr);
        http_response_code(400);
    }

    return $result;
}

function deleteStudentMultiData($studentId = 0, $userId = 0) {
    $updateArr = Array();
    $updateArr['DELETE_FLAG'] = 1;
    $updateArr['CHANGED_BY'] = $userId;
    $updateArr['CHANGED_BY_DATE'] = time();

    $whereStudentCond = "PK_STUD_ID =" . $studentId;
    $updateStudData = update_rec(STUDENTMASTER, $updateArr, $whereStudentCond);
}

function getStudentData($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $membershipId = isset($postData['postData']['membershipId']) ? addslashes(trim($postData['postData']['membershipId'])) : "";
    $studentName = isset($postData['postData']['studentName']) ? addslashes(trim($postData['postData']['studentName'])) : "";
    $enrollmentDate = !empty($postData['postData']['enrollmentDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['enrollmentDate'])))) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";
    /* Student id condition start */
    $whereStudentCond = "";
    if (!empty($studentId)) {
        $whereStudentCond = " AND SM.PK_STUD_ID ='" . $studentId . "'";
    }
    /* Student id condition End */

    /* Student Member ship Number condition start */
    $whereMembershipCond = "";
    if (!empty($membershipId)) {
        $whereMembershipCond = " AND SM.MEMBERSHIP_NUMBER ='" . $membershipId . "'";
    }
    /* Student Member ship Number condition End */

    /* Student status condition start */
    $whereStudentstatusCond = "";
    if ($status != "") {
        $whereStudentstatusCond = " AND SM.IS_ACTIVE ='" . $status . "'";
    }
    /* Student status condition End */

    /* Student Name condition start */
    $whereStudentNameCond = "";
    if (!empty($studentName)) {
        $whereStudentNameCond = " HAVING  STUD_NAME='" . $studentName . "'";
    }
    /* Student Name condition End */

    /* Student Enrollment Date condition start */
    $whereStudentEnrollmentDateCond = "";
    if (!empty($enrollmentDate)) {
        $whereStudentEnrollmentDateCond = " AND (DATE_FORMAT(FROM_UNIXTIME(SM.CREATED_BY_DATE), '%Y-%m-%d'))='" . $enrollmentDate . "'";
    }
    /* Student Enrollment Date condition End */

    // ADDED BY KAUSHA SHAH ON 17-08-2018 START
    $countryArr = $stateArr = $cityArr = $clientLookupArr = $itemLookupArr = $lookupArr = array();
    $countrySql = fetch_rec_query("SELECT PK_COUNTRY_ID,COUNTRY_NAME FROM " . COUNTRY . "");
    if (count($countrySql) > 0) {
        foreach ($countrySql as $countrySqlData) {
            $countryArr[$countrySqlData['PK_COUNTRY_ID']] = $countrySqlData['COUNTRY_NAME'];
        }
    }
    $stateSql = fetch_rec_query("SELECT PK_STATE_ID,STATE_NAME FROM " . STATE . "");
    if (count($stateSql) > 0) {
        foreach ($stateSql as $stateSqlData) {
            $stateArr[$stateSqlData['PK_STATE_ID']] = $stateSqlData['STATE_NAME'];
        }
    }
    $citySql = fetch_rec_query("SELECT PK_CITY_ID,CITY_NAME FROM " . CITY . "");
    if (count($citySql) > 0) {
        foreach ($citySql as $citySqlData) {
            $cityArr[$citySqlData['PK_CITY_ID']] = $citySqlData['CITY_NAME'];
        }
    }
    $cLookupSql = fetch_rec_query("SELECT PK_CLT_LKUP_ID,DISPLAY_VALUE FROM " . CLIENTLOOKUP . "");
    if (count($cLookupSql) > 0) {
        foreach ($cLookupSql as $cLookupSqlData) {
            $clientLookupArr[$cLookupSqlData['PK_CLT_LKUP_ID']] = $cLookupSqlData['DISPLAY_VALUE'];
        }
    }

    $itemMasterSql = fetch_rec_query("SELECT PK_ITEM_ID,ITEM_NAME FROM " . ITEMMASTER . "");
    if (count($itemMasterSql) > 0) {
        foreach ($itemMasterSql as $itemMasterSqlData) {
            $itemLookupArr[$itemMasterSqlData['PK_ITEM_ID']] = $itemMasterSqlData['ITEM_NAME'];
        }
    }
    $lookupDataSql = fetch_rec_query("SELECT PK_LOOKUP_ID,VALUE FROM " . LOOKUPMASTER . "");
    if (count($lookupDataSql) > 0) {
        foreach ($lookupDataSql as $cLookupSqlData) {
            $lookupArr[$cLookupSqlData['PK_LOOKUP_ID']] = $cLookupSqlData['VALUE'];
        }
    }
    // ADDED BY KAUSHA SHAH ON 17-08-2018 END
    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND SM.CREATED_BY IN (" . $userIds . ")";
    }


//add by Saitsh Karena for master user Permission all data END


    $sqlStudentData = "SELECT SM.PK_STUD_ID,SM.FK_CLIENT_ID,SM.USER_NAME,SM.MEMBERSHIP_NUMBER,SM.FK_ORG_ID,SM.FIRST_NAME,SM.LAST_NAME,SM.FK_COUNTRY_ID,SM.FK_STATE_ID,SM.FK_CITY_ID,SM.MOBILE_NO,SM.EMAIL,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.COLLEGE_NAME,SM.COLLEGE_ADDRESS_LINE1,SM.COLLEGE_ADDRESS_LINE2,SM.COLLEGE_COUNTRY_ID,SM.COLLEGE_STATE_ID,SM.COLLEGE_CITY_ID,CI.STATE_NAME AS COLLEGE_STATE_NAME,CI.CITY_NAME AS COLLEGE_CITY_NAME,SM.COLLEGE_ADDRESS_LINE3,SM.COLLEGE_PINCODE,SM.GENDER,SM.BLOOD_GROUP,SM.COURSE,SM.ACDEMIC_START_DATE,SM.ACDEMIC_END_DATE,SM.SEMESTER,SM.PROFILE_IMAGE,GT.STATE_NAME AS GUARANTOR_STATE_NAME,GT.CITY_NAME AS GUARANTOR_CITY_NAME,SM.MEDICAL_CONDITION,SM.AADHAR_NO,SM.TYPE_OF_ROOM_REQUIRED,SM.EMERGENCY_CONTACT_PERSON_NO,SM.BIRTHDATE,CO.COUNTRY_NAME,ST.STATE_NAME,SM.PASSPORT_NO,CT.CITY_NAME,CL.DISPLAY_VALUE AS ITEM_NAME,SM.EMERGENCY_CONTACT_PERSON_NAME,SM.PASSPORT_NO,DATE_FORMAT(FROM_UNIXTIME(SM.CREATED_BY_DATE), '%Y-%m-%d') AS ENROLLMENT_DATE,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,PM.PK_PARENT_ID AS parentId,PM.PARENT_NAME AS parentName,PM.USER_NAME AS ParentUserName,PM.MEMBERSHIP_NUMBER AS parentMembershipNumber,PM.MOBILE_NO1 AS parentMobileNo,PM.EMAIL AS parentEmail,PM.ADDRESS_LINE1 AS parentAddress1,PM.ADDRESS_LINE2 AS parentAddress2,PM.ADDRESS_LINE3 AS parentAddress3,PM.FK_CITY_ID AS parentCityId,PM.FK_STATE_ID AS parentStateId,PM.FK_COUNTRY_ID AS parentCountryId,PM.TYPE AS parentType,PM.RELATION AS parentRelation,GM.PK_PARENT_ID AS guarantorId,GM.PARENT_NAME AS guarantorName,GM.MOBILE_NO1 AS guarantorMobileNo1,GM.MOBILE_NO2 AS guarantorMobileNo2,GM.EMAIL AS guarantorEmail,GM.ADDRESS_LINE1 AS guarantorAddress1,GM.ADDRESS_LINE2 AS guarantorAddress2,GM.ADDRESS_LINE3 AS guarantorAddress3,GM.FK_CITY_ID AS guarantorCityId,GM.FK_STATE_ID AS guarantorStateId,GM.FK_COUNTRY_ID AS guarantorCountryId,GM.TYPE AS guarantorType,GM.RELATION AS guarantorRelation,PM.PINCODE AS parentPincode,GM.PINCODE AS guarantorPincode,SM.NATIONALITY,SM.RECEIPT_AMT,SM.UNBILLED_AMT,SM.BILL_AMT,SM.CLOSINGBAL_AMT,SM.CLOSINGBAL_TYPE,SM.IS_ACTIVE,SM.FK_EPOS_CUST_ID FROM " . STUDENTMASTER . " SM LEFT JOIN " . PARENTSMASTER . " PM ON PM.FK_STUD_ID =SM.PK_STUD_ID AND PM.TYPE ='Parent' LEFT JOIN " . PARENTSMASTER . " GM ON GM.FK_STUD_ID =SM.PK_STUD_ID AND GM.TYPE ='Guarantor' LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . STATE . " ST ON ST.PK_STATE_ID=SM.FK_STATE_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  LEFT JOIN " . CITY . " GT ON GT.PK_CITY_ID =GM.FK_CITY_ID  LEFT JOIN " . CITY . " CI ON CI.PK_CITY_ID =SM.COLLEGE_CITY_ID LEFT JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID=SM.TYPE_OF_ROOM_REQUIRED  WHERE SM.DELETE_FLAG =0 " . $whereStudentCond . $whereMembershipCond . $whereStudentstatusCond . $whereStudentEnrollmentDateCond . " AND SM.FK_CLIENT_ID ='" . $clientId . "' AND SM.FK_ORG_ID ='" . $orgId . "'  " . $whereStudentNameCond . $whereUserCond." ORDER BY SM.PK_STUD_ID DESC"; // QUERY FOR STUDENT DATA.
    // INNER JOIN " . ALLOTMASTER . " AT ON SM.PK_STUD_ID=AT.FK_STUDENT_ID AND AT.DELETE_FLAG ='0' AND DATE(now()) >= AT.TENANCY_START_DATE 
    // 
    //AT.ROOM_NAME,AT.TENANCY_START_DATE,AT.TENANCY_END_DATE,AT.TENANCY_DEPOSIT,AT.TENANCY_RENT,AT.FOOD_PREFERENCE,AT.BED_NO,AT.FK_ROOM_ID,AT.FACILITY_ALLOCATION,AT.ASSET_ISSUED

    $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR STUDENT DATA.
    // print_r($resStudentData);
    // exit;
    if (count($resStudentData) > 0) {
        $studentArr = Array();
        foreach ($resStudentData as $keyStudent => $valueStudent) {

            $studentArr[$keyStudent]['studId'] = $valueStudent['PK_STUD_ID'];
            $studentArr[$keyStudent]['clientId'] = $valueStudent['FK_CLIENT_ID'];
            $studentArr[$keyStudent]['orgId'] = $valueStudent['FK_ORG_ID'];
            $studentArr[$keyStudent]['userName'] = $valueStudent['USER_NAME'];
            $studentArr[$keyStudent]['firstName'] = $valueStudent['FIRST_NAME'];
            $studentArr[$keyStudent]['lastName'] = $valueStudent['LAST_NAME'];
            $studentArr[$keyStudent]['memberShipNumber'] = $valueStudent['MEMBERSHIP_NUMBER'];
            $studentArr[$keyStudent]['countryId'] = $valueStudent['FK_COUNTRY_ID'];
            $studentArr[$keyStudent]['countryName'] = $valueStudent['COUNTRY_NAME'];
            $studentArr[$keyStudent]['stateId'] = $valueStudent['FK_STATE_ID'];
            $studentArr[$keyStudent]['stateName'] = $valueStudent['STATE_NAME'];
            $studentArr[$keyStudent]['cityId'] = $valueStudent['FK_CITY_ID'];
            $studentArr[$keyStudent]['cityName'] = $valueStudent['CITY_NAME'];
            $studentArr[$keyStudent]['mobileNo'] = $valueStudent['MOBILE_NO'];
            $studentArr[$keyStudent]['email'] = $valueStudent['EMAIL'];

            $studentArr[$keyStudent]['parentId'] = $valueStudent['parentId'];
            $studentArr[$keyStudent]['parentName'] = $valueStudent['parentName'];
            $studentArr[$keyStudent]['parentEmailId'] = $valueStudent['parentEmail'];
            $studentArr[$keyStudent]['parentMobileNo'] = $valueStudent['parentMobileNo'];
            $studentArr[$keyStudent]['parentUserName'] = $valueStudent['ParentUserName'];
            $studentArr[$keyStudent]['parentMembershipNumber'] = $valueStudent['parentMembershipNumber'];
            $studentArr[$keyStudent]['parentAddressLine1'] = $valueStudent['parentAddress1'];
            $studentArr[$keyStudent]['parentAddressLine2'] = $valueStudent['parentAddress2'];
            $studentArr[$keyStudent]['parentAddressLine3'] = $valueStudent['parentAddress3'];
            $studentArr[$keyStudent]['parentCityId'] = $valueStudent['parentCityId'];
            $studentArr[$keyStudent]['parentStateId'] = $valueStudent['parentStateId'];
            $studentArr[$keyStudent]['parentCountryId'] = $valueStudent['parentCountryId'];
            $studentArr[$keyStudent]['parentRelation'] = $valueStudent['parentRelation'];
            $studentArr[$keyStudent]['parentPincode'] = $valueStudent['parentPincode'];

            $studentArr[$keyStudent]['addressLine1'] = $valueStudent['ADDRESS_LINE1'];
            $studentArr[$keyStudent]['addressLine2'] = $valueStudent['ADDRESS_LINE2'];
            $studentArr[$keyStudent]['addressLine3'] = $valueStudent['ADDRESS_LINE3'];
            $studentArr[$keyStudent]['pincode'] = $valueStudent['PINCODE'];
            $studentArr[$keyStudent]['collegeName'] = $valueStudent['COLLEGE_NAME'];
            $studentArr[$keyStudent]['collegeAddressLine1'] = $valueStudent['COLLEGE_ADDRESS_LINE1'];
            $studentArr[$keyStudent]['collegeAddressLine2'] = $valueStudent['COLLEGE_ADDRESS_LINE2'];
            $studentArr[$keyStudent]['collegeAddressLine3'] = $valueStudent['COLLEGE_ADDRESS_LINE3'];
            $studentArr[$keyStudent]['collegeCountryId'] = $valueStudent['COLLEGE_COUNTRY_ID'];
            $studentArr[$keyStudent]['collegeStateId'] = $valueStudent['COLLEGE_STATE_ID'];
            $studentArr[$keyStudent]['collegeCityId'] = $valueStudent['COLLEGE_CITY_ID'];
            $studentArr[$keyStudent]['collegeCountryName'] = $valueStudent['COUNTRY_NAME'];
            $studentArr[$keyStudent]['collegeStateName'] = $valueStudent['COLLEGE_STATE_NAME'];
            $studentArr[$keyStudent]['collegeCityName'] = $valueStudent['COLLEGE_CITY_NAME'];
            $studentArr[$keyStudent]['collegePinCode'] = $valueStudent['COLLEGE_PINCODE'];


            $studentArr[$keyStudent]['gender'] = $valueStudent['GENDER'];
            $studentArr[$keyStudent]['bloodGroup'] = $valueStudent['BLOOD_GROUP'];
            $studentArr[$keyStudent]['course'] = $valueStudent['COURSE'];
            $studentArr[$keyStudent]['acdemicStartYear'] = $valueStudent['ACDEMIC_START_DATE'];
            $studentArr[$keyStudent]['acdemicEndYear'] = $valueStudent['ACDEMIC_END_DATE'];
            $studentArr[$keyStudent]['semester'] = $valueStudent['SEMESTER'];
            $studentArr[$keyStudent]['guarantorName'] = $valueStudent['guarantorName'];

            $studentArr[$keyStudent]['guarantorId'] = $valueStudent['guarantorId'];
            $studentArr[$keyStudent]['guarantorName'] = $valueStudent['guarantorName'];
            $studentArr[$keyStudent]['guarantorAddress1'] = $valueStudent['guarantorAddress1'];
            $studentArr[$keyStudent]['guarantorAddress2'] = $valueStudent['guarantorAddress2'];
            $studentArr[$keyStudent]['guarantorAddress3'] = $valueStudent['guarantorAddress3'];
            $studentArr[$keyStudent]['guarantorPinCode'] = $valueStudent['guarantorPincode'];
            $studentArr[$keyStudent]['guarantorEmail'] = $valueStudent['guarantorEmail'];
            $studentArr[$keyStudent]['guarantorMobileNo1'] = $valueStudent['guarantorMobileNo1'];
            $studentArr[$keyStudent]['guarantorMobileNo2'] = $valueStudent['guarantorMobileNo2'];
            $studentArr[$keyStudent]['guarantorCityId'] = $valueStudent['guarantorCityId'];
            $studentArr[$keyStudent]['guarantorStateId'] = $valueStudent['guarantorStateId'];
            $studentArr[$keyStudent]['guarantorCountryId'] = $valueStudent['guarantorCountryId'];
            $studentArr[$keyStudent]['guarantorCityName'] = $valueStudent['GUARANTOR_CITY_NAME'];
            $studentArr[$keyStudent]['guarantorStateName'] = $valueStudent['GUARANTOR_STATE_NAME'];
            $studentArr[$keyStudent]['guarantorCountryName'] = $valueStudent['COUNTRY_NAME'];
            $studentArr[$keyStudent]['guarantorType'] = $valueStudent['guarantorType'];
            $studentArr[$keyStudent]['guarantorRelation'] = $valueStudent['guarantorRelation'];

            $studentArr[$keyStudent]['guarantorPincode'] = $valueStudent['guarantorPincode'];
            $studentArr[$keyStudent]['eposCustId'] = $valueStudent['FK_EPOS_CUST_ID'];
            // ADDED BY KAVITA PATEL ON 20-08-2018 START
            $medicalCondition = (isset($clientLookupArr[$valueStudent['MEDICAL_CONDITION']]) && $clientLookupArr[$valueStudent['MEDICAL_CONDITION']] != '') ? trim($clientLookupArr[$valueStudent['MEDICAL_CONDITION']]) : "";
            $studentArr[$keyStudent]['medicalConditionName'] = $medicalCondition;
            // ADDED BY KAVITA PATEL ON 20-08-2018 END
            $studentArr[$keyStudent]['medicalCondition'] = $valueStudent['MEDICAL_CONDITION'];
            $studentArr[$keyStudent]['aadharNo'] = $valueStudent['AADHAR_NO'];
            $studentArr[$keyStudent]['passportNo'] = $valueStudent['PASSPORT_NO'];
            $studentArr[$keyStudent]['typeOfRoomRequired'] = $valueStudent['TYPE_OF_ROOM_REQUIRED'];
            $studentArr[$keyStudent]['emeregencyContactPersonNo'] = $valueStudent['EMERGENCY_CONTACT_PERSON_NO'];
            $studentArr[$keyStudent]['emeregencyContactPersonName'] = $valueStudent['EMERGENCY_CONTACT_PERSON_NAME'];
            $studentArr[$keyStudent]['birthDate'] = $valueStudent['BIRTHDATE'];
            $studentArr[$keyStudent]['passportNo'] = $valueStudent['PASSPORT_NO'];

            $studentArr[$keyStudent]['roomName'] = !empty($valueStudent['ROOM_NAME']) ? $valueStudent['ROOM_NAME'] : "";
            $studentArr[$keyStudent]['enrollmentDate'] = !empty($valueStudent['ENROLLMENT_DATE']) ? $valueStudent['ENROLLMENT_DATE'] : "";
            $studentArr[$keyStudent]['nationality'] = !empty($valueStudent['NATIONALITY']) ? $valueStudent['NATIONALITY'] : "";
            $studentArr[$keyStudent]['status'] = !empty($valueStudent['IS_ACTIVE']) ? $valueStudent['IS_ACTIVE'] : "";
            $studentArr[$keyStudent]['receiptAmt'] = !empty($valueStudent['RECEIPT_AMT']) ? $valueStudent['RECEIPT_AMT'] : "0.00";
            $studentArr[$keyStudent]['unbilledAmt'] = !empty($valueStudent['UNBILLED_AMT']) ? $valueStudent['UNBILLED_AMT'] : "";
            $studentArr[$keyStudent]['billedAmt'] = !empty($valueStudent['BILL_AMT']) ? $valueStudent['BILL_AMT'] : "";
            $studentArr[$keyStudent]['closingBalAmt'] = !empty($valueStudent['CLOSINGBAL_AMT']) ? $valueStudent['CLOSINGBAL_AMT'] : "";
            $studentArr[$keyStudent]['closingBalType'] = $valueStudent['CLOSINGBAL_TYPE'] ? $valueStudent['CLOSINGBAL_TYPE'] : "";

            $studentArr[$keyStudent]['profileImage'] = !empty($valueStudent['PROFILE_IMAGE']) ? $valueStudent['PROFILE_IMAGE'] : "";
            $requestData = $studentData = Array();
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];
            $requestData['postData']['type'] = 'PHOTO';
            $studentAttachmentPhotoData = listStudentAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentPhotoAllData = ($studentAttachmentPhotoData['status'] == SCS) ? $studentAttachmentPhotoData['data'] : $studentAttachmentPhotoData['status'];
            $studentArr[$keyStudent]['attachmentData']['photosArr'] = $studentAttachmentPhotoAllData;

            $requestData = $studentData = Array();
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];
            $requestData['postData']['type'] = 'MEDICAL';
            $studentAttachmentCertificateData = listStudentAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentCertificateAllData = ($studentAttachmentCertificateData['status'] == SCS) ? $studentAttachmentCertificateData['data'] : $studentAttachmentCertificateData['status'];
            $studentArr[$keyStudent]['attachmentData']['certificateArr'] = $studentAttachmentCertificateAllData;

            $sqlStudentSuspenssion = "SELECT PK_SUSPENSION_ID as suspenssionId,FK_STUDENT_ID AS studId,SUSPENSION_START_DATE as startDate,SUSPENSION_END_DATE as endDate,REASON as reason FROM " . STUDENTSUSPENSION . " WHERE FK_STUDENT_ID ='" . $valueStudent['PK_STUD_ID'] . "' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "'  ORDER BY CREATED_BY_DATE DESC LIMIT 0,1";


            $resStudentSuspenssion = fetch_rec_query($sqlStudentSuspenssion);
            if (count($resStudentSuspenssion) > 0) {
                $studentArr[$keyStudent]['suspenssionData'] = $resStudentSuspenssion;
            } else {
                $studentArr[$keyStudent]['suspenssionData'] = NORECORDS;
            }

            //$sqlStudent
            // ADDED BY KAUSHA SHAH ON 17-08-2018 START
            $nationality = isset($countryArr[$valueStudent['NATIONALITY']]) ? $countryArr[$valueStudent['NATIONALITY']] : "";
            $studentArr[$keyStudent]['nationalityName'] = (isset($nationality) && $nationality != '') ? trim($nationality) : "";
            $studentArr[$keyStudent]['studAddress'] = $valueStudent['ADDRESS_LINE1'] . ", " . $valueStudent['ADDRESS_LINE2'] . ", " . $valueStudent['ADDRESS_LINE3'];
            $parentCityName = (isset($cityArr[$valueStudent['parentCityId']]) && $cityArr[$valueStudent['parentCityId']] != '') ? trim($cityArr[$valueStudent['parentCityId']]) : "";
            $studentArr[$keyStudent]['parentCityName'] = $parentCityName;
            $parentStateName = (isset($stateArr[$valueStudent['parentStateId']]) && $stateArr[$valueStudent['parentStateId']] != '') ? trim($stateArr[$valueStudent['parentStateId']]) : "";
            $studentArr[$keyStudent]['parentStateName'] = $parentStateName;
            $parentCountryName = (isset($countryArr[$valueStudent['parentCountryId']]) && $countryArr[$valueStudent['parentCountryId']] != '') ? trim($countryArr[$valueStudent['parentCountryId']]) : "";
            $studentArr[$keyStudent]['parentCountryName'] = $parentCountryName;
            $parentRelation = (isset($clientLookupArr[$valueStudent['parentRelation']]) && $clientLookupArr[$valueStudent['parentRelation']] != '') ? trim($clientLookupArr[$valueStudent['parentRelation']]) : ""; // GET PARENT RELATION
            $studentArr[$keyStudent]['parentRelationName'] = $parentRelation;
            $guarantorRelation = (isset($clientLookupArr[$valueStudent['guarantorRelation']]) && $clientLookupArr[$valueStudent['guarantorRelation']] != '') ? trim($clientLookupArr[$valueStudent['guarantorRelation']]) : "";
            $studentArr[$keyStudent]['guarantorRelationName'] = $guarantorRelation;
            $studentArr[$keyStudent]['guarantorRelationId'] = $valueStudent['guarantorRelation'];

            $roomCategory = 0;

            if (isset($valueStudent['TYPE_OF_ROOM_REQUIRED']) && $valueStudent['TYPE_OF_ROOM_REQUIRED'] != '') {
                $roomCategory = isset($lookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']]) ? $lookupArr[$valueStudent['TYPE_OF_ROOM_REQUIRED']] : 0;
            }

            $tenancyEndDate = date("Y-m-d");
            $sqlTenancyData = "SELECT AT.ROOM_NAME,AT.TENANCY_START_DATE,AT.TENANCY_END_DATE,AT.TENANCY_DEPOSIT,AT.TENANCY_RENT,AT.FOOD_PREFERENCE,AT.BED_NO,AT.FK_ROOM_ID,AT.FACILITY_ALLOCATION,AT.ASSET_ISSUED,IM.ITEM_NAME FROM " . ALLOTMASTER . " AT LEFT JOIN " . ITEMMASTER . " IM ON AT.FK_ITEM_ID =IM.PK_ITEM_ID  WHERE AT.FK_STUDENT_ID =" . $valueStudent['PK_STUD_ID'] . " ORDER BY PK_ALLOTMENT_ID DESC LIMIT 0,1";
            $resTenancyData = fetch_rec_query($sqlTenancyData);

            if (count($resTenancyData) > 0) {
                $studentArr[$keyStudent]['roomNo'] = $resTenancyData[0]['ROOM_NAME'];
                $studentArr[$keyStudent]['bedNo'] = $resTenancyData[0]['BED_NO'];
                $studentArr[$keyStudent]['foodPreference'] = $resTenancyData[0]['FOOD_PREFERENCE'];
                $studentArr[$keyStudent]['tenacyStartDate'] = $resTenancyData[0]['TENANCY_START_DATE'];
                $studentArr[$keyStudent]['tenacyEndDate'] = $resTenancyData[0]['TENANCY_END_DATE'];
                $studentArr[$keyStudent]['tenacyDeposit'] = $resTenancyData[0]['TENANCY_DEPOSIT'];
                $studentArr[$keyStudent]['totalRent'] = $resTenancyData[0]['TENANCY_RENT'];
                $tenancyEndDate = $resTenancyData[0]['TENANCY_END_DATE'];
                $studentArr[$keyStudent]['roomType'] = $resTenancyData[0]['ITEM_NAME'];

                $studentArr[$keyStudent]['totalRent'] = $resTenancyData[0]['TENANCY_RENT'];
                if (isset($resTenancyData[0]['ASSET_ISSUED']) && $resTenancyData[0]['ASSET_ISSUED'] != '') {
                    $asset = explode(",", $resTenancyData[0]['ASSET_ISSUED']);

                    for ($i = 0; $i < count($asset); $i++) {
                        $assetVal[] = (isset($clientLookupArr[$asset[$i]]) && $clientLookupArr[$asset[$i]] != '') ? $clientLookupArr[$asset[$i]] : '';
                    }

                    $assetIssuedVal = implode(",", $assetVal);
                    $studentArr[$keyStudent]['assetIssued'] = $assetIssuedVal;
                } else {
                    $studentArr[$keyStudent]['assetIssued'] = '';
                }

                if (isset($resTenancyData[0]['FACILITY_ALLOCATION']) && $resTenancyData[0]['FACILITY_ALLOCATION'] != '') {
                    $facilityAlloc = explode(",", $resTenancyData[0]['FACILITY_ALLOCATION']);
                    for ($i = 0; $i < count($facilityAlloc); $i++) {
                        $facilityVal[] = isset($itemLookupArr[$facilityAlloc[$i]]) ? $itemLookupArr[$facilityAlloc[$i]] : '';
                    }
                    $facilityAllocation = implode(",", $facilityVal);
                    $studentArr[$keyStudent]['facilityAllocation'] = $facilityAllocation;
                } else {
                    $studentArr[$keyStudent]['facilityAllocation'] = '';
                }
                // ADDED BY KAUSHA SHAH ON 17-08-2018 END
            }


            $studentArr[$keyStudent]['roomCategory'] = (isset($roomCategory) && $roomCategory != '') ? trim($roomCategory) : "";

            /* START Code for renew button hide and show by satish */
            $today = date("Y-m-d");
            $endDate = date('Y-m-d', strtotime($today . ' + 1 month'));

            $renewButtonShow = 0;
            if (strtotime($tenancyEndDate) < strtotime($endDate)) {
                $renewButtonShow = 1;
            }
            $studentArr[$keyStudent]['renewStatus'] = $renewButtonShow;
            /* END Code for renew button hide and show by satish */

            //query for getting event count

            $today = date("Y-m-d");
            $whereClientIdCond = $whereOrgIdCond = "";

            if (!empty($clientid)) {
                $whereClientIdCond = " AND IM.FK_CLIENT_ID = '" . $clientid . "'";
            }
            if (!empty($orgId)) {
                $whereOrgIdCond = "AND IM.FK_ORG_ID = '" . $orgId . "'";
            }



            $sqlEventDetail = "SELECT COUNT(IM.PK_ITEM_ID) AS eventCount FROM " . ITEMMASTER . " IM  WHERE IM.DELETE_FLAG='0' " . $whereClientIdCond . $whereOrgIdCond . " AND DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%Y-%m-%d') >='" . $today . "' AND ITEM_TYPE='EVENTS' AND IS_ACTIVE='1' ";

            $fetch_eventCount = fetch_rec_query($sqlEventDetail);
            $eventCount = 0;
            if (count($fetch_eventCount) > 0) {
                $eventCount = $fetch_eventCount[0]['eventCount'];
            }

            $whereClientIdCond = $whereOrgIdCond = $whereStudIdCond = "";

            if (!empty($clientid)) {
                $whereClientIdCond = " AND CM.FK_CLIENT_ID = '" . $clientid . "'";
            }
            if (!empty($orgId)) {
                $whereOrgIdCond = " AND CM.FK_ORG_ID = '" . $orgId . "'";
            }

            if (!empty($valueStudent['PK_STUD_ID'])) {
                $whereStudIdCond = " AND CM.FK_STUD_ID = '" . $valueStudent['PK_STUD_ID'] . "'";
            }

            //query for getting complaint count student wise
            $query_complaintCount = "SELECT COUNT(CM.PK_COMPLAINT_ID) AS complaintCount  FROM " . COMPLAINTMASTER . " CM WHERE  DELETE_FLAG = '0' " . $whereClientIdCond . $whereOrgIdCond . $whereStudIdCond;
            $fetch_complaintCount = fetch_rec_query($query_complaintCount);
            $complaintCount = 0;
            if (count($fetch_complaintCount) > 0) {
                $complaintCount = $fetch_complaintCount[0]['complaintCount'];
            }

            $studentArr[$keyStudent]['eventCount'] = $eventCount;
            $studentArr[$keyStudent]['complaintCount'] = $complaintCount;
        }
        $result = array("status" => SCS, "data" => array_values($studentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function attachmentStudentUpload($postData, $postFile) {

    // echo "<pre>"; print_r($postData); exit;
    // POST DATA
    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $studId = isset($postdata['studentId']) ? addslashes(trim($postdata['studentId'])) : ""; // STUD ID
    $type = isset($postdata['type']) ? addslashes(trim($postdata['type'])) : ""; // TYPE
    $title = isset($postdata['title']) ? addslashes(trim($postdata['title'])) : ""; // TITLE
    $path = STUDENT_ATTACH_PATH;
    $attachmentName = $postFile; // ATTACH NAME 
    // START UPLOADING DATA INTO DATA BASE
    $uploadAttachment = uploadImage($attachmentName, $path); // UPLOAD STUDENT ATTACTMENT 
    //print_r($uploadAttachment); exit; 
    if ($uploadAttachment['status'] == SCS) {

        // INSERT DATA TO DATABASE FOR FURTHER REF.
        $insertAttach['FK_CLIENT_ID'] = $clientId;
        $insertAttach['FK_ORG_ID'] = $orgId;
        $insertAttach['ATTACH_CAT'] = $type;
        $insertAttach['REF_ID'] = $studId;
        $insertAttach['ATTACHFILE_LINK'] = $uploadAttachment['fileName'];
        $insertAttach['TITLE'] = $title;
        $insertAttach['CREATED_BY'] = $userId;
        $insertAttach['CREATED_BY_DATE'] = time();

        $insertAttach['DELETE_FLAG'] = 0;
        $insertAttach = insert_rec(ATTACHSTUDENTMST, $insertAttach); // INSERTING DATA IN ATTACH_STUDENT_MASTER TABLE


        if ($insertAttach) { // IF SUCCESS THEN GIVE SUCCESS RESPONSE.
            $finalJson = array("attachmentId" => $insertAttach['lastInsertedId'], "fileName" => basename($uploadAttachment['fileName']), "fileLink" => $uploadAttachment['fileName'], 'title' => $title);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        }
    } else {
        $result = $uploadAttachment;
        http_response_code(400);
    }

    return $result; // RETURN DATA TO PL
}

function addEditStudentAllotment($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $roomId = isset($postData['postData']['roomId']) ? addslashes(trim($postData['postData']['roomId'])) : "";
    $bedNo = isset($postData['postData']['bedNo']) ? addslashes(trim($postData['postData']['bedNo'])) : "";
    $assetIssued = isset($postData['postData']['assetIssued']) ? addslashes(trim($postData['postData']['assetIssued'])) : "";
    $roomName = isset($postData['postData']['roomName']) ? addslashes(trim($postData['postData']['roomName'])) : "";
    $tenancyStartDate = !empty($postData['postData']['tenancyStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['tenancyStartDate'])))) : "";
    $tenancyEndDate = !empty($postData['postData']['tenancyEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['tenancyEndDate'])))) : "";
    $foodPreference = !empty($postData['postData']['foodPreference']) ? addslashes(trim($postData['postData']['foodPreference'])) : "";
    $tenancyDeposit = !empty($postData['postData']['tenancyDeposit']) ? addslashes(trim($postData['postData']['tenancyDeposit'])) : "";
    $tenancyRent = !empty($postData['postData']['tenancyRent']) ? addslashes(trim($postData['postData']['tenancyRent'])) : "";
    $facilityAllocation = !empty($postData['postData']['facilityAllocation']) ? addslashes(trim($postData['postData']['facilityAllocation'])) : "";
    $allotmentId = isset($postData['postData']['allotmentId']) ? addslashes(trim($postData['postData']['allotmentId'])) : "";
    $studentPassword = isset($postData['postData']['studentPassword']) ? addslashes(trim($postData['postData']['studentPassword'])) : "";
    $parentsPassword = isset($postData['postData']['parentsPassword ']) ? addslashes(trim($postData['postData']['parentsPassword'])) : "";

    $finalAttachmentArr = array(); // GET ALL INVOICE ATTACHMENT LINK INTO SINGLE ARRAY TO SEND MAIL
    $whereAllotementCond = "";
    if (!empty($allotmentId)) {
        $whereAllotementCond = " AND PK_ALLOTMENT_ID!='" . $allotmentId . "'";
    }

    $sqlAllotmentQry = "SELECT PK_ALLOTMENT_ID FROM " . ALLOTMASTER . " WHERE FK_ITEM_ID='" . $itemId . "' AND FK_ROOM_ID='" . $roomId . "' " . $whereAllotementCond . " AND BED_NO='" . $bedNo . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DATE(TENANCY_END_DATE) >= '" . DATE($tenancyStartDate) . "'"; // QUERY FOR BED ROOM IS AVAILABLE OR NOT.

    $resAllotmentQry = fetch_rec_query($sqlAllotmentQry); // RESULT FOR BED ROOM IS AVAILABLE OR NOT.
    if (count($resAllotmentQry) > 0) {
        $result = array("status" => ALREADYADDEDBEDNO);
        http_response_code(400);
    } else {

        if ($clientId != "" && $orgId != "" && $itemId != "" && $studentId != "" && $roomId != "" && $bedNo != "" && $assetIssued != "") { // CHECK REQURIED FIELD CONDITION
            if (is_numeric($tenancyRent) && is_numeric($tenancyDeposit)) {
                $facilityAllocationArr = explode(",", $facilityAllocation);

                if (!empty($facilityAllocationArr)) {
                    $facilityAllocationIds = "";
                    foreach ($facilityAllocationArr as $keyAllocation => $valueAllocation) {
                        if ($valueAllocation != "") {
                            $allocationStrArr = explode(":", $valueAllocation);
                            $facilityAllocationIds .= ($allocationStrArr[0]) ? $allocationStrArr[0] . "," : "";
                        }
                    }
                    $facilityAllocationIds = trim($facilityAllocationIds, ",");
                } else {
                    $facilityAllocationIds = "";
                }

                // INSERT STUDENT ALLOTMENT ARRAY.
                $insertStudArray = Array(); //STUDENT ALLOTMENT ARRAY CREATE
                $insertStudArray['FK_CLIENT_ID'] = $clientId;
                $insertStudArray['FK_ORG_ID'] = $orgId;
                $insertStudArray['FK_ITEM_ID'] = $itemId;
                $insertStudArray['FK_STUDENT_ID'] = $studentId;
                $insertStudArray['FK_ROOM_ID'] = $roomId;
                $insertStudArray['BED_NO'] = $bedNo;
                $insertStudArray['ASSET_ISSUED'] = $assetIssued;
                $insertStudArray['TENANCY_START_DATE'] = $tenancyStartDate;
                $insertStudArray['TENANCY_END_DATE'] = $tenancyEndDate;
                $insertStudArray['ROOM_NAME'] = $roomName;
                $insertStudArray['FOOD_PREFERENCE'] = $foodPreference;
                $insertStudArray['TENANCY_DEPOSIT'] = $tenancyDeposit;
                $insertStudArray['TENANCY_RENT'] = $tenancyRent;
                $insertStudArray['FACILITY_ALLOCATION'] = $facilityAllocationIds;


                if (!empty($allotmentId)) {
                    $insertStudArray['CHANGED_BY'] = $userId;
                    $insertStudArray['CHANGED_BY_DATE'] = time();

                    $whereStudUpdate = "PK_ALLOTMENT_ID =" . $allotmentId;
                    $updateStudData = update_rec(ALLOTMASTER, $insertStudArray, $whereStudUpdate); // UPDATE IN STUDENT MASTER TABLE.
                    $allotmentId = $allotmentId;
                } else {
                    $insertStudArray['CREATED_BY'] = $userId;
                    $insertStudArray['CREATED_BY_DATE'] = time();
                    $insertStudent = insert_rec(ALLOTMASTER, $insertStudArray); // INSERT RECORD IN ALLOTMENT_MASTER TABLE.
                    $allotmentId = isset($insertStudent['lastInsertedId']) ? $insertStudent['lastInsertedId'] : "";
                }

                // NEW 
                if (!empty($allotmentId)) {


                    // START ALLOTEMENT TRAIL AND CALENDAR ENTRIES FOR STUDENT MASTER 

                    unset($insertStudArray['ASSET_ISSUED']);
                    unset($insertStudArray['ASSET_ISSUED']);
                    unset($insertStudArray['TENANCY_START_DATE']);
                    unset($insertStudArray['TENANCY_END_DATE']);
                    unset($insertStudArray['FOOD_PREFERENCE']);
                    unset($insertStudArray['TENANCY_DEPOSIT']);
                    unset($insertStudArray['TENANCY_RENT']);
                    unset($insertStudArray['FACILITY_ALLOCATION']);
                    /* End :Unset Extra key in StudArray */
                    $insertStudArray['DATE'] = date("Y-m-d");
                    $insertStudArray['CREATED_BY'] = $userId;
                    $insertStudArray['CREATED_BY_DATE'] = time();
                    $insertStudent = insert_rec(ALLOTTRAIL, $insertStudArray); // INSERT RECORD IN ALLOTMENT_TRAIL TABLE.

                    $facilityAllocationArr = explode(",", $facilityAllocation);

                    foreach ($facilityAllocationArr as $keyAlloaction => $valueAllocation) {
                        $allocationArr = explode(":", $valueAllocation);
                        // INSERT INVOICE TRANS ARRAY.
                        $insertAllotmentTrans = Array(); //CREATE INVOICE TRANS.
                        // $insertAllotmentTrans['FK_CLIENT_ID'] = $clientId;
                        //  $insertAllotmentTrans['FK_ORG_ID'] = $orgId;
                        $insertAllotmentTrans['FK_ALLOCATION_ID'] = $allotmentId;
                        $insertAllotmentTrans['FK_ITEM_ID'] = $valueAllocation;
                        $insertAllotmentTrans['FK_STUDENT_ID'] = $studentId;
                        $insertAllotmentTrans['ALLOCATION_START_DATE'] = $tenancyStartDate;
                        $insertAllotmentTrans['ALLOCATION_END_DATE'] = $tenancyEndDate;
                        $insertAllotmentTrans['QTY'] = (($allocationArr[1]) && $allocationArr[1] == "Laundry Services") ? 50 : -1;
                        $insertAllotmentTrans['CREATED_BY'] = $userId;
                        $insertAllotmentTrans['CREATED_BY_DATE'] = time();
                        $insertAllotmentTransData = insert_rec(ALLOCATIONTRANS, $insertAllotmentTrans); // INSERT RECORD IN ALLOCATION_TRANS TABLE.

                        if ($insertAllotmentTransData['lastInsertedId'] > 0) {

                            if (($allocationArr[1]) && $allocationArr[1] == "Laundry Services") {
                                $getAllocattionDates = getMonthWiseDateFromRange($tenancyStartDate, $tenancyEndDate);
                                foreach ($getAllocattionDates as $keyDate => $valDate) {
                                    $insertAllotmentTrans['ALLOCATION_START_DATE'] = $valDate['monthStartDate'];
                                    $insertAllotmentTrans['ALLOCATION_END_DATE'] = $valDate['monthEndDate'];
                                    $insertAllotmentTrans['FK_TRANS_ID'] = $insertAllotmentTransData['lastInsertedId'];
                                    $insertAllotmentTrans['FK_CLIENT_ID'] = $clientId;
                                    $insertAllotmentTrans['FK_ORG_ID'] = $orgId;
                                    $insertCalAllotmentTransData = insert_rec(CALALLOCATIONTRANS, $insertAllotmentTrans); // INSERT RECORD IN CALENDAR_ALLOCATION_TRANS TABLE.
                                }
                            } else {

                                $insertAllotmentTrans['FK_TRANS_ID'] = $insertAllotmentTransData['lastInsertedId'];
                                $insertAllotmentTrans['ALLOCATION_START_DATE'] = $tenancyStartDate;
                                $insertAllotmentTrans['ALLOCATION_END_DATE'] = $tenancyEndDate;
                                $insertAllotmentTrans['FK_CLIENT_ID'] = $clientId;
                                $insertAllotmentTrans['FK_ORG_ID'] = $orgId;
                                $insertCalAllotmentTransData = insert_rec(CALALLOCATIONTRANS, $insertAllotmentTrans); // INSERT RECORD IN CALENDAR_ALLOCATION_TRANS TABLE.
                            }
                        }
                    }
                    // END ALLOTEMENT TRAIL AND CALENDAR ENTRIES FOR STUDENT MASTER 
                    // START INSERTING DATA INTO CHARGE MASTER AGINST ALLOTMENT OF STUDENT
                    $finalInvoiceIds = array();

                    $checkStudentData = "SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS studName FROM " . STUDENTMASTER . " WHERE DELETE_FLAG='0' AND PK_STUD_ID='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
                    $resStudentData = fetch_rec_query($checkStudentData);
                    $studName = "";
                    if (count($resStudentData) > 0) {
                        $studName = $resStudentData[0]['studName'];
                    }

                    $itemIdRent = "-4"; // IF THIS FOUND INTO CHARGE MASTER THEN NEED TO CHECK THE CODE.
                    $itemNameRent = "Allotment Rent"; // IF THIS FOUND INTO CHARGE MASTER THEN NEED TO CHECK THE CODE.

                    $sqlItemData = "SELECT PK_ITEM_ID,ITEM_NAME FROM " . ITEMMASTER . " WHERE ITEM_NAME ='Rent' AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "' AND DELETE_FLAG ='0'";
                    $resItemData = fetch_rec_query($sqlItemData);
                    //echo "<pre>"; print_r($resItemData);
                    if (count($resItemData) > 0) {
                        $itemIdRent = $resItemData[0]['PK_ITEM_ID'];
                        $itemNameRent = $resItemData[0]['ITEM_NAME'];
                    }


                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['userId'] = $userId;
                    $requestData['postData']['invoiceStartDate'] = $tenancyStartDate;
                    $requestData['postData']['invoiceEndDate'] = $tenancyEndDate;
                    $requestData['postData']['chargeStartDate'] = date("Y-m-d");
                    $requestData['postData']['chargeEndDate'] = date("Y-m-d");
                    $requestData['postData']['invoiceName'] = "Allotment Rent";
                    $requestData['postData']['itemId'] = $itemIdRent;
                    $requestData['postData']['studentId'] = $studentId;
                    $requestData['postData']['studentName'] = $studName;
                    $requestData['postData']['itemName'] = $itemNameRent;
                    $requestData['postData']['amount'] = $tenancyRent;
                    $requestData['postData']['invoiceType'] = 'ENROLLMENT';
                    $requestData['postData']['chargeType'] = 'ENROLLMENT';
                    $requestData['postData']['studentPassword'] = $studentPassword;
                    $requestData['postData']['parentsPassword'] = $parentsPassword;


                    $chargeMasterData = addChargePostingData($requestData, 1); // GET ALL CHARGE MASTER DATA.
                    if ($chargeMasterData['status'] == SCS) {

                        $requestDataInvoice = Array();
                        $requestDataInvoice['postData']['clientId'] = $clientId;
                        $requestDataInvoice['postData']['orgId'] = $orgId;
                        $requestDataInvoice['postData']['userId'] = $userId;
                        $requestDataInvoice['postData']['invoiceStartDate'] = $tenancyStartDate;
                        $requestDataInvoice['postData']['invoiceEndDate'] = $tenancyEndDate;
                        $requestDataInvoice['postData']['chargeStartDate'] = date("Y-m-d");
                        ;
                        $requestDataInvoice['postData']['chargeEndDate'] = date("Y-m-d");
                        $requestDataInvoice['postData']['invoiceName'] = "Allotment Rent";
                        $requestDataInvoice['postData']['studentId'] = $studentId;
                        $requestDataInvoice['postData']['invoiceType'] = 'ENROLLMENT';
                        $requestDataInvoice['postData']['chargeType'] = 'ENROLLMENT';
                        $requestDataInvoice['postData']['studentPassword'] = $studentPassword;
                        $requestDataInvoice['postData']['parentsPassword'] = $parentsPassword;

                        $chargeMasterData = createInvoice($requestDataInvoice); // GET ALL CHARGE MASTER DATA.
                        if ($chargeMasterData['status'] == SCS) {
                            $finalInvoiceIds[] = $chargeMasterData['data'];
                        } else {
                            $finalInvoiceIds[] = NORECORDS;
                        }
                    } else {
                        $finalInvoiceIds[] = FAIL;
                    }
                    // END INSERTING DATA INTO CHARGE MASTER AGINST ALLOTMENT OF STUDENT
                    // START INSERTING DATA INTO CHARGE MASTER FOR DEPOSITE

                    $itemIdDeposite = "-4";
                    $itemNameDeposite = "Allotment Deposit";
                    $sqlItemDataDepo = "SELECT PK_ITEM_ID,ITEM_NAME FROM " . ITEMMASTER . " WHERE ITEM_NAME ='Deposit'";
                    $resItemDataDepo = fetch_rec_query($sqlItemDataDepo);

                    if (count($resItemDataDepo) > 0) {
                        $itemIdDeposite = $resItemDataDepo[0]['PK_ITEM_ID'];
                        $itemNameDeposite = $resItemDataDepo[0]['ITEM_NAME'];
                    }
                    $finalAttachmentArr[] = $chargeMasterData['data'];
                    $requestDataDeposit = Array();
                    $requestDataDeposit['postData']['clientId'] = $clientId;
                    $requestDataDeposit['postData']['orgId'] = $orgId;
                    $requestDataDeposit['postData']['userId'] = $userId;
                    $requestDataDeposit['postData']['invoiceStartDate'] = $tenancyStartDate;
                    $requestDataDeposit['postData']['invoiceEndDate'] = $tenancyEndDate;
                    $requestDataDeposit['postData']['chargeStartDate'] = date("Y-m-d");
                    $requestDataDeposit['postData']['chargeEndDate'] = date("Y-m-d");
                    $requestDataDeposit['postData']['invoiceName'] = "Allotment Deposit";
                    $requestDataDeposit['postData']['studentId'] = $studentId;
                    $requestDataDeposit['postData']['studentName'] = $studName;
                    $requestDataDeposit['postData']['itemId'] = $itemIdDeposite;
                    $requestDataDeposit['postData']['itemName'] = $itemNameDeposite;
                    $requestDataDeposit['postData']['amount'] = $tenancyDeposit;
                    $requestDataDeposit['postData']['invoiceType'] = 'ENROLLMENT';
                    $requestDataDeposit['postData']['chargeType'] = 'ENROLLMENT';
                    $requestDataDeposit['postData']['studentPassword'] = $studentPassword;
                    $requestDataDeposit['postData']['parentsPassword'] = $parentsPassword;


                    $chargeMasterDataDeposite = addChargePostingData($requestDataDeposit); // GET ALL CHARGE MASTER DATA. 
                    if ($chargeMasterDataDeposite['status'] == SCS) {

                        $requestDataInvoiceDepo = Array();
                        $requestDataInvoiceDepo['postData']['clientId'] = $clientId;
                        $requestDataInvoiceDepo['postData']['orgId'] = $orgId;
                        $requestDataInvoiceDepo['postData']['userId'] = $userId;
                        $requestDataInvoiceDepo['postData']['invoiceStartDate'] = $tenancyStartDate;
                        $requestDataInvoiceDepo['postData']['invoiceEndDate'] = $tenancyEndDate;
                        $requestDataInvoiceDepo['postData']['chargeStartDate'] = date("Y-m-d");
                        ;
                        $requestDataInvoiceDepo['postData']['chargeEndDate'] = date("Y-m-d");
                        $requestDataInvoiceDepo['postData']['invoiceName'] = "Allotment Deposit";
                        $requestDataInvoiceDepo['postData']['studentId'] = $studentId;
                        $requestDataInvoiceDepo['postData']['invoiceType'] = 'ENROLLMENT';
                        $requestDataInvoiceDepo['postData']['chargeType'] = 'ENROLLMENT';
                        $requestDataInvoiceDepo['postData']['studentPassword'] = $studentPassword;
                        $requestDataInvoiceDepo['postData']['parentsPassword'] = $parentsPassword;

                        $invoiceMasterDeposite = createInvoice($requestDataInvoiceDepo); // GET ALL CHARGE MASTER DATA.
                        if ($invoiceMasterDeposite['status'] == SCS) {
                            $finalInvoiceIds[] = $invoiceMasterDeposite['data'];
                        } else {
                            $finalInvoiceIds[] = NORECORDS;
                        }
                    } else {
                        $finalInvoiceIds[] = FAIL;
                    }
                    // END INSERTING DATA INTO CHARGE MASTER FOR DEPOSITE 
                    //print_r($finalInvoiceIds); exit;
                    // START SENDINF EMAIL INTO SINGLE MAIL OF RENT AND DEPOSITE
                    if (!empty($finalInvoiceIds)) {

                        $requestData = $studentData = Array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['studentId'] = $studentId;
                        $requestData['postData']['invoiceId'] = $finalInvoiceIds;

                        $invoiceSendStatus = sendGeneralInvoiceMail($requestData);  // GENERAL MAIL SENT CODE   
                        if ($invoiceSendStatus['status'] == SCS) {


                            /* Start Code for password sent to student and parent email id */
                            // GET STUDENT DETAIL
                            $sqlStudentData = "SELECT CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,SM.PK_STUD_ID,SM.EMAIL,CONCAT(SM.ADDRESS_LINE1,',',SM.ADDRESS_LINE2,',',SM.ADDRESS_LINE3) AS ADDRESS,SM.PINCODE,CO.COUNTRY_NAME,CT.STATE_NAME,CT.CITY_NAME,SM.MEMBERSHIP_NUMBER,SM.USER_NAME FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  WHERE SM.DELETE_FLAG ='0' AND SM.PK_STUD_ID='" . $studentId . "' AND SM.FK_CLIENT_ID = '" . $clientId . "' AND SM.FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA
                            $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

                            if (count($resStudentData) > 0 && !empty($resStudentData[0]['EMAIL'])) {
                                $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR CLIENT DATA
                                $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA
                                $fromAddress = 'support@lithe.in';
                                $fromName = 'Lithe Technology';


                                $mailSubject = "INDE CAMPUS : Student Login Detail";

                                $messageBody = '<!DOCTYPE html>
                            <html class="no-js">

                               <head>
                                  <link rel="stylesheet" type="text/css" href="css/app.css">
                                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                  <meta name="viewport" content="width=device-width">
                                  <title>Charge Posting Email Template</title>
                                  <!-- <style> -->
                               </head>
                                <body style="background:#fff;">
                                    <table style="background:#0077c0; padding:10px 10px 10px 10px; width:100%; max-width:600px;margin:0 auto;">
                                        <tbody><tr>
                                            <td>
                                                <table width="100%" style="border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #0077c0 solid;"><img src="logo-web.png" alt="" style="width:50px; height:auto;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                                                        </tr>
                                                    </tbody>
                                                </table>  
                                                <table width="100%" style="background:#ffffff;border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"> <p style="margin:0 0 5px 0;"> Inde Campus is a chain of hostels who wish to ensure that the hostel experience one has gets duly shared with the world class. A bit of us can be seen in all our hostels through the personalized artworks and the little touches put in by our team to enable our student to distinctly remember us by. Student are encouraged to leave their mark at our hostels by way off wall art or by just getting involved.

We believe in providing affordable living with all basic amenities in place along with security. We strive to expand within the next few years to ensure that our student get to stay with us as a part of the inde campus family.</p> </td> 
                                                        </tr>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;">
                                                                <ol style="padding:0 0 0 20px; margin:0;">
                                                                    <li>Student UserName :- ' . $resStudentData[0]['USER_NAME'] . '</li>
                                                                    <li>Student PassWord :- 1234</li>
                                                                </ol>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;"> Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/> ' . $resClientTrans[0]['ADDRESS'] . '</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table width="100%" style="border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:25px 5px 25px 5px; background:#a6ce39; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                                                        </tr>
                                                    </tbody>      
                                                </table> 
                                            </td>
                                        </tr>            
                                    </tbody>
                                </table>
                            </body>
                            </html>';



                                $insertCronMailMaster = Array();
                                $insertCronMailMaster['FK_CLIENT_ID'] = $clientId;
                                $insertCronMailMaster['TYPE_OF_MESSAGE'] = "MAIL";
                                $insertCronMailMaster['SENDER_EMAIL'] = $fromAddress;
                                $insertCronMailMaster['SENDER_NAME'] = $fromName;
                                $insertCronMailMaster['RECIVER_EMAIL'] = $resStudentData[0]['EMAIL'];
                                $insertCronMailMaster['RECIVER_NAME'] = $resStudentData[0]['STUD_NAME'];
                                $insertCronMailMaster['MAIL_SUBJECT'] = $mailSubject;
                                $insertCronMailMaster['MAIL_CONTENT'] = $messageBody;
                                $insertCronMailMaster['ATTACHMENT_LINK'] = '';
                                $insertCronMailMaster['SEND_STATUS'] = "0";
                                $insertCronMailMaster['TYPE_OF_MAIL'] = "INVOICE";
                                $insertMailData = insert_rec(CRONMAILMASTER, $insertCronMailMaster);
                            }


                            /* Start Code for password sent to student and parent email id */
                            // GET STUDENT DETAIL
                            $sqlStudentData = "SELECT PARENT_NAME AS STUD_NAME,USER_NAME,EMAIL FROM " . PARENTMASTER . " PM  WHERE PM.DELETE_FLAG ='0' AND PM.FK_STUD_ID='" . $studentId . "' AND PM.FK_CLIENT_ID = '" . $clientId . "' AND PM.FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR STUDENT DATA
                            $resStudentData = fetch_rec_query($sqlStudentData); //RESULT FOR STUDENT DATA

                            if (count($resStudentData) > 0 && !empty($resStudentData[0]['EMAIL'])) {
                                $sqlClientTrans = "SELECT FULLNAME,CONTACT_NO,EMAIL,ADDRESS FROM " . USERMASTER . " WHERE PK_USER_ID='" . $userId . "' AND DELETE_FLAG='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; // QUERY FOR CLIENT DATA
                                $resClientTrans = fetch_rec_query($sqlClientTrans); //RESULT FOR CLIENT DATA

                                $fromAddress = 'support@lithe.in';
                                $fromName = 'Lithe Technology';


                                $mailSubject = "INDE CAMPUS : Parent Login Detail";

                                $messageBody = '<!DOCTYPE html>
                            <html class="no-js">

                               <head>
                                  <link rel="stylesheet" type="text/css" href="css/app.css">
                                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                  <meta name="viewport" content="width=device-width">
                                  <title>Charge Posting Email Template</title>
                                  <!-- <style> -->
                               </head>
                                <body style="background:#fff;">
                                    <table style="background:#0077c0; padding:10px 10px 10px 10px; width:100%; max-width:600px;margin:0 auto;">
                                        <tbody><tr>
                                            <td>
                                                <table width="100%" style="border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:20px 10px 20px 10px; background:#ffffff; text-align:center; color:#0077c0; font-size:26px;font-family: arial; line-height: 40px; border-bottom: 4px #0077c0 solid;"><img src="logo-web.png" alt="" style="width:50px; height:auto;"> <br/> Dear ' . $resStudentData[0]['STUD_NAME'] . '</td>
                                                        </tr>
                                                    </tbody>
                                                </table>  
                                                <table width="100%" style="background:#ffffff;border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;"> <p style="margin:0 0 5px 0;"> Inde Campus is a chain of hostels who wish to ensure that the hostel experience one has gets duly shared with the world class. A bit of us can be seen in all our hostels through the personalized artworks and the little touches put in by our team to enable our student to distinctly remember us by. Student are encouraged to leave their mark at our hostels by way off wall art or by just getting involved.

We believe in providing affordable living with all basic amenities in place along with security. We strive to expand within the next few years to ensure that our student get to stay with us as a part of the inde campus family.</p> </td> 
                                                        </tr>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:16px;font-family: arial; line-height: 20px;">
                                                                <ol style="padding:0 0 0 20px; margin:0;">
                                                                     <li>Parent UserName :- ' . $resStudentData[0]['USER_NAME'] . '</li>
                                                                    <li>Parent PassWord :- 1234</li>
                                                                </ol>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middel" style="padding:10px 10px 10px 10px; text-align:left; color:#000; font-size:14px;font-family: arial; line-height: 18px;"> Thanks &amp; Regards. <br/>' . $resClientTrans[0]['FULLNAME'] . ' <br/> M : ' . $resClientTrans[0]['CONTACT_NO'] . ' <br/> Email : ' . $resClientTrans[0]['EMAIL'] . ' <br/> ' . $resClientTrans[0]['ADDRESS'] . '</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table width="100%" style="border-spacing: 0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middel" style="padding:25px 5px 25px 5px; background:#a6ce39; text-align:left; color:#fff; font-size:14px;font-family: arial; line-height: 18px;"> <b>Note :</b> This message is intended to send the recipient, in case you have received this by mistake then please ignore this mail.</td>
                                                        </tr>
                                                    </tbody>      
                                                </table> 
                                            </td>
                                        </tr>            
                                    </tbody>
                                </table>
                            </body>
                            </html>';



                                $insertCronMailMaster = Array();
                                $insertCronMailMaster['FK_CLIENT_ID'] = $clientId;
                                $insertCronMailMaster['TYPE_OF_MESSAGE'] = "MAIL";
                                $insertCronMailMaster['SENDER_EMAIL'] = $fromAddress;
                                $insertCronMailMaster['SENDER_NAME'] = $fromName;
                                $insertCronMailMaster['RECIVER_EMAIL'] = $resStudentData[0]['EMAIL'];
                                $insertCronMailMaster['RECIVER_NAME'] = $resStudentData[0]['STUD_NAME'];
                                $insertCronMailMaster['MAIL_SUBJECT'] = $mailSubject;
                                $insertCronMailMaster['MAIL_CONTENT'] = $messageBody;
                                $insertCronMailMaster['ATTACHMENT_LINK'] = '';
                                $insertCronMailMaster['SEND_STATUS'] = "0";
                                $insertCronMailMaster['TYPE_OF_MAIL'] = "INVOICE";
                                $insertMailData = insert_rec(CRONMAILMASTER, $insertCronMailMaster);
                            }

                            /* End Code for password sent to student and parent email id */

                            /* End Code for password sent to student and parent email id */

                            $requestData = $studentData = Array();
                            $requestData['postData']['clientId'] = $clientId;
                            $requestData['postData']['orgId'] = $orgId;
                            $requestData['postData']['studentId'] = $studentId;
                            $studentData = getStudentAllotment($requestData); // GET ALL STUDENT ALLOTMENT DATA.
                            $studentAllData = ($studentData['status'] == SCS) ? $studentData['data'] : $studentData['status'];

                            $result = array("status" => SCS, "data" => $studentAllData);
                            http_response_code(200);
                        } else {
                            $result = array("status" => $invoiceSendStatus['status']);
                            http_response_code(400);
                        }
                    } else {
                        $result = array("status" => NOTGENERATEINVOICE);
                        http_response_code(400);
                    }
                } else {
                    $result = array("status" => INSERTFAIL . " " . ALLOTMASTER);
                    http_response_code(400);
                }
            } else {
                $result = array("status" => RENTANDDEPOSITISONLYNUMERIC);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    }
    return $result;
}

function getStudentAllotment($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";

    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($studentId)) {
        $sqlStudentAllotment = "SELECT PK_ALLOTMENT_ID,FK_CLIENT_ID,FK_ORG_ID,FK_ITEM_ID,FK_STUDENT_ID,FK_ROOM_ID,BED_NO,TENANCY_START_DATE,TENANCY_END_DATE,ASSET_ISSUED,FOOD_PREFERENCE,TENANCY_DEPOSIT,TENANCY_RENT,FACILITY_ALLOCATION FROM " . ALLOTMASTER . " WHERE FK_STUDENT_ID='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG ='0' ORDER BY CREATED_BY_DATE DESC"; // QUERY FOR STUDENT ALLOTMENT DATA
        $resStudentAllotment = fetch_rec_query($sqlStudentAllotment); // RESULT FOR STUDENT ALLOTMENT DATA
        if (count($resStudentAllotment) > 0) {
            $allotmentArr = Array();
            foreach ($resStudentAllotment as $keyAllotment => $valueAllotment) {
                $allotmentArr[$keyAllotment]['allotmentId'] = $valueAllotment['PK_ALLOTMENT_ID'];
                $allotmentArr[$keyAllotment]['clientId'] = $valueAllotment['FK_CLIENT_ID'];
                $allotmentArr[$keyAllotment]['orgId'] = $valueAllotment['FK_ORG_ID'];
                $allotmentArr[$keyAllotment]['itemId'] = $valueAllotment['FK_ITEM_ID'];
                $allotmentArr[$keyAllotment]['studeId'] = $valueAllotment['FK_STUDENT_ID'];
                $allotmentArr[$keyAllotment]['roomId'] = $valueAllotment['FK_ROOM_ID'];
                $allotmentArr[$keyAllotment]['bedNo'] = $valueAllotment['BED_NO'];
                $allotmentArr[$keyAllotment]['foodPreference'] = $valueAllotment['FOOD_PREFERENCE'];
                $allotmentArr[$keyAllotment]['tenancyDeposit'] = $valueAllotment['TENANCY_DEPOSIT'];
                $allotmentArr[$keyAllotment]['tenancyRent'] = $valueAllotment['TENANCY_RENT'];
                $allotmentArr[$keyAllotment]['facilityAllocation'] = $valueAllotment['FACILITY_ALLOCATION'];
                $allotmentArr[$keyAllotment]['tenancyStartDate'] = $valueAllotment['TENANCY_START_DATE'];
                $allotmentArr[$keyAllotment]['tenancyEndDate'] = $valueAllotment['TENANCY_END_DATE'];
                $allotmentArr[$keyAllotment]['assetIssued'] = $valueAllotment['ASSET_ISSUED'];
            }
            $result = array("status" => SCS, "data" => $allotmentArr);
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function listStudentAttachment($postData) {
    // POST DATA
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";

    $whereTypeCond = "";
    if (!empty($type)) {
        $whereTypeCond = " AND ATTACH_CAT='" . $type . "'";
    }

    if (!empty($studentId)) {

        $sqlStudentAttachment = "SELECT PK_ATTACH_ID,ATTACH_CAT,REF_ID,ATTACHFILE_LINK,TITLE FROM " . ATTACHSTUDENTMST . " WHERE REF_ID='" . $studentId . "' " . $whereTypeCond . " AND DELETE_FLAG ='0'"; // QUERY FOR LISTING STUDENT ATTACHMENT. 
        $resStudentAttachment = fetch_rec_query($sqlStudentAttachment); // RESULT FOR LISTING STUDENT ATTACHMENT. 
        if (count($resStudentAttachment) > 0) {
            $attachmentArr = Array();
            foreach ($resStudentAttachment as $keyAttachment => $valueAttachment) {
                $attachmentArr[$keyAttachment]['attachmentId'] = $valueAttachment['PK_ATTACH_ID'];
                $attachmentArr[$keyAttachment]['attachType'] = $valueAttachment['ATTACH_CAT'];
                $attachmentArr[$keyAttachment]['refId'] = $valueAttachment['REF_ID'];
                $attachmentArr[$keyAttachment]['attachmentLink'] = $valueAttachment['ATTACHFILE_LINK'];
                $attachmentArr[$keyAttachment]['title'] = $valueAttachment['TITLE'];
            }
            $result = array("status" => SCS, "data" => $attachmentArr);
            http_response_code(400);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function deleteStudentAttachment($postData) {
    //POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $attachmentId = isset($postData['postData']['attachmentId']) ? addslashes(trim($postData['postData']['attachmentId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $attachName = isset($postData['postData']['attachName']) ? addslashes(trim($postData['postData']['attachName'])) : "";
    $attachPath = isset($postData['postData']['attachPath']) ? addslashes(trim($postData['postData']['attachPath'])) : "";


    if (!empty($clientId) && !empty($orgId) && !empty($attachmentId)) {
        $updateAttachmentArray = Array(); //ATTACHMENT ARRAY UPDATE
        $updateAttachmentArray['DELETE_FLAG'] = 1;
        $updateAttachmentArray['CHANGED_BY'] = $userId;
        $updateAttachmentArray['CHANGED_BY_DATE'] = time();

        $whereAttachmentUpdate = "DELETE_FLAG ='0' AND PK_ATTACH_ID ='" . $attachmentId . "' AND REF_ID='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
        $updateAttachment = update_rec(ATTACHSTUDENTMST, $updateAttachmentArray, $whereAttachmentUpdate); // UPDATE DELETE_FLAG =1 IN ATTACH_STUDENT_MASTER TABLE.
        if ($updateAttachment) {

            if (!empty($attachName)) {
                unlink(APP_SERVER_ROOT . "/attachment/student_attachment/" . $attachName); //DELETE ATTACHMENT FILE FROM FOLDER.
            }
            $requestData = $studentAttachmentData = Array();
            $requestData['postData']['studentId'] = $studentId;
            $requestData['postData']['type'] = $type;
            $studentAttachmentData = listStudentAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentAllData = ($studentAttachmentData['status'] == SCS) ? $studentAttachmentData['data'] : $studentAttachmentData['status'];

            $result = array("status" => SCS, "data" => $studentAttachmentAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . ATTACHSTUDENTMST);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function deleteStudentData($postData) {


    //POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    $today = date("Y-m-d");
    if (!empty($clientId) && !empty($orgId) && !empty($studentId)) {

        $checkStudentAllotmentAvailabel = "SELECT PK_ALLOTMENT_ID FROM " . ALLOTMASTER . " WHERE FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_STUDENT_ID ='" . $studentId . "' AND DELETE_FLAG ='0' AND TENANCY_END_DATE > '" . $today . "'";
        $resStudentAllotmentAvailabel = fetch_rec_query($checkStudentAllotmentAvailabel);
        if (count($resStudentAllotmentAvailabel) == 0) {
            $updateStudentArray = Array(); //STUDENT ARRAY UPDATE
            $updateStudentArray['DELETE_FLAG'] = 1;
            $updateStudentArray['CHANGED_BY'] = $userId;
            $updateStudentArray['CHANGED_BY_DATE'] = time();

            $whereStudentUpdate = "DELETE_FLAG ='0' AND PK_STUD_ID='" . $studentId . "'";
            $whereMultiStudentUpdate = "DELETE_FLAG ='0' AND FK_STUD_ID='" . $studentId . "'";
            $updateStudent = update_rec(STUDENTMASTER, $updateStudentArray, $whereStudentUpdate); // UPDATE DELETE_FLAG =1 IN STUDENT_MASTER TABLE.
            $updateMultiPhone = update_rec(MULTIPHONEMASTER, $updateStudentArray, $whereMultiStudentUpdate); // UPDATE DELETE_FLAG =1 IN MULTI PHONE MASTER TABLE.
            $updateMultiAddress = update_rec(MULTIADDRESSMASTER, $updateStudentArray, $whereMultiStudentUpdate); // UPDATE DELETE_FLAG =1 IN MULTI PHONE ADDRESS TABLE.
            $updateMultiEmail = update_rec(MULTIEMAILMASTER, $updateStudentArray, $whereMultiStudentUpdate); // UPDATE DELETE_FLAG =1 IN MULTI EMAIL MASTER TABLE.
            if ($updateStudent) {

                /* if (!empty($attachPath)) {
                  unlink($attachPath); //DELETE ATTACHMENT FILE FROM FOLDER.
                  }
                 */
                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $studentData = getStudentData($requestData); // GET ALL STUDENT DATA.
                $studentAllData = ($studentData['status'] == SCS) ? $studentData['data'] : $studentData['status'];

                $result = array("status" => SCS, "data" => $studentAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => STUDENTNOTDELETED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function getStudentDetailData($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $whereStudentCond = "";
    if (!empty($studentId)) {
        $whereStudentCond = " AND SM.PK_STUD_ID =" . $studentId;
    }


    $sqlStudentData = "SELECT SM.PK_STUD_ID,SM.FK_CLIENT_ID,SM.USER_NAME,SM.MEMBERSHIP_NUMBER,SM.FK_ORG_ID,SM.FIRST_NAME,SM.LAST_NAME,SM.FK_COUNTRY_ID,SM.FK_STATE_ID,SM.FK_CITY_ID,SM.COUNTRY_CODE,SM.MOBILE_NO,SM.EMAIL,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.PARENT_NAME,SM.PARENT_EMAIL_ID,SM.PARENT_MOBILE_NO,SM.PARENT_USERNAME,SM.PARENT_MEMBERSHIP_NUMBER,SM.COLLEGE_NAME,SM.COLLEGE_ADDRESS_LINE1,SM.COLLEGE_ADDRESS_LINE2,SM.COLLEGE_COUNTRY_ID,SM.COLLEGE_STATE_ID,SM.COLLEGE_CITY_ID,CI.STATE_NAME AS COLLEGE_STATE_NAME,CI.CITY_NAME AS COLLEGE_CITY_NAME,SM.COLLEGE_ADDRESS_LINE3,SM.COLLEGE_PINCODE,SM.GENDER,SM.BLOOD_GROUP,SM.COURSE,SM.ACDEMIC_START_DATE,SM.ACDEMIC_END_DATE,SM.SEMESTER,SM.GUARANTOR_NAME,SM.GUARANTOR_ADDRESS_LINE1,SM.GUARANTOR_ADDRESS_LINE2,SM.GUARANTOR_ADDRESS_LINE3,SM.GUARANTOR_EMAIL,SM.GUARANTOR_MOBILE1,SM.GUARANTOR_MOBILE2,SM.GUARANTOR_CITY_ID,SM.GUARANTOR_STATE_ID,SM.GUARANTOR_COUNTRY_ID,GT.STATE_NAME AS GUARANTOR_STATE_NAME,GT.CITY_NAME AS GUARANTOR_CITY_NAME,SM.MEDICAL_CONDITION,SM.AADHAR_NO,SM.TYPE_OF_ROOM_REQUIRED,SM.EMERGENCY_CONTACT_PERSON_NO,SM.BIRTHDATE,SM.ANNIVERSARYDATE,CO.COUNTRY_NAME,ST.STATE_NAME,SM.PASSPORT_NO,CT.CITY_NAME,CL.DISPLAY_VALUE AS ITEM_NAME,AT.ROOM_NAME,SM.EMERGENCY_CONTACT_PERSON_NAME,SM.PASSPORT_NO,SM.GUARANTOR_PINCODE FROM " . STUDENTMASTER . " SM LEFT JOIN " . COUNTRY . " CO ON SM.FK_COUNTRY_ID=CO.PK_COUNTRY_ID LEFT JOIN " . STATE . " ST ON ST.PK_STATE_ID=SM.FK_STATE_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID =SM.FK_CITY_ID  LEFT JOIN " . CITY . " GT ON GT.PK_CITY_ID =SM.GUARANTOR_CITY_ID  LEFT JOIN " . CITY . " CI ON CI.PK_CITY_ID =SM.COLLEGE_CITY_ID LEFT JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID=SM.TYPE_OF_ROOM_REQUIRED  LEFT JOIN " . ALLOTMASTER . " AT ON SM.PK_STUD_ID=AT.FK_STUDENT_ID AND AT.DELETE_FLAG ='0' AND DATE(now()) >= AT.TENANCY_START_DATE AND DATE(now()) <= AT.TENANCY_END_DATE WHERE SM.DELETE_FLAG =0 " . $whereStudentCond . " AND SM.FK_CLIENT_ID ='" . $clientId . "' AND SM.FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR STUDENT DATA.

    $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR STUDENT DATA.
    if (count($resStudentData) > 0) {
        $studentArr = Array();
        foreach ($resStudentData as $keyStudent => $valueStudent) {

            $studentArr[$keyStudent]['studId'] = $valueStudent['PK_STUD_ID'];
            $studentArr[$keyStudent]['clientId'] = $valueStudent['FK_CLIENT_ID'];
            $studentArr[$keyStudent]['orgId'] = $valueStudent['FK_ORG_ID'];
            $studentArr[$keyStudent]['userName'] = $valueStudent['USER_NAME'];
            $studentArr[$keyStudent]['firstName'] = $valueStudent['FIRST_NAME'];
            $studentArr[$keyStudent]['lastName'] = $valueStudent['LAST_NAME'];
            $studentArr[$keyStudent]['memberShipNumber'] = $valueStudent['MEMBERSHIP_NUMBER'];
            $studentArr[$keyStudent]['countryId'] = $valueStudent['FK_COUNTRY_ID'];
            $studentArr[$keyStudent]['countryName'] = $valueStudent['COUNTRY_NAME'];
            $studentArr[$keyStudent]['stateId'] = $valueStudent['FK_STATE_ID'];
            $studentArr[$keyStudent]['stateName'] = $valueStudent['STATE_NAME'];
            $studentArr[$keyStudent]['cityId'] = $valueStudent['FK_CITY_ID'];
            $studentArr[$keyStudent]['cityName'] = $valueStudent['CITY_NAME'];
            $studentArr[$keyStudent]['countryCode'] = $valueStudent['COUNTRY_CODE'];
            $studentArr[$keyStudent]['mobileNo'] = $valueStudent['MOBILE_NO'];
            $studentArr[$keyStudent]['email'] = $valueStudent['EMAIL'];

            $studentArr[$keyStudent]['parentName'] = $valueStudent['PARENT_NAME'];
            $studentArr[$keyStudent]['parentEmailId'] = $valueStudent['PARENT_EMAIL_ID'];
            $studentArr[$keyStudent]['parentMobileNo'] = $valueStudent['PARENT_MOBILE_NO'];
            $studentArr[$keyStudent]['parentUserName'] = $valueStudent['PARENT_USERNAME'];
            $studentArr[$keyStudent]['parentMembershipNumber'] = $valueStudent['PARENT_MEMBERSHIP_NUMBER'];

            $studentArr[$keyStudent]['addressLine1'] = $valueStudent['ADDRESS_LINE1'];
            $studentArr[$keyStudent]['addressLine2'] = $valueStudent['ADDRESS_LINE2'];
            $studentArr[$keyStudent]['addressLine3'] = $valueStudent['ADDRESS_LINE3'];
            $studentArr[$keyStudent]['pincode'] = $valueStudent['PINCODE'];
            $studentArr[$keyStudent]['collegeName'] = $valueStudent['COLLEGE_NAME'];
            $studentArr[$keyStudent]['collegeAddressLine1'] = $valueStudent['COLLEGE_ADDRESS_LINE1'];
            $studentArr[$keyStudent]['collegeAddressLine2'] = $valueStudent['COLLEGE_ADDRESS_LINE2'];
            $studentArr[$keyStudent]['collegeAddressLine3'] = $valueStudent['COLLEGE_ADDRESS_LINE3'];
            $studentArr[$keyStudent]['collegeCountryId'] = $valueStudent['COLLEGE_COUNTRY_ID'];
            $studentArr[$keyStudent]['collegeStateId'] = $valueStudent['COLLEGE_STATE_ID'];
            $studentArr[$keyStudent]['collegeCityId'] = $valueStudent['COLLEGE_CITY_ID'];
            $studentArr[$keyStudent]['collegeCountryName'] = $valueStudent['COUNTRY_NAME'];
            $studentArr[$keyStudent]['collegeStateName'] = $valueStudent['COLLEGE_STATE_NAME'];
            $studentArr[$keyStudent]['collegeCityName'] = $valueStudent['COLLEGE_CITY_NAME'];
            $studentArr[$keyStudent]['collegePinCode'] = $valueStudent['COLLEGE_PINCODE'];


            $studentArr[$keyStudent]['gender'] = $valueStudent['GENDER'];
            $studentArr[$keyStudent]['bloodGroup'] = $valueStudent['BLOOD_GROUP'];
            $studentArr[$keyStudent]['course'] = $valueStudent['COURSE'];
            $studentArr[$keyStudent]['acdemicStartYear'] = $valueStudent['ACDEMIC_START_DATE'];
            $studentArr[$keyStudent]['acdemicEndYear'] = $valueStudent['ACDEMIC_END_DATE'];
            $studentArr[$keyStudent]['semester'] = $valueStudent['SEMESTER'];
            $studentArr[$keyStudent]['guarantorName'] = $valueStudent['GUARANTOR_NAME'];
            $studentArr[$keyStudent]['guarantorAddressLine1'] = $valueStudent['GUARANTOR_ADDRESS_LINE1'];
            $studentArr[$keyStudent]['guarantorAddressLine2'] = $valueStudent['GUARANTOR_ADDRESS_LINE2'];
            $studentArr[$keyStudent]['guarantorAddressLine3'] = $valueStudent['GUARANTOR_ADDRESS_LINE3'];
            $studentArr[$keyStudent]['guarantorPinCode'] = $valueStudent['GUARANTOR_PINCODE'];
            $studentArr[$keyStudent]['guarantorEmail'] = $valueStudent['GUARANTOR_EMAIL'];
            $studentArr[$keyStudent]['guarantorMobile1'] = $valueStudent['GUARANTOR_MOBILE1'];
            $studentArr[$keyStudent]['guarantorMobile2'] = $valueStudent['GUARANTOR_MOBILE2'];
            $studentArr[$keyStudent]['guarantorCityId'] = $valueStudent['GUARANTOR_CITY_ID'];
            $studentArr[$keyStudent]['guarantorStateId'] = $valueStudent['GUARANTOR_STATE_ID'];
            $studentArr[$keyStudent]['guarantorCountryId'] = $valueStudent['FK_COUNTRY_ID'];
            $studentArr[$keyStudent]['guarantorCityName'] = $valueStudent['GUARANTOR_CITY_NAME'];
            $studentArr[$keyStudent]['guarantorStateName'] = $valueStudent['GUARANTOR_STATE_NAME'];
            $studentArr[$keyStudent]['guarantorCountryName'] = $valueStudent['COUNTRY_NAME'];
            $studentArr[$keyStudent]['medicalCondition'] = $valueStudent['MEDICAL_CONDITION'];
            $studentArr[$keyStudent]['aadharNo'] = $valueStudent['AADHAR_NO'];
            $studentArr[$keyStudent]['passportNo'] = $valueStudent['PASSPORT_NO'];
            $studentArr[$keyStudent]['typeOfRoomRequired'] = $valueStudent['TYPE_OF_ROOM_REQUIRED'];
            $studentArr[$keyStudent]['emeregencyContactPersonNo'] = $valueStudent['EMERGENCY_CONTACT_PERSON_NO'];
            $studentArr[$keyStudent]['emeregencyContactPersonName'] = $valueStudent['EMERGENCY_CONTACT_PERSON_NAME'];
            $studentArr[$keyStudent]['birthDate'] = $valueStudent['BIRTHDATE'];
            $studentArr[$keyStudent]['aniversaryDate'] = $valueStudent['ANNIVERSARYDATE'];
            $studentArr[$keyStudent]['passportNo'] = $valueStudent['PASSPORT_NO'];

            $studentArr[$keyStudent]['roomName'] = !empty($valueStudent['ROOM_NAME']) ? $valueStudent['ROOM_NAME'] : "";


            $requestData = $emailData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];

            $emailData = getStudentMultiEmail($requestData); // GET ALL EMAIL DATA.
            $emailAllData = ($emailData['status'] == SCS) ? $emailData['data'] : $emailData['status'];

            $studentArr[$keyStudent]['multiEmailData'] = $emailAllData;

            $requestData = $phoneData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];

            $phoneData = getStudentMultiPhone($requestData); // GET ALL PHONE DATA.
            $phoneAllData = ($phoneData['status'] == SCS) ? $phoneData['data'] : $phoneData['status'];

            $studentArr[$keyStudent]['multiPhoneData'] = $phoneAllData;

            $requestData = $phoneData = Array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['orgId'] = $orgId;
            $requestData['postData']['studentId'] = $valueStudent['PK_STUD_ID'];

            $addressData = getMultiAddressData($requestData); // GET ALL Address DATA.
            $addressAllData = ($addressData['status'] == SCS) ? $addressData['data'] : $addressData['status'];

            $studentArr[$keyStudent]['multiAddressData'] = $addressAllData;
        }
        $result = array("status" => SCS, "data" => array_values($studentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getStudentMultiEmail($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    $sqlStudentMultiEmailData = "SELECT PK_EMAIL_ID,FK_STUD_ID,EMAIL_ADDRESS,DEFAULT_FLAG,DELETE_FLAG FROM " . MULTIEMAILMASTER . " SM  WHERE DELETE_FLAG =0 AND FK_STUD_ID ='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR STUDENT Multi Email DATA.
    $resStudentData = fetch_rec_query($sqlStudentMultiEmailData); // RESULT FOR STUDENT Multi Email DATA.
    if (count($resStudentData) > 0) {
        $studentEmailArr = Array();
        foreach ($resStudentData as $keyStudent => $valueStudent) {
            $studentEmailArr[$keyStudent]['pkEmailId'] = $valueStudent['PK_EMAIL_ID'];
            $studentEmailArr[$keyStudent]['studId'] = $valueStudent['FK_STUD_ID'];
            $studentEmailArr[$keyStudent]['emailAddress'] = $valueStudent['EMAIL_ADDRESS'];
            $studentEmailArr[$keyStudent]['defaultFlag'] = $valueStudent['DEFAULT_FLAG'];
            $studentEmailArr[$keyStudent]['deleteFlag'] = $valueStudent['DELETE_FLAG'];
        }
        $result = array("status" => SCS, "data" => array_values($studentEmailArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getStudentMultiPhone($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    $sqlStudentMultiEmailData = "SELECT PK_PHONE_ID,FK_STUD_ID,COUNTRY_CODE,PHONE_NO,DEFAULT_FLAG,DELETE_FLAG FROM " . MULTIPHONEMASTER . " SM  WHERE DELETE_FLAG =0 AND FK_STUD_ID ='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR STUDENT Multi Email DATA.
    $resStudentData = fetch_rec_query($sqlStudentMultiEmailData); // RESULT FOR STUDENT Multi Email DATA.
    if (count($resStudentData) > 0) {
        $studentPhoneArr = Array();
        foreach ($resStudentData as $keyStudent => $valueStudent) {
            $studentPhoneArr[$keyStudent]['phoneId'] = $valueStudent['PK_PHONE_ID'];
            $studentPhoneArr[$keyStudent]['studId'] = $valueStudent['FK_STUD_ID'];
            $studentPhoneArr[$keyStudent]['countryCode'] = $valueStudent['COUNTRY_CODE'];
            $studentPhoneArr[$keyStudent]['phoneNo'] = $valueStudent['PHONE_NO'];
            $studentPhoneArr[$keyStudent]['defaultFlag'] = $valueStudent['DEFAULT_FLAG'];
            $studentPhoneArr[$keyStudent]['deleteFlag'] = $valueStudent['DELETE_FLAG'];
        }
        $result = array("status" => SCS, "data" => array_values($studentPhoneArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.18.7.2018 get all multi address data.student wise.
function getMultiAddressData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    $tempAddressArr = array();
    $sqlAddress = "SELECT MA.PK_ADDRESS_ID,MA.PINCODE,MA.ADDRESS,MA.CITYID,MA.STATEID,MA.COUNTRYID,CO.COUNTRY_NAME,CT.CITY_NAME,CT.STATE_NAME,MA.DELETE_FLAG,MA.DEFAULT_FLAG,MA.CATCHMENT_AREA FROM " . MULTIADDRESSMASTER . " MA LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = MA.COUNTRYID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=MA.CITYID  WHERE FK_STUD_ID = '" . $studentId . "' AND MA.DELETE_FLAG= '0' AND MA.FK_CLIENT_ID='" . $clientId . "' AND MA.FK_ORG_ID='" . $orgId . "' ";

    $resultAddress = fetch_rec_query($sqlAddress);

    if (count($resultAddress) > 0) {

        foreach ($resultAddress as $keyAddress => $valueAddres) {
            $tempAddressArr[$keyAddress]['addressId'] = $valueAddres['PK_ADDRESS_ID'];
            $tempAddressArr[$keyAddress]['pincode'] = $valueAddres['PINCODE'];
            $tempAddressArr[$keyAddress]['address'] = $valueAddres['ADDRESS'];
            $tempAddressArr[$keyAddress]['countryid'] = $valueAddres['COUNTRYID'];
            $tempAddressArr[$keyAddress]['countryname'] = $valueAddres['COUNTRY_NAME'];
            $tempAddressArr[$keyAddress]['stateid'] = $valueAddres['STATEID'];
            $tempAddressArr[$keyAddress]['statename'] = $valueAddres['STATE_NAME'];
            $tempAddressArr[$keyAddress]['cityid'] = $valueAddres['CITYID'];
            $tempAddressArr[$keyAddress]['cityname'] = $valueAddres['CITY_NAME'];
            $tempAddressArr[$keyAddress]['catchArea'] = $valueAddres['CATCHMENT_AREA'];
            $tempAddressArr[$keyAddress]['defaultFlag'] = $valueAddres['DEFAULT_FLAG'];
            $tempAddressArr[$keyAddress]['deleteFlag'] = $valueAddres['DELETE_FLAG'];
        }

        $result = array("status" => SCS, "data" => array_values($tempAddressArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.18.7.2018


function studentSearch($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $keyword = isset($postData['postData']['keyword']) ? addslashes(trim($postData['postData']['keyword'])) : "";

    $studentArr = array();

    if (!empty($clientId) && !empty($orgId) && !empty($keyword) && strlen($keyword) >= 2) {
        $whereStudentCond = "";
        if (!empty($keyword)) {
            $whereStudentCond = " AND (MEMBERSHIP_NUMBER LIKE '%" . $keyword . "%' OR FIRST_NAME LIKE'%" . $keyword . "%' OR LAST_NAME LIKE '%" . $keyword . "%' OR CONCAT(FIRST_NAME,' ',LAST_NAME) LIKE '%" . $keyword . "%')";
        }


        $sqlStudentData = "  SELECT PK_STUD_ID,MEMBERSHIP_NUMBER,CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUD_NAME,(SELECT ROOM_NAME FROM " . ALLOTMASTER . " WHERE FK_STUDENT_ID = PK_STUD_ID AND DELETE_FLAG ='0' ORDER BY PK_ALLOTMENT_ID DESC LIMIT 0,1) AS ROOM_NAME FROM " . STUDENTMASTER . "  WHERE DELETE_FLAG =0 " . $whereStudentCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND IS_ACTIVE ='1'"; // QUERY FOR CUSTOMER DATA.
        // OR CONCAT(FIRST_NAME,' ',LAST_NAME) LIKE '%satish karena%'
        $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR CUSTOMER DATA.
        if (count($resStudentData) > 0) {
            foreach ($resStudentData as $keyStudent => $valueStudent) { // CUSTOMER DATA LOOP
                $studentArr[$keyStudent]['studId'] = $valueStudent['PK_STUD_ID'];
                $studentArr[$keyStudent]['membershipNumber'] = $valueStudent['MEMBERSHIP_NUMBER'];
                $studentArr[$keyStudent]['studName'] = $valueStudent['STUD_NAME'];
                $studentArr[$keyStudent]['roomName'] = $valueStudent['ROOM_NAME'];
            }
            $result = array("status" => SCS, "data" => $studentArr);
            http_response_code(200);
        } else {

            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//ADD BY DEVIKA.31.7.2018.
function updateStudentStatus($postData) {
    //POST DATA//
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";



    if (!empty($clientId) && !empty($orgId) && !empty($studentId)) {
        $updateStudentStatusArray = Array(); //ATTACHMENT ARRAY UPDATE
        $updateStudentStatusArray['IS_ACTIVE'] = $status; //0 : Inactive , 1 : Active,2 : Termination, 3 : Suspensio
        $updateStudentStatusArray['CHANGED_BY'] = $userId;
        $updateStudentStatusArray['CHANGED_BY_DATE'] = time();

        $whereStudentStatusUpdate = "DELETE_FLAG ='0' AND PK_STUD_ID ='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'";
        $updateStatus = update_rec(STUDENTMASTER, $updateStudentStatusArray, $whereStudentStatusUpdate); // UPDATE DELETE_FLAG =1.
        if (!$updateStatus) {
            $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
            http_response_code(400);
        } else {
            $result = array("status" => SCS);
            http_response_code(200);
        }
    }
    return $result;
}

//END BY DEVIKA.31.7.2018.
function studentResetPassword($postData) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $password = isset($postData['postData']['password']) ? addslashes(trim($postData['postData']['password'])) : "";

    $studentArr = array();

    if (!empty($clientId) && !empty($orgId) && !empty($studentId) && !empty($type) && !empty($password)) {

        $updArr = array();
        if ($type == 'student') {
            $updArr['PASSWORD'] = password_hash(addslashes(trim(md5($password))), PASSWORD_DEFAULT);
            $updArr['SETPASSWORD_FLAG'] = "0"; // FLAG SET FOR CHANGE PASSWORD.
        } else {
            $updArr['PASSWORD'] = password_hash(addslashes(trim(md5($password))), PASSWORD_DEFAULT);
        }


        $updArr['CHANGED_BY'] = $userId;
        $updArr['CHANGED_BY_DATE'] = time();


        if ($type == 'student') {
            $where = "PK_STUD_ID='" . $studentId . "'";
            $updateData = update_rec(STUDENTMASTER, $updArr, $where);
        } else {
            $where = "FK_STUD_ID='" . $studentId . "'";
            $updateData = update_rec(PARENTMASTER, $updArr, $where);
        }

        if ($updateData) {
            if ($type == 'student') {
                $sqlStudentData = "  SELECT PK_STUD_ID,MEMBERSHIP_NUMBER,CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUD_NAME,EMAIL FROM " . STUDENTMASTER . "  WHERE DELETE_FLAG =0 AND PK_STUD_ID='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR STUDENt DATA.
            } else {
                $sqlStudentData = "  SELECT FK_STUD_ID,MEMBERSHIP_NUMBER,PARENT_NAME,EMAIL FROM " . PARENTMASTER . "  WHERE DELETE_FLAG =0 AND FK_STUD_ID='" . $studentId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "'"; // QUERY FOR STUDENt DATA. 
            }

            $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR CUSTOMER DATA.
            if (count($resStudentData) > 0) {
                if ($type == 'student') {
                    $receiverName = $resStudentData[0]['STUD_NAME'];
                    $receiverEmail = $resStudentData[0]['EMAIL'];
                } else {
                    $receiverName = $resStudentData[0]['PARENT_NAME'];
                    $receiverEmail = $resStudentData[0]['EMAIL'];
                }
                // START GETTING MAIL CONFIG 
                $mailConfig = "SELECT * FROM " . MAILSMSCONFIG . " WHERE FK_CLIENT_ID = '" . $clientId . "' ";

                $resultConfig = fetch_rec_query($mailConfig);

                if (count($resultConfig) > 0) {

                    $sendgrid_username = "arunnagpal";
                    $sendgrid_password = "devil1502";

                    $sendGrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));

//            print_r($sendGrid);            exit();
                    $fromAddress = $resultConfig[0]['MAILCONFIG_USERNAME'];
                    $sfName = $resultConfig[0]['SENDERFNAME'];
                    $slname = $resultConfig[0]['SENDERLNAME'];
                    $fromName = $sfName . "" . $slname;
                    //  $reciverAddress = $resultCheckName[0]['EMAIL'];
//print_r($reciverAddress); die;
                    //   $reciverName = $resultCheckName[0]['FIRST_NAME'] . $resultCheckName[0]['LAST_NAME'];

                    $subject = "Reset Password : Link to activate Account.";
                    $messageBody = "<div style = 'height:100px'>"
                            . "<div style = 'padding:10px'>Hi " . $receiverName . "</div>"
                            . "<div style = 'padding:10px'>Your New Password  : " . $password . "</div>";


                    $email = new SendGrid\Email();
                    $email->addTo($receiverEmail);
                    $email->addToName($receiverName);
                    $email->setFrom($fromAddress);
                    $email->setFromName($fromName);
                    $email->setSubject($subject);
                    $email->setHtml($messageBody);
//print_r($email); die;
                    $response = $sendGrid->send($email);
                    $responseMail = $response->getBody();

                    if ($responseMail['message'] == "success") {
                        $result = array("status" => SCS);
                        http_response_code(200);
                    } else {
                        $result = array("status" => SENDEMAILERROR);
                        http_response_code(400);
                    }
                }
            } else {

                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } else {
            $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function getStudentInvoiceData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";

    if (!empty($clientId) && !empty($orgId)) {
        $whereCond = "";
        if (!empty($invoiceId)) {
            $whereCond = " AND IM.C_INVOICE_ID ='" . $invoiceId . "'";
        }

        $sqlPaymentInvoice = "SELECT IF(PAID_AMOUNT > 0,SUM(PAID_AMOUNT),0) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $invoiceId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; //QUERY FOR INVOICE PAYMENT DATA 
        $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);

        $totalPayment = 0;
        if (count($resPaymentInvoice) > 0) {
            $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
        }

        $sqlstudentInvoice = "SELECT IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.INVOICE_NAME,IM.PERIOD_FROM,IM.PERIOD_TO,IM.FK_STUDENT_ID,SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.FK_CITY_ID,SM.FK_STATE_ID,SM.FK_COUNTRY_ID,CT.CITY_NAME,CT.STATE_NAME,CO.COUNTRY_NAME,IM.INVOICE_TYPE  FROM " . INVOICEMASTER . " IM LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID=IM.FK_STUDENT_ID LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = SM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=SM.FK_CITY_ID  WHERE IM.DELETE_FLAG=0 AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' " . $whereCond . "  ";
        $resultStudentInvoice = fetch_rec_query($sqlstudentInvoice);
        $studentInvoiceArr = array();
        if (count($resultStudentInvoice) > 0) {
            foreach ($resultStudentInvoice as $keyStudentInvoice => $valueStudentInvoice) {
                $studentInvoiceArr[$keyStudentInvoice]['cInvoiceId'] = $valueStudentInvoice['C_INVOICE_ID'];
                $studentInvoiceArr[$keyStudentInvoice]['invoiceDate'] = $valueStudentInvoice['INVOICE_DATE'];
                $studentInvoiceArr[$keyStudentInvoice]['invoiceName'] = $valueStudentInvoice['INVOICE_NAME'];
                $studentInvoiceArr[$keyStudentInvoice]['periodFrom'] = $valueStudentInvoice['PERIOD_FROM'];
                $studentInvoiceArr[$keyStudentInvoice]['periodFrom'] = $valueStudentInvoice['PERIOD_FROM'];
                $studentInvoiceArr[$keyStudentInvoice]['periodTo'] = $valueStudentInvoice['PERIOD_TO'];
                $studentInvoiceArr[$keyStudentInvoice]['memberShipNo'] = $valueStudentInvoice['MEMBERSHIP_NUMBER'];
                $studentInvoiceArr[$keyStudentInvoice]['fullName'] = $valueStudentInvoice['FIRST_NAME'] . " " . $valueStudentInvoice['LAST_NAME'];
                $studentInvoiceArr[$keyStudentInvoice]['address'] = $valueStudentInvoice['ADDRESS_LINE1'] . ", " . $valueStudentInvoice['ADDRESS_LINE2'] . ", " . $valueStudentInvoice['ADDRESS_LINE3'];
                $studentInvoiceArr[$keyStudentInvoice]['pinCode'] = $valueStudentInvoice['PINCODE'];
                $studentInvoiceArr[$keyStudentInvoice]['cityName'] = $valueStudentInvoice['CITY_NAME'];
                $studentInvoiceArr[$keyStudentInvoice]['stateName'] = $valueStudentInvoice['STATE_NAME'];
                $studentInvoiceArr[$keyStudentInvoice]['countryName'] = $valueStudentInvoice['COUNTRY_NAME'];
                $studentInvoiceArr[$keyStudentInvoice]['totalPayment'] = $totalPayment;
                $studentInvoiceArr[$keyStudentInvoice]['invoiceType'] = $valueStudentInvoice['INVOICE_TYPE']; // ADDED BY KAVITA PATEL ON 23-08-2018
                //COMMENT BY DEVIKA.18.8.2018.NOT PROPER DATA IN CENTRALISED INVOICE.
//                $sqlChargeData = "SELECT ITEM_NAME,TAX_CGST,TAX_SGST,HSN_CODE,SUM(QTY_OF_SESSION) AS QTY_OF_SESSION,ITEM_BASE_PRICE,SUM(ITEM_TOTAL_PRICE) AS ITEM_TOTAL_PRICE,SUM(TOTAL_TAX) AS TOTAL_TAX,INVOICE_ID FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $valueStudentInvoice['C_INVOICE_ID'] . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1' AND IS_TRANSFER='0' GROUP BY FK_ITEM_ID";
//                
                //ADD BY DEVIKA.18.8.2018.
                $sqlChargeData = "SELECT ITEM_NAME,TAX_CGST,TAX_SGST,TAX_IGST,HSN_CODE,SUM(QTY_OF_SESSION) AS QTY_OF_SESSION,ITEM_BASE_PRICE,SUM(ITEM_TOTAL_PRICE) AS ITEM_TOTAL_PRICE,SUM(TOTAL_TAX) AS TOTAL_TAX,INVOICE_ID FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $valueStudentInvoice['C_INVOICE_ID'] . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1' GROUP BY FK_ITEM_ID";
                $resultStudentChargeData = fetch_rec_query($sqlChargeData);
                $itemDataArr = array();
                if (count($resultStudentChargeData) > 0) {
                    foreach ($resultStudentChargeData as $keyStudentChargeData => $valueStudentChargeData) {
                        $itemDataArr[$keyStudentChargeData]['itemName'] = $valueStudentChargeData['ITEM_NAME'];
                        $itemDataArr[$keyStudentChargeData]['cgst'] = $valueStudentChargeData['TAX_CGST'];
                        $itemDataArr[$keyStudentChargeData]['sgst'] = $valueStudentChargeData['TAX_SGST'];
                        $itemDataArr[$keyStudentChargeData]['igst'] = $valueStudentChargeData['TAX_IGST'];
                        $itemDataArr[$keyStudentChargeData]['hsnCode'] = $valueStudentChargeData['HSN_CODE'];
                        $itemDataArr[$keyStudentChargeData]['qty'] = $valueStudentChargeData['QTY_OF_SESSION'];
                        $itemDataArr[$keyStudentChargeData]['itemPrice'] = $valueStudentChargeData['ITEM_BASE_PRICE'];
                        $itemDataArr[$keyStudentChargeData]['itemTotalTax'] = $valueStudentChargeData['TOTAL_TAX'];
                        $itemDataArr[$keyStudentChargeData]['itemTotalPrice'] = $valueStudentChargeData['ITEM_TOTAL_PRICE'];
                        $itemDataArr[$keyStudentChargeData]['invoiceId'] = $valueStudentChargeData['INVOICE_ID'];
                    }
                    $studentInvoiceArr[$keyStudentInvoice]['itemData'] = array_values($itemDataArr);
                } else {
                    $studentInvoiceArr[$keyStudentInvoice]['itemData'] = NORECORDS;
                }
            }
            $result = array("status" => SCS, "data" => array_values($studentInvoiceArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//END BY DEVIKA.31.7.2018.
//ADD BY DEVIKA.3.8.2018.
function attachmentStudentComplainUpload($postData, $postFile) {
    // POST DATA
    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $studId = isset($postdata['studentId']) ? addslashes(trim($postdata['studentId'])) : ""; // STUD ID
    $complainId = isset($postdata['complainId']) ? addslashes(trim($postdata['complainId'])) : ""; // COMPLAIN ID
    $type = isset($postdata['type']) ? addslashes(trim($postdata['type'])) : ""; // TYPE
    $title = isset($postdata['title']) ? addslashes(trim($postdata['title'])) : ""; // TITLE

    $attachmentName = $postFile; // ATTACH NAME 
    // START UPLOADING DATA INTO DATA BASE
    $path = STUDENT_ATTACH_PATH;
    $uploadAttachment = uploadImage($attachmentName, $path); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
    //print_r($uploadAttachment); exit; 
    if ($uploadAttachment['status'] == SCS) {

        // INSERT DATA TO DATABASE FOR FURTHER REF.
        $insertAttach['FK_CLIENT_ID'] = $clientId; //COMPLAIN
        $insertAttach['FK_ORG_ID'] = $orgId;
        $insertAttach['ATTACH_CAT'] = $type; //COMPLAIN
        $insertAttach['REF_ID'] = $complainId;
        $insertAttach['ATTACHFILE_LINK'] = $uploadAttachment['fileName'];
        $insertAttach['TITLE'] = $title;
        $insertAttach['CREATED_BY'] = $userId;
        $insertAttach['CREATED_BY_DATE'] = time();

        $insertAttach['DELETE_FLAG'] = 0;
        //print_r($insertAttach); die;
        $insertAttach = insert_rec(ATTACHSTUDENTMST, $insertAttach); // INSERTING DATA IN ATTACH_STUDENT_MASTER TABLE


        if ($insertAttach) { // IF SUCCESS THEN GIVE SUCCESS RESPONSE.
            $finalJson = array("attachmentId" => $insertAttach['lastInsertedId'], "fileName" => basename($uploadAttachment['fileName']), "fileLink" => $uploadAttachment['fileName'], 'title' => $title);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        }
    } else {
        $result = $uploadAttachment;
        http_response_code(400);
    }

    return $result; // RETURN DATA TO PL
}

//END BY DEVIKA.3.8.2018.

function listSuppesionAttachment($postData) {
    // POST DATA
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";

    $whereTypeCond = "";
    if (!empty($type)) {
        $whereTypeCond = " AND ATTACH_CAT='" . $type . "'";
    }

    if (!empty($suspensionId)) {
        $sqlStudentAttachment = "SELECT PK_ATTACH_ID,ATTACH_CAT,REF_ID,ATTACHFILE_LINK,TITLE FROM " . ATTACHSTUDENTMST . " WHERE REF_ID='" . $suspensionId . "' " . $whereTypeCond . " AND DELETE_FLAG ='0'"; // QUERY FOR LISTING STUDENT ATTACHMENT. 
        $resStudentAttachment = fetch_rec_query($sqlStudentAttachment); // RESULT FOR LISTING STUDENT ATTACHMENT. 
        if (count($resStudentAttachment) > 0) {
            $attachmentArr = Array();
            foreach ($resStudentAttachment as $keyAttachment => $valueAttachment) {
                $attachmentArr[$keyAttachment]['attachmentId'] = $valueAttachment['PK_ATTACH_ID'];
                $attachmentArr[$keyAttachment]['attachType'] = $valueAttachment['ATTACH_CAT'];
                $attachmentArr[$keyAttachment]['refId'] = $valueAttachment['REF_ID'];
                $attachmentArr[$keyAttachment]['attachmentLink'] = $valueAttachment['ATTACHFILE_LINK'];
                $attachmentArr[$keyAttachment]['title'] = $valueAttachment['TITLE'];
            }
            $result = array("status" => SCS, "data" => $attachmentArr);
            http_response_code(400);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function deleteSuppesionAttachment($postData) {
    //POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $attachmentId = isset($postData['postData']['attachmentId']) ? addslashes(trim($postData['postData']['attachmentId'])) : "";
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $attachName = isset($postData['postData']['attachName']) ? addslashes(trim($postData['postData']['attachName'])) : "";
    $attachPath = isset($postData['postData']['attachPath']) ? addslashes(trim($postData['postData']['attachPath'])) : "";


    if (!empty($clientId) && !empty($orgId) && !empty($attachmentId)) {
        $updateAttachmentArray = Array(); //ATTACHMENT ARRAY UPDATE
        $updateAttachmentArray['DELETE_FLAG'] = 1;
        $updateAttachmentArray['CHANGED_BY'] = $userId;
        $updateAttachmentArray['CHANGED_BY_DATE'] = time();

        $whereAttachmentUpdate = "DELETE_FLAG ='0' AND PK_ATTACH_ID ='" . $attachmentId . "' AND REF_ID='" . $suspensionId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
        $updateAttachment = update_rec(ATTACHSTUDENTMST, $updateAttachmentArray, $whereAttachmentUpdate); // UPDATE DELETE_FLAG =1 IN ATTACH_STUDENT_MASTER TABLE.
        if ($updateAttachment) {

            if (!empty($attachName)) {
                unlink(APP_SERVER_ROOT . "/attachment/student_attachment/" . $attachName); //DELETE ATTACHMENT FILE FROM FOLDER.
            }
            $requestData = $studentAttachmentData = Array();
            $requestData['postData']['suspensionId'] = $suspensionId;
            $requestData['postData']['type'] = $type;
            $studentAttachmentData = listSuppesionAttachment($requestData); // GET ALL STUDENT ATTACHMENT DATA.
            $studentAttachmentAllData = ($studentAttachmentData['status'] == SCS) ? $studentAttachmentData['data'] : $studentAttachmentData['status'];

            $result = array("status" => SCS, "data" => $studentAttachmentAllData);
            http_response_code(200);
        } else {
            $result = array("status" => UPDATEFAIL . " " . ATTACHSTUDENTMST);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function renewStudentAllotment($postData) {




    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $roomId = isset($postData['postData']['roomId']) ? addslashes(trim($postData['postData']['roomId'])) : "";
    $bedNo = isset($postData['postData']['bedNo']) ? addslashes(trim($postData['postData']['bedNo'])) : "";
    $assetIssued = isset($postData['postData']['assetIssued']) ? addslashes(trim($postData['postData']['assetIssued'])) : "";
    $roomName = isset($postData['postData']['roomName']) ? addslashes(trim($postData['postData']['roomName'])) : "";
    $tenancyStartDate = !empty($postData['postData']['tenancyStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['tenancyStartDate'])))) : "";
    $tenancyEndDate = !empty($postData['postData']['tenancyEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['tenancyEndDate'])))) : "";
    $foodPreference = !empty($postData['postData']['foodPreference']) ? addslashes(trim($postData['postData']['foodPreference'])) : "";
    $tenancyRent = !empty($postData['postData']['tenancyRent']) ? addslashes(trim($postData['postData']['tenancyRent'])) : "";
    $acdemicStartYear = !empty($postData['postData']['acdemicStartYear']) ? addslashes(trim($postData['postData']['acdemicStartYear'])) : "";
    $acdemicEndYear = !empty($postData['postData']['acdemicEndYear']) ? addslashes(trim($postData['postData']['acdemicEndYear'])) : "";
    $semester = !empty($postData['postData']['semester']) ? addslashes(trim($postData['postData']['semester'])) : "";
    $facilityAllocation = !empty($postData['postData']['facilityAllocation']) ? addslashes(trim($postData['postData']['facilityAllocation'])) : "";




    if ($clientId != "" && $orgId != "" && $itemId != "" && $studentId != "" && $roomId != "" && $bedNo != "" && $assetIssued != "" && $tenancyStartDate != "" && $tenancyEndDate != "" && $tenancyRent != "") { // CHECK REQURIED FIELD CONDITION
        if (is_numeric($tenancyRent)) {

            $facilityAllocationArr = explode(",", $facilityAllocation);

            if (!empty($facilityAllocationArr)) {
                $facilityAllocationIds = "";
                foreach ($facilityAllocationArr as $keyAllocation => $valueAllocation) {
                    if ($valueAllocation != "") {
                        $allocationStrArr = explode(":", $valueAllocation);
                        $facilityAllocationIds .= ($allocationStrArr[0]) ? $allocationStrArr[0] . "," : "";
                    }
                }
                $facilityAllocationIds = trim($facilityAllocationIds, ",");
            } else {
                $facilityAllocationIds = "";
            }

            // INSERT STUDENT ALLOTMENT ARRAY.
            $insertStudArray = Array(); //STUDENT ALLOTMENT ARRAY CREATE
            $insertStudArray['FK_CLIENT_ID'] = $clientId;
            $insertStudArray['FK_ORG_ID'] = $orgId;
            $insertStudArray['FK_ITEM_ID'] = $itemId;
            $insertStudArray['FK_STUDENT_ID'] = $studentId;
            $insertStudArray['FK_ROOM_ID'] = $roomId;
            $insertStudArray['BED_NO'] = $bedNo;
            $insertStudArray['ASSET_ISSUED'] = $assetIssued;
            $insertStudArray['TENANCY_START_DATE'] = $tenancyStartDate;
            $insertStudArray['TENANCY_END_DATE'] = $tenancyEndDate;
            $insertStudArray['ROOM_NAME'] = $roomName;
            $insertStudArray['FOOD_PREFERENCE'] = $foodPreference;
            $insertStudArray['TENANCY_RENT'] = $tenancyRent;
            $insertStudArray['FACILITY_ALLOCATION'] = $facilityAllocationIds;
            $insertStudArray['CREATED_BY'] = $userId;
            $insertStudArray['CREATED_BY_DATE'] = time();

            // echo "<pre>"; print_r($insertStudArray); exit;
            $insertStudent = insert_rec(ALLOTMASTER, $insertStudArray); // INSERT RECORD IN ALLOTMENT_MASTER TABLE.
            $allotmentId = isset($insertStudent['lastInsertedId']) ? $insertStudent['lastInsertedId'] : "";
            if ($allotmentId > 0) {


                unset($insertStudArray['ASSET_ISSUED']);
                unset($insertStudArray['ASSET_ISSUED']);
                unset($insertStudArray['TENANCY_START_DATE']);
                unset($insertStudArray['TENANCY_END_DATE']);
                unset($insertStudArray['FOOD_PREFERENCE']);
                unset($insertStudArray['TENANCY_DEPOSIT']);
                unset($insertStudArray['TENANCY_RENT']);
                unset($insertStudArray['FACILITY_ALLOCATION']);
                /* End :Unset Extra key in StudArray */
                $insertStudArray['DATE'] = date("Y-m-d");
                $insertStudArray['CREATED_BY'] = $userId;
                $insertStudArray['CREATED_BY_DATE'] = time();
                $insertStudent = insert_rec(ALLOTTRAIL, $insertStudArray); // INSERT RECORD IN ALLOTMENT_TRAIL TABLE.

                /* START UPDATE IN STUDENT MASTER TABLE. */
                $updateStudArr = Array();
                $updateStudArr['ACDEMIC_START_DATE'] = $acdemicStartYear;
                $updateStudArr['ACDEMIC_END_DATE'] = $acdemicEndYear;
                $updateStudArr['SEMESTER'] = $semester;
                $updateStudArr['ENROLLMENT_TYPE'] = 'RENEW';
                $updateStudArr['CHANGED_BY'] = $userId;
                $updateStudArr['CHANGED_BY_DATE'] = time();


                $whereStudCond = "PK_STUD_ID='" . $studentId . "'";
                $updateStudData = update_rec(STUDENTMASTER, $updateStudArr, $whereStudCond);
                /* END UPDATE IN STUDENT MASTER TABLE. */

                $checkStudentData = "SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS studName FROM " . STUDENTMASTER . " WHERE DELETE_FLAG='0' AND PK_STUD_ID='" . $studentId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
                $resStudentData = fetch_rec_query($checkStudentData);
                $studName = "";
                if (count($resStudentData) > 0) {
                    $studName = $resStudentData[0]['studName'];
                }

                $itemIdRent = "-4";
                $itemNameRent = "Allotment Rent";
                $sqlItemDataDepo = "SELECT PK_ITEM_ID,ITEM_NAME FROM " . ITEMMASTER . " WHERE ITEM_NAME ='Rent'";
                $resItemDataDepo = fetch_rec_query($sqlItemDataDepo);

                if (count($resItemDataDepo) > 0) {
                    $itemIdDeposite = $resItemDataDepo[0]['PK_ITEM_ID'];
                    $itemNameDeposite = $resItemDataDepo[0]['ITEM_NAME'];
                }

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['invoiceStartDate'] = $tenancyStartDate;
                $requestData['postData']['invoiceEndDate'] = $tenancyEndDate;
                $requestData['postData']['chargeStartDate'] = date("Y-m-d");
                $requestData['postData']['chargeEndDate'] = date("Y-m-d");
                $requestData['postData']['invoiceName'] = "Allotment Renewals Rent";
                $requestData['postData']['itemId'] = $itemIdRent;
                $requestData['postData']['studentId'] = $studentId;
                $requestData['postData']['studentName'] = $studName;
                $requestData['postData']['itemName'] = $itemNameRent;
                $requestData['postData']['amount'] = $tenancyRent;
                $requestData['postData']['invoiceType'] = 'RENEWALS';
                $requestData['postData']['chargeType'] = 'RENEWALS';

                $chargeMasterData = addChargePostingData($requestData); // INSERT DATA ALL CHARGE MASTER DATA.
                if ($chargeMasterData['status'] == SCS) {
                    $finalInvoiceIds = array();
                    $requestDataInvoice = Array();
                    $requestDataInvoice['postData']['clientId'] = $clientId;
                    $requestDataInvoice['postData']['orgId'] = $orgId;
                    $requestDataInvoice['postData']['userId'] = $userId;
                    $requestDataInvoice['postData']['invoiceStartDate'] = $tenancyStartDate;
                    $requestDataInvoice['postData']['invoiceEndDate'] = $tenancyEndDate;
                    $requestDataInvoice['postData']['chargeStartDate'] = date("Y-m-d");
                    ;
                    $requestDataInvoice['postData']['chargeEndDate'] = date("Y-m-d");
                    $requestDataInvoice['postData']['invoiceName'] = "Allotment Renewals Rent";
                    $requestDataInvoice['postData']['studentId'] = $studentId;
                    $requestDataInvoice['postData']['invoiceType'] = 'RENEWALS';
                    $requestDataInvoice['postData']['chargeType'] = 'RENEWALS';

                    $invoiceMasterData = createInvoice($requestDataInvoice); // CREATE INVOICE FOR STUDENT RENEWALS PROCESS.
                    if ($invoiceMasterData['status'] == SCS) {
                        $finalInvoiceIds[] = $invoiceMasterData['data'];

                        $insertAlertStagingArr = Array(); //ALERT STAGING DATA INSERT
                        $insertAlertStagingArr['FK_CLIENT_ID'] = $clientId;
                        $insertAlertStagingArr['FK_ORG_ID'] = $orgId;
                        $insertAlertStagingArr['APP_NAME'] = 'indecampus';
                        $insertAlertStagingArr['DEVICE_TYPE'] = 'UBANTU';
                        $insertAlertStagingArr['ALERT_SUB_ID'] = $userId;
                        $insertAlertStagingArr['MESSAGE_TYPE'] = 'ALERT';
                        $insertAlertStagingArr['ACTION_FLAG'] = '0';
                        $insertAlertStagingArr['REF_ID'] = $studentId;
                        //$insertAlertStagingArr['MESSAGE_TEXT'] = ($type == 2) ? ASSIGNCOMPLAINT : ASSIGNREQUEST; // COMMENT BY KAVITA PATEL ON 17-08-2018
                        // ADDED BY KAVITA PATEL ON 17-08-2018 START

                        $insertAlertStagingArr['MESSAGE_TEXT'] = "(Renew)Create Student Renewal ENrollment.";

                        // ADDED BY KAVITA PATEL ON 17-08-2018 END
                        $insertAlertStagingArr['STATUS'] = '0';
                        $insertAlertStagingArr['LOGIN_SESSION_ID'] = $userId;
                        $insertAlertStagingArr['CREATED_BY'] = $userId;
                        $insertAlertStagingArr['CREATED_BY_DATE'] = time();
                        $insertAlertStagingData = insert_rec(ALERTSTAGINGDATA, $insertAlertStagingArr);

                        // START SENDING EMAIL INTO SINGLE MAIL OF RENT AND DEPOSITE
                        //  if (!empty($finalInvoiceIds)) {
                        $requestData = $studentData = Array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['userId'] = $userId;
                        $requestData['postData']['studentId'] = $studentId;
                        $requestData['postData']['invoiceId'] = $finalInvoiceIds;

                        $invoiceSendStatus = sendGeneralInvoiceMail($requestData);  // GENERAL MAIL SENT CODE   
                        if ($invoiceSendStatus['status'] == SCS) {
                            $requestData = $studentData = Array();
                            $requestData['postData']['clientId'] = $clientId;
                            $requestData['postData']['orgId'] = $orgId;
                            $requestData['postData']['studentId'] = $studentId;
                            $studentData = getStudentAllotment($requestData); // GET ALL STUDENT ALLOTMENT DATA.
                            $studentAllData = ($studentData['status'] == SCS) ? $studentData['data'] : $studentData['status'];

                            $result = array("status" => SCS, "data" => $studentAllData);
                            http_response_code(200);
                        } else {
                            $result = array("status" => $invoiceSendStatus['status']);
                            http_response_code(400);
                        }
                    } else {
                        $result = array("status" => NOTGENERATEINVOICE);
                        http_response_code(400);
                    }
                } else {
                    $result = array("status" => $chargeMasterData['status']);
                    http_response_code(400);
                }
            } else {
                $result = array("status" => INSERTFAIL . " " . ALLOTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => RENTISONLYNUMERIC);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.7.8.2018
//ADD BY DEVIKA.7.8.2018.
function attachmentStudentSuspensionUpload($postData, $postFile) {
// POST DATA
    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $suspensionId = isset($postdata['suspensionId']) ? addslashes(trim($postdata['suspensionId'])) : ""; // COMPLAIN ID
    $type = isset($postdata['type']) ? addslashes(trim($postdata['type'])) : ""; // TYPE
    $title = isset($postdata['title']) ? addslashes(trim($postdata['title'])) : ""; // TITLE

    $attachmentName = $postFile; // ATTACH NAME 
// START UPLOADING DATA INTO DATA BASE
    $path = STUDENT_ATTACH_PATH;
    $uploadAttachment = uploadImage($attachmentName, $path); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
//print_r($uploadAttachment); exit; 
    if ($uploadAttachment['status'] == SCS) {

// INSERT DATA TO DATABASE FOR FURTHER REF.
        $insertAttach['FK_CLIENT_ID'] = $clientId;
        $insertAttach['FK_ORG_ID'] = $orgId;
        $insertAttach['ATTACH_CAT'] = $type; //SUSPENCED
        $insertAttach['REF_ID'] = $suspensionId;
        $insertAttach['ATTACHFILE_LINK'] = $uploadAttachment['fileName'];
        $insertAttach['TITLE'] = $title;
        $insertAttach['CREATED_BY'] = $userId;
        $insertAttach['CREATED_BY_DATE'] = time();

        $insertAttach['DELETE_FLAG'] = 0;
//print_r($insertAttach); die;
        $insertAttach = insert_rec(ATTACHSTUDENTMST, $insertAttach); // INSERTING DATA IN ATTACH_STUDENT_MASTER TABLE


        if ($insertAttach) { // IF SUCCESS THEN GIVE SUCCESS RESPONSE.
            $finalJson = array("attachmentId" => $insertAttach['lastInsertedId'], "fileName" => basename($uploadAttachment['fileName']), "fileLink" => $uploadAttachment['fileName'], 'title' => $title);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        }
    } else {
        $result = $uploadAttachment;
        http_response_code(400);
    }

    return $result; // RETURN DATA TO PL
}

//END BY DEVIKA.7.8.2018.

function addEditStudentSuspension($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : ""; // 1 = Suspenssion ,2 =Termination.
    $terminationDate = isset($postData['postData']['terminationDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['terminationDate'])))) : ""; // Termination Date
    $recommendedId = isset($postData['postData']['recommendedId']) ? addslashes(trim($postData['postData']['recommendedId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $reason = isset($postData['postData']['reason']) ? addslashes(trim($postData['postData']['reason'])) : "";
    $suspenStartDate = !empty($postData['postData']['suspenStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['suspenStartDate'])))) : "";
    $suspenEndDate = !empty($postData['postData']['suspenEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['suspenEndDate'])))) : "";
    $roomServiceAllow = isset($postData['postData']['roomServiceAllow']) ? addslashes(trim($postData['postData']['roomServiceAllow'])) : "";
    $fbAllow = isset($postData['postData']['fbAllow']) ? addslashes(trim($postData['postData']['fbAllow'])) : "";
    $laundryAllow = isset($postData['postData']['laundryAllow']) ? addslashes(trim($postData['postData']['laundryAllow'])) : "";
    $eventBooking = isset($postData['postData']['eventBookingAllow']) ? addslashes(trim($postData['postData']['eventBookingAllow'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($userId) && !empty($studentId) && !empty($type)) {  // CHECK REQURIED FIELD CONDITION
// ITEM INSERT ARRAY.
        $msg = "";
        $insertSuspenArray = Array(); //ITEM ARRAY CREATE
        $insertSuspenArray['FK_CLIENT_ID'] = $clientId;
        $insertSuspenArray['FK_ORG_ID'] = $orgId;
        $insertSuspenArray['FK_STUDENT_ID'] = $studentId;
        $insertSuspenArray['TYPE'] = $type;
        $insertSuspenArray['RECOMMENDED_BY'] = $recommendedId;
        $insertSuspenArray['REASON'] = $reason;

        if ($type == 1) { // SUSPENSSION ARRAY
            $insertSuspenArray['SUSPENSION_START_DATE'] = $suspenStartDate;
            $insertSuspenArray['SUSPENSION_END_DATE'] = $suspenEndDate;
            $insertSuspenArray['IS_ROOM_SERVICE_ALLOW'] = $roomServiceAllow;
            $insertSuspenArray['IS_FB_ALLOW'] = $fbAllow;
            $insertSuspenArray['IS_LAUNDRY_ALLOW'] = $laundryAllow;
            $insertSuspenArray['IS_EVENT_BOOKING'] = $eventBooking;
            $insertSuspenArray['IS_EVENT_BOOKING'] = $eventBooking;
        } else {// TERMINATION ARRAY
            $insertSuspenArray['TERMINATION_DATE'] = $terminationDate;
        }

        if (isset($suspensionId) && $suspensionId > 0) {
            $insertSuspenArray['CHANGED_BY'] = $userId;
            $insertSuspenArray['CHANGED_BY_DATE'] = time();
            $whereISuspenCond = "PK_SUSPENSION_ID ='" . $suspensionId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
            $updateItem = update_rec(STUDENTSUSPENSION, $insertSuspenArray, $whereISuspenCond); // UPDATE RECORD IN Student Suspension TABLE.
            if (!$updateItem) {
                $msg = UPDATEFAIL . " " . STUDENTSUSPENSION;
            }
        } else {
            $insertSuspenArray['CREATED_BY'] = $userId;
            $insertSuspenArray['CREATED_BY_DATE'] = time();
            $insertSuspen = insert_rec(STUDENTSUSPENSION, $insertSuspenArray); // INSERT RECORD IN Student Suspension TABLE.          
            if (!isset($insertSuspen['lastInsertedId']) || $insertSuspen['lastInsertedId'] == 0) {
                $msg = INSERTFAIL . " " . STUDENTSUSPENSION;
            } else {
                $suspensionId = $insertSuspen['lastInsertedId'];
                // update student master IS_ACTIVE field
                $studentMasterArray = Array();
                if ($type == 1) { // SUSPENSSIOIN
                    $studentMasterArray['IS_ACTIVE'] = '3';
                } else { // TERMINATION
                    $studentMasterArray['IS_ACTIVE'] = '2';
                }
                $studentMasterArray['CHANGED_BY'] = $userId;
                $studentMasterArray['CHANGED_BY_DATE'] = time();
                $stuMasterCond = "PK_STUD_ID ='" . $studentId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
                $updateMaster = update_rec(STUDENTMASTER, $studentMasterArray, $stuMasterCond); // UPDATE RECORD IN Student Master TABLE.
                if (!$updateMaster) {
                    $msg = UPDATEFAIL . " " . STUDENTMASTER;
                }
            }
        }

        if ($msg == "") {

            $insertAlertStagingArr = Array(); //ALERT STAGING DATA INSERT
            $insertAlertStagingArr['FK_CLIENT_ID'] = $clientId;
            $insertAlertStagingArr['FK_ORG_ID'] = $orgId;
            $insertAlertStagingArr['APP_NAME'] = 'indecampus';
            $insertAlertStagingArr['DEVICE_TYPE'] = 'UBANTU';
            $insertAlertStagingArr['ALERT_SUB_ID'] = $recommendedId;
            $insertAlertStagingArr['MESSAGE_TYPE'] = 'ALERT';
            $insertAlertStagingArr['ACTION_FLAG'] = '0';
            $insertAlertStagingArr['REF_ID'] = $suspensionId;
            //$insertAlertStagingArr['MESSAGE_TEXT'] = ($type == 1) ? ADMINSTUDENTSUSPENSSION : ADMINSTUDENTTERMINATION;// COMMMENT BY KAVITA PATEL ON 17-08-2018 
            // ADDED BY KAVITA PATEL ON 17-08-2018 START
            $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $recommendedId . "'");
            $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
            $getStudData = fetch_rec_query("SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUDENT_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "'"); // GET STUDENT NAME
            $studentName = $getStudData[0]['STUDENT_NAME'];
            if ($type == 1) {
                $insertAlertStagingArr['MESSAGE_TEXT'] = "" . $studentName . " suspended by " . $userName . " from " . $suspenStartDate . " to " . $suspenEndDate . "";
            } else {
                $insertAlertStagingArr['MESSAGE_TEXT'] = "" . $studentName . " terminated by " . $userName . " from " . $terminationDate . "";
            }
            // ADDED BY KAVITA PATEL ON 17-08-2018 END
            // ADDED BY KAVITA PATEL ON 17-08-2018 START
            $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $recommendedId . "'");
            $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
            $getStudData = fetch_rec_query("SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUDENT_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "'"); // GET STUDENT NAME
            $studentName = $getStudData[0]['STUDENT_NAME'];
            if ($type == 1) {
                $insertAlertStagingArr['MESSAGE_TEXT'] = "(SUP) " . $studentName . " suspended by " . $userName . " from " . date('d-m-Y', strtotime($suspenStartDate)) . " to " . date('d-m-Y', strtotime($suspenEndDate)) . "";
            } else {
                $insertAlertStagingArr['MESSAGE_TEXT'] = "(TER) " . $studentName . " terminated by " . $userName . " from " . date('d-m-Y', strtotime($terminationDate)) . "";
            }
            // ADDED BY KAVITA PATEL ON 17-08-2018 END

            $insertAlertStagingArr['STATUS'] = '0';
            $insertAlertStagingArr['LOGIN_SESSION_ID'] = $userId;
            $insertAlertStagingArr['CREATED_BY'] = $userId;
            $insertAlertStagingArr['CREATED_BY_DATE'] = time();
            $insertAlertStagingData = insert_rec(ALERTSTAGINGDATA, $insertAlertStagingArr);
            if ($insertAlertStagingData['lastInsertedId'] > 0) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;

                $itemData = listStudentSuspensionData($requestData); //GET STUDENT SUSPENSION DATA.

                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];

                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => INSERTFAIL . " " . ALERTSTAGINGDATA);
                http_response_code(400);
            }
        } else {
            $result = array("status" => $msg);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

function listStudentSuspensionData($postData) {
// POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";


    if (!empty($clientId) && !empty($orgId)) {
        $whereCond = "";
        if (!empty($suspensionId)) {
            $whereCond = " AND SS.PK_SUSPENSION_ID ='" . $suspensionId . "'";
        }

        //add by Saitsh Karena for master user Permission all data START
        $whereUserCond = "";
        if (!empty($userId)) {

            $requestData = array();
            $requestData['postData']['clientId'] = $clientId;
            $requestData['postData']['userId'] = $userId;
            $requestData['postData']['requestuserId'] = $userId;
            $getUserHierarchy = getUserHierarchy($requestData);
            //print_r($getUserHierarchy); exit;
            if ($getUserHierarchy['status'] == SCS) {
                $finalUserId = array();
                foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                    if ($valueUH['lastLevelFlag'] == 'N') {
                        foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                            if ($valueUD['lastLevelFlag'] == 'Y') {
                                $finalUserId[$keyUH] = $valueUD['userId'];
                            }
                        }
                    } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                        $finalUserId[$keyUH] = $valueUH['userId'];
                    }
                }

                $userIds = implode(",", $finalUserId);
            } else if ($userId == "-1") {
                //add by Saitsh Karena for see master user all data
                $finalUserId = array();
                $getUserDetail = getUserDetail($clientId);

                for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                    $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                    $finalUserId[$ii] = $idOfUser;
                }
                $userIds = implode(",", array_unique($finalUserId));
            }
            $whereUserCond = " AND SS.CREATED_BY IN (" . $userIds . ")";
        }


        $sqlSuspenData = "SELECT SS.PK_SUSPENSION_ID,SS.FK_STUDENT_ID,SS.SUSPENSION_START_DATE,SS.SUSPENSION_END_DATE,SS.RECOMMENDED_BY,SS.IS_ROOM_SERVICE_ALLOW,SS.IS_FB_ALLOW,SS.IS_LAUNDRY_ALLOW,SS.IS_EVENT_BOOKING,SS.REVOKE_BY,SS.REVOKE_DATE,SM.MEMBERSHIP_NUMBER,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) STUD_NAME,(SELECT ROOM_NAME FROM " . ALLOTMASTER . " WHERE FK_STUDENT_ID = SM.PK_STUD_ID AND DELETE_FLAG ='0' AND DATE(now()) >= TENANCY_START_DATE AND DATE(now()) <= TENANCY_END_DATE ORDER BY PK_ALLOTMENT_ID DESC LIMIT 0,1) AS ROOM_NAME,UM.FULLNAME AS REVOKE_BY_USERNAME,SM.IS_ACTIVE,SS.TYPE,SS.TERMINATION_DATE,SS.REASON FROM " . STUDENTSUSPENSION . " SS INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID =SS.FK_STUDENT_ID LEFT JOIN " . USERMASTER . " UM ON UM.PK_USER_ID = SS.REVOKE_BY WHERE SS.DELETE_FLAG ='0'" . $whereCond . $whereUserCond." ORDER BY SS.PK_SUSPENSION_ID DESC"; // QUERY FOR ITEM DATA.
        $resSuspenData = fetch_rec_query($sqlSuspenData); // RESULT FOR ITEM DATA.
        if (count($resSuspenData) > 0) {
            foreach ($resSuspenData as $keyItem => $valueItem) { // ITEM DATA LOOP
                $suspenArr[$keyItem]['suspensionId'] = $valueItem['PK_SUSPENSION_ID'];
                $suspenArr[$keyItem]['studentId'] = $valueItem['FK_STUDENT_ID'];
                $suspenArr[$keyItem]['studentName'] = $valueItem['STUD_NAME'];
                $suspenArr[$keyItem]['studentMembershipNo'] = $valueItem['MEMBERSHIP_NUMBER'];
                $suspenArr[$keyItem]['suspensionStartDate'] = $valueItem['SUSPENSION_START_DATE'];
                $suspenArr[$keyItem]['suspensionEndDate'] = $valueItem['SUSPENSION_END_DATE'];
                $suspenArr[$keyItem]['recommendedBy'] = $valueItem['RECOMMENDED_BY'];
                $suspenArr[$keyItem]['roomServiceAllow'] = $valueItem['IS_ROOM_SERVICE_ALLOW'];
                $suspenArr[$keyItem]['FBAllow'] = $valueItem['IS_FB_ALLOW'];
                $suspenArr[$keyItem]['laundryAllow'] = $valueItem['IS_LAUNDRY_ALLOW'];
                $suspenArr[$keyItem]['eventBooking'] = $valueItem['IS_EVENT_BOOKING'];
                $suspenArr[$keyItem]['revokeBy'] = $valueItem['REVOKE_BY'];
                $suspenArr[$keyItem]['revokeDate'] = $valueItem['REVOKE_DATE'];
                $suspenArr[$keyItem]['roomName'] = $valueItem['ROOM_NAME'];
                $suspenArr[$keyItem]['revokeByName'] = $valueItem['REVOKE_BY_USERNAME'];
                $suspenArr[$keyItem]['status'] = $valueItem['IS_ACTIVE'];
                $suspenArr[$keyItem]['type'] = $valueItem['TYPE'];
                $suspenArr[$keyItem]['terminationDate'] = $valueItem['TERMINATION_DATE'];
                $suspenArr[$keyItem]['reason'] = $valueItem['REASON'];

                if ($valueItem['TYPE'] == 1) { // IF TYPE = SUSPENSSION THEN CALL ATTACHMENT API.
                    $requestData = Array();
                    $requestData['postData']['suspensionId'] = $valueItem['PK_SUSPENSION_ID'];
                    $requestData['postData']['type'] = 'SUSPENSED';

                    $itemData = listSuppesionAttachment($requestData); //GET STUDENT ATTACHMENT SUSPENSION DATA.

                    $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                    $suspenArr[$keyItem]['attachmentData'] = $itemAllData;
                }
            }
            $result = array("status" => SCS, "data" => array_values($suspenArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);

        http_response_code(400);
    }
    return $result;
}

function deleteStudentSuspension($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    if (!empty($clientId) && !empty($orgId) && !empty($studentId) && !empty($suspensionId)) {

        $updateItemArray = Array(); //ITEM ARRAY UPDATE
        $updateItemArray['DELETE_FLAG'] = 1;
        $updateItemArray['CHANGED_BY'] = $userId;
        $updateItemArray['CHANGED_BY_DATE'] = time();

        $whereISuspenCond = "PK_SUSPENSION_ID ='" . $suspensionId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
        $updateItem = update_rec(STUDENTSUSPENSION, $updateItemArray, $whereISuspenCond);  // UPDATE DELETE_FLAG =1 IN STUDENT_SUSPENSION TABLE.

        if ($updateItem) {

            $updatestudentArray = Array(); //ITEM ARRAY UPDATE
            $updatestudentArray['IS_ACTIVE'] = 1;
            $updatestudentArray['CHANGED_BY'] = $userId;
            $updatestudentArray['CHANGED_BY_DATE'] = time();

            $whereIStudCond = "PK_STUD_ID ='" . $studentId . "'";
            $updateStud = update_rec(STUDENTMASTER, $updatestudentArray, $whereIStudCond);  // UPDATE DELETE_FLAG =1 IN STUDENT_SUSPENSION TABLE.
            if ($updateStud) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['userId'] = $userId;

                $itemData = listStudentSuspensionData($requestData); //GET STUDENT SUSPENSION DATA.

                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => UPDATEFAIL . " " . STUDENTSUSPENSION);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function revokeStudentSuspension($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $revokeBy = isset($postData['postData']['revokeBy']) ? addslashes(trim($postData['postData']['revokeBy'])) : "";
    $revokeDate = !empty($postData['postData']['revokeDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['revokeDate'])))) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($suspensionId) && !empty($revokeDate)) {

        $updateItemArray = Array(); //ITEM ARRAY UPDATE
        $updateItemArray['REVOKE_BY'] = $revokeBy;
        $updateItemArray['REVOKE_DATE'] = $revokeDate;
        $updateItemArray['CHANGED_BY'] = $userId;
        $updateItemArray['CHANGED_BY_DATE'] = time();

        $whereISuspenCond = "PK_SUSPENSION_ID ='" . $suspensionId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
        $updateItem = update_rec(STUDENTSUSPENSION, $updateItemArray, $whereISuspenCond);  // UPDATE REVOKE BY AND REVOKE DATE IN STUDENT_SUSPENSION TABLE.

        if ($updateItem) {

            $updatestudentArray = Array(); //ITEM ARRAY UPDATE
            $updatestudentArray['IS_ACTIVE'] = 1;
            $updatestudentArray['CHANGED_BY'] = $userId;
            $updatestudentArray['CHANGED_BY_DATE'] = time();

            $whereIStudCond = "PK_STUD_ID ='" . $studentId . "'";
            $updateStud = update_rec(STUDENTMASTER, $updatestudentArray, $whereIStudCond);  // UPDATE DELETE_FLAG =1 IN STUDENT_MASTER TABLE.
            if ($updateStud) {

                $insertAlertStagingArr = Array(); //ALERT STAGING DATA INSERT
                $insertAlertStagingArr['FK_CLIENT_ID'] = $clientId;
                $insertAlertStagingArr['FK_ORG_ID'] = $orgId;
                $insertAlertStagingArr['APP_NAME'] = 'indecampus';
                $insertAlertStagingArr['DEVICE_TYPE'] = 'UBANTU';
                $insertAlertStagingArr['ALERT_SUB_ID'] = $revokeBy;
                $insertAlertStagingArr['MESSAGE_TYPE'] = 'ALERT';
                $insertAlertStagingArr['ACTION_FLAG'] = '0';
                $insertAlertStagingArr['REF_ID'] = $suspensionId;
                // $insertAlertStagingArr['MESSAGE_TEXT'] = ADMINSTUDENTREVOKE; // COMMENT BY KAVITA PATEL ON 17-08-2018
                // ADDED BY KAVITA PATEL ON 17-08-2018 START
                $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $revokeBy . "'");
                $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
                $getStudData = fetch_rec_query("SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUDENT_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "'"); // GET STUDENT NAME
                $studentName = $getStudData[0]['STUDENT_NAME'];
                $insertAlertStagingArr['MESSAGE_TEXT'] = "Student suspension of " . $studentName . " is revoke by " . $userName . " on " . $revokeDate . ".";
                // ADDED BY KAVITA PATEL ON 17-08-2018 END
                // ADDED BY KAVITA PATEL ON 17-08-2018 START
                $getUserData = fetch_rec_query("SELECT FULLNAME FROM " . USERMASTER . " WHERE PK_USER_ID='" . $revokeBy . "'");
                $userName = $getUserData[0]['FULLNAME']; // GET USER NAME
                $getStudData = fetch_rec_query("SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUDENT_NAME FROM " . STUDENTMASTER . " WHERE PK_STUD_ID='" . $studentId . "'"); // GET STUDENT NAME
                $studentName = $getStudData[0]['STUDENT_NAME'];
                $insertAlertStagingArr['MESSAGE_TEXT'] = "(SUP) Student suspension of " . $studentName . " is revoke by " . $userName . " on " . date('d-m-Y', strtotime($revokeDate)) . ".";
                // ADDED BY KAVITA PATEL ON 17-08-2018 END

                $insertAlertStagingArr['STATUS'] = '0';
                $insertAlertStagingArr['LOGIN_SESSION_ID'] = $userId;
                $insertAlertStagingArr['CREATED_BY'] = $userId;
                $insertAlertStagingArr['CREATED_BY_DATE'] = time();
                $insertAlertStagingData = insert_rec(ALERTSTAGINGDATA, $insertAlertStagingArr);
                if ($insertAlertStagingData['lastInsertedId'] > 0) {

                    $requestData = $studentData = Array();
                    $requestData['postData']['clientId'] = $clientId;
                    $requestData['postData']['orgId'] = $orgId;
                    $requestData['postData']['userId'] = $userId;

                    $itemData = listStudentSuspensionData($requestData); //GET STUDENT SUSPENSION DATA.

                    $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                    $result = array("status" => SCS, "data" => $itemAllData);
                    http_response_code(200);
                } else {
                    $result = array("status" => INSERTFAIL . " " . ALERTSTAGINGDATA);
                    http_response_code(400);
                }
            } else {
                $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => UPDATEFAIL . " " . STUDENTSUSPENSION);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function studentTermination($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $suspensionId = isset($postData['postData']['suspensionId']) ? addslashes(trim($postData['postData']['suspensionId'])) : "";
    $type = isset($postData['postData']['type']) ? addslashes(trim($postData['postData']['type'])) : "";
    $terminationDate = isset($postData['postData']['terminationDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['terminationDate'])))) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($suspensionId) && !empty($terminationDate)) {

        $updateItemArray = Array(); //ITEM ARRAY UPDATE
        $updateItemArray['TYPE'] = $type;
        $updateItemArray['TERMINATION_DATE'] = $terminationDate;
        $updateItemArray['CHANGED_BY'] = $userId;
        $updateItemArray['CHANGED_BY_DATE'] = time();

        $whereISuspenCond = "PK_SUSPENSION_ID ='" . $suspensionId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
        $updateItem = update_rec(STUDENTSUSPENSION, $updateItemArray, $whereISuspenCond);  // UPDATE REVOKE BY AND REVOKE DATE IN STUDENT_SUSPENSION TABLE.

        if ($updateItem) {

            $updatestudentArray = Array(); //ITEM ARRAY UPDATE
            $updatestudentArray['IS_ACTIVE'] = 2;
            $updatestudentArray['CHANGED_BY'] = $userId;
            $updatestudentArray['CHANGED_BY_DATE'] = time();

            $whereIStudCond = "PK_STUD_ID ='" . $studentId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "'";
            $updateStud = update_rec(STUDENTMASTER, $updatestudentArray, $whereIStudCond);  // UPDATE DELETE_FLAG =1 IN STUDENT_MASTER TABLE.

            if ($updateStud) {

                $requestData = $studentData = Array();
                $requestData['postData']['clientId'] = $clientId;
                $requestData['postData']['orgId'] = $orgId;
                $requestData['postData']['userId'] = $userId;

                $itemData = listStudentSuspensionData($requestData); //GET STUDENT SUSPENSION DATA.

                $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];
                $result = array("status" => SCS, "data" => $itemAllData);
                http_response_code(200);
            } else {
                $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => UPDATEFAIL . " " . STUDENTSUSPENSION);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//ADD BY DEVIKA.10.8.2018.
function attachmentStudentProfileUpload($postData, $postFile) {
// POST DATA
    $postdata = $postData['postData'];
    $clientId = isset($postdata['clientId']) ? addslashes(trim($postdata['clientId'])) : ""; // CLIENT ID
    $userId = isset($postdata['userId']) ? addslashes(trim($postdata['userId'])) : ""; // USER ID
    $orgId = isset($postdata['orgId']) ? addslashes(trim($postdata['orgId'])) : ""; // ORG ID
    $studentId = isset($postdata['studentId']) ? addslashes(trim($postdata['studentId'])) : ""; // COMPLAIN ID
// echo "<pre>"; print_r($postdata); exit;
    $attachmentName = $postFile; // ATTACH NAME 
// START UPLOADING DATA INTO DATA BASE
    $path = STUDENT_PROFILE_PATH;
    $type = 'profile';
    $uploadAttachment = uploadImage($attachmentName, $path, $type); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
    if ($uploadAttachment['status'] == SCS) {

// INSERT DATA TO DATABASE FOR FURTHER REF.

        $updateAttach['PROFILE_IMAGE'] = $uploadAttachment['fileName'];
        $updateAttach['CHANGED_BY'] = $userId;
        $updateAttach['CHANGED_BY_DATE'] = time();
//print_r($updateAttach); die;
        $whereStudentattachCond = "PK_STUD_ID ='" . $studentId . "' AND FK_ORG_ID='" . $orgId . "' AND FK_CLIENT_ID='" . $clientId . "' AND DELETE_FLAG='0'";
        $updateAttach = update_rec(STUDENTMASTER, $updateAttach, $whereStudentattachCond); // INSERTING DATA IN ATTACH_STUDENT_MASTER TABLE

        if ($updateAttach) { // IF SUCCESS THEN GIVE SUCCESS RESPONSE.
            $finalJson = array("fileName" => basename($uploadAttachment['fileName']), "fileLink" => $uploadAttachment['fileName']);
            $result = array("status" => SCS, "data" => $finalJson);
            http_response_code(200);
        }
    } else {
        $result = $uploadAttachment;
        http_response_code(400);
    }

    return $result; // RETURN DATA TO PL
}

//END BY DEVIKA.10.8.2018.

function getUsageHistory($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    if (!empty($clientId) && !empty($orgId)) {

        $sqlUsageData = "SELECT CT.PK_CAL_TRANS_ID,CT.FK_ITEM_ID,IM.ITEM_NAME,CT.ALLOCATION_START_DATE,CT.ALLOCATION_END_DATE,CT.QTY,CT.USED_QTY FROM " . CALALLOCATIONTRANS . " CT INNER JOIN " . ITEMMASTER . " IM ON IM.PK_ITEM_ID =CT.FK_ITEM_ID AND IM.ITEM_TYPE='SUBSC' WHERE  CT.FK_STUDENT_ID='" . $studentId . "' AND CT.DELETE_FLAG ='0'"; // QUERY FOR ITEM DATA.

        $resUsageData = fetch_rec_query($sqlUsageData); // RESULT FOR ITEM DATA.
        if (count($resUsageData) > 0) {
            $listUsageDataArr = array();
            foreach ($resUsageData as $keyUsage => $valueUsage) { // ITEM DATA LOOP
                $listUsageDataArr[$keyUsage]['itemName'] = $valueUsage['ITEM_NAME'];
                $listUsageDataArr[$keyUsage]['startDate'] = $valueUsage['ALLOCATION_START_DATE'];
                $listUsageDataArr[$keyUsage]['endDate'] = $valueUsage['ALLOCATION_END_DATE'];
                $listUsageDataArr[$keyUsage]['qty'] = ($valueUsage['QTY'] == "-1") ? 'Unlimited' : $valueUsage['QTY'];
                $listUsageDataArr[$keyUsage]['usedQty'] = $valueUsage['USED_QTY'];
                If ($valueUsage['QTY'] == "-1") {
                    $listUsageDataArr[$keyUsage]['balanceQty'] = "Unlimited";
                    $listUsageDataArr[$keyUsage]['excessQty'] = "-";
                } else {
                    $listUsageDataArr[$keyUsage]['balanceQty'] = ($valueUsage['QTY'] - $valueUsage['USED_QTY'] > 0) ? $valueUsage['QTY'] - $valueUsage['USED_QTY'] : 0;


                    $listUsageDataArr[$keyUsage]['excessQty'] = ($valueUsage['USED_QTY'] - $valueUsage['QTY'] > 0) ? $valueUsage['USED_QTY'] - $valueUsage['QTY'] : 0;
                }
            }
            $result = array("status" => SCS, "data" => array_values($listUsageDataArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function getCustomerInvoiceData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";

    if (!empty($clientId) && !empty($orgId)) {
        $whereCond = "";
        if (!empty($invoiceId)) {
            $whereCond = " AND IM.C_INVOICE_ID ='" . $invoiceId . "'";
        }

        $sqlPaymentInvoice = "SELECT IF(PAID_AMOUNT > 0,SUM(PAID_AMOUNT),0) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " WHERE PAYMENT_TYPE='2' AND DELETE_FLAG ='0' AND FK_INVOICE_ID='" . $invoiceId . "' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'"; //QUERY FOR INVOICE PAYMENT DATA 
        $resPaymentInvoice = fetch_rec_query($sqlPaymentInvoice);

        $totalPayment = 0;
        if (count($resPaymentInvoice) > 0) {
            $totalPayment = $resPaymentInvoice[0]['PAID_AMOUNT'];
        }

        $sqlstudentInvoice = "SELECT IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.INVOICE_NAME,IM.PERIOD_FROM,IM.PERIOD_TO,IM.FK_STUDENT_ID,CM.FIRST_NAME,CM.LAST_NAME FROM " . INVOICEMASTER . " IM LEFT JOIN " . CUSTOMERMASTER . " CM ON CM.PK_CUST_ID=IM.FK_CUST_ID  WHERE IM.DELETE_FLAG=0 AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "' " . $whereCond . "  ";
        $resultStudentInvoice = fetch_rec_query($sqlstudentInvoice);
        $studentInvoiceArr = array();
        if (count($resultStudentInvoice) > 0) {
            foreach ($resultStudentInvoice as $keyStudentInvoice => $valueStudentInvoice) {
                $studentInvoiceArr[$keyStudentInvoice]['cInvoiceId'] = $valueStudentInvoice['C_INVOICE_ID'];
                $studentInvoiceArr[$keyStudentInvoice]['invoiceDate'] = $valueStudentInvoice['INVOICE_DATE'];
                $studentInvoiceArr[$keyStudentInvoice]['invoiceName'] = $valueStudentInvoice['INVOICE_NAME'];
                $studentInvoiceArr[$keyStudentInvoice]['periodFrom'] = $valueStudentInvoice['PERIOD_FROM'];
                $studentInvoiceArr[$keyStudentInvoice]['periodFrom'] = $valueStudentInvoice['PERIOD_FROM'];
                $studentInvoiceArr[$keyStudentInvoice]['periodTo'] = $valueStudentInvoice['PERIOD_TO'];
                $studentInvoiceArr[$keyStudentInvoice]['fullName'] = $valueStudentInvoice['FIRST_NAME'] . " " . $valueStudentInvoice['LAST_NAME'];

                $studentInvoiceArr[$keyStudentInvoice]['totalPayment'] = $totalPayment;

                $sqlChargeData = "SELECT ITEM_NAME,TAX_CGST,TAX_SGST,TAX_IGST,HSN_CODE,SUM(QTY_OF_SESSION) AS QTY_OF_SESSION,ITEM_BASE_PRICE,SUM(ITEM_TOTAL_PRICE) AS ITEM_TOTAL_PRICE,SUM(TOTAL_TAX) AS TOTAL_TAX,INVOICE_ID FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $valueStudentInvoice['C_INVOICE_ID'] . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1' GROUP BY FK_ITEM_ID";
                $resultStudentChargeData = fetch_rec_query($sqlChargeData);
                $itemDataArr = array();
                if (count($resultStudentChargeData) > 0) {
                    foreach ($resultStudentChargeData as $keyStudentChargeData => $valueStudentChargeData) {
                        $itemDataArr[$keyStudentChargeData]['itemName'] = $valueStudentChargeData['ITEM_NAME'];
                        $itemDataArr[$keyStudentChargeData]['cgst'] = $valueStudentChargeData['TAX_CGST'];
                        $itemDataArr[$keyStudentChargeData]['sgst'] = $valueStudentChargeData['TAX_SGST'];
                        $itemDataArr[$keyStudentChargeData]['igst'] = $valueStudentChargeData['TAX_IGST'];
                        $itemDataArr[$keyStudentChargeData]['hsnCode'] = $valueStudentChargeData['HSN_CODE'];
                        $itemDataArr[$keyStudentChargeData]['qty'] = $valueStudentChargeData['QTY_OF_SESSION'];
                        $itemDataArr[$keyStudentChargeData]['itemPrice'] = $valueStudentChargeData['ITEM_BASE_PRICE'];
                        $itemDataArr[$keyStudentChargeData]['itemTotalTax'] = $valueStudentChargeData['TOTAL_TAX'];
                        $itemDataArr[$keyStudentChargeData]['itemTotalPrice'] = $valueStudentChargeData['ITEM_TOTAL_PRICE'];
                        $itemDataArr[$keyStudentChargeData]['invoiceId'] = $valueStudentChargeData['INVOICE_ID'];
                    }
                    $studentInvoiceArr[$keyStudentInvoice]['itemData'] = array_values($itemDataArr);
                } else {
                    $studentInvoiceArr[$keyStudentInvoice]['itemData'] = NORECORDS;
                }
            }
            $result = array("status" => SCS, "data" => array_values($studentInvoiceArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

function addEditNonAvailingService($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $serviceId = isset($postData['postData']['serviceId']) ? addslashes(trim($postData['postData']['serviceId'])) : "";
    $serviceTranId = isset($postData['postData']['serviceTranId']) ? addslashes(trim($postData['postData']['serviceTranId'])) : "";
    $serviceStartDate = isset($postData['postData']['serviceStartDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['serviceStartDate'])))) : "";
    $serviceEndDate = isset($postData['postData']['serviceEndDate']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['serviceEndDate'])))) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $description = isset($postData['postData']['description']) ? addslashes(trim($postData['postData']['description'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($userId) && !empty($studentId) && !empty($serviceStartDate) && !empty($serviceEndDate) && !empty($itemId)) {  // CHECK REQURIED FIELD CONDITION
// ITEM INSERT ARRAY.
        $sqlNonAvalingService = "SELECT * FROM " . SERVICEMASTER . " WHERE (('" . $serviceStartDate . "' BETWEEN SERVICE_START_DATE AND SERVICE_END_DATE) OR (('" . $serviceEndDate . "' BETWEEN SERVICE_START_DATE AND SERVICE_END_DATE))) AND FK_STUD_ID = '" . $studentId . "'";
        $resNonAvalingService = fetch_rec_query($sqlNonAvalingService);

        if (count($resNonAvalingService) == 0) {

            $msg = "";
            $insertSuspenArray = Array(); //ITEM ARRAY CREATE
            $insertSuspenArray['FK_CLIENT_ID'] = $clientId;
            $insertSuspenArray['C_SERVICE_ID'] = generateId(SERVICEMASTER, 'SEV', $clientId);
            $insertSuspenArray['FK_ORG_ID'] = $orgId;
            $insertSuspenArray['FK_STUD_ID'] = $studentId;
            $insertSuspenArray['SERVICE_START_DATE'] = $serviceStartDate;
            $insertSuspenArray['SERVICE_END_DATE'] = $serviceEndDate;
            $insertSuspenArray['FK_ITEM_ID'] = $itemId;
            $insertSuspenArray['DESCRIPTION'] = $description;

            $insertSuspenArray['CREATED_BY'] = $userId;
            $insertSuspenArray['CREATED_BY_DATE'] = time();

            // echo '<pre>';print_r($insertSuspenArray);exit

            $insertSuspen = insert_rec(SERVICEMASTER, $insertSuspenArray); // INSERT RECORD IN Student Suspension TABLE.          
            if (!isset($insertSuspen['lastInsertedId']) || $insertSuspen['lastInsertedId'] == 0) {
                $msg = INSERTFAIL . " " . SERVICEMASTER;
            } else {

                $begin = new DateTime($serviceStartDate);
                $end = new DateTime($serviceEndDate);

                for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
                    $serviceDate = $i->format("Y-m-d");


                    $insertServiceArray = Array(); //ITEM ARRAY CREATE
                    $insertServiceArray['FK_SERVICE_ID'] = $insertSuspen['lastInsertedId'];
                    $insertServiceArray['FK_STUD_ID'] = $studentId;
                    $insertServiceArray['SERVICE_DATE'] = $serviceDate;
                    $insertServiceArray['STATUS'] = '1';


                    $insertServiceArray['CREATED_BY'] = $userId;
                    $insertServiceArray['CREATED_BY_DATE'] = time();
                    $insertService = insert_rec(SERVICETRANS, $insertServiceArray); // INSERT RECORD IN Student Suspension TABLE.          
                    if (!isset($insertService['lastInsertedId']) || $insertService['lastInsertedId'] == 0) {
                        $msg = INSERTFAIL . " " . SERVICETRANS;
                    }
                }
            }

            $result = array("status" => SCS);
            http_response_code(200);
        } else {
            $result = array("status" => SERVICEALREADYBOOKED);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

?>
