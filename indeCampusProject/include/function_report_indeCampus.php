<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : AKSHAT SAVANI                   */
/*   CREATION DATE : 08-AUG-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */

//ADD BY AKSHAT.07.08.2018 // LIST STUDENT ENROLLMENT REPORT API.

function studentEnrollmentReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $enrollType = isset($postData['postData']['enrollmentType']) ? addslashes(trim($postData['postData']['enrollmentType'])) : "";
    $gender = isset($postData['postData']['gender']) ? addslashes(trim($postData['postData']['gender'])) : "";
    $year = date('Y');
// echo '<pre>';print_r($postData);exit;
    $whereEnrollTypeCond = "";

    if ($enrollType != "") {
        $whereEnrollTypeCond = " AND ENROLLMENT_TYPE = '" . $enrollType . "'";
    }

    $whereGenderCond = "";

    if ($gender != "") {
        $whereGenderCond = " AND GENDER = '" . $gender . "'";
    }

    $whereStudentIdCond = "";
    if (!empty($studentId)) {
        $whereStudentIdCond = " AND PK_STUD_ID = '" . $studentId . "'";
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND FK_CLIENT_ID = '" . $clientId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END



    $sqlStudentEnrollData = " SELECT FIRST_NAME,LAST_NAME,MEMBERSHIP_NUMBER,ACDEMIC_START_DATE,ACDEMIC_END_DATE,ENROLLMENT_TYPE,GENDER,MOBILE_NO,IS_ACTIVE,CREATED_BY_DATE FROM " . STUDENTMASTER . " WHERE DATE(DATE_FORMAT(FROM_UNIXTIME(CREATED_BY_DATE), '%Y')) = '" . $year . "'" . $whereEnrollTypeCond . $whereGenderCond . $whereStudentIdCond . $whereClientIdCond . $whereUserCond;

    $resStudentEnrollData = fetch_rec_query($sqlStudentEnrollData); // RESULT FOR GET Invoice Data
    $studentEnrollArr = Array();
    if (count($resStudentEnrollData) > 0) {
        foreach ($resStudentEnrollData as $keyStudentEnroll => $valueStudentEnroll) { // Invoice Data FOREACH LOOP.
            $studentEnrollArr[$keyStudentEnroll]['studentName'] = $valueStudentEnroll['FIRST_NAME'] . " " . $valueStudentEnroll['LAST_NAME'];
            $studentEnrollArr[$keyStudentEnroll]['memberShipId'] = $valueStudentEnroll['MEMBERSHIP_NUMBER'];
            $studentEnrollArr[$keyStudentEnroll]['enrollmentDate'] = date('d-m-Y', $valueStudentEnroll['CREATED_BY_DATE']);
            $studentEnrollArr[$keyStudentEnroll]['acdemicYear'] = $valueStudentEnroll['ACDEMIC_START_DATE'] . '/' . $valueStudentEnroll['ACDEMIC_END_DATE'];
            $studentEnrollArr[$keyStudentEnroll]['enrollmentType'] = $valueStudentEnroll['ENROLLMENT_TYPE'];
            $studentEnrollArr[$keyStudentEnroll]['gender'] = $valueStudentEnroll['GENDER'];
            $studentEnrollArr[$keyStudentEnroll]['mobileNo'] = $valueStudentEnroll['MOBILE_NO'];
            $studentEnrollArr[$keyStudentEnroll]['status'] = $valueStudentEnroll['IS_ACTIVE'];
        }
        $result = array("status" => SCS, "data" => array_values($studentEnrollArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.07.08.2018 // LIST STUDENT STUDENT ENROLLMENT REPORT API.
//START BY AKSHAT.08.08.2018 // LIST RENTAL SPACE REPORT API.
function rentalSpaceReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $propertyId = isset($postData['postData']['propertyId']) ? addslashes(trim($postData['postData']['propertyId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";
    $partyId = isset($postData['postData']['partyId']) ? addslashes(trim($postData['postData']['partyId'])) : "";
    $inquiryStatus = isset($postData['postData']['inquiryStatus']) ? addslashes(trim($postData['postData']['inquiryStatus'])) : "";


    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND IM.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END


    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND IM.FK_CLIENT_ID = '" . $clientId . "'";
    }

    $wherePropertIdCond = "";

    if ($propertyId != "") {
        $wherePropertIdCond = " AND IM.FK_PROPERTY_ID = '" . $propertyId . "'";
    }

    $wherePartyIdCond = "";

    if ($partyId != "") {
        $wherePartyIdCond = " AND IM.FK_CUST_ID = '" . $partyId . "'";
    }

    $whereStatusCond = "";

    if ($inquiryStatus != "") {
        $whereStatusCond = " AND IM.STATUS = '" . $inquiryStatus . "'";
    }
    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND DATE(DATE_FORMAT(FROM_UNIXTIME(IM.CREATED_BY_DATE), '%Y-%m-%d')) BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "'";
    }


    $sqlRentalData = " SELECT IM.FK_CUST_ID,IM.PROPERTY_NAME,DATE(DATE_FORMAT(FROM_UNIXTIME(IM.CREATED_BY_DATE), '%Y-%m-%d')) AS INQUIRYDATE,IM.STATUS,CM.FIRST_NAME,CM.LAST_NAME FROM " . INQMASTER . " AS IM INNER JOIN " . CUSTOMERMASTER . " AS CM ON IM.FK_CUST_ID = CM.PK_CUST_ID WHERE IM.DELETE_FLAG ='0'" . $wherePropertIdCond . $whereStatusCond . $wherePartyIdCond . $whereDateCond . $whereClientIdCond . $whereUserCond;


    $resRentalData = fetch_rec_query($sqlRentalData); // RESULT FOR GET Invoice Data
    $rentalArr = Array();
    if (count($resRentalData) > 0) {
        foreach ($resRentalData as $keyRental => $valueRental) { // Invoice Data FOREACH LOOP.
            if ($valueRental['STATUS'] == '1') {
                $inqStatus = 'Pending';
            } else if ($valueRental['STATUS'] == '2') {
                $inqStatus = 'Booked';
            } else {
                $inqStatus = 'Cancel';
            }

            $rentalArr[$keyRental]['propertyName'] = $valueRental['PROPERTY_NAME'];
            $rentalArr[$keyRental]['acedamicyear'] = $acedmeyear;
            $rentalArr[$keyRental]['inquiryDate'] = $valueRental['INQUIRYDATE'];
            $rentalArr[$keyRental]['partyName'] = $valueRental['FIRST_NAME'] . ' ' . $valueRental['LAST_NAME'];
            $rentalArr[$keyRental]['inquiryStatus'] = $inqStatus;
        }
        $result = array("status" => SCS, "data" => array_values($rentalArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST RENTAL SPACE REPORT API.
//START BY AKSHAT.08.08.2018 // LIST REVENUE REPORT API.
function revenueReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-01", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-t", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";
    $invoiceType = isset($postData['postData']['invoiceType']) ? addslashes(trim($postData['postData']['invoiceType'])) : "";
    $invoiceNumber = isset($postData['postData']['invoiceNumber']) ? addslashes(trim($postData['postData']['invoiceNumber'])) : "";
    $stuMembershipNo = isset($postData['postData']['stuMembershipNo']) ? addslashes(trim($postData['postData']['stuMembershipNo'])) : "";


    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND IM.FK_CLIENT_ID = '" . $clientId . "'";
    }


    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND DATE(DATE_FORMAT(FROM_UNIXTIME(IM.CREATED_BY_DATE), '%Y-%m-%d')) BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "'";
    }

    $whereInvoiceTypeCond = "";

    if ($invoiceType != "") {
        $whereInvoiceTypeCond = " AND IM.INVOICE_TYPE = '" . $invoiceType . "'";
    }

    $whereInvoiceNumCond = "";

    if ($invoiceNumber != "") {
        $whereInvoiceNumCond = " AND IM.C_INVOICE_ID = '" . $invoiceNumber . "'";
    }

    $whereStudentIdCond = "";

    if ($studentId != "") {
        $whereStudentIdCond = " AND SM.PK_STUD_ID = '" . $studentId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND IM.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END
    // $sqlRevenueData = " SELECT IM.INVOICE_DATE,IM.C_INVOICE_ID,IM.INVOICE_TYPE,IM.TOTAL_CHARGES,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME FROM " . INVOICEMASTER . " AS IM INNER JOIN " . STUDENTMASTER . " AS SM ON IM.FK_STUDENT_ID = SM.PK_STUD_ID WHERE IM.DELETE_FLAG = '0' " . $whereInvoiceTypeCond . $whereInvoiceNumCond . $whereStudentIdCond . $whereDateCond . $whereClientIdCond;
    // $sqlRevenueData = " SELECT IM.INVOICE_DATE,IM.C_INVOICE_ID,IM.INVOICE_TYPE,IM.TOTAL_CHARGES,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,SUM(PM.PAID_AMOUNT) AS PAID_AMT FROM " . INVOICEMASTER . " AS IM INNER JOIN " . STUDENTMASTER . " AS SM ON IM.FK_STUDENT_ID = SM.PK_STUD_ID LEFT JOIN ".PAYMENTMASTER." AS PM ON PM.FK_INVOICE_ID = IM.C_INVOICE_ID WHERE IM.DELETE_FLAG = '0' ". $whereInvoiceTypeCond . " " .$whereInvoiceNumCond ." ". $whereStudentIdCond ."  " . $whereDateCond ." " .$whereClientIdCond ." GROUP BY (PM.FK_INVOICE_ID)";
    $sqlRevenueData = "SELECT IM.INVOICE_DATE,IM.C_INVOICE_ID,IM.INVOICE_TYPE,IM.TOTAL_CHARGES,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,(SELECT IF(PAID_AMOUNT > 0,SUM(PAID_AMOUNT),0) AS PAID_AMOUNT FROM " . PAYMENTMASTER . " where FK_INVOICE_ID = C_INVOICE_ID  GROUP BY (FK_INVOICE_ID)) AS PAID_AMT FROM " . INVOICEMASTER . " AS IM INNER JOIN " . STUDENTMASTER . " AS SM ON IM.FK_STUDENT_ID = SM.PK_STUD_ID  WHERE IM.DELETE_FLAG = '0' " . $whereInvoiceTypeCond . " " . $whereInvoiceNumCond . " " . $whereStudentIdCond . "  " . $whereDateCond . " " . $whereClientIdCond . $whereUserCond . " ORDER BY IM.PK_INVOICE_ID DESC";

    $resRevenueData = fetch_rec_query($sqlRevenueData); // RESULT FOR GET Invoice Data
    $revenueArr = Array();
    if (count($resRevenueData) > 0) {
        foreach ($resRevenueData as $keyRevenue => $valueRevenue) { // Invoice Data FOREACH LOOP.
            $revenueArr[$keyRevenue]['memberShipId'] = $valueRevenue['MEMBERSHIP_NUMBER'];
            $revenueArr[$keyRevenue]['studentName'] = $valueRevenue['FIRST_NAME'] . ' ' . $valueRevenue['LAST_NAME'];
            $valueRevenue['PAID_AMT'] = ($valueRevenue['PAID_AMT'] == NULL) ? 0 : $valueRevenue['PAID_AMT'];
            $revenueArr[$keyRevenue]['acdemicYear'] = $acedmeyear;
            $revenueArr[$keyRevenue]['invoiceDate'] = $valueRevenue['INVOICE_DATE'];
            $revenueArr[$keyRevenue]['invoiceNumber'] = $valueRevenue['C_INVOICE_ID'];
            $revenueArr[$keyRevenue]['invoiceType'] = $valueRevenue['INVOICE_TYPE'];
            $revenueArr[$keyRevenue]['invoiceAmount'] = $valueRevenue['TOTAL_CHARGES'];
            $revenueArr[$keyRevenue]['status'] = ($valueRevenue['PAID_AMT'] >= $valueRevenue['TOTAL_CHARGES'] ) ? 'PAID' : 'UNPAID';
        }
        $result = array("status" => SCS, "data" => array_values($revenueArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST REVENUE REPORT API.
//START BY AKSHAT.08.08.2018 // LIST REQUEST REPORT API.
function requestReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-01", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-t", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";
    $departmet = isset($postData['postData']['departmet']) ? addslashes(trim($postData['postData']['departmet'])) : "";
    $openCloseStatus = isset($postData['postData']['openCloseStatus']) ? addslashes(trim($postData['postData']['openCloseStatus'])) : "";

    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND DATE(DATE_FORMAT(FROM_UNIXTIME(CM.CREATED_BY_DATE), '%Y-%m-%d')) BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "'";
    }

    $whereDepartmentCond = "";

    if ($departmet != "") {
        $whereDepartmentCond = " AND CM.DEPARTMENT_ID = '" . $departmet . "'";
    }

    $whereStatusCond = "";

    if ($openCloseStatus != "") {
        $whereStatusCond = " AND CM.STATUS = '" . $openCloseStatus . "'";
    }

    $whereStudentIdCond = "";

    if ($studentId != "") {
        $whereStudentIdCond = " AND SM.PK_STUD_ID = '" . $studentId . "'";
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND CM.FK_CLIENT_ID = '" . $clientId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND CM.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END

    $sqlRequestData = " SELECT CM.TICKET_ID,CL.VALUE AS DEPARTMENT,IM.DEPARTMENT_CATEGORY_NAME AS CATEGORY,IM1.DEPARTMENT_CATEGORY_NAME AS SUB_CATEGORY,CM.FK_STUD_ID,CM.STATUS,DATE(DATE_FORMAT(FROM_UNIXTIME(CM.CREATED_BY_DATE), '%Y-%m-%d')) AS TICKET_DATE,SM.FIRST_NAME,SM.LAST_NAME,SM.MEMBERSHIP_NUMBER,(SELECT DESCRIPTION FROM " . COMPLAINTTRANS . " WHERE FK_COMPLAINT_ID = CM.PK_COMPLAINT_ID ORDER BY CREATED_BY_DATE LIMIT 0,1) AS issue FROM " . COMPLAINTMASTER . " AS CM INNER JOIN " . STUDENTMASTER . " AS SM ON CM.FK_STUD_ID = SM.PK_STUD_ID INNER JOIN " . LOOKUPMASTER . " AS CL ON CL.PK_LOOKUP_ID = CM.DEPARTMENT_ID INNER JOIN " . ITEMDEPARTMENTMST . " AS IM ON IM.PK_ITEM_DEPARTMENT_ID = CM.DEPARTMENT_CATGORY_ID INNER JOIN " . ITEMDEPARTMENTMST . " AS IM1 ON IM1.PK_ITEM_DEPARTMENT_ID = CM.DEPARTMENT_SUBCAT_ID WHERE CM.TYPE = '1'" . $whereDepartmentCond . $whereStatusCond . $whereDateCond . $whereStudentIdCond . $whereClientIdCond . $whereUserCond;

    $resRequestData = fetch_rec_query($sqlRequestData);
    $requestArr = Array();
    if (count($resRequestData) > 0) {
        foreach ($resRequestData as $keyRequest => $valueRequest) {

            if ($valueRequest['STATUS'] == '0') {
                $status = 'Open Request';
            } else if ($valueRequest['STATUS'] == '2') {
                $status = 'Closed Request';
            } else if ($valueRequest['STATUS'] == '3') {
                $status = 'Reopen Request';
            } else if ($valueRequest['STATUS'] == '4') {
                $status = 'Cancel Request';
            }


            $requestArr[$keyRequest]['studentId'] = $valueRequest['FK_STUD_ID'];
            $requestArr[$keyRequest]['studentName'] = $valueRequest['FIRST_NAME'] . ' ' . $valueRequest['LAST_NAME'];
            $requestArr[$keyRequest]['studMembershipNo'] = $valueRequest['MEMBERSHIP_NUMBER'];
            $requestArr[$keyRequest]['ticketId'] = $valueRequest['TICKET_ID'];
            $requestArr[$keyRequest]['department'] = $valueRequest['DEPARTMENT'];
            $requestArr[$keyRequest]['category'] = $valueRequest['CATEGORY'];
            $requestArr[$keyRequest]['subCategory'] = $valueRequest['SUB_CATEGORY'];
            $requestArr[$keyRequest]['ticketDate'] = $valueRequest['TICKET_DATE'];
            $requestArr[$keyRequest]['issue'] = $valueRequest['issue'];
            $requestArr[$keyRequest]['status'] = $status;
        }
        $result = array("status" => SCS, "data" => array_values($requestArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST REQUEST REPORT API.
//START BY AKSHAT.08.08.2018 // LIST COMPLAINT REPORT API.
function complaintReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-01", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-t", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";
    $departmet = isset($postData['postData']['departmet']) ? addslashes(trim($postData['postData']['departmet'])) : "";
    $openCloseStatus = isset($postData['postData']['openCloseStatus']) ? addslashes(trim($postData['postData']['openCloseStatus'])) : "";

    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND DATE(DATE_FORMAT(FROM_UNIXTIME(CM.CREATED_BY_DATE), '%Y-%m-%d')) BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "'";
    }

    $whereDepartmentCond = "";

    if ($departmet != "") {
        $whereDepartmentCond = " AND CM.DEPARTMENT_ID = '" . $departmet . "'";
    }

    $whereStatusCond = "";

    if ($openCloseStatus != "") {
        $whereStatusCond = " AND CM.STATUS = '" . $openCloseStatus . "'";
    }


    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND CM.FK_CLIENT_ID = '" . $clientId . "'";
    }

    $whereStudentIdCond = "";

    if ($studentId != "") {
        $whereStudentIdCond = " AND SM.PK_STUD_ID = '" . $studentId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND CM.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END

    $sqlRequestData = " SELECT CM.TICKET_ID,CL.VALUE AS DEPARTMENT,IM.DEPARTMENT_CATEGORY_NAME AS CATEGORY,IM1.DEPARTMENT_CATEGORY_NAME AS SUB_CATEGORY,CM.FK_STUD_ID,CM.STATUS,DATE(DATE_FORMAT(FROM_UNIXTIME(CM.CREATED_BY_DATE), '%Y-%m-%d')) AS TICKET_DATE,SM.FIRST_NAME,SM.LAST_NAME,SM.MEMBERSHIP_NUMBER,(SELECT DESCRIPTION FROM " . COMPLAINTTRANS . " WHERE FK_COMPLAINT_ID = CM.PK_COMPLAINT_ID ORDER BY CREATED_BY_DATE LIMIT 0,1) AS issue FROM " . COMPLAINTMASTER . " AS CM INNER JOIN " . STUDENTMASTER . " AS SM ON CM.FK_STUD_ID = SM.PK_STUD_ID INNER JOIN " . LOOKUPMASTER . " AS CL ON CL.PK_LOOKUP_ID = CM.DEPARTMENT_ID INNER JOIN " . ITEMDEPARTMENTMST . " AS IM ON IM.PK_ITEM_DEPARTMENT_ID = CM.DEPARTMENT_CATGORY_ID INNER JOIN " . ITEMDEPARTMENTMST . " AS IM1 ON IM1.PK_ITEM_DEPARTMENT_ID = CM.DEPARTMENT_SUBCAT_ID WHERE CM.TYPE = '2'" . $whereDepartmentCond . $whereStatusCond . $whereDateCond . $whereClientIdCond . $whereStudentIdCond . $whereUserCond;

    $resRequestData = fetch_rec_query($sqlRequestData);
    $requestArr = Array();
    if (count($resRequestData) > 0) {
        foreach ($resRequestData as $keyRequest => $valueRequest) {


            if ($valueRequest['STATUS'] == '0') {
                $status = 'Open Request';
            } else if ($valueRequest['STATUS'] == '2') {
                $status = 'Closed Request';
            } else if ($valueRequest['STATUS'] == '3') {
                $status = 'Reopen Request';
            } else if ($valueRequest['STATUS'] == '4') {
                $status = 'Cancel Request';
            }

            $requestArr[$keyRequest]['studentId'] = $valueRequest['FK_STUD_ID'];
            $requestArr[$keyRequest]['studMembershipNo'] = $valueRequest['MEMBERSHIP_NUMBER'];
            $requestArr[$keyRequest]['studentName'] = $valueRequest['FIRST_NAME'] . ' ' . $valueRequest['LAST_NAME'];
            $requestArr[$keyRequest]['ticketId'] = $valueRequest['TICKET_ID'];
            $requestArr[$keyRequest]['department'] = $valueRequest['DEPARTMENT'];
            $requestArr[$keyRequest]['category'] = $valueRequest['CATEGORY'];
            $requestArr[$keyRequest]['subCategory'] = $valueRequest['SUB_CATEGORY'];
            $requestArr[$keyRequest]['ticketDate'] = $valueRequest['TICKET_DATE'];
            $requestArr[$keyRequest]['issue'] = $valueRequest['issue'];
            $requestArr[$keyRequest]['status'] = $status;
        }
        $result = array("status" => SCS, "data" => array_values($requestArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST COMPLAINT REPORT API.
//START BY AKSHAT.08.08.2018 // LIST SUSPENSION REPORT API.
function suspensionReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-01", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-t", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";

    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND SS.SUSPENSION_END_DATE BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "'";
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND SS.FK_CLIENT_ID = '" . $clientId . "'";
    }

    $whereStudentIdCond = "";

    if ($studentId != "") {
        $whereStudentIdCond = " AND SM.PK_STUD_ID = '" . $studentId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND SS.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END

    $sqlSuspenData = "SELECT SS.FK_STUDENT_ID,SS.SUSPENSION_START_DATE,SS.SUSPENSION_END_DATE,SS.REVOKE_DATE,SM.FIRST_NAME,SM.LAST_NAME,CL.DISPLAY_VALUE,SM.MEMBERSHIP_NUMBER FROM " . STUDENTSUSPENSION . " AS SS INNER JOIN " . STUDENTMASTER . " AS SM ON SM.PK_STUD_ID=SS.FK_STUDENT_ID LEFT JOIN " . CLIENTLOOKUP . " AS CL ON CL.PK_CLT_LKUP_ID = SS.REASON WHERE SS.TYPE = '1'" . $whereDateCond . $whereClientIdCond . $whereStudentIdCond . $whereUserCond;

    $resSuspenData = fetch_rec_query($sqlSuspenData);
    $suspenArr = Array();
    if (count($resSuspenData) > 0) {
        foreach ($resSuspenData as $keySuspen => $valueSuspen) {

            $date1 = date_create($valueSuspen['SUSPENSION_START_DATE']);
            $date2 = date_create($valueSuspen['SUSPENSION_END_DATE']);
            $diff = date_diff($date1, $date2);
            $noOfDays = $diff->format("%R%a days");

            $days = explode(" ", $noOfDays);
            $day = $days[0] + 1;

            $reason = isset($valueSuspen['DISPLAY_VALUE']) ? $valueSuspen['DISPLAY_VALUE'] : "";

            $suspenArr[$keySuspen]['studentId'] = $valueSuspen['FK_STUDENT_ID'];
            $suspenArr[$keySuspen]['studMembershipNo'] = $valueSuspen['MEMBERSHIP_NUMBER'];
            $suspenArr[$keySuspen]['studentName'] = $valueSuspen['FIRST_NAME'] . ' ' . $valueSuspen['LAST_NAME'];
            $suspenArr[$keySuspen]['acdemicYear'] = $acedmeyear;
            $suspenArr[$keySuspen]['fromDate'] = $valueSuspen['SUSPENSION_START_DATE'];
            $suspenArr[$keySuspen]['toDate'] = $valueSuspen['SUSPENSION_END_DATE'];
            $suspenArr[$keySuspen]['noOfDays'] = $day;
            $suspenArr[$keySuspen]['reason'] = $reason;
            $suspenArr[$keySuspen]['revocationDate'] = $valueSuspen['REVOKE_DATE'];
        }
        $result = array("status" => SCS, "data" => array_values($suspenArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST SUSPENSION REPORT API.
//START BY AKSHAT.08.08.2018 // LIST TERMINATION REPORT API.
function terminationReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-01", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-t", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";

    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereStudentIdCond = "";

    if ($studentId != "") {
        $whereStudentIdCond = " AND SM.PK_STUD_ID = '" . $studentId . "'";
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND SS.FK_CLIENT_ID = '" . $clientId . "'";
    }

    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND SS.TERMINATION_DATE BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND SS.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END

    $sqlSuspenData = "SELECT SS.FK_STUDENT_ID,SS.TERMINATION_DATE,SM.FIRST_NAME,SM.LAST_NAME,CL.DISPLAY_VALUE,SM.MEMBERSHIP_NUMBER FROM " . STUDENTSUSPENSION . " AS SS INNER JOIN " . STUDENTMASTER . " AS SM ON SM.PK_STUD_ID=SS.FK_STUDENT_ID LEFT JOIN " . CLIENTLOOKUP . " AS CL ON CL.PK_CLT_LKUP_ID = SS.REASON WHERE SS.TYPE = '2'" . $whereDateCond . $whereClientIdCond . $whereStudentIdCond . $whereUserCond;

    $resSuspenData = fetch_rec_query($sqlSuspenData);
    $suspenArr = Array();
    if (count($resSuspenData) > 0) {
        foreach ($resSuspenData as $keySuspen => $valueSuspen) {

            $reason = isset($valueSuspen['DISPLAY_VALUE']) ? $valueSuspen['DISPLAY_VALUE'] : "";

            $suspenArr[$keySuspen]['studentId'] = $valueSuspen['FK_STUDENT_ID'];
            $suspenArr[$keySuspen]['studMembershipNo'] = $valueSuspen['MEMBERSHIP_NUMBER'];
            $suspenArr[$keySuspen]['studentName'] = $valueSuspen['FIRST_NAME'] . ' ' . $valueSuspen['LAST_NAME'];
            $suspenArr[$keySuspen]['acdemicYear'] = $acedmeyear;
            $suspenArr[$keySuspen]['reason'] = $reason;
            $suspenArr[$keySuspen]['terminationDate'] = $valueSuspen['TERMINATION_DATE'];
        }
        $result = array("status" => SCS, "data" => array_values($suspenArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST TERMINATION REPORT API.
//START BY AKSHAT.08.08.2018 // LIST EVENT MANAGEMENT REPORT API.
function eventMangementReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $fromMonth = !empty($postData['postData']['fromMonth']) ? date("Y-m-d", strtotime(addslashes(trim($postData['postData']['fromMonth'])))) : "";
    $toMonth = !empty($postData['postData']['toMonth']) ? date("Y-m-t", strtotime(addslashes(trim($postData['postData']['toMonth'])))) : "";
    $eventName = isset($postData['postData']['eventName']) ? addslashes(trim($postData['postData']['eventName'])) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";

    if (!empty($fromMonth) && !empty($toMonth)) {
        $yearExplod = explode("-", $fromMonth);
        $year1 = $yearExplod[0];

        $yearExplod1 = explode("-", $toMonth);
        $year2 = $yearExplod1[0];

        $acedmeyear = $year1 . '/' . $year2;
    } else {
        $acedmeyear = date('Y') . "/" . date('Y');
    }

    $whereDateCond = "";
    if (!empty($fromMonth) && !empty($toMonth)) {
        $whereDateCond = " AND (DATE(DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%Y-%m-%d')) BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "' OR DATE(DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%Y-%m-%d')) BETWEEN '" . $fromMonth . "' AND '" . $toMonth . "' )";
    }

    $whereEventCond = "";

    if ($eventName != "") {
        $whereEventCond = " AND IM.PK_ITEM_ID = '" . $eventName . "'";
    }

    $whereEventStatus = "";
    if ($status != "") {
        $whereEventStatus = " AND IM.IS_ACTIVE ='" . $status . "'";
    }

    $whereClientIdCond = "";

    if ($clientId != "") {
        $whereClientIdCond = " AND IM.FK_CLIENT_ID = '" . $clientId . "'";
    }

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND IM.CREATED_BY IN (" . $userIds . ")";
    }

//add by Saitsh Karena for master user Permission all data END

    $sqlSuspenData = "SELECT IM.ITEM_NAME,(IM.IS_ACTIVE)AS STATUS,(IM.QTY)AS TOTAL_CAPACITY,IM.QTY,(SELECT IF(QTY > 0,SUM(QTY),0) FROM " . EVENTSBOOKING . "  WHERE FK_ITEM_ID = IM.PK_ITEM_ID GROUP BY FK_ITEM_ID) AS TOTAL_BOOKED_SEATS,IM.EVENT_START_DATETIME,IM.EVENT_END_DATETIME FROM " . ITEMMASTER . " IM  WHERE IM.DELETE_FLAG =0 AND IM.ITEM_TYPE='EVENTS' " . $whereEventCond . " " . $whereEventStatus . " " . $whereDateCond . " " . $whereClientIdCond . $whereUserCond;



    $resSuspenData = fetch_rec_query($sqlSuspenData);
    $suspenArr = Array();
    if (count($resSuspenData) > 0) {
        foreach ($resSuspenData as $keySuspen => $valueSuspen) {

            $suspenArr[$keySuspen]['startDate'] = date('d-m-Y', $valueSuspen['EVENT_START_DATETIME']);
            $suspenArr[$keySuspen]['endDate'] = date('d-m-Y', $valueSuspen['EVENT_END_DATETIME']);
            $suspenArr[$keySuspen]['eventName'] = $valueSuspen['ITEM_NAME'];
            $suspenArr[$keySuspen]['totalCapacity'] = $valueSuspen['TOTAL_CAPACITY'];
            $suspenArr[$keySuspen]['bookedCapacity'] = ($valueSuspen['TOTAL_BOOKED_SEATS'] == "" || $valueSuspen['TOTAL_BOOKED_SEATS'] == NULL) ? 0 : $valueSuspen['TOTAL_BOOKED_SEATS'];
            $remainingSeats = intval($valueSuspen['TOTAL_CAPACITY']) - intval($suspenArr[$keySuspen]['bookedCapacity']);
            $suspenArr[$keySuspen]['unbookedCapacity'] = $remainingSeats;
            $suspenArr[$keySuspen]['status'] = ($valueSuspen['STATUS'] == '0') ? "Inactive" : "Active";
        }
        $result = array("status" => SCS, "data" => array_values($suspenArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY AKSHAT.08.08.2018 // LIST EVENT MANAGEMENT REPORT REPORT API
//ADDED BY AKSHAT UPLOAD ITEM MASTER CSV CODE END FOR CMS DATA ON 10-08-2018
function itemMasterCSVUpload($postData, $postFile) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    $attachmentName = $postFile; // ATTACH NAME 
    // START UPLOADING DATA INTO DATA BASE
    $path = MASTER_ATTACH_PATH;
    $uploadAttachment = uploadImage($attachmentName, $path, $type); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
    //print_r($uploadAttachment); exit;
    if ($uploadAttachment['status'] == SCS) {
        //echo $attachmentName;exit;
        if (!empty($clientId) && !empty($orgId) && !empty($userId)) {  // CHECK REQURIED FIELD CONDITION
// ITEM INSERT ARRAY.
            $filename1 = $attachmentName;
            $count = 0;

            if (($handle = fopen($filename1, 'r')) !== FALSE) {

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    if ($count > 0) {

                        $insertItemArray = Array(); //ITEM ARRAY CREATE
                        $insertItemArray['ITEM_NAME'] = $data[0];
                        $insertItemArray['FK_CLIENT_ID'] = $clientId;
                        $insertItemArray['FK_ORG_ID'] = $orgId;
                        $insertItemArray['FK_USER_ID'] = $userId;
                        $insertItemArray['TALLY_NAME'] = $data[1];
                        $insertItemArray['HSN_CODE'] = $data[2];
                        $insertItemArray['ITEM_TYPE'] = $data[3];
                        $insertItemArray['ITEM_PRICE'] = $data[4];
                        $insertItemArray['DESCRIPTION'] = $data[5];
                        $insertItemArray['QTY'] = $data[6];
                        $insertItemArray['CGST'] = $data[7];
                        $insertItemArray['SGST'] = $data[8];
                        $insertItemArray['IGST'] = $data[9];
                        $insertItemArray['ITEM_VALIDITY_END_DATE'] = $data[10];
                        $insertItemArray['EVENT_START_DATETIME'] = $data[11];
                        $insertItemArray['EVENT_END_DATETIME'] = $data[12];

                        $insertItem = insert_rec(ITEMMASTER, $insertItemArray);
                    }
                    $count++;
                }
            }

            $result = array("status" => SCS);
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    } else {
        $result = array("status" => $uploadAttachment['status']);
        http_response_code(400);
    }

    return $result;
}

//IMPORT ITEM MASTER CSV CODE END FOR CMS DATA ON 10-08-2018
//ADDED BY AKSHAT IMPORT PROPERTY MASTER CSV CODE  FOR CMS DATA ON 13-08-2018
function propertyMasterCSVUpload($postData, $postFile) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    $attachmentName = $postFile; // ATTACH NAME 
    // START UPLOADING DATA INTO DATA BASE
    $path = MASTER_ATTACH_PATH;
    $uploadAttachment = uploadImage($attachmentName, $path, $type); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
    //print_r($uploadAttachment); exit;
    if ($uploadAttachment['status'] == SCS) {

        //echo $attachmentName;exit;
        if (!empty($clientId) && !empty($orgId) && !empty($userId)) {  // CHECK REQURIED FIELD CONDITION
// ITEM INSERT ARRAY.
            $filename1 = $attachmentName;
            $count = 0;

            if (($handle = fopen($filename1, 'r')) !== FALSE) {

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    if ($count > 0) {

                        $insertPropArray = Array(); //ITEM ARRAY CREATE
                        $insertPropArray['PROPERTY_NAME'] = $data[0];
                        $insertPropArray['FK_CLIENT_ID'] = $clientId;
                        $insertPropArray['FK_ORG_ID'] = $orgId;
                        $insertPropArray['PROPERTY_VALIDITY_END_DATE'] = $data[1];
                        $insertPropArray['CGST'] = $data[2];
                        $insertPropArray['SGST'] = $data[3];
                        $insertPropArray['IGST'] = $data[4];
                        $insertPropArray['PRICE'] = $data[5];
                        $insertPropArray['TYPE'] = $data[6];
                        $insertPropArray['HSN_CODE'] = $data[7];
                        $insertPropArray['TALLY_NAME'] = $data[8];



                        $insertItem = insert_rec(PROPERTYMASTER, $insertPropArray);
                    }
                    $count++;
                }
            }

            $result = array("status" => SCS);
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    } else {
        $result = array("status" => $uploadAttachment['status']);
        http_response_code(400);
    }

    return $result;
}

//IMPORT PROPERTY MASTER CSV CODE END ON 13-08-2018
//ADDED BY AKSHAT UPLOAD DEPARTMENT MASTER CSV CODE END FOR CMS DATA ON 10-08-2018
function departmentMasterCSVUpload($postData, $postFile) {
    // POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    $attachmentName = $postFile; // ATTACH NAME 
    // START UPLOADING DATA INTO DATA BASE
    $path = MASTER_ATTACH_PATH;
    $uploadAttachment = uploadImage($attachmentName, $path, $type); // UPLOAD STUDENT COMPLAIN ATTACTMENT 
    //print_r($uploadAttachment); exit;
    if ($uploadAttachment['status'] == SCS) {

        //echo $attachmentName;exit;
        if (!empty($clientId) && !empty($orgId) && !empty($userId)) {  // CHECK REQURIED FIELD CONDITION
// ITEM INSERT ARRAY.
            $filename1 = $attachmentName;
            $count = 0;

            if (($handle = fopen($filename1, 'r')) !== FALSE) {

                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    if ($count > 0) {

                        $insertDeptArray = Array(); //ITEM ARRAY CREATE
                        $insertDeptArray['DEPARTMENT_CATEGORY_NAME'] = $data[0];
                        $insertDeptArray['FK_PARENT_DEPARTMENT_ID'] = $data[1];
                        $insertDeptArray['FK_DEPARTMENT_TYPE_ID'] = $data[2];
                        $insertDeptArray['FK_CLIENT_ID'] = $clientId;
                        $insertDeptArray['FK_ORG_ID'] = $orgId;


                        $insertItem = insert_rec(ITEMDEPARTMENTMST, $insertDeptArray);
                    }
                    $count++;
                }
            }

            $result = array("status" => SCS);
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    } else {
        $result = array("status" => $uploadAttachment['status']);
        http_response_code(400);
    }

    return $result;
}

//IMPORT DEPARTMENT MASTER CSV CODE END FOR CMS DATA ON 10-08-2018

function getCentraliseTransactionInvoices($postData) {
    $clientId = isset($postData['postData']['clientId']) ? $postData['postData']['clientId'] : "";
    $userId = isset($postData['postData']['userId']) ? $postData['postData']['userId'] : "";
    $orgId = isset($postData['postData']['orgId']) ? $postData['postData']['orgId'] : "";
    $type = isset($postData['postData']['type']) ? $postData['postData']['type'] : "";
    $subType = isset($postData['postData']['subType']) ? $postData['postData']['subType'] : "";
    $billId = isset($postData['postData']['billId']) ? $postData['postData']['billId'] : "";
    $data = array();

    /* === PROCESS START === */
    //====EPOSE SALES ======
    // 1. CREATE SALES IN EPOSE SIDE.
    // 2. GET THE DATA FROM CENTRALISED EPOS SALES
    // 3. GET EPOSE SALES ITEM FROM  CENTRALISED EPOS ITEMS
    // 4. GET EPOSE SALES TAX FROM CENTRALISED EPOSE SALES TAX.
    /* === PROCESS END === */

    if ($type == 'FNB') {
        if ($subType == 'sales') {
            $sqlEposSales = fetch_rec_query("SELECT PK_SALES_ID,FK_SPACE_ID,LOCATION,BILL_NO,BILL_DATE,MEMBERSHIP_NUMBER,STUDENT_NAME,BASIC_AMOUNT,DISCOUNT_AMOUNT,DISCOUNT_TYPE,ROUND_OFF,FINAL_AMOUNT,NARRATION FROM " . CENTRALISEEPOSSALES . " WHERE DELETE_FLAG=0 AND BILL_NO = '" . $billId . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_SALES_ID DESC ");

            $data = $sqlEposSales;


            if (count($sqlEposSales) > 0) {

                for ($i = 0; $i < count($sqlEposSales); $i++) {

                    $salesId = $sqlEposSales[$i]['PK_SALES_ID'];
                    $billNo = $sqlEposSales[$i]['BILL_NO'];

                    //SALES ITEMS
                    $sqlEposItems = fetch_rec_query("SELECT FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,ITEM_AMOUNT,ITEM_RATE FROM " . CENTRALISEEPOSSALESITEMS . " WHERE FK_SALES_ID='" . $salesId . "' ");
                    if (count($sqlEposItems) > 0) {
                        $data[$i]['items'] = $sqlEposItems;
                    }

                    //$salesItemId = $sqlEposItems[$i]['FK_ITEM_ID'];
                    //SALES TAX
                    $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,FK_ITEM_ID,TAX_NAME,TAX_PERCENTAGE,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSSALESTAX . " WHERE FK_SALES_ID='" . $salesId . "' AND BILL_NO='" . $billNo . "' GROUP BY TAX_NAME, TAX_PERCENTAGE ");
                    if (count($sqlEposTax) > 0) {
                        $data[$i]['tax'] = $sqlEposTax;
                    } else {
                        $data[$i]['tax'] = 0;
                    }
                }
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } elseif ($subType == 'creditNote') {

            $sqlEposCrNote = fetch_rec_query("SELECT PK_CREDIT_NOTE_ID,FK_SPACE_ID,LOCATION,BILL_NO,BILL_DATE,MEMBERSHIP_NUMBER,STUDENT_NAME,BASIC_AMOUNT,TAX_AMOUNT,DISCOUNT_AMOUNT,DISCOUNT_TYPE,ROUND_OFF,FINAL_AMOUNT,NARRATION,REFERENCE_NO,BILL_NO FROM " . CENTRALISEEPOSCREDITNOTE . " WHERE DELETE_FLAG=0 AND BILL_NO = '" . $billId . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_CREDIT_NOTE_ID DESC ");

            $data = $sqlEposCrNote;

            if (count($sqlEposCrNote) > 0) {
                for ($i = 0; $i < count($sqlEposCrNote); $i++) {

                    $crNoteId = $sqlEposCrNote[$i]['PK_CREDIT_NOTE_ID'];
                    $billNo = $sqlEposCrNote[$i]['BILL_NO'];

                    //SALES ITEMS
                    $sqlEposItems = fetch_rec_query("SELECT PK_CREDIT_NOTE_ITEM_ID,FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,ITEM_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTEITEMS . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' ");
                    if (count($sqlEposItems) > 0) {
                        $data[$i]['items'] = $sqlEposItems;
                    }

                    $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,FK_ITEM_ID,TAX_NAME,TAX_PERCENTAGE,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTETAX . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' AND BILL_NO='" . $billNo . "' GROUP BY TAX_NAME, TAX_PERCENTAGE ");
                    if (count($sqlEposTax) > 0) {
                        $data[$i]['tax'] = $sqlEposTax;
                    } else {
                        $data[$i]['tax'] = 0;
                    }
                }
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } elseif ($subType == 'receipt') {
            $sqlEposReceipt = fetch_rec_query("SELECT PK_RECEIPT_ID,FK_SPACE_ID,BILL_NO,BILL_DATE,STUDENT_NAME,MEMBERSHIP_NUMBER,PAYMENT_MODE,PAYMENT_AMOUNT,NARRATION,PARTY_ADDRESS,CHEQUE_NUMBER,REFERENCE_NO FROM " . CENTRALISEEPOSRECEIPT . " WHERE DELETE_FLAG=0 AND BILL_NO = '" . $billId . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_RECEIPT_ID DESC ");

            if (count($sqlEposReceipt) > 0) {

                $data = $sqlEposReceipt;
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        }
    } else if ($type == 'Laundry') {
        if ($subType == 'sales') {
            $sqlEposSales = fetch_rec_query("SELECT PK_SALES_ID,FK_SPACE_ID,LOCATION,BILL_NO,BILL_DATE,MEMBERSHIP_NUMBER,STUDENT_NAME,BASIC_AMOUNT,DISCOUNT_AMOUNT,DISCOUNT_TYPE,ROUND_OFF,FINAL_AMOUNT,NARRATION FROM " . CENTRALISEEPOSSALES . " WHERE DELETE_FLAG=0 AND BILL_NO = '" . $billId . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_SALES_ID DESC ");
            //echo '<pre>';print_r($sqlEposSales);die;
            $data = $sqlEposSales;

            if (count($sqlEposSales) > 0) {

                for ($i = 0; $i < count($sqlEposSales); $i++) {

                    $salesId = $sqlEposSales[$i]['PK_SALES_ID'];
                    $billNo = $sqlEposSales[$i]['BILL_NO'];

                    //SALES ITEMS
                    $sqlEposItems = fetch_rec_query("SELECT FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,ITEM_AMOUNT FROM " . CENTRALISEEPOSSALESITEMS . " WHERE FK_SALES_ID='" . $salesId . "' ");
                    if (count($sqlEposItems) > 0) {
                        $data[$i]['items'] = $sqlEposItems;
                    }

                    //$salesItemId = $sqlEposItems[$i]['FK_ITEM_ID'];
                    //SALES TAX
                    $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,FK_ITEM_ID,TAX_NAME,TAX_PERCENTAGE,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSSALESTAX . " WHERE FK_SALES_ID='" . $salesId . "' AND BILL_NO='" . $billNo . "' GROUP BY TAX_NAME, TAX_PERCENTAGE ");
                    if (count($sqlEposTax) > 0) {
                        $data[$i]['tax'] = $sqlEposTax;
                    } else {
                        $data[$i]['tax'] = 0;
                    }
                }
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } elseif ($subType == 'creditNote') {
            $sqlEposCrNote = fetch_rec_query("SELECT PK_CREDIT_NOTE_ID,FK_SPACE_ID,LOCATION,BILL_NO,BILL_DATE,MEMBERSHIP_NUMBER,STUDENT_NAME,BASIC_AMOUNT,TAX_AMOUNT,DISCOUNT_AMOUNT,DISCOUNT_TYPE,ROUND_OFF,FINAL_AMOUNT,NARRATION,REFERENCE_NO,BILL_NO FROM " . CENTRALISEEPOSCREDITNOTE . " WHERE DELETE_FLAG=0 AND BILL_NO = '" . $billId . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_CREDIT_NOTE_ID DESC ");

            $data = $sqlEposCrNote;

            if (count($sqlEposCrNote) > 0) {
                for ($i = 0; $i < count($sqlEposCrNote); $i++) {

                    $crNoteId = $sqlEposCrNote[$i]['PK_CREDIT_NOTE_ID'];
                    $billNo = $sqlEposCrNote[$i]['BILL_NO'];

                    //SALES ITEMS
                    $sqlEposItems = fetch_rec_query("SELECT PK_CREDIT_NOTE_ITEM_ID,FK_ITEM_ID,ITEM_NAME,ITEM_QTY,UOM_NAME,ITEM_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTEITEMS . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' ");
                    if (count($sqlEposItems) > 0) {
                        $data[$i]['items'] = $sqlEposItems;
                    }

                    $sqlEposTax = fetch_rec_query("SELECT FK_TAX_ID,FK_ITEM_ID,TAX_NAME,TAX_PERCENTAGE,SUM(TAX_AMOUNT) TAX_AMOUNT FROM " . CENTRALISEEPOSCREDITNOTETAX . " WHERE FK_CREDIT_NOTE_ID='" . $crNoteId . "' AND BILL_NO='" . $billNo . "' GROUP BY TAX_NAME, TAX_PERCENTAGE ");
                    if (count($sqlEposTax) > 0) {
                        $data[$i]['tax'] = $sqlEposTax;
                    } else {
                        $data[$i]['tax'] = 0;
                    }
                }
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        } elseif ($subType == 'receipt') {
            $sqlEposReceipt = fetch_rec_query("SELECT PK_RECEIPT_ID,FK_SPACE_ID,BILL_NO,BILL_DATE,STUDENT_NAME,MEMBERSHIP_NUMBER,PAYMENT_MODE,PAYMENT_AMOUNT,NARRATION,PARTY_ADDRESS,CHEQUE_NUMBER,REFERENCE_NO FROM " . CENTRALISEEPOSRECEIPT . " WHERE DELETE_FLAG=0 AND BILL_NO = '" . $billId . "' AND LOCATION=1 ORDER BY BILL_DATE,PK_RECEIPT_ID DESC ");

            if (count($sqlEposReceipt) > 0) {

                $data = $sqlEposReceipt;
            } else {
                $result = array("status" => NORECORDS);
                http_response_code(400);
            }
        }
    } elseif ($type == 'receipt') {
        $sqlCustomReceipt = fetch_rec_query("SELECT PK_RECEIPT_ID,INVOICE_ID,MEMBERSHIP_NUMBER,STUDENT_NAME,NARRATION,RECEIPT_DATE,PAYMENT_MODE,BANK_NAME,CHEQUE_NO,PAYMENT_AMOUNT,NARRATION FROM " . CENTRALISECUSTOMRECEIPT . " WHERE DELETE_FLAG=0 AND FK_CLIENT_ID = '" . $clientId . "' AND PK_RECEIPT_ID = '" . $billId . "' ORDER BY RECEIPT_DATE,PK_RECEIPT_ID DESC ");

        if (count($sqlCustomReceipt) > 0) {
            $data = $sqlCustomReceipt;
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } elseif ($type == 'generalInvoice') {
        $sqlCustomInvoice = fetch_rec_query("SELECT IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.INVOICE_NAME,IM.PERIOD_FROM,IM.PERIOD_TO,IM.FK_STUDENT_ID,SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,CONCAT_WS(' ',SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3) ADDRESS,SM.PINCODE,SM.FK_CITY_ID,SM.FK_STATE_ID,SM.FK_COUNTRY_ID,CT.CITY_NAME,CT.STATE_NAME,CO.COUNTRY_NAME FROM " . INVOICEMASTER . " IM LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID=IM.FK_STUDENT_ID LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = SM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=SM.FK_CITY_ID  WHERE IM.C_INVOICE_ID = '" . $billId . "' AND IM.DELETE_FLAG=0 AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID='" . $orgId . "'");
        $data = $sqlCustomInvoice;


        if (count($sqlCustomInvoice) > 0) {
            for ($i = 0; $i < count($sqlCustomInvoice); $i++) {
                $invoiceId = $sqlCustomInvoice[$i]['C_INVOICE_ID'];

                //INVOICE ITEMS
                //            $sqlInvoiceItems = fetch_rec_query("SELECT ITEM_NAME,ITEM_TOTAL_PRICE FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $invoiceId . "' ");
                $sqlInvoiceItems = fetch_rec_query("SELECT ITEM_NAME,TAX_CGST,TAX_SGST,TAX_IGST,HSN_CODE,QTY_OF_SESSION,ITEM_BASE_PRICE,ITEM_TOTAL_PRICE,TOTAL_TAX,INVOICE_ID FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $invoiceId . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1'");
                if (count($sqlInvoiceItems) > 0) {
                    $data[$i]['invoiceItems'] = $sqlInvoiceItems;
                } else {
                    $data[$i]['invoiceItems'] = NORECORDS;
                }
            }
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } elseif ($type == 'invoice') {
        $sqlIndInvoice = fetch_rec_query("SELECT PERIOD_FROM,PERIOD_TO,CONCAT_WS(' ',SM.FIRST_NAME,SM.LAST_NAME) STUDENTNAME,CONCAT_WS(' ',SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3) ADDRESS,PINCODE,SM.MEMBERSHIP_NUMBER,IM.TOTAL_CHARGES,IM.INVOICE_NAME,IM.INVOICE_DATE,IM.C_INVOICE_ID,IM.PK_INVOICE_ID,CT.CITY_NAME,CT.STATE_NAME,CO.COUNTRY_NAME FROM " . INVOICEMASTER . " IM INNER JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID = IM.FK_STUDENT_ID AND SM.DELETE_FLAG = '0' LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = SM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=SM.FK_CITY_ID WHERE  IM.C_INVOICE_ID = '" . $billId . "' AND IM.DELETE_FLAG = '0' AND IM.FK_CLIENT_ID = '" . $clientId . "' ORDER BY C_INVOICE_ID DESC "); // CHANGED BY KAUSHA SHAH ON 23-08-2018
        if (count($sqlIndInvoice) > 0) {

            $data = $sqlIndInvoice;
            for ($i = 0; $i < count($sqlIndInvoice); $i++) {

                $invoiceId = $sqlIndInvoice[$i]['C_INVOICE_ID'];

                $sqlInvoiceItems = fetch_rec_query("SELECT ITEM_NAME,TAX_CGST,TAX_SGST,TAX_IGST,QTY_OF_SESSION,ITEM_BASE_PRICE,ITEM_TOTAL_PRICE,TOTAL_TAX,INVOICE_ID,HSN_CODE FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $invoiceId . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1'");
                if (count($sqlInvoiceItems) > 0) {
                    $data[$i]['invoiceItems'] = $sqlInvoiceItems;
                } else {
                    $data[$i]['invoiceItems'] = NORECORDS;
                }
            }
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }

    //GET INVOICE DATA.
    //print_r($data);die;
    $result = array("status" => SCS, "data" => $data);

    return $result;
}

//START BY SATISH KARENA ON 05-09-2018 FOR ALLOTMENT REPORT.
function allotmentReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $roomType = isset($postData['postData']['roomType']) ? addslashes(trim($postData['postData']['roomType'])) : "";

    //add by Saitsh Karena for master user Permission all data START
    $whereUserCond = "";
    if (!empty($userId)) {

        $requestData = array();
        $requestData['postData']['clientId'] = $clientId;
        $requestData['postData']['userId'] = $userId;
        $requestData['postData']['requestuserId'] = $userId;
        $getUserHierarchy = getUserHierarchy($requestData);
        //print_r($getUserHierarchy); exit;
        if ($getUserHierarchy['status'] == SCS) {
            $finalUserId = array();
            foreach ($getUserHierarchy['data'] as $keyUH => $valueUH) {
                if ($valueUH['lastLevelFlag'] == 'N') {
                    foreach ($valueUH['userDetail'] as $keyUD => $valueUD) {
                        if ($valueUD['lastLevelFlag'] == 'Y') {
                            $finalUserId[$keyUH] = $valueUD['userId'];
                        }
                    }
                } elseif ($valueUH['lastLevelFlag'] == 'Y') {
                    $finalUserId[$keyUH] = $valueUH['userId'];
                }
            }

            $userIds = implode(",", $finalUserId);
        } else if ($userId == "-1") {
            //add by Saitsh Karena for see master user all data
            $finalUserId = array();
            $getUserDetail = getUserDetail($clientId);

            for ($ii = 0; $ii < count($getUserDetail['data']); $ii++) {

                $idOfUser = $getUserDetail['data'][$ii]['PK_USER_ID'];
                $finalUserId[$ii] = $idOfUser;
            }
            $userIds = implode(",", array_unique($finalUserId));
        }
        $whereUserCond = " AND AT.CREATED_BY IN (" . $userIds . ")";
    }
//add by Saitsh Karena for master user Permission all data END
    $whereRoomType ='';
    if(!empty($roomType)){
        $whereRoomType =" AND RM.FK_ITEM_ID='".$roomType."'";
    }

    //$sqlAllotmentData = "SELECT IM.ITEM_NAME,AT.ROOM_NAME,GROUP_CONCAT(DISTINCT(AT.BED_NO)) AS BED_NO from " . ALLOTMASTER . " AT  INNER JOIN " . ITEMMASTER . " IM ON IM.PK_ITEM_ID = AT.FK_ITEM_ID WHERE AT.DELETE_FLAG='0' AND AT.FK_CLIENT_ID ='" . $clientId . "' AND AT.FK_ORG_ID='" . $orgId . "' GROUP BY FK_ITEM_ID,FK_ROOM_ID ORDER BY IM.ITEM_NAME";
    
    
  $sqlAllotmentData = "SELECT RM.PK_ROOM_ID,RM.FK_CLIENT_ID,RM.FK_ORG_ID,RM.FK_ITEM_ID,RM.ROOM_NAME,RM.ROOM_CAPACITY,IM.ITEM_NAME,(SELECT COUNT(BED_NO) FROM ".ALLOTMASTER." WHERE FK_ROOM_ID = RM.PK_ROOM_ID   ) AS TOTAL_BED_NO FROM " . ROOMNOMASTER . " RM INNER JOIN ".ITEMMASTER." IM ON RM.FK_ITEM_ID =IM.PK_ITEM_ID WHERE RM.DELETE_FLAG ='0' AND RM.FK_CLIENT_ID = '" . $clientId . "' AND RM.FK_ORG_ID = '" . $orgId . "'".$whereRoomType;
      
 

    $resAllotmentData = fetch_rec_query($sqlAllotmentData);
    if (count($resAllotmentData) > 0) {

        foreach ($resAllotmentData as $keyAllotment => $valueAllotment) {
            
            $totalVecant =($valueAllotment['ROOM_CAPACITY']- $valueAllotment['TOTAL_BED_NO']  > 0)?$valueAllotment['ROOM_CAPACITY']- $valueAllotment['TOTAL_BED_NO']: 0;
            
            
            $allotmentArr[$keyAllotment]['roomTypeName'] = $valueAllotment['ITEM_NAME'];
            $allotmentArr[$keyAllotment]['roomName'] = $valueAllotment['ROOM_NAME'];
            $allotmentArr[$keyAllotment]['totalBedNo'] = $valueAllotment['ROOM_CAPACITY'];
            $allotmentArr[$keyAllotment]['totalBedAlloted'] = $valueAllotment['TOTAL_BED_NO'];
            $allotmentArr[$keyAllotment]['totalBedVacent'] = $totalVecant;
        }
        $result = array("status" => SCS, "data" => array_values($allotmentArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY SATISH KARENA ON 05-09-2018 FOR ALLOTMENT REPORT.
//START BY SATISH KARENA ON 05-09-2018 FOR STUDENT DEPARTMENT WISE REPORT.
function studentDepartmentwiseComplaintReport($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";

    $sqlComplaintData = "SELECT IF(CM.TYPE='1','Request','Complaint') AS TYPE,count(CM.STATUS) AS STATUS_COUNT,CONCAT(SM.FIRST_NAME,' ',SM.LAST_NAME) AS STUD_NAME,LM.VALUE AS DEPARTMENT, CASE WHEN CM.STATUS ='0' THEN 'Pending' WHEN CM.STATUS ='1' THEN 'Resloved' WHEN CM.STATUS ='2' THEN 'Close'
WHEN CM.STATUS ='3' THEN 'Reopen' WHEN CM.STATUS ='4' THEN 'Canceled' END AS STATUS_NAME from " . COMPLAINTMASTER . " CM  INNER JOIN " . STUDENTMASTER . " SM ON CM.FK_STUD_ID = SM.PK_STUD_ID INNER JOIN " . LOOKUPMASTER . " LM ON LM.PK_LOOKUP_ID=CM.DEPARTMENT_ID WHERE CM.DELETE_FLAG='0' AND CM.FK_CLIENT_ID ='" . $clientId . "' AND CM.FK_ORG_ID='" . $orgId . "' GROUP BY CM.FK_STUD_ID,CM.DEPARTMENT_ID,CM.TYPE,CM.STATUS";

    $resComplaintData = fetch_rec_query($sqlComplaintData);
    if (count($resComplaintData) > 0) {
        $complaintArr =Array();
        foreach ($resComplaintData as $keyComplaint => $valueComplaint) {
            $complaintArr[$keyComplaint]['Type'] = $valueComplaint['TYPE'];
            $complaintArr[$keyComplaint]['Status Name'] = $valueComplaint['STATUS_NAME'];
            $complaintArr[$keyComplaint]['Status Count'] = $valueComplaint['STATUS_COUNT'];
            $complaintArr[$keyComplaint]['Student Name'] = $valueComplaint['STUD_NAME'];
            $complaintArr[$keyComplaint]['Department Name'] = $valueComplaint['DEPARTMENT'];
        }
        $result = array("status" => SCS, "data" => array_values($complaintArr));
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//END BY SATISH KARENA ON 05-09-2018 FOR STUDENT DEPARTMENT WISE REPORT.
?>
