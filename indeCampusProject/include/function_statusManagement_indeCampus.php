<?php

/***************************************************/
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.           */
/*   DEVELOPED BY : DARSHAN PATEL                  */
/*   CREATION DATE : 03/07/2018                    */
/*   FILE TYPE : PHP                               */
/*   FILE NAME : STATUS MANAGEMENT                 */
/***************************************************/

function getCategoryForStatus($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $lookupId =  isset($postData['postData']['lookupId']) ? addslashes(trim($postData['postData']['lookupId'])) : "";
    
    $whereLkp = "";
    if(!empty($lookupId)){
        $whereLkp = " AND PK_LOOKUP_ID = '".$lookupId."'";
    }

    $sqlGetCategory = "SELECT PK_LOOKUP_ID,TYPE,VALUE FROM ".LOOKUPMASTER." WHERE TYPE = 'STATUSCATEGORY' AND DELETE_FLAG = 0".$whereLkp;
    $resultCategory = fetch_rec_query($sqlGetCategory);
    if(count($resultCategory) > 0){
        $finalJson = array();
        //for($i=0;$i<count($resultCategory);$i++){
        foreach($resultCategory as $keyCat => $valueCat){
            $finalJson[$keyCat]['lookupId'] = $valueCat['PK_LOOKUP_ID'];
            $finalJson[$keyCat]['lookupType'] = $valueCat['TYPE'];
            $finalJson[$keyCat]['lookupValue'] = $valueCat['VALUE'];
        }
        $result = array("status"=>SCS,"data"=>$finalJson);
        http_response_code(200);
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

//FOR ADD EDIT STATUS CATEGORY
function createUpdateCategoryForStatus($postData){
    //print_r($postData); exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $statusName = isset($postData['postData']['statusName']) ? addslashes(trim($postData['postData']['statusName'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $statusId = isset($postData['postData']['statusId']) ? addslashes(trim($postData['postData']['statusId'])) : "";
    
    $where = "";
    if(!empty($statusId)){
        $where = " AND PK_LOOKUP_ID != '".$statusId."'";
    }
    $insertUpdate = "";
    $sqlLookup = "SELECT PK_LOOKUP_ID FROM ".LOOKUPMASTER." WHERE TYPE = 'STATUSCATEGORY' AND DELETE_FLAG = 0 AND LOWER(VALUE) = '".strtolower($statusName)."' AND DELETE_FLAG = '0' $where ";
    $resultCategory = fetch_rec_query($sqlLookup);
    //print_r($resultCategory); exit;
    if(count($resultCategory) > 0){
        $insertUpdate = '1';
    }
    
    if($insertUpdate != '1'){
        if(!empty($statusId)){ // UPDATE CASE
            $updateStatus = array();
            ///$updateStatus['TYPE'] = $statusName; 
            $updateStatus['VALUE'] = $statusName; 
            $updateStatus['CHANGED_BY'] = $userId;         
            $updateStatus['CHANGED_BY_DATE'] = time(); 
            //$updateStatus['DELETE_FLAG'] = 0;

            $updateStatusWhere = "PK_LOOKUP_ID = '".$statusId."' AND DELETE_FLAG = '0'";

            $updateStatusRequest = update_rec(LOOKUPMASTER, $updateStatus, $updateStatusWhere);

            if($updateStatusRequest){
                $requestarr = array();
                $requestarr['postdata']['clientId'] = $clientId;
                $requestarr['postdata']['orgId'] = $orgId;
                $requestarr['postdata']['userId'] = $userId;
                $requestarr['postdata']['lookupId'] = $statusId;

                $getStatusListing = getCategoryForStatus($requestarr);

                $result = array("status"=>SCS,"data"=>$getStatusListing['data']);
                http_response_code(200);
            }else{
                $result = array("status"=>UPDATEFAIL." lookupmaster");
                http_response_code(400);
            }
            
        }else{ // INSERT CASE
            
            $insertStatus = array();
            $insertStatus['TYPE'] = 'STATUSCATEGORY'; 
            $insertStatus['VALUE'] = $statusName; 
            $insertStatus['CREATED_BY'] = $userId; 
            $insertStatus['CREATED_BY_DATE'] = time(); 
            $insertStatus['DELETE_FLAG'] = 0; 
            $insertLookReq = insert_rec(LOOKUPMASTER, $insertStatus);
            if($insertLookReq){
                $requestarr = array();
                $requestarr['postData']['clientId'] = $clientId;
                $requestarr['postData']['orgId'] = $orgId;
                $requestarr['postData']['userId'] = $userId;
                $requestarr['postData']['lookupId'] = $insertLookReq['lastInsertedId'];

                $getStatusListing = getCategoryForStatus($requestarr);
                $result = array("status"=>SCS,"data"=>$getStatusListing['data']);
                http_response_code(200);
            }else{
                $result = array("status"=>INSERTFAIL." lookupmaster");
                http_response_code(400);
            }        

        }
    }else{
        $result = array("status"=>'duplicate name not allowed....');
        http_response_code(400);
    }
    
    return $result;
}


function createEditStatus($postData){
    //print_r($postData); exit;
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    
    $categoryId = isset($postData['postData']['categoryId']) ? addslashes(trim($postData['postData']['categoryId'])) : "";
    $statusName = isset($postData['postData']['statusName']) ? addslashes(trim($postData['postData']['statusName'])) : "";
    $priorityLevel = isset($postData['postData']['priorityLevel']) ? addslashes(trim($postData['postData']['priorityLevel'])) : "";
    $statusId = isset($postData['postData']['statusId']) ? addslashes(trim($postData['postData']['statusId'])) : "";

    if(!empty($statusId)){ // UPDATE CASE
        //ADDED BY MONTU ON 26-10-2016 FOR CHECK PRIORITY LEVEL SAME OR NOT
        $updateStatusFlag = 0;
        $sqlGetLevel = "SELECT PRIORITY_LEVEL,PK_STATUS_ID,THRID_PARTY_FLAG,CANCEL_REMARK_FLAG,FK_CATEGORY_ID FROM ".STATUSMANAGEMENT." WHERE FK_CATEGORY_ID = '".$categoryId."' AND FK_CLIENT_ID = '".$clientId."' AND PK_STATUS_ID != '".$statusId."' AND PRIORITY_LEVEL = '".$priorityLevel."' AND DELETE_FLAG=0 ";
        $fetchLevel = fetch_rec_query($sqlGetLevel);
        //print_r($fetchLevel); exit;
        if(count($fetchLevel) > 0) { // IF SAME LEVEL UPDATE THEN RETURN
            //for($d=0;$d<count($fetchLevel);$d++){
            foreach($fetchLevel as $keyFetchLevel => $valueLevel){
                if($valueLevel['PRIORITY_LEVEL'] == $priorityLevel && ($valueLevel['THRID_PARTY_FLAG'] == 1)){
                    $updateStatusFlag = 1;
                }else if($valueLevel['PRIORITY_LEVEL'] == $priorityLevel && ($valueLevel['CANCEL_REMARK_FLAG'] == 1)){
                    $updateStatusFlag = 1;
                }else if($valueLevel['FK_CATEGORY_ID'] != 23 && $valueLevel['PRIORITY_LEVEL'] == $priorityLevel && $priorityLevel != 1){
                    $updateStatusFlag = 1;
                }else{
                    $result = array("status"=>DUBLICATELEVEL);
                    $updateStatusFlag = 0;
                }
            }
        } else { //IF NEXT LEVEL
            $updateStatusFlag = 1;
        }
        
        if($updateStatusFlag){
            $updateStatus = array();
            $updateStatus['STATUS_NAME'] = $statusName; 
            $updateStatus['FK_CATEGORY_ID'] = $categoryId; 
            $updateStatus['PRIORITY_LEVEL'] = $priorityLevel; 
            //$updateStatus['THRID_PARTY_FLAG'] = "0"; 
            $updateStatus['CHANGED_BY'] = $userId; 
            $updateStatus['CHANGED_BY_DATE'] = time(); 
            $updateStatus['DELETE_FLAG'] = 0;
            // WHERE
            $updateStatusWhere = "PK_STATUS_ID = '".$statusId."' AND FK_CLIENT_ID = '".$clientId."'";
            // update
            $updateStatusRequest = update_rec(STATUSMANAGEMENT, $updateStatus, $updateStatusWhere);
            if($updateStatusRequest){
                $result = array("status"=>SCS);
                http_response_code(200);
            }else{
                $result = array("status"=>UPDATEQUERYFAIL);
                http_response_code(400);
            }
        }
        
    }else{ // INSERT CASE
        
        // START CHECK PRIORITY LEVEL FOR LEVEL 1 PRIORITY
        $flag = false;
        if(!empty($priorityLevel) && $priorityLevel != 1){
            if($categoryId == 23){
                $sqlCheckPrio = "SELECT SM.THRID_PARTY_FLAG,SM.CANCEL_REMARK_FLAG,(SELECT COUNT(*) FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND PRIORITY_LEVEL = '".$priorityLevel."' AND FK_CATEGORY_ID = '".$categoryId."') STATUSCOUNT FROM ".STATUSMANAGEMENT." SM WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND PRIORITY_LEVEL = '".$priorityLevel."' AND FK_CATEGORY_ID = '".$categoryId."' ORDER BY PK_STATUS_ID DESC";
                $resultPriorityLevel = fetch_rec_query($sqlCheckPrio);
    //            print_r($resultPriorityLevel); exit;
                if(count($resultPriorityLevel) > 0){
                    foreach($resultPriorityLevel as $key=>$value){
                        if(($value['THRID_PARTY_FLAG'] == 1 || $value['CANCEL_REMARK_FLAG'] == 1) && $value['STATUSCOUNT'] > 1){
                            $result = array("status"=>NOTALLOWEDPRIORITY);
                            http_response_code(400);
                            $flag = false;
                        }else if($value['STATUSCOUNT'] >= 1){
                            if(($value['THRID_PARTY_FLAG'] == 1 || $value['CANCEL_REMARK_FLAG'] == 1)){
                                $flag = true;
                            }else{
                                $result = array("status"=>NOTALLOWEDPRIORITY);
                                http_response_code(400);
                                $flag = false;
                            }
                        }else{
                            $flag = true;
                        }
                    }
                }else{
                    $flag = true;
                }
            }else{
                $flag = true;
            }
            
        }else{
            $sqlCheckPrio = "SELECT SM.PK_STATUS_ID,(SELECT COUNT(*) FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND PRIORITY_LEVEL = '".$priorityLevel."' AND FK_CATEGORY_ID = '".$categoryId."') STATUSCOUNT FROM ".STATUSMANAGEMENT." SM WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND PRIORITY_LEVEL = '".$priorityLevel."' AND FK_CATEGORY_ID = '".$categoryId."' ORDER BY PK_STATUS_ID DESC";
            $resultPriorityLevel = fetch_rec_query($sqlCheckPrio);
            if(count($resultPriorityLevel) > 0){
                 if($resultPriorityLevel[0]['STATUSCOUNT'] >= 1){
                    $result = array("status"=>NOTALLOWEDPRIORITY);
                    http_response_code(400);
                    $flag = false;
                 }
                     
            }else{
                $flag = true;
            }
        }
        
        if(!empty($priorityLevel) && $priorityLevel != 1 && $categoryId == 23){
            $sqlCheckPrio = "SELECT THRID_PARTY_FLAG FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND PRIORITY_LEVEL = '".($priorityLevel-1)."' AND FK_CATEGORY_ID = '".$categoryId."' ";
            $resultPriorityLevel = fetch_rec_query($sqlCheckPrio);
            if(count($resultPriorityLevel)>0){
                if($resultPriorityLevel[0]['THRID_PARTY_FLAG'] == 1){
                    $result = array("status"=>NOTALLOWEDTOADDSTATUS);
                    http_response_code(400);
                    $flag = false;
                }
            }
        }
        
//        }else{
//            $flag = true;
//        }
        //echo $flag; exit;
        if($flag){
            //ADDED BY MONTU ON 26-10-2016 FOR CHECK PRIORITY LEVEL SAME OR NOT
            $sqlGetLevel = "SELECT PRIORITY_LEVEL FROM ".STATUSMANAGEMENT." WHERE FK_CATEGORY_ID = '".$categoryId."' AND FK_CLIENT_ID = '".$clientId."' AND PRIORITY_LEVEL = '".$priorityLevel."' AND DELETE_FLAG=0 ";
            $fetchLevel = fetch_rec_query($sqlGetLevel);
            if(count($fetchLevel) > 0) {
                $result = array("status"=>DUBLICATELEVEL);
                http_response_code(400);
                return $result;
            } else {
                $insertStatus = array();
                $insertStatus['STATUS_NAME'] = ucfirst(strtolower($statusName)); 
                $insertStatus['FK_CATEGORY_ID'] = $categoryId; 
                $insertStatus['PRIORITY_LEVEL'] = $priorityLevel; 
                $insertStatus['THRID_PARTY_FLAG'] = "0"; 
                $insertStatus['FK_CLIENT_ID'] = $clientId; 
                $insertStatus['CREATED_BY'] = $userId; 
                $insertStatus['CREATED_BY_DATE'] = time(); 
                $insertStatus['DELETE_FLAG'] = 0;
                // insert
                $insertStatusRequest = insert_rec(STATUSMANAGEMENT, $insertStatus);
                 if($insertStatusRequest){
                    $finalJson = array("statusid"=>$insertStatusRequest['lastInsertedId']);
                    $result = array("status"=>SCS,"data"=>$finalJson);
                    http_response_code(200);
                }else{
                    $result = array("status"=>INSERTQUERYFAIL);
                    http_response_code(400);
                }
            }
        }
        
    }
    return $result;
}

function deleteStatus($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";    
    $statusId = isset($postData['postData']['statusId']) ? addslashes(trim($postData['postData']['statusId'])) : "";
    $catId = isset($postData['postData']['catId']) ? addslashes(trim($postData['postData']['catId'])) : "";//CATEGORY ID STATUS 23=INQ , 24=PROPOASL , 40=ORDER , 41=SERVICE , 45=LEAD , 46=EXPENSE
    
    if(!empty($statusId)){
        
        $deleteFlag = '1';//FOR DELTE THE DATA
//        print_r($deleteFlag);
//        exit;
        if(isset($deleteFlag) && $deleteFlag == '1'){
            $updateStatus = array();
            $updateStatus['CHANGED_BY'] = $userId; 
            $updateStatus['CHANGED_BY_DATE'] = time(); 
            $updateStatus['DELETE_FLAG'] = 1;
            // WHERE
            $updateStatusWhere = "PK_STATUS_ID = '".$statusId."' AND FK_CLIENT_ID = '".$clientId."'";
            // update
            $updateStatusRequest = update_rec(STATUSMANAGEMENT, $updateStatus, $updateStatusWhere);
            if($updateStatusRequest){
                $result = array("status"=>SCS);
                http_response_code(200);

            }else{
                $result = array("status"=>UPDATEQUERYFAIL);
                http_response_code(400);
            }
        }
    }else{
        $result = array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}

function getStatusListing($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";    
    $statusId = isset($postData['postData']['statusId']) ? addslashes(trim($postData['postData']['statusId'])) : "";    
    // START GETTING STATUS
    $statusDataWhere = "";
    if(!empty($statusId)){
        $statusDataWhere = " AND PK_STATUS_ID = '".$statusId."'";
    }
//    $sqlStatus = "SELECT * FROM ".STATUSMANAGEMENT." SM INNER JOIN ".LOOKUPMASTER." LM ON LM.PK_LOOKUP_ID = SM.FK_CATEGORY_ID WHERE SM.DELETE_FLAG = 0 AND SM.FK_CLIENT_ID = '".$clientId."' ".$statusDataWhere." ";
    $sqlStatus = "SELECT SM.PK_STATUS_ID,SM.STATUS_NAME,SM.FK_CATEGORY_ID,SM.PRIORITY_LEVEL,SM.THRID_PARTY_FLAG,SM.CANCEL_REMARK_FLAG,SM.APPROVAL_FLAG,SM.FK_USER_ID,SM.SMS_ACT_FLAG,SM.EMAIL_ACT_FLAG,LM.VALUE FROM ".STATUSMANAGEMENT." SM INNER JOIN ".LOOKUPMASTER." LM ON LM.PK_LOOKUP_ID = SM.FK_CATEGORY_ID WHERE SM.DELETE_FLAG = 0 AND SM.FK_CLIENT_ID = '".$clientId."' ".$statusDataWhere." ORDER BY SM.FK_CATEGORY_ID,SM.PRIORITY_LEVEL ";
    $resultStatus = fetch_rec_query($sqlStatus);
    if(count($resultStatus) > 0){
        $finalJson = array();
        //for($i=0;$i<count($resultStatus);$i++){
        foreach($resultStatus as $keyStatus => $valueStatus){
            $finalJson[$keyStatus]['statusId'] = $valueStatus['PK_STATUS_ID'];
            $finalJson[$keyStatus]['statusName'] = $valueStatus['STATUS_NAME'];
            $finalJson[$keyStatus]['statusCatName'] = $valueStatus['VALUE'];
            $finalJson[$keyStatus]['statusCatId'] = $valueStatus['FK_CATEGORY_ID'];
            $finalJson[$keyStatus]['statusPriorityLevel'] = $valueStatus['PRIORITY_LEVEL'];
            $finalJson[$keyStatus]['statusThirdPartyFlag'] = $valueStatus['THRID_PARTY_FLAG'];
            $finalJson[$keyStatus]['cancelRemarkFlag'] = $valueStatus['CANCEL_REMARK_FLAG'];
            $finalJson[$keyStatus]['approvalFlag'] = $valueStatus['APPROVAL_FLAG'];
            $finalJson[$keyStatus]['userId'] = $valueStatus['FK_USER_ID'];
            $finalJson[$keyStatus]['smsFlag'] = $valueStatus['SMS_ACT_FLAG'];
            $finalJson[$keyStatus]['emailFlag'] = $valueStatus['EMAIL_ACT_FLAG'];
        }
        $result = array("status"=>SCS,"data"=>$finalJson);
        http_response_code(200);
    }else{
       $result = array("status"=>NORECORDS); 
       http_response_code(400);
    }
    return $result;
}

function changeThirdPartyFlag($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";    
    $statusId = isset($postData['postData']['statusId']) ? addslashes(trim($postData['postData']['statusId'])) : "";    
    $categoryId = isset($postData['postData']['categoryId']) ? addslashes(trim($postData['postData']['categoryId'])) : "";    
    $thirdPartyFlag = isset($postData['postData']['thirdPartyFlag']) ? addslashes(trim($postData['postData']['thirdPartyFlag'])) : ""; 
    $flagType = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['flagType'])) : ""; 
    if(!empty($statusId)){
        if($flagType == 1){ // THIRD PARTY 
//            $sqlCheckTPF = "SELECT * FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$categoryId."' AND THRID_PARTY_FLAG = '1' LIMIT 0,1";
            $sqlCheckTPF = "SELECT PK_STATUS_ID FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$categoryId."' AND THRID_PARTY_FLAG = '1' LIMIT 0,1";
            $resultCheckTPF = fetch_rec_query($sqlCheckTPF);
            if(count($resultCheckTPF) > 0){
               if($resultCheckTPF[0]['PK_STATUS_ID'] == $statusId){
                    $flag = true;
                }else{
                    $result = array("status"=>ALREADYSETTPF);
                    http_response_code(400);
                    $flag = false;
                }
            }else{
                $flag = true;
            }

            if($flag){
                $updateStatus = array();
                $updateStatus['CHANGED_BY'] = $userId; 
                $updateStatus['CHANGED_BY_DATE'] = time(); 
                $updateStatus['THRID_PARTY_FLAG'] = $thirdPartyFlag;
                // WHERE
                $updateStatusWhere = "PK_STATUS_ID = '".$statusId."' AND FK_CLIENT_ID = '".$clientId."'";
                // update
                $updateStatusRequest = update_rec(STATUSMANAGEMENT, $updateStatus, $updateStatusWhere);
                if($updateStatusRequest){
                    $result = array("status"=>SCS);
                    http_response_code(200);
                }else{
                    $result = array("status"=>UPDATEQUERYFAIL);
                    http_response_code(400);
                }
            }
        }else if($flagType == 2){ // CANCEL REMARK 
            
//            $sqlCheckTPF = "SELECT * FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$categoryId."' AND CANCEL_REMARK_FLAG = '1' LIMIT 0,1 ";
            $sqlCheckTPF = "SELECT PK_STATUS_ID FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$categoryId."' AND CANCEL_REMARK_FLAG = '1' LIMIT 0,1 ";
            $resultCheckTPF = fetch_rec_query($sqlCheckTPF);
            //print_r($resultCheckTPF); exit;
            if(count($resultCheckTPF) > 0){
                if($resultCheckTPF[0]['PK_STATUS_ID'] == $statusId){
                    $flag = true;
                }else{
                    $result = array("status"=>ALREADYSETCRF);
                    http_response_code(400);
                    $flag = false;
                }
            }else{
                $flag = true;
            }
            if($flag){
                $updateStatus = array();
                $updateStatus['CHANGED_BY'] = $userId; 
                $updateStatus['CHANGED_BY_DATE'] = time(); 
                $updateStatus['CANCEL_REMARK_FLAG'] = $thirdPartyFlag;
                // WHERE
                $updateStatusWhere = "PK_STATUS_ID = '".$statusId."' AND FK_CLIENT_ID = '".$clientId."'";
                // update
                $updateStatusRequest = update_rec(STATUSMANAGEMENT, $updateStatus, $updateStatusWhere);
                if($updateStatusRequest){
                    $result = array("status"=>SCS);
                    http_response_code(200);
                }else{
                    $result = array("status"=>UPDATEQUERYFAIL);
                    http_response_code(400);
                }
            }
        }
        else if($flagType == 3){
//            $sqlCheckTPF = "SELECT * FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$categoryId."' AND APPROVAL_FLAG = '1' LIMIT 0,1 ";
            $sqlCheckTPF = "SELECT PK_STATUS_ID FROM ".STATUSMANAGEMENT." WHERE FK_CLIENT_ID = '".$clientId."' AND DELETE_FLAG = 0 AND FK_CATEGORY_ID = '".$categoryId."' AND APPROVAL_FLAG = '1' LIMIT 0,1 ";
            $resultCheckTPF = fetch_rec_query($sqlCheckTPF);
            //print_r($resultCheckTPF); exit;
            if(count($resultCheckTPF) > 0){
                if($resultCheckTPF[0]['PK_STATUS_ID'] == $statusId){
                    $flag = true;
                }else{
                    $result = array("status"=>ALREADYSETAF);
                    http_response_code(400);
                    $flag = false;
                }
            }else{
                $flag = true;
            }
            if($flag){
                $updateStatus = array();
                $updateStatus['CHANGED_BY'] = $userId; 
                $updateStatus['CHANGED_BY_DATE'] = time(); 
                $updateStatus['APPROVAL_FLAG'] = $thirdPartyFlag;
                // WHERE
                $updateStatusWhere = "PK_STATUS_ID = '".$statusId."' AND FK_CLIENT_ID = '".$clientId."'";
                // update
                $updateStatusRequest = update_rec(STATUSMANAGEMENT, $updateStatus, $updateStatusWhere);
                if($updateStatusRequest){
                    $result = array("status"=>SCS);
                    http_response_code(200);
                    
                }else{
                    $result = array("status"=>UPDATEQUERYFAIL);
                    http_response_code(400);
                }
            }
        }
       
    }else{
        $result= array("status"=>NORECORDS);
        http_response_code(400);
    }
    return $result;
}


function updateStatusOnClick($postData){
    
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $stagingId = isset($postData['postData']['alertId']) ? addslashes(trim($postData['postData']['alertId'])) : "";
    
    // WHERE
    $where = "PK_STAGING_ID IN (".$stagingId.") AND FK_CLIENT_ID = '".$clientId."'";
//    $updateRequest = update_rec(ALERTSTAGINGDATA, $updateArr, $where);
    $updateRequest = delete_rec(ALERTSTAGINGDATA, $where);
    if($updateRequest){
        $result = array("status"=>SCS);
        http_response_code(200);
    }else{
        $result = array("status"=>UPDATEQUERYFAIL);
        http_response_code(400);
    }
    return $result;
}