<?php

/* * ************************************************ */
/*   AUTHOR : LITHE TECHNOLOGY PVT. LTD.              */
/*   DEVELOPED BY : SATISH KARENA                     */
/*   CREATION DATE : 18-JUL-2018                      */
/*   FILE TYPE : PHP                                  */
/* * ************************************************ */


/* ========== USED CODE INTO INDICAMPUS START ============== */

//FUNCTION FOR Add/Edit Item Detail. 
function addEditEvent($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $itemName = isset($postData['postData']['itemName']) ? addslashes(trim($postData['postData']['itemName'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $price = isset($postData['postData']['price']) ? addslashes(trim($postData['postData']['price'])) : "";

    $itemType = isset($postData['postData']['itemType']) ? addslashes(trim($postData['postData']['itemType'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $description = isset($postData['postData']['description']) ? addslashes(trim($postData['postData']['description'])) : "";
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "";
    $cgst = isset($postData['postData']['cgst']) ? addslashes(trim($postData['postData']['cgst'])) : "";
    $igst = isset($postData['postData']['igst']) ? addslashes(trim($postData['postData']['igst'])) : "";
    $sgst = isset($postData['postData']['sgst']) ? addslashes(trim($postData['postData']['sgst'])) : "";
    $hsnCode = isset($postData['postData']['hsnCode']) ? addslashes(trim($postData['postData']['hsnCode'])) : "";
    $eventStartDateTime = isset($postData['postData']['eventStartDateTime']) ? addslashes(trim($postData['postData']['eventStartDateTime'])) : '';
    $eventEndDateTime = isset($postData['postData']['eventEndDateTime']) ? addslashes(trim($postData['postData']['eventEndDateTime'])) : ''; //
    $eventVenue = isset($postData['postData']['eventVenue']) ? addslashes(trim($postData['postData']['eventVenue'])) : ''; //
    $maxNosOfBook = isset($postData['postData']['maxNosOfBook']) ? addslashes(trim($postData['postData']['maxNosOfBook'])) : ''; //
    $priceStartDate = date("Y-m-d", strtotime($eventStartDateTime));
    $itemValidityEndDate = date("Y-m-d", strtotime($eventEndDateTime));


    if (strtotime(date("Y-m-d H:i:s", strtotime($eventStartDateTime))) < strtotime(date("Y-m-d H:i:s", strtotime($eventEndDateTime)))) {

        if (!empty($clientId) && !empty($orgId) && !empty($itemName) && !empty($price) && !empty($eventStartDateTime) && !empty($eventEndDateTime) && !empty($eventVenue) && !empty($qty)) { // CHECK REQURIED FIELD CONDITION
            if (is_numeric($cgst) && is_numeric($sgst) && is_numeric($price) && is_numeric($qty)) {
                $whereCond = "";
                if (!empty($itemId)) {
                    $whereCond = " AND PK_ITEM_ID !=" . $itemId;
                    $bookedQty = 0;
                    $sqlCheckEventBooking = "SELECT IF(QTY> 0,SUM(QTY),0) AS QTY FROM " . EVENTSBOOKING . " WHERE FK_ITEM_ID ='" . $itemId . "'"; // QUERY FOR TOTAL BOOKED SEATS .
                    $resCheckEventBooking = fetch_rec_query($sqlCheckEventBooking); // RESULT FOR TOTAL BOOKED SEATS .

                    $bookedQty = $resCheckEventBooking[0]['QTY'];
                    if ($bookedQty > 0) {

                        if ($qty < $bookedQty) {
                            $result = array("status" => QTYISNOTLESSTHANBOOKEDSEATS);
                            http_response_code(400);
                            return $result;
                        }
                    }
                }

                $sqlCheckItemExist = "SELECT PK_ITEM_ID	FROM " . ITEMMASTER . " WHERE ITEM_NAME ='" . $itemName . "' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "'" . $whereCond . " AND FK_ORG_ID ='" . $orgId . "' AND ITEM_TYPE ='EVENTS' AND ITEM_VALIDITY_END_DATE <= '" . $itemValidityEndDate . "'"; // QUERY FOR ITEM NAME IS ALREADY AVAILABLE OR NOT.
                $resCheckItemExist = fetch_rec_query($sqlCheckItemExist); // RESULT FOR ITEM NAME IS ALREADY AVAILABLE OR NOT.
                if (count($resCheckItemExist) > 0) {
                    $result = array("status" => EVENTNAMEISEXIST);
                    http_response_code(200);
                } else {
                    $msg = "";
// ITEM INSERT ARRAY.
                    $insertItemArray = Array(); //ITEM ARRAY CREATE
                    $insertItemArray['FK_CLIENT_ID'] = $clientId;
                    $insertItemArray['EVENT_NO'] = generateId(EVENTMASTER, "EVE", $clientId);
                    ;
                    $insertItemArray['FK_ORG_ID'] = $orgId;
                    $insertItemArray['ITEM_NAME'] = $itemName;
                    $insertItemArray['ITEM_PRICE'] = $price;
                    $insertItemArray['DESCRIPTION'] = $description;
                    $insertItemArray['QTY'] = $qty;
                    $insertItemArray['ITEM_TYPE'] = $itemType;
                    $insertItemArray['PRICE_START_DATE'] = $priceStartDate;
                    $insertItemArray['FK_USER_ID'] = $userId;
                    $insertItemArray['ITEM_VALIDITY_END_DATE'] = $itemValidityEndDate;
                    $insertItemArray['EVENT_START_DATETIME'] = strtotime($eventStartDateTime);
                    $insertItemArray['EVENT_END_DATETIME'] = strtotime($eventEndDateTime);
                    $insertItemArray['EVENT_VENUE'] = $eventVenue;
                    $insertItemArray['CGST'] = $cgst;
                    $insertItemArray['SGST'] = $sgst;
                    $insertItemArray['IGST'] = $igst;
                    $insertItemArray['HSN_CODE'] = $hsnCode;
                    $insertItemArray['MAX_NOS_OF_BOOK'] = $maxNosOfBook;

                    if (isset($itemId) && $itemId > 0) {
                        $insertItemArray['CHANGED_BY'] = $userId;
                        $insertItemArray['CHANGED_BY_DATE'] = time();
                        $whereItemCond = "PK_ITEM_ID ='" . $itemId . "'";
                        $updateItem = update_rec(ITEMMASTER, $insertItemArray, $whereItemCond); // UPDATE RECORD IN ITEM_MASTER TABLE.
                        if (!$updateItem) {
                            $msg = UPDATEFAIL . " " . ITEMMASTER;
                        }
                    } else {
                        $insertItemArray['CREATED_BY'] = $userId;
                        $insertItemArray['CREATED_BY_DATE'] = time();
                        $insertItem = insert_rec(ITEMMASTER, $insertItemArray); // INSERT RECORD IN ITEM_MASTER TABLE.          
                        if (!isset($insertItem['lastInsertedId']) || $insertItem['lastInsertedId'] == 0) {
                            $msg = INSERTFAIL . " " . ITEMMASTER;
                        }
                    }

                    if ($msg == "") {
                        $requestData = $studentData = Array();
                        $requestData['postData']['clientId'] = $clientId;
                        $requestData['postData']['orgId'] = $orgId;
                        $requestData['postData']['itemType'] = $itemType;

                        $itemData = getItemsData($requestData); // GET ALL ROOM DATA.

                        $itemAllData = ($itemData['status'] == SCS) ? $itemData['data'] : $itemData['status'];

                        $result = array("status" => SCS, "data" => $itemAllData);
                        http_response_code(200);
                    } else {
                        $result = array("status" => $msg);
                        http_response_code(400);
                    }
                }
            } else {
                $result = array("status" => EVENTDATAISALLOWNOLYNUMBER);
                http_response_code(400);
            }
        } else {
            $result = array("status" => PARAM);
            http_response_code(400);
        }
    } else {
        $result = array("status" => EVENTSTARTDATEISNOTGRATERTHANENDDATE);
        http_response_code(400);
    }

    return $result;
}

//FUNCTION FOR Add/Edit Events Booking Detail. 
function addEditEventsBooking($postData) {
// POST DATA
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $qty = isset($postData['postData']['qty']) ? addslashes(trim($postData['postData']['qty'])) : "";


    if (!empty($clientId) && !empty($orgId) && !empty($qty) && !empty($itemId) && !empty($studentId)) { // CHECK REQURIED FIELD CONDITION
        $todayDate = date("Y-m-d");

        $sqlCheckUserForEventBook = " SELECT IS_EVENT_BOOKING FROM " . STUDENTSUSPENSION . " WHERE FK_STUDENT_ID ='" . $studentId . "' AND SUSPENSION_END_DATE >='" . $todayDate . "' AND REVOKE_BY ='0' AND DELETE_FLAG ='0' AND FK_CLIENT_ID='" . $clientId . "' ORDER BY CHANGED_BY_DATE DESC LIMIT 0,1";

        $resCheckUserForEventBook = fetch_rec_query($sqlCheckUserForEventBook);

        if (count($resCheckUserForEventBook) > 0) {
            $isEventBook = $resCheckUserForEventBook[0]['IS_EVENT_BOOKING'];
            if ($isEventBook == 0) {
                $result = array("status" => EVENTISNOTALLOWFORBOOKING);
                http_response_code(400);
                return $result;
            }
        }

        $sqlCheckStudentForEventBook = " SELECT IM.MAX_NOS_OF_BOOK FROM " . ITEMMASTER . " IM WHERE IM.PK_ITEM_ID='" . $itemId . "' AND IM.DELETE_FLAG ='0' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID ='" . $orgId . "' AND IM.ITEM_TYPE='EVENTS' LIMIT 0,1";
        $resCheckStudentForEventBook = fetch_rec_query($sqlCheckStudentForEventBook);

        if (count($resCheckStudentForEventBook) > 0) {
            $maxNosSeatsBook = $resCheckStudentForEventBook[0]['MAX_NOS_OF_BOOK'];
            if ($qty > $maxNosSeatsBook) {
                $result = array("status" => BOOKINGISNOTAVAILABLE . " Because of maximum Nos for  booking is :" . intval($maxNosSeatsBook));
                http_response_code(400);
                return $result;
            }
        }




        $sqlCheckSeatsExist = " SELECT IM.QTY AS TOTAL_SEATS,IF(SUM(EB.QTY)>0,SUM(EB.QTY),0) AS TOTAL_BOOKING  FROM " . ITEMMASTER . " IM LEFT JOIN " . EVENTSBOOKING . " EB ON IM.PK_ITEM_ID =EB.FK_ITEM_ID WHERE IM.PK_ITEM_ID='" . $itemId . "' AND IM.DELETE_FLAG ='0' AND IM.FK_CLIENT_ID='" . $clientId . "' AND IM.FK_ORG_ID ='" . $orgId . "' AND IM.ITEM_TYPE='EVENTS' AND ITEM_VALIDITY_END_DATE >='" . $todayDate . "' GROUP BY(EB.FK_ITEM_ID)";

        $resCheckSeatsExist = fetch_rec_query($sqlCheckSeatsExist); // RESULT FOR PROPERTY NAME IS ALREADY AVAILABLE OR NOT.
        if (count($resCheckSeatsExist) > 0) {

            $totalSeats = $resCheckSeatsExist[0]['TOTAL_SEATS'];
            $totalBooking = $resCheckSeatsExist[0]['TOTAL_BOOKING'];
            if ((intval($totalBooking) + intval($qty)) > intval($totalSeats)) {
                $totalSeatAvailabel = intval($totalSeats) - intval($totalBooking);
                $result = array("status" => BOOKINGISNOTAVAILABLE . " Because of total booking available is :" . intval($totalSeatAvailabel));
                http_response_code(400);
                return $result;
            } else {
                $msg = "";
// EVENTS BOOKING INSERT ARRAY.

                $insertBookingArray = Array(); //EVENTS  BOOKING ARRAY CREATE
                $insertBookingArray['FK_CLIENT_ID'] = $clientId;
                $insertBookingArray['FK_ORG_ID'] = $orgId;
                $insertBookingArray['FK_STUD_ID'] = $studentId;
                $insertBookingArray['FK_ITEM_ID'] = $itemId;
                $insertBookingArray['BOOKING_DATE'] = date("Y-m-d");
                $insertBookingArray['BOOKING_NO'] = generateId(EVENTSBOOKING, "BOOK", $clientId);
                ;
                $insertBookingArray['QTY'] = $qty;
                $insertBookingArray['CREATED_BY'] = $userId;
                $insertBookingArray['CREATED_BY_DATE'] = time();

                $insertBooking = insert_rec(EVENTSBOOKING, $insertBookingArray); // INSERT RECORD IN PROPERTY_MASTER TABLE.          
                if (!isset($insertBooking['lastInsertedId']) || $insertBooking['lastInsertedId'] == 0) {
                    $msg = INSERTFAIL . " " . EVENTSBOOKING;
                } else {
                    $checkItemData = "SELECT PK_ITEM_ID,ITEM_NAME,ITEM_PRICE,ITEM_TYPE,CGST,SGST,IGST,HSN_CODE FROM " . ITEMMASTER . " WHERE DELETE_FLAG='0' AND PK_ITEM_ID ='" . $itemId . "'";
                    $resCheckItemData = fetch_rec_query($checkItemData);
                    if (count($resCheckItemData) > 0) {
                        foreach ($resCheckItemData as $keyItem => $valueItem) {

                            $sqlStudentData = "SELECT CONCAT(FIRST_NAME,' ',LAST_NAME) AS STUD_NAME FROM " . STUDENTMASTER . " WHERE DELETE_FLAG='0' AND PK_STUD_ID ='" . $studentId . "'"; //QUERY FOR  FETCH STUDENT NAME 
                            $resStudentData = fetch_rec_query($sqlStudentData); // RESULT FOR  FETCH STUDENT NAME   

                            $itemName = $valueItem['ITEM_NAME'];
                            $itemPrice = $valueItem['ITEM_PRICE'];
                            $itemCgst = $valueItem['CGST'];
                            $itemSgst = $valueItem['SGST'];
                            $itemIgst = $valueItem['IGST'];
                            $hsnCode = $valueItem['HSN_CODE'];
                            $qtyOfSession = ($qty) ? $qty : 1;
                            $calItemBasicTPrice = $itemPrice * $qtyOfSession;
                            $cgstAmt = ($itemCgst > 0) ? ($calItemBasicTPrice * $itemCgst) / 100 : 0;
                            $sgstAmt = ($itemSgst > 0) ? ($calItemBasicTPrice * $itemSgst) / 100 : 0;
                            $igstAmt = ($itemIgst > 0) ? ($calItemBasicTPrice * $itemIgst) / 100 : 0;

                            $calTotalTax = $cgstAmt + $sgstAmt + $igstAmt;
                            $calItemTotalPrice = $calItemBasicTPrice + $calTotalTax;

                            /* START Code for Set user id when complaint and request create from student portal */
                            if (empty($userId)) {
                                $sqlUserData = "SELECT UM.PK_USER_ID FROM " . USERMASTER . " UM INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID=UM.USER_LEVEL AND CL.LOOK_TYPE ='USERLEVEL' AND CL.LOOK_VALUE ='Branch Manager' WHERE UM.FK_CLIENT_ID ='" . $clientId . "' AND UM.FK_ORG_ID ='" . $orgId . "' LIMIT 0,1"; // QUERY FOR BRANCH MANAGER
                                $resUserData = fetch_rec_query($sqlUserData);
                                if (count($resUserData) > 0) {
                                    $userId = $resUserData[0]['PK_USER_ID'];
                                } else if (count($resUserData) == 0) {
                                    $sqlUserData = "SELECT UM.PK_USER_ID FROM " . USERMASTER . " UM INNER JOIN " . CLIENTLOOKUP . " CL ON CL.PK_CLT_LKUP_ID=UM.USER_LEVEL AND CL.LOOK_TYPE ='USERLEVEL' AND CL.LOOK_VALUE ='CEO' WHERE UM.FK_CLIENT_ID ='" . $clientId . "' AND UM.FK_ORG_ID ='" . $orgId . "' LIMIT 0,1"; // QUERY FOR CEO
                                    $resUserData = fetch_rec_query($sqlUserData);
                                    if (count($resUserData) > 0) {
                                        $userId = $resUserData[0]['PK_USER_ID'];
                                    }
                                }
                            }

                            /* END Code for Set user id when complaint and request create from student portal */

                            $insertChargePosting = Array();
                            $insertChargePostingArray = Array(); //CHARGE POSTING ARRAY CREATE
                            $insertChargePostingArray['FK_CLIENT_ID'] = $clientId;
                            $insertChargePostingArray['FK_ORG_ID'] = $orgId;
                            $insertChargePostingArray['TYPEOFCUST'] = 'student';
                            $insertChargePostingArray['FK_ITEM_ID'] = $itemId;
                            $insertChargePostingArray['REF_ITEM_ID'] = $itemId;
                            $insertChargePostingArray['ITEM_NAME'] = $itemName;
                            $insertChargePostingArray['CHARGE_DATE'] = date("Y-m-d");
                            $insertChargePostingArray['STUDENT_ID'] = $studentId;
                            $insertChargePostingArray['STUDENT_FULL_NAME'] = $resStudentData['0']['STUD_NAME'];
                            $insertChargePostingArray['QTY_OF_SESSION'] = $qty;
                            $insertChargePostingArray['BIILED_SESSION'] = $qty;
                            $insertChargePostingArray['TAX_CGST'] = $itemCgst;
                            $insertChargePostingArray['TAX_SGST'] = $itemSgst;
                            $insertChargePostingArray['TAX_IGST'] = $itemIgst;
                            $insertChargePostingArray['HSN_CODE'] = $hsnCode;
                            $insertChargePostingArray['ITEM_BASE_PRICE'] = $itemPrice;
                            $insertChargePostingArray['TOTAL_TAX'] = $calTotalTax;
                            $insertChargePostingArray['ITEM_TOTAL_PRICE'] = $calItemTotalPrice;
                            $insertChargePostingArray['TYPE'] = 'EVENTS';
                            $insertChargePostingArray['CREATED_BY'] = $userId;
                            $insertChargePostingArray['CREATED_BY_DATE'] = time();

                            $insertChargePosting = insert_rec(CHARGEMASTER, $insertChargePostingArray); // INSERT RECORD IN CHARGE_MASTER TABLE.

                            if ($insertChargePosting) {
                                $unbilledAmt = 0;
                                $sqlStudentData = "SELECT RECEIPT_AMT,UNBILLED_AMT,BILL_AMT,CLOSINGBAL_AMT FROM " . STUDENTMASTER . " WHERE PK_STUD_ID ='" . $studentId . "'";
                                $resStudentData = fetch_rec_query($sqlStudentData); // 
                                if (count($resStudentData) > 0) {
                                    $unbilledAmt = $resStudentData[0]['UNBILLED_AMT'];
                                }


                                $updateStudentArr = ARRAY();
                                $updateStudentArr['UNBILLED_AMT'] = floatval($calItemTotalPrice) + floatval($unbilledAmt);
                                $updateStudentArr['UNBILLED_TYPE'] = 'DR';
                                $updateStudentArr['CHANGED_BY'] = $userId;
                                $updateStudentArr['CHANGED_BY_DATE'] = time();
                                $whereStudCond = "PK_STUD_ID='" . $studentId . "'";
                                $updateStud = update_rec(STUDENTMASTER, $updateStudentArr, $whereStudCond);
                                if ($updateStud) {

                                    /*  $requestData = $studentData = Array();
                                      $requestData['postData']['clientId'] = $clientId;
                                      $requestData['postData']['orgId'] = $orgId;
                                      $requestData['postData']['userId'] = $userId;
                                      $requestData['postData']['invoiceStartDate'] = date('Y-m-d');
                                      $requestData['postData']['invoiceEndDate'] = date('Y-m-d');
                                      $requestData['postData']['chargeStartDate'] = date('Y-m-d');
                                      $requestData['postData']['chargeEndDate'] = date('Y-m-d');
                                      $requestData['postData']['invoiceName'] = 'Event Booking -' . $studentId;
                                      $requestData['postData']['invoiceType'] = 'EVENTS';
                                      $requestData['postData']['itemId'] = $itemId;
                                      $requestData['postData']['qty'] = $qty;
                                      $requestData['postData']['chargeType'] = 'EVENTS';

                                      $chargeMasterData = createInvoice($requestData); // GET ALL CHARGE MASTER DATA.
                                      //  echo "<pre>"; print_r($chargeMasterData); exit;
                                      $invoiceData = ($chargeMasterData['status'] == SCS) ? $chargeMasterData['inseterdInvoiceId'] : $chargeMasterData['status'];
                                      if (!empty($chargeMasterData['inseterdInvoiceId'])) {
                                      $insertEventArray = Array();
                                      $insertEventArray['CHANGED_BY'] = $userId;
                                      $insertEventArray['CHANGED_BY_DATE'] = time();
                                      $insertEventArray['FK_INVOICE_ID'] = $chargeMasterData['inseterdInvoiceId'];
                                      $whereItemCond = "PK_BOOKING_ID ='" . $insertBooking['lastInsertedId'] . "'";
                                      $updateItem = update_rec(EVENTSBOOKING, $insertEventArray, $whereItemCond); // UPDATE RECORD IN EVENT BOOKING MASTER TABLE.
                                      } */

                                    $result = array("status" => SCS, "data" => $insertChargePosting);
                                    http_response_code(200);
                                } else {
                                    $result = array("status" => UPDATEFAIL . " " . STUDENTMASTER);
                                    http_response_code(400);
                                }
                            } else {
                                $result = array("status" => INSERTFAIL . " " . CHARGEMASTER);
                                http_response_code(400);
                            }
                        }
                    }
                }
            }
        } else {

            $result = array("status" => EVENTNOTFOUND);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//ADD BY DEVIKA.1.8.2018
function updateEventStatus($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $eventId = isset($postData['postData']['eventId']) ? addslashes(trim($postData['postData']['eventId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $status = isset($postData['postData']['status']) ? addslashes(trim($postData['postData']['status'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($eventId)) {

        $sqlEventBooked = "SELECT PK_BOOKING_ID FROM " . EVENTSBOOKING . " WHERE FK_ITEM_ID='" . $eventId . "' AND FK_CLIENT_ID='" . $clientId . "' AND FK_ORG_ID='" . $orgId . "' AND DELETE_FLAG ='0'";
        $resEventBooked = fetch_rec_query($sqlEventBooked); //  
        if (count($resEventBooked) == 0 || $status == 1) {

            $updateEventStatusArray = Array(); //ATTACHMENT ARRAY UPDATE
            $updateEventStatusArray['IS_ACTIVE'] = $status; //0 : Inactive , 1 : Active
            $updateEventStatusArray['CHANGED_BY'] = $eventId;
            $updateEventStatusArray['CHANGED_BY_DATE'] = time();
            //print_r($updateEventStatusArray); die;
            $whereEventStatusUpdate = "DELETE_FLAG ='0' AND PK_ITEM_ID ='" . $eventId . "'";
            $updateStatus = update_rec(ITEMMASTER, $updateEventStatusArray, $whereEventStatusUpdate); // UPDATE DELETE_FLAG =1.
            if (!$updateStatus) {
                $result = array("status" => UPDATEFAIL . " " . ITEMMASTER);
                http_response_code(400);
            } else {
                $result = array("status" => SCS);
                http_response_code(200);
            }
        } else {
            $result = array("status" => EVENTISDELETE);
            http_response_code(400);
        }
    }
    return $result;
}

//ADD BY DEVIKA.1.8.2018
function listEventNoSearch($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $keyWord = isset($postData['postData']['keyWord']) ? addslashes(trim($postData['postData']['keyWord'])) : "";

    $eventNoArr = array();

    if (!empty($clientId) && !empty($orgId) && !empty($keyWord) && strlen($keyWord) >= 2) {
        $whereEventNoCond = "";
        if (!empty($keyWord)) {
            $whereEventNoCond = " AND EVENT_NO LIKE '%" . $keyWord . "%'";
        }

        $sqlEventNoData = "  SELECT EVENT_NO FROM " . ITEMMASTER . "  WHERE ITEM_TYPE='EVENTS' AND IS_ACTIVE='1' AND NOW()>EVENT_END_DATETIME AND DELETE_FLAG =0 " . $whereEventNoCond . " AND FK_CLIENT_ID ='" . $clientId . "' AND FK_ORG_ID ='" . $orgId . "'"; // QUERY FOR EVENT NO DATA.    ;

        $resEventNoData = fetch_rec_query($sqlEventNoData); // RESULT FOR CUSTOMER DATA.
        if (count($resEventNoData) > 0) {
            foreach ($resEventNoData as $keyEventNo => $valueEventNo) { // CUSTOMER DATA LOOP
                $eventNoArr[$keyEventNo]['eventNo'] = $valueEventNo['EVENT_NO'];
            }
            $result = array("status" => SCS, "data" => $eventNoArr);
            http_response_code(200);
        } else {

            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.1.8.2018
//ADD BY DEVIKA 2.8.2018
function getEventDetails($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    //$studentId = isset($postData['postData']['studentId']) ? addslashes(trim($postData['postData']['studentId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($itemId)) {
        $whereCond = "";
        if (!empty($itemId)) {
            $whereCond = " AND IM.PK_ITEM_ID ='" . $itemId . "' AND IM.ITEM_TYPE='EVENTS'";
        }
        $sqlEventDetail = "SELECT IM.PK_ITEM_ID,IM.EVENT_NO,IM.ITEM_NAME,IM.ITEM_TYPE,IM.ITEM_PRICE,IM.DESCRIPTION,IM.QTY,IM.EVENT_START_DATETIME,IM.EVENT_END_DATETIME,IM.EVENT_VENUE FROM " . ITEMMASTER . " IM  WHERE IM.DELETE_FLAG=0 " . $whereCond . " AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "' ";
        $resultEventDetail = fetch_rec_query($sqlEventDetail);
        $eventDetailArr = array();
        if (count($resultEventDetail) > 0) {
            foreach ($resultEventDetail as $keyEventDetail => $valueEventDetail) {
                $eventDetailArr[$keyEventDetail]['itemId'] = $valueEventDetail['PK_ITEM_ID'];
                $eventDetailArr[$keyEventDetail]['eventNo'] = $valueEventDetail['EVENT_NO'];
                $eventDetailArr[$keyEventDetail]['itemName'] = $valueEventDetail['ITEM_NAME'];
                $eventDetailArr[$keyEventDetail]['itemType'] = $valueEventDetail['ITEM_TYPE'];
                $eventDetailArr[$keyEventDetail]['itemPrice'] = $valueEventDetail['ITEM_PRICE'];
                $eventDetailArr[$keyEventDetail]['description'] = $valueEventDetail['DESCRIPTION'];
                $eventDetailArr[$keyEventDetail]['qty'] = $valueEventDetail['QTY'];
                $eventDetailArr[$keyEventDetail]['eventStartDate'] = date('d-m-Y H:i:s', $valueEventDetail['EVENT_START_DATETIME']);
                $eventDetailArr[$keyEventDetail]['eventEndDate'] = date('d-m-Y H:i:s', $valueEventDetail['EVENT_END_DATETIME']);
                $eventDetailArr[$keyEventDetail]['eventVenue'] = $valueEventDetail['EVENT_VENUE'];


                $sqlStudentData = "SELECT EM.FK_ITEM_ID,EM.QTY,SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,EM.FK_INVOICE_ID FROM " . STUDENTMASTER . " SM INNER JOIN " . EVENTSBOOKING . " EM ON SM.PK_STUD_ID =EM.FK_STUD_ID  WHERE EM.FK_ITEM_ID='" . $valueEventDetail['PK_ITEM_ID'] . "' AND SM.DELETE_FLAG=0 AND SM.FK_CLIENT_ID = '" . $clientId . "' AND SM.FK_ORG_ID = '" . $orgId . "'";
                $resultStudentData = fetch_rec_query($sqlStudentData);
                $studentDataArr = array();
                if (count($resultStudentData) > 0) {
                    foreach ($resultStudentData as $keyStudentData => $valueStudentData) {
                        $studentDataArr[$keyStudentData]['studentId'] = $valueStudentData['PK_STUD_ID'];
                        $studentDataArr[$keyStudentData]['fkItemId'] = $valueStudentData['FK_ITEM_ID'];
                        $studentDataArr[$keyStudentData]['qty'] = $valueStudentData['QTY'];
                        $studentDataArr[$keyStudentData]['membershipNo'] = $valueStudentData['MEMBERSHIP_NUMBER'];
                        $studentDataArr[$keyStudentData]['fullName'] = $valueStudentData['FIRST_NAME'] . " " . $valueStudentData['LAST_NAME'];
                        $studentDataArr[$keyStudentData]['invoiceId'] = $valueStudentData['FK_INVOICE_ID'];
                    }
                    $eventDetailArr[$keyEventDetail]['studentData'] = array_values($studentDataArr);
                } else {
                    $eventDetailArr[$keyEventDetail]['studentData'] = NORECORDS;
                }
            }
            $result = array("status" => SCS, "data" => array_values($eventDetailArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//END BY DEVIKA.02.8.2018.

function getEventInvoiceData($postData) {
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $userId = isset($postData['postData']['userId']) ? addslashes(trim($postData['postData']['userId'])) : "";
    $itemId = isset($postData['postData']['itemId']) ? addslashes(trim($postData['postData']['itemId'])) : "";
    $invoiceId = isset($postData['postData']['invoiceId']) ? addslashes(trim($postData['postData']['invoiceId'])) : "";

    if (!empty($clientId) && !empty($orgId) && !empty($itemId)) {
        $whereCond = "";
        if (!empty($itemId)) {
            $whereCond = " AND IM.INVOICE_REF_ID ='" . $itemId . "' AND IM.INVOICE_TYPE='EVENTS' AND C_INVOICE_ID ='" . $invoiceId . "'";
        }
        $sqleventInvoice = "SELECT IM.INVOICE_REF_ID,IM.C_INVOICE_ID,IM.INVOICE_DATE,IM.INVOICE_NAME,IM.PERIOD_FROM,IM.PERIOD_TO,IM.FK_STUDENT_ID,SM.PK_STUD_ID,SM.MEMBERSHIP_NUMBER,SM.FIRST_NAME,SM.LAST_NAME,SM.ADDRESS_LINE1,SM.ADDRESS_LINE2,SM.ADDRESS_LINE3,SM.PINCODE,SM.FK_CITY_ID,SM.FK_STATE_ID,SM.FK_COUNTRY_ID,CT.CITY_NAME,CT.STATE_NAME,CO.COUNTRY_NAME,ITM.EVENT_NO,ITM.ITEM_NAME,ITM.HSN_CODE,ITM.CGST,ITM.SGST,ITM.ITEM_PRICE,ITM.DESCRIPTION,ITM.EVENT_START_DATETIME,ITM.EVENT_END_DATETIME,ITM.EVENT_VENUE FROM " . INVOICEMASTER . " IM INNER JOIN " . ITEMMASTER . " ITM ON ITM.PK_ITEM_ID=IM.INVOICE_REF_ID LEFT JOIN " . STUDENTMASTER . " SM ON SM.PK_STUD_ID=IM.FK_STUDENT_ID LEFT JOIN " . COUNTRY . " CO ON CO.PK_COUNTRY_ID = SM.FK_COUNTRY_ID LEFT JOIN " . CITY . " CT ON CT.PK_CITY_ID=SM.FK_CITY_ID  WHERE IM.DELETE_FLAG=0 " . $whereCond . " AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "'";
        $resultEventInvoice = fetch_rec_query($sqleventInvoice);
        $eventInvoiceArr = array();
        if (count($resultEventInvoice) > 0) {
            foreach ($resultEventInvoice as $keyEventInvoice => $valueEventInvoice) {
                $eventInvoiceArr[$keyEventInvoice]['itemId'] = $valueEventInvoice['INVOICE_REF_ID'];
                $eventInvoiceArr[$keyEventInvoice]['cInvoiceId'] = $valueEventInvoice['C_INVOICE_ID'];
                $eventInvoiceArr[$keyEventInvoice]['invoiceDate'] = $valueEventInvoice['INVOICE_DATE'];
                $eventInvoiceArr[$keyEventInvoice]['invoiceName'] = $valueEventInvoice['INVOICE_NAME'];
                $eventInvoiceArr[$keyEventInvoice]['periodFrom'] = $valueEventInvoice['PERIOD_FROM'];
                $eventInvoiceArr[$keyEventInvoice]['periodFrom'] = $valueEventInvoice['PERIOD_FROM'];
                $eventInvoiceArr[$keyEventInvoice]['periodTo'] = $valueEventInvoice['PERIOD_TO'];
                $eventInvoiceArr[$keyEventInvoice]['memberShipNo'] = $valueEventInvoice['MEMBERSHIP_NUMBER'];
                $eventInvoiceArr[$keyEventInvoice]['fullName'] = $valueEventInvoice['FIRST_NAME'] . " " . $valueEventInvoice['LAST_NAME'];
                $eventInvoiceArr[$keyEventInvoice]['address'] = $valueEventInvoice['ADDRESS_LINE1'] . ", " . $valueEventInvoice['ADDRESS_LINE2'] . ", " . $valueEventInvoice['ADDRESS_LINE3'];
                $eventInvoiceArr[$keyEventInvoice]['pinCode'] = $valueEventInvoice['PINCODE'];
                $eventInvoiceArr[$keyEventInvoice]['cityName'] = $valueEventInvoice['CITY_NAME'];
                $eventInvoiceArr[$keyEventInvoice]['stateName'] = $valueEventInvoice['STATE_NAME'];
                $eventInvoiceArr[$keyEventInvoice]['countryName'] = $valueEventInvoice['COUNTRY_NAME'];
                $eventInvoiceArr[$keyEventInvoice]['eventNo'] = $valueEventInvoice['EVENT_NO'];
                $eventInvoiceArr[$keyEventInvoice]['itemName'] = $valueEventInvoice['ITEM_NAME'];
                $eventInvoiceArr[$keyEventInvoice]['itemPrice'] = $valueEventInvoice['ITEM_PRICE'];
                $eventInvoiceArr[$keyEventInvoice]['description'] = $valueEventInvoice['DESCRIPTION'];
                $eventInvoiceArr[$keyEventInvoice]['hsnCode'] = $valueEventInvoice['HSN_CODE'];
                $eventInvoiceArr[$keyEventInvoice]['cgst'] = $valueEventInvoice['CGST'];
                $eventInvoiceArr[$keyEventInvoice]['sgst'] = $valueEventInvoice['SGST'];
                $eventInvoiceArr[$keyEventInvoice]['eventStartDate'] = date('d-m-Y H:i:s', $valueEventInvoice['EVENT_START_DATETIME']);
                $eventInvoiceArr[$keyEventInvoice]['eventEndDate'] = date('d-m-Y H:i:s', $valueEventInvoice['EVENT_END_DATETIME']);
                $eventInvoiceArr[$keyEventInvoice]['eventVenue'] = $valueEventInvoice['EVENT_VENUE'];


                $sqlChargeData = "SELECT ITEM_NAME,TAX_CGST,TAX_SGST,HSN_CODE,ITEM_TOTAL_PRICE,INVOICE_ID,QTY_OF_SESSION FROM " . CHARGEMASTER . " WHERE INVOICE_ID='" . $valueEventInvoice['C_INVOICE_ID'] . "' AND DELETE_FLAG=0 AND INVOICE_FLAG='1' AND IS_TRANSFER='0' AND FK_CLIENT_ID = '" . $clientId . "' AND FK_ORG_ID = '" . $orgId . "'";
                $resultStudentChargeData = fetch_rec_query($sqlChargeData);
                $itemDataArr = array();
                if (count($resultStudentChargeData) > 0) {
                    foreach ($resultStudentChargeData as $keyStudentChargeData => $valueStudentChargeData) {
                        $itemDataArr[$keyStudentChargeData]['itemName'] = $valueStudentChargeData['ITEM_NAME'];
                        $itemDataArr[$keyStudentChargeData]['cgst'] = $valueStudentChargeData['TAX_CGST'];
                        $itemDataArr[$keyStudentChargeData]['sgst'] = $valueStudentChargeData['TAX_SGST'];
                        $itemDataArr[$keyStudentChargeData]['hsnCode'] = $valueStudentChargeData['HSN_CODE'];
                        $itemDataArr[$keyStudentChargeData]['itemTotalPrice'] = $valueStudentChargeData['ITEM_TOTAL_PRICE'];
                        $itemDataArr[$keyStudentChargeData]['invoiceId'] = $valueStudentChargeData['INVOICE_ID'];
                        $itemDataArr[$keyStudentChargeData]['qty'] = $valueStudentChargeData['QTY_OF_SESSION'];
                    }
                    $eventInvoiceArr[$keyEventInvoice]['itemData'] = array_values($itemDataArr);
                } else {
                    $eventInvoiceArr[$keyEventInvoice]['itemData'] = NORECORDS;
                }
            }
            $result = array("status" => SCS, "data" => array_values($eventInvoiceArr));
            http_response_code(200);
        } else {
            $result = array("status" => NORECORDS);
            http_response_code(400);
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }

    return $result;
}

//ADD BY DEVIKA.10.8.2018
function eventSearch($postData) {
    // POST DATA
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $keyword = isset($postData['postData']['keyword']) ? addslashes(trim($postData['postData']['keyword'])) : "";

    $eventArr = array();

    if (!empty($clientId) && !empty($orgId) && !empty($keyword) && strlen($keyword) >= 2) {
        $whereEventCond = "";
        if (!empty($keyword)) {
            $whereEventCond = " AND (ITEM_NAME LIKE '%" . $keyword . "%' OR EVENT_NO LIKE '%" . $keyword . "%')";
        }

        $sqlEventDetail = "SELECT IM.PK_ITEM_ID,IM.ITEM_NAME,IM.ITEM_TYPE,IM.EVENT_NO FROM " . ITEMMASTER . " IM  WHERE IM.DELETE_FLAG=0 " . $whereEventCond . " AND IM.FK_CLIENT_ID = '" . $clientId . "' AND IM.FK_ORG_ID = '" . $orgId . "' AND ITEM_TYPE='EVENTS' AND IM.IS_ACTIVE ='1'";

        $resEventData = fetch_rec_query($sqlEventDetail); // RESULT FOR EVENT DATA.
        if (count($resEventData) > 0) {
            foreach ($resEventData as $keyEvent => $valueEvent) { // EVENT DATA LOOP
                $eventArr[$keyEvent]['itemId'] = $valueEvent['PK_ITEM_ID'];
                $eventArr[$keyEvent]['itemName'] = $valueEvent['ITEM_NAME'];
                $eventArr[$keyEvent]['eventNo'] = $valueEvent['EVENT_NO'];
            }
            $result = array("status" => SCS, "data" => $eventArr);
            http_response_code(200);
        } else {
            //ADD BY DEVIKA.17.7.2018
            $eventSearchArr = Array();
            $eventSearchArr[0]['itemId'] = "";
            $eventSearchArr[0]['itemName'] = "";

            $result = array("status" => SCS, "data" => $eventSearchArr);
            http_response_code(200);
            //END BY DEVIKA.17.7.2018
        }
    } else {
        $result = array("status" => PARAM);
        http_response_code(400);
    }
    return $result;
}

//END BY DEVIKA.10.8.2018

function upcomingEventList($postData) {
    $orgId = isset($postData['postData']['orgId']) ? addslashes(trim($postData['postData']['orgId'])) : "";
    $clientId = isset($postData['postData']['clientId']) ? addslashes(trim($postData['postData']['clientId'])) : "";
    $today = date("Y-m-d");
    $whereClientIdCond = $whereOrgIdCond = "";

    if (!empty($clientId)) {
        $whereClientIdCond = " AND IM.FK_CLIENT_ID = '" . $clientId . "'";
    }
    if (!empty($orgId)) {
        $whereOrgIdCond = "AND IM.FK_ORG_ID = '" . $orgId . "'";
    }

    $sqlEventDetail = "SELECT IM.PK_ITEM_ID,IM.ITEM_NAME,DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%d-%m-%Y') AS START_DATE,DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%d-%m-%Y') AS END_DATE,DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%H:%i:%s') AS START_DATE_TIME,DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_END_DATETIME), '%H:%i:%s') AS END_DATE_TIME,IM.ITEM_PRICE FROM " . ITEMMASTER . " IM  WHERE IM.DELETE_FLAG='0' " . $whereClientIdCond . $whereOrgIdCond . "AND DATE_FORMAT(FROM_UNIXTIME(IM.EVENT_START_DATETIME), '%Y-%m-%d') >='" . $today . "'  AND ITEM_TYPE='EVENTS' AND IS_ACTIVE='1'";

    $resEventData = fetch_rec_query($sqlEventDetail); // RESULT FOR EVENT DATA.
    if (count($resEventData) > 0) {
        foreach ($resEventData as $keyEvent => $valueEvent) { // EVENT DATA LOOP
            $eventArr[$keyEvent]['eventName'] = $valueEvent['ITEM_NAME'];
            $eventArr[$keyEvent]['eventStartDate'] = $valueEvent['START_DATE'];
            $eventArr[$keyEvent]['eventEndDate'] = $valueEvent['END_DATE'];
            $eventArr[$keyEvent]['eventStartTime'] = $valueEvent['START_DATE_TIME'];
            $eventArr[$keyEvent]['eventEndTime'] = $valueEvent['END_DATE_TIME'];
            $eventArr[$keyEvent]['eventPrice'] = $valueEvent['ITEM_PRICE'];
        }
        $result = array("status" => SCS, "data" => $eventArr);
        http_response_code(200);
    } else {
        $result = array("status" => NORECORDS);
        http_response_code(400);
    }
    return $result;
}

?>
